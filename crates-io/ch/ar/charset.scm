(define-module (crates-io ch ar charset) #:use-module (crates-io))

(define-public crate-charset-0.1.0 (c (n "charset") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.9") (d #t) (k 0)))) (h "0xdv74hh1h9gwqaiinldvgwa074x5hiar3qp7hmls1j5b4imk51p")))

(define-public crate-charset-0.1.1 (c (n "charset") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.11") (d #t) (k 0)))) (h "110km1lg9xg47sm84nha8x8ax1lfz7zaac2gf0fv4293q3ygdvlm")))

(define-public crate-charset-0.1.2 (c (n "charset") (v "0.1.2") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wzwnck82maqa03hvpprpd1zvnzmzxpkqna4pxnf4g8wvxj6whjg")))

(define-public crate-charset-0.1.3 (c (n "charset") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0iidr9d5a0jghkaj0888skm3c6368ff07nxmzwmwr8hj3afhgs8q")))

