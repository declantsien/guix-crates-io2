(define-module (crates-io ch ar charx) #:use-module (crates-io))

(define-public crate-charx-1.0.0 (c (n "charx") (v "1.0.0") (h "0rs2b6qlxc0p3nrlqif0654f5c8fpiys1f6anv4v0pk54h7brs85")))

(define-public crate-charx-1.0.1 (c (n "charx") (v "1.0.1") (h "1vq2h0imrrpz4v9q30fm54q383fxbgyjvaf67xnwqcy3r5ya5401") (r "1.56.0")))

(define-public crate-charx-1.0.2 (c (n "charx") (v "1.0.2") (h "0rj7jz9qxh37dfwqq2bk0w8dfwc31r8z1gvshivdyx7padykv43k") (r "1.56.0")))

