(define-module (crates-io ch ar chargrid_render) #:use-module (crates-io))

(define-public crate-chargrid_render-0.1.0 (c (n "chargrid_render") (v "0.1.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "rgb24") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "04jrcrpjwpp8ik843awnry58rq1ij4zphr2qigg86l1ccl7m83rc") (f (quote (("serialize" "serde" "grid_2d/serialize" "rgb24/serialize"))))))

(define-public crate-chargrid_render-0.1.1 (c (n "chargrid_render") (v "0.1.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "rgb24") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "14bp1xp8r0znhkhs4wdw82k5jl4hkq2hfggh0sc28p24i917ypzi") (f (quote (("serialize" "serde" "grid_2d/serialize" "rgb24/serialize"))))))

(define-public crate-chargrid_render-0.2.0 (c (n "chargrid_render") (v "0.2.0") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb24") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1483nqkrp4sr1yaglml02iw41nig144r8aaad3wgyvm9zn0ypngp") (f (quote (("serialize" "serde" "grid_2d/serialize" "rgb24/serialize"))))))

