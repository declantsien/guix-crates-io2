(define-module (crates-io ch ar char_combinator) #:use-module (crates-io))

(define-public crate-char_combinator-1.0.0 (c (n "char_combinator") (v "1.0.0") (d (list (d (n "num-bigint") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (d #t) (k 0)))) (h "1cydnxrfc6qcj5spzllcmyfjln9i4sbnjb11qmjcfjk9njv8b4nb") (f (quote (("default") ("bigint" "num-bigint"))))))

(define-public crate-char_combinator-1.1.0 (c (n "char_combinator") (v "1.1.0") (d (list (d (n "num-bigint") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h6sd598dmvq5cz8vjd6fxaw7n52crmdcj5y6638i72gpw5ai6i3") (f (quote (("serialize" "serde") ("default") ("bigint" "num-bigint"))))))

(define-public crate-char_combinator-1.1.2 (c (n "char_combinator") (v "1.1.2") (d (list (d (n "num-bigint") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0a4crxycff5j38zk8jh9wh4mcg6fdbj7k2kpz41iqdbhc25wy9ga") (f (quote (("serialize" "serde/derive") ("default") ("bigint_serialize" "num-bigint/serde" "bigint" "serialize") ("bigint" "num-bigint"))))))

