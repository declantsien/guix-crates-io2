(define-module (crates-io ch ar charsets) #:use-module (crates-io))

(define-public crate-charsets-0.1.0 (c (n "charsets") (v "0.1.0") (h "139413bwj9rx6q7jsdcncy11n2xznnx6zrv4wk5zyf7yh81jfmn9")))

(define-public crate-charsets-0.1.1 (c (n "charsets") (v "0.1.1") (h "1lz6w581dkmvgnsr6h15qyl2pmfhz3gbqr901kaiycn143gf0jgi")))

(define-public crate-charsets-0.2.0 (c (n "charsets") (v "0.2.0") (h "0d4473qa5mc93fdw1zhlqwphw10cipxzibxppaxllr1i9x3h9j7z")))

