(define-module (crates-io ch ar char_set) #:use-module (crates-io))

(define-public crate-char_set-0.0.1 (c (n "char_set") (v "0.0.1") (d (list (d (n "integer_set") (r "^0.0.2") (d #t) (k 0)) (d (n "interval") (r "^0.0.1") (d #t) (k 0)))) (h "0lwxq8ydjwqixq65kywxnq6b5y6kx4ri2xls3j62r9pgfhxnadh1")))

