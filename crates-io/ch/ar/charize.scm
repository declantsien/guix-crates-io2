(define-module (crates-io ch ar charize) #:use-module (crates-io))

(define-public crate-charize-0.0.1 (c (n "charize") (v "0.0.1") (h "0q04w60cy8k12ym011gql4jvja5q47jfcv2kdbhwqmsggkbbhc83")))

(define-public crate-charize-0.0.2 (c (n "charize") (v "0.0.2") (h "1f3brw04nqrvnpqvgsszhzz8y1hb80z2j04aa8qprk8ckddd50d0")))

