(define-module (crates-io ch ar chardet) #:use-module (crates-io))

(define-public crate-chardet-0.1.0 (c (n "chardet") (v "0.1.0") (h "18czq8xdkglm7z2fj4zd0c99dbqw90h1ldjnz49x2l2d7bhalvly")))

(define-public crate-chardet-0.2.0 (c (n "chardet") (v "0.2.0") (h "1a7gh9mnnjs6cl1bmcq4mfw8xhvwpwy4gvn90dfz08g371pcs5p4")))

(define-public crate-chardet-0.2.1 (c (n "chardet") (v "0.2.1") (h "19ds9ryv73zndzvrikv0fx3i3d3z923bylp6r6h0ygx0q2x8l9jj")))

(define-public crate-chardet-0.2.2 (c (n "chardet") (v "0.2.2") (h "10536x4iahkh5qihf52sz1l0q8fhrabsjzav26k3h53qlwvnh5na")))

(define-public crate-chardet-0.2.3 (c (n "chardet") (v "0.2.3") (h "1flzydm6hvhvi2g7l5cx0w2czk97z4k7i68a4awcr94a2hkxkry8")))

(define-public crate-chardet-0.2.4 (c (n "chardet") (v "0.2.4") (h "0c9fa4w05a1889q0biij2mg8hs5bgz446wpvl0xh0z5nhhr5cj0s")))

