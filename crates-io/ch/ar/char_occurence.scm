(define-module (crates-io ch ar char_occurence) #:use-module (crates-io))

(define-public crate-char_occurence-0.1.0 (c (n "char_occurence") (v "0.1.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 0)))) (h "19p64ryw5aj3c228dg7m4hg34hlyxc02zqm2z7h1mx281mbgf31b")))

(define-public crate-char_occurence-0.1.1 (c (n "char_occurence") (v "0.1.1") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 0)))) (h "0aplcj097nf2j5i5461gxbq3v0zba4rx836gf4gpxz1zw8x3rw4z")))

(define-public crate-char_occurence-0.2.0 (c (n "char_occurence") (v "0.2.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 0)))) (h "1yab2nwlf89hpd5vdrsmf84w4iaawvlmxd460pjqknj5q35pca1g")))

(define-public crate-char_occurence-0.2.1 (c (n "char_occurence") (v "0.2.1") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 0)))) (h "150czqwykyips8nfwb9z16qy0dn2q8dmrjz8lyn9lvnvk62c11yw")))

