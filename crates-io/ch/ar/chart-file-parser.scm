(define-module (crates-io ch ar chart-file-parser) #:use-module (crates-io))

(define-public crate-chart-file-parser-0.1.0 (c (n "chart-file-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0a3ak8lhg5z08y1b707zm0vinivrli3qlhrd2f720r8kv2hy1k8w")))

(define-public crate-chart-file-parser-0.2.0 (c (n "chart-file-parser") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "047zbl5cz0lvjmkxdpnf3yd3y1v4xwxyykrawl54a0r11wir7zpw")))

(define-public crate-chart-file-parser-0.2.1 (c (n "chart-file-parser") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "187wxvxr4fjwjw35xkm5xsv5gp3g4ddlma9ll0fkakd5m6dyjyma")))

(define-public crate-chart-file-parser-0.3.0 (c (n "chart-file-parser") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "054c069g2kc8jgkq0rihnn0wlyllkpd3s7505y3zwqla4hgjlnsk")))

(define-public crate-chart-file-parser-0.3.1 (c (n "chart-file-parser") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0vyvv15wknlmpz5qcj8kc280a96hrf1dxvjq2afywirqzqzm0ndm")))

(define-public crate-chart-file-parser-0.3.2 (c (n "chart-file-parser") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1lya17aj6m3q5bpfjm37zrdbqicms15s8y4a1a7bhcfqjnwmnvh2")))

(define-public crate-chart-file-parser-0.3.3 (c (n "chart-file-parser") (v "0.3.3") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "14g3ljgkv44sjr9l132whnbr85f150dcz52j14f9g23sb5yziks9")))

(define-public crate-chart-file-parser-0.4.0 (c (n "chart-file-parser") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1w9ngicgdxzm1q4gx9jkcj9lir5n9mqyx76vzbb91rqfvvwq72qp")))

(define-public crate-chart-file-parser-0.4.1 (c (n "chart-file-parser") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0mwc2n6a9lqb469mhz786z43fchmzawybaj9n04q1dgja5zbzbin")))

(define-public crate-chart-file-parser-0.4.2 (c (n "chart-file-parser") (v "0.4.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0ripa465pbvbaac0fnmgnd7rg31bph44gqi3y1cq7w9m2m65ga2a")))

(define-public crate-chart-file-parser-0.4.3 (c (n "chart-file-parser") (v "0.4.3") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "06qqds5gf38sqrz79f8xcvm4j5as4csfvmidv4dxhcsa18kq5g74")))

(define-public crate-chart-file-parser-0.4.4 (c (n "chart-file-parser") (v "0.4.4") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "15bd1kav302d7d47hi88h3170727rnvmlpajlhjk6argwdynq6pa")))

(define-public crate-chart-file-parser-0.4.5 (c (n "chart-file-parser") (v "0.4.5") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1vp447rgyj4vha0sn4g0m3fixdhlhr5xabih4i1xfn00rj43ga6f")))

(define-public crate-chart-file-parser-0.4.6 (c (n "chart-file-parser") (v "0.4.6") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1yqp4phq07g7k89jd0qpphhkc9mh4jqnih2brcx8k519n1pkqks2")))

