(define-module (crates-io ch ar charmly) #:use-module (crates-io))

(define-public crate-charmly-0.1.0 (c (n "charmly") (v "0.1.0") (h "1wsq81y2zbgi9ajqnk2gbqgy9g9wsrz3y6xn0l4kr61a42jg5z6z") (y #t)))

(define-public crate-charmly-0.1.0-alpha (c (n "charmly") (v "0.1.0-alpha") (h "1w6sb592wadlkxr4k9xvcqzls6kp2v8czan3qhchz0annc644d2h")))

