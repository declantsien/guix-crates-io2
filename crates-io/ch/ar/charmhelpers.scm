(define-module (crates-io ch ar charmhelpers) #:use-module (crates-io))

(define-public crate-charmhelpers-0.1.0 (c (n "charmhelpers") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0bln1iyqgs93j4935z7gwlsisf2ah249mxkznip9706glibcd6f1")))

(define-public crate-charmhelpers-0.1.1 (c (n "charmhelpers") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "06alp4k0lfs432da3hha2rv7r50p4l0l92wdxq7zqs03x7yyzzaj")))

(define-public crate-charmhelpers-0.1.2 (c (n "charmhelpers") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "161zr4rl3pm5lgvzhs5bhi6d2a1wmdwaarbr8sl9171fn3cndl7c")))

(define-public crate-charmhelpers-0.1.3 (c (n "charmhelpers") (v "0.1.3") (d (list (d (n "juju") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "02cy6mwwsxpk1250ljj3n9gd59q29f9h4i669msvfp0pgjsar0yj")))

