(define-module (crates-io ch ar character_converter) #:use-module (crates-io))

(define-public crate-character_converter-0.1.0 (c (n "character_converter") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)))) (h "1yszq98ga3q07zay4ywlra583vcqi7xdnwbyiikygp6vrpg21n1z")))

(define-public crate-character_converter-0.1.1 (c (n "character_converter") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)))) (h "1d2ph16pilffwfy5yxmcag5imdp84v8ibjybqv5wrw80k0yzj3fn")))

(define-public crate-character_converter-0.1.4 (c (n "character_converter") (v "0.1.4") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)))) (h "0ziidgikscmamylc690paca6ba56kzlnj8x6vgynyz5gc485z30y")))

(define-public crate-character_converter-0.1.6 (c (n "character_converter") (v "0.1.6") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)))) (h "03plhia7zb5i5gi7v9g8vxpgb2ddf7gmx8p9b2mcym33cbp6in3h")))

(define-public crate-character_converter-1.0.0 (c (n "character_converter") (v "1.0.0") (d (list (d (n "bincode") (r ">=1.2.1, <2.0.0") (d #t) (k 0)))) (h "0p0yg30zmakgdxfha9r7af17g8r4hnb09diw0cfa5mh9rrz4fj1f")))

(define-public crate-character_converter-2.0.0 (c (n "character_converter") (v "2.0.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 1)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1c7cn5yp41di0fgwpl6s5175zkgc3i0xh2rczz4z1fblgxph4yym") (f (quote (("bench"))))))

(define-public crate-character_converter-2.1.0 (c (n "character_converter") (v "2.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 1)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "00mdmrszf7am2hxw35xwyjyymhy3i2fzbzjj3iabc94ij5p4q1np") (f (quote (("bench"))))))

(define-public crate-character_converter-2.1.1 (c (n "character_converter") (v "2.1.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 1)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0kjw6hl97al05w388bcig33a3jabv35645n30k0scs8h9zmwx4v9") (f (quote (("bench"))))))

(define-public crate-character_converter-2.1.2 (c (n "character_converter") (v "2.1.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 1)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0xmk07q6a58mpvsa0qwwrc6p93f9gldzfkg609lg0k1rsfl0gw4b") (f (quote (("bench"))))))

(define-public crate-character_converter-2.1.3 (c (n "character_converter") (v "2.1.3") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 1)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "146362j9v8q58h0gf2mkpx1ijdfb9gn8gnravq9yszrjmajqflx7") (f (quote (("bench"))))))

(define-public crate-character_converter-2.1.4 (c (n "character_converter") (v "2.1.4") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 1)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "075vjf93q1mj111afqf2inmq8dms930klaykh58hk9siakqm9sql") (f (quote (("bench"))))))

(define-public crate-character_converter-2.1.5 (c (n "character_converter") (v "2.1.5") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 1)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "15vphgprq2kpl1w9hn727mh3w1ynmy0kr8wynwlv7syal3qx96gk") (f (quote (("bench"))))))

