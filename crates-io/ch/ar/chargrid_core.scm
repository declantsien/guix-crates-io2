(define-module (crates-io ch ar chargrid_core) #:use-module (crates-io))

(define-public crate-chargrid_core-0.1.0 (c (n "chargrid_core") (v "0.1.0") (d (list (d (n "chargrid_input") (r "^0.3") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)))) (h "1yp3hhnh1kasll276zhn17llv2kq4qpbmc2dlqfb1ndhlkmjai4w") (f (quote (("serialize" "chargrid_input/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.1.1 (c (n "chargrid_core") (v "0.1.1") (d (list (d (n "chargrid_input") (r "^0.3") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)))) (h "1dqp8gjfkzl3axsi4gqiym84avrpma686my59mqy9lklw4s7gpzw") (f (quote (("serialize" "chargrid_input/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.1.2 (c (n "chargrid_core") (v "0.1.2") (d (list (d (n "chargrid_input") (r "^0.3") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)))) (h "1z6cpa5fj7cx8jr6jgg0midgrzpgfys2am6kfig2jvnnmwi1kiwy") (f (quote (("serialize" "chargrid_input/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.1.3 (c (n "chargrid_core") (v "0.1.3") (d (list (d (n "chargrid_input") (r "^0.3") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)))) (h "0y6fdnb58y9sbplzy0znj7lbdpyjmqjmgmlyjimbgkm3dv7k2vzv") (f (quote (("serialize" "chargrid_input/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.1.4 (c (n "chargrid_core") (v "0.1.4") (d (list (d (n "chargrid_input") (r "^0.3") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)))) (h "0cgsdlxmnmj1mdjn7b7wrsvrc983dmf8dx9x8vbz76s6k5l85hlm") (f (quote (("serialize" "chargrid_input/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.1.5 (c (n "chargrid_core") (v "0.1.5") (d (list (d (n "chargrid_input") (r "^0.3") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)))) (h "1pa61fd38fld5jmrwicvlvd7kdalkm9401q8msnmm410sq3q61vr") (f (quote (("serialize" "chargrid_input/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.1.6 (c (n "chargrid_core") (v "0.1.6") (d (list (d (n "chargrid_input") (r "^0.3") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)))) (h "1l10l3z9rmavr1m9rhc8z65m2zxpz0laq31dqk9n7a85h514cbjj") (f (quote (("serialize" "chargrid_input/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.2.0 (c (n "chargrid_core") (v "0.2.0") (d (list (d (n "chargrid_input") (r "^0.4") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)))) (h "0pz9f9rpwl6l4pzdnky8sv5cdsxc9g5vdppwxyd6zzjswb1p5644") (f (quote (("serialize" "chargrid_input/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.2.1 (c (n "chargrid_core") (v "0.2.1") (d (list (d (n "chargrid_input") (r "^0.4") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0x8gwlsj7spfbyfzf4x4gq1ksrb17s3wcs3k4nykb45y4jwqsapw") (f (quote (("serialize" "serde" "chargrid_input/serialize" "rgb_int/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.2.2 (c (n "chargrid_core") (v "0.2.2") (d (list (d (n "chargrid_input") (r "^0.4") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1sibqkg9vbd8z6qx6dpcyp6ibr73cxi7lx1dmy0rysz498s7agl9") (f (quote (("serialize" "serde" "chargrid_input/serialize" "rgb_int/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.3.0 (c (n "chargrid_core") (v "0.3.0") (d (list (d (n "chargrid_input") (r "^0.5") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0zd2isgqndnkg8zz0s23v539xmpwy1m26d8dlx4804yhx34960yv") (f (quote (("serialize" "serde" "chargrid_input/serialize" "rgb_int/serialize") ("gamepad" "chargrid_input/gamepad"))))))

(define-public crate-chargrid_core-0.4.0 (c (n "chargrid_core") (v "0.4.0") (d (list (d (n "chargrid_input") (r "^0.6") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rgb_int") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1ax2d9gl2yj2ycj642ix83dl2ybd10zh6m6g9ch99x6yjd4wbspa") (f (quote (("serialize" "serde" "chargrid_input/serialize" "rgb_int/serialize") ("gamepad" "chargrid_input/gamepad"))))))

