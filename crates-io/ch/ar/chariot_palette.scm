(define-module (crates-io ch ar chariot_palette) #:use-module (crates-io))

(define-public crate-chariot_palette-0.1.0 (c (n "chariot_palette") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "1jf3pc28x3dbx0168fn2ycpia42ks3j3n8g73v6v3y0v9s5ldk2h")))

(define-public crate-chariot_palette-0.1.1 (c (n "chariot_palette") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "1ckx53ljxxy687pym7fx9960g0xdnw1jjf9s043ridyifx44x4km")))

(define-public crate-chariot_palette-0.1.3 (c (n "chariot_palette") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "1xw2fvx86nshc0wnqpvcm0zvwna6dmjndzn3f135g3wi16hahpj7")))

(define-public crate-chariot_palette-0.1.4 (c (n "chariot_palette") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "16r2gq011b3lcyq0jy9q1dly2px7rm8qx3fcg1vassfk54j0n9nx")))

(define-public crate-chariot_palette-0.1.5 (c (n "chariot_palette") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "1h2560xpraw2926ibq2pnvq6rbnr706d82a6mbcm7k9q8v7fb7m6")))

