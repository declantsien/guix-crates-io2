(define-module (crates-io ch ar chargrid_decorator) #:use-module (crates-io))

(define-public crate-chargrid_decorator-0.1.0 (c (n "chargrid_decorator") (v "0.1.0") (d (list (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "00hgbvn7n669qy7ahbsyz6yykzr31bsw7r5g0633vhvinkvlymba") (f (quote (("serialize" "serde" "chargrid_render/serialize"))))))

(define-public crate-chargrid_decorator-0.1.1 (c (n "chargrid_decorator") (v "0.1.1") (d (list (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0l2ifxx2ryp52v0nqg9ykra6w1ncfh9xbv2n5lmqikx2mql9rr1k") (f (quote (("serialize" "serde" "chargrid_render/serialize"))))))

(define-public crate-chargrid_decorator-0.2.0 (c (n "chargrid_decorator") (v "0.2.0") (d (list (d (n "chargrid_render") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "11dj8gl1bicdswc071pd446asass8n8yab9azsqxmkka5k4nynsj") (f (quote (("serialize" "serde" "chargrid_render/serialize"))))))

