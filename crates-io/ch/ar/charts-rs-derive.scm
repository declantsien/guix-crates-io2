(define-module (crates-io ch ar charts-rs-derive) #:use-module (crates-io))

(define-public crate-charts-rs-derive-0.1.0 (c (n "charts-rs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0f6lcdbdnknc7apj7q34ag60xlk24mqq0j6l5yq6pv1xq5axi06w")))

(define-public crate-charts-rs-derive-0.1.1 (c (n "charts-rs-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0bxww2pf8cky1n2ignf6q48zx0mhcblyrn0kg1f072478i5a2p4w")))

(define-public crate-charts-rs-derive-0.1.2 (c (n "charts-rs-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "1pnci68xmix5a1853pziydwgja7r7lzz9zlq7r3ivilvzx05sakq")))

(define-public crate-charts-rs-derive-0.1.3 (c (n "charts-rs-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "180jawwhyvbd7v5hi82dr571d6qljdsgvgra6f2m1vdziqc2kgwf")))

(define-public crate-charts-rs-derive-0.1.4 (c (n "charts-rs-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0jm7k8axknmliwh7fj8hnw077j2wbpn0j3ah0pw9ig1q21xx6z8x")))

(define-public crate-charts-rs-derive-0.1.5 (c (n "charts-rs-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0nydgwsbslarmmqalk0zi9wlsf7h8h2w84s8pn1j7170yjhjj8v7")))

(define-public crate-charts-rs-derive-0.1.6 (c (n "charts-rs-derive") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "019j71sigvyw105fjsricvc1y9ik43ql5vxn0wiipq2bjwn9z55b")))

(define-public crate-charts-rs-derive-0.1.7 (c (n "charts-rs-derive") (v "0.1.7") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1iv2q7jb6lnkf9hs6bydjxk080hjs1sh4fghybcmbmw7b2szlnfr")))

(define-public crate-charts-rs-derive-0.1.8 (c (n "charts-rs-derive") (v "0.1.8") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0lpgrwy17k5lh92scbd6b5y21xkhi97bl0d4xf37kadqkrnigz7y")))

(define-public crate-charts-rs-derive-0.1.9 (c (n "charts-rs-derive") (v "0.1.9") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0lrqnf1ncnisirhywx7lj870sswm8i0fnd6r87r26vmmxhjqix78")))

(define-public crate-charts-rs-derive-0.1.10 (c (n "charts-rs-derive") (v "0.1.10") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0y5n5ir22z9lj5w8kpf5nn09xjgjfl7r5bdxwz54ziwdsjbwy81h")))

(define-public crate-charts-rs-derive-0.1.11 (c (n "charts-rs-derive") (v "0.1.11") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0m3gbrsk3kvmmqkv325dn16vlh4s12wf67pgimqa6mv3ickqip5k")))

(define-public crate-charts-rs-derive-0.1.12 (c (n "charts-rs-derive") (v "0.1.12") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0x05f2c26nd1j4xx6bilfx3wnrd18mc8fr174pvvwz7c86scl0sb")))

(define-public crate-charts-rs-derive-0.1.13 (c (n "charts-rs-derive") (v "0.1.13") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1d5g5phsf4jziyqvxxxmdrwlg4659wq7769hmicsb85czwkjx4bv")))

(define-public crate-charts-rs-derive-0.1.14 (c (n "charts-rs-derive") (v "0.1.14") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1zxpj56gjidmrx9xz185vym457vz9067dsznlgjavjf7nzal6aj7")))

(define-public crate-charts-rs-derive-0.1.15 (c (n "charts-rs-derive") (v "0.1.15") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (d #t) (k 0)))) (h "0nf9i6c3nm021gwi9cinlr6hc2mpgddinqmzxyrxkgds8axfql2k")))

(define-public crate-charts-rs-derive-0.1.16 (c (n "charts-rs-derive") (v "0.1.16") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0bn0cm2fv1fw2mw2q3r89j7g240yy19cscl66jwxqmahkmbk2w0y")))

(define-public crate-charts-rs-derive-0.1.17 (c (n "charts-rs-derive") (v "0.1.17") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0053b6vkap26f2bqd6qikimrv10sdf6yvhi366sw4r42gbaqjqdc")))

(define-public crate-charts-rs-derive-0.1.18 (c (n "charts-rs-derive") (v "0.1.18") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0sm0g06rrhrzmjh1nxn0a4k6fzlwh31q81mg9fyy4ih5pn4a92rw")))

(define-public crate-charts-rs-derive-0.1.19 (c (n "charts-rs-derive") (v "0.1.19") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1zsnlbx850h8hxbx61bxs4ymmg54qcs4kx5c5mpwf6m0723ch0d8")))

(define-public crate-charts-rs-derive-0.1.20 (c (n "charts-rs-derive") (v "0.1.20") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0q88fqh0sd4rd45a87mv0vm9ab148gjim0xwvk31n270fdc4x4in")))

(define-public crate-charts-rs-derive-0.1.21 (c (n "charts-rs-derive") (v "0.1.21") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1iy11cfrxgcmzskvcqw6ihla86hhzvvwmi27kfksgmvnhg4nm2v9")))

(define-public crate-charts-rs-derive-0.1.22 (c (n "charts-rs-derive") (v "0.1.22") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "01g50mrls0i92g2q15vxcs3fp6caxbd7bybgmrz1b7dajvr209cn")))

(define-public crate-charts-rs-derive-0.1.23 (c (n "charts-rs-derive") (v "0.1.23") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0a76dx9q3dv53j5fzdsx7qqfz9580s52nk3kyjdizk00m0akwgik")))

(define-public crate-charts-rs-derive-0.2.0 (c (n "charts-rs-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1cnpspw0ksq0yjfz194arb1blmm840vdhnb5zcy4kv2mfbq0qgr7")))

(define-public crate-charts-rs-derive-0.2.1 (c (n "charts-rs-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "11ai0ynzl0bdwlnc6ash24rwk1w1bv3im3lfgaxyfmiznplrgzjw")))

