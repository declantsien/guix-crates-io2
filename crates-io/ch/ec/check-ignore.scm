(define-module (crates-io ch ec check-ignore) #:use-module (crates-io))

(define-public crate-check-ignore-1.0.0 (c (n "check-ignore") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0r2fsw21cgvi7xx65dzhbb767iz5pp18f4qq71hbxj3rdim4n5y3")))

