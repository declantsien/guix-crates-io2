(define-module (crates-io ch ec checkito) #:use-module (crates-io))

(define-public crate-checkito-0.1.0 (c (n "checkito") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "1iybarbwxs3z5gdzrdzhsij0riyzzb2y6191z4bzvz8866v0plls")))

(define-public crate-checkito-0.1.1 (c (n "checkito") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "03d23arpby0m9wa4x4l8k04d8a1w1bzf783g8qwgd3v5ahr3p1lj")))

(define-public crate-checkito-0.1.2 (c (n "checkito") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "0z9b2da85z8f75qs867v1rygwc16b76aqn0ambbr6fnibz8c34bg")))

(define-public crate-checkito-0.1.3 (c (n "checkito") (v "0.1.3") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "15rkijl6ap06ay20sgvzrxxihnp84fqh81c2lxh5zrn0b2aimk7s")))

(define-public crate-checkito-0.1.4 (c (n "checkito") (v "0.1.4") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "1j1ps3bx8xnhx4pi7qf8vcrck1468hyi4wrs6vv48cdhrkkjwrb4")))

(define-public crate-checkito-0.2.0 (c (n "checkito") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "09dk9qspal3s78pnk5gqfbdiq6q0dc4kgy3xbfyil9ghfzq91mfz")))

(define-public crate-checkito-1.0.0 (c (n "checkito") (v "1.0.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "0w70wk1830lm4dbqv9m03d5wc88irqpml1c65dv45pcm597wjwy5")))

(define-public crate-checkito-1.0.1 (c (n "checkito") (v "1.0.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "1y11mws3flc1a2dhzmyap2qd6jszhz56mwjkcgyyxsllxj5yk2iv")))

(define-public crate-checkito-1.1.0 (c (n "checkito") (v "1.1.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "1f805mdgpiq59w99xc0baxd1j87h48dmafyvwywh2aq42q90v1gx")))

(define-public crate-checkito-1.2.0 (c (n "checkito") (v "1.2.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.6.29") (d #t) (k 0)))) (h "1g1xnmb8zwf94qmr1vgrmn37w5pkzbbhwdzylci48cp9k9c07wf4")))

(define-public crate-checkito-1.2.1 (c (n "checkito") (v "1.2.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.6.29") (d #t) (k 0)))) (h "1nrgdry3929ds5mvczvxdnpiffvfyn5l1s2ip1vcnzk20f2p91ld")))

(define-public crate-checkito-1.3.0 (c (n "checkito") (v "1.3.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.7.1") (d #t) (k 0)))) (h "1s3hclx127navnxdmnqm4vgsbnk7ajkq292ysqwsi92g7iv97a9k")))

(define-public crate-checkito-1.3.1 (c (n "checkito") (v "1.3.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.7.1") (d #t) (k 0)))) (h "0pj58qhikbgw5f5cv2g24316lb5rxadsr7s47ikpkwhsx07a9v92")))

(define-public crate-checkito-1.3.2 (c (n "checkito") (v "1.3.2") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.7.1") (d #t) (k 0)))) (h "1zl0jcwidbjlkayy8j19s9px1f7v6d2hkh1m6q1zrwrm49j2r06y")))

(define-public crate-checkito-1.3.3 (c (n "checkito") (v "1.3.3") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.7.1") (d #t) (k 0)))) (h "07k34zarcpcb11w02as590g898aql4y5hq1mwy83l9sqjs5nch9n")))

(define-public crate-checkito-1.3.4 (c (n "checkito") (v "1.3.4") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.7.1") (d #t) (k 0)))) (h "16ynw6lmjnd3s8jba7amns1lrg58wd3775fx2km30anvghqrmbzv")))

(define-public crate-checkito-1.3.5 (c (n "checkito") (v "1.3.5") (d (list (d (n "fastrand") (r "^1.9") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.7") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1xcbd1jzqywkm1m20c9ilbzrr22l0zxcbdqbqr4dfz32696hnnkd")))

(define-public crate-checkito-1.3.6 (c (n "checkito") (v "1.3.6") (d (list (d (n "fastrand") (r "^1.9") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.7") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1mrhxqlkm1ys5dczhwmam9gili0yhp1s341q3rkwl19qzykb8imm")))

(define-public crate-checkito-1.3.7 (c (n "checkito") (v "1.3.7") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.7.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "16myz60n544wgci2c98b2d060197avzh7kgfi3r84viibkysazjj")))

(define-public crate-checkito-1.3.8 (c (n "checkito") (v "1.3.8") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.7") (d #t) (k 0)) (d (n "skeptic") (r "^0.12") (d #t) (k 1)) (d (n "skeptic") (r "^0.12") (d #t) (k 2)))) (h "05gljgnm65fz73vdwsvcavjmwka945jbwkh0c6w66p8qdh87pzla") (r "1.63")))

(define-public crate-checkito-1.3.9 (c (n "checkito") (v "1.3.9") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "05la9zq5xr2saixharv277vlcw5zklxxk5184qn11nb3fwvqvsn2") (r "1.65.0")))

(define-public crate-checkito-1.3.10 (c (n "checkito") (v "1.3.10") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "17v1sha2a84866rkran071d149bpkxmfbbljbq52crd65cmzwipl") (r "1.65.0")))

(define-public crate-checkito-1.4.0 (c (n "checkito") (v "1.4.0") (d (list (d (n "checkito_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "0vncrbdh2ad2cfnv0m5q4zlvhsiiyyn5723jybm4jqv8k0jrp5gg") (r "1.66.1")))

(define-public crate-checkito-1.4.1 (c (n "checkito") (v "1.4.1") (d (list (d (n "checkito_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "1m20di4ljhm9d989hxqi46hhvfk80iilg7gy12phzqa4r49kx0w1") (r "1.65.0")))

(define-public crate-checkito-1.4.2 (c (n "checkito") (v "1.4.2") (d (list (d (n "checkito_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "00d2vc3qi52nz39fc6f9jbqx0n295nf2qzj07whp4v86p20g9pqf") (r "1.65.0")))

(define-public crate-checkito-1.5.0 (c (n "checkito") (v "1.5.0") (d (list (d (n "checkito_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "orn") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "1ibc3h3lr9cl67xsc2lrn6y3wnj2xx7j1rbwsxz06876r0kihrwn") (r "1.65.0")))

(define-public crate-checkito-1.5.1 (c (n "checkito") (v "1.5.1") (d (list (d (n "checkito_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "orn") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "07cpli0vz6x4m8360knispznb31wshlqwp6d0jgrrdsfq6fw6s7v") (r "1.65.0")))

