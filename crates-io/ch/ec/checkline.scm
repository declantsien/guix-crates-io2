(define-module (crates-io ch ec checkline) #:use-module (crates-io))

(define-public crate-checkline-1.0.0 (c (n "checkline") (v "1.0.0") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "1h2zisnbr2g8wracans5p19i2h7ss8kj9r017myms58mj8k9y9vp")))

(define-public crate-checkline-1.1.0 (c (n "checkline") (v "1.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("deprecated" "derive" "cargo" "env" "unicode" "wrap_help" "string"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (f (quote ("termion-backend" "crossterm-backend"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1qjgsc849931bp4nzavbnh8kanmyl7fglfmxlic9lq08cymbadi8") (y #t)))

(define-public crate-checkline-1.1.1 (c (n "checkline") (v "1.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("deprecated" "derive" "cargo" "env" "unicode" "wrap_help" "string"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0786k4jc87mnh7qds8cj7z47va8dk9xrjn5ljyirxjywy8d6gb3x")))

