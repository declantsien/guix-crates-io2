(define-module (crates-io ch ec check_elevation) #:use-module (crates-io))

(define-public crate-check_elevation-0.1.0 (c (n "check_elevation") (v "0.1.0") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Memory"))) (d #t) (k 0)))) (h "0lqzqxrm6rlx11yi6hpkdma2ykm62vyzva4ynj0lngqhqy7i49n0") (y #t)))

(define-public crate-check_elevation-0.1.1 (c (n "check_elevation") (v "0.1.1") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Memory"))) (d #t) (k 0)))) (h "18b7915lh2hwiljinxpf0j8hn621gi7gsim8d2id2aqcdpvfz4l4") (y #t)))

(define-public crate-check_elevation-0.1.2 (c (n "check_elevation") (v "0.1.2") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Memory"))) (d #t) (k 0)))) (h "0pwmg2g7v1a9zyfl82hkhcsrznsk62vwvwq6pa02dmddd631axja") (y #t)))

(define-public crate-check_elevation-0.2.0 (c (n "check_elevation") (v "0.2.0") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Memory"))) (d #t) (k 0)))) (h "0dw9rahqyn0q78lcsyzyfppdr22p1mhygsn5g579kq81y9rq8rz9")))

(define-public crate-check_elevation-0.2.1 (c (n "check_elevation") (v "0.2.1") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Memory"))) (d #t) (k 0)))) (h "101z3jfxg69h4cfyp1h2ymm8v6z03lp1ib7sd9dp8rrzl6npzb41")))

(define-public crate-check_elevation-0.2.2 (c (n "check_elevation") (v "0.2.2") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading"))) (k 0)))) (h "1q6k0cd1x06jhhwa6hl2f4qqg2awfs3wa4h5vqz0ikds4ry6w8q5")))

(define-public crate-check_elevation-0.2.3 (c (n "check_elevation") (v "0.2.3") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading"))) (k 0)))) (h "0b8hmkpririra9fskj95wm5jxnckz2gx1hd51xzy82hzq1n8yk6g")))

(define-public crate-check_elevation-0.2.4 (c (n "check_elevation") (v "0.2.4") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading"))) (k 0)))) (h "005slkbvipcfgqqxfzznfgqzkppjwjwq0d6imp6nifg7f45k3xrl")))

