(define-module (crates-io ch ec check-versions) #:use-module (crates-io))

(define-public crate-check-versions-0.1.0 (c (n "check-versions") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.0.11") (k 0)) (d (n "semver-parser") (r "^0.7") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0gclig2iik3kv876fa5ivr5zlwza8ck02y3jg3ap9j3aq1wkwzcl") (y #t)))

(define-public crate-check-versions-0.1.1 (c (n "check-versions") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.0.11") (k 0)) (d (n "semver-parser") (r "^0.7") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1z8iyl7861a5x3f6if32ya6w04ablnd65b90bjzryrkav2yqqcnw") (y #t)))

(define-public crate-check-versions-0.1.2 (c (n "check-versions") (v "0.1.2") (d (list (d (n "pulldown-cmark") (r "^0.0.11") (k 0)) (d (n "semver-parser") (r "^0.7") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "11m6ihrwj85a23y82j23cszxkq41yrlbdfjm9c9m05n3hv0915vd") (y #t)))

