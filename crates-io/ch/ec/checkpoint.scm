(define-module (crates-io ch ec checkpoint) #:use-module (crates-io))

(define-public crate-checkpoint-0.1.0 (c (n "checkpoint") (v "0.1.0") (h "07z7c673s0wk80526yxixwn39sbd20n6y7n24q3gmh85x84hn6w4") (y #t)))

(define-public crate-checkpoint-0.1.1 (c (n "checkpoint") (v "0.1.1") (h "1f1xzm6fli2659k1j1gsg3yf722acirslashhzh0d8rhzqgywjw2") (y #t)))

(define-public crate-checkpoint-0.1.2 (c (n "checkpoint") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.1") (d #t) (k 0)))) (h "05fr6m2i61m4v3r257j6wmgx6nwqrchdlfwdmyrsmy5j9l3vqaxk") (f (quote (("filestorage-tests")))) (y #t)))

(define-public crate-checkpoint-0.1.3 (c (n "checkpoint") (v "0.1.3") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.1") (d #t) (k 0)))) (h "1pkrfkhg8wvfl5s24w85hx1cyz0im5v1wilp6796dc8djng6bjri") (f (quote (("filestorage-tests")))) (y #t)))

(define-public crate-checkpoint-0.1.4 (c (n "checkpoint") (v "0.1.4") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.1") (d #t) (k 0)))) (h "0xiilgn3gbij64rqn8x2c7ghxxn9a7281k101p7qwy3dfn404wzi") (f (quote (("filestorage-tests")))) (y #t)))

(define-public crate-checkpoint-0.1.5 (c (n "checkpoint") (v "0.1.5") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.1") (d #t) (k 0)))) (h "0rwmhjl1676w3byrkr1iavqpymnl1hrajqvsd6wkxqgr57p3jcnz") (f (quote (("filestorage-tests")))) (y #t)))

(define-public crate-checkpoint-0.1.6 (c (n "checkpoint") (v "0.1.6") (h "0vpnlncryyh29r0nllwh7ywaq7kia1iamn58nfhf4zbsp2kh3w9l") (y #t)))

