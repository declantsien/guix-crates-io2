(define-module (crates-io ch ec checksum-tapestry) #:use-module (crates-io))

(define-public crate-checksum-tapestry-0.3.0 (c (n "checksum-tapestry") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1kpc9c874qm91q9ga2d6wpdazqzgcyvqvvwq6lb7sfj4v6l7l1ld") (s 2) (e (quote (("experiments" "dep:rand"))))))

(define-public crate-checksum-tapestry-0.4.0 (c (n "checksum-tapestry") (v "0.4.0") (h "0a1ycmyb3mapq7q7zhvkzwb841nyrjps0bydsm8anyw3ajfv1kas")))

