(define-module (crates-io ch ec check-hash) #:use-module (crates-io))

(define-public crate-check-hash-0.1.0 (c (n "check-hash") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "19jzpbwarvhyhqz3mmm06i35nqq85n0vq5gvl7k2mnphp7cp96xd")))

