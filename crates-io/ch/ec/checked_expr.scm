(define-module (crates-io ch ec checked_expr) #:use-module (crates-io))

(define-public crate-checked_expr-0.1.0 (c (n "checked_expr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1982sa2swqygc0gplc5gv6wdsnpigm604a4vl1k7al14g3j80ycb")))

(define-public crate-checked_expr-0.2.0 (c (n "checked_expr") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01rylzxbpqh9hwpam8xinm2qkvcn9hzahls2l8sdamyscs5765bw")))

(define-public crate-checked_expr-0.2.1 (c (n "checked_expr") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sh8jhxy3lhnxny6214ziq38jiv13j3yrsyck5n7r3bm0vp06k2a")))

(define-public crate-checked_expr-0.2.2 (c (n "checked_expr") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qd2myiiy9i6ifdd61lmc03ck4pfjydgvsk26qj5zv0s2lslhdw1")))

(define-public crate-checked_expr-0.2.3 (c (n "checked_expr") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hfgpsdyf4wk8c1vwddkmpkvdy8nkzxs4klwx5g8bgdv0by7yw86")))

(define-public crate-checked_expr-0.3.0 (c (n "checked_expr") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gplq4x5034ybckgmk5ajmwz41gf6n2jjs8siliakjb36cl1m458")))

