(define-module (crates-io ch ec checkm) #:use-module (crates-io))

(define-public crate-checkm-0.0.1 (c (n "checkm") (v "0.0.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "1c466igsihayypfpbz2kvaczaqiaql453xis59cry04y6gf7p2ym")))

(define-public crate-checkm-0.1.0 (c (n "checkm") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "0ikmppz3v7ia2hhg498ns5cilqsz22skkqslinm0avzdi3b6bvl1")))

(define-public crate-checkm-0.1.1 (c (n "checkm") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "1a1k4mclv5i8f4zcyyvqpkgb6466fc0y76f30pipxg5hibylzxqc")))

(define-public crate-checkm-0.2.0 (c (n "checkm") (v "0.2.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-clippy"))) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "0.10.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "083w2bbf1cr5ip2p3b2836vzkabcq62whc1377nx1zqr2d59zqxa")))

