(define-module (crates-io ch ec checkssl) #:use-module (crates-io))

(define-public crate-checkssl-0.1.0 (c (n "checkssl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "rustls") (r "^0.18.0") (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20.0") (d #t) (k 0)) (d (n "x509-parser") (r "^0.8.0-beta4") (d #t) (k 0)))) (h "13dn0qr7w1xmgkyypvsf82iabxb58baiaahzjb5nn0yrf89whpk9")))

(define-public crate-checkssl-0.1.1 (c (n "checkssl") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "rustls") (r "^0.18.0") (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20.0") (d #t) (k 0)) (d (n "x509-parser") (r "^0.8.0-beta4") (d #t) (k 0)))) (h "1ns10yphqvwjfrlas2y11b2gmpdakwg6yc86zac0f00kri8gkbkn")))

(define-public crate-checkssl-0.2.0 (c (n "checkssl") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rustls") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20.0") (d #t) (k 0)) (d (n "x509-parser") (r "^0.8.0-beta4") (d #t) (k 0)))) (h "0mdxm1w9k2pcmax7p1j1qn8qla5kj2fbcxc15pdly96d0rz5718q")))

