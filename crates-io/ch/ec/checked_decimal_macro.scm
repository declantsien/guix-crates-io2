(define-module (crates-io ch ec checked_decimal_macro) #:use-module (crates-io))

(define-public crate-checked_decimal_macro-0.1.0 (c (n "checked_decimal_macro") (v "0.1.0") (d (list (d (n "checked_decimal_macro_core") (r "^0.1.0") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0dgdd1c18xfafxl4zh4p3p1w58vxzkw8ryn2xk2cxprv8ilnbpdy")))

(define-public crate-checked_decimal_macro-0.1.2 (c (n "checked_decimal_macro") (v "0.1.2") (d (list (d (n "checked_decimal_macro_core") (r "^0.1.2") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0d2rp2mzx9c1yrpyywmk3kay75jv1q21n1vn241vyz6nkdyf5saj")))

(define-public crate-checked_decimal_macro-0.1.3 (c (n "checked_decimal_macro") (v "0.1.3") (d (list (d (n "checked_decimal_macro_core") (r "^0.1.3") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0yxisfzzvkvydqn36j08qa4ygk4sw484nhay5zcv79kz83729gws")))

(define-public crate-checked_decimal_macro-0.1.4 (c (n "checked_decimal_macro") (v "0.1.4") (d (list (d (n "checked_decimal_macro_core") (r "^0.1.4") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "1s9ybigrii3x27vs1qy5zgxjb5sjxzp30kimwapmz96yaglddnkq")))

