(define-module (crates-io ch ec checked_array) #:use-module (crates-io))

(define-public crate-checked_array-0.1.0 (c (n "checked_array") (v "0.1.0") (h "0kmbqffzxdhmwd9a8jq91zixgy6zs1p56k0m5hhvgaai5xr73k2m") (f (quote (("std") ("default" "std"))))))

(define-public crate-checked_array-0.1.1 (c (n "checked_array") (v "0.1.1") (h "06vc757ggcqvw3gs1zc8prfwwa21damx8rp8ixwv95h44fpf42hb") (f (quote (("std") ("default" "std"))))))

(define-public crate-checked_array-0.1.3 (c (n "checked_array") (v "0.1.3") (h "023f9y10h5nwkgjbv5f82cg3c78dywvnnb9qbmlrc3a83m8szxxx") (f (quote (("std") ("default" "std"))))))

(define-public crate-checked_array-0.1.4 (c (n "checked_array") (v "0.1.4") (h "0p4r5bz5njixbkajb24b213h8qv95c5byyjk9i6dadirzd84yysr") (f (quote (("std") ("default" "std"))))))

(define-public crate-checked_array-0.1.5 (c (n "checked_array") (v "0.1.5") (h "0gbgpdyjhnyhi74sisfiaan0nnishy7q4w2my8k1wkbqc37qsw98") (f (quote (("std") ("default" "std"))))))

