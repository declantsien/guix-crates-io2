(define-module (crates-io ch ec checksumdir) #:use-module (crates-io))

(define-public crate-checksumdir-0.1.0 (c (n "checksumdir") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0gagqmkahnhi4rzal88ib9kk6p4lcspj87mzcmv6mzr4fyp1q8p1")))

(define-public crate-checksumdir-0.2.0 (c (n "checksumdir") (v "0.2.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0v953lkpxda929synlk657fkskfrxfzc5xr936hd3pccdn9p6miw")))

(define-public crate-checksumdir-0.3.0 (c (n "checksumdir") (v "0.3.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ai3sqwq8w8hs7nj3cxg3cbz5pmn0q754giilclb3qjifn7qb2x4")))

