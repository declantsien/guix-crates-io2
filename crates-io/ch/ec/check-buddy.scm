(define-module (crates-io ch ec check-buddy) #:use-module (crates-io))

(define-public crate-check-buddy-0.1.0 (c (n "check-buddy") (v "0.1.0") (d (list (d (n "check-buddy-core") (r "^0.1") (d #t) (k 0)))) (h "0ry1ppmz7ynfxigr755r2ynpxax05i4rbqm3zkkajd8qpmwq39j2")))

(define-public crate-check-buddy-0.2.0 (c (n "check-buddy") (v "0.2.0") (d (list (d (n "check-buddy-core") (r "^0.1") (d #t) (k 0)))) (h "11bh6hpfqh8vxrq6v4f0cqr9hp2l9k9fcxr7mwqg5vgr31k5insr")))

(define-public crate-check-buddy-0.2.1 (c (n "check-buddy") (v "0.2.1") (d (list (d (n "check-buddy-core") (r "^0.1") (d #t) (k 0)))) (h "0w3q60jdqs9651h1lq2jc4va3idxzf93qdh6kmjk1vs8aixn53l6")))

(define-public crate-check-buddy-0.2.2 (c (n "check-buddy") (v "0.2.2") (d (list (d (n "check-buddy-core") (r "^0.1") (d #t) (k 0)))) (h "03r63q5j0kpa530jlm4lczm826qacigfwynm21bnjx483vxkcpb5")))

(define-public crate-check-buddy-0.2.3 (c (n "check-buddy") (v "0.2.3") (d (list (d (n "check-buddy-core") (r "^0.2.3") (d #t) (k 0)))) (h "08vjh5vf97qnkbanlq8ls8hyd1y1xjpxbg6aiq0rq3g8a6h3cmb7")))

(define-public crate-check-buddy-0.2.4 (c (n "check-buddy") (v "0.2.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "calamine") (r "^0.19") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mvn54wvx28dkgglcc6hff8zspihrb5sszapgqs9zsnyl5gcbc6c")))

