(define-module (crates-io ch ec checkpwn_lib) #:use-module (crates-io))

(define-public crate-checkpwn_lib-0.1.0-alpha1 (c (n "checkpwn_lib") (v "0.1.0-alpha1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (k 0)) (d (n "ureq") (r "^1.4.1") (f (quote ("tls"))) (k 0)) (d (n "zeroize") (r "^1.1.1") (d #t) (k 0)))) (h "071s5m2szggkdl91fwyvn6gbk50lb6911iqjslh4xcf0j4v62bh8") (f (quote (("ci_test")))) (y #t)))

(define-public crate-checkpwn_lib-0.1.0 (c (n "checkpwn_lib") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (k 0)) (d (n "ureq") (r "^1.5.1") (f (quote ("tls"))) (k 0)) (d (n "zeroize") (r "^1.1.1") (d #t) (k 0)))) (h "07wsdaqil8f1m7aqsm45cxdrfxx62jq3bbi3nb6qk5qmacmwaxfh") (f (quote (("ci_test"))))))

(define-public crate-checkpwn_lib-0.1.1 (c (n "checkpwn_lib") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("tls"))) (k 0)) (d (n "zeroize") (r "^1.1.1") (d #t) (k 0)))) (h "18y0nc6p8v8d6rfxda6bhdns2scdf6y2lg7gxlxadf8xbwi90x6i") (f (quote (("ci_test"))))))

(define-public crate-checkpwn_lib-0.2.0 (c (n "checkpwn_lib") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (k 0)) (d (n "ureq") (r "^2.1.1") (f (quote ("tls"))) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "1ddabb296xq57hp278hl07jkypgcy2msldw786wny1fzhxynl29r") (f (quote (("ci_test"))))))

(define-public crate-checkpwn_lib-0.2.1 (c (n "checkpwn_lib") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "sha-1") (r "^0.10.0") (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls"))) (k 0)) (d (n "zeroize") (r "^1.5.0") (d #t) (k 0)))) (h "19dzfr93hd7nqcj7qpyqiibq14qk4j1nvf8m2rwndbhzq68krk73") (f (quote (("ci_test"))))))

