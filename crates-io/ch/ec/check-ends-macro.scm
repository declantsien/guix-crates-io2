(define-module (crates-io ch ec check-ends-macro) #:use-module (crates-io))

(define-public crate-check-ends-macro-1.0.0 (c (n "check-ends-macro") (v "1.0.0") (h "15b4xdbnhwwf3lklgbjd5aq04bq4qzisacsfi3xqnhrk7pxd68nr") (y #t)))

(define-public crate-check-ends-macro-1.0.1 (c (n "check-ends-macro") (v "1.0.1") (h "097w0bxqv1bac18m4g7inqr5mdnr9japzxhcfx2krgc2m9rnlkbk") (y #t)))

(define-public crate-check-ends-macro-1.0.2 (c (n "check-ends-macro") (v "1.0.2") (h "0c6nx5djbzi2gzlir2mqgmf70vkm1i7k5bv66ppxmb10bvbpb977") (y #t)))

(define-public crate-check-ends-macro-1.0.3 (c (n "check-ends-macro") (v "1.0.3") (h "07xwp19cibdsvmidh92siafxcg7z042qgbzld2byzg3fyr88h7kw")))

