(define-module (crates-io ch ec check_keyword) #:use-module (crates-io))

(define-public crate-check_keyword-0.1.0 (c (n "check_keyword") (v "0.1.0") (h "1qkifwg72xi6pqa8vf96bn1i1g0r9jb223q2cw000nfkn175jbrg") (f (quote (("default" "2018") ("2018"))))))

(define-public crate-check_keyword-0.1.1 (c (n "check_keyword") (v "0.1.1") (h "0xapwznxkfi1fihy95gnxvdlhqyvf9z3dxxhllblp743nsnkjvsr") (f (quote (("default" "2018") ("2018"))))))

(define-public crate-check_keyword-0.2.0 (c (n "check_keyword") (v "0.2.0") (h "11zplk3d94p04ljmjzbcjvy6q2kngr92bga4z61gpm3ih8ck5mg4") (f (quote (("default" "2018") ("2018"))))))

