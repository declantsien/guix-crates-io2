(define-module (crates-io ch ec check-lockfile-intersection) #:use-module (crates-io))

(define-public crate-check-lockfile-intersection-0.1.0 (c (n "check-lockfile-intersection") (v "0.1.0") (d (list (d (n "attohttpc") (r "^0.16.0") (d #t) (k 0)) (d (n "cargo-lock") (r "^9.0.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "xflags") (r "^0.3.2") (d #t) (k 0)))) (h "03g763shk5vc8xgvqzlpnssc8pnqhiqr6d2sln1rphswjsqndz7g")))

