(define-module (crates-io ch ec check-buddy-core) #:use-module (crates-io))

(define-public crate-check-buddy-core-0.1.0 (c (n "check-buddy-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "calamine") (r "^0.19") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cw6y5i7vd222kcljxl5y59l52ni0vf69i7jbaf16p916kar124d")))

(define-public crate-check-buddy-core-0.2.1 (c (n "check-buddy-core") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "calamine") (r "^0.19") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1l86dmdgznsx22rbrxg1ay3iygqm8smknsmrn7i3b2cv021vhdj0")))

(define-public crate-check-buddy-core-0.2.2 (c (n "check-buddy-core") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "calamine") (r "^0.19") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rnsafr6agkx2qy7xa8x29g2cp23rm3q2fqj9p9q77cfa8d8xsna")))

(define-public crate-check-buddy-core-0.2.3 (c (n "check-buddy-core") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "calamine") (r "^0.19") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01nsjb8nsv76q4jwjxjc0p37fh9jl9rlgmqd42axrvs1knrk19kv")))

