(define-module (crates-io ch ec checkeo) #:use-module (crates-io))

(define-public crate-checkeo-0.1.0 (c (n "checkeo") (v "0.1.0") (h "0k05x6398k0gsbviwvz3rfkakcqibzvywc8y4azx73sq1dqx5a7c") (y #t)))

(define-public crate-checkeo-0.1.1 (c (n "checkeo") (v "0.1.1") (d (list (d (n "checkeo") (r "^0.1.0") (d #t) (k 0)))) (h "0k5235maig82p8lgmp33wqnmwp7bxkb6qp1brim401r9ck5i4dbf") (y #t)))

(define-public crate-checkeo-0.1.2 (c (n "checkeo") (v "0.1.2") (h "0p6sjycvavr03x77p16cssnqi8gd23fn2pq0zpqg6kq1qir0d19d") (y #t)))

(define-public crate-checkeo-0.1.3 (c (n "checkeo") (v "0.1.3") (h "0bi65kv5maz1hhnlpqfj85qk1y88wx6lfka6g110yc53lr55iwmk") (y #t)))

(define-public crate-checkeo-0.1.4 (c (n "checkeo") (v "0.1.4") (h "0gihwjlllg4n4mdaaipam3ka5wi012srmy52vaj0ks5qsk95hg2c") (y #t)))

(define-public crate-checkeo-0.1.5 (c (n "checkeo") (v "0.1.5") (h "1k9mw75yh7xqm55dfn4bcj7371s5qhgv1ybyscbzy5ah6b4xi5s4")))

