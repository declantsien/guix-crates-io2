(define-module (crates-io ch ec checke-rs) #:use-module (crates-io))

(define-public crate-checke-rs-0.0.1 (c (n "checke-rs") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "141144az278bv16fgnhfxzd2zh5xf54hifmdd3s7w6c04apx3vlk")))

