(define-module (crates-io ch ec checker) #:use-module (crates-io))

(define-public crate-checker-0.0.1 (c (n "checker") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0f15w494b5hwpycszm8awry2sa2w504p5riwf8ih5xj4m231ksma")))

(define-public crate-checker-0.0.2 (c (n "checker") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "010s9ncqgfqk00j5nnh27xpb0yjdajwgl6ia8j1ykx4izxr631kp")))

(define-public crate-checker-0.0.3 (c (n "checker") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "0av3sw46p85vms00pbcm4v0bh5kypi4waspbsaq4j11dp70bj9an")))

