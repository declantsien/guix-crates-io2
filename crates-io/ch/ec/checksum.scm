(define-module (crates-io ch ec checksum) #:use-module (crates-io))

(define-public crate-checksum-0.1.0 (c (n "checksum") (v "0.1.0") (h "0c9inrz5dhg59f5n47r54rp88z9hapx92f0wjwg13c00zj7jhy4n")))

(define-public crate-checksum-0.2.0 (c (n "checksum") (v "0.2.0") (h "1f3i6kr44w15riv85qwana71f14iqlsx6n2zigd13dgmhcqd0z9d")))

(define-public crate-checksum-0.2.1 (c (n "checksum") (v "0.2.1") (h "0pxl7r6h17l5vj9sd1ww52pmp7v8dwkwrhpk7pdp769y8rm4zhnm")))

