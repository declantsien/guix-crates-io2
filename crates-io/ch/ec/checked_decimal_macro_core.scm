(define-module (crates-io ch ec checked_decimal_macro_core) #:use-module (crates-io))

(define-public crate-checked_decimal_macro_core-0.1.0 (c (n "checked_decimal_macro_core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0sl0szxmifb1v9vcbp6ihw3rcnh7r3vj26hsx45dpxx4247m3mlx")))

(define-public crate-checked_decimal_macro_core-0.1.1 (c (n "checked_decimal_macro_core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0srfb4wvvij37agkzgmmm4zawl4miks2dj7sn8lf6imkv75fynq8")))

(define-public crate-checked_decimal_macro_core-0.1.2 (c (n "checked_decimal_macro_core") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0l5kqg4vsywy4phki9pma9jyv7sjnjikfpfazfl2fy5b8qj5q07d")))

(define-public crate-checked_decimal_macro_core-0.1.3 (c (n "checked_decimal_macro_core") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1zhqq7nxh1lapdf7pvwc04lr0v8ac6kkr41vdq3w365y1a726598")))

(define-public crate-checked_decimal_macro_core-0.1.4 (c (n "checked_decimal_macro_core") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "080wcc5y5236bgazd4dfqw1v6y9qnwanj4jpmc1ibrqz6cjdl7hc")))

