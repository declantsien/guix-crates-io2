(define-module (crates-io ch ec checs) #:use-module (crates-io))

(define-public crate-checs-0.0.0 (c (n "checs") (v "0.0.0") (h "1sk4rqhnd3sl6s5zawb1dxv2mz8x4cjb37xx5wwp2az93snirxf8")))

(define-public crate-checs-0.1.0 (c (n "checs") (v "0.1.0") (h "1j4nwbbazkyq6fgfc1v96w675hz2yvz4202gjhdxqk1r6ck7wv2r")))

(define-public crate-checs-0.2.0 (c (n "checs") (v "0.2.0") (h "0aqdwsvcc27m21yxa1bmrzc1kprxxa4qi368rxfbpq963ryb1fmj")))

(define-public crate-checs-0.3.0 (c (n "checs") (v "0.3.0") (h "0d2l1nsgs7mcdzn8k120ncsf8drjq6dkhl39bxa72h108pzk8njj")))

(define-public crate-checs-0.4.0 (c (n "checs") (v "0.4.0") (d (list (d (n "checs_proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1n9pyh4xfz9b0iy5h2n2w27byz9qkrjmlqp859ddavwj0gnj4yhl") (f (quote (("proc" "checs_proc") ("default" "proc"))))))

