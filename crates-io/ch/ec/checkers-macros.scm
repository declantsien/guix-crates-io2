(define-module (crates-io ch ec checkers-macros) #:use-module (crates-io))

(define-public crate-checkers-macros-0.1.0 (c (n "checkers-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0ndkrbx3y4mlvb3pxcfr1y17i6h0n40aljyx0kmsz3xsd8jb1iqb")))

(define-public crate-checkers-macros-0.1.1 (c (n "checkers-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "07njf275k8rlhryw2h92jqd09jcpava9g06myf4mlnvq0ly3d1bb")))

(define-public crate-checkers-macros-0.1.3 (c (n "checkers-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1v1c59x489dxinw5rhry5mffkw2jzwvrbbvfislza063ngcs1sh6")))

(define-public crate-checkers-macros-0.2.0 (c (n "checkers-macros") (v "0.2.0") (d (list (d (n "checkers") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "16dvnv95sa48l0g75ydv0ih6iaakjqi7y13n8pgx54mlr763d850")))

(define-public crate-checkers-macros-0.2.1 (c (n "checkers-macros") (v "0.2.1") (d (list (d (n "checkers") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "03l1sxb6jwyrrlakxlr2g2bp4xlicdvrdmmazw8hzjnh1q5hv5vb")))

(define-public crate-checkers-macros-0.3.0 (c (n "checkers-macros") (v "0.3.0") (d (list (d (n "checkers") (r "^0.4.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1vdc3p7gjwlam8ynw9m4j0cpd7yf6qyg5dlndk9smmrdj5wfb59h")))

(define-public crate-checkers-macros-0.3.1 (c (n "checkers-macros") (v "0.3.1") (d (list (d (n "checkers") (r "^0.4.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0nyzq5cwj1p6c0ff98461yczzcir6x4m9xfz72v82ribl3mj5rbb")))

(define-public crate-checkers-macros-0.5.0 (c (n "checkers-macros") (v "0.5.0") (d (list (d (n "checkers") (r "^0.5.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1wd3z5h9dplp9v9lg4nhs9py7hw0q576cybls92343yx4h6rb4qs")))

(define-public crate-checkers-macros-0.5.1 (c (n "checkers-macros") (v "0.5.1") (d (list (d (n "checkers") (r "^0.5.3") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1is8svrk7dm3xmdjdj1dsj9gs4ksrc3pgrrhzrw2y45hxqa5h0zg")))

(define-public crate-checkers-macros-0.5.7 (c (n "checkers-macros") (v "0.5.7") (d (list (d (n "checkers") (r "^0.5.7") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1m90qj77pqqbd5dilsv522k664s9c0v822ka17i1igm6a3jaagnf")))

(define-public crate-checkers-macros-0.6.0 (c (n "checkers-macros") (v "0.6.0") (d (list (d (n "checkers") (r "^0.6.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0k22rvmdr0lxl490piayjrc12f7gv10z8azkwxjmb0hxafg9nf1r")))

(define-public crate-checkers-macros-0.6.1 (c (n "checkers-macros") (v "0.6.1") (d (list (d (n "checkers") (r "^0.6.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1v4nswnbm474b7gljkp7k5ayv70lxg5y9c5lh7dhyca0hpwz85pb")))

(define-public crate-checkers-macros-0.6.2 (c (n "checkers-macros") (v "0.6.2") (d (list (d (n "checkers") (r "^0.6.2") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "16b0ds7x4z4qdhan8i5bv1n71am058m962788i08q0sblkjgp904")))

