(define-module (crates-io ch ec checkdigit) #:use-module (crates-io))

(define-public crate-checkdigit-0.1.0 (c (n "checkdigit") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1q62v932fc1j0r7k7dshvs71b8yrjdwfiysfdfrav485a3xx9k06")))

