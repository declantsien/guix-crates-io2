(define-module (crates-io ch ec checked_cast) #:use-module (crates-io))

(define-public crate-checked_cast-0.0.1 (c (n "checked_cast") (v "0.0.1") (h "0dwxwrfkxxvwcazrv629q9qhlkb19ph23gy3x6rnmq5jvb9dfhzh")))

(define-public crate-checked_cast-0.0.2 (c (n "checked_cast") (v "0.0.2") (h "0ja18c67gmzcrcd1q38mpc9n7whi1m0i26r4nl4kzyga2vndsi81")))

(define-public crate-checked_cast-0.0.3 (c (n "checked_cast") (v "0.0.3") (h "0789bvd4gipy3jzlahv120xjkhspm2wdjwq8wwbanj2y03alw6q4")))

