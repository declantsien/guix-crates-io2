(define-module (crates-io ch ec check) #:use-module (crates-io))

(define-public crate-check-0.1.0 (c (n "check") (v "0.1.0") (h "0fxpi8i9nargcx9jc16509hgqr4is7qf96kfhrc9ynbf87sngq6k") (y #t)))

(define-public crate-check-1.0.0 (c (n "check") (v "1.0.0") (h "1c9l0q83l28fx82wl5pb02kb8hrdvp8z2sdb40dm0hxgvyisvgl9")))

