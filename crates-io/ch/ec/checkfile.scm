(define-module (crates-io ch ec checkfile) #:use-module (crates-io))

(define-public crate-checkfile-1.0.0 (c (n "checkfile") (v "1.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)) (d (n "sha256") (r "^1") (d #t) (k 0)))) (h "11fkbrgr3z67f8q5d548h2sii0qwpd05yg0ff4qw6gvl8pglvppz")))

(define-public crate-checkfile-1.1.0 (c (n "checkfile") (v "1.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "rev_lines") (r "^0.3.0") (d #t) (k 0)) (d (n "sha256") (r "^1") (d #t) (k 0)))) (h "0b91h7qj68xl5z0dmhz4zwkzbq07fg5h0kv01s2wcf2axvi0a7z0")))

(define-public crate-checkfile-1.1.1 (c (n "checkfile") (v "1.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "rev_lines") (r "^0.3.0") (d #t) (k 0)) (d (n "sha256") (r "^1") (d #t) (k 0)))) (h "135fhf97f3gv1xk6dl2iwzsh7rn76wj163xb6b9nlnprqd1fqnl8")))

(define-public crate-checkfile-1.2.0 (c (n "checkfile") (v "1.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "rev_lines") (r "^0.3.0") (d #t) (k 0)) (d (n "sha256") (r "^1") (d #t) (k 0)))) (h "1iy06na6qnmbgvjibpx536bigag5glqf91z1fyfb6s8jayl92wkn")))

(define-public crate-checkfile-1.3.0 (c (n "checkfile") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "rev_lines") (r "^0.3.0") (d #t) (k 0)) (d (n "sha256") (r "^1") (d #t) (k 0)))) (h "0p9wkd9cnl4wizag695hla00w78qkx9qapgq420cpm9pvcx90xm5")))

