(define-module (crates-io ch ec checkerboard) #:use-module (crates-io))

(define-public crate-checkerboard-0.1.0 (c (n "checkerboard") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.22.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.18.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "opencv") (r "^0.17") (f (quote ("opencv-32"))) (k 0)))) (h "16xs6mlq8r4b989a6qpdn3zd2x4lalac3bhnz4vb3csdi0cs2z8s")))

