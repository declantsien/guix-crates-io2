(define-module (crates-io ch ec checked-enum) #:use-module (crates-io))

(define-public crate-checked-enum-0.1.0 (c (n "checked-enum") (v "0.1.0") (h "1wagh62p5r010rzj77376y2h9lwkpxhh84jky3ln91y49j2zl8kr")))

(define-public crate-checked-enum-0.1.1-alpha1 (c (n "checked-enum") (v "0.1.1-alpha1") (h "034dxbq94jwmw449a3c5hcrhvzwdsyjchdn4851i78hwwq5cfy4b")))

