(define-module (crates-io ch ec check-buddy-pgn-parser) #:use-module (crates-io))

(define-public crate-check-buddy-pgn-parser-0.2.1 (c (n "check-buddy-pgn-parser") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "check-buddy-core") (r "^0.2.1") (d #t) (k 0)))) (h "0cl9xrwmndy227r5jm8ds3rwiv3g6xyjp6bs4gnni8bbf5hyjw9l")))

(define-public crate-check-buddy-pgn-parser-0.2.2 (c (n "check-buddy-pgn-parser") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "check-buddy-core") (r "^0.2.2") (d #t) (k 0)))) (h "12mnkvvjfh99m3pxza4i3yhqq2mspn7g1zpql27skqbxb7qn2zzg")))

(define-public crate-check-buddy-pgn-parser-0.2.3 (c (n "check-buddy-pgn-parser") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "check-buddy-core") (r "^0.2.3") (d #t) (k 0)))) (h "0i3zmqbs519sjg8bzhzg4ir07j9hjz15krxlcl2lxiwhf10vmn7a")))

(define-public crate-check-buddy-pgn-parser-0.2.4 (c (n "check-buddy-pgn-parser") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "check-buddy") (r "^0.2.4") (d #t) (k 0)))) (h "0g5qayhxbqng6qfcb0ldi3q81kkdzg02rwjlj6lm8ig6cd8i7gr9")))

