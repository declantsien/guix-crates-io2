(define-module (crates-io ch ec checkout) #:use-module (crates-io))

(define-public crate-checkout-0.1.0 (c (n "checkout") (v "0.1.0") (d (list (d (n "json_errors") (r "^0.5.4") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.2") (d #t) (k 0)))) (h "12v31jwlnm1cs3vwiwj3w71b8l505rivgfn2rz92ylyg2y96sf8w")))

(define-public crate-checkout-0.1.1 (c (n "checkout") (v "0.1.1") (d (list (d (n "json_errors") (r "^0.5.4") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.2") (d #t) (k 0)))) (h "170x2syr96dhlsgczvsv9ira2hh1zgbfmxps293wj2c28h724x32")))

(define-public crate-checkout-0.1.2 (c (n "checkout") (v "0.1.2") (d (list (d (n "json_errors") (r "^0.5.4") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.2") (d #t) (k 0)))) (h "004h01iya17ljzwqr3dk3l28l84ih3x05kq7lhns75wk90jn7cj1")))

(define-public crate-checkout-0.1.3 (c (n "checkout") (v "0.1.3") (d (list (d (n "json_errors") (r "^0.5.4") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.2") (d #t) (k 0)))) (h "03s10icz6fi92sh17lcka3g2sff9m1ivnfsyfzjy4w93fyxrkcpw")))

(define-public crate-checkout-0.1.4 (c (n "checkout") (v "0.1.4") (d (list (d (n "json_errors") (r "^0.5.4") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.2") (d #t) (k 0)))) (h "0vw2gd83laqd6iyrgrksvvhwpbphpqg89xg4ql8zxcv2fcfdda7k")))

(define-public crate-checkout-0.1.5 (c (n "checkout") (v "0.1.5") (d (list (d (n "json_errors") (r "^0.5.4") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.2") (d #t) (k 0)))) (h "1fsvhw6l8jzsyw3qdr49a8gwfnwxfsg4kv7lk9dg7n5101h7w007")))

