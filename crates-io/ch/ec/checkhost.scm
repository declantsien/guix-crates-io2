(define-module (crates-io ch ec checkhost) #:use-module (crates-io))

(define-public crate-checkhost-0.1.0 (c (n "checkhost") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "04fg9jk38d4ahdn6160zsg3pf9g14pi39jq6zlqrsfah06jfyiqi")))

