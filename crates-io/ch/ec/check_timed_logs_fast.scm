(define-module (crates-io ch ec check_timed_logs_fast) #:use-module (crates-io))

(define-public crate-check_timed_logs_fast-0.1.0 (c (n "check_timed_logs_fast") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "01i8dqm7kz4ph97y1rnkxmn1355xy019wmabw00w7ckz4i8pa9sq")))

(define-public crate-check_timed_logs_fast-0.2.0 (c (n "check_timed_logs_fast") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1d1ma12xfjw83xszi6cwxybwblsiy4iv7lqlrz0101d5lmpv4kb3")))

(define-public crate-check_timed_logs_fast-0.3.0 (c (n "check_timed_logs_fast") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1pz854iywvyjrdn1ff8mmc47z875b5xm3h7swzb9fhx5p3p5n6r7")))

