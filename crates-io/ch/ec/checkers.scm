(define-module (crates-io ch ec checkers) #:use-module (crates-io))

(define-public crate-checkers-0.1.0 (c (n "checkers") (v "0.1.0") (h "15sax8282bfzvwxhpj6vn71935clivgx38wj13sin9yhdga6cy4p")))

(define-public crate-checkers-0.2.0 (c (n "checkers") (v "0.2.0") (h "1p2kxrgwxz4ahw23rj6piark55sggd7bn9dzaddzimv2dyzq6cpj")))

(define-public crate-checkers-0.2.1 (c (n "checkers") (v "0.2.1") (d (list (d (n "checkers-macros") (r "^0.1.0") (d #t) (k 0)))) (h "09jzp3prw2x5anq5qnvj7g24vbwdvbr14bhsch3b4m44pwq20w37")))

(define-public crate-checkers-0.2.2 (c (n "checkers") (v "0.2.2") (d (list (d (n "checkers-macros") (r "^0.1.0") (d #t) (k 0)))) (h "08n4brsc0pacyjhzdls587qw6dr1grvzmxp9zk6iiiafhpi7v57f")))

(define-public crate-checkers-0.2.3 (c (n "checkers") (v "0.2.3") (d (list (d (n "checkers-macros") (r "^0.1.3") (d #t) (k 0)))) (h "0bljkxiizqsj0wmq4zxksvgf72la71bhgrp21ds3xmdg7lk7zilq")))

(define-public crate-checkers-0.3.0 (c (n "checkers") (v "0.3.0") (d (list (d (n "checkers-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1zc2mc4hljpsj7z6844r57l6znkqa4m6pc9dsnva0yx65z6cy8kr")))

(define-public crate-checkers-0.3.1 (c (n "checkers") (v "0.3.1") (d (list (d (n "checkers-macros") (r "^0.2.0") (d #t) (k 0)))) (h "076fb9py9457f9bvnq25sxpv7lz27h3c1xvrjgxnx2l4bj32zn40")))

(define-public crate-checkers-0.4.0 (c (n "checkers") (v "0.4.0") (d (list (d (n "checkers-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0cvrxblnlal5mg7b06bdcdh734ra3w1z3j1glshw4cps98nvhlsx")))

(define-public crate-checkers-0.4.1 (c (n "checkers") (v "0.4.1") (d (list (d (n "checkers-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1h66wi9y2v8mmrvr2szvvxkv3j46yshj30w9z592qa4qm7az41fc")))

(define-public crate-checkers-0.4.2 (c (n "checkers") (v "0.4.2") (d (list (d (n "checkers-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1jxpzyd81s05qlk58rcnhkqkmvyk3ga54pz8r50pjrd9cgxfiwp1")))

(define-public crate-checkers-0.5.0 (c (n "checkers") (v "0.5.0") (d (list (d (n "checkers-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0zkz19silhf9d30mhdm752h0fsz5wan26wmbvw1yh32igf10y55p") (f (quote (("realloc" "fxhash") ("default" "realloc"))))))

(define-public crate-checkers-0.5.1 (c (n "checkers") (v "0.5.1") (d (list (d (n "checkers-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "00d80c7xfzbqifwl6cc8rq6jf67qs1kmf6cpwvfbyg90pr6fyxhb") (f (quote (("realloc" "fxhash") ("default" "realloc"))))))

(define-public crate-checkers-0.5.2 (c (n "checkers") (v "0.5.2") (d (list (d (n "checkers-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "038378rm49zn42gdxnbygigakrcw5ixvcjhxk7ralj45xp8ghyrf") (f (quote (("realloc" "fxhash") ("default" "realloc"))))))

(define-public crate-checkers-0.5.3 (c (n "checkers") (v "0.5.3") (d (list (d (n "checkers-macros") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0l0simdv5k9iz67dxf5s7ivr1klmnx05igiy89fgk3jgavik8p56") (f (quote (("realloc" "fxhash") ("macros" "checkers-macros") ("default" "realloc" "macros"))))))

(define-public crate-checkers-0.5.4 (c (n "checkers") (v "0.5.4") (d (list (d (n "checkers-macros") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1m6khlc7dvr6cv23x4icg8ppavlb9y6qvaiqmw1hnalzk5zla8if") (f (quote (("realloc" "fxhash") ("macros" "checkers-macros") ("default" "realloc" "macros"))))))

(define-public crate-checkers-0.5.5 (c (n "checkers") (v "0.5.5") (d (list (d (n "checkers-macros") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "085cyvgs8aam9wzym8rp34ypzfl9hby10zbw2whs0jg4hbh8yvvm") (f (quote (("zeroed") ("realloc" "fxhash") ("macros" "checkers-macros") ("default" "realloc" "zeroed" "macros"))))))

(define-public crate-checkers-0.5.6 (c (n "checkers") (v "0.5.6") (d (list (d (n "checkers-macros") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1d9ds5cj9q5b3nraysfxbyj1zyb7dqahzxrj9d081wccx0973xvm") (f (quote (("zeroed") ("realloc" "fxhash") ("macros" "checkers-macros") ("default" "realloc" "zeroed" "macros"))))))

(define-public crate-checkers-0.5.7 (c (n "checkers") (v "0.5.7") (d (list (d (n "checkers-macros") (r "^0.5.7") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1c2dwmx6yaph8v1ff7rbyn8q61kr28l4gv7j6fkgwp0p0bvh3m0q") (f (quote (("zeroed") ("realloc" "fxhash") ("macros" "checkers-macros") ("default" "realloc" "zeroed" "macros"))))))

(define-public crate-checkers-0.6.0 (c (n "checkers") (v "0.6.0") (d (list (d (n "backtrace") (r "^0.3.61") (o #t) (d #t) (k 0)) (d (n "checkers-macros") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1v7byi5kb1jmwazchjack6arlg3vx4p7jrklyfpmfpnvdmg86q4g") (f (quote (("zeroed") ("realloc" "fxhash") ("macros" "checkers-macros") ("default" "realloc" "zeroed" "macros"))))))

(define-public crate-checkers-0.6.1 (c (n "checkers") (v "0.6.1") (d (list (d (n "backtrace") (r "^0.3.61") (o #t) (d #t) (k 0)) (d (n "checkers-macros") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "14yx7g3qv7w7sl8zzwqjy0gvygxfkymdc9bd61lngplpl597r62x") (f (quote (("zeroed") ("realloc" "fxhash") ("macros" "checkers-macros") ("default" "realloc" "zeroed" "macros"))))))

(define-public crate-checkers-0.6.2 (c (n "checkers") (v "0.6.2") (d (list (d (n "backtrace") (r "^0.3.61") (o #t) (d #t) (k 0)) (d (n "checkers-macros") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "17f7ghaiing8193fcd9zs7bm63wjw97y62zgczvn4cmlvp3hjjdi") (f (quote (("zeroed") ("realloc" "fxhash") ("macros" "checkers-macros") ("default" "realloc" "zeroed" "macros"))))))

(define-public crate-checkers-0.6.3 (c (n "checkers") (v "0.6.3") (d (list (d (n "backtrace") (r "^0.3.61") (o #t) (d #t) (k 0)) (d (n "checkers-macros") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0ibgjb3142fs6ys5rqsal0p7a7rpg5q2ika48ndv16lcwpraxpx7") (f (quote (("zeroed") ("realloc" "fxhash") ("macros" "checkers-macros") ("default" "realloc" "zeroed" "macros")))) (r "1.56")))

