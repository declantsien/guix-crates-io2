(define-module (crates-io ch ec checkr) #:use-module (crates-io))

(define-public crate-checkr-0.0.1 (c (n "checkr") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01b7kbfd6dqha8aidy5vswgj89z3j63j90v5m60gqdnc36bsr85a")))

(define-public crate-checkr-0.0.2 (c (n "checkr") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0irb6blsp91nlai553razc649rlrq3m2vv7wlk8q3f531mk5wfa9")))

(define-public crate-checkr-0.0.3 (c (n "checkr") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0na7n8521rhjicgkwybr08d7ylzlrc7mn0gkb5ssjqk0ldbv82j0")))

(define-public crate-checkr-0.0.4 (c (n "checkr") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z3jsa2q7w69l7vzxkx4xlyn6wwjbz8mrxkgkpkwxz0zr4z44n1y")))

(define-public crate-checkr-0.0.5 (c (n "checkr") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04q5ydsc1f8fl0dd2dmirr3dg4hf6q1kd0jfr8aafn0n1i7z50jk")))

(define-public crate-checkr-0.0.6 (c (n "checkr") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q0bqfgv1ijvq0dfvwkpdq2bciix4cf9smi8gmglh0sqw3rk1gmh")))

(define-public crate-checkr-0.0.7 (c (n "checkr") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dwzrymn1s402qbsbyjnn3i2zk2vf995cysbvcp4vl51riqx07kn")))

(define-public crate-checkr-0.0.8 (c (n "checkr") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("chrono" "uuid"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m1w914x358x8004qxc4jnnnp8l9wm7kvdr8rv7k2q4hpk5kv57b")))

(define-public crate-checkr-0.0.9 (c (n "checkr") (v "0.0.9") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("chrono" "uuid"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sk3zvw0q2vpzvlr8vrpy03v29s5cxhn0q7fivl86ar437rk1a9g")))

(define-public crate-checkr-0.0.10 (c (n "checkr") (v "0.0.10") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("chrono" "uuid"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04d89qfz0rf6i59790sr3yj1gha1nwy2mx0zf4qq13cyk3mavljc")))

