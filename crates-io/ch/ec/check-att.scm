(define-module (crates-io ch ec check-att) #:use-module (crates-io))

(define-public crate-check-att-0.1.0 (c (n "check-att") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0qg84wgrc1mja1knnfxfng2z77wqnwg3rl7k6s59dz5wv7qbi4xc")))

(define-public crate-check-att-0.1.1 (c (n "check-att") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "09pmjll1cz0ywhpqv09kgli65131lp8hmp36hx45gn99jrmn522d")))

(define-public crate-check-att-0.1.2 (c (n "check-att") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.191") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vff3sc90461fla5crlr48vsai3h4nwzsyagzx5pjrbwwd3mpr2f")))

