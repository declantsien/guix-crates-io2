(define-module (crates-io ch ec checkpoint_derive) #:use-module (crates-io))

(define-public crate-checkpoint_derive-0.1.0 (c (n "checkpoint_derive") (v "0.1.0") (h "0vjxwb35l2qplznal1iq9s4vnk924j1k996lywq6g2pqf8p6hjif") (y #t)))

(define-public crate-checkpoint_derive-0.1.1 (c (n "checkpoint_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "14kgx1na15z6kq12q1pi4d0ggq38245fi7fxvqk83x1d1mmg3h2i") (y #t)))

(define-public crate-checkpoint_derive-0.1.2 (c (n "checkpoint_derive") (v "0.1.2") (h "17rfqi5g3212wi88yy3b88zv76kh3ji6kq539spb5cgmpw9sr3gg") (y #t)))

(define-public crate-checkpoint_derive-0.1.3 (c (n "checkpoint_derive") (v "0.1.3") (h "17lmvwgfnicvv93mvy0pq5k8gmkyvgk78k7flyz7mwq2x9hmcwgw") (y #t)))

(define-public crate-checkpoint_derive-0.1.4 (c (n "checkpoint_derive") (v "0.1.4") (h "07725q7k3sfwhwj9pbxhjd04572x9dy0dl74yygb58sdjql4wpx7") (y #t)))

