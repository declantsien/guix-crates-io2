(define-module (crates-io ch ec checkito_macro) #:use-module (crates-io))

(define-public crate-checkito_macro-0.1.0 (c (n "checkito_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "11zr0dk20vmwqy8pf46zqhh0awilfc09k4kpxqfbs207r3aw1spi") (r "1.65.0")))

(define-public crate-checkito_macro-0.1.1 (c (n "checkito_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "07wln53cdc0q9w03f71bs5jkqc65wby4ipadkdfakh5bz29canis") (r "1.65.0")))

