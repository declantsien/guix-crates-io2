(define-module (crates-io ch ec checked-float) #:use-module (crates-io))

(define-public crate-checked-float-0.1.0 (c (n "checked-float") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1bvzxfcz3a8jx0l5im2hc5xmq6998fkhcbq6i5ygsy49k27wsq4y")))

(define-public crate-checked-float-0.1.1 (c (n "checked-float") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1laib0162svgqr8k0q7gxys5if3sawbla9qqq3slaxbhwg2k2yav")))

(define-public crate-checked-float-0.1.2 (c (n "checked-float") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "11ybhwgj6c9g7sm2dwnqm01n3511hj60kkym8dy1ic1sb4yfjrbf")))

(define-public crate-checked-float-0.1.3 (c (n "checked-float") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1aq38gpli0hygc2wfhsqv0nqcfwp3ai0zlrxdl9ixyh1nrkaq0pg")))

(define-public crate-checked-float-0.1.4 (c (n "checked-float") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1msqakv6j9jxbjkiqh3hhh84w0ga7zg1wvc850qjmacbp8d1hkab")))

(define-public crate-checked-float-0.1.5 (c (n "checked-float") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "03ws7z4bbnjfdrmvz1dvrks6a0f836i762x8pr986ljbaili6fn0")))

