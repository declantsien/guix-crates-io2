(define-module (crates-io ch ec checks) #:use-module (crates-io))

(define-public crate-checks-0.0.0 (c (n "checks") (v "0.0.0") (h "05p17yfs9qi2z853pjyj8qzh6n14f5pg7mdfy0h1c0ai1rv4ydf0")))

(define-public crate-checks-1.0.0 (c (n "checks") (v "1.0.0") (h "0gnrjh83p931s59iicbn2x303hymsw98fymnidqi2r1h3fwk7r3l")))

(define-public crate-checks-1.0.1 (c (n "checks") (v "1.0.1") (h "1hnhn3z6jxb61ihwg2h203ri0phiba8kliqmnyy9x453axx0nbzr")))

(define-public crate-checks-1.0.2 (c (n "checks") (v "1.0.2") (h "1j1cj00nnnqc6wz0wlchv5xmylk6qz39d6vs1j1p7f8xv1ypsf6v")))

(define-public crate-checks-1.0.3 (c (n "checks") (v "1.0.3") (h "03wv8gaq419b0bvknlp5kdcghb9bmwk1psmqfj3ywjyxdfnpmkf6")))

(define-public crate-checks-1.0.4 (c (n "checks") (v "1.0.4") (h "0h2kx6w3c8579qq8mrmgl2fq56ccqf0qrx5cryrc9cki789v2zmd")))

(define-public crate-checks-1.0.5 (c (n "checks") (v "1.0.5") (h "186pvyvalhl19c40xkxb6pj4r5i51q8gvlyllvjaz7mi92ihli5h")))

(define-public crate-checks-1.0.6 (c (n "checks") (v "1.0.6") (h "1aaj3b9c2ibsc7h03s9qp8byyznyiqhyjshnjbglnwwp9wqpr7zk")))

