(define-module (crates-io ch ec check-symlinks) #:use-module (crates-io))

(define-public crate-check-symlinks-0.1.0 (c (n "check-symlinks") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1sqfbgax5x8gb0pyr690hvj909ksg61pixb7wmjfds2w1mkf1cr7")))

(define-public crate-check-symlinks-0.1.1 (c (n "check-symlinks") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1f5cks6vfz5vwxg38vl23w68wkppwah74jj78rgiqwwqq1qbv1ky")))

(define-public crate-check-symlinks-0.1.2 (c (n "check-symlinks") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1fp5fljvwd840h0s27j9yzfgjg7sihcd2qkp1lnmfimi286zx44d")))

(define-public crate-check-symlinks-0.2.0 (c (n "check-symlinks") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "077r0n13zvdfzm5l3i2ahglaxdlh5v3jl3c5j3z60zz3d6z6qvny")))

(define-public crate-check-symlinks-0.3.0 (c (n "check-symlinks") (v "0.3.0") (d (list (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0m7rx4dc9m6h32y65zm667qk14r9mgmga8jdvl3mz1hvmn1bf0gz")))

(define-public crate-check-symlinks-0.3.1 (c (n "check-symlinks") (v "0.3.1") (d (list (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1xsgzgyjsmrzr310g4jdvqjqkx9y4pn3c2qjw0b40w9dr63vdiw2")))

(define-public crate-check-symlinks-0.3.2 (c (n "check-symlinks") (v "0.3.2") (d (list (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "02qj2f0c8p5qd5vdq0knk78lqjh53408s2c36dpa7qx5gxs4qpj4")))

