(define-module (crates-io ch ec check_mate) #:use-module (crates-io))

(define-public crate-check_mate-0.1.0 (c (n "check_mate") (v "0.1.0") (h "0nvafp73iy9lnj8dmkk58fx306rknaxqrk32k2zwrfr37vw0jwqh")))

(define-public crate-check_mate-0.2.0 (c (n "check_mate") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0srjcnlnaladvhq3s017vhws3ksjvic6kzzmsijw88687l8qf4fx")))

