(define-module (crates-io ch ec checked_command) #:use-module (crates-io))

(define-public crate-checked_command-0.2.0 (c (n "checked_command") (v "0.2.0") (d (list (d (n "quick-error") (r "^1.0") (d #t) (k 0)))) (h "1mrgcmr5y8f87sqq4awmmri22c6z89vc1qj82pxin018kzkbhcvp") (f (quote (("use_std_output") ("process_try_wait" "nightly") ("nightly") ("enable_integration_tests") ("default") ("command_envs" "nightly"))))))

(define-public crate-checked_command-0.2.1 (c (n "checked_command") (v "0.2.1") (d (list (d (n "quick-error") (r "^1.0") (d #t) (k 0)))) (h "05r63c78qh57pkpdq7dvisv5qframjkzihw1lpnprl9zbx5ch0ms") (f (quote (("use_std_output") ("process_try_wait" "nightly") ("nightly") ("enable_integration_tests") ("default") ("command_envs" "nightly"))))))

(define-public crate-checked_command-0.2.2 (c (n "checked_command") (v "0.2.2") (d (list (d (n "quick-error") (r "^1.0") (d #t) (k 0)))) (h "1fnl02vlm7y7b8pqbvi9zynnhc5kdh5rf5s3bdwzyis65l5di3b0") (f (quote (("use_std_output") ("process_try_wait" "nightly") ("nightly") ("enable_integration_tests") ("default") ("command_envs" "nightly"))))))

(define-public crate-checked_command-0.3.0 (c (n "checked_command") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1qxc00y0d66d9bkfc7b3zw39w3p1c6575pfb22kpn3xjxbs38hrc") (f (quote (("default")))) (y #t)))

(define-public crate-checked_command-0.2.3 (c (n "checked_command") (v "0.2.3") (d (list (d (n "quick-error") (r "^1.0") (d #t) (k 0)))) (h "065zk5k1x22rgmmcfnvrqk4v2pyxmmyad2pw03f7c9bkvwf9zhh7") (f (quote (("use_std_output") ("process_try_wait" "nightly") ("nightly") ("enable_integration_tests") ("default") ("command_envs" "nightly"))))))

(define-public crate-checked_command-0.2.4 (c (n "checked_command") (v "0.2.4") (d (list (d (n "quick-error") (r "^1.0") (d #t) (k 0)))) (h "05bhmc2byqipdnww5h989xnvpkj2c8mgq9ayiv56g5k12r3j9z0n") (f (quote (("use_std_output") ("process_try_wait" "nightly") ("nightly") ("enable_integration_tests") ("default") ("command_envs" "nightly"))))))

