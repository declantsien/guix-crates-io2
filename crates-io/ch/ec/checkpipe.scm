(define-module (crates-io ch ec checkpipe) #:use-module (crates-io))

(define-public crate-checkpipe-0.1.0 (c (n "checkpipe") (v "0.1.0") (d (list (d (n "cargo-release") (r "^0.24.10") (d #t) (k 2)))) (h "064dmgwfpdlxgl3lxrya7pdl5xwx9zgslqjvxy6slv9iqwydb4jr")))

(define-public crate-checkpipe-0.1.1 (c (n "checkpipe") (v "0.1.1") (d (list (d (n "cargo-release") (r "^0.24.10") (d #t) (k 2)))) (h "16spy0z1rh23i4bnmbqmqyk462c7sxha4j9md2vqvwpvmr2n7qwc")))

