(define-module (crates-io ch ec checked) #:use-module (crates-io))

(define-public crate-checked-0.1.0 (c (n "checked") (v "0.1.0") (h "0yf9bm9aqq7dwjfcw3wb1mcg2gggpx2dwzwcnbjhijjvg234rlfj")))

(define-public crate-checked-0.1.1 (c (n "checked") (v "0.1.1") (h "0cwlvy195hw2rnf3b4x87qzx2dbgp9l4dsn8ix9pyp0ryhar2rv8")))

(define-public crate-checked-0.2.0 (c (n "checked") (v "0.2.0") (h "1l22rzzipv9swd5n8cfm4wj874ch9nw52pg2akwm9x8i3y86q4l4")))

(define-public crate-checked-0.3.0 (c (n "checked") (v "0.3.0") (h "13lr56ci1i90d11ibp4ya6jw99i6r7zqaml00kg97fy0djk45mqh")))

(define-public crate-checked-0.3.1 (c (n "checked") (v "0.3.1") (h "0h754flcf3v9s16wr6y925r2af8w99kkydrn73d3wrfgsppyv8a4")))

(define-public crate-checked-0.4.0 (c (n "checked") (v "0.4.0") (h "01bk9fbl78v3ggi6j7xlz43v268a301a6ms7hzr6bzi9bw0sl4c9")))

(define-public crate-checked-0.5.0 (c (n "checked") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0sdf1dwlhswnyjbhv9x3dacli2758qa6kd4yb8cy1pj43324caz8")))

