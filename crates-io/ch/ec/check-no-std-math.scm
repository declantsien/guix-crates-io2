(define-module (crates-io ch ec check-no-std-math) #:use-module (crates-io))

(define-public crate-check-no-std-math-0.0.1 (c (n "check-no-std-math") (v "0.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("cargo" "std"))) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "046yqbil4s0xyaifqr058yjnpr4g3whms1v1563c1j3mw49chiy0")))

