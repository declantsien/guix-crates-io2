(define-module (crates-io ch ec checkasum) #:use-module (crates-io))

(define-public crate-checkasum-0.1.0 (c (n "checkasum") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1isp17m2lp2bbn5fj4hi5pdz4x6dp2q9i8k201zyjhzlqfzc2xpy")))

(define-public crate-checkasum-0.1.1 (c (n "checkasum") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "022q2rk38x441dczwcsqkq38dy3lihrf1b58lkjlsxazpyyq43fh")))

(define-public crate-checkasum-1.0.0 (c (n "checkasum") (v "1.0.0") (d (list (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1n7asn51nc46a5n1lsh65z0rz500n33xminhlfinpxxjphzpaq47")))

(define-public crate-checkasum-1.1.0 (c (n "checkasum") (v "1.1.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0slnz8qv6phs1cawyy1688mk9hkz8kx6syy23bvcnjf619c0a4w2")))

(define-public crate-checkasum-1.1.1 (c (n "checkasum") (v "1.1.1") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1r9mk48p5abxjby6lw139m7pirsqaxmj05ddwjh0h8p47fz10idl")))

(define-public crate-checkasum-2.0.0 (c (n "checkasum") (v "2.0.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0wz8nz5k4k5zbwhzagpsij2i82is306qwjg01mh9xshrkj1sq7x7")))

(define-public crate-checkasum-2.1.0 (c (n "checkasum") (v "2.1.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0vpyvir71p01gs56avq9dhn0bf06fa0194vw8qnx42z5xr6120v0")))

(define-public crate-checkasum-2.2.0 (c (n "checkasum") (v "2.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0yhg9qm2l9pd2xq2l2gq1jsv1h9dzvnp65qw04r68zrwaqly9cfi")))

(define-public crate-checkasum-3.0.0 (c (n "checkasum") (v "3.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "01z1v0mfyp0wdlyq3dycab3zd3fp1w7gqzmzc5yxrga9lcqkp2ly")))

