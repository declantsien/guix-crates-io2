(define-module (crates-io ch ec checkmail) #:use-module (crates-io))

(define-public crate-checkmail-0.1.0 (c (n "checkmail") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "01xnfsmiv8llb964v2a3qhpy1ik8d422hhhz38pqfj25h837m0cz")))

(define-public crate-checkmail-0.1.1 (c (n "checkmail") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0scdmqqwgi7rqniy1pyz8n78jnr159wch1gmlfln7cv453wfzrsf")))

