(define-module (crates-io ch ec checkout_server_derive) #:use-module (crates-io))

(define-public crate-checkout_server_derive-0.0.1 (c (n "checkout_server_derive") (v "0.0.1") (d (list (d (n "checkout_core") (r "^0.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "040zhma3wv028bk25z6wz8c8zpgh8664licb5dxbnhr4ncp2ifp7")))

(define-public crate-checkout_server_derive-0.0.2 (c (n "checkout_server_derive") (v "0.0.2") (d (list (d (n "checkout_core") (r "^0.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "191425kzapgvzvfq63kgqz4lz9xs8rgfh4xxmjmfc6w6yrpywwcg")))

(define-public crate-checkout_server_derive-0.0.3 (c (n "checkout_server_derive") (v "0.0.3") (d (list (d (n "checkout_core") (r "^0.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pxd1fkk20wpwy157xqhsfx0al550b1ws807gm17rggkj6jx9n22")))

(define-public crate-checkout_server_derive-0.0.4 (c (n "checkout_server_derive") (v "0.0.4") (d (list (d (n "checkout_core") (r "^0.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zrnflgp3v3zbywkahifgzx3az5bxsfig8qxv9h46bk9xxhvjisn")))

(define-public crate-checkout_server_derive-0.0.5 (c (n "checkout_server_derive") (v "0.0.5") (d (list (d (n "checkout_core") (r "^0.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "022j1x4a8901s8py1z6xjjpf851p60qz8f8w9p0nlz6wf6f2q3cs")))

(define-public crate-checkout_server_derive-0.0.6 (c (n "checkout_server_derive") (v "0.0.6") (d (list (d (n "checkout_core") (r "^0.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "129pj6gw51q3wk9vp7nsvsglhix9qp5pp617pm5mgvdjxqfcqh0z")))

(define-public crate-checkout_server_derive-0.0.7 (c (n "checkout_server_derive") (v "0.0.7") (d (list (d (n "checkout_core") (r "^0.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kpki38qka2km6395lsv92kgg1x2wza3wchqlj5nbd7sr7qa10jr")))

(define-public crate-checkout_server_derive-0.0.8 (c (n "checkout_server_derive") (v "0.0.8") (d (list (d (n "checkout_core") (r "^0.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gak7l861rpm402bgi6ikz99kik3j0fwmf0gyw5qwlwa1xzlsdjm")))

(define-public crate-checkout_server_derive-0.0.9 (c (n "checkout_server_derive") (v "0.0.9") (d (list (d (n "checkout_core") (r "^0.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y5i734lzki0fj1wsv5qwzjrsnifgksi5ipx6n8h8vsa1gvfpr8q")))

(define-public crate-checkout_server_derive-0.0.10 (c (n "checkout_server_derive") (v "0.0.10") (d (list (d (n "checkout_core") (r "^0.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1frmymjrz2dnsqdib4isclc8zw0zakcdz4bh7ki4931mv1kx38vy")))

(define-public crate-checkout_server_derive-0.0.11 (c (n "checkout_server_derive") (v "0.0.11") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hxrj643s9w45xr3w6ahbaxnk7j9f1q4lia71k9jks70sm4w4hcb")))

(define-public crate-checkout_server_derive-0.0.12 (c (n "checkout_server_derive") (v "0.0.12") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1iwjahnq3dzixv1qgc855dirl0nfaf57ifwnhfknnd26k6p1m4qc")))

(define-public crate-checkout_server_derive-0.0.13 (c (n "checkout_server_derive") (v "0.0.13") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d491crwbl9rf5h101nrvf3dpi3zhgrqnn8y7x3c102x08rglfak")))

(define-public crate-checkout_server_derive-0.0.14 (c (n "checkout_server_derive") (v "0.0.14") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0avm23pbpvjrxg0zlcdqwl5k693dga6ksvbnzkgqswb25bcl8hc4")))

(define-public crate-checkout_server_derive-0.0.15 (c (n "checkout_server_derive") (v "0.0.15") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sm8zaakgzxzffinb2ian084yrsvbbrxxjj37sjqsp7b8mql3390")))

(define-public crate-checkout_server_derive-0.0.16 (c (n "checkout_server_derive") (v "0.0.16") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1250ch80ak3j02l93as02pw538i45zmgd0mm56r3d5a2zzxx0qch")))

(define-public crate-checkout_server_derive-0.0.17 (c (n "checkout_server_derive") (v "0.0.17") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0waqaynyzmzz7z4j7zcx5n0qlw8qyix52iw9sfkjdibbs33bapw9")))

(define-public crate-checkout_server_derive-0.0.18 (c (n "checkout_server_derive") (v "0.0.18") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y7yb2f8i91l39mnxlm9xfgxp460nndpfpg7gxsscplclcc7d0d4")))

(define-public crate-checkout_server_derive-0.0.19 (c (n "checkout_server_derive") (v "0.0.19") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mwyw77ah3r009l807skfpgf7ng7qcmhfg6v9rwpvdliq3p0kl8p")))

(define-public crate-checkout_server_derive-0.0.20 (c (n "checkout_server_derive") (v "0.0.20") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1563rsycsrsy4709a2pqkznf8zs1y3w019mzqps5bp7gbxmp94cd")))

(define-public crate-checkout_server_derive-0.0.21 (c (n "checkout_server_derive") (v "0.0.21") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nnqb48cq3rvlhgnqi91qn094g1zclprxq2i705shjrz76d5pvms")))

(define-public crate-checkout_server_derive-0.0.22 (c (n "checkout_server_derive") (v "0.0.22") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1isi2p5slsyr3aprsip6frinfgi23bhgqhhygmgljzf9x2dg3fcn")))

(define-public crate-checkout_server_derive-0.0.23 (c (n "checkout_server_derive") (v "0.0.23") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dsym02d0frq0bjww86pyk3bxzm4qhandpzlrd7358v1l6jx99i1")))

(define-public crate-checkout_server_derive-0.0.24 (c (n "checkout_server_derive") (v "0.0.24") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g20ya2fz5c5jgh2dm1d8izbaz9yba592k6cyr0z7cscgv2ijgsi")))

(define-public crate-checkout_server_derive-0.0.25 (c (n "checkout_server_derive") (v "0.0.25") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n27vfzkyks7j2h6pqshmdnic9d4fqclw59nx56l7ciqr9qbp4ai")))

(define-public crate-checkout_server_derive-0.0.26 (c (n "checkout_server_derive") (v "0.0.26") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jqf6hmdhp0l7q9d6z8ijpw1bw8irrndgyl73pbi011949j0v1b3")))

(define-public crate-checkout_server_derive-0.0.27 (c (n "checkout_server_derive") (v "0.0.27") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dp3krw42vrb2bypq3wk4nvc58l19iqkxqv69darvawsx2a80y5p")))

(define-public crate-checkout_server_derive-0.0.28 (c (n "checkout_server_derive") (v "0.0.28") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16qjkkk5x6aj2l4g9q4gpniw9ydmf2z4xrqh8ipg81faj2k0mkbx")))

(define-public crate-checkout_server_derive-0.0.29 (c (n "checkout_server_derive") (v "0.0.29") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17wklaikyv3sm8b5mzbx75h79hpc99np5c0qi6h16blrxxdg1hfs")))

(define-public crate-checkout_server_derive-0.0.30 (c (n "checkout_server_derive") (v "0.0.30") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0amn5yda0g5ml9qlsqwpq66y3lgq52dqgpl99w8538rrfsp2safh")))

(define-public crate-checkout_server_derive-0.0.31 (c (n "checkout_server_derive") (v "0.0.31") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d9zf2kvwry4np6p4vms1p7siwxl69h17k1jk7p8pvhvlcsq64z5")))

(define-public crate-checkout_server_derive-0.0.32 (c (n "checkout_server_derive") (v "0.0.32") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0brzs7nk956rjclzhl6jiwpxvv9v7kqdl87ihx4n1v96jksmqsmq")))

(define-public crate-checkout_server_derive-0.0.33 (c (n "checkout_server_derive") (v "0.0.33") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fhp1p38h8bqp5j3yri9qw6pi46nz57l5h9y90hcla6ch8qvbrzc")))

(define-public crate-checkout_server_derive-0.0.34 (c (n "checkout_server_derive") (v "0.0.34") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07kyy9pvzgx0wgxr8fwqwdpllplin30ch5yndhkz5p8kfnmlvyhq")))

(define-public crate-checkout_server_derive-0.0.35 (c (n "checkout_server_derive") (v "0.0.35") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v5zb6qndlcf71ykr2h2758rhds7w9bybpn94llklsgw6rxsm9g2")))

(define-public crate-checkout_server_derive-0.0.36 (c (n "checkout_server_derive") (v "0.0.36") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02xrsz4wagidj0b2w829ra17lb1l9c93hbx5dl2jxbwa4cp2g4j0")))

(define-public crate-checkout_server_derive-0.0.37 (c (n "checkout_server_derive") (v "0.0.37") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f36his0h57yg4m0i506ykixl94jj6d74ngzmb68i45vn13nm0rf")))

(define-public crate-checkout_server_derive-0.0.38 (c (n "checkout_server_derive") (v "0.0.38") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15f26mm1v55mv7qk4l676n6zrzgh87v84i1rd055lhazzsaqq9fx")))

(define-public crate-checkout_server_derive-0.0.39 (c (n "checkout_server_derive") (v "0.0.39") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mvvbn2h790vzrf01x632yv0ldb7cqpzr42v6b5gk1mpvp5wv70n")))

(define-public crate-checkout_server_derive-0.0.40 (c (n "checkout_server_derive") (v "0.0.40") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hdi1in8wkkp14bibfzhnp4603j5dnamgrg99m89jjzfa5fyy6ww")))

(define-public crate-checkout_server_derive-0.0.41 (c (n "checkout_server_derive") (v "0.0.41") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01r8b0mvn2xvfd2sdmnpyal9m6zrzjszfir164fim4x7vb8va0r7")))

(define-public crate-checkout_server_derive-0.0.42 (c (n "checkout_server_derive") (v "0.0.42") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xsb3p9my9a4vqy8dwazyc1g7p647353f6mr67s9hb4zgpnb6mkh")))

(define-public crate-checkout_server_derive-0.0.43 (c (n "checkout_server_derive") (v "0.0.43") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yziy7ln5f5dn4h8rx44i8i8xcxr8y26rb6i2ybgzwkwd5bmf7rk")))

(define-public crate-checkout_server_derive-0.0.44 (c (n "checkout_server_derive") (v "0.0.44") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jagljc7hmv9b3xmj77f5nf7and35f37ybd48nzify0bdl8mill9")))

(define-public crate-checkout_server_derive-0.0.45 (c (n "checkout_server_derive") (v "0.0.45") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cfw3j62d62x9rmv1wbqji67x0icarwgvv96b6jsnhiyf3bpc2ws")))

(define-public crate-checkout_server_derive-0.0.46 (c (n "checkout_server_derive") (v "0.0.46") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xpapl60cjrci9vzac7yd8v7mlhnfsgfs2vavkni0a5pnjihxr7m")))

(define-public crate-checkout_server_derive-0.0.47 (c (n "checkout_server_derive") (v "0.0.47") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01i2p5yf4h4ch3kpz8gx9mp1fnqibldaxhfrw1qa411mbsgg6fzn")))

(define-public crate-checkout_server_derive-0.0.48 (c (n "checkout_server_derive") (v "0.0.48") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vkvhcn5n18mlc8gymyndh1cx63sqmp07jk11jsgfrrqpxl9iwab")))

(define-public crate-checkout_server_derive-0.0.49 (c (n "checkout_server_derive") (v "0.0.49") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10g6yx0wlfkzl0677z3jy41n3hgpi8y161k93is57vwmwf6s0fmd")))

(define-public crate-checkout_server_derive-0.0.50 (c (n "checkout_server_derive") (v "0.0.50") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mp668ygyfp9c4gzfcxjyj52xvgq9faa7k4z44wqjlxawwvakn12")))

(define-public crate-checkout_server_derive-0.0.51 (c (n "checkout_server_derive") (v "0.0.51") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qidkymh0xybqkmky4ivq7y832pr66zfl6jny821vaq8gzxvin5z")))

(define-public crate-checkout_server_derive-0.0.52 (c (n "checkout_server_derive") (v "0.0.52") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qk23mg5fzvl67jlz5kwmf5kwzw85kjwl1d8s14qfzydngj6gwaj")))

(define-public crate-checkout_server_derive-0.0.53 (c (n "checkout_server_derive") (v "0.0.53") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l0pxh9ik03w1qicf6xi3bjh2p1bv06yqlimj3bs137am42dcnmn")))

(define-public crate-checkout_server_derive-0.0.54 (c (n "checkout_server_derive") (v "0.0.54") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01l0yr4a3c3ymr7km8jrg8kbynqx00zqqrjmmj7kp4g1xgxh9xv2")))

(define-public crate-checkout_server_derive-0.0.55 (c (n "checkout_server_derive") (v "0.0.55") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j4sar4dr8v8x7s18xbd5yljq28vbfd2k9bws65ncc64k1aa538y")))

(define-public crate-checkout_server_derive-0.0.56 (c (n "checkout_server_derive") (v "0.0.56") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jh8a08yb11pjd77f8970qlym0iw8vhb9sx835siq22h96v4jl3y")))

(define-public crate-checkout_server_derive-0.0.57 (c (n "checkout_server_derive") (v "0.0.57") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cq8l7a4197ksrn0n3plh3avcrmj09byrmcg0bna736q3c3m6bq6")))

(define-public crate-checkout_server_derive-0.0.58 (c (n "checkout_server_derive") (v "0.0.58") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kj1sqhpn02kgirypamc39ahx3nhkvf7h4xxm7qm4s87ikir2gjv")))

(define-public crate-checkout_server_derive-0.0.59 (c (n "checkout_server_derive") (v "0.0.59") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bjk6pyp8gwln135nnvbjxxyiypii4iki5mgpm66ww6m292rkmdr")))

(define-public crate-checkout_server_derive-0.0.60 (c (n "checkout_server_derive") (v "0.0.60") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14alifp7yda02z0r0cwd0pbnjbs9r4wxhrdx21g9jdvabixk16p6")))

(define-public crate-checkout_server_derive-0.0.61 (c (n "checkout_server_derive") (v "0.0.61") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p33w3icjynanigg6b5iylvi7db4cj5mwqg0ys0kz07y0xm48xwm")))

(define-public crate-checkout_server_derive-0.0.62 (c (n "checkout_server_derive") (v "0.0.62") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rwxq6n1r2zijm6clqhl3n7x63i4ib2x1g9pab0qch8273pyam7r")))

(define-public crate-checkout_server_derive-0.0.63 (c (n "checkout_server_derive") (v "0.0.63") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qwq07vxjwfp5yh1lh3sbllj9x5mazwaxvlbs48sl297z8fmx7z2")))

(define-public crate-checkout_server_derive-0.0.64 (c (n "checkout_server_derive") (v "0.0.64") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p33v1i95f9n8k6hncjpyvisnp9njxw0kmzn47pydqj7s7ihahnp")))

(define-public crate-checkout_server_derive-0.0.65 (c (n "checkout_server_derive") (v "0.0.65") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01p5n0fkcd5gnbkr46n3q3xhaaj3wqrxsj3wsr1prkxlj0a2m70q")))

(define-public crate-checkout_server_derive-0.0.66 (c (n "checkout_server_derive") (v "0.0.66") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xhd1q23lzkw7w21j3pgamgvazi68zhni93r63w7q46vk18jsmmw")))

(define-public crate-checkout_server_derive-0.0.67 (c (n "checkout_server_derive") (v "0.0.67") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a57f4rv9zqf033qj8xrcha0lsc35pq3qccshcmkxfnjqzd94y7r")))

(define-public crate-checkout_server_derive-0.0.68 (c (n "checkout_server_derive") (v "0.0.68") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m8g3d3gycg3jycl6z58b856x9sad9m0r07zjyli8k0hy32amsrk")))

(define-public crate-checkout_server_derive-0.0.69 (c (n "checkout_server_derive") (v "0.0.69") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jpj24svwhxcdiq2139xcxssr2njf5f7jsvzn4zb3h2zwv4yzcx7")))

(define-public crate-checkout_server_derive-0.0.70 (c (n "checkout_server_derive") (v "0.0.70") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hbmny448i3n9p54qizjrnw86w5n0rayvhm1bknhdi81lm2cg8pi")))

(define-public crate-checkout_server_derive-0.0.71 (c (n "checkout_server_derive") (v "0.0.71") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wgzs151ha0lxwhlszbnfg1dayl36l2hbb953yswk4v6q3cjhda6")))

(define-public crate-checkout_server_derive-0.0.72 (c (n "checkout_server_derive") (v "0.0.72") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fn0655yhh3v32ygngl5bwl7ryr0ga7hxxb13ylnqjwxl0llm207")))

(define-public crate-checkout_server_derive-0.0.73 (c (n "checkout_server_derive") (v "0.0.73") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ab9m24m4idg0cjj7hnm8f76g0s38jl2r9nvpabdscpycyc9ijk2")))

(define-public crate-checkout_server_derive-0.0.74 (c (n "checkout_server_derive") (v "0.0.74") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b93xsga30p663k93pdhybpjpja72fc0rxf0zlli90w9sjd3apn0")))

(define-public crate-checkout_server_derive-0.0.75 (c (n "checkout_server_derive") (v "0.0.75") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a6g5d8vmqv1rrvfbl50nygw5ql1frlw9qikgc9kzaag1l8d7mzj")))

(define-public crate-checkout_server_derive-0.0.76 (c (n "checkout_server_derive") (v "0.0.76") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19lm57mnrgbsvyzyaxnqkzwlpkbsjmz52wn7i8c7snrwm7yzahpp")))

(define-public crate-checkout_server_derive-0.0.77 (c (n "checkout_server_derive") (v "0.0.77") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gck0cj9pvfhczz56whfm131280kcpq486vip4m1mwdy5qkfjq6v")))

(define-public crate-checkout_server_derive-0.0.78 (c (n "checkout_server_derive") (v "0.0.78") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ma1x09z12wha798w8wwfrpmbbv6kkhq7ssgs0012acqwil0gb4g")))

(define-public crate-checkout_server_derive-0.0.79 (c (n "checkout_server_derive") (v "0.0.79") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n2620gyxrsyqykzk928z5xl4k5kbx8mg010m6vlv2ix1pm5s27d")))

(define-public crate-checkout_server_derive-0.0.80 (c (n "checkout_server_derive") (v "0.0.80") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wgp568qi1nzgw2c9wqbgcyzxqdjs7b6sc9gmskh2aa79468qqkx")))

(define-public crate-checkout_server_derive-0.0.81 (c (n "checkout_server_derive") (v "0.0.81") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v4538xfn45hsvgav84px7qi4lal3jwn3p6kfg926zr685w6mjsp")))

(define-public crate-checkout_server_derive-0.0.82 (c (n "checkout_server_derive") (v "0.0.82") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0axpzk855qwbcdyzs5j4jfk52sjwjksqz7mkbffj00k1gi3a8i36")))

(define-public crate-checkout_server_derive-0.0.83 (c (n "checkout_server_derive") (v "0.0.83") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cxc2pycixbib94dw5xcp6k22nm5ynzsp9pglkvzbnjjcvjjpklw")))

(define-public crate-checkout_server_derive-0.0.84 (c (n "checkout_server_derive") (v "0.0.84") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11628575arncq68pfvlagy4jy954mgyplvc5acdxw1xmksisq2y6")))

(define-public crate-checkout_server_derive-0.0.86 (c (n "checkout_server_derive") (v "0.0.86") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "098scsr2frhzdq5sg3pwa8dl1g1rkis8xlfwhlwjcc1xwyb85c6m")))

(define-public crate-checkout_server_derive-0.0.87 (c (n "checkout_server_derive") (v "0.0.87") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w4241paixmrf5icqkvcib8ghy5xc9nz5qp8vvg292g7r4fad5cc")))

(define-public crate-checkout_server_derive-0.0.88 (c (n "checkout_server_derive") (v "0.0.88") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kph8cjnzwba7dhy54p0gyjjgq6rdz6gvy6qdpfq9k796xwda87d")))

(define-public crate-checkout_server_derive-0.0.90 (c (n "checkout_server_derive") (v "0.0.90") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ydlw3afjh7zp2n2v4r8llcxcj6icsx2w4ayikdhznnxzn4f6aqr")))

(define-public crate-checkout_server_derive-0.0.91 (c (n "checkout_server_derive") (v "0.0.91") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19zhrwcnbf52c7sw5jdp9v7k3z6afsprvb244a5c3v83cqrhlgbw")))

(define-public crate-checkout_server_derive-0.0.92 (c (n "checkout_server_derive") (v "0.0.92") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13c1q1dycw4p6z51xrjyjqdpqyb06gs0ri4kswczx5najapg4f42")))

(define-public crate-checkout_server_derive-0.0.95 (c (n "checkout_server_derive") (v "0.0.95") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zq6jagghdxpcxirb44151qgqg53nwpksa8pj3c70bpk6r1qq92y")))

(define-public crate-checkout_server_derive-0.0.108 (c (n "checkout_server_derive") (v "0.0.108") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lbd4rxixyp1qmk0qg2jxjcyi8j00w7pqg9ydiafwgjslynq97sf")))

(define-public crate-checkout_server_derive-0.0.109 (c (n "checkout_server_derive") (v "0.0.109") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zmd7ix7mzmxpgkmks2f6sa5sm3g3008yj3cn8q3f77aqlgapqif")))

(define-public crate-checkout_server_derive-0.0.110 (c (n "checkout_server_derive") (v "0.0.110") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hsqx4zgxylyqhl3q1hc79msyb95ysf8jgpjl0dm0ziq7kj0s4mn")))

(define-public crate-checkout_server_derive-0.0.111 (c (n "checkout_server_derive") (v "0.0.111") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kc7gyi9qxrw3w4fipr4yw6insxsxvn2vzs8gmg928arwrg46ajw")))

(define-public crate-checkout_server_derive-0.0.112 (c (n "checkout_server_derive") (v "0.0.112") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "189360n39ks0k1fpg5xah626fdw8nplh627gls3hpim69734y0sq")))

(define-public crate-checkout_server_derive-0.0.113 (c (n "checkout_server_derive") (v "0.0.113") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pzarmf22iwxnw196jvagqcsfgba0v8y6dzj84myikai22i3d5ch")))

(define-public crate-checkout_server_derive-0.0.114 (c (n "checkout_server_derive") (v "0.0.114") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dajzcil43g07xn8z1l6dkkw6p5f3vs2x299db5n2g3f3w9sr8v7")))

(define-public crate-checkout_server_derive-0.0.115 (c (n "checkout_server_derive") (v "0.0.115") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03xv5d83hcald797dbi8s9zv7xdpizvyxfjiflk7a8iw09izmv3g")))

(define-public crate-checkout_server_derive-0.0.116 (c (n "checkout_server_derive") (v "0.0.116") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v1yx6z5c6djpwicg9rgkyj3g8dzqh83ig4j7z7hf97z963d8dcc")))

(define-public crate-checkout_server_derive-0.0.117 (c (n "checkout_server_derive") (v "0.0.117") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y3ndyy53hki08jq294z8hijfhlv90xz5kdrxwsfqs2wwl6r3awn")))

(define-public crate-checkout_server_derive-0.0.118 (c (n "checkout_server_derive") (v "0.0.118") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yj5w4ysky9y4qh4083xjrsz4ax1yf97b5k2dihp7rric313s4f6")))

(define-public crate-checkout_server_derive-0.0.119 (c (n "checkout_server_derive") (v "0.0.119") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nlm3wfpnzq0dbzq3lksq1xpfrv3yjg99wb6asid1k8k6wa1anxv")))

(define-public crate-checkout_server_derive-0.0.120 (c (n "checkout_server_derive") (v "0.0.120") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0daych8j29lw4kaxc9p6svwvjm2yn3nvywa082nv5p93ya4q4l1d")))

(define-public crate-checkout_server_derive-0.0.121 (c (n "checkout_server_derive") (v "0.0.121") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15mg9k39ny8pxhxrdmrfib6cd7hlb8dc15sz4jwq4rvv07qzzh2b")))

(define-public crate-checkout_server_derive-0.0.122 (c (n "checkout_server_derive") (v "0.0.122") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "145ncxpmpx9z6d743znqxz1psph0wf65f55k9w9xh618i6iyvmq6")))

(define-public crate-checkout_server_derive-0.0.123 (c (n "checkout_server_derive") (v "0.0.123") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z4dg9401wf9ywcqa9bll44nppanlm9l2ll6njn7lsd5sg8sfw5d")))

(define-public crate-checkout_server_derive-0.0.124 (c (n "checkout_server_derive") (v "0.0.124") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cby91nc6wi0vcq9d2d7dbgzdrplcagcpz2w8y10jp0xxj3i8j1b")))

(define-public crate-checkout_server_derive-0.0.125 (c (n "checkout_server_derive") (v "0.0.125") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0myhgm0s35k6n85cw4si76anilkzcn8yjsgii8s0g9n534l8895d")))

(define-public crate-checkout_server_derive-0.0.126 (c (n "checkout_server_derive") (v "0.0.126") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rgay4sn4z2hn2fkqflhqqvydjih5zlrcjdln0wsqrjvi0smjwb9")))

(define-public crate-checkout_server_derive-0.0.127 (c (n "checkout_server_derive") (v "0.0.127") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cbyq15hgqrxzzfir8qwr4s0wn3gwvsl71a7z1rgwd5brlxf37zb")))

(define-public crate-checkout_server_derive-0.0.128 (c (n "checkout_server_derive") (v "0.0.128") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rybl4p1s16w26dy8zny7l80pdwyjqb4gwjyrcfp5vy3l42rkwqn")))

(define-public crate-checkout_server_derive-0.0.129 (c (n "checkout_server_derive") (v "0.0.129") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xibnrjhxy1lh4y68qwxb2bmvl1z6422mjn7sb1lnsdd550r1m9m")))

(define-public crate-checkout_server_derive-0.0.130 (c (n "checkout_server_derive") (v "0.0.130") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0crm17nrhci7z60xq77vmfr10dr1mj7i3mzk9qfcm92nqz6z9xsh")))

(define-public crate-checkout_server_derive-0.0.131 (c (n "checkout_server_derive") (v "0.0.131") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06k7px06f7xs2xs1254wyx8zsdjz6ki5vx3vm38yxawanfvzl79w")))

(define-public crate-checkout_server_derive-0.0.132 (c (n "checkout_server_derive") (v "0.0.132") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06k1y1xcksz14vdbmiga11idsqvjn4rmqpmg58nrjrb9r6q3bqzm")))

