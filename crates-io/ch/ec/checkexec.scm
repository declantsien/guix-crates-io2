(define-module (crates-io ch ec checkexec) #:use-module (crates-io))

(define-public crate-checkexec-0.1.1 (c (n "checkexec") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "0kdzl5mmsx2vwdnmc95sb9cnjzv6kcb5gigydny1g98imynr2zzk")))

(define-public crate-checkexec-0.1.2 (c (n "checkexec") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "15w11s7ic4nfsd036dyphm0slbdi713b5ig4slq6yf9r8v5hjiy3")))

(define-public crate-checkexec-0.1.3 (c (n "checkexec") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "1xwyi8i51v8lfds5n3fqvwgwpwi4sih18llbsjwxjjrishd0hz8i")))

(define-public crate-checkexec-0.1.6 (c (n "checkexec") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "02nrlx1lvyhd2xhfdg00qvgnppjcn7ny3d188ys5nbrc0i1sl0mg")))

(define-public crate-checkexec-0.1.9 (c (n "checkexec") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "01fknnwkdjz4l9ri7ra4s44p24mnj0if47dq1vydgw1ff12ipc16")))

(define-public crate-checkexec-0.1.10 (c (n "checkexec") (v "0.1.10") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0jlanx9ds35ldzs89p1vb24hh6l6645yg40wdkyq1hcap2dsw9mm")))

(define-public crate-checkexec-0.1.12 (c (n "checkexec") (v "0.1.12") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0xq7fflnxzp5abamx0achwh71jscqvc5rxswnvmhj00aj7sx64sz")))

(define-public crate-checkexec-0.1.13 (c (n "checkexec") (v "0.1.13") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1h99cc4mn0m9k8pim4qwvy33vfrqxphwad654p4b04aycg8icsxz")))

(define-public crate-checkexec-0.1.14 (c (n "checkexec") (v "0.1.14") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1kr5ijq2yhdz0g1zxlfws9w5ncv0xis5sphllxzgbmg9q20sp150")))

(define-public crate-checkexec-0.1.15 (c (n "checkexec") (v "0.1.15") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0dkqx1bmhdqsr90zkbydb1rjrrir4mnaizhssz2czgmsjafwh11h")))

(define-public crate-checkexec-0.1.16 (c (n "checkexec") (v "0.1.16") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0x3mx2qbmdydmbryrfy0pv9s57d9rs6j67apd8ig5psivlky6nvs")))

(define-public crate-checkexec-0.2.0 (c (n "checkexec") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0z8fivsqhvm5ismh52zmh5ib32dnq41054m5adlffl7rs7fsxfcq")))

