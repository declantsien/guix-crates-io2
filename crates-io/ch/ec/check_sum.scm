(define-module (crates-io ch ec check_sum) #:use-module (crates-io))

(define-public crate-check_sum-1.0.0 (c (n "check_sum") (v "1.0.0") (d (list (d (n "clap") (r "^3") (f (quote ("std" "color" "derive" "cargo"))) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0hf7vwsnxc1sdrrfs8rhvpr7m5f8885nffckm8ck27xhxiw9dxb5") (r "1.59")))

(define-public crate-check_sum-1.0.1 (c (n "check_sum") (v "1.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("std" "color" "derive" "cargo"))) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1kv4jb19anq5pb4r5b8zkq2x6c6vmb6mbjas2dmn3k9hp4mxg8zr") (r "1.59")))

