(define-module (crates-io ch ec checkasm) #:use-module (crates-io))

(define-public crate-checkasm-0.1.0 (c (n "checkasm") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "nasm-rs") (r "^0.2.0") (d #t) (k 1)))) (h "0wpgqqxrszmqca85fvl69s2s12ks3q7gqvcdq0aldscflc7ylshz")))

