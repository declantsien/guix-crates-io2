(define-module (crates-io ch u- chu-liu-edmonds) #:use-module (crates-io))

(define-public crate-chu-liu-edmonds-0.1.0 (c (n "chu-liu-edmonds") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14") (d #t) (k 2)) (d (n "ordered-float") (r "^4") (d #t) (k 0)))) (h "0alcnj1wz6c4vz0nv8h8zfmw604m0np2a7ciagcjlq070xvs3x03") (r "1.66")))

