(define-module (crates-io ch oi choice_nocase) #:use-module (crates-io))

(define-public crate-choice_nocase-0.1.1 (c (n "choice_nocase") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15fxm0r5bcq77r4bbcva1n63lki805mxsx6myylhckgg2i1c2ryf")))

(define-public crate-choice_nocase-0.1.2 (c (n "choice_nocase") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "09qajcn3i5skmhwqwfx8d6hq4a5lxfdpqsi70s531a3lvp0xaxvb")))

(define-public crate-choice_nocase-0.2.0 (c (n "choice_nocase") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1g4smya6awqydv5wj659pb8qixai4hi5h1wina7v7ja4mb01d28q")))

