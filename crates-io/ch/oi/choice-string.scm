(define-module (crates-io ch oi choice-string) #:use-module (crates-io))

(define-public crate-choice-string-0.0.1 (c (n "choice-string") (v "0.0.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "range_union_find") (r "^0.4.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b4bwhx7ajn1nz6vrn4c0810h269bpsa0c913p9z5rgd1s784q72") (r "1.56")))

