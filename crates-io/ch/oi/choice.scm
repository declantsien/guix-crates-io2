(define-module (crates-io ch oi choice) #:use-module (crates-io))

(define-public crate-choice-0.0.0 (c (n "choice") (v "0.0.0") (h "0k7dyh9akfcwd9w5dzzqp7xhac425kr6hccsawhx8gd8wygbmj9s")))

(define-public crate-choice-0.0.1 (c (n "choice") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h1dpnvn4qkkakm43dshsz5f327b6znbkxwa0x48lkr0ym15sqxs")))

(define-public crate-choice-0.0.2 (c (n "choice") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0lv58s9zw2irvqzg9szjrp85d088bn25rnisjcm61byy4741zdx3")))

