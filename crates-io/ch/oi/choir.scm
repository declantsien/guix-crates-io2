(define-module (crates-io ch oi choir) #:use-module (crates-io))

(define-public crate-choir-0.1.0 (c (n "choir") (v "0.1.0") (d (list (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fqr14kq1lmf4m8da0ki7678rhqgfmv9vkpbcckl9i0q14f44dsj")))

(define-public crate-choir-0.2.0 (c (n "choir") (v "0.2.0") (d (list (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cbs7ygb3rxsvpix85z852vfq638bm9v0954bnah86ay6qhfamxk")))

(define-public crate-choir-0.3.0 (c (n "choir") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "0npgpw6bc4afl990dq2ff95wp6z63h0p0q2pajmzwd5cswhwsnng")))

(define-public crate-choir-0.4.0 (c (n "choir") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "0j1hqa1mjr3y5y4r4d4a9ypazrv6rqh8xwnd737gnflrrfylh11x") (y #t)))

(define-public crate-choir-0.4.1 (c (n "choir") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "0igy5kc0qqp0lkmqaf1bm6a8bk3bs75r1rphwv72lxrbkq54yc7d")))

(define-public crate-choir-0.4.2 (c (n "choir") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "1c5n25p2y9cihjds5j4fr6yavyg2f3yqg6d4pp8h6m76jw12q61d")))

(define-public crate-choir-0.5.0 (c (n "choir") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "1w9r8l5q5kyg28dad7711xb9p6vs90lqw5nq0in28y4zjag674nm")))

(define-public crate-choir-0.6.0 (c (n "choir") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "1r3x59g94hblwa3lbngn1j5xhca0as9fqdcvvzz5gxgd0wkh0fr1")))

(define-public crate-choir-0.7.0 (c (n "choir") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam-deque") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "1wxbnz20irn5743akk889bxpslprka380wm5w9l4m2n7fn8p6f0j")))

