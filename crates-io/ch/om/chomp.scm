(define-module (crates-io ch om chomp) #:use-module (crates-io))

(define-public crate-chomp-0.1.0 (c (n "chomp") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.10") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (k 0)))) (h "0gg2dnmfpsyw276ki99d4w6cnscspkb03cd0rqsbckviislp9n20") (f (quote (("verbose_error") ("nightly_test" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.1.1 (c (n "chomp") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.10") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (k 0)))) (h "0dby2kdj78f7hlx1ssf4m2j0y3kngaxqsayylgiyrl9zz955k7y0") (f (quote (("verbose_error") ("nightly_test" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.1.2 (c (n "chomp") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.10") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (k 0)))) (h "10zvn8lxy6df87p9qabxxbg1c4agsp32c7irsnsbnh0r2mj4i0z5") (f (quote (("verbose_error") ("nightly_test" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.2.0 (c (n "chomp") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (k 0)))) (h "0drj2fbk4y4qxiv9f5fvjard6kif0f5v2g9xzh4xg0l7kbpl9cwy") (f (quote (("verbose_error") ("unstable" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.2.1 (c (n "chomp") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (k 0)))) (h "0wsf9qsqaz7wflgg27n9dlsc5zpypwiv2x1y25p5xg8j8s7sm21h") (f (quote (("verbose_error") ("unstable" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.2.2 (c (n "chomp") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (k 0)))) (h "0397xcj8d73cszjhhlvzgijnbbq3lvbnn0bla94842lzqgaps730") (f (quote (("verbose_error") ("unstable" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.2.3 (c (n "chomp") (v "0.2.3") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (k 0)))) (h "1v0dca192y2v5x1vnf0c985bkhvmic1g9ks5p9n0ih5ry1ij1npl") (f (quote (("verbose_error") ("unstable" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.2.4 (c (n "chomp") (v "0.2.4") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (k 0)))) (h "04vxr7ghsszyzkpp9bw87jy5hqchlsg448map5h81sg3c4axryjv") (f (quote (("verbose_error") ("unstable" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.2.5 (c (n "chomp") (v "0.2.5") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (k 0)))) (h "0qzakgillg7ahlkrikhl53zxg68viw2jzanxw1xvz8mi4kc7d6d7") (f (quote (("verbose_error") ("unstable" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.2.6 (c (n "chomp") (v "0.2.6") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (f (quote ("std"))) (k 0)))) (h "0q9kya0685h9f9nik928726f36ray4wkffz7bhysk9ym007pvanx") (f (quote (("verbose_error") ("unstable" "compiletest_rs") ("default" "verbose_error"))))))

(define-public crate-chomp-0.3.0 (c (n "chomp") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clippy") (r "> 0.0.1") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (f (quote ("std"))) (k 0)) (d (n "debugtrace") (r "^0.1.0") (d #t) (k 0)) (d (n "tendril") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "0f69gcnvgi9gfjycl9xy3k8zfmkcid2f68fxw2gc9nrfv4snnqkr") (f (quote (("unstable" "compiletest_rs") ("noop_error_and_backtrace" "noop_error" "backtrace") ("noop_error") ("backtrace" "debugtrace/backtrace"))))))

(define-public crate-chomp-0.3.1 (c (n "chomp") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "clippy") (r "> 0.0.1") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (f (quote ("std"))) (k 0)) (d (n "debugtrace") (r "^0.1.0") (d #t) (k 0)) (d (n "either") (r "^0.1.7") (d #t) (k 0)) (d (n "tendril") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "0g8nblv76q72gcliihkapa01wqhxizxr7xi3zl8rncv6iqhssx4z") (f (quote (("unstable" "compiletest_rs") ("noop_error_and_backtrace" "noop_error" "backtrace") ("noop_error") ("backtrace" "debugtrace/backtrace"))))))

