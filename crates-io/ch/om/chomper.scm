(define-module (crates-io ch om chomper) #:use-module (crates-io))

(define-public crate-chomper-0.1.0 (c (n "chomper") (v "0.1.0") (h "0kwfjv7ygc2l6wigrc9yflba3hp618kajb0wgsq4hfac0dsxaym0") (y #t)))

(define-public crate-chomper-0.1.1 (c (n "chomper") (v "0.1.1") (h "1sam6g1f7qa7hmz108xl6d2r76a6gp798m7b179h12kggi8g85ha") (y #t)))

(define-public crate-chomper-0.1.2 (c (n "chomper") (v "0.1.2") (h "1p8irpqlbv37fbqnkmig68jwghx0n0j1r3xa3hazk8xdi1bk649x") (y #t)))

(define-public crate-chomper-0.1.3 (c (n "chomper") (v "0.1.3") (h "18fy0b0sv2yrg3sqbk33bghj2z952gc0vwp5q3mxx3n323hvh8al") (y #t)))

(define-public crate-chomper-0.1.4 (c (n "chomper") (v "0.1.4") (h "0iy1pj2jsjy1dn59ahs8ndxjjgsbf0iw03lypabsb61aj69983lh") (y #t)))

(define-public crate-chomper-0.1.5 (c (n "chomper") (v "0.1.5") (h "11nfi4vjmwrzvjzfangzh6bbr3ypidsrv983gbldvmspbkvddlrr") (y #t)))

(define-public crate-chomper-0.1.6 (c (n "chomper") (v "0.1.6") (h "03n7xww33g81j4kn972yywxgav0iksrpfpsja9pxb925n7l8yiyr") (y #t)))

(define-public crate-chomper-0.1.7 (c (n "chomper") (v "0.1.7") (h "04wfadlqai9l4q4d2r0lfvlcisnc2aznhh7dv15lkhnn3bfs9b9s") (y #t)))

(define-public crate-chomper-0.1.8 (c (n "chomper") (v "0.1.8") (h "11bhgcx33q98q8p9s9ydmnhxhjkamlr5mv15jzwlcp1iykfmi36a") (y #t)))

