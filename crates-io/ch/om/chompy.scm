(define-module (crates-io ch om chompy) #:use-module (crates-io))

(define-public crate-chompy-0.1.0 (c (n "chompy") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "peekmore") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "09yl199qi9335k65pm4hpv7rzphpnlpzz41cp8bivv4c1zdrvp76")))

(define-public crate-chompy-0.1.1 (c (n "chompy") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "peekmore") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1baflq4xblhrpjvsz8mg6kwlr7sw7f0m2chys6mnri7rnc0kkl6p")))

(define-public crate-chompy-0.1.2 (c (n "chompy") (v "0.1.2") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "peekmore") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "11z6v56szqybzw0r648dpdzp2zjz17d0869hhcxlrd76c4mjscv6")))

