(define-module (crates-io ch om chomp-nl) #:use-module (crates-io))

(define-public crate-chomp-nl-0.1.0 (c (n "chomp-nl") (v "0.1.0") (h "177iynscpfc0gqmlhbb7sh28cckhlpsnl6lsn741sbv8i17m0wb0")))

(define-public crate-chomp-nl-0.1.1 (c (n "chomp-nl") (v "0.1.1") (h "17jlsn3x3lkmp8jfnq7lf63h7qyyymzc1lj4h0mc9zwsjfdrrpfn")))

(define-public crate-chomp-nl-0.1.2 (c (n "chomp-nl") (v "0.1.2") (h "1rm6bhzxl4n9ag1zl4h5ygjbd416b0pj22p7v3cx4901rbldkkap")))

