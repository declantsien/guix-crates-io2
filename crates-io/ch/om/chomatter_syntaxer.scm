(define-module (crates-io ch om chomatter_syntaxer) #:use-module (crates-io))

(define-public crate-chomatter_syntaxer-0.0.1 (c (n "chomatter_syntaxer") (v "0.0.1") (d (list (d (n "chomatter_commons") (r "^0.0.1") (d #t) (k 0)) (d (n "chomatter_lexer") (r "^0.0.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1bjymrac4ar34fj6mdj76mss0hwis8immzp36p4zk89qasv273ir")))

