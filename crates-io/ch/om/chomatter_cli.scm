(define-module (crates-io ch om chomatter_cli) #:use-module (crates-io))

(define-public crate-chomatter_cli-0.0.1 (c (n "chomatter_cli") (v "0.0.1") (d (list (d (n "chomatter_commons") (r "^0.0.1") (d #t) (k 0)) (d (n "chomatter_lexer") (r "^0.0.1") (d #t) (k 0)) (d (n "chomatter_syntaxer") (r "^0.0.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1h1ajprlckqwakiqvdz5v7iml7wb7qx8czj2pcmwnd8s1vildiis")))

(define-public crate-chomatter_cli-0.0.2 (c (n "chomatter_cli") (v "0.0.2") (d (list (d (n "chomatter_commons") (r "^0.0.1") (d #t) (k 0)) (d (n "chomatter_lexer") (r "^0.0.1") (d #t) (k 0)) (d (n "chomatter_syntaxer") (r "^0.0.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1nidrbs33a97n0lvms2jhrb3id3yz3hbbhfbjnfrxh4il0233wgx")))

(define-public crate-chomatter_cli-0.0.3 (c (n "chomatter_cli") (v "0.0.3") (d (list (d (n "chomatter_commons") (r "^0.0.1") (d #t) (k 0)) (d (n "chomatter_lexer") (r "^0.0.1") (d #t) (k 0)) (d (n "chomatter_syntaxer") (r "^0.0.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1z66d36bj8f4s06i3f270f5v4gr9gijnnwa7g1ajznwasvjw35hi")))

(define-public crate-chomatter_cli-0.0.4 (c (n "chomatter_cli") (v "0.0.4") (d (list (d (n "chomatter_commons") (r "^0.0.1") (d #t) (k 0)) (d (n "chomatter_lexer") (r "^0.0.1") (d #t) (k 0)) (d (n "chomatter_syntaxer") (r "^0.0.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1kkyw1838jq2bajnp2wpw5svhnsv1a6ylf66hlvp9mg83xahy14b")))

