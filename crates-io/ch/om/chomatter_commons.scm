(define-module (crates-io ch om chomatter_commons) #:use-module (crates-io))

(define-public crate-chomatter_commons-0.0.1 (c (n "chomatter_commons") (v "0.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0diy26ciygzbbbi9ny43ijfdy51jn5h4g7fx1rxd3p9lwjj1fd1n")))

