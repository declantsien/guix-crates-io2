(define-module (crates-io ch ia chia-ssl) #:use-module (crates-io))

(define-public crate-chia-ssl-0.2.14 (c (n "chia-ssl") (v "0.2.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rcgen") (r "^0.11.1") (f (quote ("pem" "x509-parser"))) (d #t) (k 0)) (d (n "rsa") (r "^0.9.5") (d #t) (k 0)) (d (n "rustls") (r "^0.21.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "06029w8q2zxddy2fdc3565f27d2gyl16hm8qc3jbdz2yzl7mgr8c")))

(define-public crate-chia-ssl-0.6.0 (c (n "chia-ssl") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rcgen") (r "^0.11.1") (f (quote ("pem" "x509-parser"))) (d #t) (k 0)) (d (n "rsa") (r "^0.9.5") (d #t) (k 0)) (d (n "rustls") (r "^0.21.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "17qnvi243k41w81m7dn35p0h2zwqvi1v5w3r1cdpzfnnhgvh0qav")))

(define-public crate-chia-ssl-0.7.0 (c (n "chia-ssl") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rcgen") (r "^0.11.1") (f (quote ("pem" "x509-parser"))) (d #t) (k 0)) (d (n "rsa") (r "^0.9.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "1iysm41wkfk4k32jl6mnj5cxdxy041yrb87ar59phbjp06ap8qv5")))

