(define-module (crates-io ch ia chia-sdk-client) #:use-module (crates-io))

(define-public crate-chia-sdk-client-0.8.0 (c (n "chia-sdk-client") (v "0.8.0") (d (list (d (n "chia-client") (r "^0.8.0") (d #t) (k 0)) (d (n "chia-ssl") (r "^0.7.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "0izig56giba675g03fhfrfqg0ny3cbpl3cqpb4ap0icvlz6vkvqq")))

