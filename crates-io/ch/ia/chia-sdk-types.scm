(define-module (crates-io ch ia chia-sdk-types) #:use-module (crates-io))

(define-public crate-chia-sdk-types-0.8.0 (c (n "chia-sdk-types") (v "0.8.0") (d (list (d (n "chia-bls") (r "^0.8.0") (d #t) (k 0)) (d (n "chia-protocol") (r "^0.8.0") (d #t) (k 0)) (d (n "chia-puzzles") (r "^0.8.0") (d #t) (k 0)) (d (n "clvm-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "clvmr") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "0sl3svj574vp4x78mml58k62kz0dax8l0qxi715lg0jskvf4axr8")))

