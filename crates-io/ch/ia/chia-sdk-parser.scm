(define-module (crates-io ch ia chia-sdk-parser) #:use-module (crates-io))

(define-public crate-chia-sdk-parser-0.8.0 (c (n "chia-sdk-parser") (v "0.8.0") (d (list (d (n "chia-protocol") (r "^0.8.0") (d #t) (k 0)) (d (n "chia-puzzles") (r "^0.8.0") (d #t) (k 0)) (d (n "chia-sdk-types") (r "^0.8.0") (d #t) (k 0)) (d (n "clvm-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "clvm-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "clvmr") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "02gqfl542vrxfhjrxpk2sx1qgxf60178i3v4pxch6fq30mpgznqq")))

