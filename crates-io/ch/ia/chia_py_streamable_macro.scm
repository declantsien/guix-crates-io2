(define-module (crates-io ch ia chia_py_streamable_macro) #:use-module (crates-io))

(define-public crate-chia_py_streamable_macro-0.1.0 (c (n "chia_py_streamable_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "00qnj79ik94qbk8ci3s2p50zy9llz1n77k5lhckizrshcf8ip6ys")))

(define-public crate-chia_py_streamable_macro-0.1.1 (c (n "chia_py_streamable_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0pm8xpnlbgc97h5wk6gv23qv0mlmyyb4kbq3drlywjjv8q95xfd3")))

(define-public crate-chia_py_streamable_macro-0.1.2 (c (n "chia_py_streamable_macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0cnlr3bzr3ygnm9z2cy6r58lby5688qnxmyirz77dcb2xzgnvlgl")))

(define-public crate-chia_py_streamable_macro-0.1.3 (c (n "chia_py_streamable_macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1yfrg412j198f8arn5a8ggi2ch9xiiipn08riw1gqw5l1rnrjrhb")))

(define-public crate-chia_py_streamable_macro-0.1.4 (c (n "chia_py_streamable_macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "1w6kflwzn7qkysabiq5p4pqhz86bpbnb169m70rvlzcgyl5mlmz1")))

(define-public crate-chia_py_streamable_macro-0.2.12 (c (n "chia_py_streamable_macro") (v "0.2.12") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0l1yg6bs33xjh7cj3zxa5nfjhvk2agv2sl2gavnnxkzagk2cvbxb")))

(define-public crate-chia_py_streamable_macro-0.2.13 (c (n "chia_py_streamable_macro") (v "0.2.13") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0i6xd2i01iqgz03d7r1jyfkj1rcf5n3a1wb38avwz5ss3yxzy8aa")))

(define-public crate-chia_py_streamable_macro-0.2.14 (c (n "chia_py_streamable_macro") (v "0.2.14") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0h00gs9cz9vp76b1bwmyknvawvvsmbz0ak191m5hsl6gj5iqvvi3")))

(define-public crate-chia_py_streamable_macro-0.3.0 (c (n "chia_py_streamable_macro") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0knjw14z93kycij5ga6b6cmkh9znp76s0ql3162k7vmkngn082mc")))

(define-public crate-chia_py_streamable_macro-0.5.0 (c (n "chia_py_streamable_macro") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "1v2j5snskzfil1vwyxm9sp6xsfmmzlsb8lmrmzisss4y7shanjp3")))

(define-public crate-chia_py_streamable_macro-0.5.1 (c (n "chia_py_streamable_macro") (v "0.5.1") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0p0wvsk51k0m90qna1jnk8xg9pppfsahabzayx4zq24qxp37whw1")))

(define-public crate-chia_py_streamable_macro-0.6.0 (c (n "chia_py_streamable_macro") (v "0.6.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0k8ldxz7kf6418g5ilhq9mx4adni42jrq79sdyh6q684czfd74ak")))

(define-public crate-chia_py_streamable_macro-0.7.0 (c (n "chia_py_streamable_macro") (v "0.7.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "1phama45dgjbc01m3w084kxd1sjbgjjwm44c38m1lyay2psqkcix")))

(define-public crate-chia_py_streamable_macro-0.9.0 (c (n "chia_py_streamable_macro") (v "0.9.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "18v6gl2m6gky012cq2xh3zfqilla4cq3hx3bdfp9cffr78mp1w1v")))

