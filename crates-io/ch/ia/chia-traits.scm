(define-module (crates-io ch ia chia-traits) #:use-module (crates-io))

(define-public crate-chia-traits-0.2.13 (c (n "chia-traits") (v "0.2.13") (d (list (d (n "chia_py_streamable_macro") (r "^0.2.13") (o #t) (d #t) (k 0)) (d (n "chia_streamable_macro") (r "^0.2.12") (d #t) (k 0)) (d (n "clvmr") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "pyo3") (r ">=0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1d6xb6ni24dnbs0niqxzvj8px8as2wfc8caf2pszf2g51wymkb2l") (s 2) (e (quote (("py-bindings" "dep:pyo3" "dep:chia_py_streamable_macro"))))))

(define-public crate-chia-traits-0.2.14 (c (n "chia-traits") (v "0.2.14") (d (list (d (n "chia_py_streamable_macro") (r "=0.2.14") (o #t) (d #t) (k 0)) (d (n "chia_streamable_macro") (r "=0.2.14") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1zcyh6hbxjlxw7sjhihybcwyf08hmdchfm37pzrx63q2fjk7wj8w") (s 2) (e (quote (("py-bindings" "dep:pyo3" "dep:chia_py_streamable_macro"))))))

(define-public crate-chia-traits-0.3.0 (c (n "chia-traits") (v "0.3.0") (d (list (d (n "chia_py_streamable_macro") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "chia_streamable_macro") (r "=0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "00zkvs0kh9zvkj3c6kxa2vb6sbf3zpxkf5vdhqp4xhadrqp86vk3") (s 2) (e (quote (("py-bindings" "dep:pyo3" "dep:chia_py_streamable_macro"))))))

(define-public crate-chia-traits-0.3.3 (c (n "chia-traits") (v "0.3.3") (d (list (d (n "chia_py_streamable_macro") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "chia_streamable_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0d0k129inrv0v5bb7vqfb788fwnxqk1yv1gvq46b0kgrkpajwlzy") (s 2) (e (quote (("py-bindings" "dep:pyo3" "dep:chia_py_streamable_macro"))))))

(define-public crate-chia-traits-0.5.0 (c (n "chia-traits") (v "0.5.0") (d (list (d (n "chia_py_streamable_macro") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "chia_streamable_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0wwxcfar0sdmpb1x3143v4617p1ddcw0hh4bdy22akw3y3c7jcm8") (s 2) (e (quote (("py-bindings" "dep:pyo3" "dep:chia_py_streamable_macro"))))))

(define-public crate-chia-traits-0.5.1 (c (n "chia-traits") (v "0.5.1") (d (list (d (n "chia_py_streamable_macro") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "chia_streamable_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1rrx0w8f9wm910b2bj949j3pwplf4jpdlvibpl5agmsiwcz8y333") (s 2) (e (quote (("py-bindings" "dep:pyo3" "dep:chia_py_streamable_macro"))))))

(define-public crate-chia-traits-0.5.2 (c (n "chia-traits") (v "0.5.2") (d (list (d (n "chia_py_streamable_macro") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "chia_streamable_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "02blrh6c58li3rg7kmjrp50qv1d89dwgyizi628vqfd1i8sic2d8") (s 2) (e (quote (("py-bindings" "dep:pyo3" "dep:chia_py_streamable_macro"))))))

(define-public crate-chia-traits-0.6.0 (c (n "chia-traits") (v "0.6.0") (d (list (d (n "chia_py_streamable_macro") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "chia_streamable_macro") (r "^0.6.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1n3liqp83ky5l0qwzh6yggv0jsiffkj80lv6prfqiw4flgfna0jb") (s 2) (e (quote (("py-bindings" "dep:pyo3" "dep:chia_py_streamable_macro"))))))

(define-public crate-chia-traits-0.7.0 (c (n "chia-traits") (v "0.7.0") (d (list (d (n "chia_py_streamable_macro") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "chia_streamable_macro") (r "^0.6.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0nll23gx3hxx26y282gjfd66vmy6chbr46xjnf9ih56ddf2lp35m") (s 2) (e (quote (("py-bindings" "dep:pyo3" "dep:chia_py_streamable_macro"))))))

(define-public crate-chia-traits-0.8.0 (c (n "chia-traits") (v "0.8.0") (d (list (d (n "chia_streamable_macro") (r "^0.8.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0gxcx2cq296mgsh48avpxshs04lyqdyjyzkw5rb8i4nrai795jib") (s 2) (e (quote (("py-bindings" "dep:pyo3"))))))

(define-public crate-chia-traits-0.9.0 (c (n "chia-traits") (v "0.9.0") (d (list (d (n "chia_streamable_macro") (r "^0.8.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1kcpggswqg81i3nb8qjkwh09namav7lplds160hm2lk0zwvzz9s9") (s 2) (e (quote (("py-bindings" "dep:pyo3"))))))

