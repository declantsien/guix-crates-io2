(define-module (crates-io ch bs chbs_password_checker) #:use-module (crates-io))

(define-public crate-chbs_password_checker-0.1.0 (c (n "chbs_password_checker") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "13q0gr0hwmh140firnk72s3sh1xyjll7b8mwc0vnw9hmq39nl21g")))

