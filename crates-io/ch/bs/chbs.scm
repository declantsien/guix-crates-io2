(define-module (crates-io ch bs chbs) #:use-module (crates-io))

(define-public crate-chbs-0.0.1 (c (n "chbs") (v "0.0.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1vihpz999myzdzcmprj5qwhagaw8m9zv2sz2knzrpsnx9ihwz03m")))

(define-public crate-chbs-0.0.2 (c (n "chbs") (v "0.0.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "12pgg1wxj2hhzsdliv5zgxc088wmzvna792x51jfpm60nv3v9ml5")))

(define-public crate-chbs-0.0.3 (c (n "chbs") (v "0.0.3") (d (list (d (n "derive_builder") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1r90pihky1bj0d4hb4ifmb2fpkinvbh55005ahai7cyfpcygxval")))

(define-public crate-chbs-0.0.4 (c (n "chbs") (v "0.0.4") (d (list (d (n "derive_builder") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0995npgns6bhx6s09bza53k0n8lj1ix95s17bfsxxcw2wz13lk6a")))

(define-public crate-chbs-0.0.5 (c (n "chbs") (v "0.0.5") (d (list (d (n "derive_builder") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1a8sk3xbg7rvbgc99wx4pbsibgkqwnm9pcm6xj2i1wxx6d2p1laq")))

(define-public crate-chbs-0.0.6 (c (n "chbs") (v "0.0.6") (d (list (d (n "derive_builder") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "19hrij3vkvhkw97hf1g574mlbgs37d458r2i4g80lkdajycn7k50")))

(define-public crate-chbs-0.0.7 (c (n "chbs") (v "0.0.7") (d (list (d (n "derive_builder") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0hdl9cbaw3dgrkaavc4qgp05vi69z0jv31q8daxf97lng6ddk979")))

(define-public crate-chbs-0.0.8 (c (n "chbs") (v "0.0.8") (d (list (d (n "derive_builder") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0dsw0ck901jiijhhs0wrzzwzy8czp4kd2443afi1s80zzcy7ydsh")))

(define-public crate-chbs-0.0.9 (c (n "chbs") (v "0.0.9") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0nli626g17d80xr6l1rs6z1hqm93zsv5r81bznr1fihlcpdzrbbh")))

(define-public crate-chbs-0.0.10 (c (n "chbs") (v "0.0.10") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1aw2k95xsi0gb888k0r3yf3dwgjlz5qmbg4ibyv8bq81552pk1cv")))

(define-public crate-chbs-0.1.0 (c (n "chbs") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1xzppmzdjazg608dasv275afc7gfg142w3vxncbh02dhg3ra2lrm")))

(define-public crate-chbs-0.1.1 (c (n "chbs") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ggaz9ia4ljcvdd601iwwkq7apl5kz7fhiiz5m13yi7ihy12k9s5")))

