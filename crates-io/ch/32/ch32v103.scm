(define-module (crates-io ch #{32}# ch32v103) #:use-module (crates-io))

(define-public crate-ch32v103-0.0.0 (c (n "ch32v103") (v "0.0.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "07j1csfygjbcmzlzklbwfsqljm8dbmxj2mj27h1is5dxs4fzdi74") (f (quote (("rt" "riscv-rt") ("default" "rt") ("ch32v103"))))))

