(define-module (crates-io ch #{32}# ch32x0) #:use-module (crates-io))

(define-public crate-ch32x0-0.1.6 (c (n "ch32x0") (v "0.1.6") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "05mf24sfjpzjjznbqvh5w86nf538sy72mcbhr3274avi2m0djmwd") (f (quote (("rt") ("default") ("ch32x035"))))))

(define-public crate-ch32x0-0.1.7 (c (n "ch32x0") (v "0.1.7") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "18klrdhq07q7j25adrcdr9s31vwnk91nws4pw89dgf5970jglpx4") (f (quote (("rt") ("default") ("ch32x035"))))))

