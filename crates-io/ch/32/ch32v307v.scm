(define-module (crates-io ch #{32}# ch32v307v) #:use-module (crates-io))

(define-public crate-ch32v307v-0.0.1 (c (n "ch32v307v") (v "0.0.1") (d (list (d (n "ch32v30x-hal") (r "^0.0.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.7") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)))) (h "10k7vij0xmlbgi2whrfdq7w8w97mvnvi0ab74vgjjkjwg5hqdsvj") (f (quote (("board-ch32v307v-r1")))) (r "1.59")))

