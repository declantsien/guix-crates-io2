(define-module (crates-io ch #{32}# ch32v1) #:use-module (crates-io))

(define-public crate-ch32v1-0.1.0 (c (n "ch32v1") (v "0.1.0") (d (list (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "164mczg292vjd906lmfbzqhscbd06lrkpvk5mhlyk24zw0wswrvw") (f (quote (("rt") ("default"))))))

(define-public crate-ch32v1-0.1.3 (c (n "ch32v1") (v "0.1.3") (d (list (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "096fwdanhqjbs0am3k0gf6vm5m5chj8s8s573qd7kwkdrqp4c6bs") (f (quote (("rt" "riscv-rt") ("default") ("ch32v103"))))))

(define-public crate-ch32v1-0.1.4 (c (n "ch32v1") (v "0.1.4") (d (list (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1yr40xa3cxxcxnpp0ggswiz6h3gc5lf951c0743w9gg9ym2m1w78") (f (quote (("rt" "riscv-rt") ("default") ("ch32v103"))))))

(define-public crate-ch32v1-0.1.5 (c (n "ch32v1") (v "0.1.5") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1h0jpq5qagyp300ckrypdmzdinskcnq6zanndw5x35idbsczzlvs") (f (quote (("rt" "riscv-rt") ("default" "rt") ("ch32v103"))))))

(define-public crate-ch32v1-0.1.6 (c (n "ch32v1") (v "0.1.6") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1bcdsyv4zrpjcmx986ba7xn6qv10rb28gpmmbsxmy36gyal09ckl") (f (quote (("rt") ("default") ("ch32v103"))))))

