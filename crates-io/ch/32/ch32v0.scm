(define-module (crates-io ch #{32}# ch32v0) #:use-module (crates-io))

(define-public crate-ch32v0-0.1.6 (c (n "ch32v0") (v "0.1.6") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "189cvrkd25wlj6fydda2w8cczfs3pkz2kzfp38rxm9ylrl542js1") (f (quote (("rt") ("default") ("ch32v003"))))))

(define-public crate-ch32v0-0.1.7 (c (n "ch32v0") (v "0.1.7") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "07lhp4ml18i9nchhzwvdq5lmzfrsxvr0fx81zcp3l6wgid6vpxbz") (f (quote (("rt") ("default") ("ch32v003"))))))

(define-public crate-ch32v0-0.2.0 (c (n "ch32v0") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "16wrz3w2rys70b7p0nbyczmlwj59rwxkljjx191rsaryz8v33ar8") (f (quote (("rt") ("default") ("ch32v003"))))))

