(define-module (crates-io ch #{32}# ch32v-rt) #:use-module (crates-io))

(define-public crate-ch32v-rt-0.0.0 (c (n "ch32v-rt") (v "0.0.0") (h "011kxbp5j452drk962dc4y9x592vaydayhwsgc1n5x9z8ifikxp8")))

(define-public crate-ch32v-rt-0.0.1 (c (n "ch32v-rt") (v "0.0.1") (d (list (d (n "riscv-rt-macros") (r "^0.2.0") (d #t) (k 0)))) (h "056hmar5x0pwa29rwjqybbjqwgqpv58364bwam02bqax3vnn9wgi")))

(define-public crate-ch32v-rt-0.0.2 (c (n "ch32v-rt") (v "0.0.2") (d (list (d (n "ch32v-rt-macros") (r "^0.0.1") (d #t) (k 0)))) (h "110ny9r2zsslhqyvxxmqgbknzk9bzjv8aqvcfggn0zljlv20n0fh")))

(define-public crate-ch32v-rt-0.0.3 (c (n "ch32v-rt") (v "0.0.3") (d (list (d (n "ch32v-rt-macros") (r "^0.0.2") (d #t) (k 0)))) (h "06q6nfkhcnldb6rmpchpgnkifb4vspc7ifwk6kwibq11aiqhgazg")))

