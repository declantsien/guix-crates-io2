(define-module (crates-io ch #{32}# ch32v30x) #:use-module (crates-io))

(define-public crate-ch32v30x-0.0.1 (c (n "ch32v30x") (v "0.0.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "03xncjljdn27qgmml4897sdlgs42fzdrwvgl91mc695v3sqw88qw") (f (quote (("rt")))) (r "1.59")))

