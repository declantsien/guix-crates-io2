(define-module (crates-io ch #{32}# ch32v20x) #:use-module (crates-io))

(define-public crate-ch32v20x-0.0.0 (c (n "ch32v20x") (v "0.0.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1j03c54alixa8vn72r5n2ffaps478z3jchgvakhc5vrfzyx3lbvb") (f (quote (("rt" "riscv-rt") ("default" "rt") ("ch32v20x"))))))

