(define-module (crates-io ch #{32}# ch32l1) #:use-module (crates-io))

(define-public crate-ch32l1-0.1.7 (c (n "ch32l1") (v "0.1.7") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0rhri6z9c76hv6a7sgr5j7781ml5lj2jm3ifr1c1m6qlaigcl7j2") (f (quote (("rt") ("default") ("ch32l103"))))))

