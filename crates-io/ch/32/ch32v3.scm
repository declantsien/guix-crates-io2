(define-module (crates-io ch #{32}# ch32v3) #:use-module (crates-io))

(define-public crate-ch32v3-0.1.0 (c (n "ch32v3") (v "0.1.0") (d (list (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1rx6pbpb8hv71qjcfymy2pds6avw6h4xyl4i6dv781inki11zvdc") (f (quote (("rt") ("default"))))))

(define-public crate-ch32v3-0.1.1 (c (n "ch32v3") (v "0.1.1") (d (list (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0xksgy2nw5isy789l4llwap8b691svxrsslyjvnx250dqcr7dqk9") (f (quote (("rt") ("default"))))))

(define-public crate-ch32v3-0.1.2 (c (n "ch32v3") (v "0.1.2") (d (list (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0jbaw46g76h98nccg66rrx1k3q3lzp54xphii184c5030hyspaxr") (f (quote (("rt") ("default"))))))

(define-public crate-ch32v3-0.1.3 (c (n "ch32v3") (v "0.1.3") (d (list (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1x7smilfl6i9w7krf5xnj0m8224jlg9s31v5j6l97swhqr0y7f9a") (f (quote (("rt" "riscv-rt") ("default") ("ch32v30x"))))))

(define-public crate-ch32v3-0.1.4 (c (n "ch32v3") (v "0.1.4") (d (list (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0rr6xwvmicbyy3xrcdvyq028c75myfgyawnkqz5ixsb7zl6ylx8p") (f (quote (("rt" "riscv-rt") ("default") ("ch32v30x"))))))

(define-public crate-ch32v3-0.1.5 (c (n "ch32v3") (v "0.1.5") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0w3hc9dk7mgf9hzlpkn4i7n55857r3ag3y9y88ahf0ia2q3faq60") (f (quote (("rt" "riscv-rt") ("default" "rt") ("ch32v30x"))))))

(define-public crate-ch32v3-0.1.6 (c (n "ch32v3") (v "0.1.6") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14kw6cdxcqmihqbxw5kd5l1rp5292k048ijm6vpjqshsgcbwx227") (f (quote (("rt") ("default") ("ch32v30x"))))))

