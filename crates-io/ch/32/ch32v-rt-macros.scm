(define-module (crates-io ch #{32}# ch32v-rt-macros) #:use-module (crates-io))

(define-public crate-ch32v-rt-macros-0.0.1 (c (n "ch32v-rt-macros") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1z586qqcss7fvz874vrkmy9qx3mld4rlbw1qx1kn3vr2wy894x4f")))

(define-public crate-ch32v-rt-macros-0.0.2 (c (n "ch32v-rt-macros") (v "0.0.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0wvg6anbx1npxf1z7ygiv1kr4blnw2vdz9jfjrkg3kz5jh3jialx")))

