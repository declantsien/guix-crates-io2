(define-module (crates-io ch #{32}# ch32v2) #:use-module (crates-io))

(define-public crate-ch32v2-0.1.0 (c (n "ch32v2") (v "0.1.0") (d (list (d (n "ch32v3") (r "^0.1.0") (d #t) (k 0)))) (h "0d6ia6vqjzjg4aq7p08qbimqxx25wvdv7cyszgn67anpqsh454r1") (f (quote (("rt" "ch32v3/rt") ("default"))))))

(define-public crate-ch32v2-0.1.1 (c (n "ch32v2") (v "0.1.1") (d (list (d (n "ch32v3") (r "^0.1") (d #t) (k 0)))) (h "1jyyj4wag9mf4c45s5qd6jq86fv8jc7f8rcrc2qpchdykskk9y2k") (f (quote (("rt" "ch32v3/rt") ("default"))))))

(define-public crate-ch32v2-0.1.3 (c (n "ch32v2") (v "0.1.3") (d (list (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0y5dbhbwhhwzx56k6rp91xn63nivpmyiq78j169m5wanqr7bavv8") (f (quote (("rt" "riscv-rt") ("default") ("ch32v20x"))))))

(define-public crate-ch32v2-0.1.4 (c (n "ch32v2") (v "0.1.4") (d (list (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rkgiv94yb4v0gb5ms569n8nzak92s61y25yjsbwxyqscjbwp3gi") (f (quote (("rt" "riscv-rt") ("default") ("ch32v20x"))))))

(define-public crate-ch32v2-0.1.5 (c (n "ch32v2") (v "0.1.5") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0dm5sn52nvyia0kywx6z6v9017bivqh8v80lg8kg5ikhssq7jpln") (f (quote (("rt" "riscv-rt") ("default" "rt") ("ch32v20x"))))))

(define-public crate-ch32v2-0.1.6 (c (n "ch32v2") (v "0.1.6") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "116qnp5chyi6w6c8jkk4ny6bdvh6alx9d4vmk9r3lyr7f1c6w0zz") (f (quote (("rt") ("default") ("ch32v20x"))))))

