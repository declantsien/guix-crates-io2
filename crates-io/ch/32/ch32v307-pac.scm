(define-module (crates-io ch #{32}# ch32v307-pac) #:use-module (crates-io))

(define-public crate-ch32v307-pac-0.1.0 (c (n "ch32v307-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1rn3yyhbpsq03wf72gjq4dh5qsh9hd5dvikgf974dmccz3g9jk0b") (f (quote (("rt"))))))

