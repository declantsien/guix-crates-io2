(define-module (crates-io ch #{32}# ch32v103-hal) #:use-module (crates-io))

(define-public crate-ch32v103-hal-0.0.1 (c (n "ch32v103-hal") (v "0.0.1") (d (list (d (n "ch32v1") (r "^0.1.6") (f (quote ("critical-section" "ch32v103"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "04q3k1yyf472mwbp7mhfyf8iqiln4cizghm62f46rra4bnckjjd4")))

(define-public crate-ch32v103-hal-0.0.2 (c (n "ch32v103-hal") (v "0.0.2") (d (list (d (n "ch32v1") (r "^0.1.6") (f (quote ("critical-section" "ch32v103"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "08n1mpp1yqqs2b3ljbmicmfd5vxlbmblqw90yzwc60cjq2jlj44b")))

(define-public crate-ch32v103-hal-0.0.3 (c (n "ch32v103-hal") (v "0.0.3") (d (list (d (n "ch32v1") (r "^0.1.6") (f (quote ("critical-section" "ch32v103"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "1m6hya9ab24c6xjy5hiwrc58cnk6c2flrh0kk6c4rn70l5jymwk1")))

(define-public crate-ch32v103-hal-0.0.4 (c (n "ch32v103-hal") (v "0.0.4") (d (list (d (n "ch32v1") (r "^0.1.6") (f (quote ("critical-section" "ch32v103"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "1hcah051jnkmb3x7358lwig3z8y11i5wj5wyskl6c112mgqb75yq")))

