(define-module (crates-io ch an chandeliers-err) #:use-module (crates-io))

(define-public crate-chandeliers-err-0.2.0 (c (n "chandeliers-err") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "12kml9ng6l88kckvqq8z71m9gkh9g0rrxqb5yqh0b7jiikx4w3ff")))

(define-public crate-chandeliers-err-0.2.1 (c (n "chandeliers-err") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1bifpydxd1gm48zkbf9vs31rm6v9c76k0bw54wyp6ylrnhzyksvm")))

(define-public crate-chandeliers-err-0.2.2 (c (n "chandeliers-err") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0wd3z740j1mcvq53xx55jxhhwz3byavavd0h4n5nx8v109q9qcfp")))

(define-public crate-chandeliers-err-0.2.3 (c (n "chandeliers-err") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0ppfp4qfzxllbxa9vl3grzh0gfw0q881jxzlmh41ffxd1p3i1wdk")))

(define-public crate-chandeliers-err-0.2.4 (c (n "chandeliers-err") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1gw2h6m38263c8j7qf9mdslizw8qz828rawxkqbz8mkyvqg1c4ql")))

(define-public crate-chandeliers-err-0.2.5 (c (n "chandeliers-err") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "04zpcfffihjdirlmhrgjj6vh6873p42h6ia044i6izcxj53szqwb")))

(define-public crate-chandeliers-err-0.2.6 (c (n "chandeliers-err") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "13799wgw1kr5047plrgrrwajlqyvcx53k0yy03hf26if444klz0l")))

(define-public crate-chandeliers-err-0.3.0 (c (n "chandeliers-err") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1myhxqnzh6l95yi9km5nqdmm5ybv7h6jdvbnwxf63s7y07zwjxg0")))

(define-public crate-chandeliers-err-0.3.1 (c (n "chandeliers-err") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "049mxif1nkg20mrf1jlj3w9dxczs8r3p1nd9iz6vi1d2rj8vncys")))

(define-public crate-chandeliers-err-0.3.2 (c (n "chandeliers-err") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1ihdg73fhz71qvvk2cw28pn60q4m64qqjfj6m33gkvcsmsmjn9j1")))

(define-public crate-chandeliers-err-0.3.3 (c (n "chandeliers-err") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1v9c5vagpivy879ab9m3b45x9jayg10nkhkw18ckj1jsai14kvjs")))

(define-public crate-chandeliers-err-0.4.0 (c (n "chandeliers-err") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0bvi610jx279xbxc9hklqd36lylzr43dc6l4nz0jcwa3qpqanx4y")))

(define-public crate-chandeliers-err-0.4.1 (c (n "chandeliers-err") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "101j80v2ikb159xia97lc41hk518mqsaywzigni2hhzwkzwh0n4m")))

(define-public crate-chandeliers-err-0.4.2 (c (n "chandeliers-err") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0877rcb631yam3pn17ms63zzn3sw90sijz8jd7cd8680p15zrjrs")))

(define-public crate-chandeliers-err-0.4.3 (c (n "chandeliers-err") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0m3hpfdj9qm21d5lddm72hb3ma8kqxkdiw4k4l4xg8ykx3sf28h8")))

(define-public crate-chandeliers-err-0.4.4 (c (n "chandeliers-err") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1s0l7g50pscx4cnjqn3a3cajh3vd07mhqrbg22simw3gq3pgbldf")))

(define-public crate-chandeliers-err-0.5.0 (c (n "chandeliers-err") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1z885wlmn1v0x390z9w57qyj3inw6plnnz1qw2hv2kd4fmq8sf2z")))

(define-public crate-chandeliers-err-0.5.2 (c (n "chandeliers-err") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "106p2wablk5xk0pxrbgw541as2wkgqs5z8q21afa3qzmjd0ba4h8")))

(define-public crate-chandeliers-err-0.6.0 (c (n "chandeliers-err") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "15c2dcv2z6zssmwgmiap9znx7rw1kha1w8qm892b2f5gxvscm9c9")))

(define-public crate-chandeliers-err-0.6.1 (c (n "chandeliers-err") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1k74mqi9wfjm2yw7dmz0agnjpfkmq0x27s3a99kj8yww3a07hph5")))

(define-public crate-chandeliers-err-1.0.0 (c (n "chandeliers-err") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1fyyyyz9j23lzlbrvv8sc4rpafjcbyxy6lrzvkal7f1g87hwmyrn")))

