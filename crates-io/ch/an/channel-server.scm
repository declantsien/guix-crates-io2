(define-module (crates-io ch an channel-server) #:use-module (crates-io))

(define-public crate-channel-server-0.1.0 (c (n "channel-server") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "channel-server-derive") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ljwpx6036chmb9k6iih5qvkxj1ln8y5svmbb3fs1c4lzydadhi4")))

