(define-module (crates-io ch an changecase) #:use-module (crates-io))

(define-public crate-changecase-0.0.1 (c (n "changecase") (v "0.0.1") (h "1l1grkl7ijkvvncy8k5w7gh00l7ns5y5idwqhj0hnxra7ca6wy8r")))

(define-public crate-changecase-0.0.2 (c (n "changecase") (v "0.0.2") (h "1b4kzpjg6zrjiw9wgniqva7nwxgryjxsl9nmwc375gwsgd082iam")))

(define-public crate-changecase-0.0.3 (c (n "changecase") (v "0.0.3") (h "1bv13gkbjmcwr9lcdzgfndc00smz8j078cf87h44c9x7na1z09rb")))

(define-public crate-changecase-0.0.4 (c (n "changecase") (v "0.0.4") (h "1g6kps86zk2fjl1761aqlijiyr1al8fra8c1pibvc7075yw1nl7j")))

(define-public crate-changecase-0.0.5 (c (n "changecase") (v "0.0.5") (h "0iwd8ibfj37cbj7jw8j5hwj8nyb1b9qjy3snqah6p6xlgca96qb2")))

(define-public crate-changecase-0.0.6 (c (n "changecase") (v "0.0.6") (h "1clxry64r702153nzxbfwzpqp1439fc6dkz1v8psp7nspak0v256")))

(define-public crate-changecase-0.0.7 (c (n "changecase") (v "0.0.7") (h "0hf5k7r4x3cii0xcrwkg49bwz9889i57c7hvdzd147sw9fzs2sak")))

