(define-module (crates-io ch an changesets) #:use-module (crates-io))

(define-public crate-changesets-0.0.1 (c (n "changesets") (v "0.0.1") (d (list (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "0173yj727zg7mdvm2jjjcrhks7l7bm34q9k9wlxnggswvr522jw2") (r "1.70")))

(define-public crate-changesets-0.1.0 (c (n "changesets") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "1kzhfys6ny28v2g5ld2rivrhwpzc62mnq118mbxkhan9rpzn645r") (r "1.70")))

(define-public crate-changesets-0.1.1 (c (n "changesets") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "1hj8h7k4ylchgr7ijhc4mvjvjsj2yfq8k1a8zafv7zpv7wnp8fvc") (r "1.70")))

(define-public crate-changesets-0.2.0 (c (n "changesets") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "1nj4f4pkrwh2nw2z43abwxpc1db6nhdn7sx9r7r2d6kvlg4kyr87") (r "1.70")))

(define-public crate-changesets-0.2.1 (c (n "changesets") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "045yxsymp7z47z1vr8yyfbdhwvmabk8kw2ws5n3qpplbcnnbfka2") (r "1.70")))

(define-public crate-changesets-0.2.2 (c (n "changesets") (v "0.2.2") (d (list (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)))) (h "0gpng5rgnx01waad48d6r5yf6g8mmpzbx7xciy61plq31mcpzw6w") (r "1.70")))

(define-public crate-changesets-0.2.3 (c (n "changesets") (v "0.2.3") (d (list (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "038hx32x2j54gcimnmy51d1k58qn8vd3xc0k1gkcxh26s5x1lslx") (r "1.70")))

