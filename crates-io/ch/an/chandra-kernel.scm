(define-module (crates-io ch an chandra-kernel) #:use-module (crates-io))

(define-public crate-chandra-kernel-0.0.0 (c (n "chandra-kernel") (v "0.0.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1jpl6fmzkxp5xbjqz1p5p650964gy7qgh8mhf4mp3a87mynvc0kc")))

