(define-module (crates-io ch an chandra) #:use-module (crates-io))

(define-public crate-chandra-0.0.0 (c (n "chandra") (v "0.0.0") (d (list (d (n "chandra-kernel") (r "^0.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.16.1") (o #t) (d #t) (k 0)))) (h "0cnpkl0ial51228l1z76rp1f45alr8p01vgqdz97fsbl6c6jdf1m") (f (quote (("std") ("nightly") ("default" "gpu" "std")))) (s 2) (e (quote (("gpu" "dep:wgpu")))) (r "1.65.0")))

