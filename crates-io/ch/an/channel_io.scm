(define-module (crates-io ch an channel_io) #:use-module (crates-io))

(define-public crate-channel_io-0.1.1 (c (n "channel_io") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)))) (h "1kbkif17xnbmlfnb83kyk2mwymx7dd9kqyzlx27bfc9ksn8xg8bj")))

(define-public crate-channel_io-0.1.2 (c (n "channel_io") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)))) (h "166b8k8jg1s5mx6zmzadhyz0mm8h2klsh36j3spjk6yihnk8v6rn")))

(define-public crate-channel_io-0.1.3 (c (n "channel_io") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)))) (h "0k0i9nbjsbmz67a96ijyd4k38kjfalxf2cayxy44h6xrvv21d295")))

