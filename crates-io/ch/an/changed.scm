(define-module (crates-io ch an changed) #:use-module (crates-io))

(define-public crate-changed-0.1.0 (c (n "changed") (v "0.1.0") (h "17kiiw0wzx9xipvynbwlsyamfn549z52qk929b8v22wcir11l9ml")))

(define-public crate-changed-0.1.1 (c (n "changed") (v "0.1.1") (h "1lfg44grzrpz212z9d1v4ixs5js318ys8vlglblhw6pvsfhky569")))

(define-public crate-changed-0.1.2 (c (n "changed") (v "0.1.2") (h "1a0n2cwic0g6idq8alr098dqmlwykicxnnh7vp84gylp3ysm9204")))

