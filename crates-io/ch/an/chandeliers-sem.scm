(define-module (crates-io ch an chandeliers-sem) #:use-module (crates-io))

(define-public crate-chandeliers-sem-0.0.1 (c (n "chandeliers-sem") (v "0.0.1") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0v483506q0fkhhvcqc61wgnyilmsp2dlvry3w3gqf7h7la8kb31c")))

(define-public crate-chandeliers-sem-0.0.2 (c (n "chandeliers-sem") (v "0.0.2") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "01l4qg0v18wfwb4f3fibxc80mjqylqnld594a483fjm14azsrz3f")))

(define-public crate-chandeliers-sem-0.0.3 (c (n "chandeliers-sem") (v "0.0.3") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "02c3z265a5k74b24na6iwaf2f5jz3ly94gizvkqnh7d46f0hc3wi")))

(define-public crate-chandeliers-sem-0.0.4 (c (n "chandeliers-sem") (v "0.0.4") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "1zi8nlvkaw75w3s9ilw41p67pakh68kz5gigzk18pp56dvxbbg9v")))

(define-public crate-chandeliers-sem-0.0.5 (c (n "chandeliers-sem") (v "0.0.5") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0c59zys7s4y6zd14pmih452hxryrf1lwvscarw8nh58xq7hikvf5")))

(define-public crate-chandeliers-sem-0.0.6 (c (n "chandeliers-sem") (v "0.0.6") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "1xsnamjnygsxmq02pl6l9blgq0hj0is19chnjvxv066jnwanrl4b")))

(define-public crate-chandeliers-sem-0.0.7 (c (n "chandeliers-sem") (v "0.0.7") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "14bl3v0qvibh5lx9j2k5abp9z2cflz3lf5xinqa665cq5gnr844y")))

(define-public crate-chandeliers-sem-0.1.0 (c (n "chandeliers-sem") (v "0.1.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0s6pz0cwkb45p5nrxsr6ifvfz03c3l7l2ya7lwlwb0gkpmn2j171")))

(define-public crate-chandeliers-sem-0.1.1 (c (n "chandeliers-sem") (v "0.1.1") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "1739r030vby1jfdiyqhhq2hpv1scfv5hjzi587cg936fx01cmvk2")))

(define-public crate-chandeliers-sem-0.2.0 (c (n "chandeliers-sem") (v "0.2.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "085kkxndgzs1m152mk70i3hk9vqp65hxx87b2h12gzxcd59ik0w6")))

(define-public crate-chandeliers-sem-0.2.1 (c (n "chandeliers-sem") (v "0.2.1") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "013zf0qpyzi6l6c148s0miqrl96cy1d09s11sa0f95j5yq5xkw2k")))

(define-public crate-chandeliers-sem-0.2.2 (c (n "chandeliers-sem") (v "0.2.2") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0pavk7pkh564l2v1wbsqr729jd3nsprrqxh6rwhm4kvdx1wxizrb")))

(define-public crate-chandeliers-sem-0.2.3 (c (n "chandeliers-sem") (v "0.2.3") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0hmslqlhnz81n2jx98m6xivkr8szr87mlgslzriy6xpyjhl3x613")))

(define-public crate-chandeliers-sem-0.2.4 (c (n "chandeliers-sem") (v "0.2.4") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "06bdyxvyr46470hnll7b0p9qh7j8b5ky58y4vbymfdvwvdjlmmjj")))

(define-public crate-chandeliers-sem-0.2.5 (c (n "chandeliers-sem") (v "0.2.5") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "166d26sk1rspda4p1gikwssn7661p6bvh3k89frk21dn8jzrki3j")))

(define-public crate-chandeliers-sem-0.2.6 (c (n "chandeliers-sem") (v "0.2.6") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "1i2cxxqj8xs3693srf23lh9784jvpw8zg8plndwbzxs9h48nlxk0")))

(define-public crate-chandeliers-sem-0.3.0 (c (n "chandeliers-sem") (v "0.3.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "1gsg9ig4qq7m4pb5gmvfwi24bmig7njy7mfq6fs2lqplnfc20wj5")))

(define-public crate-chandeliers-sem-0.3.1 (c (n "chandeliers-sem") (v "0.3.1") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0yf544dj1z4v4axv729mk9fx4qms5hq1mjjnhp25cxqlkaam50gl")))

(define-public crate-chandeliers-sem-0.3.2 (c (n "chandeliers-sem") (v "0.3.2") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0j98bq1rpaz72rd3a6z8bw3s2ivlh7nyfiyqvv91jqwiky2f0gbl")))

(define-public crate-chandeliers-sem-0.3.3 (c (n "chandeliers-sem") (v "0.3.3") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "03mhra44jvci3dll91r2kan7mn3993yjkyk32ixb7n24ja4xfrw4")))

(define-public crate-chandeliers-sem-0.4.0 (c (n "chandeliers-sem") (v "0.4.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "13vx3ml8bs415y83mv4701dyq6pfgxipdi0zdzmlrwgj15zd2vg2")))

(define-public crate-chandeliers-sem-0.4.1 (c (n "chandeliers-sem") (v "0.4.1") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "1v8ddpxl7rwgxa9wpjfm6cy9k81hdvpv5la8012pr4h91rfkcsjf")))

(define-public crate-chandeliers-sem-0.4.2 (c (n "chandeliers-sem") (v "0.4.2") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0xbxl77zpksljn9aa9xhqp4nl8v7aij04690zhqi8yps2k595sb3")))

(define-public crate-chandeliers-sem-0.4.3 (c (n "chandeliers-sem") (v "0.4.3") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "01qvianww9h1pcjzx8m1r4drxi8amjz591yqn53x9yv667nczhq3")))

(define-public crate-chandeliers-sem-0.4.4 (c (n "chandeliers-sem") (v "0.4.4") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "17nj8c7na2sli3k5d7ra8vdg7v5y8njxc72dkzshbgy7r8knss0i")))

(define-public crate-chandeliers-sem-0.5.0 (c (n "chandeliers-sem") (v "0.5.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "04cq9hkzpdgm5cv7ma7ba2s6d338n775vxnbkajan8kd8c3iphy5")))

(define-public crate-chandeliers-sem-0.5.2 (c (n "chandeliers-sem") (v "0.5.2") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "03bpvfwdizdqhz88q8xhqcycivi6b92wv36ml4kl09qvv6a98nsq")))

(define-public crate-chandeliers-sem-0.6.0 (c (n "chandeliers-sem") (v "0.6.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0wh55gkgs8jc70z7qhp09nr2p325hiii4l0d4cz6nifq123lv3k2")))

(define-public crate-chandeliers-sem-0.6.1 (c (n "chandeliers-sem") (v "0.6.1") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "1qisdishpzsm0931hmzj87gm5jb9yj5ss254dhx7y627zpa8fj0d")))

(define-public crate-chandeliers-sem-1.0.0 (c (n "chandeliers-sem") (v "1.0.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "136q6pnxif2m7h94x02n6205bn7kcybpbqavf3j6xd5synr5p5wf")))

