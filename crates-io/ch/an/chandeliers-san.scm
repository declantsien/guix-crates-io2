(define-module (crates-io ch an chandeliers-san) #:use-module (crates-io))

(define-public crate-chandeliers-san-0.1.0 (c (n "chandeliers-san") (v "0.1.0") (d (list (d (n "chandeliers-sem") (r "^0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04rkfxr9rvzw6aifki587ximnxhlp5qszhl2n1l2x6cyzca72crn")))

(define-public crate-chandeliers-san-0.1.1 (c (n "chandeliers-san") (v "0.1.1") (d (list (d (n "chandeliers-sem") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "11a25na7sb789yjnfrxvjsq1ca551scb136hrwwl5c2djp2a6wps")))

(define-public crate-chandeliers-san-0.1.2 (c (n "chandeliers-san") (v "0.1.2") (d (list (d (n "chandeliers-sem") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "029dqkqb7rf7rz036jf7nkkrgfp20cgcaksbsrfjcc3g9myyjr7a")))

(define-public crate-chandeliers-san-0.1.3 (c (n "chandeliers-san") (v "0.1.3") (d (list (d (n "chandeliers-sem") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0v3qbpgs1mz95h3fqzi5z4ljzzyw2bfjaaics18cr2kyagmbngfh")))

(define-public crate-chandeliers-san-0.2.0 (c (n "chandeliers-san") (v "0.2.0") (d (list (d (n "chandeliers-err") (r "^0.2") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1har3j04k5p7m6igmcxbnmfvllkn1kzfjij59frvj9lxdxgb9wbi")))

(define-public crate-chandeliers-san-0.2.1 (c (n "chandeliers-san") (v "0.2.1") (d (list (d (n "chandeliers-err") (r "^0.2") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17x5sv36ycxab7v509xdb4zqfl55105wd0mvc9g5a6yc1zxw919h")))

(define-public crate-chandeliers-san-0.2.2 (c (n "chandeliers-san") (v "0.2.2") (d (list (d (n "chandeliers-err") (r "^0.2.2") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1psaawj50f23n07a2xwjrsfajyk27a3vnqa2f9afi6c364k8hyn7")))

(define-public crate-chandeliers-san-0.2.3 (c (n "chandeliers-san") (v "0.2.3") (d (list (d (n "chandeliers-err") (r "^0.2.3") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00rcvz12njjkmmhqq9rhpvgi5g9jmh6vhncjdxgwvksfkbnm6irk")))

(define-public crate-chandeliers-san-0.2.4 (c (n "chandeliers-san") (v "0.2.4") (d (list (d (n "chandeliers-err") (r "^0.2.4") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0lhfrfafaaxk9dsj8pb5im9w0w4j28qsjqwdmfxis3ddn40q81fc")))

(define-public crate-chandeliers-san-0.2.5 (c (n "chandeliers-san") (v "0.2.5") (d (list (d (n "chandeliers-err") (r "^0.2.5") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.2.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1539dw8gqmg2myr4jzxqr9pwqrddd39z22zrvpx32qq8zafx5cpj")))

(define-public crate-chandeliers-san-0.2.6 (c (n "chandeliers-san") (v "0.2.6") (d (list (d (n "chandeliers-err") (r "^0.2.6") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "07aifacmncm5nk7l89fsjh5m1sqigqpdri4i9qrbq5lndrfq4d28")))

(define-public crate-chandeliers-san-0.3.0 (c (n "chandeliers-san") (v "0.3.0") (d (list (d (n "chandeliers-err") (r "^0.3.0") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1brzfm1695p3hkpxjhv538r2izg8c4zhps6ljkimfpizg88hmlbf")))

(define-public crate-chandeliers-san-0.3.1 (c (n "chandeliers-san") (v "0.3.1") (d (list (d (n "chandeliers-err") (r "^0.3.1") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00a52k7scdf2xgff5r3xa602z3ykq5v0jjk3h94gdp4kysrv3icd")))

(define-public crate-chandeliers-san-0.3.2 (c (n "chandeliers-san") (v "0.3.2") (d (list (d (n "chandeliers-err") (r "^0.3.2") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0nnfh6w12g7bmq6aqzrqq4fwdbk70vxi8qyrficc2z5f86g7bn2r")))

(define-public crate-chandeliers-san-0.3.3 (c (n "chandeliers-san") (v "0.3.3") (d (list (d (n "chandeliers-err") (r "^0.3.3") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0vm3j0cv30z7r6hay5k1wan0p78k6ka3kf5bkss7kkw8q5mf5whw")))

(define-public crate-chandeliers-san-0.4.0 (c (n "chandeliers-san") (v "0.4.0") (d (list (d (n "chandeliers-err") (r "^0.4.0") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1vbdbjc70ggxhvw8ymhilj01z35201zw1fprwx7zhypzzv01nx03")))

(define-public crate-chandeliers-san-0.4.1 (c (n "chandeliers-san") (v "0.4.1") (d (list (d (n "chandeliers-err") (r "^0.4.1") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "017q3w3svzg9dmrvfv551zzp1rx8w4gmi8bilkcaz8v1dlfwcbk4")))

(define-public crate-chandeliers-san-0.4.2 (c (n "chandeliers-san") (v "0.4.2") (d (list (d (n "chandeliers-err") (r "^0.4.2") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mb2sk8fj7g52vijz55b8gqvkrrwmh0s0jf9v4g6npj92z9xp1xg")))

(define-public crate-chandeliers-san-0.4.3 (c (n "chandeliers-san") (v "0.4.3") (d (list (d (n "chandeliers-err") (r "^0.4.3") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1hlargla7lgzkrcv5k1rybrv7imwjc1qc0kvr1mwqyxb8kf77r3w")))

(define-public crate-chandeliers-san-0.4.4 (c (n "chandeliers-san") (v "0.4.4") (d (list (d (n "chandeliers-err") (r "^0.4.4") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.4.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0z92v3qmhj0796b4980s5f2hkwg7xakx4fbamhldf73jhdflwnjy")))

(define-public crate-chandeliers-san-0.5.0 (c (n "chandeliers-san") (v "0.5.0") (d (list (d (n "chandeliers-err") (r "^0.5.0") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1gxpb37ja3na9lijrdgr68j1skma3dckzawly8klrqgf43wrill7")))

(define-public crate-chandeliers-san-0.5.2 (c (n "chandeliers-san") (v "0.5.2") (d (list (d (n "chandeliers-err") (r "^0.5.2") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1201cbxa2v5dwaksykh1m079xrb45633ncdi4pfmlgqv1wgi2mq1")))

(define-public crate-chandeliers-san-0.6.0 (c (n "chandeliers-san") (v "0.6.0") (d (list (d (n "chandeliers-err") (r "^0.6.0") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "056bm62dg12p7n6qlwwgp6y4jac6k6zcvxsf8sy1nvspznvsai8b")))

(define-public crate-chandeliers-san-0.6.1 (c (n "chandeliers-san") (v "0.6.1") (d (list (d (n "chandeliers-err") (r "^0.6.1") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "113ck3bcpzgfdh7m1fh8da22ipcxv8mq63i4qais6hdjw0gfvw29")))

(define-public crate-chandeliers-san-1.0.0 (c (n "chandeliers-san") (v "1.0.0") (d (list (d (n "chandeliers-err") (r "^1.0.0") (d #t) (k 0)) (d (n "chandeliers-sem") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xznyx30b7a63f0pvnk43aid8qfmaz4d85inlza4ynin003k5kv6")))

