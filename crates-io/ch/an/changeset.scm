(define-module (crates-io ch an changeset) #:use-module (crates-io))

(define-public crate-changeset-0.1.0 (c (n "changeset") (v "0.1.0") (h "1zkq6dyyb50jj4my2pmh5cx277k634263hb34v67fpfwsh2f1jdz")))

(define-public crate-changeset-0.1.1 (c (n "changeset") (v "0.1.1") (h "1m30yb4alis6h1v95s034zn1j1zykmn005h145ffa52xa0gp5bgx") (f (quote (("std") ("default" "std"))))))

