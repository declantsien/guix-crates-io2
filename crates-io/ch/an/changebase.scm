(define-module (crates-io ch an changebase) #:use-module (crates-io))

(define-public crate-changebase-0.1.0 (c (n "changebase") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1vfwi5s4vrwdf9wnx1gr9r9lplq47fs891r3sz07lydl9nw3pxim")))

