(define-module (crates-io ch an chanvese) #:use-module (crates-io))

(define-public crate-chanvese-0.1.0 (c (n "chanvese") (v "0.1.0") (d (list (d (n "distance-transform") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0dip9n9v53zmdz6s800135xjxyx0izr4wncmp3qma1h1ln2ns7lk") (f (quote (("image-utils" "image" "rand") ("default"))))))

