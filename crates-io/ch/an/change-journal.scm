(define-module (crates-io ch an change-journal) #:use-module (crates-io))

(define-public crate-change-journal-0.1.0 (c (n "change-journal") (v "0.1.0") (d (list (d (n "fanotify") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)) (d (n "usn-journal") (r "^0.1.0") (d #t) (k 0)))) (h "06sqm4a65n3n8n7ls631yg3y3x3sgcpn89y49ipj7k4aif82dfvq")))

