(define-module (crates-io ch an changelog-rs) #:use-module (crates-io))

(define-public crate-changelog-rs-0.1.0 (c (n "changelog-rs") (v "0.1.0") (d (list (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "0rc4g6qd696r2n825cyfb1j8yl2nz1ycffglkr7cfjw5md8x7y5m")))

(define-public crate-changelog-rs-0.2.0 (c (n "changelog-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "0rxcdy5r3wzklp6430m26az2963wawsri6z24nncxsji0iw41l70")))

(define-public crate-changelog-rs-0.3.0 (c (n "changelog-rs") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "0bqkx8xia4p6rpxi7z0di42qlcs0k3k6cailch8z2nh0z36j6ndj")))

(define-public crate-changelog-rs-0.3.1 (c (n "changelog-rs") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "011h7d18lv5rqw81109wfk5my1ji0wp9rm74ylgvrwyr2yksavq0")))

