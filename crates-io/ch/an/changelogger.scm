(define-module (crates-io ch an changelogger) #:use-module (crates-io))

(define-public crate-changelogger-0.1.0 (c (n "changelogger") (v "0.1.0") (h "0jmpizhrq9mwfil414xvfxbw7d7rgs02rg2qsym8icvi4v4zac04") (y #t)))

(define-public crate-changelogger-0.1.1 (c (n "changelogger") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0wvm8vsy55pl8yxxdmrxy2b2icz9gynxv9xw239r4jipk1cx54qq") (y #t)))

(define-public crate-changelogger-0.1.2 (c (n "changelogger") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fy0mibv6khsj135lk84575v3hinfj14ygffs3vlpjfzz3jcr4rb") (y #t)))

(define-public crate-changelogger-0.1.3 (c (n "changelogger") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0wjgm25df9q8wdr2klwb5v5d2p13jw5p21y7gx26q8yzdwakllfv") (y #t)))

(define-public crate-changelogger-0.1.4 (c (n "changelogger") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0sm4zv2sb2casslpjqjv3yllhz0hsj8dxmbbyyfjvy09rr6ca6sq")))

