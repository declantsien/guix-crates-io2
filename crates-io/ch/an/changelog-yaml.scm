(define-module (crates-io ch an changelog-yaml) #:use-module (crates-io))

(define-public crate-changelog-yaml-0.0.1 (c (n "changelog-yaml") (v "0.0.1") (d (list (d (n "indexmap") (r "^2.2.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)))) (h "12829kv2zzypjklkk8vr8n40z98zqz0mjz2sskcgb6k8m1yslsg2")))

