(define-module (crates-io ch an change-case) #:use-module (crates-io))

(define-public crate-change-case-0.1.0 (c (n "change-case") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b1f5sqsh9hiy897m0p3qa05q6yd9hwxv1jjm6rk58k9b9aw461b")))

(define-public crate-change-case-0.1.1 (c (n "change-case") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0577r0q3nrywm2w2zy2qf7lap7bjv33mn67rafszlzq2jcm2vs0p")))

(define-public crate-change-case-0.1.2 (c (n "change-case") (v "0.1.2") (d (list (d (n "fancy-regex") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b3bpzfgqzyx2mq3h76p61swwrkr45g4zghqwql4pp1v4q0j01nm")))

(define-public crate-change-case-0.2.0 (c (n "change-case") (v "0.2.0") (d (list (d (n "fancy-regex") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b5rzy6nhbjdpf6v5x166m2xmzp54c7rfb4h2wyy9yvks5gvif9c")))

