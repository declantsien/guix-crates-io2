(define-module (crates-io ch an change-detection) #:use-module (crates-io))

(define-public crate-change-detection-1.0.0 (c (n "change-detection") (v "1.0.0") (d (list (d (n "path-slash") (r "^0.1") (d #t) (k 0)))) (h "0k13pr8p8g1wbpmby7rvyv115ll4agis4vl3y98w2i8y8cx4fm15")))

(define-public crate-change-detection-1.1.0 (c (n "change-detection") (v "1.1.0") (d (list (d (n "path-matchers") (r "^1.0") (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)))) (h "1csr1dnzzj0grf4cqcb194qdl7n8zrb1swh7q15037sw2qlljcwc") (f (quote (("glob" "path-matchers/glob") ("default" "glob"))))))

(define-public crate-change-detection-1.2.0 (c (n "change-detection") (v "1.2.0") (d (list (d (n "path-matchers") (r "^1.0") (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)))) (h "06mwv8d25gzxsfa27ij7lfr6xrbwr62xpv5rs2a1v2p4x89a97qm") (f (quote (("glob" "path-matchers/glob") ("default" "glob"))))))

