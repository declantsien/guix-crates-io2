(define-module (crates-io ch an chandeliers-std) #:use-module (crates-io))

(define-public crate-chandeliers-std-0.2.1 (c (n "chandeliers-std") (v "0.2.1") (d (list (d (n "chandeliers-sem") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qxp66dq903gqfwvz18ihcvwrwck9a9sz8qa8xrm9ayxyc7418y6")))

(define-public crate-chandeliers-std-0.2.2 (c (n "chandeliers-std") (v "0.2.2") (d (list (d (n "chandeliers-sem") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yz7n99dsdkbp256fdkjwx51b9hdwvqqvps45caycc2znkw2hkz0")))

(define-public crate-chandeliers-std-0.2.3 (c (n "chandeliers-std") (v "0.2.3") (d (list (d (n "chandeliers-sem") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zyc529pnaayf71gk9p4jx8zrvalcz3z7zpan9kkllshcyz4wv1a")))

(define-public crate-chandeliers-std-0.2.4 (c (n "chandeliers-std") (v "0.2.4") (d (list (d (n "chandeliers-sem") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0phniakqv1hrv3b317mgjcbqnj18zrlk30fqah5kd0sva3qg8w7j")))

(define-public crate-chandeliers-std-0.2.5 (c (n "chandeliers-std") (v "0.2.5") (d (list (d (n "chandeliers-sem") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "052l3gyz138gmc13m97inrag08z02hwwdr4zj5k1q3zks15jwr9d")))

(define-public crate-chandeliers-std-0.2.6 (c (n "chandeliers-std") (v "0.2.6") (d (list (d (n "chandeliers-sem") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1f2knl15r5m94il337dv1yb4vsqi18zkph18f1h4nz1f2i24hc2c")))

(define-public crate-chandeliers-std-0.3.0 (c (n "chandeliers-std") (v "0.3.0") (d (list (d (n "chandeliers-sem") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09rwjx8az35xvlpbzan23wzcxad53icqniiw7zphlr2jbs43ixd7")))

(define-public crate-chandeliers-std-0.3.1 (c (n "chandeliers-std") (v "0.3.1") (d (list (d (n "chandeliers-sem") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f2y2av6pcr9lrxw6ig5lhpjpzsqzp36h7d2gjz5v8ivsdwsdaz1")))

(define-public crate-chandeliers-std-0.3.2 (c (n "chandeliers-std") (v "0.3.2") (d (list (d (n "chandeliers-sem") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04kfgkh4ik8a1jjnhc3b1qvlkvw0h7a2a1q4916sbbmb3h2zlbpy")))

(define-public crate-chandeliers-std-0.3.3 (c (n "chandeliers-std") (v "0.3.3") (d (list (d (n "chandeliers-sem") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0abhys7z58raddwzxdpi1vhgnjdals6rs4cvfz8yrxsw35qphazb")))

(define-public crate-chandeliers-std-0.4.0 (c (n "chandeliers-std") (v "0.4.0") (d (list (d (n "chandeliers-sem") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "058z8vxmvipsc8z4qjaw7fawpa64wqdp7jvlbgk1f63riicyzs79")))

(define-public crate-chandeliers-std-0.4.1 (c (n "chandeliers-std") (v "0.4.1") (d (list (d (n "chandeliers-sem") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10vl5011m8jxxnnzliljva876281063sxys51n0drxd9k5sq58yq")))

(define-public crate-chandeliers-std-0.4.2 (c (n "chandeliers-std") (v "0.4.2") (d (list (d (n "chandeliers-sem") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zmgcrsj3k3hrvr8bfhyk8ldkakb2k8dnnb1v3ag8jvmn7l2laaa")))

(define-public crate-chandeliers-std-0.4.3 (c (n "chandeliers-std") (v "0.4.3") (d (list (d (n "chandeliers-sem") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xgff1cpjvnxw6zfakkpjjl2zzapcqkv8xnx8amr6g5mwh7w5i64")))

(define-public crate-chandeliers-std-0.4.4 (c (n "chandeliers-std") (v "0.4.4") (d (list (d (n "chandeliers-sem") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0d1xc9hnjaja1jpw1cfsd5gzyrk2a4qm4wjzs6fjq4icbg6lzbah")))

(define-public crate-chandeliers-std-0.5.0 (c (n "chandeliers-std") (v "0.5.0") (d (list (d (n "chandeliers-sem") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03irq6sa8fmfxbn2m3gr5b67isf08vll5bjcqzy6swqwhkmdbk5h")))

(define-public crate-chandeliers-std-0.5.2 (c (n "chandeliers-std") (v "0.5.2") (d (list (d (n "chandeliers-sem") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0swmhml8a7f9kv2fkjlv2pgsv5rxg2jzlj8ly4jnv34wc9px1z24")))

(define-public crate-chandeliers-std-0.6.0 (c (n "chandeliers-std") (v "0.6.0") (d (list (d (n "chandeliers-sem") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qdvw6g15nkli6147gy7mz6wqg0m92gr7dj0yd4w26hdv8xrlsx6")))

(define-public crate-chandeliers-std-0.6.1 (c (n "chandeliers-std") (v "0.6.1") (d (list (d (n "chandeliers-sem") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0aabsxn922wdrbm002dc94b53vp7kjwiii21v2qhl8l2m3q7vdxl")))

(define-public crate-chandeliers-std-1.0.0 (c (n "chandeliers-std") (v "1.0.0") (d (list (d (n "chandeliers-sem") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1d3rpfhynx8xb1mqh2v93kdml6vq3jfirxzlihjc4r7d66vvw86n")))

