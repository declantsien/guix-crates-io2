(define-module (crates-io ch an change_stream) #:use-module (crates-io))

(define-public crate-change_stream-0.1.0 (c (n "change_stream") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "multi_stream") (r "^0.1.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full" "rt-multi-thread"))) (d #t) (k 2)))) (h "12yl3gfk2j85x42r2ispjf3na0h9171wbvwpbvnaszqn137frkip")))

