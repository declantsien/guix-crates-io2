(define-module (crates-io ch an chan-signal) #:use-module (crates-io))

(define-public crate-chan-signal-0.1.0 (c (n "chan-signal") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "07i9l6ilfc6n4yxbcx17dpkpmy8kn9kd947i2xnqchgkb2in38yr")))

(define-public crate-chan-signal-0.1.1 (c (n "chan-signal") (v "0.1.1") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "07mblg8hs7wa5azz8j4c237z5wz711i1pp2b6y0w3d5rv93c5994")))

(define-public crate-chan-signal-0.1.3 (c (n "chan-signal") (v "0.1.3") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1m2f3d99rn1vkm9wgv8aaf26pwvsqdyc1dhqa96incbb37i24igp")))

(define-public crate-chan-signal-0.1.4 (c (n "chan-signal") (v "0.1.4") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0335ch7p92sd7k42nd0czgqzy8f8riih40d6lgpjmiq9z3xc8h26")))

(define-public crate-chan-signal-0.1.5 (c (n "chan-signal") (v "0.1.5") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pw05l4ysls2zi9xhyznhad584dj38mz6bl6n2c5v8d9975lqplp")))

(define-public crate-chan-signal-0.1.6 (c (n "chan-signal") (v "0.1.6") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16ipj9kbwa19x7zdjv75zvpzav1d9l7f011vikq0zlf15lhadfxg")))

(define-public crate-chan-signal-0.1.7 (c (n "chan-signal") (v "0.1.7") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ypvnjzpjayxdrcbz61x159ki71qnh8x0fcfni065p59c2mj4l9n")))

(define-public crate-chan-signal-0.2.0 (c (n "chan-signal") (v "0.2.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "186k5ksig85i53r3bv5v0ivbzs5c6dzmn32gj6nh8w1qpk1vcfqg")))

(define-public crate-chan-signal-0.3.0 (c (n "chan-signal") (v "0.3.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i5n00ki7inwwwjdh8rbgj71z7gvqi8wxirlf7a7sqm1j634025a")))

(define-public crate-chan-signal-0.3.1 (c (n "chan-signal") (v "0.3.1") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1znp09kwr47k18as469i7cl3ddpwp0n64z580plcj50wdqgy3wgi")))

(define-public crate-chan-signal-0.3.2 (c (n "chan-signal") (v "0.3.2") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dr9ixs87rd73s7ji1f5vlzgc4glj3m1si4ar6j1l9gji9fv0iq0")))

(define-public crate-chan-signal-0.3.3 (c (n "chan-signal") (v "0.3.3") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "008b2bdv04q2rr3zndb75q2phzlkik49zn3pcp20rnijahzrx0jv")))

