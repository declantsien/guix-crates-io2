(define-module (crates-io ch an channel-server-derive) #:use-module (crates-io))

(define-public crate-channel-server-derive-0.1.0 (c (n "channel-server-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "08mp0lvadpff05nk8wqzh1s082b5m4vdf72alfi122hxm6dpwcjp")))

