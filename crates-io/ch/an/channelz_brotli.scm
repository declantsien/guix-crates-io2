(define-module (crates-io ch an channelz_brotli) #:use-module (crates-io))

(define-public crate-channelz_brotli-0.6.1 (c (n "channelz_brotli") (v "0.6.1") (d (list (d (n "cc") (r "=1.0.73") (d #t) (k 1)))) (h "1z47brwbsqhihldpqy2vmw7z52dnqba938zlcdajf7kn8b574pj7") (r "1.59")))

(define-public crate-channelz_brotli-0.6.2 (c (n "channelz_brotli") (v "0.6.2") (d (list (d (n "cc") (r "=1.0.73") (d #t) (k 1)))) (h "0zdngsrf53byhwl3q7l3i7rfkahczp5fnn2c88mhqsj5klrv0jfq") (r "1.59")))

(define-public crate-channelz_brotli-0.6.3 (c (n "channelz_brotli") (v "0.6.3") (d (list (d (n "cc") (r "=1.0.73") (d #t) (k 1)))) (h "1wcdmd2kk30yzfd638nv8qs3qzdpnj7n74yaxvhy5g45jb6304w5") (r "1.61")))

(define-public crate-channelz_brotli-0.6.4 (c (n "channelz_brotli") (v "0.6.4") (d (list (d (n "cc") (r "=1.0.73") (d #t) (k 1)))) (h "142f8vi913hpwk59lnsvdg4qgdzij3fng430py91lpsn80d2bpf8") (r "1.61")))

(define-public crate-channelz_brotli-0.6.6 (c (n "channelz_brotli") (v "0.6.6") (d (list (d (n "cc") (r "=1.0.73") (d #t) (k 1)))) (h "0hcsxd10fj33rrxy8z1694vwmjbxsyp6z4qvcx4ipg5n3qzqj874") (r "1.64")))

(define-public crate-channelz_brotli-0.6.7 (c (n "channelz_brotli") (v "0.6.7") (d (list (d (n "cc") (r "=1.0.74") (d #t) (k 1)))) (h "1ifv7c30d01n2bcqfik4v5m5xaangm72gqgmj5l0h3ijp6ma0n0f") (r "1.64")))

(define-public crate-channelz_brotli-0.6.8 (c (n "channelz_brotli") (v "0.6.8") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)))) (h "1hfgm5j0xvdamx0w93x4l6ph4zmxh3jlbkwayz156489xwa6sw1f") (r "1.64")))

(define-public crate-channelz_brotli-0.6.9 (c (n "channelz_brotli") (v "0.6.9") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)))) (h "0f1d0kfanpggj9735imwgsbwsvjmlbs1m1zxprnnam2l3pkvk3j4") (r "1.64")))

(define-public crate-channelz_brotli-0.6.10 (c (n "channelz_brotli") (v "0.6.10") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)))) (h "1anqn9nkz0d2jfqqlrgrza78xzjlr135llmdh12hivqq13dqs1my") (r "1.64")))

(define-public crate-channelz_brotli-0.6.11 (c (n "channelz_brotli") (v "0.6.11") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)))) (h "04aq425v3w790z62qqi74diphr4ly25c9hwbp2ygw2ijrnhavq6s") (r "1.64")))

(define-public crate-channelz_brotli-0.6.12 (c (n "channelz_brotli") (v "0.6.12") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)))) (h "0lp48ddq714rhfvxxjarsazp25ac0i1wp49kd65v6d0yy8zc546x") (r "1.64")))

(define-public crate-channelz_brotli-0.7.0 (c (n "channelz_brotli") (v "0.7.0") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)))) (h "052v5qyb9fscgvwad1qy8dvg810bh162695cy3kmnvqb7w52ir99") (r "1.64")))

