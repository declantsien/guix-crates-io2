(define-module (crates-io ch an channel_pipes) #:use-module (crates-io))

(define-public crate-channel_pipes-0.1.0 (c (n "channel_pipes") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "14wvikprv93hr8bp1h3jah5snldihdqki2z0w7lj59yqcsam6k59") (f (quote (("mpsc") ("default" "crossbeam") ("crossbeam" "crossbeam-channel"))))))

(define-public crate-channel_pipes-0.2.0 (c (n "channel_pipes") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "dyn-clonable") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "011xzkhbi8qdzgn2w14qdfvbaf9x5mviy5pxh89d4f0rzr2sl74v") (f (quote (("mpsc") ("futures") ("flume") ("default" "crossbeam") ("crossbeam" "crossbeam-channel") ("bus"))))))

