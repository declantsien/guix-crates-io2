(define-module (crates-io ch an channel-drain) #:use-module (crates-io))

(define-public crate-channel-drain-0.1.0 (c (n "channel-drain") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09di9yfs2awyx1pnbdxxr6sxb6x0g3qsq4gipw09scrqz5mzi7vr")))

