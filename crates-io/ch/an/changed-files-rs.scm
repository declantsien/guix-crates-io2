(define-module (crates-io ch an changed-files-rs) #:use-module (crates-io))

(define-public crate-changed-files-rs-0.1.0 (c (n "changed-files-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "1mm9i3z4wwvgzdq9285m3y6l5yabq7mqaqkf0mcllhss1nm2n6x6")))

(define-public crate-changed-files-rs-0.1.4 (c (n "changed-files-rs") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "0sp89jkz7si2vhz8kmcxm8zmmivmjx4m5iyw5hr4pr7kq8pazdan")))

(define-public crate-changed-files-rs-0.1.5 (c (n "changed-files-rs") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "0g1lq1xli1ifmnq2h7p0rnyn9vjb41dvmv32jh57ym7ffakc9mhr")))

(define-public crate-changed-files-rs-0.1.6 (c (n "changed-files-rs") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "1l1jhid244x3pg5vbyyc8v64kvhf18q2w4vs1jlf9qnrqvsz79li")))

(define-public crate-changed-files-rs-0.1.9 (c (n "changed-files-rs") (v "0.1.9") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "0zllx927py9jg3sn7w3rmiv9p6ia0l0vj9qfha22pbpzsdlvh3a0")))

(define-public crate-changed-files-rs-0.1.11 (c (n "changed-files-rs") (v "0.1.11") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "15sgvykgj0g58yk3hlj37b4yli6vv0pg0mb9gjdmby6d0aqwq8aq")))

