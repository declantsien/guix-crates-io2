(define-module (crates-io ch ic chickensay) #:use-module (crates-io))

(define-public crate-chickensay-0.1.0 (c (n "chickensay") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0c6n67wbnba3wwpis8pv2nyj8q76vgb8adw1snh5c2gpyd6vk816")))

