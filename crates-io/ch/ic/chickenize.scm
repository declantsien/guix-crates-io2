(define-module (crates-io ch ic chickenize) #:use-module (crates-io))

(define-public crate-chickenize-0.1.1 (c (n "chickenize") (v "0.1.1") (h "0qbax7jsi0bhn6bniqjkqzg06806dil0lpz43qpx1is266kyydpb")))

(define-public crate-chickenize-0.2.0 (c (n "chickenize") (v "0.2.0") (h "1fdqhr33jra681jgl8y9j3iih93w5x64kh686wrb9jfg3js674k2")))

(define-public crate-chickenize-0.2.1 (c (n "chickenize") (v "0.2.1") (h "1yw6s1kkxblq45ydban5p5y5dxwf41h92yyn7nw7p3bg45jgh4bz")))

(define-public crate-chickenize-0.3.0 (c (n "chickenize") (v "0.3.0") (h "0ij8lk29wshaf9ll8w62j9djc7311s626ivgr0fwqrlsaabyvxhf")))

(define-public crate-chickenize-0.3.1 (c (n "chickenize") (v "0.3.1") (h "015w803hyw1l0spcdm9dg30fvvsd2kkbx8x2mhpa4k5zsmdyqnm7")))

(define-public crate-chickenize-0.3.2 (c (n "chickenize") (v "0.3.2") (h "1mgnzfwqifbfiz21hyp4ikkx9qwmpfqvikcqdgfxr6hbbqiqgzas")))

(define-public crate-chickenize-0.3.3 (c (n "chickenize") (v "0.3.3") (h "08dyhpsqrlb8cf4fi01hr6hds3fj0bfyl8q6kw4l93c0rvy31r6c")))

