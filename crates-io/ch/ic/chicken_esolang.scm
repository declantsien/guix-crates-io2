(define-module (crates-io ch ic chicken_esolang) #:use-module (crates-io))

(define-public crate-chicken_esolang-0.1.0 (c (n "chicken_esolang") (v "0.1.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0dzqiqgj6mk9qz507lrhkrq4f4sx87vx9f4rf45565451dqj7swf")))

(define-public crate-chicken_esolang-0.1.1 (c (n "chicken_esolang") (v "0.1.1") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1yir01gi1qnmrslnhwv3s1lhxjwsgx0y3xjdsgbcmaiw6j70vpgi")))

(define-public crate-chicken_esolang-0.1.2 (c (n "chicken_esolang") (v "0.1.2") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "11ffmxsbl8pcqaa7yrv2d5wz83hjv7vb996py53imqr6x69a91bl")))

