(define-module (crates-io ch ic chico) #:use-module (crates-io))

(define-public crate-chico-0.1.0 (c (n "chico") (v "0.1.0") (h "15j638wb5wm155mb9qgsav3i72hc69s1wwbcyj6jgbq8sl0xaga4")))

(define-public crate-chico-0.2.0 (c (n "chico") (v "0.2.0") (d (list (d (n "cliply") (r "^0.2.0") (d #t) (k 0)))) (h "0adwxqc67dbpdqx2frqk0358gqpc7jnbfzz4y6xnla6z1kbp0498")))

