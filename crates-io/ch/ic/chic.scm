(define-module (crates-io ch ic chic) #:use-module (crates-io))

(define-public crate-chic-0.0.0 (c (n "chic") (v "0.0.0") (h "196k4s7nw1y4gc7dsb2wb3vrbqw3sbkin4n2wb9c1g73w6kf3d2a")))

(define-public crate-chic-1.0.0 (c (n "chic") (v "1.0.0") (d (list (d (n "annotate-snippets") (r "^0.6.1") (d #t) (k 0)))) (h "0c74928i0bjv87lb7s9p9fcflvkfvd4axvqwar078qfzj8fmjkpa")))

(define-public crate-chic-1.1.0 (c (n "chic") (v "1.1.0") (d (list (d (n "annotate-snippets") (r "^0.6.1") (d #t) (k 0)))) (h "1r4plh8c0glgabdavvqfbxhbk3r8np855dajpwpzxxqcqrdc2nwm")))

(define-public crate-chic-1.1.1 (c (n "chic") (v "1.1.1") (d (list (d (n "annotate-snippets") (r "^0.6.1") (d #t) (k 0)))) (h "0zrsw1lbqn6q2avr2pbqln6fqzzrhvfjj6c4yiws1gnligxp67pb")))

(define-public crate-chic-1.2.0 (c (n "chic") (v "1.2.0") (d (list (d (n "annotate-snippets") (r "^0.6.1") (d #t) (k 0)))) (h "06khfzra7bbracj6m1kwq0fj967jhj92fwpkqilygkr8bss0r7l4")))

(define-public crate-chic-1.2.1 (c (n "chic") (v "1.2.1") (d (list (d (n "annotate-snippets") (r "^0.6.1") (d #t) (k 0)))) (h "12mc5igshk3g7racy2qdfx8ddklcsz5cy3ksdk37x7irfv584898")))

(define-public crate-chic-1.2.2 (c (n "chic") (v "1.2.2") (d (list (d (n "annotate-snippets") (r "^0.6.1") (d #t) (k 0)))) (h "037pkdccj25gr4my8fq1qni9v87rydpyhfi2naf86mimkxhxpdd5")))

