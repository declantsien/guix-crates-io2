(define-module (crates-io ch ic chickensources) #:use-module (crates-io))

(define-public crate-chickensources-0.1.0 (c (n "chickensources") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncli") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)))) (h "0jnx4bq22i6limq6n38da2vy5d8zy421idsz1j53m1qhkmlmhv2g")))

(define-public crate-chickensources-0.1.1 (c (n "chickensources") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncli") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)))) (h "03nfrqry58wm31xcsxbgbn9f9jgylgzvgslwx8kds62c4x7786yi")))

(define-public crate-chickensources-0.1.2 (c (n "chickensources") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fncli") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)))) (h "1c56k5b3b06vq1m6j7fl95gr8m9vfc65bnhgaj3sfdhhf3vlppsp")))

