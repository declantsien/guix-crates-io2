(define-module (crates-io ch ac chacli) #:use-module (crates-io))

(define-public crate-chacli-0.1.0 (c (n "chacli") (v "0.1.0") (d (list (d (n "chacha20") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1rv5k9vylrpjbc72a8q1dj9rx8w8j60m0254r9mfx9pwyja9h1x9")))

(define-public crate-chacli-0.1.1 (c (n "chacli") (v "0.1.1") (d (list (d (n "chacha20") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0j6l4ab65ibbkjhshrh4230wzjkflab7gxyi81nw93fqj88r037r")))

