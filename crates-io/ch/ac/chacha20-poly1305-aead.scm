(define-module (crates-io ch ac chacha20-poly1305-aead) #:use-module (crates-io))

(define-public crate-chacha20-poly1305-aead-0.1.0 (c (n "chacha20-poly1305-aead") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)))) (h "1zxkhjvyj02wagdqjfwag9pkdyhgmx7ns4if4lj0klv4j56z7a7r") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

(define-public crate-chacha20-poly1305-aead-0.1.1 (c (n "chacha20-poly1305-aead") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)))) (h "0incz3fwn5i403pii6rz7lq8gvy926x0kbfw66vv01k06qm5rgfv") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

(define-public crate-chacha20-poly1305-aead-0.1.2 (c (n "chacha20-poly1305-aead") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)))) (h "0fs2ym190lk4rl9kckh8lm693qw50j703ag8fnfgd54mla5hblkp") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

