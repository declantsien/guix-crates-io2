(define-module (crates-io ch ac chacha) #:use-module (crates-io))

(define-public crate-chacha-0.1.0 (c (n "chacha") (v "0.1.0") (d (list (d (n "byteorder") (r "0.4.*") (d #t) (k 0)) (d (n "keystream") (r "^1.0") (d #t) (k 0)))) (h "05mqvm0srhddcdxavwjmsr9xa4h0klnagjncaraj4q0p0d0ii4r3") (f (quote (("nightly") ("bench"))))))

(define-public crate-chacha-0.2.0 (c (n "chacha") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "keystream") (r "^1.0") (d #t) (k 0)))) (h "119k9xxd9q1m6zfdf8af558d6sp8s23gn604ydv2sp6hl182wlx9") (f (quote (("nightly") ("bench"))))))

(define-public crate-chacha-0.3.0 (c (n "chacha") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "keystream") (r "^1") (d #t) (k 0)))) (h "0qj839h7fjkmxlwyx5yqrcj0rlgvw2cfkaj0arhyb8gvnn0w1wyx") (f (quote (("nightly") ("bench"))))))

