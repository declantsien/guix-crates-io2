(define-module (crates-io ch ac chacha20-poly1305-stream) #:use-module (crates-io))

(define-public crate-chacha20-poly1305-stream-0.1.0 (c (n "chacha20-poly1305-stream") (v "0.1.0") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "chacha20-poly1305-aead") (r "^0.1.2") (d #t) (k 2)) (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "16hpficdl35y402r0fdvd2a8y95psaq8l5ssw5ial809zn5s6pkd") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

(define-public crate-chacha20-poly1305-stream-0.1.1 (c (n "chacha20-poly1305-stream") (v "0.1.1") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "chacha20-poly1305-aead") (r "^0.1.2") (d #t) (k 2)) (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0mx417pkfci590616akz2ml658di3ik0gsnmwk9g2jqharl47q1x") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

