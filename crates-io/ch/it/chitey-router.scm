(define-module (crates-io ch it chitey-router) #:use-module (crates-io))

(define-public crate-chitey-router-0.1.0 (c (n "chitey-router") (v "0.1.0") (d (list (d (n "chitey") (r "^0.1") (d #t) (k 0)))) (h "05mc1hsm3s79ajbv2ywczpkr1r916l4jjfhb5nwzmvr2h76vr1fg") (r "1.72")))

(define-public crate-chitey-router-0.2.0 (c (n "chitey-router") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "urlpattern") (r "^0.2") (d #t) (k 0)))) (h "18c82cgp3brr70vzzgr6ibsdpp0lj3pczlbxa1k9dhxsyii6gmvz") (r "1.64")))

(define-public crate-chitey-router-0.2.2 (c (n "chitey-router") (v "0.2.2") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "urlpattern") (r "^0.2") (d #t) (k 0)))) (h "1gwpx9a5f5y59fxl5jgiwv5c74b3w5g83lk6ycrp0fiizbrznn6j") (r "1.64")))

(define-public crate-chitey-router-0.2.4 (c (n "chitey-router") (v "0.2.4") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "urlpattern") (r "^0.2") (d #t) (k 0)))) (h "1ccknnbg23q23pas41gh74j78wivm2zap8q733acq6r5ckjpv57q") (r "1.64")))

(define-public crate-chitey-router-0.2.7 (c (n "chitey-router") (v "0.2.7") (h "0kr7lmhn3nsm44rwi3wdd6lx1ldlsw6p2xrq9dh9hx0j7k1c9qy1") (r "1.64")))

(define-public crate-chitey-router-0.2.8 (c (n "chitey-router") (v "0.2.8") (h "1hdpsdj8dg1lzcza7mcl4znfnjdy3fdmqbmz5f06k862dp77gdjr") (r "1.64")))

(define-public crate-chitey-router-0.2.15 (c (n "chitey-router") (v "0.2.15") (h "1qrfh4h42k83qq8s3fk55cwyn6kx3l9zjh3abblb06s9mf7c4xv1") (r "1.64")))

(define-public crate-chitey-router-0.2.19 (c (n "chitey-router") (v "0.2.19") (h "0dgypm0wmzpfxm4g9hlka3gl1mgi1mh65j0xyh7v0x9bn41igwa8") (r "1.64")))

