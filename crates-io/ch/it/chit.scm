(define-module (crates-io ch it chit) #:use-module (crates-io))

(define-public crate-chit-0.0.1 (c (n "chit") (v "0.0.1") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ar5gfid84vki7bs5nh6pyalssdw1i1g517v3wzp66cqx846xwvi")))

(define-public crate-chit-0.0.2 (c (n "chit") (v "0.0.2") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0li854y43vvyj74apyvxvjx0yggki7z4wsik7kyavny3ihm3sc2p")))

(define-public crate-chit-0.0.3 (c (n "chit") (v "0.0.3") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mmjfkk4kl927fl2ibr1v5946507dz51iwhf5n9k7nyan1z4481q")))

(define-public crate-chit-0.0.4 (c (n "chit") (v "0.0.4") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i4afryqcfsqjs6lvw077rbd664k1gbq0vi9iawh5v2p1dvd4yaf")))

(define-public crate-chit-0.0.5 (c (n "chit") (v "0.0.5") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04sf9i1algkj29mpwn5bxclcpj38hqqv4dx646886iwp1dx39vd5")))

(define-public crate-chit-0.1.0 (c (n "chit") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1affxv1wx58za3kpzag75d7w9za4dmps9qq9zi1vnz59fbhvymvn")))

(define-public crate-chit-0.1.1 (c (n "chit") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05qrxqx37lh62w395pidx7yrg4wg3m3j727f40v25gwpb2cjz9nq")))

(define-public crate-chit-0.1.2 (c (n "chit") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hs5rd817mrf92hfdfpf8a39x05zyxaivcz4rfxwbgrxpkfg2gxm")))

(define-public crate-chit-0.1.3 (c (n "chit") (v "0.1.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ws43wgffkgxvincxz82xxwwwh4jj3zz8q5yn3nrb9b1ni4jimq9")))

(define-public crate-chit-0.1.4 (c (n "chit") (v "0.1.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "020ylxv2jmg5prny7158aa1295vyl8d9xqycpzghwsj9x961m3ac")))

(define-public crate-chit-0.1.5 (c (n "chit") (v "0.1.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r8i8ywj7xkqncjng7afpl31jq2x52nwk6jvbpmsik36xr2gj7q7")))

(define-public crate-chit-0.1.6 (c (n "chit") (v "0.1.6") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sx3l82l6rgsnj3f81x2ldbrjwz9zwy33v3nvrj0z1g6qy0nyiy2")))

(define-public crate-chit-0.1.7 (c (n "chit") (v "0.1.7") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v738dxp3jpxz5pv489qr34b16py9hs6a2nscaabqmgdl05pf7kf")))

(define-public crate-chit-0.1.9 (c (n "chit") (v "0.1.9") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1acvz59kdnzrjihxsvwvx8pbm8vglg07mx9ygi0ms1rchrck8vfz")))

(define-public crate-chit-0.1.10 (c (n "chit") (v "0.1.10") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16skns2giyf0xhn8g0p1iwd22x5m6di04w8mf40srbbznsfy2x89")))

(define-public crate-chit-0.1.11 (c (n "chit") (v "0.1.11") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xxw9sh7zp93lhc7bc4r593h0vf7xakdvjjsp6w0skif1c6c9167")))

(define-public crate-chit-0.1.12 (c (n "chit") (v "0.1.12") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "openssl") (r "^0.10.11") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nscw1250qdsxdadg5887cwgkp3z71c70s9js783bsf2x4d2jhi5") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-chit-0.1.13 (c (n "chit") (v "0.1.13") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "openssl") (r "^0.10.11") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1px423wv3cm13plgsnx2702xn4m0ls0ffgbn241h9bsr4gvzs9f3") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-chit-0.1.14 (c (n "chit") (v "0.1.14") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "openssl") (r "^0.10.11") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pqf2la0xs5kcmjx35aps4m71fgv46xdbm9wkwxl1cbd46lvzvdb") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-chit-0.1.15 (c (n "chit") (v "0.1.15") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "hyphenation") (r "^0.7.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.11") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (f (quote ("hyphenation"))) (d #t) (k 0)))) (h "1zs4k2529xhzdw5fmqj2hbimq4hjfmpr8pq8s614gp47fmasfc7n") (f (quote (("vendored-openssl" "openssl/vendored"))))))

