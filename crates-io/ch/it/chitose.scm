(define-module (crates-io ch it chitose) #:use-module (crates-io))

(define-public crate-chitose-0.1.0 (c (n "chitose") (v "0.1.0") (h "0chx9blq9lwicr9ffr6zsrb3y8y6j73a8s4vwsgjc0vnfzn85a13") (y #t)))

(define-public crate-chitose-0.1.1 (c (n "chitose") (v "0.1.1") (h "1rbmjk43ir0055w6siy8pyqn65z9dwxxr9l0lj03s8az7rvpqp6r") (y #t)))

(define-public crate-chitose-0.1.2 (c (n "chitose") (v "0.1.2") (h "1a91z28yqxp2wxra9iqs2rbyqx402l02izvx9nb0jl2x36f8n638") (y #t)))

(define-public crate-chitose-0.1.3 (c (n "chitose") (v "0.1.3") (h "0gg53ac5ry2q5a8yqdvwjnhgkrffn0sybhz2zd1d14xj6w9m2fbl") (y #t)))

(define-public crate-chitose-0.1.4 (c (n "chitose") (v "0.1.4") (h "0rrarqghqhd8x3qhmk4s65i3d4xi48rp0znl9n6w0x3wc6i4g9zv") (y #t)))

(define-public crate-chitose-0.1.5 (c (n "chitose") (v "0.1.5") (h "1jr69w260j1qxq0h9sgxw27kb90brl3hhkamd1ahqdr6bd1q8x8b") (y #t)))

(define-public crate-chitose-0.1.6 (c (n "chitose") (v "0.1.6") (h "13y97w7yp448pwq3jxsap873nby4z38ki858gdjbbvg35hvnz3vb") (y #t)))

