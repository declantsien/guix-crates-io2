(define-module (crates-io ch it chitey-codegen) #:use-module (crates-io))

(define-public crate-chitey-codegen-0.1.0 (c (n "chitey-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rncz2qz2ybsxj8hsg3fmv7dd712plhrygb6qsla23k31xqbhhcw") (r "1.72")))

(define-public crate-chitey-codegen-0.2.0 (c (n "chitey-codegen") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sdnz9dyb3g8m8g1idfa84vvph2l8b4xd62rnbl97s6yawfbvsbx") (r "1.64")))

(define-public crate-chitey-codegen-0.2.1 (c (n "chitey-codegen") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1grgcrsl9li3n7nkrfzidfn42iwk73ld8n7dqv4x2s15781yjaak") (r "1.64")))

(define-public crate-chitey-codegen-0.2.2 (c (n "chitey-codegen") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kcr0sw5zsmw7lfi7n6ma8zd0xv1x1ynz49sravbp2blh4x54qlb") (r "1.64")))

(define-public crate-chitey-codegen-0.2.3 (c (n "chitey-codegen") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11fy9ql8wa4qczn07zd8mvxx1ccq048x903sa5rs6c7in8mdim8c") (r "1.64")))

(define-public crate-chitey-codegen-0.2.4 (c (n "chitey-codegen") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v87arkmh0x490fijld0xlabrg6df9wcmnwyl1n1mcb6xpk2p5sr") (r "1.64")))

(define-public crate-chitey-codegen-0.2.7 (c (n "chitey-codegen") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v187s6qag5hjf89k1s6lbcrbmz3mmwwm64yhdbbj7rq8pjk173d") (r "1.64")))

(define-public crate-chitey-codegen-0.2.8 (c (n "chitey-codegen") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ja4fnl2c4122vhmbyics98x2bb09l9fqr6r2p2jwrpfjg29ay5c") (r "1.64")))

(define-public crate-chitey-codegen-0.2.11 (c (n "chitey-codegen") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1szcnkcxbimgk8qd35c97w33a8zyzwa043947x1z62sq5fzfnf02") (r "1.64")))

(define-public crate-chitey-codegen-0.2.12 (c (n "chitey-codegen") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11fy455g4axdjan97xxii15f92afm704g818y0hm62qzcxxq8dhi") (r "1.64")))

(define-public crate-chitey-codegen-0.2.13 (c (n "chitey-codegen") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1br664dm3pqpkw8x5b1qr2k2vpa0q4lgcl2cxig0bm79kq730zic") (r "1.64")))

(define-public crate-chitey-codegen-0.2.16 (c (n "chitey-codegen") (v "0.2.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "150k3cxzvqr021lhz4k18w9jzqxmi7rxd0r03f55dlb4ff3l0bzg") (r "1.64")))

(define-public crate-chitey-codegen-0.2.19 (c (n "chitey-codegen") (v "0.2.19") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19gpz8m9nd47vq9kdq5vvjfkzzfhyqmlsqyl3cdjn34wibp3i8xz") (r "1.64")))

(define-public crate-chitey-codegen-0.3.0 (c (n "chitey-codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "091j1f6m7r0ly8j65yph7ragv976ib7wpjyg96q26fvs9df69szl") (r "1.64")))

(define-public crate-chitey-codegen-0.4.0 (c (n "chitey-codegen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vwi5z6lvl2m47gmzc5vh339yhbk4crwflahcz2bhq742489ami0") (r "1.64")))

