(define-module (crates-io ch it chitin_security) #:use-module (crates-io))

(define-public crate-chitin_security-0.0.0 (c (n "chitin_security") (v "0.0.0") (h "0rxm4n6ik51nv1h47ybf11raypgz77pm5sybn15am2hpsn9z5g6m")))

(define-public crate-chitin_security-0.0.1 (c (n "chitin_security") (v "0.0.1") (h "1s6mi7q9d9jlpdlf7y4cl06p2z8liaw5rnzyfigzbajr8n0qagxw")))

