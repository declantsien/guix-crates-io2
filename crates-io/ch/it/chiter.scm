(define-module (crates-io ch it chiter) #:use-module (crates-io))

(define-public crate-chiter-0.1.0 (c (n "chiter") (v "0.1.0") (h "1f3x16fbp3rdnqxgcx9mkfgcgx0kvk9rf65kq6z77x9i1ggqd9jq")))

(define-public crate-chiter-0.2.0 (c (n "chiter") (v "0.2.0") (h "0q71fxk2c9bvpa2qx14rs3rx9rwvdwxj22kddvqx7g346pj3qs7l")))

(define-public crate-chiter-0.3.0 (c (n "chiter") (v "0.3.0") (h "14cajk8blxahd5fjg5nk5jfkhc0ig9v6n77i78qgwpgf35cp5dh4")))

(define-public crate-chiter-0.4.0 (c (n "chiter") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3.5") (d #t) (t "cfg(windows)") (k 0)))) (h "1ivwi6bb6nhf1kz12ndgamips684m8fqkhfwf1bc796f4hmh7z5a")))

(define-public crate-chiter-0.4.2 (c (n "chiter") (v "0.4.2") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (d #t) (t "cfg(windows)") (k 0)))) (h "017723gigz0j17ll4a76shjlw4906rmyrcnwbf2jbp7zymc0vznr")))

(define-public crate-chiter-0.4.3 (c (n "chiter") (v "0.4.3") (d (list (d (n "detour") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fssaq25gw16gq73czya8yayqffhakdwa8yqz1g36f09dlvqf0ir") (f (quote (("nightly" "detour"))))))

