(define-module (crates-io ch ai chained) #:use-module (crates-io))

(define-public crate-chained-0.1.0 (c (n "chained") (v "0.1.0") (h "0jb86d3h95yyagb5p9ym9ydsiqsl3pqgyrp8d6x5cvvzaxwngm6p")))

(define-public crate-chained-0.1.1 (c (n "chained") (v "0.1.1") (h "0rlx5jxcdk7najvvilm0szn8b9fwiyvcysmsjw45b18fmynm6alc")))

