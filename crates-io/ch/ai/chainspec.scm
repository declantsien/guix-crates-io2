(define-module (crates-io ch ai chainspec) #:use-module (crates-io))

(define-public crate-chainspec-0.1.0 (c (n "chainspec") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vapjson") (r "^0.1.0") (d #t) (k 0)))) (h "18alj9mx19l80cscdrgj753sn7rip7bl11xl0irfgm4jbs6sb43d")))

