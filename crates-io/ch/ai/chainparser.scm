(define-module (crates-io ch ai chainparser) #:use-module (crates-io))

(define-public crate-chainparser-0.1.0 (c (n "chainparser") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 2)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.18.4") (d #t) (k 0)) (d (n "solana_idl") (r "^0.0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "17lmadaw9nvny2vg2avmypk3hxl2kyv86dwjk6bqgh7fy4z8ar78")))

