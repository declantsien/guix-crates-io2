(define-module (crates-io ch ai chainbuf) #:use-module (crates-io))

(define-public crate-chainbuf-0.0.4 (c (n "chainbuf") (v "0.0.4") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "*") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "01131xs2qaby270cw1yhvn3snqz6j37vrc18cf8l6kgkf425nh86") (f (quote (("default" "nix"))))))

(define-public crate-chainbuf-0.0.5 (c (n "chainbuf") (v "0.0.5") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "14yqj95ffmdbz16y3hb84jbw40ahg3ahrji71vazvbw94xh2s4qy") (f (quote (("default" "nix"))))))

(define-public crate-chainbuf-0.0.7 (c (n "chainbuf") (v "0.0.7") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0bn53dhns1q17hp2qa3lv92c9c0in7f2wvnwg8n3w52db3k0l91g") (f (quote (("default" "nix"))))))

(define-public crate-chainbuf-0.0.9 (c (n "chainbuf") (v "0.0.9") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1lpz0n9vfd6kwi57ywmvbk9yk3wllpk1kvmvnf98qhm0a1f6q6j4") (f (quote (("default" "nix"))))))

(define-public crate-chainbuf-0.0.10 (c (n "chainbuf") (v "0.0.10") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r ">=0.15.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1cghzrhg04574sqjjn6447pas92igr8w5hz8wi6ib5i0hczgl3zq") (f (quote (("default" "nix"))))))

(define-public crate-chainbuf-0.1.0 (c (n "chainbuf") (v "0.1.0") (d (list (d (n "libc") (r ">=0.2") (d #t) (k 0)) (d (n "nix") (r ">=0.20.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1h11nh82m74h4z27zjnq7cxdkx4vpyx3n5cap8ra5z39kpc5j9m7") (f (quote (("default" "nix"))))))

