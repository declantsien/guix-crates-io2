(define-module (crates-io ch ai chainthru-middleware) #:use-module (crates-io))

(define-public crate-chainthru-middleware-0.0.1 (c (n "chainthru-middleware") (v "0.0.1") (h "04ybws20pj3bcy8zkm4p4914pdy0mpdj0h7mm64gb8vjknf2f13h") (y #t) (r "1.73.0")))

(define-public crate-chainthru-middleware-0.0.0-reserved (c (n "chainthru-middleware") (v "0.0.0-reserved") (h "1naxlp6alddq48r3ins5n1m2rqv2rsbinvifq0lman191dz9xkhb") (r "1.73.0")))

