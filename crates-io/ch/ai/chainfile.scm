(define-module (crates-io ch ai chainfile) #:use-module (crates-io))

(define-public crate-chainfile-0.1.0 (c (n "chainfile") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 2)))) (h "1nlmd52xhnn2x227gcyraqnxddfpvvrzm60mwhl46npsfjdj36lq") (r "1.68.0")))

(define-public crate-chainfile-0.2.0 (c (n "chainfile") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 2)) (d (n "noodles") (r "^0.35.0") (f (quote ("core" "fasta"))) (d #t) (k 2)) (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "0ydf3i5qf63s6cszk6brcl0xld0hichs56s8ix8j722ziax1zyvh") (r "1.68.0")))

(define-public crate-chainfile-0.2.1 (c (n "chainfile") (v "0.2.1") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 2)) (d (n "noodles") (r "^0.35.0") (f (quote ("core" "fasta"))) (d #t) (k 2)) (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "0shr2gp5rfcrsnf8d1vh5qwxm63gwfp883l7sf5sx3pdvbk6v1cq") (r "1.72.0")))

