(define-module (crates-io ch ai chain-debug) #:use-module (crates-io))

(define-public crate-chain-debug-0.1.0 (c (n "chain-debug") (v "0.1.0") (h "14kq1nda58plbfbmna0n0z3sf36gmiwjxavxbf39r1ck5nfhdsrn")))

(define-public crate-chain-debug-0.1.1 (c (n "chain-debug") (v "0.1.1") (h "02agn9wm3l5gngj2q7bw9vygqrplk789jk3bgcwnjrg9n2459mkn")))

