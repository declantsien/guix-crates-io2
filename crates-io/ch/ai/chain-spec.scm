(define-module (crates-io ch ai chain-spec) #:use-module (crates-io))

(define-public crate-chain-spec-0.0.0 (c (n "chain-spec") (v "0.0.0") (h "0xnvvq5y332svibvp6kgdxvpdxacp37g2lfciza9zw2kvgjhr0lk")))

(define-public crate-chain-spec-2.0.1 (c (n "chain-spec") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0ha7vcg2g40silfcrv0dznfz7lw21jlwpr0ms2605wa8f300s40b")))

