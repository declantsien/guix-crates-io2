(define-module (crates-io ch ai chain) #:use-module (crates-io))

(define-public crate-chain-0.1.0 (c (n "chain") (v "0.1.0") (h "05sajlkhda4k4r8m1yvj1m3idsdb7pwgwbwly3skkv90f73dh68q")))

(define-public crate-chain-0.1.1 (c (n "chain") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "ring") (r "^0.4.3") (d #t) (k 0)) (d (n "untrusted") (r "^0.3.1") (d #t) (k 0)))) (h "19pim57mvdssv5f81l0m9hzz4dlxdmsqgrvdzqqa7w8sq38hqv9y")))

