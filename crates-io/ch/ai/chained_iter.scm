(define-module (crates-io ch ai chained_iter) #:use-module (crates-io))

(define-public crate-chained_iter-0.1.0 (c (n "chained_iter") (v "0.1.0") (h "09w2s64mz30dpwiyg4m3ryy6rmap2knsdg49mlqki1aak4z5kq4r") (f (quote (("nightly")))) (y #t)))

(define-public crate-chained_iter-0.1.1 (c (n "chained_iter") (v "0.1.1") (h "1syr5yz8fcccjib81x5y0f0im3hak5p67fgn8170ibya836vpxx9") (f (quote (("nightly")))) (y #t)))

(define-public crate-chained_iter-0.1.2 (c (n "chained_iter") (v "0.1.2") (h "06qg0hc75ksdl3rv1adsi0p7ynxfj87b0pjfl9k4jw3v90w8f3pg") (f (quote (("nightly")))) (y #t)))

