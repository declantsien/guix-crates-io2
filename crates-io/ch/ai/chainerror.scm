(define-module (crates-io ch ai chainerror) #:use-module (crates-io))

(define-public crate-chainerror-0.1.0 (c (n "chainerror") (v "0.1.0") (h "0m227pzmir5wawnvkgw3xs6ixbjzfvdp4ll3cbrb2wbn0gcch99q") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.2.0 (c (n "chainerror") (v "0.2.0") (h "1cdd9h8ac8k6f9k4llvhlj8dcvv39bzwa7jg1bpwz895kv451ix7") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.3.0 (c (n "chainerror") (v "0.3.0") (h "1c518bx0dakjbczkd57ipm2rwc0faqkvb5anwj257s5bnsw84fh8") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.3.1 (c (n "chainerror") (v "0.3.1") (h "0qz488crap1lhvqg6fganl7hz42r2p3kikpi904h1kbc2w1hik2z") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.3.2 (c (n "chainerror") (v "0.3.2") (h "01amnids813jnx2f0bqkvgy8bsydcaig3vkk1wa0zpj5zxihdc95") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.4.0 (c (n "chainerror") (v "0.4.0") (h "0zyiqglra2ypl7wwb5lx2w2sffmhip09xqp5509xwiv28zqr8cf3") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.4.1 (c (n "chainerror") (v "0.4.1") (h "1haz0h745lxw6884s3m3dic75q1f7gsjmqqs46ji70bm0vzh45a2") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.4.2 (c (n "chainerror") (v "0.4.2") (h "0p3pswxvjncm051733hzm5y804smsz949f2cj790mya5nliajp14") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.4.3 (c (n "chainerror") (v "0.4.3") (h "1r4xvnz7av7pg92ad41gh0ryfazdh48c01hqmpw8bb61df36kw2p") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.5.0 (c (n "chainerror") (v "0.5.0") (h "0fi84r4x1d26pkc6si84jxdly4achz7nv47ch1vlcfix4cvxfv94") (f (quote (("no-fileline") ("no-debug-cause") ("display-cause") ("default"))))))

(define-public crate-chainerror-0.6.0 (c (n "chainerror") (v "0.6.0") (h "1frkq60806cpw7zk3j6cfh1fwcm1f1r9l2zxjj4kgvl75ym548ib") (f (quote (("display-cause") ("default"))))))

(define-public crate-chainerror-0.6.1 (c (n "chainerror") (v "0.6.1") (h "1ygjz4p9nbxcnkhm27qwm71i2r64q9azgqci91mzwj7dp018mhlq") (f (quote (("display-cause") ("default"))))))

(define-public crate-chainerror-0.7.0 (c (n "chainerror") (v "0.7.0") (h "11mjxcismk68zz01nz3fjqx2kb7wmbasy4qz9f0rdih3rfvb7ydq") (f (quote (("display-cause") ("default"))))))

(define-public crate-chainerror-0.7.1 (c (n "chainerror") (v "0.7.1") (h "13p7zkw1rwdmd5bq3kn3csnrsyks6kd516b91mhacn62n1zvpq8c") (f (quote (("display-cause") ("default"))))))

(define-public crate-chainerror-0.8.0 (c (n "chainerror") (v "0.8.0") (h "02mivvx1kbl7hw0sjg47i6svmxfi9bjpmwfrh19sj20xgvnq4jff")))

(define-public crate-chainerror-0.8.1 (c (n "chainerror") (v "0.8.1") (h "1746bczngw7ykibnaglg3q58ydfzrq7flcj77cr92lplvv7ja36q")))

(define-public crate-chainerror-0.8.2 (c (n "chainerror") (v "0.8.2") (h "1y1fqw9xqp28h1xpjsrn2l71l1kbwygm1xaqjwba940dgzsg3752")))

(define-public crate-chainerror-1.0.0 (c (n "chainerror") (v "1.0.0") (h "1d9ig4yibym8mlznrc615m0ngfnhl0a2wrrjdc5n47xr5s0r0q1z")))

