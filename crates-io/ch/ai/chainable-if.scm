(define-module (crates-io ch ai chainable-if) #:use-module (crates-io))

(define-public crate-chainable-if-0.0.0 (c (n "chainable-if") (v "0.0.0") (h "1swq0hzvy3r0zd5all7x8aa8zlxp10azik7gbxqk1df247v8441c")))

(define-public crate-chainable-if-0.1.0 (c (n "chainable-if") (v "0.1.0") (h "000b7yqpzxmlxnap6a1ij77dsxdbxrk664s740y4q5s09h8rl23g")))

(define-public crate-chainable-if-0.1.1 (c (n "chainable-if") (v "0.1.1") (h "0y3n4zj39zxz112flvgfkji8453raar99fqcmm2a5d4gc060xf96")))

