(define-module (crates-io ch ai chainsaw) #:use-module (crates-io))

(define-public crate-chainsaw-1.8.2 (c (n "chainsaw") (v "1.8.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)))) (h "1zjihpvjmpa7ma2fzj4fxf342wk1dkfxci9q172763n266k883q7")))

(define-public crate-chainsaw-1.9.0 (c (n "chainsaw") (v "1.9.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "syntesuite") (r "^0.1") (d #t) (k 0)))) (h "1ilk1bjawy54nhhhvz1zvf0q3rn3pas076zahd8ckcmi3scvph0m")))

(define-public crate-chainsaw-1.9.1 (c (n "chainsaw") (v "1.9.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "syntesuite") (r "^0.1") (d #t) (k 0)))) (h "0fzjs5hj8053iyw86h2yhb0v56ysr3swmhh740d9p918vvmn1k6a")))

(define-public crate-chainsaw-1.11.0 (c (n "chainsaw") (v "1.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.7.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "syntesuite") (r "^0.1") (d #t) (k 0)))) (h "0l5jj07x5hby52ic8xq512pxaa48mgqm1s10qc2z4pll6dxxqhzg")))

(define-public crate-chainsaw-1.11.1 (c (n "chainsaw") (v "1.11.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.7.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "syntesuite") (r "^0.2") (d #t) (k 0)))) (h "15a62p957pl0y1jq5j9pwynhdb6nwmbbrzrbjjm35fzcyj4nm0xd")))

(define-public crate-chainsaw-1.11.2 (c (n "chainsaw") (v "1.11.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.7.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "syntesuite") (r "^0.2") (d #t) (k 0)))) (h "0nx4p48qz0d6izd93aka3av29dqvsjwyil2qhhx0rj7k9lglq9h7")))

(define-public crate-chainsaw-1.11.3 (c (n "chainsaw") (v "1.11.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.7.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "syntesuite") (r "^0.2") (d #t) (k 0)))) (h "0vwcbvac5nw7v2fpd4n0y38g8rchhz5wp9jzp4q3blrpasxk2522")))

(define-public crate-chainsaw-1.11.4 (c (n "chainsaw") (v "1.11.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.7.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "syntesuite") (r "^0.2") (d #t) (k 0)))) (h "0hnd5jgk7sx26knaaxpc15hclp0q24lxh9w6ch11qwdsab68mzq3")))

(define-public crate-chainsaw-1.11.5 (c (n "chainsaw") (v "1.11.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)) (d (n "syntesuite") (r "^0.2") (d #t) (k 0)))) (h "1kcgvgy9bvc4d1fvvrjk132nn9xqxvhb0081pkb91q7g2prphh4v")))

(define-public crate-chainsaw-1.11.6 (c (n "chainsaw") (v "1.11.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "syntesuite") (r "^0.2") (d #t) (k 0)))) (h "09lywa92m1jkplg5b0f4001m2zw93zx1fkd9k1lxbn048kda4x6f")))

(define-public crate-chainsaw-1.12.0 (c (n "chainsaw") (v "1.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.9") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "syntesuite") (r "^0.2") (d #t) (k 0)))) (h "1walanw2fs3dgb1cv7wjcyfl46akfqz4cwabnazx6b9ssccxf4bx")))

(define-public crate-chainsaw-1.13.0 (c (n "chainsaw") (v "1.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "syntesuite") (r "^0.2") (d #t) (k 0)))) (h "0jqzwy0kidx8zayq1fjlsd0z6pyzbv3hlknqmn05mkzwd76mgx5v")))

(define-public crate-chainsaw-1.14.0 (c (n "chainsaw") (v "1.14.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.11") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "syntesuite") (r "^0.2") (d #t) (k 0)))) (h "1h2ah46xgf2zgx6llv2z04hhd09m3anrx1f7kh4k8lrpbd7nkrbc")))

(define-public crate-chainsaw-1.14.1 (c (n "chainsaw") (v "1.14.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.11") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "syntesuite") (r "^0.3") (d #t) (k 0)))) (h "1q46fkb3z2cin7ba6mvrb56xlk866iqc3lnn7ngz1cp138mrkwpp")))

(define-public crate-chainsaw-1.14.2 (c (n "chainsaw") (v "1.14.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "newick") (r "^0.11") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "syntesuite") (r "^0.4") (d #t) (k 0)))) (h "02vgqf23zymgrbks7bn076dh02lk6iy4zclg041882m4mkbnyy4g")))

