(define-module (crates-io ch ai chains) #:use-module (crates-io))

(define-public crate-chains-0.1.0 (c (n "chains") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.16") (f (quote ("alloc" "critical-section"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (o #t) (k 0)))) (h "0l0qrp1rkp3amh16ip47byh9n99x157xrwl0pvr05qx5k3amw4dm") (f (quote (("std" "once_cell/std" "serde/std" "serde_json/std") ("mini") ("default" "std" "zip")))) (s 2) (e (quote (("zip" "dep:zip"))))))

