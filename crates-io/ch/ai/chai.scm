(define-module (crates-io ch ai chai) #:use-module (crates-io))

(define-public crate-chai-0.1.0 (c (n "chai") (v "0.1.0") (h "0pr2w1i6ldp38fkkl5xbs2cwq8867f55qcirdy6gri7siksklf7v")))

(define-public crate-chai-0.0.1 (c (n "chai") (v "0.0.1") (h "1rf4vid59b0xl4gk2ip1yr7ch65xjq49jqp5bfamy2j66jlragc4")))

