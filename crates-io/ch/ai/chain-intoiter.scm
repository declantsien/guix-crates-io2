(define-module (crates-io ch ai chain-intoiter) #:use-module (crates-io))

(define-public crate-chain-intoiter-0.1.4 (c (n "chain-intoiter") (v "0.1.4") (h "06dgxx88j9nywc8f7220571ab1wkmll3dbdhawky0ggq6asfcchn")))

(define-public crate-chain-intoiter-0.2.0 (c (n "chain-intoiter") (v "0.2.0") (h "0gi4cwxc99hlzmv5ns6amwx882gqi4k9w58hr43yxwmwqlw6ihni")))

(define-public crate-chain-intoiter-0.3.0 (c (n "chain-intoiter") (v "0.3.0") (h "19g2xdwhbm8z2bad0a2bdqpnzqsr58mwv261pzph4lfyhk1v0jhl")))

(define-public crate-chain-intoiter-0.4.0 (c (n "chain-intoiter") (v "0.4.0") (h "17yiqyhyqd6bhv7hcsjmk7y5g1lbzh0v9q25555cmjlzni2annq3")))

