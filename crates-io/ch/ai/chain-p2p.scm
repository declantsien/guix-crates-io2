(define-module (crates-io ch ai chain-p2p) #:use-module (crates-io))

(define-public crate-chain-p2p-0.1.2 (c (n "chain-p2p") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "mio") (r "^0.6.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "18cjyphzwx6abaxfygcl79hgyyi6q6czjans9nh2dq1zzn19axsq") (y #t)))

(define-public crate-chain-p2p-0.1.3 (c (n "chain-p2p") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "mio") (r "^0.6.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "050vvjzm76kzymgzqa8mnwljxvgs6ira4s3r1jf0znymlkqhfgvv")))

(define-public crate-chain-p2p-0.1.4 (c (n "chain-p2p") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "mio") (r "^0.6.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "02xmyik8girwyzs3aya3qpam337j1qfhqlgq2rx8b2m6skar9kfr")))

(define-public crate-chain-p2p-0.1.5 (c (n "chain-p2p") (v "0.1.5") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "0kics73pzdv5fk310p2sab3k8bary9acl5bhwnpq553929c63cmx")))

(define-public crate-chain-p2p-0.1.6 (c (n "chain-p2p") (v "0.1.6") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "10bc01cmy7fva6gq9m3742smkxhd00hqy3x5p5mlyv33499cvzk9")))

(define-public crate-chain-p2p-0.1.7 (c (n "chain-p2p") (v "0.1.7") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "113b80rmlp9v5h8mhqjlz7bjigr1dlpv4jdma9xgcyb7hh82csx6")))

