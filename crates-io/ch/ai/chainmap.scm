(define-module (crates-io ch ai chainmap) #:use-module (crates-io))

(define-public crate-chainmap-0.0.0 (c (n "chainmap") (v "0.0.0") (h "1sck56gqgp9j8s2rqzcq8g8sq2fqhprfyg4f60m7qjd2bwh0qbdn")))

(define-public crate-chainmap-0.0.1 (c (n "chainmap") (v "0.0.1") (h "00y3pvp43644vq9fk1hhs8zkrchlb2nvpkbqsx4sgss175j3bjyg")))

(define-public crate-chainmap-0.1.0 (c (n "chainmap") (v "0.1.0") (h "13pv7wy3zq9x6plagf76zcbbiaz19cy2lb82lj57lagn11nzj1vw")))

(define-public crate-chainmap-0.1.1 (c (n "chainmap") (v "0.1.1") (h "0v64czpqxp0h5yszlxrpckjcik0rz9bc52gxbpscq8cx81fhxyhi")))

(define-public crate-chainmap-0.1.2 (c (n "chainmap") (v "0.1.2") (h "1icxvaqvn37ay5sg3vay3fkanyszxwpkzpsvw5v4vpym6b9m3m88")))

