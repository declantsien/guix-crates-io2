(define-module (crates-io ch ai chainer) #:use-module (crates-io))

(define-public crate-chainer-0.1.0 (c (n "chainer") (v "0.1.0") (h "07y9j91ngxppmw6467v0xz4y7m9rwqhpim5ilxx1438zxj2g7jcf") (f (quote (("results"))))))

(define-public crate-chainer-0.1.1 (c (n "chainer") (v "0.1.1") (h "00gmhy2fmimdm7j5b58i9wpjrhvd7vi9varknk1hnn76w7v4jznb") (f (quote (("results"))))))

