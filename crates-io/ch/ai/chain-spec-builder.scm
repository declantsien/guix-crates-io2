(define-module (crates-io ch ai chain-spec-builder) #:use-module (crates-io))

(define-public crate-chain-spec-builder-0.0.0 (c (n "chain-spec-builder") (v "0.0.0") (h "11kx8pn1im4spnrmg2hkymsbpgcqsmf4rznqmi1q7pnsrxx7clcl") (y #t)))

(define-public crate-chain-spec-builder-2.0.0 (c (n "chain-spec-builder") (v "2.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "node-cli") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "tc-chain-spec") (r "^2.0.0") (d #t) (k 0)) (d (n "tc-keystore") (r "^2.0.0") (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-keystore") (r "^0.8.1") (d #t) (k 0)))) (h "1lchsqyxsbf51iy3lmlaai2g90x7zq7drpyz6ppn4kd4xhlpacml")))

