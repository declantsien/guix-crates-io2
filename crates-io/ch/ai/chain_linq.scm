(define-module (crates-io ch ai chain_linq) #:use-module (crates-io))

(define-public crate-chain_linq-0.1.0 (c (n "chain_linq") (v "0.1.0") (d (list (d (n "big_mac") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "1smjdrjaaqn383h18mr46ccq5lrnvajddk7bjbiz1hk4bldbpmim") (y #t)))

(define-public crate-chain_linq-0.1.1 (c (n "chain_linq") (v "0.1.1") (d (list (d (n "big_mac") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "1ca7618abfpn34gv70np9dcsr4kczs4sycw8vnziaszgv1mgyx42") (y #t)))

(define-public crate-chain_linq-0.1.2 (c (n "chain_linq") (v "0.1.2") (d (list (d (n "big_mac") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "1z6zvnabid0c3d1zcnb9k9hpx43kfy2f1h5wljacapipx15832xm") (y #t)))

(define-public crate-chain_linq-0.1.3 (c (n "chain_linq") (v "0.1.3") (d (list (d (n "big_mac") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "1pk851yn722lml4327mvjbjjpkppjrnxjb7dzf5pldf6qqzci375")))

