(define-module (crates-io ch ai chain-cmp) #:use-module (crates-io))

(define-public crate-chain-cmp-0.2.0 (c (n "chain-cmp") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0zazck6irbx5p1jf92myn4qz19bb4h4ygg4fzzskv37jgqrdyy9g")))

