(define-module (crates-io ch ai chainblocks) #:use-module (crates-io))

(define-public crate-chainblocks-0.1.0 (c (n "chainblocks") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "csv") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "ctor") (r "^0.1.2") (d #t) (k 0)) (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)))) (h "0yxjx6ysm2n21vbqr0dkhcka2zqa3bkyjfa9dvm6h9x1iix9yk4b") (f (quote (("dummy") ("default") ("cb_static") ("cb_dynamic") ("blocks" "csv"))))))

