(define-module (crates-io ch ai chaintools) #:use-module (crates-io))

(define-public crate-chaintools-0.1.0 (c (n "chaintools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.7") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rxbh5fa2m67fw78mlmi9s5bd62kfcdb1yi3bik3djnxmnn22dkg") (f (quote (("read" "rayon" "fxhash" "memchr") ("default" "read"))))))

