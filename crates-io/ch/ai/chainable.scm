(define-module (crates-io ch ai chainable) #:use-module (crates-io))

(define-public crate-chainable-0.1.0 (c (n "chainable") (v "0.1.0") (h "12900nl258hh37g3g2kz4jki042nn3v893hd4ac95kfyn9wk2swc") (y #t)))

(define-public crate-chainable-1.0.0 (c (n "chainable") (v "1.0.0") (h "1msw8gjjw32m7iq0vj40k2jsg8v2429b6vmllksb7akhpv7wrrgf") (y #t)))

