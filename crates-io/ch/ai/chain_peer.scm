(define-module (crates-io ch ai chain_peer) #:use-module (crates-io))

(define-public crate-chain_peer-0.1.0 (c (n "chain_peer") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.6.11") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)))) (h "0y6ncpvwgxiq1dnkgs47ibkk4lw17qwj2qnfsm9dv0gjifb9w27j")))

(define-public crate-chain_peer-0.1.1 (c (n "chain_peer") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.6.11") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)))) (h "09bzr5qlff7jsmqrn6hw4gz0fzcx9h8g4xgaa8aq847rsjjv8mw8")))

