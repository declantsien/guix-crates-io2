(define-module (crates-io ch ai chainthru-client) #:use-module (crates-io))

(define-public crate-chainthru-client-0.0.1 (c (n "chainthru-client") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0zmlzivmy6181xic17ihvhl2wl33ml6qg22kicb5vf1cxd3cx5pa") (y #t)))

(define-public crate-chainthru-client-0.0.0-reserved (c (n "chainthru-client") (v "0.0.0-reserved") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1rwbhnlxanzl89f78sc7blhg2bj7p33i9d9jqsn8frn2pmz79f26")))

