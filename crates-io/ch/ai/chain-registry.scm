(define-module (crates-io ch ai chain-registry) #:use-module (crates-io))

(define-public crate-chain-registry-0.1.0 (c (n "chain-registry") (v "0.1.0") (d (list (d (n "assay") (r "^0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gbzw8naibwivlrjdz4r3nh03pjsa0ha5dg8i8zqi4n8y4w0jp11") (f (quote (("default" "cache") ("cache"))))))

(define-public crate-chain-registry-0.2.0-rc1 (c (n "chain-registry") (v "0.2.0-rc1") (d (list (d (n "assay") (r "^0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11d92gh1bpf52vw58jv4p3dkrax72xyl8a0hjlwq8clryznj54lf") (f (quote (("default" "cache") ("cache"))))))

(define-public crate-chain-registry-0.2.0-rc2 (c (n "chain-registry") (v "0.2.0-rc2") (d (list (d (n "assay") (r "^0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qq7mv7xkzgbr7av1wdmk7bzqyg4cdaqn2pl320mxn8j3jpyn3rl") (f (quote (("default" "cache") ("cache"))))))

(define-public crate-chain-registry-0.2.0-rc3 (c (n "chain-registry") (v "0.2.0-rc3") (d (list (d (n "assay") (r "^0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "044jhwz3f21g57jq2xllgknzwpzqqv0k6xkmn0nlifxgh8cpzlaq") (f (quote (("default" "cache") ("cache"))))))

