(define-module (crates-io ch ai chainlink_solana) #:use-module (crates-io))

(define-public crate-chainlink_solana-0.0.1 (c (n "chainlink_solana") (v "0.0.1") (h "0c0slsxzml7kxa1cpv3pqjfrjdzdpplz30kbq54gmh5inx4i9mrp") (y #t)))

(define-public crate-chainlink_solana-0.1.0 (c (n "chainlink_solana") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.6") (d #t) (k 0)))) (h "10gbivlfzbj65j2j7r5i168qva15md51ddnpxr0i1af4gqvxjkfy") (f (quote (("default")))) (y #t)))

(define-public crate-chainlink_solana-0.1.1 (c (n "chainlink_solana") (v "0.1.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.6") (d #t) (k 0)))) (h "0iqn1crlh4haij44bl0f2kz37i3i28x18l8p15y99vyd9b4sdqa4") (f (quote (("default"))))))

(define-public crate-chainlink_solana-0.1.2 (c (n "chainlink_solana") (v "0.1.2") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.6") (d #t) (k 0)))) (h "1mab3npsh60ycwc9wysdcp3dxdhdcjjn8blfcrf7nb7znv4a7rf5") (f (quote (("default")))) (y #t)))

(define-public crate-chainlink_solana-1.0.0 (c (n "chainlink_solana") (v "1.0.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.6") (d #t) (k 0)))) (h "16rknvnvq85l830fh6ypka8kzf7wmas166cf7qcnsdl7g7zhgg51") (f (quote (("default"))))))

