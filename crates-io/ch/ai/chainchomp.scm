(define-module (crates-io ch ai chainchomp) #:use-module (crates-io))

(define-public crate-chainchomp-0.1.0 (c (n "chainchomp") (v "0.1.0") (h "1y6m28pzdjdwf280shr4npjl9fips39q9849c7qilm6dpjflf1l1")))

(define-public crate-chainchomp-0.1.1 (c (n "chainchomp") (v "0.1.1") (h "0jrvbxil7xqga59z0d2ifqzdh18h4dhr6x5335178sdc394hp5hw")))

(define-public crate-chainchomp-0.1.2 (c (n "chainchomp") (v "0.1.2") (h "03mgjchqa25vr2y927z4js4pk4rdh1zy17ksbnzd0k6zm0gvmlv9")))

(define-public crate-chainchomp-0.1.3 (c (n "chainchomp") (v "0.1.3") (h "1m2yycl98afs0l6a8yn4bfxdzyxjvhah80x01schfy8lxxy062rv")))

(define-public crate-chainchomp-0.1.4 (c (n "chainchomp") (v "0.1.4") (h "0kjs0rlijbjw9pm4pqwhagxdg6xbr5ag66n95pkr50izhz65gd9s")))

(define-public crate-chainchomp-0.1.5 (c (n "chainchomp") (v "0.1.5") (h "159mnbw4nri4ch781yx8adm8g0cp998yw5yvlxw8dpn4wr2i4aix")))

(define-public crate-chainchomp-0.1.6 (c (n "chainchomp") (v "0.1.6") (h "0a0y430376ph37bmmgkh1vsk9lzapqcwjlwrhsl4f0scd0cgkkki")))

(define-public crate-chainchomp-0.2.0 (c (n "chainchomp") (v "0.2.0") (h "0rikjylgz15g49ga2zn0q1wbq4kll8fx4xhbb1g4kksyr80fnbsx")))

(define-public crate-chainchomp-0.2.1 (c (n "chainchomp") (v "0.2.1") (h "0mgidh9i86gjq2da2cw0rnkpyi4566n2ckdafdmin61v7l51jln0")))

