(define-module (crates-io ch ai chainkov) #:use-module (crates-io))

(define-public crate-chainkov-0.1.0 (c (n "chainkov") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0cq6kzd9f2djy2714bq4fxp3z9qwf9k9dkdigrcq4afbpfifmd13")))

(define-public crate-chainkov-1.0.0 (c (n "chainkov") (v "1.0.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1whkwkw48m268f2d518g0s3p2ilf8irzs6g09502s0yn3r9iqfj3") (y #t)))

(define-public crate-chainkov-0.2.0 (c (n "chainkov") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0bk87xzf2hwz0g2hracm5gghj1fhrvnshdpf0iph947fyv78i0ld")))

(define-public crate-chainkov-1.1.0 (c (n "chainkov") (v "1.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "10lb8l7fjhnm24mfmgz5sr1lamfbpn479548w7ypi1h1gkpr8pyd") (y #t)))

(define-public crate-chainkov-1.1.1 (c (n "chainkov") (v "1.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1ass5llalz6w7gpi2677sqbi5dbiv4qlw6wl9xaq6dbk49y8dy8h")))

(define-public crate-chainkov-1.1.2 (c (n "chainkov") (v "1.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0pighkqv7xghfp7hc635jhhsy1dlsxww6qprn6b2kchfqs262nfw")))

(define-public crate-chainkov-1.1.3 (c (n "chainkov") (v "1.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1312hi5jjl113p3jgpyg3qm2sm75yy59qhwywaq3aggcf0rcrjy1")))

