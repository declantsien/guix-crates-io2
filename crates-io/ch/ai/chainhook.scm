(define-module (crates-io ch ai chainhook) #:use-module (crates-io))

(define-public crate-chainhook-0.0.0 (c (n "chainhook") (v "0.0.0") (h "1k8ljyjqpx903ghcpdv4bipy4dfg3gl91n2jv73wps8y109hzijf")))

(define-public crate-chainhook-0.0.1 (c (n "chainhook") (v "0.0.1") (d (list (d (n "poem") (r "^3") (d #t) (k 0)) (d (n "poem-openapi") (r "^5.0.0") (f (quote ("swagger-ui" "websocket"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18xbcdk9lz4rwhjlyiyi1sb1m50a5xvp44jl0wyjlcx0s0dhjagl")))

