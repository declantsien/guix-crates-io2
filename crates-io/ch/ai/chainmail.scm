(define-module (crates-io ch ai chainmail) #:use-module (crates-io))

(define-public crate-chainmail-0.1.0 (c (n "chainmail") (v "0.1.0") (d (list (d (n "iron") (r "0.4.*") (d #t) (k 0)))) (h "0fggbqqpa2cymkqwpm0jrkdpn6pz9jnyxpcii1yz2lvc4nf7v3fz")))

(define-public crate-chainmail-0.2.0 (c (n "chainmail") (v "0.2.0") (d (list (d (n "iron") (r "0.4.*") (d #t) (k 0)))) (h "0v5xd8k80znqa34zxswcr37dd1mqkxsgjp7v1fwdq0jkr5ph9hxm")))

