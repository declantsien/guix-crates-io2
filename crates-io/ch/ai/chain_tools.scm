(define-module (crates-io ch ai chain_tools) #:use-module (crates-io))

(define-public crate-chain_tools-0.1.0 (c (n "chain_tools") (v "0.1.0") (h "10yyz377nwmmhqq6xmf9i0cq292zdbciyp5gc2z6s8gws2q3jiy7")))

(define-public crate-chain_tools-0.1.1 (c (n "chain_tools") (v "0.1.1") (h "0a71c56vq9l9nni1nymbscn62i5rpm73kymywnkf63sni661nk1j")))

(define-public crate-chain_tools-0.2.0 (c (n "chain_tools") (v "0.2.0") (h "1yqdqpppbfx2jqqsal56fa8l7x1fxqlm9pdhwi4pf74q3bss29pd")))

