(define-module (crates-io ch a- cha-rs) #:use-module (crates-io))

(define-public crate-cha-rs-0.0.1 (c (n "cha-rs") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "18nklkx63vrh4l83c8j44q4sls57a49zbcfh2hskh173phcp7wqy")))

(define-public crate-cha-rs-0.0.2 (c (n "cha-rs") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0680z3jpdj0rxqrbgsbqi5b46pjmgfv3pmw0agkb7zi1fzq4y22f")))

(define-public crate-cha-rs-0.0.3 (c (n "cha-rs") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0q18an2dpb2vzapzcn4fslfcp8p7fr1nip0fb3kyk21ibia75v3p")))

