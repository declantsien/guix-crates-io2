(define-module (crates-io ch ew chewing_capi) #:use-module (crates-io))

(define-public crate-chewing_capi-0.8.0-beta.1 (c (n "chewing_capi") (v "0.8.0-beta.1") (d (list (d (n "chewing") (r "^0.8.0-beta.1") (d #t) (k 0)) (d (n "env_logger") (r ">=0.10.2") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1gwwwfp3w9r4i7m8hcyilswwxz1mirlw7zsly79w9ipxba7v0ha6") (f (quote (("sqlite" "chewing/sqlite")))) (r "1.70")))

(define-public crate-chewing_capi-0.8.0-beta.2 (c (n "chewing_capi") (v "0.8.0-beta.2") (d (list (d (n "chewing") (r "^0.8.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r ">=0.10.2") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "16d8cm6wph1vsvs1hmxx61ggbd6n8fj5aa0jwadsa4cy6zkgr3bi") (f (quote (("sqlite" "chewing/sqlite")))) (r "1.70")))

(define-public crate-chewing_capi-0.8.0-beta.3 (c (n "chewing_capi") (v "0.8.0-beta.3") (d (list (d (n "chewing") (r "^0.8.0-beta.3") (d #t) (k 0)) (d (n "env_logger") (r ">=0.10.2") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "0i32z623c0ynbyrgz2ha76r0fi5hpjmqikdyshidkgvh0gq9bbwp") (f (quote (("sqlite" "chewing/sqlite")))) (r "1.70")))

(define-public crate-chewing_capi-0.8.0-beta.4 (c (n "chewing_capi") (v "0.8.0-beta.4") (d (list (d (n "chewing") (r "^0.8.0-beta.4") (d #t) (k 0)) (d (n "env_logger") (r ">=0.10.2") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1bj9z8jy9b50l47qf5aiy4fr0bdsmap7f1r3wcfkgrwnv3f1fqwl") (f (quote (("sqlite" "chewing/sqlite")))) (r "1.70")))

(define-public crate-chewing_capi-0.8.0 (c (n "chewing_capi") (v "0.8.0") (d (list (d (n "chewing") (r "^0.8.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.10.2") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "0ybcgmgx33v1y222xakc8w01qnb61z2cnjq6g2x9yk048xsnk2sz") (f (quote (("sqlite" "chewing/sqlite")))) (r "1.70")))

(define-public crate-chewing_capi-0.8.1 (c (n "chewing_capi") (v "0.8.1") (d (list (d (n "chewing") (r "^0.8.1") (d #t) (k 0)) (d (n "env_logger") (r ">=0.10.2") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1k9fd7sp96fcahaghhmy8kqbaziy3w7lqc50g75gywrkma3nfanw") (f (quote (("sqlite" "chewing/sqlite")))) (r "1.70")))

(define-public crate-chewing_capi-0.8.2 (c (n "chewing_capi") (v "0.8.2") (d (list (d (n "chewing") (r "^0.8.2") (d #t) (k 0)) (d (n "env_logger") (r ">=0.10.2") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "0hzsqxrgp7zdnpwwpprs7gk1pb6mq9bf5z5syby68hfqqa2a3m49") (f (quote (("sqlite" "chewing/sqlite")))) (r "1.70")))

(define-public crate-chewing_capi-0.8.3 (c (n "chewing_capi") (v "0.8.3") (d (list (d (n "chewing") (r "^0.8.3") (d #t) (k 0)) (d (n "env_logger") (r ">=0.10.2") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1lbcws6vk0b7zlf5a5qkbkzbgdrfgq3g0jw32692sfz2vzx7y989") (f (quote (("sqlite" "chewing/sqlite")))) (r "1.70")))

