(define-module (crates-io ch ew chewing-cli) #:use-module (crates-io))

(define-public crate-chewing-cli-0.7.0-beta.1 (c (n "chewing-cli") (v "0.7.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.7.0-beta.1") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "xflags") (r "^0.3.2") (d #t) (k 0)))) (h "0kwhql15q0pk40gghh3yrjfq27gm1xzhwmsyr70y2qn8h217vmkm")))

(define-public crate-chewing-cli-0.7.0-beta.2 (c (n "chewing-cli") (v "0.7.0-beta.2") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.7.0-beta.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "xflags") (r "^0.3.2") (d #t) (k 0)))) (h "1bysn09pakm6p1gjm1kh7rnnihyfxq6f5bd2a9xr0ddaw0pjpp1j")))

(define-public crate-chewing-cli-0.7.0-beta.3 (c (n "chewing-cli") (v "0.7.0-beta.3") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.7.0-beta.3") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "xflags") (r "^0.3.2") (d #t) (k 0)))) (h "0j9r9826wpkhva86d2dgkljp49i2hd6zglf5x6gjkx0ss4x3lpcw")))

(define-public crate-chewing-cli-0.8.0-beta.2 (c (n "chewing-cli") (v "0.8.0-beta.2") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.8.0-beta.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "xflags") (r "^0.3.2") (d #t) (k 0)))) (h "0cgjpz4lk8mld6q7mjwv8zqrmbkr3g6bj7i0h0gp66q8i6xp0337")))

(define-public crate-chewing-cli-0.8.0-beta.3 (c (n "chewing-cli") (v "0.8.0-beta.3") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.8.0-beta.3") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "clap") (r ">=4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.20") (o #t) (d #t) (k 0)))) (h "0acwb9yfbv4c7x4hdyj7gviq9kx2xf97xyygli3jnb59c4kck5vn") (s 2) (e (quote (("mangen" "dep:clap_mangen"))))))

(define-public crate-chewing-cli-0.8.0-beta.4 (c (n "chewing-cli") (v "0.8.0-beta.4") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.8.0-beta.4") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "clap") (r ">=4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.20") (o #t) (d #t) (k 0)))) (h "1cfbcy7549a5br2w4rk8xs775lh9k243awmvq0dcfmcfjksmh8cc") (s 2) (e (quote (("mangen" "dep:clap_mangen"))))))

(define-public crate-chewing-cli-0.8.0 (c (n "chewing-cli") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.8.0") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "0ilb6rndqnkkd22gl7g31vpq399wapp14m3c34rb7zmg19ny2j8l") (s 2) (e (quote (("mangen" "dep:clap_mangen"))))))

(define-public crate-chewing-cli-0.8.1 (c (n "chewing-cli") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.8.1") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "01hdqicrlzldwhi214mjvl7qb68fm19bazz665arpzh06fl67ph6") (s 2) (e (quote (("mangen" "dep:clap_mangen"))))))

(define-public crate-chewing-cli-0.8.2 (c (n "chewing-cli") (v "0.8.2") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.8.2") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "04fqvzzbng2yca8ihjwzf1hj9yxbj4k28xrl0sdzxd2pqjx5bwyc") (s 2) (e (quote (("mangen" "dep:clap_mangen"))))))

(define-public crate-chewing-cli-0.8.3 (c (n "chewing-cli") (v "0.8.3") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chewing") (r "^0.8.3") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "0qj5ykj3vmdxw2kpm1i2z6rsk3lvlvwb44xifc77i3j776mirmp2") (s 2) (e (quote (("mangen" "dep:clap_mangen"))))))

