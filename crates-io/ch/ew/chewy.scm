(define-module (crates-io ch ew chewy) #:use-module (crates-io))

(define-public crate-chewy-1.0.0 (c (n "chewy") (v "1.0.0") (h "12iq1cmfvyfijavp4rjx2j8c1bn2wbwbmvzhkjxfq5401iql0b8l")))

(define-public crate-chewy-2.0.0 (c (n "chewy") (v "2.0.0") (h "1xwp4kd1pcirjilxppqm64x54nbrdm9l9rpc3d8r2wm9rdmczqnd")))

(define-public crate-chewy-3.0.1 (c (n "chewy") (v "3.0.1") (h "1x9hlfsnslgz37ri7diiksia7lnp7sq4yh7fhzsym6dd9ds3a2d0")))

