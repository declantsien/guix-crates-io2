(define-module (crates-io ch ew chewing-rs) #:use-module (crates-io))

(define-public crate-chewing-rs-0.1.0 (c (n "chewing-rs") (v "0.1.0") (d (list (d (n "chewing-sys") (r "^0.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "0hbksz62i0b16yjq3imyijdnhc66wwa0zspjdd1b28l6hj519i85")))

(define-public crate-chewing-rs-0.1.1 (c (n "chewing-rs") (v "0.1.1") (d (list (d (n "chewing-sys") (r "^0.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "1vgs7py5zryam6pcpx2rslgwp442a9dvz3brkz9ril719dyd1kl7")))

(define-public crate-chewing-rs-0.1.2 (c (n "chewing-rs") (v "0.1.2") (d (list (d (n "chewing-sys") (r "^0.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "0785sjx1fyhj5a6x93xbygq7dss7kgzn50b72y0pp3x5qsmh570q")))

(define-public crate-chewing-rs-0.1.3 (c (n "chewing-rs") (v "0.1.3") (d (list (d (n "chewing-sys") (r "^0.2.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "1iyz37h2v6ka2ifxzg5w5bkzy4i4v143d5pqjcda34arxv0df1c7")))

