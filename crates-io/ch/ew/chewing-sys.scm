(define-module (crates-io ch ew chewing-sys) #:use-module (crates-io))

(define-public crate-chewing-sys-0.1.0 (c (n "chewing-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)))) (h "15dm06nhvs8rf7qw84hgmbywk90ph0yp1izwq4ls8xs7gi5l46hi") (l "chewing")))

(define-public crate-chewing-sys-0.2.0 (c (n "chewing-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1mk9i84mqi5w1l6abw28rjn4mggwlmrvm38067q50ys6zkn0f5w2") (l "chewing")))

(define-public crate-chewing-sys-0.2.1 (c (n "chewing-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0d17zb8ai8hvmcyg1n198jbrq8r9i5q1pxc29g9d1r7664gz58wv") (l "chewing")))

(define-public crate-chewing-sys-0.2.2 (c (n "chewing-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1g28gr0rk3dfs2vn190i959jrj09hz6nwxrvvr84m1ix8dw6i64n") (l "chewing")))

(define-public crate-chewing-sys-0.2.3 (c (n "chewing-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1lds652zfws9ym2q8lp9f0w57jdp6ablb7803k3pxlgvj401d3nf") (l "chewing")))

(define-public crate-chewing-sys-0.2.4 (c (n "chewing-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0vh90zvracnpbni9941y96vkq008lvpfisyx6d9rrf5yk5g5zg5k") (l "chewing")))

(define-public crate-chewing-sys-0.2.5 (c (n "chewing-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "17qdds1n8i4vwbvha1l4f37fbw1ccm7mwwfw1a3640y5w8fzdxi6") (l "chewing")))

