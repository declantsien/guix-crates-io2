(define-module (crates-io ch im chimera) #:use-module (crates-io))

(define-public crate-chimera-0.1.0 (c (n "chimera") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1lc0aqjvi8g35ik6xxz5rwxqv697m0lfgfh55mkc84kv5brzbb87")))

