(define-module (crates-io ch im chime-flux-proc-macro) #:use-module (crates-io))

(define-public crate-chime-flux-proc-macro-0.1.0 (c (n "chime-flux-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1a2hby1kp22xlf7rhvkabivcnr6fv212kq467v1nb59p5ajb1hvl")))

(define-public crate-chime-flux-proc-macro-0.2.0 (c (n "chime-flux-proc-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "19cq600c8izb4fiqsbgpx1nq4i7hrn0iyr60858hs0z9dnbi92w1")))

(define-public crate-chime-flux-proc-macro-0.3.0 (c (n "chime-flux-proc-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "06qal2xp07z380n7qxyamc2bqr7mcfhmz8986hx4sm7sa2hfnx7b")))

