(define-module (crates-io ch im chimney) #:use-module (crates-io))

(define-public crate-chimney-0.1.0 (c (n "chimney") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "indradb-lib") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0715d2fl9nh2rxcw87vfyrlrizvnjhxa8h0pk1yx6j0aibcsa3z0")))

