(define-module (crates-io ch im chimney-resources) #:use-module (crates-io))

(define-public crate-chimney-resources-0.1.0 (c (n "chimney-resources") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "prost") (r "^0.11") (f (quote ("prost-derive"))) (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "prost-wkt") (r "^0.4") (d #t) (k 0)) (d (n "prost-wkt-build") (r "^0.4") (d #t) (k 1)) (d (n "prost-wkt-types") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v74vjixnbg8q62pcifqq6s0xzy4097f3zfvjgz6xfy5mxf3qcpc")))

