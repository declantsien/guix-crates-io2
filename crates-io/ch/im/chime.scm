(define-module (crates-io ch im chime) #:use-module (crates-io))

(define-public crate-chime-0.1.0 (c (n "chime") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)))) (h "1xbfidkyjqvynqdsyp72l70kx1w4nsj6v88c0q7a9mx6y9nvbvl9") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy"))))))

(define-public crate-chime-0.2.0 (c (n "chime") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)))) (h "0jsnyijsg0w0kqd7y5wdl452ai847sig47n41vbf6dyw7bx86nhh") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy"))))))

(define-public crate-chime-0.3.0 (c (n "chime") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)))) (h "1wpl2szdjd1k6jazy7481ncnk15mip86lgg9xzrp882111gilhwz") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy"))))))

(define-public crate-chime-0.3.1 (c (n "chime") (v "0.3.1") (d (list (d (n "bevy_ecs") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)))) (h "1asapix2gbhyigacc13dfq4gbqsxl7cml76pn0yalc9lc09ypl8b") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy_ecs" "dep:glam"))))))

(define-public crate-chime-0.3.2 (c (n "chime") (v "0.3.2") (d (list (d (n "bevy_ecs") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)))) (h "0zzizkc4jm8yyignyiwv7m7317gnpvmavygqavvaxiyaww2vhnnf") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy_ecs" "glam"))))))

(define-public crate-chime-0.3.3 (c (n "chime") (v "0.3.3") (d (list (d (n "bevy_ecs") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)))) (h "08vq76pr9xzh2jvv4fpk0rh1ijvqah25b7lp4ja20v02bfararx6") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy_ecs" "glam"))))))

(define-public crate-chime-0.3.4 (c (n "chime") (v "0.3.4") (d (list (d (n "bevy_ecs") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)))) (h "1lc7iqc6bn48hzlafrji1ijd7pws4npna4qw2zy5rnxy97i7hx8x") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy_ecs" "glam"))))))

(define-public crate-chime-0.3.5 (c (n "chime") (v "0.3.5") (d (list (d (n "bevy_ecs") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (o #t) (d #t) (k 0)))) (h "0dxcbbzi366izf5bm1k61w8mgqkdgxrwy967in94qss0gdy82zf9") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy_ecs" "glam"))))))

(define-public crate-chime-0.4.0 (c (n "chime") (v "0.4.0") (d (list (d (n "bevy_ecs") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "1k36g66bfb4x9cmdgn3ap68sv4svh9yp7jnad2hf0ciklfp6q8rs") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy_ecs" "glam"))))))

(define-public crate-chime-0.5.0 (c (n "chime") (v "0.5.0") (d (list (d (n "bevy_ecs") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "1jngwxfn43zw04p7mady29cfxhf71jmaylp6nlmagfqbhb85q74a") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy_ecs" "glam"))))))

(define-public crate-chime-0.6.0 (c (n "chime") (v "0.6.0") (d (list (d (n "bevy_ecs") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "chime-flux-proc-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "0632r36vnxiad481dsrl2np2xvyxcfhky755cqjdzk52l55v00cs") (s 2) (e (quote (("glam" "dep:glam") ("bevy" "dep:bevy_ecs" "glam"))))))

