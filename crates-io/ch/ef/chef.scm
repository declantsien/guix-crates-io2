(define-module (crates-io ch ef chef) #:use-module (crates-io))

(define-public crate-chef-0.1.0 (c (n "chef") (v "0.1.0") (d (list (d (n "chef_api") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m95wf8sz8j6cvq56q3pq9b3lwablvrabgc31pas3h3qifjxyf7r") (f (quote (("dev" "clippy") ("default"))))))

