(define-module (crates-io ch ef chef-rs) #:use-module (crates-io))

(define-public crate-chef-rs-0.1.0 (c (n "chef-rs") (v "0.1.0") (h "1c71491vijd0gvyn9ib0865kphzc9l0gkzxq5nvxi1qmsmkb9lqc") (y #t)))

(define-public crate-chef-rs-0.1.1 (c (n "chef-rs") (v "0.1.1") (h "0gzgpslhddad9r2nwr9kz94jfzw9y98dq6rmazgadbp2qyws9b74") (y #t)))

