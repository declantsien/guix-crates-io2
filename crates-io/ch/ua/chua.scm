(define-module (crates-io ch ua chua) #:use-module (crates-io))

(define-public crate-chua-0.1.0 (c (n "chua") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rrsp5x7kzsjpfh4ndc6jbrxy50zslga4adj51za3d4dfngi7dgd")))

