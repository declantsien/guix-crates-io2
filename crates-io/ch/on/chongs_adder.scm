(define-module (crates-io ch on chongs_adder) #:use-module (crates-io))

(define-public crate-chongs_adder-0.1.0 (c (n "chongs_adder") (v "0.1.0") (h "14rry4qp8bm1z05d5sz8gqqqz9wxcxva77gy7kmdh2kfnbqbmq8k")))

(define-public crate-chongs_adder-0.2.0 (c (n "chongs_adder") (v "0.2.0") (h "07gycsx8s2kdyj99gc68ldd4fzjc2naiwzd2zrnghbwg0qrgwfqs")))

