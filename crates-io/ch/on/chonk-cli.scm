(define-module (crates-io ch on chonk-cli) #:use-module (crates-io))

(define-public crate-chonk-cli-0.1.0 (c (n "chonk-cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "195b65bny28w5qw0ghwimff7px3963ldigj3gcfj4kpm2jn3yg54")))

(define-public crate-chonk-cli-0.1.1 (c (n "chonk-cli") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1hzm623q1b0pg1ldndrvz33rwv0kf0sc0s61ws9y0s8gi63k5gjv")))

