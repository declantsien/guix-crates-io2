(define-module (crates-io ch on chonk) #:use-module (crates-io))

(define-public crate-chonk-0.1.0 (c (n "chonk") (v "0.1.0") (h "0npc0s7f5r9rq96xjn26s9zchv40jypx74n8l7xhv9kz0zxsfznk")))

(define-public crate-chonk-0.2.0 (c (n "chonk") (v "0.2.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "04vc467iwgmslfbri0h3rxymsygpp0ky0babr0xcfi8ggnwb16ym")))

(define-public crate-chonk-0.3.0 (c (n "chonk") (v "0.3.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "14pq8r1m1z1bls2nqwc8bi9n30598wjysiqjldhjb2cpz7n091j4")))

(define-public crate-chonk-0.3.1 (c (n "chonk") (v "0.3.1") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1v5wapjmavw802p3gdj5il85yn4jxk1419pxmbg13vda8lmjbxg2")))

(define-public crate-chonk-0.4.0 (c (n "chonk") (v "0.4.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1pqdhr5kspwadz0vhhyjddn6ya03x1gnl97392qy5byfw4s4j82z")))

(define-public crate-chonk-0.5.0 (c (n "chonk") (v "0.5.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0pkpprz9mrax5340hay53a9jfvzcppsj1gk7zl4z0wwwwilpjpc3")))

