(define-module (crates-io lq th lqth) #:use-module (crates-io))

(define-public crate-lqth-0.1.0 (c (n "lqth") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "x11") (r "^2.21") (f (quote ("xlib"))) (k 0)))) (h "0f8qhg4v0i7z1i576cff9i60w2fliq0rrri1mgi9a5xm9rjy9wh3") (r "1.65.0")))

(define-public crate-lqth-0.1.1 (c (n "lqth") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)) (d (n "x11") (r "^2.21") (f (quote ("xlib"))) (k 0)))) (h "0w7mfx5c15ci1cjjq051in5mis3z6qml9ckimimrlm3nzxc3v5jd") (r "1.65.0")))

(define-public crate-lqth-0.1.2 (c (n "lqth") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)) (d (n "x11") (r "^2.21") (f (quote ("xlib"))) (k 0)))) (h "0aya7yjl69qmz1fvmiy8a4li2cp17jn43vinpy1pbms23v6plkmn") (r "1.65.0")))

(define-public crate-lqth-0.1.3 (c (n "lqth") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)) (d (n "x11") (r "^2.21") (f (quote ("xlib"))) (k 0)))) (h "0x3bm81gvwx483vn4d5wxf0mn7rddwqm35ppqad00rcjissn0h2d") (r "1.65.0")))

(define-public crate-lqth-0.2.0 (c (n "lqth") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)) (d (n "x11") (r "^2.21") (f (quote ("xlib"))) (k 0)))) (h "0nyxghiw4934hsjhjmbwghkpm166lfpni244dj06i8wdk5yv206n") (y #t) (r "1.65.0")))

(define-public crate-lqth-0.2.1 (c (n "lqth") (v "0.2.1") (d (list (d (n "thiserror") (r ">=1.0.2") (d #t) (k 0)) (d (n "x11") (r "^2.21") (f (quote ("xlib"))) (k 0)))) (h "02r5sw4cisnkvhssw501n1f1008magcgvwr238692a1lccxix606") (r "1.72.1")))

(define-public crate-lqth-0.2.2 (c (n "lqth") (v "0.2.2") (d (list (d (n "thiserror") (r ">=1.0.2") (d #t) (k 0)) (d (n "x11") (r "^2.21") (f (quote ("xlib"))) (k 0)))) (h "06vsd6m14kjh7xs3nks2zjp48f4f95giyvnvgw4r6izrivqfsll4") (r "1.72.1")))

(define-public crate-lqth-0.2.3-alpha.1 (c (n "lqth") (v "0.2.3-alpha.1") (d (list (d (n "thiserror") (r ">=1.0.2") (d #t) (k 0)) (d (n "x11") (r "^2.21") (f (quote ("xlib"))) (k 0)))) (h "0zxcsr9hy4kr8i7y500czidbdlbi1zx23a376ls3viqhbab0jwbi") (r "1.72.1")))

