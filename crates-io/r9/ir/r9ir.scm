(define-module (crates-io r9 ir r9ir) #:use-module (crates-io))

(define-public crate-r9ir-0.1.1 (c (n "r9ir") (v "0.1.1") (h "0wbs8cpsgjy5dkv9931ym5p0npdf4fq14dg4qw6mymgwsfmg8w1a") (y #t)))

(define-public crate-r9ir-1.0.0 (c (n "r9ir") (v "1.0.0") (h "05wpfsx9qkyw34q0h4mz4l16apl65cwc0w6syc6f1bs2bzw6qv6w")))

