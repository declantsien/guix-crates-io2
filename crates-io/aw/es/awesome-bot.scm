(define-module (crates-io aw es awesome-bot) #:use-module (crates-io))

(define-public crate-awesome-bot-0.1.0 (c (n "awesome-bot") (v "0.1.0") (d (list (d (n "regex") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "rustc_version") (r "~0.1") (d #t) (k 1)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 0)) (d (n "telegram-bot") (r "0.3.*") (d #t) (k 0)))) (h "12h6jk60l0sn77pzypddy4w1w6w1pg28viycd0xq73rls7wf1mpm")))

(define-public crate-awesome-bot-0.2.0 (c (n "awesome-bot") (v "0.2.0") (d (list (d (n "regex") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 0)) (d (n "telegram-bot") (r "^0.4.2") (d #t) (k 0)))) (h "0n84gm337khzasc8ssadq2a6938ycpp8a73djcxa2w4qmxwf47kr")))

