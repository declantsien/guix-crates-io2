(define-module (crates-io aw es awesome-glib) #:use-module (crates-io))

(define-public crate-awesome-glib-0.1.0 (c (n "awesome-glib") (v "0.1.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "gio") (r "^0.14") (d #t) (k 2)) (d (n "glib") (r "^0.14") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11qfajci9rbqiifx5y1yirs5l3ng13ifrjqbafzf735nxslm3yxa")))

(define-public crate-awesome-glib-0.1.1 (c (n "awesome-glib") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "gio") (r "^0.14") (d #t) (k 2)) (d (n "glib") (r "^0.14") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qg9za34vdpgr7kwhm9dkk10jp22vw2iknihrzfym83lwj8by0xl")))

(define-public crate-awesome-glib-0.2.0 (c (n "awesome-glib") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "gio") (r "^0.17") (d #t) (k 2)) (d (n "glib") (r "^0.17") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yqpcrissarfc6zxi4s48h5ka8alp79lil9f33svrv30lqdjln8g")))

(define-public crate-awesome-glib-0.2.1 (c (n "awesome-glib") (v "0.2.1") (d (list (d (n "gio") (r "^0.17") (d #t) (k 2)) (d (n "glib") (r "^0.17") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vrjwslcdfnym5kqmvhcqc4jxgki35vqb2pw0hfh3v04sqp99vmd")))

(define-public crate-awesome-glib-0.3.0 (c (n "awesome-glib") (v "0.3.0") (d (list (d (n "gio") (r "^0.18") (d #t) (k 2)) (d (n "glib") (r "^0.18") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "070w0f0m7db438wq789lcx09415p04kahbf08xv08qmbczp0sd97")))

(define-public crate-awesome-glib-0.4.0 (c (n "awesome-glib") (v "0.4.0") (d (list (d (n "gio") (r "^0.19") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "04k3ga03bjnykbqbwj328ilb6lab3wd1ldn3arlvsnaz8l5bx3mm")))

