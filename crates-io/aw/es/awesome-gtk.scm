(define-module (crates-io aw es awesome-gtk) #:use-module (crates-io))

(define-public crate-awesome-gtk-0.1.0 (c (n "awesome-gtk") (v "0.1.0") (d (list (d (n "gtk") (r "^0.6") (d #t) (k 0) (p "gtk4")))) (h "1bizsbqw25zcizygm3psgg02cqp0d20mxj3dq0ha1wbgkdq5ksqa")))

(define-public crate-awesome-gtk-0.2.0 (c (n "awesome-gtk") (v "0.2.0") (d (list (d (n "gtk") (r "^0.6") (d #t) (k 0) (p "gtk4")))) (h "052y9fpzsg5s39bk6qx2fmm8w8s5h2z363sv7c2cd46ba5idr975")))

(define-public crate-awesome-gtk-0.3.0 (c (n "awesome-gtk") (v "0.3.0") (d (list (d (n "gtk") (r "^0.7") (d #t) (k 0) (p "gtk4")))) (h "1pslgds5aky2argvlpy2sy10ixw861mfcjf11ba1l5332kcw0n1z")))

(define-public crate-awesome-gtk-0.4.0 (c (n "awesome-gtk") (v "0.4.0") (d (list (d (n "gtk") (r "^0.8") (d #t) (k 0) (p "gtk4")))) (h "1w29rsirip2rj3sbd9y31saz7hlnfnzvm5610ngf2l6m4p03bk76")))

