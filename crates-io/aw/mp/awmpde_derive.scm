(define-module (crates-io aw mp awmpde_derive) #:use-module (crates-io))

(define-public crate-awmpde_derive-0.1.0 (c (n "awmpde_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rvi1kn1pfxzsbjpbvdzn3b5ga2hv3k768v6f48s5iff2y05hy8h")))

(define-public crate-awmpde_derive-0.2.0 (c (n "awmpde_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1prv8z8afapx6x550yypd1lzdazrn6paiws75lqzqixwfjf6szzy")))

(define-public crate-awmpde_derive-0.2.1 (c (n "awmpde_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1kk4mrdrbn70ql1hhspz6lb6254pk3063bi4l7q9y5azj7gkghic")))

(define-public crate-awmpde_derive-0.2.2 (c (n "awmpde_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jhp290ajfn9y0h9xlsw1rvvcmlhm8hd05b7rb0iyzfzm0rr64jc")))

(define-public crate-awmpde_derive-0.3.0 (c (n "awmpde_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0r360w1yv5npf926gm2d9n0hz50jxaf36h0iwgmwba5l378iv9n6")))

(define-public crate-awmpde_derive-0.4.0 (c (n "awmpde_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hg51gzl1dyl7jy0x1y52b2rnznp3qrxcswpzvip72x2vlyy2wp4")))

(define-public crate-awmpde_derive-0.7.0 (c (n "awmpde_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ibvnjyl5hl5z24l7vgiyiigk6kkr55sz4pd4raxsbwj1rjm3z3d")))

