(define-module (crates-io aw -s aw-sync) #:use-module (crates-io))

(define-public crate-aw-sync-0.1.0 (c (n "aw-sync") (v "0.1.0") (d (list (d (n "aw-client-rust") (r "^0.1") (d #t) (k 0)) (d (n "aw-datastore") (r "^0.1") (d #t) (k 0)) (d (n "aw-models") (r "^0.1") (d #t) (k 0)) (d (n "aw-server") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ncnfnfkg2vv0ycygx8qah9vpdk9hn6kjfwhfhcv4llzx79rkc30")))

