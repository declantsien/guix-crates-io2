(define-module (crates-io aw -s aw-soc) #:use-module (crates-io))

(define-public crate-aw-soc-0.0.0 (c (n "aw-soc") (v "0.0.0") (d (list (d (n "base-address") (r "^0.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "embedded-hal-nb") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "plic") (r "^0.0.2") (d #t) (k 0)) (d (n "uart16550") (r "^0.0.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.8") (d #t) (k 2)))) (h "0ks414svsy30daw6g8i8zbl7bpyh8iwchwzgk80m6xqy74p8zwrn") (f (quote (("d1"))))))

