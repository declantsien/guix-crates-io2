(define-module (crates-io aw f- awf-help) #:use-module (crates-io))

(define-public crate-awf-help-0.1.0 (c (n "awf-help") (v "0.1.0") (d (list (d (n "actix-web") (r "^1.0") (d #t) (k 0)) (d (n "awf-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)))) (h "1w4vpp5cggrsxjqh2892ddldvxfb34gikycf66skplmrijk336mn")))

(define-public crate-awf-help-0.1.1 (c (n "awf-help") (v "0.1.1") (d (list (d (n "actix-web") (r "^1.0") (d #t) (k 0)) (d (n "awf-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)))) (h "1di35qlw1b7j2q2sjq675lcla9a9461zkdf9pvnda14nsp8cmq34")))

(define-public crate-awf-help-0.1.2 (c (n "awf-help") (v "0.1.2") (d (list (d (n "actix-web") (r "^1.0") (d #t) (k 0)) (d (n "awf-codegen") (r "^0.1.2") (d #t) (k 0)) (d (n "inventory") (r "^0.1") (d #t) (k 0)))) (h "1m0p2c76vn334hwkibkh313vi0s3v6bjhb4jhxnlycabxmfnl1r3")))

