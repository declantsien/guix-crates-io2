(define-module (crates-io aw f- awf-codegen) #:use-module (crates-io))

(define-public crate-awf-codegen-0.1.0 (c (n "awf-codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19009kxfpvj0fpgcwnf2848ghqnd8qnjrfsl5qkqckdhj0r76mb8")))

(define-public crate-awf-codegen-0.1.1 (c (n "awf-codegen") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13sv3rvsmzvky9mri3a4mf8qznmvpgp5n350012h213381a6fxv8")))

(define-public crate-awf-codegen-0.1.2 (c (n "awf-codegen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m5j5lb3qg0pq1gv448pxlksfywb15vz64l2cnv2g4kjlcmnwfkw")))

