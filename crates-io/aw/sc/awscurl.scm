(define-module (crates-io aw sc awscurl) #:use-module (crates-io))

(define-public crate-awscurl-0.1.0 (c (n "awscurl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "sha256") (r "^1.1.2") (d #t) (k 0)) (d (n "tungstenite") (r "^0.18.0") (f (quote ("rustls-tls-webpki-roots"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0iyfbzg39fwslhrnvyjmbliv2nddwjqnixb5k7bz65jfm4wzcqsc")))

