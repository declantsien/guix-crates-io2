(define-module (crates-io aw sc awscli-pull) #:use-module (crates-io))

(define-public crate-awscli-pull-0.1.0 (c (n "awscli-pull") (v "0.1.0") (d (list (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b4a95s291crvlnfy9f1d6q5hxn5svgf1lq26a8svc33bs0vhmh9")))

