(define-module (crates-io aw c- awc-uds) #:use-module (crates-io))

(define-public crate-awc-uds-0.1.0 (c (n "awc-uds") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.2") (d #t) (k 0)) (d (n "actix-service") (r "^2.0.1") (d #t) (k 0)) (d (n "actix-tls") (r "^3.0.0-beta.8") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.9") (d #t) (k 2)) (d (n "awc") (r "^3.0.0-beta.9") (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "13xk17alh10ha9m1p3gxdhcwwaxaxsnslid1grrdyz2yhkv9mn2i")))

(define-public crate-awc-uds-0.1.1 (c (n "awc-uds") (v "0.1.1") (d (list (d (n "actix-rt") (r "^2.2") (d #t) (k 0)) (d (n "actix-service") (r "^2.0.2") (d #t) (k 0)) (d (n "actix-tls") (r "^3.0.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.21") (d #t) (k 2)) (d (n "awc") (r "^3.0.0-beta.19") (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1rviv21pbif550vy5zp0kijf2qb03s14cwnnklzm9ja7x76hi4jk")))

