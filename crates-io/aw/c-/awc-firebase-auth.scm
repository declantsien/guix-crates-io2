(define-module (crates-io aw c- awc-firebase-auth) #:use-module (crates-io))

(define-public crate-awc-firebase-auth-0.1.0 (c (n "awc-firebase-auth") (v "0.1.0") (d (list (d (n "awc") (r "^3.0.0") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "1mmd5j0h61qa6n13n7mgd3wfa59m5kmw7q496rz8965wn9cdnhj0")))

