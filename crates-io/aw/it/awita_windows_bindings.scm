(define-module (crates-io aw it awita_windows_bindings) #:use-module (crates-io))

(define-public crate-awita_windows_bindings-0.0.1 (c (n "awita_windows_bindings") (v "0.0.1") (d (list (d (n "windows") (r "^0.18.0") (d #t) (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (k 1)))) (h "17m32frz6cmzdwdmkyyghcnag6d9kfa0zyrm1kdfhh1s87pmzm1k")))

(define-public crate-awita_windows_bindings-0.0.2 (c (n "awita_windows_bindings") (v "0.0.2") (d (list (d (n "windows") (r "^0.18.0") (d #t) (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (k 1)))) (h "1s167ilgfphlhxds33k7v8qh4hzimwkqj1ny17x3wzx2spg3izb8")))

(define-public crate-awita_windows_bindings-0.0.3 (c (n "awita_windows_bindings") (v "0.0.3") (d (list (d (n "windows") (r "^0.19.0") (d #t) (k 0)) (d (n "windows") (r "^0.19.0") (d #t) (k 1)))) (h "037x3g3mcr1m4g3c20zjj2y77z8p5vqngggvyi3k8v6fzzyxla0f")))

(define-public crate-awita_windows_bindings-0.0.4 (c (n "awita_windows_bindings") (v "0.0.4") (d (list (d (n "windows") (r "^0.20.0") (d #t) (k 0)) (d (n "windows") (r "^0.20.0") (d #t) (k 1)))) (h "0mbcv7lhkq3g626hr78jsfa0wlwz847zn3457r4ww8r1wi73x9ap")))

(define-public crate-awita_windows_bindings-0.1.0 (c (n "awita_windows_bindings") (v "0.1.0") (d (list (d (n "windows") (r "^0.21.0") (d #t) (k 0)) (d (n "windows") (r "^0.21.0") (d #t) (k 1)))) (h "1pz9mpxvzsr48x87yxm859rkw6rvd8dh35jnwq8x2l7wxdck3x5k")))

