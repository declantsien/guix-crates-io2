(define-module (crates-io aw ed awedio_esp32) #:use-module (crates-io))

(define-public crate-awedio_esp32-0.1.0 (c (n "awedio_esp32") (v "0.1.0") (d (list (d (n "awedio") (r "^0.1") (k 0)) (d (n "esp-idf-sys") (r "^0.32") (f (quote ("native"))) (k 0)))) (h "0mxwy8j4wk77hafgxkkgvv18xlp0wi97954zq6ncivkrf6an9xh4")))

(define-public crate-awedio_esp32-0.1.1 (c (n "awedio_esp32") (v "0.1.1") (d (list (d (n "awedio") (r "^0.1") (k 0)) (d (n "esp-idf-sys") (r "^0.32") (f (quote ("native"))) (k 0)))) (h "0jvv99fhclb08na7mblfnb33fpslxysi1kngjlnpi5byx3qcsnpd")))

(define-public crate-awedio_esp32-0.1.2 (c (n "awedio_esp32") (v "0.1.2") (d (list (d (n "awedio") (r "^0.1") (k 0)) (d (n "esp-idf-sys") (r "^0.32") (f (quote ("native"))) (k 0)))) (h "1235ph8hk04p6di5dkq66qh8psfpnzqkb0j7g8n97c7lwajd113d")))

(define-public crate-awedio_esp32-0.2.0 (c (n "awedio_esp32") (v "0.2.0") (d (list (d (n "awedio") (r "^0.2") (k 0)) (d (n "esp-idf-sys") (r "^0.32") (f (quote ("native"))) (k 0)))) (h "09yn4kp45vbbq2l6hygf99r0fmv7my63jr84s302r8cg2nch8l23")))

(define-public crate-awedio_esp32-0.3.0 (c (n "awedio_esp32") (v "0.3.0") (d (list (d (n "awedio") (r "^0.2") (k 0)) (d (n "esp-idf-hal") (r "^0.42") (f (quote ("native"))) (k 0)))) (h "1yvrda360nba8anrrbcvjgcg2y4hnqy3rv5cwnlmygk3hkdqn59y") (f (quote (("report-render-time"))))))

(define-public crate-awedio_esp32-0.4.0 (c (n "awedio_esp32") (v "0.4.0") (d (list (d (n "awedio") (r "^0.3.1") (k 0)) (d (n "esp-idf-hal") (r "^0.42") (f (quote ("native"))) (k 0)))) (h "0w037i0m25rw2p6m0lnqrfx4q9ssa2xghyf8332x20whiqg5dc1f") (f (quote (("report-render-time"))))))

(define-public crate-awedio_esp32-0.4.1 (c (n "awedio_esp32") (v "0.4.1") (d (list (d (n "awedio") (r "^0.3.1") (k 0)) (d (n "esp-idf-hal") (r "^0.42") (f (quote ("native"))) (k 0)))) (h "1cz0na867dc7s7qbqa41sx282ffh6i9lkgq26lin28h8mh61r9zs") (f (quote (("report-render-time"))))))

(define-public crate-awedio_esp32-0.5.0 (c (n "awedio_esp32") (v "0.5.0") (d (list (d (n "awedio") (r "^0.4") (k 0)) (d (n "esp-idf-hal") (r "^0.42") (f (quote ("native"))) (k 0)))) (h "08230wi3z56fjqlgyn6cdawf62ml2br9lmycy2zhhh8wyph5sajn") (f (quote (("report-render-time"))))))

