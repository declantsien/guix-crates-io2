(define-module (crates-io aw at awattar-api) #:use-module (crates-io))

(define-public crate-awattar-api-0.1.0 (c (n "awattar-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wd6xvlv24i1mjz10jln6ch2ld7zyf1skjzw9ghdy3xb3sna10z2")))

(define-public crate-awattar-api-0.2.0 (c (n "awattar-api") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0fgqpcw906fj499qs5z12yiz93na23q527p4p8hqdgk1ymi9pdws")))

