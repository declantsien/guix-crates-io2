(define-module (crates-io aw -c aw-client-rust) #:use-module (crates-io))

(define-public crate-aw-client-rust-0.1.0 (c (n "aw-client-rust") (v "0.1.0") (d (list (d (n "aw-datastore") (r "^0.1") (d #t) (k 2)) (d (n "aw-models") (r "^0.1") (d #t) (k 0)) (d (n "aw-server") (r "^0.8") (k 2)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "gethostname") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12ib5rfgbw58l902df64f081bb43gb2wncal4f3bngab8yn7q7aq")))

