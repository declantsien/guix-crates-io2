(define-module (crates-io aw sl awsl-pest) #:use-module (crates-io))

(define-public crate-awsl-pest-0.1.0 (c (n "awsl-pest") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.16") (d #t) (k 2)))) (h "0h4rsbb3f3zcaw7kdanrnqsb83p74wi4grvha4msvqqygm1wxmgk")))

