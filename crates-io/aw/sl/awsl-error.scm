(define-module (crates-io aw sl awsl-error) #:use-module (crates-io))

(define-public crate-awsl-error-0.1.0 (c (n "awsl-error") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (f (quote ("std" "serde" "rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "type-uuid") (r "^0.1.2") (d #t) (k 0)) (d (n "yggdrasil-shared") (r "^0.2.1") (d #t) (k 0)))) (h "1943sxxp795j9zxihjbwi9044kgxnv9l67nzcm07vy1mbz729kn3")))

