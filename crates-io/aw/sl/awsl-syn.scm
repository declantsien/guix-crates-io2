(define-module (crates-io aw sl awsl-syn) #:use-module (crates-io))

(define-public crate-awsl-syn-0.1.0 (c (n "awsl-syn") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0nmnylfzwnklbd71n2fc1h2ggkp6i47n63fpvlwl964mk6kcx62m")))

