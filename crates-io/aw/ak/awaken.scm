(define-module (crates-io aw ak awaken) #:use-module (crates-io))

(define-public crate-awaken-0.1.0 (c (n "awaken") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "mio") (r "^0.8.5") (d #t) (k 0)))) (h "1py61mpyn5617nzvzvazsbzgh4d4jf9ch6pybc99kia0smh9y9xx")))

(define-public crate-awaken-0.2.0 (c (n "awaken") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "mio") (r "^0.8.5") (d #t) (k 0)))) (h "07gj2b4jrr5ffh5jiq4k7aw6ack5sn25psi65s42snf749mcmrg0")))

