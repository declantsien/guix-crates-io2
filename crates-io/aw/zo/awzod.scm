(define-module (crates-io aw zo awzod) #:use-module (crates-io))

(define-public crate-awzod-0.1.0 (c (n "awzod") (v "0.1.0") (d (list (d (n "asky") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1akf8w4i1kkhikqnbscwmy9b63dqgw4ir0wygf1yk1b0nbyj7kr2")))

(define-public crate-awzod-0.1.1 (c (n "awzod") (v "0.1.1") (d (list (d (n "asky") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1cfkq62mbcyx54wcv8hh382q3bb8cjd9xcb675z6fwwzpw5m63v5")))

(define-public crate-awzod-0.1.2 (c (n "awzod") (v "0.1.2") (d (list (d (n "asky") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1if4lnjpkdp0s0587blfhf092a2q0g5iqkqpw55h20hmd4xalchl")))

(define-public crate-awzod-0.1.4 (c (n "awzod") (v "0.1.4") (d (list (d (n "asky") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1vvi5w61afnc9hcnc99gb75640kn29m2v20bf6bpr6an5kg2mc7p")))

