(define-module (crates-io aw re awred) #:use-module (crates-io))

(define-public crate-awred-0.1.0 (c (n "awred") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.0.0-beta.9") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0vnwzbbhyikmchn7l6gc39wcska6pkd636xw0gjdjlyh3qdfypw2")))

(define-public crate-awred-0.1.1 (c (n "awred") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.0.0-beta.9") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0q5055yj4hb9pa69g3q547gz7vrfvqp2x3akng7pf65h022gd4dz")))

(define-public crate-awred-0.2.0 (c (n "awred") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.0.0-beta.9") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1ayxygcv2x984m11rwrd27v37c68jva4hxv9gpf7zg73lxi1prpp")))

