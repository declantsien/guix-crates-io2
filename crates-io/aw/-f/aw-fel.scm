(define-module (crates-io aw -f aw-fel) #:use-module (crates-io))

(define-public crate-aw-fel-0.1.0 (c (n "aw-fel") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "02xbj72iz4s0aa8l4p78wa7vahqmxjzhgw1v4jzw4m16azraz6gi")))

(define-public crate-aw-fel-0.2.0 (c (n "aw-fel") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "1x42arlvm6njl47dypygcb2y5d524xv8ynv75djgd3y7rp03n3dj") (f (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.3.0 (c (n "aw-fel") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.9") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "0xk55nd8piix4bxkkjx33i45z0q05497j2z7wm9v7503wqz164gf") (f (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.4.0 (c (n "aw-fel") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)))) (h "1ma1kc0cf082kzzq29lw11n5g1yjdwx0c3sxb9zxhlrqk5spki16") (f (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.4.1 (c (n "aw-fel") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)))) (h "1h33xwv4y7qv20xllbfw017qgzk8w61h6rf58kc2nsrixyx3bmb7") (f (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.5.0 (c (n "aw-fel") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)))) (h "1zr5ghqj4vp95cxw3ghsf7i74i7n52dhbznfljcmdzbhdnv8zzb9") (f (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.5.1 (c (n "aw-fel") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)))) (h "0fb6lfrray1jg0fraviw4dyj2d4vc2f2qkiwcbk7wm2g99cz814m") (f (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.5.2 (c (n "aw-fel") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)))) (h "07s76rygm6rsjh3y3i95ijwrl7wva4kp1d828wcw4yi16sahv4fl") (f (quote (("uboot") ("default" "uboot"))))))

