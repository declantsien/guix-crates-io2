(define-module (crates-io aw -q aw-query) #:use-module (crates-io))

(define-public crate-aw-query-0.1.0 (c (n "aw-query") (v "0.1.0") (d (list (d (n "aw-datastore") (r "^0.1") (d #t) (k 0)) (d (n "aw-models") (r "^0.1") (d #t) (k 0)) (d (n "aw-transform") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plex") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03lsifhaachdj1qkyqhbiy9izifikl7azqcfhdd1yvpvvsrwqh3k")))

