(define-module (crates-io aw -m aw-models) #:use-module (crates-io))

(define-public crate-aw-models-0.1.0 (c (n "aw-models") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n60db86pd2945nrq4ixgisvfaq1ik3bfg0ijjk1a0qyk13wi8lv")))

