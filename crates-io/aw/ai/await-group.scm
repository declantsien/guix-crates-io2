(define-module (crates-io aw ai await-group) #:use-module (crates-io))

(define-public crate-await-group-0.1.0 (c (n "await-group") (v "0.1.0") (d (list (d (n "atomic-waker") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "00h6qx6rpypb44m9g6bc6vfsd6zcf8n3dhdc3vl29c94ih3x7g07")))

