(define-module (crates-io aw ai awair-local-api) #:use-module (crates-io))

(define-public crate-awair-local-api-0.1.0 (c (n "awair-local-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "03hwjkprk5707vrrnhsbp73p8hkpbqfgvskmyyz40f7adypqsnxq")))

