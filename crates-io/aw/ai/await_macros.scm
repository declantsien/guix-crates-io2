(define-module (crates-io aw ai await_macros) #:use-module (crates-io))

(define-public crate-await_macros-0.1.0 (c (n "await_macros") (v "0.1.0") (h "0cnb6v9dzsjl2pg9fvc4477v8dfk7bb5bh7fqw5b854zwv23dbhl")))

(define-public crate-await_macros-0.1.1 (c (n "await_macros") (v "0.1.1") (h "08w0qswwb315zz7q71maf00nr8q791hir5x3xw1kq0y1acca29gd")))

(define-public crate-await_macros-0.1.2 (c (n "await_macros") (v "0.1.2") (h "11q4j0pxvnijpqiq90fihyw6kyfx0q2kc227gk1z7flpjbafxs6m")))

(define-public crate-await_macros-0.1.3 (c (n "await_macros") (v "0.1.3") (h "1f2fnbd3b7p3s91c056kpcrxa8fwx756ail97man9cp7qjssl7an")))

(define-public crate-await_macros-0.2.0 (c (n "await_macros") (v "0.2.0") (h "09wyp0h1d239364lc9gf66vl822j3gz09yd1ff320ba9i0xlybzb")))

