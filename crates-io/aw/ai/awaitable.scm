(define-module (crates-io aw ai awaitable) #:use-module (crates-io))

(define-public crate-awaitable-0.1.0 (c (n "awaitable") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0qma50ph9xg16396k1vmix8naf006hhr2w1m0hrzw10a8dj6bdzs")))

(define-public crate-awaitable-0.1.1 (c (n "awaitable") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "1507ddzfpnxzcyfx0kh6hs6207vfdqi387p2b6c45dmmdscc46ac")))

(define-public crate-awaitable-0.1.2 (c (n "awaitable") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "1hxb6m08wdxsf6lc6rp4cfl127zmp5h589lhgkppjvw76m5bi6yw")))

(define-public crate-awaitable-0.1.3 (c (n "awaitable") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0r2jrvfsqzv37xx6kszb0mkhisak1zc16qb6mk9bli1vwxrpzby8")))

(define-public crate-awaitable-0.2.0 (c (n "awaitable") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0y8b12z0cmpxa95xb97pb2xf5gl0ian08kjkkqxnawqpp0qniyy3")))

(define-public crate-awaitable-0.3.0 (c (n "awaitable") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "050b6dxvvl93mw7rav6a0qdcy6x4wf2w42l16a90cw4011ykk69f")))

(define-public crate-awaitable-0.3.1 (c (n "awaitable") (v "0.3.1") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "09n5rhyz9ykdswydi4bbhbsm0kkm29a645phbg1djgyacf826dsv")))

(define-public crate-awaitable-0.3.2 (c (n "awaitable") (v "0.3.2") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "09av2gsd85a5xwqygbnn432mm9gp245x849f4lcpx150mnri373c")))

(define-public crate-awaitable-0.3.3 (c (n "awaitable") (v "0.3.3") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0wyzlrw93qpjsrggwfxq9vpf5xlk7c56s10qd2bzdsncmisa9pv3")))

(define-public crate-awaitable-0.4.0 (c (n "awaitable") (v "0.4.0") (d (list (d (n "awaitable-error") (r "^0.1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "0x879j2bwc0yiijr1zr410cng76r8a5k7rd1qravcg3nkaf49bvh")))

