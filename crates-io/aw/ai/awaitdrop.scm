(define-module (crates-io aw ai awaitdrop) #:use-module (crates-io))

(define-public crate-awaitdrop-0.1.0 (c (n "awaitdrop") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1fkpqkqvwjdd5rapr098amzjwriayhsaza1b5b61rnk57nmyhd1j")))

(define-public crate-awaitdrop-0.1.1 (c (n "awaitdrop") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "0dws6792v0gr2yd58bdlhkjwbj97vzydx0dg1r6nmaynngf08k73")))

(define-public crate-awaitdrop-0.1.2 (c (n "awaitdrop") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "0ad4k7niwzrpbs3pq8klwdpi72dvgyxp1y7v4cdxrhpfqz6m243p")))

