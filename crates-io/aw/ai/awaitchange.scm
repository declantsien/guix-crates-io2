(define-module (crates-io aw ai awaitchange) #:use-module (crates-io))

(define-public crate-awaitchange-0.3.0 (c (n "awaitchange") (v "0.3.0") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "1hf7x88z37cgicxr09qdwlzzn803yqh68ljg6gqkm9brnvcybf0q")))

(define-public crate-awaitchange-0.4.0 (c (n "awaitchange") (v "0.4.0") (d (list (d (n "last-update-time") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0dh0spcp0123ksk5n33b9irk9zd4g0vqw6x85716cbfnwrx6266b")))

(define-public crate-awaitchange-0.4.1 (c (n "awaitchange") (v "0.4.1") (d (list (d (n "last-update-time") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0dvs6hxjiy1rphgkik4yci5c2prqial5jk96nsiafrzl6razgi76")))

