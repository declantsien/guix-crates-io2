(define-module (crates-io aw to awto) #:use-module (crates-io))

(define-public crate-awto-0.1.0 (c (n "awto") (v "0.1.0") (d (list (d (n "awto-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "0kajqcagixh80xz73x7kbv91m3fkp7iqn2hsq332p3dry0mrgl6f")))

(define-public crate-awto-0.1.1 (c (n "awto") (v "0.1.1") (d (list (d (n "awto-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "1psswsn02bxjil1xp5yzdk77hl7wgmns5fcv3bddpiwq9kqg9bwn")))

(define-public crate-awto-0.1.2 (c (n "awto") (v "0.1.2") (d (list (d (n "awto-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tonic") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "0b3wwr8vah3rmj0lb6ylj7jq5nlfs2phmr58s1pj59lrkbzx35v9")))

