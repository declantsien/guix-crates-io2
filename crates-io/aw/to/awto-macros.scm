(define-module (crates-io aw to awto-macros) #:use-module (crates-io))

(define-public crate-awto-macros-0.1.0 (c (n "awto-macros") (v "0.1.0") (d (list (d (n "better-bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "069fwg0c0m7rh23dj18pkwrgh20pcmj2i9969c7fwkd6fl4dszy0")))

(define-public crate-awto-macros-0.1.1 (c (n "awto-macros") (v "0.1.1") (d (list (d (n "better-bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0capsrn1k4qiw2l83vsk8c09vv1lrv76p5vsvzh9bhiyf4mf165r")))

(define-public crate-awto-macros-0.1.2 (c (n "awto-macros") (v "0.1.2") (d (list (d (n "better-bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0vqbazssff8p4mmabcdk2x9p1f6mfipbpi7ac3h0fknvf8nn8nm1")))

