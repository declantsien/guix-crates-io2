(define-module (crates-io aw pa awpack) #:use-module (crates-io))

(define-public crate-awpack-0.1.0 (c (n "awpack") (v "0.1.0") (d (list (d (n "clap") (r "^2.14") (d #t) (k 0)) (d (n "git2") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0hqvxa03yd7lks0219prx3i2ay29331gf6b3rz98sjfzc590qjg4")))

