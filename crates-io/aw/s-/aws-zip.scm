(define-module (crates-io aw s- aws-zip) #:use-module (crates-io))

(define-public crate-aws-zip-0.1.0 (c (n "aws-zip") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("bzip2" "deflate"))) (k 0)))) (h "0q4rbzw7lpkglspjkzf9798r3l5gs41203vq0m1im4c750p0b5sl")))

(define-public crate-aws-zip-0.1.1 (c (n "aws-zip") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("bzip2" "deflate"))) (k 0)))) (h "1w3aywvnji9z4r4yzl8m9aazbdi0ly69h25y2x924dd39i4gazd1")))

(define-public crate-aws-zip-0.1.2 (c (n "aws-zip") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("bzip2" "deflate"))) (k 0)))) (h "1in8f9n4svqfapxw9l8kjsscbglcq9azs96fs2pbfwkx4185rfyj")))

