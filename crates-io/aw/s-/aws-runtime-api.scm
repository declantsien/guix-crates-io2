(define-module (crates-io aw s- aws-runtime-api) #:use-module (crates-io))

(define-public crate-aws-runtime-api-0.0.1 (c (n "aws-runtime-api") (v "0.0.1") (h "0bz193nk91yl6008xh8da3qzyq6gchyrkixb3yac3kqi1sv307zd")))

(define-public crate-aws-runtime-api-0.55.2 (c (n "aws-runtime-api") (v "0.55.2") (h "1fyz8nbpsw9dmjimbsl6p0mli6119gx0fhrm6c3kqb4slxn1clrz")))

(define-public crate-aws-runtime-api-0.55.3 (c (n "aws-runtime-api") (v "0.55.3") (h "1i3g2l4mpcfydbyjq95c8r8a4mggqyk31077xfcynd9rll1y0v5s")))

(define-public crate-aws-runtime-api-0.56.0 (c (n "aws-runtime-api") (v "0.56.0") (h "0xxy98avvvlf5giapy98c3gygdh2fpsmmv04z7lhbmpdsc9czgfg")))

(define-public crate-aws-runtime-api-0.56.1 (c (n "aws-runtime-api") (v "0.56.1") (h "1jvd1pxxdvjdk9bnkd3ppc7nr719gndwssmz3n40idwj4yx7x85s")))

(define-public crate-aws-runtime-api-0.57.1 (c (n "aws-runtime-api") (v "0.57.1") (h "18xmxq38x141khgbnk7m01amrpz2bf8dz5kvg2vdqfp5w9hl1cps")))

(define-public crate-aws-runtime-api-0.57.2 (c (n "aws-runtime-api") (v "0.57.2") (h "1j6smq5vlvnrikpvwipw1vsjyzgyldzqnzk5vpzd80yy6xdyw9bb")))

(define-public crate-aws-runtime-api-0.100.0 (c (n "aws-runtime-api") (v "0.100.0") (h "1f74nhkznh88bn0cd39cmww8r1ka6y1ymmvz8awln1jpm3s1j3ls")))

(define-public crate-aws-runtime-api-0.101.0 (c (n "aws-runtime-api") (v "0.101.0") (h "008c2s1mk1hz4s92smv8p9wxgl6lfyfq174dg5ipgwscca059r1a")))

(define-public crate-aws-runtime-api-1.0.0 (c (n "aws-runtime-api") (v "1.0.0") (h "1mkvyzmjwj3qya9p1y8i37n7jrn9yrqslvff4rjczkan7w5v8d3b")))

(define-public crate-aws-runtime-api-1.0.1 (c (n "aws-runtime-api") (v "1.0.1") (h "0fnwc049ahkq5cg4zbchqzfgnlj9nv267d6qddcg3r7jzxxw77l4")))

(define-public crate-aws-runtime-api-1.0.3 (c (n "aws-runtime-api") (v "1.0.3") (h "101x4wyl7fz12md108qzclk76b07ffiljpydvwzb845gr6dyx26a")))

(define-public crate-aws-runtime-api-1.1.0 (c (n "aws-runtime-api") (v "1.1.0") (h "18f9x12qyaxqgy5s1v8grrs6q2x2h7wl2g6im2fjms73n7vljhv6") (y #t)))

(define-public crate-aws-runtime-api-1.1.1 (c (n "aws-runtime-api") (v "1.1.1") (h "1ajnwfrw4s9g23ayyhcgy2ryzp7dbxw8pwzpj0drq424qw84sq3a")))

(define-public crate-aws-runtime-api-1.1.2 (c (n "aws-runtime-api") (v "1.1.2") (h "188j3lk5h30q91kw1ixzqwxvy0gpczkznkvm4pcdpkkg33fym7yn")))

(define-public crate-aws-runtime-api-1.1.3 (c (n "aws-runtime-api") (v "1.1.3") (h "1q5am551476la685ynkbb4a7y3rc22562d08y1ffmsxp0kd26c4x")))

(define-public crate-aws-runtime-api-1.1.4 (c (n "aws-runtime-api") (v "1.1.4") (h "1rnxl1lld5n4m1zgcn6cnkfwbq40c3y3x72qp3l4kmyajnb0w27f")))

(define-public crate-aws-runtime-api-1.1.5 (c (n "aws-runtime-api") (v "1.1.5") (h "0mzi78i5mfnmr10h9qc6zw8viwsnjhq5jhm2z2kx79jcjp8lqadf")))

(define-public crate-aws-runtime-api-1.1.6 (c (n "aws-runtime-api") (v "1.1.6") (h "17mhixhnwkpq5xp4csib7ih428wvndvcnwbiy03b3ppbx9bpfca3")))

(define-public crate-aws-runtime-api-1.1.7 (c (n "aws-runtime-api") (v "1.1.7") (h "11gb5ar0sp0pxajg2f1xr0qlvx1bzwhga14ks3dg7kwv6pbp8r36")))

(define-public crate-aws-runtime-api-1.1.8 (c (n "aws-runtime-api") (v "1.1.8") (h "1ljvbs8ilsq6ddxgpipg59ccw19dkpdfdadqcmwryka247f2cwxc")))

