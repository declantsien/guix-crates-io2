(define-module (crates-io aw s- aws-lambda-log-proxy) #:use-module (crates-io))

(define-public crate-aws-lambda-log-proxy-0.1.0 (c (n "aws-lambda-log-proxy") (v "0.1.0") (d (list (d (n "aws-lambda-runtime-proxy") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wf8jr87jaqkjm7zhvdpqj77bag7wc5sp5gyvgwimva27v0s6qa3")))

(define-public crate-aws-lambda-log-proxy-0.1.1 (c (n "aws-lambda-log-proxy") (v "0.1.1") (d (list (d (n "aws-lambda-runtime-proxy") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gx40vcwv2brn97isnhcdn0klhw1i07mlg98y38q6dishrfdx5ij")))

(define-public crate-aws-lambda-log-proxy-0.2.0 (c (n "aws-lambda-log-proxy") (v "0.2.0") (d (list (d (n "aws-lambda-runtime-proxy") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "1gh5g1qam14c3s3cf5z58pm2nvx6ya913h1wqkij8020mkr1gcd7")))

(define-public crate-aws-lambda-log-proxy-0.2.1 (c (n "aws-lambda-log-proxy") (v "0.2.1") (d (list (d (n "aws-lambda-runtime-proxy") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "15px267zyc9lspysraj11dw42chzdjwmc3bk2vm2kgfc1xjlnlma")))

(define-public crate-aws-lambda-log-proxy-0.3.0 (c (n "aws-lambda-log-proxy") (v "0.3.0") (d (list (d (n "aws-lambda-runtime-proxy") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "05nal3r02d0vlvqhii1xzdjdqy9x2jym01bwwzhabkkmq1qw7xsm")))

