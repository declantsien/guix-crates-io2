(define-module (crates-io aw s- aws-token-rotate) #:use-module (crates-io))

(define-public crate-aws-token-rotate-1.0.2 (c (n "aws-token-rotate") (v "1.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "configparser") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.45") (d #t) (k 0)) (d (n "rusoto_iam") (r "^0.45") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "0rgzy58hyl4m28q35iapzcwfnmmvzx2cmnmqfclf0ilk0vn627dd")))

(define-public crate-aws-token-rotate-1.0.3 (c (n "aws-token-rotate") (v "1.0.3") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "configparser") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.48") (d #t) (k 0)) (d (n "rusoto_iam") (r "^0.48") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "10ipr8yzfvh2a03xidyjdimkvygcj1d7vcpwfp49kbb6wq14cva1")))

(define-public crate-aws-token-rotate-1.0.4 (c (n "aws-token-rotate") (v "1.0.4") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "configparser") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rusoto_core") (r "^0.48") (d #t) (k 0)) (d (n "rusoto_iam") (r "^0.48") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1gd7k9jrv1lh1r692rxpnzw5jgmaq55vqx22mbbxpd9lsawymb19")))

