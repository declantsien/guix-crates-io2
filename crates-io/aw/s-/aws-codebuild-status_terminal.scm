(define-module (crates-io aw s- aws-codebuild-status_terminal) #:use-module (crates-io))

(define-public crate-aws-codebuild-status_terminal-0.3.0 (c (n "aws-codebuild-status_terminal") (v "0.3.0") (d (list (d (n "aws-codebuild-status_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "07wyjvd8a7xwcg3wl3d80h8n1d0rhd1why7b9w18qwfdc4wh3lzj")))

