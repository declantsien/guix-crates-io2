(define-module (crates-io aw s- aws-sqs-create-queue) #:use-module (crates-io))

(define-public crate-aws-sqs-create-queue-0.1.0 (c (n "aws-sqs-create-queue") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "aws-config") (r "^0.49.0") (d #t) (k 0)) (d (n "aws-sdk-sqs") (r "^0.19.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "064wn6vd4isw03c662i5s8fw0ym16z50c65hdaq58j3163za0pnd")))

