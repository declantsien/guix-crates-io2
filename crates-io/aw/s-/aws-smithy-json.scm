(define-module (crates-io aw s- aws-smithy-json) #:use-module (crates-io))

(define-public crate-aws-smithy-json-0.27.0-alpha (c (n "aws-smithy-json") (v "0.27.0-alpha") (d (list (d (n "aws-smithy-types") (r "^0.27.0-alpha") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0h4y3kdfm7zyb69y4mnljjnw280r0lpcmc0xnpwwvm6bpi00ij8m")))

(define-public crate-aws-smithy-json-0.27.0-alpha.1 (c (n "aws-smithy-json") (v "0.27.0-alpha.1") (d (list (d (n "aws-smithy-types") (r "^0.27.0-alpha.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0m97srdcqw9cazw5zwgsirgdkb535imklh2b1lazslvi69wghl0b")))

(define-public crate-aws-smithy-json-0.27.0-alpha.2 (c (n "aws-smithy-json") (v "0.27.0-alpha.2") (d (list (d (n "aws-smithy-types") (r "^0.27.0-alpha.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0l8p9lhawanlwlhn4sdv9h1zk9l7ap1hvbp087iqddvg6qnqlcff") (y #t)))

(define-public crate-aws-smithy-json-0.28.0-alpha (c (n "aws-smithy-json") (v "0.28.0-alpha") (d (list (d (n "aws-smithy-types") (r "^0.28.0-alpha") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "129pdray9n7q8kc6gpglgc17zqr3in7vyj5chfjzrid4mmd7qjz8")))

(define-public crate-aws-smithy-json-0.30.0-alpha (c (n "aws-smithy-json") (v "0.30.0-alpha") (d (list (d (n "aws-smithy-types") (r "^0.30.0-alpha") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1cwfh1i7lm1l7dy1r6688k3dv0xyrr0amk31dlid6wbvqkp7c4bj")))

(define-public crate-aws-smithy-json-0.31.0 (c (n "aws-smithy-json") (v "0.31.0") (d (list (d (n "aws-smithy-types") (r "^0.31.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d0gz81b5iykmly2dimp5b0jz56hz87vvjg7raw2vdgvdz889qrp") (y #t)))

(define-public crate-aws-smithy-json-0.32.0 (c (n "aws-smithy-json") (v "0.32.0") (d (list (d (n "aws-smithy-types") (r "^0.32.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0b24lkgvfm8b5smx4js5kkbcgryas5vbzzicnjrdkwpaxnfc2mhn")))

(define-public crate-aws-smithy-json-0.33.1 (c (n "aws-smithy-json") (v "0.33.1") (d (list (d (n "aws-smithy-types") (r "^0.33.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hp8nlc1jsr75j6afx8yhhdlcknll6szhbpx4gcl68jk51zqdnxc")))

(define-public crate-aws-smithy-json-0.34.0 (c (n "aws-smithy-json") (v "0.34.0") (d (list (d (n "aws-smithy-types") (r "^0.34.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16wszx3va62qwb2mldd44wi7yjb6zz7fp525v2l6c3qpblg97lv2")))

(define-public crate-aws-smithy-json-0.34.1 (c (n "aws-smithy-json") (v "0.34.1") (d (list (d (n "aws-smithy-types") (r "^0.34.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ki77f75wmp4khaw7s78m7pvnh9x8v4295hvx9vyn4lw0ksmij06")))

(define-public crate-aws-smithy-json-0.35.1 (c (n "aws-smithy-json") (v "0.35.1") (d (list (d (n "aws-smithy-types") (r "^0.35.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1aiddk6vkbpgqp8drv03j8nn76ff05z0fyk1abh5cbimciwj7kl0") (y #t)))

(define-public crate-aws-smithy-json-0.35.2 (c (n "aws-smithy-json") (v "0.35.2") (d (list (d (n "aws-smithy-types") (r "^0.35.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0h0ynsgn7gb6l1nj8xy6njz3zjv0np19vrxcqi9l0498flnd8fm7")))

(define-public crate-aws-smithy-json-0.36.0 (c (n "aws-smithy-json") (v "0.36.0") (d (list (d (n "aws-smithy-types") (r "^0.36.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02bac6xrgcfpljf09j9v3mjb3ix7gn8xvpy5pyk1lmha5cvrrh9w")))

(define-public crate-aws-smithy-json-0.37.0 (c (n "aws-smithy-json") (v "0.37.0") (d (list (d (n "aws-smithy-types") (r "^0.37.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1yh7prp3l2lzgavds323v64zx09mjrlmh3n736dkj7icsi3skk4m")))

(define-public crate-aws-smithy-json-0.38.0 (c (n "aws-smithy-json") (v "0.38.0") (d (list (d (n "aws-smithy-types") (r "^0.38.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1n3ba3gip73w41bngcb8sdy75mbp4wk7zsvmxdg4idzcpbc6g7dm")))

(define-public crate-aws-smithy-json-0.39.0 (c (n "aws-smithy-json") (v "0.39.0") (d (list (d (n "aws-smithy-types") (r "^0.39.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17i3p79ja5hw874w9pw16b7awdvdzfmr19aa4xvqnlx26n2z48dx")))

(define-public crate-aws-smithy-json-0.40.2 (c (n "aws-smithy-json") (v "0.40.2") (d (list (d (n "aws-smithy-types") (r "^0.40.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0s40l09hc2vncxysvpc2bgblzfg23b77y4r8bajpavcl93f7bdfg")))

(define-public crate-aws-smithy-json-0.41.0 (c (n "aws-smithy-json") (v "0.41.0") (d (list (d (n "aws-smithy-types") (r "^0.41.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1yjkjr5ayg65nmvnm9gh90k366894bljvv2sznr5vldplfjf55ip")))

(define-public crate-aws-smithy-json-0.42.0 (c (n "aws-smithy-json") (v "0.42.0") (d (list (d (n "aws-smithy-types") (r "^0.42.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0iz6ji4z0rwb82kk923f4xmwy9zxpmn3r4f00rr6qf5scnlnmjc1")))

(define-public crate-aws-smithy-json-0.43.0 (c (n "aws-smithy-json") (v "0.43.0") (d (list (d (n "aws-smithy-types") (r "^0.43.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19ylglvd2yl819lpcqdn1an2qg996dw1b85zhvq1339fhzxmm30n")))

(define-public crate-aws-smithy-json-0.44.0 (c (n "aws-smithy-json") (v "0.44.0") (d (list (d (n "aws-smithy-types") (r "^0.44.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "112ikw9r5vvhi72yb68wv1mr5yb0c5ivx6x8qg64xsj22qdz1g9s")))

(define-public crate-aws-smithy-json-0.45.0 (c (n "aws-smithy-json") (v "0.45.0") (d (list (d (n "aws-smithy-types") (r "^0.45.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09844g7ixhfmpy04hra12y4adnrg2n275jqh0wqghm4wgy8h51vl")))

(define-public crate-aws-smithy-json-0.46.0 (c (n "aws-smithy-json") (v "0.46.0") (d (list (d (n "aws-smithy-types") (r "^0.46.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0biwr4vha2yj6l3n590ym7n3h0a265jqc9lbpn0awfl5k0v67dri")))

(define-public crate-aws-smithy-json-0.47.0 (c (n "aws-smithy-json") (v "0.47.0") (d (list (d (n "aws-smithy-types") (r "^0.47.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ll92x2x293mj04im7x124444x8zqdyz8sshjqmds261qdvbqvhf")))

(define-public crate-aws-smithy-json-0.48.0 (c (n "aws-smithy-json") (v "0.48.0") (d (list (d (n "aws-smithy-types") (r "^0.48.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04cblh586zn5mmzm4980dgjmj4dalz2kdbcgz00w00rqcj9vcj2h")))

(define-public crate-aws-smithy-json-0.49.0 (c (n "aws-smithy-json") (v "0.49.0") (d (list (d (n "aws-smithy-types") (r "^0.49.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0k6mkxgybw3rx8slrsfiq6ban0iihcwx5jbx9xr3jjiwz5m4b4v5")))

(define-public crate-aws-smithy-json-0.50.0 (c (n "aws-smithy-json") (v "0.50.0") (d (list (d (n "aws-smithy-types") (r "^0.50.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "157v40nxvy8w6mwfbwlajjy639vxkdc26j6kr8af7aii564bmap8") (y #t)))

(define-public crate-aws-smithy-json-0.51.0 (c (n "aws-smithy-json") (v "0.51.0") (d (list (d (n "aws-smithy-types") (r "^0.51.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06vhxasvm5gl78n3sp76drj095d0xxxc65nc4dvii54aijllncnq")))

(define-public crate-aws-smithy-json-0.52.0 (c (n "aws-smithy-json") (v "0.52.0") (d (list (d (n "aws-smithy-types") (r "^0.52.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03ll3ksdab5a1bwk4mi2a45yqxv1lpn6jr24x5cvqrxifn9dsgcy")))

(define-public crate-aws-smithy-json-0.53.0 (c (n "aws-smithy-json") (v "0.53.0") (d (list (d (n "aws-smithy-types") (r "^0.53.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11h90c2vskiwfi0ggd2bs6kx96c3b5z1y2z0nfq1wfv08r81qkss")))

(define-public crate-aws-smithy-json-0.53.1 (c (n "aws-smithy-json") (v "0.53.1") (d (list (d (n "aws-smithy-types") (r "^0.53.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1981r81p61jsl8k7w7cb250894j2hrn98swhbxdlk044nrckdcyz")))

(define-public crate-aws-smithy-json-0.54.0 (c (n "aws-smithy-json") (v "0.54.0") (d (list (d (n "aws-smithy-types") (r "^0.54.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1915dpafgn11aqrzzz007pyd7h6q451qkpca1psvpjl8gkmmc7w7")))

(define-public crate-aws-smithy-json-0.54.1 (c (n "aws-smithy-json") (v "0.54.1") (d (list (d (n "aws-smithy-types") (r "^0.54.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vrajsc12m5gmz6gwv0wvxsrk80vgsjrfr1qj54ba9qyf893z5qq")))

(define-public crate-aws-smithy-json-0.54.2 (c (n "aws-smithy-json") (v "0.54.2") (d (list (d (n "aws-smithy-types") (r "^0.54.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jf88czvliv0msna3w7335bgvf4sqk7r630pgklfsvl7bh53q4ik") (y #t)))

(define-public crate-aws-smithy-json-0.54.3 (c (n "aws-smithy-json") (v "0.54.3") (d (list (d (n "aws-smithy-types") (r "^0.54.3") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0yr2ryq9j94xcpk98r63inlby0m2ll8wrmshn5lxkib8244wq8qs")))

(define-public crate-aws-smithy-json-0.54.4 (c (n "aws-smithy-json") (v "0.54.4") (d (list (d (n "aws-smithy-types") (r "^0.54.4") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10m1fyi0clwsarqnb63jxbk8zdknhrwsvj7w8sbp57wynzr3zn2b")))

(define-public crate-aws-smithy-json-0.55.0 (c (n "aws-smithy-json") (v "0.55.0") (d (list (d (n "aws-smithy-types") (r "^0.55.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0x5a36zylbxq04w71aji91q9g197bgg864adbjy0l7qx3fmir8xd")))

(define-public crate-aws-smithy-json-0.55.1 (c (n "aws-smithy-json") (v "0.55.1") (d (list (d (n "aws-smithy-types") (r "^0.55.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0w0cl3gr8hb13pn7z3j44a0xwb6sh2i5nzrarafqib2wsy3iw34v")))

(define-public crate-aws-smithy-json-0.55.2 (c (n "aws-smithy-json") (v "0.55.2") (d (list (d (n "aws-smithy-types") (r "^0.55.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fdgz3bd23l4q6wxj44m7b69p3shxkarl1wz6z72q2xhxgkddqsw")))

(define-public crate-aws-smithy-json-0.55.3 (c (n "aws-smithy-json") (v "0.55.3") (d (list (d (n "aws-smithy-types") (r "^0.55.3") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1y07k1jjik7yp6q6wlxsirs7f07n36ngncm6ji8hjvd9pwpz9y93")))

(define-public crate-aws-smithy-json-0.56.0 (c (n "aws-smithy-json") (v "0.56.0") (d (list (d (n "aws-smithy-types") (r "^0.56.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kv5sk3gd73ghpxqys0ljwkb5hnhysha0is8bq1bh6p05xydv49m")))

(define-public crate-aws-smithy-json-0.56.1 (c (n "aws-smithy-json") (v "0.56.1") (d (list (d (n "aws-smithy-types") (r "^0.56.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kx37qrp699l39h07gsq8ygrx6104inyaymgjg0i1bd7iymygcag")))

(define-public crate-aws-smithy-json-0.57.0 (c (n "aws-smithy-json") (v "0.57.0") (d (list (d (n "aws-smithy-types") (r "~0.57") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0i298hdyakyjz8ygkr4k3b4n4ljr8wrisvi0787bh5p1qgg7dhjp")))

(define-public crate-aws-smithy-json-0.57.1 (c (n "aws-smithy-json") (v "0.57.1") (d (list (d (n "aws-smithy-types") (r "^0.57.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0c0giiaxf2si6zl1sf08n9j1228v6pkscvj06dqb3jnmvild68fy")))

(define-public crate-aws-smithy-json-0.57.2 (c (n "aws-smithy-json") (v "0.57.2") (d (list (d (n "aws-smithy-types") (r "^0.57.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ib63kr2xcxyq90xwf5addyd9ahs7i10dipnkzg8bmbdy5d4vmmn")))

(define-public crate-aws-smithy-json-0.58.0 (c (n "aws-smithy-json") (v "0.58.0") (d (list (d (n "aws-smithy-types") (r "^0.100.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kd6mbbbmjds2himc0jrv7l0llk9sdk3lj31zazmiaqcspqxx4sm")))

(define-public crate-aws-smithy-json-0.59.0 (c (n "aws-smithy-json") (v "0.59.0") (d (list (d (n "aws-smithy-types") (r "^0.101.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r5a0s1c24div76bn32sqmvij5m10hfxryh1qmnh88gywfz8by8g")))

(define-public crate-aws-smithy-json-0.60.0 (c (n "aws-smithy-json") (v "0.60.0") (d (list (d (n "aws-smithy-types") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0p55s65mimzc537i3vpcxhcd1jldcyyijnjvd9m6smy9ilrxsika")))

(define-public crate-aws-smithy-json-0.61.0 (c (n "aws-smithy-json") (v "0.61.0") (d (list (d (n "aws-smithy-types") (r "^1.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12bap2zvsr3gw6ryghd9385q7jdbarjvx6hycclbg0nkr00751sh") (y #t)))

(define-public crate-aws-smithy-json-0.60.1 (c (n "aws-smithy-json") (v "0.60.1") (d (list (d (n "aws-smithy-types") (r "^1.1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1b79lnwyqjwmgczc9lslc2vnf7lfk5l5pfz10n6jzpq8kvagdcwa")))

(define-public crate-aws-smithy-json-0.60.2 (c (n "aws-smithy-json") (v "0.60.2") (d (list (d (n "aws-smithy-types") (r "^1.1.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kncz2v5l3q3i0acixkhhl51nihkhbkkrmdzmj7ry1ha6wiznazi")))

(define-public crate-aws-smithy-json-0.60.3 (c (n "aws-smithy-json") (v "0.60.3") (d (list (d (n "aws-smithy-types") (r "^1.1.3") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rq3wp9fh08xzqzdn1v5pkvjxbbm7m43qfxa6q43fqxc4yvwsg3k")))

(define-public crate-aws-smithy-json-0.60.4 (c (n "aws-smithy-json") (v "0.60.4") (d (list (d (n "aws-smithy-types") (r "^1.1.4") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13mahdnzavhz7z8fz413j4qh1w4q8c371237c9gj3y8qcp59hf7x")))

(define-public crate-aws-smithy-json-0.60.5 (c (n "aws-smithy-json") (v "0.60.5") (d (list (d (n "aws-smithy-types") (r "^1.1.5") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bhk9sva1xiih78s27ina5nnv51xvnlwq6yyy2dmfp3gdccbbhd1")))

(define-public crate-aws-smithy-json-0.60.6 (c (n "aws-smithy-json") (v "0.60.6") (d (list (d (n "aws-smithy-types") (r "^1.1.7") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rdc8jvfrxrm0qdprs51qqbph6k0qja7ws6i73z2bysp637hxy0s")))

(define-public crate-aws-smithy-json-0.60.7 (c (n "aws-smithy-json") (v "0.60.7") (d (list (d (n "aws-smithy-types") (r "^1.1.8") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mpjhs26q22jglmm85rnb79s0681jqlx2wrlmn6lc2ggd6adz0s6")))

