(define-module (crates-io aw s- aws-ne-sys) #:use-module (crates-io))

(define-public crate-aws-ne-sys-0.1.0 (c (n "aws-ne-sys") (v "0.1.0") (h "0pj3bhj1payfz0hc7f68qbk3s79h4cm2nlj293klcs560cqph380")))

(define-public crate-aws-ne-sys-0.2.0 (c (n "aws-ne-sys") (v "0.2.0") (h "0n9grngmn9gkp0h4ws3m9bnvnjxm13lgkmjag8mx95vks35p6yd5")))

(define-public crate-aws-ne-sys-0.3.0 (c (n "aws-ne-sys") (v "0.3.0") (h "0fwa3b09cf56v1270z0i5akl5x8phvc7fmphyv40mhhkw7bc7r5l")))

(define-public crate-aws-ne-sys-0.4.0 (c (n "aws-ne-sys") (v "0.4.0") (h "0dbv36192gyjgidyw8fv3zcb7iis08cg8f58mxms15d4dp2l8syk")))

