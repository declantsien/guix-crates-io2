(define-module (crates-io aw s- aws-region) #:use-module (crates-io))

(define-public crate-aws-region-0.20.0 (c (n "aws-region") (v "0.20.0") (d (list (d (n "simpl") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "030vrrgw81f43v3v3bx197hanbds0n3vwkq2jvni2iangdsq5g7n") (f (quote (("no-verify-ssl"))))))

(define-public crate-aws-region-0.21.0 (c (n "aws-region") (v "0.21.0") (d (list (d (n "simpl") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "0imm9ya5gc73d978pfk0mljnr6zjllznzkzw6gw9c6y23khv4155")))

(define-public crate-aws-region-0.22.0 (c (n "aws-region") (v "0.22.0") (d (list (d (n "simpl") (r "^0.1.0") (d #t) (k 0)))) (h "0hyj5d5wb0rrbxzab8dms9xx7cyf8va7zvg4dwv1m7xj7akd1m78")))

(define-public crate-aws-region-0.22.1 (c (n "aws-region") (v "0.22.1") (d (list (d (n "simpl") (r "^0.1.0") (d #t) (k 0)))) (h "104v6awh9bf1zmnbp69ahlqvkykqmpv10d6w2hh5j1vg755ay47n")))

(define-public crate-aws-region-0.23.0 (c (n "aws-region") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0gmlk9aya4i0id91lsjlc3fk13ys3j8mnwlmqab2labq1sn5m98z")))

(define-public crate-aws-region-0.23.1 (c (n "aws-region") (v "0.23.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0rsmv8q27nxvqwx8wsl06qd51q7k7ihp8dah00iafsksap1va979")))

(define-public crate-aws-region-0.23.2 (c (n "aws-region") (v "0.23.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1gbq39blz8nl1xzcay8n6flrq4s0sghlz6r11gw4njpbmbrbi118")))

(define-public crate-aws-region-0.23.3 (c (n "aws-region") (v "0.23.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0d0bn823d5sly1yrll4hvzw6c0yswyy404jfxbgd0790ixxvdvxi")))

(define-public crate-aws-region-0.23.4 (c (n "aws-region") (v "0.23.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0rvjyxgmwi9gaqywywibqcbkz1dkzc7mw5zg24ck2iwh5kfc4dsf")))

(define-public crate-aws-region-0.23.5 (c (n "aws-region") (v "0.23.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0k8a5rdlzb539wm2g1gmab99ams92gy8hpprpvk4gyq0v3dhs48h")))

(define-public crate-aws-region-0.24.0 (c (n "aws-region") (v "0.24.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10g15dizh59inh9qxdm90bjj6rm8dh7r5ps21agxmanll2d8zfah") (y #t)))

(define-public crate-aws-region-0.24.1 (c (n "aws-region") (v "0.24.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hrnikblyfacgf23y2gz1jw2y5nnsqjw6gig5a0p43x7987irpcv")))

(define-public crate-aws-region-0.25.0 (c (n "aws-region") (v "0.25.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dysgq13k43a0fk4rjaijr7mqc2w9j308a7waxnysq3w8mqbfy5f")))

(define-public crate-aws-region-0.25.1 (c (n "aws-region") (v "0.25.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dxgn3gy26p8jrwd43xpyh5ys7cmhsmibq53dj8s03hdhpsqlapr")))

(define-public crate-aws-region-0.25.2 (c (n "aws-region") (v "0.25.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "114nxyfhp8bpax4gqrzxjrj1jw930d9rypz2z9n75yq5i8957wgk")))

(define-public crate-aws-region-0.25.3 (c (n "aws-region") (v "0.25.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1flx1bmqv79ad5awvrshlpklxvgzr0qi14yx56xfbl172jk5fr85")))

(define-public crate-aws-region-0.25.4 (c (n "aws-region") (v "0.25.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "08jf4s6h7sbxb2l6x1dzxm3nz41agxh7l1cd4q42j3x7zjwx5zj2")))

