(define-module (crates-io aw s- aws-endpoint) #:use-module (crates-io))

(define-public crate-aws-endpoint-0.0.22-alpha (c (n "aws-endpoint") (v "0.0.22-alpha") (d (list (d (n "aws-smithy-http") (r "^0.27.0-alpha") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.22-alpha") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "108ia9jcdwd0428dqdmgfxqws3iylpma5cxlzi9gclqk0y8svdi3")))

(define-public crate-aws-endpoint-0.0.23-alpha (c (n "aws-endpoint") (v "0.0.23-alpha") (d (list (d (n "aws-smithy-http") (r "^0.27.0-alpha.1") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.23-alpha") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ra9b8plpk4xiy1lghnx1cjdavx5zdxwk3ydwi05fk4xz1il7klm")))

(define-public crate-aws-endpoint-0.0.24-alpha (c (n "aws-endpoint") (v "0.0.24-alpha") (d (list (d (n "aws-smithy-http") (r "^0.27.0-alpha.2") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.24-alpha") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1jis0w759zm386qm1zknw42kwgld634j7w1h7h41w4m50vzzmnq1") (y #t)))

(define-public crate-aws-endpoint-0.0.25-alpha (c (n "aws-endpoint") (v "0.0.25-alpha") (d (list (d (n "aws-smithy-http") (r "^0.28.0-alpha") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.25-alpha") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1cxrd43s72g169z0rf493a859l7ajxcmjwaa7gj04f8f06ml2rag")))

(define-public crate-aws-endpoint-0.0.26-alpha (c (n "aws-endpoint") (v "0.0.26-alpha") (d (list (d (n "aws-smithy-http") (r "^0.30.0-alpha") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.26-alpha") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ikadsj1kgcg022qvd31kmfdfw3lfq71h4dfpc9w0h7nj6c96kab")))

(define-public crate-aws-endpoint-0.1.0 (c (n "aws-endpoint") (v "0.1.0") (d (list (d (n "aws-smithy-http") (r "^0.31.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "10pbwhs7fr2dyqrbr1d9zs9cfr256278155l11c6pxsnvszpm547") (y #t)))

(define-public crate-aws-endpoint-0.2.0 (c (n "aws-endpoint") (v "0.2.0") (d (list (d (n "aws-smithy-http") (r "^0.32.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.2.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1sqfb7mgfryrhigaf707r0q6340y9rs1dswvpjdl1khn7ncn4f0d")))

(define-public crate-aws-endpoint-0.3.0 (c (n "aws-endpoint") (v "0.3.0") (d (list (d (n "aws-smithy-http") (r "^0.33.1") (d #t) (k 0)) (d (n "aws-types") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "145ch876lk7xccl1bwx08zlw09zzlx038nziig6iwxa1fb24sn9h")))

(define-public crate-aws-endpoint-0.4.0 (c (n "aws-endpoint") (v "0.4.0") (d (list (d (n "aws-smithy-http") (r "^0.34.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "00gd8fz7kdww3rq68rfg3apfncn7ngsr3qm4qb6hvyink189lm3j")))

(define-public crate-aws-endpoint-0.4.1 (c (n "aws-endpoint") (v "0.4.1") (d (list (d (n "aws-smithy-http") (r "^0.34.1") (d #t) (k 0)) (d (n "aws-types") (r "^0.4.1") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0m5x1xy8cp2iyln1rvqajv1dx1gyqf3y2nh1f6ym6jfcmg6ayny4")))

(define-public crate-aws-endpoint-0.5.1 (c (n "aws-endpoint") (v "0.5.1") (d (list (d (n "aws-smithy-http") (r "^0.35.1") (d #t) (k 0)) (d (n "aws-types") (r "^0.5.1") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0x90mw0vhihk4a2bkkgb89n7k1v29la0zxdps7fzxg8k3f2nzjwr") (y #t)))

(define-public crate-aws-endpoint-0.5.2 (c (n "aws-endpoint") (v "0.5.2") (d (list (d (n "aws-smithy-http") (r "^0.35.2") (d #t) (k 0)) (d (n "aws-types") (r "^0.5.2") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1kcygy32pwzjwgqx2rl0z5k4w6dxxrm269cnpjyydsch98qw0qj8")))

(define-public crate-aws-endpoint-0.6.0 (c (n "aws-endpoint") (v "0.6.0") (d (list (d (n "aws-smithy-http") (r "^0.36.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.6.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1migqjsqvs82dgspb2axyijzxd6ffg0yrvz8a6vl0ndjh6qmkl06")))

(define-public crate-aws-endpoint-0.7.0 (c (n "aws-endpoint") (v "0.7.0") (d (list (d (n "aws-smithy-http") (r "^0.37.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.7.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fwnq48w00wk4pjfv65wiaxynmyxmfj6gk4rkqnpaymbg2rj2j0k")))

(define-public crate-aws-endpoint-0.8.0 (c (n "aws-endpoint") (v "0.8.0") (d (list (d (n "aws-smithy-http") (r "^0.38.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.8.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0rq12cqdilhiiclp1v4r2kkspagl0ni102w587mfz181svlhz6fh")))

(define-public crate-aws-endpoint-0.9.0 (c (n "aws-endpoint") (v "0.9.0") (d (list (d (n "aws-smithy-http") (r "^0.39.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.9.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1867jzicwwlqjp0cwmsa31g3mw0shmz9q2afhrzjhaz9906mjyaj")))

(define-public crate-aws-endpoint-0.10.1 (c (n "aws-endpoint") (v "0.10.1") (d (list (d (n "aws-smithy-http") (r "^0.40.2") (d #t) (k 0)) (d (n "aws-types") (r "^0.10.1") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "04fz9gzcr8ipimqzac9ps78hgs6vabkiljdkjjak0slr3nrqd57v")))

(define-public crate-aws-endpoint-0.11.0 (c (n "aws-endpoint") (v "0.11.0") (d (list (d (n "aws-smithy-http") (r "^0.41.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.11.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0jxkzz5g9zfl6vikaqz9lrqrwmgz84ln2a5aklq2fy0h73qd0nsi")))

(define-public crate-aws-endpoint-0.12.0 (c (n "aws-endpoint") (v "0.12.0") (d (list (d (n "aws-smithy-http") (r "^0.42.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.12.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0bwm6q2685vk39d37kaxmcpmhqykmqm2mcl8s3xqjmpn8pmdkbbf")))

(define-public crate-aws-endpoint-0.13.0 (c (n "aws-endpoint") (v "0.13.0") (d (list (d (n "aws-smithy-http") (r "^0.43.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.13.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0rzb1w9vvwhlkbb8j25yiwwvdwlvg6hw58w2f3dmyrrr0d69lq84")))

(define-public crate-aws-endpoint-0.14.0 (c (n "aws-endpoint") (v "0.14.0") (d (list (d (n "aws-smithy-http") (r "^0.44.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.14.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1xsj6n3n6amq8ra3240skv8rgq1l65ixcpa6hwnjyhqhx2zwa7fk")))

(define-public crate-aws-endpoint-0.15.0 (c (n "aws-endpoint") (v "0.15.0") (d (list (d (n "aws-smithy-http") (r "^0.45.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.15.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1zmx300wg5l06prd0w4gq25f4zkibm2as1k6l1hx0lf5lyn6gblb")))

(define-public crate-aws-endpoint-0.46.0 (c (n "aws-endpoint") (v "0.46.0") (d (list (d (n "aws-smithy-http") (r "^0.46.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.46.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0hwfbizgc3vcvhzdykn33sd1yga6l98wfnrf6wap59yx2ps5djab")))

(define-public crate-aws-endpoint-0.47.0 (c (n "aws-endpoint") (v "0.47.0") (d (list (d (n "aws-smithy-http") (r "^0.47.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.47.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1z4cbf0gxn5lcq0bdi6m3pbfh01p0dp1i7sjxqhph0akspdfkm4b")))

(define-public crate-aws-endpoint-0.48.0 (c (n "aws-endpoint") (v "0.48.0") (d (list (d (n "aws-smithy-http") (r "^0.48.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.48.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.48.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1blqil0f9pf0f6slg37kgkg2hmg2n8m21pbwzrd8mlqigbdg3ykz")))

(define-public crate-aws-endpoint-0.49.0 (c (n "aws-endpoint") (v "0.49.0") (d (list (d (n "aws-smithy-http") (r "^0.49.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.49.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.49.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "129fqqg5iq4yyf6md7lzag6ahcr2rgxwv78d9zdn1bbpb27mrwvn")))

(define-public crate-aws-endpoint-0.50.0 (c (n "aws-endpoint") (v "0.50.0") (d (list (d (n "aws-smithy-http") (r "^0.50.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.50.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.50.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05sa3sgf54gq0aha6lfdijjf60nyjdxjk7rmdz68s0nw6n2gi4z6") (y #t)))

(define-public crate-aws-endpoint-0.51.0 (c (n "aws-endpoint") (v "0.51.0") (d (list (d (n "aws-smithy-http") (r "^0.51.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.51.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.51.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1yy3yssncwp9j3hyls3nz4gwld6qypbn3j4dm2m5jr2ghxsg7a3c")))

(define-public crate-aws-endpoint-0.52.0 (c (n "aws-endpoint") (v "0.52.0") (d (list (d (n "aws-smithy-http") (r "^0.52.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.52.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.52.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0wfjlxm0n1xixs0pslg0fni0d3ssi28rw3ir4d9rm9dzh3a7qg95")))

(define-public crate-aws-endpoint-0.53.0 (c (n "aws-endpoint") (v "0.53.0") (d (list (d (n "aws-smithy-http") (r "^0.53.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.53.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.53.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1283cj3xx4nnrnr87jadm064djp998f37ll3rw5y35bxql0grz8k")))

(define-public crate-aws-endpoint-0.54.1 (c (n "aws-endpoint") (v "0.54.1") (d (list (d (n "aws-smithy-http") (r "^0.54.1") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.54.1") (d #t) (k 0)) (d (n "aws-types") (r "^0.54.1") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0q9jj18i1zja0xaa2jzr8xgzznfc9w6ah0k1zpxij6bamcszk940")))

(define-public crate-aws-endpoint-0.55.0 (c (n "aws-endpoint") (v "0.55.0") (d (list (d (n "aws-smithy-http") (r "^0.55.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.55.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.55.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1j5lrj9m0iyff65zfs10qvy35jbnsygh9bmlnadrmcbmb1njff4z")))

(define-public crate-aws-endpoint-0.55.1 (c (n "aws-endpoint") (v "0.55.1") (d (list (d (n "aws-smithy-http") (r "^0.55.1") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.55.1") (d #t) (k 0)) (d (n "aws-types") (r "^0.55.1") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0qni4y3d5hdcpzg6abjfn762lwvd0l9s5jncyy8wl71z7fq4mw47")))

(define-public crate-aws-endpoint-0.55.2 (c (n "aws-endpoint") (v "0.55.2") (d (list (d (n "aws-smithy-http") (r "^0.55.2") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.55.2") (d #t) (k 0)) (d (n "aws-types") (r "^0.55.2") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1b7lk4i7ad5dcr7yhfn4if3fnrydfc5k1nbipflravzllj26ypww")))

(define-public crate-aws-endpoint-0.55.3 (c (n "aws-endpoint") (v "0.55.3") (d (list (d (n "aws-smithy-http") (r "^0.55.3") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.55.3") (d #t) (k 0)) (d (n "aws-types") (r "^0.55.3") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "010b6dldj3z2ql28mza9pvcbyayddyjvksz9vrm75anglr0irklc")))

(define-public crate-aws-endpoint-0.56.0 (c (n "aws-endpoint") (v "0.56.0") (d (list (d (n "aws-smithy-http") (r "^0.56.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.56.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.56.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "06ca3qvsgms3r1pvav603grjxhblvigs1i8f4qpma0nlza8fvlci")))

(define-public crate-aws-endpoint-0.56.1 (c (n "aws-endpoint") (v "0.56.1") (d (list (d (n "aws-smithy-http") (r "^0.56.1") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.56.1") (d #t) (k 0)) (d (n "aws-types") (r "^0.56.1") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1qiay30bir7w27wk8g5dbmv8nx2ymgizl9jid9g2j88lc4zl6wcn")))

(define-public crate-aws-endpoint-0.57.1 (c (n "aws-endpoint") (v "0.57.1") (h "0hx54ydb1fndqs89ikq3bi98sjsy26grpihmvnp1f5rnd9s5mdg1")))

(define-public crate-aws-endpoint-0.57.2 (c (n "aws-endpoint") (v "0.57.2") (h "1zhw9wkpvaz6hw4pn8qs46nrj7vhfzifmlzd78aw76ssdba2rqah")))

(define-public crate-aws-endpoint-0.58.0 (c (n "aws-endpoint") (v "0.58.0") (h "1jxm57p6yw0p1pma4518mshm9rh101j94qybqwaybzkaadbbsj0s")))

(define-public crate-aws-endpoint-0.59.0 (c (n "aws-endpoint") (v "0.59.0") (h "0ybblrwvfdlika9b9vgjb1v60zmvzhf0nxxh6ma3kjnj4j3f9d7y")))

(define-public crate-aws-endpoint-0.60.0 (c (n "aws-endpoint") (v "0.60.0") (h "00wg1gizsckapvgmixq8awa63ww4y5z6sikimd2rjh5n5sf51rpy")))

(define-public crate-aws-endpoint-0.61.0 (c (n "aws-endpoint") (v "0.61.0") (h "0iylh3jg5x7lvijf72fwazp7qman3c556bwqgdzrphgffbzh035h") (y #t)))

(define-public crate-aws-endpoint-0.60.1 (c (n "aws-endpoint") (v "0.60.1") (h "1q774zqwmgvl50iqhi12b38fqxf35qi65ddrlr7axvbvr2gblgfh")))

(define-public crate-aws-endpoint-0.60.2 (c (n "aws-endpoint") (v "0.60.2") (h "0smsk4p6kbx193770cs1pbnvm558qppmm0my9z4yvxps17vdcscc")))

(define-public crate-aws-endpoint-0.60.3 (c (n "aws-endpoint") (v "0.60.3") (h "1jz0hzsiv4kn896adszn6s6nw0lz8i0f80ym5bvvxda3chhpgh7j")))

