(define-module (crates-io aw s- aws-rotate-iam-keys-rs) #:use-module (crates-io))

(define-public crate-aws-rotate-iam-keys-rs-1.0.1 (c (n "aws-rotate-iam-keys-rs") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.46.0") (d #t) (k 0)) (d (n "rusoto_credential") (r "^0.46.0") (d #t) (k 0)) (d (n "rusoto_iam") (r "^0.46.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0xy76x1i6cvppc1yygdsqiknzdr3qp701ylfpdy3yw7zqavsc52a")))

