(define-module (crates-io aw s- aws-codebuild-status_aws) #:use-module (crates-io))

(define-public crate-aws-codebuild-status_aws-0.6.0 (c (n "aws-codebuild-status_aws") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "loggify") (r "^1.1.0") (d #t) (k 0)) (d (n "rusoto_codebuild") (r "^0.39.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.39.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pjymf74x5lclzw4j6h59pgwmma0p1mzg4b4l0gg8654919rxad5")))

