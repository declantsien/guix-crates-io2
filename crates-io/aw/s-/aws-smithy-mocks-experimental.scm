(define-module (crates-io aw s- aws-smithy-mocks-experimental) #:use-module (crates-io))

(define-public crate-aws-smithy-mocks-experimental-0.1.0 (c (n "aws-smithy-mocks-experimental") (v "0.1.0") (d (list (d (n "aws-sdk-s3") (r "^1") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-runtime-api") (r "^1") (f (quote ("client" "http-02x"))) (d #t) (k 0)) (d (n "aws-smithy-types") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1xbiqdwg8yzg2hpaala76iirl2mbg17337xkb2ic1bh40a9wzd85")))

(define-public crate-aws-smithy-mocks-experimental-0.1.1 (c (n "aws-smithy-mocks-experimental") (v "0.1.1") (d (list (d (n "aws-sdk-s3") (r "^1") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-runtime-api") (r "^1") (f (quote ("client" "http-02x"))) (d #t) (k 0)) (d (n "aws-smithy-types") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0afx0qfz2wck74v7dsf2fgsh868cxxfcpl1bh2avdnxzyg2i5gxv")))

(define-public crate-aws-smithy-mocks-experimental-0.2.0 (c (n "aws-smithy-mocks-experimental") (v "0.2.0") (d (list (d (n "aws-sdk-s3") (r "^1") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-runtime-api") (r "^1.2.0") (f (quote ("client" "http-02x"))) (d #t) (k 0)) (d (n "aws-smithy-types") (r "^1.1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "06237i92pl0zyhj79cg6hvifwsl3ca5r7ad00mb4hbkqw5h79fsl")))

(define-public crate-aws-smithy-mocks-experimental-0.2.1 (c (n "aws-smithy-mocks-experimental") (v "0.2.1") (d (list (d (n "aws-sdk-s3") (r "^1") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-runtime-api") (r "^1.3.0") (f (quote ("client" "http-02x"))) (d #t) (k 0)) (d (n "aws-smithy-types") (r "^1.1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0yv14ji1qaj1n2dlnd45mmqwsv4rxmvhzqv7rz6kgkal9wb6j43f")))

