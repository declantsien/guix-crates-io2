(define-module (crates-io aw s- aws-smt-strings) #:use-module (crates-io))

(define-public crate-aws-smt-strings-0.1.0 (c (n "aws-smt-strings") (v "0.1.0") (h "0q0jfvbw574n49fncbyan4p9b4gjixc35v90ikr0gdnaj819adwm")))

(define-public crate-aws-smt-strings-0.2.0 (c (n "aws-smt-strings") (v "0.2.0") (h "01snqh121qycfjck4h37bpr6cj35wmn28p45k4nzjbdmjq8sm7ry") (y #t)))

(define-public crate-aws-smt-strings-0.3.0 (c (n "aws-smt-strings") (v "0.3.0") (h "1shiisa223qfr51wm320vpydp9s9jdbjmamm03b3z5fjgbhypgx2")))

