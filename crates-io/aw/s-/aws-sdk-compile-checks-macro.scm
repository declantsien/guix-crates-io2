(define-module (crates-io aw s- aws-sdk-compile-checks-macro) #:use-module (crates-io))

(define-public crate-aws-sdk-compile-checks-macro-0.1.0 (c (n "aws-sdk-compile-checks-macro") (v "0.1.0") (d (list (d (n "aws-config") (r "^1.5.0") (d #t) (k 2)) (d (n "aws-sdk-sqs") (r "^1.27.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "02shid1yv3g83zr2n3hkz8657dfr3hzkm7bp2yb321iwy69wna64")))

