(define-module (crates-io aw s- aws-smt-ir-derive) #:use-module (crates-io))

(define-public crate-aws-smt-ir-derive-0.1.0 (c (n "aws-smt-ir-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "15r4rnpycdsr4b2g07i5qv5q23kpmnnrxgv16fsx6vk0zf2inx1x")))

(define-public crate-aws-smt-ir-derive-0.1.1 (c (n "aws-smt-ir-derive") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0qmznfbsb2l8dbhfkscggmah7smcw86ysv324arkg23c2wrh9mzm") (y #t)))

(define-public crate-aws-smt-ir-derive-0.1.2 (c (n "aws-smt-ir-derive") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0cbr3w7gwy4gxf48mai8mhdghb3a3lmmcq9rxgfnr0pd3rim4zb1")))

