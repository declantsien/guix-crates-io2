(define-module (crates-io aw s- aws-nitro-enclaves-nsm-api) #:use-module (crates-io))

(define-public crate-aws-nitro-enclaves-nsm-api-0.2.0 (c (n "aws-nitro-enclaves-nsm-api") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "0jlslpd0k1pfs4njpvvbjmqfcgjpjddv8pvc1a1wn7qr6z18l4z9")))

(define-public crate-aws-nitro-enclaves-nsm-api-0.2.1 (c (n "aws-nitro-enclaves-nsm-api") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "09q6kjvzllb0y4hhz875davnzygq5j6l6j2y454l2iql6zskxs0p")))

(define-public crate-aws-nitro-enclaves-nsm-api-0.3.0 (c (n "aws-nitro-enclaves-nsm-api") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "0jsx0iqp56ybgkpkqjrd7fzk39cfnpzqd5mdq6mdsr8cb0r7629n") (f (quote (("default" "nix")))) (r "1.60")))

(define-public crate-aws-nitro-enclaves-nsm-api-0.4.0 (c (n "aws-nitro-enclaves-nsm-api") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)))) (h "028vmnk8y54kcaplnmjn273i32nikqj1nhpaz5x6lgxkf521yb6r") (f (quote (("default" "nix")))) (r "1.63")))

