(define-module (crates-io aw s- aws-athena-parser) #:use-module (crates-io))

(define-public crate-aws-athena-parser-0.1.0 (c (n "aws-athena-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "aws-sdk-athena") (r "^1.19.0") (d #t) (k 0)) (d (n "from-athena-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0csshghpxfaanw7x66jiw17zrzg97vayfvbf3vzbgsf3knw51gnd") (y #t)))

(define-public crate-aws-athena-parser-0.1.1 (c (n "aws-athena-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "aws-sdk-athena") (r "^1.19.0") (d #t) (k 0)) (d (n "from-athena-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1svh6ijwlz62jy9ddd45fnc1q2mlqii1dni3cgzvs5p2kqa98gs1")))

(define-public crate-aws-athena-parser-0.1.2 (c (n "aws-athena-parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "aws-sdk-athena") (r "^1.19.0") (d #t) (k 0)) (d (n "from-athena-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16nrhkss4j4pl6bssb0aaz9zsk9wsmfzj6xadbscai2ni0c4rqyc")))

(define-public crate-aws-athena-parser-0.1.3 (c (n "aws-athena-parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "aws-sdk-athena") (r "^1.19.0") (d #t) (k 0)) (d (n "from-athena-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06rbgpqww6fzvf9hq1aacqxjcs03rz9x14373ainxsd80gykrqy9")))

(define-public crate-aws-athena-parser-0.1.4 (c (n "aws-athena-parser") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "aws-sdk-athena") (r "^1.19.0") (d #t) (k 0)) (d (n "from-athena-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s3hgyrxkfj47k5qq6xrvkq3p15v46kndrian5v1vxry44nm5vr9")))

(define-public crate-aws-athena-parser-0.1.5 (c (n "aws-athena-parser") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "aws-sdk-athena") (r "^1.19.0") (d #t) (k 0)) (d (n "from-athena-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w6x31rmmcv0adjpki0b9i0yh1rkanmnaddkmrivikrhygw4b1fy")))

