(define-module (crates-io aw s- aws-sign-v4) #:use-module (crates-io))

(define-public crate-aws-sign-v4-0.1.0 (c (n "aws-sign-v4") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.2") (d #t) (k 2)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1gq6i5dzrjbj1hlpajlsickwa68v83f6lzcfwchjg9q9i70cf883")))

(define-public crate-aws-sign-v4-0.1.0-1 (c (n "aws-sign-v4") (v "0.1.0-1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.2") (d #t) (k 2)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0lv9z7j9zl0g11fqdgpmx0088k71byjngln3w0yx730clby5k5l7")))

(define-public crate-aws-sign-v4-0.1.1 (c (n "aws-sign-v4") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (d #t) (k 2)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0wa87lbp89f8rcf43wnl279ha9ybqqw16j15kkbccycg59xg6si8")))

(define-public crate-aws-sign-v4-0.2.0 (c (n "aws-sign-v4") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "sha256") (r "^1.1.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0j0fxb76vbs98d48y6r7pvz57ivppbc2gm3isgl62wm3vd68gpwk")))

(define-public crate-aws-sign-v4-0.3.0 (c (n "aws-sign-v4") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.27") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)) (d (n "sha256") (r "^1.5.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1my2w0bhba4f39k9x6s5ik210lzh5cay9admxx72mvs8cv2v2dba")))

