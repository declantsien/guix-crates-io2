(define-module (crates-io aw s- aws-smithy-http-auth) #:use-module (crates-io))

(define-public crate-aws-smithy-http-auth-0.0.0 (c (n "aws-smithy-http-auth") (v "0.0.0") (h "02rhrq4r3a6f1j4ggfhh926ql3ww53i6qpirkzr0cv3045nv6wcc")))

(define-public crate-aws-smithy-http-auth-0.54.3 (c (n "aws-smithy-http-auth") (v "0.54.3") (d (list (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1w8zzddc0324yz036vh2sniaqg700jnly0qbfxrlpqa0iizfy7z6")))

(define-public crate-aws-smithy-http-auth-0.54.4 (c (n "aws-smithy-http-auth") (v "0.54.4") (d (list (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "02i6dcahsz63ghw7anachdzqbqppd1x9zl0lhgp5p119z4abs335")))

(define-public crate-aws-smithy-http-auth-0.55.0 (c (n "aws-smithy-http-auth") (v "0.55.0") (d (list (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1m3kxf9qr5q01j5zch11cykb5pm4nigk8105r502qw5x0d7gqy38")))

(define-public crate-aws-smithy-http-auth-0.55.1 (c (n "aws-smithy-http-auth") (v "0.55.1") (d (list (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "184gnvx57ad48ik78sk75sz5msh7pa7sr2032q1knazdd7hfbvk3")))

(define-public crate-aws-smithy-http-auth-0.55.2 (c (n "aws-smithy-http-auth") (v "0.55.2") (d (list (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "01v7jiwd6n7s0jwj7zxjs0v0dbn38a70p5rkrx4nyp6d07i62n36")))

(define-public crate-aws-smithy-http-auth-0.55.3 (c (n "aws-smithy-http-auth") (v "0.55.3") (d (list (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "0166nw5118zk344l2ng77353jil3p7hxrwc9npswm6szxpacbf6l")))

(define-public crate-aws-smithy-http-auth-0.56.0 (c (n "aws-smithy-http-auth") (v "0.56.0") (d (list (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "09ki9yab3787lsh3rqszh8b0xzfbpgxhibvhziqvsc0mjvhksp79")))

(define-public crate-aws-smithy-http-auth-0.56.1 (c (n "aws-smithy-http-auth") (v "0.56.1") (d (list (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1m4kgqix0xlf34w2m8d3f6ikdx1bka9j5cbdawz1g3f9m70y8cwp")))

(define-public crate-aws-smithy-http-auth-0.57.0 (c (n "aws-smithy-http-auth") (v "0.57.0") (h "0x7xiq9q73kjq5f6isbf9ldd44p758p19rcccn3jnwspanvndvn8")))

(define-public crate-aws-smithy-http-auth-0.57.1 (c (n "aws-smithy-http-auth") (v "0.57.1") (h "0ddi4a1n0dhd8f0nf32vjfr5f8qyp9zwpgn2kx0q3rjjlj380a3s")))

(define-public crate-aws-smithy-http-auth-0.57.2 (c (n "aws-smithy-http-auth") (v "0.57.2") (h "0m8jrh1lkqzfiv1j3d1d87cv9l7rd6hccw5grqa2a9qr5fdqaram")))

(define-public crate-aws-smithy-http-auth-0.58.0 (c (n "aws-smithy-http-auth") (v "0.58.0") (h "0drrlnq3795mgmwwvf4956bb141ikmplsw054rfrzypzs3dkq48w")))

(define-public crate-aws-smithy-http-auth-0.59.0 (c (n "aws-smithy-http-auth") (v "0.59.0") (h "0g8wqb2y3ahn853wixj87fbyy30d0lgh4n8scrl4s3781rnqxzbj")))

(define-public crate-aws-smithy-http-auth-0.60.0 (c (n "aws-smithy-http-auth") (v "0.60.0") (h "1lqmhcjb8k88wp46pa0lnrhg7wrj5y01nylgmici97l4nkxb6w9y")))

(define-public crate-aws-smithy-http-auth-0.61.0 (c (n "aws-smithy-http-auth") (v "0.61.0") (h "0mzn012rrbhdfd93d49vm8gj7rd14asgs62b3wp0wdk02y0qmxp2") (y #t)))

(define-public crate-aws-smithy-http-auth-0.60.1 (c (n "aws-smithy-http-auth") (v "0.60.1") (h "1qp142r66ii7mk3g7kcisyknxnqjvlqffnhwx47ywj14yr0p2il3")))

(define-public crate-aws-smithy-http-auth-0.60.2 (c (n "aws-smithy-http-auth") (v "0.60.2") (h "19jczg51kjl5v8923vvfqhlhk057dmywhb8vi4rhnzb811f9zgpz")))

(define-public crate-aws-smithy-http-auth-0.60.3 (c (n "aws-smithy-http-auth") (v "0.60.3") (h "0s9wj5pslkjsvlpr33nj6nvap6a1p2dv652bnzl1qk0a57rbxvdv")))

