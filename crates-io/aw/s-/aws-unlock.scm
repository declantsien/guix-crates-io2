(define-module (crates-io aw s- aws-unlock) #:use-module (crates-io))

(define-public crate-aws-unlock-0.1.0 (c (n "aws-unlock") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cancellable-timer") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kxyfzqciqmrjqfzjxy0a4jj9aadx5lqdmv936qlwwlwf5y6x8s2")))

