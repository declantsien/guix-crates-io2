(define-module (crates-io aw s- aws-nitro-enclaves-attestation-ffi) #:use-module (crates-io))

(define-public crate-aws-nitro-enclaves-attestation-ffi-0.1.0 (c (n "aws-nitro-enclaves-attestation-ffi") (v "0.1.0") (d (list (d (n "aws-nitro-enclaves-attestation") (r "^0.1.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.18.0") (d #t) (k 1)) (d (n "inline-c") (r "^0.1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.91") (d #t) (k 0)))) (h "0r8pablaryphc6b54npqc9d4mkslh46s8y9r9j2g690618jbj8xa")))

