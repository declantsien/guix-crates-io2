(define-module (crates-io aw s- aws-ip-ranges) #:use-module (crates-io))

(define-public crate-aws-ip-ranges-0.1.0 (c (n "aws-ip-ranges") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "056r6mv3k9718619hndlpxg426gfc11h90pl6dpfms7vs967s3nd")))

(define-public crate-aws-ip-ranges-0.2.0 (c (n "aws-ip-ranges") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "12j5ai84s6mq5pk8lhpb0z20kgkgvxycsppz6wmgzzikas27ih9g")))

(define-public crate-aws-ip-ranges-0.3.0 (c (n "aws-ip-ranges") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "11010jqg5q4pqgg4g5qvb3kc5vgs6c0316wnmn0cix7bn7xbm8i4")))

(define-public crate-aws-ip-ranges-0.4.0 (c (n "aws-ip-ranges") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "116ndh562ffk8r1s4pl227qlpv1r50mycdy5xhjcmfrbs2j0x46q")))

(define-public crate-aws-ip-ranges-0.5.0 (c (n "aws-ip-ranges") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0f9ng5a90ryfdskcf1hq71bdhjrwvr61lrldfwcza40i0lp4kwqn")))

(define-public crate-aws-ip-ranges-0.6.0 (c (n "aws-ip-ranges") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0ndnsdxs1vrc4pf2f7fdm462nba371qfbn1myd8hvi0flq3bws82")))

(define-public crate-aws-ip-ranges-0.7.0 (c (n "aws-ip-ranges") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1fd0dvsw2m64b226dxwx63gzgy4nk7k6lm81i723igy1d5hnp54z")))

(define-public crate-aws-ip-ranges-0.8.0 (c (n "aws-ip-ranges") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0c2x92m6q0mm1w4jqyj1fdrrwcj5sif63l3h45x5a06lfsy6vdal")))

(define-public crate-aws-ip-ranges-0.9.0 (c (n "aws-ip-ranges") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0wzdp13w6ji305cr9nib1pb6zca59rl9mm9frahfhz9bx7iagpsi")))

(define-public crate-aws-ip-ranges-0.10.0 (c (n "aws-ip-ranges") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0cv2ydrib14cc5nxlf2wran3d6il55rr9hhaamw89rkqlf6vkd1p")))

(define-public crate-aws-ip-ranges-0.11.0 (c (n "aws-ip-ranges") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1j679p21w8q1zkk4fvmn7i2lmrp23y6r7d9bkdlcllh5g73iabki")))

(define-public crate-aws-ip-ranges-0.12.0 (c (n "aws-ip-ranges") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "00nckfzlzm63jjfmjz1pv892vm8b43ff678pkabm9mjlmcg50qdi")))

(define-public crate-aws-ip-ranges-0.13.0 (c (n "aws-ip-ranges") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0bl9ig3p5ag3hkmajc0yrrdr0307y7js6d91jkf175jbm9qxf4j4")))

(define-public crate-aws-ip-ranges-0.14.0 (c (n "aws-ip-ranges") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1b4d1s48hw0fjr2qh0kq5hgjcz98iaznna3pb9m4cz2pjarg90r2")))

(define-public crate-aws-ip-ranges-0.15.0 (c (n "aws-ip-ranges") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0lafhhw2q3s7sjmv98fb5s2k5jnn0vzs9xwjiy64nyyy1mh2vhvl")))

(define-public crate-aws-ip-ranges-0.16.0 (c (n "aws-ip-ranges") (v "0.16.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1n8n2jsabqpzgssf50hzji0zkl5j4n2g7ks73hqbwy6d67hzira5")))

(define-public crate-aws-ip-ranges-0.17.0 (c (n "aws-ip-ranges") (v "0.17.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "088d5y617ky2nx2p7g68fd3j865m47dl32hhsyl8s87lm11s64qm")))

(define-public crate-aws-ip-ranges-0.18.0 (c (n "aws-ip-ranges") (v "0.18.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0q02j90rpkijy4vmqpxqyfhnfk52fsqvf9qjcwshyg586j69ywzm")))

(define-public crate-aws-ip-ranges-0.19.0 (c (n "aws-ip-ranges") (v "0.19.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1q5lxchabb0n7iq0h1c0sldw8dfla8fwc27znyzjzmxkwfaxwvr3")))

(define-public crate-aws-ip-ranges-0.20.0 (c (n "aws-ip-ranges") (v "0.20.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "12ln4qlfj3k057s9px8njj7ryjhpg4dvwyr5wnfn5zzx0rp08ynr")))

(define-public crate-aws-ip-ranges-0.21.0 (c (n "aws-ip-ranges") (v "0.21.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "159lfd576h9fzcl6gwinwfk89y0mqxy67cyycbq55mp992x5nkvk")))

(define-public crate-aws-ip-ranges-0.22.0 (c (n "aws-ip-ranges") (v "0.22.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0k0r6sp43hadp20i6l3gpqfkvbvd4v2i0whgcdjm0ninl22gfkr8")))

(define-public crate-aws-ip-ranges-0.23.0 (c (n "aws-ip-ranges") (v "0.23.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1xgm0j427vvrj3shjmaj6gzjcas360l5ildif3ppxgrvg1ndr86x")))

(define-public crate-aws-ip-ranges-0.24.0 (c (n "aws-ip-ranges") (v "0.24.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1qs1ib1s9imyh57hpvc80lfa1kxwxxpnf2vnh1ivh7z1ni33lfar")))

(define-public crate-aws-ip-ranges-0.25.0 (c (n "aws-ip-ranges") (v "0.25.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "120gr32fanf8hima7ij8767zlvsby2a7ljxw2lyvmb1hldkvhfkx")))

(define-public crate-aws-ip-ranges-0.26.0 (c (n "aws-ip-ranges") (v "0.26.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "13rg8460y0yn7cb0hcw9vm2pvmy836kakkmvbqip0pffflxyzxfa")))

(define-public crate-aws-ip-ranges-0.27.0 (c (n "aws-ip-ranges") (v "0.27.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0ss0m6zy327vkp9vlrxsm3ch43mnxaa30npcqcjrl6ld147k19f6")))

(define-public crate-aws-ip-ranges-0.28.0 (c (n "aws-ip-ranges") (v "0.28.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1pwk73c6j2h6zb86056xlnyd9svmgsvr8k0cgvppbm5nsfzk79q2")))

(define-public crate-aws-ip-ranges-0.29.0 (c (n "aws-ip-ranges") (v "0.29.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0vg4x2idqb9hqsmyy21ss533d5bkh1rs5kl7rfr726b4s4wx13ac")))

(define-public crate-aws-ip-ranges-0.30.0 (c (n "aws-ip-ranges") (v "0.30.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1djk1938v9nyc7g6lwx39rlzcp5y81199f6wkbnrbvgr2jdz9dch")))

(define-public crate-aws-ip-ranges-0.31.0 (c (n "aws-ip-ranges") (v "0.31.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0cy4sywlm3aim05hp24jrlm8xrb3j3v1slhix4zdnfx0q53zrl0r")))

(define-public crate-aws-ip-ranges-0.32.0 (c (n "aws-ip-ranges") (v "0.32.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "169pnad1hix94i64hs27478zzpfxwqypx8wa4n7p0zmiciaih2ic")))

(define-public crate-aws-ip-ranges-0.33.0 (c (n "aws-ip-ranges") (v "0.33.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0gvzfrwn49zcv5i0adsicjshwg1cbwn70jrfhcgcr87b0pnch75x")))

(define-public crate-aws-ip-ranges-0.34.0 (c (n "aws-ip-ranges") (v "0.34.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0vqfi5rq6zc4brc3dq4bsg3prv6j35liy2xz5lcxi3iia02iymiq")))

(define-public crate-aws-ip-ranges-0.35.0 (c (n "aws-ip-ranges") (v "0.35.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1d2szg0h635fwxh6167zxv7d5jgv5gz39xb1clpahn9d30x8khgh")))

(define-public crate-aws-ip-ranges-0.36.0 (c (n "aws-ip-ranges") (v "0.36.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1513aa97g706ifgngi38zhsvw6wkf3w29lz8kaqgb559qyd2xwxd")))

(define-public crate-aws-ip-ranges-0.37.0 (c (n "aws-ip-ranges") (v "0.37.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1k1kwsjkqkxciiakc0z8gfjdc6h7khwjg1820kbcigj0j4503apq")))

(define-public crate-aws-ip-ranges-0.38.0 (c (n "aws-ip-ranges") (v "0.38.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1m5841mxdbwwdgx1n17v9v6cyckr1yyak7m08h6s66ing3yllyw8")))

(define-public crate-aws-ip-ranges-0.39.0 (c (n "aws-ip-ranges") (v "0.39.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0i3dlbmq537q0nlc8v34zf0l3l6lh6cfdmvgid6d2wrsfmg44y8d")))

(define-public crate-aws-ip-ranges-0.40.0 (c (n "aws-ip-ranges") (v "0.40.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1l0l858zfy8i9zliy2zm2q4c216fanw2nv8c1vp5jw8hgr7i8wv7")))

(define-public crate-aws-ip-ranges-0.41.0 (c (n "aws-ip-ranges") (v "0.41.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "01kfhlh4br2wn20p1ix29fl8rnadw9rdkxk4mwlwzp6r68sjhhai")))

(define-public crate-aws-ip-ranges-0.42.0 (c (n "aws-ip-ranges") (v "0.42.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1jwm16kq51qiy3l6bl36y2w014vmssjrkrq1crrcd5ys6bn02sag")))

(define-public crate-aws-ip-ranges-0.43.0 (c (n "aws-ip-ranges") (v "0.43.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "11y2yncwdh8bdsmhp7bd6aisnrpm41bb6nk75v676qwbbgmv0h3p")))

(define-public crate-aws-ip-ranges-0.44.0 (c (n "aws-ip-ranges") (v "0.44.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "118xgrzn4b7zsq6mn5rh7jf6gamqhggw36hfxc8h1py28kgqhp60")))

(define-public crate-aws-ip-ranges-0.45.0 (c (n "aws-ip-ranges") (v "0.45.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1l7jy5rk714vgs581g5nlksl89kmis8ahinh2dx0bpyhdj5v08xn")))

(define-public crate-aws-ip-ranges-0.46.0 (c (n "aws-ip-ranges") (v "0.46.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1ywpwca3cqigxxfam5q3acggdzkzw12424rif2w0cy9qllfb86yh")))

(define-public crate-aws-ip-ranges-0.47.0 (c (n "aws-ip-ranges") (v "0.47.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "18fn3zq7s580qyw9rsi3z6x355v9s48hriqw9fhkknxhrh01pnlc")))

(define-public crate-aws-ip-ranges-0.48.0 (c (n "aws-ip-ranges") (v "0.48.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1ikqkymcb3h5a7fmw2c4dzbn2ldchayxvql5l6lzp01l322vv6g7")))

(define-public crate-aws-ip-ranges-0.49.0 (c (n "aws-ip-ranges") (v "0.49.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1xvxrrwzyks59d0bk7aryk6555r7s99yd6yljv48iw6d4f2mxz4k")))

(define-public crate-aws-ip-ranges-0.50.0 (c (n "aws-ip-ranges") (v "0.50.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "101mmzpv84w03j8c87m3dnq1dklzpdxwj9s0yjcy9qwmvspr0ckv")))

(define-public crate-aws-ip-ranges-0.51.0 (c (n "aws-ip-ranges") (v "0.51.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1cnjh4igdachpk65kszmqcpjqmrgb3afiy8zs1zpw60x4j9vj5gq")))

(define-public crate-aws-ip-ranges-0.52.0 (c (n "aws-ip-ranges") (v "0.52.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0jnql88da7h4mrby1h57s81si55m8hxlc5pv7mr705rsgiylr81s")))

(define-public crate-aws-ip-ranges-0.53.0 (c (n "aws-ip-ranges") (v "0.53.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1mcykcpqa3hxp9k4lf0zbaz7pxac1pk7dbwsgfshz3bss5g3a4hf")))

(define-public crate-aws-ip-ranges-0.54.0 (c (n "aws-ip-ranges") (v "0.54.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "08sj73p7spxmvlx5q6i2zz2341s40jpx5vnbspa5h7kf4h85598g")))

(define-public crate-aws-ip-ranges-0.55.0 (c (n "aws-ip-ranges") (v "0.55.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "18dcgyh4sjrgi8i3xshsxy0lds7znf6pxz52gg9wpcdy3fbgxfa1")))

(define-public crate-aws-ip-ranges-0.56.0 (c (n "aws-ip-ranges") (v "0.56.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1jlahvh08ljpsfk1c906kjx30wflx88nrlxr3kcswsi24mfb5qm7")))

(define-public crate-aws-ip-ranges-0.57.0 (c (n "aws-ip-ranges") (v "0.57.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1dwg8b178x03nmdpznp7fm9fwmvqfd92j1a9pj74z5kznqn4yp5w")))

(define-public crate-aws-ip-ranges-0.58.0 (c (n "aws-ip-ranges") (v "0.58.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1aslzi32gcrw7safwdp8raglnapqayagqaq7h6khc7rbnkpwyq95")))

(define-public crate-aws-ip-ranges-0.59.0 (c (n "aws-ip-ranges") (v "0.59.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0pv1dnyr0icr39qfhs4ybpkf098dfb6k5p2sj8lyq4x6wsprz4gg")))

(define-public crate-aws-ip-ranges-0.60.0 (c (n "aws-ip-ranges") (v "0.60.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0x7wb02alipkmp7h38mpa9qzdfp0smf8ghawk2h6c7zphadacbdw")))

(define-public crate-aws-ip-ranges-0.61.0 (c (n "aws-ip-ranges") (v "0.61.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "00079bziz4g4vd28m30ghvypvbn6k99z09vn4mxakk947crzvk1f")))

(define-public crate-aws-ip-ranges-0.62.0 (c (n "aws-ip-ranges") (v "0.62.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0drl2c1j4zs2cvkawiyyh444c22pxcfw788f5nz7icsl0xfr0vpv")))

(define-public crate-aws-ip-ranges-0.63.0 (c (n "aws-ip-ranges") (v "0.63.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "12a7aqmlnarz5ysiw432vvvvdp05dvdjalc4rvh0a72h04xdqzb5")))

(define-public crate-aws-ip-ranges-0.64.0 (c (n "aws-ip-ranges") (v "0.64.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0r15q79aplrzqwppl1aglwbmfxg8ipnckq5yxp8gxz2460mxigsi")))

(define-public crate-aws-ip-ranges-0.65.0 (c (n "aws-ip-ranges") (v "0.65.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0c1f3awqjif75acc7v9chvl1dp92mjw68qdspkj8yicw4rpm6h4v")))

(define-public crate-aws-ip-ranges-0.66.0 (c (n "aws-ip-ranges") (v "0.66.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "07dm2dg3c4qi7zz3nfkqwzjjy16jasja05xd44l6mxqa6ginz8bl")))

(define-public crate-aws-ip-ranges-0.67.0 (c (n "aws-ip-ranges") (v "0.67.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0y85hg3f9zj3ab7a0h2sd363wn9sgm89pbg0qgqn9lhn0r1i0way")))

(define-public crate-aws-ip-ranges-0.68.0 (c (n "aws-ip-ranges") (v "0.68.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "05vqfczs04c854wfaarkva6xrz1dpfjbdxk2nqdfdb63ixp7lxmg")))

(define-public crate-aws-ip-ranges-0.69.0 (c (n "aws-ip-ranges") (v "0.69.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1294vgv3vy08fj7kfyg3qf6axcqhd7nivbyd1346jvmac05nwaf0")))

(define-public crate-aws-ip-ranges-0.70.0 (c (n "aws-ip-ranges") (v "0.70.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0wcv78r6a50nsr2x9131x6i1nmghn9kwyc8h53dczannvj9cz9nr")))

(define-public crate-aws-ip-ranges-0.71.0 (c (n "aws-ip-ranges") (v "0.71.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0666jl2ff3mp17sxacnmxl2hzvb5m5mlwxcabwilmqanqbz07k73")))

(define-public crate-aws-ip-ranges-0.72.0 (c (n "aws-ip-ranges") (v "0.72.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0nha9bd6al3hf438s2dsl1acsb8r5dw0lp81js4sawlmnqjldy3w")))

(define-public crate-aws-ip-ranges-0.73.0 (c (n "aws-ip-ranges") (v "0.73.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1355p5snd5crmc7b3pvzgw6sph05c1m7ry0nm0d42djcaj31p44w")))

(define-public crate-aws-ip-ranges-0.74.0 (c (n "aws-ip-ranges") (v "0.74.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0s9grmn19bg86ba1qswljq18hj96fly57rmrx2wb0nrdsfx6d7qs")))

(define-public crate-aws-ip-ranges-0.75.0 (c (n "aws-ip-ranges") (v "0.75.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1r2z6wam29kpkrkiblynfd38ri1p9fiwxbr45zgixb3yrqqd48xh")))

(define-public crate-aws-ip-ranges-0.76.0 (c (n "aws-ip-ranges") (v "0.76.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1ql6lwj7r34rmpl9wwdysybslfwykwgdz78f2hh1wjjvkc4b5x6p")))

(define-public crate-aws-ip-ranges-0.77.0 (c (n "aws-ip-ranges") (v "0.77.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0k8hxd27pqzq96lilb55370nlishg1fig9lfnzfd2k5zkj4dxsrc")))

(define-public crate-aws-ip-ranges-0.78.0 (c (n "aws-ip-ranges") (v "0.78.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0rj6jlai0vrwls16hax9rifh0fh9qvhji4vksd1qbyhr0gy5071f")))

(define-public crate-aws-ip-ranges-0.79.0 (c (n "aws-ip-ranges") (v "0.79.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "16hhvsgsaynkqp45d6gcw9phg9w9sji8xwsi7sm7iyw0k74405dc")))

(define-public crate-aws-ip-ranges-0.80.0 (c (n "aws-ip-ranges") (v "0.80.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "12hx5nh3lxxvggd383kny388q1n4qi07yc9c2pzyaqmil2hx0b6l")))

(define-public crate-aws-ip-ranges-0.81.0 (c (n "aws-ip-ranges") (v "0.81.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0d31m32bm2zrvq1ixvkmxbqspyf5cqg18zahm639skbcs7lsqmfx")))

(define-public crate-aws-ip-ranges-0.82.0 (c (n "aws-ip-ranges") (v "0.82.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1xnaa8hagig9fflksy5ri2z96w43mxsrmvw4xxc7hw7mq3hnils0")))

(define-public crate-aws-ip-ranges-0.83.0 (c (n "aws-ip-ranges") (v "0.83.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1lhs9q6l33hpgs3qnc5jb585mzvmqxq4d1zjzr16kcfj954zw4c3")))

(define-public crate-aws-ip-ranges-0.84.0 (c (n "aws-ip-ranges") (v "0.84.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1wkhh4nx7l5p2m7ml2k0dfnf4mvjn1xc8c1ybf5xpb4sr4jms7a0")))

(define-public crate-aws-ip-ranges-0.85.0 (c (n "aws-ip-ranges") (v "0.85.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1slpk8cm0xj2kw1x6s1p4l0w5sxw1j2cibj2n9bsvw5h2bpak9kl")))

(define-public crate-aws-ip-ranges-0.86.0 (c (n "aws-ip-ranges") (v "0.86.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0bb9219ggzpj5q16d2nbk3dpbmbqiribf74q9n92lil12475chn8")))

(define-public crate-aws-ip-ranges-0.87.0 (c (n "aws-ip-ranges") (v "0.87.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1vy466p0iq2hbchhch2z9q8yah7xc8kypl2kiyw6pigfrllwd5rr")))

(define-public crate-aws-ip-ranges-0.88.0 (c (n "aws-ip-ranges") (v "0.88.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1wmks0vdbwjkk0b4xvyn69j8mc58rhiz03f16fylnjhwvv53p2xd")))

(define-public crate-aws-ip-ranges-0.89.0 (c (n "aws-ip-ranges") (v "0.89.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "01bchzfc53g95py7m3vzwbvq3zpv269yxhxnic07sg45kxzbr29k")))

(define-public crate-aws-ip-ranges-0.90.0 (c (n "aws-ip-ranges") (v "0.90.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0m2gy8d4hll22aiw5pnb9zpl1svdfm1i8jfw9yndyzqhkb016hjv")))

(define-public crate-aws-ip-ranges-0.91.0 (c (n "aws-ip-ranges") (v "0.91.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0780vnbyjjyby4pymbbxw4qs3blyl2h086ac5sdxqafixds863cr")))

(define-public crate-aws-ip-ranges-0.92.0 (c (n "aws-ip-ranges") (v "0.92.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "091rrws1xqj6kpdfbkrrrp0k5kkzr8vngxv8lvyv06cv89dkgqa9")))

(define-public crate-aws-ip-ranges-0.93.0 (c (n "aws-ip-ranges") (v "0.93.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "07zywlnj04pk1ng1a0hnh5wxmc2471p4hvp9y5af59v02lmypvkq")))

(define-public crate-aws-ip-ranges-0.94.0 (c (n "aws-ip-ranges") (v "0.94.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0pxml3a5ws4ia19jc40y13l03aflkbshp3hxmgjwbsb9r3jcxzpc")))

(define-public crate-aws-ip-ranges-0.95.0 (c (n "aws-ip-ranges") (v "0.95.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "06k0ikdq88l4wymv6b836f86z9il60fzd9hvgzgm0g62iszk3qv4")))

(define-public crate-aws-ip-ranges-0.96.0 (c (n "aws-ip-ranges") (v "0.96.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1cgpwwrhl2whgn3zfhz42zqhjq182wdk36b5c2lh5kkqyw85vs4y")))

(define-public crate-aws-ip-ranges-0.97.0 (c (n "aws-ip-ranges") (v "0.97.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "045fgbciz9ykkambc9jvqd76q358rcnqdbzqsxmj0wvj1hfl88sc")))

(define-public crate-aws-ip-ranges-0.98.0 (c (n "aws-ip-ranges") (v "0.98.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "00kdnpb6nng4b2nnyg60pwm6lnvhqkkhhzficz7m54466c7av4rp")))

(define-public crate-aws-ip-ranges-0.99.0 (c (n "aws-ip-ranges") (v "0.99.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0fshwx496z489ddgd9d80jn70pk82rc8p0ajisp3fzv6ciq2ikpf")))

(define-public crate-aws-ip-ranges-0.100.0 (c (n "aws-ip-ranges") (v "0.100.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0hzdgyqx3dqr54i78db4nz0hr2qrjkrmx609i91xy3kwxy9p2g5k")))

(define-public crate-aws-ip-ranges-0.101.0 (c (n "aws-ip-ranges") (v "0.101.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1r32dhjskglr5b2w1slaafylkwz63087hnp5zn6h27cpjyy22cc6")))

(define-public crate-aws-ip-ranges-0.102.0 (c (n "aws-ip-ranges") (v "0.102.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "09jxbpfy7qjdib47jysqvbdl1g33jh24ggsaa6ybyylbbbhb3qds")))

(define-public crate-aws-ip-ranges-0.103.0 (c (n "aws-ip-ranges") (v "0.103.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "19zvzwns2ln6zlf9ijw89b8m12njmjxdc2faki1n9h64jq75qibj")))

(define-public crate-aws-ip-ranges-0.104.0 (c (n "aws-ip-ranges") (v "0.104.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0x4rq09yfhgq8m9a5zs9qj39jvffmmwqykyn5rhy186652qzxy4b")))

(define-public crate-aws-ip-ranges-0.105.0 (c (n "aws-ip-ranges") (v "0.105.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "16czi6kj3agd31zdw9advgrv42g5gkjhyg2n3vymzns9fzirm39y")))

(define-public crate-aws-ip-ranges-0.106.0 (c (n "aws-ip-ranges") (v "0.106.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "133p0bf5mz6223sc51lww8kkal5v86larlfgpdmwb8wkhm7a4akr")))

(define-public crate-aws-ip-ranges-0.107.0 (c (n "aws-ip-ranges") (v "0.107.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1dz0iq4xg665n0l1d40d6jkn5h2y3v8j61998yisq6rldl6qiqac")))

(define-public crate-aws-ip-ranges-0.108.0 (c (n "aws-ip-ranges") (v "0.108.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "060rvmq4a9yzdq1r2r1vw4jld9gy9am6cgznw2wgrx3saz4m496l")))

(define-public crate-aws-ip-ranges-0.109.0 (c (n "aws-ip-ranges") (v "0.109.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "00djs70bak1zr1cgv631fw7n1k5r3nl973fn1drdslxlc1p011qr")))

(define-public crate-aws-ip-ranges-0.110.0 (c (n "aws-ip-ranges") (v "0.110.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1fbf0ajl69n1j5bpzmchspaq15ilk6y1905vkkzbfvxic41bj81g")))

(define-public crate-aws-ip-ranges-0.111.0 (c (n "aws-ip-ranges") (v "0.111.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "122cg6cpa015v8nd54pspbxzw39k06iq2s6bs0xs2f1scvq0hqv9")))

(define-public crate-aws-ip-ranges-0.112.0 (c (n "aws-ip-ranges") (v "0.112.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1c72ml3wzlm7pqs7bzkwrhwdq5akkdn06kckwx8h6caqfv303ni2")))

(define-public crate-aws-ip-ranges-0.113.0 (c (n "aws-ip-ranges") (v "0.113.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0yqcw4p5ak88pyf5wysyrs8x8px1ymasw93jryslxpf04jx4jjfd")))

(define-public crate-aws-ip-ranges-0.114.0 (c (n "aws-ip-ranges") (v "0.114.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0axiq7zbx32mki8hd1d927cklbdsjnn022pj2syr8swwwk3759b4")))

(define-public crate-aws-ip-ranges-0.115.0 (c (n "aws-ip-ranges") (v "0.115.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1ss92c6w7bysbh4j6kdgimzdw8dasfscai1bfd27yhiagxw345pm")))

(define-public crate-aws-ip-ranges-0.116.0 (c (n "aws-ip-ranges") (v "0.116.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1p02j8mp2r436x8z35l1v8ahknzghqmi6swwc4d0h1x7h1k8k74r")))

(define-public crate-aws-ip-ranges-0.117.0 (c (n "aws-ip-ranges") (v "0.117.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0c1bf48pxjj6yjzaii8qdxgmyi748g4gk0h0bxh1kagrwvycdpxd")))

(define-public crate-aws-ip-ranges-0.118.0 (c (n "aws-ip-ranges") (v "0.118.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1jz4gi199kkzwqxp1j2m8xhdl8lbblmv0372mqhmbxcnw3c0ci4q")))

(define-public crate-aws-ip-ranges-0.119.0 (c (n "aws-ip-ranges") (v "0.119.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "19qhf9vfyzjv6hcp39kds4fbs9l4djqw0r67p7666cba1nblf2v5")))

(define-public crate-aws-ip-ranges-0.120.0 (c (n "aws-ip-ranges") (v "0.120.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0p0y3wpkc2vclg8gpp1mzrkcg5y3ws0j62kgvdid2dvdw4c5yk7w")))

(define-public crate-aws-ip-ranges-0.121.0 (c (n "aws-ip-ranges") (v "0.121.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0wz45hyl5axsdypfkvix99hn2z3711y7gyd6bjifb0z1bmif9gz6")))

(define-public crate-aws-ip-ranges-0.122.0 (c (n "aws-ip-ranges") (v "0.122.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "12d320x96jxdzsyhq2rny0pqvw8jy43hvk64av51xbwz37c7l75l")))

(define-public crate-aws-ip-ranges-0.123.0 (c (n "aws-ip-ranges") (v "0.123.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0xlhs0yrs63zp98lz644fzh0di0z301qkskhb8rpvpmh4fkh9afc")))

(define-public crate-aws-ip-ranges-0.124.0 (c (n "aws-ip-ranges") (v "0.124.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1yzpdhcxkpsafjj1nfg07jfsbmcj595bj36cdvmcdm1r89qjj4dp")))

(define-public crate-aws-ip-ranges-0.125.0 (c (n "aws-ip-ranges") (v "0.125.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0544wcqk47bcjpr2pmhk5mznpkk9l3mk5ckskhx99x0cfy9cv3pj")))

(define-public crate-aws-ip-ranges-0.126.0 (c (n "aws-ip-ranges") (v "0.126.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0dzb1qvx5qcrrsw6bnr9xlir3jsgsn6x2hjr2ivn5x2a2gbxsvnm")))

(define-public crate-aws-ip-ranges-0.127.0 (c (n "aws-ip-ranges") (v "0.127.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1pkyfsapbyvf6ybpd79108wym1f034mp90chijf5zbnq0zwj2pvn")))

(define-public crate-aws-ip-ranges-0.128.0 (c (n "aws-ip-ranges") (v "0.128.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0yksl4wdlz4p71rf121mhm4fc0f1azls52ws2q2q2scwb0gj8qzl")))

(define-public crate-aws-ip-ranges-0.129.0 (c (n "aws-ip-ranges") (v "0.129.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "17frrlzxl2j14sg2kdxggfji7393dhmvhawqhj1r3605yr4x4x6w")))

(define-public crate-aws-ip-ranges-0.130.0 (c (n "aws-ip-ranges") (v "0.130.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0akbvgqy2kpdpl4hlkch78qprmc7965c48r1jdc7phis8nh5pkya")))

(define-public crate-aws-ip-ranges-0.131.0 (c (n "aws-ip-ranges") (v "0.131.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0dl01nl5hxac57lfxxc106l4gbpsq9g9pblvdg1qlnjc2g4lzcms")))

(define-public crate-aws-ip-ranges-0.132.0 (c (n "aws-ip-ranges") (v "0.132.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "06l3z7sik194sjy81yc7l01cvkclgm9ji7d9r3v7ghkca2mbfmbr")))

(define-public crate-aws-ip-ranges-0.133.0 (c (n "aws-ip-ranges") (v "0.133.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0i41ri62ch9k40nppkxndz6xxqg2dr68gmzzg15isx28q5948bl2")))

(define-public crate-aws-ip-ranges-0.134.0 (c (n "aws-ip-ranges") (v "0.134.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0n1fiaxiyccdccibcyb99spxikz1mdfx507cf49cjs9mw6s3j1hd")))

(define-public crate-aws-ip-ranges-0.135.0 (c (n "aws-ip-ranges") (v "0.135.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0fsv0zv8jyq0anjj7lnhwjk38lr5y4a0d41vyzfkdycg9c36wanr")))

(define-public crate-aws-ip-ranges-0.136.0 (c (n "aws-ip-ranges") (v "0.136.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1jk7kf4j29dv94wkniwca276knp784709hf1p0vcpbjrparnw6dz")))

(define-public crate-aws-ip-ranges-0.137.0 (c (n "aws-ip-ranges") (v "0.137.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "18iaidx7vzdi4bxzqqf5hvxs18zl7vdblpgncvga4pibr6846ikh")))

(define-public crate-aws-ip-ranges-0.138.0 (c (n "aws-ip-ranges") (v "0.138.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0zag3ax4s6r3zip9zyh12gal043ngxprs0vrp07y1qa7sfa3z9fq")))

(define-public crate-aws-ip-ranges-0.139.0 (c (n "aws-ip-ranges") (v "0.139.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0zkn50mlgfkcsfwdqgv85r1nikl3hsijl49xqank7nls7qnz4rsk")))

(define-public crate-aws-ip-ranges-0.140.0 (c (n "aws-ip-ranges") (v "0.140.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "010sk1rjk2l1q2vcgw0h85ipdifi1xi5pqb0vd0fv3xfg8mlmfr5")))

(define-public crate-aws-ip-ranges-0.141.0 (c (n "aws-ip-ranges") (v "0.141.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "01h71mmgp8zpd71cy3nyyi3djr4f4nz8sfx1zvcrgang5yvp25n1")))

(define-public crate-aws-ip-ranges-0.142.0 (c (n "aws-ip-ranges") (v "0.142.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "13nwq9imjg6byx4bkw2ldl2866ikx248k3q74ai3ji03xz7d2m3w")))

(define-public crate-aws-ip-ranges-0.143.0 (c (n "aws-ip-ranges") (v "0.143.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0q40i50q2nhyg8176s81549yd14f5nc41in9vmpl0idx7m9c57qw")))

(define-public crate-aws-ip-ranges-0.144.0 (c (n "aws-ip-ranges") (v "0.144.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1gplgq049nkx9i7lha6ggqlpzv69j6pm9ps85xsakk6f98xm2sci")))

(define-public crate-aws-ip-ranges-0.145.0 (c (n "aws-ip-ranges") (v "0.145.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1bwcqz4ny8rd4g1c0wm5mv87bq34s80h1jmpw54w553xjkab535p")))

(define-public crate-aws-ip-ranges-0.146.0 (c (n "aws-ip-ranges") (v "0.146.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "18b0jg4lhwf5flnq3zyrw0927xd7zy3rq9vx485xa4gk00ynjjjl")))

(define-public crate-aws-ip-ranges-0.147.0 (c (n "aws-ip-ranges") (v "0.147.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1h9pfa5gyr55dycs3v8q0zans3qmg96kw0kmkw9wdlapxzvr8vzs")))

(define-public crate-aws-ip-ranges-0.148.0 (c (n "aws-ip-ranges") (v "0.148.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "02dihgwy10cnyx6dh8yw347ljwci7im8209kaj92vhy4i3w4783z")))

(define-public crate-aws-ip-ranges-0.149.0 (c (n "aws-ip-ranges") (v "0.149.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1r4ciq6nzp8iqm3n1x2hcs02101gc5hvqsgnld9br7sziqx0fy3w")))

(define-public crate-aws-ip-ranges-0.150.0 (c (n "aws-ip-ranges") (v "0.150.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1vy4ibilgr3wdhs9dinhjjdv1sra97cgcyiiv9cfwwg73a0f4swn")))

(define-public crate-aws-ip-ranges-0.151.0 (c (n "aws-ip-ranges") (v "0.151.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "193z060a5vls3p9jlp58ba324wj2vva275d9qivnvw4a2px180ah")))

(define-public crate-aws-ip-ranges-0.152.0 (c (n "aws-ip-ranges") (v "0.152.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1na90d1pdvginwdnkp30rq5amsfb27zg5hpznha0jwjs5ckbdwm5")))

(define-public crate-aws-ip-ranges-0.153.0 (c (n "aws-ip-ranges") (v "0.153.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "13z4a999ylwj08p4mf0q31pfr3dh50c77053lm5gjikhbljxnp5f")))

(define-public crate-aws-ip-ranges-0.154.0 (c (n "aws-ip-ranges") (v "0.154.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1jwgcn865b55pnkgvg710sqmy9yxy21hfx6dk3i8j5190hb5x8xk")))

(define-public crate-aws-ip-ranges-0.155.0 (c (n "aws-ip-ranges") (v "0.155.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1h6ipc229rhkgjbbd944wadnm2md8rankmgsk59j6ysk18s6gfic")))

(define-public crate-aws-ip-ranges-0.156.0 (c (n "aws-ip-ranges") (v "0.156.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0xdwa7cq02lzr450mndz15wirb7i3n16s01rkmqd9l659rxckn3a")))

(define-public crate-aws-ip-ranges-0.157.0 (c (n "aws-ip-ranges") (v "0.157.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "00f9slxq3da9n28q7xig9paz5x9iii8658qr7wc3qlkndb0s1b0h")))

(define-public crate-aws-ip-ranges-0.158.0 (c (n "aws-ip-ranges") (v "0.158.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0z3g2ikdb7mgscs716zyf33s4i3gvjfvjgkaqiq7dry63af19ngs")))

(define-public crate-aws-ip-ranges-0.159.0 (c (n "aws-ip-ranges") (v "0.159.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "104w256f8wvfmd0pj5xgch75vlvrwrjawrr5wyz47vglszp92j55")))

(define-public crate-aws-ip-ranges-0.160.0 (c (n "aws-ip-ranges") (v "0.160.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0flxfqslsd408gq85lzg48pzrii36a5i2ag16myxhnr3lgb93n12")))

(define-public crate-aws-ip-ranges-0.161.0 (c (n "aws-ip-ranges") (v "0.161.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0nrl1nry2dasx5q3n0myaarfqw5s1pi538plns7mdrmgpyr9pngz")))

(define-public crate-aws-ip-ranges-0.162.0 (c (n "aws-ip-ranges") (v "0.162.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "04lw434b1vvcbhx0vp8amq0bwn7p3cr8kwwlmfrc71y85rkpq0r5")))

(define-public crate-aws-ip-ranges-0.163.0 (c (n "aws-ip-ranges") (v "0.163.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0h6my210xi863b2g61qqz56xmd7m4qng9y35h7x8bxmjzq2did31")))

(define-public crate-aws-ip-ranges-0.164.0 (c (n "aws-ip-ranges") (v "0.164.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0q6hmqm2ij77sqjrqrm4sm2jcs6as7sjxvx8qkwbfnr159x85nqd")))

(define-public crate-aws-ip-ranges-0.165.0 (c (n "aws-ip-ranges") (v "0.165.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1wqgw0w5qipb833sy28nq5xaq8jyx5fbh13pj451y714jvarlljd")))

(define-public crate-aws-ip-ranges-0.166.0 (c (n "aws-ip-ranges") (v "0.166.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0h5yw8jc39w3pns4sn028g0xqmpbygffa0i74wzycr00q03dsz1l")))

(define-public crate-aws-ip-ranges-0.167.0 (c (n "aws-ip-ranges") (v "0.167.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "03vn5a656ry8rwxbchix2y4lf25mr9iwf44vwh8vxz4q8z0bm7nc")))

(define-public crate-aws-ip-ranges-0.168.0 (c (n "aws-ip-ranges") (v "0.168.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "19m3xwz5bfxail1j2aqpbh7wmpcfb1882p1yrwy80ksq4d6rnnfi")))

(define-public crate-aws-ip-ranges-0.169.0 (c (n "aws-ip-ranges") (v "0.169.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0c1bjjka3mynr3jf496919skp4w80cb85rv2mqh0mh1jmkbp43fn")))

(define-public crate-aws-ip-ranges-0.170.0 (c (n "aws-ip-ranges") (v "0.170.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "07d2d512vwjd3gxp3qlpx7yhf4l7wmcvzjhzqin0w08clhrcsdfg")))

(define-public crate-aws-ip-ranges-0.171.0 (c (n "aws-ip-ranges") (v "0.171.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0nacglzdhm0j0akjb5kmfn1yf85szi4zs267h36bixhz6ak4p3k8")))

(define-public crate-aws-ip-ranges-0.172.0 (c (n "aws-ip-ranges") (v "0.172.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1b58m9m2s2fb6wpz8m6113ac5sg94xfmwfr14y200h6zaslqkg8d")))

(define-public crate-aws-ip-ranges-0.173.0 (c (n "aws-ip-ranges") (v "0.173.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0isrva32wnpa69xrr544pjxm9swjdaz5afjdjlgmjbcqpzhrby57")))

(define-public crate-aws-ip-ranges-0.174.0 (c (n "aws-ip-ranges") (v "0.174.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0prdf0yvp5sxiq57hazx429nznlpm61d0xc1k89y76bd4zjrinr1")))

(define-public crate-aws-ip-ranges-0.175.0 (c (n "aws-ip-ranges") (v "0.175.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0kssgi338pzr3nk0655a1kf4l5drp88kfklrv00f4w2d8ikmmjc1")))

(define-public crate-aws-ip-ranges-0.176.0 (c (n "aws-ip-ranges") (v "0.176.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "12zkrmd04q7ck0z1vpzn91pgp5nl2bxkj80mhhimzm8mg459h04j")))

(define-public crate-aws-ip-ranges-0.177.0 (c (n "aws-ip-ranges") (v "0.177.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0g87xfs8i2rbqd7iy0mkbxm8b6mgixxyipd6mw6wl19nmx512gra")))

(define-public crate-aws-ip-ranges-0.178.0 (c (n "aws-ip-ranges") (v "0.178.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "18cjrl5rg0s12c27086slphw4ykkspip9awgza2l72ncgc4jl0gq")))

(define-public crate-aws-ip-ranges-0.179.0 (c (n "aws-ip-ranges") (v "0.179.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1kpk042pbqxid23yizkhw1mas84185g89byassriwf6bgfa9zjhg")))

(define-public crate-aws-ip-ranges-0.180.0 (c (n "aws-ip-ranges") (v "0.180.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0j6hzqkqg8766d2rsc5wbhw8sakncdvhx10fdyllnkz5288w786c")))

(define-public crate-aws-ip-ranges-0.181.0 (c (n "aws-ip-ranges") (v "0.181.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1mcj41p40ijfwx1lj9wm83zbxdwld4x6pfkdwkcia7zj0fr4i08w")))

(define-public crate-aws-ip-ranges-0.183.0 (c (n "aws-ip-ranges") (v "0.183.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0kjiz9ay5f2fyxv51pys0xiwf50w3khpdam8qy6pj635rb2z8pfs")))

(define-public crate-aws-ip-ranges-0.184.0 (c (n "aws-ip-ranges") (v "0.184.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "08lgycn0ca4radv8b5ma6ap322f5kckxw3aza154mb0x7p4ykj46")))

(define-public crate-aws-ip-ranges-0.185.0 (c (n "aws-ip-ranges") (v "0.185.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0hakxm555nvzaicldhkrxfsfngn9q1ms8hfj91i6m0wxy4ril6sv")))

(define-public crate-aws-ip-ranges-0.186.0 (c (n "aws-ip-ranges") (v "0.186.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1mgk90nlpbfjlydyv628prl3xwrgyqajj1swxqdrglp40b3jkh66")))

(define-public crate-aws-ip-ranges-0.187.0 (c (n "aws-ip-ranges") (v "0.187.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "08a9ig827x6d502675crl30v57rh70g2zm7m2nkxnlk992alhgbh")))

(define-public crate-aws-ip-ranges-0.188.0 (c (n "aws-ip-ranges") (v "0.188.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "04cd38ij8p0cgyjs3md0s989cjsissahfqn1vhrfd51rwhjgk80q")))

(define-public crate-aws-ip-ranges-0.189.0 (c (n "aws-ip-ranges") (v "0.189.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1m69jg4rlq0w1a72jiaxnmb54isk236023zph4m1ncarwfh27r94")))

(define-public crate-aws-ip-ranges-0.190.0 (c (n "aws-ip-ranges") (v "0.190.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0mkfwa6rhbi8rc8wpf2agrmky16ylslhlfxvyz8d1wffsgd3h2zz")))

(define-public crate-aws-ip-ranges-0.191.0 (c (n "aws-ip-ranges") (v "0.191.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0vr48gc912qsqgfbqfmzgsaalcha20grr46vlv6085wpd4859xpp")))

(define-public crate-aws-ip-ranges-0.192.0 (c (n "aws-ip-ranges") (v "0.192.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0dw2f45ya2mziy34vkrgb4n59d226fd722s04jxwgik8340266y0")))

(define-public crate-aws-ip-ranges-0.193.0 (c (n "aws-ip-ranges") (v "0.193.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0qwx4ikbbmk0xld21v3vv5hvp0j8kkkdjjqyff4n3w62g0pphyq7")))

(define-public crate-aws-ip-ranges-0.194.0 (c (n "aws-ip-ranges") (v "0.194.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0xxfnkqlsg8cp29r5nzq1h9bpq0rhdcnbr53dpwjs1kawa7s9d69")))

(define-public crate-aws-ip-ranges-0.195.0 (c (n "aws-ip-ranges") (v "0.195.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "14zvmhz7y2r1l8jia9y5px91vc9pfb731y2qv71s7qpb3rrhsq0p")))

(define-public crate-aws-ip-ranges-0.196.0 (c (n "aws-ip-ranges") (v "0.196.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0vyk78s87l89yy177rby261y8phwkq3r53qfz1cdcghnj6nr6lzi")))

(define-public crate-aws-ip-ranges-0.197.0 (c (n "aws-ip-ranges") (v "0.197.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1hpfic1pmq2hpxhrfamq935cvr43sppp1agagml23lijbkjkk2q7")))

(define-public crate-aws-ip-ranges-0.198.0 (c (n "aws-ip-ranges") (v "0.198.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1ix4jzidn71c668lh7kfwddl336hi70fi5abqhnd1fzm27lsg30b")))

(define-public crate-aws-ip-ranges-0.199.0 (c (n "aws-ip-ranges") (v "0.199.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1idbfxscz2p2x73ywc8biklv9ccrsifyfblhzk2bj1kwr6br2p3f")))

(define-public crate-aws-ip-ranges-0.200.0 (c (n "aws-ip-ranges") (v "0.200.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1ggdwlb3a298j6bajm5lk90p2zb0j6l21xmz44iis9yrjqxw8vah")))

(define-public crate-aws-ip-ranges-0.201.0 (c (n "aws-ip-ranges") (v "0.201.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "19lj93jaqdi9bj22cgy4klcfpd4r09j9qywp1kzwl420bbbckdmz")))

(define-public crate-aws-ip-ranges-0.202.0 (c (n "aws-ip-ranges") (v "0.202.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "01k2h5i9irrasbdwc7nfn8m1kbxhmnndarz1217k7f5yw0zp3ssq")))

(define-public crate-aws-ip-ranges-0.203.0 (c (n "aws-ip-ranges") (v "0.203.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0h3hd46s6hanamsa8mf9l6ca8fc6j16zy5rk3darhb94mshx4hah")))

(define-public crate-aws-ip-ranges-0.204.0 (c (n "aws-ip-ranges") (v "0.204.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1pvpsrbrvc1fs5v8rq18haf7d7dk5352avl8vap22z4irc2v47qr")))

(define-public crate-aws-ip-ranges-0.205.0 (c (n "aws-ip-ranges") (v "0.205.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0x8p4l2v5ja157q30mas2hcs60vq00zbbygs9jwgsg7x2vp0s3jm")))

(define-public crate-aws-ip-ranges-0.206.0 (c (n "aws-ip-ranges") (v "0.206.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "12yjdw2zhc1f0b7j3a8658qxymqydlq4hrx71bq7ra11vyjb41jc")))

(define-public crate-aws-ip-ranges-0.207.0 (c (n "aws-ip-ranges") (v "0.207.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0zq9pi3wwncddc9fxkhb01dgsbf9399g31yfsrqzq6nclb9mpkiz")))

(define-public crate-aws-ip-ranges-0.208.0 (c (n "aws-ip-ranges") (v "0.208.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0irq47hp8c857lby182vhv89x17bxwdxfxmpwmx49236gvh5xkz0")))

(define-public crate-aws-ip-ranges-0.209.0 (c (n "aws-ip-ranges") (v "0.209.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0ifxvq3y436774dg5i32axn6jd8spm3p9n7q883jqzli75wiv11s")))

(define-public crate-aws-ip-ranges-0.210.0 (c (n "aws-ip-ranges") (v "0.210.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "111s8fzsqfw6ljz15rx9361x89xjic518f27qghlsaw524kizz9a")))

(define-public crate-aws-ip-ranges-0.211.0 (c (n "aws-ip-ranges") (v "0.211.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "13jhwm4ixl3hfbmi1yic63mcj4976rh0irr8nfb6ng2i9f22crlj")))

(define-public crate-aws-ip-ranges-0.212.0 (c (n "aws-ip-ranges") (v "0.212.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1xxca8f0iykzs532dxgnx12mi06qp9ql999jm54138d2mhbq15s3")))

(define-public crate-aws-ip-ranges-0.213.0 (c (n "aws-ip-ranges") (v "0.213.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "132dmrd3s0vhg8yhj23nhdqybpy46lqpvbj5q1pyka47b1sc8pcy")))

(define-public crate-aws-ip-ranges-0.214.0 (c (n "aws-ip-ranges") (v "0.214.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0d0yldx99zh2fbahbyh2rzk8cv7yj6q51zanbb58r3aclvq09ys5")))

(define-public crate-aws-ip-ranges-0.215.0 (c (n "aws-ip-ranges") (v "0.215.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "09ss16xpgz0rcn8lhy2g6kn8576yiblvsv42376yk2rfcffv792c")))

(define-public crate-aws-ip-ranges-0.216.0 (c (n "aws-ip-ranges") (v "0.216.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1scbzjq0apd0w8ghy6a0rpsr40h1dg4cb90pmhp1v6n4vy092c92")))

(define-public crate-aws-ip-ranges-0.217.0 (c (n "aws-ip-ranges") (v "0.217.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0j1b14icvlbkps44jy3hdgjipnmvxgcqhklc30sfhb1zql2xzy61")))

(define-public crate-aws-ip-ranges-0.218.0 (c (n "aws-ip-ranges") (v "0.218.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "10m6612qpgpc552savzm1ixm0d2dl72by6s2gal3ybf7srbm546p")))

(define-public crate-aws-ip-ranges-0.219.0 (c (n "aws-ip-ranges") (v "0.219.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0cs2vcznv4kwfsc1dp19nb2xyl8ajfvlvnqpp0xba0sqhm5z9z2v")))

(define-public crate-aws-ip-ranges-0.220.0 (c (n "aws-ip-ranges") (v "0.220.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "02lzi7psp88dv5v03mi37z9zm4i1q0pd0dhfvx2chigs02nldi1a")))

(define-public crate-aws-ip-ranges-0.221.0 (c (n "aws-ip-ranges") (v "0.221.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "09nn8m38cql6d9kjp8lxgkrgknb51dlla3ghi0lvgzm09zvqyqvd")))

(define-public crate-aws-ip-ranges-0.222.0 (c (n "aws-ip-ranges") (v "0.222.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "038n9gzvjdx54djsf14n8vad9jzmq1nlw3vca7jclcpws18griks")))

(define-public crate-aws-ip-ranges-0.223.0 (c (n "aws-ip-ranges") (v "0.223.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1rmhkngmvg2dw639gl6bz2zk5q0p07ddi6767ij64jx9x4841bw3")))

(define-public crate-aws-ip-ranges-0.224.0 (c (n "aws-ip-ranges") (v "0.224.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "057nh3qm7l0kalk973a0w05zdjs3ddl9kv74zp2ydpzd1l08x4iw")))

(define-public crate-aws-ip-ranges-0.225.0 (c (n "aws-ip-ranges") (v "0.225.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1xsixmks8x32ayfmb64h6kgfaz6asgh7r7zvvx3f257hip8nw2yn")))

(define-public crate-aws-ip-ranges-0.226.0 (c (n "aws-ip-ranges") (v "0.226.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "16dflirb2wym0vqfd2lb5z6wqgvpmrhxkjsfvjgvqb22vx16cjqw")))

(define-public crate-aws-ip-ranges-0.227.0 (c (n "aws-ip-ranges") (v "0.227.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1qqgrpvmf0lgjcn8b95m271wgvhz56fcx8gqxvbgdn84m9i18jfz")))

(define-public crate-aws-ip-ranges-0.228.0 (c (n "aws-ip-ranges") (v "0.228.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0135j5ap1rfaz4kvvwmspfhfm6v5lfnqjwx9brphgyjl4bl23gss")))

(define-public crate-aws-ip-ranges-0.229.0 (c (n "aws-ip-ranges") (v "0.229.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0a5sf4ribm741xr5ji1d7hdh91ai64wk0zw1pw47rhp63v27jkg4")))

(define-public crate-aws-ip-ranges-0.230.0 (c (n "aws-ip-ranges") (v "0.230.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1qkbmrqi6p3bvcnj199vilvw4wvm7hzbh7nyvyda24s3a5rb12g7")))

(define-public crate-aws-ip-ranges-0.231.0 (c (n "aws-ip-ranges") (v "0.231.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0k031waizwjrq1f3jrl7vqwvrbp9fl5wxrxnalrnlvcn4dvgy972")))

(define-public crate-aws-ip-ranges-0.232.0 (c (n "aws-ip-ranges") (v "0.232.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1s57ndh1r5g5ibgfpw64ffx7kgms9kxsin72i3r4m2mfn4kafqwj")))

(define-public crate-aws-ip-ranges-0.233.0 (c (n "aws-ip-ranges") (v "0.233.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0ysaa4x4wq0hvmpzyjlrvc3k79wrbhq5xyz2wffhhmqgpflv7vg9")))

(define-public crate-aws-ip-ranges-0.234.0 (c (n "aws-ip-ranges") (v "0.234.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0aj4aghlzyvfim3s3x6sj8cnh8mkkdnxss74c9mmal7lrsfxf704")))

(define-public crate-aws-ip-ranges-0.235.0 (c (n "aws-ip-ranges") (v "0.235.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1fi59bsz83502rnmq0gy0zxim60nqg2zm4gxij8cjki3w287an4q")))

(define-public crate-aws-ip-ranges-0.236.0 (c (n "aws-ip-ranges") (v "0.236.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0fhfx4mkzq01ap66iyi12w7f8pqccjm6dwzfxzd8r2hydfg7nihr")))

(define-public crate-aws-ip-ranges-0.237.0 (c (n "aws-ip-ranges") (v "0.237.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "11vpa3hqfdl0s14aybq2lpwvhrqgwp01r0mdgns7h3zq0ssnajqb")))

(define-public crate-aws-ip-ranges-0.238.0 (c (n "aws-ip-ranges") (v "0.238.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "196cyv8b97wdgjlsjwrh74hbf62ax8bmxyzgprqdpl67bs23q9f3")))

(define-public crate-aws-ip-ranges-0.239.0 (c (n "aws-ip-ranges") (v "0.239.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0lv431c4d72wvbgj7r4ylhws82l91ff55xf996iv3kh9j4l0i3bf")))

(define-public crate-aws-ip-ranges-0.240.0 (c (n "aws-ip-ranges") (v "0.240.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0qnnnw9jcbjm0gk1lmxjhymwalfgjsgaw88mp2a6i7iixcy7r4a3")))

(define-public crate-aws-ip-ranges-0.241.0 (c (n "aws-ip-ranges") (v "0.241.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "00xjn1y03ywp6nvrgysrvfn280d31j6ip8lq641sdlwawdrzq6zd")))

(define-public crate-aws-ip-ranges-0.242.0 (c (n "aws-ip-ranges") (v "0.242.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "195zivlz2xa5i1c6q05hx21if6hd6zddmfd0xhhlbsw05w0y18g0")))

(define-public crate-aws-ip-ranges-0.243.0 (c (n "aws-ip-ranges") (v "0.243.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0ikjxz8450ix5yypiq95rh614g1xydz2qnrjww18jw5njc7y6yhd")))

(define-public crate-aws-ip-ranges-0.244.0 (c (n "aws-ip-ranges") (v "0.244.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0gd1ag8fylqa49h1s7rydxrsd3i9z2h3x29xrf31l46wzlibpjsz")))

(define-public crate-aws-ip-ranges-0.245.0 (c (n "aws-ip-ranges") (v "0.245.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "02ji05yia42hnrxbry40dnm3117v6k8jyjddc55agq4ww4kznyiz")))

(define-public crate-aws-ip-ranges-0.246.0 (c (n "aws-ip-ranges") (v "0.246.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0r1d0v4lz8nym9nlpjpyrc2gaf1grgbi5mv047c8wswy5xxgh0vh")))

(define-public crate-aws-ip-ranges-0.247.0 (c (n "aws-ip-ranges") (v "0.247.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "118l67mlrmz0mgyfzz534vrg146mx7iidw14rgpxjv0s16j8vxl6")))

(define-public crate-aws-ip-ranges-0.248.0 (c (n "aws-ip-ranges") (v "0.248.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0pd7lh0jaj5749i59y9n1k5c3yspwp3lyhhk0dz1b053g2rmnx8l")))

(define-public crate-aws-ip-ranges-0.249.0 (c (n "aws-ip-ranges") (v "0.249.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0plhqyai6m32n2qmcszwr28gb9398in4ws4bg0vbwlixhc6jkjfl")))

(define-public crate-aws-ip-ranges-0.250.0 (c (n "aws-ip-ranges") (v "0.250.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1dzlf495v9rdygs8x0d3hmzig7xfzb12c1icakkgklz9lybj3icv")))

(define-public crate-aws-ip-ranges-0.251.0 (c (n "aws-ip-ranges") (v "0.251.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "01nf5c9mxfndzmhcqb1sqpspzpqs3bql4ggjahnjnmq92nry9ijf")))

(define-public crate-aws-ip-ranges-0.252.0 (c (n "aws-ip-ranges") (v "0.252.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "00y928jnj3w6z8riwqavfpf4d4xnd3n53py4841bdv0s4f7cyszk")))

(define-public crate-aws-ip-ranges-0.253.0 (c (n "aws-ip-ranges") (v "0.253.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0s49gr80bh0j04i5gbn1pshg3pfxqp02zdv2y8gy35x7ni5f99zb")))

(define-public crate-aws-ip-ranges-0.254.0 (c (n "aws-ip-ranges") (v "0.254.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1gvs1md925p1y1w9nsihvw1f1zsan97xc4ckl9gac4kahrx0im6x")))

(define-public crate-aws-ip-ranges-0.255.0 (c (n "aws-ip-ranges") (v "0.255.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1488lnhcjhfzzhazd1flgxikbiwv25jnhg86mi1r1ml9mz5dxbal")))

(define-public crate-aws-ip-ranges-0.256.0 (c (n "aws-ip-ranges") (v "0.256.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0vlnpfqcxdkmfhfv9p4scymzjqpk9vgfxlj3qbxyx8xpz8gxrb8g")))

(define-public crate-aws-ip-ranges-0.257.0 (c (n "aws-ip-ranges") (v "0.257.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1as530nm74m1q9vv8q13khsir6pwvqbhpbc5516z088sn8r3pkbw")))

(define-public crate-aws-ip-ranges-0.258.0 (c (n "aws-ip-ranges") (v "0.258.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0pgbc6ag8d1g4llidrdnl8w0gsk69s1g91d2i6a9n8v9bf3q4sf2")))

(define-public crate-aws-ip-ranges-0.259.0 (c (n "aws-ip-ranges") (v "0.259.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0n5vli5nkjxzi1m03fgf086zkph60s5c7j5q638scrdyygazci0i")))

(define-public crate-aws-ip-ranges-0.260.0 (c (n "aws-ip-ranges") (v "0.260.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "060sfn5fjp916b30b2b95dja8fylw973mlivk3g0dwqk5vhy2j46")))

(define-public crate-aws-ip-ranges-0.261.0 (c (n "aws-ip-ranges") (v "0.261.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0xl7i2qa13v0hffdjcrd7pz58k3kpzrr742acd35dgvrjgqlcqbx")))

(define-public crate-aws-ip-ranges-0.262.0 (c (n "aws-ip-ranges") (v "0.262.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1mdd93xnjc3mhnfbfwzj9dix6xv8p3803bj5vkcs7k8war9pccbc")))

(define-public crate-aws-ip-ranges-0.263.0 (c (n "aws-ip-ranges") (v "0.263.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0yi3ym0k8yqvcg5x37d5i6313wzb0bk2dq313swfgs46azy1h5k3")))

(define-public crate-aws-ip-ranges-0.264.0 (c (n "aws-ip-ranges") (v "0.264.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1zckaqq26725lk8786rwl7j0yc7gcz8gd0fjjwlhh06nrxqh5ypp")))

(define-public crate-aws-ip-ranges-0.265.0 (c (n "aws-ip-ranges") (v "0.265.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "098gbijqymn5cj7i1plc0xj27ljkmdfs3xhi5yi5fg75fiqr5pw8")))

(define-public crate-aws-ip-ranges-0.266.0 (c (n "aws-ip-ranges") (v "0.266.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0wxlglnaaf5fafbvbigh8jj2dmmngdabw66pni72v66xil46wlkp")))

(define-public crate-aws-ip-ranges-0.267.0 (c (n "aws-ip-ranges") (v "0.267.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "15xhfr4d4zn0jlpbw1f503wlfaxf58vggspj64y4gdzgdgc6dgb5")))

(define-public crate-aws-ip-ranges-0.268.0 (c (n "aws-ip-ranges") (v "0.268.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1r7il1i7hf0my42kpnrji6qak71w4rkzgrg9d5qz8qan7gxbbdms")))

(define-public crate-aws-ip-ranges-0.269.0 (c (n "aws-ip-ranges") (v "0.269.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1r4qi2bx33mr71f2hspvd7kig1bahacvnavf2ysnwxzq9zcadc19")))

(define-public crate-aws-ip-ranges-0.270.0 (c (n "aws-ip-ranges") (v "0.270.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0vfn3sl01sj7yzzp0d3qzza93igq1fq4zc30aqmq1idvhnp8xzrg")))

(define-public crate-aws-ip-ranges-0.271.0 (c (n "aws-ip-ranges") (v "0.271.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0lydbvpl6i961mbfrk52aprafwsi0apl5d9mcylkfglm7vcjy4iv")))

(define-public crate-aws-ip-ranges-0.272.0 (c (n "aws-ip-ranges") (v "0.272.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "17c174g1f9jnz5m67ly8kgnw5qd550m7wdh016xpmx2jg18dmfja")))

(define-public crate-aws-ip-ranges-0.273.0 (c (n "aws-ip-ranges") (v "0.273.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "05igh7g36mvmnabz0ay5pxdg7lsfcj1s9xncl621qj4ir1yahkig")))

(define-public crate-aws-ip-ranges-0.274.0 (c (n "aws-ip-ranges") (v "0.274.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0mv60aff612wwsanh2y27lm98dfnsfrdnl4bd8dmm3ah2c7ghv42")))

(define-public crate-aws-ip-ranges-0.275.0 (c (n "aws-ip-ranges") (v "0.275.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "00kr2jl7lzj7ksqyq7333cikc5qa6jk7nsa007312jv5yp4f9k1f")))

(define-public crate-aws-ip-ranges-0.276.0 (c (n "aws-ip-ranges") (v "0.276.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "17nhq1hqmzs562xs5ln0gbq3hzd4h2hzkwrdwdhlxmxfqx9h5lx9")))

(define-public crate-aws-ip-ranges-0.277.0 (c (n "aws-ip-ranges") (v "0.277.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1walrh54nw748sxx1jzpy5pdh9l3kjgq5dfa348n25fc13awp047")))

(define-public crate-aws-ip-ranges-0.278.0 (c (n "aws-ip-ranges") (v "0.278.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1zsa1lmnw3z3d9kfiid5lbsrc78izy54frwpla77w75zkykk1mxx")))

(define-public crate-aws-ip-ranges-0.279.0 (c (n "aws-ip-ranges") (v "0.279.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1vs08hg1i6h1i7z2s0bcs9ss19w1lsh8vkpdbrfmcxl0nx67qwri")))

(define-public crate-aws-ip-ranges-0.280.0 (c (n "aws-ip-ranges") (v "0.280.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "12hwshdgjvr18y4gqbzbay3gv0j4ivhb0d4f7sb0p6vfjlxs91fv")))

(define-public crate-aws-ip-ranges-0.281.0 (c (n "aws-ip-ranges") (v "0.281.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0gp7jj76j6m2r2yr9y42cra1igjw05a63bb1dahbjxg1p2mcwzjx")))

(define-public crate-aws-ip-ranges-0.282.0 (c (n "aws-ip-ranges") (v "0.282.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "09x1yj1pvki4yixcq44pl3x6sbjdnia8xgffpdm1kn56pl1777nd")))

(define-public crate-aws-ip-ranges-0.283.0 (c (n "aws-ip-ranges") (v "0.283.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1j1n918nxsa4d0dmr95vv5hrqq5pmaqqapys2al6ljyzlxyp0z78")))

(define-public crate-aws-ip-ranges-0.284.0 (c (n "aws-ip-ranges") (v "0.284.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1fs60xc22ykjn83mfavcy801piqywr4d1lfcrrv0r1rw4bkn637a")))

(define-public crate-aws-ip-ranges-0.285.0 (c (n "aws-ip-ranges") (v "0.285.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "17nd891wfgr88kjfknr1ffv4zi1ap2chh2hf68xd67dgnrjkpb5y")))

(define-public crate-aws-ip-ranges-0.286.0 (c (n "aws-ip-ranges") (v "0.286.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "11ckbya0fbn0kjafdgxbj6z0pa477ckf74phd7i56xi8cl3syizb")))

(define-public crate-aws-ip-ranges-0.287.0 (c (n "aws-ip-ranges") (v "0.287.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1p7c1dd4hywxl943igmgbshkzc4zr5fj11wci1fqb0cwb6mglfy3")))

(define-public crate-aws-ip-ranges-0.288.0 (c (n "aws-ip-ranges") (v "0.288.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1045avg6jhy6zx03dj6rpw4q6xflhs7vxi4lb5lxsnwi1fn947lk")))

(define-public crate-aws-ip-ranges-0.289.0 (c (n "aws-ip-ranges") (v "0.289.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0wd9vz4b85knvv14f46f7ag4d86kg1dk762ic011cbw06b0phpyi")))

(define-public crate-aws-ip-ranges-0.290.0 (c (n "aws-ip-ranges") (v "0.290.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "19mz011xf7k2y906vsjqwpj4ycrc0h9rhm00mwq1wppkpnd928fh")))

(define-public crate-aws-ip-ranges-0.291.0 (c (n "aws-ip-ranges") (v "0.291.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0ws28saxkiijxdalgfl69wv51v2k37dywwf1b3dzyz62iggng0m8")))

(define-public crate-aws-ip-ranges-0.292.0 (c (n "aws-ip-ranges") (v "0.292.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1cm4bwg3j9v7abxhmirhzny06acx1rd67ryg5bw91i6ihv08rd9s")))

(define-public crate-aws-ip-ranges-0.293.0 (c (n "aws-ip-ranges") (v "0.293.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1j2d4qw865rmw4wv6whbzcrdydrr3hmbz6j2gafmaf9ljyx7l6z2")))

(define-public crate-aws-ip-ranges-0.294.0 (c (n "aws-ip-ranges") (v "0.294.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1mw7sslynywvqnii2z8r4xx3pk2hlmxw4zs1hipm15cmzgxl4cbh")))

(define-public crate-aws-ip-ranges-0.295.0 (c (n "aws-ip-ranges") (v "0.295.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "04jvzzbzbcvf1p1m1nlfr14bm9bfk5fy7mpk7cha39b92zhhkz51")))

(define-public crate-aws-ip-ranges-0.296.0 (c (n "aws-ip-ranges") (v "0.296.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "08sjryxvqfj919kqk71g57lk8mn0ifs85znf33g11kfrrax9vjri")))

(define-public crate-aws-ip-ranges-0.297.0 (c (n "aws-ip-ranges") (v "0.297.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0k6bq346k28zlck97vfipvr380qxyl11lk3bn8p0w0cjaph23zh7")))

(define-public crate-aws-ip-ranges-0.298.0 (c (n "aws-ip-ranges") (v "0.298.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1dx0npacsqr693qb5ycb73fziqwiyb8isw7sk24f6vzly8nr3izj")))

(define-public crate-aws-ip-ranges-0.299.0 (c (n "aws-ip-ranges") (v "0.299.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1zn55s9mxkigkfaf0rx0bgwa1080j9mpsba3gcarbny076chdgja")))

(define-public crate-aws-ip-ranges-0.300.0 (c (n "aws-ip-ranges") (v "0.300.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1b3f6cp6hcynmghn74smg7ax9in5ia9qmjpzgkicnrg81k69drrp")))

(define-public crate-aws-ip-ranges-0.301.0 (c (n "aws-ip-ranges") (v "0.301.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1qrhqyj8g3vi9agcd8ylphpvs3cavrr7gnbmxz0kad2fj8iryass")))

(define-public crate-aws-ip-ranges-0.302.0 (c (n "aws-ip-ranges") (v "0.302.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0hxr16hn2m56alpyx0mk58798hwbmqznwdcg20w223chv0xnh607")))

(define-public crate-aws-ip-ranges-0.303.0 (c (n "aws-ip-ranges") (v "0.303.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0pds53nxi024d74ncp64vrvydhzgnf3zpw3hb180xd351xb9kp58")))

(define-public crate-aws-ip-ranges-0.304.0 (c (n "aws-ip-ranges") (v "0.304.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0r577v0j8r5z5iyq82fbv1yx4nn1b5qvqp2g90jfqvy742s891sz")))

(define-public crate-aws-ip-ranges-0.305.0 (c (n "aws-ip-ranges") (v "0.305.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1hbl4x9a296kdaffjaklrrq2carjhqmbvrh1hgzfiy83n47bcabr")))

(define-public crate-aws-ip-ranges-0.306.0 (c (n "aws-ip-ranges") (v "0.306.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1n06hn575pz3qm858q832rnr6hi9i9jvp2zarpnsidh09gv15lw0")))

(define-public crate-aws-ip-ranges-0.307.0 (c (n "aws-ip-ranges") (v "0.307.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "07y6v05gnfh3l4dzwyi3iiysf1x2yfh4f7dfb8l9dmy5mlsk1k34")))

(define-public crate-aws-ip-ranges-0.308.0 (c (n "aws-ip-ranges") (v "0.308.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0ryw42svdf74p9dva1mlinv3r0w4z9pa3n3jgjgzq1x3jrajm1a8")))

(define-public crate-aws-ip-ranges-0.309.0 (c (n "aws-ip-ranges") (v "0.309.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1fqbvd9vypkpwcj36szhaj4v43i42mm5mm6dbfqy7vzs9wq5zxkk")))

(define-public crate-aws-ip-ranges-0.310.0 (c (n "aws-ip-ranges") (v "0.310.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0vympy5ihi2s9rixxi7p6z3jjkzmpqhdvyp4lvhmxkllc6zgna58")))

(define-public crate-aws-ip-ranges-0.311.0 (c (n "aws-ip-ranges") (v "0.311.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1w7fb2mlzcd7w3imlpkl8hx1ri9d4f7kqppd8ndhlwiap985vm3l")))

(define-public crate-aws-ip-ranges-0.312.0 (c (n "aws-ip-ranges") (v "0.312.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0kzm7mp0i6hj93badmmks3c082hxrcyzp3z4mmki1nmjk0zxcm7j")))

(define-public crate-aws-ip-ranges-0.313.0 (c (n "aws-ip-ranges") (v "0.313.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1fbrx7axhmxq6xaixxjc4l5kym5f4jdh4j4q6h3s5cgiay0n7jj2")))

(define-public crate-aws-ip-ranges-0.314.0 (c (n "aws-ip-ranges") (v "0.314.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1z54wxgvb6dwqqwxs6bavxn8snaypiynsn90pz58x5l1wiq67hnj")))

(define-public crate-aws-ip-ranges-0.315.0 (c (n "aws-ip-ranges") (v "0.315.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "049mgzabwh7fggi89vqq4rszhc47g9nhski0wmradspcql37diqw")))

(define-public crate-aws-ip-ranges-0.316.0 (c (n "aws-ip-ranges") (v "0.316.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "17sy796cizq45f4syksjw0dfp5naqnnab6vplxjcb2g53kirm7hi")))

(define-public crate-aws-ip-ranges-0.317.0 (c (n "aws-ip-ranges") (v "0.317.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0i6px9r5ddndx2r7xcmkkx83i9sfqx35df56rm1a6rs8njs7r1ih")))

(define-public crate-aws-ip-ranges-0.318.0 (c (n "aws-ip-ranges") (v "0.318.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0655lyy0xd1bng3hj202d1k21wbnd9ylp7a8fmky813l3v7nly39")))

(define-public crate-aws-ip-ranges-0.319.0 (c (n "aws-ip-ranges") (v "0.319.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1kmrb1q0fpdhvizacg5d4y1fsrdcm0dz23q2f6lc16f8qfjjg9w0")))

(define-public crate-aws-ip-ranges-0.320.0 (c (n "aws-ip-ranges") (v "0.320.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0561f56qlc817rhh00384iqx3y9fnj66nd437kpch2ykyyx0mxfv")))

(define-public crate-aws-ip-ranges-0.321.0 (c (n "aws-ip-ranges") (v "0.321.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1ynniyfa8j0vzl8q6wz6sc5njsy5ns5zj76q9w0gr01y06f8pw24")))

(define-public crate-aws-ip-ranges-0.322.0 (c (n "aws-ip-ranges") (v "0.322.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0xinywh5n5811h0vcd362vxlyzf57ik0miqfj3kpwyi5fmycpiqa")))

