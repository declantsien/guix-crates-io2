(define-module (crates-io aw s- aws-iot-device-sdk-rust) #:use-module (crates-io))

(define-public crate-aws-iot-device-sdk-rust-0.0.1 (c (n "aws-iot-device-sdk-rust") (v "0.0.1") (d (list (d (n "rumqtt") (r "^0.31.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0inrr6kghyyvrj6a2spqyfq1q3zlglcp4dq6a7cn4r5km5lkvjlg")))

(define-public crate-aws-iot-device-sdk-rust-0.0.2 (c (n "aws-iot-device-sdk-rust") (v "0.0.2") (d (list (d (n "rumqtt") (r "^0.31.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pjvdlrig7pjflj5f3kgsdm2zw65g53p7wrqfv7cfp9zbcwfw2xg")))

(define-public crate-aws-iot-device-sdk-rust-0.0.3 (c (n "aws-iot-device-sdk-rust") (v "0.0.3") (d (list (d (n "rumqtt") (r "^0.31.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13z4j30m244pcxdag40l57dzp6lqvnwgiv55277bs8yfr9kpib8k")))

(define-public crate-aws-iot-device-sdk-rust-0.0.4 (c (n "aws-iot-device-sdk-rust") (v "0.0.4") (d (list (d (n "rumqtt") (r "^0.31.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m1bwj113m1rh9y5vv92b2gm2b1zwxmf0bbkgcf0jj3symz7yrgq")))

(define-public crate-aws-iot-device-sdk-rust-0.1.0 (c (n "aws-iot-device-sdk-rust") (v "0.1.0") (d (list (d (n "mqtt4bytes") (r "^0.1.6") (d #t) (k 0)) (d (n "rumqttc") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "sync" "macros" "net" "time"))) (o #t) (d #t) (k 0)))) (h "12nn8lilcp1k4d3hzdvl351l7aqyalj6p2z5fjpb9p45rxxqvalk") (f (quote (("default" "async") ("async" "tokio")))) (y #t)))

(define-public crate-aws-iot-device-sdk-rust-0.1.1 (c (n "aws-iot-device-sdk-rust") (v "0.1.1") (d (list (d (n "mqtt4bytes") (r "^0.1.6") (d #t) (k 0)) (d (n "rumqttc") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "sync" "macros" "net" "time"))) (o #t) (d #t) (k 0)))) (h "0k6nmav79zsghdrpw4yr7irm65vc9ia0bv3jnvjp5gmavw6lvqka") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-aws-iot-device-sdk-rust-0.1.2 (c (n "aws-iot-device-sdk-rust") (v "0.1.2") (d (list (d (n "mqtt4bytes") (r "^0.1.6") (d #t) (k 0)) (d (n "rumqttc") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "sync" "macros" "net" "time"))) (o #t) (d #t) (k 0)))) (h "102abl5s7qpbhnxq1x59rqdwyfwn055m4mn2fqkd00p2i7iy4i8r") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-aws-iot-device-sdk-rust-0.2.0 (c (n "aws-iot-device-sdk-rust") (v "0.2.0") (d (list (d (n "mqtt4bytes") (r "^0.1.6") (d #t) (k 0)) (d (n "rumqttc") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "sync" "macros" "net" "time"))) (o #t) (d #t) (k 0)))) (h "1a6v2klgyqbmim9w3fddjabz34xddigk739m6g35l5fr6ryps7z2") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-aws-iot-device-sdk-rust-0.2.1 (c (n "aws-iot-device-sdk-rust") (v "0.2.1") (d (list (d (n "mqtt4bytes") (r "^0.1.6") (d #t) (k 0)) (d (n "rumqttc") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "sync" "macros" "net" "time"))) (o #t) (d #t) (k 0)))) (h "1ygvxflsx03j32316c83iavjvsmvxy6kddcjlz563j138ihjp1p7") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-aws-iot-device-sdk-rust-0.3.0 (c (n "aws-iot-device-sdk-rust") (v "0.3.0") (d (list (d (n "bus") (r "^2.4.0") (o #t) (d #t) (k 0)) (d (n "mqtt4bytes") (r "^0.1.6") (d #t) (k 0)) (d (n "rumqttc") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "sync" "macros" "net" "time" "fs"))) (o #t) (d #t) (k 0)))) (h "0v0yv55pj38viskqcn76wqhc0h0zdhpr8sgmkrqsswqi2g54w4ss") (f (quote (("default" "async")))) (s 2) (e (quote (("sync" "dep:bus") ("async" "dep:tokio"))))))

