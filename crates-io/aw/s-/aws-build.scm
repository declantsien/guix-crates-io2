(define-module (crates-io aw s- aws-build) #:use-module (crates-io))

(define-public crate-aws-build-0.9.0 (c (n "aws-build") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "aws-build-lib") (r "^0.9") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pclc5v3igki8kkmmhzb2yda7jvz9s1iv4fqya1vk8syc0wnrjyl")))

(define-public crate-aws-build-0.9.1 (c (n "aws-build") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "aws-build-lib") (r "^0.9") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hgj6f1fzs99g55g4yp4d8f94j7sgfm0dgnpc7v40br63aicrwqz")))

(define-public crate-aws-build-0.9.2 (c (n "aws-build") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "aws-build-lib") (r "^0.9.2") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ljk644x8l3iyxjdg2j795wzaimblm2kb4q2w52y4bs58956hbwk")))

(define-public crate-aws-build-0.9.3 (c (n "aws-build") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "aws-build-lib") (r "^0.9.3") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0s09qvj421827v2bw73xxnp6yhfrxd2vjnw3i4mz0z401bpx4bvs")))

(define-public crate-aws-build-0.10.0 (c (n "aws-build") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.45") (f (quote ("std"))) (k 0)) (d (n "argh") (r "^0.1.6") (k 0)) (d (n "aws-build-lib") (r "^0.10.0") (k 0)) (d (n "fehler") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.14") (k 0)))) (h "0mr0lkp8ymxa6w68jn5f5ry900bl8nj730m2q4z89qpgdrrffv78")))

(define-public crate-aws-build-0.10.1 (c (n "aws-build") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.45") (f (quote ("std"))) (k 0)) (d (n "argh") (r "^0.1.6") (k 0)) (d (n "aws-build-lib") (r "^0.10.0") (k 0)) (d (n "fehler") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.14") (k 0)))) (h "0jfj0asklwass1914b1989jfvjph3myb50fhhxvlhw3hnbvzn9xn")))

