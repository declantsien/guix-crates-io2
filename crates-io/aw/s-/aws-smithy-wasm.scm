(define-module (crates-io aw s- aws-smithy-wasm) #:use-module (crates-io))

(define-public crate-aws-smithy-wasm-0.0.1 (c (n "aws-smithy-wasm") (v "0.0.1") (h "185l55n981l8pzqpm11k4rj694plx7q2sg0aqj1bnrf2xnryap13")))

(define-public crate-aws-smithy-wasm-0.1.0 (c (n "aws-smithy-wasm") (v "0.1.0") (d (list (d (n "aws-smithy-http") (r "^0.60.7") (d #t) (k 0)) (d (n "aws-smithy-runtime-api") (r "^1.2.0") (f (quote ("http-1x"))) (d #t) (k 0)) (d (n "aws-smithy-types") (r "^1.1.8") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "wasi") (r "^0.12.0") (d #t) (k 0)))) (h "1p4ki42qy3wcawj5gz8xzc8ryizrksbc061hybf10yx0gvlns134")))

(define-public crate-aws-smithy-wasm-0.1.1 (c (n "aws-smithy-wasm") (v "0.1.1") (d (list (d (n "aws-smithy-http") (r "^0.60.7") (d #t) (k 0)) (d (n "aws-smithy-runtime-api") (r "^1.2.0") (f (quote ("http-1x"))) (d #t) (k 0)) (d (n "aws-smithy-types") (r "^1.1.8") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "wasi") (r "^0.12.1") (d #t) (k 0)))) (h "1qxb8yiq8vdh0xzfj043d6ki3midw7nb8c6scikq0ykz5gh08jqv")))

(define-public crate-aws-smithy-wasm-0.1.2 (c (n "aws-smithy-wasm") (v "0.1.2") (d (list (d (n "aws-smithy-http") (r "^0.60.7") (d #t) (k 0)) (d (n "aws-smithy-runtime-api") (r "^1.3.0") (f (quote ("http-1x"))) (d #t) (k 0)) (d (n "aws-smithy-types") (r "^1.1.8") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "wasi") (r "^0.12.1") (d #t) (k 0)))) (h "0xpmqwy2f7d5m86hi2z29v15fil1fh6q24kwhfiilycg74ds6xjm")))

