(define-module (crates-io aw s- aws-region-nearby) #:use-module (crates-io))

(define-public crate-aws-region-nearby-0.0.1-alpha (c (n "aws-region-nearby") (v "0.0.1-alpha") (d (list (d (n "geoutils") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)))) (h "15j9j04gdg505dd7q5j1d28wlmr785vibzvjcq4qbqj8ah1n2dmp")))

(define-public crate-aws-region-nearby-0.0.2-alpha (c (n "aws-region-nearby") (v "0.0.2-alpha") (d (list (d (n "geoutils") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f4yya370f6l366d8ly7cybnxs9yxri1si0h8rrfdyq0zk70c09l")))

(define-public crate-aws-region-nearby-0.0.3-alpha (c (n "aws-region-nearby") (v "0.0.3-alpha") (d (list (d (n "geoutils") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0lpxvi9zaryjchyi5ylwpf129vkb2xv15mswga66w813x8z7xknf")))

(define-public crate-aws-region-nearby-0.0.4-alpha (c (n "aws-region-nearby") (v "0.0.4-alpha") (d (list (d (n "geoutils") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0if4pvgbhicdfg34vx68rxwxj7rjxvclk1hpv86frn3487cjqwpz")))

(define-public crate-aws-region-nearby-0.0.5-alpha (c (n "aws-region-nearby") (v "0.0.5-alpha") (d (list (d (n "geoutils") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0pa9nzp446n95firg1mrlxvdsk6d6qvkqhf3xkzkb4n9hdczm6vf")))

(define-public crate-aws-region-nearby-0.1.0 (c (n "aws-region-nearby") (v "0.1.0") (d (list (d (n "geoutils") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rkq8r1gambd58cjgyz3zqgq0x3k0g5b823105y9d0s6z7bd7430")))

(define-public crate-aws-region-nearby-0.1.1 (c (n "aws-region-nearby") (v "0.1.1") (d (list (d (n "geoutils") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g9rk1qz8lymh3cn5rrc09jd7c2jm3qylikm25h3hm26ks7k5ydn")))

(define-public crate-aws-region-nearby-0.1.2 (c (n "aws-region-nearby") (v "0.1.2") (d (list (d (n "geoutils") (r "^0.5") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hdzvl5pbrqvxd5jhv77a427xsx9ifg89qnbjxm247rs2vrk1qm0")))

(define-public crate-aws-region-nearby-0.1.3 (c (n "aws-region-nearby") (v "0.1.3") (d (list (d (n "geoutils") (r "^0.5.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lgbiks672qs8nqfh0qsrif3kgkjmchk6yw1ssmkqwwsp0lncyr6")))

(define-public crate-aws-region-nearby-0.2.0 (c (n "aws-region-nearby") (v "0.2.0") (d (list (d (n "geoutils") (r "^0.5.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hhfqn2xxliqvx7p56kfkgqd2p3lfd2lx7prnbjfcmjvh7nzlzjv") (f (quote (("deno") ("default"))))))

(define-public crate-aws-region-nearby-0.2.1 (c (n "aws-region-nearby") (v "0.2.1") (d (list (d (n "geoutils") (r "^0.5.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xpaiciv2s0v20pg739k5c8z4m87pfiwj11dsw6f9b5l6vb9ji6a") (f (quote (("deno") ("default"))))))

(define-public crate-aws-region-nearby-0.2.2 (c (n "aws-region-nearby") (v "0.2.2") (d (list (d (n "geoutils") (r "^0.5.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "041pz12fszs04nq2mwxsc4mqqwmd14krb49j2dg938khkpb9ncj6") (f (quote (("deno") ("default"))))))

(define-public crate-aws-region-nearby-0.2.3 (c (n "aws-region-nearby") (v "0.2.3") (d (list (d (n "geoutils") (r "^0.5.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qhdgns5ayvam24h9mld98jzw5br16l75rbcm5dpd4bmq0cn13if") (f (quote (("deno") ("default"))))))

