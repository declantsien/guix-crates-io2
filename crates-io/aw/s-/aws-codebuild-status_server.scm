(define-module (crates-io aw s- aws-codebuild-status_server) #:use-module (crates-io))

(define-public crate-aws-codebuild-status_server-0.6.0 (c (n "aws-codebuild-status_server") (v "0.6.0") (d (list (d (n "actix-files") (r "^0.1.1") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2.2") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0") (d #t) (k 0)) (d (n "askama") (r "^0.8.0") (d #t) (k 0)) (d (n "aws-codebuild-status_aws") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "loggify") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pivx2ziz4frlm7gqz6ciz5anim68n190gwmkbxvp24i6vqmbnq7")))

