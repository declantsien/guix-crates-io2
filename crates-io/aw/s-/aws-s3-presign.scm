(define-module (crates-io aw s- aws-s3-presign) #:use-module (crates-io))

(define-public crate-aws-s3-presign-0.1.0 (c (n "aws-s3-presign") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "url-search-params") (r "^12.0.0") (d #t) (k 0)))) (h "14641fhv9jg1nwn5f2jfjfda64zd26cgp57m6d90n4l0krkf9xr6")))

