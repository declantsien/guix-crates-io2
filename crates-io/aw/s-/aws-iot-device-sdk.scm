(define-module (crates-io aw s- aws-iot-device-sdk) #:use-module (crates-io))

(define-public crate-aws-iot-device-sdk-0.0.1 (c (n "aws-iot-device-sdk") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)))) (h "0y9rfz9f2j13rm4vwc0zwj0zwbx6qvng4wzs7zaa168645aa8jl0") (f (quote (("std")))) (y #t)))

(define-public crate-aws-iot-device-sdk-0.0.2 (c (n "aws-iot-device-sdk") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.7") (k 0)))) (h "1gz69xif1dyc3vk8q72zcrl3slhfk0aqb0v0dvphbcc3d15hy8pb") (f (quote (("std"))))))

(define-public crate-aws-iot-device-sdk-0.0.3 (c (n "aws-iot-device-sdk") (v "0.0.3") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hvyvnk36ki8vif2grws1z1zwbf11ph3cjwib7yaaclymdaymaxv") (f (quote (("std"))))))

(define-public crate-aws-iot-device-sdk-0.0.4 (c (n "aws-iot-device-sdk") (v "0.0.4") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yvnplaf25a3ng7wjz3kixkbh940fxkcgxg790jhndv1493srv5y") (f (quote (("std"))))))

(define-public crate-aws-iot-device-sdk-0.0.5 (c (n "aws-iot-device-sdk") (v "0.0.5") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1a2s3sqi7faj4khjn93a8cgd42cgk7mr29a2j0immkcwz4nc2w1j") (f (quote (("std"))))))

(define-public crate-aws-iot-device-sdk-0.0.6 (c (n "aws-iot-device-sdk") (v "0.0.6") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2") (d #t) (k 0)))) (h "0fd5yjswwx3afblcdzw0fp3achmwyaqrdmq2cdzr500k66779g75") (f (quote (("std"))))))

