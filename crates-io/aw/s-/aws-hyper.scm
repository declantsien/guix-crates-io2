(define-module (crates-io aw s- aws-hyper) #:use-module (crates-io))

(define-public crate-aws-hyper-0.0.22-alpha (c (n "aws-hyper") (v "0.0.22-alpha") (d (list (d (n "aws-endpoint") (r "^0.0.22-alpha") (d #t) (k 0)) (d (n "aws-http") (r "^0.0.22-alpha") (d #t) (k 0)) (d (n "aws-sig-auth") (r "^0.0.22-alpha") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.27.0-alpha") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.27.0-alpha") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-http") (r "^0.27.0-alpha") (d #t) (k 0)) (d (n "aws-smithy-http-tower") (r "^0.27.0-alpha") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.27.0-alpha") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.22-alpha") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "http-body") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.2") (f (quote ("client" "http1" "http2" "tcp" "runtime"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22.1") (f (quote ("rustls-native-certs"))) (o #t) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.6") (f (quote ("util" "retry"))) (d #t) (k 0)) (d (n "tower-test") (r "^0.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09rji5ff6wmfj32v58yrpg9n0xwnvyqbrsmik7xbxjpk9rlqpy94") (f (quote (("rustls" "hyper-rustls" "aws-smithy-client/rustls") ("native-tls" "hyper-tls" "aws-smithy-client/native-tls") ("default"))))))

(define-public crate-aws-hyper-0.0.23-alpha (c (n "aws-hyper") (v "0.0.23-alpha") (d (list (d (n "aws-endpoint") (r "^0.0.23-alpha") (d #t) (k 0)) (d (n "aws-http") (r "^0.0.23-alpha") (d #t) (k 0)) (d (n "aws-sig-auth") (r "^0.0.23-alpha") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.27.0-alpha.1") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.27.0-alpha.1") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-http") (r "^0.27.0-alpha.1") (d #t) (k 0)) (d (n "aws-smithy-http-tower") (r "^0.27.0-alpha.1") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.27.0-alpha.1") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.23-alpha") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "http-body") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.2") (f (quote ("client" "http1" "http2" "tcp" "runtime"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22.1") (f (quote ("rustls-native-certs"))) (o #t) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.6") (f (quote ("util" "retry"))) (d #t) (k 0)) (d (n "tower-test") (r "^0.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1jzlrcv4nv47rg27sw82zml51cj3scvjpdd0bi7bl2774vmappr6") (f (quote (("rustls" "hyper-rustls" "aws-smithy-client/rustls") ("native-tls" "hyper-tls" "aws-smithy-client/native-tls") ("default"))))))

(define-public crate-aws-hyper-0.0.24-alpha (c (n "aws-hyper") (v "0.0.24-alpha") (d (list (d (n "aws-endpoint") (r "^0.0.24-alpha") (d #t) (k 0)) (d (n "aws-http") (r "^0.0.24-alpha") (d #t) (k 0)) (d (n "aws-sig-auth") (r "^0.0.24-alpha") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.27.0-alpha.2") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.27.0-alpha.2") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-http") (r "^0.27.0-alpha.2") (d #t) (k 0)) (d (n "aws-smithy-http-tower") (r "^0.27.0-alpha.2") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.27.0-alpha.2") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.24-alpha") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "http-body") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.2") (f (quote ("client" "http1" "http2" "tcp" "runtime"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22.1") (f (quote ("rustls-native-certs"))) (o #t) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.6") (f (quote ("util" "retry"))) (d #t) (k 0)) (d (n "tower-test") (r "^0.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "199yclxf7fs9lfhva8n2pmiharcffz965xci6y42hd88vff7a94x") (f (quote (("rustls" "hyper-rustls" "aws-smithy-client/rustls") ("native-tls" "hyper-tls" "aws-smithy-client/native-tls") ("default")))) (y #t)))

(define-public crate-aws-hyper-0.0.25-alpha (c (n "aws-hyper") (v "0.0.25-alpha") (d (list (d (n "aws-endpoint") (r "^0.0.25-alpha") (d #t) (k 0)) (d (n "aws-http") (r "^0.0.25-alpha") (d #t) (k 0)) (d (n "aws-sig-auth") (r "^0.0.25-alpha") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.28.0-alpha") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.28.0-alpha") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-http") (r "^0.28.0-alpha") (d #t) (k 0)) (d (n "aws-smithy-http-tower") (r "^0.28.0-alpha") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.28.0-alpha") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.25-alpha") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "http-body") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.2") (f (quote ("client" "http1" "http2" "tcp" "runtime"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22.1") (f (quote ("rustls-native-certs"))) (o #t) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.6") (f (quote ("util" "retry"))) (d #t) (k 0)) (d (n "tower-test") (r "^0.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0nwyb3dyp88alk8savx1s47jxw7dw5p7dps07ra6kl01a79bc1j6") (f (quote (("rustls" "hyper-rustls" "aws-smithy-client/rustls") ("native-tls" "hyper-tls" "aws-smithy-client/native-tls") ("default"))))))

(define-public crate-aws-hyper-0.0.26-alpha (c (n "aws-hyper") (v "0.0.26-alpha") (d (list (d (n "aws-endpoint") (r "^0.0.26-alpha") (d #t) (k 0)) (d (n "aws-http") (r "^0.0.26-alpha") (d #t) (k 0)) (d (n "aws-sig-auth") (r "^0.0.26-alpha") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.30.0-alpha") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.30.0-alpha") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-http") (r "^0.30.0-alpha") (d #t) (k 0)) (d (n "aws-smithy-http-tower") (r "^0.30.0-alpha") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.30.0-alpha") (d #t) (k 0)) (d (n "aws-types") (r "^0.0.26-alpha") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "http-body") (r "^0.4.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14.2") (f (quote ("client" "http1" "http2" "tcp" "runtime"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22.1") (f (quote ("rustls-native-certs"))) (o #t) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.6") (f (quote ("util" "retry"))) (d #t) (k 0)) (d (n "tower-test") (r "^0.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "19z2m3j3xymvj8azf9qzbdav18yi9msxymjvm5xv1i0fa2is3w5m") (f (quote (("rustls" "hyper-rustls" "aws-smithy-client/rustls") ("native-tls" "hyper-tls" "aws-smithy-client/native-tls") ("default"))))))

(define-public crate-aws-hyper-0.2.0 (c (n "aws-hyper") (v "0.2.0") (d (list (d (n "aws-endpoint") (r "^0.2.0") (d #t) (k 0)) (d (n "aws-http") (r "^0.2.0") (d #t) (k 0)) (d (n "aws-sig-auth") (r "^0.2.0") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.32.0") (d #t) (k 0)) (d (n "aws-smithy-client") (r "^0.32.0") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "aws-smithy-http") (r "^0.32.0") (d #t) (k 0)) (d (n "aws-smithy-http-tower") (r "^0.32.0") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.32.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "http-body") (r "^0.4.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14.2") (f (quote ("client" "http1" "http2" "tcp" "runtime"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22.1") (f (quote ("rustls-native-certs"))) (o #t) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.6") (f (quote ("util" "retry"))) (d #t) (k 0)) (d (n "tower-test") (r "^0.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "189dxdr755sq7fi8x40j2v75bxc7w2vrvzkjxm3zaw87h611zrjv") (f (quote (("rustls" "hyper-rustls" "aws-smithy-client/rustls") ("native-tls" "hyper-tls" "aws-smithy-client/native-tls") ("default"))))))

(define-public crate-aws-hyper-0.3.0 (c (n "aws-hyper") (v "0.3.0") (h "1wn3j03512qp12w3gawqwcw5y3n3vyr6yfk4q9lxklqk312yd6vq")))

(define-public crate-aws-hyper-0.4.0 (c (n "aws-hyper") (v "0.4.0") (h "16pzinq3zd9zfl3m54hzsafkrskd3vjxfnf32q8jjqn9b0j0757h")))

(define-public crate-aws-hyper-0.4.1 (c (n "aws-hyper") (v "0.4.1") (h "0d5p0bkxsg6sxynqsndcfx8sgrd5qwkam8qqfn4m2kdayxrd0v7q")))

(define-public crate-aws-hyper-0.5.1 (c (n "aws-hyper") (v "0.5.1") (h "0rwa3dpmnqkn0akncxq96vhw5cv0dvny4bcp8dpkdclya0fgbgaw") (y #t)))

(define-public crate-aws-hyper-0.5.2 (c (n "aws-hyper") (v "0.5.2") (h "0vqmha3zn9p6npxi50lpvjkskn0xpwbvxbgdy0y3cz4h87d9qa7c")))

(define-public crate-aws-hyper-0.6.0 (c (n "aws-hyper") (v "0.6.0") (h "0ink0ycsizq39lpy2yvxp7jmk7k497shfbwd45rvwjv5l9jbbp3n")))

(define-public crate-aws-hyper-0.7.0 (c (n "aws-hyper") (v "0.7.0") (h "114f0ylj468axidhkl1jbhs6zxx1zclvdll0glbq3g0gwhkg580g")))

(define-public crate-aws-hyper-0.8.0 (c (n "aws-hyper") (v "0.8.0") (h "0s1d9fmkcgy02wvy42lxpm1zm7bmqbs9fhc16nqipk4kgjpkj19y")))

(define-public crate-aws-hyper-0.9.0 (c (n "aws-hyper") (v "0.9.0") (h "1kx3z3f9f0q8r4j5ac9f925g9fg19lin87cchxhzl5ga4ycx901v")))

(define-public crate-aws-hyper-0.10.0 (c (n "aws-hyper") (v "0.10.0") (h "08z3andb3cb93d6177x6indxrpzfrmb30rc0h2846l640dg2fh17") (y #t)))

(define-public crate-aws-hyper-0.10.1 (c (n "aws-hyper") (v "0.10.1") (h "115p5iy1c7x5ch68cdcizlxzr8mf8nwk31yfq8r0cwa3kahxlhpg")))

(define-public crate-aws-hyper-0.11.0 (c (n "aws-hyper") (v "0.11.0") (h "1cmz6kj88amka35idc1pn2dzyk5c9fvyy26zqh01wdkfmk0sa5ra")))

(define-public crate-aws-hyper-0.12.0 (c (n "aws-hyper") (v "0.12.0") (h "19jz56vr6sq3rv6yxkrklqwfvlbkmmci6l44kmx11r29i8hfvl9r")))

(define-public crate-aws-hyper-0.13.0 (c (n "aws-hyper") (v "0.13.0") (h "0p4pbn8kl1x7w3rycacyj0lsdwn7nwpywdi5pigv4i4d5wx5xn49")))

(define-public crate-aws-hyper-0.14.0 (c (n "aws-hyper") (v "0.14.0") (h "18h6wjdbqixgq044x5r0yg9sdg9ig7fwh527jy9zq9ngw33mcxif")))

(define-public crate-aws-hyper-0.15.0 (c (n "aws-hyper") (v "0.15.0") (h "1wa2kh891417h941yb691a0l06zyh250x9y850g909pr34clckh9")))

(define-public crate-aws-hyper-0.46.0 (c (n "aws-hyper") (v "0.46.0") (h "1v0fmnjyghsk2kzr84wklxc4nfkrpb6333bjjckd4m52jkimrjxm")))

(define-public crate-aws-hyper-0.47.0 (c (n "aws-hyper") (v "0.47.0") (h "0smlhabqpz6pg00jr4jv5gakw67babakq7fnwdc6i8lqr5ywraa7")))

(define-public crate-aws-hyper-0.48.0 (c (n "aws-hyper") (v "0.48.0") (h "0kx0w4iczvjnz8z05vm4alj3npbwyfr4n2rm740a6rls3q8kn84h")))

(define-public crate-aws-hyper-0.49.0 (c (n "aws-hyper") (v "0.49.0") (h "090pgnvrvw909v3g8jdgxqcrwr2667qyz1zmvbnii14cy4k53pc0")))

(define-public crate-aws-hyper-0.50.0 (c (n "aws-hyper") (v "0.50.0") (h "18c2gxhgnwrdl7i782031b50wbygy6f6gd4x9hwf95ls31jbj6y3") (y #t)))

(define-public crate-aws-hyper-0.51.0 (c (n "aws-hyper") (v "0.51.0") (h "006ynvx0llc95p5rwcxhi1bxzwb895r9c0j1izlhay5vfhispxrj")))

(define-public crate-aws-hyper-0.52.0 (c (n "aws-hyper") (v "0.52.0") (h "1xigbcpq3zml0b1lhl7jvf5wkp2nsmr2yx31yzr932rny0nambfp")))

(define-public crate-aws-hyper-0.53.0 (c (n "aws-hyper") (v "0.53.0") (h "0jw29gf5cmcbgwqh9ynl86x6mpyyr7ls3ysywfi3xw760jis629f")))

(define-public crate-aws-hyper-0.54.1 (c (n "aws-hyper") (v "0.54.1") (h "08yh519r6hcqig4jirmipws0lam8mql8cw6mqzj9916dn1nw9kcl")))

(define-public crate-aws-hyper-0.55.0 (c (n "aws-hyper") (v "0.55.0") (h "081p1w2kvmkig9pangxwq2dwx2lxgl1qv67vgls312xpv1j51yqb")))

(define-public crate-aws-hyper-0.55.1 (c (n "aws-hyper") (v "0.55.1") (h "1m0qhk9s8a62y8ji58mvxijjqc4h34bv47zp76q2kj8bw6d73mdb")))

(define-public crate-aws-hyper-0.55.2 (c (n "aws-hyper") (v "0.55.2") (h "0w7ccf6vlhcg9g74c0121svc5kqb6fp3jnb0ybn8k8ls5gbgjf9x")))

(define-public crate-aws-hyper-0.55.3 (c (n "aws-hyper") (v "0.55.3") (h "0s6l4rwm6405jc7nwvakz5jhc4jb3ky7wqwgcngkgs64m5b8i9g0")))

(define-public crate-aws-hyper-0.56.0 (c (n "aws-hyper") (v "0.56.0") (h "0gy1fszq0yfl9s3x6hn7pn3g587w07yi5ww3lck19c1j1mvf60ya")))

(define-public crate-aws-hyper-0.56.1 (c (n "aws-hyper") (v "0.56.1") (h "074nkdwslm4qn7zkhd8lcy2xrmbx0kg9yahqbwywfj2r87jk5kr9")))

(define-public crate-aws-hyper-0.57.1 (c (n "aws-hyper") (v "0.57.1") (h "1mb6aqhfmwf2ijkada11ijrs5hqxmcp0gbbjdgdzr752wxyp9fpl")))

(define-public crate-aws-hyper-0.57.2 (c (n "aws-hyper") (v "0.57.2") (h "1s4gagsvdqr8ncgz13k3yffbqr8d7chshn3r3i1km8iscqhxag2q")))

(define-public crate-aws-hyper-0.58.0 (c (n "aws-hyper") (v "0.58.0") (h "1dnkcx542217rn8qmkdvyng9wf131qj327np0vyxwj0xiqvq5bha")))

(define-public crate-aws-hyper-0.59.0 (c (n "aws-hyper") (v "0.59.0") (h "06r98axgc7ribgcmc71j6j7iza8m6z79psc27l8h87gx4ni0rgpw")))

(define-public crate-aws-hyper-0.60.0 (c (n "aws-hyper") (v "0.60.0") (h "0zn4ligmnnicxwklgzmjwxps5zzn1dcx7z16qfcz8n1pxif2jrx0")))

(define-public crate-aws-hyper-0.61.0 (c (n "aws-hyper") (v "0.61.0") (h "0iv5sc9brw6xjy2qh73kwi22rc0nymxpf4dvixbpmqxa7n66b4rz") (y #t)))

(define-public crate-aws-hyper-0.60.1 (c (n "aws-hyper") (v "0.60.1") (h "1i1yw3qpl1fv8m0qhz63qjsa7issscc2h28b2vh4zjj52006wdfa")))

(define-public crate-aws-hyper-0.60.2 (c (n "aws-hyper") (v "0.60.2") (h "1g0yhixn6nladck7c6ll14gw22bfiih92r2j286hr6zxiv2a3h0s")))

(define-public crate-aws-hyper-0.60.3 (c (n "aws-hyper") (v "0.60.3") (h "0jy0rhg5gnb84zpjycnj6m2mrmsxkzkhwv03lcys2l320na3lzbx")))

