(define-module (crates-io aw s- aws-smithy-xml) #:use-module (crates-io))

(define-public crate-aws-smithy-xml-0.27.0-alpha (c (n "aws-smithy-xml") (v "0.27.0-alpha") (d (list (d (n "aws-smithy-protocol-test") (r "^0.27.0-alpha") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "01yc7ffpkkhqyip7vhxiyn829zzhf27m4pg17x9c183aj0s2z6xk")))

(define-public crate-aws-smithy-xml-0.27.0-alpha.1 (c (n "aws-smithy-xml") (v "0.27.0-alpha.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.27.0-alpha.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1p9xqxjb9pxx7zlh9jwx092w2prixk7sbdab4x23nrfcvvsl04bm")))

(define-public crate-aws-smithy-xml-0.27.0-alpha.2 (c (n "aws-smithy-xml") (v "0.27.0-alpha.2") (d (list (d (n "aws-smithy-protocol-test") (r "^0.27.0-alpha.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1rwp4gi0lhcs2845ixnqkli3l3n3fwfwgwd3jkk0vbgbcdxcvz8z") (y #t)))

(define-public crate-aws-smithy-xml-0.28.0-alpha (c (n "aws-smithy-xml") (v "0.28.0-alpha") (d (list (d (n "aws-smithy-protocol-test") (r "^0.28.0-alpha") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "04gyw2izc2d1x0bip68jbn2hlnp564mdan73aanc7hy3spfnh45b")))

(define-public crate-aws-smithy-xml-0.30.0-alpha (c (n "aws-smithy-xml") (v "0.30.0-alpha") (d (list (d (n "aws-smithy-protocol-test") (r "^0.30.0-alpha") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1m3080ndk9c75gwj4nd9cganqfs94n2gbfa27mv4bkvwq8flv22w")))

(define-public crate-aws-smithy-xml-0.31.0 (c (n "aws-smithy-xml") (v "0.31.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.31.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "0n608l4nkpgfs2cl90dn93vfl61w82fszp325d76j73c3qcva8xh") (y #t)))

(define-public crate-aws-smithy-xml-0.32.0 (c (n "aws-smithy-xml") (v "0.32.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.32.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "0fy9fnabyzxwv0v6rvwp5ha28kbgvnkcahs1v4lhvqmcpripwz0n")))

(define-public crate-aws-smithy-xml-0.33.1 (c (n "aws-smithy-xml") (v "0.33.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.33.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "01zvxdr6x3zxk3hwd48rm2gl324m2wvr1xi5bzgj93gnlwvncii5")))

(define-public crate-aws-smithy-xml-0.34.0 (c (n "aws-smithy-xml") (v "0.34.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.34.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1b91nvrrr4zycmdffczcwh5sflvrc2ncx86n9hrlx26mf2fvxjcz")))

(define-public crate-aws-smithy-xml-0.34.1 (c (n "aws-smithy-xml") (v "0.34.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.34.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1wfy66dn90cxip2imav5ng35p3p24zv7n4x9164cyh35x4ibs6yv")))

(define-public crate-aws-smithy-xml-0.35.1 (c (n "aws-smithy-xml") (v "0.35.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.35.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1xb3lljnb8bfz0jswaf8a62ya5j43wpc0ca0m7cwkx31l5r86phq") (y #t)))

(define-public crate-aws-smithy-xml-0.35.2 (c (n "aws-smithy-xml") (v "0.35.2") (d (list (d (n "aws-smithy-protocol-test") (r "^0.35.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "01f4x8cl6wrbdnlahwc0w1yqbylrckhsm7v8w5pkj759m4h2l8ky")))

(define-public crate-aws-smithy-xml-0.36.0 (c (n "aws-smithy-xml") (v "0.36.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.36.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1i18kpj0paapnd519mmpqp2r1p4xk97jh4bbx6k78q58jijldc73")))

(define-public crate-aws-smithy-xml-0.37.0 (c (n "aws-smithy-xml") (v "0.37.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.37.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "06y1a52j9zwz40d98nyv1qdq0fjiwx6v7xwdbpr7xg085ch10inn")))

(define-public crate-aws-smithy-xml-0.38.0 (c (n "aws-smithy-xml") (v "0.38.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.38.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "17yaqdl3b7ki67vra9y2py1ic97gyjqizbdapjm5z1rzdkgck9ks")))

(define-public crate-aws-smithy-xml-0.39.0 (c (n "aws-smithy-xml") (v "0.39.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.39.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "0ik5i3wy7sl2bn3zxgv77ivgipc9la9zsj7bdi9x19mpjbypx742")))

(define-public crate-aws-smithy-xml-0.40.2 (c (n "aws-smithy-xml") (v "0.40.2") (d (list (d (n "aws-smithy-protocol-test") (r "^0.40.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "0zma33f31i690rjx2bn5szzc0bwbk9d77f2vac4cwb7sbf1xycf4")))

(define-public crate-aws-smithy-xml-0.41.0 (c (n "aws-smithy-xml") (v "0.41.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.41.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "0hxqkrcxvy32b1cdida4y5dl7a3a7m09nlcgn2idjj7j6dsdf08v")))

(define-public crate-aws-smithy-xml-0.42.0 (c (n "aws-smithy-xml") (v "0.42.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.42.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "156xfdzfjinhi0my9l6zxjw6vw4ajbnfa3jm8jxa5n60bf4xm5fp")))

(define-public crate-aws-smithy-xml-0.43.0 (c (n "aws-smithy-xml") (v "0.43.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.43.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "0np55mr9mkx2jl1f3gbmywn108ssc8qrf0qi34dnbfzyd59x61mn")))

(define-public crate-aws-smithy-xml-0.44.0 (c (n "aws-smithy-xml") (v "0.44.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.44.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "01a24m08dqa6rikymp72i79sk165d2n1sjr3fmvhc9xi3p94gidp")))

(define-public crate-aws-smithy-xml-0.45.0 (c (n "aws-smithy-xml") (v "0.45.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.45.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1rlazjhhx5hn3plvhavbkm63kjjrm0akq5g9s6f70cfmgp8g5gzl")))

(define-public crate-aws-smithy-xml-0.46.0 (c (n "aws-smithy-xml") (v "0.46.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.46.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1wrdhiw813gwxxlprvk72dr6anb3p0rn5afsv7jynjd51xnazb2a")))

(define-public crate-aws-smithy-xml-0.47.0 (c (n "aws-smithy-xml") (v "0.47.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.47.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "0p5zvlwv73y4520q6llgskgx0ar58p70dyzgdwls798mlxzq1wkc")))

(define-public crate-aws-smithy-xml-0.48.0 (c (n "aws-smithy-xml") (v "0.48.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.48.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "09fr0ja843h97ap69q839ddgw0vfq7vfivgh3h24zc7y2mx8wv9c")))

(define-public crate-aws-smithy-xml-0.49.0 (c (n "aws-smithy-xml") (v "0.49.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.49.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1pbm4ls4r9cd0ypbng3qqkbvsg0gd12nsxqsjqlwnjjvhnsfzf9n")))

(define-public crate-aws-smithy-xml-0.50.0 (c (n "aws-smithy-xml") (v "0.50.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.50.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "=0.13.3") (d #t) (k 0)))) (h "1d7q76772717kz9484kqgqx8vkan4y2xrq5yvx3g82f43j6yasp6") (y #t)))

(define-public crate-aws-smithy-xml-0.51.0 (c (n "aws-smithy-xml") (v "0.51.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.51.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1kcldxj79a5gjhysjx6dwhnd9ld962ny82m3gws5vpqzvn1ryvi4")))

(define-public crate-aws-smithy-xml-0.52.0 (c (n "aws-smithy-xml") (v "0.52.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.52.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1n55lqa2ga7vig644zr98ccb7rhwkxaml6l5mkzpbv2fw68kgkip")))

(define-public crate-aws-smithy-xml-0.53.0 (c (n "aws-smithy-xml") (v "0.53.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.53.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "184crdimjwbjkg6n4mkq34di65fpw3wvfyn8da5z7zbmr1qkmmmi")))

(define-public crate-aws-smithy-xml-0.53.1 (c (n "aws-smithy-xml") (v "0.53.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.53.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1phfbwf9dd03w5rsb22y4y571glalwxah9hpfan4maaa2smby9vd")))

(define-public crate-aws-smithy-xml-0.54.0 (c (n "aws-smithy-xml") (v "0.54.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.54.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "19h3gmb26kr74zkgi8xg96g5djfkr1ap933w7sxlffhlqnh41da7")))

(define-public crate-aws-smithy-xml-0.54.1 (c (n "aws-smithy-xml") (v "0.54.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.54.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1806nq88wjd7scm3sakrk28p0xhh0sh4v58ix0gkcjiz08phhwws")))

(define-public crate-aws-smithy-xml-0.54.2 (c (n "aws-smithy-xml") (v "0.54.2") (d (list (d (n "aws-smithy-protocol-test") (r "^0.54.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "13gcf3aj5wmfmnjgnbkfrcmrzidi37m98il7h7qiprnda5v94mqg") (y #t)))

(define-public crate-aws-smithy-xml-0.54.3 (c (n "aws-smithy-xml") (v "0.54.3") (d (list (d (n "aws-smithy-protocol-test") (r "^0.54.3") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1d6m4lfwp207m4zx1i8wqs9xss3zxvd38arzdd2md5m4nk5jjc0s")))

(define-public crate-aws-smithy-xml-0.54.4 (c (n "aws-smithy-xml") (v "0.54.4") (d (list (d (n "aws-smithy-protocol-test") (r "^0.54.4") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "114nm68acyvns5x918wc0fnxc4rmjgay1w2dbxkl5xdkkfdgwgrl")))

(define-public crate-aws-smithy-xml-0.55.0 (c (n "aws-smithy-xml") (v "0.55.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.55.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "16gwlvb4ndyvna282fmmgv5jzx3477vzx946ql0hn2adfchrj5dv")))

(define-public crate-aws-smithy-xml-0.55.1 (c (n "aws-smithy-xml") (v "0.55.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.55.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1hxyb5w4x92ap623l6syw7bcj1x8dak5459b5266zz2xqb0rhlwb")))

(define-public crate-aws-smithy-xml-0.55.2 (c (n "aws-smithy-xml") (v "0.55.2") (d (list (d (n "aws-smithy-protocol-test") (r "^0.55.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "0k36x481cwdpm2fkwy1aj3f73ab68qjvnjr4zf3yrnavk7j7ymgv")))

(define-public crate-aws-smithy-xml-0.55.3 (c (n "aws-smithy-xml") (v "0.55.3") (d (list (d (n "aws-smithy-protocol-test") (r "^0.55.3") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "0fx6big2mdgdvsfyqc37nlvk233hjnnvmrvvfrzd06vkflld3fdi")))

(define-public crate-aws-smithy-xml-0.56.0 (c (n "aws-smithy-xml") (v "0.56.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.56.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "0ygals72018j5dnimqs91w7gy2ajadirqwmsswx195pn2b455068")))

(define-public crate-aws-smithy-xml-0.56.1 (c (n "aws-smithy-xml") (v "0.56.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.56.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1di39dwr3243jkhxhrg3zi3x9wjrdhyfpzkcf51k00nqrpnjs7g0")))

(define-public crate-aws-smithy-xml-0.57.0 (c (n "aws-smithy-xml") (v "0.57.0") (d (list (d (n "aws-smithy-protocol-test") (r "~0.57") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "0yph0qlfhssf70d2lvx09nszwsg21kfs2q7mk8zk3by9aisha5zc")))

(define-public crate-aws-smithy-xml-0.57.1 (c (n "aws-smithy-xml") (v "0.57.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.57.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1bdlwi258w8mbqhdf79xqcayydq88j1alr7p7id5cv1y7s6mq8k8")))

(define-public crate-aws-smithy-xml-0.57.2 (c (n "aws-smithy-xml") (v "0.57.2") (d (list (d (n "aws-smithy-protocol-test") (r "^0.57.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1b4fw6y2bivnzsr10mmb1i9g7il0mzwscdwfdh4n49k55k13l7nb")))

(define-public crate-aws-smithy-xml-0.58.0 (c (n "aws-smithy-xml") (v "0.58.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.58.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1wj6sqksyxrzisb18yg9653602sw54b2swaw20jix87y9af19znw")))

(define-public crate-aws-smithy-xml-0.59.0 (c (n "aws-smithy-xml") (v "0.59.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.59.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "0a750hi5iw0pscfwmdbx9ywgp7493pw7aq07cmp5gfc4106hll4p")))

(define-public crate-aws-smithy-xml-0.60.0 (c (n "aws-smithy-xml") (v "0.60.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.60.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "15fb62vsbbl9jsbkzvq5v4i2crs32pgwnk3b7yy9blvzlrs0vi0f")))

(define-public crate-aws-smithy-xml-0.61.0 (c (n "aws-smithy-xml") (v "0.61.0") (d (list (d (n "aws-smithy-protocol-test") (r "^0.61.0") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1ayyxh45lcj2zf0g4xqrsfaia463191dhx7gsb6nxp6r5gb5s258") (y #t)))

(define-public crate-aws-smithy-xml-0.60.1 (c (n "aws-smithy-xml") (v "0.60.1") (d (list (d (n "aws-smithy-protocol-test") (r "^0.60.1") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "0qw26yifllfc1ippfgn8p1zvy1a3n25h80mi28dqiz47cn9073rf")))

(define-public crate-aws-smithy-xml-0.60.2 (c (n "aws-smithy-xml") (v "0.60.2") (d (list (d (n "aws-smithy-protocol-test") (r "^0.60.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "04yw00m6b62rndwzagg4z2cgczd966w38b7ib6xcn8jcnkibx12s")))

(define-public crate-aws-smithy-xml-0.60.3 (c (n "aws-smithy-xml") (v "0.60.3") (d (list (d (n "aws-smithy-protocol-test") (r "^0.60.3") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1fkfhzgwmvp2xv77r4m354dyswzyxxx26dcjhq1zvmwlz3m6yygg")))

(define-public crate-aws-smithy-xml-0.60.4 (c (n "aws-smithy-xml") (v "0.60.4") (d (list (d (n "aws-smithy-protocol-test") (r "^0.60.4") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "066jbrd1blwn3q3z0xrld9h2ypiw4pzbjmmaz4razv794lds2146")))

(define-public crate-aws-smithy-xml-0.60.5 (c (n "aws-smithy-xml") (v "0.60.5") (d (list (d (n "aws-smithy-protocol-test") (r "^0.60.5") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "006n5ak1gi1lxlpa37pm1cr1q249vv4gwgiw5skvf4ilcz4r8vyi")))

(define-public crate-aws-smithy-xml-0.60.6 (c (n "aws-smithy-xml") (v "0.60.6") (d (list (d (n "aws-smithy-protocol-test") (r "^0.60.6") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "0hlai2s3xnb8537h2k7b5zwqajjik5mycj15kygq7jnhjpsxik0g")))

(define-public crate-aws-smithy-xml-0.60.7 (c (n "aws-smithy-xml") (v "0.60.7") (d (list (d (n "aws-smithy-protocol-test") (r "^0.60.7") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1abc6jwjwm74yldpf4ijcrqlpkl8f97kqxg7bpy4l3lw077nhb47")))

(define-public crate-aws-smithy-xml-0.60.8 (c (n "aws-smithy-xml") (v "0.60.8") (d (list (d (n "aws-smithy-protocol-test") (r "^0.60.7") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "0mdz0v73m58cn977ifgzb9rd3habpx4y3a1bcl0w7hxdlk1gn8yi")))

