(define-module (crates-io aw #{20}# aw2013) #:use-module (crates-io))

(define-public crate-aw2013-1.0.0 (c (n "aw2013") (v "1.0.0") (d (list (d (n "rppal") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1n5s75kvll28ysc8zk5l1n8gi5gday94cf1sw8v6f87ai00mn8a5")))

(define-public crate-aw2013-1.0.1 (c (n "aw2013") (v "1.0.1") (d (list (d (n "rppal") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "09f4262fnw642mal12v7aigmz0g0xi30gc45fs0a1s0z7wapwk4n")))

(define-public crate-aw2013-2.0.0 (c (n "aw2013") (v "2.0.0") (d (list (d (n "embedded-hal") (r "=1.0.0-rc.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4.0-alpha.4") (d #t) (t "cfg(target_os = \"linux\")") (k 2)))) (h "1rwvzv307xbm9bk6bjybyfwq7n4sgp7pnfp9jqlhdwc828d79g7j")))

(define-public crate-aw2013-2.1.0 (c (n "aw2013") (v "2.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (t "cfg(target_os = \"linux\")") (k 2)))) (h "0irfpbw67nrqh8wciks41852l9i11wlc2zcpayyypl3cjxz6c6y3")))

