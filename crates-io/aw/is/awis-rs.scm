(define-module (crates-io aw is awis-rs) #:use-module (crates-io))

(define-public crate-awis-rs-0.1.0 (c (n "awis-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1z921jn5bgwfj90zrqz70iqbadr7hg1r90j5s48r624922hcavf0")))

(define-public crate-awis-rs-0.1.1 (c (n "awis-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0cxy32i84jlmpl379ddqmgw5gi3dsy45s0n7rs5c0g3mnp4xkzxa")))

