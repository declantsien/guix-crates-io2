(define-module (crates-io aw oo awoo) #:use-module (crates-io))

(define-public crate-awoo-0.1.0 (c (n "awoo") (v "0.1.0") (d (list (d (n "try-guard") (r "^0.1") (d #t) (k 0)))) (h "0rx5rvfzg71zhv4g6bcafj5792j69x5bk34cifbj4fgnx91c2f5j")))

(define-public crate-awoo-0.2.0 (c (n "awoo") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "try-guard") (r "^0.2") (d #t) (k 0)))) (h "0irfrfv0w747g3dnji9jmbcl1g5kh6pn21gj6f9dng8ain3ajha7") (f (quote (("json" "serde") ("default" "json"))))))

