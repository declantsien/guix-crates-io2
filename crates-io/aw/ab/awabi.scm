(define-module (crates-io aw ab awabi) #:use-module (crates-io))

(define-public crate-awabi-0.1.0 (c (n "awabi") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0q6k0afrvaq8pnvm7f8r1q1nx2jiq6k7ygyxz2hk30l879smp5ik")))

(define-public crate-awabi-0.2.0 (c (n "awabi") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "01vcl073lcs1bwjjykq0gfy1jz353fzdv2p1wnkkvspnl6hzfqzm")))

(define-public crate-awabi-0.2.1 (c (n "awabi") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1zgw9vy4prf5x43ypqijfn0amy771q9pcq3rpbgsrh8cb9q5hfcc")))

(define-public crate-awabi-0.2.2 (c (n "awabi") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0hw09pxl51ga3pzrqlm84h97kn3dr3374zxnj0pwlxhd84mj7csh")))

(define-public crate-awabi-0.2.3 (c (n "awabi") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1jfc68951kpkvqyizpxi3yifhpd53jk9g903g10rxw7xan2xmk3b")))

(define-public crate-awabi-0.2.4 (c (n "awabi") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "19kvxfraw1brvxhgryybq6msz5pz7i0fcj3hsh357qgk1xmiqwhx")))

(define-public crate-awabi-0.2.5 (c (n "awabi") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0a1cinc9bw32nn610yhzq7s60xfqdls17cgq9vn42yc0wcmsp45r")))

(define-public crate-awabi-0.2.6 (c (n "awabi") (v "0.2.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1vaw8ld1dfxm825q4gkj78mgsr1cqz7v21hn7lapkq4cfn58nr9v")))

(define-public crate-awabi-0.2.7 (c (n "awabi") (v "0.2.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "03cpn5fivvkgkp00a42wdwcyx78z9n1a2i8nskb2yi54a3scn3ld")))

(define-public crate-awabi-0.2.8 (c (n "awabi") (v "0.2.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "15ciqkj9y1d464piqps2wikg2l3v1fcffmpwsbb9jkhrq5vw5rwj")))

(define-public crate-awabi-0.2.9 (c (n "awabi") (v "0.2.9") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1zzgxsw7k0ywcf2idgrajvncbkl3m5qr12glfq4brgvjxxv5jnxq")))

(define-public crate-awabi-0.2.10 (c (n "awabi") (v "0.2.10") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0vh6gsxhb1nhyzjdh1y263fsz975iil8bl23xnd95za6p7saj6cb")))

(define-public crate-awabi-0.3.0 (c (n "awabi") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0izcj51awdfnp5zf2ibk8bwhp1h4vmgkh6izh97cshmwgq9518nd")))

