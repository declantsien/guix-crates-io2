(define-module (crates-io aw -d aw-datastore) #:use-module (crates-io))

(define-public crate-aw-datastore-0.1.0 (c (n "aw-datastore") (v "0.1.0") (d (list (d (n "appdirs") (r "^0.2") (d #t) (k 0)) (d (n "aw-models") (r "^0.1") (d #t) (k 0)) (d (n "aw-transform") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mpsc_requests") (r "^0.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23") (f (quote ("chrono" "serde_json" "bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gi10bqm8hsq53na2wlgq0i968gq8fx6yfhf7v4msw9fppw83fm8") (f (quote (("legacy_import_tests") ("default"))))))

