(define-module (crates-io aw sm awsm_webgl) #:use-module (crates-io))

(define-public crate-awsm_webgl-0.0.1 (c (n "awsm_webgl") (v "0.0.1") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.43") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.20") (f (quote ("console" "WebGlUniformLocation" "WebGlBuffer" "CanvasRenderingContext2d" "WebGlRenderingContext" "WebGl2RenderingContext" "ImageBitmap" "ImageData" "HtmlImageElement" "HtmlCanvasElement" "HtmlVideoElement" "WebGlProgram" "WebGlShader" "WebGlTexture" "Window"))) (d #t) (k 0)))) (h "002va1jgjgya7kx8vz1xgphxrnjvs3l3zqw80wwg6n37znr5kih3") (y #t)))

