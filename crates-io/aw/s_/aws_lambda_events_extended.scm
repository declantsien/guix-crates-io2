(define-module (crates-io aw s_ aws_lambda_events_extended) #:use-module (crates-io))

(define-public crate-aws_lambda_events_extended-0.1.0 (c (n "aws_lambda_events_extended") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jqarnj0pbvldxq03kd22fb0z7mgjgmc69cs1j4cddlwnm1lba58")))

