(define-module (crates-io aw s_ aws_account_id_from_key_id) #:use-module (crates-io))

(define-public crate-aws_account_id_from_key_id-0.8.1 (c (n "aws_account_id_from_key_id") (v "0.8.1") (h "0cj874fjrqaj50y6z1kanyjkfyicrgkkzjp2w01j7rji2yrpicag")))

(define-public crate-aws_account_id_from_key_id-0.8.2 (c (n "aws_account_id_from_key_id") (v "0.8.2") (h "0qrjxp3yyyzi19sa577nzlz2z7frzk6gwrpkzvfyi6pwlpc6xynb")))

