(define-module (crates-io aw s_ aws_cred) #:use-module (crates-io))

(define-public crate-aws_cred-0.0.1 (c (n "aws_cred") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0aab0pj292wj3zb14fp6qh823whrl7shzb94dc0k6b48zssmm031")))

(define-public crate-aws_cred-0.0.2 (c (n "aws_cred") (v "0.0.2") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "01d13mpdjic6g0bxr1f39qy8a1x0c2splmvi2g5h3xc5abh2zaa5")))

(define-public crate-aws_cred-0.0.3 (c (n "aws_cred") (v "0.0.3") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0zi15qqjd6v7yl0l6akazq7320qnay7f4xwibg2b3mhccc4m1a7z")))

(define-public crate-aws_cred-0.0.4 (c (n "aws_cred") (v "0.0.4") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "aws-sdk-sts") (r "^0.31.1") (o #t) (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rusoto_sts") (r "^0.48.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "test-util" "macros"))) (d #t) (k 2)))) (h "1902vnk3mbkjza559i9wgkmld3pzb8gl13ag8abn9vs583c50kkq") (f (quote (("rusoto" "rusoto_sts") ("default") ("aws_sdk" "aws-sdk-sts") ("async_std" "async-std"))))))

