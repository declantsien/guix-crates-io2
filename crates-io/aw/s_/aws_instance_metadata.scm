(define-module (crates-io aw s_ aws_instance_metadata) #:use-module (crates-io))

(define-public crate-aws_instance_metadata-0.1.0 (c (n "aws_instance_metadata") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "rusoto") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1kwkzzbrricrvvhc1sjs2klk5k32k0l7rf101a6lbnz0736yglhl")))

(define-public crate-aws_instance_metadata-0.1.1 (c (n "aws_instance_metadata") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "rusoto") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0g399dcbnsgxj8r2gvvm4ddrfk16izmyz5aa3qbr9gwdigka1mb8")))

