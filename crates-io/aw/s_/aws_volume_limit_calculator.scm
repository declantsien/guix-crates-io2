(define-module (crates-io aw s_ aws_volume_limit_calculator) #:use-module (crates-io))

(define-public crate-aws_volume_limit_calculator-0.1.0 (c (n "aws_volume_limit_calculator") (v "0.1.0") (h "1v0h5rzqnypw4sr3rwsyxs1yv6qrr0igpxjlk6q0ymnz1p2vqwvs")))

(define-public crate-aws_volume_limit_calculator-0.1.1 (c (n "aws_volume_limit_calculator") (v "0.1.1") (h "1730l39hlfclfmwbalh6bv73v3jsi4hb0kr4p6yg770qhj8vc570")))

