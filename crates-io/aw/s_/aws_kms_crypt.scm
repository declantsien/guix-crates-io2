(define-module (crates-io aw s_ aws_kms_crypt) #:use-module (crates-io))

(define-public crate-aws_kms_crypt-0.1.0 (c (n "aws_kms_crypt") (v "0.1.0") (d (list (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "base64-serde") (r "^0.2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hex-serde") (r "^0.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.31.0") (d #t) (k 0)) (d (n "rusoto_kms") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z93hw7wp5m6bhi210n04lx5sk4psgm539hsm0mjbb5j5szpqw57")))

