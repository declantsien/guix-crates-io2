(define-module (crates-io aw in awint_internals) #:use-module (crates-io))

(define-public crate-awint_internals-0.1.0-pre.0 (c (n "awint_internals") (v "0.1.0-pre.0") (h "0mfrv1k53k9xs79x9p95fscm8w665d942m4wzwwvhnwn9jg14g6w") (y #t)))

(define-public crate-awint_internals-0.1.0 (c (n "awint_internals") (v "0.1.0") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "0vhrqdggc7f80i3hpcg5lmrkcj5znpvkih0cay9i2l7m4wyyh0r4") (f (quote (("default" "const_support") ("const_support"))))))

(define-public crate-awint_internals-0.2.0 (c (n "awint_internals") (v "0.2.0") (h "1lzqbnhs9ahrfaw19m39947x4bwy6ycila2hgrr76l5v9hwiwz19")))

(define-public crate-awint_internals-0.3.0 (c (n "awint_internals") (v "0.3.0") (h "07vcdypgxzi31ycwbxs76qqlsdkdg6shvqn1x09f5kyj75i77iw9")))

(define-public crate-awint_internals-0.4.0 (c (n "awint_internals") (v "0.4.0") (h "0cfm81jy65y2byg98b0w12sqn2x61vgaws1bs8rq8rbzgwpafydl")))

(define-public crate-awint_internals-0.5.0 (c (n "awint_internals") (v "0.5.0") (h "1h3m1hjr4lh80xymk82p5yh9ang7zdad67lzkas9mc5plpm2xdcy") (r "1.61")))

(define-public crate-awint_internals-0.6.0 (c (n "awint_internals") (v "0.6.0") (h "0i0zb2bmh30q1j3lidll53kqshz0sgljr5cxsv8qigv7j3vc16kl") (r "1.61")))

(define-public crate-awint_internals-0.7.0 (c (n "awint_internals") (v "0.7.0") (h "1q19yisfqv960fispv0v998gnz3qckc2vh68lvmgds29ph5qbh0j") (r "1.61")))

(define-public crate-awint_internals-0.8.0 (c (n "awint_internals") (v "0.8.0") (h "1rmsysc4zlyqvi4aha3bfylfmkpmjhy48r57792cswwipxbxvm5v") (r "1.66")))

(define-public crate-awint_internals-0.9.0 (c (n "awint_internals") (v "0.9.0") (h "091ww6nj3spgjrir4n0s6gwl8bc5lqm7xlk0ipc83fdhjsir3qai") (r "1.66")))

(define-public crate-awint_internals-0.10.0 (c (n "awint_internals") (v "0.10.0") (h "1dgrw4dbfa70dlsl0pw1v38rs7l2q9dkxgqq3a0a7mwnf544bhq6") (f (quote (("u8_digits") ("u64_digits") ("u32_digits") ("u16_digits") ("u128_digits")))) (r "1.66")))

(define-public crate-awint_internals-0.11.0 (c (n "awint_internals") (v "0.11.0") (h "1548d6kq3vdbfw3m7rz68d97bfxnz4vrmj8pi6ma927pjc38crkg") (f (quote (("u8_digits") ("u64_digits") ("u32_digits") ("u16_digits") ("u128_digits")))) (r "1.66")))

(define-public crate-awint_internals-0.12.0 (c (n "awint_internals") (v "0.12.0") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "07h1pdhd1w3m2cmwv1qywywcyv3l67hv2hs1qfnm4ndfn49llsw0") (f (quote (("u8_digits") ("u64_digits") ("u32_digits") ("u16_digits") ("u128_digits") ("const_support")))) (r "1.70")))

(define-public crate-awint_internals-0.13.0 (c (n "awint_internals") (v "0.13.0") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "1v8sh0w92x7llzwqfrk4nhyxdvnyxyp3x82j97j8c16x49k2pv30") (f (quote (("u8_digits") ("u64_digits") ("u32_digits") ("u16_digits") ("u128_digits") ("const_support")))) (r "1.70")))

(define-public crate-awint_internals-0.14.0 (c (n "awint_internals") (v "0.14.0") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "0zpmigll07rap3hxz46rdwva455aa2rw14br3rzpxndila7swh76") (f (quote (("u8_digits") ("u64_digits") ("u32_digits") ("u16_digits") ("u128_digits") ("const_support")))) (r "1.70")))

(define-public crate-awint_internals-0.16.0 (c (n "awint_internals") (v "0.16.0") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "1dm9r0nh9cya8ma27k7sb4553gwh5is1iidr4n4j3kmphyz63v8f") (f (quote (("u8_digits") ("u64_digits") ("u32_digits") ("u16_digits") ("u128_digits") ("const_support")))) (r "1.70")))

(define-public crate-awint_internals-0.17.0 (c (n "awint_internals") (v "0.17.0") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "16ls62shjzyzp0kc6vgp045h9akq51n6361wh5hvfqz8h6x7csh2") (f (quote (("u8_digits") ("u64_digits") ("u32_digits") ("u16_digits") ("u128_digits") ("const_support")))) (r "1.70")))

