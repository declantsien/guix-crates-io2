(define-module (crates-io aw in awint_macros) #:use-module (crates-io))

(define-public crate-awint_macros-0.1.0-pre.0 (c (n "awint_macros") (v "0.1.0-pre.0") (d (list (d (n "awint_core") (r "^0.1.0-pre.0") (k 0)) (d (n "awint_ext") (r "^0.1.0-pre.0") (k 0)))) (h "0zvxwsqq5j06rgapja6k8rfch759yci119ydbrkjzarg8ycyfbp7") (y #t)))

(define-public crate-awint_macros-0.1.0 (c (n "awint_macros") (v "0.1.0") (d (list (d (n "awint_internals") (r "^0.1.0") (k 0)) (d (n "awint_macro_internals") (r "^0.1.0") (d #t) (k 0)))) (h "1psdq99znlv0vx19fb3jd7m57wsxf4kczbaasq9qgknzgwj3n2b9")))

(define-public crate-awint_macros-0.2.0 (c (n "awint_macros") (v "0.2.0") (d (list (d (n "awint_internals") (r "^0.2.0") (k 0)) (d (n "awint_macro_internals") (r "^0.2.0") (d #t) (k 0)))) (h "0qqx36rma2j5kkyjawz5gmsmixb1f2nm0n2dhjqajpnrg6zanx59")))

(define-public crate-awint_macros-0.3.0 (c (n "awint_macros") (v "0.3.0") (d (list (d (n "awint_internals") (r "^0.3.0") (k 0)) (d (n "awint_macro_internals") (r "^0.3.0") (d #t) (k 0)))) (h "02gzigq51amzsah1gysl90a2laai9l5kvvw63sbw8jzgdmbfaagm")))

(define-public crate-awint_macros-0.4.0 (c (n "awint_macros") (v "0.4.0") (d (list (d (n "awint_internals") (r "^0.4.0") (k 0)) (d (n "awint_macro_internals") (r "^0.4.0") (d #t) (k 0)))) (h "0hkafx44fhdlz7ff68if3glpavns16pqcq4wrkvyd9pcbam3si54")))

(define-public crate-awint_macros-0.5.0 (c (n "awint_macros") (v "0.5.0") (d (list (d (n "awint_internals") (r "^0.5.0") (k 0)) (d (n "awint_macro_internals") (r "^0.5.0") (d #t) (k 0)))) (h "0s3ighb4pvddvkw11bas5fk8gzx3d7nk5kp05hgnxc8kdg6iw9y6")))

(define-public crate-awint_macros-0.6.0 (c (n "awint_macros") (v "0.6.0") (d (list (d (n "awint_internals") (r "^0.6.0") (k 0)) (d (n "awint_macro_internals") (r "^0.6.0") (d #t) (k 0)))) (h "0rjhxkjhsijl72yr030660czq9qiqrwj7b0hr9qslh3wbzj1m2j3")))

(define-public crate-awint_macros-0.7.0 (c (n "awint_macros") (v "0.7.0") (d (list (d (n "awint_internals") (r "^0.7.0") (k 0)) (d (n "awint_macro_internals") (r "^0.7.0") (d #t) (k 0)))) (h "0058gvpmgj71y4h6bn08g3frlprjfd2l8jf8n03rbcdlf9yjmqa8")))

(define-public crate-awint_macros-0.8.0 (c (n "awint_macros") (v "0.8.0") (d (list (d (n "awint_internals") (r "^0.8.0") (k 0)) (d (n "awint_macro_internals") (r "^0.8.0") (d #t) (k 0)))) (h "1fkmc7dk9s8cpb9n1brwmbby4ylagmzfh1jkm1l9jlkii6wgz953")))

(define-public crate-awint_macros-0.9.0 (c (n "awint_macros") (v "0.9.0") (d (list (d (n "awint_internals") (r "^0.9.0") (k 0)) (d (n "awint_macro_internals") (r "^0.9.0") (d #t) (k 0)))) (h "14phxp2v2qd8p0lsnsr2b58iy6h52407528xa19bcad2wcvjnsa7")))

(define-public crate-awint_macros-0.10.0 (c (n "awint_macros") (v "0.10.0") (d (list (d (n "awint_internals") (r "^0.10.0") (k 0)) (d (n "awint_macro_internals") (r "^0.10.0") (d #t) (k 0)))) (h "181pdq7rjs8mh1mqr9r2xgj8sah392c53h353rb5zvw33cnhrg3i")))

(define-public crate-awint_macros-0.11.0 (c (n "awint_macros") (v "0.11.0") (d (list (d (n "awint_internals") (r "^0.11.0") (k 0)) (d (n "awint_macro_internals") (r "^0.11.0") (d #t) (k 0)))) (h "1908b306f60v2xqqjkkprn9fz353npndhc134wl5plpfnw613v7a")))

(define-public crate-awint_macros-0.12.0 (c (n "awint_macros") (v "0.12.0") (d (list (d (n "awint_internals") (r "^0.12.0") (k 0)) (d (n "awint_macro_internals") (r "^0.12.0") (d #t) (k 0)))) (h "17hxnmbr1anccp8v4npydg23wlmwb4rv3b7widncjaasqjy5y45c")))

(define-public crate-awint_macros-0.13.0 (c (n "awint_macros") (v "0.13.0") (d (list (d (n "awint_internals") (r "^0.13.0") (k 0)) (d (n "awint_macro_internals") (r "^0.13.0") (d #t) (k 0)))) (h "0a543qams231938bgi244kkbyq7ccxyz4g93r5nldy951wn85lzc")))

(define-public crate-awint_macros-0.14.0 (c (n "awint_macros") (v "0.14.0") (d (list (d (n "awint_internals") (r "^0.14.0") (k 0)) (d (n "awint_macro_internals") (r "^0.14.0") (d #t) (k 0)))) (h "17scayyzirfal9a40rl5jzns80bcvg3pds0a0k189fq09a2617is")))

(define-public crate-awint_macros-0.16.0 (c (n "awint_macros") (v "0.16.0") (d (list (d (n "awint_internals") (r "^0.16.0") (k 0)) (d (n "awint_macro_internals") (r "^0.16.0") (d #t) (k 0)))) (h "12acq3s8i0jnfjb3h8g2mn8nh8visb7y5z7vhr5kkkk5sw25qfyp")))

(define-public crate-awint_macros-0.17.0 (c (n "awint_macros") (v "0.17.0") (d (list (d (n "awint_internals") (r "^0.17.0") (k 0)) (d (n "awint_macro_internals") (r "^0.17.0") (d #t) (k 0)))) (h "0p5gpgihhm9zij01yb2l0v9q3gik2rimhvvwgzkvc3fvhl0pahnm")))

