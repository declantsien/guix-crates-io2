(define-module (crates-io aw in awint_macro_internals) #:use-module (crates-io))

(define-public crate-awint_macro_internals-0.1.0 (c (n "awint_macro_internals") (v "0.1.0") (d (list (d (n "awint_core") (r "^0.1.0") (k 0)) (d (n "awint_ext") (r "^0.1.0") (k 0)))) (h "0dlb9brgz4jxnhzf7dviwmsn4qy715jnvhkc3psjwyq61xl5b2f6")))

(define-public crate-awint_macro_internals-0.2.0 (c (n "awint_macro_internals") (v "0.2.0") (d (list (d (n "awint_core") (r "^0.2.0") (k 0)) (d (n "awint_ext") (r "^0.2.0") (k 0)))) (h "05wps7q3qmr3hvwxw5dk53l2pfy5xwcm4125fdir1gcdjckzzw4i")))

(define-public crate-awint_macro_internals-0.3.0 (c (n "awint_macro_internals") (v "0.3.0") (d (list (d (n "awint_core") (r "^0.3.0") (k 0)) (d (n "awint_ext") (r "^0.3.0") (k 0)))) (h "1xahk8z709qpqj3a6844m7xzrplffxr80j6dnr5q480zid074pxv")))

(define-public crate-awint_macro_internals-0.4.0 (c (n "awint_macro_internals") (v "0.4.0") (d (list (d (n "awint_core") (r "^0.4.0") (k 0)) (d (n "awint_ext") (r "^0.4.0") (k 0)))) (h "0xfy270qnckaq1xljn2qyn6lz27zyfm42jc6s1kvscb1awllqc4g")))

(define-public crate-awint_macro_internals-0.5.0 (c (n "awint_macro_internals") (v "0.5.0") (d (list (d (n "awint_core") (r "^0.5.0") (k 0)) (d (n "awint_ext") (r "^0.5.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.5") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.5") (o #t) (d #t) (k 0)))) (h "12wf6ncrkch4nm7wqgpnay7lvkbnw5y9yzwag7qqf7hns79jazdc") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.6.0 (c (n "awint_macro_internals") (v "0.6.0") (d (list (d (n "awint_core") (r "^0.6.0") (k 0)) (d (n "awint_ext") (r "^0.6.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.5") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1bn18rxxiv0kh14fihkkz9zpxnpqqqmwdiy56gp1yw99npngaysj") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.7.0 (c (n "awint_macro_internals") (v "0.7.0") (d (list (d (n "awint_core") (r "^0.7.0") (k 0)) (d (n "awint_ext") (r "^0.7.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.6") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1wpaqjsl4bk4zxivy92d073c1amprljb1k0jwbzyyc8drvhv18l6") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.8.0 (c (n "awint_macro_internals") (v "0.8.0") (d (list (d (n "awint_ext") (r "^0.8.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.7") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.7") (o #t) (d #t) (k 0)))) (h "11qha0bl46zi7z8cpmxmifan982n6dj3h8kskr384wslpss84a66") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.9.0 (c (n "awint_macro_internals") (v "0.9.0") (d (list (d (n "awint_ext") (r "^0.9.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.7") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1b2vfhx6q8d7nv5sb093kbs4mvmarri7b1alv45d9hwimiqddvyd") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.10.0 (c (n "awint_macro_internals") (v "0.10.0") (d (list (d (n "awint_ext") (r "^0.10.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.7") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0b17n8rp1350ylvpdj116jcsfkrz92g4297mr7fdnfhxhmg0gy2m") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.11.0 (c (n "awint_macro_internals") (v "0.11.0") (d (list (d (n "awint_ext") (r "^0.11.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.9") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0z4ar72lf5s9ksaq2z2zg1kcxpy335w4j24x1ncc5rvz51gkxxji") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.12.0 (c (n "awint_macro_internals") (v "0.12.0") (d (list (d (n "awint_ext") (r "^0.12.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.12") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1ad1njzgqx8fnxnwyn9y3d95q351m1rfssfsym7wbwba9m0hmppl") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.13.0 (c (n "awint_macro_internals") (v "0.13.0") (d (list (d (n "awint_ext") (r "^0.13.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.12") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1ggmpxmyswnb24fx218f82xpkprb2lzcixw255jg37xwfbzzpfrp") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.14.0 (c (n "awint_macro_internals") (v "0.14.0") (d (list (d (n "awint_ext") (r "^0.14.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.12") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1flw4iq9kxy45p3crb7f2q87z87ps8abffmf38qh4n470mwb97j0") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.16.0 (c (n "awint_macro_internals") (v "0.16.0") (d (list (d (n "awint_ext") (r "^0.16.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.12") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.12") (o #t) (d #t) (k 0)))) (h "03d2lm7s2wi6lhalqkfm5gqq7sypgdk6v4rh1wdkhb7w5ihbj3hb") (f (quote (("debug" "triple_arena_render"))))))

(define-public crate-awint_macro_internals-0.17.0 (c (n "awint_macro_internals") (v "0.17.0") (d (list (d (n "awint_ext") (r "^0.17.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "triple_arena") (r "^0.13") (d #t) (k 0)) (d (n "triple_arena_render") (r "^0.13") (o #t) (d #t) (k 0)))) (h "07jvdgz4182l4mnl9nnjy8g9jf4dbwika6xj8n3qmh9xbqlxy668") (f (quote (("debug" "triple_arena_render"))))))

