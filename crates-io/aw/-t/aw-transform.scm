(define-module (crates-io aw -t aw-transform) #:use-module (crates-io))

(define-public crate-aw-transform-0.1.0 (c (n "aw-transform") (v "0.1.0") (d (list (d (n "aw-models") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s9qm2b8f90ghyrbkn5yv1a3x1dz9zibhcj7lg0vdbridm6m99zm")))

