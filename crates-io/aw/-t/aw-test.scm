(define-module (crates-io aw -t aw-test) #:use-module (crates-io))

(define-public crate-aw-test-0.0.1 (c (n "aw-test") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1fp6pvb3b4fjv6nw89g9583nbivmbydpzjignlczgan6a7cqbvf3")))

(define-public crate-aw-test-0.0.2 (c (n "aw-test") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0gd5b7xzrkbi49x2jj150b41m7qj2l6m6l919bg1blawzh29xryc")))

