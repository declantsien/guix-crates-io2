(define-module (crates-io rs _d rs_dsa) #:use-module (crates-io))

(define-public crate-rs_dsa-0.1.0 (c (n "rs_dsa") (v "0.1.0") (h "0j83mz6x5sqq4jsjpxs7ivq2x8jhqzzcz2cmfg22s306i9jva71n")))

(define-public crate-rs_dsa-0.1.1 (c (n "rs_dsa") (v "0.1.1") (h "1dbmi1dypk316vq2na6hp2fbd2qlpr1dlhjl8i7s4bd5mhli4j2k")))

(define-public crate-rs_dsa-0.1.2 (c (n "rs_dsa") (v "0.1.2") (h "1kf8q9a55znfvgv0c37yyxfwiqpwsrma0r0wk5jcg6slj37vp5m0")))

