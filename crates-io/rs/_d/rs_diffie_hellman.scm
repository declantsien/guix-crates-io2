(define-module (crates-io rs _d rs_diffie_hellman) #:use-module (crates-io))

(define-public crate-rs_diffie_hellman-0.1.0 (c (n "rs_diffie_hellman") (v "0.1.0") (h "1ncb60rf3l18fnzr8k10czam8r7fvlpygcnvkxs99ajggv33bkg0")))

(define-public crate-rs_diffie_hellman-0.1.1 (c (n "rs_diffie_hellman") (v "0.1.1") (h "19bjxckql093hc060jnkd5xgd52n9jpzzqcbb8nl8f84bh4f2n1b")))

(define-public crate-rs_diffie_hellman-0.1.2 (c (n "rs_diffie_hellman") (v "0.1.2") (h "1dqh63qkir32dx23qglgmyn21s7ry2rs56p2gpay739xrmhjiyap")))

