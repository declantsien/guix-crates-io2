(define-module (crates-io rs #{2j}# rs2js) #:use-module (crates-io))

(define-public crate-rs2js-0.1.0 (c (n "rs2js") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "rs2js_macro") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0mbai5m1x7y3ijjm96jx52wy3yzpy5zpckq6aylp8vl1f1k9pz0d")))

(define-public crate-rs2js-0.2.0 (c (n "rs2js") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "rs2js_macro") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1pby6s2z6ap3y6mzvz9dv085z2zgvyqfql7r6r1djs3qn2ymylqk")))

(define-public crate-rs2js-0.2.1 (c (n "rs2js") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "rs2js_macro") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0biym6q6kxvvfyq870lighi1h7gaal8vg0yha3n014ki8ga1hqz4")))

(define-public crate-rs2js-0.2.2 (c (n "rs2js") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "rs2js_macro") (r "^0.2.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0nxvcnr9ihx1kgi54zipg0z0ajw9nwi9y5g8jv3ax1fw8zarw2lg")))

(define-public crate-rs2js-0.2.3 (c (n "rs2js") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "rs2js_macro") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1pqmg61rxjaj38l36kkn022rjns2ji0iwvbbk18fy4mwrchp4nl0")))

