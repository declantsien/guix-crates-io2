(define-module (crates-io rs #{2j}# rs2js_macro) #:use-module (crates-io))

(define-public crate-rs2js_macro-0.1.0 (c (n "rs2js_macro") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12dmxwdzkqpd6fmji4xql16g1jfikvlk62x2zibv5zzmsbxzlpgl")))

(define-public crate-rs2js_macro-0.2.0 (c (n "rs2js_macro") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0lxvgjy96459i5js5hrfb0jr8wn5icyjv7327568kdw1llslwzy1")))

(define-public crate-rs2js_macro-0.2.2 (c (n "rs2js_macro") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0dizb458iribig44hdnlxsan1h086wmzsqfd45v14b90f0gqviwi")))

(define-public crate-rs2js_macro-0.2.3 (c (n "rs2js_macro") (v "0.2.3") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1bqk1kbm485p6nnvrsr8iq5aisawhf3251d1lh0vnjvn6fsv9k6k")))

