(define-module (crates-io rs fl rsflex) #:use-module (crates-io))

(define-public crate-rsflex-0.2.3 (c (n "rsflex") (v "0.2.3") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "19l0av4ncwcbmyydr214j0gkv4986gznczdfb1bx6ldr3q2mjs53")))

(define-public crate-rsflex-0.3.0 (c (n "rsflex") (v "0.3.0") (d (list (d (n "cmd_lib") (r "^0.8.5") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.24") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1ib03szi5hy0wqhw2mfiwz1zp5znqwgls3cd2dpqrxllrvp5l90k")))

