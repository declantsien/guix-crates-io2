(define-module (crates-io rs vr rsvr_tweak) #:use-module (crates-io))

(define-public crate-rsvr_tweak-0.0.1-alpha.0 (c (n "rsvr_tweak") (v "0.0.1-alpha.0") (d (list (d (n "glium") (r "^0.22.0") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.2") (d #t) (k 0)) (d (n "ron") (r "^0.4.0") (d #t) (k 0)) (d (n "rsvr") (r "^0.0.1-alpha.0") (d #t) (k 0)) (d (n "rsvr_opengl") (r "^0.0.1-alpha.0") (d #t) (k 0)) (d (n "rsvr_profile") (r "^0.0.1-alpha.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)) (d (n "wavefront_obj") (r "^5.1.0") (d #t) (k 0)))) (h "0gms4is6icarqgyq58bnjff7sgwlb4sh5lp0mkapzz1dq9pdcrsf")))

