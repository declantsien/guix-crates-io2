(define-module (crates-io rs vr rsvr_opengl) #:use-module (crates-io))

(define-public crate-rsvr_opengl-0.0.1-alpha.0 (c (n "rsvr_opengl") (v "0.0.1-alpha.0") (d (list (d (n "glium") (r "^0.22.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.2") (d #t) (k 0)) (d (n "rental") (r "^0.5.2") (d #t) (k 0)) (d (n "rsvr_profile") (r "^0.0.1-alpha.0") (d #t) (k 0)) (d (n "rsvr_webrtc") (r "^0.0.1-alpha.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "099n2cv3y415rc8gvlcyazv862cn90ydqf6f03icmygc3axijk3q")))

