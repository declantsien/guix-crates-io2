(define-module (crates-io rs pg rspg) #:use-module (crates-io))

(define-public crate-rspg-0.0.1-alpha.1 (c (n "rspg") (v "0.0.1-alpha.1") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "034fpk5svgkmsx0ljhqyn1ymg0702l4y139wrq7q8jfa4sk2zv5b")))

(define-public crate-rspg-0.0.1 (c (n "rspg") (v "0.0.1") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "13dyq96j7vkr1bp12nz5b7876kzd1sr83zj7xnmai6kgs5477zxd")))

(define-public crate-rspg-0.0.2 (c (n "rspg") (v "0.0.2") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "17761kxysd2131fadb22qx22fk9rbhjjr71skcs6q0mm59k677cf")))

(define-public crate-rspg-0.0.3 (c (n "rspg") (v "0.0.3") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rgvqrawjxx6w2lgr4gpaa8ih7822spg2ysyxm6fs794w5niqi6w")))

