(define-module (crates-io rs pg rspg-macros) #:use-module (crates-io))

(define-public crate-rspg-macros-0.0.1-alpha.1 (c (n "rspg-macros") (v "0.0.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "rspg") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^1.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1piai43js6r2yz4n8w2hnywywv6p9k1m35y2az41885r25vzyrir")))

(define-public crate-rspg-macros-0.0.1 (c (n "rspg-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "rspg") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^1.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vbj1jca9sbp47l4j3s6gn7hbr5ydk42lidcaagxnq5jcfgzf0sz") (y #t)))

(define-public crate-rspg-macros-0.0.2 (c (n "rspg-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "rspg") (r "^0.0.2-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^1.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g9irgx67px77f2vic6a6ja20rd8hnm2jpxx82z4vgn46qsrf00x")))

(define-public crate-rspg-macros-0.0.3 (c (n "rspg-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ron") (r "^0.6.1") (d #t) (k 0)) (d (n "rspg") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z3vg2chsir768ifdp7537i7xbbdr1gy9m6x460165yi5cbcyf0a")))

