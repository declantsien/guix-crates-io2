(define-module (crates-io rs ys rsys_macro) #:use-module (crates-io))

(define-public crate-rsys_macro-0.1.0 (c (n "rsys_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "0nj5vw4hz4m3niaryc35hn7macrdcsxy85135lndghifgpdgp2ak")))

(define-public crate-rsys_macro-0.1.1 (c (n "rsys_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "1xya4440j9g1qscvj6wp1ldbksvp9f96313ply9m8qbf6lkgh0wy")))

(define-public crate-rsys_macro-0.1.2 (c (n "rsys_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)))) (h "0ppyqd1lwjjf4as7zg6kp9k2gxnxijpcfljdd4gbm30ya8x167ii")))

