(define-module (crates-io rs ha rshacker) #:use-module (crates-io))

(define-public crate-rshacker-0.1.0 (c (n "rshacker") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "spinach") (r "^2.1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "10wnzv0hcp64k7mp5ngxrcgm50gyxlzq4vpjihvz872c5j7xzjbl")))

(define-public crate-rshacker-0.2.0 (c (n "rshacker") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "spinach") (r "^2.1.0") (d #t) (k 0)))) (h "0lixlqmbv0f1psddaf64z1h8pll89hqb9pl9yz1jycz7dh0l22if")))

(define-public crate-rshacker-0.2.1 (c (n "rshacker") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "spinach") (r "^2.1.0") (d #t) (k 0)))) (h "0w7gkwsakizvfg12j93d6vh98ijlz15gdnvhqbld727a8sy78pr0")))

