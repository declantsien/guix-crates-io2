(define-module (crates-io rs ha rshader) #:use-module (crates-io))

(define-public crate-rshader-0.1.0 (c (n "rshader") (v "0.1.0") (d (list (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "0rln4g6is5rk149cdlp4zlwbz0a256iwr3rvkhg42r02ppsahy1j") (f (quote (("dynamic_shaders") ("default"))))))

