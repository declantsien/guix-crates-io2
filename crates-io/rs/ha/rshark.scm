(define-module (crates-io rs ha rshark) #:use-module (crates-io))

(define-public crate-rshark-0.0.1 (c (n "rshark") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "docopt") (r "^0.6.70") (d #t) (k 0)) (d (n "pcap") (r "^0.4.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1ypm87jx39nh8yvfbs9kr70na4alha23fj2dqnj78b68fcy87srk")))

