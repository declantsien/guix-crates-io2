(define-module (crates-io rs ki rskit) #:use-module (crates-io))

(define-public crate-rskit-0.0.1 (c (n "rskit") (v "0.0.1") (d (list (d (n "fast_log") (r "^1.6.12") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1cjdnklv8n4rlqmgr7w42z68m3g28dknz60zgy38pjvjxkmrd8v7")))

(define-public crate-rskit-0.0.2 (c (n "rskit") (v "0.0.2") (d (list (d (n "fast_log") (r "^1.6.12") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "1f079b7nvvnp665wsmwcj2jv30jmji7siq62ahvj05nx781kanbw")))

(define-public crate-rskit-0.0.3 (c (n "rskit") (v "0.0.3") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 0)) (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "fast_log") (r "^1.6.12") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "06iyr0676v3cmf8lrdnpvi6v4i0mlbf1v60lmnn9rk7syij49vv1")))

(define-public crate-rskit-0.0.4 (c (n "rskit") (v "0.0.4") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 0)) (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "fast_log") (r "^1.6.12") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0ghbcfi07dzgqrs3r0sss99smjaarg9rwg442jjcbsfn83jv9c7x")))

