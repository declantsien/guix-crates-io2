(define-module (crates-io rs -g rs-graph-derive) #:use-module (crates-io))

(define-public crate-rs-graph-derive-0.9.0 (c (n "rs-graph-derive") (v "0.9.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rs-graph") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1vq97ffm642l0kkr9f0qhwh2jla1c5fqji1952wycqj88n1n2bcx")))

(define-public crate-rs-graph-derive-0.10.0 (c (n "rs-graph-derive") (v "0.10.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "rs-graph") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0dz3q9kv29il3yvq5fdx2amqb9ydgb5ypv62d74f09pqljwnz0yk")))

(define-public crate-rs-graph-derive-0.11.0 (c (n "rs-graph-derive") (v "0.11.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "rs-graph") (r "^0.11") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0h1369r1p7fh5baikbkwdhh8bd8rrbkrgvl7vs3vwc8amv9kmxx6")))

(define-public crate-rs-graph-derive-0.12.0 (c (n "rs-graph-derive") (v "0.12.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "rs-graph") (r "^0.12") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0lf1f12jr0x0ggmns3wlwjfkjqya1dzla1pzfvbsyfz9gk8znx4a")))

(define-public crate-rs-graph-derive-0.13.0 (c (n "rs-graph-derive") (v "0.13.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "rs-graph") (r "^0.13") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0y9g533z7qglzbvkkw52s6ixrgarnrn2p6d1an8gavgjhx2apyap")))

(define-public crate-rs-graph-derive-0.14.0 (c (n "rs-graph-derive") (v "0.14.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "rs-graph") (r "^0.14") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1gn9115wjnz5lgxgj6gcv2wp455py76582px612s6l341h2jy20r")))

(define-public crate-rs-graph-derive-0.14.1 (c (n "rs-graph-derive") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "rs-graph") (r "^0.14") (d #t) (k 0)) (d (n "syn") (r "^0.14.7") (d #t) (k 0)))) (h "14hayc931wn7qv6laxxfqxfdw97xwfxq1h6y67jq34qpdq6n9bim")))

(define-public crate-rs-graph-derive-0.15.0 (c (n "rs-graph-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "rs-graph") (r "^0.14") (d #t) (k 2)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0jca8hicmk1awz2qbb48biq4kv9k4rs445kkavrm86ci169rbx2h")))

(define-public crate-rs-graph-derive-0.16.0 (c (n "rs-graph-derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "rs-graph") (r "^0.16.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "11lh3iicmmz411xkvhmy9m311d1jjar5ghx2gcknhfacdjc42f27")))

(define-public crate-rs-graph-derive-0.16.1 (c (n "rs-graph-derive") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "rs-graph") (r "^0.16.1") (d #t) (k 2)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0jar6s5phjpmf3rzgw5j5k46snhgws3x0ay1gm7gff2w67zbv1rm")))

(define-public crate-rs-graph-derive-0.16.2 (c (n "rs-graph-derive") (v "0.16.2") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "rs-graph") (r "^0.16") (d #t) (k 2)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "1fhrl9j8ffqm7c3limw4ax039p0j3scql8jrp5x9cww83mla16jd")))

(define-public crate-rs-graph-derive-0.17.0 (c (n "rs-graph-derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "rs-graph") (r "^0.17") (d #t) (k 2)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "1awjk26wp9m96ffw3if52944qqwryhhqir6d1if8xhi5s97z5qf8")))

(define-public crate-rs-graph-derive-0.17.1 (c (n "rs-graph-derive") (v "0.17.1") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "rs-graph") (r "^0.17.1") (d #t) (k 2)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "1l1wsb0yvf4jhnv2lkdxxn7qb4vas1wfj6asd7x8rp1gr4flw549")))

(define-public crate-rs-graph-derive-0.17.2 (c (n "rs-graph-derive") (v "0.17.2") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.5") (d #t) (k 0)) (d (n "rs-graph") (r "^0.17.1") (d #t) (k 2)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "17w6md4yxg9h06x2kq851dbwr55igxciqjvzrkdsw6zypab6vmi7")))

(define-public crate-rs-graph-derive-0.18.0 (c (n "rs-graph-derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rs-graph") (r "^0.18.0") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "16jznrlzj5rb0lphd3pviw0sxjrr1196pw9x39dklrhdpmdwnscm")))

(define-public crate-rs-graph-derive-0.19.0 (c (n "rs-graph-derive") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1x4zi3hyn1y4mdhflrv2acysb90vgm0hahsd6wwq2k637ha267wj")))

(define-public crate-rs-graph-derive-0.19.1 (c (n "rs-graph-derive") (v "0.19.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rs-graph") (r "^0.19") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ya8pjlrqrcpb85xpa0yf5amsmqc1gbjqvbsv7s943i3glp043z8")))

(define-public crate-rs-graph-derive-0.19.2 (c (n "rs-graph-derive") (v "0.19.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rs-graph") (r "^0.19") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pmavpzggsxk87pqlf3lnm165l9cwsc815dqg4vdjnxi8ifzfa40")))

(define-public crate-rs-graph-derive-0.20.0 (c (n "rs-graph-derive") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rs-graph") (r "^0.20") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vnhwbhhsz22i3dsvlvjpz2cm4j5j3f4lkcfxdxn9akp53znqqsj")))

(define-public crate-rs-graph-derive-0.20.1 (c (n "rs-graph-derive") (v "0.20.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rs-graph") (r "^0.20.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1j16c4ngsyz8aiscp530hc5nfb5gb4axlw58fdmlw781pw1ak7ly")))

(define-public crate-rs-graph-derive-0.21.0 (c (n "rs-graph-derive") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rs-graph") (r "^0.21.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "07b1qc3srl9qrpxhi1g4k2kgwnn10aqqqpw0199vglsf3r33qcw6")))

