(define-module (crates-io rs -g rs-glfw3) #:use-module (crates-io))

(define-public crate-rs-glfw3-0.1.0 (c (n "rs-glfw3") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.0") (d #t) (k 0)))) (h "0yvdivfrg1jx4ysla3v42wi4wzdg12dkfmycj9k4chp202fmyh1q") (f (quote (("withbindgen" "bindgen"))))))

(define-public crate-rs-glfw3-0.1.1 (c (n "rs-glfw3") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "0igllvzjq2lfwpvarmdrimiz2zcdczkkmx0pixhlc3kll98pfbfi") (f (quote (("withbindgen" "bindgen"))))))

(define-public crate-rs-glfw3-0.1.2 (c (n "rs-glfw3") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "16s2r3mrs61cqmw32aac9ipgpdjl2nb1z7sbcwlc8m0g1iw9cdb2") (f (quote (("withbindgen" "bindgen"))))))

(define-public crate-rs-glfw3-0.1.3 (c (n "rs-glfw3") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "18ixbl35m0aqvjfbfp9bzhmd19bqqjdr2l2hbjadnlzcq6xxsr6q") (f (quote (("withbindgen" "bindgen"))))))

(define-public crate-rs-glfw3-0.1.4 (c (n "rs-glfw3") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.54.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "1phb7cdamqzncgjw5lhzncmjnjv6mmfpa3f187m7a4agr3b4glm4") (f (quote (("withbindgen" "bindgen"))))))

(define-public crate-rs-glfw3-0.1.5 (c (n "rs-glfw3") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "0c4b9hhcciwm5nhaqpybvypq1r53wp9alc8a5qiqbvyp811qm3hr")))

(define-public crate-rs-glfw3-0.1.6 (c (n "rs-glfw3") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "1kc43vj9z2n1pg2ldxn2dww0bi6a7r6zgc7v4kvic47d78phv5f0")))

