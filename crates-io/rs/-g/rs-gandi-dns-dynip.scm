(define-module (crates-io rs -g rs-gandi-dns-dynip) #:use-module (crates-io))

(define-public crate-rs-gandi-dns-dynip-0.1.2 (c (n "rs-gandi-dns-dynip") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zjx5pxgdsvnch58svsn557w7cx21mckzvnf5wkg19lcpfmyq0h8")))

(define-public crate-rs-gandi-dns-dynip-0.1.3 (c (n "rs-gandi-dns-dynip") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09b5vqa7qjd5n9dqxp3618w86mpwjp9j55zr60dgi3afimin2flb")))

