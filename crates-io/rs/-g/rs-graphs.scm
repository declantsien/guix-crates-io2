(define-module (crates-io rs -g rs-graphs) #:use-module (crates-io))

(define-public crate-rs-graphs-0.1.1 (c (n "rs-graphs") (v "0.1.1") (h "1vg5bvi8any7kn1cgwgbbiq0mvcb5nk79vc8fr43zz15dmfcikjw")))

(define-public crate-rs-graphs-0.1.2 (c (n "rs-graphs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gmypvpdqmb5xjg94v25f3jglxsbfx7gmrsjz8p8xf226iyy4lyi")))

