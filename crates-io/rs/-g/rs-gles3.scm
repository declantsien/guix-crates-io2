(define-module (crates-io rs -g rs-gles3) #:use-module (crates-io))

(define-public crate-rs-gles3-0.1.0 (c (n "rs-gles3") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "1rxwvi48wc09dw731ci75rbbps1kw87mzfqmzy2xwxbn8dxmzq4v") (f (quote (("withbindgen" "bindgen"))))))

