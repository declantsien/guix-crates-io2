(define-module (crates-io rs -g rs-gles2) #:use-module (crates-io))

(define-public crate-rs-gles2-0.1.0 (c (n "rs-gles2") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.0") (d #t) (k 0)))) (h "1yx347giq90vd6z8lzwxhkl00ngwr20l0shi1kddbl8aa5l4n5j0") (f (quote (("withbindgen" "bindgen"))))))

(define-public crate-rs-gles2-0.1.1 (c (n "rs-gles2") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "12cx2nw6k954fkryxjq6wa0jsvrh89nmg9vglzg28xqgdk363rs5") (f (quote (("withbindgen" "bindgen"))))))

(define-public crate-rs-gles2-0.1.2 (c (n "rs-gles2") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "12367728bmhgi9skm59ri8bnkkyxwj58qiapjz3bax4ja63i02am") (f (quote (("withbindgen" "bindgen"))))))

(define-public crate-rs-gles2-0.1.3 (c (n "rs-gles2") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "15y2a30a1yym4xhj8axw9i0xfvbnnlg4v2l6wai8rhckds8w94zr")))

(define-public crate-rs-gles2-0.1.4 (c (n "rs-gles2") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.59") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "1wa7c0wlkbk255rdy80im6y0sihjj0p8xr3crqkwvpcgbw1wxj5r")))

