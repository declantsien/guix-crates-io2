(define-module (crates-io rs dp rsdp) #:use-module (crates-io))

(define-public crate-rsdp-1.0.0 (c (n "rsdp") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1gaxcqfyrflxh5kpca42yi70s959h2b1qagwa9qxp19c2fq6rcj9")))

(define-public crate-rsdp-1.1.0-pre0 (c (n "rsdp") (v "1.1.0-pre0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0n31mcvcd40p8j0kh6aakzi30kq5w5x68hcjs7k0asbka33qq6dx")))

(define-public crate-rsdp-1.1.0 (c (n "rsdp") (v "1.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09jj1ybhw636842j5a13l4nnp6a4gmpqf5s1l28iqw40bgmkskrz")))

(define-public crate-rsdp-2.0.0 (c (n "rsdp") (v "2.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1c84qqw4l0i9daq3ibisn83zz7m0wy7a10fg3d8kgvsmzk9avlv6")))

(define-public crate-rsdp-2.0.1 (c (n "rsdp") (v "2.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06qhv35vkyrdhdv1m64pnl1y4g67pygibp9wq4sy7b4fp4xz7m8y")))

