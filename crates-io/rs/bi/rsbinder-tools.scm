(define-module (crates-io rs bi rsbinder-tools) #:use-module (crates-io))

(define-public crate-rsbinder-tools-0.1.2 (c (n "rsbinder-tools") (v "0.1.2") (d (list (d (n "anstyle") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)) (d (n "rsbinder") (r "^0.1.2") (d #t) (k 0)) (d (n "rsbinder-aidl") (r "^0.1.2") (d #t) (k 1)) (d (n "rsbinder-hub") (r "^0.1.2") (d #t) (k 0)))) (h "0pix8l5xc25sb7gp072625vd8f2azm3c5y5pi1n0cxl5b7ppnq0v") (y #t)))

(define-public crate-rsbinder-tools-0.2.0 (c (n "rsbinder-tools") (v "0.2.0") (d (list (d (n "anstyle") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)) (d (n "rsbinder") (r "^0.2.0") (d #t) (k 0)))) (h "11a0xldh9nqv10ga0ma806l36j26dhjd3ck5nm1gf1ax7vmf1n8g")))

(define-public crate-rsbinder-tools-0.2.1 (c (n "rsbinder-tools") (v "0.2.1") (d (list (d (n "anstyle") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)) (d (n "rsbinder") (r "^0.2.1") (d #t) (k 0)))) (h "14ls66qk8wrv879d3miafd85id8rmvxz44rnzqjqg14i1wrfrjf8")))

(define-public crate-rsbinder-tools-0.2.2 (c (n "rsbinder-tools") (v "0.2.2") (d (list (d (n "anstyle") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)) (d (n "rsbinder") (r "^0.2.2") (d #t) (k 0)))) (h "0y61sp9w1d1b655kp8pdzl53zqwd84sdgc5jywclqb1nr6shqs52")))

(define-public crate-rsbinder-tools-0.2.3 (c (n "rsbinder-tools") (v "0.2.3") (d (list (d (n "anstyle") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)) (d (n "rsbinder") (r "^0.2.3") (d #t) (k 0)))) (h "12b1dkvxq4yb7bgc6bb6nk7v91k5njnd13arxp9whwg7dffai5p3")))

