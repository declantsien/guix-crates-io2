(define-module (crates-io rs bi rsbinder-hub) #:use-module (crates-io))

(define-public crate-rsbinder-hub-0.1.0 (c (n "rsbinder-hub") (v "0.1.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsbinder") (r "^0.1.0") (d #t) (k 0)) (d (n "rsbinder-aidl") (r "^0.1.0") (d #t) (k 1)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 0)))) (h "119x4qfq70vhrjz836x8jd2qzv0s6yq5pci7s6qnpr16zsn50r19") (y #t)))

(define-public crate-rsbinder-hub-0.1.1 (c (n "rsbinder-hub") (v "0.1.1") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsbinder") (r "^0.1.1") (d #t) (k 0)) (d (n "rsbinder-aidl") (r "^0.1") (d #t) (k 1)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0irij0bj2adkpfjfpmsymqp89f2awjsx5y4hbyypkrx680hbvpbx") (y #t)))

(define-public crate-rsbinder-hub-0.1.2 (c (n "rsbinder-hub") (v "0.1.2") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsbinder") (r "^0.1.2") (d #t) (k 0)) (d (n "rsbinder-aidl") (r "^0.1.2") (d #t) (k 1)))) (h "0sbjcgci16krxh7hj8knrc1yj5wz2ys4phvih3fwmwx84bc9095n") (y #t)))

