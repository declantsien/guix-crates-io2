(define-module (crates-io rs bi rsbind-build-script) #:use-module (crates-io))

(define-public crate-rsbind-build-script-0.1.0 (c (n "rsbind-build-script") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "119zngs0f01b0lal4xr8xhzxzcxyzgih7iwxkspbi58bbfr6li5v")))

