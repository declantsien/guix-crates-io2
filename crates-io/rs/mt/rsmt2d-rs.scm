(define-module (crates-io rs mt rsmt2d-rs) #:use-module (crates-io))

(define-public crate-rsmt2d-rs-0.0.1 (c (n "rsmt2d-rs") (v "0.0.1") (h "07hpyxfifhn2qb0zglyqb2ma0dakxm8jwz6mlvx98c1w7wr5b80i")))

(define-public crate-rsmt2d-rs-0.0.2 (c (n "rsmt2d-rs") (v "0.0.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reed-solomon-erasure") (r "^6.0.0") (d #t) (k 0)))) (h "0dw939qs9g0gkx4nq0805m7mbx88wrd0i4jkckjpqfp72hrgw0i7")))

