(define-module (crates-io rs mt rsmtp) #:use-module (crates-io))

(define-public crate-rsmtp-0.0.3 (c (n "rsmtp") (v "0.0.3") (h "1vdkfvywz06xk2k2c3xg5rd6gcgrf1qqhwv00rfv3v0ws49fh4xi")))

(define-public crate-rsmtp-0.1.0 (c (n "rsmtp") (v "0.1.0") (h "0aai64i012fpffxf0hbp54m28jzga4c7rd112d6m0w2rlqmahw69")))

(define-public crate-rsmtp-0.1.1 (c (n "rsmtp") (v "0.1.1") (h "0gbwgi6n5vj3wwmfhkx16n6j7bg6fbgqdl32xck6fq2gzybzj53x")))

(define-public crate-rsmtp-0.1.2 (c (n "rsmtp") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1184angml3hdgasrqxzqgd5jdg3x3583pmzmsch4ih2wcwidnl3k")))

(define-public crate-rsmtp-0.1.3 (c (n "rsmtp") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wrkwv8hc4cdna6zpvhxfv4dwjdc1mvh7n992mhy247amiznrm1v")))

