(define-module (crates-io rs mt rsmt2) #:use-module (crates-io))

(define-public crate-rsmt2-0.2.0 (c (n "rsmt2") (v "0.2.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "1zs65p9qp7y550mjwhhxcb4klnxc4xf7kbh0gcggd1y2qsg4mli5")))

(define-public crate-rsmt2-0.4.0 (c (n "rsmt2") (v "0.4.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)))) (h "0dwxra9qr9zs9jyp0idf7nvrc5cpc4wnphhsry60ljnpl1ixyw0m")))

(define-public crate-rsmt2-0.4.1 (c (n "rsmt2") (v "0.4.1") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)))) (h "0n30lxlmyrb4zhs121vaqx96m1asl2ig43njqxwv4sn4zih23vyk")))

(define-public crate-rsmt2-0.4.2 (c (n "rsmt2") (v "0.4.2") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)))) (h "12grzghw9j871f43mm9r25168pq9grmfv5fnpm2fr4ah1ax51ddz")))

(define-public crate-rsmt2-0.4.3 (c (n "rsmt2") (v "0.4.3") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)))) (h "1wy2aqjfgb2i18wd3m93adp9g5wqs5iz9ql1bw0h75xlhkzfbnj7")))

(define-public crate-rsmt2-0.4.4 (c (n "rsmt2") (v "0.4.4") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)))) (h "08yyijcwd14m12js34xwvxzymhll0ikbygn6wv3l0vy9cjvfv5ml")))

(define-public crate-rsmt2-0.4.5 (c (n "rsmt2") (v "0.4.5") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)))) (h "085yra2x1l5hw3g88c5dh256cbm68lq6ybab637kzmvcpd0v44kg")))

(define-public crate-rsmt2-0.5.0 (c (n "rsmt2") (v "0.5.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)))) (h "1yhfdzm5py8ac1rgp9xs2pkh6p34341790ara5pvip5wlc4pka3j")))

(define-public crate-rsmt2-0.9.0 (c (n "rsmt2") (v "0.9.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "03i80l2ljpfvjcj17w5ahhbwz7fcm3d8qz95nyz1bx2bdkfzr9ik")))

(define-public crate-rsmt2-0.9.1 (c (n "rsmt2") (v "0.9.1") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "1agrhbsjngybilf6cphwn0hn8s6041jdzw9nyn3ji2sqqr7sqabm")))

(define-public crate-rsmt2-0.9.2 (c (n "rsmt2") (v "0.9.2") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "09kxrw20b4g2f94jrf5v0296cqdh6qb1pdazidnmi6cvmm7yy7jj")))

(define-public crate-rsmt2-0.9.3 (c (n "rsmt2") (v "0.9.3") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "04kpxgh3w6cabx01hz1dkwyp7jgd8q1ivmh83p4kv1xx8p4c1bhs")))

(define-public crate-rsmt2-0.9.5 (c (n "rsmt2") (v "0.9.5") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "1cn9z44r7vyb431fgbq0qcjfzqj6bm19sa57wslh9aywrnb9df0z")))

(define-public crate-rsmt2-0.9.6 (c (n "rsmt2") (v "0.9.6") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "1j8jraqi8alh572fafd1dwgy36ssgw3nxby2122lyyb788vzhfwh")))

(define-public crate-rsmt2-0.9.10 (c (n "rsmt2") (v "0.9.10") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "1v6b6b0k7b2sny3fbyqpryy7cbqrfjizz1ynim631297x9l4q2d1")))

(define-public crate-rsmt2-0.9.11 (c (n "rsmt2") (v "0.9.11") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "14xr4xa77kbmz9h00mzip3chi35mlh5119n084cyj9frfx478jar")))

(define-public crate-rsmt2-0.10.0 (c (n "rsmt2") (v "0.10.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "16h9a8rc1afmy6cq0prkkcfdxz4q3m2cy3cwcxvkhpfbg9ydmdwq")))

(define-public crate-rsmt2-0.11.0-beta.0 (c (n "rsmt2") (v "0.11.0-beta.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "1n1wnvpr199l9i73x80rkrysi54yb8r5m1irswkkpfimadzvnl5f")))

(define-public crate-rsmt2-0.11.0-beta.2 (c (n "rsmt2") (v "0.11.0-beta.2") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "0wx14zca632c4wq2n523rr066r10rgz8mz2kxa0yfdydyrlxsfv0")))

(define-public crate-rsmt2-0.11.0 (c (n "rsmt2") (v "0.11.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "164nww5yisdy0gwcskwqyvhilbc0lmd8zjb9r7sdchh6f763cm5a") (f (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.12.0 (c (n "rsmt2") (v "0.12.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "12fwikb9g0ybaa45rmhxqq4x5k0vbfrh2qqd392g94q3gynb25mn") (f (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.13.1 (c (n "rsmt2") (v "0.13.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "04l6za0a9acypz5rda8qv2xxf1z08lyvr76ww5g9c5x99adffa31") (f (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.14.0 (c (n "rsmt2") (v "0.14.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "167nsx32k6dnlrj1jhb64nf5d80a2qsxv10pniam7ss0q6qglf6m") (f (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.14.1 (c (n "rsmt2") (v "0.14.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "0z8ccpaiyv5cqgshqxhls6hkmgq2s15rfa2744p2sws1jalwizsa") (f (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.15.0 (c (n "rsmt2") (v "0.15.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "17w7rank7gpmxf4znan1j60345q9klsddgr8z0dk8l6pc4yldby9") (f (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.15.1 (c (n "rsmt2") (v "0.15.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "1v8yr50wishif64m7b90ipn2v4gpyb04j363jlib5931333b2254") (f (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.16.0 (c (n "rsmt2") (v "0.16.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "05lv64hzbssakylfbssgmcg3p8408mdwwqr3yznxw7v8692isny4") (f (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.16.1 (c (n "rsmt2") (v "0.16.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "01851kdcrpwfs8w85yzqpc750p5l2hk4xfixqgx5a6prrsq374cy") (f (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.16.2 (c (n "rsmt2") (v "0.16.2") (d (list (d (n "clap") (r "^3.2") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)))) (h "17kg468dbwm5b9pzxbwiyy7kz81z1qsm6s02lqw6mp6vbwz7vyrf") (f (quote (("examples") ("default"))))))

