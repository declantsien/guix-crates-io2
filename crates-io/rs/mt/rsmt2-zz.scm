(define-module (crates-io rs mt rsmt2-zz) #:use-module (crates-io))

(define-public crate-rsmt2-zz-0.11.0 (c (n "rsmt2-zz") (v "0.11.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "1ykvv56n21iangzqdqmys23s3sbj1dmg3vydhp0jk5lrbkywkd5x")))

(define-public crate-rsmt2-zz-0.11.1 (c (n "rsmt2-zz") (v "0.11.1") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)))) (h "1qyqh47dzr9d23vl9h49gd8wr266x4nf0ay4xqnzjjswmajrb30n")))

