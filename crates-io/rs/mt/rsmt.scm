(define-module (crates-io rs mt rsmt) #:use-module (crates-io))

(define-public crate-rsmt-0.1.0 (c (n "rsmt") (v "0.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "ring") (r "^0.14") (d #t) (k 0)))) (h "0agjyhrf1m29kczysa3cwav9jn67vvgd72789r0gn5y6lggcg0nj") (y #t)))

(define-public crate-rsmt-0.1.1 (c (n "rsmt") (v "0.1.1") (h "0xpn5y2x1p5gz2kg5d7hzizxsk7hll5dw21wg7qcb49gjv4a6pmy") (y #t)))

