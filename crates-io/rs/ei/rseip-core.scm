(define-module (crates-io rs ei rseip-core) #:use-module (crates-io))

(define-public crate-rseip-core-0.1.0 (c (n "rseip-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "inlinable_string") (r "^0.1.14") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "067naqviix2aj95dzypbgz7ng866rqqpzzjrkari8i811srl7rkf") (f (quote (("feat-inlinable-string" "inlinable_string") ("default" "feat-inlinable-string" "cip") ("cip" "bytes" "byteorder"))))))

(define-public crate-rseip-core-0.1.1 (c (n "rseip-core") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "inlinable_string") (r "^0.1.14") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "17hq7xk9h7x52rdd1mxzfl0vj4ramaz5xxaqpq8m8vh4pgr9x4rf") (f (quote (("feat-inlinable-string" "inlinable_string") ("default" "feat-inlinable-string" "cip") ("cip" "bytes" "byteorder"))))))

(define-public crate-rseip-core-0.1.2 (c (n "rseip-core") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "inlinable_string") (r "^0.1.14") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0zi45mc6763zpcplyixqf0nwaskirh1mr57l2xwgsgm135sm1fj7") (f (quote (("feat-inlinable-string" "inlinable_string") ("default" "feat-inlinable-string" "cip") ("cip" "bytes" "byteorder"))))))

(define-public crate-rseip-core-0.1.3 (c (n "rseip-core") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "inlinable_string") (r "^0.1.14") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0z04sbfpmi1z9ll0wnm2xb3bfz3g3kzasspfasngina34xwgkb9x") (f (quote (("feat-inlinable-string" "inlinable_string") ("default" "feat-inlinable-string" "cip") ("cip" "byteorder"))))))

