(define-module (crates-io rs hm rshmem) #:use-module (crates-io))

(define-public crate-rshmem-0.1.0 (c (n "rshmem") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "handleapi" "winbase" "errhandlingapi"))) (d #t) (k 0)))) (h "0lka5kzhkdjlj37wz62d0kvkl0v6piyjlb0nani2pzc8px2zsdx5")))

(define-public crate-rshmem-0.1.1 (c (n "rshmem") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "handleapi" "winbase" "errhandlingapi"))) (d #t) (k 0)))) (h "0py0ny1yal82gf1lnj6i13b2pbdjrqxxqhmph1pnbf362zcq5dvs")))

(define-public crate-rshmem-0.1.2 (c (n "rshmem") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "handleapi" "winbase" "errhandlingapi"))) (d #t) (k 0)))) (h "0zppbkq2nz3mxs66lxqphbilj6s99aj9d7sqgiss318q78cj17mn")))

