(define-module (crates-io rs hm rshm) #:use-module (crates-io))

(define-public crate-rshm-0.1.0 (c (n "rshm") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.131") (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1knvn6rx2m0z6c0ccvj4yiyiphbc89w3jwj0z30v52impfkj3h6n")))

