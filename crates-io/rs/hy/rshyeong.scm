(define-module (crates-io rs hy rshyeong) #:use-module (crates-io))

(define-public crate-rshyeong-0.1.1 (c (n "rshyeong") (v "0.1.1") (d (list (d (n "clap") (r "~2.20.1") (f (quote ("color" "wrap_help" "yaml"))) (k 0)) (d (n "num") (r "^0.1.36") (f (quote ("bigint" "rational"))) (k 0)))) (h "1xw9hpckvpi0izz5hpz2qg1njdir59a9g3xry3sra3bmbq1dfvx1") (f (quote (("big-rational"))))))

(define-public crate-rshyeong-0.2.0 (c (n "rshyeong") (v "0.2.0") (d (list (d (n "clap") (r "~2.20.1") (f (quote ("color" "wrap_help" "yaml"))) (k 0)) (d (n "num") (r "^0.1.36") (f (quote ("bigint" "rational"))) (k 0)))) (h "0k0x03z92piij4qlin3z1lkafqjr6zhpkgl5j84ab01xn74vms3n") (f (quote (("big-rational"))))))

(define-public crate-rshyeong-0.2.1 (c (n "rshyeong") (v "0.2.1") (d (list (d (n "clap") (r "~2.20.1") (f (quote ("color" "wrap_help" "yaml"))) (k 0)) (d (n "num") (r "^0.1.36") (f (quote ("bigint" "rational"))) (k 0)))) (h "11kxv56d03kslsjd4h9389dsr7k83xkyn0qmv26p2jrvpi4fzp0d") (f (quote (("big-rational"))))))

(define-public crate-rshyeong-0.3.0 (c (n "rshyeong") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("std" "color" "wrap_help" "derive"))) (k 0)) (d (n "num-rational") (r "^0.4.0") (f (quote ("num-bigint"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "195hydnj9giv31n6jg5nd1z9g5xjc2aia39p37ilagjp110hx9r4") (f (quote (("big-rational"))))))

