(define-module (crates-io rs op rsop) #:use-module (crates-io))

(define-public crate-rsop-0.1.0 (c (n "rsop") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "rpgpie-sop") (r "^0.1") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "1g7ifh1ms9q3f9p09hankig64irc6ihnb8wllf4nshm8sr2s1rpg") (f (quote (("default" "cli") ("cli" "sop/cli"))))))

(define-public crate-rsop-0.2.0 (c (n "rsop") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "rpgpie-sop") (r "^0.2") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "0w30jmv98wkvf3dqnban9341ymril90mwl33a671328ifp6b1q8b") (f (quote (("default" "cli") ("cli" "sop/cli"))))))

(define-public crate-rsop-0.2.1 (c (n "rsop") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "rpgpie-sop") (r "^0.2") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "0riqsr8xwgmnvvdrs0nqcb0f9i7rrdizv4w9papwsmg4h6nicn0d") (f (quote (("default" "cli") ("cli" "sop/cli"))))))

(define-public crate-rsop-0.3.0 (c (n "rsop") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "rpgpie-sop") (r "^0.3") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "0dyravna9sy9gfp2d66g7k3wz143zvi205ifzf6xpwccdpahkv44") (f (quote (("default" "cli") ("cli" "sop/cli"))))))

(define-public crate-rsop-0.3.1 (c (n "rsop") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "rpgpie-sop") (r "^0.3") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "19vjb3rzdqvn6j9p002z7s0n79w5qz11xxgrij3frrjcs8w7xqgl") (f (quote (("default" "cli") ("cli" "sop/cli"))))))

