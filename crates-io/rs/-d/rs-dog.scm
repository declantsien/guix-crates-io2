(define-module (crates-io rs -d rs-dog) #:use-module (crates-io))

(define-public crate-rs-dog-0.1.0 (c (n "rs-dog") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0adfv52zqrgh5934rcpl5jgfivpr41a3lzvfp8jnnfwhpbays6yv") (y #t)))

(define-public crate-rs-dog-0.1.1 (c (n "rs-dog") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "18x6gpwa64a8vqa03b0nw2cq5mzzql814ym04i8x4s8aqdxfwp60") (y #t)))

