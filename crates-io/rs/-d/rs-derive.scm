(define-module (crates-io rs -d rs-derive) #:use-module (crates-io))

(define-public crate-rs-derive-0.0.1 (c (n "rs-derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.45") (d #t) (k 0)))) (h "00w4fvfg6v022g6ngdy4d8jap78h9ir34kyja7d7b7xswv5ibvyp")))

(define-public crate-rs-derive-0.1.0 (c (n "rs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.45") (d #t) (k 0)))) (h "0gdlkjj19b86s9h8q15cmz8xrg6jafn10l5fz9d3wmp88d2256yq")))

