(define-module (crates-io rs -d rs-docker-api) #:use-module (crates-io))

(define-public crate-rs-docker-api-0.0.61 (c (n "rs-docker-api") (v "0.0.61") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "hyper") (r "^0.14.9") (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.7.0") (d #t) (k 0)))) (h "09phfr96n8q9ajhr2j39rx6s5fy37nh7vdc9w93yrh5bz8rybg6f") (y #t)))

(define-public crate-rs-docker-api-0.0.62 (c (n "rs-docker-api") (v "0.0.62") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "hyper") (r "^0.14.9") (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.7.0") (d #t) (k 0)))) (h "0kdh6rqnhd8yb7qpll00451z6mi0gxpy8i6jdpmrxgrqyzkn9n6x") (y #t)))

(define-public crate-rs-docker-api-0.0.63 (c (n "rs-docker-api") (v "0.0.63") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "hyper") (r "^0.14.9") (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.7.0") (d #t) (k 0)))) (h "0k5gaazjx8a9wj01azhbgr4g8ajdqhn1vm9krk6hkzvjfxfznknj") (y #t)))

