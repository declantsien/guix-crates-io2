(define-module (crates-io rs -d rs-data-formats_derive) #:use-module (crates-io))

(define-public crate-rs-data-formats_derive-0.1.0 (c (n "rs-data-formats_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "1y98hvli8mbbzla42kyy2wv743d6a2y94b00nyc7qka4vp9smm8m")))

