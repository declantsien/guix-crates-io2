(define-module (crates-io rs cn rscni) #:use-module (crates-io))

(define-public crate-rscni-0.0.1 (c (n "rscni") (v "0.0.1") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "124gpwwwa5f8bqxrnzcbj568f9rk286p6g3zxpsh197c9srrk5bq")))

(define-public crate-rscni-0.0.2 (c (n "rscni") (v "0.0.2") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1016jhyra7xgnzlr1m933211zgwbzzwiqaw3gi7j9v5m23dv66yc")))

(define-public crate-rscni-0.0.3 (c (n "rscni") (v "0.0.3") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "06gzg44ry9y1zqf7nsfbf7b9gr8rzk6fw7i0a1adzabqpnmdda64") (f (quote (("std") ("default" "std") ("async"))))))

(define-public crate-rscni-0.0.4 (c (n "rscni") (v "0.0.4") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1mfpf85yz7w861c5cd5xia941qiq38lijkq8dl7mgdqxmicb9cc7") (f (quote (("std") ("default" "std") ("async"))))))

