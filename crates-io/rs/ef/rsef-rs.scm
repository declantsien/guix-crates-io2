(define-module (crates-io rs ef rsef-rs) #:use-module (crates-io))

(define-public crate-rsef-rs-0.1.0 (c (n "rsef-rs") (v "0.1.0") (d (list (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "15gdckp3pqkh7nfryv5fm49ws8ws026cszhb6jkafbajx16r87yd")))

(define-public crate-rsef-rs-0.1.1 (c (n "rsef-rs") (v "0.1.1") (d (list (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "0b7a13b0v1w7nrcaryfpmg4gi4jy0k1rfqja820i2k8ascn2my8z")))

(define-public crate-rsef-rs-0.1.2 (c (n "rsef-rs") (v "0.1.2") (d (list (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "0clv5n2jkwrbx0a541c66bcyv35br0wnv5n2pnw2pn960vw623rg")))

(define-public crate-rsef-rs-0.1.3 (c (n "rsef-rs") (v "0.1.3") (d (list (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "0jmazcwjb9sp8mfn7aqbwhn2v5ca57jbfjz4nxyx5jjz21gdnvbl")))

(define-public crate-rsef-rs-0.2.0 (c (n "rsef-rs") (v "0.2.0") (d (list (d (n "bzip2") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "libflate") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "11lmr5qnn2896y2amsgxbg49bpffxxqnjndns6fxj1d2g6h2iw8b") (f (quote (("download" "reqwest" "bzip2" "libflate" "chrono") ("default"))))))

