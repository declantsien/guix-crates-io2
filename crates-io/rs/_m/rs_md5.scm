(define-module (crates-io rs _m rs_md5) #:use-module (crates-io))

(define-public crate-rs_md5-0.1.0 (c (n "rs_md5") (v "0.1.0") (h "0s2dllv5z970g5wwhvknpd7kn0vm4vnarv8phhl59dmywciar8g4")))

(define-public crate-rs_md5-0.1.1 (c (n "rs_md5") (v "0.1.1") (h "1mnwi8isl0m14771p2r9yiriadp3jsw87rn53sx3dnsmb1fbmwq9")))

(define-public crate-rs_md5-0.1.2 (c (n "rs_md5") (v "0.1.2") (h "0k4f1xmjb9rzjwzdz0y12nd0kk6jpdv57mgci883jpz6bpf0hira")))

