(define-module (crates-io rs _m rs_mdc_2) #:use-module (crates-io))

(define-public crate-rs_mdc_2-0.1.0 (c (n "rs_mdc_2") (v "0.1.0") (h "02h5v86ii3paddnhsf7s9q6wmsw3l3vkki0l29axi68cmj430mjc")))

(define-public crate-rs_mdc_2-0.1.1 (c (n "rs_mdc_2") (v "0.1.1") (h "1rsy2g4rcmg1vlgkg9c6hv09k4w0lbqqsmj9x6hxa92i7bgjh6ml")))

(define-public crate-rs_mdc_2-0.1.2 (c (n "rs_mdc_2") (v "0.1.2") (h "0vlnwg62brrjhimkbdmdm3lp3ldlxh2wa3pjypybk4a2yawsxz41")))

