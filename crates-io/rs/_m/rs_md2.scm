(define-module (crates-io rs _m rs_md2) #:use-module (crates-io))

(define-public crate-rs_md2-0.1.0 (c (n "rs_md2") (v "0.1.0") (h "1w9509sig93xi30x1zdi91nd4nnwdi2h1ii91sxv5avhmab8y4rg")))

(define-public crate-rs_md2-0.1.1 (c (n "rs_md2") (v "0.1.1") (h "05kix4sixld2s79xsa161bapx14ibixjx4c8mj36crq1bbc0cjaj")))

(define-public crate-rs_md2-0.1.2 (c (n "rs_md2") (v "0.1.2") (h "077jqj0765gnwds648mn1nzn1b1mb3dpadpibh274zc7j4bjpbba")))

