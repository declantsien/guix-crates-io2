(define-module (crates-io rs _m rs_md4) #:use-module (crates-io))

(define-public crate-rs_md4-0.1.0 (c (n "rs_md4") (v "0.1.0") (h "07sdwk843sq7w6fk26bp5mbmsa60r8f3nvb09jm00y8r8hhk2wkp")))

(define-public crate-rs_md4-0.1.1 (c (n "rs_md4") (v "0.1.1") (h "1xsh48zx9nrl8m2cjwlxlphs0b7zkdxkwipz6v4wmm2sdxw20v1q")))

(define-public crate-rs_md4-0.1.2 (c (n "rs_md4") (v "0.1.2") (h "0kh4msxq94z381j9dm8198a4ngv35a0liz40i4xjmwalp40razrb")))

