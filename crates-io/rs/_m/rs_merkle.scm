(define-module (crates-io rs _m rs_merkle) #:use-module (crates-io))

(define-public crate-rs_merkle-0.1.0 (c (n "rs_merkle") (v "0.1.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1mvbiqvb1frdrs4cjv21m093dnq3lk7syg23figirplqb1c5dcxv")))

(define-public crate-rs_merkle-0.2.0 (c (n "rs_merkle") (v "0.2.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "0qp9hbkjhls2wbvz0gl5yh5kinijxcgqd8fw3215pkj0z8pga11a")))

(define-public crate-rs_merkle-0.2.1 (c (n "rs_merkle") (v "0.2.1") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "0g1j9m034bv0hsfgan3bi18ik92p5yrxacm2lw8246xjpixfw4k6")))

(define-public crate-rs_merkle-0.2.2 (c (n "rs_merkle") (v "0.2.2") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "10irszcydcznp7g05wrrpzl4ydr6p0lfyqr29brzgc1rlbcky0cl")))

(define-public crate-rs_merkle-1.0.0 (c (n "rs_merkle") (v "1.0.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "07lbws16wl9mzinll8c6dhb8dp2g9jf0h7dwdgvplc0mv0r7h0zd")))

(define-public crate-rs_merkle-1.1.0 (c (n "rs_merkle") (v "1.1.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "06www551q1pznqzxgh72pjdnvwxq5awf4h08mps8s8binxrdnf6a")))

(define-public crate-rs_merkle-1.2.0 (c (n "rs_merkle") (v "1.2.0") (d (list (d (n "micromath") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "0wkkhcvdbb0ya94xn12fnbx20s4vz1wq0n43w7l2ncy1hwsa8cm6") (f (quote (("std" "sha2/std") ("default" "std"))))))

(define-public crate-rs_merkle-1.3.0 (c (n "rs_merkle") (v "1.3.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (k 0)))) (h "0g3js9w69ia6723rkc0mnb3c847gqw8brbpn9w9xmwqsdbxipwy6") (f (quote (("std" "sha2/std") ("default" "std"))))))

(define-public crate-rs_merkle-1.4.0 (c (n "rs_merkle") (v "1.4.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (k 0)))) (h "19l91vfyyjwf8g63v74k4jb1w9sspyqi2g9il4cmczgc08c1l98y") (f (quote (("std" "sha2/std") ("default" "std"))))))

(define-public crate-rs_merkle-1.4.1 (c (n "rs_merkle") (v "1.4.1") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (k 0)))) (h "04588zyynf7larpj8a4gx51d1zsaj3ipr8rsnyqlrpkfr995f8h5") (f (quote (("std" "sha2/std") ("default" "std"))))))

(define-public crate-rs_merkle-1.4.2 (c (n "rs_merkle") (v "1.4.2") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (k 0)))) (h "13qj85ha2wnkvwa21y5s58wwh11xc93qriwliplzjkmpb4p1s91v") (f (quote (("std" "sha2/std") ("default" "std"))))))

