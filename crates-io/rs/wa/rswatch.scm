(define-module (crates-io rs wa rswatch) #:use-module (crates-io))

(define-public crate-rswatch-0.1.0 (c (n "rswatch") (v "0.1.0") (d (list (d (n "libcli") (r "^0.3.4") (d #t) (k 0)))) (h "09rb1zfs0v3iscvd150i49bcnkxbwmnfy98ayzh3qf0hy1yhqscx")))

(define-public crate-rswatch-0.1.1 (c (n "rswatch") (v "0.1.1") (d (list (d (n "libcli") (r "^0.3.4") (d #t) (k 0)))) (h "13rbd1wpsq37gwwc0j1jwk49xd5m6dxwyjldvfn0fgky1n2977cr")))

(define-public crate-rswatch-0.1.2 (c (n "rswatch") (v "0.1.2") (d (list (d (n "libcli") (r "^0.3.7") (d #t) (k 0)))) (h "02jzn7f4df65wlb10gqmcvxxb3f6vw88n3d87vgfnc9ijhsiqbp2")))

(define-public crate-rswatch-0.1.3 (c (n "rswatch") (v "0.1.3") (d (list (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "libcli") (r "^0.3.7") (d #t) (k 0)))) (h "0nbl59j18grydlxcdj6m77f8xc40q4zfczya9mp85jhj041lm59h")))

(define-public crate-rswatch-0.1.4 (c (n "rswatch") (v "0.1.4") (d (list (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "libcli") (r "^0.3.7") (d #t) (k 0)))) (h "00nj1kfra4iyhvw9l0lbw1pzrm58ffpfb0b8fm69d2sg429jhav7")))

(define-public crate-rswatch-0.1.5 (c (n "rswatch") (v "0.1.5") (d (list (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "libcli") (r "^0.3.7") (d #t) (k 0)))) (h "1zz7zgln3b76jz57iy3i48zl2ljzpvjk4ygk32zbfyb93j7g7kga")))

