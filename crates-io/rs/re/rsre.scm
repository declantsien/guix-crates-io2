(define-module (crates-io rs re rsre) #:use-module (crates-io))

(define-public crate-rsre-0.1.0 (c (n "rsre") (v "0.1.0") (h "1df0kw0inym5h6h99vp892mjw938pwqs6dl9y6kn8b9mr10xkr0g")))

(define-public crate-rsre-0.1.1 (c (n "rsre") (v "0.1.1") (h "13ayxr1cb2n0dn5xcd33s8jb9lnj32qjj825vzd66lxp7hcklr8n")))

(define-public crate-rsre-0.2.0 (c (n "rsre") (v "0.2.0") (h "05r8grkbi3wylwcy1bikv4afjr2y0ll0figz1s0i33v50a5qq45q") (f (quote (("debug"))))))

(define-public crate-rsre-0.2.1 (c (n "rsre") (v "0.2.1") (h "1n6mc5bippld7wzmnpp00wqhjs62ycr6xvf7g8x2crprzwhq6dxp") (f (quote (("debug"))))))

(define-public crate-rsre-0.2.2 (c (n "rsre") (v "0.2.2") (h "0rslmlyhxwkchb7xgqa0kdd2rfwn7a2ba8mv916i3qikkd6c9imp") (f (quote (("debug"))))))

(define-public crate-rsre-0.2.3 (c (n "rsre") (v "0.2.3") (h "03krzffj6db9wcm751xzkaryixpygs7xv36q0ivnykp3jzfazpja") (f (quote (("debug"))))))

(define-public crate-rsre-0.2.4 (c (n "rsre") (v "0.2.4") (h "0zwmdfxz8ml30wk9x3fql4dqw3400r7iggkyx7af1mr1y4npjl1y") (f (quote (("debug"))))))

