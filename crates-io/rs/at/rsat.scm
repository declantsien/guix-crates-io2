(define-module (crates-io rs at rsat) #:use-module (crates-io))

(define-public crate-rsat-0.1.0 (c (n "rsat") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "084k93yzdi030qrkafz3pzm8lm1hs21a7qxmqiljbqcqrbdmkna1")))

(define-public crate-rsat-0.1.1 (c (n "rsat") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zwkrdxjklzs8c13g97bv278yh8yg97zms7gzpxvjcpgvig88bm2")))

(define-public crate-rsat-0.1.2 (c (n "rsat") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "061a5fvh6y79lkdxrz8dvympcjbfljmq0dvjyy43dh5jq1ci9gzz")))

(define-public crate-rsat-0.1.3 (c (n "rsat") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "04ymbbjimdvw2csi6w0nqd2b4dznzasilxa88lp0gfv0kv9qbsiw")))

(define-public crate-rsat-0.1.4 (c (n "rsat") (v "0.1.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "04ic8wxrp032m5kajnhni2ib829kbbhsv6bpfyr0bwfs6v2yjm0x")))

(define-public crate-rsat-0.1.5 (c (n "rsat") (v "0.1.5") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1n70wn7p5a95wk1lxqy5kxfqga8jwlzpl7wv3kbdz5y3qs94wwwy")))

(define-public crate-rsat-0.1.6 (c (n "rsat") (v "0.1.6") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0xbll2hikqw10gwfjd5jd6xs0rdhdd08xs0h5s6498jcrm5irazj")))

(define-public crate-rsat-0.1.7 (c (n "rsat") (v "0.1.7") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "154wrsf2nca1jhmrm8kpcyj6psb5kvq3hn0zipk7ygpqizh4qs8y")))

(define-public crate-rsat-0.1.8 (c (n "rsat") (v "0.1.8") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zdf8s7nd1pr962clywikl93xda2n2jxh29ifrygca2v231b49zl")))

(define-public crate-rsat-0.1.9 (c (n "rsat") (v "0.1.9") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "15cm0q47yiy7lq0xka8krxwka9sry1wjv8v5x4jyj139vcp7m99p")))

(define-public crate-rsat-0.1.10 (c (n "rsat") (v "0.1.10") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0j5k8yikh6vwd1pghcg8ck07sqmm0b1pl1a7320wb240fzvffhq4")))

(define-public crate-rsat-0.1.11 (c (n "rsat") (v "0.1.11") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1a65w3y1bmj1xbs5jz99gic6gwpg84488hs37w3vgx8aq08l8nmq")))

(define-public crate-rsat-0.1.12 (c (n "rsat") (v "0.1.12") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.4") (d #t) (k 0)) (d (n "solhop-types") (r "=0.1.0") (d #t) (k 0)))) (h "0f33k2xi5yix9m2niwbar125xkjr1281q94gl5hvw1pxd225dcfp")))

