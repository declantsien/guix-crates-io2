(define-module (crates-io rs _o rs_observable) #:use-module (crates-io))

(define-public crate-rs_observable-0.1.3 (c (n "rs_observable") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1aankks3123c1m6w3c8jkcrjwnb966likkkvikv1vm5ab4z41hgg") (f (quote (("tokio") ("single") ("default" "tokio") ("all" "single" "tokio"))))))

(define-public crate-rs_observable-0.1.4 (c (n "rs_observable") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.11.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0acjaw0zb730k4ass5qggsfjqy3rhl1l896bb5bm2zfp1jqza8k7") (f (quote (("tokio") ("single") ("default" "tokio") ("all" "single" "tokio"))))))

