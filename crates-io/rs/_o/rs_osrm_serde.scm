(define-module (crates-io rs _o rs_osrm_serde) #:use-module (crates-io))

(define-public crate-rs_osrm_serde-0.1.0 (c (n "rs_osrm_serde") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qnrjp01b2f3qh372z7gl8b8lrgk5mbq2nlfmlqzq93pw3d8jkcr")))

