(define-module (crates-io rs _o rs_osrm) #:use-module (crates-io))

(define-public crate-rs_osrm-0.1.0 (c (n "rs_osrm") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1azh0ai9nl6xnvjrkkvw383ck98yh8x92nx861qqmq9bb21sy7xm")))

(define-public crate-rs_osrm-0.1.1 (c (n "rs_osrm") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1cbcf14ym4mdd9cqxn0x0n6cg4jnq8kj4jin66cd42xsh6pmg1n4")))

(define-public crate-rs_osrm-0.1.2 (c (n "rs_osrm") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1z8h7xsf8s2wnwm4rj2ji4py4zglyvbdjjdmrhdy1vni1ddpfr2p")))

(define-public crate-rs_osrm-0.1.4 (c (n "rs_osrm") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1yjvfiazgm7p1ck7iy46kw5m9pqa5y6rlshv774ds8ga7k73wzyd")))

(define-public crate-rs_osrm-0.2.0 (c (n "rs_osrm") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0h7kari84228pdyz0j24adh68pnqjslxsmii5y413y0m56n894y8")))

(define-public crate-rs_osrm-0.2.1 (c (n "rs_osrm") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1sip18z31cp81p6xp5d5rh8rapnp8vf7apvzv1a2fmdkvq113fh5")))

(define-public crate-rs_osrm-0.3.0 (c (n "rs_osrm") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0h49zkjp1qlvlld18djjp5qkg1vb7qg22z1vai4qmi6g7qvjj67h")))

(define-public crate-rs_osrm-0.3.1 (c (n "rs_osrm") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "05wcva2cywv7mw0vll27vwi2ni6idzd09fasl3jh16ayk49pagwq")))

(define-public crate-rs_osrm-0.3.2 (c (n "rs_osrm") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1pjxd731gr5x55f2hgzi6sckwq4z4hyxh12j7nm7ikgz8blap1hg")))

(define-public crate-rs_osrm-0.4.0 (c (n "rs_osrm") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0zp7vv737hibykn23lr58xjlpvjd52ihscllipb0pp7npgjm24w7")))

(define-public crate-rs_osrm-0.5.0 (c (n "rs_osrm") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0gj76qwaik20kx1a3fmv0ppgngjz7ij34p0r2cvqi89hag84fkk9")))

(define-public crate-rs_osrm-0.6.0 (c (n "rs_osrm") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1l0ifnrjc2bsvj4mnkgkm5z4w0sshaspgqxri0yg19kvi8b0k7fr")))

(define-public crate-rs_osrm-1.0.0 (c (n "rs_osrm") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0l1lzhayj22km0ahnjzsfzpv7hy4j4lvd448gqaxf4ydf1icz202")))

(define-public crate-rs_osrm-1.0.1 (c (n "rs_osrm") (v "1.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1h3lw9lnagxzgn5bc5p3b7ba4jxjp3ibxlqppsjzj84jrh85bapm")))

(define-public crate-rs_osrm-1.0.2 (c (n "rs_osrm") (v "1.0.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0qzyqc5xbrw1lj0ibrrbd0izjrp5ycgp2yy09bvqhlhaxfrdr2h5")))

(define-public crate-rs_osrm-1.0.3 (c (n "rs_osrm") (v "1.0.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "167lqzflcy4qbsz2a3w3r6ylgnxp8c2a5l3fv043xks0x7yvf53b")))

(define-public crate-rs_osrm-1.0.4 (c (n "rs_osrm") (v "1.0.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1vp34kz3m2df46q1dhgfcix02vjxrj7jzwpyxmgyhc5gqsngr9fb")))

(define-public crate-rs_osrm-1.0.5 (c (n "rs_osrm") (v "1.0.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "15fb884jfcdk1n69hrbgklrspgxjadrdl82bh1mdf671f0jr0l6w")))

(define-public crate-rs_osrm-1.0.6 (c (n "rs_osrm") (v "1.0.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1gl03pshr9jgyl3nj2qcsa262xkqp2jsp698b0xlig6gl1rwah27")))

(define-public crate-rs_osrm-1.0.7 (c (n "rs_osrm") (v "1.0.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1k0rpjfr1mhmxbnbyrb1b9xkwlz9dhj5s80a24ql4d1mragdicsg")))

(define-public crate-rs_osrm-1.0.8 (c (n "rs_osrm") (v "1.0.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1b3z6kki4dq4s8qq69d6y9n0cpmskr52qc9yi0ccdxq77zz6ap5m")))

(define-public crate-rs_osrm-1.0.9 (c (n "rs_osrm") (v "1.0.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0v4rgwc1nl4qvsy5x4j3jky4g8k0694zi8nmniycjyyrrl92dlgd")))

(define-public crate-rs_osrm-1.0.10 (c (n "rs_osrm") (v "1.0.10") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0x8kxjpiyqk1lia8cf14kwwsymvwwn2ny0v2s6imsyzkm70nivq4")))

(define-public crate-rs_osrm-1.0.11 (c (n "rs_osrm") (v "1.0.11") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0hkj760bpfk9z34l9dax4bsxqgpqj4jf96fxsv2qaaq77shrp1gl")))

(define-public crate-rs_osrm-2.0.0 (c (n "rs_osrm") (v "2.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ynp3qbwp3gani1p66n29pv6wv68gv3a87q9bdg9svhlg8yfn5p6")))

(define-public crate-rs_osrm-2.0.1 (c (n "rs_osrm") (v "2.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1h2srgn4v9jasad2g6x1n8bkcpzgja11y0kfbzhlw0kg64kxfb7w")))

