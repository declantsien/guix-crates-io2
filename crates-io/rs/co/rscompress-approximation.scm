(define-module (crates-io rs co rscompress-approximation) #:use-module (crates-io))

(define-public crate-rscompress-approximation-0.1.0 (c (n "rscompress-approximation") (v "0.1.0") (h "1sjk1wqca80ljdrayjlm0q3d8nkszry6k7incr35gz34xprm98nl")))

(define-public crate-rscompress-approximation-0.1.1 (c (n "rscompress-approximation") (v "0.1.1") (h "0dakikgapsqsiwfrbv8nc8z6hy24nxji20dyw24z1kdpwh287k88")))

