(define-module (crates-io rs co rsconnect_macros) #:use-module (crates-io))

(define-public crate-rsconnect_macros-0.2.0 (c (n "rsconnect_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0pd1wa3j4rvway4awi17l2d5gkxh8s3d3khv5q1js6whsjvb4881")))

(define-public crate-rsconnect_macros-0.2.2 (c (n "rsconnect_macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0rq382h51450pcwgmd6g9yn3wd3k3pb80b2kyf316r07zrfzm6q4")))

