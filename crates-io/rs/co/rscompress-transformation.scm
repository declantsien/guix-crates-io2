(define-module (crates-io rs co rscompress-transformation) #:use-module (crates-io))

(define-public crate-rscompress-transformation-0.1.0 (c (n "rscompress-transformation") (v "0.1.0") (h "0qfi97c35sqcdb57zdkjxx1bjs42qidw8rk3gfla7scx00lxrw8q")))

(define-public crate-rscompress-transformation-0.1.1 (c (n "rscompress-transformation") (v "0.1.1") (d (list (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "0gyyh71n6kdwbj4pjvknhd6b0rfca9cj1k8qb006rxxirlagsb2b")))

(define-public crate-rscompress-transformation-0.2.0 (c (n "rscompress-transformation") (v "0.2.0") (d (list (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "13a2f8slg63s66nqipd7bp837bzd3cqg9nrkb0994vk0ml49rmrf")))

(define-public crate-rscompress-transformation-0.2.1 (c (n "rscompress-transformation") (v "0.2.1") (d (list (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "0wczj87m0jdsh0m9bc2fqng916qbqsh5wz3d8678nkzrhnb90pyh")))

(define-public crate-rscompress-transformation-0.2.2 (c (n "rscompress-transformation") (v "0.2.2") (d (list (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)) (d (n "suffix_array") (r "0.5.*") (d #t) (k 0)))) (h "1n9jc83inwn4ss2rdf09wksx9rymv1ikybsk5h1b2ws4123zv9g7")))

(define-public crate-rscompress-transformation-0.2.3 (c (n "rscompress-transformation") (v "0.2.3") (d (list (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)) (d (n "suffix_array") (r "0.5.*") (d #t) (k 0)))) (h "0mghj78q0j36m508gp9mlghp3wgzg36r77vwjzw4bvvj13qlbprh")))

