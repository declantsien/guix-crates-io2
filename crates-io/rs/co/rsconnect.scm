(define-module (crates-io rs co rsconnect) #:use-module (crates-io))

(define-public crate-rsconnect-0.1.0 (c (n "rsconnect") (v "0.1.0") (h "0m224c20zk9vdrdllagzlcc4nr4hb9wkhv89kfxga6m30wjzarhr")))

(define-public crate-rsconnect-0.2.0 (c (n "rsconnect") (v "0.2.0") (d (list (d (n "rsconnect_macros") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "10kc5gn7yc0v4nsxb68d4h07pgy6v8fjkmay4a66104g2wjzg2qg") (s 2) (e (quote (("macros" "dep:rsconnect_macros"))))))

(define-public crate-rsconnect-0.2.2 (c (n "rsconnect") (v "0.2.2") (d (list (d (n "rsconnect_macros") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1z24xvkxszfz4ndzgnlz6zgnd7ib5w7p1hkv2l9vs4bhkclh4xv9") (s 2) (e (quote (("macros" "dep:rsconnect_macros"))))))

