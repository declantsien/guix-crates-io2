(define-module (crates-io rs co rscon) #:use-module (crates-io))

(define-public crate-rscon-0.1.0 (c (n "rscon") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive" "cargo" "string"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "figment") (r "^0.10") (f (quote ("toml"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xdg") (r "^2.5") (d #t) (k 0)))) (h "0sxrpmj8lsbvbc7300k2y0lvj6pp6q3akydxwg6p7z5gm7kdr9w9")))

