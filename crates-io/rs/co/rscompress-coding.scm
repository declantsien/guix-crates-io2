(define-module (crates-io rs co rscompress-coding) #:use-module (crates-io))

(define-public crate-rscompress-coding-0.1.0 (c (n "rscompress-coding") (v "0.1.0") (h "0vkqhank5jykhdd353sqd23inc5j1yc0ym32yvhbdlncmmgvsfgh")))

(define-public crate-rscompress-coding-0.1.1 (c (n "rscompress-coding") (v "0.1.1") (h "1b53f0lar9g06lrbdamfk4ifrd9fwc23cc4mcjcwyyqzrzq8q32i")))

