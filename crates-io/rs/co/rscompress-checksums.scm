(define-module (crates-io rs co rscompress-checksums) #:use-module (crates-io))

(define-public crate-rscompress-checksums-0.1.0 (c (n "rscompress-checksums") (v "0.1.0") (h "1fwj91vwz4n14f3l73vh4bpp6bfx2gxr9sjnyiv975716nzlcfy3")))

(define-public crate-rscompress-checksums-0.1.1 (c (n "rscompress-checksums") (v "0.1.1") (d (list (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "1p8a1mgj5r2jjyc1pzbbsiyjammqm2788564nyv3ndiliw9yzxnr")))

(define-public crate-rscompress-checksums-0.1.2 (c (n "rscompress-checksums") (v "0.1.2") (d (list (d (n "crc") (r "1.8.*") (d #t) (k 0)) (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "19givg2lk2z5j7fvlrly6v34j3mxjv8l3c7m2yij51wg60c3k4sg")))

(define-public crate-rscompress-checksums-0.2.0 (c (n "rscompress-checksums") (v "0.2.0") (d (list (d (n "crc") (r "1.8.*") (d #t) (k 0)) (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "0wzkf84b224hw0nsa7bqr8g2s5bv63ag80ccgni2ksaa09id0iq7")))

(define-public crate-rscompress-checksums-0.2.1 (c (n "rscompress-checksums") (v "0.2.1") (d (list (d (n "crc") (r "1.8.*") (d #t) (k 0)) (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "14q5iq0szb9hq8lvadd3lh02hj639fq8m6n7j17gsnxmllxaw1xj")))

(define-public crate-rscompress-checksums-0.2.2 (c (n "rscompress-checksums") (v "0.2.2") (d (list (d (n "crc") (r "1.8.*") (d #t) (k 0)) (d (n "criterion") (r "0.3.*") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "0gfk5fk7z6m51kbc0vfbnxrnlqbpi3a29hd7bnsxnym788r7j4d9")))

