(define-module (crates-io rs co rsconfig) #:use-module (crates-io))

(define-public crate-rsconfig-0.1.0 (c (n "rsconfig") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1sphvc55p0h7ijvapfrn9cxalhxnwjgn7r5xndzlsdsmf1l4gnrd")))

(define-public crate-rsconfig-0.1.1 (c (n "rsconfig") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1szir3qxnd1xcaggq17iifsm4rcmh4ipx65kxklkniwppcxyqymj")))

(define-public crate-rsconfig-0.1.2 (c (n "rsconfig") (v "0.1.2") (d (list (d (n "rsconfig-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1qffhcawrn3141741frws6ar2nyfh1flsiidb58pqlyqr79ih3r6")))

(define-public crate-rsconfig-0.1.3 (c (n "rsconfig") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0msd1v4r3ip8gpcapmpgy05q3c84wyg96cnmmb8hpix1iafqp8gs")))

