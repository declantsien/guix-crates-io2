(define-module (crates-io rs co rscompress) #:use-module (crates-io))

(define-public crate-rscompress-0.1.0 (c (n "rscompress") (v "0.1.0") (d (list (d (n "rscompress-approximation") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-checksums") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-encoding") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-transformation") (r "0.1.*") (d #t) (k 0)))) (h "0manbwlp5apmxn5n1pmrk8sf97gw21610q4vpraa31192qjyarxx")))

(define-public crate-rscompress-0.1.1 (c (n "rscompress") (v "0.1.1") (d (list (d (n "rscompress-approximation") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-checksums") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-coding") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-transformation") (r "0.1.*") (d #t) (k 0)))) (h "0ma76kq8k0zqmi3j8yiiwzln26qkxhkragplam015abs9qdb8c1j")))

(define-public crate-rscompress-0.1.2 (c (n "rscompress") (v "0.1.2") (d (list (d (n "rscompress-approximation") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-checksums") (r "0.2.*") (d #t) (k 0)) (d (n "rscompress-coding") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-transformation") (r "0.2.*") (d #t) (k 0)))) (h "01kpacy2gjy0y6m62z02jgy6jghj8p623wqp2iqxp1lfljnvxd0x")))

(define-public crate-rscompress-0.1.3 (c (n "rscompress") (v "0.1.3") (d (list (d (n "rscompress-approximation") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-checksums") (r "0.2.*") (d #t) (k 0)) (d (n "rscompress-coding") (r "0.1.*") (d #t) (k 0)) (d (n "rscompress-transformation") (r "0.2.*") (d #t) (k 0)))) (h "0y41zdp9v7110sazngb790bcb5yf9bvf8wnppw0zasjhpcrdsvx4")))

