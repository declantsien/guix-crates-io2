(define-module (crates-io rs nt rsntp) #:use-module (crates-io))

(define-public crate-rsntp-0.1.0 (c (n "rsntp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "090m2xbv1z9i6b027bka4w5yd34prbb1dbiia5fcsxy06y4ayhp2")))

(define-public crate-rsntp-0.2.0 (c (n "rsntp") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("udp" "dns" "time"))) (o #t) (d #t) (k 0)))) (h "1bhdfnk1s0divhl9aysi40ch2373znczzv83j16q9fdm0ng4kwbq") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-0.3.0 (c (n "rsntp") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("udp" "dns" "time"))) (o #t) (d #t) (k 0)))) (h "1d2j4m06kf1bs7qq196hvrc483bpzfhf822amf12pvc239awn2aw") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-0.3.1 (c (n "rsntp") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("udp" "dns" "time"))) (o #t) (d #t) (k 0)))) (h "0dpif9sh5ckhaqp8r7378y67w5a57c5srrydbv4a0wkbwpby908n") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-0.3.2 (c (n "rsntp") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("udp" "dns" "time"))) (o #t) (d #t) (k 0)))) (h "16cgwz3aaj0b6ai2a5p1vy224ilsp6k7acz7qm6x9gwbxdpg5qkd") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-1.0.0 (c (n "rsntp") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("udp" "dns" "time"))) (o #t) (d #t) (k 0)))) (h "0x0r0i7cdg4i39292lma70pf5ylww7ibpzg0nryxl7a1aarsf0jw") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-1.0.1 (c (n "rsntp") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("udp" "dns" "time"))) (o #t) (d #t) (k 0)))) (h "026cfnss40wf9qbhm5w4b6qc6gjmhfn6xhdp83k4ffg1l3fwqnpy") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-1.0.2 (c (n "rsntp") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("udp" "dns" "time"))) (o #t) (d #t) (k 0)))) (h "0ka1qq872nd2qzf0azwlx754w31ql6fryjlyl6bj6c1s07x1m25r") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-1.0.3 (c (n "rsntp") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("udp" "dns" "time"))) (o #t) (d #t) (k 0)))) (h "1hy479rr4zy3r3qyxfv7gl8d3malvg671wmr458bvni9n690zxil") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-2.0.0 (c (n "rsntp") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "time"))) (o #t) (d #t) (k 0)))) (h "0bc5149pmp9yv7dpav89k4ynd7z5cjl4fgwi2x25v8vdx0vwlvzx") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-2.1.0 (c (n "rsntp") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "time"))) (o #t) (d #t) (k 0)))) (h "1crxd5bvifndyvrb0h8lgqllklbhdv6lg7mpq9bv6m253b40j0zg") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-3.0.0 (c (n "rsntp") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4.10") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "time"))) (o #t) (d #t) (k 0)))) (h "0737ixqqlfj3mxq55qm1j07sxlv6vqgcanh0lvj29bp2cggbzjx1") (f (quote (("default" "async" "chrono") ("async" "tokio"))))))

(define-public crate-rsntp-3.0.1 (c (n "rsntp") (v "3.0.1") (d (list (d (n "chrono") (r "^0.4.10") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "time"))) (o #t) (d #t) (k 0)))) (h "0w3nlgkwd8hdn3k4d1i9wkmichh39d3wf443vglfj2laqagdnc2z") (f (quote (("default" "async" "chrono") ("async" "tokio"))))))

(define-public crate-rsntp-3.0.2 (c (n "rsntp") (v "3.0.2") (d (list (d (n "chrono") (r "^0.4.10") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "time"))) (o #t) (d #t) (k 0)))) (h "127107dkz7v0542255jxq8xy69wh61h8l163lh6k113j57sqwlwm") (f (quote (("default" "async" "chrono") ("async" "tokio"))))))

(define-public crate-rsntp-4.0.0 (c (n "rsntp") (v "4.0.0") (d (list (d (n "chrono") (r "^0.4.10") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "time"))) (o #t) (d #t) (k 0)))) (h "1qgkday7add8g0ki39rb5dzi6gqjxigidp5shbdg1i2gfr2mzkjw") (f (quote (("default" "async" "chrono") ("async" "tokio"))))))

