(define-module (crates-io rs td rstdev-domain) #:use-module (crates-io))

(define-public crate-rstdev-domain-0.1.0 (c (n "rstdev-domain") (v "0.1.0") (d (list (d (n "rst-common") (r "^1.1") (f (quote ("with-errors"))) (d #t) (k 0)))) (h "1bm49i53vz2zgdfw0zyyz02zj4hw7bfp3z9far4lvnck9z62mii8") (r "1.74")))

