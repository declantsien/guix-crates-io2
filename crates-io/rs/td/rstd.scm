(define-module (crates-io rs td rstd) #:use-module (crates-io))

(define-public crate-rstd-0.1.0 (c (n "rstd") (v "0.1.0") (d (list (d (n "promptly") (r "^0.1.5") (d #t) (k 0)))) (h "086dqqgdiy4sckxyyb5jcf5dxy6828x7a5y6xqi24di1zc0rvxxi")))

(define-public crate-rstd-0.2.0 (c (n "rstd") (v "0.2.0") (d (list (d (n "promptly") (r "^0.1.5") (d #t) (k 0)))) (h "17121axpikkk6slwihbsz30ni4gsj62p4cqgizyv6277f9j6c9ma")))

(define-public crate-rstd-0.3.0 (c (n "rstd") (v "0.3.0") (d (list (d (n "promptly") (r "^0.1.5") (d #t) (k 0)))) (h "1ky9v05bw7yvs745p8fvk38ip7qwpbis6c0ak7gydgbl966i8s8w")))

