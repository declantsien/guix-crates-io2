(define-module (crates-io rs td rstdev-config) #:use-module (crates-io))

(define-public crate-rstdev-config-0.1.0 (c (n "rstdev-config") (v "0.1.0") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "rst-common") (r "^1.1") (f (quote ("with-errors"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0vpingh2b4k4dyx1xspcmslpviyhw10yk7gavdsags2wq1489sim") (r "1.74")))

(define-public crate-rstdev-config-0.1.1 (c (n "rstdev-config") (v "0.1.1") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "rst-common") (r "^1.1") (f (quote ("with-errors"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1185prdr09shk13qrvj9m25g2fzy8k3xxaahzbdjc4m0s549nbaq") (r "1.74")))

(define-public crate-rstdev-config-0.1.2 (c (n "rstdev-config") (v "0.1.2") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "rst-common") (r "^1.1") (f (quote ("with-errors"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0g06w28bwhhwplgn0b1a3vidj0z1b4qnj9q4bnhdm6y4f0gy5v0z") (r "1.74")))

(define-public crate-rstdev-config-0.1.3 (c (n "rstdev-config") (v "0.1.3") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "rst-common") (r "^1.1") (f (quote ("with-errors"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1n4jq3i33x5ydxd83qqhc48s7pr46gzn5v7krng8ya53kzqhdl0b") (r "1.74")))

