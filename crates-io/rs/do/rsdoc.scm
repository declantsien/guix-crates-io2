(define-module (crates-io rs do rsdoc) #:use-module (crates-io))

(define-public crate-rsdoc-0.1.0 (c (n "rsdoc") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1bfyh34bid9xm95sw859zv773m7324n7k45xlrc2pkcyb1qg4l3a") (f (quote (("default"))))))

(define-public crate-rsdoc-0.2.0 (c (n "rsdoc") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "12gzcsf86qln56ga0d1ygydhdm5yhp4px18x80ys9hrhf7l64227") (f (quote (("default"))))))

