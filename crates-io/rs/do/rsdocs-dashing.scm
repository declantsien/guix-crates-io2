(define-module (crates-io rs do rsdocs-dashing) #:use-module (crates-io))

(define-public crate-rsdocs-dashing-0.1.0 (c (n "rsdocs-dashing") (v "0.1.0") (d (list (d (n "copy_dir") (r "^0.1.2") (d #t) (k 0)) (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0cr4lhn9c9fqy6z4b5dicfhyk79frxr5xr6jk2my94q1dzxf36f0")))

