(define-module (crates-io rs #{64}# rs64-periph) #:use-module (crates-io))

(define-public crate-rs64-periph-0.1.0 (c (n "rs64-periph") (v "0.1.0") (d (list (d (n "volatile") (r "^0.2.3") (d #t) (k 0)))) (h "1qghw5mslqnw8hf1j23w0hi7kd9ks5x74nf57haddd56s5jyqb91")))

