(define-module (crates-io rs js rsjsonnet) #:use-module (crates-io))

(define-public crate-rsjsonnet-0.1.0 (c (n "rsjsonnet") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "rsjsonnet-front") (r "^0.1.0") (f (quote ("crossterm"))) (d #t) (k 0)) (d (n "rsjsonnet-lang") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.12") (d #t) (k 2)) (d (n "unified-diff") (r "^0.2.1") (d #t) (k 2)))) (h "0551r6faf6ghbg6ai9a8rdyi63p2v44y4x20gjbpm4igbgxw6fzq") (r "1.74")))

(define-public crate-rsjsonnet-0.1.1 (c (n "rsjsonnet") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.2") (d #t) (k 2)) (d (n "rsjsonnet-front") (r "^0.1.1") (f (quote ("crossterm"))) (d #t) (k 0)) (d (n "rsjsonnet-lang") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.12") (d #t) (k 2)) (d (n "unified-diff") (r "^0.2.1") (d #t) (k 2)))) (h "03l57qnlbm34920c4xc5rpf51fwjmc87pwjycvdsdcw5zg6vw929") (r "1.74")))

