(define-module (crates-io rs js rsjson) #:use-module (crates-io))

(define-public crate-rsjson-0.1.0 (c (n "rsjson") (v "0.1.0") (h "0wxyxy70fvbk73pv4q6rf8751i5v12s15r0ry7hwc0jhlqc8bal4")))

(define-public crate-rsjson-0.1.1 (c (n "rsjson") (v "0.1.1") (h "1szr4swjb5h40rwgxzlr6p4giaksxpirx9p5hsbbysh6v5mxqbc4")))

(define-public crate-rsjson-0.1.2 (c (n "rsjson") (v "0.1.2") (h "0b124sg4v2h493fhvc53akdds77728f5h9l8kivnms8lqv40ckil")))

(define-public crate-rsjson-0.1.3 (c (n "rsjson") (v "0.1.3") (h "1mslcmr8firnaavjbm1qvsbdyia695x65v6dsls2ak2nl9w8b7ys")))

(define-public crate-rsjson-0.1.4 (c (n "rsjson") (v "0.1.4") (h "0f641482ra0y21qv77sqag1kkj2nklz347mjnlyd5sab1kaxwzb4")))

(define-public crate-rsjson-0.1.5 (c (n "rsjson") (v "0.1.5") (h "17wzijvhr6xrkipspsbww652dq0vpz4k8gapsn76gbw42shv6j75")))

(define-public crate-rsjson-0.2.0 (c (n "rsjson") (v "0.2.0") (h "0mj4xswc0y1bbv6lg5955irj26997cqf4023p4nl6i7h5k0vs8zr")))

(define-public crate-rsjson-0.2.1 (c (n "rsjson") (v "0.2.1") (h "1phdbxrksi2ljwkb6z3fnbhal18m1di4cplh2kmvbfggxhk6d1mc")))

(define-public crate-rsjson-0.2.2 (c (n "rsjson") (v "0.2.2") (h "03h0jlv6586yhskkhlv6vq51yv37313bb7ys1f1ryp51hszvqdlv")))

(define-public crate-rsjson-0.3.0 (c (n "rsjson") (v "0.3.0") (h "0vq0f8vb1cfvyszvfh28z2k5d9x5rpljwl2wh087i9ycb9v20b1g")))

(define-public crate-rsjson-0.3.1 (c (n "rsjson") (v "0.3.1") (h "0q726pzdl3sm62hdfgimcyx0pl2f68rcmpmvn4mzd9ad2q28hq9i")))

(define-public crate-rsjson-0.3.2 (c (n "rsjson") (v "0.3.2") (h "0yyqrbsmn6byrcvjld57qjbacs1n1091ahh0m4ksfp88gl47808a")))

(define-public crate-rsjson-0.3.3 (c (n "rsjson") (v "0.3.3") (h "17n4ak6hzziw5a8mxz5n4waq8v95rgkz7gjajbb7zvslz1bfxwgi")))

(define-public crate-rsjson-0.3.4 (c (n "rsjson") (v "0.3.4") (h "1q6gi4hivjkhi8p67s8v6yanavb3fm0mld728fkib13sdvr922px")))

(define-public crate-rsjson-0.3.5 (c (n "rsjson") (v "0.3.5") (h "0kkk2k2h7gnhacf1q93lbahrl0pwm8fzwzycpy0lnxxpads5w86r")))

(define-public crate-rsjson-0.3.6 (c (n "rsjson") (v "0.3.6") (h "0sxpyq8wvj86qk3zij760wsn2lb9klcs229imfksh4dyd6f0cniw")))

(define-public crate-rsjson-0.4.0 (c (n "rsjson") (v "0.4.0") (h "1gjjdvdwy3d6dqgyzf8jj9vgjwanvr7hnfydll6zql786k6iph1c")))

