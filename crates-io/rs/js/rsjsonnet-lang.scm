(define-module (crates-io rs js rsjsonnet-lang) #:use-module (crates-io))

(define-public crate-rsjsonnet-lang-0.1.0 (c (n "rsjsonnet-lang") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "libyaml-safer") (r "^0.1.1") (d #t) (k 0)) (d (n "md-5") (r "^0.10.6") (k 0)))) (h "1wgbs1fz7rzzjyd5yi4h5znqmxdqxx1kyx2jzqrsqg1gnvph6y20") (r "1.74")))

(define-public crate-rsjsonnet-lang-0.1.1 (c (n "rsjsonnet-lang") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "libyaml-safer") (r "^0.1.1") (d #t) (k 0)) (d (n "md-5") (r "^0.10.6") (k 0)))) (h "0rnsxxnzg1454farfdnjjzmri41krix98ga9746f4gg4jh5w2xhp") (r "1.74")))

