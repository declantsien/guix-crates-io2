(define-module (crates-io rs js rsjsonnet-front) #:use-module (crates-io))

(define-public crate-rsjsonnet-front-0.1.0 (c (n "rsjsonnet-front") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (o #t) (d #t) (k 0)) (d (n "rsjsonnet-lang") (r "^0.1.0") (d #t) (k 0)) (d (n "sourceannot") (r "^0.1.1") (d #t) (k 0)))) (h "02zjgmdqf1jk64c21k9xgi94y32jhm1gxrxv27xipf8fhh8vlmzq") (f (quote (("default")))) (r "1.74")))

(define-public crate-rsjsonnet-front-0.1.1 (c (n "rsjsonnet-front") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (o #t) (d #t) (k 0)) (d (n "rsjsonnet-lang") (r "^0.1.1") (d #t) (k 0)) (d (n "sourceannot") (r "^0.2.0") (d #t) (k 0)))) (h "0p0wmpm91gw9axzwd0777826mqi0axs5azlblrciqy5n0ypshcmm") (f (quote (("default")))) (r "1.74")))

