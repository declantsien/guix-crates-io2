(define-module (crates-io rs js rsjs) #:use-module (crates-io))

(define-public crate-rsjs-0.1.0 (c (n "rsjs") (v "0.1.0") (h "0pdk8qm9rmb9nknkqky07dsk4mdbdwmzifv6wi4yjsffklrrmdzw")))

(define-public crate-rsjs-0.1.1 (c (n "rsjs") (v "0.1.1") (h "0v362c9zmk029d5n4akgq9n8g3jz5jnn6pmh8n9sn3rrik3mrx49")))

(define-public crate-rsjs-0.1.2 (c (n "rsjs") (v "0.1.2") (h "0yjlqx7ybm30k1h94swbykcgab7ykdjajri4nfbmbkmzaa7sln00")))

