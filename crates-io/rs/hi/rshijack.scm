(define-module (crates-io rs hi rshijack) #:use-module (crates-io))

(define-public crate-rshijack-0.1.0 (c (n "rshijack") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "pcap") (r "^0.7") (d #t) (k 0)) (d (n "pktparse") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.20") (d #t) (k 0)))) (h "0r4drg1mlhvxf5sgw702xngvfp58p960rbs5fb60c9fd677mxb17")))

(define-public crate-rshijack-0.2.0 (c (n "rshijack") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "pktparse") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.20") (d #t) (k 0)))) (h "19q8l9ldjrp2m474gkmqx350rnfcnzs13fnxf49qzsr0jq08qzfl")))

(define-public crate-rshijack-0.3.0 (c (n "rshijack") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "pktparse") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.21") (d #t) (k 0)))) (h "0dcz7qf7nsypn5w4gjhynjag703md53767abff287ipn3s4hdvl2")))

(define-public crate-rshijack-0.4.0 (c (n "rshijack") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pktparse") (r "^0.5") (d #t) (k 0)) (d (n "pnet") (r "^0.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)))) (h "1f3x08qqimvihfp06ail3pd2rgmr66srlxqflhwi1g6qv5awrll0")))

(define-public crate-rshijack-0.5.0 (c (n "rshijack") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pktparse") (r "^0.7") (d #t) (k 0)) (d (n "pnet") (r "^0.33") (d #t) (k 0)))) (h "0p1gmgwk3xm3cn7a8mvmrih79b3j9myfjxf7380690vsv7jxdhhd")))

(define-public crate-rshijack-0.5.1 (c (n "rshijack") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pktparse") (r "^0.7") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)))) (h "0whxc335iq74prij2pb3fc6lz0r7wbgkrsj8x4814a0xbldq9k4c")))

