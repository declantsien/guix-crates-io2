(define-module (crates-io rs ed rsed) #:use-module (crates-io))

(define-public crate-rsed-0.1.0 (c (n "rsed") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1rslcf1fcssyjc0b1gzz5yz5sf9w2952f6c3dk9j73fdqzn00vai")))

