(define-module (crates-io rs fu rsfuzzy) #:use-module (crates-io))

(define-public crate-rsfuzzy-0.1.0 (c (n "rsfuzzy") (v "0.1.0") (h "14igg0p56j084l6b96nmpnv0a1f7f2q8zr74ddfmjvzdqqhbvp2l")))

(define-public crate-rsfuzzy-0.1.1 (c (n "rsfuzzy") (v "0.1.1") (h "13hkh59p4ip7jbl6pq946nq98z0hf2ka9660qcip84prcff5d8f4")))

(define-public crate-rsfuzzy-0.1.2 (c (n "rsfuzzy") (v "0.1.2") (h "05g8misvvxkv6mny6mgxd697i5228m8f2b8sp8l68mlahcj2q233") (y #t)))

(define-public crate-rsfuzzy-0.1.3 (c (n "rsfuzzy") (v "0.1.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1fx8v4fn35ykg1nrdvq25r59qhmnv1bamydq5rlx7caclwfcwi5l")))

