(define-module (crates-io rs #{11}# rs118-chip8) #:use-module (crates-io))

(define-public crate-rs118-chip8-0.1.0 (c (n "rs118-chip8") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11") (d #t) (k 0)))) (h "00linpghxigzmjc2l2v0cblxlzy89kx423cr98ds3rrc0qpsyfjg")))

(define-public crate-rs118-chip8-0.1.1 (c (n "rs118-chip8") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11") (d #t) (k 0)))) (h "03a6ajacs5zvjivr1x2bvvdbr94w5klz3pwbkwbchjhai3b1x412")))

(define-public crate-rs118-chip8-0.2.0 (c (n "rs118-chip8") (v "0.2.0") (d (list (d (n "chip8_base") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1d83pm80rzmk796h3sfaq7jlv0wp110plgagf4c3lw5ihfa7kw2q")))

