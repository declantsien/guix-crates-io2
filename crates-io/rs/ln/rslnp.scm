(define-module (crates-io rs ln rslnp) #:use-module (crates-io))

(define-public crate-rslnp-0.1.0 (c (n "rslnp") (v "0.1.0") (d (list (d (n "token-lists") (r "^0.1.1") (f (quote ("parser"))) (d #t) (k 2)) (d (n "token-parser") (r "^0.2.0") (d #t) (k 0)))) (h "1j1b0v2hxc6y9abgwipq5r1n9sji6al7b23a08k6lrx9kv4g9mih")))

(define-public crate-rslnp-0.2.1 (c (n "rslnp") (v "0.2.1") (d (list (d (n "token-lists") (r "^0.1.1") (f (quote ("parser"))) (d #t) (k 2)) (d (n "token-parser") (r ">=0.2.0, <=0.3.0") (d #t) (k 0)) (d (n "token-parser") (r "^0.2.0") (d #t) (k 2)))) (h "195vimrvjankzk59ys1rqc41b4jr41mwdb0nilg6vf06k6cq2rsf")))

