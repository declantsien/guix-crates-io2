(define-module (crates-io rs nl rsnltk) #:use-module (crates-io))

(define-public crate-rsnltk-0.1.0 (c (n "rsnltk") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "15ccbi3vsyiz8waqmsgyq8chkrsx5yy9961hp6si90cwhps4ybf1")))

(define-public crate-rsnltk-0.1.1 (c (n "rsnltk") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0a9fk9syibahv7pk0q5c63y8089a83y69m0r1jpi4r5rw50xfw3k")))

(define-public crate-rsnltk-0.1.2 (c (n "rsnltk") (v "0.1.2") (d (list (d (n "natural") (r "^0.4.0") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "whatlang") (r "^0.12.0") (d #t) (k 0)) (d (n "word2vec") (r "^0.3.3") (d #t) (k 0)) (d (n "yn") (r "^0.1.1") (d #t) (k 0)))) (h "140nggvv86hd2x63ylja4ksrlmz3g2yrb9wpavlwch485a20sz5s")))

(define-public crate-rsnltk-0.1.3 (c (n "rsnltk") (v "0.1.3") (d (list (d (n "natural") (r "^0.4.0") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "whatlang") (r "^0.12.0") (d #t) (k 0)) (d (n "word2vec") (r "^0.3.3") (d #t) (k 0)) (d (n "yn") (r "^0.1.1") (d #t) (k 0)))) (h "1vs8abjzky29nrn9cpx5djll5760whq680wpphx5zhl29kgqss80")))

