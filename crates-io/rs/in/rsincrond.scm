(define-module (crates-io rs in rsincrond) #:use-module (crates-io))

(define-public crate-rsincrond-0.0.2 (c (n "rsincrond") (v "0.0.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "inotify") (r "^0.10.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1kl0mqx5hvd2lz1xcy59k1qnfzvvn8pbr5v1pa1lnd7zdmwcsd34") (y #t)))

