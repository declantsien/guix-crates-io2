(define-module (crates-io rs gr rsgrad) #:use-module (crates-io))

(define-public crate-rsgrad-0.1.0 (c (n "rsgrad") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1qjzj0bz8p5z0krdm469ykhf0x36sf29ng2m5ypgzq20j0lrgrhg")))

(define-public crate-rsgrad-0.1.1 (c (n "rsgrad") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1lgfz4jrrzgjh5z3i76yin6jrgli3lizl9xxghp30lz4fcpmr6yi")))

