(define-module (crates-io rs gr rsgraph) #:use-module (crates-io))

(define-public crate-rsgraph-0.1.0 (c (n "rsgraph") (v "0.1.0") (h "10l1c3pvb2vhgza37f7qb89qxc6ikl9qm9dx69r7y8shzhw4s3fb")))

(define-public crate-rsgraph-0.1.1 (c (n "rsgraph") (v "0.1.1") (h "18ai66qxkcp4kmb6p5xhrwbrxbyvd8al7bayz8spndnl3163bl5c")))

