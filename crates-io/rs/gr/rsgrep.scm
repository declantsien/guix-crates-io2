(define-module (crates-io rs gr rsgrep) #:use-module (crates-io))

(define-public crate-rsgrep-0.1.0 (c (n "rsgrep") (v "0.1.0") (h "00rxcwksr2a8gr7xr9nsp3293a7nm0inp2ds8nz7hy6mayc0rx05")))

(define-public crate-rsgrep-0.2.0 (c (n "rsgrep") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0q466m4vy017nq1892c9fb3biia1ds53wsbbkrcd4gfwhfr8bm9l")))

(define-public crate-rsgrep-0.3.0 (c (n "rsgrep") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1waqpymjm1bcdbdqwq691i74nm9y9d4r97nk1zr1zjzbyqf4l869")))

(define-public crate-rsgrep-0.4.0 (c (n "rsgrep") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0gqqgaywdhqffbrg4q2hnppbis4iqiafss2bhm1nqry9j73bxip8")))

(define-public crate-rsgrep-0.5.0 (c (n "rsgrep") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1nc8a5l11330ww1z8zdvx4bl1xa52xksvnkbnrfa219j8dbgr22s")))

