(define-module (crates-io rs tl rstler) #:use-module (crates-io))

(define-public crate-rstler-1.0.0 (c (n "rstler") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "04k7invc48shnsclgx9gla428jnfp2paa4bnx6sngn4if8d3pnmd")))

