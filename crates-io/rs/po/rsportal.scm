(define-module (crates-io rs po rsportal) #:use-module (crates-io))

(define-public crate-rsportal-0.1.0 (c (n "rsportal") (v "0.1.0") (d (list (d (n "display-interface-parallel-gpio") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "ili9341") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "pyportal") (r "^0.8.0") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.1") (f (quote ("cortex-m"))) (o #t) (d #t) (k 0)) (d (n "smart-leds") (r "~0.3") (o #t) (d #t) (k 0)) (d (n "ws2812-timer-delay") (r "~0.3") (o #t) (d #t) (k 0)))) (h "1s390mhnm5j6ci5njxbhv5vkwwyzy1a0f5k42hbld2rzdnfncv60") (f (quote (("neopixel" "ws2812-timer-delay" "smart-leds" "rtt-target") ("display" "ili9341" "display-interface-parallel-gpio") ("default" "neopixel" "display"))))))

