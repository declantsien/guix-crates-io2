(define-module (crates-io rs po rspotify-s-model) #:use-module (crates-io))

(define-public crate-rspotify-s-model-0.13.1 (c (n "rspotify-s-model") (v "0.13.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "05kljdkc9rv86dxs7b17zd5p88qg9gm9f7j8r5nlq4kk0zaic5ql")))

(define-public crate-rspotify-s-model-0.13.2 (c (n "rspotify-s-model") (v "0.13.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "15fm28h9gi6n01b1brgccb4zp0i4qam135igpvn18avrj249k74l")))

