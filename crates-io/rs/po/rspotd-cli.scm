(define-module (crates-io rs po rspotd-cli) #:use-module (crates-io))

(define-public crate-rspotd-cli-0.1.0 (c (n "rspotd-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "rspotd") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1lhkkm5jplpk93qnkzhy4gfh89g4yl6s4sf8zkj4slifmqxphcn8") (y #t)))

(define-public crate-rspotd-cli-0.2.0 (c (n "rspotd-cli") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "rspotd") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1pai86h8pqmk3wccsg2mrisk2sng69q80l2qkzqc9iwrf7z947rr") (y #t)))

(define-public crate-rspotd-cli-0.2.1 (c (n "rspotd-cli") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "rspotd") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0mrf46fspfr54xxfndngz4pkjrs4h9kpvks1jlvjhzvy62y9s0mg") (y #t)))

(define-public crate-rspotd-cli-0.3.0 (c (n "rspotd-cli") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "rspotd") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1igp5yjnkqa9kxmlmszs5z6bjgh7k6f5c9k3rvj5jcdy92vzic5q") (y #t)))

(define-public crate-rspotd-cli-0.4.0 (c (n "rspotd-cli") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "rspotd") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1icq0c7v376n0nzy5jycnxb21llzw9dkll0dz4qnizlccbfrzvg3")))

