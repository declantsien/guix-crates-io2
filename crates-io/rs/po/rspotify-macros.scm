(define-module (crates-io rs po rspotify-macros) #:use-module (crates-io))

(define-public crate-rspotify-macros-0.11.0 (c (n "rspotify-macros") (v "0.11.0") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "1isklz18ghcp4kkgljvdvs5rkwzi5c88hgi610xyj1y3k5200vl8")))

(define-public crate-rspotify-macros-0.11.1 (c (n "rspotify-macros") (v "0.11.1") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "1iby91gilb2zagkj7dbiv73hzxqnx7l8i8y9znykklmflxkp8lp5")))

(define-public crate-rspotify-macros-0.11.2 (c (n "rspotify-macros") (v "0.11.2") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "05z8716vyqfz7v73v484j2z6496kx8pyzh8xakxqj89lbq5x11qh")))

(define-public crate-rspotify-macros-0.11.3 (c (n "rspotify-macros") (v "0.11.3") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "0njvfycza37c97ds7j34vgqh4pgn6jnyrszwx8cb8wfphkwvrgcz")))

(define-public crate-rspotify-macros-0.11.4 (c (n "rspotify-macros") (v "0.11.4") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "0lbls1mn7g477c5afd1w4mhhk2xjr27fgsq0hjvkpbpakq26jw7z")))

(define-public crate-rspotify-macros-0.11.5 (c (n "rspotify-macros") (v "0.11.5") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "1afzdwg4nwk0pdmikxw4bf1936ln0pi2745sak51q11idkh6nwa7")))

(define-public crate-rspotify-macros-0.11.6 (c (n "rspotify-macros") (v "0.11.6") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "0iqc7b1srhn9hanjh6s6qsmzhrvyn7gi803zjcsm5isiqwgm51hc")))

(define-public crate-rspotify-macros-0.11.7 (c (n "rspotify-macros") (v "0.11.7") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "00zjxnbhljak54bf5q1h09487bh7sza3mqsxk21x43q7pw5jlrkg")))

(define-public crate-rspotify-macros-0.12.0 (c (n "rspotify-macros") (v "0.12.0") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "12k3alwfk3r0gvrrlr3w2lwq4vhirqjkfhhf47kpxaai52494j6c")))

(define-public crate-rspotify-macros-0.13.0 (c (n "rspotify-macros") (v "0.13.0") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "0drks92drmi8q0qak5jn5p2795bdcahydrvwn4l0whh71w7755g3")))

(define-public crate-rspotify-macros-0.13.1 (c (n "rspotify-macros") (v "0.13.1") (d (list (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "14wsmn3f1wb5msrjz1j44ph7paygvgql8zkajr8mcqg8d18975k3")))

