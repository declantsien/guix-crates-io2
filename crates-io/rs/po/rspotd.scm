(define-module (crates-io rs po rspotd) #:use-module (crates-io))

(define-public crate-rspotd-0.1.0 (c (n "rspotd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mi9fqf69sddm7pacnw4fwc791qz88jg3rmbdhxmyggdp9gnpv6v") (y #t)))

(define-public crate-rspotd-0.1.1 (c (n "rspotd") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02pz2cs2280g7slgp776q5wnq44b875b52m2ihidlmkwgrm1h57z") (y #t)))

(define-public crate-rspotd-0.1.2 (c (n "rspotd") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18x0x8kg2df5xs5hw05lnnpb9rv9pfzxjw99mn73k6phc4gb90pm") (y #t)))

(define-public crate-rspotd-0.1.3 (c (n "rspotd") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (o #t) (d #t) (k 0)))) (h "0ghphwm2h1s7d9dr4f2fn92ggj4n5cy4pvs4myy6qydw9azahnsb") (f (quote (("wasm" "wasm-bindgen")))) (y #t)))

(define-public crate-rspotd-0.1.4 (c (n "rspotd") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17nz6d5lbqk9fmi3b2qpdaxpbwr89arkcdkdxw093kg2q7a9lb16") (y #t)))

(define-public crate-rspotd-0.1.5 (c (n "rspotd") (v "0.1.5") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04p8jc6pqdz2d24503xjqbzih7g8fd0bx7d70kg35ijrpxadl3z7") (y #t)))

(define-public crate-rspotd-0.1.6 (c (n "rspotd") (v "0.1.6") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17cy5bdjbdxvwml42gl3wqdr9fxrmv4xmwrf3fin9069hgmxib1c") (y #t)))

(define-public crate-rspotd-0.1.7 (c (n "rspotd") (v "0.1.7") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04s1mzgwakrzx26fn77z21kwa8m3rhfzdlg8amcghj92dl9lly1j") (y #t)))

(define-public crate-rspotd-0.1.8 (c (n "rspotd") (v "0.1.8") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19y9k0rlw0qfk2q6j01wj5ssbf3g89ymqqf8h746zq5spp3rnrlp") (y #t)))

(define-public crate-rspotd-0.1.9 (c (n "rspotd") (v "0.1.9") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01qw6yfc6lwv98p9gwsj31a4v4mn8wh5rzl6ixnjk3l79cb0r62w") (y #t)))

(define-public crate-rspotd-0.2.0 (c (n "rspotd") (v "0.2.0") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14zn0rphjsmsr40fd4q48i1i0q17s2cskb3qg3cmc2rhlin3gdgx") (y #t)))

(define-public crate-rspotd-0.2.1 (c (n "rspotd") (v "0.2.1") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sqm8fm3ckkr41bx2g8vpd29qz45c9ycbjdg8rqwzmr5bkylx593") (y #t)))

(define-public crate-rspotd-0.2.2 (c (n "rspotd") (v "0.2.2") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pz97kyzsb59wwbd4i1ys62chbfgq6bzhwxyfxbjcp6v5wx1k8fz") (y #t)))

(define-public crate-rspotd-0.2.3 (c (n "rspotd") (v "0.2.3") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04606yfl6hrk55zhhk98idkkzkjmmyza01vynz4mzcdspan7cf2r") (y #t)))

(define-public crate-rspotd-0.3.0 (c (n "rspotd") (v "0.3.0") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b9syi6m7861arxwv599k87zhsp5lh4rkhmrxh94xal3ydwy5pcs") (y #t)))

(define-public crate-rspotd-0.4.0 (c (n "rspotd") (v "0.4.0") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1q13cy6fsq6w1jkwz7w859d90970cp0iksl5gfiw2jl62dhv9nq6")))

(define-public crate-rspotd-0.5.0 (c (n "rspotd") (v "0.5.0") (d (list (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3.36") (f (quote ("macros" "formatting" "parsing"))) (d #t) (k 0)))) (h "1ihsn8893438znysd1p5svy3j67bp2clg6rgva22sqlaff1162c5")))

