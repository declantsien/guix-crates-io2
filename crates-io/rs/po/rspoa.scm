(define-module (crates-io rs po rspoa) #:use-module (crates-io))

(define-public crate-rspoa-0.1.0 (c (n "rspoa") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "gfa") (r "^0.8.0") (d #t) (k 0)) (d (n "handlegraph") (r "^0.5.0") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)))) (h "0vmii9p45f54878v8nqk33fd5di0l1jfizqm22h5n7qx3fn6p3zd")))

