(define-module (crates-io rs ho rshotkey) #:use-module (crates-io))

(define-public crate-rshotkey-0.1.0 (c (n "rshotkey") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rdev") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vafisf1q7vdrjnv4ai6zb77i71kggfl60aha58iylhs1d6jihl4")))

(define-public crate-rshotkey-0.1.1 (c (n "rshotkey") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rdev") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d4avrqw2gi842afy9r5f1b7y5s8hk0kls6sbxqzlj24l7k1haw3")))

