(define-module (crates-io rs db rsdbc-sqlite) #:use-module (crates-io))

(define-public crate-rsdbc-sqlite-0.0.1 (c (n "rsdbc-sqlite") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "rsdbc-core") (r "^0.0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (k 0)))) (h "089nx8xaw97dnzb44klbx69a4rh0lwf3bqijy7x3nbsc46c6nf1r")))

