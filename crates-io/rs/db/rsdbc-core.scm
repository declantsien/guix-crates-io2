(define-module (crates-io rs db rsdbc-core) #:use-module (crates-io))

(define-public crate-rsdbc-core-0.0.1 (c (n "rsdbc-core") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "06j60xkdla1f88s2vmh6ccr01h113x44qqjvymwxdhb1l30590ss")))

