(define-module (crates-io rs db rsdbc) #:use-module (crates-io))

(define-public crate-rsdbc-0.0.1 (c (n "rsdbc") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (o #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdbc-core") (r "^0.0.1") (d #t) (k 0)) (d (n "rsdbc-mysql") (r "^0.0.1") (d #t) (k 0)) (d (n "rsdbc-postgres") (r "^0.0.1") (d #t) (k 0)) (d (n "rsdbc-sqlite") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0g5ra4n6mf50wxdd3shv4dspg3rdhyd5wq2gss72n319vi5xw03z")))

