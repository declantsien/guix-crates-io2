(define-module (crates-io rs db rsdbc-mysql) #:use-module (crates-io))

(define-public crate-rsdbc-mysql-0.0.1 (c (n "rsdbc-mysql") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mysql") (r "^21.0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.28.0") (d #t) (k 0)) (d (n "rsdbc-core") (r "^0.0.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.12.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (k 0)))) (h "151wvl1jihy6i6dbjvb776r93xna2p2v3bh1hgkfb1lm9pppr22i")))

