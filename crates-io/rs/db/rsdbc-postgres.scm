(define-module (crates-io rs db rsdbc-postgres) #:use-module (crates-io))

(define-public crate-rsdbc-postgres-0.0.1 (c (n "rsdbc-postgres") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19.2") (d #t) (k 0)) (d (n "postgres-native-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "rsdbc-core") (r "^0.0.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (k 0)))) (h "0bv5nziwvr8xml1gw25kz3z5568x0mg8r0zwrlk8mzdj0iqiacmk")))

