(define-module (crates-io rs fi rsfile) #:use-module (crates-io))

(define-public crate-rsfile-0.1.0 (c (n "rsfile") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "0n48g7n25wx07sgmqpbwcv24vmy7dc2zgrh1fk7sa3hvm9g38ybg")))

(define-public crate-rsfile-0.1.1 (c (n "rsfile") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "webpage") (r "^1.4.0") (d #t) (k 0)))) (h "1wi35mmldxqw4p1hs9kmjy79hp3pd8f8hhyksxc916gkgl53bixg")))

(define-public crate-rsfile-0.1.2 (c (n "rsfile") (v "0.1.2") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "webpage") (r "^1.4.0") (d #t) (k 0)))) (h "0vamikr13pw4gn3slskbjldksd2fn8inh8i8964p4787b67062ix")))

