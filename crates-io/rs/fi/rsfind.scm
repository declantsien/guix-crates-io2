(define-module (crates-io rs fi rsfind) #:use-module (crates-io))

(define-public crate-rsfind-0.2.1 (c (n "rsfind") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)))) (h "1widanpcv2aipz71wlk5j7wkcdn57y5557l39dpmjbz97pz6zwnd")))

(define-public crate-rsfind-0.2.2 (c (n "rsfind") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)))) (h "0wm5qp752ga292v3dsams4xv71g7p93rx5s1zwykij0s3545rfz9")))

