(define-module (crates-io rs fi rsfi) #:use-module (crates-io))

(define-public crate-rsfi-0.0.0 (c (n "rsfi") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "1lslykxp4vvd37jfr5pcrf9s72qycpzqpibjdzzcds3s2n8yb23z") (f (quote (("wasm") ("wasi") ("rand" "num/rand") ("full" "default" "rand" "serde") ("default" "std")))) (s 2) (e (quote (("std" "alloc" "num/std" "serde?/std") ("serde" "dep:serde") ("alloc" "num/alloc" "serde?/alloc"))))))

