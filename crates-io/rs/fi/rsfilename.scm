(define-module (crates-io rs fi rsfilename) #:use-module (crates-io))

(define-public crate-rsfilename-0.1.0 (c (n "rsfilename") (v "0.1.0") (h "1567rr8jbqm68jsqg3x1h6mmmj4b6b50y0d7pa6q917yx7ndhh75")))

(define-public crate-rsfilename-0.2.0 (c (n "rsfilename") (v "0.2.0") (h "0gnj5s18mj1546n2hbx0zsxhaki5admk71dmbj0q7afw5kmy0ghx")))

