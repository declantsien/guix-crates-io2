(define-module (crates-io rs fi rsfinance) #:use-module (crates-io))

(define-public crate-rsfinance-0.1.0 (c (n "rsfinance") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "15f4v5c198s8jbjjk8i9bxlkzipzl0h2a7v7c86kv8by3wzf0ws8")))

