(define-module (crates-io rs lu rslua_derive) #:use-module (crates-io))

(define-public crate-rslua_derive-0.1.0 (c (n "rslua_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04bcl02fjljkalf4vqc5xfkvljkdr8svsn2n5n9hck6pzg5hi3sx") (r "1.72.0")))

