(define-module (crates-io rs lu rslua) #:use-module (crates-io))

(define-public crate-rslua-0.1.0 (c (n "rslua") (v "0.1.0") (h "1wxrb1pygnfcj21p55wly0yn0flzx17g36qz3f5s3rxqgx5n1611")))

(define-public crate-rslua-0.2.0 (c (n "rslua") (v "0.2.0") (h "1jlj73v9pfifcw4723dx2lmplw9p7rlpzjq8i31jqqb5lrgw91sy")))

(define-public crate-rslua-0.2.1 (c (n "rslua") (v "0.2.1") (h "0xl7hvqiyx1xy7w1cx78299i69hf7j7nan1il6hhjwhkj8z8c6vf")))

(define-public crate-rslua-0.2.2 (c (n "rslua") (v "0.2.2") (h "1bk04phpiz6ii3ywv1v7vrhxansh0cqljpamrgvh6cz9ac376f9b")))

(define-public crate-rslua-0.2.3 (c (n "rslua") (v "0.2.3") (h "18fa1m72ibphwlvbd54169bgcmy75j2f9v0ba4jppvlfdagvfr56")))

(define-public crate-rslua-0.2.4 (c (n "rslua") (v "0.2.4") (h "0zhk1102ishf1fi2m6dz4n0mqlc0srhhilr9vh5lr7ww1qccm0fa")))

(define-public crate-rslua-0.2.5 (c (n "rslua") (v "0.2.5") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1h215r55ip56bzzlvymxqky10099hf79pr6q7dg6jniv2r5b8d87")))

(define-public crate-rslua-0.2.6 (c (n "rslua") (v "0.2.6") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0pdws0h4gfb21p2ir9z9r0pl89m6hspcxzakkzlfvhy311rcbgy7")))

(define-public crate-rslua-0.2.7 (c (n "rslua") (v "0.2.7") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "04zjdm9mdrpghnkmp6y04avpdyrhd0b4sa5ggs5phizlld5yphhx")))

(define-public crate-rslua-0.2.8 (c (n "rslua") (v "0.2.8") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1cwqh8xlnd3lkgyglf4jlsbgwpanx16zwqkjddak75vpmvns85n0")))

(define-public crate-rslua-0.3.0 (c (n "rslua") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rslua_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rslua_traits") (r "^0.1.0") (d #t) (k 0)))) (h "03jrax68vqa30ycb9km5ybq2bmdj0z5ianf2zqb8hnsg7iyp1608") (r "1.72.0")))

