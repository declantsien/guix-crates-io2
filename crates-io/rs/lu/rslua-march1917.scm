(define-module (crates-io rs lu rslua-march1917) #:use-module (crates-io))

(define-public crate-rslua-march1917-0.2.7 (c (n "rslua-march1917") (v "0.2.7") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "06xbnyma1ycwrmj5cyr5f74b9njshd5sw55qb4gcgpwjxayzf9cv")))

(define-public crate-rslua-march1917-0.2.9 (c (n "rslua-march1917") (v "0.2.9") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "112cf85gdn17yh9gba9n8hjj80gr5iqc3b1pksr8vfdwzixl8jx4")))

(define-public crate-rslua-march1917-0.2.10 (c (n "rslua-march1917") (v "0.2.10") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "06xmvlsvkd199js4jfafnjy945hxxajqarsiy2qyvjvglvhdjb35")))

(define-public crate-rslua-march1917-0.2.11 (c (n "rslua-march1917") (v "0.2.11") (d (list (d (n "num-traits") (r "^0.2.13") (d #t) (k 0)))) (h "0f7jzsfa5msxsrklbkzbqmgdh4pfnszhssxnabqnmpz3dblfa736")))

(define-public crate-rslua-march1917-0.2.12 (c (n "rslua-march1917") (v "0.2.12") (d (list (d (n "num-traits") (r "^0.2.13") (d #t) (k 0)))) (h "06za5pkl1lgjmi8104nmqhvw3xp1pb83bj9v3m30n8kxlcg8argh")))

