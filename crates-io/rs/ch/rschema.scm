(define-module (crates-io rs ch rschema) #:use-module (crates-io))

(define-public crate-rschema-0.1.0 (c (n "rschema") (v "0.1.0") (d (list (d (n "rschema-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "1vrcaswjj64g1yi5asv6p898i56md0z80a74fixrcg3wc2rn823j")))

(define-public crate-rschema-0.1.1 (c (n "rschema") (v "0.1.1") (d (list (d (n "rschema-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "1xlf67m8az4axadq4wbwdq8y6fvd9l15rv87k8cj2yfq4agsmwrw")))

(define-public crate-rschema-0.1.2 (c (n "rschema") (v "0.1.2") (d (list (d (n "rschema-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "0vck0j8rqi0sq91qnjqbajxp83s7rv2lnfxhcid6xrvyda33x5rx")))

(define-public crate-rschema-0.1.3 (c (n "rschema") (v "0.1.3") (d (list (d (n "rschema-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "0i0rpw294rcda2hdlpy8gdg140jmfjcz5ji0mzsyvnya9gxl1d3f")))

(define-public crate-rschema-0.1.4 (c (n "rschema") (v "0.1.4") (d (list (d (n "rschema-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "09i9lmraaa622ya3cx74b9kgvmwlawm9v340zlaxlb0rvidn612r")))

(define-public crate-rschema-0.1.5 (c (n "rschema") (v "0.1.5") (d (list (d (n "rschema-core") (r "^0.2.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "11nqj337zm74jc14v6f6f6n938hg1p34q325z0fdig32m36mxpks")))

(define-public crate-rschema-0.1.6 (c (n "rschema") (v "0.1.6") (d (list (d (n "rschema-core") (r "^0.2.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "1cjbbfgg4drqd9xf7lcaldgmrj5pb46x2k3f7kdkpf05s9xc7wld")))

(define-public crate-rschema-0.2.0 (c (n "rschema") (v "0.2.0") (d (list (d (n "rschema-core") (r "^0.3.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "1a29qb13ishipj4qd9zyf9a4kznalxwai0hxqfgd4qrjrs487wav")))

(define-public crate-rschema-0.3.0 (c (n "rschema") (v "0.3.0") (d (list (d (n "rschema-core") (r "^0.3.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "0pva1qir81f795ngabqcixdr933kc99h48h3y2sfyqn18xr44dlf")))

(define-public crate-rschema-0.3.1 (c (n "rschema") (v "0.3.1") (d (list (d (n "rschema-core") (r "^0.3.1") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "02qkbsl5n4mlaa4nax1f706i0i9rxc6akpl1r7v7lm1qffz4cw75")))

(define-public crate-rschema-0.3.2 (c (n "rschema") (v "0.3.2") (d (list (d (n "rschema-core") (r "^0.3.2") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "1r6nh64m6js308xlqxc8z8jms50pa4dfk121a1d9a2x2ngikfp86")))

(define-public crate-rschema-0.3.3 (c (n "rschema") (v "0.3.3") (d (list (d (n "rschema-core") (r "^0.3.3") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "1bs32sg8ii5a9sx7x1wga1pcddh865z3c6mm93angkywiv60pm6c")))

(define-public crate-rschema-0.4.0 (c (n "rschema") (v "0.4.0") (d (list (d (n "rschema-core") (r "^0.3.3") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "005agbz9qbdqxf1if6rpg336g9c2hggzr51qspdk1ng9skrcw395")))

(define-public crate-rschema-0.4.1 (c (n "rschema") (v "0.4.1") (d (list (d (n "rschema-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "1d3kn4s3ckskyggp56jhhbr1qw4z1kx6xl64bvawnq5s4lfzzclw")))

(define-public crate-rschema-0.4.2 (c (n "rschema") (v "0.4.2") (d (list (d (n "rschema-core") (r "^0.3.5") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "05kawq6hppzamdl0maxm8ahizzcv6n9nxzc1kj1yfw02q73pk56m")))

(define-public crate-rschema-0.4.3 (c (n "rschema") (v "0.4.3") (d (list (d (n "rschema-core") (r "^0.3.6") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "197kkqnkr3lm6l0lqv8jqi3yzlhci7nqxl43p1k1hb8qgys78q0z")))

(define-public crate-rschema-0.5.0 (c (n "rschema") (v "0.5.0") (d (list (d (n "rschema-core") (r "^0.4.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "078q58j158hx5r820q94i7ivcrvsfzf9rf44hwx0p5cyyhwwp5j6")))

(define-public crate-rschema-0.5.1 (c (n "rschema") (v "0.5.1") (d (list (d (n "rschema-core") (r "^0.4.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "1xjbdx65k3c8042zqm7y84qs3s8yayb9p9bxm10ygkcvjhhrmfap")))

(define-public crate-rschema-0.6.0 (c (n "rschema") (v "0.6.0") (d (list (d (n "rschema-core") (r "^0.5.0") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 2)))) (h "08jfx3yh1m6g87kjh7k7l22mhp8nr32xm7csv5hzsvrp632ddnnn")))

(define-public crate-rschema-0.6.1 (c (n "rschema") (v "0.6.1") (d (list (d (n "rschema-core") (r "^0.5.1") (d #t) (k 0)) (d (n "rschema-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (d #t) (k 2)))) (h "0383lfd6j5bi2n6sqy9cwycnysl6983lwkzp1vc3pga28azilixs")))

