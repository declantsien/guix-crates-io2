(define-module (crates-io rs an rsanim) #:use-module (crates-io))

(define-public crate-rsanim-0.1.0 (c (n "rsanim") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "0nah9pw4spq0yg0qk8xijc1c9i154ad1a2585ylk01g942csgcxk")))

(define-public crate-rsanim-0.2.0 (c (n "rsanim") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "0xbhlv9ninldms5nwnz7lg4m8b6xz5mxi48z1qzxygf4lp96ckp3")))

(define-public crate-rsanim-0.2.1 (c (n "rsanim") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "1hlgsqkylhcybw0n5i1yr999wbn195jyqkxqk2g988s8y2c1c8nr")))

(define-public crate-rsanim-0.3.0 (c (n "rsanim") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "1wjcwa1c3y1fr2zcp9qnjzab7lifpinxk1ph9mpqrk5ghdcc9l90")))

