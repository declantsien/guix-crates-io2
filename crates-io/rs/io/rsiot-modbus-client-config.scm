(define-module (crates-io rs io rsiot-modbus-client-config) #:use-module (crates-io))

(define-public crate-rsiot-modbus-client-config-0.0.1 (c (n "rsiot-modbus-client-config") (v "0.0.1") (d (list (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "0k2vvblal7kzflshd1ncn9h5ip5rfhpmshp2kws4xp5c2npgr33w")))

(define-public crate-rsiot-modbus-client-config-0.0.2 (c (n "rsiot-modbus-client-config") (v "0.0.2") (d (list (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "12nqiqbsm2xfnmzxkk40p8h2lg6d38f9ps5qdgwzmjy1x4s8y6yb")))

(define-public crate-rsiot-modbus-client-config-0.0.3 (c (n "rsiot-modbus-client-config") (v "0.0.3") (d (list (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "07jhjgfaa8pprc6hrh43wnv5mwjqdgyfm6w0lc1ga28isg3krqml")))

(define-public crate-rsiot-modbus-client-config-0.0.4 (c (n "rsiot-modbus-client-config") (v "0.0.4") (d (list (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "1dhlk149fp3qj63ylgfdv6v7kxlld026h5iq8kz7dzzrssh0xx31")))

(define-public crate-rsiot-modbus-client-config-0.0.5 (c (n "rsiot-modbus-client-config") (v "0.0.5") (d (list (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "0d0s50h1jqbq7hk0p61lkrvdykn234jqjfk51qfa9fbh80m3z67d") (r "1.70.0")))

(define-public crate-rsiot-modbus-client-config-0.0.6 (c (n "rsiot-modbus-client-config") (v "0.0.6") (d (list (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "1w901znnl1djy87h87s0r16avdwy1w8gbsfp50ija3iddgggmmf6") (r "1.70.0")))

