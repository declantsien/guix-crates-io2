(define-module (crates-io rs io rsiot-http-client-config) #:use-module (crates-io))

(define-public crate-rsiot-http-client-config-0.0.2 (c (n "rsiot-http-client-config") (v "0.0.2") (d (list (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "0r8xw8kkpw95rdcqb4z1g8j092agdvhwhra4v26l0r72glbaa3kk")))

(define-public crate-rsiot-http-client-config-0.0.3 (c (n "rsiot-http-client-config") (v "0.0.3") (d (list (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "rsiot-messages-core") (r "^0.0.3") (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "1rb7d4qd705rkybb4bnsjkhk5bnn2cz6xjw8fqhxlzfzswhvmv4p")))

(define-public crate-rsiot-http-client-config-0.0.4 (c (n "rsiot-http-client-config") (v "0.0.4") (d (list (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "rsiot-messages-core") (r "^0.0.4") (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "0a639l43gzkf1l5qbx2g6zhrv34llibw4gmh46dh5ww19sckr7yh")))

(define-public crate-rsiot-http-client-config-0.0.5 (c (n "rsiot-http-client-config") (v "0.0.5") (d (list (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "rsiot-messages-core") (r "^0.0.5") (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "0872j0kqys0l79k8hwc68qib2syfyivnd2cd8w1vf783cq9qp39r") (r "1.70.0")))

(define-public crate-rsiot-http-client-config-0.0.6 (c (n "rsiot-http-client-config") (v "0.0.6") (d (list (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "rsiot-messages-core") (r "^0.0.6") (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "1gl9j1ad2q6qlghjcypx3h2li9bmz61aszac4x9054arxnjzcwyn") (r "1.70.0")))

(define-public crate-rsiot-http-client-config-0.0.7 (c (n "rsiot-http-client-config") (v "0.0.7") (d (list (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "rsiot-messages-core") (r "^0.0.7") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0fqx3mpxbhd95nfnwcpjxvcis74p8yp31g314mab8qgs492yk23v") (r "1.74.0")))

(define-public crate-rsiot-http-client-config-0.0.8 (c (n "rsiot-http-client-config") (v "0.0.8") (d (list (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "rsiot-messages-core") (r "^0.0.8") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0h75slbhvhb56dg6kawfwwljryvv199abpf2x7drayldcfnaqw4y") (r "1.74.0")))

