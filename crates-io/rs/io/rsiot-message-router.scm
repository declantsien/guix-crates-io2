(define-module (crates-io rs io rsiot-message-router) #:use-module (crates-io))

(define-public crate-rsiot-message-router-0.0.19 (c (n "rsiot-message-router") (v "0.0.19") (h "1scq6q4kaqcfnngv2chza0007an4d1s2ch07fqap4wm0av1858ql") (r "1.74.1")))

(define-public crate-rsiot-message-router-0.0.20 (c (n "rsiot-message-router") (v "0.0.20") (h "0888q5vn9203sgvhv36v5xcj1cq448z4faxjwsmjd9z4pjrxq7ii") (r "1.74.1")))

(define-public crate-rsiot-message-router-0.0.21 (c (n "rsiot-message-router") (v "0.0.21") (h "1l6k52gf4mjmj08xp8y4bbrjsm45igzgai3lxclnw2qwin6ss325") (r "1.74.1")))

