(define-module (crates-io rs io rsiot-macros) #:use-module (crates-io))

(define-public crate-rsiot-macros-0.0.13 (c (n "rsiot-macros") (v "0.0.13") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0wwcmhncay1wzh64g5fjfl9l3xnx49mm54f14i1nrkyz6d77zxyl") (r "1.74.0")))

(define-public crate-rsiot-macros-0.0.40 (c (n "rsiot-macros") (v "0.0.40") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0sz7x5wqi97g9qwjm8r066mmlw1fxns7kg4bfxscp9sn6wd66j2f") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.44 (c (n "rsiot-macros") (v "0.0.44") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0hs7q8lkgp9ggcsf6j189asyqp8f6ivvhcaaq67hfnz5lxplpxyp") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.45 (c (n "rsiot-macros") (v "0.0.45") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0hfcqvza2i49aghh7c0wpm67fskw7cm9dn50m6k9mqcha0r38jff") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.47 (c (n "rsiot-macros") (v "0.0.47") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "19npygnnq8alr88qxp4ji4012l8ivdzwf49qx1ai5ypgf2xcmkij") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.49 (c (n "rsiot-macros") (v "0.0.49") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "12s1x90qcqyqrkxy7vgc5q5r46gawz0sg7q1cyydgfw0732n7avy") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.52 (c (n "rsiot-macros") (v "0.0.52") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1hf545qc4mfshmzmn1iycvmh6cp1xvkkrr3miam2cv3bdj776vv4") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.54 (c (n "rsiot-macros") (v "0.0.54") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0a1rmrs6rnfwn6q3gy74hcq1iib3x0hk41yf35fx4r6cmj2484j9") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.55 (c (n "rsiot-macros") (v "0.0.55") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0maw705fvvx169lj9ksgclhawazygm0niqikfdyrfnp5rmhm1jdy") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.56 (c (n "rsiot-macros") (v "0.0.56") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0k2ic1nndgr9c0nkmxjri7vzkbs1r4h6wh729wgypcfnh4jk4a7d") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.60 (c (n "rsiot-macros") (v "0.0.60") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0g6d8pav22k9w6p5lh8d3c5smxa00p6xbpcdzvwmd5zz0b39gvrf") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.61 (c (n "rsiot-macros") (v "0.0.61") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0dwsk55qbnpmlf6ik353n4v1j2jjzacnlxi23a260smq74w1800c") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.62 (c (n "rsiot-macros") (v "0.0.62") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "01x8x8nfbc8ck3qrrqdq3aap0fnrwhvdg4b8ndnij244a5zk8m3n") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.63 (c (n "rsiot-macros") (v "0.0.63") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "028gpmfwh68jkhb2hkaq3bl5s630mvx26457vfqx5g6nmhivc4mk") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.64 (c (n "rsiot-macros") (v "0.0.64") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1qrgwznf8pdvsz3p4fr6q4w42qy3n52r5idnn7m5v0wpcvgi0v1v") (r "1.75.0")))

(define-public crate-rsiot-macros-0.0.72 (c (n "rsiot-macros") (v "0.0.72") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0wynvvvk8hvaq0bxdb2lj2sra5vc9dgqag8xl2rkmy2k2vkf91g6") (r "1.75.0")))

