(define-module (crates-io rs ye rsyesql) #:use-module (crates-io))

(define-public crate-rsyesql-0.3.0 (c (n "rsyesql") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mx6ywwmfiypgzvp5qlhc1s4lbl8h23cgqkldjh3mqa6szam28bg")))

(define-public crate-rsyesql-0.3.1 (c (n "rsyesql") (v "0.3.1") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jf8vl4klvs12a99vi7mkp31r0g23n54wmnyvyb96ww3xg9b1d1s")))

