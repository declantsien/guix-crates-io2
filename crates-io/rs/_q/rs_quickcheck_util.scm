(define-module (crates-io rs _q rs_quickcheck_util) #:use-module (crates-io))

(define-public crate-rs_quickcheck_util-0.1.0 (c (n "rs_quickcheck_util") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1g2fvggi5b558xbnlwqdsxh5hhlin1v6a0flqjcasq0vsy9a2yk7") (r "1.56")))

(define-public crate-rs_quickcheck_util-0.1.1 (c (n "rs_quickcheck_util") (v "0.1.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "03d43500q6c8axfg6770zb38ja6jnq2l004hzb5qgy47r30z8liz") (r "1.56")))

(define-public crate-rs_quickcheck_util-0.2.0 (c (n "rs_quickcheck_util") (v "0.2.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "15wmh7ayk96r62lyv0wlr3acv7s3akzdhdgzkaqgpm09y9b8dwhh") (r "1.56")))

