(define-module (crates-io rs ke rskey) #:use-module (crates-io))

(define-public crate-rskey-0.1.0 (c (n "rskey") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "09s7gqphrd9x2gsvf29jq6d67kyv03nlyzb77vn6plz1gkfi0zvw")))

(define-public crate-rskey-0.1.1 (c (n "rskey") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1pghdij1fiv68acaf1v69viby857h7910zsk7lsi1irgfz1h41v7")))

