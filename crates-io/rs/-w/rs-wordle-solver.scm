(define-module (crates-io rs -w rs-wordle-solver) #:use-module (crates-io))

(define-public crate-rs-wordle-solver-0.1.0 (c (n "rs-wordle-solver") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1w9i5f1dpcl1i4097llmy6vmy6x395jds86ci23w2r4n50q0b189")))

(define-public crate-rs-wordle-solver-0.1.1 (c (n "rs-wordle-solver") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1wxgzgrn994jz6dfw3h5i6jckyjw62smlm6ck2zdiwp5886m16ic")))

(define-public crate-rs-wordle-solver-0.1.2 (c (n "rs-wordle-solver") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0bl2ic2m3wac9yibr1xzdcraaysm6kyvp6adz0gc6vdgl13y94g4")))

(define-public crate-rs-wordle-solver-0.2.0 (c (n "rs-wordle-solver") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0iis69q8kvi7g32hd8jg29sqwr5x1kr70lpann614zxgpjmgfnym")))

(define-public crate-rs-wordle-solver-0.3.0 (c (n "rs-wordle-solver") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("rc"))) (d #t) (k 2)))) (h "1wa934xzk16fmcrp30djpyhnyj3w25c5r4yfg31pk8m05njj4dhd")))

(define-public crate-rs-wordle-solver-1.0.0 (c (n "rs-wordle-solver") (v "1.0.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("rc" "derive"))) (o #t) (d #t) (k 0)))) (h "1bwahz0a9iwv2i9am8zx7wfjs357gi4g67axkjskfazmh7fz1603") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-rs-wordle-solver-1.1.0 (c (n "rs-wordle-solver") (v "1.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("rc" "derive"))) (o #t) (d #t) (k 0)))) (h "1rfyaj8vgm5wadjx9nw3xjydsihxcqgnlwnlakym6hlpbwcdgvmd") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-rs-wordle-solver-1.2.0 (c (n "rs-wordle-solver") (v "1.2.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("rc" "derive"))) (o #t) (d #t) (k 0)))) (h "0h8l8f52hskif69r66iyzdwmain7gwias7lj4mhr4y33k7bqmvgn") (s 2) (e (quote (("serde" "dep:serde"))))))

