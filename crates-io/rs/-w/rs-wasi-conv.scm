(define-module (crates-io rs -w rs-wasi-conv) #:use-module (crates-io))

(define-public crate-rs-wasi-conv-0.1.0 (c (n "rs-wasi-conv") (v "0.1.0") (d (list (d (n "wasmedge-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmedge-types") (r "^0.2.0") (d #t) (k 0)))) (h "0b3ajqrq8dfibpvqcanfp74in03py7hxp51ls0lgcg41w60ci6kq")))

(define-public crate-rs-wasi-conv-0.1.1 (c (n "rs-wasi-conv") (v "0.1.1") (d (list (d (n "wasmedge-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmedge-types") (r "^0.2.0") (d #t) (k 0)))) (h "0dsf4n55v8lby2i8jk7bvfx8dsp7dby6xdbmdi2b42bi1010czhn")))

(define-public crate-rs-wasi-conv-0.1.2 (c (n "rs-wasi-conv") (v "0.1.2") (d (list (d (n "wasmedge-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmedge-types") (r "^0.2.0") (d #t) (k 0)))) (h "1i312mnpvh8h2z6z17lrv6nvhgw3h1yh21vhvx779m0q7qpmmk12")))

