(define-module (crates-io rs pl rspl) #:use-module (crates-io))

(define-public crate-rspl-0.1.0 (c (n "rspl") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1zxffc9syac95pvsxfp588wfy7gcbmrvk23g4l31p54qmjlk69sv") (f (quote (("std" "crossbeam") ("default" "std"))))))

(define-public crate-rspl-0.1.1 (c (n "rspl") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1q3ldjr7rgw9gynr220xk006ivkwn2fyvg0myxs8drxrlbm7m3f9") (f (quote (("std" "crossbeam") ("default" "std"))))))

(define-public crate-rspl-0.1.2 (c (n "rspl") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1c8dxph3k9s0yvnway4y5rwcmx8ps52jn79fg33sn8g6dd56k5x3") (f (quote (("std" "crossbeam") ("default" "std"))))))

