(define-module (crates-io rs pl rsplit) #:use-module (crates-io))

(define-public crate-rsplit-0.1.0 (c (n "rsplit") (v "0.1.0") (h "00m5ysjsyfh10k21z1hyn7dr4cdvq94xxnrsjxqs1qmslb44cnj5")))

(define-public crate-rsplit-0.1.1 (c (n "rsplit") (v "0.1.1") (h "1x90f5fjpvy2aizfa4qfxnmhxfph4l6yhm6zddfn94qc2ci0r08w")))

(define-public crate-rsplit-0.1.2 (c (n "rsplit") (v "0.1.2") (h "19n948l3qfk4d6z1brxndwgwijiq7gaj4ig24q4sp7p6y5382ksd")))

