(define-module (crates-io rs pl rsplitter) #:use-module (crates-io))

(define-public crate-rsplitter-0.1.0 (c (n "rsplitter") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1wz81ww0x9z21hl5lql29f4yr241zlj68pgqi1w0xa0cp0yx024y")))

(define-public crate-rsplitter-0.1.1 (c (n "rsplitter") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04izp13nlxg0zcbvkclysc3k1d58x72a9zdvyhqr2apnxwxhaq2s")))

(define-public crate-rsplitter-0.1.2 (c (n "rsplitter") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01fih05bp14labk357xhvgw1pi6hkx1y5akpsahh0xzqw82n6nz6")))

(define-public crate-rsplitter-0.1.3 (c (n "rsplitter") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1alzl5777nfi64jifawq991z67c950yfywac2z89zxz7pr1f3mlk")))

(define-public crate-rsplitter-0.1.4 (c (n "rsplitter") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "14x0s8fjh0rp4f0q06bncabzhqn6sj2bgb0iv67m6c3g8y058rly")))

(define-public crate-rsplitter-0.1.5 (c (n "rsplitter") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0974xvzjfjkigyrdb36blcjdcf2v6sf563a7inb2xgswlk049y85")))

(define-public crate-rsplitter-0.2.0 (c (n "rsplitter") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zgswp8rd5mjavgg257apfghhycshvzwbcikry0smhggd4zwfpws")))

