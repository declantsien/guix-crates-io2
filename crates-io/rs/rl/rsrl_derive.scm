(define-module (crates-io rs rl rsrl_derive) #:use-module (crates-io))

(define-public crate-rsrl_derive-0.1.0 (c (n "rsrl_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yg75x252i3d2fyvbxkmnw9484rahrsj7p7sqli1lipzfidxa16j")))

