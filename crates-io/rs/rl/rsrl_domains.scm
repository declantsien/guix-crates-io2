(define-module (crates-io rs rl rsrl_domains) #:use-module (crates-io))

(define-public crate-rsrl_domains-0.1.0 (c (n "rsrl_domains") (v "0.1.0") (d (list (d (n "cpython") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "spaces") (r "^5.0") (d #t) (k 0)))) (h "0k96cz83fl4msgaspczvp0sil2sxmg6h81g9lyqg5r4q9wqzpcpp") (f (quote (("openai" "cpython") ("default"))))))

(define-public crate-rsrl_domains-0.2.0 (c (n "rsrl_domains") (v "0.2.0") (d (list (d (n "cpython") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "spaces") (r "^5.0") (d #t) (k 0)))) (h "14pmyny35k1sibnhpr3w3lblylm2v4an2fwz4n1x227mybkvrbli") (f (quote (("openai" "cpython") ("default"))))))

