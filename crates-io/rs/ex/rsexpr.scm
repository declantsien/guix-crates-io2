(define-module (crates-io rs ex rsexpr) #:use-module (crates-io))

(define-public crate-rsexpr-0.1.0 (c (n "rsexpr") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1f3bzwy22a37xhql7kibhdwab5simhdyz5kwf4xbzhxji623b99n")))

(define-public crate-rsexpr-0.1.1 (c (n "rsexpr") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0fkfcd9q5zxnrmapsfmxknanc24d1h6qpiyfbgpslc4idflywaqn")))

(define-public crate-rsexpr-0.2.0 (c (n "rsexpr") (v "0.2.0") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0bpcnf3v14qg5nz20wq9xs65z62b8c4848wfw8w5szy7cb27hlra") (f (quote (("default") ("comments")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-rsexpr-0.2.1 (c (n "rsexpr") (v "0.2.1") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1p7zs9q82rksmc9vdnbs9f41fw0hk010g1mb4klg3hgyp8sq021i") (f (quote (("default") ("comments")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-rsexpr-0.2.2 (c (n "rsexpr") (v "0.2.2") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0rig6hngk74mbdihx4ljnzsbwnx0i97yhrlm08znq474a8w63fx6") (f (quote (("default") ("comments")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-rsexpr-0.2.3 (c (n "rsexpr") (v "0.2.3") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0h5nrwxrm462raacgmw9hbrgk4h2w70cma20l1b4afk2zkn348zs") (f (quote (("default") ("comments")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-rsexpr-0.2.4 (c (n "rsexpr") (v "0.2.4") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "18i710mprs9kdg306masmb5qv2h2xp90504rh51zfyr98ihv3wzg") (f (quote (("default") ("comments")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-rsexpr-0.2.5 (c (n "rsexpr") (v "0.2.5") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1rbv951ghx82ff3cvmkg0ra8sqai50kb7zlw1wpwh2y8c0p240wk") (f (quote (("default") ("comments")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

