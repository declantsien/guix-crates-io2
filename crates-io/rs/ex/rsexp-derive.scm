(define-module (crates-io rs ex rsexp-derive) #:use-module (crates-io))

(define-public crate-rsexp-derive-0.1.0 (c (n "rsexp-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f0rx72d45g6pzb9asrcksas02367mcp27gwcc88w12jalq67kr8")))

(define-public crate-rsexp-derive-0.2.0 (c (n "rsexp-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fgmr998wjw6vmpzifrwvcx4sm34lp98r15vq9jkvsj13jpvxlkc")))

(define-public crate-rsexp-derive-0.2.1 (c (n "rsexp-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wdqpl2qr1ir6vny1fssyy8dxlm78vzzilhxj2f7abiqlgi6a5yz")))

(define-public crate-rsexp-derive-0.2.2 (c (n "rsexp-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05ns1m5hqzbvjq3ighn9rpsdq1h0m438c9sh2mcybqp3ylff4yji")))

(define-public crate-rsexp-derive-0.2.3 (c (n "rsexp-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00rl9cn2a89d9n9gwazr2zrp64870fl0z1yr54qn2bscyqq62wp0")))

