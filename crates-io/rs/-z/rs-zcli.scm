(define-module (crates-io rs -z rs-zcli) #:use-module (crates-io))

(define-public crate-rs-zcli-0.1.0 (c (n "rs-zcli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rprompt") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.178") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0k992xj14sa8q2r8qksjnq8i56plsdz5gij1i8midxlsll97lisv")))

(define-public crate-rs-zcli-0.1.1 (c (n "rs-zcli") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rprompt") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.178") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1z2jsrnjdg6dzjf4n240af31lyqsww6g7x81blj46kywjawmrx1s")))

