(define-module (crates-io rs -m rs-matter-macros) #:use-module (crates-io))

(define-public crate-rs-matter-macros-0.1.0 (c (n "rs-matter-macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rhx3shj71a6ri4sw6ayc2ax533f69bfbqr4zz0yk2lwmi5nmidv")))

