(define-module (crates-io rs -m rs-mem) #:use-module (crates-io))

(define-public crate-rs-mem-0.1.0 (c (n "rs-mem") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "1b9aip1qs5r5n671c33a426ks592j62c1a5rx1canngbpkinj3bf")))

(define-public crate-rs-mem-0.1.1 (c (n "rs-mem") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "11q8p30jfiyamr0gdyj6k8hdxdjzh0d0iw6wp6sqiv8k63sg0a2a")))

(define-public crate-rs-mem-0.2.0 (c (n "rs-mem") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "0rdnbjvhayrp03xqnznlvcl1g0d4bx2csk1abaakwkdm2cf8mnyz")))

(define-public crate-rs-mem-0.2.1 (c (n "rs-mem") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "1d18p5yap0lidmfxw8vb5n5qbih5q5i3gw9my5jgqjb49vp1i436")))

(define-public crate-rs-mem-0.2.2 (c (n "rs-mem") (v "0.2.2") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "095c4s518kpnn88sl1sspb35x73sf6rari8g6pjf1wahxr1ddrm1") (y #t)))

(define-public crate-rs-mem-0.2.3 (c (n "rs-mem") (v "0.2.3") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "0wkv621mihwxps7pgj7w4cjb6cl2ynqdyv8px7w12gm8f2bii10s") (y #t)))

(define-public crate-rs-mem-0.2.4 (c (n "rs-mem") (v "0.2.4") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "17078vfhgfhc9jhx08f36bp2m4pkhx7qmfk02f9my44rfpls254v") (y #t)))

(define-public crate-rs-mem-0.2.5 (c (n "rs-mem") (v "0.2.5") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "09cqrx24ihp1rg62ggkfmlv1djqn8g6n3yg9438aqxzhhin5k4j5")))

(define-public crate-rs-mem-0.2.6 (c (n "rs-mem") (v "0.2.6") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "02l4z0ih83qg2idpmbz9g3cd3giginjziz05d1g2aavhldvvriqj")))

