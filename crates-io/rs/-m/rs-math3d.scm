(define-module (crates-io rs -m rs-math3d) #:use-module (crates-io))

(define-public crate-rs-math3d-0.9.0 (c (n "rs-math3d") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "0qq6yb4q57c9843in65fkd9fa2ylpqyvq6cwmm7f0zjgcgyh25x3")))

(define-public crate-rs-math3d-0.9.1 (c (n "rs-math3d") (v "0.9.1") (d (list (d (n "libc") (r "^0.2.76") (d #t) (k 0)))) (h "1wmnaxsd017kznk85vql6qgb3mdyp06jyw0qml524nzmavba0wz6")))

(define-public crate-rs-math3d-0.9.2 (c (n "rs-math3d") (v "0.9.2") (d (list (d (n "libc") (r "^0.2.76") (d #t) (k 0)))) (h "19xhf1liyvlcmhbxbwdsp1vp3ih2m228zxbp2ryhb6d68fprp3m7")))

(define-public crate-rs-math3d-0.9.3 (c (n "rs-math3d") (v "0.9.3") (d (list (d (n "libc") (r "^0.2.76") (d #t) (k 0)))) (h "0qpxsjygl3vky0h88wbyzjqmaqms67cw8jxl2m9j6j96zly7m6ck")))

(define-public crate-rs-math3d-0.9.4 (c (n "rs-math3d") (v "0.9.4") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "07xwzy3y2yjn798ngs1666cqh6iyya8dcqi5ixc2n5h4dd0fxjb5") (y #t)))

(define-public crate-rs-math3d-0.9.5 (c (n "rs-math3d") (v "0.9.5") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "0sdhbwcsjycwib8xmqwclf3w3wl9h02k9h3akxz0d4n6pgs6kgsm")))

(define-public crate-rs-math3d-0.9.6 (c (n "rs-math3d") (v "0.9.6") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "12f84k5qp8qib90ybcdqk498lfnnm43x46yjgii05jl7nbgqbydp")))

(define-public crate-rs-math3d-0.9.7 (c (n "rs-math3d") (v "0.9.7") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "136ch979qi9rjms0icdlqg1fy7g0wdf0zm7mp2l7zvp287sbgl5c")))

(define-public crate-rs-math3d-0.9.8 (c (n "rs-math3d") (v "0.9.8") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "0v31i2rgj22kxkxydnz2ianpr52kp9lvd6wwinq82mz4gc8wgw60")))

(define-public crate-rs-math3d-0.9.9 (c (n "rs-math3d") (v "0.9.9") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "0gqjy7m1dc59g742ia07ap9w5nvadavsjkzhga71g450j4nk5x63")))

(define-public crate-rs-math3d-0.9.10 (c (n "rs-math3d") (v "0.9.10") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "0mw855bfwcswjz51nq9zafxiykkfcrgxg6ggpp1wqlj7p7cma4zj")))

(define-public crate-rs-math3d-0.9.11 (c (n "rs-math3d") (v "0.9.11") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "1sz5y5x60g50xs205xmsanzgmp0d894i0yrhv3qrij2053fgws2a")))

(define-public crate-rs-math3d-0.9.12 (c (n "rs-math3d") (v "0.9.12") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "0prdxz9ava5py1p72ad5yvvhsv5wbsmd4whg7lylgpbmydx0i7v3")))

(define-public crate-rs-math3d-0.9.13 (c (n "rs-math3d") (v "0.9.13") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "190g6q9hksw42p7d7ifqk6pj77abcsy4r63zpslmafln11k3rfap")))

(define-public crate-rs-math3d-0.9.14 (c (n "rs-math3d") (v "0.9.14") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "0is032wy0py86say9qdjwl4pfyafn400qzr6qwpmfk3sdk5plj0j")))

(define-public crate-rs-math3d-0.9.15 (c (n "rs-math3d") (v "0.9.15") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "0gm9mzypxq844f78dnb1l37631z55yw6jkdvqfxz557i934rw9i6")))

(define-public crate-rs-math3d-0.9.16 (c (n "rs-math3d") (v "0.9.16") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "1xa0viiqvmn2kwpzrifq29150qjni2bazglal94a2hgdaql5m4v4")))

(define-public crate-rs-math3d-0.9.17 (c (n "rs-math3d") (v "0.9.17") (d (list (d (n "fixed") (r "^1.16.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "1qjy0grwyak5ra95qdvhavhxw23rjh46pzz9lg0d0y2zv1z72wxz") (f (quote (("fixedpoint" "fixed") ("default"))))))

(define-public crate-rs-math3d-0.9.18 (c (n "rs-math3d") (v "0.9.18") (d (list (d (n "fixed") (r "^1.16.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "1dp77mz3098wa0bkc3sc2k0xcikq7cbfr44d61f9n0zm4zlhls7g") (f (quote (("fixedpoint" "fixed") ("default"))))))

(define-public crate-rs-math3d-0.9.19 (c (n "rs-math3d") (v "0.9.19") (d (list (d (n "fixed") (r "^1.16.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "14ssfas1v84gsj7b6gqz866cbblrgzdx295h0hhva4bw1s4br9s1") (f (quote (("fixedpoint" "fixed") ("default"))))))

(define-public crate-rs-math3d-0.9.20 (c (n "rs-math3d") (v "0.9.20") (d (list (d (n "fixed") (r "^1.16.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "00hrxpki5992h16jh86imszvhh6j125bf14q3xlvpghvs9k8xcj5") (f (quote (("fixedpoint" "fixed") ("default"))))))

(define-public crate-rs-math3d-0.9.21 (c (n "rs-math3d") (v "0.9.21") (h "1542hxlbmxz5a55b13f811gddcpl78b8rz3mj369rwfc309wwhpi") (f (quote (("default"))))))

(define-public crate-rs-math3d-0.9.22 (c (n "rs-math3d") (v "0.9.22") (h "0ncb77xn7fs6zd1jd3l0vsr3515q0kkm6az339lq9qixmaxif4sb") (f (quote (("default"))))))

(define-public crate-rs-math3d-0.9.23 (c (n "rs-math3d") (v "0.9.23") (h "0ia1diak8ck9kwyhqr43r6ddzhga0k2hag70j0cb0z3gmsxim7am") (f (quote (("default"))))))

