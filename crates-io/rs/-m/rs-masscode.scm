(define-module (crates-io rs -m rs-masscode) #:use-module (crates-io))

(define-public crate-rs-masscode-0.1.0 (c (n "rs-masscode") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (d #t) (k 2)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1yc5y3fmjkrhidl0i4926yyg1kl1b2wl458vps3f9p4nzynv2q09") (r "1.64.0")))

