(define-module (crates-io rs -m rs-measures) #:use-module (crates-io))

(define-public crate-rs-measures-0.0.0 (c (n "rs-measures") (v "0.0.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 2)))) (h "045r10dqvdlyr73wzvyk0m2s92zfln5cqfb06zpd6i58mq3gy0f1")))

(define-public crate-rs-measures-0.1.0 (c (n "rs-measures") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 2)))) (h "1kv4xxr1d1f75dcw9shn1hb3p4pmq2hs4br13h2nbr5hh8n1dzps")))

(define-public crate-rs-measures-0.2.0 (c (n "rs-measures") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 2)))) (h "1nbd96xwi4g22lwj3h75s9qlyql50zwn9i8sp60q2ypzdb7zk9v7")))

(define-public crate-rs-measures-0.3.0 (c (n "rs-measures") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 2)))) (h "1qjwca36wi4n44bq3c9nssfb3xmc9h4fswg4y864psgywigd23si")))

(define-public crate-rs-measures-0.4.0 (c (n "rs-measures") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05lpr2gxvkz6ny7r1mrkwdzxj25vg083ry6njndql74rig4msbys")))

