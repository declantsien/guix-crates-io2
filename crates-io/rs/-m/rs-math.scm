(define-module (crates-io rs -m rs-math) #:use-module (crates-io))

(define-public crate-rs-math-0.1.0 (c (n "rs-math") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "ctor") (r "^0.1.19") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0mm1z1v65z6sa2pgq0g8j5nms6rdncni329qyiz8jihhlakdn86v")))

