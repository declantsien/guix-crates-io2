(define-module (crates-io rs _p rs_pyro) #:use-module (crates-io))

(define-public crate-rs_pyro-0.0.1 (c (n "rs_pyro") (v "0.0.1") (h "1gbl3vg68ifxnnx43vbjm74pqk0r9r5w48hfsr11z44jnwb5ilbg") (y #t)))

(define-public crate-rs_pyro-0.0.0 (c (n "rs_pyro") (v "0.0.0") (d (list (d (n "plotters") (r "^0.3.5") (d #t) (k 0)))) (h "17vg4pbglc6qpi0r94gpalk793xb3rd5vv0pg7rjfh9wk2d73d1q") (y #t)))

