(define-module (crates-io rs _p rs_poly1305) #:use-module (crates-io))

(define-public crate-rs_poly1305-0.1.0 (c (n "rs_poly1305") (v "0.1.0") (h "05bhz7hrml5zlfiwb1ilwd57h0xb1yv1i95pp4s2cjf03km1yl21")))

(define-public crate-rs_poly1305-0.1.1 (c (n "rs_poly1305") (v "0.1.1") (h "0dqm3qwpwjas3n6bqkz7l2nwx9fl58lhdm9cx2agb277kbrgvkbi")))

(define-public crate-rs_poly1305-0.1.2 (c (n "rs_poly1305") (v "0.1.2") (h "0rj1a92hm8riw318bcpmz0f88hjwsk3ngnbmvq24ikn5b1s20ih2")))

