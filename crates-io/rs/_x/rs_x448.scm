(define-module (crates-io rs _x rs_x448) #:use-module (crates-io))

(define-public crate-rs_x448-0.1.0 (c (n "rs_x448") (v "0.1.0") (h "1r2wipana50k1zp8ws4nfjqj6az034r63d7qsw82bnyyqzb1sdpk")))

(define-public crate-rs_x448-0.1.1 (c (n "rs_x448") (v "0.1.1") (h "16b8mimagymvhhqh8zx1cbpqxcj6ckif1xnq1n2w13141lrpy3l9")))

(define-public crate-rs_x448-0.1.2 (c (n "rs_x448") (v "0.1.2") (h "1f4yj7zfx1a7qjvscm40sgpxn5spcrxj1c2494fc7n0x96vbmwgv")))

