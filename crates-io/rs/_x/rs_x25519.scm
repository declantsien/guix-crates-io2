(define-module (crates-io rs _x rs_x25519) #:use-module (crates-io))

(define-public crate-rs_x25519-0.1.0 (c (n "rs_x25519") (v "0.1.0") (h "083ydxnlg3llpj0fmqig16v60dnpc6dvc9b2mlq8g9al3gnqm7g4")))

(define-public crate-rs_x25519-0.1.1 (c (n "rs_x25519") (v "0.1.1") (h "13iyihflfqz6jah0d0qd87g71znrlnch7cfqsipbz06x6r3y2h2m")))

(define-public crate-rs_x25519-0.1.2 (c (n "rs_x25519") (v "0.1.2") (h "0b7330fj5byl8kkmpia5rz45r0pd3yw1c38falk2bzvg6hlx9kxl")))

