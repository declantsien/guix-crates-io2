(define-module (crates-io rs ol rsolace-sys) #:use-module (crates-io))

(define-public crate-rsolace-sys-0.1.0 (c (n "rsolace-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0pbvxxvxjfbndjgbzvnwp8al9w26m466v1cvfaggyyjivcddh9k9")))

(define-public crate-rsolace-sys-0.1.1 (c (n "rsolace-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0fg6xk8fn8pf6d69li59zzzgg62f8ndrim1mj0izmwzr8cx5dyyl")))

(define-public crate-rsolace-sys-0.1.3 (c (n "rsolace-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "10y78z687h2a0bamzncb650js0a5qq6f460rcm9zvbp0qhsjpyzz")))

(define-public crate-rsolace-sys-0.1.4 (c (n "rsolace-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "078cb6fskj1fa2vbsi961zqjg69ik57x1rjxibrphddh39gjkagz")))

(define-public crate-rsolace-sys-0.1.5 (c (n "rsolace-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "10imdsswcq2aqrbyky5l8gfk41asswm8rg7kqhzfppiadpvqfpw4")))

