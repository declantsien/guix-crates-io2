(define-module (crates-io rs _e rs_envflag_macros) #:use-module (crates-io))

(define-public crate-rs_envflag_macros-0.2.0 (c (n "rs_envflag_macros") (v "0.2.0") (d (list (d (n "rs_envflag") (r "^0.2") (d #t) (k 0)))) (h "10n7a287k3nyl0xxvviaiylhjfcs4wma6n2cqjwdyqrbpmm8f0cm")))

(define-public crate-rs_envflag_macros-0.3.0 (c (n "rs_envflag_macros") (v "0.3.0") (d (list (d (n "rs_envflag") (r "^0.3") (d #t) (k 0)))) (h "1wdq2gfy9df2vixnamnxbpd30yby0fkawjnknqvndrafp22r0872")))

