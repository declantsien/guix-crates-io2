(define-module (crates-io rs _e rs_envflag) #:use-module (crates-io))

(define-public crate-rs_envflag-0.1.0 (c (n "rs_envflag") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fqypmivmiyy3cc01znayg3ll2pkxd4b1xyps58rrnzrh336g871")))

(define-public crate-rs_envflag-0.1.1 (c (n "rs_envflag") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0gk3nmg2jd66flhr3hm8n971lnkxmi79cwv3c7gy8vyx49is2d8s")))

(define-public crate-rs_envflag-0.2.0 (c (n "rs_envflag") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kndgwdn31phw341vv251j8rb14v7nvwfln1a8wg3k3s9g25n17g")))

(define-public crate-rs_envflag-0.3.0 (c (n "rs_envflag") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "078srkcdj8ic31c1dz5dkkb3j6i1kzhq2d92pyi7l7nh14q5hz0a")))

(define-public crate-rs_envflag-0.4.0 (c (n "rs_envflag") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0n2qirizmpg5mn6y4ff46npyi8z6cv6hbnwnr3pg86mzdf2l64p4")))

