(define-module (crates-io rs _e rs_ed25519) #:use-module (crates-io))

(define-public crate-rs_ed25519-0.1.0 (c (n "rs_ed25519") (v "0.1.0") (h "0i46rsz5bnj7pk7yzrip80vk97i7zvlbmamn8a4g1pl2q39dqd8r")))

(define-public crate-rs_ed25519-0.1.1 (c (n "rs_ed25519") (v "0.1.1") (h "0k0cjyjb8dfqr3q5y5q15dg5p0rpmgpdqwxpvzm26d72z97g83s4")))

(define-public crate-rs_ed25519-0.1.2 (c (n "rs_ed25519") (v "0.1.2") (h "09wrin4x4njiq31jp4a8cahq80gfzq47b8286vghd07i419w5r3x")))

