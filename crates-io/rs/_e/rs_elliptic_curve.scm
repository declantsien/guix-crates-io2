(define-module (crates-io rs _e rs_elliptic_curve) #:use-module (crates-io))

(define-public crate-rs_elliptic_curve-0.1.0 (c (n "rs_elliptic_curve") (v "0.1.0") (h "0kpfglskk3qv02d2nhnn28ixxqpny6xfaiq2lqvmkvxpfry1rdmg")))

(define-public crate-rs_elliptic_curve-0.1.1 (c (n "rs_elliptic_curve") (v "0.1.1") (h "1flq7nwkhsg2c5zi7n42sy62kwn7bjak8w6ghqzwfy1znrgvkx0j")))

(define-public crate-rs_elliptic_curve-0.1.2 (c (n "rs_elliptic_curve") (v "0.1.2") (h "16xi50mq38x9n5a382rrq40d03nxysgikppx6fpphhxp3vbz6nxg")))

