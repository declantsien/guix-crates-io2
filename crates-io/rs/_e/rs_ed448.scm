(define-module (crates-io rs _e rs_ed448) #:use-module (crates-io))

(define-public crate-rs_ed448-0.1.0 (c (n "rs_ed448") (v "0.1.0") (h "1znx1ydlay1jfhlrnfgn0f0rsx4sblfgwy8sffdyq5d5djhgmjwl")))

(define-public crate-rs_ed448-0.1.1 (c (n "rs_ed448") (v "0.1.1") (h "1iixjj5cnrz6ji65kic8azyqv5hj15pd3mgskdvp6pzkv4p92c6n")))

(define-public crate-rs_ed448-0.1.2 (c (n "rs_ed448") (v "0.1.2") (h "1axmcgx6jnmbmps5yz7fj5qq2lvs9jzp4im3dzmix5lyrsaknqhr")))

