(define-module (crates-io rs mo rsmonad-macros) #:use-module (crates-io))

(define-public crate-rsmonad-macros-0.1.0 (c (n "rsmonad-macros") (v "0.1.0") (d (list (d (n "heck") (r ">=0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "070264ds4lsij697gxpparm4zlp5mh7xs20hi9ff0rd4yi4a58iw") (f (quote (("quickcheck") ("default" "quickcheck"))))))

(define-public crate-rsmonad-macros-0.1.1 (c (n "rsmonad-macros") (v "0.1.1") (d (list (d (n "heck") (r ">=0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "136cxja2rgwlxp65bglyklbfylvrp1vl1vhyfy33rk1gyp8sjapp") (f (quote (("quickcheck") ("default" "quickcheck"))))))

(define-public crate-rsmonad-macros-0.1.2 (c (n "rsmonad-macros") (v "0.1.2") (d (list (d (n "heck") (r ">=0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pkk5b6j347n57cas3x99vckyxajw84b43kmkdy3r9bmqj7fbwn1") (f (quote (("quickcheck") ("default" "quickcheck"))))))

