(define-module (crates-io rs mo rsmonad) #:use-module (crates-io))

(define-public crate-rsmonad-0.1.0 (c (n "rsmonad") (v "0.1.0") (d (list (d (n "ahash") (r ">=0.8.3") (d #t) (k 0)) (d (n "quickcheck") (r ">=1.0.3") (d #t) (k 0)) (d (n "rsmonad-macros") (r ">=0.1.0") (d #t) (k 0)) (d (n "same-as") (r ">=1") (d #t) (k 0)))) (h "0dfn2dmh5v3ym5rak8ff5vzxwrhf14inh35pfc7zlx50gbxnj444") (f (quote (("std") ("default"))))))

(define-public crate-rsmonad-0.1.1 (c (n "rsmonad") (v "0.1.1") (d (list (d (n "ahash") (r ">=0.8.3") (d #t) (k 0)) (d (n "quickcheck") (r ">=1.0.3") (d #t) (k 0)) (d (n "rsmonad-macros") (r ">=0.1.0") (d #t) (k 0)) (d (n "same-as") (r ">=1") (d #t) (k 0)))) (h "0nca7yl51smv2a5k7fzvxyr4a8mf1mgb70qn3myhspicjrr1wdhz") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-rsmonad-0.1.2 (c (n "rsmonad") (v "0.1.2") (d (list (d (n "ahash") (r ">=0.8.3") (d #t) (k 0)) (d (n "quickcheck") (r ">=1.0.3") (d #t) (k 0)) (d (n "rsmonad-macros") (r ">=0.1.2") (d #t) (k 0)) (d (n "same-as") (r ">=1") (d #t) (k 0)))) (h "00qaicvc3d9xzswmab4hlb96zv6cbb10qkz266vmjk8jwx0v58f5") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-rsmonad-0.2.0 (c (n "rsmonad") (v "0.2.0") (d (list (d (n "ahash") (r ">=0.8.3") (d #t) (k 0)) (d (n "derive-quickcheck") (r ">=0.1.1") (d #t) (k 0)) (d (n "paste") (r ">=1") (d #t) (k 0)) (d (n "quickcheck") (r ">=1") (d #t) (k 0)) (d (n "same-as") (r ">=1") (d #t) (k 0)))) (h "1hknp240jr3pqr7699q8dcd5aj5pdh8wz4bqv270wl90vwmgzf13") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-rsmonad-0.2.1 (c (n "rsmonad") (v "0.2.1") (d (list (d (n "ahash") (r ">=0.8.3") (d #t) (k 0)) (d (n "derive-quickcheck") (r ">=0.1.1") (d #t) (k 0)) (d (n "paste") (r ">=1") (d #t) (k 0)) (d (n "quickcheck") (r ">=1") (d #t) (k 0)) (d (n "same-as") (r ">=1") (d #t) (k 0)))) (h "1iywgkf26y0w6xidykqvgcpsqby6x8ygyy6fwh4clxf1pind4rin") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-rsmonad-0.2.2 (c (n "rsmonad") (v "0.2.2") (d (list (d (n "ahash") (r ">=0.8.3") (d #t) (k 0)) (d (n "derive-quickcheck") (r ">=0.1.1") (d #t) (k 0)) (d (n "paste") (r ">=1") (d #t) (k 0)) (d (n "quickcheck") (r ">=1") (d #t) (k 0)) (d (n "same-as") (r ">=1") (d #t) (k 0)))) (h "0cjsc14rk6virggg5hf9zn0k27vnyyaalibigai7pca92cpb6rjj") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-rsmonad-0.2.4 (c (n "rsmonad") (v "0.2.4") (d (list (d (n "ahash") (r ">=0.8.3") (d #t) (k 0)) (d (n "derive-quickcheck") (r ">=0.1.1") (d #t) (k 0)) (d (n "paste") (r ">=1") (d #t) (k 0)) (d (n "quickcheck") (r ">=1") (d #t) (k 0)) (d (n "same-as") (r ">=1") (d #t) (k 0)))) (h "079gl5xi0cm1ndxhsy15hkclh1ky965vsl3smmg8l7n4rdlfgal8") (f (quote (("std") ("nightly") ("default" "std"))))))

