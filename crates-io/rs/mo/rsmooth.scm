(define-module (crates-io rs mo rsmooth) #:use-module (crates-io))

(define-public crate-rsmooth-0.3.0 (c (n "rsmooth") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)))) (h "0xabm4gw3p6qdfb65i82wm86j6rj06rj72pfhdnppy82xsskbgcn")))

(define-public crate-rsmooth-0.3.1 (c (n "rsmooth") (v "0.3.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)))) (h "0qbdlbbmd2w7yyjgwsg4whdpdbi5prjpf36gn4a81cd09b1p7665")))

