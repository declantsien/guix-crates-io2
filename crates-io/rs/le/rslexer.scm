(define-module (crates-io rs le rslexer) #:use-module (crates-io))

(define-public crate-rslexer-1.0.0 (c (n "rslexer") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0p47hy8bz3lcan0l44g7r4rvxry3jlwffzfxpc6sqrxjgzbmcqbv") (y #t)))

(define-public crate-rslexer-1.0.1 (c (n "rslexer") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pw1jg890q5c39d82jwbdz4n8a1jqshz2zkddcyby6blqfgz0n6j") (y #t)))

(define-public crate-rslexer-1.0.2 (c (n "rslexer") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07z6dqm7vdnihwjzz8gcg3lc4j3bkmbyvmkqmbfmplm2hvriw8gb") (y #t)))

