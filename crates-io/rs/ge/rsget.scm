(define-module (crates-io rs ge rsget) #:use-module (crates-io))

(define-public crate-rsget-0.1.0 (c (n "rsget") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsget_lib") (r "^0.1.0") (d #t) (k 0)))) (h "1nwqmz1d8aw6k7n72mnq3xlc1lhzsf3hfhpjzqqqxzb99s0b5mfz")))

(define-public crate-rsget-0.1.1 (c (n "rsget") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsget_lib") (r "^0.1.1") (d #t) (k 0)))) (h "1bn9fw8gq85war64xvccq1vasvx5pmqkbbn2wmmkqar2k6wkmb8h")))

(define-public crate-rsget-0.1.2 (c (n "rsget") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsget_lib") (r "^0.1.2") (d #t) (k 0)))) (h "1iqhfi9x353qw0i7adw47pnrb70vzk7bryjmml0ma4yq2kr2156d")))

(define-public crate-rsget-0.1.3 (c (n "rsget") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsget_lib") (r "^0.1.2") (d #t) (k 0)))) (h "0z0a60h5c85i9z8lpf98jwyri9h7qqvmjl1dg7bz9cdfj1igxslj")))

(define-public crate-rsget-0.1.4 (c (n "rsget") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsget_lib") (r "^0.2.0") (d #t) (k 0)))) (h "15f309a1p01ph028aw1qrc344sp262iavzrmpy81dsld2xfm3yll")))

(define-public crate-rsget-0.1.5 (c (n "rsget") (v "0.1.5") (d (list (d (n "flexi_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsget_lib") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "196dp8x741y0zqiqmb5kpz8lg4ix6lw2wiw2mw4xwfh37r5miapd")))

