(define-module (crates-io rs ge rsgen) #:use-module (crates-io))

(define-public crate-rsgen-0.1.0 (c (n "rsgen") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)))) (h "0qpbrjcwjimrbpp0nzj9ad37vb3di7g3b2j4m3kpzkjv0iv2hfsa")))

(define-public crate-rsgen-0.1.1 (c (n "rsgen") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)))) (h "0fl4ks57a7l592dy6i37j2kp5rhziwvf7kk85cv06557kizc1b1m")))

(define-public crate-rsgen-0.1.2 (c (n "rsgen") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)))) (h "0p6h3q2dyyn9mdfqx48rxk6jsq4497lqxvf9ni2zyri55cq4b5d8")))

(define-public crate-rsgen-0.1.3 (c (n "rsgen") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)))) (h "1mhsy4zw7pxsr2k36pgaz7z9a12775hilqbvzlfb0c857sn6k4hy")))

(define-public crate-rsgen-0.2.0 (c (n "rsgen") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)))) (h "13nipgz3x3cy95ivc4di9rchskiqqw8j2h68j4pf8ff5gm2lsmrf")))

