(define-module (crates-io rs ft rsftch) #:use-module (crates-io))

(define-public crate-rsftch-0.1.0 (c (n "rsftch") (v "0.1.0") (d (list (d (n "cargo-watch") (r "^8.5.2") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1w8m6ys3imjprxq168aayz11j9lxpa72mwgf26vfwk4i53jqnc5m")))

(define-public crate-rsftch-0.1.1 (c (n "rsftch") (v "0.1.1") (d (list (d (n "cargo-watch") (r "^8.5.2") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "029sn0hhqr14xd33xprc8j9cilk8n9zackxz54hjiajmkw4l22qq")))

(define-public crate-rsftch-0.1.2 (c (n "rsftch") (v "0.1.2") (d (list (d (n "cargo-watch") (r "^8.5.2") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "085w660arg0kp75ammgxdq2bpinjdvifvb5balzgxrwradwk5c9n")))

(define-public crate-rsftch-0.1.3 (c (n "rsftch") (v "0.1.3") (d (list (d (n "cargo-watch") (r "^8.5.2") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0dr4jrb938jd6zg8h6hp5z5560z5kyzl2gffp3c24yb50yliaids")))

(define-public crate-rsftch-0.1.4 (c (n "rsftch") (v "0.1.4") (d (list (d (n "cargo-watch") (r "^8.5.2") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1fh7h7c6l6lbmc43spaj2gzb0wj5yjhpfjrwy49s3yprlwpym31v")))

(define-public crate-rsftch-0.1.5 (c (n "rsftch") (v "0.1.5") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "03ib27617zshnai1hwrm1kwsipvsycb6bny7fsm1r53qbad4gp6l")))

(define-public crate-rsftch-0.1.6 (c (n "rsftch") (v "0.1.6") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "19dgfv0h6pfcbl5yp45ph5svjc7nfm51r9858797jvx58y5phmi6")))

(define-public crate-rsftch-0.1.7 (c (n "rsftch") (v "0.1.7") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.7") (d #t) (k 0)))) (h "0ws8zlg6xdimh93nqfgs2mg9zv5yxbncnjxk18kfrfgmqqrhf6xr")))

(define-public crate-rsftch-0.1.8 (c (n "rsftch") (v "0.1.8") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.7") (d #t) (k 0)))) (h "0g856rwv0g1srbdy9xfg6pjmdryziajal2bblg9yr4qbv2nxdnqj")))

(define-public crate-rsftch-0.2.0 (c (n "rsftch") (v "0.2.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.7") (d #t) (k 0)))) (h "02vbrlwjq3qlsqap4jwcx0kadk44v9r07961f1mpmpsagyqxl0rw")))

(define-public crate-rsftch-0.2.1 (c (n "rsftch") (v "0.2.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.7") (d #t) (k 0)))) (h "0rd6q03kgchmck0wcf4wa4b7jzdvqg6dqcmnkb4nafn894h50a2j")))

(define-public crate-rsftch-0.2.2 (c (n "rsftch") (v "0.2.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.7") (d #t) (k 0)))) (h "0d1vk39gcspa0qsx6wsm4r4ll94npfh34s71w2gb1f7zsy1p66cl")))

(define-public crate-rsftch-0.2.3 (c (n "rsftch") (v "0.2.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.7") (d #t) (k 0)))) (h "0bbyw1fmpirdrjgcbcqlm1y8pjx88w457894b7ld7hmpjh552254")))

(define-public crate-rsftch-0.2.4 (c (n "rsftch") (v "0.2.4") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "1psvcbirgcvasdic7r7ib113sw5zrvg35y5v9j3s1n9l4s9daagd")))

(define-public crate-rsftch-0.2.5 (c (n "rsftch") (v "0.2.5") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "0qrrfqlpv6ria5k3n07g5qimfg4xr46fa9lll18y8775kxkfg33q")))

(define-public crate-rsftch-0.2.6 (c (n "rsftch") (v "0.2.6") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "0y48i62169c1raranh8yzx4dif68bw3hk7wkc3fg3v42yq34zgy3")))

(define-public crate-rsftch-0.2.7 (c (n "rsftch") (v "0.2.7") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "0hrs01plfaaz41s9gm42qppwjmdqzhczyvy948k4iccaprllxwjv")))

(define-public crate-rsftch-0.2.8 (c (n "rsftch") (v "0.2.8") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "0hn18l7w7mw23l0zmb8wx5wwjgkb1ndcm2511a6bv4nj9vv9mahp")))

(define-public crate-rsftch-0.2.9 (c (n "rsftch") (v "0.2.9") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "0cln5791fi6ksrl65zcszcp0zk5w8b6n22369xdnzq6i5qi8gzqp")))

(define-public crate-rsftch-0.3.0 (c (n "rsftch") (v "0.3.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "1gasqwkf2pa5bc8mf9pn4pm3cc16w13bvlwhssiq0r35daxnrbb9")))

(define-public crate-rsftch-0.3.1 (c (n "rsftch") (v "0.3.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "0qfvjqh0fl9zv71i6lpnl5gancnqdp3sy6vbdmp64jw2kmjvrc3g")))

(define-public crate-rsftch-0.3.2 (c (n "rsftch") (v "0.3.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "1i06jgqcgfdqv69hfkzhd5ygiy7rh7kmckadq10jy3wpgkq0kllx")))

(define-public crate-rsftch-0.3.3 (c (n "rsftch") (v "0.3.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "13jmwz45nf4g9irnjy892vgvk4hcs26fdkd935f35d65lrh9fy6m")))

(define-public crate-rsftch-0.3.4 (c (n "rsftch") (v "0.3.4") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "0hp3i2q0jzdgmpvmkpn2qyg3cd5lamnqw7xphrqa14y1a84adpm5")))

(define-public crate-rsftch-0.3.5 (c (n "rsftch") (v "0.3.5") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)))) (h "04cbqlq4sx4jwr9d8ip2s4v8rg9l1cg1c1kmsg0jixyn3x81g875")))

(define-public crate-rsftch-0.3.6 (c (n "rsftch") (v "0.3.6") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)))) (h "1q06fdnh5293xyk92v3bl7x9j9ilrplib76mwylgj5qz1bx9dh9c")))

(define-public crate-rsftch-0.3.7 (c (n "rsftch") (v "0.3.7") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)))) (h "0xlchx8kyb77gjigx71md6qqmywj7lp89vzwx6pbbln8clv8sy2i")))

(define-public crate-rsftch-0.3.8 (c (n "rsftch") (v "0.3.8") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)))) (h "0cs94bfxz5ahzh327vqfnaqfsvn2ib6kk0ljpmxrcpskzv82lhpv")))

(define-public crate-rsftch-0.3.9 (c (n "rsftch") (v "0.3.9") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)))) (h "1v3ay1jbpxsrg8kbxj9slp8dgdn0sn4myl81z42a8f3b3nm03dy0")))

(define-public crate-rsftch-0.4.1 (c (n "rsftch") (v "0.4.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)))) (h "13y366jvzjx8fh2lsp1gsja1kh26jxsldl0hbwf9z601l8cvbldj")))

(define-public crate-rsftch-0.4.2 (c (n "rsftch") (v "0.4.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)))) (h "1mskj9kihzxmcc381y7bvxvsfwgyaqw96n0j09y7vd5qnb86ssdp")))

(define-public crate-rsftch-0.4.3 (c (n "rsftch") (v "0.4.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)))) (h "1rp6mxprmhyhqid2cixmyingnyvm683b1iflsr59027j9q7fn7nz")))

(define-public crate-rsftch-0.4.4 (c (n "rsftch") (v "0.4.4") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)))) (h "1c5gkzwrkyhfn2zs96knd7dq5cn2v487nyrr27v2bp3r3803apdk")))

(define-public crate-rsftch-0.4.5 (c (n "rsftch") (v "0.4.5") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)))) (h "0bnynyszj6dn103fkdn6a2mj8pg5ayizksgaqm07455bbia95aig")))

(define-public crate-rsftch-0.5.0 (c (n "rsftch") (v "0.5.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1znswnrr7g1fb1s8vycfsa3rhnz3cr0billqfwkds0d7sz7aw1hy")))

(define-public crate-rsftch-0.5.1 (c (n "rsftch") (v "0.5.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1sljc3hbyyy0783j0qp763fyx0yd83swfzn8ypqz5sc33k24j596")))

(define-public crate-rsftch-0.5.2 (c (n "rsftch") (v "0.5.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1jc779cdp5fpi9cvp3sfa5r9cb4cbwmp9vh8m3srd7bvpd779rxv")))

(define-public crate-rsftch-0.5.3 (c (n "rsftch") (v "0.5.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1z9q7mzq0w4pahaz10nh1mwls6qcpjfrwpix0icbc8qvx6zv2141")))

(define-public crate-rsftch-0.5.4 (c (n "rsftch") (v "0.5.4") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "027gj4iyzsbkhp8mq2gjr283qxc0rlgsmjx1w0cy5w67xlqcigi5")))

(define-public crate-rsftch-0.5.5 (c (n "rsftch") (v "0.5.5") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0a1y12yp79n09lzln3741r3vl66vzck4ldgw8yiivpzdfz54aamq")))

(define-public crate-rsftch-0.5.6 (c (n "rsftch") (v "0.5.6") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0ldsbfxykjn5a4lhldcknx57zydfz13zqgfvgq32a1fy6rgfk6gf")))

(define-public crate-rsftch-0.5.7 (c (n "rsftch") (v "0.5.7") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "07ilpi8pk3dhm6xlvq8bklmz0nqwi7m4q02196ywac2jbn90pqcx")))

(define-public crate-rsftch-0.5.8 (c (n "rsftch") (v "0.5.8") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1igr2gza0h458g53pr7z64kn34y868261ahrajmvlzi1ri52xkmq")))

(define-public crate-rsftch-0.5.9 (c (n "rsftch") (v "0.5.9") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1f3n8iqkzbpbr35b2z3pqfnglqpd8ajk88gk6nj6wxgypkk9k6wz")))

(define-public crate-rsftch-0.6.0 (c (n "rsftch") (v "0.6.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libmacchina") (r "^7.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1c581mam33nfv4xgqbf2ly31y4j10w6fakw8pdb7kdp593xc3y8b")))

(define-public crate-rsftch-0.6.1 (c (n "rsftch") (v "0.6.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.155") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1c9k8y1xi545308bvam48d61rgwhxlc58vbkspp7rzp99qwrch8n")))

(define-public crate-rsftch-0.6.2 (c (n "rsftch") (v "0.6.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.155") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0w3wv1p3j61w90vg7m6i5c0vmkvj9akcs57izvqrhwb3rmxgln0a")))

(define-public crate-rsftch-0.6.3 (c (n "rsftch") (v "0.6.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.155") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1jkx5jnqvk9i742sjax4ng5x5qda12i7w32lgjgrli0412x5kf90")))

