(define-module (crates-io rs #{10}# rs1090-python) #:use-module (crates-io))

(define-public crate-rs1090-python-0.1.2 (c (n "rs1090-python") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "rs1090") (r "^0.1.2") (d #t) (k 0)))) (h "15jgp25p8i9l5if21h07byxr541pg8v3ya56jf8p1yych0ra7fn1")))

(define-public crate-rs1090-python-0.1.3 (c (n "rs1090-python") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "rs1090") (r "^0.1.3") (d #t) (k 0)) (d (n "serde-pickle") (r "^1.1.1") (d #t) (k 0)))) (h "0ky2q3s62nqgvlfa2qj3slksnmkf2zqv6pfrz0za36s9hz8iq3ci")))

(define-public crate-rs1090-python-0.2.0 (c (n "rs1090-python") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "rs1090") (r "^0.2.0") (d #t) (k 0)) (d (n "serde-pickle") (r "^1.1.1") (d #t) (k 0)))) (h "1kdk4ab3avsix9aw8zkywg8vzrf4s00g1jzyazmdk6flj1w95m1n")))

(define-public crate-rs1090-python-0.2.1 (c (n "rs1090-python") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rs1090") (r "^0.2.1") (d #t) (k 0)) (d (n "serde-pickle") (r "^1.1.1") (d #t) (k 0)))) (h "0h9dcifrq2q18ik4nvh0f6vdyz9vlym0s1kc0cvw9bllj7vpzv3b")))

(define-public crate-rs1090-python-0.2.2 (c (n "rs1090-python") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rs1090") (r "^0.2.2") (d #t) (k 0)) (d (n "serde-pickle") (r "^1.1.1") (d #t) (k 0)))) (h "02dx26x0cd03m8xavqnqi6rbwdxy2r7vnbdw0k99n7x9xjzx41ax")))

