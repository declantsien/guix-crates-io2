(define-module (crates-io rs as rsass-macros) #:use-module (crates-io))

(define-public crate-rsass-macros-0.27.0 (c (n "rsass-macros") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "rsass") (r "^0.27.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.106") (d #t) (k 0)))) (h "0hjrjd658lx51y2zp427fkdg309np0vby5j33h5jya1ay8x9c1az") (r "1.60.0")))

(define-public crate-rsass-macros-0.28.0 (c (n "rsass-macros") (v "0.28.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "rsass") (r "^0.28.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "0l8qglh5zhgazv97dl579ddi4fcpqb6pzlvsg461m2ha2cgr284j") (r "1.60.0")))

(define-public crate-rsass-macros-0.28.8 (c (n "rsass-macros") (v "0.28.8") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "rsass") (r "^0.28.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "0j5pry67rxbxwi1rwclzqv0kv18d5732g2npsb2vp41jw7mc9q4v") (r "1.61.0")))

(define-public crate-rsass-macros-0.28.10 (c (n "rsass-macros") (v "0.28.10") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "rsass") (r "^0.28.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "124c66sikf3kisl3yac84kgrwm3n81gyn28n9shrkvhl0g7ih22m") (r "1.61.0")))

