(define-module (crates-io rs as rsass-cli) #:use-module (crates-io))

(define-public crate-rsass-cli-0.27.0 (c (n "rsass-cli") (v "0.27.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rsass") (r "^0.27.0") (d #t) (k 0)))) (h "0cs2r4lmghks5bnn68zjvinb6w6f81c86mmgl6r9c9nwybhkjyd3") (f (quote (("unimplemented_args")))) (r "1.64.0")))

(define-public crate-rsass-cli-0.28.0 (c (n "rsass-cli") (v "0.28.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rsass") (r "^0.28.0") (d #t) (k 0)))) (h "06grwr1zna184q7paac1psd2lzlznnpg2clf3bv3140l3dhjq9cd") (f (quote (("unimplemented_args")))) (r "1.64.0")))

(define-public crate-rsass-cli-0.28.8 (c (n "rsass-cli") (v "0.28.8") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rsass") (r "^0.28.8") (d #t) (k 0)))) (h "051csiq5gvdm9icdvvf10gk7100sgk242g63fbr92r26w4pknb4m") (f (quote (("unimplemented_args")))) (r "1.70.0")))

(define-public crate-rsass-cli-0.28.10 (c (n "rsass-cli") (v "0.28.10") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rsass") (r "^0.28.10") (d #t) (k 0)))) (h "1pi4rx236df33ngxx0nc2rglsavpsqbxb07cbzk5ji2x0w3mg7cz") (f (quote (("unimplemented_args")))) (r "1.74.0")))

