(define-module (crates-io rs ql rsql_builder) #:use-module (crates-io))

(define-public crate-rsql_builder-0.1.0 (c (n "rsql_builder") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vhk3mw53w0jh35s9j79n37b0cy9rdq1r3mm0d1r9hfcrm5fkcxf")))

(define-public crate-rsql_builder-0.1.1 (c (n "rsql_builder") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19bjah37wfrx0044y0ljhlygwl330n0ysryb8l71ryyqg5650fwy")))

(define-public crate-rsql_builder-0.1.2 (c (n "rsql_builder") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m93ib9mp454y1hh78icsllpmn8g60mvassi2dnkkvrwi095ggcx")))

(define-public crate-rsql_builder-0.1.3 (c (n "rsql_builder") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xyahgia7vbj69d7sfpzd76q3cln8n4rs46f1wlmafnjrbbc8f1m")))

(define-public crate-rsql_builder-0.1.4 (c (n "rsql_builder") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1my77r2nfywxfxzmrmxpg9hv04r0gwm1s84m5j9d255xdw8k58b9")))

(define-public crate-rsql_builder-0.1.5 (c (n "rsql_builder") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0snlf1520z3cdx5lk94drl9avn7kn1mhrcddfg1hqvp1riqx73rc")))

