(define-module (crates-io rs ql rsqlite) #:use-module (crates-io))

(define-public crate-rsqlite-0.0.0 (c (n "rsqlite") (v "0.0.0") (h "0rh8v2rn8v5q58zc72wz5rhgk30ypkbkm46vwl6cai1pp6p96p1w")))

(define-public crate-rsqlite-1.0.0 (c (n "rsqlite") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vn730jnfd9g6ins6sng7zjjinak29656n761a32d5zs408s4kzv")))

