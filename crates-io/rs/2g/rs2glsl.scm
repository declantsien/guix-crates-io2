(define-module (crates-io rs #{2g}# rs2glsl) #:use-module (crates-io))

(define-public crate-rs2glsl-0.1.0 (c (n "rs2glsl") (v "0.1.0") (d (list (d (n "rs2glsl-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1wspg85xhy0l7696vwlxpsjxsq65f41grjzxkr51kr35rlxihi01")))

(define-public crate-rs2glsl-0.2.0 (c (n "rs2glsl") (v "0.2.0") (d (list (d (n "rs2glsl-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0vly3m6l1m287wcd7pi5ryx88hk0igc71kzibi82lhswh8zrvgmq")))

(define-public crate-rs2glsl-0.2.1 (c (n "rs2glsl") (v "0.2.1") (d (list (d (n "rs2glsl-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0wzy47vvxbi5kyfv5avnmchwg9icmdvxlhgwb0kmp9qnasd3glcn")))

(define-public crate-rs2glsl-0.3.0 (c (n "rs2glsl") (v "0.3.0") (d (list (d (n "rs2glsl-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1d6z4b9cs79cbm966hi1523qcv0wda4fkyp0fclcxgsa63d3587i") (f (quote (("default" "GLSL_450") ("GLSL_450") ("GLSL_440") ("GLSL_430") ("GLSL_420") ("GLSL_410") ("GLSL_400") ("GLSL_330") ("GLSL_150") ("GLSL_140") ("GLSL_130") ("GLSL_120") ("GLSL_110"))))))

(define-public crate-rs2glsl-0.3.1 (c (n "rs2glsl") (v "0.3.1") (d (list (d (n "rs2glsl-macros") (r "^0.3.1") (d #t) (k 0)))) (h "1cz1inq0cwd7h0v6icwzxpaqlasp06rbcmvmnafzmk05njyl2d9j") (f (quote (("default" "GLSL_450") ("GLSL_450") ("GLSL_440") ("GLSL_430") ("GLSL_420") ("GLSL_410") ("GLSL_400") ("GLSL_330") ("GLSL_150") ("GLSL_140") ("GLSL_130") ("GLSL_120") ("GLSL_110"))))))

(define-public crate-rs2glsl-0.3.3 (c (n "rs2glsl") (v "0.3.3") (d (list (d (n "rs2glsl-macros") (r "^0.3.3") (d #t) (k 0)))) (h "0j02nd3hz9nvvq6rlz22lh65n98rpk9j2hz4dns9v5vci9f6hf0h") (f (quote (("default" "GLSL_450") ("GLSL_450") ("GLSL_440") ("GLSL_430") ("GLSL_420") ("GLSL_410") ("GLSL_400") ("GLSL_330") ("GLSL_150") ("GLSL_140") ("GLSL_130") ("GLSL_120") ("GLSL_110"))))))

(define-public crate-rs2glsl-0.3.4 (c (n "rs2glsl") (v "0.3.4") (d (list (d (n "rs2glsl-macros") (r "^0.3.4") (d #t) (k 0)))) (h "0piix7bbdnsmavcz42wi3lrh651zw97hqikh7gs9gms5j0q9ncdh") (f (quote (("default" "GLSL_450") ("GLSL_450") ("GLSL_440") ("GLSL_430") ("GLSL_420") ("GLSL_410") ("GLSL_400") ("GLSL_330") ("GLSL_150") ("GLSL_140") ("GLSL_130") ("GLSL_120") ("GLSL_110"))))))

