(define-module (crates-io rs #{2g}# rs2glsl-macros) #:use-module (crates-io))

(define-public crate-rs2glsl-macros-0.1.0 (c (n "rs2glsl-macros") (v "0.1.0") (d (list (d (n "cartesian") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1w8i8hnisj1ngw8xlfxfp7jmc2fk89ad06zdpd5dcn5axxhkdy4y")))

(define-public crate-rs2glsl-macros-0.2.0 (c (n "rs2glsl-macros") (v "0.2.0") (d (list (d (n "cartesian") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "09s12k9clzd16q7kzbyw7k32yfx2cswak7pbfd79agkh5vdk2w1n")))

(define-public crate-rs2glsl-macros-0.3.0 (c (n "rs2glsl-macros") (v "0.3.0") (d (list (d (n "cartesian") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0cwpk0a2xfifzsvlgx142bgsl7vzg4aglspxyp83cmq1s7k7b8r4")))

(define-public crate-rs2glsl-macros-0.3.1 (c (n "rs2glsl-macros") (v "0.3.1") (d (list (d (n "cartesian") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1xn5fd5p1hf50608nkb0ri47z0655g9dki9h82liwbcbad7mshn7")))

(define-public crate-rs2glsl-macros-0.3.2 (c (n "rs2glsl-macros") (v "0.3.2") (d (list (d (n "cartesian") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0mnk3bpwqbb0wp382k0p5h2vdjnikqx3pjjavdzskknq2xg37wwd")))

(define-public crate-rs2glsl-macros-0.3.3 (c (n "rs2glsl-macros") (v "0.3.3") (d (list (d (n "cartesian") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "10w6g958nlns0w9w5d6jyw724pz53h7fwznppvwin2xvr796xkj8")))

(define-public crate-rs2glsl-macros-0.3.4 (c (n "rs2glsl-macros") (v "0.3.4") (d (list (d (n "cartesian") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0mq57f32mhiy6vh0jyxixgdfzhp91mhbz6gvwfp079pvajiim5xv")))

