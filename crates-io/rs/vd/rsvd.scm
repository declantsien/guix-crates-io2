(define-module (crates-io rs vd rsvd) #:use-module (crates-io))

(define-public crate-rsvd-0.1.1 (c (n "rsvd") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-static"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1f0lip68s2ay6xqq561xn4ibrwa9mxlsyifsg8cvj758ikrgygjh")))

(define-public crate-rsvd-0.1.2 (c (n "rsvd") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-static"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1rrfknv4pjw1z65fmfsfhd7rp9f6n3l5rk7s1a950c9z852wrfjr")))

