(define-module (crates-io rs _a rs_aes) #:use-module (crates-io))

(define-public crate-rs_aes-0.1.0 (c (n "rs_aes") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "05gx5azidzn0kckvvjjwc06707l89nn7m7qkq9ybkkgpabx65rz0")))

(define-public crate-rs_aes-0.1.1 (c (n "rs_aes") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0ysdhq9p80189wbr39866vsl73h47ydk2p6sg7lgj1l9qjvkxinv")))

(define-public crate-rs_aes-0.1.2 (c (n "rs_aes") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "148653yqsap4fb1p27c3jani5gnzihd6f978pb8kshs9iwv19q1g")))

