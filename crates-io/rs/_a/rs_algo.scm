(define-module (crates-io rs _a rs_algo) #:use-module (crates-io))

(define-public crate-rs_algo-0.1.0 (c (n "rs_algo") (v "0.1.0") (h "1rq7m020cj5mp3h34f2fzk47dhfaf8v3fihv9jwf8hw7f6jckgzk")))

(define-public crate-rs_algo-0.1.1 (c (n "rs_algo") (v "0.1.1") (h "1a0mg3f62z6in33b4xm6w9pl7w85f385f3pcdj3fbrjygpn00ijs")))

(define-public crate-rs_algo-0.1.2 (c (n "rs_algo") (v "0.1.2") (h "0qn424mx51v5ppahg3dp7q28x7swf3j9gr1s7cgp40hapm45w71b")))

(define-public crate-rs_algo-0.1.3 (c (n "rs_algo") (v "0.1.3") (h "1b5wrj62qxs5rrg2xs4kgrj8zih294nqpajv0habg02fbhh5mcrd")))

(define-public crate-rs_algo-0.1.4 (c (n "rs_algo") (v "0.1.4") (h "04j4gbv0zmjc0i180swp6i5yqdyazbcpv9ygjwn4y63wnwa3dp8m")))

(define-public crate-rs_algo-0.1.5 (c (n "rs_algo") (v "0.1.5") (h "08hxjxvnxkpwd9833drfjvzcvd66543rbcqv4izzrw0b9ck3dz3q")))

(define-public crate-rs_algo-0.1.6 (c (n "rs_algo") (v "0.1.6") (h "0iwchl12vsw6amwhhwyj9n8g6kwf9ng6hhivdw2pq07w6s4zf5vr")))

(define-public crate-rs_algo-0.1.7 (c (n "rs_algo") (v "0.1.7") (h "1z80c8xfvbr18rw7nxs3xs2paxmr718w27kssrrg9vi5vvmq6b2g")))

(define-public crate-rs_algo-0.2.0 (c (n "rs_algo") (v "0.2.0") (h "0kpg08k2h8j0d3zsavsgaycbjiw8iik5z8h4qdnfa3iqpl1k5g04")))

(define-public crate-rs_algo-0.2.1 (c (n "rs_algo") (v "0.2.1") (h "0ql390bvcfw46a2hb9b71228gbwlbbcva2404fmcdxnx28c84379")))

