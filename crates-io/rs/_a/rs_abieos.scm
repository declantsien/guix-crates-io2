(define-module (crates-io rs _a rs_abieos) #:use-module (crates-io))

(define-public crate-rs_abieos-0.1.1 (c (n "rs_abieos") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 1)))) (h "1k3mxb8ddsd09k3s7b1xvr7g83h4j8a7qglbrnqbvd5qgvfb2pqw") (y #t)))

(define-public crate-rs_abieos-0.1.2 (c (n "rs_abieos") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "13nkvzla6cc73cjgl5552nih1xqrg4wlmfdn8348wryhj4zhrlhd") (y #t)))

(define-public crate-rs_abieos-0.1.3 (c (n "rs_abieos") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 1)))) (h "188xrr5dkxlvy5la4px2na11k9q2715082bpj4lvr89hsjxspg47") (y #t)))

(define-public crate-rs_abieos-0.1.4 (c (n "rs_abieos") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 1)))) (h "02zis1hsfj3zz2nrgh8nlh2gay0c7yg1hr538qjdjipq0i3c60p2")))

(define-public crate-rs_abieos-0.1.5 (c (n "rs_abieos") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 1)))) (h "11b3nd297r7z1z52k2zd902s262136cwxhnqlwwk27zqgjp0qhx0")))

