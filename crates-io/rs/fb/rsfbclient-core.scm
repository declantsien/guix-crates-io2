(define-module (crates-io rs fb rsfbclient-core) #:use-module (crates-io))

(define-public crate-rsfbclient-core-0.5.0 (c (n "rsfbclient-core") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "0q2km5bv5cizsd8yfkx92s8y0rh1nganpx6h9fcsabqf3g7ci3ms") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.5.1 (c (n "rsfbclient-core") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "0lb4s2c1cv27r2yfbdxns2c7ds79y6m79pm3lg2a2rcdql480cdi") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.6.0 (c (n "rsfbclient-core") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "0d1zp1wlxxg437mvw4km8kq56dzc6nk09i231k7rzf6q5rslbgfr") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.7.0 (c (n "rsfbclient-core") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "14ib8j51xhyai7sb43jrman22x38s1qkl335zh3rby4mr046grs2") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.8.0 (c (n "rsfbclient-core") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1kr9n5bdfwxwsarlchrhm7294iyyr7v6yjcrrvk4bk4ma1m7sda5") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.9.0 (c (n "rsfbclient-core") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0fhxqlnr7q2mvn7l61kbrn8g579a2718kpc7bi1dddp0qwrb3kr1") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.10.0 (c (n "rsfbclient-core") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0wc5pxqwia8f3m6rrh3di45yhd6gfs37sr9aah5ngkfsilhhi3yc") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.11.0 (c (n "rsfbclient-core") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1v1a1q0qp0b9gm4zsjqd3kq13jk9d0lfsyidj5d5a7c7j1bx5629") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.12.0 (c (n "rsfbclient-core") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "00p2xsjvh2c6qrdhizbynl4r0jwbkxx1g7h1l3r9dslbxy443q72") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.13.0 (c (n "rsfbclient-core") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1a695mxwjy92if980aaklbkb9jmsnhmpv26r9vykgzrj3gkdf34p") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.14.0 (c (n "rsfbclient-core") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1f4vypn2lmm6i40p1wplvnmj9icmvh3nvdyx33hia142f4wlg8fq") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.15.0 (c (n "rsfbclient-core") (v "0.15.0") (d (list (d (n "chrono") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1njdnn20bs7mnr3pjxyac6ywzca885d4ag53jgdss2cf261272pj") (f (quote (("date_time" "chrono"))))))

(define-public crate-rsfbclient-core-0.16.0 (c (n "rsfbclient-core") (v "0.16.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "10zmbki6m0gx15c2mmx9dyvlmcpd7v9jikp4w9p71lahwnkya95g")))

(define-public crate-rsfbclient-core-0.17.0 (c (n "rsfbclient-core") (v "0.17.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0jdva81fqk4y2ichcnas1mkr7iw81f4xc3pa47l2pn4z002nsa3j")))

(define-public crate-rsfbclient-core-0.18.0 (c (n "rsfbclient-core") (v "0.18.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock" "std"))) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "184zb61y3s63kjl11lirb09rj78yn6h4zaqjc27nyg6c64d1p9qc")))

(define-public crate-rsfbclient-core-0.19.0 (c (n "rsfbclient-core") (v "0.19.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock" "std"))) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "10lggxm0pqn2z100x32ri1kq49jmwm7s7gfncyx943rl0b27z7z8")))

(define-public crate-rsfbclient-core-0.20.0 (c (n "rsfbclient-core") (v "0.20.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock" "std"))) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0687qpikcza65rmkr936rrcvhi6cvmw15bbl7fqq3fdn35qa0f2p")))

(define-public crate-rsfbclient-core-0.21.0 (c (n "rsfbclient-core") (v "0.21.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock" "std"))) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0rzlsklvbca3nsr2q89v0xc9z75c2a5xhpgqsv8knh4dgibw3x8g")))

(define-public crate-rsfbclient-core-0.22.0 (c (n "rsfbclient-core") (v "0.22.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock" "std"))) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0cj8h0n01i2cfbfyagrspgz0wj55a66p8jg4i4lgywyvh5p98n5r")))

(define-public crate-rsfbclient-core-0.23.0 (c (n "rsfbclient-core") (v "0.23.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock" "std"))) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1qd2qawi12csjci113islranv9wz83fcc607visrq7jkig2lg347")))

(define-public crate-rsfbclient-core-0.23.1 (c (n "rsfbclient-core") (v "0.23.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock" "std"))) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0qmac7yayvwb6i5cyf6dkfcq4rjlk70y320641c3nkd66bhmfg83")))

(define-public crate-rsfbclient-core-0.23.2 (c (n "rsfbclient-core") (v "0.23.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock" "std"))) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "16bq751la02wq5zn19wgx1ch6wr3ciwvnh28yiq6ik1nw70s3c2j")))

(define-public crate-rsfbclient-core-0.24.0 (c (n "rsfbclient-core") (v "0.24.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock" "std"))) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1i93b0f482ijam29215p7fnsgh1917p7fxl4lfrx6giadn3x2z54")))

