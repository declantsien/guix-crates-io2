(define-module (crates-io rs fb rsfbclient-derive) #:use-module (crates-io))

(define-public crate-rsfbclient-derive-0.12.0 (c (n "rsfbclient-derive") (v "0.12.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dsml4wavvqifl8064dlazrx17b6cr3bm16n7ama0vn3xy36c31s")))

(define-public crate-rsfbclient-derive-0.13.0 (c (n "rsfbclient-derive") (v "0.13.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hl5509k218w9gslf0cc76djail07id9agl6lbjzg63xd6ikz1ah")))

(define-public crate-rsfbclient-derive-0.14.0 (c (n "rsfbclient-derive") (v "0.14.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z6mkq3aziq5i8wy379ya74bygnryi65n778i4skrvqqn0ih7y4y")))

(define-public crate-rsfbclient-derive-0.15.0 (c (n "rsfbclient-derive") (v "0.15.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zqwbssk5nrqlm23zdykp1wljx0pp6lix47mqwmrcgrkg1sa38k9")))

(define-public crate-rsfbclient-derive-0.16.0 (c (n "rsfbclient-derive") (v "0.16.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "1kzgxbwaf19lr47av9dd1rwhm4ly4xbvrdxkkbjpwc7hi25v0z6a")))

(define-public crate-rsfbclient-derive-0.17.0 (c (n "rsfbclient-derive") (v "0.17.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "0v95wib10y3gn17vkf4pbxiqjravy94y8pglyk6jhzfmshc5pwcs")))

(define-public crate-rsfbclient-derive-0.18.0 (c (n "rsfbclient-derive") (v "0.18.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "02fpxywwy7andw317al59za0wy4cbdbw1fir9qjv2hqaxyjsiyz5")))

(define-public crate-rsfbclient-derive-0.19.0 (c (n "rsfbclient-derive") (v "0.19.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "1kkkp0ilwy0n8wi4cir6jyywrkcr9ylxf7i43j7g3g0kyfxi7vn5")))

(define-public crate-rsfbclient-derive-0.20.0 (c (n "rsfbclient-derive") (v "0.20.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "0wadlxmz3bsd1mw1yapi9f39b3z6b5zjxgpfkzcgs4xyfpcwnmyz")))

(define-public crate-rsfbclient-derive-0.21.0 (c (n "rsfbclient-derive") (v "0.21.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "0b41lh92a3vycs85dan33k83nw9vqd3jx6s5qlaa4vz21939fvks")))

(define-public crate-rsfbclient-derive-0.22.0 (c (n "rsfbclient-derive") (v "0.22.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "1gnn984xdsda9prvf7l8wykfifkbkraasxw2q2p2f11jpbhsrfr7")))

(define-public crate-rsfbclient-derive-0.23.0 (c (n "rsfbclient-derive") (v "0.23.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "1imipvn3wh4km7j6z1fr1caxl837knc3i1ziqmgp47cfwqwaj0n5")))

(define-public crate-rsfbclient-derive-0.23.1 (c (n "rsfbclient-derive") (v "0.23.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "0y9abh38vq5ciddmfcqvcfda5lpcn4c578q42ngpp17jzsaiigih")))

(define-public crate-rsfbclient-derive-0.23.2 (c (n "rsfbclient-derive") (v "0.23.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "0nvsxhmjhv0pqjjvx205bwrzli3drq1mb8h51rdfrx2p60a90hjb")))

(define-public crate-rsfbclient-derive-0.24.0 (c (n "rsfbclient-derive") (v "0.24.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "1w4ybmr33nnqmd291kwzihlbpnzn1pqzvcv7p3wnj1lk1hv3b6xm")))

