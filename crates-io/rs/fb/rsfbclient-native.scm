(define-module (crates-io rs fb rsfbclient-native) #:use-module (crates-io))

(define-public crate-rsfbclient-native-0.5.0 (c (n "rsfbclient-native") (v "0.5.0") (d (list (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.5.0") (d #t) (k 0)))) (h "1wpzz7h8v9mrs1ln60hzxfsaiaf9vgxa32qrv16y8yq801nbq3qf") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.5.1 (c (n "rsfbclient-native") (v "0.5.1") (d (list (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.5.1") (d #t) (k 0)))) (h "1abc6ddhz6xvgwwl0dp4vp72s4i7nq48mk3v3d5hzsjwyvcm9fb9") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.6.0 (c (n "rsfbclient-native") (v "0.6.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.6.0") (d #t) (k 0)))) (h "1wsjp5mgqfgvkmvj8j5wf0pn3383gb6dr251csfpzyv1i2bzj2zw") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.7.0 (c (n "rsfbclient-native") (v "0.7.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.7.0") (d #t) (k 0)))) (h "0g50z4v1giywsncw64qhk7hdqrg9s2s1xly7wk87r2q1myf0grp6") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.8.0 (c (n "rsfbclient-native") (v "0.8.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.8.0") (d #t) (k 0)))) (h "1ffxkirwbaqr8yyrsmzx9zmi5dlrvvwkhccdr3zqnma44d52n1am") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.9.0 (c (n "rsfbclient-native") (v "0.9.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.9.0") (d #t) (k 0)))) (h "1qr3j0ckiyg7nmfihddglagbw59jrszx0f4bvafl2la4xpk60csq") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.10.0 (c (n "rsfbclient-native") (v "0.10.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.10.0") (d #t) (k 0)))) (h "1fzw51mw8dyagnf8h9jz3kllfrfalb1g9f5762y3201ycf4hsmd1") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.11.0 (c (n "rsfbclient-native") (v "0.11.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.11.0") (d #t) (k 0)))) (h "0ak8kgcfi82sd85nlc2yykvmxiknjkw40a53fa0z1irg4faqvkjd") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.12.0 (c (n "rsfbclient-native") (v "0.12.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.12.0") (d #t) (k 0)))) (h "0jlah3vjaz3mg7fb91p37lifiw4zym7zcdb2z9w1dh99f59ng2yx") (f (quote (("linking") ("dynamic_loading" "libloading") ("date_time" "rsfbclient-core/date_time"))))))

(define-public crate-rsfbclient-native-0.13.0 (c (n "rsfbclient-native") (v "0.13.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.13.0") (d #t) (k 0)))) (h "1h99n8m7h3g96nf764gvgb164s1hkwh0b1rgm2p2g7hkdx8nqsza") (f (quote (("linking") ("dynamic_loading" "libloading") ("date_time" "rsfbclient-core/date_time"))))))

(define-public crate-rsfbclient-native-0.14.0 (c (n "rsfbclient-native") (v "0.14.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.14.0") (d #t) (k 0)))) (h "0fvqj1z8f905svniw4w9w849xx52ahrbiaxrgk34i6zwbwvx1q5n") (f (quote (("linking") ("dynamic_loading" "libloading") ("date_time" "rsfbclient-core/date_time"))))))

(define-public crate-rsfbclient-native-0.15.0 (c (n "rsfbclient-native") (v "0.15.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.15.0") (d #t) (k 0)))) (h "1w6ngnwj0jzs591wqqc8yc8gln4jasi87x32qjbb449nw9q7z55v") (f (quote (("linking") ("dynamic_loading" "libloading") ("date_time" "rsfbclient-core/date_time"))))))

(define-public crate-rsfbclient-native-0.16.0 (c (n "rsfbclient-native") (v "0.16.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.16.0") (d #t) (k 0)))) (h "048k6ysdq1ihvqs13wwdy592rxcsf9ys5r6mc8a5jpgsiiz0k7q5") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.17.0 (c (n "rsfbclient-native") (v "0.17.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.17.0") (d #t) (k 0)))) (h "0fkslgmb0dfc2gr99af4y3nab4z9587fy96l27834vd05kw5wmbx") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.18.0 (c (n "rsfbclient-native") (v "0.18.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.18.0") (d #t) (k 0)))) (h "1pn76799ljbch1nwhi80imh75znyb6clbwf8hfcri172kv9ghrgw") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.19.0 (c (n "rsfbclient-native") (v "0.19.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.19.0") (d #t) (k 0)))) (h "0ph4q6c3pxglsjrq64n85q0ncjy0qvry6lfny5pmh7kzw798851k") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.20.0 (c (n "rsfbclient-native") (v "0.20.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.20.0") (d #t) (k 0)))) (h "0l8k3b4z8lgl42ys7yvz8snq8nc7xjkzksfyknpafk844ziz0wfd") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.21.0 (c (n "rsfbclient-native") (v "0.21.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.21.0") (d #t) (k 0)))) (h "05hhh3w6p89r7i9cvzana38hypywv8bzlm7vv4zrgq56lk621j3m") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.22.0 (c (n "rsfbclient-native") (v "0.22.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.22.0") (d #t) (k 0)))) (h "1qwibfyrbvcvmrmyn9a1kywkgndxgr6mai835p8zi2jk1adxf00l") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.23.0 (c (n "rsfbclient-native") (v "0.23.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.23.0") (d #t) (k 0)))) (h "0nyynjdvw7wb3bikrd4mdd9w6r67svm553gysbbm3z90wqry8w3p") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.23.1 (c (n "rsfbclient-native") (v "0.23.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.23.1") (d #t) (k 0)))) (h "10wv8cw3w74v0rwfjhjbngz94a3acli53kwd2f2l3svxx267pw82") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.23.2 (c (n "rsfbclient-native") (v "0.23.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.23.2") (d #t) (k 0)))) (h "11j8phawcnpbvjzhcb4yapjydgyb6lp9f0mskac3hnmwz4k0vb91") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

(define-public crate-rsfbclient-native-0.24.0 (c (n "rsfbclient-native") (v "0.24.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rsfbclient-core") (r "^0.24.0") (d #t) (k 0)))) (h "05ndp20c33gyql9dkdwm11848mmxrzirk7w9y4lfkk566bik0cmb") (f (quote (("linking") ("dynamic_loading" "libloading"))))))

