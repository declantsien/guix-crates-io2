(define-module (crates-io rs -f rs-fsring) #:use-module (crates-io))

(define-public crate-rs-fsring-0.1.0 (c (n "rs-fsring") (v "0.1.0") (h "11vzq44jshbxqyyz596adx4dd6a1xsxjm9z5skhjhyz0id1akk3g")))

(define-public crate-rs-fsring-2.0.0 (c (n "rs-fsring") (v "2.0.0") (h "093r1b0grafq5fl8vf3fakzaggrp4jn19wy6n4hpayjxbpx40c4s")))

(define-public crate-rs-fsring-2.1.0 (c (n "rs-fsring") (v "2.1.0") (h "181pnkqwl8sypnvwlbzwwrfv9n19393rlix3hc6k2xbrpzi9wc2y")))

(define-public crate-rs-fsring-2.2.0 (c (n "rs-fsring") (v "2.2.0") (h "0g4whgmfswrvps8jqqqw1h1dy8p4na8zif91k6kq1izi09wyk92r")))

(define-public crate-rs-fsring-2.3.0 (c (n "rs-fsring") (v "2.3.0") (h "17968h9sq7h6ng306d97pg4rrf3y07i9p6gd9adnnyk6dz6bslph")))

(define-public crate-rs-fsring-3.0.0 (c (n "rs-fsring") (v "3.0.0") (h "0wz906sps1s8w0l7qiik69y07123614mzhwbqs4ihdzcb7jkgabk")))

(define-public crate-rs-fsring-3.1.0 (c (n "rs-fsring") (v "3.1.0") (h "0yxahj0hz6kfznf92dh7rs425jrc0g1jhz1isx0zsc7qvwvkyrca")))

