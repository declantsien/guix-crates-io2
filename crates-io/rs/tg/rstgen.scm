(define-module (crates-io rs tg rstgen) #:use-module (crates-io))

(define-public crate-rstgen-0.1.0 (c (n "rstgen") (v "0.1.0") (h "18vlj9syp88nl0r3pahqksb51mjr6g52vhiziria7s5vywgyg15s")))

(define-public crate-rstgen-0.1.1 (c (n "rstgen") (v "0.1.1") (h "17kifcp6khjfygxwdn4xq1vr7hhq4cwvmxx3ypp9h7yk13lc1fcz")))

(define-public crate-rstgen-0.1.2 (c (n "rstgen") (v "0.1.2") (h "10qy8kgnr5nih6j33mywaz3xsnb4zf3v80bbxdgyqf6fvv94hllw")))

(define-public crate-rstgen-0.1.3 (c (n "rstgen") (v "0.1.3") (h "1vn9dr7zfkb1f4w64q0dvb4r8kk33rkljxndv9bl0vlqx80aj29x")))

(define-public crate-rstgen-0.1.4 (c (n "rstgen") (v "0.1.4") (h "0fapd156qvxdkangarbmcjrwlqijjjznmq5lq4i3mdhklyn0h0xh")))

