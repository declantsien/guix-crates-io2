(define-module (crates-io rs cm rscmm) #:use-module (crates-io))

(define-public crate-rscmm-0.3.0 (c (n "rscmm") (v "0.3.0") (d (list (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "0wrk4lmwkrnvln68nd97iz2jvpcdgf4653a595dmdk77w0yj99iw") (y #t) (r "1.57")))

(define-public crate-rscmm-0.3.1 (c (n "rscmm") (v "0.3.1") (d (list (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "1gk6sf1vj1181f55j4dsvkb3pbl76i5vxbhlx7896ilv88fsdknd") (r "1.57")))

