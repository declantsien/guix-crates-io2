(define-module (crates-io rs -p rs-pwsafe) #:use-module (crates-io))

(define-public crate-rs-pwsafe-0.0.1 (c (n "rs-pwsafe") (v "0.0.1") (d (list (d (n "lsx") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0hvbx054wg789xsxbl3ck4lg8nxiqz0r6ljxm44syqzjv73l70rr")))

(define-public crate-rs-pwsafe-0.0.2 (c (n "rs-pwsafe") (v "0.0.2") (d (list (d (n "lsx") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "16d2cy5nfrmx9z3sghzmshibl3r4pk8kj3bx1m4pvfvp5q4jlw3l")))

(define-public crate-rs-pwsafe-0.0.3 (c (n "rs-pwsafe") (v "0.0.3") (d (list (d (n "lsx") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "05s6nmxj8vrb8ynbfncnkac9gcsm4s0x0380dppp7cpn8fill0la")))

(define-public crate-rs-pwsafe-0.0.4 (c (n "rs-pwsafe") (v "0.0.4") (d (list (d (n "lsx") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0k1shbdfsphj4xw9mxsmlsk6ax9as2dqw3a5h44d0f5j02csgj15")))

(define-public crate-rs-pwsafe-0.0.5 (c (n "rs-pwsafe") (v "0.0.5") (d (list (d (n "lsx") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1d6zij3ax0bvm4k5h8zzl3cyqgf15ijg23afyimg2vdz9p81c9gl")))

(define-public crate-rs-pwsafe-0.0.7 (c (n "rs-pwsafe") (v "0.0.7") (d (list (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "lsx") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "179jkn67q659i96mv120l08zjsmhlmx15lm4ypvqlcafwwmlqr5h")))

