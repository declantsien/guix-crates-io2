(define-module (crates-io rs -p rs-pop3) #:use-module (crates-io))

(define-public crate-rs-pop3-1.0.7 (c (n "rs-pop3") (v "1.0.7") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0bzgbs1rwzn4zraqa0s1y82qvagxrz1lkhzdvx9pm08vpl61n3gw")))

(define-public crate-rs-pop3-1.0.8 (c (n "rs-pop3") (v "1.0.8") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1g0nnk34f3mcp6zpw5iv0qi5xfkb4qr595vb27kwhg68fs239135")))

