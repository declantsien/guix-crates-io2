(define-module (crates-io rs -p rs-postgres-stat2otel) #:use-module (crates-io))

(define-public crate-rs-postgres-stat2otel-0.1.1 (c (n "rs-postgres-stat2otel") (v "0.1.1") (d (list (d (n "opentelemetry") (r "^0.18.0") (f (quote ("metrics"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "14m614byf1w6w0wwhfjcdz67wk8qm54yji4mk93jxksfvnb780hn") (f (quote (("default" "config_toml") ("config_toml" "toml"))))))

(define-public crate-rs-postgres-stat2otel-0.2.0 (c (n "rs-postgres-stat2otel") (v "0.2.0") (d (list (d (n "opentelemetry") (r "^0.18.0") (f (quote ("metrics"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "070d5py7a6xawp5w1j42419swigs6c4fmsfsvb0gbi8f9wmpmjdy") (f (quote (("default" "config_toml") ("config_toml" "toml"))))))

(define-public crate-rs-postgres-stat2otel-0.2.1 (c (n "rs-postgres-stat2otel") (v "0.2.1") (d (list (d (n "opentelemetry") (r "^0.18.0") (f (quote ("metrics"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "10ks3zr9483dyxwhx3l4cj5nk5xy83dnbzpj5g6ggbs53qkb24nx") (f (quote (("default" "config_toml") ("config_toml" "toml"))))))

