(define-module (crates-io rs -p rs-pixel) #:use-module (crates-io))

(define-public crate-rs-pixel-0.1.0 (c (n "rs-pixel") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("curl-client"))) (d #t) (k 0)))) (h "1plrz2xywqs7757zl1l82rnsaky5r8znsxc9j6fhkgyxd0kz3g4a")))

(define-public crate-rs-pixel-0.1.1 (c (n "rs-pixel") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("curl-client"))) (d #t) (k 0)))) (h "1hwrc93z64m9mpg7fkxiqh4jn7gd3a1711ywszzp4yfghv6xxqiy")))

(define-public crate-rs-pixel-0.2.0 (c (n "rs-pixel") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "moka") (r "^0.11.2") (f (quote ("future"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("curl-client"))) (d #t) (k 0)))) (h "0gxd9afzhlj59wy1m97frw2v7i8k885glss42w7shwzzwx7mdwv5")))

