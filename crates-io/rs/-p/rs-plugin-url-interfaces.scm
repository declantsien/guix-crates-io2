(define-module (crates-io rs -p rs-plugin-url-interfaces) #:use-module (crates-io))

(define-public crate-rs-plugin-url-interfaces-0.1.0 (c (n "rs-plugin-url-interfaces") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.31") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)))) (h "1srdd57nrzsvm4s08h322fhwf06dgra17adb1209djjsdsa333xn")))

(define-public crate-rs-plugin-url-interfaces-0.1.1 (c (n "rs-plugin-url-interfaces") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.31") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)))) (h "0qa979d07k7v3mq2qc7xnjjbiryy6k3nf4cwi3nhy35c9l9cj00g")))

(define-public crate-rs-plugin-url-interfaces-0.1.2 (c (n "rs-plugin-url-interfaces") (v "0.1.2") (d (list (d (n "rusqlite") (r "^0.31") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)))) (h "1a18y0ykvn81qshc96frg44pzgj2j7k1xkwy6pv964k595lkgda5")))

