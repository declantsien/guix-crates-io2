(define-module (crates-io rs -p rs-progress) #:use-module (crates-io))

(define-public crate-rs-progress-0.1.0 (c (n "rs-progress") (v "0.1.0") (h "1gry4751lcqb5ljzdc4p2mrnm68iikgri6f5k90i4smfrxn1zghn")))

(define-public crate-rs-progress-0.1.1 (c (n "rs-progress") (v "0.1.1") (h "0h7hfyahdigbrgxq4gv08nxjj52l07d1dadaff4p7nsrp8xv1kk9")))

