(define-module (crates-io rs -p rs-proxy) #:use-module (crates-io))

(define-public crate-rs-proxy-0.7.6 (c (n "rs-proxy") (v "0.7.6") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "0q3b4dlx7d7pci6v09gnkqb1wzsw51m5l934nzss89c3xvg2mx3d") (y #t)))

(define-public crate-rs-proxy-0.1.0 (c (n "rs-proxy") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "135qphssa4zmp23w6l6s5x0a9msrygghnx7r2grsjzdmifay0mwy")))

(define-public crate-rs-proxy-0.1.1 (c (n "rs-proxy") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "1h9wg1whbkbxak5h2c02vfpa37z0kbhzxhkxskx7dc91n073v4sb")))

(define-public crate-rs-proxy-0.1.2 (c (n "rs-proxy") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "net" "rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "1gzz758kacavrhb3v4v0ans4isd4pbayswmcggl3ir0y8jgdvrky")))

(define-public crate-rs-proxy-0.2.0 (c (n "rs-proxy") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "0fbj8p9f2axdqfvrpyds7i4sp86m2yvpyizb139i32m03b9v0i7w")))

(define-public crate-rs-proxy-0.2.1 (c (n "rs-proxy") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "1piphvw1gs3zn9r3i6kc2r9mz5ng4ncl3kbq16408z82b1jyf0mb")))

(define-public crate-rs-proxy-0.2.2 (c (n "rs-proxy") (v "0.2.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "17dx1dgnwi5qx6d5zc6m9r6qxvnrgpjqrd9dmmqd0wnl42zp8y6k")))

(define-public crate-rs-proxy-0.2.3 (c (n "rs-proxy") (v "0.2.3") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "1x2lxbdwxss47idhwg62jayic51av5m8in908lihvbb7nrh66qqi")))

(define-public crate-rs-proxy-0.2.4 (c (n "rs-proxy") (v "0.2.4") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "1410mx6fwh7wr9mvyb4zpk0zz2jm2bmzx9ikmyhpa5p4fch3bdg7")))

(define-public crate-rs-proxy-0.3.0 (c (n "rs-proxy") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "124gaf3kq7kl0sf7knhiyki8xd5j26gbv6w3bd16vl0rxa861jhn")))

(define-public crate-rs-proxy-0.3.1 (c (n "rs-proxy") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "0zn3kikwkafswf7y0ys8dk2h9lha5gh7gzz3cfx26galff0395ra")))

(define-public crate-rs-proxy-0.4.0 (c (n "rs-proxy") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proxy-header") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "06chlwid50pc7yhw348bwrazvc928rm8m67c1xmcm1kh1mg9x401")))

(define-public crate-rs-proxy-0.5.0 (c (n "rs-proxy") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proxy-header") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "1hyiyin8hk0vw23r7wyhymry959s2xmhwfygyya9v7kvfs81iqrm")))

(define-public crate-rs-proxy-0.5.1 (c (n "rs-proxy") (v "0.5.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proxy-header") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (d #t) (k 0)))) (h "19adrl67ad3ciyypq5pg3g3pv0nqvaabfh9mm82qnsl3ckbknydd")))

(define-public crate-rs-proxy-0.5.2 (c (n "rs-proxy") (v "0.5.2") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proxy-header") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (f (quote ("bytes"))) (d #t) (k 0)))) (h "0q5ym9ki7cmb5d0alm6pxs4d0cw3g6rx5kiakak3lwyiwrmxgzb4")))

(define-public crate-rs-proxy-0.6.0 (c (n "rs-proxy") (v "0.6.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proxy-header") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (f (quote ("bytes"))) (d #t) (k 0)))) (h "0c0vpmh12cxfw3rpxmzad39pqy2ignhvb7wawkgckch4sbcmc4iq")))

(define-public crate-rs-proxy-0.8.0 (c (n "rs-proxy") (v "0.8.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proxy-header") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (f (quote ("bytes"))) (d #t) (k 0)))) (h "1nmgsmjsflb2vgnq94lh5pknld1w1jkfqj2zrp3zkqw4nq3gnfhs")))

(define-public crate-rs-proxy-0.9.0 (c (n "rs-proxy") (v "0.9.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proxy-header") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (f (quote ("bytes"))) (d #t) (k 0)))) (h "0pfk7aspydjijhnx1b8ymhvdvp57w1267f63fxlcw2a9phd8vmsg")))

(define-public crate-rs-proxy-0.10.0 (c (n "rs-proxy") (v "0.10.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "proxy-header") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (f (quote ("bytes"))) (d #t) (k 0)))) (h "1p6g29wrhzrny7f5nf661dza5ickyxjpaw4m89dblhw755ks2ryh")))

