(define-module (crates-io rs -p rs-parse-snapshot) #:use-module (crates-io))

(define-public crate-rs-parse-snapshot-1.0.6 (c (n "rs-parse-snapshot") (v "1.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "yuuang_dominators") (r "^0.6.3") (d #t) (k 0)))) (h "134s8wqvi2a787bsczjhw0vsl42m0gyi7iph84nwvs723qlgwwy8")))

