(define-module (crates-io rs fm rsfmt) #:use-module (crates-io))

(define-public crate-rsfmt-1.60.0 (c (n "rsfmt") (v "1.60.0") (d (list (d (n "rustc-ap-rustc_ast") (r "^727.0.0") (d #t) (k 0)) (d (n "rustc-ap-rustc_data_structures") (r "^727.0.0") (d #t) (k 0)) (d (n "rustc-ap-rustc_parse") (r "^727.0.0") (d #t) (k 0)) (d (n "rustc-ap-rustc_session") (r "^727.0.0") (d #t) (k 0)) (d (n "rustc-ap-rustc_span") (r "^727.0.0") (d #t) (k 0)) (d (n "rustc-ap-rustc_target") (r "^727.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1ap539qa7fb5l1xdzwrd9az8xp2zzyaqhp0vln3rmcl4r9v7jc20") (r "1.60")))

