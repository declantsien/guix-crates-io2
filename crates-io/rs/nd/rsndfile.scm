(define-module (crates-io rs nd rsndfile) #:use-module (crates-io))

(define-public crate-rsndfile-0.0.1 (c (n "rsndfile") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "1a1v272d5rpkmfwsxrpg34q6yi81j1lijdh4v3x1sa55pm1b4n9z")))

(define-public crate-rsndfile-0.0.2 (c (n "rsndfile") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "0w399nblwhzfgplbph6qyyz3rqrjhbq979x1ia2vs9ravj6scksr")))

