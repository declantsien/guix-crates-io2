(define-module (crates-io rs nd rsndom-demo) #:use-module (crates-io))

(define-public crate-rsndom-demo-0.1.0 (c (n "rsndom-demo") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0lq73cm1692ki7vqmj7lc5smrgdr2w3kz7ihrb7iglsfqgcc45qx")))

(define-public crate-rsndom-demo-0.1.1 (c (n "rsndom-demo") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1xchhamr3kkppwbnq6yp5xbbksgg018b3w0ki7xvnyq6jrsvg8bf")))

