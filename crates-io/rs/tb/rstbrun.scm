(define-module (crates-io rs tb rstbrun) #:use-module (crates-io))

(define-public crate-rstbrun-0.1.0 (c (n "rstbrun") (v "0.1.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.131") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1q9cc11zisr0qk3pxai5m784svikm1cmgc8dwv9x8j54kqnmpg56")))

(define-public crate-rstbrun-0.1.1 (c (n "rstbrun") (v "0.1.1") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.131") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1p533k5p8va3qfbdr7ccv3bx482x30qnqym7s34gjcrs1jga5nj2")))

