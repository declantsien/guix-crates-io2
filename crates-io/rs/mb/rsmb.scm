(define-module (crates-io rs mb rsmb) #:use-module (crates-io))

(define-public crate-rsmb-0.10.0 (c (n "rsmb") (v "0.10.0") (d (list (d (n "lettre") (r "^0.9") (d #t) (k 0)) (d (n "lettre_email") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "01di6q26jnvxiz54d7xmjjgri6kg4mr24gk3dl89p5wpxaiilc0c") (r "1.61.0")))

