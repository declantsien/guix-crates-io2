(define-module (crates-io rs mb rsmb-catsay) #:use-module (crates-io))

(define-public crate-rsmb-catsay-0.1.0 (c (n "rsmb-catsay") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 0)))) (h "01lawgwrb256fhr7d8l3ps94bvpcr4gx5am206ga7dd4d6124cby")))

