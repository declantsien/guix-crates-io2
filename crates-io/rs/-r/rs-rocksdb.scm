(define-module (crates-io rs -r rs-rocksdb) #:use-module (crates-io))

(define-public crate-rs-rocksdb-0.5.0 (c (n "rs-rocksdb") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "rs-rocksdb-sys") (r "^0.5") (d #t) (k 0)))) (h "0279hsnwapmanxwvn65wf1dbvrszqvbv2y658xi001wyd0n1xhfr") (f (quote (("valgrind") ("default"))))))

