(define-module (crates-io rs -r rs-rocksdb-sys) #:use-module (crates-io))

(define-public crate-rs-rocksdb-sys-0.5.5 (c (n "rs-rocksdb-sys") (v "0.5.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)))) (h "0b8j52v5i6says6dqby1pbwss0zj1pdbgz29ns8jfxzln46c8097") (f (quote (("default")))) (l "rocksdb")))

