(define-module (crates-io rs -r rs-rdb2kv) #:use-module (crates-io))

(define-public crate-rs-rdb2kv-0.2.0 (c (n "rs-rdb2kv") (v "0.2.0") (h "0xcrx7kvxwamxnrp1bz6vkyd37z0b9077h5kfkxaxngxixzhx0rp")))

(define-public crate-rs-rdb2kv-0.3.0 (c (n "rs-rdb2kv") (v "0.3.0") (h "14r2nkz6cwkr7halyad26jnss63hgf54rczqcr309zcwz0l3ig0f")))

(define-public crate-rs-rdb2kv-0.4.0 (c (n "rs-rdb2kv") (v "0.4.0") (h "11c31y3qsly1mi5l53h924nvlhadpjs021l3mfqjh048xgkx9yp2")))

(define-public crate-rs-rdb2kv-0.5.0 (c (n "rs-rdb2kv") (v "0.5.0") (h "1ai4j0rj7aa0kjajw67188xi7gdrara0441x4hvp02jl4j5bcfyp")))

(define-public crate-rs-rdb2kv-0.6.0 (c (n "rs-rdb2kv") (v "0.6.0") (h "0j9nwmfbp4bc1463azqgcjvnmg84azqqazsnx6x6k4g8ims3jivk")))

(define-public crate-rs-rdb2kv-0.7.0 (c (n "rs-rdb2kv") (v "0.7.0") (h "15cq4s87yrkjzfh2pn4zh1sg3vh8b7g95j00kda7z5gh4b9cb10r")))

(define-public crate-rs-rdb2kv-0.8.0 (c (n "rs-rdb2kv") (v "0.8.0") (h "05mwzas759vng5z6hr6a5xa5yall1dg6jisjhknm9sn7hw2ahndm")))

