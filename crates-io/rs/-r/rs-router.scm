(define-module (crates-io rs -r rs-router) #:use-module (crates-io))

(define-public crate-rs-router-0.1.1 (c (n "rs-router") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 2)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 2)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0wcgkwc009w99hb8x9ng7y8yanwh21a536laz5lpaqndfkrqqs1s")))

