(define-module (crates-io rs _r rs_rc4) #:use-module (crates-io))

(define-public crate-rs_rc4-0.1.0 (c (n "rs_rc4") (v "0.1.0") (h "1n56pd9y2alxgidz147aacrr08cwnlaw0xphk3nqxqirlrq33mlg")))

(define-public crate-rs_rc4-0.1.1 (c (n "rs_rc4") (v "0.1.1") (h "1j2xlafhcs1pvbszp6x60whx534gas0y5ba35cdfikdx3l4693iy")))

(define-public crate-rs_rc4-0.1.2 (c (n "rs_rc4") (v "0.1.2") (h "1891hqh9s79s5gyhbgpmyczq1z0ca1akhf2a4nw5rj5dxvfa54as")))

