(define-module (crates-io rs _r rs_rc5) #:use-module (crates-io))

(define-public crate-rs_rc5-0.1.0 (c (n "rs_rc5") (v "0.1.0") (h "1b4b7pmwzan3n7hy4vy46i82l2sl460j1am54j7lfdnisysviidy")))

(define-public crate-rs_rc5-0.1.1 (c (n "rs_rc5") (v "0.1.1") (h "11nnndlcz8jrqbafsja63hab4hsqrh8pa546q3v7n04pwmrd0xvh")))

(define-public crate-rs_rc5-0.1.2 (c (n "rs_rc5") (v "0.1.2") (h "1lna2xsjvb06df3sda2ynlrfljc1176gww6y9ginpxmivclbjqcn")))

