(define-module (crates-io rs _r rs_ripemd_160) #:use-module (crates-io))

(define-public crate-rs_ripemd_160-0.1.0 (c (n "rs_ripemd_160") (v "0.1.0") (h "1xzz5qa91lvlxwnvl7fbbw27p6r7d3nif5z6j0ds0rhs0jy6i0j4")))

(define-public crate-rs_ripemd_160-0.1.1 (c (n "rs_ripemd_160") (v "0.1.1") (h "0m371pbbg28g14a5i5zsnmc8rbw9zyzwyc2i7hpym5hannjnq2pd")))

(define-public crate-rs_ripemd_160-0.1.2 (c (n "rs_ripemd_160") (v "0.1.2") (h "0dq5h2kr9jvlvsa9i7qziwiq0bz7ajxasgmnl4hwihlylijh95kk")))

