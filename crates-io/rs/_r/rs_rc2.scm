(define-module (crates-io rs _r rs_rc2) #:use-module (crates-io))

(define-public crate-rs_rc2-0.1.0 (c (n "rs_rc2") (v "0.1.0") (h "0qphbmmwnh1ybm3f9icm074rdkvr0r9bq2v2jsqa5xc11ani1m0z")))

(define-public crate-rs_rc2-0.1.1 (c (n "rs_rc2") (v "0.1.1") (h "0rb0499p5jyvd3s89yhd7nv4mxrb86bxa0pzcbw37l37cwmf9gsr")))

(define-public crate-rs_rc2-0.1.2 (c (n "rs_rc2") (v "0.1.2") (h "1nhmfzjx1pj3j0k354cic9xphhnvz1i521nn5x2c2ay7kl1rfvc2")))

