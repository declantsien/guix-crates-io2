(define-module (crates-io rs _r rs_rsa) #:use-module (crates-io))

(define-public crate-rs_rsa-0.1.0 (c (n "rs_rsa") (v "0.1.0") (h "08k421h0rsh379yajrs8qqpl0gw7wvc9wpjx4bpjiirhzl2pblq3")))

(define-public crate-rs_rsa-0.1.1 (c (n "rs_rsa") (v "0.1.1") (h "12bgxd6dl9a1b6kqwlm50pdg4k9m4bpmmwi1xclq4p3wmiq94n8c")))

(define-public crate-rs_rsa-0.1.2 (c (n "rs_rsa") (v "0.1.2") (h "18q0gi6pgvfm6nrqh8m6hxnp50mh2wg7l3gw38n3x72cliwdb2g2")))

