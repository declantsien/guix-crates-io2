(define-module (crates-io rs ep rsepl) #:use-module (crates-io))

(define-public crate-rsepl-0.1.0 (c (n "rsepl") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^3.0.0") (d #t) (k 0)) (d (n "shellfn") (r "^0.1.1") (d #t) (k 0)))) (h "1kvdpsx2ylvnnzrj90kr08vs2wai6fqk82jlqvbslk9k65xvgwxv")))

(define-public crate-rsepl-0.2.0 (c (n "rsepl") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^3.0.0") (d #t) (k 0)) (d (n "shellfn") (r "^0.1.1") (d #t) (k 0)))) (h "1dradzix167r876vn7sjnv7hi3xzbfiqab80aismmbx8i91kz7pr")))

(define-public crate-rsepl-0.2.1 (c (n "rsepl") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^3.0.0") (d #t) (k 0)))) (h "08sybnbdad58x7fyccw284wbmvvpxk3yzn14x9xxxjycb06d4jsi")))

(define-public crate-rsepl-0.3.0 (c (n "rsepl") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "reedline") (r "^0.31.0") (d #t) (k 0)))) (h "10x4qgy6bp3xfras7086ia6cynwc3v7w2j1kk4ywj1qckabhdsx9")))

(define-public crate-rsepl-0.3.1 (c (n "rsepl") (v "0.3.1") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "reedline") (r "^0.31.0") (d #t) (k 0)))) (h "0lqizmb8b3zbkd5a7f7fnq1gsvbcg7mw6xd6fbpgwbq2j535c6m2")))

