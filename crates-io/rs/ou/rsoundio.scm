(define-module (crates-io rs ou rsoundio) #:use-module (crates-io))

(define-public crate-rsoundio-0.1.1 (c (n "rsoundio") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "rci") (r "0.1.*") (d #t) (k 2)))) (h "126b32cxz77pwcf5w91v9fdd9v6spqycgjzvky9wwrb04cmyr0zj")))

(define-public crate-rsoundio-0.1.2 (c (n "rsoundio") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "rci") (r "0.1.*") (d #t) (k 2)))) (h "0xmv87hz689ynzk8lapqgrkcmm4s12zy9f01ijmkpbjrb8jw7vba")))

(define-public crate-rsoundio-0.1.3 (c (n "rsoundio") (v "0.1.3") (d (list (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "rci") (r "0.1.*") (d #t) (k 2)))) (h "0h0nyp1qq6fvzxp2h3qczq62v4f1ajdaaxas5bcgxjq0633458dh")))

(define-public crate-rsoundio-0.1.4 (c (n "rsoundio") (v "0.1.4") (d (list (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "rb") (r "^0.1") (d #t) (k 2)) (d (n "rci") (r "0.1.*") (d #t) (k 2)))) (h "00chj18vg6dzan6dw8ylgynfn9jks0lv8xlllppiblhv44zysjxn")))

(define-public crate-rsoundio-0.1.5 (c (n "rsoundio") (v "0.1.5") (d (list (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "rb") (r "^0.2") (d #t) (k 2)) (d (n "rci") (r "0.1.*") (d #t) (k 2)))) (h "1jrgn6byz8nqzjnyipzgg4rswrq65yndhn3krhwjqh34kljh7809")))

(define-public crate-rsoundio-0.1.6 (c (n "rsoundio") (v "0.1.6") (d (list (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "rb") (r "^0.2") (d #t) (k 2)) (d (n "rci") (r "0.1.*") (d #t) (k 2)))) (h "0qm7ff60yppbxl5vl3y004id38xjw4w1v43vjd2kd4r1sdm1qwag")))

