(define-module (crates-io rs oc rsocx) #:use-module (crates-io))

(define-public crate-rsocx-0.1.0 (c (n "rsocx") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "02sv9g0wjzbigvyxy6yskjmbgf6d8l4ld8apcr1kwqmc285v0nc7") (y #t)))

(define-public crate-rsocx-0.1.1 (c (n "rsocx") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "1zzjcxglfb7385ymvnrhjd9hfj9xqlvmdqgpkcl6xxjd820nfk3k") (y #t)))

(define-public crate-rsocx-0.1.2 (c (n "rsocx") (v "0.1.2") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "1xvaxbm991g2hm4h853d4v8qzca3ckm5x4nhb6ca69c3v4x7pjmk")))

(define-public crate-rsocx-0.1.3 (c (n "rsocx") (v "0.1.3") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)))) (h "0py6as6634fw2xjb2gm6iav73138krrrm9z7saxgny8xp94p899z")))

