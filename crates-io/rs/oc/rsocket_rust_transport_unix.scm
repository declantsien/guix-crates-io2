(define-module (crates-io rs oc rsocket_rust_transport_unix) #:use-module (crates-io))

(define-public crate-rsocket_rust_transport_unix-0.5.3 (c (n "rsocket_rust_transport_unix") (v "0.5.3") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rsocket_rust") (r "^0.5.3") (f (quote ("frame"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("rt-core" "rt-threaded" "uds" "sync" "stream"))) (k 0)) (d (n "tokio-util") (r "^0.3.1") (f (quote ("codec"))) (k 0)))) (h "1m6r44fpvj0fc719ml51fqzrnbxfcwlp6m32jgvm8xqim908ddf9")))

