(define-module (crates-io rs mq rsmq-rs) #:use-module (crates-io))

(define-public crate-rsmq-rs-1.0.0 (c (n "rsmq-rs") (v "1.0.0") (d (list (d (n "async-std") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)))) (h "0gv6n6vmmfk0b4h60cbi6daz9336l5cr5ca3v3qks9bx27jl8qc8") (y #t)))

(define-public crate-rsmq-rs-1.0.1 (c (n "rsmq-rs") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)))) (h "193wvwwwrarwrkljpf1n2apqn7j24rjwlxwyhyr2vwlq695qbwix") (y #t)))

(define-public crate-rsmq-rs-1.0.2 (c (n "rsmq-rs") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)))) (h "13nwy5iln3z0v18zaz05z5w3zzlrcrwg4kjkifrpyk6yfqy00757") (y #t)))

(define-public crate-rsmq-rs-1.0.3 (c (n "rsmq-rs") (v "1.0.3") (h "0rqvjny0lvabgyvx9s2b1gjmgcnksv5q6i643z34xfxjgpqsygbv")))

