(define-module (crates-io rs mq rsmq) #:use-module (crates-io))

(define-public crate-rsmq-0.0.1 (c (n "rsmq") (v "0.0.1") (d (list (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "14zn2p3il0iw43vhd1dayz5nkgbib79m815z4r1l0ggk6bmbirkr")))

(define-public crate-rsmq-0.1.0 (c (n "rsmq") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "059l1qd3h735da0pskgb6rcx0p8xh5jhj6hw0nvlv3iqwdjhz99c")))

(define-public crate-rsmq-0.2.0 (c (n "rsmq") (v "0.2.0") (d (list (d (n "criterion") (r "^0.1.0") (d #t) (k 2)) (d (n "r2d2") (r "^0.8.1") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "radix") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0k2smn4xih59k80iz8hav2skb8l0js6n2dksxxvpzlpqb1cxm3m8")))

(define-public crate-rsmq-0.2.1 (c (n "rsmq") (v "0.2.1") (d (list (d (n "criterion") (r "^0.1.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.1") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "radix") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1wi3nmk1nf9y5gr18phsz91gpl0jpcm2q4pgj59nc2lgi3wz289p")))

