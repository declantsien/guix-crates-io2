(define-module (crates-io rs -o rs-ovpinergy-macros) #:use-module (crates-io))

(define-public crate-rs-ovpinergy-macros-0.1.0 (c (n "rs-ovpinergy-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gaz3px8w38aqf980724q4hr4dizlih61vhja96sdy1fwbmh21cw")))

