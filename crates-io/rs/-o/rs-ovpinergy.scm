(define-module (crates-io rs -o rs-ovpinergy) #:use-module (crates-io))

(define-public crate-rs-ovpinergy-0.1.0 (c (n "rs-ovpinergy") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rppal") (r "^0.14") (d #t) (k 0)) (d (n "rs-ovpinergy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "065gdjv2jcfadawnh6flarqd21mx214m4q87slxqy7w00hkl0140")))

