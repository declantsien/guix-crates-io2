(define-module (crates-io rs -o rs-odbc) #:use-module (crates-io))

(define-public crate-rs-odbc-0.1.0 (c (n "rs-odbc") (v "0.1.0") (d (list (d (n "mockall") (r "^0") (d #t) (k 2)) (d (n "mockall_double") (r "^0") (d #t) (k 0)) (d (n "rs-odbc_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1rj0iq9llj836azigrgfgbyf9bz3y3imwi6d0p9m6ggln51k28m2") (f (quote (("static") ("default"))))))

(define-public crate-rs-odbc-0.2.0 (c (n "rs-odbc") (v "0.2.0") (d (list (d (n "mockall") (r "^0.11.2") (d #t) (k 2)) (d (n "mockall_double") (r "^0.3.0") (d #t) (k 0)) (d (n "rs-odbc_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.65") (d #t) (k 2)))) (h "0pl5jck7q5cx5nbbink8razymnv5a2xmkl4xq6rd690kcb66jmwn") (f (quote (("std") ("static") ("default" "std"))))))

