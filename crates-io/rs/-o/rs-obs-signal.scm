(define-module (crates-io rs -o rs-obs-signal) #:use-module (crates-io))

(define-public crate-rs-obs-signal-0.2.0 (c (n "rs-obs-signal") (v "0.2.0") (h "0a61yfymddqsybq79qgcm48bymk5wlpp6pkg4awqjzl6z9bn1hr8")))

(define-public crate-rs-obs-signal-0.3.0 (c (n "rs-obs-signal") (v "0.3.0") (h "16q711c6yaw4syg7nfk55bippv2xsr8wd9kj2ck2y6mygbpnaq5v")))

(define-public crate-rs-obs-signal-0.4.0 (c (n "rs-obs-signal") (v "0.4.0") (h "0qba1n4adn46ss31sv471mdsnam3q74c2s1lshpmmmbnfk8qdlmd")))

(define-public crate-rs-obs-signal-0.5.0 (c (n "rs-obs-signal") (v "0.5.0") (h "03nz6riqf0r8q943p78yldxxjqcfhvmh1qx0sbb43k9rkmqw2wan")))

(define-public crate-rs-obs-signal-0.6.0 (c (n "rs-obs-signal") (v "0.6.0") (h "0g25rn6b8waxs2w2as40kdzf7m0dlbdjrv9gl6qcbs7cs9gy17yn")))

