(define-module (crates-io rs _c rs_cast_128) #:use-module (crates-io))

(define-public crate-rs_cast_128-0.1.0 (c (n "rs_cast_128") (v "0.1.0") (h "0lx14rr7zsky8xy209r808p2vhsslfvdba6hzz05ai4fbywwfc0b")))

(define-public crate-rs_cast_128-0.1.1 (c (n "rs_cast_128") (v "0.1.1") (h "1xg7fdpxrzzfyc4f5bwxlx5qw3qkbp29n7a11jkyqfpyryqfmyjp")))

(define-public crate-rs_cast_128-0.1.2 (c (n "rs_cast_128") (v "0.1.2") (h "1jfl8b6gasrikbz3xbk7rrmvphsli7cpd9ma170w858pg5rjz5am")))

