(define-module (crates-io rs _c rs_concaveman) #:use-module (crates-io))

(define-public crate-rs_concaveman-1.0.0 (c (n "rs_concaveman") (v "1.0.0") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "robust-predicates") (r "0.1.*") (d #t) (k 0)))) (h "0z26p1ym95kj413pqi0sjskp9wm0k6ahz9vkspqicnyqqs2pv2n0")))

(define-public crate-rs_concaveman-1.0.1 (c (n "rs_concaveman") (v "1.0.1") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "robust-predicates") (r "0.1.*") (d #t) (k 0)))) (h "13cmhqnx8b2xx5mda5hric2ybl6fqyxd5pk74fhyr2qiyw9x7fs3")))

(define-public crate-rs_concaveman-1.0.2 (c (n "rs_concaveman") (v "1.0.2") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "robust-predicates") (r "0.1.*") (d #t) (k 0)))) (h "01vi5ba4zv741csb4ppx881k2g17a2bn8lbw2qc2g4xjl7x157zm")))

(define-public crate-rs_concaveman-1.0.3 (c (n "rs_concaveman") (v "1.0.3") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "robust-predicates") (r "0.1.*") (d #t) (k 0)))) (h "0bj1z6dy0qdn4clmq8ifyw80csgwliz8a1aa6qdx1cihq6gp7q5f")))

(define-public crate-rs_concaveman-1.0.4 (c (n "rs_concaveman") (v "1.0.4") (d (list (d (n "cc") (r "1.0.*") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "robust-predicates") (r "0.1.*") (d #t) (k 0)))) (h "1sblymblrdn4fiz26lbk31snkl8f7m3wvfywwkzl864yq9nrqmkh")))

