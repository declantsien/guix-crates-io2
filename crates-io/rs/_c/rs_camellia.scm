(define-module (crates-io rs _c rs_camellia) #:use-module (crates-io))

(define-public crate-rs_camellia-0.1.0 (c (n "rs_camellia") (v "0.1.0") (h "1h6zfcfma3ndy2z96a0kr4d18vx73a2y21frkl2pnfphfiqa48x1")))

(define-public crate-rs_camellia-0.1.1 (c (n "rs_camellia") (v "0.1.1") (h "0f56kwzdgss9k81nbjbpdhc4iyk1rdc9wsggvxw0lvaiv3im5ml9")))

(define-public crate-rs_camellia-0.1.2 (c (n "rs_camellia") (v "0.1.2") (h "0vh7768jqknn1v72cy4was58vv4m4j64l4j2v5zwbp0cr60jk4p0")))

