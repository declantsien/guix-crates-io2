(define-module (crates-io rs _l rs_lib) #:use-module (crates-io))

(define-public crate-rs_lib-0.1.0 (c (n "rs_lib") (v "0.1.0") (h "0zvsqbrs9n038l1sas7ympf87d9x9c8m5fwpwhw86wizkd5hgngd")))

(define-public crate-rs_lib-0.1.1 (c (n "rs_lib") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k3l71fg15dwfhfz0wfqnyrmg6vwivcarj8pr1nkdkxrv16615p3")))

