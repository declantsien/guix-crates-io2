(define-module (crates-io rs _l rs_lockfree) #:use-module (crates-io))

(define-public crate-rs_lockfree-0.1.0 (c (n "rs_lockfree") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1cnyzpdp34xbaxcrg6xakazprcj15xbc4gdjdqd2ss89xcin36kx") (f (quote (("max_thread_count_4096") ("max_thread_count_256") ("max_thread_count_16") ("default" "max_thread_count_16"))))))

(define-public crate-rs_lockfree-0.1.1 (c (n "rs_lockfree") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1chyrnh6r2ar830jvxnbl7rm60gcjs4pjknigxw6blnm19rxq637") (f (quote (("max_thread_count_4096") ("max_thread_count_256") ("max_thread_count_16") ("default" "max_thread_count_16"))))))

