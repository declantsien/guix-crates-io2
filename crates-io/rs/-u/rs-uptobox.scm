(define-module (crates-io rs -u rs-uptobox) #:use-module (crates-io))

(define-public crate-rs-uptobox-0.1.0 (c (n "rs-uptobox") (v "0.1.0") (d (list (d (n "json-patch") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1dpz7vnchgaymdvgdqgsckf93cawnab804fjifiqr8bjch5vhqnr")))

