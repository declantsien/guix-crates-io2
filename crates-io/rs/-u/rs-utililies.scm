(define-module (crates-io rs -u rs-utililies) #:use-module (crates-io))

(define-public crate-rs-utililies-0.1.0 (c (n "rs-utililies") (v "0.1.0") (d (list (d (n "android_logger") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)))) (h "04mljak9fxwa118chmklbk0808hk1iskr8m52qciq44j890mrhz3")))

(define-public crate-rs-utililies-0.1.1 (c (n "rs-utililies") (v "0.1.1") (d (list (d (n "android_logger") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)))) (h "0lqm0hpjkc716kwhswkrxnj7zcj5vqzck2zsbzd8knniw4pjiaak")))

