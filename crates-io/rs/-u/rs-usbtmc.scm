(define-module (crates-io rs -u rs-usbtmc) #:use-module (crates-io))

(define-public crate-rs-usbtmc-0.1.0 (c (n "rs-usbtmc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1i9c8vqsyipi60zc9x5g2r1sjkdf26p1b7gj2gq9adxpfsxrm5gz")))

(define-public crate-rs-usbtmc-0.1.1 (c (n "rs-usbtmc") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1966rmkh1adk1h92qsziah51ag1nxk8lg02iaailf5pwl2lqmjn0")))

