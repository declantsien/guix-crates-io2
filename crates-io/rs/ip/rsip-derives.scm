(define-module (crates-io rs ip rsip-derives) #:use-module (crates-io))

(define-public crate-rsip-derives-0.1.0 (c (n "rsip-derives") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "00c69hwxa4rm0nc6pwl85a3nyi13cswlb7x6rfvdn3hzka2a2vq9")))

(define-public crate-rsip-derives-0.3.0 (c (n "rsip-derives") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14w97a0h9rnbx3ndd7mvz0bvchr8q2bzrq461wkvc9z43c7gn010")))

(define-public crate-rsip-derives-0.4.0 (c (n "rsip-derives") (v "0.4.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1sky9147i5k2370apzsvwafdnm8nvh96i1r3ba0sak3lkm2nrhdn")))

