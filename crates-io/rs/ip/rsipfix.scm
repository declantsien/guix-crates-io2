(define-module (crates-io rs ip rsipfix) #:use-module (crates-io))

(define-public crate-rsipfix-0.1.2 (c (n "rsipfix") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "nom-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bmbqh000ld6bxvyy49jrfl80nz63yn24z2s887a15pwmsv7lfk0")))

(define-public crate-rsipfix-0.1.3 (c (n "rsipfix") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "nom-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qys9j2w8zgi9yc1fp6dqscqc36zl6vswmymwicxm2sz2iv44dhh")))

(define-public crate-rsipfix-0.1.4 (c (n "rsipfix") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "nom-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yfjis8g1sn42gby2hm80gg86nfi4kzspdvqmx3bxqlka7n877gk")))

