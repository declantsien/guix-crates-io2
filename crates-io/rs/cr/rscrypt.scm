(define-module (crates-io rs cr rscrypt) #:use-module (crates-io))

(define-public crate-rscrypt-0.1.0 (c (n "rscrypt") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1a8zx41zn6zfs445js6dw6acv79f95s7kr7xg47r3f4h1mk2584i") (y #t)))

(define-public crate-rscrypt-0.1.1 (c (n "rscrypt") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0r5y5zs3b78cdha6psx8mwdz77fkwb8ww7kr2yp8caizaab2zrxv") (y #t)))

(define-public crate-rscrypt-0.1.2 (c (n "rscrypt") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1bqj44wak7hygzvwwgsrgvcwi1ixhs173wgmxjdfmmx73s15fxkg") (y #t)))

(define-public crate-rscrypt-0.1.3 (c (n "rscrypt") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "00r7v29ibnhgbjil181070w34411y892rykdn3xmx5nwnzqdkp20") (y #t)))

(define-public crate-rscrypt-0.1.4 (c (n "rscrypt") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0dh82n04zsmc1p1hsgjnn97r6d3n3xrhx1p46c04br7m6f0dxlhb") (y #t)))

(define-public crate-rscrypt-0.1.5 (c (n "rscrypt") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1h8qlw00n98z9c3ilyibfcmkml4lzw9dvr1ngz23d02j3y3kl0b3") (y #t)))

(define-public crate-rscrypt-0.1.6 (c (n "rscrypt") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0q2sm05yk106mp3c5r9w8z0zk4gvv5a9428lkrv16naw1digxgc6") (y #t)))

(define-public crate-rscrypt-0.1.7 (c (n "rscrypt") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1i4gjpw018kg2ym9hpix2p7vwfc5y8rxjrr4k2p98s7a4d642via") (y #t)))

(define-public crate-rscrypt-0.1.8 (c (n "rscrypt") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0nssadld8afp93wfmvl6gyvcvlnpmpxga168356l9k0994v166c1") (y #t)))

(define-public crate-rscrypt-0.2.0 (c (n "rscrypt") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1mlzj2vk2khq4kjk01kfc9spj3ybv6j1475g6ra9g9415gs6qdiv") (y #t)))

(define-public crate-rscrypt-0.2.1 (c (n "rscrypt") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "072wwjcz5rpv03ddadylq3v1mmdywjjsny9m917qa5113jfjcmmf")))

