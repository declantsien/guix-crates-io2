(define-module (crates-io rs cr rscripter) #:use-module (crates-io))

(define-public crate-rscripter-0.1.0 (c (n "rscripter") (v "0.1.0") (h "02pq5g22x3y99zx2vj5giqjawwq3jbcx060w9fsbrgy829c1b1ds")))

(define-public crate-rscripter-0.1.1 (c (n "rscripter") (v "0.1.1") (h "0ifpd7wghj8y9zdv3v3pk5g0vf6wk8pfjx2j4lzafnx5nrsmgzh1")))

