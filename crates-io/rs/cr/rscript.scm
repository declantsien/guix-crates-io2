(define-module (crates-io rs cr rscript) #:use-module (crates-io))

(define-public crate-rscript-0.1.0 (c (n "rscript") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "14j8lijyjvyksph4zk9v2xf4gxhhn7c52zj4h8d16m7cwmrbhmr0")))

(define-public crate-rscript-0.2.0 (c (n "rscript") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vzhfv2n5rfiq7cc5rd49jhm4hvsa9i61dj1y6263rf80g9nj52m")))

(define-public crate-rscript-0.3.0 (c (n "rscript") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "05ir0d0rvangm2c2hsaigi8vwl3573najgfclq669hgayzbc9j9d")))

(define-public crate-rscript-0.4.0 (c (n "rscript") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lwk2nk0r61f2z15w5sy1chsjkj09w8gz446kdxvvim016z1hbcm")))

(define-public crate-rscript-0.5.0 (c (n "rscript") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kv94qksqhgkzx00ns0fsbpgika7v83ml1dggw98nzdpyglmi1f7")))

(define-public crate-rscript-0.6.0 (c (n "rscript") (v "0.6.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pj20c28z5mwxcyymzaywp8ky4dzh6wmsbxxkqsrdya4k43h8bdv")))

(define-public crate-rscript-0.7.0 (c (n "rscript") (v "0.7.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a70p6537kalrk3qvi8a7yblf7l3ccc3w53xqsg14vx4a4qsq5bp")))

(define-public crate-rscript-0.8.0 (c (n "rscript") (v "0.8.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f192d39ki29vbry2hi9yf1wfggh78dnhdxm192kl1f9bykbhzq6")))

(define-public crate-rscript-0.9.0 (c (n "rscript") (v "0.9.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sklfkzzg1s2sylvi1phch45a2bymyr4k7fgl6yy70ikyf4bxfm4")))

(define-public crate-rscript-0.10.0 (c (n "rscript") (v "0.10.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "133sqjlms6wxz7j1fyifmrz8mp5nd1qwng9wbg6amp6wa39cs86r")))

(define-public crate-rscript-0.11.0 (c (n "rscript") (v "0.11.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qgg9z1nslj927bbx8k04b8fd28gf7jr23awj6jyh683ig5f9ypl")))

(define-public crate-rscript-0.12.0 (c (n "rscript") (v "0.12.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "12q4m8vbvyh7r9jh959bd0265szmwlvc3i8iba6l8vil9p622jha")))

(define-public crate-rscript-0.13.0 (c (n "rscript") (v "0.13.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yjn2i25vwm809gw2j2qngppcpj5k617gxmwch42iy3i2scjhp7w")))

(define-public crate-rscript-0.14.0 (c (n "rscript") (v "0.14.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "12c267cqwzil3jrr95if1ahms3kx4dlngra2ba9csr907gn4hfvk")))

(define-public crate-rscript-0.15.0 (c (n "rscript") (v "0.15.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "037s9v7waj6j5akyv6k9115cdw4adfjklyb281bd2n1jmvn285nx")))

(define-public crate-rscript-0.16.0 (c (n "rscript") (v "0.16.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ysg7bknx0h8lycswwrs5agaj7hmpczd47dc1rvfx68aagmqpjp4")))

(define-public crate-rscript-0.17.0 (c (n "rscript") (v "0.17.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "152vp7g08zhsnfi6xbanc57pxiwai65s37d0q32xfl9w8qimx9vq")))

