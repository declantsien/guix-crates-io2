(define-module (crates-io rs -h rs-humanize) #:use-module (crates-io))

(define-public crate-rs-humanize-0.1.0 (c (n "rs-humanize") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0rqmidi8b74i97m60bfwig91x016zyv4951jrqwiy824h87nrklz")))

(define-public crate-rs-humanize-0.1.1 (c (n "rs-humanize") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0jszsyz40bgbkynay9yf1rpshgkfzcxm9b8q499nsqaf5xg78avz")))

(define-public crate-rs-humanize-1.0.0 (c (n "rs-humanize") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0j1gxbaka16z0rmhclzly3jmybvqdv70xl7vaw82wn4wi9mhrwl4")))

(define-public crate-rs-humanize-1.0.1 (c (n "rs-humanize") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0mcgm0bpf2k6svh99bbr1vk6wmgyf41nzjz1qia00f91ziqjw5in")))

(define-public crate-rs-humanize-1.1.0 (c (n "rs-humanize") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0rcw6iyrkkrqawsnr2sm1jq1843i2wzqkkp8gxiqfk1lm161wjr7")))

(define-public crate-rs-humanize-1.2.0 (c (n "rs-humanize") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1dcwnpq4xddpsg55rkyyic7mqr1gvc8iirnrjabkhv2l1rpg9npb")))

