(define-module (crates-io rs cp rscpi) #:use-module (crates-io))

(define-public crate-rscpi-0.1.0 (c (n "rscpi") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "nusb") (r "^0.1.7") (d #t) (k 0)))) (h "1vdx2jkwnpq0711n23sbq7244fvvgr55hydw6vhlmr4zk1n3gmg5")))

