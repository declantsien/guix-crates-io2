(define-module (crates-io rs cp rscp) #:use-module (crates-io))

(define-public crate-rscp-0.1.0 (c (n "rscp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "simple-rijndael") (r "^0.3.2") (d #t) (k 0)))) (h "0rvakbhvymqskgfi77la4pa1bpck100icmmv5qpg1bn4mvj0vyfi")))

(define-public crate-rscp-0.1.1 (c (n "rscp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "simple-rijndael") (r "^0.3.2") (d #t) (k 0)))) (h "0yqlf3abn4qn1r1mvcm83nnqdp956ay3xsh56nqa52i1z9jdv86q")))

(define-public crate-rscp-0.1.2 (c (n "rscp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "simple-rijndael") (r "^0.3.2") (d #t) (k 0)))) (h "1j227kbbvfn82vy9xhrz3m54nssi247pqz196c2ns5nc3b923gnj")))

(define-public crate-rscp-0.1.3 (c (n "rscp") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "simple-rijndael") (r "^0.3.2") (d #t) (k 0)))) (h "1vsv6hc3499c43w1sc001a9fc1jj2581d9mrc6wji02jl8vmw7ya")))

(define-public crate-rscp-0.1.4 (c (n "rscp") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "simple-rijndael") (r "^0.3.2") (d #t) (k 0)))) (h "0srf2jlz2p2fvdr696ml67qmrwd8sdrp7bsyl9z4l05qr57g70x6")))

(define-public crate-rscp-0.1.5 (c (n "rscp") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "simple-rijndael") (r "^0.3.2") (d #t) (k 0)))) (h "03m0s5fz3rsv9gj6fpg53sy2pcqjnxpd5vridrgmk9vwwk7gkb6r")))

(define-public crate-rscp-0.1.6 (c (n "rscp") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "macro-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "simple-rijndael") (r "^0.3.2") (d #t) (k 0)))) (h "1ggv0saxn6i7slaifs55grprair7pz9lh8fp1gb03fr0ccyzv1vc")))

