(define-module (crates-io rs s4 rss4mdbook) #:use-module (crates-io))

(define-public crate-rss4mdbook-0.1.42 (c (n "rss4mdbook") (v "0.1.42") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clia-tracing-config") (r "^0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rss") (r "^2.0.2") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1h4iqgr5ij630j9nbzb63a35vwczawx8dw5kh2jm5zij85gzd6g1")))

(define-public crate-rss4mdbook-0.2.42 (c (n "rss4mdbook") (v "0.2.42") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clia-tracing-config") (r "^0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rss") (r "^2.0.2") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0b57cwi2cbiknl6gjjf5x2i2mg9qrh2yyls7g3xm9663fvn4bs2h")))

