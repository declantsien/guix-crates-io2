(define-module (crates-io rs er rserver) #:use-module (crates-io))

(define-public crate-rserver-0.1.0 (c (n "rserver") (v "0.1.0") (h "1hb9gq94ds1pnwdw7c4399fwajw3j502q7f9fb70pgsjc0f9b8fs")))

(define-public crate-rserver-0.1.1 (c (n "rserver") (v "0.1.1") (h "0ibwfw699dddb47js3sk96p950qqldcrifc13p8zlzwcrqwvhgzz")))

(define-public crate-rserver-0.1.2 (c (n "rserver") (v "0.1.2") (h "0blh4nlxizh0j1p9fi5p924h44x8hi2ph47jf9dr06alx5fh3mq9")))

(define-public crate-rserver-0.1.3 (c (n "rserver") (v "0.1.3") (h "0m9d07cbmbqjdipmz16mal0b53cg9ak5dawb1phy79yqfw463hm7")))

(define-public crate-rserver-0.1.4 (c (n "rserver") (v "0.1.4") (h "1wf3bb7f01sjc0jx601rxc05ahkhnkyi8qp802zp1drm91g222h2")))

(define-public crate-rserver-0.1.5 (c (n "rserver") (v "0.1.5") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlparse") (r "^0.7.3") (d #t) (k 0)))) (h "06qj3wa89acp0njk0c74nhbpvbfawqylirzxydvjanrvw25p8yzr")))

(define-public crate-rserver-0.1.6 (c (n "rserver") (v "0.1.6") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlparse") (r "^0.7.3") (d #t) (k 0)))) (h "0svamvsqm4kd3a80lr78n3v3d12lcgf6v6bzhnymjd6pm7jgzc66")))

