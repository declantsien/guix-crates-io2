(define-module (crates-io rs er rserve) #:use-module (crates-io))

(define-public crate-rserve-0.1.0 (c (n "rserve") (v "0.1.0") (d (list (d (n "actix-cors") (r "^0.6.4") (d #t) (k 0)) (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y8zisjzvq0sd6fhjiw838881mb9l2w5i7rnr174wn52mmj4d0p5")))

(define-public crate-rserve-0.1.1 (c (n "rserve") (v "0.1.1") (d (list (d (n "actix-cors") (r "^0.6.4") (d #t) (k 0)) (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "025k3hazhglbmmi89avyphvfidg1zplfv4302i6g2kybvcg4znn2")))

(define-public crate-rserve-0.1.2 (c (n "rserve") (v "0.1.2") (d (list (d (n "actix-cors") (r "^0.6.4") (d #t) (k 0)) (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)))) (h "15ha2519yq27ifxhydb8yq71rys43j8yiyj72ffnix55cv4if81g")))

(define-public crate-rserve-0.1.3 (c (n "rserve") (v "0.1.3") (d (list (d (n "actix-cors") (r "^0.6.4") (d #t) (k 0)) (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)))) (h "1yffbpxrh6p8kjirsgy9zqqh2j4kfin1j525p543sjc8pi33zg3n")))

(define-public crate-rserve-0.1.4 (c (n "rserve") (v "0.1.4") (d (list (d (n "actix-cors") (r "^0.6.4") (d #t) (k 0)) (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)))) (h "0561aascbcr6d07svy8s7jd184fkwr0jqzfiiaf1nqs5c1vlj4z5")))

(define-public crate-rserve-0.1.5 (c (n "rserve") (v "0.1.5") (d (list (d (n "actix-cors") (r "^0.6.4") (d #t) (k 0)) (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "find_port") (r "^0.1.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)))) (h "0jlg8ilcnmwjxwrjmyabwfjcm7wyg0is63pbcq7g4b5ysj6rbbas")))

(define-public crate-rserve-0.1.6 (c (n "rserve") (v "0.1.6") (d (list (d (n "actix-cors") (r "^0.6.4") (d #t) (k 0)) (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "find_port") (r "^0.1.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)))) (h "1d3wf3bkvw7bns9cpw9ifzhlv7d54ghswrxf7pgzskv7fh4fdq51")))

(define-public crate-rserve-0.1.7 (c (n "rserve") (v "0.1.7") (d (list (d (n "actix-cors") (r "^0.6.4") (d #t) (k 0)) (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "find_port") (r "^0.1.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.6") (d #t) (k 0)))) (h "13awlkwr00khfxndcf52nsni27gsibk6gdq0iavgs3hqwpsk7bm0")))

