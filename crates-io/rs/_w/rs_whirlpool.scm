(define-module (crates-io rs _w rs_whirlpool) #:use-module (crates-io))

(define-public crate-rs_whirlpool-0.1.0 (c (n "rs_whirlpool") (v "0.1.0") (h "0ba3m09hxg30phfmhmg98v2rfwnv3d65hal0s8cazammfjmyfaz5")))

(define-public crate-rs_whirlpool-0.1.1 (c (n "rs_whirlpool") (v "0.1.1") (h "1f1652sk1fn9rcpj4qi38n2gdmjhpfa6b3f809mnvfcd0wg06yrh")))

(define-public crate-rs_whirlpool-0.1.2 (c (n "rs_whirlpool") (v "0.1.2") (h "118400v75q39ry35355r772lmh4bb0gch7zzzzn8lbhvqyv5bq6h")))

