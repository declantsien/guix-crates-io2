(define-module (crates-io rs _w rs_ws281x) #:use-module (crates-io))

(define-public crate-rs_ws281x-0.1.0 (c (n "rs_ws281x") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0hrgshx1z98a2jimfyhjxgdcmdiknz12b21nirb8fnrjh09yw4v2")))

(define-public crate-rs_ws281x-0.1.1 (c (n "rs_ws281x") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.26") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1paz1g91m54xjs5n4vf2kjp0qnb0hknszn1rfrq2gzf3vh0476yv")))

(define-public crate-rs_ws281x-0.2.0 (c (n "rs_ws281x") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.26") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0czj3ws5dm4588c33mz0pk1i4nx9hbbj5j5sb2dg5kyg3zbj28my")))

(define-public crate-rs_ws281x-0.2.1 (c (n "rs_ws281x") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wbxv5y95l56pvamijwfz9lb37pi5lrsi4ms9yw98grwjgrskh9d")))

(define-public crate-rs_ws281x-0.2.2 (c (n "rs_ws281x") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0jkv9jzz0dbcm7r6nyr4a5d8s6nyajpkiwl2v3ijd5r1i01dgqwk")))

(define-public crate-rs_ws281x-0.2.3 (c (n "rs_ws281x") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1chrqsrywm8p9lqd5m2bp27g46ybqzpmxhnrnp5j517ff1mic4bh")))

(define-public crate-rs_ws281x-0.3.1 (c (n "rs_ws281x") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.54") (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1i859yhn053w55j14g42zi4q1cx0zi3by46jlqdmaycrkks43363")))

(define-public crate-rs_ws281x-0.4.1 (c (n "rs_ws281x") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ia8na7cbkvzy1020dr1lyqkp0c0ksl4apq2qsz6121am40lzjfl")))

(define-public crate-rs_ws281x-0.4.2 (c (n "rs_ws281x") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.54") (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1jhngwc13wrmfffzxqgzgizdvw3rl92l14cgl6kxicwrphwvg840")))

(define-public crate-rs_ws281x-0.4.4 (c (n "rs_ws281x") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.60") (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1y04iq56qj6nx4aq06q9rfl5zlj7xpip61jiil4p7rkis40j6vnq")))

(define-public crate-rs_ws281x-0.5.0 (c (n "rs_ws281x") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.60") (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "04nhgq4349wdsl5pcp4y1m44nsj6pfc1y8wx4ycmvzpyq55vjaxb")))

(define-public crate-rs_ws281x-0.5.1 (c (n "rs_ws281x") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.69") (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "053dcpz9qw05r0inap1rkkm73xwjnvqqygyhwmz7dg7faj649lc9")))

