(define-module (crates-io rs _w rs_web_component) #:use-module (crates-io))

(define-public crate-rs_web_component-0.1.0 (c (n "rs_web_component") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement"))) (d #t) (k 0)))) (h "1n239s4knx53qwgliyw2hnsfgwdbamg63c1a500ynfrlqmlnjy0b")))

(define-public crate-rs_web_component-0.1.1 (c (n "rs_web_component") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement"))) (d #t) (k 0)))) (h "1hmhv25fl29mcixhmmxsw676qrzc3hd2ypkcqa5spnvrwlywsr50")))

(define-public crate-rs_web_component-0.1.2 (c (n "rs_web_component") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement"))) (d #t) (k 0)))) (h "1z1yy9y6k8hr4i0qwk83anfm0cw1s36vlcld65l1jm21db2n85gc")))

(define-public crate-rs_web_component-0.1.3 (c (n "rs_web_component") (v "0.1.3") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement" "Document" "Window"))) (d #t) (k 0)))) (h "0m6k0nq8mrw7igi08h340izjlfybp9lizgcd9vx3x2dka1a062pc")))

(define-public crate-rs_web_component-0.1.4 (c (n "rs_web_component") (v "0.1.4") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement" "Document" "Window"))) (d #t) (k 0)))) (h "17s6zihmb5kmdci78r13ir36k9rqx2vqy3fg7c29n4vnixm0rxis")))

(define-public crate-rs_web_component-0.1.5 (c (n "rs_web_component") (v "0.1.5") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement" "Document" "Window"))) (d #t) (k 0)))) (h "0cgrmk2r6bqqqldkr7wrl6ilkxsni09a3cxzhc18s62xd7wb705s")))

(define-public crate-rs_web_component-0.1.6 (c (n "rs_web_component") (v "0.1.6") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement" "Document" "Window"))) (d #t) (k 0)))) (h "1rj4mzhq407pbyw6xdpalrg7yg1yqm40a14kg3y760a0cm3v3ql1")))

(define-public crate-rs_web_component-0.1.7 (c (n "rs_web_component") (v "0.1.7") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("HtmlElement" "Document" "Window"))) (d #t) (k 0)))) (h "0q8zlskciwsfd7i5hzw4awqvvqs4hjp6xd89ars9v5ay4jdb0032")))

