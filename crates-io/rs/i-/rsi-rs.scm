(define-module (crates-io rs i- rsi-rs) #:use-module (crates-io))

(define-public crate-rsi-rs-0.1.0 (c (n "rsi-rs") (v "0.1.0") (d (list (d (n "sma-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "ta-common") (r "^0.1.2") (d #t) (k 0)) (d (n "wilders-rs") (r "^0.1.1") (d #t) (k 0)))) (h "113ss44qijivbwdx22vlqar1lfilm99aqkzdw9sgg3gw72garl2y")))

