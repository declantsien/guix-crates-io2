(define-module (crates-io rs _i rs_idea) #:use-module (crates-io))

(define-public crate-rs_idea-0.1.0 (c (n "rs_idea") (v "0.1.0") (h "1qrbl9bf1rr2n1654z88dy8978vq37yyss17xi5g82q66ijyhcal")))

(define-public crate-rs_idea-0.1.1 (c (n "rs_idea") (v "0.1.1") (h "1jzcjwxpinajj3glyjgrmn7978x4a281ncqvzcwsina83bjxadkp")))

(define-public crate-rs_idea-0.1.2 (c (n "rs_idea") (v "0.1.2") (h "02a6n37l9rqrf720xmnbx3f5i56gc7hzm3i6bqzb3gnrc2r6xhvx")))

