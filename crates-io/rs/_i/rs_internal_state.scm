(define-module (crates-io rs _i rs_internal_state) #:use-module (crates-io))

(define-public crate-rs_internal_state-0.1.1 (c (n "rs_internal_state") (v "0.1.1") (d (list (d (n "rs_n_bit_words") (r "^0.1.1") (d #t) (k 0)))) (h "1vcnfcv830zddxfc25g2r0b3y1zzl6imgmsv77k1m086j2x0byn6")))

(define-public crate-rs_internal_state-0.1.2 (c (n "rs_internal_state") (v "0.1.2") (d (list (d (n "rs_n_bit_words") (r "^0.1.1") (d #t) (k 0)))) (h "0jj4bx36hbpmbrqil9h9m5gfskdgfm50rz3wqj0pd98q7v8k275b")))

(define-public crate-rs_internal_state-0.1.3 (c (n "rs_internal_state") (v "0.1.3") (d (list (d (n "rs_n_bit_words") (r "^0.1.1") (d #t) (k 0)))) (h "099nzlwwn1dbhjqz3rb6mb16qj99m3378n372pb53dn5zqklwji1")))

