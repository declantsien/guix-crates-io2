(define-module (crates-io rs _i rs_internal_hasher) #:use-module (crates-io))

(define-public crate-rs_internal_hasher-0.1.1 (c (n "rs_internal_hasher") (v "0.1.1") (d (list (d (n "rs_internal_state") (r "^0.1.1") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.1") (d #t) (k 0)))) (h "0a562rm95zijrwg7gkl1pzfc532ajz53nyi2r4nq59kqfd44cacg")))

(define-public crate-rs_internal_hasher-0.1.2 (c (n "rs_internal_hasher") (v "0.1.2") (d (list (d (n "rs_internal_state") (r "^0.1.1") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.1") (d #t) (k 0)))) (h "09wsj4jjkdjiipnz2z7lsbnflf2frbhp27rhygr28byr9lryl8iv")))

(define-public crate-rs_internal_hasher-0.1.3 (c (n "rs_internal_hasher") (v "0.1.3") (d (list (d (n "rs_internal_state") (r "^0.1.1") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.1") (d #t) (k 0)))) (h "09n44ybnlbn70pw5pxf3lx5i6vj6304k7xmijplr5yyp2dy4nx8r")))

