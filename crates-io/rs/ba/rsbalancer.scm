(define-module (crates-io rs ba rsbalancer) #:use-module (crates-io))

(define-public crate-rsbalancer-0.1.0 (c (n "rsbalancer") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12s5rpdww48jzaf80aldcwd2527p4wp1bxz24bc18kb5f8d07pz8")))

(define-public crate-rsbalancer-0.2.0 (c (n "rsbalancer") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1cnja5dj7fs9f0fi9bpjq4zwwzaxx5k6hi47sfyj2rsaflwmh656")))

(define-public crate-rsbalancer-0.3.0 (c (n "rsbalancer") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03dc2vrm5qrq8aw0cjvmgppa7bhjvh1xn1g0cpm56y778hav2sv8")))

