(define-module (crates-io rs pw rspw) #:use-module (crates-io))

(define-public crate-rspw-0.1.0 (c (n "rspw") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19gn4jhmbdhgj2k167dyamvh02i7lhh65csfkl41s0g4y6p1nnsb")))

(define-public crate-rspw-0.1.1 (c (n "rspw") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "038frzdwipdvsn3r8s4dxmxwx84ci5wjwda9kfa57i3ckcply12x")))

(define-public crate-rspw-0.2.0 (c (n "rspw") (v "0.2.0") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02lnw86nvn30kj3y3xlxix96rjfsqd0rs1rpq7d0kprnyp7llg86")))

(define-public crate-rspw-0.2.1 (c (n "rspw") (v "0.2.1") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1glmaa5cx248jzbnbn32yj60d8fzw1xx6lf59x81q84rr2sxz32l")))

(define-public crate-rspw-0.2.2 (c (n "rspw") (v "0.2.2") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13vbqldp0sw520783y0rvj6fs95wvp02wj4qjrgzi603cccwaqqg")))

