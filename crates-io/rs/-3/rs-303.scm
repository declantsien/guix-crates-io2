(define-module (crates-io rs #{-3}# rs-303) #:use-module (crates-io))

(define-public crate-rs-303-0.0.0 (c (n "rs-303") (v "0.0.0") (h "1n3lw00kqjv1yx4gdzdcvvryglxynq2nvyh3kmw8xhv2rf6cxz98")))

(define-public crate-rs-303-0.0.1 (c (n "rs-303") (v "0.0.1") (h "1wkhqzvm3kc4v2mxz6z4qfbf0b1g3plf1c5b7mkw716zz47mb9jg")))

