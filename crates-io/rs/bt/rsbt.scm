(define-module (crates-io rs bt rsbt) #:use-module (crates-io))

(define-public crate-rsbt-0.1.0 (c (n "rsbt") (v "0.1.0") (h "0mvkafzyhj80rmj8asp6b66x0d2m7sbn2q1h88c86wkjni0jy2v4")))

(define-public crate-rsbt-0.1.1 (c (n "rsbt") (v "0.1.1") (h "0k70kiq5hmn5smmcncykhd7kkn28d61ljcij2gkcx8m2m2g8f4kd")))

