(define-module (crates-io rs ni rsniff) #:use-module (crates-io))

(define-public crate-rsniff-0.1.0 (c (n "rsniff") (v "0.1.0") (d (list (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1dkizijz7gjd6pab7hvgkwb4lql329acbz91xwyrsvr0zk45l7b1")))

(define-public crate-rsniff-0.1.1 (c (n "rsniff") (v "0.1.1") (d (list (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "07i32d534d2hkv4kly2g9wrd4h058ilxiqpwhq8z8vi0zjj6hqy7")))

