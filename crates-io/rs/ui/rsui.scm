(define-module (crates-io rs ui rsui) #:use-module (crates-io))

(define-public crate-rsui-0.1.0 (c (n "rsui") (v "0.1.0") (d (list (d (n "piston_window") (r "^0.87.0") (d #t) (k 0)) (d (n "pistoncore-glfw_window") (r "^0.47.0") (d #t) (k 0)))) (h "1dqagqcqqgn36bfkq1hh6lzw2nzcv28dd69paha37adc7frywv5i")))

(define-public crate-rsui-0.4.0 (c (n "rsui") (v "0.4.0") (d (list (d (n "piston_window") (r "^0.87.0") (d #t) (k 0)) (d (n "pistoncore-glfw_window") (r "^0.47.0") (d #t) (k 0)))) (h "0sp0a52cxnpklirnkjjp6vcbim7xwk0sjn724rh0dv4c47p4z5y0")))

(define-public crate-rsui-0.10.0 (c (n "rsui") (v "0.10.0") (d (list (d (n "piston_window") (r "^0.87.0") (d #t) (k 0)) (d (n "pistoncore-glfw_window") (r "^0.47.0") (d #t) (k 0)))) (h "0bhf41pfgnc16ivzivx04figb9jca79b22vni3k7rmjxd1hxbiqf")))

(define-public crate-rsui-0.11.0 (c (n "rsui") (v "0.11.0") (d (list (d (n "piston_window") (r "^0.87.0") (d #t) (k 0)) (d (n "pistoncore-glfw_window") (r "^0.47.0") (d #t) (k 0)))) (h "0blhfvmrp2r4qshmcvwva3mwz24x3hs27hslykvq2yzzzzz6cihf")))

(define-public crate-rsui-0.12.0 (c (n "rsui") (v "0.12.0") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.87.0") (d #t) (k 0)) (d (n "pistoncore-glfw_window") (r "^0.47.0") (d #t) (k 0)))) (h "13kfsppafmvd16radmca71ccr5j401a4i56h4ns5yzifj171brcy")))

(define-public crate-rsui-0.13.0 (c (n "rsui") (v "0.13.0") (d (list (d (n "piston_window") (r "^0.87.0") (d #t) (k 0)) (d (n "pistoncore-glfw_window") (r "^0.47.0") (d #t) (k 0)))) (h "17najwd4k8svlzs1b3bb7x2k7pyyyin9p5gw44ghd548cgwa6xbm")))

