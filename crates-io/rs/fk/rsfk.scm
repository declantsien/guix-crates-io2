(define-module (crates-io rs fk rsfk) #:use-module (crates-io))

(define-public crate-rsfk-0.1.0 (c (n "rsfk") (v "0.1.0") (d (list (d (n "input-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rsfk-core") (r "^0.1.0") (d #t) (k 0)))) (h "04h6lwlzbgm7c47ihhhp1ld4cd0la20wazvq8bgf267b569b9qqf")))

