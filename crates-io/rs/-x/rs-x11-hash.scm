(define-module (crates-io rs -x rs-x11-hash) #:use-module (crates-io))

(define-public crate-rs-x11-hash-0.1.2 (c (n "rs-x11-hash") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "07bmc1cl359wxncjh8ndrwawq6fxswd19lmnjp9cqdrhpmamdjrb") (y #t)))

(define-public crate-rs-x11-hash-0.1.3 (c (n "rs-x11-hash") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "028xa1dynx7bxq6ixdqs31cgzmna1d75a3zm1rwmyhzrxg0412w9") (y #t)))

(define-public crate-rs-x11-hash-0.1.4 (c (n "rs-x11-hash") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "15f5n3h0wyvb4y2m5zsxgnkkl5hh803jf9bv4v6l5yyyri2gi116")))

(define-public crate-rs-x11-hash-0.1.5 (c (n "rs-x11-hash") (v "0.1.5") (d (list (d (n "bindgen") (r ">=0.57.0, <=0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "0z2vi0kbrpq0rq6rc0d4ga3ii4cawd7wpda0m38xwzg6ib62lh6a")))

(define-public crate-rs-x11-hash-0.1.6 (c (n "rs-x11-hash") (v "0.1.6") (d (list (d (n "bindgen") (r ">=0.57.0, <=0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "0fm27mmbyccgcwvhxv034cxsvkyzibm91nl38m6mgxhyv6sla60a")))

(define-public crate-rs-x11-hash-0.1.7 (c (n "rs-x11-hash") (v "0.1.7") (d (list (d (n "bindgen") (r ">=0.57.0, <=0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "0vl9vzp00xpkkn7aq8nx44ivj9n308ag4rkbck52m7k7wy3f2h9i")))

(define-public crate-rs-x11-hash-0.1.8 (c (n "rs-x11-hash") (v "0.1.8") (d (list (d (n "bindgen") (r ">=0.57.0, <=0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "0c1si5bn50qsdisk7l4fl60q9g3m0cq2613msxgnygai0ql8bsll")))

