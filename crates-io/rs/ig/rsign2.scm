(define-module (crates-io rs ig rsign2) #:use-module (crates-io))

(define-public crate-rsign2-0.2.0 (c (n "rsign2") (v "0.2.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rpassword") (r "^3.0") (d #t) (k 0)) (d (n "scrypt") (r "^0.2") (k 0)))) (h "17lx5hfsykmnvxmdp62qygycrwkk1948hv5bfm6fg1w1hdf3slv2")))

(define-public crate-rsign2-0.2.1 (c (n "rsign2") (v "0.2.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rpassword") (r "^3.0") (d #t) (k 0)) (d (n "scrypt") (r "^0.2") (k 0)))) (h "1blvpa2d8n54igwdidksk348il4r5k8d08blbx935dami7f0bprp")))

(define-public crate-rsign2-0.2.2 (c (n "rsign2") (v "0.2.2") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rpassword") (r "^3.0") (d #t) (k 0)) (d (n "scrypt") (r "^0.2") (k 0)))) (h "1lkrxrs77zk7411dlvvkvl8dvp1ks2zr64h0s75kkxhahjyhgwi3")))

(define-public crate-rsign2-0.3.0 (c (n "rsign2") (v "0.3.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "minisign") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rpassword") (r "^3.0") (d #t) (k 0)))) (h "10g7rgf74k9s8s4pfkpba04p9a0yfz9iqpyl3wc5vdkd1v36agbz")))

(define-public crate-rsign2-0.4.0 (c (n "rsign2") (v "0.4.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "minisign") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rpassword") (r "^3.0") (d #t) (k 0)))) (h "1y44k9z8xx2fyjsf3ic0ds524brl5l0q4sib83ak2kk7jbbxdky7")))

(define-public crate-rsign2-0.5.0 (c (n "rsign2") (v "0.5.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "minisign") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rpassword") (r "^3.0") (d #t) (k 0)))) (h "1bv92wny8alpbalkqmh1m6kcxvp2yd9a6fn5hjj9ndjbxbcf9kzc")))

(define-public crate-rsign2-0.5.1 (c (n "rsign2") (v "0.5.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.5") (d #t) (k 0)))) (h "1gsrflj0i6pb51bsbj0qxvdfk83xh2l05h6r75l1pb8ajiid440n")))

(define-public crate-rsign2-0.5.4 (c (n "rsign2") (v "0.5.4") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.5") (d #t) (k 0)))) (h "1fhnl2b45qxac82w9xsgyagjvgdj80r4dkdj85ynb2d0pv30ks9w")))

(define-public crate-rsign2-0.5.2 (c (n "rsign2") (v "0.5.2") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.5.9") (d #t) (k 0)))) (h "0imrbrb2p0abbc46c0smnb2i113y40fgp55g8zd7zjx38j1k5d89")))

(define-public crate-rsign2-0.5.5 (c (n "rsign2") (v "0.5.5") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.5.14") (d #t) (k 0)))) (h "1iqz3xzvydrzhlzqrr1vvhh73nfpq60n1m6l994xnwxr639ac035")))

(define-public crate-rsign2-0.5.6 (c (n "rsign2") (v "0.5.6") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.5.15") (d #t) (k 0)))) (h "0mbh9mgn7zdchxx7051xjhg3209spvy35ji2g1mz350wmdvi8fiq")))

(define-public crate-rsign2-0.5.7 (c (n "rsign2") (v "0.5.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs-next") (r "^1.0.0") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.5.16") (d #t) (k 0)))) (h "0701d77yv2x1m03vdkax0rpm4b3bzinkkk2m7sqsfd9ad7d4lp1a")))

(define-public crate-rsign2-0.6.0 (c (n "rsign2") (v "0.6.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.7") (d #t) (k 0)))) (h "05vw5pk7ip2g2872cl83fjpd6jmk5lxx6ijd1miam334h7lryyjl")))

(define-public crate-rsign2-0.6.1 (c (n "rsign2") (v "0.6.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.7") (d #t) (k 0)))) (h "09rin2j3cdfsbmn8kzg5v5cl4ibqnnkl2mb617nhag7i9aq8bj2b")))

(define-public crate-rsign2-0.6.2 (c (n "rsign2") (v "0.6.2") (d (list (d (n "clap") (r "^3") (f (quote ("std" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.7") (d #t) (k 0)))) (h "1i7cr1h14hx1g2ms896jf954zpg40hpdbdrgwf0p05qdfpni96qf")))

(define-public crate-rsign2-0.6.3 (c (n "rsign2") (v "0.6.3") (d (list (d (n "clap") (r "^4") (f (quote ("std" "cargo" "wrap_help" "string"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (t "cfg(any(windows, unix))") (k 0)) (d (n "minisign") (r "^0.7.5") (d #t) (k 0)))) (h "083n7zyjpj0j1hfg15l9ygm476nlmwb08x4wxfvgf8n1aiclwwfx")))

