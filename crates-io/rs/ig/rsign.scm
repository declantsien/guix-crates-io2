(define-module (crates-io rs ig rsign) #:use-module (crates-io))

(define-public crate-rsign-0.1.0 (c (n "rsign") (v "0.1.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.0.15") (d #t) (k 0)) (d (n "rpassword") (r "^1.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.15") (d #t) (k 0)))) (h "1rw2c3db5qggpn9k7ywbphhrwg2wm1s7dbnynjrxf26z8wdihk56") (y #t)))

(define-public crate-rsign-0.1.1 (c (n "rsign") (v "0.1.1") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.0.15") (d #t) (k 0)) (d (n "rpassword") (r "^1.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.15") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)))) (h "1b0136rj105zy1m16g1skwlzsgrjv06myq9y13n1zvj1vnkgjr26") (y #t)))

(define-public crate-rsign-0.1.2 (c (n "rsign") (v "0.1.2") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.0.15") (d #t) (k 0)) (d (n "rpassword") (r "^1.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.15") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)))) (h "14li9san9x7hgvfy1brwkj3smrq55qp078q62xh95mn8g930k49y")))

