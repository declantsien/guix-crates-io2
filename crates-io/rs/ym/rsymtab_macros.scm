(define-module (crates-io rs ym rsymtab_macros) #:use-module (crates-io))

(define-public crate-rsymtab_macros-0.1.0 (c (n "rsymtab_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1p1v4dc80xlqz11lfc8xrhmiv30yg3fpvbqwmg4pvai74cwsa8hj")))

