(define-module (crates-io rs py rspy) #:use-module (crates-io))

(define-public crate-rspy-0.1.0 (c (n "rspy") (v "0.1.0") (h "1kf9v623vk2ix4nkkfji86gag0psxhr04r6dxh4afbw99ili4wy5")))

(define-public crate-rspy-0.1.1 (c (n "rspy") (v "0.1.1") (h "0c398q312wjnn4iyp352s3lqzvni5n77jknbp89pjpr3icgj6lfc")))

(define-public crate-rspy-0.1.2 (c (n "rspy") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1pdh6fczi9vmchp51scihi5h6xbhhz0bp7fa7cvfbvy9kqph4qkd")))

(define-public crate-rspy-0.1.3 (c (n "rspy") (v "0.1.3") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0mkanbh20cbgvs60a73hw1ccx5m9yd1lzw321q5asly3sy5rpajr")))

(define-public crate-rspy-0.1.4 (c (n "rspy") (v "0.1.4") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0ki44b6v6nrj91jgy864vs0bhmqrg1r1j4pja4f6ks1kj1ljvxip")))

(define-public crate-rspy-0.1.5 (c (n "rspy") (v "0.1.5") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1hrark7414fl3hqihqhc65kwshrb0q5wb4x9vza8nxlxcg9ypgk0")))

(define-public crate-rspy-0.1.6 (c (n "rspy") (v "0.1.6") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "13hqbzv34k3m4gk4wmfdxz37vc88qvlriak4vr2rk4zklh31nsma")))

(define-public crate-rspy-0.1.7 (c (n "rspy") (v "0.1.7") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "05n0narzmxlqzw64m1i4zpcnnbgv2mp42f1aiwpn3z3vqzbnsxca")))

(define-public crate-rspy-0.1.8 (c (n "rspy") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "06x67r4val9pcj5k14b9vva1w97h2ggfr1wmx3wjaj9x9scws4c1")))

(define-public crate-rspy-0.1.9 (c (n "rspy") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1y8b0vnrb64fpmgv53svsk0x5bkk52p1k5bldc2sspllvncclakr")))

(define-public crate-rspy-0.1.10 (c (n "rspy") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0qk5q0237476iv1wwa628y6zssl0ibv030ri6608p598j86n0krr")))

(define-public crate-rspy-0.1.11 (c (n "rspy") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1f02h5jnkd9488my71abw37a6d8syxk6mdd68vwmcqzimk1h36w5")))

