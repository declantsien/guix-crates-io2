(define-module (crates-io rs no rsnowflake) #:use-module (crates-io))

(define-public crate-rsnowflake-0.0.3 (c (n "rsnowflake") (v "0.0.3") (h "0rgcn9l61xicjgkabyw4pb36fins2j7zlcxpdh1pkvldxnbfkzsa")))

(define-public crate-rsnowflake-0.1.0 (c (n "rsnowflake") (v "0.1.0") (h "0sjm46lzi5ch1dkbak7a1cybyrbn12x4mbxm8xz3zm1rmv3s4970")))

(define-public crate-rsnowflake-0.1.1 (c (n "rsnowflake") (v "0.1.1") (h "0zwhd730j3lzzxij6vhxwk187cs0pnfb45cr6vkcax8hp5jjnqwr")))

