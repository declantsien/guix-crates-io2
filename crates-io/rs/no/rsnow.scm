(define-module (crates-io rs no rsnow) #:use-module (crates-io))

(define-public crate-rsnow-0.1.0 (c (n "rsnow") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19k5h2pzx0k1yd27r53kcc8ghpbda4bdzk32fvz7wb2naaaykmnx")))

(define-public crate-rsnow-0.1.1 (c (n "rsnow") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h242dmx6gznr0h664bmyrycrxpmabmj4plicbydrancklfy61jm")))

