(define-module (crates-io rs no rsnotifyos) #:use-module (crates-io))

(define-public crate-rsnotifyos-0.1.0 (c (n "rsnotifyos") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gtk") (r "^0.9") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "shellapi" "errhandlingapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "175jnxxxdhbn0byk41dvqx6l00gry9svnfki7d66m8wjdqcs9b5q") (f (quote (("enable_debug"))))))

(define-public crate-rsnotifyos-0.1.1 (c (n "rsnotifyos") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gtk") (r "^0.9") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "shellapi" "errhandlingapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0kk8add9snxfx22c2fwlv12fc24fyz81i5a100kx6klx3nyvswcp") (f (quote (("enable_debug"))))))

