(define-module (crates-io rs us rsush) #:use-module (crates-io))

(define-public crate-rsush-0.1.0 (c (n "rsush") (v "0.1.0") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 0)))) (h "0fpd5bxp72snzh4lkvxgf8zdyb3zzb81mqg6s9ppm3nwl19qxfiv")))

(define-public crate-rsush-0.1.1 (c (n "rsush") (v "0.1.1") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 0)))) (h "1z2pl7pgmnjjl6mkhsbapx2v544pfy8q6ah56s2zahfnpz4rzdn8")))

(define-public crate-rsush-0.1.2 (c (n "rsush") (v "0.1.2") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 0)))) (h "1s1kdp05cwaskmn37rk3db1p2fmar1qi1prgkr0yjhdjr0x50f30")))

(define-public crate-rsush-0.1.3 (c (n "rsush") (v "0.1.3") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 0)))) (h "1gq4qzmy6wk5hsp8k11f3ycaf1sj228jfmhascvlf9gdl22qazwc")))

(define-public crate-rsush-0.1.4 (c (n "rsush") (v "0.1.4") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 2)))) (h "14f2y936aa3wnfbyngyrd2qsw07wkaddy0379qffv1yy0wy06h6x")))

(define-public crate-rsush-0.1.5 (c (n "rsush") (v "0.1.5") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 2)))) (h "0n8n9n09rwnv0crxsfpsc69p748kplqn7r74dpjx3q25j1ri6izh")))

(define-public crate-rsush-0.2.0 (c (n "rsush") (v "0.2.0") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 2)))) (h "153jwpwq300arkrhwi10mdyqvbwzx1nm8q451skq3ai93r68zzwr")))

(define-public crate-rsush-0.2.1 (c (n "rsush") (v "0.2.1") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 2)))) (h "1pg6f270zfzixh4zx2yx8acgydpk4dfa5mj94l17xc9prbln6zi2")))

(define-public crate-rsush-0.2.2 (c (n "rsush") (v "0.2.2") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 2)))) (h "1rn90cnrnhizigkmdjn0j5a4qlacgmbwk69qn1vqdhbnaspaxg4c")))

(define-public crate-rsush-0.2.3 (c (n "rsush") (v "0.2.3") (d (list (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "sealed_test") (r "^1.0.0") (d #t) (k 2)))) (h "0fiahzd9jd3yi8njq5cmmfhmr9mkihkna1ajpq6kby184p2jdi45")))

