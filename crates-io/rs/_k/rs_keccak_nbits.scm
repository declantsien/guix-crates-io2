(define-module (crates-io rs _k rs_keccak_nbits) #:use-module (crates-io))

(define-public crate-rs_keccak_nbits-0.1.0 (c (n "rs_keccak_nbits") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "074h4djmal3lyp0fllfqb8ixh1cybbvy5sbhkjhp47262x7c3jvi")))

(define-public crate-rs_keccak_nbits-0.1.1 (c (n "rs_keccak_nbits") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "02c1v64praagdccql8zw5nd72b6ks2rp47imsbf386bj13nhw94k")))

(define-public crate-rs_keccak_nbits-0.1.2 (c (n "rs_keccak_nbits") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "17jbfd0wzvjw8rzg52lw73flqz0j8vzf2n400a1zrhy73r885s38")))

