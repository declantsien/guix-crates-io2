(define-module (crates-io rs _b rs_blake2) #:use-module (crates-io))

(define-public crate-rs_blake2-0.1.0 (c (n "rs_blake2") (v "0.1.0") (h "11hvs5w10qlpf6wnm4yn1pvlgz0inpjamr2db25drxh1xk9br2nx")))

(define-public crate-rs_blake2-0.1.1 (c (n "rs_blake2") (v "0.1.1") (h "0lw9rxb6h6nclaqadqb1akzk5qfz46nnwf80q20n295ck785qb97")))

(define-public crate-rs_blake2-0.1.2 (c (n "rs_blake2") (v "0.1.2") (h "0jm9wr9kla777x2a10y1pxilip38ncx79k3xnb2frd0bmwpih09z")))

