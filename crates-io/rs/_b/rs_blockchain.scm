(define-module (crates-io rs _b rs_blockchain) #:use-module (crates-io))

(define-public crate-rs_blockchain-0.1.0 (c (n "rs_blockchain") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "00m26mk5yv6ycgpxrc2n1zk2ah2wsixmg73xbfhx13m296s4mab0") (f (quote (("std" "serde") ("default" "std")))) (y #t)))

(define-public crate-rs_blockchain-0.1.1 (c (n "rs_blockchain") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "1spgz9b1s1affj19rizqfhk518rbpkzp9kw3b6kbcpbmxhxp2wwh") (f (quote (("std" "serde") ("default" "std")))) (y #t)))

(define-public crate-rs_blockchain-0.1.5 (c (n "rs_blockchain") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "1nkfjgnawggc7w3jaib83djb0869k8psxyjf1ha3i1h96q7dx7c9") (f (quote (("std" "serde") ("default" "std")))) (y #t)))

(define-public crate-rs_blockchain-0.1.7 (c (n "rs_blockchain") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "03ibk5k4d92s2igz3y2lran9ggvi0hzjzp1v7a382pdks6w87y1a") (f (quote (("std" "serde") ("default" "std")))) (y #t)))

(define-public crate-rs_blockchain-0.2.0 (c (n "rs_blockchain") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "1k59npc140a7k9jz6z2mssl1p353f9j3b5phvbkdscnn8dbnrgj6") (y #t)))

(define-public crate-rs_blockchain-0.2.1 (c (n "rs_blockchain") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "1qjq26d2g03m5481vxalz0z356693a15fwr30p1g3y9d4xj7v9x1") (y #t)))

(define-public crate-rs_blockchain-0.2.2 (c (n "rs_blockchain") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "0fwqynpmzmbfsgzf0m9wpz4nnhrghlyjj17bfbmzqqcf8szc5vrl") (y #t)))

(define-public crate-rs_blockchain-0.2.5 (c (n "rs_blockchain") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "12773kizn1b8j6h44lm6p3jqyvxqnj91xkv8l4646vj2bgy62nid") (y #t)))

(define-public crate-rs_blockchain-0.2.6 (c (n "rs_blockchain") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "0pw22g0vpvkfsxkj1ji3r7hcyphynlldkykxk9cxpvmzjls6nkmn") (y #t)))

(define-public crate-rs_blockchain-0.2.7 (c (n "rs_blockchain") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "1h8388xryz8qc8wfw2c59gcni33ssr0qg8mv073lnkaq9swfzmj9") (y #t)))

