(define-module (crates-io rs ca rscam) #:use-module (crates-io))

(define-public crate-rscam-0.2.5 (c (n "rscam") (v "0.2.5") (h "0jh3mi8cdvdilz5zq8mfakzdm9rk055rh1s6a5cjdjlyag5n7710")))

(define-public crate-rscam-0.3.0 (c (n "rscam") (v "0.3.0") (h "15sablj373j07fc36fpgs59vfd2dwkkk81i7lga3ai4v9sf0jqiq")))

(define-public crate-rscam-0.3.5 (c (n "rscam") (v "0.3.5") (h "1gx37fyr0s7x49mn5ylm80yqyghs976g865l14xrydghbxrcay76") (f (quote (("use_wrapper") ("default" "use_wrapper"))))))

(define-public crate-rscam-0.3.6 (c (n "rscam") (v "0.3.6") (h "0j6kc1dn9v1r7p3gws9pb13b3qfzxrvsil164gxaagph47qj95hx") (f (quote (("use_wrapper") ("default" "use_wrapper"))))))

(define-public crate-rscam-0.3.7 (c (n "rscam") (v "0.3.7") (h "0lsv4cipz21bq3lxrlnq9n8gd5r54v42vw8b0lplnq578s2ihk6z") (f (quote (("use_wrapper") ("default" "use_wrapper"))))))

(define-public crate-rscam-0.3.8 (c (n "rscam") (v "0.3.8") (h "00zprzfrk8j3n3c9fnh6w4lhpzr886shn4bik4lamww3fqmsvmpf") (f (quote (("use_wrapper") ("default" "use_wrapper"))))))

(define-public crate-rscam-0.3.9 (c (n "rscam") (v "0.3.9") (h "111hrrmmannl3kx51k4w1il45ifghddfc272q77lg97i8gym6d8a") (f (quote (("use_wrapper") ("default" "use_wrapper"))))))

(define-public crate-rscam-0.3.10 (c (n "rscam") (v "0.3.10") (h "1la4ch8qm7vv12pmqz7d45yi3nxc0dzspn88rw8a6m9gx3qq94kp") (f (quote (("use_wrapper") ("default" "use_wrapper"))))))

(define-public crate-rscam-0.4.0 (c (n "rscam") (v "0.4.0") (h "06gp2jxf7py7zmn5knnkq34yfg796nsk6fhlxmzbm6z7pl18cr15") (f (quote (("no_wrapper"))))))

(define-public crate-rscam-0.4.1 (c (n "rscam") (v "0.4.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0rv0a94lz91z5ihyjidxng91mwq5b7gfx1sjamk3f88xqkylippx") (f (quote (("no_wrapper"))))))

(define-public crate-rscam-0.5.0 (c (n "rscam") (v "0.5.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1kxc99vjvj9fyshkwa5fzi8gn6afgjfh6ij3jdsydwx785snqy89") (f (quote (("no_wrapper"))))))

(define-public crate-rscam-0.5.1 (c (n "rscam") (v "0.5.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ramak8braf8fxpg2zqkvflxyd0r5icfh0nc0xhpmnmk61j2cryj") (f (quote (("no_wrapper"))))))

(define-public crate-rscam-0.5.2 (c (n "rscam") (v "0.5.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1g4pjs1k2bq12w4cln45g7ls5071ydgyf1bgdlci5871nhpv31h7") (f (quote (("no_wrapper"))))))

(define-public crate-rscam-0.5.3 (c (n "rscam") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pnvw7q1v8qcs7b7ibj8dhg0m27m90dgrvlgki3pwk6ac8sjy0aa") (f (quote (("no_wrapper"))))))

(define-public crate-rscam-0.5.4 (c (n "rscam") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x9yvm3m8ynj1r1i5580jdik0nfisk2wc020kd5jvffh772r1rzi") (f (quote (("no_wrapper"))))))

(define-public crate-rscam-0.5.5 (c (n "rscam") (v "0.5.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "063h8m11f1ki69z8xzawg4grsh06p6cj2bnzzqj4km8w462601c9") (f (quote (("no_wrapper"))))))

