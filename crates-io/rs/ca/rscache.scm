(define-module (crates-io rs ca rscache) #:use-module (crates-io))

(define-public crate-rscache-0.2.2 (c (n "rscache") (v "0.2.2") (d (list (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "libflate") (r "^0.1.27") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 2)))) (h "1p7cg62f9fba3b3yhqy2sgiprlzy0ddinpwqpkdvpqcjmfx5wf8n") (y #t)))

