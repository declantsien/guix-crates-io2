(define-module (crates-io rs ca rscalc) #:use-module (crates-io))

(define-public crate-rscalc-0.0.1 (c (n "rscalc") (v "0.0.1") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)))) (h "09i7vc7xfffaklsdin9igdajlaj82y6894dvczr7pidrjq9hkqlh") (f (quote (("build_deps" "man"))))))

