(define-module (crates-io rs ca rscash) #:use-module (crates-io))

(define-public crate-rscash-0.0.1 (c (n "rscash") (v "0.0.1") (d (list (d (n "bech32") (r "^0.7.1") (d #t) (k 0)) (d (n "bitcoin") (r "^0.21.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7") (d #t) (k 0)) (d (n "bitcoinconsensus") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hex") (r "= 0.3.2") (d #t) (k 0)) (d (n "secp256k1") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "13rgpd6imazwzj1sy76kml2baz95vii4fi26dfpazrahnlpbdzn8")))

