(define-module (crates-io rs ca rscan) #:use-module (crates-io))

(define-public crate-rscan-0.1.0 (c (n "rscan") (v "0.1.0") (d (list (d (n "pnet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1l3gv4dji7fa6qj892p3rmhap0yn0kp1aa6af81xgms49aqh3yid")))

(define-public crate-rscan-0.2.0 (c (n "rscan") (v "0.2.0") (d (list (d (n "pnet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0c5i8i9x7si4ai8m8fq1qll0vg6wbjaa5dpqfasv98q70gjafazg")))

(define-public crate-rscan-0.2.1 (c (n "rscan") (v "0.2.1") (d (list (d (n "pnet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0sl6jcy5scvqcfpv6p2dyxd4ly1lk9bx5h6zsym1csk7i3j1xarp")))

