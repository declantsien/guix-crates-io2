(define-module (crates-io rs ca rscat) #:use-module (crates-io))

(define-public crate-rscat-0.1.0 (c (n "rscat") (v "0.1.0") (h "09yrv1chvh994wgf6k967l19ssdfrlvnh41mqqahvcgxv509khvc")))

(define-public crate-rscat-0.1.1 (c (n "rscat") (v "0.1.1") (h "0fqscnmvwskbrvnw8dk3y1987ggm11dzgakn06wj7hvv50v03akc")))

