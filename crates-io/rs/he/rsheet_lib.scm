(define-module (crates-io rs he rsheet_lib) #:use-module (crates-io))

(define-public crate-rsheet_lib-0.1.0 (c (n "rsheet_lib") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rhai") (r "^1.17.1") (f (quote ("internals" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0ixhqv0jhsxsjz7sh2klywp95s25ccfwzagwzwardqacwcra6siy")))

(define-public crate-rsheet_lib-0.1.1 (c (n "rsheet_lib") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rhai") (r "^1.17.1") (f (quote ("internals" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "10nfvsy8sb2pcl0cd7nrd02hj5wgl0hxlr8ja6jcpbia4hc4cmc6")))

(define-public crate-rsheet_lib-0.1.2 (c (n "rsheet_lib") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rhai") (r "^1.17.1") (f (quote ("internals" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "15vkvmp8drmwcc0s6c30xsxpj3napjrpvqbfbb1v25qrjw7h5mxj")))

(define-public crate-rsheet_lib-0.1.3 (c (n "rsheet_lib") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rhai") (r "^1.17.1") (f (quote ("internals" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "04wq3shanqb7hapkzs2wznhlrkyrw6ziw40plqnfmgvifllbirdb")))

(define-public crate-rsheet_lib-0.1.4 (c (n "rsheet_lib") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rhai") (r "=1.17.1") (f (quote ("internals" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0x026nfizz65m102xjmqcjmg98dyk3lsppcz9yibwrq4hv5yvrz0")))

