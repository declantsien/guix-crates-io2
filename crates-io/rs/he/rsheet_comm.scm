(define-module (crates-io rs he rsheet_comm) #:use-module (crates-io))

(define-public crate-rsheet_comm-0.1.0 (c (n "rsheet_comm") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1i2a7gcxwg9c6dkdpjvi096484jlfswdffwzrg132r3rq45s55qz")))

