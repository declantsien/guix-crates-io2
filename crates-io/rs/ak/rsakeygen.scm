(define-module (crates-io rs ak rsakeygen) #:use-module (crates-io))

(define-public crate-rsakeygen-0.1.0 (c (n "rsakeygen") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rsa_keygen") (r "^1.0.6") (d #t) (k 0)))) (h "1qf645794xxwl963svqm19fyfb14zjd903a2x1sk78blnnqdc6ic")))

(define-public crate-rsakeygen-0.1.1 (c (n "rsakeygen") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "rsa_keygen") (r "^1.0.6") (d #t) (k 0)))) (h "01jfjdqsd6gbsprian6a97n25x26hr1666r7g77qxg873d90r38i")))

(define-public crate-rsakeygen-0.1.2 (c (n "rsakeygen") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "rsa_keygen") (r "^1.0.6") (d #t) (k 0)))) (h "1qswwqyq0m388bw6vjcyb1l9achxbyjfpq0n09r6k4j0g5irhzg8")))

