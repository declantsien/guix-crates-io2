(define-module (crates-io rs om rsomeip) #:use-module (crates-io))

(define-public crate-rsomeip-0.1.0 (c (n "rsomeip") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("net" "rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "17f0fldc89ndbi9m2v5sagnss8qj9v1pgl95kgc368pdr6pzczb3")))

