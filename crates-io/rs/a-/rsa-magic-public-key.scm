(define-module (crates-io rs a- rsa-magic-public-key) #:use-module (crates-io))

(define-public crate-rsa-magic-public-key-0.1.0 (c (n "rsa-magic-public-key") (v "0.1.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1znsrxgi6cyiyxnkq40rjqnapvb49c6lla0l72ayfb8dg59r1s6z")))

(define-public crate-rsa-magic-public-key-0.1.1 (c (n "rsa-magic-public-key") (v "0.1.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1b75v1rs1kbk717rg79v6zp0ndyi2ywhn9czdzkaql7xgvip9g0i")))

(define-public crate-rsa-magic-public-key-0.2.0 (c (n "rsa-magic-public-key") (v "0.2.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1hxlvfja4ds0vpdbxn4d896i3mykyz874axlggdq7sbrqzscfn0d")))

(define-public crate-rsa-magic-public-key-0.2.1 (c (n "rsa-magic-public-key") (v "0.2.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1jfhvz64iyp6cz1vm78riki8wdngi4wzm72wqz4n0jkrfy41m9qa")))

(define-public crate-rsa-magic-public-key-0.3.0 (c (n "rsa-magic-public-key") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rsa") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0v83n7d3n5a7x9mkwn8gjdy5vdgx2rzq0ral1wf1q59idh90aqn8")))

(define-public crate-rsa-magic-public-key-0.4.0 (c (n "rsa-magic-public-key") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rsa") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1k29vphvp00jn0jla3sl444ckzgjkhmgfkn75bc7fadn91xm1z9x")))

(define-public crate-rsa-magic-public-key-0.5.0 (c (n "rsa-magic-public-key") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rsa") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1hyr1706njwcn9jy3v1ify8syqpgzi87c589wdbj5ziavv91a44i")))

(define-public crate-rsa-magic-public-key-0.6.0 (c (n "rsa-magic-public-key") (v "0.6.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rsa") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0nf1v5g0gl2k82l1drykdni4vpgs3c27xsp0f7j0xf804k4kbh4d")))

(define-public crate-rsa-magic-public-key-0.7.0 (c (n "rsa-magic-public-key") (v "0.7.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rsa") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1853bgriziq9nvx790pifd0ia4wz50kf4qwxlg7pdqfn4lsbjv58")))

(define-public crate-rsa-magic-public-key-0.8.0 (c (n "rsa-magic-public-key") (v "0.8.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rsa") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0k1l4wi0y49i0vipavlhkrphws7rj9dvcfrwr5a69zn68xlnmjl8")))

