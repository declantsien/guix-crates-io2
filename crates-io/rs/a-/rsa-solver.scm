(define-module (crates-io rs a- rsa-solver) #:use-module (crates-io))

(define-public crate-rsa-solver-0.1.0 (c (n "rsa-solver") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "modinverse") (r "^0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primal-tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1lpl85zsl3xvv3mr1hgjwkbmppfjcj9md2746wawnnip35xbq6xc")))

