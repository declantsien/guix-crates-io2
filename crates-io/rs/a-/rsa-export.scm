(define-module (crates-io rs a- rsa-export) #:use-module (crates-io))

(define-public crate-rsa-export-0.1.0 (c (n "rsa-export") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.6") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "pem") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.2") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.4") (d #t) (k 0)))) (h "12izdp0c63rh8kiwgcabsnkcw2959anw7vx0ayzzarwbsh5ch6y3") (f (quote (("pem_export" "pem")))) (y #t)))

(define-public crate-rsa-export-0.1.1 (c (n "rsa-export") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.6") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "pem") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.4") (d #t) (k 0)))) (h "0wsw6vj25yj3xxjhqp6xrzwl8inpp3lxfk6hmyzkc57q7adrzri2") (f (quote (("pem_export" "pem"))))))

(define-public crate-rsa-export-0.1.2 (c (n "rsa-export") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.6") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "pem") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.4") (d #t) (k 0)))) (h "06d5yxil77kschfhnpazc8n3ibkjq3qg3z2dxw3s2w2z5zm9fmlv") (f (quote (("pem_export" "pem"))))))

(define-public crate-rsa-export-0.2.0 (c (n "rsa-export") (v "0.2.0") (d (list (d (n "num-bigint-dig") (r "^0.6") (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "pem") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.5") (d #t) (k 0)))) (h "1xzpjc4i4y4cavvh86zwg4a5y3ylfl4xqf2mc1x1yzy60ys696m2") (f (quote (("default" "pem"))))))

(define-public crate-rsa-export-0.2.1 (c (n "rsa-export") (v "0.2.1") (d (list (d (n "num-bigint-dig") (r ">=0.6.0, <0.7.0") (k 0)) (d (n "pem") (r ">=0.8.0, <0.9.0") (o #t) (d #t) (k 0)) (d (n "rand") (r ">=0.7.0, <0.8.0") (d #t) (k 2)) (d (n "rsa") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "simple_asn1") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "0xdqgjgc9aamisp9jfgfq0qmlz6rkn1xw5yvravrw587gqr7xcz8") (f (quote (("default" "pem")))) (y #t)))

(define-public crate-rsa-export-0.3.0 (c (n "rsa-export") (v "0.3.0") (d (list (d (n "num-bigint-dig") (r "^0.6") (k 0)) (d (n "pem") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.5") (d #t) (k 0)))) (h "15a4ny231658b14jyfcp4a43xgmihxypvvn4yyn43fqm0dgn69d7") (f (quote (("default" "pem")))) (y #t)))

(define-public crate-rsa-export-0.3.1 (c (n "rsa-export") (v "0.3.1") (d (list (d (n "num-bigint-dig") (r "^0.6") (k 0)) (d (n "pem") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.5") (d #t) (k 0)))) (h "0z97khhhc3s0853fjqdydb4nfjmnp3gns0w7ign8vdaqmrq8s9gs") (f (quote (("default" "pem")))) (y #t)))

(define-public crate-rsa-export-0.3.2 (c (n "rsa-export") (v "0.3.2") (d (list (d (n "num-bigint-dig") (r "^0.6") (k 0)) (d (n "pem") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.5") (d #t) (k 0)))) (h "0750jrraghq404bk3kp0agapxip24b97lpy8xfbib9x5i5ff539m") (f (quote (("default" "pem"))))))

(define-public crate-rsa-export-0.3.3 (c (n "rsa-export") (v "0.3.3") (d (list (d (n "num-bigint-dig") (r "^0.6") (k 0)) (d (n "pem") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.5") (d #t) (k 0)))) (h "0qvjb8hg130895bkxsn3y1v4bd0v3jh7vrk2bbb51sfawx4dxrpw") (f (quote (("default" "pem"))))))

