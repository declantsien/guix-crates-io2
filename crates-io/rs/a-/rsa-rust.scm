(define-module (crates-io rs a- rsa-rust) #:use-module (crates-io))

(define-public crate-rsa-rust-0.1.0 (c (n "rsa-rust") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1.39") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1mh9y0ikcywvlpdc2s0w9dzjydki8if4iz5l4cfbl9pww3zxl6zv")))

(define-public crate-rsa-rust-0.1.1 (c (n "rsa-rust") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1.39") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1m48pxxn9hnzq9hwz5xrym9qxjq3gqh3zmmv4mkdq1nns59971dg")))

