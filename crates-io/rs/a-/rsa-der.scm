(define-module (crates-io rs a- rsa-der) #:use-module (crates-io))

(define-public crate-rsa-der-0.1.0 (c (n "rsa-der") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10.24") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rsa") (r "^0.1.3") (d #t) (k 2)) (d (n "simple_asn1") (r "^0.4") (d #t) (k 0)))) (h "14qkh7zkqqqaflr074xzfqnmjjmfg9cz3a0igkk6n2pw3jkvd3j1")))

(define-public crate-rsa-der-0.1.1 (c (n "rsa-der") (v "0.1.1") (d (list (d (n "openssl") (r "^0.10.24") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rsa") (r "^0.1.3") (d #t) (k 2)) (d (n "simple_asn1") (r "^0.4") (d #t) (k 0)))) (h "0s3hgzga8hq734jv5ql450jdhfgnadp1h0vwm6pmc0f2rgbacqd3")))

(define-public crate-rsa-der-0.2.0 (c (n "rsa-der") (v "0.2.0") (d (list (d (n "num-bigint-dig") (r "^0.4") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rsa") (r "^0.1") (d #t) (k 2)) (d (n "simple_asn1") (r "^0.4") (d #t) (k 0)))) (h "0w9hlhxm5yqwrjwxzbv20yyswbz4cjdwpl30gf5c0cajnyy8cwks")))

(define-public crate-rsa-der-0.2.1 (c (n "rsa-der") (v "0.2.1") (d (list (d (n "num-bigint-dig") (r "^0.4") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rsa") (r "^0.1") (d #t) (k 2)) (d (n "simple_asn1") (r "^0.4") (d #t) (k 0)))) (h "0kflldz44kwq49h812my2m2xvbpbh7ifcf8f39wglirmd1nchw0i")))

(define-public crate-rsa-der-0.3.0 (c (n "rsa-der") (v "0.3.0") (d (list (d (n "num-bigint-dig") (r "^0.4") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rsa") (r "^0.5") (d #t) (k 2)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)))) (h "19lshn8zjy6jwwiafpxdwl7qvfj84k20jhwfydznfr1ivsr77551")))

