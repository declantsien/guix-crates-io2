(define-module (crates-io rs a- rsa-openssl-format) #:use-module (crates-io))

(define-public crate-rsa-openssl-format-0.1.0 (c (n "rsa-openssl-format") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.4") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "153p7z701s2gzprl5zfbqgjgal87p3fk9wzzz5pvqrpwf7iv6kk7")))

