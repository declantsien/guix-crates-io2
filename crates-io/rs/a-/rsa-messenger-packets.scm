(define-module (crates-io rs a- rsa-messenger-packets) #:use-module (crates-io))

(define-public crate-rsa-messenger-packets-0.1.0 (c (n "rsa-messenger-packets") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "1jnzzgkkdm9zyagkw1syf34jzwd59f6lpm5xbsj4xiniz2w089bv") (y #t)))

