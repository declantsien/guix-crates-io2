(define-module (crates-io rs a- rsa-vdf) #:use-module (crates-io))

(define-public crate-rsa-vdf-0.0.1 (c (n "rsa-vdf") (v "0.0.1") (d (list (d (n "curv") (r "^0.6") (k 0) (p "curv-kzen")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19mw4fg4d7acs21fg096yzn7iccsy0m9r7pkl6c97z01al90jirv") (f (quote (("default" "curv/num-bigint"))))))

