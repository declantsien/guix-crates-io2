(define-module (crates-io rs a- rsa-msg-packets) #:use-module (crates-io))

(define-public crate-rsa-msg-packets-0.1.0 (c (n "rsa-msg-packets") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "186wzl6i748svyppbbgara30sqnpqflq0gmj8apga23mblsvm7gr")))

(define-public crate-rsa-msg-packets-0.1.1 (c (n "rsa-msg-packets") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "openssl") (r "^0.10.45") (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "1zhb5ay4j709zwhh7haw0bzxy9dax9sxs0flk24sx65g0p3y2j1l")))

(define-public crate-rsa-msg-packets-0.1.2 (c (n "rsa-msg-packets") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "openssl") (r "^0.10.45") (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "1a32i2rfbr6pwxcccd3cwvzaw42s62zy31vcl2ph7vxfv7hji39a")))

(define-public crate-rsa-msg-packets-0.1.3 (c (n "rsa-msg-packets") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "openssl") (r "^0.10.45") (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "15kw5vyh89qzkg88dhqs01b4a7jbdp004ykzm2g48r4sncbk8kmf")))

(define-public crate-rsa-msg-packets-0.1.4 (c (n "rsa-msg-packets") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "openssl") (r "^0.10.45") (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "1qi2xqs51jwxgn0yld9wn677mz0zca5zac3k123rm44gmbprdn81")))

(define-public crate-rsa-msg-packets-0.1.5 (c (n "rsa-msg-packets") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "openssl") (r "^0.10.45") (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "1fw61lyy02ivabpcriybdvcji9r1ymv7glrlv76mgkrb4fsrpk9i")))

(define-public crate-rsa-msg-packets-0.1.6 (c (n "rsa-msg-packets") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "openssl") (r "^0.10.45") (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "118s48bzslrcmd5sk6qc99c98xn32zqxrna625wykldicccp140f")))

(define-public crate-rsa-msg-packets-0.1.7 (c (n "rsa-msg-packets") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "openssl") (r "^0.10.45") (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "0dmp0fcsrkda5s4v93gm48lmyaq1zn3gk8lw39c4hf6ynl74ixp3")))

