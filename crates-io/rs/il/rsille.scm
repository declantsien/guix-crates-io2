(define-module (crates-io rs il rsille) #:use-module (crates-io))

(define-public crate-rsille-1.0.0 (c (n "rsille") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (o #t) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "0pja3i8jlhzxnl16y4lqfzbnkwd921fnbh0n2ka0x9fpd3rpw5nd") (f (quote (("termsize" "libc") ("img" "termsize" "image" "color") ("default" "img") ("color" "ansi") ("ansi"))))))

(define-public crate-rsille-1.0.1 (c (n "rsille") (v "1.0.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (o #t) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "1fkqwkkfn19cq99fq58b0kyijdffnszy90sz10ldnsbyfh0jx9s6") (f (quote (("termsize" "libc") ("img" "termsize" "image" "color") ("default" "img") ("color" "ansi") ("ansi"))))))

(define-public crate-rsille-1.0.2 (c (n "rsille") (v "1.0.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (o #t) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "08fk0s6n403z2rgcg96iidmdcymljvwxcq10p8r9ssi26xbcb7xh") (f (quote (("termsize" "libc") ("img" "termsize" "image" "color") ("default" "img") ("color" "ansi") ("ansi"))))))

(define-public crate-rsille-1.1.0 (c (n "rsille") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (o #t) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "1w0hx2jcbid94l5pd7c07x0r0r3pixbbsaa0ijgzijj5ib1nj226") (f (quote (("termsize" "libc") ("img" "termsize" "image" "color") ("default" "img") ("color" "ansi") ("ansi"))))))

(define-public crate-rsille-1.2.0 (c (n "rsille") (v "1.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (o #t) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "0c70lzlq8msa8i75fjlp83l9jsn02003rfm3daflw75bwhc907cp") (f (quote (("termsize" "libc") ("img" "termsize" "image" "color") ("default" "img") ("color" "ansi") ("ansi"))))))

(define-public crate-rsille-2.1.0 (c (n "rsille") (v "2.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)))) (h "1496vd0c9hav3haasgkk3wjx160csjac3l1ykaw2zwzwnjz8xyvl") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rsille-2.1.1 (c (n "rsille") (v "2.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)))) (h "0ixs5pgs3ham55zdrb7wdjs8h8kmdp9p99qvg9pjdib62r2zabl5") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rsille-2.1.2 (c (n "rsille") (v "2.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)))) (h "1rg1kasfbacwvxn835qc2cqrg1pmfpwdqy9p6ah1w4w2zn3zwi02") (f (quote (("img" "image") ("default"))))))

(define-public crate-rsille-2.1.3 (c (n "rsille") (v "2.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)))) (h "11l44jy1l5jqpyvhi9ml99y384i61fbqk45wsibfsllcxfi9pnm3") (f (quote (("img" "image") ("default"))))))

(define-public crate-rsille-2.2.0 (c (n "rsille") (v "2.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)))) (h "0haq63ix1pxr5fxr6iah69vfszdqgk2f14qjxc3nh85d1z4x8acd") (f (quote (("img" "image") ("default"))))))

(define-public crate-rsille-2.3.0 (c (n "rsille") (v "2.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (o #t) (d #t) (k 0)))) (h "0vhsjq5lzs1h33c2l8cnq3jnch9ajgk4xsfbc71c5p4bsyabr7s9") (f (quote (("img" "image") ("default"))))))

