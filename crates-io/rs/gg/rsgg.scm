(define-module (crates-io rs gg rsgg) #:use-module (crates-io))

(define-public crate-rsgg-0.1.0 (c (n "rsgg") (v "0.1.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dipn2wv8ijwzxchgw2gwc120qlii8qvjr263w25fybxbndvssi3")))

