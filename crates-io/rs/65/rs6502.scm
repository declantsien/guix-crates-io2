(define-module (crates-io rs #{65}# rs6502) #:use-module (crates-io))

(define-public crate-rs6502-0.1.0 (c (n "rs6502") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0gciqg42gcd2m2jizpylp6caavmazhwdak8qg03pbmbh3032va4c")))

(define-public crate-rs6502-0.2.0 (c (n "rs6502") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "1sf92fyg0qkixrgymm4iwlbkli4f2xs788l61lvdabc7z88jhm8w")))

(define-public crate-rs6502-0.3.0 (c (n "rs6502") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0plsvh6ngygknjvwizyzdm5lm81rrw1h7kabh32w2m3x0rlp5znc")))

(define-public crate-rs6502-0.3.1 (c (n "rs6502") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "07xfz409h9llndx06m07k5g8dn4zx7p6yvfmfa9vns9yz02c96xp")))

(define-public crate-rs6502-0.3.2 (c (n "rs6502") (v "0.3.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0gqcrjy01mbnfhhrxrnq14qfj7hn7g39p7gxs045syxk87slrfyf")))

(define-public crate-rs6502-0.3.3 (c (n "rs6502") (v "0.3.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "11sqnvpjn38h7hhv279vsa6s5hjjdqr693byvana6qlqa3nk5kbb")))

(define-public crate-rs6502-0.3.4 (c (n "rs6502") (v "0.3.4") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0gx1mr7b1432j383fnxysjcpmh4m4lk38kddwxrqb4r62zx4ky8g")))

