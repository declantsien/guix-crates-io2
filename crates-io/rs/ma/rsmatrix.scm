(define-module (crates-io rs ma rsmatrix) #:use-module (crates-io))

(define-public crate-rsmatrix-0.1.0 (c (n "rsmatrix") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1ib2wf1lm3vss01prb08rcdqr2r639wbrfmha8gf5r4f3pzc519a")))

(define-public crate-rsmatrix-0.2.0 (c (n "rsmatrix") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0kfd13v4wwhfdik91fm89n4mrglh379y4zacrk9gbi5js3w26gfg")))

