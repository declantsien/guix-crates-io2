(define-module (crates-io rs ma rsmath) #:use-module (crates-io))

(define-public crate-rsmath-0.1.3 (c (n "rsmath") (v "0.1.3") (d (list (d (n "num") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1macw4d80b7z0frawcsifb1c6iay4b1y2qaplj1xhrgiawwis7d7")))

(define-public crate-rsmath-0.1.4 (c (n "rsmath") (v "0.1.4") (d (list (d (n "num") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1b27qvy6vjly01d2wak4dlyjlalmi1q36p9957ddh6h8b41g88f8")))

(define-public crate-rsmath-0.1.5 (c (n "rsmath") (v "0.1.5") (d (list (d (n "num") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0h16l8v43hwam2fwgl438x3ni405779bq5aqg3cqnc035ykylvj6")))

(define-public crate-rsmath-0.1.6 (c (n "rsmath") (v "0.1.6") (d (list (d (n "num") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0zaj1r34qxlqlh2javhfh4zf6pgq017ppmv0j8j9dwq5z8l0vsc7")))

