(define-module (crates-io rs ma rsmanuf) #:use-module (crates-io))

(define-public crate-rsmanuf-1.0.0 (c (n "rsmanuf") (v "1.0.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "13ziaqq313fd40fgkmsy1imvf6wq6mil2m4dq537crsaa8xwi3q8") (y #t)))

(define-public crate-rsmanuf-1.0.1 (c (n "rsmanuf") (v "1.0.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1aszkvrxbxpk497hwmgzs6zfcspqmdqk8xsb3ajx3py637zw6kaz") (y #t)))

(define-public crate-rsmanuf-1.0.2 (c (n "rsmanuf") (v "1.0.2") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0v2zafpmfd0dh45fhjy240lw4g3qhzskiav0pparxk4yzb4321a6") (y #t)))

(define-public crate-rsmanuf-2.0.2 (c (n "rsmanuf") (v "2.0.2") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1y23rpa4924sldhm0kbxkdmy59vinz4v2m37lk94na0k8y0f44nq")))

(define-public crate-rsmanuf-2.0.3 (c (n "rsmanuf") (v "2.0.3") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "15gmdyvlrkm2l6l4q2vpwvkyfdl9bvkzxwhvd7z3f0rkqcks3y8h")))

(define-public crate-rsmanuf-2.0.4 (c (n "rsmanuf") (v "2.0.4") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1b7134avbpx5snxmavjzizfhlrx8m6rpl93jj9icl0wq7wihb28q")))

(define-public crate-rsmanuf-2.0.5 (c (n "rsmanuf") (v "2.0.5") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1bhwryg3shkw6xkrr929wsw4w8ml9va7mg7sf53821d0cwwcf29k")))

(define-public crate-rsmanuf-2.0.6 (c (n "rsmanuf") (v "2.0.6") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1z2kkq5dsmp1cw8zw60b57lhryr2w0yncyw5d78bi3ql9rlxvnln")))

(define-public crate-rsmanuf-2.1.0 (c (n "rsmanuf") (v "2.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "082s7jrx0py9grp3ggk6s26pv55jjxbrajqn97r7lsi92c0svbf7")))

(define-public crate-rsmanuf-2.1.1 (c (n "rsmanuf") (v "2.1.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "17lkd7rmv6lh65cyr9jh615h7wxz3zkybhn446m43nxa2v8w1386")))

(define-public crate-rsmanuf-2.1.2 (c (n "rsmanuf") (v "2.1.2") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "13jlbkp5ypynyyyh2y5j3rhbar7j7sv8ylzvyavrv5j0zjby9dgk")))

(define-public crate-rsmanuf-2.1.3 (c (n "rsmanuf") (v "2.1.3") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0y7m9pmlr7qsiywjaqrlppn5i70z3g7rgcjjr4z95myc8dyx2pac")))

