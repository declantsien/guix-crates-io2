(define-module (crates-io rs pc rspc-macros) #:use-module (crates-io))

(define-public crate-rspc-macros-0.0.3 (c (n "rspc-macros") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1v3lw48cggs4rq2819xr9adv9c812qh1yc3hhq359rrfyw9965gg") (y #t)))

