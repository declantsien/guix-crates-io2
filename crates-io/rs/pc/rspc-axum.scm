(define-module (crates-io rs pc rspc-axum) #:use-module (crates-io))

(define-public crate-rspc-axum-0.0.1 (c (n "rspc-axum") (v "0.0.1") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "httpz") (r "^0.0.6") (f (quote ("axum"))) (d #t) (k 0)) (d (n "rspc") (r "^0.1.4") (f (quote ("internal_axum_07"))) (d #t) (k 0)))) (h "1gvqhgiyw84caqlm5chbx10l4zxbxhpzw84krpw0sik28vd9h6yj")))

(define-public crate-rspc-axum-0.1.0 (c (n "rspc-axum") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rspc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (o #t) (d #t) (k 0)))) (h "0qk609lmqhcc591xbxvyjvq3mb11jjchx3qjvcb0c3j0xg16lw3w") (f (quote (("default")))) (s 2) (e (quote (("ws" "dep:tokio" "axum/ws"))))))

(define-public crate-rspc-axum-0.1.1 (c (n "rspc-axum") (v "0.1.1") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rspc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (o #t) (d #t) (k 0)))) (h "1lsarvzam6x39ja7lc40pqjx5cdh77gr4sdrpx4f9157mfn7f9sv") (f (quote (("default")))) (s 2) (e (quote (("ws" "dep:tokio" "axum/ws"))))))

