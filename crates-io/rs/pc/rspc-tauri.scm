(define-module (crates-io rs pc rspc-tauri) #:use-module (crates-io))

(define-public crate-rspc-tauri-0.0.1 (c (n "rspc-tauri") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rspc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "specta") (r "^1.0.5") (f (quote ("serde" "typescript"))) (d #t) (k 0)) (d (n "tauri") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)))) (h "028nrm3zp5cw9brq580klram5iiz9ij53cmvlw8lazmbsypl7wjd")))

