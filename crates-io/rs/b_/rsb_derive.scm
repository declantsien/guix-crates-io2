(define-module (crates-io rs b_ rsb_derive) #:use-module (crates-io))

(define-public crate-rsb_derive-0.1.0 (c (n "rsb_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03w3cq9k0djklslzqpx84xq2yv12wq5h8318d9ghnx0wmqy10shc")))

(define-public crate-rsb_derive-0.1.1 (c (n "rsb_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ckpxk0dw4c7jkimldpa0nkhwilqvgvf21p7bf8889z1hbfvlpa9")))

(define-public crate-rsb_derive-0.1.2 (c (n "rsb_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "159ljrcs3pkzycd7ndnm12di41b3c748knp9v2hxpb4k0x5cl5zp")))

(define-public crate-rsb_derive-0.1.3 (c (n "rsb_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "119m3kw5vr1hsc93a3v62gz763xmmnir4bml2z0sxgyc2pgl7k70")))

(define-public crate-rsb_derive-0.1.4 (c (n "rsb_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zzpcm99yh9qvmqhc62dj29a8nfam5nlw6cay23bic273h073k3j")))

(define-public crate-rsb_derive-0.2.1 (c (n "rsb_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yk8j3x89gmjvzdxpj9jb3hv1j5ljizn5jfwb0hi28y3fpx3301d")))

(define-public crate-rsb_derive-0.2.2 (c (n "rsb_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d9jybwsw2cjyqissfna12rxvr66srjrd9yp741dn8d21rmk9lxl")))

(define-public crate-rsb_derive-0.2.3 (c (n "rsb_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f1cj96xzd0sq0s1z5hb58nvifh4xjqj0c5myipja2kjcgk6mflz")))

(define-public crate-rsb_derive-0.2.4 (c (n "rsb_derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11bxi3drksk6vhdj7al6lw86j0ivkkwq2psscj8zv4738v83y5si")))

(define-public crate-rsb_derive-0.2.5 (c (n "rsb_derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wa4i4dcvhsfd8a7mycyfq5wanjl9vl9ppf4wsl7lhwxzjkd1n17")))

(define-public crate-rsb_derive-0.3.0 (c (n "rsb_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ph56ddzm4p9pj0yvyj791cm43x1pcqx2zj8xbbwyxf359wlc2vx")))

(define-public crate-rsb_derive-0.4.0 (c (n "rsb_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dmgk7n476xdqzpmprmpmz4bd5f5xf260hqxjdzj3s9h85k770c5")))

(define-public crate-rsb_derive-0.4.1 (c (n "rsb_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iazgcwdiyhwlzmxqhdrp2im3vlgmgn0l0bbhf1x31qxhk9mmi7d")))

(define-public crate-rsb_derive-0.4.2 (c (n "rsb_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10ixzg32dvk2c6rs036k818cmpas874nxbiymmfjkfk1ncp4cw54")))

(define-public crate-rsb_derive-0.5.0 (c (n "rsb_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08gfyda5mbyy3l2vdg6m5qixih4kwk1sgf7kyi8zn63lkwch7mgw")))

(define-public crate-rsb_derive-0.5.1 (c (n "rsb_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19794z2gmyqyb7pw5l2pwv0w16zqg3z8b5q95qbz3ifdzi13xidj")))

