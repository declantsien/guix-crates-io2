(define-module (crates-io rs _g rs_gost_r34_10_2001) #:use-module (crates-io))

(define-public crate-rs_gost_r34_10_2001-0.1.0 (c (n "rs_gost_r34_10_2001") (v "0.1.0") (h "1hqca53ffx56k9j2gqncqq5x3rrkbiz7zjcrkqry7rhh5k0z9hls")))

(define-public crate-rs_gost_r34_10_2001-0.1.1 (c (n "rs_gost_r34_10_2001") (v "0.1.1") (h "0dwsbs3hfsyg0ccb8q2gcib3l41mx4q0s1fnmfzrps633jn1cmsk")))

(define-public crate-rs_gost_r34_10_2001-0.1.2 (c (n "rs_gost_r34_10_2001") (v "0.1.2") (h "1rbcvxandsh2x17g7wfyg6pqarww8nsrl8qipvhgxz3dmmm58qfx")))

