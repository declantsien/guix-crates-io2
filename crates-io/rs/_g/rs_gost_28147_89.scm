(define-module (crates-io rs _g rs_gost_28147_89) #:use-module (crates-io))

(define-public crate-rs_gost_28147_89-0.1.0 (c (n "rs_gost_28147_89") (v "0.1.0") (h "0dlc6i37svdhj9dj682496izc56md9j267rckkq8m57acbjpvkkn")))

(define-public crate-rs_gost_28147_89-0.1.1 (c (n "rs_gost_28147_89") (v "0.1.1") (h "0lzz3aqdlgvxnhk222inw9f3289zdfn4n1w73kgb4xz4vyp4b1sf")))

(define-public crate-rs_gost_28147_89-0.1.2 (c (n "rs_gost_28147_89") (v "0.1.2") (h "00160v1rvjjl3df3f8fqyvbdnnp7xk85yi2qbqvn00v6kf6bac7n")))

