(define-module (crates-io rs _g rs_gost_r34_11_94) #:use-module (crates-io))

(define-public crate-rs_gost_r34_11_94-0.1.0 (c (n "rs_gost_r34_11_94") (v "0.1.0") (h "1xkr7s81kypkfl4yyal0ias9drz57zl0c0x8rk737mqclrcagszi")))

(define-public crate-rs_gost_r34_11_94-0.1.1 (c (n "rs_gost_r34_11_94") (v "0.1.1") (h "1c24jhnzn6pz965qmswh22d021sxg694c5y2svxf4agn0x7rlh7x")))

(define-public crate-rs_gost_r34_11_94-0.1.2 (c (n "rs_gost_r34_11_94") (v "0.1.2") (h "01hfsvybj5pcbih1m8v2bmri7v3ijffx7ihvysq0pv2hr2pwxvih")))

