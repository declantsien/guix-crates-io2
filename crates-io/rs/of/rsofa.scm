(define-module (crates-io rs of rsofa) #:use-module (crates-io))

(define-public crate-rsofa-0.1.0 (c (n "rsofa") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 1)))) (h "11fja42ljz34vnhz7gnlhbjzmy71najwj6j5p2675ndyy3vv17nm")))

(define-public crate-rsofa-0.2.0 (c (n "rsofa") (v "0.2.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 1)))) (h "0pwpywji21kfzrd9dlz0r8ypjz6m6anmfwbgp1gd4p7l2w7r5r02")))

(define-public crate-rsofa-0.3.0 (c (n "rsofa") (v "0.3.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 1)))) (h "0s5yhikgnbqwcydrx21r2p24z53k6falbjj0ckv654766ndwwvmy")))

(define-public crate-rsofa-0.4.0 (c (n "rsofa") (v "0.4.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 1)))) (h "1zjzjj21flgnmb4v7rgwx4qacqwdmxqci5q0b59v8lkf22yhdjzn")))

(define-public crate-rsofa-0.4.1 (c (n "rsofa") (v "0.4.1") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 1)))) (h "193xysj8p4s1rrmc70km5h86x554nhqfxkc7fjvcclwa46lvxrhh")))

(define-public crate-rsofa-0.4.2 (c (n "rsofa") (v "0.4.2") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 1)))) (h "13dkgqq1id5cpknwsdgaa5ls3wdwjxh015lhpl242gh9mx86j0vx")))

(define-public crate-rsofa-0.4.3 (c (n "rsofa") (v "0.4.3") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "118yawp22533bvqg5pr11ia3vkgg2bfmdbwpwmh9krbk36gv8ff5")))

(define-public crate-rsofa-0.4.4 (c (n "rsofa") (v "0.4.4") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1b692pd7hzmch1hz6z5b9x65wp1mdnsqg52a2jc6sb490i28bar5")))

(define-public crate-rsofa-0.4.5 (c (n "rsofa") (v "0.4.5") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0dr76rr43bxiasir51r3ava5nlm82nihsgvj3zdxaanl8zkd5by7")))

(define-public crate-rsofa-0.5.0 (c (n "rsofa") (v "0.5.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.60.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "0zhnvrgg5gmjydv6gmgiw58p2s6k2fg7clxw2lmymjzx6j4p2wi1")))

