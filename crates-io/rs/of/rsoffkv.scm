(define-module (crates-io rs of rsoffkv) #:use-module (crates-io))

(define-public crate-rsoffkv-0.1.0 (c (n "rsoffkv") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f3l8krfdzgz7q0qzzx6yzf5xk7z0iah1dxfk98ynskmxrglls4k")))

(define-public crate-rsoffkv-0.1.1 (c (n "rsoffkv") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1imbcjsdqsgjg3z7przdpz62rrazc5n23b0ic5vx2jzw3a5l91ri")))

(define-public crate-rsoffkv-0.1.3 (c (n "rsoffkv") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15i6545v01v0xpryim02x11mlgjjz08jlfs3x8gm9vcbmwc1hq8c")))

(define-public crate-rsoffkv-0.1.4 (c (n "rsoffkv") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "183wby00ilaz8jmwhv21x0mzlwdyc6lv3swwwj8s9a7gfb82r15n")))

(define-public crate-rsoffkv-0.1.5 (c (n "rsoffkv") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1886qcgvpzjxz832mffrr258rlsi7d5lqc97sb5rps6r7yaqbj8x")))

(define-public crate-rsoffkv-0.1.6 (c (n "rsoffkv") (v "0.1.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kw3x6cwj7qnz09c62awrskqv5yn8s4rj170044m6mss92b1m8ww")))

(define-public crate-rsoffkv-0.1.7 (c (n "rsoffkv") (v "0.1.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03g53f83vz0x2sywp7cs7r3afpndm89hvqrdcz7jzyn60liv5mbx")))

(define-public crate-rsoffkv-0.1.8 (c (n "rsoffkv") (v "0.1.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qlhif0f9qijcryl699bvg6mg96wzy60f6r3hik2wvypv1wnqq17")))

