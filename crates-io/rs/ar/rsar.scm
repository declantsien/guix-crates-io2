(define-module (crates-io rs ar rsar) #:use-module (crates-io))

(define-public crate-rsar-0.1.0 (c (n "rsar") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x7jlhrhf1bbhpgn2lc0d4ksh6faflddsvvkmqvn72d9qj2bz7mm")))

(define-public crate-rsar-0.1.1 (c (n "rsar") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11zbvm99d8b477yaw2jnkq4ny0199ld4319xj99dycxz17w6yfjc")))

(define-public crate-rsar-0.2.1 (c (n "rsar") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19lihyymn2wjbsjc45lyp9zgsr98c092g94rc9yka61j29c9sf35")))

