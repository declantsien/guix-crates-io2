(define-module (crates-io rs st rsst) #:use-module (crates-io))

(define-public crate-rsst-0.1.0 (c (n "rsst") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "md5") (r "^0.6") (d #t) (k 0)) (d (n "rss") (r "^1.7") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0b00wmcphdj49pmca1iy5wljz3qa95qw90rx3fgwspi77cn73bqw")))

