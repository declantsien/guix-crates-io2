(define-module (crates-io rs im rsimple_http) #:use-module (crates-io))

(define-public crate-rsimple_http-0.1.0 (c (n "rsimple_http") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1nynbnfr5wbm7qfnkl25wbdgfqghdrsbxv05np4b0a54qsspzh4s")))

(define-public crate-rsimple_http-0.1.1 (c (n "rsimple_http") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "16q3p5lldl38pcq44h0c361qnvjh8mbi3spqyn81l460vqyhz7zb")))

(define-public crate-rsimple_http-0.2.0 (c (n "rsimple_http") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 2)))) (h "1y26b836jdqj2k1lylyhzkbwc5frllmxzgwsa5glfl81fdlg2qxw")))

