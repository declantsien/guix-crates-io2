(define-module (crates-io rs mn rsmnl-derive) #:use-module (crates-io))

(define-public crate-rsmnl-derive-0.1.0 (c (n "rsmnl-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cdaqm0lwhkgmpp3aibv3nx0532j0cvi5v2ry2mlr66rsklqh0s7")))

