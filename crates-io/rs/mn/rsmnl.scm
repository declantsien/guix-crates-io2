(define-module (crates-io rs mn rsmnl) #:use-module (crates-io))

(define-public crate-rsmnl-0.1.0 (c (n "rsmnl") (v "0.1.0") (d (list (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "mio") (r "^0.7.13") (f (quote ("os-poll" "os-util" "udp"))) (d #t) (k 2)))) (h "13glqgc0hzplm9hyh5jabqp21rdrwwqxllk9hw3f7xd2g7hhznhx")))

