(define-module (crates-io rs te rsteglib) #:use-module (crates-io))

(define-public crate-rsteglib-0.1.0 (c (n "rsteglib") (v "0.1.0") (h "1hsxmiwaw3kqd8b4m9yb3gsld4s5si6ks62f2wqv6xi599k65p87")))

(define-public crate-rsteglib-0.1.1 (c (n "rsteglib") (v "0.1.1") (h "13zi6fl58xc6g1pm4n3xfhxww6q7nddr54fahkz3r4w86db4iif2")))

