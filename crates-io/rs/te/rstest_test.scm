(define-module (crates-io rs te rstest_test) #:use-module (crates-io))

(define-public crate-rstest_test-0.1.0 (c (n "rstest_test") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.1.5") (d #t) (k 0)))) (h "1whn1kwxhanjb82fj5674flskjj8sqknawffi945dwx6qxqr77h1")))

(define-public crate-rstest_test-0.2.0 (c (n "rstest_test") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.1.5") (d #t) (k 0)))) (h "0bv429k1n9wspnds5w0bl6d49dfj1cx08gncjqjnp084v8q73d73")))

(define-public crate-rstest_test-0.3.0 (c (n "rstest_test") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1j5sqwkzhyj1vwvdjq429qvgw1bypcyvhqz9j63h2wyx8l2gcs9f")))

(define-public crate-rstest_test-0.4.0 (c (n "rstest_test") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.5.0") (d #t) (k 0)))) (h "1038ckjm5di3v7qbixil1fnf1fq1rm0h31wi7hqh5hg0kyirzjag")))

(define-public crate-rstest_test-0.5.0 (c (n "rstest_test") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.5.0") (d #t) (k 0)))) (h "0mgkk9kqrhca5lvxv8iwbxyvm84wg56y8hrmlrbmq0jzpq57c16w")))

(define-public crate-rstest_test-0.6.0 (c (n "rstest_test") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.5.0") (d #t) (k 0)))) (h "08kgf60s6s5rjch75lrfc9yk7q222m09ai28pyxrg2m2fjkxzxhi")))

(define-public crate-rstest_test-0.7.0 (c (n "rstest_test") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)))) (h "1a0a954a2fahg4hw6nlaq7ql964m00mvldm3b47n0hpfclcandwj")))

(define-public crate-rstest_test-0.8.0 (c (n "rstest_test") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)))) (h "191k8z7ai7snvprd6yrlf0fs82dz7mdzpizhav2z0jkplx6wbflb")))

(define-public crate-rstest_test-0.9.0 (c (n "rstest_test") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (k 2)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.15.0") (d #t) (k 0)))) (h "0bdy2pni88vh6m2w2mk0wj5hsgka4yslwv472wyfk6794ilgl203")))

(define-public crate-rstest_test-0.10.0 (c (n "rstest_test") (v "0.10.0") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (k 2)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)))) (h "034smgskarl4cwl66pd0b9kbv2mi6bkvq0nipq022ck42hj4f2aj")))

(define-public crate-rstest_test-0.11.0 (c (n "rstest_test") (v "0.11.0") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (k 2)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)))) (h "16z1525ww9n1x94mvvgd6zcpxc2pzlf1r031kryk383lgd3wgi9p")))

