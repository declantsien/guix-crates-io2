(define-module (crates-io rs _n rs_n_bit_words) #:use-module (crates-io))

(define-public crate-rs_n_bit_words-0.1.1 (c (n "rs_n_bit_words") (v "0.1.1") (h "0a3a93jkxaskwaqpgdrdbg7viyjfv163rcm789n1srh9csl82zl1")))

(define-public crate-rs_n_bit_words-0.1.2 (c (n "rs_n_bit_words") (v "0.1.2") (h "12cvcs46k8ghc9rpyxr7cpdrz2ip5gjn9zcpx46vwni8wap1r6xd")))

(define-public crate-rs_n_bit_words-0.1.3 (c (n "rs_n_bit_words") (v "0.1.3") (h "1dlwv1anfww8sk16dj9gwyk0y8ay0ssp1fp666rpc3x6qasbphcb")))

