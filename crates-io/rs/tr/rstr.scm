(define-module (crates-io rs tr rstr) #:use-module (crates-io))

(define-public crate-rstr-0.1.0 (c (n "rstr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "tree_magic_mini") (r "^3.0.0") (d #t) (k 0)))) (h "1m5rfz4z8hdz8gwx9ff8finjzy2f20wxd040ggrl5gx09325msyz") (f (quote (("progress_bar" "indicatif"))))))

