(define-module (crates-io rs tr rstructure) #:use-module (crates-io))

(define-public crate-rstructure-0.1.0 (c (n "rstructure") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1bl8irbarm52cg39h2wmc3cylyiqhx6bgpjy8fnzs8qxkbj0h1lp")))

(define-public crate-rstructure-0.2.0 (c (n "rstructure") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0n3l6vphsb129q40gxg8bfkcmb2sy2mrdv5lhpfh0ambfda9fpgn")))

