(define-module (crates-io rs tr rstree) #:use-module (crates-io))

(define-public crate-rstree-0.1.0 (c (n "rstree") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0pc1jp1zyn23wvic93mqxj7lybkw949krqr7ld70c8iimmxvm90y") (y #t)))

(define-public crate-rstree-0.1.1 (c (n "rstree") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "026sqxzj0wil5b3y843zji6mc57r8nm3wwhhfddwmj87xazvbqrr") (y #t)))

