(define-module (crates-io rs tr rstrpc-macros) #:use-module (crates-io))

(define-public crate-rstrpc-macros-0.0.1 (c (n "rstrpc-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "ts-rs") (r "^7.1.1") (d #t) (k 0)))) (h "05cjqvgv9vcb5x89lp6j2zsma7b75dy4kvamgz1jkavhba0sdxw1")))

