(define-module (crates-io rs -l rs-logger) #:use-module (crates-io))

(define-public crate-rs-logger-0.1.0 (c (n "rs-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "0hwqqr3vvdvxdpvykj7kvfj5kwyxybcn4p327hnw46xihxi677y0")))

(define-public crate-rs-logger-0.1.1 (c (n "rs-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "1g2fp9xxp4fym24n13f7mplyqi1z7xqlx2jfqwfwkssrsx4f39wr") (y #t)))

(define-public crate-rs-logger-0.1.2 (c (n "rs-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "1flk01xwr35j42yhvrq0hyi17aghi47ii4szq21hnjllkz8v4x7k") (y #t)))

(define-public crate-rs-logger-0.1.3 (c (n "rs-logger") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "1rqsm7gni7vqqd9nn04hbiflg5ksc3bfnz6i8xbszfsml8mj5p1s") (y #t)))

(define-public crate-rs-logger-0.1.4 (c (n "rs-logger") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "0k431m98lr8vc7wrgnvpbx5lri51albh0fcnz0yayqi46x33cz3m")))

(define-public crate-rs-logger-0.1.5 (c (n "rs-logger") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "18s0gsz227ccq01nqlz3ac7y10h2m6myxqx0ggh43xbsg90sm9pn")))

(define-public crate-rs-logger-0.1.6 (c (n "rs-logger") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "0ks2ivrv31rm9hdvv5ri8kcqkiiyazmc8y2j9canfhxkw9qmf83z")))

(define-public crate-rs-logger-0.1.7 (c (n "rs-logger") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "0w2zgsyf0mrnjjm77hraj974ax3zs977x5glwrc5dd4ak744c55y")))

(define-public crate-rs-logger-0.1.8 (c (n "rs-logger") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "0mkb2938nj5kj83lwr1wvbkwakkl81s1ikby0hk9wr4rdd271hi3") (y #t)))

(define-public crate-rs-logger-0.2.0 (c (n "rs-logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "1vwhw8207n1a92kzc2wqpr5f9dpz3m4pk0m4vbyrqb5q4ji2yrqs")))

(define-public crate-rs-logger-0.3.0 (c (n "rs-logger") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "0gx84443gvx7qwls9rd8x24903w9xns0vr3hqkb13ds2fmw2a9rj")))

(define-public crate-rs-logger-0.3.1 (c (n "rs-logger") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "1agvhhris6chviv856xjq4sl7zn585pdvm2iw19bv4fmhfl0d80f")))

(define-public crate-rs-logger-0.3.2 (c (n "rs-logger") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "13knc104yc0bn7fm8x78vs3s974r56icy6a09zv55ycsni2118s4")))

