(define-module (crates-io rs -l rs-leveldb) #:use-module (crates-io))

(define-public crate-rs-leveldb-0.1.0 (c (n "rs-leveldb") (v "0.1.0") (d (list (d (n "leveldb-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0c2rkm8ch2440nyyzylxsyxpmn5cxx96v3pp5v4rhs7mcf5afjbg") (f (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1.1 (c (n "rs-leveldb") (v "0.1.1") (d (list (d (n "leveldb-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1m5vfbsngkm0pn5r7lrvsvxpjg3w8gsk0zircdnz3sybfp5s1gif") (f (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1.2 (c (n "rs-leveldb") (v "0.1.2") (d (list (d (n "leveldb-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1clg1gvqbxj3c8s92j6axs4dq2iyqx2qm4jqlqf21qlh2vqqbpwh") (f (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1.3 (c (n "rs-leveldb") (v "0.1.3") (d (list (d (n "leveldb-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1gaapnd6y3mg61pdrxw8cfa4a623bidmrcb07n0z1rwq577m2y55") (f (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1.4 (c (n "rs-leveldb") (v "0.1.4") (d (list (d (n "leveldb-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "104yjp1h6hsddnlflwsxm9ypgzw6f4bvhfxb9qsi4zrrbkmdz7qj") (f (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1.5 (c (n "rs-leveldb") (v "0.1.5") (d (list (d (n "leveldb-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0bc16fpdmdsznrpxvrbm633w97nvq8wk8b133vfw9ybq5rwdanyl") (f (quote (("default" "leveldb-sys/snappy"))))))

