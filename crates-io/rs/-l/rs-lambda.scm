(define-module (crates-io rs -l rs-lambda) #:use-module (crates-io))

(define-public crate-rs-lambda-0.1.0 (c (n "rs-lambda") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1kjqbprcvn933128r8h968882p2qslfxj4r6yk1x9gisrv6vydf2")))

(define-public crate-rs-lambda-1.0.0 (c (n "rs-lambda") (v "1.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "reglex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0f2v342cl5xkl34nxgz28h3jqqqrndxb853njsqlmk3b8wknl5yx")))

(define-public crate-rs-lambda-1.0.1 (c (n "rs-lambda") (v "1.0.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "reglex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0kg7mpbn7g0is2mfhdwkr0wz2z1ivb6ncmw3cdlp1p7ab59lza3n")))

