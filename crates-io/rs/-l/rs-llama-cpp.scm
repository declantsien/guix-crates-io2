(define-module (crates-io rs -l rs-llama-cpp) #:use-module (crates-io))

(define-public crate-rs-llama-cpp-0.1.6 (c (n "rs-llama-cpp") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "1b7gmnn8d9axkzddaba0rnmabi29icglgjd30n4czc5vimzkzfbz") (y #t)))

(define-public crate-rs-llama-cpp-0.1.7 (c (n "rs-llama-cpp") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "1b2s1qpwv8l6w750hfisv8q1mrsy8mpy6cqsvspis632w3d36fv2") (y #t)))

(define-public crate-rs-llama-cpp-0.1.8 (c (n "rs-llama-cpp") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "1j3ykza7vmxy7rfhgjnvd3y5pq29m0d8avg3q9vqsi164hwz7d5y")))

(define-public crate-rs-llama-cpp-0.1.9 (c (n "rs-llama-cpp") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "18fn5cgagvm8pnxmc02cjxacf103z29n8w1dnnbizylhd4qqy0gj")))

(define-public crate-rs-llama-cpp-0.1.10 (c (n "rs-llama-cpp") (v "0.1.10") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "0mi9fwn0jj9sxmyw2wk6mmh8f6iynhj634zxz9dlv774j1s1vs36")))

(define-public crate-rs-llama-cpp-0.1.11 (c (n "rs-llama-cpp") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "00xzvqka6z01wb2fbml42z9rydgj3ha8wqhcmav14svd91y3n2fq")))

(define-public crate-rs-llama-cpp-0.1.12 (c (n "rs-llama-cpp") (v "0.1.12") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "18s3jrglsfj00ky2dfmfa6rhjlzmvaaghrk0qw5idnkq5hrgz27j")))

(define-public crate-rs-llama-cpp-0.1.13 (c (n "rs-llama-cpp") (v "0.1.13") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1a356fbhrgg7c2rkwq897scsw351w9i733ml35578vwwvcx0vr8i")))

(define-public crate-rs-llama-cpp-0.1.14 (c (n "rs-llama-cpp") (v "0.1.14") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "168iab138y14yzbg5q0qxkcqm89i1hkxwxak5pihq5wwp3kkwxxr")))

(define-public crate-rs-llama-cpp-0.1.15 (c (n "rs-llama-cpp") (v "0.1.15") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1kvfvqmsd6r0rzk6769b55j59bsj419hc5y55v1sfpayg6jb33fw")))

(define-public crate-rs-llama-cpp-0.1.16 (c (n "rs-llama-cpp") (v "0.1.16") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "111pfs4xrj492l3sgwbby9g22frvvs3p64azgr800bg3mm8k9blv")))

(define-public crate-rs-llama-cpp-0.1.17 (c (n "rs-llama-cpp") (v "0.1.17") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0vgfkpycblzmxdlmrgdsq8nzjg1psdvnsmj3r9vhvmqd0y250mhy")))

(define-public crate-rs-llama-cpp-0.1.18 (c (n "rs-llama-cpp") (v "0.1.18") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "018swdq2f4g6w72anqw0shj1ryzhl5lbxqk571xy02mh0cq6ldn7")))

(define-public crate-rs-llama-cpp-0.1.19 (c (n "rs-llama-cpp") (v "0.1.19") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1mzyba665hf0v2xhxzcxi40ba9hl65862zs6n7b8f3r1wacnamv4")))

(define-public crate-rs-llama-cpp-0.1.20 (c (n "rs-llama-cpp") (v "0.1.20") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1br28p6fajx2yk9k44brgrljqzcvyc6kmqw0a4chsgl8wkn6036m")))

(define-public crate-rs-llama-cpp-0.1.21 (c (n "rs-llama-cpp") (v "0.1.21") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0dg7qnzfbd1f3fabm2h3l0zvmjdxdqfrd4m99pjls72vg5grvc2g")))

(define-public crate-rs-llama-cpp-0.1.22 (c (n "rs-llama-cpp") (v "0.1.22") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1zfvqvxk2sfsf2c4bsr1a02w01b7kww7narc3prz5wkr9vs4wkkf")))

(define-public crate-rs-llama-cpp-0.1.23 (c (n "rs-llama-cpp") (v "0.1.23") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1hc9y868dl2vd4zdbn86rqz8d7rbg3k8njin0c5l3qkflp6blgvs")))

(define-public crate-rs-llama-cpp-0.1.24 (c (n "rs-llama-cpp") (v "0.1.24") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1qx53g33x3511mrs478fm975s32q5ar2d1dmc0x242x8ng578qdl")))

(define-public crate-rs-llama-cpp-0.1.25 (c (n "rs-llama-cpp") (v "0.1.25") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0jjz3bvbhh349xdiksmkmvg2z0ikly0b94fxirq4rzlnwr1s90yf")))

(define-public crate-rs-llama-cpp-0.1.26 (c (n "rs-llama-cpp") (v "0.1.26") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1h85p9bqpa3vijgglfn5qx28cifx7gdf6ay81fphqpqx6cd49ipj")))

(define-public crate-rs-llama-cpp-0.1.27 (c (n "rs-llama-cpp") (v "0.1.27") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0d6blm1lrmar0b2dr41r3cwp7p74b3934kycrms6c28a6f72md4g")))

(define-public crate-rs-llama-cpp-0.1.28 (c (n "rs-llama-cpp") (v "0.1.28") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0hgz7r61lgmzkcqb6xi6r2701i87jngnm52v0j7ic65zi2y1py6f")))

(define-public crate-rs-llama-cpp-0.1.29 (c (n "rs-llama-cpp") (v "0.1.29") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1ciyka1kc8r3hjn2hg4ypw31ywwq18p15av22hx16852n91kzpp9")))

(define-public crate-rs-llama-cpp-0.1.30 (c (n "rs-llama-cpp") (v "0.1.30") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0gq5mxcc7zzvhm9d5yby7r3cllqf41nxnsngf3xgpbncghxlwldj")))

(define-public crate-rs-llama-cpp-0.1.31 (c (n "rs-llama-cpp") (v "0.1.31") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "08q6kq515hsq57vn3kx40djdwj8mai7lnvhb3rg5afxp71rbpg0q")))

(define-public crate-rs-llama-cpp-0.1.32 (c (n "rs-llama-cpp") (v "0.1.32") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1x0nqzv74x3z5knw9jdyplkbhl6dvbj40n8ckdcn69xg37pq8qq1")))

(define-public crate-rs-llama-cpp-0.1.33 (c (n "rs-llama-cpp") (v "0.1.33") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1c6dxxjf0k1phj3k07yca3kj2bm6l0h87lhhgqjyjwgqj4mh34ia")))

(define-public crate-rs-llama-cpp-0.1.34 (c (n "rs-llama-cpp") (v "0.1.34") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "05g731rh26sy8b02jcah1iw5i02w1par758mm0k9544zam04nzk9")))

(define-public crate-rs-llama-cpp-0.1.35 (c (n "rs-llama-cpp") (v "0.1.35") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1qiz4h5xg8mavawz6l9jc6v9crf16qlb13f9zbh1mjskyin1yacp")))

(define-public crate-rs-llama-cpp-0.1.36 (c (n "rs-llama-cpp") (v "0.1.36") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "07wa6whyprrid768cmagk0bbdfihd8plf7prkvwqvdxk78i9nqyc")))

(define-public crate-rs-llama-cpp-0.1.37 (c (n "rs-llama-cpp") (v "0.1.37") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1fzk5bznliy67z04h35zbbhrjy8prfg8g70d0wmpsnja435r4aa0")))

(define-public crate-rs-llama-cpp-0.1.38 (c (n "rs-llama-cpp") (v "0.1.38") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0n5bz59zcs3hl43j6cwakpqrkirmlbs2v6r15h3nm70x7qwmgdwn")))

(define-public crate-rs-llama-cpp-0.1.39 (c (n "rs-llama-cpp") (v "0.1.39") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1mvvna8vdqiamgv5r2xayc380489kwl4ag271jcy0l0vyyrkh573")))

(define-public crate-rs-llama-cpp-0.1.40 (c (n "rs-llama-cpp") (v "0.1.40") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0zmmmrvynza4cad6kjx2hsmjlbizgp9vgrpbna6j7iw6144ikllx")))

(define-public crate-rs-llama-cpp-0.1.41 (c (n "rs-llama-cpp") (v "0.1.41") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1qy1b3k53yi7kz23bw658rvgfxa5j82cyrbwb5a292qzq4nwzsdl")))

(define-public crate-rs-llama-cpp-0.1.42 (c (n "rs-llama-cpp") (v "0.1.42") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1z3gayqid725qbk9d1mvhqsxfi93cvckkl6q41mqhnpmf290adl9")))

(define-public crate-rs-llama-cpp-0.1.43 (c (n "rs-llama-cpp") (v "0.1.43") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1mflan9a6vab99v5f0i16l7k9ciang8adjbs5n0cxzmjqkbrsxi6")))

(define-public crate-rs-llama-cpp-0.1.44 (c (n "rs-llama-cpp") (v "0.1.44") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1g0fv3abphrq8wyv0z0jjcnviwzx4srhjyvs36j76g2qiidyfa8a")))

(define-public crate-rs-llama-cpp-0.1.45 (c (n "rs-llama-cpp") (v "0.1.45") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "16c65fvd5ffxxx3j715g6w2wshmmqi3nbcbml3am20fdj7gfqgc2")))

(define-public crate-rs-llama-cpp-0.1.46 (c (n "rs-llama-cpp") (v "0.1.46") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "11wp4z4k7x8rjlgj4vgfcz31y1yn2ccknq3piknhn5fhacizqjxq")))

(define-public crate-rs-llama-cpp-0.1.47 (c (n "rs-llama-cpp") (v "0.1.47") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "074rbpv86qkv6ydvn5lwmd3izvv7yqanybxnimzl73594fsr7b0l")))

(define-public crate-rs-llama-cpp-0.1.48 (c (n "rs-llama-cpp") (v "0.1.48") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0pv078ivda0ddir3xcw95cilr9qpkliqqap8miww5pl9akgg6kg3")))

(define-public crate-rs-llama-cpp-0.1.49 (c (n "rs-llama-cpp") (v "0.1.49") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0q6wd9arkn2gcrxg4vwv344pxjfff3cnb4b7a4mfhm5y6qp5k4k4")))

(define-public crate-rs-llama-cpp-0.1.50 (c (n "rs-llama-cpp") (v "0.1.50") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0fpcsc5vskv53386xxijla814c8kc256aj4mfsnav79wvcviy10r")))

(define-public crate-rs-llama-cpp-0.1.51 (c (n "rs-llama-cpp") (v "0.1.51") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0g07lh4k3r08spsmllzbnx2fiy2f0z70r8c5zp189smqhcyly1sk")))

(define-public crate-rs-llama-cpp-0.1.52 (c (n "rs-llama-cpp") (v "0.1.52") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0mcihzqlrf5pvnciz0v1qhgx9k976fd6wvlvigmmvk77103z6xq1")))

(define-public crate-rs-llama-cpp-0.1.53 (c (n "rs-llama-cpp") (v "0.1.53") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0xlvbg8fl8mfzi9jaxqrp24liz84k6fdadf883mmqj8rdklky2f7")))

(define-public crate-rs-llama-cpp-0.1.54 (c (n "rs-llama-cpp") (v "0.1.54") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1wr6jfkgvh1izhi97rjrn6lzh4jgm76b9216pzdjdaxdddd37w4i")))

(define-public crate-rs-llama-cpp-0.1.55 (c (n "rs-llama-cpp") (v "0.1.55") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "19qfjn2332qc7qdqfpyjc9lcnr1vjqn2gmz7zq2s2jkkxfk2kbnh")))

(define-public crate-rs-llama-cpp-0.1.56 (c (n "rs-llama-cpp") (v "0.1.56") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "007gy502fwn5pvaxxx79vch71wsjggrv9cm3672wx86s66zjf9cl")))

(define-public crate-rs-llama-cpp-0.1.57 (c (n "rs-llama-cpp") (v "0.1.57") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1rdws6a0jiw4f4jl8blf4dzmvbb9brphsvbvvvrjfpdh9h8i7f2l")))

(define-public crate-rs-llama-cpp-0.1.58 (c (n "rs-llama-cpp") (v "0.1.58") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0rfdknizkgn440zjdnv0h2fhnvkfkzz21zvwwyg993ddi6c1f2mj")))

(define-public crate-rs-llama-cpp-0.1.59 (c (n "rs-llama-cpp") (v "0.1.59") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "04hbxcv228ajdjzd0i0rfq62nqkyl0pw895drij2ix0zzl7g7zwj")))

(define-public crate-rs-llama-cpp-0.1.60 (c (n "rs-llama-cpp") (v "0.1.60") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1gvzdim0x9v840kf3ihk26zlb2kni2pgyk96kggjwi87rl3c0kbp")))

(define-public crate-rs-llama-cpp-0.1.61 (c (n "rs-llama-cpp") (v "0.1.61") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0szg8z6zfwxsh5ilgg325qy6zzgh9jyixwg3cxy0s90ai7pqklw8")))

(define-public crate-rs-llama-cpp-0.1.62 (c (n "rs-llama-cpp") (v "0.1.62") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1xd4ajqmlca7lpkwmygyjnkir5lmqb0i44a86bgj6624pxwv9mfs")))

(define-public crate-rs-llama-cpp-0.1.63 (c (n "rs-llama-cpp") (v "0.1.63") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0ka7pcwnflbssfjdg1gld0jfba0wz3366sj5dbaqv1g9xlarzcz2")))

(define-public crate-rs-llama-cpp-0.1.64 (c (n "rs-llama-cpp") (v "0.1.64") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1fl20z7bjkp3qrq76j764jjkjycd0irji9y68382a25jib3p4ppw")))

(define-public crate-rs-llama-cpp-0.1.65 (c (n "rs-llama-cpp") (v "0.1.65") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "097rj9v2qb6z7bgxrpxp5kndj16ww7gg6q6bn0r95vkjxagdsad3")))

(define-public crate-rs-llama-cpp-0.1.66 (c (n "rs-llama-cpp") (v "0.1.66") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1zybwf342a640m0zbij8fs5p2yyrgrza2zm4x6vqiffvd19h813y")))

(define-public crate-rs-llama-cpp-0.1.67 (c (n "rs-llama-cpp") (v "0.1.67") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0wvsxx4fv3i8bmh5fmfnjm08z3fprgzqhw3rji5cwvwbg61zsfpv")))

