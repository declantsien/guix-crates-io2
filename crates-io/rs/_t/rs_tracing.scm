(define-module (crates-io rs _t rs_tracing) #:use-module (crates-io))

(define-public crate-rs_tracing-1.0.0 (c (n "rs_tracing") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0dqrcvi4zzsgkwx5cjk0q9ixmnwlwayx1ryxfz2mb16drgz8nj8b") (f (quote (("rs_tracing" "serde_json" "serde" "time")))) (y #t)))

(define-public crate-rs_tracing-1.0.1 (c (n "rs_tracing") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0fy0d8ri46kqd7zncpvkrcfmh3xm0sykc0wwdbcya6x2b2zhrccj") (f (quote (("rs_tracing" "serde_json" "serde" "time"))))))

(define-public crate-rs_tracing-1.1.0 (c (n "rs_tracing") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1q31g35vjg9cwg677h2df3wdf38mvg4p4y8f270f29x61mkj3cg3") (f (quote (("rs_tracing" "serde_json" "serde"))))))

