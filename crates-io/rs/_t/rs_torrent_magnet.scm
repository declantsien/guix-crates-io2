(define-module (crates-io rs _t rs_torrent_magnet) #:use-module (crates-io))

(define-public crate-rs_torrent_magnet-0.1.0 (c (n "rs_torrent_magnet") (v "0.1.0") (d (list (d (n "bt_bencode") (r "^0.8.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1kcbakcy8a1a4hgaf9wrqa0faqrp44g7jirz1j0bhwaj2qvnyhhq")))

(define-public crate-rs_torrent_magnet-0.2.0 (c (n "rs_torrent_magnet") (v "0.2.0") (d (list (d (n "bt_bencode") (r "^0.8.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "06bq7ii34ci7bmay2ynqxgimpa6ypgw18k1maljzh9aa2hv8x5x0")))

