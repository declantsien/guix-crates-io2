(define-module (crates-io rs _t rs_triple_des) #:use-module (crates-io))

(define-public crate-rs_triple_des-0.1.0 (c (n "rs_triple_des") (v "0.1.0") (h "004vi6mrjxcq1ic0ggv3g6545q1k0z6zw6qip19hwm1mp97qlxlr")))

(define-public crate-rs_triple_des-0.1.1 (c (n "rs_triple_des") (v "0.1.1") (h "1y1zqxqnz6l8ca1zmxw1pd1w1nc5ppgnryigd51n1mn8fjakffml")))

(define-public crate-rs_triple_des-0.1.2 (c (n "rs_triple_des") (v "0.1.2") (h "1ysr73dmxr7vvwm68gq12jm4w6m42lg4c46bigy628a8xyvd6vd7")))

