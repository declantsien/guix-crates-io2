(define-module (crates-io rs _t rs_taskflow) #:use-module (crates-io))

(define-public crate-rs_taskflow-0.1.0 (c (n "rs_taskflow") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 0)) (d (n "rs_taskflow_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 2)))) (h "18q3if1jyycrf0k7mrj2ylh4rwyqsp6zp9zpp2ph5sy9s8sazmi0") (f (quote (("macro_task_ifaces") ("default" "macro_task_ifaces"))))))

