(define-module (crates-io rs _t rs_transducers) #:use-module (crates-io))

(define-public crate-rs_transducers-0.0.1 (c (n "rs_transducers") (v "0.0.1") (h "01793r3557lym6n7w1yywdli3jpmgl17afckrg31fa1s00sfybzi")))

(define-public crate-rs_transducers-0.0.2 (c (n "rs_transducers") (v "0.0.2") (h "1q7k27nksfvjpfhgzvi2lmashqd02g2703gxga83f4kp42rxgjr9")))

(define-public crate-rs_transducers-0.0.3 (c (n "rs_transducers") (v "0.0.3") (h "1q8rywd1lwkdm4ja1cysxd1gzfvxcslgs6x8m18h5v2hv0j10iz6")))

