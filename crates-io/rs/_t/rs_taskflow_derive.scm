(define-module (crates-io rs _t rs_taskflow_derive) #:use-module (crates-io))

(define-public crate-rs_taskflow_derive-0.1.0 (c (n "rs_taskflow_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "04g4hnvq8z3gxpipcdy49afpyaf9ikkc6f2hplrfd2y3gjjyv554")))

