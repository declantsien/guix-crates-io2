(define-module (crates-io rs -t rs-tree) #:use-module (crates-io))

(define-public crate-rs-tree-0.1.0 (c (n "rs-tree") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.16") (d #t) (k 0)))) (h "0g5rr1bv8xcvmg32b2kqx1vzwb2spn140g67463l05gr09kw1q80")))

(define-public crate-rs-tree-0.1.1 (c (n "rs-tree") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.16") (d #t) (k 0)))) (h "1b0vyz180pm95y8ssvyrhcb6m8simc775wcwn2m9c0ijn90hvcpx")))

(define-public crate-rs-tree-0.1.2 (c (n "rs-tree") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.16") (d #t) (k 0)))) (h "0bpgnd8ax5fhwqwgi8y9l6z6g4qhzr3p5hyh9cg3r8qp3ff35as0")))

