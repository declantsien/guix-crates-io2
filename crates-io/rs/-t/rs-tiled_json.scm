(define-module (crates-io rs -t rs-tiled_json) #:use-module (crates-io))

(define-public crate-rs-tiled_json-0.1.0 (c (n "rs-tiled_json") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0m0jrm6yib6rgrs1dxq51v5qbaysc448yq80hnjqk08wg80gcnwk")))

