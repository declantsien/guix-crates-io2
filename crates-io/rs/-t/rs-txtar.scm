(define-module (crates-io rs -t rs-txtar) #:use-module (crates-io))

(define-public crate-rs-txtar-0.1.0 (c (n "rs-txtar") (v "0.1.0") (h "1z7hmxyh1kx2iq9ia5bmfgb6zkqsqg7zh0bmiqdwhyqqsdiv787z")))

(define-public crate-rs-txtar-0.1.1 (c (n "rs-txtar") (v "0.1.1") (h "067s0z11pn88knh94g2kdq1v9hbqv4danwspfbvfw8cy6zmbfr7q")))

