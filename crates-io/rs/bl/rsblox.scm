(define-module (crates-io rs bl rsblox) #:use-module (crates-io))

(define-public crate-rsblox-0.0.1 (c (n "rsblox") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1n1bnswjibibamn3spmi38sb6yhkikhpkabgh76fji62175cb2sm") (y #t)))

