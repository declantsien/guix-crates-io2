(define-module (crates-io rs bl rsblk) #:use-module (crates-io))

(define-public crate-rsblk-0.1.0 (c (n "rsblk") (v "0.1.0") (d (list (d (n "anyhow") (r "~1.0.37") (d #t) (k 0)) (d (n "freebsd-geom") (r "~0.1.0") (d #t) (k 0)) (d (n "ptree") (r "~0.3.0") (f (quote ("ansi"))) (d #t) (k 0)) (d (n "strum") (r "~0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "~0.20.1") (d #t) (k 0)) (d (n "tabwriter") (r "~1.2.1") (f (quote ("ansi_formatting"))) (d #t) (k 0)))) (h "0dmjqw2ld25ansm391blsly7mazi9qha0qrywjml7a3c2cpkz91w")))

(define-public crate-rsblk-0.1.1 (c (n "rsblk") (v "0.1.1") (d (list (d (n "anyhow") (r "~1.0.37") (d #t) (k 0)) (d (n "freebsd-geom") (r "~0.1.2") (d #t) (k 0)) (d (n "ptree") (r "~0.3.2") (f (quote ("ansi"))) (d #t) (k 0)) (d (n "strum") (r "~0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "~0.20.1") (d #t) (k 0)) (d (n "tabwriter") (r "~1.2.1") (f (quote ("ansi_formatting"))) (d #t) (k 0)))) (h "0qd2zs35jfmvf7lsgak0dhs6pmw06bb4sbkb2mqhf60z5bm3qsg3")))

