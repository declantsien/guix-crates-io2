(define-module (crates-io rs bl rsblas) #:use-module (crates-io))

(define-public crate-rsblas-0.1.0 (c (n "rsblas") (v "0.1.0") (h "066lapybnz43pb0w1xzrrcn479vn2j637cw21h5maxrgz9gffddd")))

(define-public crate-rsblas-0.1.1 (c (n "rsblas") (v "0.1.1") (h "0d3gc0jja5a4v4hnih0s3g5idh3m9baf26cmra51dy8flnvy2kg2")))

(define-public crate-rsblas-0.1.2 (c (n "rsblas") (v "0.1.2") (h "1zjilhyxhy4k9mr56w56078ax3bfd8nqi6yb545sw93ghcl34ik7")))

(define-public crate-rsblas-0.1.3 (c (n "rsblas") (v "0.1.3") (h "02lgfhk09lkm37hvbpsq02j75gv884b1zavwrvg1rj76npjisrwb")))

(define-public crate-rsblas-0.1.4 (c (n "rsblas") (v "0.1.4") (h "1gb7qx1b76hqziv2zyh41k1yrrlnmf915a05m9d9h5mwsc8m0bpa")))

(define-public crate-rsblas-0.1.5 (c (n "rsblas") (v "0.1.5") (h "0kvaaf3nr6fwc7b9mv7a3l657yv9fbzkhll3sgnlrmkf3y2pdmy7")))

