(define-module (crates-io rs bl rsbloom) #:use-module (crates-io))

(define-public crate-rsbloom-0.1.0 (c (n "rsbloom") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2.6") (d #t) (k 2)) (d (n "fasthash") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "188s27i4b9mpic51ibvp1hfpvj0qrs3q4anaqf68hd3qdwmq6iz7")))

