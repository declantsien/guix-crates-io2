(define-module (crates-io rs _s rs_shake128) #:use-module (crates-io))

(define-public crate-rs_shake128-0.1.0 (c (n "rs_shake128") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "0l99n8p3908byhsgidza4riswaampbdk5k3jsvqvr4sm3jkfndmq")))

(define-public crate-rs_shake128-0.1.1 (c (n "rs_shake128") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "1hgslwa3rcsr0x6lrbr0q7gg6wwzafph8sr763pix69lsh3cpg35")))

(define-public crate-rs_shake128-0.1.2 (c (n "rs_shake128") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "1zs3hpyz02kcaxkdhkahhkpbybbri0228flm44kj8ckrf67hpqs9")))

