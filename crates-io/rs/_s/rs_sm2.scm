(define-module (crates-io rs _s rs_sm2) #:use-module (crates-io))

(define-public crate-rs_sm2-0.1.0 (c (n "rs_sm2") (v "0.1.0") (h "0icwj4i7hprnxhgd285d04cgxypxb8v9a1s8zcmnajh125g3icsz")))

(define-public crate-rs_sm2-0.1.1 (c (n "rs_sm2") (v "0.1.1") (h "0d1fbm1sirpvylpg82zqdih7bkjw6gcdkga2janlrmlrkzkrsxcz")))

(define-public crate-rs_sm2-0.1.2 (c (n "rs_sm2") (v "0.1.2") (h "1hcflzrkw9dls60g4dq0i2cnd8z8b19lszc6ilp1v5032bax32pl")))

