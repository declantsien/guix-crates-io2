(define-module (crates-io rs _s rs_sha512_256) #:use-module (crates-io))

(define-public crate-rs_sha512_256-0.1.0 (c (n "rs_sha512_256") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "0drsnlcdmcwf3vrfs9m39z0n6dbgsm2z83capqhg025naprbwajj")))

(define-public crate-rs_sha512_256-0.1.1 (c (n "rs_sha512_256") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "1bkz759yv4vrwgc16cznvbn8rv913qrchsl3ad5x7430dra0g61i")))

(define-public crate-rs_sha512_256-0.1.2 (c (n "rs_sha512_256") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0d633xpgj6zn1lg3a5n3lyszmbhxpp1kiyakiqp2l9pc55yn3fvc")))

