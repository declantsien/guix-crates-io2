(define-module (crates-io rs _s rs_sudoku) #:use-module (crates-io))

(define-public crate-rs_sudoku-0.1.1 (c (n "rs_sudoku") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1p84z4pji3a05p2dy4bphark7kmyk7dq98b1zszspcsw541skafc")))

(define-public crate-rs_sudoku-0.1.2 (c (n "rs_sudoku") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "17531qbajicxwzykzb0gl654sac7565fv7kbsxr71677kx0431rn")))

