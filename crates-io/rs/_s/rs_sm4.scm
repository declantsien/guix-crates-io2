(define-module (crates-io rs _s rs_sm4) #:use-module (crates-io))

(define-public crate-rs_sm4-0.1.0 (c (n "rs_sm4") (v "0.1.0") (h "1r3l237rbz2ld0mngk91wa3fcs6add31y0y7nvdbpna4a9bi6n19")))

(define-public crate-rs_sm4-0.1.1 (c (n "rs_sm4") (v "0.1.1") (h "1r7h30hd2jf6j8gsqbyl6jdhxlzscvmm5g8b2ih5ix7zmmdnd1b7")))

(define-public crate-rs_sm4-0.1.2 (c (n "rs_sm4") (v "0.1.2") (h "020mxa5farjgdkw9jxwy0sy9xrs78vx3kdsvfy6kc8nq3vs7a3zz")))

