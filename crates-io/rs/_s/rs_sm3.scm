(define-module (crates-io rs _s rs_sm3) #:use-module (crates-io))

(define-public crate-rs_sm3-0.1.0 (c (n "rs_sm3") (v "0.1.0") (h "1q3b8idpkl6kshax7v088hd65gmjcqisp6vavj0narkpi2h8rk26")))

(define-public crate-rs_sm3-0.1.1 (c (n "rs_sm3") (v "0.1.1") (h "1vnf80jc0v2jq1kyssbw0kjl2ss8wkjl0nys3fkzbjw52dfn7smi")))

(define-public crate-rs_sm3-0.1.2 (c (n "rs_sm3") (v "0.1.2") (h "0jkdp898v3nap7bdg5c2gdkfjq6fpahidblp39n4c3xzwjczb84k")))

