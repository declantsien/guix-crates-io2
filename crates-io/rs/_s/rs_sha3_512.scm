(define-module (crates-io rs _s rs_sha3_512) #:use-module (crates-io))

(define-public crate-rs_sha3_512-0.1.0 (c (n "rs_sha3_512") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "0fak8x9x86pkly389jlz1518h1hcrkzc1aiilm1a1rz7nbiid5sv")))

(define-public crate-rs_sha3_512-0.1.1 (c (n "rs_sha3_512") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0vnnsvi1i8fzwyh378r35i55f73b5y182b3z9ckrbg8qmipiackh")))

(define-public crate-rs_sha3_512-0.1.2 (c (n "rs_sha3_512") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "1j8qrsvw32nsd3nfv2r24wbb787j0wylgkr8l09qagrs1yymwarg")))

