(define-module (crates-io rs _s rs_sha3_384) #:use-module (crates-io))

(define-public crate-rs_sha3_384-0.1.0 (c (n "rs_sha3_384") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "0vfkfiiiracfqc5195zaib901yw2rkr5q6vvkp2niv5wrymk4ifp")))

(define-public crate-rs_sha3_384-0.1.1 (c (n "rs_sha3_384") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0ci7s3r77ggyzf9fpcq7h3n4h77ax3zg7gvnd55k1nn2386i9nkv")))

(define-public crate-rs_sha3_384-0.1.2 (c (n "rs_sha3_384") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "1yddv27f6641sh7n7f6vp5pxy2cmni6vnqvq9mvzqkb21kj1pv1f")))

