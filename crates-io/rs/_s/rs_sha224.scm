(define-module (crates-io rs _s rs_sha224) #:use-module (crates-io))

(define-public crate-rs_sha224-0.1.0 (c (n "rs_sha224") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "1pzdyp1fnhlkxawz81zpmcl342shm4wsvk9czk9faim40s8b9msf")))

(define-public crate-rs_sha224-0.1.1 (c (n "rs_sha224") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.0") (d #t) (k 0)))) (h "0riyi53biwbq7fl04qdb97ax5l3mk1i7qranrrpijbkilwb0birz")))

(define-public crate-rs_sha224-0.1.2 (c (n "rs_sha224") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.0") (d #t) (k 0)))) (h "0magympmmfc4y2fnmf0n87d886blfan105ryk6qzv6nh9ivrfg7c")))

(define-public crate-rs_sha224-0.1.3 (c (n "rs_sha224") (v "0.1.3") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0k0p38ylypmyk648hgdxnclrly5picyjbrzsi04f3f4nbg4vqb4k")))

(define-public crate-rs_sha224-0.1.4 (c (n "rs_sha224") (v "0.1.4") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0ab9s6kjgf783cgbpyvv75fsj9bx0qgndvs4b2hjky8fsv8awg2r")))

