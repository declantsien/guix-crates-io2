(define-module (crates-io rs _s rs_sha512_224) #:use-module (crates-io))

(define-public crate-rs_sha512_224-0.1.0 (c (n "rs_sha512_224") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "1cwcfrv44al92w47ddjv30za7lyf79chj4d8p4wzf56b4znidwj5")))

(define-public crate-rs_sha512_224-0.1.1 (c (n "rs_sha512_224") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "1z5ya2kgz5lz8kb4g62gi0zra1n4k4lc44xxgkp8z5r7qiymm327")))

(define-public crate-rs_sha512_224-0.1.2 (c (n "rs_sha512_224") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0xmil710zbmpldacf56ydq5v55bkx63d8h6pmjiz9dmsc7z5qvsk")))

