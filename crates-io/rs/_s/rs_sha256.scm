(define-module (crates-io rs _s rs_sha256) #:use-module (crates-io))

(define-public crate-rs_sha256-0.1.0 (c (n "rs_sha256") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "07qr5bxmalnggb022idk170pgjjhj33i5gqhhshpqrhbf713snh1")))

(define-public crate-rs_sha256-0.1.1 (c (n "rs_sha256") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.0") (d #t) (k 0)))) (h "1cd5mrbshr0l8prvvhycb381hrqx1pkmxw57qw4y9fds26cras88")))

(define-public crate-rs_sha256-0.1.2 (c (n "rs_sha256") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0f19ajanngb8hzkzdci7rhnycd259vdj4dxlww2hxq16l8nbiq4i")))

(define-public crate-rs_sha256-0.1.3 (c (n "rs_sha256") (v "0.1.3") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "024i4gricqjkj615z3yp0g5y8dicaa01fzx7wmrpxjxq1c9i5jci")))

