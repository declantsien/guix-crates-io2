(define-module (crates-io rs _s rs_sha3_256) #:use-module (crates-io))

(define-public crate-rs_sha3_256-0.1.0 (c (n "rs_sha3_256") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "04qcpykpdnxfkizvlvxhg12bv9a1j11cbwnssms4chaig84285g9")))

(define-public crate-rs_sha3_256-0.1.1 (c (n "rs_sha3_256") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "10pmshjbv7wy3xi1fdqfla7g2sjlvhr0k141b91csif16w3sz541")))

(define-public crate-rs_sha3_256-0.1.2 (c (n "rs_sha3_256") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0vcp79rsl993irxcrwckwrb56agbvcha4k39byjbyli0j1d47in9")))

