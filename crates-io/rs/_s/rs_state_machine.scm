(define-module (crates-io rs _s rs_state_machine) #:use-module (crates-io))

(define-public crate-rs_state_machine-0.1.0 (c (n "rs_state_machine") (v "0.1.0") (h "10y6xldn0wmix6yq36hm4qa4sn6d0p58pfc3cjhwv5720bzkpga1")))

(define-public crate-rs_state_machine-1.0.0 (c (n "rs_state_machine") (v "1.0.0") (h "145j1r863b179ma9nn0br2qd532lmi0jr72hbs8k0qdvgsa3ygp6")))

(define-public crate-rs_state_machine-1.0.1 (c (n "rs_state_machine") (v "1.0.1") (h "0v5n1hd4kg8hjrds383xj9laysmqfkhri1xbypgn71b1vi9zpzd3")))

(define-public crate-rs_state_machine-1.1.0 (c (n "rs_state_machine") (v "1.1.0") (h "0ng65fa0s0gg4cvj3zc43ky1awfi62ql275v418s9s41kldncmfb")))

(define-public crate-rs_state_machine-2.0.0 (c (n "rs_state_machine") (v "2.0.0") (h "0zcikrm3kvhjs5w19ig5g358ydsqcskvvrnxdbzvxg3g35907zxw")))

