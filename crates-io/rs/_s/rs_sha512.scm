(define-module (crates-io rs _s rs_sha512) #:use-module (crates-io))

(define-public crate-rs_sha512-0.1.0 (c (n "rs_sha512") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "0z25620gy1jvxcqg0krwxk134x2vvqlkr4jg4az166vya9ccc48q")))

(define-public crate-rs_sha512-0.1.1 (c (n "rs_sha512") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.0") (d #t) (k 0)))) (h "0ga7x4mlp88h3hvqx3rj2jr7qv39fg4ph95z8ram7x5cw1n4jcw6")))

(define-public crate-rs_sha512-0.1.2 (c (n "rs_sha512") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0vb9p3a06012iqvyklj8y2il0s3hkqymfs4cn1qq3gvb13ak9rdh")))

(define-public crate-rs_sha512-0.1.3 (c (n "rs_sha512") (v "0.1.3") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0mldwp5jzy0iq3mly0d2g3llmcqzgnvc6115mlpbvq7jpki3xfvq")))

