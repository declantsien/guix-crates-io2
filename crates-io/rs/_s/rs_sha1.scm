(define-module (crates-io rs _s rs_sha1) #:use-module (crates-io))

(define-public crate-rs_sha1-0.1.0 (c (n "rs_sha1") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "0q9n1xjhsdsl6dwikdivhcyps06s0lmfl97bfhz1bs4fxhazygb9")))

(define-public crate-rs_sha1-0.1.1 (c (n "rs_sha1") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0miczfbb4wpqr7r7564gq353zlcc75a9ryzf7vkmnnzbv76y0wj8")))

(define-public crate-rs_sha1-0.1.2 (c (n "rs_sha1") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "13ibvj1lawx3053givydlzdzgs03yjn69ay7y5fw7gz419pl03n7")))

(define-public crate-rs_sha1-0.1.3 (c (n "rs_sha1") (v "0.1.3") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "05i3dxn7fl94jylj6lqlb1rvpdijiwradsm8w7pa62dv0r3nf9l8")))

