(define-module (crates-io rs _s rs_sha3_224) #:use-module (crates-io))

(define-public crate-rs_sha3_224-0.1.0 (c (n "rs_sha3_224") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "12wdpx4q4p8rginnsq1i0nnp9lin9bcg704417wlqdfgwb5i4myv")))

(define-public crate-rs_sha3_224-0.1.1 (c (n "rs_sha3_224") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "1bxsr23g0cwaj45m680ybz52si0z1yi0hny54z8pgpr04zgyp2kb")))

(define-public crate-rs_sha3_224-0.1.2 (c (n "rs_sha3_224") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "09qyj0m4gl5jlbn9zvy03dpb7wa3z0ym328lsclfsyxviij2wkrw")))

