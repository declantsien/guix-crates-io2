(define-module (crates-io rs _s rs_sha384) #:use-module (crates-io))

(define-public crate-rs_sha384-0.1.0 (c (n "rs_sha384") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "1a67yvzw91g2rr2gld340qmikbf7y87gpvr8l34rid8y0jalpxh8")))

(define-public crate-rs_sha384-0.1.1 (c (n "rs_sha384") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.0") (d #t) (k 0)))) (h "03pphwsvbyzmga3q3afdg7ddr6y7c013n3jgm48975xsx8ryd17a")))

(define-public crate-rs_sha384-0.1.2 (c (n "rs_sha384") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "0w14gk7ihqmla070pjdn7z9jv711nrib9z2f7x9wmx70wm0dbv6x")))

(define-public crate-rs_sha384-0.1.3 (c (n "rs_sha384") (v "0.1.3") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "0.1.*") (d #t) (k 0)))) (h "1bn3365d70gcjsy4dp69i1w4mkmrcf257p9agnv7rkqn11gybj7k")))

