(define-module (crates-io rs _s rs_snake) #:use-module (crates-io))

(define-public crate-rs_snake-1.0.0 (c (n "rs_snake") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1kzzcmcrd5pg2hapgqbwbqnknibzajm7c6d5703k72lxp9h0n1d2")))

(define-public crate-rs_snake-1.0.1 (c (n "rs_snake") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "0cy1wip0p2jjc3xx3qva2plxp6q879p5fmz2dsgmd5mb7y2n98fi")))

(define-public crate-rs_snake-1.0.2 (c (n "rs_snake") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1mr4si96jk0sxk72g5j5q5id854556ik623v5is8v8j8ikvab0mw")))

(define-public crate-rs_snake-1.0.3 (c (n "rs_snake") (v "1.0.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "16daggf7kd91c17zq08dnx6xqkxdnp5m148vp8id7bgcg8rkx74b")))

(define-public crate-rs_snake-1.0.4 (c (n "rs_snake") (v "1.0.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1zhr4nkz14i7asbqayxx0fi4g890xhrz4y20mcfcxqrx26aaha93")))

