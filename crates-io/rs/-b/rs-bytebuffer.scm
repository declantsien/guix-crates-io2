(define-module (crates-io rs -b rs-bytebuffer) #:use-module (crates-io))

(define-public crate-rs-bytebuffer-0.1.0 (c (n "rs-bytebuffer") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1c8l35hh8b8wsyhv6gnwgxaxw88m74jws8k677ncvdb90wd03sv4") (y #t)))

(define-public crate-rs-bytebuffer-0.1.1 (c (n "rs-bytebuffer") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1m3ih1wjn40sm50vvi07651cfjiynx3f8shc53k4j6qh80z5gz4a") (y #t)))

(define-public crate-rs-bytebuffer-0.1.2 (c (n "rs-bytebuffer") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "14fj1g5cfyrj3raqjx9hhrna6i8andhhlgwrxi35phrscmgzlxws") (y #t)))

(define-public crate-rs-bytebuffer-0.1.3 (c (n "rs-bytebuffer") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "09bs842ax5idq59q9dcjpc7yyikc5jics5bmqxh3rqd747xd2qj2")))

