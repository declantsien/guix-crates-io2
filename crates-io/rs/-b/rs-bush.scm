(define-module (crates-io rs -b rs-bush) #:use-module (crates-io))

(define-public crate-rs-bush-0.1.0 (c (n "rs-bush") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0knz56ijh77bz3i21bd2wvic1z8nlqldvq94wd3msp6x0y5ssas6")))

(define-public crate-rs-bush-0.1.1 (c (n "rs-bush") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1ah4hyzzk7bafkswfdf6b29j6mzby9yb4gvv1czww31m6yq5vcil")))

(define-public crate-rs-bush-0.1.2 (c (n "rs-bush") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1vm0q8jciagkw01b6zgkhbnxvv7c5wcz6qg259p6p3gndpz58g0i")))

(define-public crate-rs-bush-0.1.3 (c (n "rs-bush") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1hmx42mcb6vfi2p659z88cpkknzqbv40lngz4pxqk9v03dmrhxrj")))

(define-public crate-rs-bush-0.1.4 (c (n "rs-bush") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "04wq28ymrwx09amgqy9bdw60xlsl5gwlxgig7d56h2gys3rqb1fp")))

(define-public crate-rs-bush-0.1.5 (c (n "rs-bush") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1mzj88nfn6irflk81cp23k52vq25w0wjlmrllzkpy730vwvgzfph")))

(define-public crate-rs-bush-0.1.6 (c (n "rs-bush") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1drj73iq1qccp7zln29024fgvjpqvqvmpphbiv0dqpwn4wngwi6g")))

(define-public crate-rs-bush-0.1.7 (c (n "rs-bush") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0bzxylps3mbgic7nh6wz457xlnjyg16jzj3j1dy74cyyj1ry2c98")))

(define-public crate-rs-bush-0.1.8 (c (n "rs-bush") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0hb654b5q2p19l97frwdly54ggkc9ksg86f03h05i3i2ldjxzqgv")))

