(define-module (crates-io rs -b rs-borsh) #:use-module (crates-io))

(define-public crate-rs-borsh-0.1.0 (c (n "rs-borsh") (v "0.1.0") (d (list (d (n "cursive") (r "^0.10") (d #t) (k 0)) (d (n "gettext") (r "^0.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1ds59bmwx6c4bnz1aqbjsfx4r8w4x5pv8cckvk52qyh6qhhnwi2r") (y #t)))

