(define-module (crates-io rs fs rsfs-tokio) #:use-module (crates-io))

(define-public crate-rsfs-tokio-0.5.0 (c (n "rsfs-tokio") (v "0.5.0") (d (list (d (n "async-stream") (r "^0.3.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.66") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.12") (f (quote ("fs"))) (d #t) (k 0)))) (h "1q1gq5i8yjf71h9cyc9hnv733an6kivi0dq1j179yzcz3hqfd79f")))

