(define-module (crates-io rs fs rsfsm) #:use-module (crates-io))

(define-public crate-rsfsm-0.1.0 (c (n "rsfsm") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0hnv636i89f3r4z84ig7rm5i8z9h8n86f12xq7yvda62sky3dp5w")))

(define-public crate-rsfsm-0.1.1 (c (n "rsfsm") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0zy34xc0miqvpdl160y4sjxbprpnj8y22bidzl2v2wdb55x77d89")))

