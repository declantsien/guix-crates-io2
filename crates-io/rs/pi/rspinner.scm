(define-module (crates-io rs pi rspinner) #:use-module (crates-io))

(define-public crate-rspinner-0.0.1 (c (n "rspinner") (v "0.0.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1q95ycjfa61cdjnqbffyhz1hggg342lx1p8akjpvpnpigkggp7s8")))

(define-public crate-rspinner-0.0.2 (c (n "rspinner") (v "0.0.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "09d73ks7s5f9bw9x8yydjgxb08mpnpkgzb26yb4822mbaamnyvj6")))

(define-public crate-rspinner-0.0.3 (c (n "rspinner") (v "0.0.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1hlcwl3nnjk25qw1sdsdgwyxg9jps24jxwf2fymjvdhxsk27kwla")))

(define-public crate-rspinner-0.0.4 (c (n "rspinner") (v "0.0.4") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "03dmz5381mc7fkkf9j003q719nqmd39pqd7xlw3xfnj1qpzy5n74")))

(define-public crate-rspinner-0.1.0 (c (n "rspinner") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "19gch0qxc2kakwlkpznwmnry6iwlk8gj61p38618lf8r797nlj5m")))

