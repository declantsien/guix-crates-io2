(define-module (crates-io rs pi rspirv-reflect) #:use-module (crates-io))

(define-public crate-rspirv-reflect-0.1.0 (c (n "rspirv-reflect") (v "0.1.0") (d (list (d (n "rspirv") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vmf6k5d7njkgrxhj66jb7ppq7qyybk7x9908fk9k2ayxzd7r589")))

(define-public crate-rspirv-reflect-0.1.1 (c (n "rspirv-reflect") (v "0.1.1") (d (list (d (n "rspirv") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15azw156198rsmp0ry2xhczsayn93ni9irsygr907vsm47zg1dsm")))

(define-public crate-rspirv-reflect-0.1.2 (c (n "rspirv-reflect") (v "0.1.2") (d (list (d (n "rspirv") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wrly7lz9wygv0nsih2vvfk6r8wvwcarsx9fdanj3qv0xx124zpl")))

(define-public crate-rspirv-reflect-0.1.3 (c (n "rspirv-reflect") (v "0.1.3") (d (list (d (n "rspirv") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g37v8xypz8062s5s0pfvvnzrjc0mlc5rf9gpvswq03xzg6acd2s")))

(define-public crate-rspirv-reflect-0.2.0 (c (n "rspirv-reflect") (v "0.2.0") (d (list (d (n "rspirv") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mcvhr644z2h68n7qzqq7rwz9d4pbji2h365g2dcgb8vid1czzbv") (y #t)))

(define-public crate-rspirv-reflect-0.3.0 (c (n "rspirv-reflect") (v "0.3.0") (d (list (d (n "rspirv") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zpm3z1a5lrksxp22mqqs5nzkf2nhcximbc6pg6r183nwczbbjrv")))

(define-public crate-rspirv-reflect-0.3.1 (c (n "rspirv-reflect") (v "0.3.1") (d (list (d (n "rspirv") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pn9w4rrbrrvz8wsyk8v71pa1kspnnra0nrvvm5hdn2r0vqp43yx")))

(define-public crate-rspirv-reflect-0.4.0 (c (n "rspirv-reflect") (v "0.4.0") (d (list (d (n "rspirv") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hda9dfpiv7x4adwhcgl5y9i585zqcx2pqh76l0hhch8qll5ahfm")))

(define-public crate-rspirv-reflect-0.4.1 (c (n "rspirv-reflect") (v "0.4.1") (d (list (d (n "rspirv") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xzpwl41wi3hnqkwzfhx4g6ixrb7l9ljbmfgl8dx3xxyb70qlvjr")))

(define-public crate-rspirv-reflect-0.5.0 (c (n "rspirv-reflect") (v "0.5.0") (d (list (d (n "rspirv") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g13x19rmhagrzc1v1vfylvcarbbiicln9gd8d8qyd3rdkahkh68")))

(define-public crate-rspirv-reflect-0.6.0 (c (n "rspirv-reflect") (v "0.6.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ggbdkf2c4wdm233gdp3szz65pv314amqz5w88i19bijrrhqwlmn")))

(define-public crate-rspirv-reflect-0.7.0 (c (n "rspirv-reflect") (v "0.7.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0msmn6qv41cnihmqb9qcbdyjh9fqpq4fl60y20m68424hvn1zf5r")))

(define-public crate-rspirv-reflect-0.8.0 (c (n "rspirv-reflect") (v "0.8.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09ycrwy83gws4gk2a270pysq93w39wpy36w36nhilzj09lm470ml")))

