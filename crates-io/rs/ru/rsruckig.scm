(define-module (crates-io rs ru rsruckig) #:use-module (crates-io))

(define-public crate-rsruckig-0.1.0 (c (n "rsruckig") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.39") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "typenum") (r "^1.17") (d #t) (k 0)))) (h "0ms2vsc9ld64l0331aan13k4rjq24br4njqppvmkxxjha4p7ph71")))

(define-public crate-rsruckig-0.1.1 (c (n "rsruckig") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.39") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "typenum") (r "^1.17") (d #t) (k 0)))) (h "1fz9j6xk0306za5mmg2k0csws0h8gbhdjiynvlb9xgswbmpqkshh")))

(define-public crate-rsruckig-0.1.2 (c (n "rsruckig") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "1yfxqsf3n2i2zh4mwlszj0rw0yf0p5lsqc9va8qviq7jrwqx3fzk")))

(define-public crate-rsruckig-0.1.3 (c (n "rsruckig") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "1f0iyq4b2vmqqi6y3br54c02vyw1cgclgh10x52h7w33435l1qsn")))

(define-public crate-rsruckig-1.0.0 (c (n "rsruckig") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "17s9mqgp63y2258ipwqzlzjxma1k341ydzp78d95xhcib514w4dv")))

(define-public crate-rsruckig-1.0.1 (c (n "rsruckig") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "1y2x51ick6y4qb1xz4vm725lrvzyqbwkihpn9clnxfr9rxi2amr6")))

(define-public crate-rsruckig-1.0.2 (c (n "rsruckig") (v "1.0.2") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "13kghn987wr9ajz4yl6xjaggf7595w903q804ib7hc8rg2qq15yw")))

(define-public crate-rsruckig-1.1.0 (c (n "rsruckig") (v "1.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "1gnw2zxxg5hr96x6jff93zndlsc01rhylv1baf2q2694839hcl0w")))

(define-public crate-rsruckig-1.1.1 (c (n "rsruckig") (v "1.1.1") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "05xi41s3gs8wl33vsh3lqzzv8pa4p5id4jxfpy29fr5llqd1mfd7")))

(define-public crate-rsruckig-1.1.2 (c (n "rsruckig") (v "1.1.2") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "06yjry6vr1cs5b6xyywq5928w9k5i6nac6y29k7gw29vr032q4s2")))

(define-public crate-rsruckig-1.1.3 (c (n "rsruckig") (v "1.1.3") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "0gsmrcw2cyiayrk1pczwil91qsijipgzk1bb9rjf8lf0yfkdkh71")))

(define-public crate-rsruckig-1.1.4 (c (n "rsruckig") (v "1.1.4") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)))) (h "13skwlbivd4xxp38j809g018rkzvcp9jj9j64b0bczx1y8x0cz02")))

