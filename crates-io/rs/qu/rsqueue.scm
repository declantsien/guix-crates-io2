(define-module (crates-io rs qu rsqueue) #:use-module (crates-io))

(define-public crate-rsqueue-0.1.0 (c (n "rsqueue") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.5") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.3") (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "117qv8psi10diqcimia77xpd0qyabgas6k1ymdhl32754sr80vr6")))

