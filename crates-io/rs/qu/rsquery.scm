(define-module (crates-io rs qu rsquery) #:use-module (crates-io))

(define-public crate-rsquery-0.1.0 (c (n "rsquery") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13wa0bnncq3drgb7yw285wigwd8jm1z9xprjbryxz1870nmic1yq")))

(define-public crate-rsquery-0.1.1 (c (n "rsquery") (v "0.1.1") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i4rvdsagjyi2ma2ai340kp589bcb811vy7anhimlpysjf6d2bpn")))

