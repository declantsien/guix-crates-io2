(define-module (crates-io rs to rstorage) #:use-module (crates-io))

(define-public crate-rstorage-1.0.0 (c (n "rstorage") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "simple_wal") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "1lyzn2k9ylxpi2a0gwajmzlx2f7m9hyyzqb50irk7ix73xy3vnyp")))

(define-public crate-rstorage-1.0.1 (c (n "rstorage") (v "1.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "simple_wal") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "1xh83bpisfxmvzr394ai4zjswf5wa066vbbwxy76mglagb1annma")))

(define-public crate-rstorage-1.0.2 (c (n "rstorage") (v "1.0.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "simple_wal") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)))) (h "00qxsl9n81vy2z6944gd40qmfiqd50hljnl154dkkj3r5pbl6pvv")))

