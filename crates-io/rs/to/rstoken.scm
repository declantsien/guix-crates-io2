(define-module (crates-io rs to rstoken) #:use-module (crates-io))

(define-public crate-rstoken-0.0.0 (c (n "rstoken") (v "0.0.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "149x013nfd3agxix72npym1yks3xnjw4ha2yf4bx7dxpjj92kj3y")))

