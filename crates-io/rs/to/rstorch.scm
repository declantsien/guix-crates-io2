(define-module (crates-io rs to rstorch) #:use-module (crates-io))

(define-public crate-rstorch-0.1.0 (c (n "rstorch") (v "0.1.0") (h "0ikbiaca1y240kv3d39airp9xg7a7a5b1d73cxam8s7vyvnifg7a")))

(define-public crate-rstorch-0.2.0 (c (n "rstorch") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0.26") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "191fxy1g5xs4cfiiifhk0drq91q037bp4609bd9j65yzhy2nwj7m") (f (quote (("full" "dataset_hub") ("default")))) (s 2) (e (quote (("dataset_hub" "dep:reqwest" "dep:flate2"))))))

