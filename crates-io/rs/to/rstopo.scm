(define-module (crates-io rs to rstopo) #:use-module (crates-io))

(define-public crate-rstopo-0.0.0 (c (n "rstopo") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "smart-default") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (k 0)))) (h "0wfmn3jm1jp3gdv95a1gq1xrlwz1ajpn01wbvhvn9kmwba9a8gw0") (f (quote (("wasm") ("wasi") ("rand" "num/rand") ("full" "default" "rand" "serde") ("default" "std")))) (s 2) (e (quote (("std" "alloc" "num/std" "serde?/std") ("serde" "dep:serde") ("alloc" "num/alloc" "serde?/alloc"))))))

