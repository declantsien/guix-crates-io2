(define-module (crates-io rs to rstop) #:use-module (crates-io))

(define-public crate-rstop-0.0.1 (c (n "rstop") (v "0.0.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.23") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.2") (k 0)) (d (n "tabled") (r "^0.8.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rsg1yih1d7ci1b1zm7c5v3pnhsfp5f5zg8avsfkgvhhsc2i71vb")))

(define-public crate-rstop-0.0.2 (c (n "rstop") (v "0.0.2") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.23") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.2") (k 0)) (d (n "tabled") (r "^0.8.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jllymx666bwmwirc6fb9ifi3qkc5s3ny8gwnxljhwx0l32s1mpc")))

