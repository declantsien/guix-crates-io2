(define-module (crates-io rs pe rspec) #:use-module (crates-io))

(define-public crate-rspec-0.1.0 (c (n "rspec") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "expectest") (r "^0.5.1") (d #t) (k 0)))) (h "1fb2slc2pspz92w22nxyjvlsdj4ic1dqwa1z161lskxhxvp17w32") (f (quote (("default"))))))

(define-public crate-rspec-1.0.0-beta.1 (c (n "rspec") (v "1.0.0-beta.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1kr1hmf9rx8mn970bbc5pkkbcxmh2qyc8w162zb2hd757976idah") (f (quote (("default"))))))

(define-public crate-rspec-1.0.0-beta.2 (c (n "rspec") (v "1.0.0-beta.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0nb1xr9qrl1s6qcimsifsjl56l44if1l8kflj7l44y0gq6sx5vh8") (f (quote (("default"))))))

(define-public crate-rspec-1.0.0-beta.3 (c (n "rspec") (v "1.0.0-beta.3") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1qkafvyg3r3h4ffhb7bhzq54mxxbirn2hk693wxdv5zhdjx68a99") (f (quote (("default"))))))

(define-public crate-rspec-1.0.0-beta.4 (c (n "rspec") (v "1.0.0-beta.4") (d (list (d (n "clippy") (r "^0.0.153") (o #t) (d #t) (k 1)) (d (n "colored") (r "^1.4.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "expectest") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1abfzwkbxlwahb243k8d3fp6i135lx1aqmbfl79w9zlpng182ndk") (f (quote (("expectest_compat" "expectest") ("default"))))))

(define-public crate-rspec-1.0.0 (c (n "rspec") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0.153") (o #t) (d #t) (k 1)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "expectest") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "02hfwxqjdc39ygnjysvn5qz343fahmwm16rxvxayh403d5y9wf49") (f (quote (("expectest_compat" "expectest") ("default"))))))

