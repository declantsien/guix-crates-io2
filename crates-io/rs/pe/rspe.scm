(define-module (crates-io rs pe rspe) #:use-module (crates-io))

(define-public crate-rspe-0.1.0 (c (n "rspe") (v "0.1.0") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Diagnostics_Debug" "Win32_System_LibraryLoader" "Win32_System_Memory" "Win32_System_SystemServices" "Win32_Foundation" "Win32_System_SystemInformation"))) (d #t) (k 0)))) (h "1q3mvlb057xn7fnkzd7ma8njmm34ymz3lrhrspi77rzqyk48r51l") (y #t)))

(define-public crate-rspe-0.1.1 (c (n "rspe") (v "0.1.1") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Diagnostics_Debug" "Win32_System_LibraryLoader" "Win32_System_Memory" "Win32_System_SystemServices" "Win32_Foundation" "Win32_System_SystemInformation"))) (d #t) (k 0)))) (h "19jhpm61wdd8grn1r7jj9891mzbi7kn0d6944v1v82kaii8dyqcf")))

(define-public crate-rspe-0.1.2 (c (n "rspe") (v "0.1.2") (h "1r3nkx7iaqs66jd49828jrwph25ln60dqf8xbrazyps24ygm40n0")))

