(define-module (crates-io rs bo rsbot) #:use-module (crates-io))

(define-public crate-rsbot-0.1.0 (c (n "rsbot") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s5yxm1y19pgbr7hlj97mxqjbf4k7agz3dr8f7krzp5lhcav2m9c")))

(define-public crate-rsbot-0.1.1 (c (n "rsbot") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "079k6yxq0rb8ymdq9m72c4wan5fb5lfnpm7l1j4vb5dgazlr88fv")))

(define-public crate-rsbot-0.2.0 (c (n "rsbot") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rsrnb74ds5md96ln8krm5hqzwb4jj6lxig93ximjps43s6yf68s")))

(define-public crate-rsbot-0.2.1 (c (n "rsbot") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0d9982yrqmjrcdgsh2x5z3751q951lwb3z788bqi6w65ac7skm6h")))

(define-public crate-rsbot-0.2.2 (c (n "rsbot") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ij6p6z2a7hag88shbm8jckd70pyx6l9dhs6gz785fys5rf4f8zf")))

(define-public crate-rsbot-0.2.3 (c (n "rsbot") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c0ncv3fqdfcjkjy1la1mj18y76dpma1fx4j7njpxzbaps6khdfm")))

