(define-module (crates-io rs bo rsbotters) #:use-module (crates-io))

(define-public crate-rsbotters-0.0.1 (c (n "rsbotters") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15580714girxi4k7djmrp5srxavwsdpcprgxh01dl6hrvh49njjk") (y #t)))

(define-public crate-rsbotters-0.0.2 (c (n "rsbotters") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17w3qwrcgbj4rml01y4zal8clki0b4x6clj4p33by6fbs1q7rlka") (r "1.69.0")))

