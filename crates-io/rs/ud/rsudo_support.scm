(define-module (crates-io rs ud rsudo_support) #:use-module (crates-io))

(define-public crate-rsudo_support-0.1.0 (c (n "rsudo_support") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "04d84hjw36h1si15djp1fd3871z43p0m6a7k863nj3d1adg83q0f")))

(define-public crate-rsudo_support-0.1.1 (c (n "rsudo_support") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "11xkgh2q72ap93jwxv553ifk61fgqvcw83cscik6gp2n0fzb5ani")))

(define-public crate-rsudo_support-0.1.2 (c (n "rsudo_support") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0vi6w8j1mh3f2yy2z44d9sbwgb12bxj9xx2xwngggvrp1zm1jlw4")))

(define-public crate-rsudo_support-0.1.3 (c (n "rsudo_support") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1d7m4l3x7bmgrm9avxcwywvyyhx4mv3d9zxxdm4a3qhpcixiwxvi")))

(define-public crate-rsudo_support-0.1.4 (c (n "rsudo_support") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0nvzbhsskwnz6b0xagsf16mplr9gr3bqm0rhnkrdfd7hbzz9qvax")))

(define-public crate-rsudo_support-0.1.5 (c (n "rsudo_support") (v "0.1.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "10dy8lvx8zplay996yp53bp2nfy32n3r23mvmca6l7grpiy2h8mg")))

