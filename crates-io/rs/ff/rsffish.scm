(define-module (crates-io rs ff rsffish) #:use-module (crates-io))

(define-public crate-rsffish-0.1.0 (c (n "rsffish") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "11mvhcvkjiidp60hlc2ah27ksdmkm0pi7mkf5wihlmmvj948nl7b") (y #t)))

(define-public crate-rsffish-0.1.1 (c (n "rsffish") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "104yk73zpjbv9fg1wn1i4l18qi97d608ppzpdmf4ag2lpiblzawa") (y #t)))

(define-public crate-rsffish-0.1.2 (c (n "rsffish") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0d0x6vycgy49h7pmcgblcrzzp330ckgppcr4b19331ky7nkn3nzh") (y #t)))

(define-public crate-rsffish-0.0.6 (c (n "rsffish") (v "0.0.6") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "02fkx1y3wqp9zhr0lb881d3236lr1sjmn2y7chv4bkafn7ph5k74")))

(define-public crate-rsffish-0.0.7 (c (n "rsffish") (v "0.0.7") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0n44f9lznr81lk7jsv1rj9346vq33v5czisypi6ix9qch1156nrq")))

(define-public crate-rsffish-0.0.8 (c (n "rsffish") (v "0.0.8") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0xh76iaql2imfxz1a6z0g0dzyhrnkbzqhbac1iknxdswqmjfj8jl")))

(define-public crate-rsffish-0.0.9 (c (n "rsffish") (v "0.0.9") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1dazmx381hirairg9nfa0dcqs9wkbnfvq7rm5k1s0dd83h7bj96m")))

(define-public crate-rsffish-0.0.10 (c (n "rsffish") (v "0.0.10") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "03jvl0a6n9k2dsjx5ylwy9cqfs097gp96cchw6hj1avxdyl1m882")))

(define-public crate-rsffish-0.0.11 (c (n "rsffish") (v "0.0.11") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0njjqjm5hxj4shjkzhxi9pqkhkvchhjgnsrmr8m8r59bcn121k9s")))

(define-public crate-rsffish-0.0.12 (c (n "rsffish") (v "0.0.12") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0zvphy6118rx34r32wh95jpl9kj95vp7i0c24hrfk4mchjzj88b0")))

(define-public crate-rsffish-0.0.13 (c (n "rsffish") (v "0.0.13") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "08dvzgjiygivkn51zxjyhh5w6qs41lvzkj8fzs4ddq4axdkx2hmc")))

(define-public crate-rsffish-0.0.14 (c (n "rsffish") (v "0.0.14") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0v90n595yf6f4spjbpws079qmb9rh0clzrbr60ps9j5f11w61s5g")))

(define-public crate-rsffish-0.0.15 (c (n "rsffish") (v "0.0.15") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "06978jg0pmy98ppdzydgl32w9y4la8m10kmkasgczxm0i7265mcr")))

(define-public crate-rsffish-0.0.18 (c (n "rsffish") (v "0.0.18") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0z1gfrf9lbx9y9z3yqs9aplykjv288h75p257yj2z4i4d8l02cac")))

(define-public crate-rsffish-0.0.18-amazonsfix1 (c (n "rsffish") (v "0.0.18-amazonsfix1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1d2gmlxh4l368799wly08zh16awb6hf093jwfca0080lyffp3m1w")))

