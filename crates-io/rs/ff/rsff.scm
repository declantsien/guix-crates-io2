(define-module (crates-io rs ff rsff) #:use-module (crates-io))

(define-public crate-rsff-1.0.0 (c (n "rsff") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.1") (d #t) (k 0)))) (h "1cc84h0hg5bbah3f60rgm1g36i3f0g0cmvzmyp2k824l9b2w4as8")))

