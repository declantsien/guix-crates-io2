(define-module (crates-io rs #{4j}# rs4j) #:use-module (crates-io))

(define-public crate-rs4j-0.1.0 (c (n "rs4j") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0xzp6w7730lh398if36adrqva4x5rpjd9pdmshs5vg6dwhsw17xs")))

(define-public crate-rs4j-0.1.1 (c (n "rs4j") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0f7b4vkw1d2hj664zhivkzk1is08mmi1phrfch5pnh6fzyx4014q")))

(define-public crate-rs4j-0.2.0 (c (n "rs4j") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0c66gng3hax7z6bzi5qlfc7h0asddbafaqjs50hrf7dxycqgxag9")))

(define-public crate-rs4j-0.2.1 (c (n "rs4j") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "11j61jka6gbcnjjzprg7hmjw46wx19icq2fpaq0fivkljh0bsg0d")))

(define-public crate-rs4j-0.3.0 (c (n "rs4j") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "12wzq5b4xgw1ri00xdifsb760hfnzvk32s59bjxwmvwifhqzkprj")))

(define-public crate-rs4j-0.3.1 (c (n "rs4j") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "05a9z05cy06qwzma90xy4iiwhxqw5hx1vfj7sirzys8z1c1sndw7")))

(define-public crate-rs4j-0.4.0 (c (n "rs4j") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1vp2scaxmcclj58398b3ryqqrjbx4rlqgf7sbq8n8nw540f3jpnb")))

(define-public crate-rs4j-0.4.1 (c (n "rs4j") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1xgk11iqp34dffmlrr9bqjlrzbd79b8ssssr625vhls3bj2h5z1k")))

