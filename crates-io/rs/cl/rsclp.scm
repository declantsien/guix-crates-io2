(define-module (crates-io rs cl rsclp) #:use-module (crates-io))

(define-public crate-rsclp-0.1.0 (c (n "rsclp") (v "0.1.0") (h "1byr0gx6mihlp1kap5fm7npx3hcf06mgmggj8w4p439hx0swnnmw")))

(define-public crate-rsclp-0.1.1 (c (n "rsclp") (v "0.1.1") (h "0wliina632m1i74hqxslz6ckcj511dmlrzwhg0kv2nmhpxp6z0a0")))

(define-public crate-rsclp-0.1.2 (c (n "rsclp") (v "0.1.2") (h "092650y66a6821539miifs417svlwnl4xdjkvqvifzrkqm3ayj7l")))

(define-public crate-rsclp-0.1.3 (c (n "rsclp") (v "0.1.3") (h "101jaddnqr865ngq0zbzm550wzkqvmzq2vs3mg035wk4xcg75cvj")))

(define-public crate-rsclp-0.1.4 (c (n "rsclp") (v "0.1.4") (h "0v9li8ayia9y5hzqs9hgwi2vklkasmdfrl0sasdqyas49h61sxlp")))

(define-public crate-rsclp-0.1.5 (c (n "rsclp") (v "0.1.5") (h "1r0w85ga3wsazkw37zgc3w3lgn0rd7jfbhdkg4z6bpmi9sfz9skm")))

(define-public crate-rsclp-0.1.6 (c (n "rsclp") (v "0.1.6") (h "1slr30vp87v5h7szmn1c07q88mvhqnd1ngznsp7pxzpm2cnpiavs")))

