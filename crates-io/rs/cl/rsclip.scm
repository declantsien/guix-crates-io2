(define-module (crates-io rs cl rsclip) #:use-module (crates-io))

(define-public crate-rsclip-0.1.0 (c (n "rsclip") (v "0.1.0") (d (list (d (n "copypasta-ext") (r "^0.4.4") (d #t) (k 0)))) (h "00fh8m6ifpsiz39snn570hkjaxk62w7rm5dvafbi8z1sn4ms8pbp")))

(define-public crate-rsclip-0.2.0 (c (n "rsclip") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "copypasta-ext") (r "^0.4.4") (d #t) (k 0)))) (h "12kk6cvk82fkw33v3izxirs7njh1difr6cqykssh8nps73dx2wpa")))

(define-public crate-rsclip-0.2.1 (c (n "rsclip") (v "0.2.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "copypasta-ext") (r "^0.4.4") (d #t) (k 0)))) (h "0kifkr54i93frv258h0zar4izrnjgnsq6812ks3ccas4x0axaymk")))

