(define-module (crates-io rs cl rsclock) #:use-module (crates-io))

(define-public crate-rsclock-0.1.1 (c (n "rsclock") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1zdxnx4hq8jivlywnvgw00pcvhsclbd5xkw04wc9gshpdkxysqbs")))

(define-public crate-rsclock-0.1.2 (c (n "rsclock") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0n1vdv70vn3x9qbq4q5rnvxs5fycs7yp7rkpzjrvx00p0zcfb08b")))

(define-public crate-rsclock-0.1.3 (c (n "rsclock") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "01vsbx84rdgfxsvx9hp9alfj4rvd15m02my85ha8nnfhr9lk6h7i")))

(define-public crate-rsclock-0.1.4 (c (n "rsclock") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1h7i2sq1x1qvgawxrzb4m1rq8l3k4chh7nk4mk9h24hm1jyawm2j")))

(define-public crate-rsclock-0.1.6 (c (n "rsclock") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1g1145k9vqk9yj5i22qb4p90jld8mmwgxi2dig1zrmx3xg427iv3")))

(define-public crate-rsclock-0.1.7 (c (n "rsclock") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0sw9fbqrba5kzyv579yy4kx232hnbvhh6frax04fbai8nk548s1r")))

(define-public crate-rsclock-0.1.9 (c (n "rsclock") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "03qp0w58dijah7ggzfgvp2gh2ghbwy7nssbvpxc2ky674009bxcn")))

(define-public crate-rsclock-0.1.10 (c (n "rsclock") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "062g45vzvvgn0wm8bxy3w7768k0m7izsvsv0kgfq7rj6a67dw6sv")))

(define-public crate-rsclock-0.1.11 (c (n "rsclock") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1acw1la7gki0q9m535ysgpyqvwibwns1dlszxxnsbsy4jak6vval")))

