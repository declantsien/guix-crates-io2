(define-module (crates-io rs ts rsts) #:use-module (crates-io))

(define-public crate-rsts-0.1.0 (c (n "rsts") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ac0clfgly6m9j69ybk187pw5mq8x8wsgcblahvf2bxd9frwmfzw")))

(define-public crate-rsts-0.2.0 (c (n "rsts") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "019kyccb4d0mizinn3r8pi1h5hl29m0kjwdx8lj40jiyp9smnp7l")))

(define-public crate-rsts-0.2.1 (c (n "rsts") (v "0.2.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1wddvbwhbv707f7cahbifwp5q53jr6n3kvpzc2hzmg38sjn0835k")))

