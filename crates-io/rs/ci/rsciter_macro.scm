(define-module (crates-io rs ci rsciter_macro) #:use-module (crates-io))

(define-public crate-rsciter_macro-0.0.1 (c (n "rsciter_macro") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "091sjng4v3ii7jwbazfgjh5hhznj2xsn1ym2hk64ln4ym2rqbi30")))

(define-public crate-rsciter_macro-0.0.2 (c (n "rsciter_macro") (v "0.0.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wxas119vrl0z9m5fi3dd8cwkl584ybm1ikgnlbc6mc3wbpvikvy")))

(define-public crate-rsciter_macro-0.0.3 (c (n "rsciter_macro") (v "0.0.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v03avlrxylryrypkl23pxi4qv6s36qln2qsq1nvmf3m64n279bg")))

(define-public crate-rsciter_macro-0.0.4 (c (n "rsciter_macro") (v "0.0.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q1xxpj1rrbz464x5cwvg1vvd9r84jwzyr083ckx8kg1dv7mr0rg")))

(define-public crate-rsciter_macro-0.0.5 (c (n "rsciter_macro") (v "0.0.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bfr2bigy6ybpdbrzk8l9w7r8mnk4c7rnd5skriqh3d7nbppsh7j")))

(define-public crate-rsciter_macro-0.0.6 (c (n "rsciter_macro") (v "0.0.6") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0yl47jif83z8w34mh8ncvqphmh6sc1ivk5fifrn64z2s0m68y32c")))

