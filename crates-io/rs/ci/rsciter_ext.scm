(define-module (crates-io rs ci rsciter_ext) #:use-module (crates-io))

(define-public crate-rsciter_ext-0.0.1 (c (n "rsciter_ext") (v "0.0.1") (h "1j0jnbqkyy7xnji6z505sqhp59ilz9rhmxdppn547xrx860q6y8l")))

(define-public crate-rsciter_ext-0.0.2 (c (n "rsciter_ext") (v "0.0.2") (h "0pa8lwfh6nk72mpymylixhx14b7w8dsfcazzmq1qawp6f9a7lsw7")))

(define-public crate-rsciter_ext-0.0.3 (c (n "rsciter_ext") (v "0.0.3") (h "0d6rf43hvah3x1x2mks8b6zqvgxnbgg8ima3zsnym149r7j8ryqi")))

(define-public crate-rsciter_ext-0.0.4 (c (n "rsciter_ext") (v "0.0.4") (h "0h0v764w29cxipbp5wj062gwlvrkh0r17kpjaf9wqvqnph5p5vqp")))

(define-public crate-rsciter_ext-0.0.5 (c (n "rsciter_ext") (v "0.0.5") (h "085x87d1h442x4fi57f879awz1pf7jnmcncby01plkj9k4370i22")))

(define-public crate-rsciter_ext-0.0.6 (c (n "rsciter_ext") (v "0.0.6") (h "0w21s5gv6bsjamw831i6xmpvgah4zi5mblj6l01shzhrv3plsdx0")))

