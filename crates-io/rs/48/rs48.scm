(define-module (crates-io rs #{48}# rs48) #:use-module (crates-io))

(define-public crate-rs48-1.0.0 (c (n "rs48") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "04cds80s1721bz57k1zfi2jjzj43h4jd7mr6w8rr0hcnkpjbagb0")))

(define-public crate-rs48-1.1.0 (c (n "rs48") (v "1.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rs48_lib") (r "^1.1") (d #t) (k 0)))) (h "1087rrkkjpkcb97xzf1nb0zz7430imnn0zy96z0ycsk8hwihkjwi")))

(define-public crate-rs48-1.2.0 (c (n "rs48") (v "1.2.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rs48_lib") (r "^1.2") (d #t) (k 0)))) (h "1kgsag667j6faxk74xdwpy017jbfd1lq0v7r0wazkfbayh9x2ak4")))

(define-public crate-rs48-1.3.0 (c (n "rs48") (v "1.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rs48_lib") (r "^1.3") (d #t) (k 0)))) (h "0c8b7pqwb9cg1qgn5lh6b6h26q5sig5j4jqwslkd91sydqj46kf3")))

(define-public crate-rs48-1.3.1 (c (n "rs48") (v "1.3.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rs48_lib") (r "^1.3") (d #t) (k 0)))) (h "1zhhja0wiv4f9ixjb31hmkqfnmnp2nfdr12nppawnkdnx3kcmjp9")))

(define-public crate-rs48-1.3.2 (c (n "rs48") (v "1.3.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rs48_lib") (r "^1.3") (d #t) (k 0)))) (h "1lq21zyzmqjfi1rizyp41dgi8s4fa47qrkqzr2smlkg8pgf86npd")))

(define-public crate-rs48-1.3.3 (c (n "rs48") (v "1.3.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rs48_lib") (r "^1.3.1") (d #t) (k 0)))) (h "1hj0s5nz6435gl0h055hawhmd6z5yd091ls558yv94sglmyd3nwg")))

