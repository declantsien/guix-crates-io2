(define-module (crates-io rs #{48}# rs485) #:use-module (crates-io))

(define-public crate-rs485-0.1.0 (c (n "rs485") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.24") (d #t) (k 0)))) (h "0nl3ms4pmjz5d32c80vq0jbh4y565bgw2vhgrrqzmyg9s97454bl")))

