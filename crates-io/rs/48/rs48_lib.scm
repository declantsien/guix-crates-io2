(define-module (crates-io rs #{48}# rs48_lib) #:use-module (crates-io))

(define-public crate-rs48_lib-1.1.0 (c (n "rs48_lib") (v "1.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "076ndfniwiv8cvcm1fmcy9rzgk979xndyq6v8rf355sm8z94v8rd")))

(define-public crate-rs48_lib-1.2.0 (c (n "rs48_lib") (v "1.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "199p3djkb5170gdcpcnc4iysrww5gkv7vq0mxh43ylcn5vsmfjcf")))

(define-public crate-rs48_lib-1.3.0 (c (n "rs48_lib") (v "1.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1qmwimdkjjlkvyvqgx6m9022zxcckmgvb5a1mz81ygic7b1pd1dm")))

(define-public crate-rs48_lib-1.3.1 (c (n "rs48_lib") (v "1.3.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0q13n21vkhqc6s20ilpkpq5pjvcab4j25yynkha0axg4cf12bcbv")))

