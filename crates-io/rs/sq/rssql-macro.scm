(define-module (crates-io rs sq rssql-macro) #:use-module (crates-io))

(define-public crate-rssql-macro-0.1.0 (c (n "rssql-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v03rbrgib6wph681pay04x5a5584y7wk739afzk83s59pspc36j") (f (quote (("polars")))) (y #t)))

