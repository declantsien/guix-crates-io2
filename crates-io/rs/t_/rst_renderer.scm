(define-module (crates-io rs t_ rst_renderer) #:use-module (crates-io))

(define-public crate-rst_renderer-0.3.0 (c (n "rst_renderer") (v "0.3.0") (d (list (d (n "document_tree") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0q9qy24lawvymwzlh49m3l8fprgisq59d1b4c5f5xddhqav8shnm")))

(define-public crate-rst_renderer-0.3.1 (c (n "rst_renderer") (v "0.3.1") (d (list (d (n "document_tree") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1vaps5wrzvnd3rsfy18xkm9gpfpvrl5a122nd2apd4i1z4nsnj8w")))

(define-public crate-rst_renderer-0.4.0 (c (n "rst_renderer") (v "0.4.0") (d (list (d (n "document_tree") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "102fmy0hg37akzfi97vx7zq73018nymrfxcj9azjs4fvzwsjn318")))

