(define-module (crates-io rs t_ rst_parser) #:use-module (crates-io))

(define-public crate-rst_parser-0.3.0 (c (n "rst_parser") (v "0.3.0") (d (list (d (n "document_tree") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1si69j7026yq1d01jl07na529531clbvr4pm8p26xb3g73pmxi3y")))

(define-public crate-rst_parser-0.3.1 (c (n "rst_parser") (v "0.3.1") (d (list (d (n "document_tree") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1mzfl81n93mlbk3aln7xficjkvfiml7dp3z1829f36xf8q3jzs8l")))

(define-public crate-rst_parser-0.3.2 (c (n "rst_parser") (v "0.3.2") (d (list (d (n "document_tree") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0x9s4y2sr2rx4ns1ns873n9bhlayyj4dp9w6r111xb86nwfaj9rd")))

(define-public crate-rst_parser-0.4.0 (c (n "rst_parser") (v "0.4.0") (d (list (d (n "document_tree") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "107wfa917k41l7qgcm7ydzshavah47lclf7ljrnyrq3lbm6snyyi")))

