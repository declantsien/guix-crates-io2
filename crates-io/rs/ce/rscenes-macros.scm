(define-module (crates-io rs ce rscenes-macros) #:use-module (crates-io))

(define-public crate-rscenes-macros-1.0.0 (c (n "rscenes-macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r0xv1x79y2k86gib7s4ql825w0id108drk13pa3zy3kv0qqa7nr")))

(define-public crate-rscenes-macros-1.0.1 (c (n "rscenes-macros") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q8qpvr71crwizka0cf2ibz1glnyg0ciqilqn7yqgpzyd4c9flld")))

