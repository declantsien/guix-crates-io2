(define-module (crates-io rs ce rscenes) #:use-module (crates-io))

(define-public crate-rscenes-0.1.0 (c (n "rscenes") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bm7wq6p0hbfsbzjx9lglnkv0gcnv382symcb7jz8a629vg8w9jv") (y #t)))

(define-public crate-rscenes-0.1.1 (c (n "rscenes") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0by3ghky1kmyldhq82ldg02kh40xbwhkwmmc6ipb065bxxj798sr") (y #t)))

(define-public crate-rscenes-0.1.2 (c (n "rscenes") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i2ag0z4c32yvvvm8ic3j05gcjzpil98zz20qpjv0mywmi6v328i") (y #t)))

(define-public crate-rscenes-0.1.3 (c (n "rscenes") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hgrcrfvd4f4gzn249pca9flc1zx8hpnyjrdy4x4jd95sn5j6qyq") (y #t)))

(define-public crate-rscenes-0.1.4 (c (n "rscenes") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06b5ydiv5z3brxcvaajb7xj97000qiqmy8m2x70li5mgsiv5x9v3") (y #t)))

(define-public crate-rscenes-0.1.5 (c (n "rscenes") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10ncnnaldfmlbkhy5bmqwkx2jlclmxgj4ivyaazrqbasgssz1qih") (y #t)))

(define-public crate-rscenes-0.1.6 (c (n "rscenes") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jg5nns5znjdjn69xprjd2z3p4xvammgb753bc0dkha6ai92grbi") (y #t)))

(define-public crate-rscenes-0.1.7 (c (n "rscenes") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j1y6zmwpnrrl5ybhdprykzxzcvda78qi6xmy17x2i2si9bpywkm") (y #t)))

(define-public crate-rscenes-1.0.0 (c (n "rscenes") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ybjd42cgp7pag0s5gc4id5dgi1k8d210rlpmc3dwcx4zwpx2f9y")))

(define-public crate-rscenes-1.1.0 (c (n "rscenes") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jhws0y5fgzhrl7n8w80vqlz2c7bcpaxr6m1ff8vclgnjffb8qyh")))

(define-public crate-rscenes-1.2.0 (c (n "rscenes") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fbl8vs8p644ldilmzfqfdlgsd1j3bzdcw0fvcfsg7vwmzwbfaps") (y #t)))

(define-public crate-rscenes-1.2.1 (c (n "rscenes") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mmhhh1wcr8d9d4r9aq9l5rk71gp5lyka3sw6p5qqxf3wz3z5s5z") (y #t)))

(define-public crate-rscenes-1.2.2 (c (n "rscenes") (v "1.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02c862f6sgd5x1pfin7vrmc4lqrjny4n4fnj0p3l59z02xpi9k44") (y #t)))

(define-public crate-rscenes-1.2.3 (c (n "rscenes") (v "1.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vb77wh8zyqdlzsap5xjpxnymmqvb2gf5m7lfc59d4zspczw9v06") (y #t)))

(define-public crate-rscenes-1.2.4 (c (n "rscenes") (v "1.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qr1j001ah86lysa15y1jy0va4nih7lxl868hax02vab1c3z9hiz") (y #t)))

(define-public crate-rscenes-1.2.5 (c (n "rscenes") (v "1.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bgn0ipi4z43x7vsqdqnp2idq3b5rwncafmhyq0cdir73g1yjgx0") (y #t)))

(define-public crate-rscenes-1.2.6 (c (n "rscenes") (v "1.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nazkfxy94q81smkcsy23v5sf02wb9s45ci2gkhdx6pkjqgamw5l")))

(define-public crate-rscenes-1.3.0 (c (n "rscenes") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hecs") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "105d33d2xz9rmpmc6r6r30jw5lqh5yhayp5a4bmrflkqc6zd6l5s") (f (quote (("ecs" "hecs"))))))

(define-public crate-rscenes-1.4.0 (c (n "rscenes") (v "1.4.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "hecs") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01jmmprngs35dk9rlsjqdy0zgd4qvz8kd2hpim7jmpvjyhlsim2y") (f (quote (("ecs" "hecs")))) (y #t)))

(define-public crate-rscenes-1.4.1 (c (n "rscenes") (v "1.4.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "hecs") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cs92cgnh58lv8iqiz20swnikdzr5kw5jkbzwrhd32na7al45iwm") (f (quote (("ecs" "hecs")))) (y #t) (s 2) (e (quote (("eyre" "dep:eyre") ("default" "dep:anyhow"))))))

(define-public crate-rscenes-1.4.2 (c (n "rscenes") (v "1.4.2") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "hecs") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12xvdkkkwra84ss9j8d8x3jvbxda691x69aqg78sgm4lslyj3ljl") (f (quote (("ecs" "hecs")))) (s 2) (e (quote (("eyre" "dep:eyre") ("default" "dep:anyhow"))))))

(define-public crate-rscenes-2.0.0 (c (n "rscenes") (v "2.0.0") (d (list (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes-macros") (r "^1.0") (d #t) (k 0)) (d (n "rscenes-raylib-connector") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13qlig016k8b335677j6pxzqx3hxc69fah7sbm8afv8sz7wrdd5s") (f (quote (("fake-fs")))) (s 2) (e (quote (("storage" "dep:serde" "dep:serde_json"))))))

(define-public crate-rscenes-2.0.1 (c (n "rscenes") (v "2.0.1") (d (list (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes-macros") (r "^1.0") (d #t) (k 0)) (d (n "rscenes-raylib-connector") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1cfqv51ifgrdnllcc31ggr0phcalf4ykrni0g24l3r2xp4lg4ngx") (f (quote (("fake-fs")))) (y #t) (s 2) (e (quote (("storage" "dep:serde" "dep:serde_json"))))))

(define-public crate-rscenes-2.0.2 (c (n "rscenes") (v "2.0.2") (d (list (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes-macros") (r "^1.0") (d #t) (k 0)) (d (n "rscenes-raylib-connector") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1s5nnp15j50hhn9y4z49icpa7d72faai4m9r0lp8gccvh9zi8jsa") (f (quote (("fake-fs")))) (s 2) (e (quote (("storage" "dep:serde" "dep:serde_json"))))))

