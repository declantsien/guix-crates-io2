(define-module (crates-io rs am rsam) #:use-module (crates-io))

(define-public crate-rsam-0.1.0 (c (n "rsam") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "0ml1m0yw2j8zrm4zqwxhrlx9jpwcmn2fyna1243iq70absrbb344")))

(define-public crate-rsam-1.0.0 (c (n "rsam") (v "1.0.0") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "1bika4m1whikx53xjdq73kw1k7sqm4dlsh73rf28w8vfawkm2rvs")))

