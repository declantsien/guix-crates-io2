(define-module (crates-io rs tm rstml-to-string-macro) #:use-module (crates-io))

(define-public crate-rstml-to-string-macro-0.1.0 (c (n "rstml-to-string-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rstml") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1kyknh3w8n78s205y2qrd4n1kx5an0b7h368q8hl1zw6arybqjbm")))

