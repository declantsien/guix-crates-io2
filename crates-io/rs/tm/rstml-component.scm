(define-module (crates-io rs tm rstml-component) #:use-module (crates-io))

(define-public crate-rstml-component-0.1.0 (c (n "rstml-component") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstml-component-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0afs2h06qs5pv2rrpkzbs6wh50cn06pcjn4dv5rdd4vhs0jv44vd")))

(define-public crate-rstml-component-0.1.1 (c (n "rstml-component") (v "0.1.1") (d (list (d (n "ammonia") (r "^3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstml-component-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1ahp5y0qzhmijlcz04gv6dsb3igwis6116f5nxi5jqzs428828hp") (s 2) (e (quote (("sanitize" "dep:ammonia"))))))

(define-public crate-rstml-component-0.1.2 (c (n "rstml-component") (v "0.1.2") (d (list (d (n "ammonia") (r "^3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstml-component-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0gpqav1l7ps13z464rbrsy59mmwb5l9fn7m06hy10m0zbsdr7w8i") (s 2) (e (quote (("sanitize" "dep:ammonia"))))))

(define-public crate-rstml-component-0.1.3 (c (n "rstml-component") (v "0.1.3") (d (list (d (n "ammonia") (r "^3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstml-component-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0kpd0md6j092zpnjw41ajxq2h6z213c3gjp9djmmwd522rqcj10p") (s 2) (e (quote (("sanitize" "dep:ammonia"))))))

(define-public crate-rstml-component-0.2.0 (c (n "rstml-component") (v "0.2.0") (d (list (d (n "ammonia") (r "^3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstml-component-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1xdwl4ifg9rxaba2f7wi3pynn603ikn0a7p016dabk13jxn0l7wl") (s 2) (e (quote (("sanitize" "dep:ammonia"))))))

(define-public crate-rstml-component-0.2.1 (c (n "rstml-component") (v "0.2.1") (d (list (d (n "ammonia") (r "^3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstml-component-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0v7kynj0ad90pmn7jf9vz90bsm8n1rl5vwxhi1livqqpkm82qji0") (s 2) (e (quote (("sanitize" "dep:ammonia"))))))

(define-public crate-rstml-component-0.2.2 (c (n "rstml-component") (v "0.2.2") (d (list (d (n "ammonia") (r "^3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstml-component-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0qkdp5a7c43r6m062rk3p219nhf9zzg0z99430ayhmdhsblijp5f") (s 2) (e (quote (("sanitize" "dep:ammonia"))))))

(define-public crate-rstml-component-0.2.3 (c (n "rstml-component") (v "0.2.3") (d (list (d (n "ammonia") (r "^3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstml-component-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1qcdxh17d9qpy4flfbm986gwvyn7vakv8jb12blwjy18mrl2w4n1") (s 2) (e (quote (("sanitize" "dep:ammonia"))))))

(define-public crate-rstml-component-0.2.4 (c (n "rstml-component") (v "0.2.4") (d (list (d (n "ammonia") (r "^3.3.0") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "rstml-component-macro") (r "^0.2.3") (d #t) (k 0)))) (h "0xr8x18fk9dmjsgcn3zycf1cr9n8pj1d6jx55wawiynll87nym5j") (s 2) (e (quote (("sanitize" "dep:ammonia"))))))

