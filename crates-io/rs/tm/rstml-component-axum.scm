(define-module (crates-io rs tm rstml-component-axum) #:use-module (crates-io))

(define-public crate-rstml-component-axum-0.1.0 (c (n "rstml-component-axum") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0dsir3c7j9wzvvq4qggy0v07pswlnrhbyhppghr5qn95pf9ndsc5")))

(define-public crate-rstml-component-axum-0.1.1 (c (n "rstml-component-axum") (v "0.1.1") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1nw1r38ps6rjnmyq5qmkql18qx67bi0c2igrljvjihfy608rbaqh") (f (quote (("sanitize" "rstml-component/sanitize"))))))

(define-public crate-rstml-component-axum-0.2.0 (c (n "rstml-component-axum") (v "0.2.0") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0sjq7n3zv09cb3b77iv5v2zpip8nqywq5lxnj4hrm2y4n69i22vb") (f (quote (("sanitize" "rstml-component/sanitize"))))))

(define-public crate-rstml-component-axum-0.2.1 (c (n "rstml-component-axum") (v "0.2.1") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0xbmz2cch5w8jzlkw4lspd65m1ba4j9i8cbzw1q500acgy4n3jdn") (f (quote (("sanitize" "rstml-component/sanitize"))))))

(define-public crate-rstml-component-axum-0.2.2 (c (n "rstml-component-axum") (v "0.2.2") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1nfcnfwd2zk1ghlpv4bxzby9k27bwgs74w21dp7dymrwpj3q2rw1") (f (quote (("sanitize" "rstml-component/sanitize"))))))

(define-public crate-rstml-component-axum-0.2.3 (c (n "rstml-component-axum") (v "0.2.3") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "04g0pjkc500x72n2rz9cwg8w3ng0glwxcbndrzfrd47l7brmih4b") (f (quote (("sanitize" "rstml-component/sanitize"))))))

(define-public crate-rstml-component-axum-0.2.4 (c (n "rstml-component-axum") (v "0.2.4") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0y3z7scf2xhvah1fdavs6w08jy861pf3c99qnji9ddjb17cdch91") (f (quote (("sanitize" "rstml-component/sanitize"))))))

(define-public crate-rstml-component-axum-0.2.5 (c (n "rstml-component-axum") (v "0.2.5") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "axum-extra") (r "^0.9") (f (quote ("typed-header"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0nypk95h6mx5a9chvq2qz44w1xpa1wrna44d9a0vln5a07c6rnr2") (f (quote (("sanitize" "rstml-component/sanitize"))))))

(define-public crate-rstml-component-axum-0.2.6 (c (n "rstml-component-axum") (v "0.2.6") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "axum-extra") (r "^0.9") (f (quote ("typed-header"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.2.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1sk4pz0w52bxds35g2zslwwi3jcq9gzq5wh3acs7p5hcq0fcw18h") (f (quote (("sanitize" "rstml-component/sanitize"))))))

(define-public crate-rstml-component-axum-0.2.7 (c (n "rstml-component-axum") (v "0.2.7") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "axum-extra") (r "^0.9") (f (quote ("typed-header"))) (d #t) (k 0)) (d (n "rstml-component") (r "^0.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1x5qjfczx1n2drzjhkz1qibw6pm7083phjrbdla4fx2ycwdv5fqr") (f (quote (("sanitize" "rstml-component/sanitize"))))))

