(define-module (crates-io rs rs rsrs-core) #:use-module (crates-io))

(define-public crate-rsrs-core-0.1.0 (c (n "rsrs-core") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "from-to-repr") (r "^0") (f (quote ("from_to_other"))) (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "recode") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wiml1mwfw2w3g0zkr83k678pjjqa4i3hnsiyxcaq9bqkz4kiwq7") (f (quote (("tagged") ("fragmentation") ("default" "fragmentation" "tagged"))))))

