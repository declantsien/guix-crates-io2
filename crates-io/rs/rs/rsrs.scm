(define-module (crates-io rs rs rsrs) #:use-module (crates-io))

(define-public crate-rsrs-0.1.0 (c (n "rsrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "wrap_help" "cargo"))) (d #t) (k 0)) (d (n "noodles") (r "^0.67.0") (f (quote ("sam" "bam" "cram"))) (d #t) (k 0)) (d (n "noodles-util") (r "^0.39.0") (f (quote ("alignment"))) (d #t) (k 0)) (d (n "seqcol_rs") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (f (quote ("indexmap" "preserve_order"))) (d #t) (k 0)))) (h "09vq0ibzxs5k29x46smdnlbgki8hxhhshxjlb7y99jksvfgcjawr")))

