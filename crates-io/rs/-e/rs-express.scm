(define-module (crates-io rs -e rs-express) #:use-module (crates-io))

(define-public crate-rs-express-0.1.0 (c (n "rs-express") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 2)))) (h "16ykj31l03b2s3jmcfx5zgzi597d94rwf0gzyzynd7z18a0avx8d")))

