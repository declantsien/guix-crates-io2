(define-module (crates-io rs -e rs-ecs) #:use-module (crates-io))

(define-public crate-rs-ecs-0.2.0 (c (n "rs-ecs") (v "0.2.0") (h "16rc1dgvhrmb63qz5j6sym5g077cp182l90kbil8nrzncxp2a200")))

(define-public crate-rs-ecs-0.2.1 (c (n "rs-ecs") (v "0.2.1") (h "1v6gw7bd08fmr4niv3xzbfdcyk3a5jih04692hjwg7bs9yh4f6ww")))

(define-public crate-rs-ecs-0.2.2 (c (n "rs-ecs") (v "0.2.2") (h "00zbfjbvyzlz4nlz0b6f3b88gzhqw5pz1fng5maxxjhkwdkghmfy")))

(define-public crate-rs-ecs-0.2.3 (c (n "rs-ecs") (v "0.2.3") (h "0rvj2abkrhxzabp9by61mw5dg75m4l0r5zcmdzv3n2f6zwpk7xs0")))

(define-public crate-rs-ecs-0.2.4 (c (n "rs-ecs") (v "0.2.4") (h "1skyrvs523cvjh133zpk05vhd9akzdq9rdcjv30fbalzzrhln6rj")))

(define-public crate-rs-ecs-0.2.5 (c (n "rs-ecs") (v "0.2.5") (h "18pdccwhbzgi552b24basqnghykzhaxpbh6vf7qknz735ql2nijl")))

(define-public crate-rs-ecs-0.2.6 (c (n "rs-ecs") (v "0.2.6") (h "0687p8yf8ypa2w645577lkm1mldbjm8hnmy4qnic36s6vdx0bbkl")))

(define-public crate-rs-ecs-0.2.7 (c (n "rs-ecs") (v "0.2.7") (h "1vig4xzrg3qabbczf47w23zn6rbm39p9rm791f1zvygdiplzxy1i")))

(define-public crate-rs-ecs-0.2.8 (c (n "rs-ecs") (v "0.2.8") (h "0irgxhi1r13pf5c917z3np8427d06yca2bnajcdaxigm5y6li1j9")))

(define-public crate-rs-ecs-0.2.9 (c (n "rs-ecs") (v "0.2.9") (h "00blf452a7fnjfq8vh8rwxczj0ih99hh96van9ddfsd3y4jkc3m5")))

(define-public crate-rs-ecs-0.2.10 (c (n "rs-ecs") (v "0.2.10") (h "136b2f2813hdnxgvchcisjfnasx3lcbshrb5x8dnwslabfwnxf7g")))

(define-public crate-rs-ecs-0.2.11 (c (n "rs-ecs") (v "0.2.11") (h "1ww6icba3zi0l2j1rbwpshpy1dy6zd7hmqf3wvgci4r9i386fqh8")))

(define-public crate-rs-ecs-0.3.0 (c (n "rs-ecs") (v "0.3.0") (h "12n1f2rr5sccn1pdz66qfm4ypbpz2rqajd08annwqnhpyrqmy8gd")))

(define-public crate-rs-ecs-0.3.1 (c (n "rs-ecs") (v "0.3.1") (h "1pg5gsyplrqb9x5mps1i7q3ajarx1hjsxl2a6xvw5l4k0ma9qas5")))

(define-public crate-rs-ecs-0.3.2 (c (n "rs-ecs") (v "0.3.2") (h "1rbf83ssb1kdv9vf91jp0qk2dvhvlwkzd23svmsij8yv5l5cipgn")))

(define-public crate-rs-ecs-0.3.3 (c (n "rs-ecs") (v "0.3.3") (h "18sbkp94ah81fw6slvj48xahdrsvsqbgamsfi99j0lhxzjim1g1b")))

(define-public crate-rs-ecs-0.3.4 (c (n "rs-ecs") (v "0.3.4") (h "00bbyydvsgi48w5a36cc47krc9mc3pdg3nwak8j8ps53plcj9icd")))

(define-public crate-rs-ecs-0.3.5 (c (n "rs-ecs") (v "0.3.5") (h "1v4fhgy353221np6ix0p10i919q458al4jwqjsap76cqghv5xxq6")))

(define-public crate-rs-ecs-0.3.6 (c (n "rs-ecs") (v "0.3.6") (h "1jazia6jcv7i8dlrnr3fny385fid7gzip0zlcy5jnp0k5bg96b5g")))

(define-public crate-rs-ecs-0.3.7 (c (n "rs-ecs") (v "0.3.7") (h "0cai33i0n11ymsh3zj70pk69772a0kgbjw6a97m8q1hx4s51wg1i")))

(define-public crate-rs-ecs-0.3.8 (c (n "rs-ecs") (v "0.3.8") (h "0xvwpkdpqxqngs26b7bxsmfszjwkq3xg5ggf397wd52c8zzrnn1h")))

(define-public crate-rs-ecs-0.3.9 (c (n "rs-ecs") (v "0.3.9") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "07h8qcx595nlm8552na6d67fgzwykmr0pmmsffsahs9nljw0a4f1")))

(define-public crate-rs-ecs-0.3.10 (c (n "rs-ecs") (v "0.3.10") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0qf4d6clya9khhr6rha03l4v8v9jwk2dvyvhvkyp81dg4wxqmfzc")))

(define-public crate-rs-ecs-0.4.0 (c (n "rs-ecs") (v "0.4.0") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0kfmps3mbn8h44xb7yyvk5r4hxhmg9fpryfxvplb3ygm3xb2dipv")))

(define-public crate-rs-ecs-0.4.1 (c (n "rs-ecs") (v "0.4.1") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0aaxy6xishlhq2k9y7zqhj1j96rnbprxfspmhvfpmgwlcv01jfq8")))

(define-public crate-rs-ecs-0.5.0 (c (n "rs-ecs") (v "0.5.0") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0xz6qkp1nfx6xr5k2fvjrx2ijxxxgbv1gmwmsv19c7v03iv7446p") (r "1.51")))

(define-public crate-rs-ecs-0.5.1 (c (n "rs-ecs") (v "0.5.1") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1v1hrm1wwv1wys0w8d0gllf2d6q0m3zrrlgf8l8y5l6pb19cc3qi") (r "1.51")))

(define-public crate-rs-ecs-0.5.2 (c (n "rs-ecs") (v "0.5.2") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1hzvr9jd5kxrbvywr7j85wa61z7p0wscxw8j5k39qwd5kckqf0fz") (r "1.51")))

(define-public crate-rs-ecs-0.5.3 (c (n "rs-ecs") (v "0.5.3") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1qxvbxpqa3sj2g2028cmwcwkpnqqqwjxwyy80nyylg2zf4430542") (r "1.51")))

(define-public crate-rs-ecs-0.6.0 (c (n "rs-ecs") (v "0.6.0") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0js25721wlg7ph2ghpk7rymnpp8gb3d6ph3i20x77nbc2nwyqxr9") (r "1.51")))

(define-public crate-rs-ecs-0.6.1 (c (n "rs-ecs") (v "0.6.1") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0fy4f4yr4kf7h2dds2hk593a3nsvh409d0l81gx3irnar4dxqw9l") (r "1.51")))

(define-public crate-rs-ecs-0.6.2 (c (n "rs-ecs") (v "0.6.2") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "07cl77gqfppywh6drk6hycjdld1c3fsj2nwnkf4m5ld6iah7842r") (r "1.51")))

(define-public crate-rs-ecs-0.6.3 (c (n "rs-ecs") (v "0.6.3") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "17k9blv7j69nwn84fc24x9hl5zchnfwzqsj2rd1nw2rcy4h97m85") (r "1.51")))

(define-public crate-rs-ecs-0.6.4 (c (n "rs-ecs") (v "0.6.4") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0vhy73rqfn9z20n37ccyz1syljbpc30i8qqa7nrv384spqak583a") (r "1.51")))

(define-public crate-rs-ecs-0.6.5 (c (n "rs-ecs") (v "0.6.5") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1jivy76hzjyk1hvqfim52qb1g5hhysmfmjrmsfv8jpzgg0q2pc4i") (r "1.51")))

(define-public crate-rs-ecs-0.6.6 (c (n "rs-ecs") (v "0.6.6") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "01ppmcndyqfhqh8jphknzsmpfzrqsn9vwgrv0fx8xplmp28hjmaa") (r "1.51")))

(define-public crate-rs-ecs-0.6.7 (c (n "rs-ecs") (v "0.6.7") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0iy2aps1xp8jkhn8niscyy5x2xdnyh3qndrzqqw58g6q4q8qrkxp") (r "1.51")))

(define-public crate-rs-ecs-0.6.8 (c (n "rs-ecs") (v "0.6.8") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0d1i910r9lafmjcc9kax5kw6xwcv20dvcmhl9yh30x8iysnh9cqf") (r "1.51")))

(define-public crate-rs-ecs-0.6.9 (c (n "rs-ecs") (v "0.6.9") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "01a553031nj8216qcj8vn3zwla4nb7lpll0zsawm2z0mbs3v4jmy") (r "1.51")))

(define-public crate-rs-ecs-0.6.10 (c (n "rs-ecs") (v "0.6.10") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0mmnb7nhcs0cp12dqxkjk1gn11b9z9473wbqg5wi1bmwm76qzplj") (r "1.51")))

(define-public crate-rs-ecs-0.7.0 (c (n "rs-ecs") (v "0.7.0") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "18rfvhfsw6kf7jx74xask0j22xq79p30n4qs5si0hzfiqvamgcpd") (r "1.51")))

(define-public crate-rs-ecs-0.7.1 (c (n "rs-ecs") (v "0.7.1") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1i18q4ma0nlh4v1hb2r4nkdym09z0ysj6xa2vinckp46wr0k48nk") (r "1.51")))

(define-public crate-rs-ecs-0.7.2 (c (n "rs-ecs") (v "0.7.2") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0l059lk5nv0jzz2v3di5cfcib0bnimffvnpx5lm9wxprjh46qnm7") (r "1.51")))

(define-public crate-rs-ecs-0.7.3 (c (n "rs-ecs") (v "0.7.3") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0sz3hzn7fpa3bnm5dah6m0xncw5xlrfvx2fsv73bcz076ikbpzc5") (r "1.51")))

(define-public crate-rs-ecs-0.7.4 (c (n "rs-ecs") (v "0.7.4") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "093kz48wgp3v5ga4b9wkcln1cbrkym3zkbi3i5q0fpkjbas4ywgw") (r "1.51")))

(define-public crate-rs-ecs-0.7.5 (c (n "rs-ecs") (v "0.7.5") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1b48ycgwxflbq7pyzsx188j7vz2rwcpp5ch6rdvg1zgxgi8h121x") (r "1.51")))

(define-public crate-rs-ecs-0.7.6 (c (n "rs-ecs") (v "0.7.6") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1gwspcczyh8bxf5409240yink6inv19hjg548as27rwcsrsji51v") (r "1.51")))

(define-public crate-rs-ecs-0.7.7 (c (n "rs-ecs") (v "0.7.7") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1knfh944cfb5znn0pxrldjrgh1j3jb0gdz34dzlli5lxy2qw2vgl") (r "1.51")))

