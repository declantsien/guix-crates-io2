(define-module (crates-io rs #{2d}# rs2d) #:use-module (crates-io))

(define-public crate-rs2d-0.1.0 (c (n "rs2d") (v "0.1.0") (d (list (d (n "glassbench") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reed-solomon-erasure") (r "^3.1") (f (quote ("pure-rust"))) (d #t) (k 0)) (d (n "solana-merkle-tree") (r "^1.15.2") (d #t) (k 0)))) (h "1lrlbvc4grbfl5cq9drgdjav6v0bi0ngf2a2kjhp6m701adb678k")))

