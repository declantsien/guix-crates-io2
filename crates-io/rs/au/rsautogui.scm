(define-module (crates-io rs au rsautogui) #:use-module (crates-io))

(define-public crate-rsautogui-0.1.0 (c (n "rsautogui") (v "0.1.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)))) (h "142mg7k69y8zkczr7pdcq5lnl4y3w6820xrz9z9283gz77rfdxvx")))

(define-public crate-rsautogui-0.1.1 (c (n "rsautogui") (v "0.1.1") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)))) (h "0237952b8ahl5jvbb0sw3xqchj4zdgcx1vjaswzd5b0xf32r05xg")))

(define-public crate-rsautogui-0.1.2 (c (n "rsautogui") (v "0.1.2") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)))) (h "1n4zviiba4jhipd6i3c2v6r7vb3w7s6yjdbg98hf46g25p8rb19m")))

(define-public crate-rsautogui-0.1.3 (c (n "rsautogui") (v "0.1.3") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)))) (h "1iixmfz9sw37hran6milc899dq5h4pyj20b02v08iq04za5v0vh5")))

(define-public crate-rsautogui-0.1.4 (c (n "rsautogui") (v "0.1.4") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)))) (h "1cqwv3dvc4kdijy1734vhnmydcs50zdrq853kzrwwha7sy0iz543")))

(define-public crate-rsautogui-0.1.5 (c (n "rsautogui") (v "0.1.5") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)))) (h "0w1kliwcslrh20y0p8j3brg231ssr7gc9kycj3dj1xwxasi7yvw3")))

(define-public crate-rsautogui-0.1.6 (c (n "rsautogui") (v "0.1.6") (d (list (d (n "enigo") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "screenshots") (r "^0.5.3") (d #t) (k 0)))) (h "1nbkg8j6gpj8bz0li5h0v6n8038wqii3j94rcgbj5rsadmmj0ih7")))

(define-public crate-rsautogui-0.1.7 (c (n "rsautogui") (v "0.1.7") (d (list (d (n "enigo") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "screenshots") (r "^0.5.3") (d #t) (k 0)))) (h "034qwk7jvaygqdrrslmwmsxjxqvbc0hk8hc0a0a9qr8qz018lc81")))

(define-public crate-rsautogui-0.2.0 (c (n "rsautogui") (v "0.2.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "screenshots") (r "^0.5.3") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winput") (r "^0.2.5") (d #t) (k 0)))) (h "1rgvcpwgny78cv920ygxhsr2r68gzgp3w1c3fi68q1zl66mf6yk8")))

(define-public crate-rsautogui-0.2.1 (c (n "rsautogui") (v "0.2.1") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "screenshots") (r "^0.5.3") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winput") (r "^0.2.5") (d #t) (k 0)))) (h "1lk9rs4jqvdlc1l4ppay7zlqlgrbqkvcw07k2pqvv6ijhjlpzvpy")))

(define-public crate-rsautogui-0.2.2 (c (n "rsautogui") (v "0.2.2") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "screenshots") (r "^0.5.3") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winput") (r "^0.2.5") (d #t) (k 0)))) (h "0zcqk2a6260yyc4fmjjlji1i9y99p1r9sf6paczcpijklpwh98ic")))

