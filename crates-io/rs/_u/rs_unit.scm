(define-module (crates-io rs _u rs_unit) #:use-module (crates-io))

(define-public crate-rs_unit-0.0.0 (c (n "rs_unit") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16nvpd7ygshis6f7fzfys0syksiqd12s95adj018wyj7rrb253c8") (y #t)))

(define-public crate-rs_unit-0.0.1 (c (n "rs_unit") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ijjl4qjcpav2rrswxbj5bqx9vyabv6db2jrp9kzfd2j1r2897yp")))

(define-public crate-rs_unit-0.0.2 (c (n "rs_unit") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "046q5rfxfrgizqp43qy6ja58l1lw7dan0gj7pz0mgd5wssk8xsd4")))

