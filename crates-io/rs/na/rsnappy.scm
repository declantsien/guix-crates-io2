(define-module (crates-io rs na rsnappy) #:use-module (crates-io))

(define-public crate-rsnappy-0.1.0 (c (n "rsnappy") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.67") (d #t) (k 2)) (d (n "rand") (r "^0.3.11") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0c138l7qf5g50m8pldnq5c1fv2wgiliad2l53cw0l5dp8604yqi5")))

