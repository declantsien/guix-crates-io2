(define-module (crates-io rs v- rsv-core) #:use-module (crates-io))

(define-public crate-rsv-core-0.0.2 (c (n "rsv-core") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 2)))) (h "1zmw3qd8chqk7y0cq2dcir41f3pxx5878xpwgizf61x28l3apx9v")))

(define-public crate-rsv-core-0.0.3 (c (n "rsv-core") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 2)))) (h "1kmx3wh7bvzzflqc6mpxl04lrycj04gi8h4081i4b5fgis0z9hzr")))

