(define-module (crates-io rs v- rsv-rust) #:use-module (crates-io))

(define-public crate-rsv-rust-0.0.2 (c (n "rsv-rust") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 2)))) (h "10m8l8l4p3261jix1hn9c6dcv4akpxjxkz0as2q55mdw2s7y7nda") (y #t)))

