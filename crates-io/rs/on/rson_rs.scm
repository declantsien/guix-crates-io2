(define-module (crates-io rs on rson_rs) #:use-module (crates-io))

(define-public crate-rson_rs-0.2.1 (c (n "rson_rs") (v "0.2.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0s898wgjvmkyjf4mxamjjlcpnjihml4vnakyp8i04g846nd03qkq")))

