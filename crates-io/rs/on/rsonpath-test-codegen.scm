(define-module (crates-io rs on rsonpath-test-codegen) #:use-module (crates-io))

(define-public crate-rsonpath-test-codegen-0.5.1 (c (n "rsonpath-test-codegen") (v "0.5.1") (d (list (d (n "heck") (r "^0.4.1") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1r2ic5x7h85x3nnbl30cfgvh5938xm4c2bw3ryjsgg30h2lp33hl") (r "1.70.0")))

