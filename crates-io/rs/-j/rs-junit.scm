(define-module (crates-io rs -j rs-junit) #:use-module (crates-io))

(define-public crate-rs-junit-0.7.0 (c (n "rs-junit") (v "0.7.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.2.0") (d #t) (k 0)) (d (n "test-to-vec") (r "^0.4.3") (d #t) (k 0)))) (h "03fzj3fchwy16s4qgp6v27wp7fkjhhfj58lh7l202911d5zpimch")))

