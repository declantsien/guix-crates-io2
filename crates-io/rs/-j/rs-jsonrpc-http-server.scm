(define-module (crates-io rs -j rs-jsonrpc-http-server) #:use-module (crates-io))

(define-public crate-rs-jsonrpc-http-server-8.0.0 (c (n "rs-jsonrpc-http-server") (v "8.0.0") (d (list (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "rs-jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "rs-jsonrpc-server-utils") (r "^8.0") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0g47ll854xdvbmc36c5jw7a7h6c6rvbx7qlzc75733wkqadyw7ys")))

