(define-module (crates-io rs -j rs-jsonrpc-core) #:use-module (crates-io))

(define-public crate-rs-jsonrpc-core-8.0.0 (c (n "rs-jsonrpc-core") (v "8.0.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0blyjr9gb22vnnzvspng3rlpjb9n1rzmk67kmwxymq4dq3l4y8vj")))

