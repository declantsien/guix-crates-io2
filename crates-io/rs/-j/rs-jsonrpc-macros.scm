(define-module (crates-io rs -j rs-jsonrpc-macros) #:use-module (crates-io))

(define-public crate-rs-jsonrpc-macros-8.0.0 (c (n "rs-jsonrpc-macros") (v "8.0.0") (d (list (d (n "rs-jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "rs-jsonrpc-pubsub") (r "^8.0") (d #t) (k 0)) (d (n "rs-jsonrpc-tcp-server") (r "^8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0j5v29p43arhc0b04dbpkn3z9mqq7sr3qwz2kyrvm7pckcfjpnx9")))

