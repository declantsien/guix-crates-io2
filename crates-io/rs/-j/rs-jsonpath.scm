(define-module (crates-io rs -j rs-jsonpath) #:use-module (crates-io))

(define-public crate-rs-jsonpath-0.1.0 (c (n "rs-jsonpath") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "1h7z4qh0mb5z2ygrslg10diwwaldqi1cd79zws4hcn2165kj4j9a")))

