(define-module (crates-io rs -j rs-jsonrpc-server-utils) #:use-module (crates-io))

(define-public crate-rs-jsonrpc-server-utils-8.0.0 (c (n "rs-jsonrpc-server-utils") (v "8.0.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "globset") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rs-jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0vym268xgngm5kjh1vp84p0gmcakqhp49rhc71r929h7j5an1947")))

