(define-module (crates-io rs -j rs-jsonrpc-pubsub) #:use-module (crates-io))

(define-public crate-rs-jsonrpc-pubsub-8.0.0 (c (n "rs-jsonrpc-pubsub") (v "8.0.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "rs-jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "rs-jsonrpc-tcp-server") (r "^8.0") (d #t) (k 2)))) (h "074l7cb2x942ga6q7d9hg8bbljaqfy7dqdc12dvppn7dv5imym6p")))

