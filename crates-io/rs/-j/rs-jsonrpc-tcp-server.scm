(define-module (crates-io rs -j rs-jsonrpc-tcp-server) #:use-module (crates-io))

(define-public crate-rs-jsonrpc-tcp-server-8.0.0 (c (n "rs-jsonrpc-tcp-server") (v "8.0.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "rs-jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "rs-jsonrpc-server-utils") (r "^8.0") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "1yi9hwlpvz2kxxxwxaiab81jlla8q8n2j3y4hhp6dmf6l7pawaiq")))

