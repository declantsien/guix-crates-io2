(define-module (crates-io rs ka rskai) #:use-module (crates-io))

(define-public crate-rskai-0.1.0 (c (n "rskai") (v "0.1.0") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18bcswac0zpafimxyxj321mhm9hb4siwhh8brv28vd9x1ksg4fwa")))

