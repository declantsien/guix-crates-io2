(define-module (crates-io rs -n rs-notifier) #:use-module (crates-io))

(define-public crate-rs-notifier-0.1.0 (c (n "rs-notifier") (v "0.1.0") (d (list (d (n "ureq") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0ydd8f1lhz5yhl5rd6gyf92f159jc9l65igim6xxz542sm69kiyp")))

(define-public crate-rs-notifier-0.1.1 (c (n "rs-notifier") (v "0.1.1") (d (list (d (n "ureq") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1s35vkd2qi6q3v9i9bi0ifxg87xq5d9007ykvlkm0pjg1jcyflzd")))

