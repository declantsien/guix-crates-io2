(define-module (crates-io rs -n rs-nanoid) #:use-module (crates-io))

(define-public crate-rs-nanoid-0.1.0 (c (n "rs-nanoid") (v "0.1.0") (d (list (d (n "criterion") (r "^0.1.2") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (f (quote ("getrandom" "std" "tls"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 2)))) (h "1f041amy9nr1yd9mdsfs5pnxk659r164s5sa96wsmvvhx52nxiwy") (y #t)))

