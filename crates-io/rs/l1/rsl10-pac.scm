(define-module (crates-io rs l1 rsl10-pac) #:use-module (crates-io))

(define-public crate-rsl10-pac-0.0.1 (c (n "rsl10-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1d92qcppbqmx1nb7gldvxq3lcvk0yq8347acdxfi4giwgzlyq3f1") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-rsl10-pac-0.0.2 (c (n "rsl10-pac") (v "0.0.2") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0fsyfq74g7iwdxnl9p9pcfd7axlzbd8wdc4hj5bibf658jvfbhmq") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

