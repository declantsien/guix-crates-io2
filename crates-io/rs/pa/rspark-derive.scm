(define-module (crates-io rs pa rspark-derive) #:use-module (crates-io))

(define-public crate-rspark-derive-0.1.0 (c (n "rspark-derive") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "03j679aj751mjgfdg18r5lfp3m8c0kw26lyak3ynjacp0xrqs44y") (y #t)))

