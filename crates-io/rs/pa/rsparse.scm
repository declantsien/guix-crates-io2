(define-module (crates-io rs pa rsparse) #:use-module (crates-io))

(define-public crate-rsparse-0.1.0 (c (n "rsparse") (v "0.1.0") (h "0b6jf188ws9ri4ijpn02d8vf3wh1cwpvgvgdsmflsqv807pmi29i")))

(define-public crate-rsparse-0.1.1 (c (n "rsparse") (v "0.1.1") (h "1m4jcd94mg577q91yrdbghxnip3wz88k79wnqvwrvw4wb8zkhj9r")))

(define-public crate-rsparse-0.1.2 (c (n "rsparse") (v "0.1.2") (h "0j8v72hlcyv9xjx2jgfxi2wdbax1jpgvddr81fy8rph3icddnwh7")))

(define-public crate-rsparse-0.1.3 (c (n "rsparse") (v "0.1.3") (h "0b5rx6rp3c2rsbr86kzkxwia2bc8dcjj5kpss9x23k0pr3zq407r")))

(define-public crate-rsparse-0.1.4 (c (n "rsparse") (v "0.1.4") (h "1hbqnivgwr5i2qcwip3iwxm6clxrigwbf88252k7zrhlnqx5k761")))

(define-public crate-rsparse-0.1.5 (c (n "rsparse") (v "0.1.5") (h "0gcgp2zzhcp605wqz9jk05mdc5mblacpcbg48sccn0fy03f0v3ic")))

(define-public crate-rsparse-0.1.6 (c (n "rsparse") (v "0.1.6") (h "0vvs89fylk4c1k7yzq7a23wkirpdzl5rvxzncr3r2jspdvg237k5")))

(define-public crate-rsparse-0.1.7 (c (n "rsparse") (v "0.1.7") (h "1ywnzh3843j35x67pvk8ghnsa3hb2jhayx51na8jy088fc4bwfr1")))

(define-public crate-rsparse-0.1.8 (c (n "rsparse") (v "0.1.8") (h "1rbkr882l0mdb49lzcdqc9i6ijc1lwnzj4qzpbj7f00x4d4lqnz4")))

(define-public crate-rsparse-0.1.9 (c (n "rsparse") (v "0.1.9") (h "197cq0zp19d3wqbqfpv3xgr229pbs4a2fmcak0mcdyqcp4l1202f")))

(define-public crate-rsparse-0.2.0 (c (n "rsparse") (v "0.2.0") (h "1w3s6hw4m1xfw0449rsprylyzai1qqrrq3rsfkwm9xlaq9qd65ln")))

(define-public crate-rsparse-0.2.1 (c (n "rsparse") (v "0.2.1") (h "0mqxx08rfzxnbg7pxhbfg43dx0hfvsj80gsq2rbryd8fmnqffrx7")))

(define-public crate-rsparse-0.2.2 (c (n "rsparse") (v "0.2.2") (h "1w9ibakpywx210xnzgzlcfcizfj37g93mjmf75ardglx16ahrdng")))

(define-public crate-rsparse-0.2.3 (c (n "rsparse") (v "0.2.3") (h "0idprhcibbs04hf10iqxp8sz6sqmbams95g5yr850d3abbw2mk5k")))

(define-public crate-rsparse-1.0.0 (c (n "rsparse") (v "1.0.0") (h "1m0r17l591jwys8d4i9shx4iv49i9b0lcwd3zb4dpq68gz88a3ll")))

