(define-module (crates-io rs pa rspark) #:use-module (crates-io))

(define-public crate-rspark-0.1.0 (c (n "rspark") (v "0.1.0") (h "179amg7h78rjpp7bi4hkj3kdy55d8alfww4rrbiasg9f4sa2g1ws") (y #t)))

(define-public crate-rspark-0.2.0 (c (n "rspark") (v "0.2.0") (h "1w5a2apsa5l79dalkp0x1hkj5hjnxq1shlj1yd7ym6qqswa7kq8h")))

