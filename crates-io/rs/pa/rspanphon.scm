(define-module (crates-io rs pa rspanphon) #:use-module (crates-io))

(define-public crate-rspanphon-0.1.0 (c (n "rspanphon") (v "0.1.0") (d (list (d (n "cached") (r "^0.26.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0fdl8r9ma0ss1vwvyypa4cppcjqp956sv488w1dd23bsfjw2xyif") (r "1.55.0")))

(define-public crate-rspanphon-0.1.1 (c (n "rspanphon") (v "0.1.1") (d (list (d (n "cached") (r "^0.26.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1316svj8x07ajbxpspj23qmsc6pvjn4r5ls2r3fazlbbk6dzi6gj") (r "1.53.0")))

(define-public crate-rspanphon-0.1.2 (c (n "rspanphon") (v "0.1.2") (d (list (d (n "cached") (r "^0.30.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1adn1rq28p914chgilhyjmq399lsd7a4hz1qhx5pys0rqw3l84cv") (r "1.53.0")))

(define-public crate-rspanphon-0.1.3 (c (n "rspanphon") (v "0.1.3") (d (list (d (n "cached") (r "^0.34.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0kad4k3dxmr3dgffrznsz371aj3djxyhbjgz8c7sf36r352gpmnk") (r "1.53.0")))

(define-public crate-rspanphon-0.1.4 (c (n "rspanphon") (v "0.1.4") (d (list (d (n "cached") (r "^0.34.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0inv37xqlbp8p2if1lxhxdd6fv06ka3n1qwzkpjvz0x1qcvgph31") (r "1.53.0")))

