(define-module (crates-io rs pa rspack-codespan-reporting) #:use-module (crates-io))

(define-public crate-rspack-codespan-reporting-0.11.2 (c (n "rspack-codespan-reporting") (v "0.11.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "insta") (r "^1.6.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "peg") (r "^0.7") (d #t) (k 2)) (d (n "rustyline") (r "^6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1y7991ikq6gg1xp386b8lqkrz1q59ka7iqmxan5rll4gwnhb6lzw") (f (quote (("serialization" "serde" "serde/rc") ("ascii-only"))))))

