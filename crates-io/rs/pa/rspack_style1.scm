(define-module (crates-io rs pa rspack_style1) #:use-module (crates-io))

(define-public crate-rspack_style1-0.1.2 (c (n "rspack_style1") (v "0.1.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 2)) (d (n "fasteval") (r "^0.2.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "iocutil") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0d0ikjdcf7r42npwqg31191xqq1wjizsb5fhd8fnlpfjj342zkn9")))

