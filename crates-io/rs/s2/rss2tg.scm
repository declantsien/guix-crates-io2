(define-module (crates-io rs s2 rss2tg) #:use-module (crates-io))

(define-public crate-rss2tg-0.1.1 (c (n "rss2tg") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "rss") (r "^1.1") (f (quote ("from_url"))) (d #t) (k 0)))) (h "19n6xrkmvnsxgicx9n248wa9ipapi5zfablifxg5776a5wavlyrf")))

(define-public crate-rss2tg-0.2.0 (c (n "rss2tg") (v "0.2.0") (d (list (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "rss") (r "^1.1") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "16gb9paixmi6hpqz4sz1drabsdfwkaq140m9pj8vd9x49cby3hp2")))

(define-public crate-rss2tg-0.2.1 (c (n "rss2tg") (v "0.2.1") (d (list (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "rss") (r "^1.1") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1vf8zglffl0ghzq6v8c8zjmql1zhdfkn1mc5a0w7i2i5jm8f97d3")))

(define-public crate-rss2tg-0.2.2 (c (n "rss2tg") (v "0.2.2") (d (list (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "rss") (r "^1.1") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "09d5zm14r14dnmgax672dy5a2nl9lw2iiznx2p28431cm3ypih5f")))

(define-public crate-rss2tg-0.2.3 (c (n "rss2tg") (v "0.2.3") (d (list (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "rss") (r "^1.1") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "19hx3ji38bism4p0sgn1b6swl1ywfn9qzs9xw3znnjga535rqcgr")))

