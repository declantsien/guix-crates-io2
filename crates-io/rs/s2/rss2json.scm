(define-module (crates-io rs s2 rss2json) #:use-module (crates-io))

(define-public crate-rss2json-1.0.0 (c (n "rss2json") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "rss") (r "^2.0") (f (quote ("serde" "validation"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18af0h3x37whdc4mvb9vya8bgaq6vayrdrfvx2br24m8nk121yxa")))

(define-public crate-rss2json-1.0.1 (c (n "rss2json") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "atom_syndication") (r "^0.12") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "rss") (r "^2.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1312m4wrp1nfiayzcw7ph07qqmafizg9gxi35v3k9ggvqnk2lv11")))

