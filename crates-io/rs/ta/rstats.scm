(define-module (crates-io rs ta rstats) #:use-module (crates-io))

(define-public crate-rstats-0.1.0 (c (n "rstats") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0mvfja6dxvbiwwlqlwgmi1jpp0swjzwyrqdcnf6qwjmjpk73dlb0")))

(define-public crate-rstats-0.1.1 (c (n "rstats") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0xl2gbbxckx27xfxz19yzwxm0ffiryyrjiv55wvp94q9b7y32i7i")))

(define-public crate-rstats-0.1.2 (c (n "rstats") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0zx76gj6fym5qnyfnbdzaa6l72qb1qpdg39fdlx551n34wp1ispp")))

(define-public crate-rstats-0.1.3 (c (n "rstats") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1vvhy46637bqbp71rl2i0qlxcxpjqxnldlgdhsf87cdjjb9406rs")))

(define-public crate-rstats-0.1.4 (c (n "rstats") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1bqj49s9lm8z9c4psrhf315mzjayjn40flfp1jm4gjhsk9gdfrav")))

(define-public crate-rstats-0.1.5 (c (n "rstats") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0nckhdg81bpxcd6353kjkhdgg2gcqzsj33zg6w3nyznmbbjx57dr")))

(define-public crate-rstats-0.2.0 (c (n "rstats") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0a5wk1c83pmgmjhswhnlhancqins0siyjychwfi7xvn7n0rgb9l4")))

(define-public crate-rstats-0.2.1 (c (n "rstats") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "05fz3cp7y9lxb83j4vdgairxs6rw51v6cn2w9kx2jac9kfr9w9v5")))

(define-public crate-rstats-0.2.2 (c (n "rstats") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0ggxs4ffk39m4414iky3d3vbxqfhlhvkmk0c63myh7z7mhjgml5c")))

(define-public crate-rstats-0.2.3 (c (n "rstats") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1cbdf46kyzpx1c7bq3wiakdq5lwn8594sqcg1kiq66g52rdmniry")))

(define-public crate-rstats-0.2.4 (c (n "rstats") (v "0.2.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1ihv1gxxidxx936majnmx5mn2xylwqvjaakszdfl7iadjp8pyn02")))

(define-public crate-rstats-0.2.5 (c (n "rstats") (v "0.2.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1hrh4f29az6zdjr32xcr6yvfc10v5by3wmjprxas0lq4zfkj124w")))

(define-public crate-rstats-0.3.0 (c (n "rstats") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "06r6mqkgiz01a49pkdx5rykv5va6613m2zkgvxn6ff3v60a3dp1h")))

(define-public crate-rstats-0.3.1 (c (n "rstats") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "131is4vd02mqdqz3jmpp3hlbz8s7hc9rgma3zgrfb72xq5ispkv8")))

(define-public crate-rstats-0.3.2 (c (n "rstats") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0jliz4rkq0nmpn1y8jiw4qvni2jw8dwf6dlqb13i5amvcyp03gwn")))

(define-public crate-rstats-0.3.3 (c (n "rstats") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "131z60yr1f7p28l73rqx93x09a66yv6ss7zar68c1nrp7s2nnybc")))

(define-public crate-rstats-0.3.4 (c (n "rstats") (v "0.3.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1xyparf2kqnfjd8rzjyj3is24p1lnbfp9c6pbinfxn0pi4y10aij")))

(define-public crate-rstats-0.3.5 (c (n "rstats") (v "0.3.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0qxzyh0jyfpz6ga1xs4z8wwrh5b9id6dwy7kldi0w8lcgc73ymzi")))

(define-public crate-rstats-0.3.6 (c (n "rstats") (v "0.3.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11iwp66wbhl6qva9vsfjy0xzf97853qpxqvrw79bz89a41jk01n8")))

(define-public crate-rstats-0.4.0 (c (n "rstats") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0k53j4q77lj8p1iccd9j9sk01zqqk9dnva8s53msh0ps6app6r5s")))

(define-public crate-rstats-0.4.1 (c (n "rstats") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "09xsmn1i15rb3m0ydklrshd94z5r336d7yw1v6dpmaibi8kvqmiw")))

(define-public crate-rstats-0.4.2 (c (n "rstats") (v "0.4.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1mn5jr3sh6jmmicnxyyf31g0jprlw442zzfl9vwxgk2z4v6zazgm")))

(define-public crate-rstats-0.4.3 (c (n "rstats") (v "0.4.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1gmzaqbqajafnm4xr4xpdrd07k6q20gmzvcf19bkn9j2lijf9qfz")))

(define-public crate-rstats-0.4.4 (c (n "rstats") (v "0.4.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0mk0fnpadffqzf0hgsfwq42zp6qmxvnqdvrnzqkb13hjhiy637gk")))

(define-public crate-rstats-0.4.5 (c (n "rstats") (v "0.4.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "039darwbjd4bqndijqaq44qxlhav079rdclk9k0pr1a6pll08sdv")))

(define-public crate-rstats-0.4.6 (c (n "rstats") (v "0.4.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0ql6wjq2jd7lrkkls83w4vbdgdm11ajzjj0z85ww5zxap3myjb03")))

(define-public crate-rstats-0.4.7 (c (n "rstats") (v "0.4.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "04df5ixfkvmb83bnbcf3ly99f130az9xfy1i306z15shs1bn4bkx")))

(define-public crate-rstats-0.4.8 (c (n "rstats") (v "0.4.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "14qj2qfj3i7lwdzx51fd1l58m94j2byhqqpbsb2y7x88wfdlld5x")))

(define-public crate-rstats-0.4.9 (c (n "rstats") (v "0.4.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "15mq9f99mj1y99rzpk53pq755la0a420imln2g3za9sn0jp42kad")))

(define-public crate-rstats-0.4.10 (c (n "rstats") (v "0.4.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "03nd6rdgc4fl07143xjq40lf8075bfzj1mnknsb55sx1j8ga1387")))

(define-public crate-rstats-0.4.11 (c (n "rstats") (v "0.4.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1g5vh7r6m8xfb1vm5pafbab621hvyxy5ymwx2p1i7fj0g5rvxriq")))

(define-public crate-rstats-0.4.12 (c (n "rstats") (v "0.4.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "09l32pdhmhl4ckn03jpwmi4rlgsgyj9sh4rzj6jq893lpw6vc8g6")))

(define-public crate-rstats-0.4.13 (c (n "rstats") (v "0.4.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1cr9qg7jy28rzinvnkxqm0lfrk33hb4cvn3wsmfcj1d24a8m66y4")))

(define-public crate-rstats-0.4.14 (c (n "rstats") (v "0.4.14") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "03373gn3m7sn137sbrllgvny4qlhmp8vlzn1zi8wxxgibxhngvbw")))

(define-public crate-rstats-0.4.15 (c (n "rstats") (v "0.4.15") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "17wmwgp59f6agzk252r79czq8xkkpy15x0kvchhhi1ki5x960blf")))

(define-public crate-rstats-0.5.0 (c (n "rstats") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "188yrmsrckyry4abaxbgpl4im07q46p8xvjwj6v9a2173ihjvc41")))

(define-public crate-rstats-0.5.1 (c (n "rstats") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0yrmhmgs9vxaxg3qmpimm7m1c5v7y3vm5xx14dxqyrv0i96rmlw0")))

(define-public crate-rstats-0.5.2 (c (n "rstats") (v "0.5.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "135sha1ki1x6za8647hrm50cmswhqkvalxc2ji1c8626in5rnxx4")))

(define-public crate-rstats-0.5.3 (c (n "rstats") (v "0.5.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "05ydx0ggr7iqd731aahdlnshbc376smrz72wciqkjfm2qlylxwrs")))

(define-public crate-rstats-0.5.4 (c (n "rstats") (v "0.5.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0i4dw002qq9kl8z9qnqq527kxnz5kbndxijsd725q47cd054s8vj")))

(define-public crate-rstats-0.5.5 (c (n "rstats") (v "0.5.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "145lflk8v6pjvhpkapzp4brf2zqllp2x22y0w1v3ndzai5i72ssh")))

(define-public crate-rstats-0.5.6 (c (n "rstats") (v "0.5.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0wnqb8i40lcianv6l9jq8byv75x77cps7vbl6bvmbhgq20cg6svy")))

(define-public crate-rstats-0.5.7 (c (n "rstats") (v "0.5.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0sfyjjdh29xxlz8ndkqfk94sz9rm744irgy0qqdzc2yhi9nlf6hq")))

(define-public crate-rstats-0.5.8 (c (n "rstats") (v "0.5.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "190252iv3i4i1w5zy5b0z8k2xmxh3g693ckg3mkg6l20d4i3c3vb")))

(define-public crate-rstats-0.5.9 (c (n "rstats") (v "0.5.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "12wqb60r3800zkwwk1a6zhbpxhk29x81ypryjd0k4xyik17w511v")))

(define-public crate-rstats-0.5.10 (c (n "rstats") (v "0.5.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0kqdyyf9rwnyxqiqkqbf5jklamm361w10wqd31av4h7zqqnkgahi")))

(define-public crate-rstats-0.6.0 (c (n "rstats") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0m2g0kqgs1hx90sj3w6rcsar30zxkvxv8kf1lc65rbn8fnwsshlv")))

(define-public crate-rstats-0.6.1 (c (n "rstats") (v "0.6.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1i925qv7lq4544gxh7771frjg4hpawhz9sjikmpnwfmlvdhq08na")))

(define-public crate-rstats-0.6.2 (c (n "rstats") (v "0.6.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1p4vbsknkrms6i4zdihfi8il44aifgzagwl1z2slgg6daxdgjl83")))

(define-public crate-rstats-0.6.3 (c (n "rstats") (v "0.6.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1n4hb6g6fmhl1nxjvx2ziwpps4bpqrnrbwq69jzk1cixs3r8iycx")))

(define-public crate-rstats-0.6.4 (c (n "rstats") (v "0.6.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1qx040rfqncjprcasyzq8jhlqb21zdzjg0r79frb9058l7k8wwra")))

(define-public crate-rstats-0.6.5 (c (n "rstats") (v "0.6.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1i8d224dmslv632a5f58fyb84c76qbhxn13yv0mv4zv9j1rg70y1")))

(define-public crate-rstats-0.6.6 (c (n "rstats") (v "0.6.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0kf534mcwfkqrxq1l5w0nkjnmsx2w40vh3bkqgdm0s2za1l1mr8j")))

(define-public crate-rstats-0.6.7 (c (n "rstats") (v "0.6.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1mil8qgvgld25s54vgmcg8m20qrys2mr44sibslcy5d21ydmi7vi")))

(define-public crate-rstats-0.6.8 (c (n "rstats") (v "0.6.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "14dmrzd10jimlchvw4bp9m3qxfk2ss0dlkrppg31p08yazhiq9cq")))

(define-public crate-rstats-0.6.9 (c (n "rstats") (v "0.6.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0nsn2cyczb2p3njfl5ff47lx6jz7m1716yam5qi3j77k8kfmnzii")))

(define-public crate-rstats-0.6.10 (c (n "rstats") (v "0.6.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0vqc20niaf40r1c4y7q78bvfx3kb2pjd8w00nw5l3iqvi98bgrjn")))

(define-public crate-rstats-0.7.0 (c (n "rstats") (v "0.7.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1824wv8r0j152ihh96pgqncyc091zahbz9fc31jklxwwlw2x46sv")))

(define-public crate-rstats-0.7.1 (c (n "rstats") (v "0.7.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1a313nk1rxxsl9pc56ccrgsjax17db2h7vq0ppqbwcby2ibkz9rs")))

(define-public crate-rstats-0.7.2 (c (n "rstats") (v "0.7.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "12w3v9mrj83ml3fg3fh4hik8p6lqljy2iw3mf7gaqpqhqhkajn9s")))

(define-public crate-rstats-0.7.3 (c (n "rstats") (v "0.7.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1wyvcb1w2m075q0kz6il87sj3qs513qib7ynv8mqxb58z2j30srw")))

(define-public crate-rstats-0.7.4 (c (n "rstats") (v "0.7.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0dgc0h66827xkz6vbqcicjfarbvygwi7dw491fgnlccm3yy9zwg6")))

(define-public crate-rstats-0.7.5 (c (n "rstats") (v "0.7.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1vnls1b51m4lnmz6kgrjcdcmcqw2kv2p2zcpcva6gy80sc7i0d63")))

(define-public crate-rstats-0.7.6 (c (n "rstats") (v "0.7.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "095cm6nc5kpk1mr7qbfzpikqq0y0zb816ixxjsmxmdnrwfzr5205")))

(define-public crate-rstats-0.7.7 (c (n "rstats") (v "0.7.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "07066a0c86kaa80w6psm981gr9izhh95l9d4dy5kmxvj4palgbl0")))

(define-public crate-rstats-0.7.8 (c (n "rstats") (v "0.7.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "1kgdv1x8gnwa7nqy8xkjkwk7avqiimg1cp3qywh59vpv96dahrmz")))

(define-public crate-rstats-0.7.9 (c (n "rstats") (v "0.7.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0h1bjn67l20m4c15aqdlzkxvds74x38ix9lf3h3zkv0z5qhfl0mk")))

(define-public crate-rstats-0.7.10 (c (n "rstats") (v "0.7.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "069h60fbagccl6l3mz8jngfmr7bkydzybax7x8sjkgqmzdl9nxg9")))

(define-public crate-rstats-0.7.11 (c (n "rstats") (v "0.7.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)))) (h "0sx8jxj2cmkdkjnv44wbinkqnysiyr3snlg6jbmdaj702zm9ymdr")))

(define-public crate-rstats-0.7.12 (c (n "rstats") (v "0.7.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.1") (d #t) (k 0)))) (h "04d5w2y6jv1jg9ah4rl59gfj9yjhdckrqjnnkwkdsfv2rhy15wcj")))

(define-public crate-rstats-0.7.13 (c (n "rstats") (v "0.7.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.1") (d #t) (k 0)))) (h "1iryiqcijh3vpq91z85949vs2i9xhhmy0q4ngvzaimf43nhhb6qx")))

(define-public crate-rstats-0.7.14 (c (n "rstats") (v "0.7.14") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.1") (d #t) (k 0)))) (h "07iwghcrwi4slyxc07ardxj2f6dg3csvqvrm5yfi0fb74xq6b462")))

(define-public crate-rstats-0.7.15 (c (n "rstats") (v "0.7.15") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.1") (d #t) (k 0)))) (h "0vds7lzziakf725dad7c3hvhzwqnkhirxpjdr2ma3gvbiy7awkrx")))

(define-public crate-rstats-0.7.16 (c (n "rstats") (v "0.7.16") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.1") (d #t) (k 0)))) (h "0y3202hz535hkv8zg3fsgwa4389kghnjx8hvbr7ah7sgzqznzcws")))

(define-public crate-rstats-0.7.17 (c (n "rstats") (v "0.7.17") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "0xhpxhcim15i06h1z2ldkbjxfmdxr68ihq99gj42n1csczzg6829")))

(define-public crate-rstats-0.8.0 (c (n "rstats") (v "0.8.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1jb2bda1ky413z1wxdy9fs47392i4zvwp3h781ll1rvpizg9chi5")))

(define-public crate-rstats-0.8.1 (c (n "rstats") (v "0.8.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1h7lxg5cf12mqlcs13fn014rzp8g7qkjijdarvlbxakbx0y3ikdy") (y #t)))

(define-public crate-rstats-0.8.2 (c (n "rstats") (v "0.8.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "18668ghgw6j1lbfccx0asm64b97zlc0l4x66kvyg343ghdfdyxkc") (y #t)))

(define-public crate-rstats-0.8.3 (c (n "rstats") (v "0.8.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1rd88gba7cvap2vynyskwhabi58lxxqdaqr7xrws38l4w34i3zy5")))

(define-public crate-rstats-0.8.4 (c (n "rstats") (v "0.8.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "00kf2x7abfd89zwwspa2c8393dvafw8qa46c3g4zg66vcba2xgrs")))

(define-public crate-rstats-0.8.5 (c (n "rstats") (v "0.8.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1h02k292mw8kcd6r1yfg1lj7mbm39pkpvh4kjyiad8lfpw9lqs45")))

(define-public crate-rstats-0.8.6 (c (n "rstats") (v "0.8.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "0vhyy8ys0px310y3l4s977qgw1amb4mnm95b6wb0whgj2y590a1n")))

(define-public crate-rstats-0.8.7 (c (n "rstats") (v "0.8.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1ilxc99vm39zzvvwg9y1s0sfvx1ssmvbvd06zh23q4qib7rgadvg")))

(define-public crate-rstats-0.8.8 (c (n "rstats") (v "0.8.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1yq579m8nads8k18m8bcnpjgcyclb50yv26q25wly3qml7xjfc0r")))

(define-public crate-rstats-0.8.9 (c (n "rstats") (v "0.8.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1npsyrgjnav53pcfa8ngdxmyycq1kil9gm91456ykd78r6l9kidq")))

(define-public crate-rstats-0.9.0 (c (n "rstats") (v "0.9.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1andbgl379k7719h1sydx1759n2pqjg9ijk9vnl4r54aamagvfn6")))

(define-public crate-rstats-0.9.1 (c (n "rstats") (v "0.9.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "148qk3adq4naa9bv45nphrc4rblqdwzib8vs92s4mr714vjx1ny0")))

(define-public crate-rstats-0.9.2 (c (n "rstats") (v "0.9.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "105inlcb96wm5qy8ahpk4yswwcbmb4ham2nyay62af2irn8mmpy6")))

(define-public crate-rstats-0.9.3 (c (n "rstats") (v "0.9.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "0in5h5d07ggn9idr4hqa3fcdlv56r0193c9dzkaifywwyc4pc0ql")))

(define-public crate-rstats-0.9.4 (c (n "rstats") (v "0.9.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "15ax5jkxlg51d9im6q6g3d8w7kcksd65g3nw93qg4q0rq40b8b6i")))

(define-public crate-rstats-0.9.5 (c (n "rstats") (v "0.9.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "132vs8vc7m30j4df2iprdcshd99nsfqabqv5fyl4m1k8213y6ldd")))

(define-public crate-rstats-1.0.0 (c (n "rstats") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "1fi0w4agniys0azia7rcl52di45fbcg3xdkbdhlji3bw86rarsrh")))

(define-public crate-rstats-1.0.1 (c (n "rstats") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^0.2") (d #t) (k 0)))) (h "0c44nav0mk6qhddkvl9ms7srrkpma4yhj83c5y3j2vi2b0904mbm")))

(define-public crate-rstats-1.0.2 (c (n "rstats") (v "1.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "1nk2p7mhqq5p2h4glf8k0923jnpz13ghy2mvkm4lwl4swa5f6yck")))

(define-public crate-rstats-1.0.3 (c (n "rstats") (v "1.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "0499vrjkx77s16674cn17avg7iv4s5x8hvmhqqrpf9qdi938iv73")))

(define-public crate-rstats-1.0.4 (c (n "rstats") (v "1.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "13wd0qlp7crf0fsnfvyslrcdfjic6yn3x9wbhymaq7daragnni1b")))

(define-public crate-rstats-1.0.5 (c (n "rstats") (v "1.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "10x54cs2zbr5i8pnfnmza73hqibx9i9balz49234y7ig45khkbkb")))

(define-public crate-rstats-1.0.6 (c (n "rstats") (v "1.0.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "1nrqyrqzaa8i381fsrx0z0bl1drdcza3mfwj2ad09vjiy4pgd3rj")))

(define-public crate-rstats-1.0.7 (c (n "rstats") (v "1.0.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "1d5gqrha6mvzc0mkl37s9340wi0kl6blsvwmk9bx8d66lbqwv9q6")))

(define-public crate-rstats-1.0.8 (c (n "rstats") (v "1.0.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "1hvylns9qx85yijzibqm2szmwc7655dqkxbkyzn3a7z9pmilv2k7")))

(define-public crate-rstats-1.0.9 (c (n "rstats") (v "1.0.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "random-number") (r "^0.1") (d #t) (k 2)))) (h "0v4kanxd2q8n71xrzp47h4pdh6d5i4iyy1h4g56sww5wwykj1306")))

(define-public crate-rstats-1.0.10 (c (n "rstats") (v "1.0.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "0xhzcqn87ndr0pbhdg2x70ns1hipfgl70xn8sryamcfj79711yyc")))

(define-public crate-rstats-1.0.11 (c (n "rstats") (v "1.0.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)))) (h "1m1bl2mcz4ahi02iy33l6y9p5kqjqhc1ijql6fpig75r2n9hys8g")))

(define-public crate-rstats-1.0.12 (c (n "rstats") (v "1.0.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.1") (d #t) (k 2)))) (h "1fvhr9xfza5pvf5rgk60ic05jc57zppgpn8jlwppsdandd69immd")))

(define-public crate-rstats-1.0.13 (c (n "rstats") (v "1.0.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "1ygd1g4q9ylmww7wz15s2ssp6abka6y2k590l0cy4kka8hbaglwi")))

(define-public crate-rstats-1.0.14 (c (n "rstats") (v "1.0.14") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "1kwpnhg3fpbwl9zs3r3cs3bkfs61wn6q2q7wgmizycqdxmqpzh4n")))

(define-public crate-rstats-1.0.15 (c (n "rstats") (v "1.0.15") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "0qby393m6qjva2cgbgn042i666c20z0fd4w0bgz8h593fn6xzbyv")))

(define-public crate-rstats-1.0.16 (c (n "rstats") (v "1.0.16") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "1fzwp0nnwwyzmp9lyim7rzy91xw692fb1rjv0n813g4yj9c575f0")))

(define-public crate-rstats-1.0.17 (c (n "rstats") (v "1.0.17") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "1mm7dph7a32565rlak155l6hydi4ws1g7nm5q09ry56h6f670ym6")))

(define-public crate-rstats-1.0.18 (c (n "rstats") (v "1.0.18") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "0yxi9mfczp72sx3d9a1xbs7y3sjg93ly9lwdgff936hlqbynz0mb")))

(define-public crate-rstats-1.0.19 (c (n "rstats") (v "1.0.19") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "0jakh1rw7qhnd4paj3djb0m822780nr1jlnm7ybml0vfnx6lpyi9")))

(define-public crate-rstats-1.0.20 (c (n "rstats") (v "1.0.20") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "0c43d1zpskzvyfgbk5rvh7gcjaj6i80lynrxkcwjw9w79z808npc")))

(define-public crate-rstats-1.0.21 (c (n "rstats") (v "1.0.21") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "0bzzd145h46drar1hprrkx97lb6gfm0yi4q8hvpd0bv5wn5i47y5")))

(define-public crate-rstats-1.1.0 (c (n "rstats") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^0") (d #t) (k 0)) (d (n "ran") (r "^0.3") (d #t) (k 2)))) (h "1pplnkpp18h625v524j75vyav7f33y50p9x52pmi0zhvrpbh4psq")))

(define-public crate-rstats-1.1.1 (c (n "rstats") (v "1.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^0") (d #t) (k 0)) (d (n "ran") (r "^0") (d #t) (k 2)))) (h "0042cida4l87n2v9w6zq50xx0bvw5dbygigywn1cff5bj30mybck")))

(define-public crate-rstats-1.1.2 (c (n "rstats") (v "1.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^0") (d #t) (k 0)) (d (n "ran") (r "^0") (d #t) (k 2)))) (h "177kqx6hr2n7yhnibyhn03bysqxzdcqr79dw43zf6kpxfi0fqj98")))

(define-public crate-rstats-1.1.3 (c (n "rstats") (v "1.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^0") (d #t) (k 0)) (d (n "ran") (r "^0") (d #t) (k 2)))) (h "0d18b8j2dskj66g18g38k7swwwgg3qqrfw2p5103f2frksl9wiva")))

(define-public crate-rstats-1.1.4 (c (n "rstats") (v "1.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^0") (d #t) (k 0)) (d (n "ran") (r "^0") (d #t) (k 2)))) (h "00xl0lmvjfg1w72dadmy12cycb161lwzxnhqcb65x9qbzr99d8cf")))

(define-public crate-rstats-1.1.5 (c (n "rstats") (v "1.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^0") (d #t) (k 0)) (d (n "ran") (r "^0") (d #t) (k 2)))) (h "0h7llqa9wc038wyz4kmdix3qjrsqariq0ipdk7sn00whf5y1yliq")))

(define-public crate-rstats-1.1.6 (c (n "rstats") (v "1.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^0") (d #t) (k 0)) (d (n "ran") (r "^0") (d #t) (k 2)))) (h "19pwz8wl1cwccn1injyhgkp1c0mp030ydagdbpbvafk8r9w187kg")))

(define-public crate-rstats-1.1.7 (c (n "rstats") (v "1.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^0") (d #t) (k 0)) (d (n "ran") (r "^0") (d #t) (k 2)))) (h "037i7xz98lvdp5qjdr7vq5kq3527acswn3n1rd1cjvv626svizhv")))

(define-public crate-rstats-1.1.8 (c (n "rstats") (v "1.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "devtimer") (r "^4") (d #t) (k 2)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^0") (d #t) (k 2)))) (h "09l4hmiqj5jdbg27apkrd08qgv2yfcwayy2sn9ialxiabn0gazn0")))

(define-public crate-rstats-1.1.9 (c (n "rstats") (v "1.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^0") (d #t) (k 2)))) (h "1a8x9nwb8dkyhcra08180s783q0fgk1hpdx7irpqjvanywk0hp6k")))

(define-public crate-rstats-1.2.0 (c (n "rstats") (v "1.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^0") (d #t) (k 2)))) (h "1zvix4p0hz2lh1pjwa7l9fkxkxb76mbqk1vkzqwhvr1cbaqzf278")))

(define-public crate-rstats-1.2.1 (c (n "rstats") (v "1.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^0") (d #t) (k 2)))) (h "1fd24vi8llazd9fighgxspnwfmmg3yb629s03jpphxlr3r2fmy1z")))

(define-public crate-rstats-1.2.2 (c (n "rstats") (v "1.2.2") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^0") (d #t) (k 2)))) (h "1nflrrcmbndhh1dkslvfich4b707j4naw324bkwi27m3h0fh1l8r")))

(define-public crate-rstats-1.2.3 (c (n "rstats") (v "1.2.3") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^0") (d #t) (k 2)))) (h "070a1w2pxnsdw9h08izxs1avvl1lsx1nlin9nipm3qmi4i95m2s5")))

(define-public crate-rstats-1.2.4 (c (n "rstats") (v "1.2.4") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^0") (d #t) (k 2)))) (h "0688166ivln9xvvf0xs6vikrwzm0n4lpzsflmwfqh0kksxnkf5dq")))

(define-public crate-rstats-1.2.5 (c (n "rstats") (v "1.2.5") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^0") (d #t) (k 2)))) (h "0zh33g72haj2vjw3wrylpa9j76rhpclxw30j7rp17964zrqi2ccf")))

(define-public crate-rstats-1.2.6 (c (n "rstats") (v "1.2.6") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^0") (d #t) (k 2)))) (h "013y215py053v8lkgqzhm8bn0wbrcn8carmqbybszm7z7lqrynnc")))

(define-public crate-rstats-1.2.7 (c (n "rstats") (v "1.2.7") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "1w2kkqwadg2vpaxlqi3mdv861z73jwfj1bwfmbsglwv53725jk7m")))

(define-public crate-rstats-1.2.8 (c (n "rstats") (v "1.2.8") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "0y8686kxvf6ysrpfv4s1yzwibxvszkg90sb77ck5jnac7w55wgb6")))

(define-public crate-rstats-1.2.9 (c (n "rstats") (v "1.2.9") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "0zla6225qcfnslzc8dsn6p9yw2sqf88g6mlyzp74nzg8wfqfy761")))

(define-public crate-rstats-1.2.10 (c (n "rstats") (v "1.2.10") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "06ixpbmr9x5sl8v22kdxgilanllb21knxgkzcfinjgigk9sba28g")))

(define-public crate-rstats-1.2.11 (c (n "rstats") (v "1.2.11") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "0xkzascvd209zlj03r504rjyswfycz488mcgxzy38hwb7jnlx14d")))

(define-public crate-rstats-1.2.12 (c (n "rstats") (v "1.2.12") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "05wq4fnpird5529ky32r815ndfab80ylmc7djmf76pzig8nj4qr8")))

(define-public crate-rstats-1.2.13 (c (n "rstats") (v "1.2.13") (d (list (d (n "indxvec") (r "^1.4.2") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "0mv8ab5rwzlhcyrjh4mykl5g9h4nfiznjxpnjfrwnlxwmzj6mjhj")))

(define-public crate-rstats-1.2.14 (c (n "rstats") (v "1.2.14") (d (list (d (n "indxvec") (r "^1.4.2") (d #t) (k 0)) (d (n "medians") (r "^1.0.4") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 2)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "1xf9ndl9wkgm4gkga8qskvb1kyg504qgh72w8hjcwmzj7izyjg8m")))

(define-public crate-rstats-1.2.15 (c (n "rstats") (v "1.2.15") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^1.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 2)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "05qp6d78g573drp16841fl528rxc2d578wczg24slwjrhcg75d93")))

(define-public crate-rstats-1.2.16 (c (n "rstats") (v "1.2.16") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^1.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 2)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "189pk1k0y20qcv2qaxycg1fw3d2dynyld1lddlv8yjz2n1aqn8pm")))

(define-public crate-rstats-1.2.17 (c (n "rstats") (v "1.2.17") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^1.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 2)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "081wbhgz2v2if9gixmj41wcb1p4zpb4fif8fp26dr9hbrkh8aq4a")))

(define-public crate-rstats-1.2.18 (c (n "rstats") (v "1.2.18") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^1.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 2)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "07y79j7pgsqlksg4zzkx8bfv0ip8x1vanakxy672zg2jfg3ka4l4")))

(define-public crate-rstats-1.2.19 (c (n "rstats") (v "1.2.19") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^1.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 2)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1wcb7dm9f43ak7znmjn2m1ghk2mavrkfxq3spixvnc2nb2zvk6bj")))

(define-public crate-rstats-1.2.20 (c (n "rstats") (v "1.2.20") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^1.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0023r0mi58ny5qyb7sh9qmz5apg4ny5h0l89ndzz1qaglxriysc1")))

(define-public crate-rstats-1.2.21 (c (n "rstats") (v "1.2.21") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "086wl2ijq2h8bp9aga7j9qv8ffi20pvs9idvfv8j7zdn6k89chvq")))

(define-public crate-rstats-1.2.22 (c (n "rstats") (v "1.2.22") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1bs3kgfw00hwk2z6ap66zv52xnb64x5q9bf9srg8m86krj12hlmk")))

(define-public crate-rstats-1.2.23 (c (n "rstats") (v "1.2.23") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1i7m54bhvgzs9m5ymv0z4yv915gv4rl5cyk5a13mmgmhwdjksgvw")))

(define-public crate-rstats-1.2.24 (c (n "rstats") (v "1.2.24") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1hx21i0gzzlqd0a65j8g1v57nfh7kdr0rxlfh6mz5qx2h75qi485")))

(define-public crate-rstats-1.2.25 (c (n "rstats") (v "1.2.25") (d (list (d (n "duplicate") (r "^0.4") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0bp5plrl2j11qgl5brj1afx5ibxrx0hrz3mpyybf7gdzv7rfgh4n")))

(define-public crate-rstats-1.2.26 (c (n "rstats") (v "1.2.26") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0l2qj5ph5is949w23mvcim4ivkbr453p8v7qj3fv6ms9l5v5bz8s")))

(define-public crate-rstats-1.2.27 (c (n "rstats") (v "1.2.27") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1nnxwl6z80qccimmrj6gp83mnv6xkxxyba90kr0yf6ncqrw4d2l2")))

(define-public crate-rstats-1.2.28 (c (n "rstats") (v "1.2.28") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "161rl99rz7ibabh1lz1iq5ls651dz63anwwys3waab48mlrilyr0")))

(define-public crate-rstats-1.2.29 (c (n "rstats") (v "1.2.29") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "15q2wnicgn732p0fsidri39h7cldfr52s5r6px5153ycwj4mv6dz")))

(define-public crate-rstats-1.2.30 (c (n "rstats") (v "1.2.30") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0fsrzvyy3gm420sv11f77b2a29iqylvh9a9hc2552iv1hind2yvh")))

(define-public crate-rstats-1.2.31 (c (n "rstats") (v "1.2.31") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1s5vmbgfinmjpggfdc8ibskmdf9nps3qxg02nwahcrkcf405hhxb")))

(define-public crate-rstats-1.2.32 (c (n "rstats") (v "1.2.32") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "11gld7i75fkv09qy0x2ifb2aa340rvqhsqfdgvl1zjaxrssyxmpv")))

(define-public crate-rstats-1.2.33 (c (n "rstats") (v "1.2.33") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0m2vy6z56bvyjhljl9421gf08ly3x11l16p4z85yvfnrb9mnz57i")))

(define-public crate-rstats-1.2.34 (c (n "rstats") (v "1.2.34") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0qb3f5l1dh2y3phsqi3mp1miq7flizhlcjd8iyx2q9ksmmg80aik")))

(define-public crate-rstats-1.2.35 (c (n "rstats") (v "1.2.35") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "16njnjlbj50p8njb6rn8znwck87p7cr4257n12pic6sq0mn3p707")))

(define-public crate-rstats-1.2.36 (c (n "rstats") (v "1.2.36") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "12cq8w81i0vfj1p4p6fnsklv66ga01ckb8mhdcbi0y6pmxyyg0r5")))

(define-public crate-rstats-1.2.37 (c (n "rstats") (v "1.2.37") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "19w9zr671gsgqxx5flvaz9qdplk7bx7z2vlczlkkf04r0aqn0316")))

(define-public crate-rstats-1.2.38 (c (n "rstats") (v "1.2.38") (d (list (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "16grm7h6cv5b38wq1niy8cvh95kj8ziafvjhjpngzl87far9rnqq")))

(define-public crate-rstats-1.2.39 (c (n "rstats") (v "1.2.39") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1m91nnsh1zwpysab9mza6c4qyffsd6sh62cds32k7rw3wrydi5qs") (y #t)))

(define-public crate-rstats-1.2.40 (c (n "rstats") (v "1.2.40") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0a6z22hlh25h6irkvncr4s7cg3rq3ljss3q164k3p2n3444kfmad")))

(define-public crate-rstats-1.2.41 (c (n "rstats") (v "1.2.41") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0y5awv646dfzw3vziadb06mjg8lzpx13zg796qxqwhsgs3maqrww")))

(define-public crate-rstats-1.2.42 (c (n "rstats") (v "1.2.42") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0c90vfcds85y18vxavgr519iddw7iqk8yspdwmngcl57xzxw40dq")))

(define-public crate-rstats-1.2.43 (c (n "rstats") (v "1.2.43") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0xdfmqygr77qqr9002p1pjyzfr4x9gwbd38z2cn1gk39gwmn2bgw")))

(define-public crate-rstats-1.2.44 (c (n "rstats") (v "1.2.44") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "03khmnrwsa5v7c31vsmk9ad9zrd6mbvv4hr28q0pwrca5znc7vaf")))

(define-public crate-rstats-1.2.45 (c (n "rstats") (v "1.2.45") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1wrgxs3k8icmzzdzraamw16bx0adazyi11ncp33znz02v8a6nzpv")))

(define-public crate-rstats-1.2.46 (c (n "rstats") (v "1.2.46") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "11bzvfj5v8pl230zbs4wkvx88hyn7sfhg99969d972wr4mg76zpq")))

(define-public crate-rstats-1.2.47 (c (n "rstats") (v "1.2.47") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1x71mwlkdf3yzrf276f9y77js8mrw7j2gwbp9zg11s5bri0zxqfh")))

(define-public crate-rstats-1.2.48 (c (n "rstats") (v "1.2.48") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0wx18dir0zw32zp5rqd7nwa8pl93l80198i09f91q4js9ri0iayi")))

(define-public crate-rstats-1.2.49 (c (n "rstats") (v "1.2.49") (d (list (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0040l7vwywk2aw45kwqmw8npcmjk14yh1ymhkqxm8f4xnz0v8f6v")))

(define-public crate-rstats-1.2.50 (c (n "rstats") (v "1.2.50") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0j6slfwyi874xq5rlli9r9fpqxc4b0qg793lsc5j1np8a8n2vlwg")))

(define-public crate-rstats-1.2.51 (c (n "rstats") (v "1.2.51") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0a28zh8600ssw26ffd7k965m19rir4fcynifxmagimfvbl9abyy9")))

(define-public crate-rstats-1.2.52 (c (n "rstats") (v "1.2.52") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0id6crpm6sm35vca0lv79n13gi0w313kdaknrglch33plk96knqx")))

(define-public crate-rstats-1.2.53 (c (n "rstats") (v "1.2.53") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0ajn4vair6320b3kbydgawqshiri5yk800bcxh5nvk26837p953d")))

(define-public crate-rstats-1.3.0 (c (n "rstats") (v "1.3.0") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0mi3va6pgfly2m9m1r5gpf87xc50l38ycc8fmbarqjngsyslh46g")))

(define-public crate-rstats-1.3.1 (c (n "rstats") (v "1.3.1") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0asjgkp3kn71935x962rmvviwp44syr79lyiii1qjcqdfrfkamp8")))

(define-public crate-rstats-1.3.2 (c (n "rstats") (v "1.3.2") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1h624a4k4lh9klxqdgz7g1mhff8flxa08fs42cqfgb0ysy30b1aj")))

(define-public crate-rstats-1.3.3 (c (n "rstats") (v "1.3.3") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1ag5nw7kizb3vaj9dps2r68fsl160wz6dxf5972g4j16lbfrlzsa")))

(define-public crate-rstats-2.0.0 (c (n "rstats") (v "2.0.0") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "17hgs146jhq2j78bx8i1skyirhsx1yf1nkf19m0jsfgqi6m7f4vv")))

(define-public crate-rstats-2.0.1 (c (n "rstats") (v "2.0.1") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0nzdlyy9qhal5i4qml6364jxxb7zv3z0yl8plxlh5l3hiz4vvbpc")))

(define-public crate-rstats-2.0.2 (c (n "rstats") (v "2.0.2") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "14lhxz0idlfvlkw1h6mvl1rzcz8hlxrcfni0c39hh0rpb8pndr16") (y #t)))

(define-public crate-rstats-2.0.3 (c (n "rstats") (v "2.0.3") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0kxj5xy8k7m8pcglcr3m7fl4svqwsikhq1571sil69wvb3mrz3s7") (y #t)))

(define-public crate-rstats-2.0.4 (c (n "rstats") (v "2.0.4") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0h77yk3bqh7jdfprp80a6wda6h4n7qq0714abjbw3vbyswkkzr6f")))

(define-public crate-rstats-2.0.5 (c (n "rstats") (v "2.0.5") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^3.0") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "0nl7ifpvcwrmkn42c526fb99x4jz5vih2q0xd16qr8y4pr3as9j2")))

(define-public crate-rstats-2.0.6 (c (n "rstats") (v "2.0.6") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^3.0") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "05wx4274j87qx7jc98lkkfkab8j873h9q7jmhzkgndx0nf1ldn2r")))

(define-public crate-rstats-2.0.7 (c (n "rstats") (v "2.0.7") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^3.0") (d #t) (k 0)) (d (n "ran") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "04yaym5ly17jylc3a1rsyzd3rb3wisqmcapqap32sic4ymp52mcf")))

(define-public crate-rstats-2.0.8 (c (n "rstats") (v "2.0.8") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^3.0") (d #t) (k 0)) (d (n "ran") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "1jj32wzbjvx9r51q23aka78pywmi9yqa82hrj7y02z7qjhlxxa3x")))

(define-public crate-rstats-2.0.9 (c (n "rstats") (v "2.0.9") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^3.0.8") (d #t) (k 0)) (d (n "ran") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.9") (d #t) (k 0)) (d (n "times") (r "^1.0") (d #t) (k 2)))) (h "16pl30jfhz0g2jzlhf8kpz1n2p8awxx8zb8iwd286fy0grwfshy9")))

(define-public crate-rstats-2.0.10 (c (n "rstats") (v "2.0.10") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "12dkwcbz7jwlwmsvhf653pm98dmn6ckdvqi76kj0yw1mg6nz3qf9")))

(define-public crate-rstats-2.0.11 (c (n "rstats") (v "2.0.11") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "0rl9siafcgpz1rjn2r6fljf2dh0dcqw54mbfdphhrz6xsjd1lfln")))

(define-public crate-rstats-2.0.12 (c (n "rstats") (v "2.0.12") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "09jblh7n29hmrgry35gvkg40f92lw38rwxvi0m4avb7na7acg6lc")))

(define-public crate-rstats-2.1.0 (c (n "rstats") (v "2.1.0") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "1vzxyk5rgz6nf2gx7y178nd4bd9jaihfx61p5v2mbi9125vc0i56")))

(define-public crate-rstats-2.1.1 (c (n "rstats") (v "2.1.1") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "04v5003pm18pq04ba7bxzyi4q3nhdq20i8d0h301nli2njn9fxhw") (y #t)))

(define-public crate-rstats-2.1.2 (c (n "rstats") (v "2.1.2") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "1fc3r65ayp9vp84ajq5n5yi01486gr4pvrlh0d3h41pp6qfd23mv")))

(define-public crate-rstats-2.1.3 (c (n "rstats") (v "2.1.3") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "032mzmvphgg5sldw7ss75zbjbkci0507ayjikhkak7pwxy5hiim4")))

(define-public crate-rstats-2.1.4 (c (n "rstats") (v "2.1.4") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "0rky89kjlykgri58jipc41zfiq3v2s2w5bmfsk1fsnqwf3yh303p")))

(define-public crate-rstats-2.1.5 (c (n "rstats") (v "2.1.5") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "0j1r1fgmk9anc7060knm0gyiipc4mhcx98cwmxgj4j1vwgl5cc63")))

(define-public crate-rstats-2.1.6 (c (n "rstats") (v "2.1.6") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "03fkqjisdjb4ffm112f1b8c0aing8bmn8zaacsyma61i99nyxn1d")))

(define-public crate-rstats-2.1.7 (c (n "rstats") (v "2.1.7") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "0adsv8r2501xvh7f13rs5d6p15v8srifi787sy390z8z2z84h9s9")))

(define-public crate-rstats-2.1.8 (c (n "rstats") (v "2.1.8") (d (list (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^3") (d #t) (k 0)) (d (n "ran") (r "^2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "times") (r "^1") (d #t) (k 2)))) (h "1b5sm4h7dwi93jpqvk1j22bfidw2ndhr5ibg0ixpqlf8qx7194g8")))

