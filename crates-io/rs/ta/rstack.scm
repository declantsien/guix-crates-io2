(define-module (crates-io rs ta rstack) #:use-module (crates-io))

(define-public crate-rstack-0.1.0 (c (n "rstack") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "unwind") (r "^0.2") (f (quote ("ptrace"))) (d #t) (k 0)))) (h "1ia1gr5wy4j1p52k8kyn37hdajr5hix1g8hki3w093bs65jrgn95")))

(define-public crate-rstack-0.1.1 (c (n "rstack") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unwind") (r "^0.2") (f (quote ("ptrace"))) (d #t) (k 0)))) (h "1xf1ml9p9khllsdyxjfdfvy51mn800nlwdpqnspknq4zr6nj5jw7")))

(define-public crate-rstack-0.1.2 (c (n "rstack") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unwind") (r "^0.2") (f (quote ("ptrace"))) (d #t) (k 0)))) (h "0dyx02jjcaisx06vc038yxbrzs3l2mglw0dcdsb0amrd0ym054fh")))

(define-public crate-rstack-0.2.0 (c (n "rstack") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dw_") (r "^0.1") (o #t) (d #t) (k 0) (p "dw")) (d (n "lazy_static") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unwind_") (r "^0.2") (f (quote ("ptrace"))) (o #t) (d #t) (k 0) (p "unwind")))) (h "0m29030b9pl6r3sc3wwrgpd713s6zqb2kff0p2zd6x5322dmg4lv") (f (quote (("unwind" "unwind_") ("dw" "dw_" "lazy_static") ("default" "unwind"))))))

(define-public crate-rstack-0.3.0 (c (n "rstack") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dw_") (r "^0.2") (o #t) (d #t) (k 0) (p "dw")) (d (n "lazy_static") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unwind_") (r "^0.3") (f (quote ("ptrace"))) (o #t) (d #t) (k 0) (p "unwind")))) (h "1f859v3xklxkdpdlhcx0r13h5vhgq8xp3q4pikhs8b3jj1kzd844") (f (quote (("unwind" "unwind_") ("dw" "dw_" "lazy_static") ("default" "unwind"))))))

(define-public crate-rstack-0.3.1 (c (n "rstack") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dw_") (r "^0.2") (o #t) (d #t) (k 0) (p "dw")) (d (n "lazy_static") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unwind_") (r "^0.3") (f (quote ("ptrace"))) (o #t) (d #t) (k 0) (p "unwind")))) (h "0x6pscpxgvpyn974r29d4qdiz03jm6hiv62an8805pb4dlrrvp45") (f (quote (("unwind" "unwind_") ("dw" "dw_" "lazy_static") ("default" "unwind"))))))

(define-public crate-rstack-0.3.2 (c (n "rstack") (v "0.3.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dw_") (r "^0.2") (o #t) (d #t) (k 0) (p "dw")) (d (n "lazy_static") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unwind_") (r "^0.4") (f (quote ("ptrace"))) (o #t) (d #t) (k 0) (p "unwind")))) (h "1gchdwzkr4pffwx2h9fy5z43il00j448pd56a846hv4asxw53csx") (f (quote (("unwind" "unwind_") ("dw" "dw_" "lazy_static") ("default" "unwind"))))))

(define-public crate-rstack-0.3.3 (c (n "rstack") (v "0.3.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "dw_") (r "^0.2") (o #t) (d #t) (k 0) (p "dw")) (d (n "lazy_static") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unwind_") (r "^0.4") (f (quote ("ptrace"))) (o #t) (d #t) (k 0) (p "unwind")))) (h "14q4fcxrk6kj3943v0myiil8070243xf5vrlc4pba5sgplz9vpz7") (f (quote (("unwind" "unwind_") ("dw" "dw_" "lazy_static") ("default" "unwind"))))))

