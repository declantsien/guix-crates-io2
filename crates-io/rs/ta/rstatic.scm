(define-module (crates-io rs ta rstatic) #:use-module (crates-io))

(define-public crate-rstatic-0.1.0 (c (n "rstatic") (v "0.1.0") (d (list (d (n "clap") (r "^1.4") (f (quote ("color"))) (k 0)) (d (n "iron") (r "^0.2.4") (d #t) (k 0)) (d (n "mount") (r "^0.0.9") (d #t) (k 0)) (d (n "staticfile") (r "^0.0.6") (d #t) (k 0)))) (h "1w8c1sjrdlj1awg847yr1bmya037vx53m61crfkvg9cgwnd83rrb")))

(define-public crate-rstatic-0.1.1 (c (n "rstatic") (v "0.1.1") (d (list (d (n "clap") (r "*") (f (quote ("color"))) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "mount") (r "*") (d #t) (k 0)) (d (n "staticfile") (r "*") (d #t) (k 0)))) (h "1mf7sd0bmippwl6pb1l5lvl6xyksxaiak3860a7l8swag98xdqbi")))

(define-public crate-rstatic-0.1.2 (c (n "rstatic") (v "0.1.2") (d (list (d (n "clap") (r "^1.5.5") (f (quote ("color"))) (k 0)) (d (n "iron") (r "^0.2.6") (d #t) (k 0)) (d (n "mount") (r "^0.0.9") (d #t) (k 0)) (d (n "staticdir") (r "^0.3.1") (d #t) (k 0)) (d (n "staticfile") (r "^0.1.0") (d #t) (k 0)))) (h "1jq0d3dgd64px5nbizzaw8wwx60la353cz7rdj6lcdgfv0imkl18")))

(define-public crate-rstatic-0.1.3 (c (n "rstatic") (v "0.1.3") (d (list (d (n "clap") (r "^2.1.0") (f (quote ("color"))) (k 0)) (d (n "iron") (r "^0.2.6") (d #t) (k 0)) (d (n "mount") (r "^0.0.9") (d #t) (k 0)) (d (n "staticdir") (r "^0.3.1") (d #t) (k 0)) (d (n "staticfile") (r "^0.1.0") (d #t) (k 0)))) (h "19mdcpl0slqwkm5xz8njgv4ncwrxqlw7n0nyh9is9vmbcl669lnj")))

