(define-module (crates-io rs #{3a}# rs3a) #:use-module (crates-io))

(define-public crate-rs3a-0.1.2 (c (n "rs3a") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pa1p2jwjw8cd705xgldksz79crc88pn62mxilf8w0k7xds435h3")))

(define-public crate-rs3a-0.2.0 (c (n "rs3a") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0s2nxpj40f44aav1qddgd9qz4mjyj56r0sphkyzh5mq7baknm3ch")))

(define-public crate-rs3a-0.2.1 (c (n "rs3a") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1l83z4kdx8gvj75fsn75v5741yg6m68836h0117jd068kj7rg39g")))

(define-public crate-rs3a-0.2.2 (c (n "rs3a") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08ia46g46gg70bz60bhz7azh25iq506qz1zd0nmh4lkkhbribvg3")))

(define-public crate-rs3a-0.2.3 (c (n "rs3a") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19lnk3ra4qkrgshdq7h0dja24fd7h191k02p8ij1f6dhj80msvzc")))

(define-public crate-rs3a-1.0.0 (c (n "rs3a") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0n2l278lfsfzindrx7wg604s5yab4wjb4pxn2k4vaqz20jxi05s5")))

(define-public crate-rs3a-1.0.1 (c (n "rs3a") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0n1zsyal4nb74agdn0sakg1q9w5ydv13z7mn2ck079ngqbcbfiac")))

(define-public crate-rs3a-1.0.2 (c (n "rs3a") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mzlrzd7kn2s1744k1swfjzmrpw672rk4jvz4cnshaawk87i6xf0")))

(define-public crate-rs3a-1.0.3 (c (n "rs3a") (v "1.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1k9fmhb9y0zagr35sypwsw60rbj5g3qf5k2k4bp16ymy3ng7mcir")))

(define-public crate-rs3a-1.0.5 (c (n "rs3a") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vh6csjwiy4rqr4iy97n548bmj1rbzpzkw03r2mws2rrj4j5shy3")))

(define-public crate-rs3a-1.0.6 (c (n "rs3a") (v "1.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0j5br1xdhzz1aj6hybvrcngygjv1sdz8z2zp2wibcbn0xa47xxf4")))

