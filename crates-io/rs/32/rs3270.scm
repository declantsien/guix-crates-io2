(define-module (crates-io rs #{32}# rs3270) #:use-module (crates-io))

(define-public crate-rs3270-0.1.0 (c (n "rs3270") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "10mvagm4f13mhxiqsiy4ra398jvl0c1vyj44x1md0f8zbabdl8qz") (y #t)))

(define-public crate-rs3270-0.1.1 (c (n "rs3270") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1kw7yqmx0cwyh2asqaj5dp5ilw0x0w9xa4hpkkbcdwdm3gjkadpv") (y #t)))

(define-public crate-rs3270-0.1.2 (c (n "rs3270") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0g0lig8whhq7xmij236bwb6c8iaaq8a28vbnc8g72fb1sx5w0i4j")))

