(define-module (crates-io rs ap rsap) #:use-module (crates-io))

(define-public crate-rsap-0.1.0 (c (n "rsap") (v "0.1.0") (h "0bq7730rbqkxryvg9n5kqy2l3fy539110l29iyxrir90s2msaq1z") (y #t)))

(define-public crate-rsap-0.2.0 (c (n "rsap") (v "0.2.0") (h "09a1bwx0mkhya6w8ggf2zwinvqqrh8528rjssj13q07gfgmvdq63") (y #t)))

(define-public crate-rsap-0.3.0 (c (n "rsap") (v "0.3.0") (h "0qbs9692vh2jbqqybzivvy1a7x1a5iq1xfdn0qvkmcspy18gx7fs") (y #t)))

(define-public crate-rsap-0.3.1 (c (n "rsap") (v "0.3.1") (d (list (d (n "mtk") (r "^0.1.1") (d #t) (k 0)))) (h "16dn41xi27638j6yw0jnncc9xa8yvvm08xfljy00957bkx6yzzyj") (y #t)))

(define-public crate-rsap-0.3.2 (c (n "rsap") (v "0.3.2") (d (list (d (n "mtk") (r "^0.1.1") (d #t) (k 0)))) (h "165q50s3r49gpxa71nihpf6qqpycxddfgh6h5xchrhlccnph18pg") (y #t)))

(define-public crate-rsap-0.3.3 (c (n "rsap") (v "0.3.3") (d (list (d (n "mtk") (r "^0.1.3") (d #t) (k 0)))) (h "1vhygj3j11q0wagvfsnq456mq87663pixar8cxaj7k5fcjl9pa0k") (y #t)))

(define-public crate-rsap-0.3.4 (c (n "rsap") (v "0.3.4") (d (list (d (n "mtk") (r "^0.1.3") (d #t) (k 0)))) (h "14gd9l39n0fix6lynw938zpw2mhj5jqjc65xxm6nc6rcks3hpcpw") (y #t)))

(define-public crate-rsap-0.3.5 (c (n "rsap") (v "0.3.5") (d (list (d (n "mtk") (r "^0.1.7") (d #t) (k 0)))) (h "0352lcqr3fp7gwrbwg78f72pqkbf3gk104c3idkvf0za2gcypizv") (y #t)))

(define-public crate-rsap-0.3.6 (c (n "rsap") (v "0.3.6") (d (list (d (n "mtk") (r "^0.2.2") (d #t) (k 0)))) (h "0jdws2i7dqk2ij76d2r0sv7wh0cynscj9z7x6bdpm12xvhal76j9") (y #t)))

(define-public crate-rsap-0.3.7 (c (n "rsap") (v "0.3.7") (d (list (d (n "mtk") (r "^0.2.3") (d #t) (k 0)))) (h "0svigf4rrapm8550sfms903b5vd685f60gyala01l9yyibmc4chs") (y #t)))

(define-public crate-rsap-0.3.8 (c (n "rsap") (v "0.3.8") (d (list (d (n "mtk") (r "^0.2.8") (d #t) (k 0)))) (h "0afmwq00hrgagb8xjmn9v0454fj9p4asprg94qi1xw513024r3n1") (y #t)))

(define-public crate-rsap-0.3.9 (c (n "rsap") (v "0.3.9") (d (list (d (n "mtk") (r "^0.3.0") (d #t) (k 0)))) (h "036c0g99scpv3p5psmvfj8m17mnsy3azvwyjfcj4s3sx54jf4iyz") (y #t)))

(define-public crate-rsap-0.4.0 (c (n "rsap") (v "0.4.0") (h "1apjfxv9igzyhnnmamz80pa1acan5j7jm8iz9z0g2w7ck65cm8p2") (y #t)))

