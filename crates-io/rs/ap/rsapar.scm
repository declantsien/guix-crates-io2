(define-module (crates-io rs ap rsapar) #:use-module (crates-io))

(define-public crate-rsapar-0.1.0 (c (n "rsapar") (v "0.1.0") (h "1k2brrmwq4xvr5yf1521vginwd4lmz3vmy1cgnv4b343nyyl5bl4") (y #t) (r "1.76")))

(define-public crate-rsapar-0.1.1 (c (n "rsapar") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.20") (d #t) (k 0)))) (h "0pnbdgyccwm2gix7mi53z593nh53w4s4rhhfjs3k54z8v8rihgz6") (r "1.65.0")))

