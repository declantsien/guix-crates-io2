(define-module (crates-io rs ap rsapi_http) #:use-module (crates-io))

(define-public crate-rsapi_http-0.5.0-rc.1 (c (n "rsapi_http") (v "0.5.0-rc.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00phlhxp0fwrazcsllkx1ldp8291bxc5xzbah4pjmgpj4vaqlgz7")))

