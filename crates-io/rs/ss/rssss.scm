(define-module (crates-io rs ss rssss) #:use-module (crates-io))

(define-public crate-rssss-0.1.0 (c (n "rssss") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "opml") (r "^1.1.5") (d #t) (k 0)) (d (n "rss") (r "^2.0.6") (f (quote ("validation"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "xml") (r "^0.8.10") (d #t) (k 0)))) (h "1z82hnkzfym9237n2y0bd66ci4d7hiz4212fdhvnkq2pdjw0k0gz")))

(define-public crate-rssss-0.1.1 (c (n "rssss") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "opml") (r "^1.1.5") (d #t) (k 0)) (d (n "rss") (r "^2.0.6") (f (quote ("validation"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "xml") (r "^0.8.10") (d #t) (k 0)))) (h "065kns5vjvy50c2sqz59abj8xa2csja91wza1m10xsp7g1aai3kj")))

