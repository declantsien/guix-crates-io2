(define-module (crates-io rs li rslint_macros) #:use-module (crates-io))

(define-public crate-rslint_macros-0.1.0 (c (n "rslint_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "02a47xfz3x4sr3x62maxy8mz6xrhfrj4prjmwy2c0ymvfglr8ik5")))

