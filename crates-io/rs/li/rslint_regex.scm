(define-module (crates-io rs li rslint_regex) #:use-module (crates-io))

(define-public crate-rslint_regex-0.1.0 (c (n "rslint_regex") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rslint_errors") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1g12p22zzf4nyvjj2mf18wz2r3v5zihxckr6fzx1vvy98108lsri")))

(define-public crate-rslint_regex-0.1.1 (c (n "rslint_regex") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rslint_errors") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0vzqnw45x0k9nshwnqf2vmdja9yb1k2mra2mxk28samanjz6gfj5")))

(define-public crate-rslint_regex-0.2.0 (c (n "rslint_regex") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rslint_errors") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1h6pjrysrk5gw0mj6njb5dkmc1cr80b30y28g3jjd4zr8fd21y4c")))

(define-public crate-rslint_regex-0.2.1 (c (n "rslint_regex") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rslint_errors") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1qcbl39vdjrh1cxd41z3zmq2zczbrbzbjjy6aj4dxqd48xfh3z70")))

(define-public crate-rslint_regex-0.3.0 (c (n "rslint_regex") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rslint_errors") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0rbi3fixyf2ckvixp9g565qwj8g7ndipb8w55wqclx15wzwhx10d")))

