(define-module (crates-io rs li rslint_syntax) #:use-module (crates-io))

(define-public crate-rslint_syntax-0.1.0 (c (n "rslint_syntax") (v "0.1.0") (d (list (d (n "rowan") (r "^0.10.0") (d #t) (k 0)))) (h "1pqi8hibqpb3r9jp1i670q6iyrcx7jbiprda94xmxrwzx6l2liwx")))

(define-public crate-rslint_syntax-0.1.1 (c (n "rslint_syntax") (v "0.1.1") (d (list (d (n "rslint_rowan") (r "^0.10.0") (d #t) (k 0)))) (h "12frsc25wym0bahzi8b6dk2p9333nv2qhdmlhasq2ca1y0cx70mq")))

(define-public crate-rslint_syntax-0.1.2 (c (n "rslint_syntax") (v "0.1.2") (d (list (d (n "rslint_rowan") (r "^0.10.0") (d #t) (k 0)))) (h "1rbfgh1rcs0jks25l4yx8s6awsq0qziyg3j00bjbcngaa2w0pjh4")))

(define-public crate-rslint_syntax-0.1.3 (c (n "rslint_syntax") (v "0.1.3") (d (list (d (n "rslint_rowan") (r "^0.10.0") (d #t) (k 0)))) (h "0jkfmv5z6ibr45v29x19rdfvn8577mqla6p9r1spadsfhqrang0j")))

(define-public crate-rslint_syntax-0.1.4 (c (n "rslint_syntax") (v "0.1.4") (h "17m74qcdjmxzw850l5ykqgz3bqgj6wz2n10a128qqycyji3p6kpg")))

