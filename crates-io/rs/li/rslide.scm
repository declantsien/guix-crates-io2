(define-module (crates-io rs li rslide) #:use-module (crates-io))

(define-public crate-rslide-0.1.0 (c (n "rslide") (v "0.1.0") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-files") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^3.3") (d #t) (k 0)) (d (n "actix-web-actors") (r "^3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0r094w52bgpkj93v08czgnbvg0n2facq8dah1c17s7fqkji1hxwk")))

