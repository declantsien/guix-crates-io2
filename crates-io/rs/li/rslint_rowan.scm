(define-module (crates-io rs li rslint_rowan) #:use-module (crates-io))

(define-public crate-rslint_rowan-0.10.0 (c (n "rslint_rowan") (v "0.10.0") (d (list (d (n "erasable") (r "^1.2.1") (d #t) (k 0)) (d (n "m_lexer") (r "^0.0.4") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)) (d (n "slice-dst") (r "^1.4.1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.10") (d #t) (k 0)) (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0abkqnymkshw1fky1dnsj4c3mpqq6i33j344gmcm7pw8gdbn1pm2") (f (quote (("serde1" "serde" "text-size/serde"))))))

