(define-module (crates-io rs li rsline) #:use-module (crates-io))

(define-public crate-rsline-0.1.0 (c (n "rsline") (v "0.1.0") (h "1zjqirc903kfld62mhxrmgxc2v3css5989z62657qz7gay6kxmz1")))

(define-public crate-rsline-0.2.0 (c (n "rsline") (v "0.2.0") (h "1rkis0s0rg970c5x1nx2j4cyz6cc2ai8h4xg4wphw1y1mphnpmlq")))

