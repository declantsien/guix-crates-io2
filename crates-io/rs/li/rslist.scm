(define-module (crates-io rs li rslist) #:use-module (crates-io))

(define-public crate-rslist-0.1.0 (c (n "rslist") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "01i7d84xfxw97pbn6zw751vgri6jhlrskmb6gxcadq9pxvwhv3mm")))

(define-public crate-rslist-0.1.1 (c (n "rslist") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0fgan54hv7327yljx1b1y18bsni53j532hbzqjq6lnscjl8iqyhr")))

(define-public crate-rslist-0.1.2 (c (n "rslist") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1x6hlh96q860p9kjgw9kkdwh4fpxfqgb0gybvcqw9xs72cwvs8yq")))

