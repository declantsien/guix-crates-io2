(define-module (crates-io rs di rsdice) #:use-module (crates-io))

(define-public crate-rsdice-0.3.1 (c (n "rsdice") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "14n9azw47i4sx5xz8mjja11a24bvjkdhy29whdwvha554nbvcws9")))

(define-public crate-rsdice-1.0.0 (c (n "rsdice") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "0452gqi2vzwiadxpzy83fypr1nz13bvj2bc7mb9gfhf81yq5f0ws")))

(define-public crate-rsdice-1.0.1 (c (n "rsdice") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "0ggq4xqma84yhvb7cqnmk299qvzi45ma69547wnifhc91qp7s6l3")))

(define-public crate-rsdice-1.0.2 (c (n "rsdice") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "073y7jvay30dsy9a3b8gzxiqahk90iwc878g8rm4xqhqm2jsj6g5")))

