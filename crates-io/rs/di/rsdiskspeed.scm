(define-module (crates-io rs di rsdiskspeed) #:use-module (crates-io))

(define-public crate-rsdiskspeed-0.1.0 (c (n "rsdiskspeed") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "time") (r "^0.2.19") (d #t) (k 0)))) (h "1ndy2h4zlm2zkr6l7gh256c3c03qhqqr0jkc0i0nx2qydcisycqy")))

(define-public crate-rsdiskspeed-0.2.0 (c (n "rsdiskspeed") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "time") (r "^0.2.19") (d #t) (k 0)))) (h "098fp6jvil81q6p45nsrabx0pf2jjbmdb2sp0zf97v9i51zfdfh8")))

(define-public crate-rsdiskspeed-0.3.0 (c (n "rsdiskspeed") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "time") (r "^0.2.19") (d #t) (k 0)))) (h "0gph38xbp39f3a90hkgr80c471qn85mmia56vqshpij8in35xj8b")))

(define-public crate-rsdiskspeed-0.3.2 (c (n "rsdiskspeed") (v "0.3.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "time") (r "^0.2.19") (d #t) (k 0)))) (h "078hp6aqqfrwy9d5yl0c0rqapq1skn5m3kpj16bm9y6y3j7bnv6d")))

