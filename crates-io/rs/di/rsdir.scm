(define-module (crates-io rs di rsdir) #:use-module (crates-io))

(define-public crate-rsdir-0.1.0 (c (n "rsdir") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.4.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "02aqlfr5rnp9r44cg1nscp64w0mypavbjv6v3vcsh2sc129n6riq")))

