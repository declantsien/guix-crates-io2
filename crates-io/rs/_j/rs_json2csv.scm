(define-module (crates-io rs _j rs_json2csv) #:use-module (crates-io))

(define-public crate-rs_json2csv-0.1.0 (c (n "rs_json2csv") (v "0.1.0") (d (list (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hard3bg7wfgzb67lbpqsipmdv7ki4xvkjs3kavib49khk2238ry")))

(define-public crate-rs_json2csv-0.1.1 (c (n "rs_json2csv") (v "0.1.1") (d (list (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09l3a5g6gf7z941ji9ib2i4amrpyf4f24fm40nfzj4rznhn9njpd")))

