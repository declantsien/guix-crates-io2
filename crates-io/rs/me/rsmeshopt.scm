(define-module (crates-io rs me rsmeshopt) #:use-module (crates-io))

(define-public crate-rsmeshopt-0.0.7 (c (n "rsmeshopt") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09x3lipfgkrp6agm2rccwhcq92fwaz9nfdy9r1sp9ddh04969y9l")))

(define-public crate-rsmeshopt-0.0.8 (c (n "rsmeshopt") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a13q05759w89hy2d9ddzp4zjs83xvf3v4ggf67nk30ghy0yxkk1")))

(define-public crate-rsmeshopt-0.0.9 (c (n "rsmeshopt") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1axab2z7p1dkxawnp3zvqax55zhizlrrd0hkjm5b0mvwj97pmxm8")))

(define-public crate-rsmeshopt-0.0.11 (c (n "rsmeshopt") (v "0.0.11") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zrrad1gmccnlax44p8q0gjnkp8aph303dp3hy5zcpacrqg40lh5")))

