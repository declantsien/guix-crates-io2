(define-module (crates-io rs me rsmemoryapi) #:use-module (crates-io))

(define-public crate-rsmemoryapi-0.0.1 (c (n "rsmemoryapi") (v "0.0.1") (h "1wfqhwryiy1q4yz8jqfsh9l7qchaabiml2hcmhg8rqbydma8m1yq")))

(define-public crate-rsmemoryapi-0.0.2 (c (n "rsmemoryapi") (v "0.0.2") (h "1y0d260jj70gjqwq34jzlszcx1hrjzhl6yl64k62blh5m8c4dq5l")))

(define-public crate-rsmemoryapi-0.0.3 (c (n "rsmemoryapi") (v "0.0.3") (d (list (d (n "rsmemoryapi") (r "^0.0.2") (d #t) (k 0)))) (h "154balzgx6ispsvc3l15sm0maakf697jc0a5mcplw796kclhbdc7")))

(define-public crate-rsmemoryapi-1.0.0 (c (n "rsmemoryapi") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("wincon"))) (d #t) (k 0)))) (h "1xc4qcjx845sqg9lqqca7b15ff5928zmxy0z609hi6k9w7x3sphp")))

(define-public crate-rsmemoryapi-1.0.1 (c (n "rsmemoryapi") (v "1.0.1") (h "0njrlzhb18wy75k7dmw81kzd5xnq8n669iv5d5rypy3p8r5k67z3")))

