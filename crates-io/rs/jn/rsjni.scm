(define-module (crates-io rs jn rsjni) #:use-module (crates-io))

(define-public crate-rsjni-0.1.0 (c (n "rsjni") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "187cpaxwx0fvsm1bq1d9lh3fwdbys1cc3qhiilff92r7r0cfkfqz") (f (quote (("seven") ("nine") ("eight") ("default" "nine"))))))

(define-public crate-rsjni-0.1.1 (c (n "rsjni") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "1dzdrdxsv6dxs5l74q3d1ccdh62k26pbv23lfm89hiz2icfi71wh") (f (quote (("seven") ("nine") ("eight") ("default" "nine"))))))

(define-public crate-rsjni-0.1.2 (c (n "rsjni") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "1yskr5hbkvqp7y2adwzvrjd1v6c6nrqim02f1qinbax6npi1ljwg") (f (quote (("seven") ("nine") ("eight") ("default" "nine"))))))

(define-public crate-rsjni-0.1.3 (c (n "rsjni") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "1ml6wpi1iz76nh71nbb2wvj08kgmysan3hmbbii1wsxpbfwif5h9") (f (quote (("seven") ("nine") ("eight") ("default" "nine"))))))

(define-public crate-rsjni-0.1.4 (c (n "rsjni") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "0pbwyhz6h8djf6v9psiw4j1dfls1nqg2nmkkl0vy06figsj9dni2") (f (quote (("seven") ("nine") ("eight") ("default" "nine"))))))

(define-public crate-rsjni-0.2.0 (c (n "rsjni") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "1szsdvswbypnc9c2z2fjkh5fhkmjq9cday929prg3izsz7haq4x5") (f (quote (("seven") ("nine") ("eight") ("default" "nine"))))))

(define-public crate-rsjni-0.3.0 (c (n "rsjni") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "0hr1h4p5lysw8081y7jwimkmc5rdqbrggrj38yz9w0gj8lq503j8") (f (quote (("seven") ("nine") ("eight") ("default" "nine"))))))

(define-public crate-rsjni-0.3.1 (c (n "rsjni") (v "0.3.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "1j66ikfh84v552zs4ykv7l2rvgqkqr0jd8pcq6pj5walxlzb8axd") (f (quote (("seven") ("nine") ("eight") ("default" "nine"))))))

(define-public crate-rsjni-0.3.2 (c (n "rsjni") (v "0.3.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "1qi9fmsb8llzly4ghjsgiv17ra630q2zwc3qkjmfn93jr5wynhmw") (f (quote (("seven") ("nine") ("eight") ("default" "nine"))))))

(define-public crate-rsjni-0.4.0 (c (n "rsjni") (v "0.4.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^0") (d #t) (k 2)))) (h "182p3jviai0acjbjkxkp42qcawc5w5368msc1pr8cx5lds9cnnl4") (f (quote (("ten") ("seven") ("nine") ("eight") ("default" "ten"))))))

