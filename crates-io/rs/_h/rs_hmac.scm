(define-module (crates-io rs _h rs_hmac) #:use-module (crates-io))

(define-public crate-rs_hmac-0.1.0 (c (n "rs_hmac") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "rs_hasher_ctx") (r "^0.1.0") (d #t) (k 0)))) (h "0qamrsvmnbql1qyykal51zffb9pa1fg3fiym3sx6vn6vzx6saqrf")))

(define-public crate-rs_hmac-0.1.1 (c (n "rs_hmac") (v "0.1.1") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)))) (h "0hvy3flqgb4a6cva8ns8y5953vcl0l0hwz5s7ajqqv9ffpiy6kk6")))

(define-public crate-rs_hmac-0.1.2 (c (n "rs_hmac") (v "0.1.2") (d (list (d (n "rs_hasher_ctx") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_hasher") (r "0.1.*") (d #t) (k 0)) (d (n "rs_internal_state") (r "0.1.*") (d #t) (k 0)))) (h "1n58jdfvx5ms8xxzda1lsdm9lfi7n5pcm63616610qhzjv55dd5b")))

