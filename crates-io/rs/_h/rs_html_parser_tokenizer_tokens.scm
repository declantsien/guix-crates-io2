(define-module (crates-io rs _h rs_html_parser_tokenizer_tokens) #:use-module (crates-io))

(define-public crate-rs_html_parser_tokenizer_tokens-0.0.2 (c (n "rs_html_parser_tokenizer_tokens") (v "0.0.2") (h "0xsn963iad8kb71az3vv5nifcqr9hh978k5iy9fisq1fh2rqczl1")))

(define-public crate-rs_html_parser_tokenizer_tokens-0.0.5 (c (n "rs_html_parser_tokenizer_tokens") (v "0.0.5") (h "1c0ddh85bm0fkr72krmysy1dfv15vrfr4y5680jy1fqvp4kzidlv")))

(define-public crate-rs_html_parser_tokenizer_tokens-0.0.6 (c (n "rs_html_parser_tokenizer_tokens") (v "0.0.6") (h "0abh9vhbdxf0ii7wzzybs8hqxy2yc01g309gfwc95a3876bnksaf")))

