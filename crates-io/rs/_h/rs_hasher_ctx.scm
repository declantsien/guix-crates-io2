(define-module (crates-io rs _h rs_hasher_ctx) #:use-module (crates-io))

(define-public crate-rs_hasher_ctx-0.1.0 (c (n "rs_hasher_ctx") (v "0.1.0") (d (list (d (n "internal_hasher") (r "^0.1.0") (d #t) (k 0)) (d (n "internal_state") (r "^0.1.0") (d #t) (k 0)) (d (n "n_bit_words") (r "^0.1.0") (d #t) (k 0)))) (h "1k4zw351js3xzqzwm7nq2sw00jdrgz1lfby6frfm7x4rwqm28bgx")))

(define-public crate-rs_hasher_ctx-0.1.1 (c (n "rs_hasher_ctx") (v "0.1.1") (d (list (d (n "rs_internal_hasher") (r "^0.1.1") (d #t) (k 0)) (d (n "rs_internal_state") (r "^0.1.1") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.1") (d #t) (k 0)))) (h "1vl4y930i1zlj0rz70wld933lmicqsccld5d5cy5dzj1fdy1fy7y")))

(define-public crate-rs_hasher_ctx-0.1.2 (c (n "rs_hasher_ctx") (v "0.1.2") (d (list (d (n "rs_internal_hasher") (r "^0.1.1") (d #t) (k 0)) (d (n "rs_internal_state") (r "^0.1.1") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.1") (d #t) (k 0)))) (h "157wc73dll3vrnx30ypf4bf1ym03y1r7wim4ymgwiy0hwfn6l5c6")))

(define-public crate-rs_hasher_ctx-0.1.3 (c (n "rs_hasher_ctx") (v "0.1.3") (d (list (d (n "rs_internal_hasher") (r "^0.1.1") (d #t) (k 0)) (d (n "rs_internal_state") (r "^0.1.1") (d #t) (k 0)) (d (n "rs_n_bit_words") (r "^0.1.1") (d #t) (k 0)))) (h "13p2cava6ixpavzl4f5qbw93vyz1jmj9sa2kc9ka4vr4qrfawica")))

