(define-module (crates-io rs _h rs_handstrength) #:use-module (crates-io))

(define-public crate-rs_handstrength-0.1.0 (c (n "rs_handstrength") (v "0.1.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "1q13hf3wrr7iw1j0ax7611lqargyqnfq80nwmbzwwpxm1q4nb9wa")))

(define-public crate-rs_handstrength-0.1.1 (c (n "rs_handstrength") (v "0.1.1") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "1135i9b0dmh36jcxylx3hk85c6x7qcx6dh96rcpck34sxibahiwl")))

(define-public crate-rs_handstrength-0.1.2 (c (n "rs_handstrength") (v "0.1.2") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "1pznw4n4086dyczy09ldp9xj3qm0apb8zj41vc07xnvar1fs6b03")))

(define-public crate-rs_handstrength-0.1.21 (c (n "rs_handstrength") (v "0.1.21") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "07jklnqjhyw112z06p4n9ssps9zrf4702sry3njf98j8bs6239n9") (y #t)))

(define-public crate-rs_handstrength-0.1.22 (c (n "rs_handstrength") (v "0.1.22") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "0591vwwjl565sl8fbiw0mj1qj7n0m3v9jg9pxc3rzgvgnz52zh4q") (y #t)))

(define-public crate-rs_handstrength-0.1.23 (c (n "rs_handstrength") (v "0.1.23") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "0jwdkw4haywc7rxgx65j07g0kk18k5d06v7vvk7630rw9npja0jw") (y #t)))

(define-public crate-rs_handstrength-0.1.3 (c (n "rs_handstrength") (v "0.1.3") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "10cbpz1y653icy5hymn55ihsqxlggxmx7861abq8fr9kywqzszv9")))

(define-public crate-rs_handstrength-0.1.4 (c (n "rs_handstrength") (v "0.1.4") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "1y2vy7j51c8hdf0inkjzf4xw5jp4sjf8aiv958xwv8cssvn1fyxs")))

(define-public crate-rs_handstrength-0.1.5 (c (n "rs_handstrength") (v "0.1.5") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "0cbj29h21sraja1pcpxa00bf9p5jx2av6yk5zaxk9rzdj4k69szk")))

(define-public crate-rs_handstrength-0.1.6 (c (n "rs_handstrength") (v "0.1.6") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "1b25vmf44iwp89x9982wy0yn7rzzcjdx5sak0x69i6cfrcahmn5p")))

(define-public crate-rs_handstrength-0.1.8 (c (n "rs_handstrength") (v "0.1.8") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "13xxjd0xcxqvhl0amy2br5835rlzgcg0rx3fg74hdj9xy4sqrc9g")))

(define-public crate-rs_handstrength-0.2.0 (c (n "rs_handstrength") (v "0.2.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "17v2dbl2ynylhd2kfgqci4f7anvw9q50xkib0gf2gfc22b0hfjdw")))

(define-public crate-rs_handstrength-0.3.0 (c (n "rs_handstrength") (v "0.3.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "09mav86d3ncz4jhv5b6y2m1ah5g8mjrny6bc014h3jrpspfv11i0")))

(define-public crate-rs_handstrength-0.4.0 (c (n "rs_handstrength") (v "0.4.0") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "18p9csgismhp3a1vaip8gdbym1y3d6w60mhv1blmyp6nc6w0pbky")))

(define-public crate-rs_handstrength-0.4.1 (c (n "rs_handstrength") (v "0.4.1") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "03i0a8vdrxsz7d6qj6zmi2hmxvcsvzy0ahvyw0k14fk8cdhx39s0")))

(define-public crate-rs_handstrength-0.4.3 (c (n "rs_handstrength") (v "0.4.3") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "10vqsg62xfs6nd4z9ag6apdsljry8qgrxlqsnmj5rv6fwzk3qica")))

