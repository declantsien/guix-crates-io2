(define-module (crates-io rs _h rs_headers) #:use-module (crates-io))

(define-public crate-rs_headers-0.1.0 (c (n "rs_headers") (v "0.1.0") (h "1x0hb54abzchb6jnjc4j6vj6cvzbfbp5i5ad0f1l08lfxg6g8498")))

(define-public crate-rs_headers-0.1.1 (c (n "rs_headers") (v "0.1.1") (h "1gi6xd97ncvr9cj3q7i2d99969h2a1qki3c7c2mbkqs2rc7035l8")))

(define-public crate-rs_headers-0.1.3 (c (n "rs_headers") (v "0.1.3") (h "1rvidy7y2cwnrgv4fi37l4xvfv66j712bxb89gilcwfvrq2ai4bd")))

