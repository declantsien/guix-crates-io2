(define-module (crates-io rs _h rs_html_parser_tokens) #:use-module (crates-io))

(define-public crate-rs_html_parser_tokens-0.0.5 (c (n "rs_html_parser_tokens") (v "0.0.5") (d (list (d (n "rs_html_parser_tokenizer_tokens") (r "^0.0.5") (d #t) (k 0)) (d (n "unicase") (r "^2.7.0") (d #t) (k 0)))) (h "1bbg3q0x94rvpi0rfy36p81rgi3ngp1bnqywr1xjr90fawa0d1fr")))

(define-public crate-rs_html_parser_tokens-0.0.6 (c (n "rs_html_parser_tokens") (v "0.0.6") (d (list (d (n "rs_html_parser_tokenizer_tokens") (r "^0.0.6") (d #t) (k 0)) (d (n "unicase") (r "^2.7.0") (d #t) (k 0)))) (h "0x1fjc81fvx8sm0s6lj09amgychf6lf04lbykqjq5razhkdmyjsv")))

(define-public crate-rs_html_parser_tokens-0.0.7 (c (n "rs_html_parser_tokens") (v "0.0.7") (d (list (d (n "rs_html_parser_tokenizer_tokens") (r "^0.0.6") (d #t) (k 0)) (d (n "unicase") (r "^2.7.0") (d #t) (k 0)) (d (n "unicase_collections") (r "^0.2.0") (d #t) (k 0)))) (h "1mr2yaqqna037zs6agk9akhmn8i4b7xn336v3y7w59jix03clq5f")))

(define-public crate-rs_html_parser_tokens-0.0.8 (c (n "rs_html_parser_tokens") (v "0.0.8") (d (list (d (n "rs_html_parser_tokenizer_tokens") (r "^0.0.6") (d #t) (k 0)) (d (n "unicase") (r "^2.7.0") (d #t) (k 0)) (d (n "unicase_collections") (r "^0.3.0") (d #t) (k 0)))) (h "1319yf97abdy0inzrdxi8d54l8wvwk1bcqw37kkzjx4dkjzdaihd")))

