(define-module (crates-io rs rc rsrc-rs) #:use-module (crates-io))

(define-public crate-rsrc-rs-0.1.0 (c (n "rsrc-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "winnow") (r "^0.4.4") (d #t) (k 0)))) (h "01f39kzgsp62awvxy3dgcryk3ba0vabxs7zxpybgvv4gfxvczk4v")))

(define-public crate-rsrc-rs-0.1.1 (c (n "rsrc-rs") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "winnow") (r "^0.4.4") (d #t) (k 0)))) (h "1h7rz2zmp3pymar40kh7aqn4ps90g75w5a7avj077qla76rfq7ih")))

