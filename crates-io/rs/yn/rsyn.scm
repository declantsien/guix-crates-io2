(define-module (crates-io rs yn rsyn) #:use-module (crates-io))

(define-public crate-rsyn-0.0.0 (c (n "rsyn") (v "0.0.0") (h "04iv3slg96lv4198phg36xkb0f217xj7fix6x9c125csn0pijj5v")))

(define-public crate-rsyn-0.0.1 (c (n "rsyn") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("wrap_help" "suggestions"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "unix_mode") (r "^0.1.1") (d #t) (k 0)))) (h "1r734gza5r98qmkb7ibic0j061w2iaj21f3a160yblgwv6yzlqrk")))

