(define-module (crates-io rs yn rsync-rdiff) #:use-module (crates-io))

(define-public crate-rsync-rdiff-0.1.1 (c (n "rsync-rdiff") (v "0.1.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 2)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)))) (h "1njyd6mb2dh64c629qhv5v0sp3f5mdl975dnimjw5m49nm23r3fj") (f (quote (("strong_hash")))) (y #t)))

(define-public crate-rsync-rdiff-0.1.2 (c (n "rsync-rdiff") (v "0.1.2") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 2)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)))) (h "1660v4ix8gjb4qzb0djq9sr81b1z6k0j42gmmwh74rq8f1pkk0hg") (f (quote (("strong_hash")))) (y #t)))

