(define-module (crates-io rs yn rsync) #:use-module (crates-io))

(define-public crate-rsync-0.1.0 (c (n "rsync") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "1ba7kmky27a2m08x67al7qnvvbinq32ab53sqwvkxavg089gv1jq")))

(define-public crate-rsync-0.1.1 (c (n "rsync") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "04dx7hr45d2xxd11g4m9azhzgq5pvkvs5rjvxq6qm5w72xsph8cq")))

(define-public crate-rsync-0.1.2 (c (n "rsync") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)))) (h "1yya3bcb2a13i6b96a98li2ravgy8n5d4q7zrk79pi31wfafjnri")))

