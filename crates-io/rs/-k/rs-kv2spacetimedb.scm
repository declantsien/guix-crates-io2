(define-module (crates-io rs -k rs-kv2spacetimedb) #:use-module (crates-io))

(define-public crate-rs-kv2spacetimedb-1.0.1 (c (n "rs-kv2spacetimedb") (v "1.0.1") (h "1v71dkhi2hfalxz9a759xslj4qch9n9wsbiwr5s6zl9c0w4ashpr")))

(define-public crate-rs-kv2spacetimedb-1.1.0 (c (n "rs-kv2spacetimedb") (v "1.1.0") (h "05yvljb4ixwi8q3mx5hzbpal2341qh7h8d13r655yyv73s5v4c8d")))

(define-public crate-rs-kv2spacetimedb-1.2.0 (c (n "rs-kv2spacetimedb") (v "1.2.0") (h "0yw97wfdd37yarspknvix60v8mvrv7232cqwzfv58wvv7nj60g7w")))

(define-public crate-rs-kv2spacetimedb-1.3.0 (c (n "rs-kv2spacetimedb") (v "1.3.0") (h "0s56023i8d86d0lpyxr3h8k9q62w3z5l9df9al2xhls3ls3z7iv5")))

(define-public crate-rs-kv2spacetimedb-1.4.0 (c (n "rs-kv2spacetimedb") (v "1.4.0") (h "1alnn0c6dhsbvg6r97p286h13njhs51gmig44aqd055yik0awp4d")))

(define-public crate-rs-kv2spacetimedb-1.5.0 (c (n "rs-kv2spacetimedb") (v "1.5.0") (h "0r2klznvdsj5kxf858hv0zfzfxk8sqjlyf93mp0mzjy092mzw238")))

(define-public crate-rs-kv2spacetimedb-1.6.0 (c (n "rs-kv2spacetimedb") (v "1.6.0") (h "0qcv54q4bc7wmjy4v1mvnf2afix5rkzsmdn71nciq6dwn3x77ydr")))

(define-public crate-rs-kv2spacetimedb-1.7.0 (c (n "rs-kv2spacetimedb") (v "1.7.0") (h "1snzgn8gqh54mwjq6jzadwsc5382cxi0jmv17y8rkz20k0xcv5ly")))

(define-public crate-rs-kv2spacetimedb-1.8.0 (c (n "rs-kv2spacetimedb") (v "1.8.0") (h "18ibch53n9xza6w7j8gcp98d9fdihfxc6axzzjqd2n66yssv3nbn")))

(define-public crate-rs-kv2spacetimedb-1.9.0 (c (n "rs-kv2spacetimedb") (v "1.9.0") (h "0wwxhspc2ljxzhsic3ia6w1klyak0nd1arkw5na5iza6kgaba4dy")))

(define-public crate-rs-kv2spacetimedb-1.10.0 (c (n "rs-kv2spacetimedb") (v "1.10.0") (h "0vkql9ji8qmg3r6czgrv0awpm0bcc7bbxm4hb8md0r2p5r9n2jzf")))

(define-public crate-rs-kv2spacetimedb-1.11.0 (c (n "rs-kv2spacetimedb") (v "1.11.0") (h "1cpffnlxxpdix4v7f4jsrvb8f05n78p69qp18gsi3ym82f7yj340")))

(define-public crate-rs-kv2spacetimedb-1.12.0 (c (n "rs-kv2spacetimedb") (v "1.12.0") (h "0hj7x2wh6486vmcsp6hgz6wis5f7i7w2mxgdbigg4x4jafl0hjhs")))

(define-public crate-rs-kv2spacetimedb-1.13.0 (c (n "rs-kv2spacetimedb") (v "1.13.0") (h "1jcp5ggg3gbfwxalkv5x2h7919pszrz8wl9r88jfwhgjja13sijg")))

(define-public crate-rs-kv2spacetimedb-1.14.0 (c (n "rs-kv2spacetimedb") (v "1.14.0") (h "1j3zz8f09fqkw349gnynqz9jjrnbsyxigdx6s3aaig1nm60p4hzb")))

(define-public crate-rs-kv2spacetimedb-2.0.0 (c (n "rs-kv2spacetimedb") (v "2.0.0") (h "1a59m218d99n5p2crzxi5ip11ing3c5k14kp4g0zidxdds56iaz6")))

(define-public crate-rs-kv2spacetimedb-2.1.0 (c (n "rs-kv2spacetimedb") (v "2.1.0") (h "15811z8h57b9qjgv0lnsnf99pn6grpirz2c0snkzlb1i6ckpvz77")))

(define-public crate-rs-kv2spacetimedb-2.2.0 (c (n "rs-kv2spacetimedb") (v "2.2.0") (h "0r5jnpz87xndfsqp7i6aa07bl7r7s9rwxpx47q5hmy9i97sn47ap")))

(define-public crate-rs-kv2spacetimedb-2.3.0 (c (n "rs-kv2spacetimedb") (v "2.3.0") (h "1877zi70fh2mkxisfwygh34rfal8l1zbf1528zxqdfc9jvg2snxl")))

(define-public crate-rs-kv2spacetimedb-2.4.0 (c (n "rs-kv2spacetimedb") (v "2.4.0") (h "1km9rky60p8fjxrhv51wrpxkvaw7nw822jq1p6ispysgynajid65")))

(define-public crate-rs-kv2spacetimedb-2.5.0 (c (n "rs-kv2spacetimedb") (v "2.5.0") (h "09a7a3llx8x8lgvhhqc29r3ccc4gvyp1j4j9673i6317xj0qa52s")))

(define-public crate-rs-kv2spacetimedb-2.6.0 (c (n "rs-kv2spacetimedb") (v "2.6.0") (h "0py3gwkc9z1mjsgf8k6qs9nc73vpvpsbbwm4i9kbzfvlh6rki49p")))

(define-public crate-rs-kv2spacetimedb-2.7.0 (c (n "rs-kv2spacetimedb") (v "2.7.0") (h "1yk5aq7xyys8ndj0npmx0arq4ha9xf31ryswkwg8glci6wr8mfvh")))

(define-public crate-rs-kv2spacetimedb-2.8.0 (c (n "rs-kv2spacetimedb") (v "2.8.0") (h "0b4ramwlzsycdgk0qz6fxqisgl0if6hnij65i66s76x5hp0i1k4s")))

(define-public crate-rs-kv2spacetimedb-2.8.1 (c (n "rs-kv2spacetimedb") (v "2.8.1") (h "00ask6aim19xr16cj74y8qhf1xz72vg1q1crx5a842b77vmbx5ld")))

(define-public crate-rs-kv2spacetimedb-2.8.2 (c (n "rs-kv2spacetimedb") (v "2.8.2") (h "0y6cwnvn3ymmcinkwskwn7p7zigh9fnmypqvd67gzb2sqdylnwjk")))

(define-public crate-rs-kv2spacetimedb-2.9.0 (c (n "rs-kv2spacetimedb") (v "2.9.0") (h "121q595xw6r5pfwl0r7b784l12skyihfgbc4yp6fllc60qs49dyq")))

(define-public crate-rs-kv2spacetimedb-2.9.1 (c (n "rs-kv2spacetimedb") (v "2.9.1") (h "1vwipsgmqk3q9ss9qnifhsllrwgn28423l3ga058wg8pjpd48zlw")))

