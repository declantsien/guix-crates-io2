(define-module (crates-io rs -c rs-complete) #:use-module (crates-io))

(define-public crate-rs-complete-0.1.0 (c (n "rs-complete") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)))) (h "10ll9z075ppcihx5xa13cpcxk62l2ghfmksg9f8145zcx7z6waw5")))

(define-public crate-rs-complete-0.1.1 (c (n "rs-complete") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)))) (h "1kjdv0lprwwbxwq52ig9d8ivdq17jrcmrwxrsgzy21jnk1f6y6fb")))

(define-public crate-rs-complete-0.2.0 (c (n "rs-complete") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)))) (h "1404shmbprscmql0bw6zxb15l2giaa63rch14cqkchg00g2pixb2")))

(define-public crate-rs-complete-1.0.0 (c (n "rs-complete") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)))) (h "1y8r5pnr0miydvgwxb3clwglfb82s7m29x49dav1kx4srq5fcn8n")))

(define-public crate-rs-complete-1.1.0 (c (n "rs-complete") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)))) (h "0pf31fmf06f8vz6k81yxasv68l6lxygkfq4cvvc62ywamwhn7q0j")))

(define-public crate-rs-complete-1.1.1 (c (n "rs-complete") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)))) (h "05mp6hqf1dzf0g0x4pzicls97l2z00jz9zdnmipx5p1lvy63h3ga")))

(define-public crate-rs-complete-1.2.0 (c (n "rs-complete") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)))) (h "17jxyxpr565binkm4rd0l13fk0wrhbwnlpcf228mj5fg39a5msc4")))

(define-public crate-rs-complete-1.2.1 (c (n "rs-complete") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)))) (h "0b5by8p002wyvllfs6hyq686dnv2nb1fdglsa03064kg551gd8nn")))

(define-public crate-rs-complete-1.3.0 (c (n "rs-complete") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)))) (h "1kdc7i8x1bs802yn6b061qap1m97smsicd51xkmimg6gwhh59p54")))

(define-public crate-rs-complete-1.3.1 (c (n "rs-complete") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "0z64klz2mw048723s77wy5bssnksaf7nq4i0vi5cv08m2dadin8m")))

