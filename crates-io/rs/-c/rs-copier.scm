(define-module (crates-io rs -c rs-copier) #:use-module (crates-io))

(define-public crate-rs-copier-0.1.0 (c (n "rs-copier") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bcahksn0pzp4sxhvn79kp36ay6cyb9h3gabhcmb5ndxns0w4q0v")))

