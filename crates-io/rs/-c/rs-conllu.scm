(define-module (crates-io rs -c rs-conllu) #:use-module (crates-io))

(define-public crate-rs-conllu-0.1.0 (c (n "rs-conllu") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1brbx8z9mw8n0mzq7rbad0500fnh42dbpvdchxgfa753n0zgkn57")))

