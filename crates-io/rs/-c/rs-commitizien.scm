(define-module (crates-io rs -c rs-commitizien) #:use-module (crates-io))

(define-public crate-rs-commitizien-0.1.0 (c (n "rs-commitizien") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "00qnrwxnaibar44kx7ywkmgv5zz6yn6ljcxynn34489ci17bvqd6")))

(define-public crate-rs-commitizien-0.1.1 (c (n "rs-commitizien") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1pi37v89qfc886hznxyivhnma840jfmz3lrxjyrb6y3kkmayzb6x") (y #t)))

(define-public crate-rs-commitizien-0.1.2 (c (n "rs-commitizien") (v "0.1.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1h9zzm22kmfxvvxv2fbs0x9r0sj46h8s55pp624c4gyydh6lnj75")))

