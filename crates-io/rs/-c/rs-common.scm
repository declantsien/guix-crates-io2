(define-module (crates-io rs -c rs-common) #:use-module (crates-io))

(define-public crate-rs-common-0.1.0 (c (n "rs-common") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "logs") (r "^0.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0blj8cxk157fvvhrrd4ml2vdwxpx2gr6rlsqcvkk2xpwb8rhjd4r")))

