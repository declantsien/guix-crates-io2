(define-module (crates-io rs -c rs-cjy) #:use-module (crates-io))

(define-public crate-rs-cjy-0.1.0 (c (n "rs-cjy") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.2") (d #t) (k 0)))) (h "0k5yjiblp2h6p9d44wgb2aq3irrdnqjkwq3z7vvwsyb0wa2djncy")))

(define-public crate-rs-cjy-0.1.2 (c (n "rs-cjy") (v "0.1.2") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.2") (d #t) (k 0)))) (h "0hxxlhdpd81j2yhmdbcgqh6gphiiqzjziqr6jg5m1xda8s1lrvf4")))

(define-public crate-rs-cjy-0.1.3 (c (n "rs-cjy") (v "0.1.3") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "05jr96709qn59p0688698f7rn48xwk76bq1k2nr6807m888yzb9i")))

(define-public crate-rs-cjy-0.1.4 (c (n "rs-cjy") (v "0.1.4") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0hhnhng0j428rshsk8r2shvyj6ssxfzpbzs6xv6xi3mbwkbyb28d")))

(define-public crate-rs-cjy-0.1.5 (c (n "rs-cjy") (v "0.1.5") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1bx8z1gh3az6fwjg0r1zgazlwrsxwgi2ljr6mpp00mx77rw539wd")))

