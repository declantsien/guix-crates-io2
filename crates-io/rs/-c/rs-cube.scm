(define-module (crates-io rs -c rs-cube) #:use-module (crates-io))

(define-public crate-rs-cube-1.0.0 (c (n "rs-cube") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1v209y87gjc9n9ckdkz5wqss9g2llhs0vcf3hymrkq3cmwgcrzak")))

(define-public crate-rs-cube-1.0.1 (c (n "rs-cube") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kmnkqp52mnqi36sz42sf6hbr872g4wrzkafby775idi1iqakj3n")))

(define-public crate-rs-cube-1.0.2 (c (n "rs-cube") (v "1.0.2") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05l7ga1jmjarb12iklnb01m0fc3f327z84df4hjvw8cg82bv2f9z")))

