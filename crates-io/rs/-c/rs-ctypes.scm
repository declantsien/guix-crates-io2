(define-module (crates-io rs -c rs-ctypes) #:use-module (crates-io))

(define-public crate-rs-ctypes-0.1.0 (c (n "rs-ctypes") (v "0.1.0") (h "0pkp3y2b5vjx5ylqgkk35xns21dqrdja072kwdavgfjsllgz8w9a")))

(define-public crate-rs-ctypes-0.1.1 (c (n "rs-ctypes") (v "0.1.1") (h "17gsc31hyjqvynl07ziy6y8a0gpbgdlc24w17ymqsf1y9vn4ydnd")))

