(define-module (crates-io rs -c rs-com) #:use-module (crates-io))

(define-public crate-rs-com-0.1.0 (c (n "rs-com") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "09q2cpisznxs98w8az0vk14iws233bq1gzb3w4y6mh4v2whr9d43")))

(define-public crate-rs-com-0.2.0 (c (n "rs-com") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "04z7z33g8xmvpk0zq942p4sjda9px3aq6r8994qb3xrqnl8j6cf1")))

(define-public crate-rs-com-0.3.0 (c (n "rs-com") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1929jhi04kl4y06ly0fb3i7a13xxdb24dbnn8b2zvdvy175zh18k")))

(define-public crate-rs-com-0.4.0 (c (n "rs-com") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1zcrd6qb47rlm02gazl0jw6bjavfdzqz9vrak9n7b397dx75cnin")))

