(define-module (crates-io rs -c rs-collections) #:use-module (crates-io))

(define-public crate-rs-collections-0.1.0 (c (n "rs-collections") (v "0.1.0") (d (list (d (n "rs-mem") (r "^0.2.0") (d #t) (k 0)))) (h "0flkkmb7bsj98nrgbxp8yfzmgd0ir3ish575p318mvwqfkd0994b")))

(define-public crate-rs-collections-0.1.1 (c (n "rs-collections") (v "0.1.1") (d (list (d (n "rs-mem") (r "^0.2.1") (d #t) (k 0)))) (h "1gnxpszcip4mgannvp0kw7lcq7dfb9q0079v0isl3y8i65gs8x25")))

(define-public crate-rs-collections-0.1.2 (c (n "rs-collections") (v "0.1.2") (d (list (d (n "rs-mem") (r "^0.2.1") (d #t) (k 0)))) (h "0z0fpg3g10dyd2brk8pc3jv41fbvvcg51y73j7wnjqza34zk65xq")))

(define-public crate-rs-collections-0.1.3 (c (n "rs-collections") (v "0.1.3") (d (list (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "15ry8qn1x7jj2c85p4bmwjqd4pmxpicavqjpq24qn4ia7c0d3brd")))

(define-public crate-rs-collections-0.1.4 (c (n "rs-collections") (v "0.1.4") (d (list (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "1gwjssw3pvn9xnnx8a31dlrna7bacmcr3r9vp7w8l590hzzk3a44")))

(define-public crate-rs-collections-0.1.5 (c (n "rs-collections") (v "0.1.5") (d (list (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "0xy37dmyqmxxn9yjdg0am47w82qx549f31szn26j05kkx9cbd91h")))

(define-public crate-rs-collections-0.1.6 (c (n "rs-collections") (v "0.1.6") (d (list (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "1vphrhxa24v9np8jpldx8v0jx9as9scnjwqal0gj9fswkav24fcm")))

(define-public crate-rs-collections-0.1.7 (c (n "rs-collections") (v "0.1.7") (d (list (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "0mxfnxcfl0h5bbg0n4bqvl1dhwaf9vp4mfjmpljx0y5ssbw3kdzn") (y #t)))

(define-public crate-rs-collections-0.1.8 (c (n "rs-collections") (v "0.1.8") (d (list (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "0sp5kywnbkbqzj7cdd0ra146y5sh53r6lxnaarmxj8d5m43y7kpm")))

(define-public crate-rs-collections-0.1.9 (c (n "rs-collections") (v "0.1.9") (d (list (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "0x5xi9ppyb1di9r21pr2b6qbqjlplxdh7xqdfbazf86s3xr3p9dx")))

