(define-module (crates-io rs -c rs-category) #:use-module (crates-io))

(define-public crate-rs-category-0.1.0 (c (n "rs-category") (v "0.1.0") (d (list (d (n "void") (r "^1") (k 0)))) (h "0d6789zi003i6v8zf5xvysjxyba51wqsi38gjxmj3mnmcyc4lp8h")))

(define-public crate-rs-category-0.1.1 (c (n "rs-category") (v "0.1.1") (d (list (d (n "void") (r "^1") (k 0)))) (h "0k3a9j8v3x2p7g11l92ngd0jhzgh4kfa1yjllprd0cykr1sx8dnp")))

