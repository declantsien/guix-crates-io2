(define-module (crates-io rs -c rs-cord) #:use-module (crates-io))

(define-public crate-rs-cord-0.0.0 (c (n "rs-cord") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "stream"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.16") (f (quote ("connect"))) (k 0)))) (h "17niyph1sncrxdi9zh67isd1f37hv4gcplg7c51mjaaixcsklj72")))

