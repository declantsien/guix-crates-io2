(define-module (crates-io rs -c rs-crisp-status-reporter) #:use-module (crates-io))

(define-public crate-rs-crisp-status-reporter-1.0.0 (c (n "rs-crisp-status-reporter") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5") (d #t) (k 0)))) (h "18zmgd75x80b7151rghab0g01wdzswfqj1kfrgyfw5nm1ikb1zxq") (y #t)))

