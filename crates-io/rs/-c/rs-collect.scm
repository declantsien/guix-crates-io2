(define-module (crates-io rs -c rs-collect) #:use-module (crates-io))

(define-public crate-rs-collect-0.1.0 (c (n "rs-collect") (v "0.1.0") (h "19xkszvwnvcxwjkhngcylw81w73ynrb0zi2v902vh11y0kbaissj")))

(define-public crate-rs-collect-0.1.1 (c (n "rs-collect") (v "0.1.1") (h "1zy436jaqbfbqr5ijyx3lsvdr8r28i83jrqy3akqxnf7ck5xf1sh")))

