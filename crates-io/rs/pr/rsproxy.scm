(define-module (crates-io rs pr rsproxy) #:use-module (crates-io))

(define-public crate-rsproxy-0.1.0 (c (n "rsproxy") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "013pq87768wr2rhahdgbbpsbpyn1pmdrq4p3y04sf02ygbqbzxra")))

(define-public crate-rsproxy-0.1.1 (c (n "rsproxy") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0ffgmqfy3i5d66i469ja5cas3002f5ims7jkzcs31gays41r9nxa")))

