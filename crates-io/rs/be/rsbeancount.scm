(define-module (crates-io rs be rsbeancount) #:use-module (crates-io))

(define-public crate-rsbeancount-0.1.0 (c (n "rsbeancount") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "1q2dl36vjyppzp20f5jhzw1mvn5g6ikkk9n2vhzickczj3wdjjfb")))

(define-public crate-rsbeancount-0.1.1 (c (n "rsbeancount") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "0wygfc1m08qzrgiz4a5qi6nhdq4whyrc059748z3ih5y6jgzy3bl")))

