(define-module (crates-io rs df rsdfind) #:use-module (crates-io))

(define-public crate-rsdfind-0.1.1 (c (n "rsdfind") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1x5apx0pmhr6mgscgyw01f2fwrsh2zzxghzqb7gn0iqb9ln5ras6")))

(define-public crate-rsdfind-0.2.0 (c (n "rsdfind") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0mdsnqqhzc0gnfyy3h2wa763hfk4jlff6lz21y10kjbn425cwvfa")))

