(define-module (crates-io rs hc rshc) #:use-module (crates-io))

(define-public crate-rshc-0.1.0 (c (n "rshc") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "149lc0rnczpz74aqkpn6v0s97qqs4a8y9np1s0bfn4sk2hxx1fwv") (y #t)))

(define-public crate-rshc-0.1.1 (c (n "rshc") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1idxwvl6lw0h8rj3jl8vgbsd0jgny0gi5g0mprndnzfc8r3rlyhg")))

(define-public crate-rshc-0.1.2 (c (n "rshc") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0748zd93ag15g6zicry0jn4zab7kcwk4z4wcm1d4pk0ry2f3kgjg")))

(define-public crate-rshc-0.1.3 (c (n "rshc") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0s2zqdya0pq7l2zzqha250dhij36qq6l9s6j1dpib3lq4ihdhdlj")))

