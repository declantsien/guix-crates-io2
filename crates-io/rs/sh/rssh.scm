(define-module (crates-io rs sh rssh) #:use-module (crates-io))

(define-public crate-rssh-0.1.0 (c (n "rssh") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.1") (d #t) (k 0)))) (h "1d69mg5zyjv89xrxd6a8fjj742qp9rzljrikl9qhf3whkn30s66r")))

