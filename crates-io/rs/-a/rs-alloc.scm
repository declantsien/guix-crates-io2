(define-module (crates-io rs -a rs-alloc) #:use-module (crates-io))

(define-public crate-rs-alloc-0.0.1 (c (n "rs-alloc") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "rs-ctypes") (r "^0.1.1") (d #t) (k 0)))) (h "05viwafhp5zr3hfni5pxh5fzlz31x166kfaw8j36clslil149x07")))

