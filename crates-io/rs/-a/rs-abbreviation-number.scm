(define-module (crates-io rs -a rs-abbreviation-number) #:use-module (crates-io))

(define-public crate-rs-abbreviation-number-0.1.0 (c (n "rs-abbreviation-number") (v "0.1.0") (h "1zyk1www1sr1hmbz3kjry25w71zmh0cp7xancm7jzb2g562pg8cj")))

(define-public crate-rs-abbreviation-number-0.1.1 (c (n "rs-abbreviation-number") (v "0.1.1") (h "1vx0njcwzsnmgqr0p6wybx5jcqxngg4159rw8ivrn816ympz58zp")))

(define-public crate-rs-abbreviation-number-0.2.1 (c (n "rs-abbreviation-number") (v "0.2.1") (h "096c139x76rwpr362b9vmgrdy67ann2dy77iy27d8p0ypq3jxbvm")))

(define-public crate-rs-abbreviation-number-0.3.1 (c (n "rs-abbreviation-number") (v "0.3.1") (h "04lm488j280bzvl2nm2a9i7d2vq2snb1mmsjncgccj5m170zfi5x")))

