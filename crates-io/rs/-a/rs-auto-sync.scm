(define-module (crates-io rs -a rs-auto-sync) #:use-module (crates-io))

(define-public crate-rs-auto-sync-0.1.0 (c (n "rs-auto-sync") (v "0.1.0") (d (list (d (n "fsevent") (r "^0.2.11") (d #t) (k 0)))) (h "0y3jkk0z90q0hbk0pynacf58ccxapykcd0y128b2hhg2smbaw6q4")))

(define-public crate-rs-auto-sync-0.1.1 (c (n "rs-auto-sync") (v "0.1.1") (d (list (d (n "fsevent") (r "^0.2.11") (d #t) (k 0)))) (h "1n45l3s5cm2d3zcx9bvmg89abxk1v5virnm17wvxz99502hplzxh")))

(define-public crate-rs-auto-sync-0.1.2 (c (n "rs-auto-sync") (v "0.1.2") (d (list (d (n "fsevent") (r "^0.2.11") (d #t) (k 0)))) (h "104n0k6q99if4j6j5x6jh6zmh84w54xyfkrxkhmhxjv48nn60kq4")))

(define-public crate-rs-auto-sync-0.1.3 (c (n "rs-auto-sync") (v "0.1.3") (d (list (d (n "fsevent") (r "^0.2.11") (d #t) (k 0)))) (h "0yxrwy9hbhdfhnqnib0r25ik626kd87887rcn1gis8rhd0zszbpj")))

(define-public crate-rs-auto-sync-0.1.4 (c (n "rs-auto-sync") (v "0.1.4") (d (list (d (n "fsevent") (r "^0.2.11") (d #t) (k 0)))) (h "1fq1pyzk9pm3y4vaxw8v5d60van6qw55qv3niawvv3ic8wxgn5i1")))

(define-public crate-rs-auto-sync-0.1.5 (c (n "rs-auto-sync") (v "0.1.5") (d (list (d (n "fsevent") (r "^0.2.11") (d #t) (k 0)))) (h "050y2zsczydy7k7mzga79dgyf54pp4nwxaz9858vvs16qd0kr4cw")))

(define-public crate-rs-auto-sync-0.1.6 (c (n "rs-auto-sync") (v "0.1.6") (d (list (d (n "fsevent") (r "^0.2.11") (d #t) (k 0)))) (h "1ss38vl9wczdn0vs3vvgmyz7i4zvd8qva3xk7g6nck3kjy40nhza")))

(define-public crate-rs-auto-sync-0.1.7 (c (n "rs-auto-sync") (v "0.1.7") (d (list (d (n "fsevent") (r "^0.2.12") (d #t) (k 0)))) (h "1cx534afdh3x5vqlws7nw98vx26y1mws1psqqlihxj1lb5jkjrvi")))

