(define-module (crates-io rs -a rs-args) #:use-module (crates-io))

(define-public crate-rs-args-0.0.1 (c (n "rs-args") (v "0.0.1") (h "1225cdfmjvhwgl2awqwbminh6b0lcvn237x11qjgqamj6hz2crmi") (y #t)))

(define-public crate-rs-args-0.1.1 (c (n "rs-args") (v "0.1.1") (h "0x8sp7dm5indiczc1nsc0b97j22ms31bgnk0shfy9ndcgk2hjwid") (y #t)))

(define-public crate-rs-args-0.1.2 (c (n "rs-args") (v "0.1.2") (h "1inh8n07v6c2l1h5c876s3xnr1s3hn8nj0rbwksm5hajm5br2nmx")))

(define-public crate-rs-args-0.1.3 (c (n "rs-args") (v "0.1.3") (h "025alca11m4bhxrxl4iggl1whlfrwv0g2bwlbcqdf1zkn4ijhq3b")))

(define-public crate-rs-args-0.2.0 (c (n "rs-args") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "07l0fdwvb46d47wfav78yv05rs8pqciwy8cca5svcz9654l1jrx1")))

(define-public crate-rs-args-0.2.1 (c (n "rs-args") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "1bhjd7xsmvk2f715jv88kjnqcbjjn6c6rapijq71qcz1kay3qnzs")))

(define-public crate-rs-args-0.2.2 (c (n "rs-args") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "1fn7fx6smpg9b7ysqghfimhmpvd11swa58y8c5nkb6bk71l58bcs")))

(define-public crate-rs-args-0.3.0 (c (n "rs-args") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "176w5cndp2xw68i4zi7km9k3jb7q7gi8khmvdwqlr2sfnva2z7vq")))

(define-public crate-rs-args-0.3.1 (c (n "rs-args") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "1kzlsyfsjnlwd3kdrgv3avk2w4fk10jiwldmfrq091pl05cqhxlh")))

