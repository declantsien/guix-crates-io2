(define-module (crates-io rs ub rsubs) #:use-module (crates-io))

(define-public crate-rsubs-0.1.0 (c (n "rsubs") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "rsubs-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0666hinkcl57df8ni6jah3m0hbivvq6hr53izmdzdnjbk274sar3") (y #t)))

