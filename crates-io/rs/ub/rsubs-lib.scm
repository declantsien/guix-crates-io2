(define-module (crates-io rs ub rsubs-lib) #:use-module (crates-io))

(define-public crate-rsubs-lib-0.1.0 (c (n "rsubs-lib") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "08bkflgrqkq8rkmv3nnwbmj8cqzmplmk3cm8b1vs65hiq610lk2k") (y #t)))

(define-public crate-rsubs-lib-0.1.1 (c (n "rsubs-lib") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1dd8yl9z4ahx37jslafw09izphzsly2xacfy25msgjrsg914jknh") (y #t)))

(define-public crate-rsubs-lib-0.1.2 (c (n "rsubs-lib") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1mg2y2pq4dg70swasiarhcrzbx7lka3gxgjx385f1a6p2mashzgy") (y #t)))

(define-public crate-rsubs-lib-0.1.3 (c (n "rsubs-lib") (v "0.1.3") (d (list (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1yr6zaw7l3p7fk80ml82dx6afiw03xlxzb0vy0wfh0ds79ln400w") (y #t)))

(define-public crate-rsubs-lib-0.1.4 (c (n "rsubs-lib") (v "0.1.4") (d (list (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0ganc9al230xm9ii6mvs2w5g2yls1zqa7aqzlv3h5qg0lwrqm3zl") (y #t)))

(define-public crate-rsubs-lib-0.1.5 (c (n "rsubs-lib") (v "0.1.5") (d (list (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qglf13d8npjbnlc4lf25q98c3zzi27pp4212bylmc7pv3k4dz3x") (y #t)))

(define-public crate-rsubs-lib-0.1.6 (c (n "rsubs-lib") (v "0.1.6") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hc121z55wr2w6b2zz6hiz02vn9njy7hv2x728lmsv73wwai6lhj")))

(define-public crate-rsubs-lib-0.1.7 (c (n "rsubs-lib") (v "0.1.7") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18yx2i6y1f54qa623xyfk553ccxjjl3fdh927020y5m4z8hvj9ii")))

(define-public crate-rsubs-lib-0.1.8 (c (n "rsubs-lib") (v "0.1.8") (d (list (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rcj1apb2a900jcg5yp60jxybp99vibhdq79150930f4w7f58y4m")))

(define-public crate-rsubs-lib-0.1.9 (c (n "rsubs-lib") (v "0.1.9") (d (list (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04vy1qc88d7nv9j5lnsg75x0mii8gji9sjmyrxl79w01ak4b92mz")))

(define-public crate-rsubs-lib-0.1.10 (c (n "rsubs-lib") (v "0.1.10") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cs68lx3f2a82pay6q2v73jjhrp9gz1j3l0m5pnrz80xwhbsx65d")))

(define-public crate-rsubs-lib-0.2.0 (c (n "rsubs-lib") (v "0.2.0") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i8wnkk37lqpil1dk66bd4wkzy3a53p3dirpnwr42nk30nd5bxqd")))

(define-public crate-rsubs-lib-0.2.1 (c (n "rsubs-lib") (v "0.2.1") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qnzpfazs3p1j88a3izjq6sd8wv6wikkgg4mizg0bjhgasls5k4x")))

(define-public crate-rsubs-lib-0.3.0 (c (n "rsubs-lib") (v "0.3.0") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros" "parsing" "serde"))) (d #t) (k 0)))) (h "1vpdxc1nscv3143rir71swq598d8xqmbsrxavc3n9isb31zilgpl")))

(define-public crate-rsubs-lib-0.3.1 (c (n "rsubs-lib") (v "0.3.1") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros" "parsing" "serde"))) (d #t) (k 0)))) (h "00mlk80csx3rp0a7wyga7b21rmy7j8bqsbim9gi4zg5iy04pc7yh")))

