(define-module (crates-io rs ub rsubox) #:use-module (crates-io))

(define-public crate-rsubox-0.1.0 (c (n "rsubox") (v "0.1.0") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0g95vw5xnwzgsk4134padc74bzmgpviz8si0yps3z5x339ziph16")))

(define-public crate-rsubox-0.1.1 (c (n "rsubox") (v "0.1.1") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "1wfa0j8b53l562pas3lyp8302dppf8kzd8a7sqgmjix8qs7v6049")))

(define-public crate-rsubox-0.1.2 (c (n "rsubox") (v "0.1.2") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "19s0f6bq40cjxjjvjnd1kkr0y5idcjiqimmhcpp4b1z5093mr0rz")))

(define-public crate-rsubox-0.1.3 (c (n "rsubox") (v "0.1.3") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0lf9ia60njc8wqgisdj2qmjx6lh2g2449sckxshib509myxv6hp3")))

(define-public crate-rsubox-0.1.4 (c (n "rsubox") (v "0.1.4") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0mhknfblzxgbpw596jzhaxcbvw2jy4pmb3lzs3xv2l65bqn5ypiq")))

(define-public crate-rsubox-0.2.0 (c (n "rsubox") (v "0.2.0") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "1xd4jqfgwyzk5433ihkwwx5izvm3p1q3p3wcw50zqm3w45kk5jnm")))

(define-public crate-rsubox-0.2.1 (c (n "rsubox") (v "0.2.1") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "fnmatch-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0nd66732fi3d9c1w0agip0d05gs2dynl7psn5c7iw4iww3i5njbq")))

