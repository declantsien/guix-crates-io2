(define-module (crates-io rs al rsalgo) #:use-module (crates-io))

(define-public crate-rsalgo-0.0.1-init (c (n "rsalgo") (v "0.0.1-init") (h "0gp6vm61rrpv9yxzfbr93vwklhlvn49asz53gfn26scscck6jnnw") (y #t)))

(define-public crate-rsalgo-0.0.1-init.1 (c (n "rsalgo") (v "0.0.1-init.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0nn2qhsirkc30r40fb761r4vss4kpvd87kgv2bn783dn7vcn7sp9")))

