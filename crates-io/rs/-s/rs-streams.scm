(define-module (crates-io rs -s rs-streams) #:use-module (crates-io))

(define-public crate-rs-streams-0.1.0 (c (n "rs-streams") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "rs-collections") (r "^0.1.2") (d #t) (k 0)) (d (n "rs-mem") (r "^0.2.1") (d #t) (k 0)))) (h "0jdb7hi196abbd9jq0ljqbpi61h10akxaby1wyqjbh5f0vz25lb2")))

(define-public crate-rs-streams-0.1.1 (c (n "rs-streams") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "rs-collections") (r "^0.1.5") (d #t) (k 0)) (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "11lfapk7zpks9zzj3r1z8cqpahhhb7q3db11cwkzblvlzdh7yb2l")))

(define-public crate-rs-streams-0.1.2 (c (n "rs-streams") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "rs-collections") (r "^0.1.6") (d #t) (k 0)) (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "15whykxp89x12xzsig0rh3iwvvvnrpv7s2k1rif3m3yg01qdyc88")))

(define-public crate-rs-streams-0.1.4 (c (n "rs-streams") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "rs-collections") (r "^0.1.9") (d #t) (k 0)) (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "1lg17cyr6k6r62p9k7wmmfn30rpyh4h70ikc0jqiqlllb7my9vr9") (y #t)))

(define-public crate-rs-streams-0.1.5 (c (n "rs-streams") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "rs-collections") (r "^0.1.9") (d #t) (k 0)) (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "0828fhcnlvdry84mgcca1ci1va42fr1kgrmsc0l511jldw326rs1")))

(define-public crate-rs-streams-0.1.6 (c (n "rs-streams") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "rs-collections") (r "^0.1.9") (d #t) (k 0)) (d (n "rs-mem") (r "^0.2.2") (d #t) (k 0)))) (h "18gphyv59x3hasamzgsv7fraz762v8cqwgq70412kykwrmji5b9y")))

(define-public crate-rs-streams-0.1.7 (c (n "rs-streams") (v "0.1.7") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "rs-alloc") (r "^0.0.1") (d #t) (k 0)))) (h "18g03haz52f6kinn9r136lq1nrbg61vk964c7djz3znzb4j29sca")))

(define-public crate-rs-streams-0.1.8 (c (n "rs-streams") (v "0.1.8") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "rs-alloc") (r "^0.0.1") (d #t) (k 0)))) (h "09r8478s5plx56amvzn7k71dlkhxvbfzzlvv3k4nafw0zibb050z")))

