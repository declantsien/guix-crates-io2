(define-module (crates-io rs -s rs-store) #:use-module (crates-io))

(define-public crate-rs-store-0.1.0 (c (n "rs-store") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0g8whsixlrlh8479vqkv1rrq651xbyp4lw6rjl965i03ip27l7md")))

(define-public crate-rs-store-0.2.1 (c (n "rs-store") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0y0rlvjl0fv3hhrlcjbm2mcx4wiwsnbg0dng15ay3niq19bk0i7d")))

(define-public crate-rs-store-0.3.1 (c (n "rs-store") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0p97isr2vj07n3lavlh3c7yakx3inrr1fjpsza3h5kckm5xzqx1z")))

(define-public crate-rs-store-0.4.1 (c (n "rs-store") (v "0.4.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1g8ywcfiin77a5dqizvw1z5kwcqxvzwnkplrjp6vhmhjvhs1sngl")))

(define-public crate-rs-store-0.5.1 (c (n "rs-store") (v "0.5.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "03f2mgbh25shyii1zmjn2cwi72hyj2f9m9vdb4da1xi6nj2jdmx1")))

