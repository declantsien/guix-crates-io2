(define-module (crates-io rs -s rs-secp256k1) #:use-module (crates-io))

(define-public crate-rs-secp256k1-0.5.7 (c (n "rs-secp256k1") (v "0.5.7") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0g9cyllnv1mv833aliqpplaz0r4k3a4xs9fid93dqg5b7z4lrcp6") (f (quote (("unstable") ("dev" "clippy") ("default"))))))

