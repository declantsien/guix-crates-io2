(define-module (crates-io rs -s rs-simple-logging) #:use-module (crates-io))

(define-public crate-rs-simple-logging-0.2.0 (c (n "rs-simple-logging") (v "0.2.0") (h "0njykshfz1ic5hqp74rhi25ymcgkqjfschn5p9iv8cl7w6i2sdqp")))

(define-public crate-rs-simple-logging-0.3.0 (c (n "rs-simple-logging") (v "0.3.0") (h "0rf0ibiqabkhpx4vcgq2mm3wc8zn1vkqwyk28md7vf2d8k2cqsb8")))

