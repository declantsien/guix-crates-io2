(define-module (crates-io rs -s rs-snowflake) #:use-module (crates-io))

(define-public crate-rs-snowflake-0.1.0 (c (n "rs-snowflake") (v "0.1.0") (h "0b3461b6d47ppl1k08mjmc7xy5r3gkn7nax7ygb2r9nz0bp2mrfc")))

(define-public crate-rs-snowflake-0.2.0 (c (n "rs-snowflake") (v "0.2.0") (h "0l0cmxcd03m9iw35ibmlb15s790fdrkasrqg5m4riyjffcnjx6ic")))

(define-public crate-rs-snowflake-0.3.0 (c (n "rs-snowflake") (v "0.3.0") (h "0cx6342p4r2qzg51r7j6ns1ggrl93d48zs6ff4pzh4j8dbv153lv")))

(define-public crate-rs-snowflake-0.4.0 (c (n "rs-snowflake") (v "0.4.0") (h "1lpd173irqpn1gjvspayn4xk370dhslsgpkndjm9q5jf5xiq0gzw")))

(define-public crate-rs-snowflake-0.5.0 (c (n "rs-snowflake") (v "0.5.0") (h "07m248bniid24154m9x624ahg7yfgdcjfjxgmrcrkddbpblqn573")))

(define-public crate-rs-snowflake-0.6.0 (c (n "rs-snowflake") (v "0.6.0") (h "0rkn29l5qhcdcy2qj5208k89xd54vjm9hk8k9sz2nw4l56wg63p6")))

