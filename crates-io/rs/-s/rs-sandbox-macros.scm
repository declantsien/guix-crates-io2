(define-module (crates-io rs -s rs-sandbox-macros) #:use-module (crates-io))

(define-public crate-rs-sandbox-macros-0.1.0 (c (n "rs-sandbox-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0s8rdis5dqasr3ffivlsg746m04fvw6x4iysgfi5ih1kf3z89c1g")))

(define-public crate-rs-sandbox-macros-0.1.1 (c (n "rs-sandbox-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1acxv20wgkab0fxlckz2l2fjd4k6p87lnfs05hac9a1jy4crzsny")))

(define-public crate-rs-sandbox-macros-0.1.2 (c (n "rs-sandbox-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1m4sb4bl1pwhyxais4jjd41m9cf8b1jqa4858hx80jxm3h11f722")))

(define-public crate-rs-sandbox-macros-0.1.3 (c (n "rs-sandbox-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1d3w0j1gg0bb3031vrbfzv9zzf6dzd45859nw7i64pmnxciv3yi9")))

