(define-module (crates-io rs -s rs-sandbox-core) #:use-module (crates-io))

(define-public crate-rs-sandbox-core-0.1.0 (c (n "rs-sandbox-core") (v "0.1.0") (d (list (d (n "bson") (r "^2.3.0") (f (quote ("chrono-0_4" "serde_with"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "scsys") (r "^0.1.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1dvc939glg5jgbl5p5945v35ml91gmv9ipvjrwnaklsl844ggpcl")))

