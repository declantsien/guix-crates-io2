(define-module (crates-io rs -s rs-sb3) #:use-module (crates-io))

(define-public crate-rs-sb3-0.1.0 (c (n "rs-sb3") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "02k8s47ykl6wzlggjc1hw3jwvnn5wd7srhwnn5kmk68b57d5ya3v") (y #t)))

(define-public crate-rs-sb3-0.1.1 (c (n "rs-sb3") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "0fzr8ldabnpb3x022z70yf11dz8gkmwyr62rvcpga06gr9hkv8wg")))

(define-public crate-rs-sb3-0.1.2 (c (n "rs-sb3") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "0dziifb6q0fclva1zdhdly7dzz85sbvyzbr7nlcpdv2cl41y95wa")))

(define-public crate-rs-sb3-0.2.0 (c (n "rs-sb3") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "1mmcm97m9jqd5gp714vp7z09hrv23rzq4472ixfgqcavbjvndqbm")))

(define-public crate-rs-sb3-0.3.0 (c (n "rs-sb3") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "16n23ka2br5l29n9pj9ixm8nalzx47z47q1aklzxjhh7sr25qc0s")))

(define-public crate-rs-sb3-0.3.1 (c (n "rs-sb3") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "0r7bcfwd9fpizrvdc610wgxv0dm78rlhi0q6wcq7k8wnf4ccnk6l")))

(define-public crate-rs-sb3-0.4.0 (c (n "rs-sb3") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "0y2yhcnq6p7lh42yaqrsi869vbsn63am60znggpv2srng8dbk0ry")))

(define-public crate-rs-sb3-0.5.0 (c (n "rs-sb3") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "0m5nxy0ci1pmjk1s0xzn3s2gm0gly9vffzm1pa4m3gwf6d8c3b62")))

(define-public crate-rs-sb3-0.6.0 (c (n "rs-sb3") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)))) (h "03sqqr0l1lwdwc26iz6633isdz0s3r4zh69q9m9hyq6la6y4vpsm")))

