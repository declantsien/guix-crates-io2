(define-module (crates-io rs -s rs-sandbox-derive) #:use-module (crates-io))

(define-public crate-rs-sandbox-derive-0.1.0 (c (n "rs-sandbox-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0d47b08snygff7lyp4gc00pd6nvcpcs37w5az3ghbwc8i0fxliyz")))

(define-public crate-rs-sandbox-derive-0.1.1 (c (n "rs-sandbox-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1yfik17zw8yapl2njd0x5yr3rf4j42djfrh1r61j3s68rz239q41")))

(define-public crate-rs-sandbox-derive-0.1.2 (c (n "rs-sandbox-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1s23s4ki1jwgfzmzs23zfhr1cvikdh3d67jmgy3p2sd269z0x394")))

(define-public crate-rs-sandbox-derive-0.1.3 (c (n "rs-sandbox-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1bsf7p28ngq4b4v2lcwhhcka4pd56fzfc5i6n9zdjha8pj20zmr6")))

