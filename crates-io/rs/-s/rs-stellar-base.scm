(define-module (crates-io rs -s rs-stellar-base) #:use-module (crates-io))

(define-public crate-rs-stellar-base-0.1.0 (c (n "rs-stellar-base") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "nacl") (r "^0.5.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.7") (d #t) (k 0)) (d (n "tweetnacl") (r "^0.4.0") (d #t) (k 0)))) (h "1xggqm2kmyjb96j76h1qn5ivakbavxq4ycdhd7ai2p9p9m1fq0li")))

