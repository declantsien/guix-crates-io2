(define-module (crates-io rs -s rs-std-ext) #:use-module (crates-io))

(define-public crate-rs-std-ext-0.1.0 (c (n "rs-std-ext") (v "0.1.0") (d (list (d (n "num") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1kwcxhhah0n40ia2pnp3qiphx8n8habbqbdv8002kqf2b3l11qwd") (f (quote (("long-tuple-impl") ("default" "crate-num") ("crate-num" "num"))))))

(define-public crate-rs-std-ext-0.2.0 (c (n "rs-std-ext") (v "0.2.0") (d (list (d (n "num") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1p9ggk7zpk1ijnjd3fmq2w691sgawid9ar0bf6n0y9wpzi17kp6g") (f (quote (("long-tuple-impl") ("default" "crate-num") ("crate-num" "num"))))))

