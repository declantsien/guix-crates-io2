(define-module (crates-io rs -s rs-sasl) #:use-module (crates-io))

(define-public crate-rs-sasl-0.2.0 (c (n "rs-sasl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0dx4q98zwz0xblb1wvxp2z1gg03q82khxvfc5fa8gg9dch1afxsp")))

(define-public crate-rs-sasl-0.2.1 (c (n "rs-sasl") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0i1sfv2269sl7p70rhzxy1yayyiz0im4x8w35wj2aq7akrhzkgi3")))

(define-public crate-rs-sasl-0.3.0 (c (n "rs-sasl") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0p51s9j853n1k975x1s9i9wljgzq7ycvqh438pfi87gzl3yavgs6")))

(define-public crate-rs-sasl-0.4.0 (c (n "rs-sasl") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zr5qzr2d6ynj29lc1gcjcsb9hy3mvv5lz83d9d0iavz51cqfszx")))

