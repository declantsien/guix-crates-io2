(define-module (crates-io rs id rsid3) #:use-module (crates-io))

(define-public crate-rsid3-1.0.0-alpha (c (n "rsid3") (v "1.0.0-alpha") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.80") (d #t) (k 1)) (d (n "id3") (r "^1.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "vergen") (r "^8.3.1") (f (quote ("build" "git" "gitcl"))) (d #t) (k 1)))) (h "0vxmfndvvm7d6iwbvagz4yr0isihznylagl4hi1m7rcqg5jihplw")))

