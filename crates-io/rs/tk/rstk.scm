(define-module (crates-io rs tk rstk) #:use-module (crates-io))

(define-public crate-rstk-0.1.0 (c (n "rstk") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0hklw1qyw8s5z0c6sd7zxi2bbkhgvxkdgksfka4wni40fpv387gh")))

(define-public crate-rstk-0.2.0 (c (n "rstk") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1ja0cljx9mmd8as39gf3rp2fpq7i43bxyymfp0hkfhslr8z3688x")))

(define-public crate-rstk-0.3.0 (c (n "rstk") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "01r85pf3cvr6wdha55v5ly84y0391yhf50s67a8gm72wwgi4pp8a")))

