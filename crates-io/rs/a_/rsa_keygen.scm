(define-module (crates-io rs a_ rsa_keygen) #:use-module (crates-io))

(define-public crate-rsa_keygen-0.1.0 (c (n "rsa_keygen") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "0m7z26nacy22iq29rl68wb5dfl7ir6gjjb7ybcv39lcj10qmlf3f") (y #t)))

(define-public crate-rsa_keygen-0.1.1 (c (n "rsa_keygen") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "0dgn1xyhjrmbq89mfvy61p257ims9cm24yz1py5v5ycj68c230bj") (y #t)))

(define-public crate-rsa_keygen-1.0.0 (c (n "rsa_keygen") (v "1.0.0") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "0m05r8hzi90na36r6398hvv87bqb6424rxfkkj8w18rdm60krv4l") (y #t)))

(define-public crate-rsa_keygen-1.0.1 (c (n "rsa_keygen") (v "1.0.1") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "0l258bcv4n39q1bdy868iwdmlgisn42f3dryx4vnnjrjrh28f8yb") (y #t)))

(define-public crate-rsa_keygen-1.0.2 (c (n "rsa_keygen") (v "1.0.2") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "0dcm5sksnmpk6lnnvag63qf4dhhmaha0hhl1zy5h47g1mb1bgqla") (y #t)))

(define-public crate-rsa_keygen-1.0.3 (c (n "rsa_keygen") (v "1.0.3") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "0v0hprv7c1qmvb7jscbqmwgvpqhdcdsm1vp4c1hh7cazxs0896ga") (y #t)))

(define-public crate-rsa_keygen-1.0.4 (c (n "rsa_keygen") (v "1.0.4") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "0hf90w32p88ncs1k66qxnwj17v4sfwnslp0qgnwqyg0ch8pzvfbr") (y #t)))

(define-public crate-rsa_keygen-1.0.5 (c (n "rsa_keygen") (v "1.0.5") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "0n9wc9cdq3l0h4psdzrpiznb5ph6lmk3fr4ckx82zjyd6ad6xcqc") (y #t)))

(define-public crate-rsa_keygen-1.0.6 (c (n "rsa_keygen") (v "1.0.6") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "01bps9bqh6fpfhnm63vz7x1j0gw12mg784krp5lxcs8x8s0bk7f4") (y #t)))

(define-public crate-rsa_keygen-1.0.7 (c (n "rsa_keygen") (v "1.0.7") (d (list (d (n "arrayref") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "1qch5z9qnn6vmzg496xdk5nf3ck1kwg2p7jlh48fcmfvr4zl41hf")))

