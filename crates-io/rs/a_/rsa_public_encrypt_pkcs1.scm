(define-module (crates-io rs a_ rsa_public_encrypt_pkcs1) #:use-module (crates-io))

(define-public crate-rsa_public_encrypt_pkcs1-0.1.0 (c (n "rsa_public_encrypt_pkcs1") (v "0.1.0") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.1.0") (d #t) (k 0)))) (h "1scwn8mfyl7h04z7f39kw0lzsxzd4s1b2aa45qlcrxcpl3qlnba1")))

(define-public crate-rsa_public_encrypt_pkcs1-0.2.0 (c (n "rsa_public_encrypt_pkcs1") (v "0.2.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.2.0") (d #t) (k 0)))) (h "1vj5g2bknvla4arcrp8gsqypxr776hpgvbl0av1188qd8i9j6an8")))

(define-public crate-rsa_public_encrypt_pkcs1-0.3.0 (c (n "rsa_public_encrypt_pkcs1") (v "0.3.0") (d (list (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.5.0") (d #t) (k 0)))) (h "1vyaj678n879s26j0cbhb99ss67xyrjvgb87hbfc5l03r9bzbib7")))

(define-public crate-rsa_public_encrypt_pkcs1-0.4.0 (c (n "rsa_public_encrypt_pkcs1") (v "0.4.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.5.4") (d #t) (k 0)))) (h "1x7b43d7d6ha9zp99c81a98ic506rh1f37f06majqccb3wx29sdk")))

