(define-module (crates-io rs a_ rsa_xml) #:use-module (crates-io))

(define-public crate-rsa_xml-0.1.0 (c (n "rsa_xml") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.40") (d #t) (k 0)) (d (n "rsa") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "059w0p2phqjrz4cb9vmp9q6y4k6a3k361qi48vxqlb252333v3vw")))

