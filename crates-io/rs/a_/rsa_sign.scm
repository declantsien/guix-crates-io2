(define-module (crates-io rs a_ rsa_sign) #:use-module (crates-io))

(define-public crate-rsa_sign-0.1.0 (c (n "rsa_sign") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rsa") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1qbpz7lq20nffxd3qisgvnhnhmq7gz7l49c0y9ddgm8mn5wg7qjq") (y #t)))

(define-public crate-rsa_sign-0.1.1 (c (n "rsa_sign") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rsa") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0sgzig5q6xxy7nhls9lssgwi4h1xjcibrh3zlv99h3w2y51j40lc")))

(define-public crate-rsa_sign-0.1.2 (c (n "rsa_sign") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rsa") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1zvcmkwrgz04lrddgig9571hdx1jif8gd7cc475iinbs2p8461ny")))

