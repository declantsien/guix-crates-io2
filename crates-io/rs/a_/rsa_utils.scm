(define-module (crates-io rs a_ rsa_utils) #:use-module (crates-io))

(define-public crate-rsa_utils-0.1.0 (c (n "rsa_utils") (v "0.1.0") (d (list (d (n "rsa_rs") (r "^0.1.5") (d #t) (k 0)))) (h "0c2l3vzzpc9vj3mik8z88ksb1v7f6dpslwjxqlzfrvr8bzm8fd4p")))

(define-public crate-rsa_utils-0.2.0 (c (n "rsa_utils") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "rsa_rs") (r "^0.2.0") (d #t) (k 0)))) (h "1wkj16wbix0v8pvj1w54gdyga4nmnqa4cnn860rnarmz3ss62cb7")))

