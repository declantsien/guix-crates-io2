(define-module (crates-io rs ev rsevents) #:use-module (crates-io))

(define-public crate-rsevents-0.1.0 (c (n "rsevents") (v "0.1.0") (d (list (d (n "parking_lot_core") (r "^0.3") (d #t) (k 0)))) (h "1f3233vdfklma0r5hhby1c67w9rp1pmflqcfj1hx8nbg5ajgx80h")))

(define-public crate-rsevents-0.1.1 (c (n "rsevents") (v "0.1.1") (d (list (d (n "parking_lot_core") (r "^0.3") (d #t) (k 0)))) (h "0mcbm0mk95660n7a0jfd167bzlmypfg64l5ky09g82k9hqarxic6")))

(define-public crate-rsevents-0.2.0 (c (n "rsevents") (v "0.2.0") (d (list (d (n "parking_lot_core") (r "^0.3") (d #t) (k 0)))) (h "1m7mv9fw5cl8yyb00jp76qs498sairky8wp6cy46y2xvr0wa9l4w")))

(define-public crate-rsevents-0.2.1 (c (n "rsevents") (v "0.2.1") (d (list (d (n "parking_lot_core") (r "^0.7") (d #t) (k 0)))) (h "0cy9p3byq22hw82kqswk8m3r60jqqcd0c2895sk5ddwhvmxq809y")))

(define-public crate-rsevents-0.3.0 (c (n "rsevents") (v "0.3.0") (d (list (d (n "parking_lot_core") (r "^0.9.3") (d #t) (k 0)) (d (n "tiny-rng") (r "^0.2.0") (d #t) (k 2)))) (h "0szlxxys3w4a90zf35pfrnpw7nbcgqdg1v8fvv9kwbvv05ifjq22") (y #t)))

(define-public crate-rsevents-0.3.1 (c (n "rsevents") (v "0.3.1") (d (list (d (n "parking_lot_core") (r "^0.9.3") (d #t) (k 0)) (d (n "tiny-rng") (r "^0.2.0") (d #t) (k 2)))) (h "0lf6n05ymzs47w3pym6z2lj3ib3s6f16v1vdw1c4s6d0zmlrgqp8")))

