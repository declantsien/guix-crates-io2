(define-module (crates-io rs ev rsevents-extra) #:use-module (crates-io))

(define-public crate-rsevents-extra-0.1.0 (c (n "rsevents-extra") (v "0.1.0") (d (list (d (n "rsevents") (r "^0.2") (d #t) (k 0)))) (h "0llh0325m31pxw2287y3ldqhnpd900hwm9bk4vsc1qvmm1hwrklz")))

(define-public crate-rsevents-extra-0.2.0 (c (n "rsevents-extra") (v "0.2.0") (d (list (d (n "rsevents") (r "^0.3.1") (d #t) (k 0)))) (h "1kfz3sq25i1lfvhq16g3lqm4wfak11dknplcl0ax8dnmj65cyq67")))

(define-public crate-rsevents-extra-0.2.1 (c (n "rsevents-extra") (v "0.2.1") (d (list (d (n "rsevents") (r "^0.3.1") (d #t) (k 0)))) (h "0qv8qi1gg9h90cf23xasgf2amah46c168knb284f028kjm4cnmsx")))

(define-public crate-rsevents-extra-0.2.2 (c (n "rsevents-extra") (v "0.2.2") (d (list (d (n "rsevents") (r "^0.3.1") (d #t) (k 0)))) (h "1r1nx447m20n7kgnbrjgxaf9sd2xzlvpm0xlwh1ki22i0y2lxxlg")))

