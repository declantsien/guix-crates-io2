(define-module (crates-io rs ev rsevmasm) #:use-module (crates-io))

(define-public crate-rsevmasm-0.1.0 (c (n "rsevmasm") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1cc4diqm4x4qx2ikjccvpmqxhc4ry71zz3w9y4zafw61psk0x7br")))

(define-public crate-rsevmasm-0.2.0 (c (n "rsevmasm") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1xxjalz23daqq4c0hij1iknqg64f6s0vqfd9yyyz77k0mf5n82i5")))

(define-public crate-rsevmasm-0.2.1 (c (n "rsevmasm") (v "0.2.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1zvbvh5rnqk1xlwp73bbz2zmnlm11j7inqjbgs3nlhnhp48b42f2")))

(define-public crate-rsevmasm-0.2.2 (c (n "rsevmasm") (v "0.2.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "0p8dq611wbld6ng7q1af0c10rxnw2fhm9bgb33gy8z5h6b0ar9pl")))

(define-public crate-rsevmasm-0.3.2 (c (n "rsevmasm") (v "0.3.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "0jqq5dgm1n7vym08r2rf1giz2a8rsk8zvbvj4pyzadmbcampbwqw")))

(define-public crate-rsevmasm-0.4.0 (c (n "rsevmasm") (v "0.4.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "123k82q8h1wjvcjqaj1ipawzbkvxq5vknaifzvbqncxan4nxlq15")))

(define-public crate-rsevmasm-0.4.1 (c (n "rsevmasm") (v "0.4.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0vs9ihzw55zdv0qaq9rm3jmw6i1lqmi0fm22xp94idgklhqarw82")))

(define-public crate-rsevmasm-0.4.2 (c (n "rsevmasm") (v "0.4.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1c98vfqp6ihmcb0g5yyc4hbhwrl3s4cwzy7lvz22z39r0icdcbdk")))

(define-public crate-rsevmasm-0.5.0 (c (n "rsevmasm") (v "0.5.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1b87vr3578ngs4ikn8ky8hjsm48q60awndmd2zjnpg77bqmqg7p4")))

