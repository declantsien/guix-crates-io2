(define-module (crates-io rs s- rss-wallpaper) #:use-module (crates-io))

(define-public crate-rss-wallpaper-0.1.0 (c (n "rss-wallpaper") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (d #t) (k 0)) (d (n "rss") (r "^2.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wallpaper") (r "^3.2.0") (f (quote ("from_url"))) (d #t) (k 0)))) (h "09437ccyknj4mcax6hi51cs5krzhld9kwws835sn0r3jsa5aqpbr")))

