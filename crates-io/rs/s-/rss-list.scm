(define-module (crates-io rs s- rss-list) #:use-module (crates-io))

(define-public crate-rss-list-0.1.0 (c (n "rss-list") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rss") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hjvmvjwmgkgm9z5gfrnzj820wi306a0j93xr15navqpc7vpn81h")))

