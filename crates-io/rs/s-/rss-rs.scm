(define-module (crates-io rs s- rss-rs) #:use-module (crates-io))

(define-public crate-rss-rs-0.1.0 (c (n "rss-rs") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (o #t) (d #t) (k 0)))) (h "08wm7z7qvndvwj12ddb738vvb4p33liay9lmhyphjni0m4f0rwvr") (f (quote (("default" "xml-rs"))))))

(define-public crate-rss-rs-0.2.0 (c (n "rss-rs") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.3") (d #t) (k 0)))) (h "0gr5h6jscsxrbl83l2934fcpx4djy8i3lm40f1waxryklw2kqk3c")))

(define-public crate-rss-rs-0.3.0 (c (n "rss-rs") (v "0.3.0") (d (list (d (n "quick-xml") (r "^0.4") (d #t) (k 0)))) (h "020sd33rj44ibsi8x04b21j7wiq314yf7dyzj8ppkqsw6bm1vg3q")))

(define-public crate-rss-rs-0.3.1 (c (n "rss-rs") (v "0.3.1") (d (list (d (n "quick-xml") (r "^0.4") (d #t) (k 0)))) (h "1w3fknam9dfbn0fl4p4nc2rv6fcbnk1v4rnf6yb8xsgvyava4xgb")))

(define-public crate-rss-rs-0.3.2 (c (n "rss-rs") (v "0.3.2") (d (list (d (n "quick-xml") (r "^0.4") (d #t) (k 0)))) (h "09hg4jwjdhy28i7p5m8xk9brab619n2gxa8vi7rjh8vb1ahn3v7k")))

