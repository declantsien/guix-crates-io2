(define-module (crates-io xd c_ xdc_macros) #:use-module (crates-io))

(define-public crate-xdc_macros-0.1.0 (c (n "xdc_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yz3c27n92mhwcap9fv5zl8f4ir215iw52wg2wg7gdannx0jc72y") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

