(define-module (crates-io xd vd xdvdfs-cli) #:use-module (crates-io))

(define-public crate-xdvdfs-cli-0.1.0 (c (n "xdvdfs-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (k 0)) (d (n "xdvdfs") (r "^0.1.0") (d #t) (k 0)))) (h "1fc4n78wkpi8q451vn2vz48y8xdf5nh5gfs6iwwcf185fmdpsb84")))

(define-public crate-xdvdfs-cli-0.2.0 (c (n "xdvdfs-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (k 0)) (d (n "xdvdfs") (r "^0.2.0") (d #t) (k 0)))) (h "008zrgz1pr2k710l64ca5ibjknjbq2v28qzph0bs87n6whhmb7b4")))

(define-public crate-xdvdfs-cli-0.3.0 (c (n "xdvdfs-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (k 0)) (d (n "xdvdfs") (r "^0.3.0") (d #t) (k 0)))) (h "1qfy915x65sx6k3vpgaiarm2cykmr44b39xv0rlwp4rcvda0bgcf") (y #t)))

(define-public crate-xdvdfs-cli-0.4.0 (c (n "xdvdfs-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (k 0)) (d (n "xdvdfs") (r "^0.4.0") (d #t) (k 0)))) (h "1mgj4gycjbm62asgn6bqzlz60mhzs5bsj5svyqf5pgw7qcrzfj24")))

(define-public crate-xdvdfs-cli-0.5.0 (c (n "xdvdfs-cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (k 0)) (d (n "xdvdfs") (r "^0.5.0") (d #t) (k 0)))) (h "00vq0pqjxadkpxkkfbinyjcdhz26ldjvrqkpyjpga69wb1af3bd6")))

(define-public crate-xdvdfs-cli-0.5.1 (c (n "xdvdfs-cli") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (k 0)) (d (n "xdvdfs") (r "^0.5.1") (d #t) (k 0)))) (h "0nnrhqlbkj45arwjv126ai5y6g59lfxfb1qgnpdfdp6rxfm4650s")))

(define-public crate-xdvdfs-cli-0.5.2 (c (n "xdvdfs-cli") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "maybe-async") (r "^0.2.7") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (k 0)) (d (n "xdvdfs") (r "^0.5.2") (d #t) (k 0)))) (h "00mg8i89fb4f9yrqbi71grd66a3i6xl70ad2mmpnr2bl5dbazh0s") (f (quote (("sync" "xdvdfs/sync"))))))

(define-public crate-xdvdfs-cli-0.6.0 (c (n "xdvdfs-cli") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "ciso") (r "^0.1.0") (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "maybe-async") (r "^0.2.7") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (k 0)) (d (n "xdvdfs") (r "^0.6.0") (d #t) (k 0)))) (h "1yvgrdab5h5rryy667yy2sa19qgzihy2g6l5acrq17g30y46hfmc") (f (quote (("sync" "xdvdfs/sync" "maybe-async/is_sync"))))))

(define-public crate-xdvdfs-cli-0.7.0 (c (n "xdvdfs-cli") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "ciso") (r "^0.2.0") (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "maybe-async") (r "^0.2.7") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (k 0)) (d (n "xdvdfs") (r "^0.7.0") (d #t) (k 0)))) (h "03p08573bixyfb2wxhzip10z3lvszwmhn0c6vgcm785iadi9ygl2") (f (quote (("sync" "xdvdfs/sync" "maybe-async/is_sync"))))))

