(define-module (crates-io xd r_ xdr_extras) #:use-module (crates-io))

(define-public crate-xdr_extras-0.1.0 (c (n "xdr_extras") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "xdr_extras_derive") (r "^0.1") (d #t) (k 0)))) (h "1g9iaj1rcicd634is0m2ghv8dhvhi0lhvssr832x0anyh89mjbf0")))

