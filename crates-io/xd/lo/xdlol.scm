(define-module (crates-io xd lo xdlol) #:use-module (crates-io))

(define-public crate-xdlol-0.0.0 (c (n "xdlol") (v "0.0.0") (d (list (d (n "clippy") (r "^0.0.169") (o #t) (d #t) (k 0)) (d (n "float-cmp") (r "^0.4") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "statrs") (r "^0.9") (d #t) (k 0)))) (h "02xalkyhhkqnv4p9fks0784rggzcad0qi3avn649zamk7jnp8b49")))

