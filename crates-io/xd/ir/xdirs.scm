(define-module (crates-io xd ir xdirs) #:use-module (crates-io))

(define-public crate-xdirs-0.1.0 (c (n "xdirs") (v "0.1.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0x6akk7di4vjj0b0g97wx9nh9dxvnjdhd96fi79irwgapdhiys20")))

(define-public crate-xdirs-0.1.1 (c (n "xdirs") (v "0.1.1") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders"))) (d #t) (t "cfg(windows)") (k 0)))) (h "08ajay2cxwgvzshx1v2ig27pzw780969i7x9r8lb8szxgnrpr4x0")))

