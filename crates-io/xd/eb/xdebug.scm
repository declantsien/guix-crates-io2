(define-module (crates-io xd eb xdebug) #:use-module (crates-io))

(define-public crate-xdebug-0.1.0 (c (n "xdebug") (v "0.1.0") (h "1a9l5jsxbchwjkbc98rkrqv17873qkdlz0mbxhcr581gn73v8d7q")))

(define-public crate-xdebug-0.1.1 (c (n "xdebug") (v "0.1.1") (h "1yinpzpyw3aj4dmg1y4dd975pw5l03pd8zvpg4kaf9vvaxm39z9i")))

(define-public crate-xdebug-0.1.2 (c (n "xdebug") (v "0.1.2") (h "0nqz9p2jnnmis2idlcq8wj6aygi90jqlhasaizkg89zx1r2ykjzk")))

(define-public crate-xdebug-0.1.3 (c (n "xdebug") (v "0.1.3") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jqf7mnfxcibppfcf585s3znxzjjwqplrq37bx65dwc2imi4s4fi")))

(define-public crate-xdebug-0.2.0 (c (n "xdebug") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bi1iprl7qxriif24g66v364f3y8q1qqgfnq45dpzpg7fm7i2ps5")))

(define-public crate-xdebug-0.2.1 (c (n "xdebug") (v "0.2.1") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ggp8lxb2vqdpxvryd43gfvxdr6zjj305l1b62d4fyq8hyahf4in")))

(define-public crate-xdebug-0.3.0 (c (n "xdebug") (v "0.3.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize" "overlapped-lists"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nrm44cbfp3x4i8v3axfda702jh2f2q2c28g3yb9c75sbsifliad")))

(define-public crate-xdebug-0.3.1 (c (n "xdebug") (v "0.3.1") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize" "overlapped-lists"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "08az63xdndbsgg89w3mfnliiw3qkd2jk2pmj285f75sjzvz3cldy")))

(define-public crate-xdebug-0.4.0 (c (n "xdebug") (v "0.4.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize" "overlapped-lists"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hralplisp4xsqghlb1b9yaxxnxzd4d7b2mni0xacjnf1n6m5sq9")))

(define-public crate-xdebug-0.4.1 (c (n "xdebug") (v "0.4.1") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize" "overlapped-lists"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0swkbv7hhkpcw0r50njv93dq7k6hqmc5mnw0i50js7wd8krnla62")))

