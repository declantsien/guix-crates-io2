(define-module (crates-io xd rf xdrfile) #:use-module (crates-io))

(define-public crate-xdrfile-0.1.0 (c (n "xdrfile") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "07jm0ymzgchqmpd7n8ndr68vg3hk15x4vsm3kc7dbj2nk9ygjpjr")))

(define-public crate-xdrfile-0.2.0 (c (n "xdrfile") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1c4sfr05xxg20d9hhg8gkndp07kd2f2nlpd953farg9x5225fy2a")))

(define-public crate-xdrfile-0.3.0 (c (n "xdrfile") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0ksvk083var54n8xfc1r0i309h6md96q27rh5sn58bb0f3sra5dz")))

