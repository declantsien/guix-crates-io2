(define-module (crates-io xd rk xdrk) #:use-module (crates-io))

(define-public crate-xdrk-1.0.0 (c (n "xdrk") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fcm7rs8izs2magx6h8mj17k8i1z7h70an9fk5wvrv14c9cc6xzn")))

