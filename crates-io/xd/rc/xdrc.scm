(define-module (crates-io xd rc xdrc) #:use-module (crates-io))

(define-public crate-xdrc-0.1.0 (c (n "xdrc") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10qlwiznidrjadnxlncyr9w3wgcadx1iappx276xki7038vabnza")))

