(define-module (crates-io xd rg xdrgen) #:use-module (crates-io))

(define-public crate-xdrgen-0.1.0 (c (n "xdrgen") (v "0.1.0") (d (list (d (n "peg") (r "^0.3.2") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.0") (d #t) (k 0)))) (h "06kcal8a8l3yz4fnqh9p3jb0han8ssg9vwczyn8aw3n5g7k5l6dw")))

(define-public crate-xdrgen-0.1.1 (c (n "xdrgen") (v "0.1.1") (d (list (d (n "peg") (r "^0.3.2") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.1") (d #t) (k 0)))) (h "0hqcfjnx1kpnfnnkgjjn31fg3841l8qpcyfazhlfm1cp8ih3n6h4")))

(define-public crate-xdrgen-0.1.2 (c (n "xdrgen") (v "0.1.2") (d (list (d (n "peg") (r "^0.3.3") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.1") (d #t) (k 0)))) (h "0wpp47a0l7bd2spsv89s43linrhykcid5ki4p146h4jj9niz4h9l")))

(define-public crate-xdrgen-0.1.3 (c (n "xdrgen") (v "0.1.3") (d (list (d (n "peg") (r "^0.3.3") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.1") (d #t) (k 0)))) (h "1vc646y8ybzhdswcjh388n9v7ipnq8fd5l8yyq9q8jd35n0zz9hx")))

(define-public crate-xdrgen-0.1.4 (c (n "xdrgen") (v "0.1.4") (d (list (d (n "peg") (r "^0.3.3") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.4") (d #t) (k 0)))) (h "17hngbwh5n3xw1l9cm30qn6ns2wdqi1k96fxmpng9v9fp37v7xdz")))

(define-public crate-xdrgen-0.1.5 (c (n "xdrgen") (v "0.1.5") (d (list (d (n "peg") (r "^0.3.3") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.4") (d #t) (k 0)))) (h "06gbdw1hv2h93zffywqanx943k64a091xsjzz2pz2rj6i5a7phjd")))

(define-public crate-xdrgen-0.1.6 (c (n "xdrgen") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "peg") (r "^0.3.3") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.6") (d #t) (k 0)))) (h "169k02mwsinngiwpgp27ixh13syd8vlw8w7xri20ayj806sgc3qb")))

(define-public crate-xdrgen-0.1.7 (c (n "xdrgen") (v "0.1.7") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "peg") (r "^0.3.3") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.7") (d #t) (k 0)))) (h "16bx666ry9y4g6jdii64c08f8g3qw1n5yzsp275fljbln6zxsb94")))

(define-public crate-xdrgen-0.1.8 (c (n "xdrgen") (v "0.1.8") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "peg") (r "^0.3.3") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.7") (d #t) (k 0)))) (h "1325h45pzvjrkam8b7ki9rq3ac6qa3rbqbmqh1dh76g3n68dj01j")))

(define-public crate-xdrgen-0.1.9 (c (n "xdrgen") (v "0.1.9") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.1.0") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.9") (d #t) (k 0)))) (h "1qr4f1sm3s5q15ravpq6mm5jczmw6lj4x1lj3pipq06czsmx0lmm") (y #t)))

(define-public crate-xdrgen-0.1.10 (c (n "xdrgen") (v "0.1.10") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.1.0") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.9") (d #t) (k 0)))) (h "1l5d1gjr04ji1swr45p1bwiyy04y4qda1kzp3jazg83fhp5c7329")))

(define-public crate-xdrgen-0.1.11 (c (n "xdrgen") (v "0.1.11") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.1.0") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.9") (d #t) (k 0)))) (h "17ri2faankq2ahh0n83mi9bipj6r5gkwda0l9ry40l3m53mg76nd") (y #t)))

(define-public crate-xdrgen-0.1.12 (c (n "xdrgen") (v "0.1.12") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.1.0") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.9") (d #t) (k 0)))) (h "160c6881136bh0mphdhd2j8skcip9wcgsj1kxiqqhlbswk3z4514") (y #t)))

(define-public crate-xdrgen-0.1.13 (c (n "xdrgen") (v "0.1.13") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.1.0") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.1.9") (d #t) (k 0)))) (h "1ik8rc3fbck5nksb1iffxc2lwk0c4q3gyr5qh9fk1mf7id480wr3")))

(define-public crate-xdrgen-0.2.0 (c (n "xdrgen") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.1.0") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.2") (d #t) (k 0)))) (h "0z0qslxw0991vx26yhnqc30r7b8gfk020n9g5w09cy878hh0s6m2")))

(define-public crate-xdrgen-0.2.1 (c (n "xdrgen") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.2") (d #t) (k 0)))) (h "14n4wsi7p8wx040iivmrs988cs3idyfl1iwwch4vmnp6zss8ivnd") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.2.2 (c (n "xdrgen") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.2") (d #t) (k 0)))) (h "118b1kd4apa125wqqsycg89bywqw30nmnvxaiw2znjmzvzq0d438") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.2.3 (c (n "xdrgen") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.2") (d #t) (k 0)))) (h "06qglpgjss2hjj479hpvqpqhqvgpmc0viz02nwjpirvb9rn3ax2h") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.2.4 (c (n "xdrgen") (v "0.2.4") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.2") (d #t) (k 0)))) (h "1bzlgcr7278ipfcy52sfb9b7qdylnx74vljimrriffr2ckwicrmp") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.2.5 (c (n "xdrgen") (v "0.2.5") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.2") (d #t) (k 0)))) (h "0rm4bvn4k0plylm10h2iqcq4b2rywwrphcmk64wvwh136cl820xd") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.3.0 (c (n "xdrgen") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "1.*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xdr-codec") (r "^0.3") (d #t) (k 0)))) (h "0wb6cv87b359pz2803i3pvzs1ldrjj8narfwr2d9aqjkp7sd7apj") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.4.0 (c (n "xdrgen") (v "0.4.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xdr-codec") (r "^0.4") (d #t) (k 0)))) (h "141bkj85nc3cibw4s3d8974wl7g4xqpsvv6nh7nldgc11j2r5mjk") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.4.1 (c (n "xdrgen") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xdr-codec") (r "^0.4") (d #t) (k 0)))) (h "0z1rpgfj8wsvfc6s3b77ysddm43b1ibk53fbsnlczzhglbgcy3aj") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.4.2 (c (n "xdrgen") (v "0.4.2") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xdr-codec") (r "^0.4") (d #t) (k 0)))) (h "187xj67k1k2acgrmh0c264z6j469fy6y62zz6k9al9gn4ws3hvw4") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.4.3 (c (n "xdrgen") (v "0.4.3") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^3.1") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xdr-codec") (r "^0.4") (d #t) (k 0)))) (h "09bl6kps4ris28c7x91ax2yk78swabs5xc1ic8fizf6dld25ja7h") (f (quote (("unstable"))))))

(define-public crate-xdrgen-0.4.4 (c (n "xdrgen") (v "0.4.4") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^3.1") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xdr-codec") (r "^0.4") (d #t) (k 0)))) (h "1lhyi46sjxy4zqgxxjwbg4ap4cgnw7c20sdrs1gyzd46wv05an7g") (f (quote (("unstable"))))))

