(define-module (crates-io xd ot xdotter) #:use-module (crates-io))

(define-public crate-xdotter-0.0.0 (c (n "xdotter") (v "0.0.0") (d (list (d (n "clap") (r "^4.5.2") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "14kv5gvl88pihcni93c9sqdzgcl90znpzv181cpbg90xwgziliry")))

(define-public crate-xdotter-0.0.1 (c (n "xdotter") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.2") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "06jzc22xsr8qsl6vc2srypk5x0r33dlh9rr8i2dw57rdj2qqhj2x")))

(define-public crate-xdotter-0.0.2 (c (n "xdotter") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "1pzby0mmxyy7rablk82krj55xsla67hhx476j0dj5nbjdccsd37x")))

(define-public crate-xdotter-0.0.3 (c (n "xdotter") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "19jxcdabv2i1z00dh5knxkqsmzv3h74v6b62j0j3ixrmvkjbyj8c")))

(define-public crate-xdotter-0.0.4 (c (n "xdotter") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "1h9i5339r0bs5bplapf107m7hfh38z8pl3mldhvgyw0biwk6j72x")))

