(define-module (crates-io xd ot xdotool) #:use-module (crates-io))

(define-public crate-xdotool-0.0.1 (c (n "xdotool") (v "0.0.1") (h "1hws93n83985y6bbfvh85lrpbs311pa2dac8fgx31ph3pxy6vizm")))

(define-public crate-xdotool-0.0.2 (c (n "xdotool") (v "0.0.2") (h "08pjsc4f8nfnda064hi2yxahlnqc07bfbqsqh1qrq25ikwq9s7ib")))

