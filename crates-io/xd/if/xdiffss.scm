(define-module (crates-io xd if xdiffss) #:use-module (crates-io))

(define-public crate-xdiffss-0.1.0 (c (n "xdiffss") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "similar") (r "^2.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zlvwlib2l828v4gwgj4ldn0460504scjflzx7f8jabwrry4b0px")))

