(define-module (crates-io xd ag xdag) #:use-module (crates-io))

(define-public crate-xdag-0.1.0 (c (n "xdag") (v "0.1.0") (h "1ndbllmz27c51jjpcamdhbjj6zzmk73w8ymzm5p6sf3d3n0dgc5g")))

(define-public crate-xdag-0.1.1 (c (n "xdag") (v "0.1.1") (h "1jihrnwb20149rcz7a7kf0i6ah8ksdxp7bj5x5k8w7xrfgj2k3hy")))

(define-public crate-xdag-0.1.2 (c (n "xdag") (v "0.1.2") (h "1jc4lrk2fd6kyx6hbnqbclm4fz7kfnl74lzn95h0ajw87kxbvamn")))

(define-public crate-xdag-0.1.3 (c (n "xdag") (v "0.1.3") (h "13rgy0764gf3rcxj1hcq7gr9hjxvl320hx824lysyf3f6xmyvd1m")))

(define-public crate-xdag-0.1.4 (c (n "xdag") (v "0.1.4") (h "1x164gkviv2rmnr40s63ba8xn7n4wxq8ir8yyd5x3yk3x1idv96d")))

