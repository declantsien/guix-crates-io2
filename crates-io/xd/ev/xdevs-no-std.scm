(define-module (crates-io xd ev xdevs-no-std) #:use-module (crates-io))

(define-public crate-xdevs-no-std-0.1.0 (c (n "xdevs-no-std") (v "0.1.0") (d (list (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "xdevs-no-std-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1dz0l1ygqgv16qkaqmybbhdvdfb35bzjf3xsz8aykydwd6habp2z") (f (quote (("std"))))))

(define-public crate-xdevs-no-std-0.1.1 (c (n "xdevs-no-std") (v "0.1.1") (d (list (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "xdevs-no-std-macros") (r "^0.1.0") (d #t) (k 0)))) (h "02r18nvzx5wac6my9w6d938kma5c5lpzix06kb7nks0q5dawc5ri") (f (quote (("std"))))))

