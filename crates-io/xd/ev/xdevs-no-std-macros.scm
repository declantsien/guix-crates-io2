(define-module (crates-io xd ev xdevs-no-std-macros) #:use-module (crates-io))

(define-public crate-xdevs-no-std-macros-0.1.0 (c (n "xdevs-no-std-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1wmpcaxg5r2byswwj1cw9zc15x4d2kh3q6avfp4ax0yd2lzv8aaf")))

