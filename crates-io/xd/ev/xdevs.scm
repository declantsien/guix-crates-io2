(define-module (crates-io xd ev xdevs) #:use-module (crates-io))

(define-public crate-xdevs-0.1.0 (c (n "xdevs") (v "0.1.0") (h "0bxahmafg9q5662qbndvh5zjsdvq8f488vznxrwjl63zciwxrl5s") (y #t)))

(define-public crate-xdevs-0.1.1 (c (n "xdevs") (v "0.1.1") (h "0ii6bn90mcl8l0lsj2mbp3ahnav5z94gh1pbcw5yn1m5yhyfcv36")))

(define-public crate-xdevs-0.2.0 (c (n "xdevs") (v "0.2.0") (d (list (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1fa18mnv2j52hwk7lh2p56x582x3l3gs405009pkdh8f5g1cv5pb") (f (quote (("par_xxc" "par_eoc" "par_xic") ("par_xic" "par_any") ("par_transition" "par_any") ("par_stop" "par_any") ("par_start" "par_any") ("par_sim_no_xxc" "par_collection" "par_transition") ("par_sim" "par_sim_no_xxc" "par_xxc") ("par_eoc" "par_any") ("par_collection" "par_any") ("par_any" "rayon") ("par_all_no_xxc" "par_start" "par_sim_no_xxc" "par_stop") ("par_all" "par_all_no_xxc" "par_xxc")))) (y #t)))

(define-public crate-xdevs-0.2.1 (c (n "xdevs") (v "0.2.1") (d (list (d (n "cpu-time") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0kzgzzr4pk3g0sa7zas4pylwbfhvvasgll04blcd48j2jiqjyv04") (f (quote (("par_xxc" "par_eoc" "par_xic") ("par_xic" "par_any") ("par_transition" "par_any") ("par_stop" "par_any") ("par_start" "par_any") ("par_sim_no_xxc" "par_collection" "par_transition") ("par_sim" "par_sim_no_xxc" "par_xxc") ("par_eoc" "par_any") ("par_collection" "par_any") ("par_any" "rayon") ("par_all_no_xxc" "par_start" "par_sim_no_xxc" "par_stop") ("par_all" "par_all_no_xxc" "par_xxc") ("devstone_busy" "cpu-time"))))))

(define-public crate-xdevs-0.3.0 (c (n "xdevs") (v "0.3.0") (d (list (d (n "cpu-time") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1id5maq3qvqd7dn0j3q7ymsjp2vq3m083cijm09vk48ny48qjqac") (f (quote (("par_xxc" "par_eoc" "par_xic") ("par_xic" "par_any") ("par_transition" "par_any") ("par_stop" "par_any") ("par_start" "par_any") ("par_sim_no_xxc" "par_collection" "par_transition") ("par_sim" "par_sim_no_xxc" "par_xxc") ("par_eoc" "par_any") ("par_collection" "par_any") ("par_any" "rayon") ("par_all_no_xxc" "par_start" "par_sim_no_xxc" "par_stop") ("par_all" "par_all_no_xxc" "par_xxc") ("devstone_busy" "cpu-time"))))))

