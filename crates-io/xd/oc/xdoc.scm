(define-module (crates-io xd oc xdoc) #:use-module (crates-io))

(define-public crate-xdoc-0.0.0 (c (n "xdoc") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)))) (h "1j762p5g02ms3cvjsnl168kq2cbgfb64wbh42qhib5b0cf74ldcx") (f (quote (("default"))))))

(define-public crate-xdoc-0.0.1 (c (n "xdoc") (v "0.0.1") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)))) (h "0hznadid8c3i4sw8sy7gwykd2a5xxygi9g7j3m8m5ssbxq4vbxjg") (f (quote (("default"))))))

(define-public crate-xdoc-0.0.2 (c (n "xdoc") (v "0.0.2") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)))) (h "1g9waqfdmafzaxiac921i31nlns8i8pz4d9nrw29g62vq3lkkc00") (f (quote (("default"))))))

