(define-module (crates-io xd el xdelta3) #:use-module (crates-io))

(define-public crate-xdelta3-0.1.0 (c (n "xdelta3") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 1)))) (h "04hzp24zx5q5mhw6vifygizd9yajkk32v186g0dib8y0bjmqrb58")))

(define-public crate-xdelta3-0.1.1 (c (n "xdelta3") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 1)))) (h "179plqnj32b0fzmhk64hflz6dbrbrhs3fqkh0ph3m9g2wp0r1myy")))

(define-public crate-xdelta3-0.1.2 (c (n "xdelta3") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 1)))) (h "1sdg6zrk3n05c4iyz899lkisk1x0w3jfppdsdpmns8mqpms67arc")))

(define-public crate-xdelta3-0.1.5 (c (n "xdelta3") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 1)))) (h "0ybfiqy42x42nqfir5m4pvbcpai2aarrx92ww83iw18psyp4akpi")))

