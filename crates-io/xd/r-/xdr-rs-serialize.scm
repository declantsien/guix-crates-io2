(define-module (crates-io xd r- xdr-rs-serialize) #:use-module (crates-io))

(define-public crate-xdr-rs-serialize-0.1.0 (c (n "xdr-rs-serialize") (v "0.1.0") (d (list (d (n "xdr-rs-serialize-derive") (r "^0.1.0") (d #t) (k 2)))) (h "108bjnm0bf08g2dpixr3l6k5l06iissd5ivk7sxjxd05wkm56k9n") (y #t)))

(define-public crate-xdr-rs-serialize-0.1.0-alpha (c (n "xdr-rs-serialize") (v "0.1.0-alpha") (d (list (d (n "xdr-rs-serialize-derive") (r "^0.1.0-alpha") (d #t) (k 2)))) (h "1pqb1mga7235ayy6yzd6q550xsivq806kg9lap1kb8scbzw3ww0i")))

(define-public crate-xdr-rs-serialize-0.2.0 (c (n "xdr-rs-serialize") (v "0.2.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.0") (d #t) (k 2)))) (h "0s8v8l9dmm9g605vn1mkc0izcmj4c9z8q6sr289h7j2zh07i3782")))

(define-public crate-xdr-rs-serialize-0.2.1 (c (n "xdr-rs-serialize") (v "0.2.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.1") (d #t) (k 2)))) (h "1mq2kbn7b699jxn8xc4j6m81mmn2qx15w0shyrnjai04f9kzwv1m")))

(define-public crate-xdr-rs-serialize-0.2.3 (c (n "xdr-rs-serialize") (v "0.2.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.3") (d #t) (k 2)))) (h "0bwl9kd2y0hdjb61fi7h8akxrkrch08mgjsj0j6vzi96swl25647")))

(define-public crate-xdr-rs-serialize-0.2.4 (c (n "xdr-rs-serialize") (v "0.2.4") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.4") (d #t) (k 2)))) (h "0hsvz1h9hcv9ly4fjki5ky9hm1ijqsqf6yr0lzp2bww223nj57zi")))

(define-public crate-xdr-rs-serialize-0.2.5 (c (n "xdr-rs-serialize") (v "0.2.5") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.2.5") (d #t) (k 2)))) (h "1sip2m1wx36cw9p5wddk29vk2zzk7k8692m50pxx8a7jd59ixg7l")))

(define-public crate-xdr-rs-serialize-0.3.0 (c (n "xdr-rs-serialize") (v "0.3.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.0") (d #t) (k 2)))) (h "1gbh2x5l2rr1i9l1mrpsicsh8cdd2pjbx963hk5q04b1vb05jknp")))

(define-public crate-xdr-rs-serialize-0.3.1 (c (n "xdr-rs-serialize") (v "0.3.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.3.1") (d #t) (k 2)))) (h "0j9p181rfp6vzl4klm3r3xzqz3rpklp3jgd14zk8namjy25ddc3i")))

