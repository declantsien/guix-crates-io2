(define-module (crates-io xd r- xdr-codec) #:use-module (crates-io))

(define-public crate-xdr-codec-0.1.0 (c (n "xdr-codec") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "0iszflv9z0yiqql8p6wyflphx62ack9r4s62if7vgikf3qpp4mq0")))

(define-public crate-xdr-codec-0.1.1 (c (n "xdr-codec") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "0acjl0f43wha5vvnlbk2pk8sanb35v6im11gpxldn55m6w9bwpa3")))

(define-public crate-xdr-codec-0.1.2 (c (n "xdr-codec") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "18lgppqrq9sra5axc4y9a5dy8h40bwdh7qiyanjpkfm1h95lwzdy")))

(define-public crate-xdr-codec-0.1.4 (c (n "xdr-codec") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "1q4k1b5q45mhsw27d0sqg75zjhh8f3hcw902bsp7hp8g00y60mr9")))

(define-public crate-xdr-codec-0.1.5 (c (n "xdr-codec") (v "0.1.5") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "05q5jka8lslbhrdrp7m6mghlcnr6pgpn8sqxxmi7xc91lhs4gn1x")))

(define-public crate-xdr-codec-0.1.6 (c (n "xdr-codec") (v "0.1.6") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "1fymragz9fapha3jbv9i68y3lpkkxfz431qqjhsy837krkiyh5b6")))

(define-public crate-xdr-codec-0.1.7 (c (n "xdr-codec") (v "0.1.7") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "1q792i151wd1fvplaqn082n1rlp3qyrvky99qs0h21pgmsf248ih")))

(define-public crate-xdr-codec-0.1.8 (c (n "xdr-codec") (v "0.1.8") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "0wy7zqls52hh8qvq58pkcwb20bssg4qxd5g6shqzv3j24zxqjlf4")))

(define-public crate-xdr-codec-0.1.9 (c (n "xdr-codec") (v "0.1.9") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "0b3p67684bsn2fi60aaidndrcx5spsbmnzmrl7hxlll0znd441hk")))

(define-public crate-xdr-codec-0.1.10 (c (n "xdr-codec") (v "0.1.10") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "0zl7az29qig4jlxhg508p5vssw8jq4an6lzisryf63pq5aa25kgl")))

(define-public crate-xdr-codec-0.1.11 (c (n "xdr-codec") (v "0.1.11") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "0aj4py0axyb695r2j8v611r36xdzwbap5isqmknav26v6aldk73g") (y #t)))

(define-public crate-xdr-codec-0.1.12 (c (n "xdr-codec") (v "0.1.12") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "1ipk1c6r0zxihm68gr6ljh5krr9xd1crnp89c3grdqp6a06z6j87") (y #t)))

(define-public crate-xdr-codec-0.1.13 (c (n "xdr-codec") (v "0.1.13") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "1kipm0qmvawq66vqnr9ml656v3v6d24mnc0307iavqb3s8jd88ws")))

(define-public crate-xdr-codec-0.2.0 (c (n "xdr-codec") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1klif7kwfrblks9cp9f919wxfsg4xfg40l7zd6mxmjq21sqkyp1b")))

(define-public crate-xdr-codec-0.2.1 (c (n "xdr-codec") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1j4ppd99n2rhvk4vrs924ijmlzvanpnkp292v8a315a0k0k31bxf") (f (quote (("unstable"))))))

(define-public crate-xdr-codec-0.2.2 (c (n "xdr-codec") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1qrw999h6k3wh7v1znx3kjxjrhadgcb2zz7y0gk85y9gakxqr6m0") (f (quote (("unstable"))))))

(define-public crate-xdr-codec-0.2.3 (c (n "xdr-codec") (v "0.2.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1r90y6pba6q651p84750rsdsw9g0frwa2s74h78gjxhra4gs6842") (f (quote (("unstable"))))))

(define-public crate-xdr-codec-0.2.4 (c (n "xdr-codec") (v "0.2.4") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "14a3gs2xdy7l3xjrljhqhk81cnh6hdkxmv8xv0w68wxxdi7b3daa") (f (quote (("unstable"))))))

(define-public crate-xdr-codec-0.2.5 (c (n "xdr-codec") (v "0.2.5") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0b686vn3ljpi39bf3di1m337kp1h3c72v812xhw60mnffnjsracv") (f (quote (("unstable"))))))

(define-public crate-xdr-codec-0.3.0 (c (n "xdr-codec") (v "0.3.0") (d (list (d (n "byteorder") (r "1.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0i3gzw7kb65w958qy1yk205yhwiqys3v440bxihbi87zknhcrbrz") (f (quote (("unstable"))))))

(define-public crate-xdr-codec-0.4.0 (c (n "xdr-codec") (v "0.4.0") (d (list (d (n "byteorder") (r "1.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "06al0jw2czd01y765v8pf7ag82yyg259qgp07qcv6q2bp45z0xyc") (f (quote (("unstable") ("bytecodec"))))))

(define-public crate-xdr-codec-0.4.1 (c (n "xdr-codec") (v "0.4.1") (d (list (d (n "byteorder") (r "1.*") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0m9ra1cvqdb96cg27ch95ys0qrh0kxrprmj7j3hk6bajbjl2f157") (f (quote (("unstable") ("bytecodec"))))))

(define-public crate-xdr-codec-0.4.2 (c (n "xdr-codec") (v "0.4.2") (d (list (d (n "byteorder") (r "1.*") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0mlsnx1x6kw35gcc2i3hx9h20dykghzj2l97ii7sx80kiwmapayv") (f (quote (("unstable") ("bytecodec"))))))

(define-public crate-xdr-codec-0.4.3 (c (n "xdr-codec") (v "0.4.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "04mz36y9ddn4wln6l2pjfzvlhkfaj044zlwr9dn79x80nxpd778h") (f (quote (("unstable") ("bytecodec"))))))

(define-public crate-xdr-codec-0.4.4 (c (n "xdr-codec") (v "0.4.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "19lg2p9if5klzqrs8pgmrl0l923b6s9m3wpag1h8bfkxawr6a1a8") (f (quote (("unstable") ("bytecodec"))))))

