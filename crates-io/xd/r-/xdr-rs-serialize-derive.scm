(define-module (crates-io xd r- xdr-rs-serialize-derive) #:use-module (crates-io))

(define-public crate-xdr-rs-serialize-derive-0.1.0-alpha (c (n "xdr-rs-serialize-derive") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1yh2akmdvbwilmdszsvr7jifqqvvf75d2x52wjmi873blnc1i1y4")))

(define-public crate-xdr-rs-serialize-derive-0.1.0 (c (n "xdr-rs-serialize-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1lmh6xdx0siqwms2xsflma7mh3bwk6d28lbxf0116g4i4x52wb81") (y #t)))

(define-public crate-xdr-rs-serialize-derive-0.2.0 (c (n "xdr-rs-serialize-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1vh6bhlrrgxf3x0b83jy7r85k3ji3lsd6cvy3phfbw6fnng6yn37")))

(define-public crate-xdr-rs-serialize-derive-0.2.1 (c (n "xdr-rs-serialize-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1pxb3025p16r2j0bwhsfb7w1k0lv5d9h16kim3i3ykzn9is4yygq")))

(define-public crate-xdr-rs-serialize-derive-0.2.2 (c (n "xdr-rs-serialize-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1r8aryp8kj0mjb68nrbv9vspn4856m0s1jdiqhmg3iks24ad2b0m")))

(define-public crate-xdr-rs-serialize-derive-0.2.3 (c (n "xdr-rs-serialize-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0igqka0698w92gdp3fkwd5h35whgcdwx3rrjvrd1m60qc67ly4h9")))

(define-public crate-xdr-rs-serialize-derive-0.2.4 (c (n "xdr-rs-serialize-derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1zwbcxw0bby81ydb2vhll34l8i72lkh9az0z7w5hpd49a6w2h4ql")))

(define-public crate-xdr-rs-serialize-derive-0.2.5 (c (n "xdr-rs-serialize-derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0pdvqzbzxcff88ykh850kzy40pivcnxvbrd7sh3x20qqxlbi1ph1")))

(define-public crate-xdr-rs-serialize-derive-0.3.0 (c (n "xdr-rs-serialize-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "16iai52vyki0l1q7fg7pyybj1p9b2kv7jy33bsy1i0qpqi92z3mj")))

(define-public crate-xdr-rs-serialize-derive-0.3.1 (c (n "xdr-rs-serialize-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "11mjfly47qddhvyzw4wmy64pnb7fmxxab1ia2zil8bdgf0rd5g3s")))

