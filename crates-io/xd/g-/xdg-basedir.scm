(define-module (crates-io xd g- xdg-basedir) #:use-module (crates-io))

(define-public crate-xdg-basedir-0.2.0 (c (n "xdg-basedir") (v "0.2.0") (h "1g6cchq490pic0nkh0alkgv6vql7h14i2jxm7xysbq8lca0pjxw5") (f (quote (("unstable"))))))

(define-public crate-xdg-basedir-0.2.1 (c (n "xdg-basedir") (v "0.2.1") (h "1c5ah0xsr14fvypyjs15g5a7dzg135a9iqkhv1g3vbcr51mw14qf") (f (quote (("unstable"))))))

(define-public crate-xdg-basedir-0.2.2 (c (n "xdg-basedir") (v "0.2.2") (h "1zaq3bnx083ckg2fxgrvz5p0jxva3g1rgjlnhdxbzypalpsiqdvd") (f (quote (("unstable"))))))

(define-public crate-xdg-basedir-1.0.0 (c (n "xdg-basedir") (v "1.0.0") (h "1z5vzk6a7qxinqn3q70wzkh9y2ngjigb8y56dv3rskwwjdvd1580") (f (quote (("unstable"))))))

