(define-module (crates-io xd g- xdg-directories) #:use-module (crates-io))

(define-public crate-xdg-directories-0.1.0 (c (n "xdg-directories") (v "0.1.0") (h "1x50zmaak8a89c2800mrnj19fkzp6d0l1yx2wljrryds77ivbjgh") (y #t)))

(define-public crate-xdg-directories-0.0.0 (c (n "xdg-directories") (v "0.0.0") (h "0x8skch2d4bcrr1dj6hh6p168r7w6k4bq08qxfxxis2rdxy0am5q")))

