(define-module (crates-io xd g- xdg-rs) #:use-module (crates-io))

(define-public crate-xdg-rs-0.0.1 (c (n "xdg-rs") (v "0.0.1") (h "1hdpj1nzfpnh1f649ax4g5msirpk26br2g6jzrphypjis84wgriw")))

(define-public crate-xdg-rs-0.0.2 (c (n "xdg-rs") (v "0.0.2") (h "0k40aa8n00l358zja5jzb0a6lcwc8d52xxgqxwczrslmli4xyy02")))

(define-public crate-xdg-rs-0.0.3 (c (n "xdg-rs") (v "0.0.3") (h "1iwxypdvdp6cghyjk4fj4zn9dj3vrqishfyy82wyzra5ayl1nwwp")))

(define-public crate-xdg-rs-0.0.4 (c (n "xdg-rs") (v "0.0.4") (h "0b8c0dn2lyqgys82jrfv35spsjnxj9irbwslr8zvwm0yv8547acv")))

(define-public crate-xdg-rs-0.0.5 (c (n "xdg-rs") (v "0.0.5") (h "1lywnvfk16849ns3jih460i9v3dcx62igwlmlyk5y478jicxcd3a")))

(define-public crate-xdg-rs-0.0.6 (c (n "xdg-rs") (v "0.0.6") (h "1cz5crll6yni7ikxixfanpld5gcv4af162by3j1pj481jrxrilrv")))

(define-public crate-xdg-rs-0.0.7 (c (n "xdg-rs") (v "0.0.7") (h "06m4y7zrc6r7qrs1jjgjxdygrp7xvx7kg5l9g7zblb525llqj59a")))

(define-public crate-xdg-rs-0.0.8 (c (n "xdg-rs") (v "0.0.8") (h "0q8lvai89v57y7zzgchsi4s19m6c3lbjglzrs0mx7i5ixmiihccn")))

(define-public crate-xdg-rs-0.1.0 (c (n "xdg-rs") (v "0.1.0") (h "19kz5h49xfw0hx7iw1w7gh75885pmkklgcil1269fkii6rn7q088")))

(define-public crate-xdg-rs-0.1.1 (c (n "xdg-rs") (v "0.1.1") (h "1zgjcgzjh0hdlisyhvddgkyhvacdjbxc8759jf3vxkp4sg9y9dhy")))

(define-public crate-xdg-rs-0.1.2 (c (n "xdg-rs") (v "0.1.2") (h "1j41fawh447w6l5lfx6gfp4zndwsk96k2izxc8ra3fs08cal33g5") (f (quote (("nightly"))))))

(define-public crate-xdg-rs-0.1.3 (c (n "xdg-rs") (v "0.1.3") (h "19iy0hgwxja2xmbmr3a5hw8bpa8wnvmw9nw187vff6kbzic7kznx") (f (quote (("unstable"))))))

(define-public crate-xdg-rs-0.1.4 (c (n "xdg-rs") (v "0.1.4") (h "0samlr0bkf3anwyd5yj6sjdvhrji9m99ys1fnnqv3l4aag64bdcl") (f (quote (("unstable"))))))

