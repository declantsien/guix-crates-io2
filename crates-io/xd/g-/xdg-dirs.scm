(define-module (crates-io xd g- xdg-dirs) #:use-module (crates-io))

(define-public crate-xdg-dirs-0.1.0 (c (n "xdg-dirs") (v "0.1.0") (h "18ngwqqwjxdhlj7gz85hlj3vlq4kf3ailvkym0lyxj720hsqv2af") (y #t)))

(define-public crate-xdg-dirs-0.0.0 (c (n "xdg-dirs") (v "0.0.0") (h "1r5apms0kxs3kp28pjpds6j71g08wdssjmd67y2siwnwrzbz1lr5")))

