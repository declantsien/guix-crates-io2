(define-module (crates-io xd g- xdg-home) #:use-module (crates-io))

(define-public crate-xdg-home-1.0.0 (c (n "xdg-home") (v "1.0.0") (d (list (d (n "nix") (r "^0.26.2") (f (quote ("user"))) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1kbd1ks8bvpsay6lgk60yaf1w13daaf75ghmslan031ss4y20s97") (r "1.60")))

(define-public crate-xdg-home-1.1.0 (c (n "xdg-home") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13mkcfgngnc1fpdg5737hvhjkp95bc9w2ngqdjnri0ybqcjs7r91") (r "1.60")))

