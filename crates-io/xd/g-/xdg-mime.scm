(define-module (crates-io xd g- xdg-mime) #:use-module (crates-io))

(define-public crate-xdg-mime-0.1.0 (c (n "xdg-mime") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "unicase") (r "^2.3.0") (d #t) (k 0)))) (h "1x5fw66cxb98bhd4ggd70vhwwn357lcd27cn2dxd447n0h139rn0")))

(define-public crate-xdg-mime-0.2.0 (c (n "xdg-mime") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "unicase") (r "^2.3.0") (d #t) (k 0)))) (h "1gn14fqyhjhh6rk67l1y23sjy7lsh7cfql1vxgdl02rf11ipyw9w")))

(define-public crate-xdg-mime-0.2.1 (c (n "xdg-mime") (v "0.2.1") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "unicase") (r "^2.3.0") (d #t) (k 0)))) (h "11sm1yl7xamnh1kdc66azq6jmdv02rrx0iaqkfi8zv1ph3jkg0cz")))

(define-public crate-xdg-mime-0.3.0 (c (n "xdg-mime") (v "0.3.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "unicase") (r "^2.3.0") (d #t) (k 0)))) (h "10khmfyjlg86h65bp15aki0lh7k9rpl6p5k0ckdq3jsgxpaq09rn")))

(define-public crate-xdg-mime-0.3.1 (c (n "xdg-mime") (v "0.3.1") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "unicase") (r "^2.3.0") (d #t) (k 0)))) (h "1iiz4xnags2k6nan4b2gcx74lgky7m3sz1swl587sbyxdbv5xyrq")))

(define-public crate-xdg-mime-0.3.2 (c (n "xdg-mime") (v "0.3.2") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "unicase") (r "^2.3.0") (d #t) (k 0)))) (h "0x9qsjy1hscvygyacpv90p2cz5ay6brs4n453zfsabjslnsfzglx")))

(define-public crate-xdg-mime-0.3.3 (c (n "xdg-mime") (v "0.3.3") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "unicase") (r "^2.3.0") (d #t) (k 0)))) (h "1alplz0ki5zdxmi6s9w04p1yiwrxkpi7nikfldq8sn2hpdlppgw7")))

(define-public crate-xdg-mime-0.4.0 (c (n "xdg-mime") (v "0.4.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "unicase") (r "^2.3.0") (d #t) (k 0)))) (h "1j8yx29crbwwy3dv0nf55syngz5c37q2dfbfln21kywkrb82blsq")))

