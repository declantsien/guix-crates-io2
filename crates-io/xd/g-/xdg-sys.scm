(define-module (crates-io xd g- xdg-sys) #:use-module (crates-io))

(define-public crate-xdg-sys-0.1.0 (c (n "xdg-sys") (v "0.1.0") (h "08n086zdsjwrzp3zi2qa2ycivch6kyraqlch9fqw270c9096q3zy") (y #t)))

(define-public crate-xdg-sys-0.0.0 (c (n "xdg-sys") (v "0.0.0") (h "0mbiilvcm009kjnzr3mslv7ql9z67q5739f7hy6p19q4p4azzzjm")))

