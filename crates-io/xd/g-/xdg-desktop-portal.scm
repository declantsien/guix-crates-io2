(define-module (crates-io xd g- xdg-desktop-portal) #:use-module (crates-io))

(define-public crate-xdg-desktop-portal-0.1.0 (c (n "xdg-desktop-portal") (v "0.1.0") (d (list (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zbus") (r "^3.14.1") (d #t) (k 0)))) (h "100zqr0dpb1xhqs6d1yxc5616y0c9ig08h50m9panfnpdyi595ya")))

