(define-module (crates-io xd g- xdg-utils) #:use-module (crates-io))

(define-public crate-xdg-utils-0.3.0 (c (n "xdg-utils") (v "0.3.0") (h "0gmffrmjlzzr4bs8q0ssjv8l5j2fz1m8yi76vd8m5cisidxa0jjb")))

(define-public crate-xdg-utils-0.3.1 (c (n "xdg-utils") (v "0.3.1") (h "1knkvmnmdp36zcq9l862kf2sadxsprz7gpn4ln9z6ja2izdpbhnn")))

(define-public crate-xdg-utils-0.3.2 (c (n "xdg-utils") (v "0.3.2") (h "1nfi4n325p69kv1q95assk06zva4qcd0pw15d50n88rdmd4clwzq")))

(define-public crate-xdg-utils-0.4.0 (c (n "xdg-utils") (v "0.4.0") (h "19nvj2afc67xgdzdddi2lh6wq089frm9wlpw5hg74sar5pkfz7yv")))

