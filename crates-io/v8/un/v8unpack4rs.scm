(define-module (crates-io v8 un v8unpack4rs) #:use-module (crates-io))

(define-public crate-v8unpack4rs-0.1.0 (c (n "v8unpack4rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "deflate") (r "^0.7.17") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "inflate") (r "^0.3.3") (d #t) (k 0)))) (h "06q6rjkkz2ycn76by967axiph3896fw1sykvc9qh2a2m3bqhr0a3")))

(define-public crate-v8unpack4rs-0.2.0 (c (n "v8unpack4rs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "deflate") (r "^0.7.17") (d #t) (k 0)) (d (n "inflate") (r "^0.3.3") (d #t) (k 0)))) (h "1mnx2j60n98qanrwrd64p89fdhn8k1is85sikambf53l8jwgha5a")))

(define-public crate-v8unpack4rs-0.2.1 (c (n "v8unpack4rs") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "deflate") (r "^0.7.17") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "inflate") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1g49i3vjwkrxsfsq1c51l9xmv7xsmk4yqknqcq1zdlqhhpwz7siv")))

(define-public crate-v8unpack4rs-0.3.0 (c (n "v8unpack4rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "deflate") (r "^0.7.19") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "inflate") (r "^0.4.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0b5fq4zr5d78czb4w71rwpscl0h4s3s6c54blcwn42kaqxa4vjsj")))

