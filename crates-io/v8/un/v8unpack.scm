(define-module (crates-io v8 un v8unpack) #:use-module (crates-io))

(define-public crate-v8unpack-0.1.0 (c (n "v8unpack") (v "0.1.0") (d (list (d (n "clap") (r "^2.28.0") (d #t) (k 0)) (d (n "v8unpack4rs") (r "^0.1") (d #t) (k 0)))) (h "0x5l5ls9p2a5kwk0wp0iv2c96hq4wiklppj9p70mwj146amnkk2d")))

(define-public crate-v8unpack-0.1.1 (c (n "v8unpack") (v "0.1.1") (d (list (d (n "clap") (r "^2.28.0") (d #t) (k 0)) (d (n "v8unpack4rs") (r "^0.1") (d #t) (k 0)))) (h "1i3066sricycq0z8lvl0wygzrrrra28bqvkl6yaqxiiw6xa8s32w")))

(define-public crate-v8unpack-0.2.0 (c (n "v8unpack") (v "0.2.0") (d (list (d (n "clap") (r "^2.28.0") (d #t) (k 0)) (d (n "v8unpack4rs") (r "^0.2.0") (d #t) (k 0)))) (h "0xd89ikfi078sbs72pavr2b2m4drzi88hklgvxwi5kbildmf0ib4")))

(define-public crate-v8unpack-0.2.1 (c (n "v8unpack") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.28.0") (d #t) (k 0)) (d (n "fern") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "v8unpack4rs") (r "^0.2.1") (d #t) (k 0)))) (h "1qn76yyfsxsmw4411vjizm75g7n181ag7l841d2lj0xp14jf5jns")))

(define-public crate-v8unpack-0.3.0 (c (n "v8unpack") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "fern") (r "^0.5.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "v8unpack4rs") (r "^0.3.0") (d #t) (k 0)))) (h "1mysfmh7wal9j9lph19zay96k886pj3v5fnak555knww0zq13pk6")))

