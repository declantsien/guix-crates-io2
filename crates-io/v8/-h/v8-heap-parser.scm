(define-module (crates-io v8 -h v8-heap-parser) #:use-module (crates-io))

(define-public crate-v8-heap-parser-0.1.0 (c (n "v8-heap-parser") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "084i285045f5867qc53in1f8jasln48qxi42rq5cw0bzgavrwi6s")))

(define-public crate-v8-heap-parser-1.1.0 (c (n "v8-heap-parser") (v "1.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "04a08ngb3gj2x0j03axwh6ahr3xky86sphn3d7bz4zr80mmsnznm")))

