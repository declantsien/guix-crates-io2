(define-module (crates-io v8 fi v8find4rs) #:use-module (crates-io))

(define-public crate-v8find4rs-0.1.0 (c (n "v8find4rs") (v "0.1.0") (d (list (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "exe") (r "^0.4.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1xhvy3ld77hxab2jqdfdyvsn4863l4gw4s30ypg8dgx5xp84yg9j")))

