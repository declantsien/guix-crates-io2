(define-module (crates-io v8 #{53}# v853-pac) #:use-module (crates-io))

(define-public crate-v853-pac-0.0.1 (c (n "v853-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "18ws1qrnwc2gs47zixp42n2p3zgn3wdswfxl6mp0c3mjfmhl7yxj") (f (quote (("rt"))))))

