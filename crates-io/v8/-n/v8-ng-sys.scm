(define-module (crates-io v8 -n v8-ng-sys) #:use-module (crates-io))

(define-public crate-v8-ng-sys-0.1.0 (c (n "v8-ng-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "clang") (r "^0.12.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 2)) (d (n "v8-api") (r "^0.1.0") (d #t) (k 1)))) (h "0is5wnms207rxdwf44pn9vs6h5s0azbjypjf40raq24aq2662jns") (f (quote (("shared"))))))

(define-public crate-v8-ng-sys-0.1.1 (c (n "v8-ng-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "clang") (r "^0.12.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 2)) (d (n "v8-api") (r "^0.1.1") (d #t) (k 1)))) (h "0vx7yk9hxjbqj0y445k183ggd7hyb7vpz9ar2lb0sbz62xl7j6lx") (f (quote (("shared"))))))

(define-public crate-v8-ng-sys-0.1.2 (c (n "v8-ng-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "clang") (r "^0.12.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 2)) (d (n "v8-api") (r "^0.1.2") (d #t) (k 1)))) (h "06bixgwncshwp1qp80ylvmdrw7mv0l0wk5c2iiha3l7hcl41bjnj") (f (quote (("shared"))))))

(define-public crate-v8-ng-sys-0.2.0 (c (n "v8-ng-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "clang") (r "^0.12.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 2)) (d (n "v8-api") (r "^0.1.2") (d #t) (k 1)))) (h "15qccza2bjgdqxx7c0gkw31c3kx8j7s7s5z8wpbcwssbn69gjgj8") (f (quote (("shared"))))))

(define-public crate-v8-ng-sys-0.2.1 (c (n "v8-ng-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "clang") (r "^0.12.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 2)) (d (n "v8-api") (r "^0.1.2") (d #t) (k 1)))) (h "0w2li73mp3kan2r0y04lfw12jfcrw6g9w0pb4qxx1l2jzwjvyrqf") (f (quote (("shared"))))))

(define-public crate-v8-ng-sys-0.3.0 (c (n "v8-ng-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "clang") (r "^0.12.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 2)) (d (n "v8-api") (r "^0.1.2") (d #t) (k 1)))) (h "0csf2pc5n8i46k5zw5j4w6h7fy72yqdhhhd24vhck96ar8lfy5m5") (f (quote (("shared"))))))

