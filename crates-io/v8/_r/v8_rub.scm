(define-module (crates-io v8 _r v8_rub) #:use-module (crates-io))

(define-public crate-v8_rub-0.0.1 (c (n "v8_rub") (v "0.0.1") (d (list (d (n "buildable") (r "= 0.0.1") (d #t) (k 0)) (d (n "docopt") (r "= 0.6.14") (d #t) (k 0)))) (h "1v92yqgq0xjp6v4nypl3viajmgivgj6xl96brqi0npk6lcbvd4rk")))

(define-public crate-v8_rub-0.0.2 (c (n "v8_rub") (v "0.0.2") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "0xp5ixxq59nbzqgavm4rix1l1m9lm66ns019nyysncyx8mbq7cm1")))

(define-public crate-v8_rub-0.0.3 (c (n "v8_rub") (v "0.0.3") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "0xrhxm4cri9mja4zlwvajik7wy6557b32k2g8vkk8qv6cqglxjlr")))

(define-public crate-v8_rub-0.0.4 (c (n "v8_rub") (v "0.0.4") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "0904jwm96p9mpyl9l1lqsy3028k2d8x0g1q5442ijg1bzl8gxi5p")))

(define-public crate-v8_rub-0.0.5 (c (n "v8_rub") (v "0.0.5") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "08z8w1mj1a2i6469b6khx58j8rc9xnshl0k39zf2x5i92z2an7qm")))

