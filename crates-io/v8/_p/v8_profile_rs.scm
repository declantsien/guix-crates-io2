(define-module (crates-io v8 _p v8_profile_rs) #:use-module (crates-io))

(define-public crate-v8_profile_rs-1.0.0 (c (n "v8_profile_rs") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rs-parse-snapshot") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11m1xifydd5fdy4wppd06r5r9rv8kzkikjklwgyycw3gmqqs8aw0") (y #t)))

(define-public crate-v8_profile_rs-1.0.1 (c (n "v8_profile_rs") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rs-parse-snapshot") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m0xk2xk5njkfvgiv0b0rg2q33sd1jkifr2cn2znb7x09n92x184") (y #t)))

(define-public crate-v8_profile_rs-1.0.2 (c (n "v8_profile_rs") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rs-parse-snapshot") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qd2kbs5822x1lb45jgb3bpy70ahzqvg0n7mcfsqhk17wbhxm2b5") (y #t)))

