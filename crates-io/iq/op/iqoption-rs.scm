(define-module (crates-io iq op iqoption-rs) #:use-module (crates-io))

(define-public crate-iqoption-rs-0.0.2 (c (n "iqoption-rs") (v "0.0.2") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("test-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "06gplg8xs4mipcxpbns5gr4s59wmiyd7prvbwrn7j9sy2j628x7z") (r "1.70.0")))

(define-public crate-iqoption-rs-0.1.0 (c (n "iqoption-rs") (v "0.1.0") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("test-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1zah64xz80hcjdmmna33gbcjkss0v4z8q2kjfw07kaai0l9jd623") (y #t) (r "1.70.0")))

