(define-module (crates-io iq ht iqhttp) #:use-module (crates-io))

(define-public crate-iqhttp-0.0.0 (c (n "iqhttp") (v "0.0.0") (h "0wrs755f1p5x7i45496hgy2pxdl0q0kg43b14cd8fdq38xbv9260") (y #t)))

(define-public crate-iqhttp-0.0.1 (c (n "iqhttp") (v "0.0.1") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-proxy") (r "=0.9.1") (o #t) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (f (quote ("rustls-native-certs"))) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1lxzjdrgxd9znihb6xirx6g9ahbrkz9npxd1zamvkpg3c3sm1pp3") (f (quote (("proxy" "hyper-proxy") ("json" "serde" "serde_json"))))))

(define-public crate-iqhttp-0.1.0 (c (n "iqhttp") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-proxy") (r "=0.9.1") (o #t) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (f (quote ("rustls-native-certs"))) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1w2jr7kfr80yh2qfxngqb56sq3s99x4jm4zv4xsg18zv2vgp2jww") (f (quote (("proxy" "hyper-proxy") ("json" "serde" "serde_json"))))))

(define-public crate-iqhttp-0.2.0 (c (n "iqhttp") (v "0.2.0") (d (list (d (n "hyper") (r "^0.14.10") (d #t) (k 0)) (d (n "hyper-proxy") (r "=0.9.1") (o #t) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (f (quote ("rustls-native-certs"))) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1wvf0kq72241m4s6bmh2y43mzghd8zlvypyxhbdjpf7pv9xlbkcx") (f (quote (("proxy" "hyper-proxy") ("json" "serde" "serde_json")))) (r "1.56")))

