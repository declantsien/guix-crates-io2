(define-module (crates-io iq s5 iqs5xx) #:use-module (crates-io))

(define-public crate-iqs5xx-0.1.0 (c (n "iqs5xx") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "1ll1p6rkac12yhr0vdw2c70iyyvnfmbl5gi0dhwgyplibshpi6wy")))

(define-public crate-iqs5xx-0.1.1 (c (n "iqs5xx") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0iyvybsfsjl1zdga3hgv338g05pp9gmgw974v6r5llh6023dwav6")))

(define-public crate-iqs5xx-0.1.2 (c (n "iqs5xx") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "053w5kql8598l3cwr42raijjdrcknpx2s759cml4fml74ajs3c5w")))

