(define-module (crates-io iq -b iq-bech32) #:use-module (crates-io))

(define-public crate-iq-bech32-0.0.1 (c (n "iq-bech32") (v "0.0.1") (d (list (d (n "clear_on_drop") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0mqyj8053v22b0q75bgjvsg25pin6yzbl02m8v32v18ilx5qli27") (y #t)))

(define-public crate-iq-bech32-0.1.0 (c (n "iq-bech32") (v "0.1.0") (d (list (d (n "clear_on_drop") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0vshfp1blfbrvz848jdrvlsgfq2qj2bi9jf9z89s9a44nzvi4sy9") (y #t)))

(define-public crate-iq-bech32-0.1.1 (c (n "iq-bech32") (v "0.1.1") (d (list (d (n "clear_on_drop") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1vs8b4jkqvhjhdxpcikp7jizg64xa2qyaym2c52w28b6arklpyj8") (y #t)))

(define-public crate-iq-bech32-0.0.999 (c (n "iq-bech32") (v "0.0.999") (h "13sw4zcaibr6vvdkhpn3cwswlg4380ahl15jjrnglpbqx7zw8wfv") (y #t)))

(define-public crate-iq-bech32-0.99.99 (c (n "iq-bech32") (v "0.99.99") (h "1sm5wr2qhfz3011378smyfzzkcwrxbk89igw23c3qjipjgxn55d2") (y #t)))

