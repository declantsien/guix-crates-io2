(define-module (crates-io iq km iqkms-signing) #:use-module (crates-io))

(define-public crate-iqkms-signing-0.0.0 (c (n "iqkms-signing") (v "0.0.0") (h "10wyrv8l70m90zb1qag5rnh5pxnnff66p61861w9673i5y151fn5")))

(define-public crate-iqkms-signing-0.0.1 (c (n "iqkms-signing") (v "0.0.1") (d (list (d (n "crypto") (r "^0.0.1") (f (quote ("ecdsa" "getrandom" "sha2" "std"))) (d #t) (k 0) (p "iq-crypto")) (d (n "types") (r "^0.0.1") (o #t) (d #t) (k 0) (p "iqkms-types")))) (h "1jq43jil6jnr33gapm0fxv78x2l2bcy5bpsrq4zzriwi9g83lkyw") (f (quote (("secp256k1" "crypto/secp256k1") ("ethereum" "crypto/sha3" "secp256k1" "types/ethereum")))) (r "1.64")))

