(define-module (crates-io iq km iqkms-proto) #:use-module (crates-io))

(define-public crate-iqkms-proto-0.0.0 (c (n "iqkms-proto") (v "0.0.0") (h "1rscnlfpppkhi8j2hqxivhg6hgam3kihlkwvsrbrinsdr4n0n4ci")))

(define-public crate-iqkms-proto-0.0.1 (c (n "iqkms-proto") (v "0.0.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "137krnhij713b537rdspak3x3gw0jvpyp283q3h8pjyp8h5ji2wh") (r "1.64")))

