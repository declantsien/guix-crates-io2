(define-module (crates-io iq km iqkms-ethereum) #:use-module (crates-io))

(define-public crate-iqkms-ethereum-0.0.0 (c (n "iqkms-ethereum") (v "0.0.0") (h "0zj9rv6fylsy5mv9fy22jpcifiw16c83labvhb93ny4vqvipi8yn")))

(define-public crate-iqkms-ethereum-0.0.1 (c (n "iqkms-ethereum") (v "0.0.1") (d (list (d (n "proto") (r "^0.0.1") (d #t) (k 0) (p "iqkms-proto")) (d (n "signing") (r "^0.0.1") (f (quote ("ethereum"))) (d #t) (k 0) (p "iqkms-signing")) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "types") (r "^0.0.1") (f (quote ("ethereum"))) (d #t) (k 0) (p "iqkms-types")))) (h "01xw51vaynpvq4vdppx1dqqym206bn4nmmgaj96b19lfqmp12cn1") (r "1.64")))

