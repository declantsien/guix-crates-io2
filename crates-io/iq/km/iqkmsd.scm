(define-module (crates-io iq km iqkmsd) #:use-module (crates-io))

(define-public crate-iqkmsd-0.0.0 (c (n "iqkmsd") (v "0.0.0") (h "1hd5qjjq2byr7z51rivi23wvz9xd749xg4zbcsxqyl8ci4m2ia4c")))

(define-public crate-iqkmsd-0.0.1 (c (n "iqkmsd") (v "0.0.1") (d (list (d (n "ethereum") (r "^0.0.1") (d #t) (k 0) (p "iqkms-ethereum")) (d (n "proto") (r "^0.0.1") (d #t) (k 0) (p "iqkms-proto")) (d (n "signing") (r "^0.0.1") (d #t) (k 0) (p "iqkms-signing")) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "0qwbi70b84pwn95cwlbx182cyqbjwgf1why29awhssh5pzdzdcam") (r "1.64")))

