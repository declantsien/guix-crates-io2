(define-module (crates-io iq km iqkms-types) #:use-module (crates-io))

(define-public crate-iqkms-types-0.0.0 (c (n "iqkms-types") (v "0.0.0") (h "1v097a5j98bn4822ygd8zf0fx2syh6vj8qzd9wc6r04lm7l34hj3") (y #t)))

(define-public crate-iqkms-types-0.0.1 (c (n "iqkms-types") (v "0.0.1") (d (list (d (n "crypto") (r "^0.0.1") (o #t) (d #t) (k 0) (p "iq-crypto")) (d (n "ethereum-types") (r "^0.13") (o #t) (k 0)) (d (n "hex") (r "^0.1") (f (quote ("alloc"))) (o #t) (d #t) (k 0) (p "base16ct")))) (h "1vwvqmhcjaxshxqnmvagx42y8rv5lrq2wvgp678v32kxilih0g9j") (f (quote (("ethereum" "crypto/secp256k1" "crypto/sha3" "ethereum-types" "hex")))) (r "1.64")))

