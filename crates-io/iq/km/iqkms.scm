(define-module (crates-io iq km iqkms) #:use-module (crates-io))

(define-public crate-iqkms-0.0.0 (c (n "iqkms") (v "0.0.0") (h "1i7cjqxy5hykdpfpva831grdg0y0rn57c9d2wsw762czh7amyk4g") (y #t)))

(define-public crate-iqkms-0.0.1 (c (n "iqkms") (v "0.0.1") (d (list (d (n "proto") (r "^0.0.1") (d #t) (k 0) (p "iqkms-proto")) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "types") (r "^0.0.1") (o #t) (d #t) (k 0) (p "iqkms-types")))) (h "1cg5fw1j46wia0hy5jg9f7h0k8nnhrwd0r4bzj7ffy3xsj090zhc") (f (quote (("ethereum" "types/ethereum")))) (r "1.64")))

