(define-module (crates-io up dl updlockfiles) #:use-module (crates-io))

(define-public crate-updlockfiles-0.1.0 (c (n "updlockfiles") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0adgnpj7bxl70964wgzqnz0c14by3gxfvcahfrjdvagmpwljzdqq")))

