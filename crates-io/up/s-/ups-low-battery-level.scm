(define-module (crates-io up s- ups-low-battery-level) #:use-module (crates-io))

(define-public crate-ups-low-battery-level-0.1.0 (c (n "ups-low-battery-level") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "online") (r "^3.0.1") (f (quote ("sync"))) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0sfzi49qxcsxd13dn8cmhrcmzppw723s1cwyg834hk7fygd03zll")))

(define-public crate-ups-low-battery-level-0.1.1 (c (n "ups-low-battery-level") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "online") (r "^3.0.1") (f (quote ("sync"))) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0k08jk459avvlpi0z1ixjawaiak77nfzk1dd1j14jaiyvzvdmxpr")))

