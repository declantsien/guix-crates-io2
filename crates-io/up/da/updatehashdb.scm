(define-module (crates-io up da updatehashdb) #:use-module (crates-io))

(define-public crate-updatehashdb-0.1.0 (c (n "updatehashdb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "libhashfindutils") (r "^0.1.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0gvginck41wyp0q6l3w2jlgrv6sd5hsvrl163i3dyi4l5xr87ql8")))

