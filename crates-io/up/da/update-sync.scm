(define-module (crates-io up da update-sync) #:use-module (crates-io))

(define-public crate-update-sync-0.1.0 (c (n "update-sync") (v "0.1.0") (d (list (d (n "update-sync_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0lzv7apf33qs9icyixw7xqqalhv6asnbhckvq479b8xisd661qy0") (f (quote (("derive" "update-sync_derive") ("default"))))))

