(define-module (crates-io up da update-iconfont) #:use-module (crates-io))

(define-public crate-update-iconfont-0.1.0 (c (n "update-iconfont") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.4") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tinyget") (r "^1.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (d #t) (k 0)))) (h "1kzfgkww41a05897why612qwc8dzc8v0ax7dy4sjwgaiq19lniim")))

