(define-module (crates-io up da update_me) #:use-module (crates-io))

(define-public crate-update_me-0.1.0 (c (n "update_me") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "079v8j4imwljdmv4kz27q7h3mzfn0bi0hl38xxrh72zz45g21wax")))

(define-public crate-update_me-0.2.0 (c (n "update_me") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "10135i62bi2rj6i65l21vjc0jn6r4mb26ld6600ckqwc8a400a9s")))

