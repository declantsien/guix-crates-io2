(define-module (crates-io up da updatehub-package-schema) #:use-module (crates-io))

(define-public crate-updatehub-package-schema-2.0.0 (c (n "updatehub-package-schema") (v "2.0.0") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "1zhk90qh14m2lmy4yhvxpkb1q13rmh34as6p7537k8n4zabc4975") (y #t)))

(define-public crate-updatehub-package-schema-2.0.1 (c (n "updatehub-package-schema") (v "2.0.1") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "0kcsk6421837b7rsskj72m7ql4ipcn27ny0jnzppf89kfn7yl5vz")))

(define-public crate-updatehub-package-schema-2.0.2 (c (n "updatehub-package-schema") (v "2.0.2") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "0mq7pcfccwf518ldr9iq88vdv8qyji7lv775z1rdwc04gs4l7884")))

(define-public crate-updatehub-package-schema-2.0.3 (c (n "updatehub-package-schema") (v "2.0.3") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "15nvr7bxi7f73nm61ra0vkagaj0zllqnxhvgb5xnrzy9my06cqnf")))

(define-public crate-updatehub-package-schema-2.0.4 (c (n "updatehub-package-schema") (v "2.0.4") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "=1.0.128") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "0rrs7w01l9njj2zlp9y7n50k3vv8c82h59k37dvd1w00lgg5f16p")))

(define-public crate-updatehub-package-schema-2.0.5 (c (n "updatehub-package-schema") (v "2.0.5") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "=1.0.128") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "0qj4m9dsdcwyln3dwrynkrprfl411assbipk2xggrhhvlf7f3r47")))

(define-public crate-updatehub-package-schema-2.0.6 (c (n "updatehub-package-schema") (v "2.0.6") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "=1.0.128") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "1zg8hwqb4hyhmsh98dqv365h3fp711fkfyr9xqn70qby4drvzj71")))

(define-public crate-updatehub-package-schema-2.1.0 (c (n "updatehub-package-schema") (v "2.1.0") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive" "std"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "1xg749q6g3jgj8c6w8s17d881wr86nzzbkl9bgxm82gkm4l8z7kr")))

(define-public crate-updatehub-package-schema-2.1.1 (c (n "updatehub-package-schema") (v "2.1.1") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive" "std"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "1l2gxc6qr9j6n6raggawqwz6i0zy5b4zcpn56f310bpinc0dxa1p")))

(define-public crate-updatehub-package-schema-2.1.3 (c (n "updatehub-package-schema") (v "2.1.3") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive" "std"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "01ilda9qbkvhsbpc3cyr07442ycrs4kya1kw6ki044hnpvcs6jjv")))

(define-public crate-updatehub-package-schema-2.1.4 (c (n "updatehub-package-schema") (v "2.1.4") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive" "std"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 2)))) (h "1yvk43ybpknj08ssdk6lgaan1cni5s146alif91kddb0frr3r1bp")))

