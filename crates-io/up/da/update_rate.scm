(define-module (crates-io up da update_rate) #:use-module (crates-io))

(define-public crate-update_rate-1.0.0 (c (n "update_rate") (v "1.0.0") (h "0sw8m4z28fa5rn4n23l9z8gwqj92f71fq5bp8q8jbyyzjx82nhsa")))

(define-public crate-update_rate-1.0.1 (c (n "update_rate") (v "1.0.1") (h "1d1yi94rnanxrk33pn81kyrr7c5diqrhn3sy53q0k8rd3wmf7rd3")))

(define-public crate-update_rate-1.1.0 (c (n "update_rate") (v "1.1.0") (h "1kzddv40ibf4grq29hdsx1g8kqk44f7fbfwibf9r9csas3xy415n")))

(define-public crate-update_rate-2.0.0 (c (n "update_rate") (v "2.0.0") (h "1n6ipcsfkp7hbjlna6qgx8m61yjgijh30wjyrkcaf407xv5m8hyd")))

(define-public crate-update_rate-2.0.1 (c (n "update_rate") (v "2.0.1") (h "1qw7mmlhjwllrrh9284ckjwch0w4hnl7akj46r5s2a4klsfgf1gp")))

