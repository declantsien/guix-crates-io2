(define-module (crates-io up da update-rust-toolchain) #:use-module (crates-io))

(define-public crate-update-rust-toolchain-0.1.0 (c (n "update-rust-toolchain") (v "0.1.0") (h "1b44dg581pm8appxbdpjyywpr9fndvm05vz42kg3z68z5672qz0p")))

(define-public crate-update-rust-toolchain-0.1.1 (c (n "update-rust-toolchain") (v "0.1.1") (h "0h4a779kfp5w614v4vrmsfhkfp7pq9zzyxgg5ahazzkskkhcn18r")))

