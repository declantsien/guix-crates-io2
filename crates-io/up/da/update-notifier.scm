(define-module (crates-io up da update-notifier) #:use-module (crates-io))

(define-public crate-update-notifier-0.1.0 (c (n "update-notifier") (v "0.1.0") (d (list (d (n "crates_io_api") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0lvknjw79n30j21gzj52xlq8pwpln2shy8c384k2ajbwfn1l89hl")))

(define-public crate-update-notifier-0.1.1 (c (n "update-notifier") (v "0.1.1") (d (list (d (n "crates_io_api") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c4lxwy71fpbcx6mxqv56iqzvzrr8lcnz6x6q9kqyq5nz5x65lzq")))

(define-public crate-update-notifier-0.1.2 (c (n "update-notifier") (v "0.1.2") (d (list (d (n "crates_io_api") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fb3c2r36ri814ly1j3fimk5c025a35lkh0inhyf45f9w5a16cfb")))

(define-public crate-update-notifier-0.1.3 (c (n "update-notifier") (v "0.1.3") (d (list (d (n "crates_io_api") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0155aw2fpvh28i6cnidq8kwpgs9akj4qklzggcklkp7fmsarmq8q")))

(define-public crate-update-notifier-0.1.4 (c (n "update-notifier") (v "0.1.4") (d (list (d (n "crates_io_api") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13dyavcsybwydq79ymmd55ms2drvxn89rc1gbk1wap8gifmmlhg6")))

(define-public crate-update-notifier-0.1.5 (c (n "update-notifier") (v "0.1.5") (d (list (d (n "crates_io_api") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yl55nvk1a91w4qmlqghlpxnhc679qcn1d37gvvbdpsla37rgnzf")))

(define-public crate-update-notifier-0.1.6 (c (n "update-notifier") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "configstore") (r "^0.1.1") (d #t) (k 0)) (d (n "mockito") (r "^0.25.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "129vkgk1pk4w71l5mp39vqckzqijxi6n38365bmnprjcahbvwbb5")))

(define-public crate-update-notifier-0.1.7 (c (n "update-notifier") (v "0.1.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "configstore") (r "^0.1.1") (d #t) (k 0)) (d (n "mockito") (r "^0.25.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zxd2imdqmbqmzscc35sg5n1k77h241kwfxcn9y8ia3l0b3i6rbr")))

(define-public crate-update-notifier-0.1.8 (c (n "update-notifier") (v "0.1.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "configstore") (r "^0.1.1") (d #t) (k 0)) (d (n "mockito") (r "^0.25.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19sgjr1wqa47z0icp1vg9p439d4vvlsdcpclsgw02ihy75nccwlq")))

(define-public crate-update-notifier-0.1.9 (c (n "update-notifier") (v "0.1.9") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "configstore") (r "^0.1.1") (d #t) (k 0)) (d (n "curl") (r "^0.4.30") (d #t) (k 0)) (d (n "mockito") (r "^0.25.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c2jc1ss3ndd3kyqyn071510k3w99vkln1vr7rqpjzh98q7xw4i8")))

