(define-module (crates-io up da update_channel) #:use-module (crates-io))

(define-public crate-update_channel-0.1.0 (c (n "update_channel") (v "0.1.0") (h "1rmd54myxvm3p2g2lgapz2xl9n12qny438kwqczdnc9mb3hdyrzp")))

(define-public crate-update_channel-0.1.1 (c (n "update_channel") (v "0.1.1") (h "11lv9r852riqdmk4x5d3gabbwn9yzzh2gx9k4hjhp9bf9fzg1dvk")))

(define-public crate-update_channel-0.1.2 (c (n "update_channel") (v "0.1.2") (h "0lsd2h6can18c2dsclm2ps39si8d8ys0jh9sfl6h90dw1d4lpcz4")))

(define-public crate-update_channel-0.1.3 (c (n "update_channel") (v "0.1.3") (h "0wgbg36g4vbwfaap5mkyz3vn8j8ci6hrdabdhnd8r57ilbhbl6bs")))

