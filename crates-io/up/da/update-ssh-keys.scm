(define-module (crates-io up da update-ssh-keys) #:use-module (crates-io))

(define-public crate-update-ssh-keys-0.3.0 (c (n "update-ssh-keys") (v "0.3.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "openssh-keys") (r "^0.3") (d #t) (k 0)) (d (n "users") (r "^0.7") (d #t) (k 0)))) (h "08snj0pzl72h80rj83b7n11i8md3j20kmqm1n1v5j7g4309xy78d")))

(define-public crate-update-ssh-keys-0.4.0 (c (n "update-ssh-keys") (v "0.4.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "openssh-keys") (r "^0.4") (d #t) (k 0)) (d (n "users") (r "^0.8") (d #t) (k 0)))) (h "08mz376csvi1bwn1vazgq1pd3a9b2pfnf686m6hsnc5vagyjac5x")))

(define-public crate-update-ssh-keys-0.4.1 (c (n "update-ssh-keys") (v "0.4.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "openssh-keys") (r "^0.4") (d #t) (k 0)) (d (n "users") (r "^0.8") (d #t) (k 0)))) (h "107nijzfiy9x3zaylgwlgyzz3pbhxl213ma8zppl1l1f1bgxxfjr")))

(define-public crate-update-ssh-keys-0.5.0 (c (n "update-ssh-keys") (v "0.5.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "openssh-keys") (r "^0.4") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "0p03i83hkzj2sfcnqh94j2l8ygm50wfs8c8wz2b92ixz10cck602")))

(define-public crate-update-ssh-keys-0.6.0 (c (n "update-ssh-keys") (v "0.6.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "openssh-keys") (r "^0.4") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "0f5gn8jwcx0yxr5w327sib7n9zqfaf1dgd2rrp8zjsinqvxxhbaa")))

(define-public crate-update-ssh-keys-0.6.1 (c (n "update-ssh-keys") (v "0.6.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "openssh-keys") (r "^0.4") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "0gnzvw0cr7cv8a8kw4kj398vw1lizkchb697y73fplxfv86qy0sy")))

(define-public crate-update-ssh-keys-0.7.0 (c (n "update-ssh-keys") (v "0.7.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "openssh-keys") (r "^0.4") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "0knkkgwk35w029np9nmnm3lzbd4xncdfyba0piw7sq681hssl87q")))

