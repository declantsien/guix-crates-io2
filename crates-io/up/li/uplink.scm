(define-module (crates-io up li uplink) #:use-module (crates-io))

(define-public crate-uplink-0.0.1 (c (n "uplink") (v "0.0.1") (h "03i6qraw51l384khx628p1bmnmx0lxwgq04axcir37p3r2006sph")))

(define-public crate-uplink-0.0.2 (c (n "uplink") (v "0.0.2") (h "0lw1sdbg354w06hgffzhyjp235n5snqjvmk0s4rqdf91pimp1r3z")))

(define-public crate-uplink-0.1.0 (c (n "uplink") (v "0.1.0") (d (list (d (n "uplink-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0y9lss5cfg40c6nkyvqbmjcp2plnk0s2xf142d143bfy76ban32l")))

(define-public crate-uplink-0.1.1 (c (n "uplink") (v "0.1.1") (d (list (d (n "uplink-sys") (r "^0.5.1") (d #t) (k 0)))) (h "0a001cq9h91823jw05syzbjdvbqywmc9rd5mmbqcp6qp6q4d30ml")))

(define-public crate-uplink-0.2.0 (c (n "uplink") (v "0.2.0") (d (list (d (n "uplink-sys") (r "^0.5.2") (d #t) (k 0)))) (h "0vwcq3xw26q3i706qpk2gpimkabsym75brlbpmsqsh4piwg7vv5l")))

(define-public crate-uplink-0.3.0 (c (n "uplink") (v "0.3.0") (d (list (d (n "uplink-sys") (r "^0.5.2") (d #t) (k 0)))) (h "07yssvwyvq1ljvnc4kd4813g4m9h1x090hrcifkpiyfirq1540zk")))

(define-public crate-uplink-0.4.0 (c (n "uplink") (v "0.4.0") (d (list (d (n "uplink-sys") (r "^0.5.2") (d #t) (k 0)))) (h "02nw4rmz6gwbpnkzcpagm73h0mpi7mcnfdsw7s321xv8h7q4i8v5")))

(define-public crate-uplink-0.5.0 (c (n "uplink") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "uplink-sys") (r "^0.5.2") (d #t) (k 0)))) (h "1j00d2cps1s61dmj0f757j7sxi399y5jgfijn10pgx3qdmnwjz3v")))

(define-public crate-uplink-0.6.0 (c (n "uplink") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "uplink-sys") (r "^0.5.2") (d #t) (k 0)))) (h "0d2dlcdg9b3qycw65rgi800b8jmm7xn8wxlhxq5pmah70xcd15zc")))

(define-public crate-uplink-0.7.0 (c (n "uplink") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "uplink-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0bij9yjr6abqmx9w7s832n60vgiq1s1k774h6rrdq15zd93rjbyr")))

(define-public crate-uplink-0.8.0 (c (n "uplink") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "uplink-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0kbyix8wl1hw5m6i14r8h27n1pdf7ma992kb02l1gsv31dxx9lgw")))

(define-public crate-uplink-0.8.1 (c (n "uplink") (v "0.8.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "uplink-sys") (r "^0.6.1") (d #t) (k 0)))) (h "03cf8pnsn9x2j39kk6rh7xq21irg756y98jgjfra4068acm56ppz")))

