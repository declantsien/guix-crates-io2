(define-module (crates-io up li uplink-sys) #:use-module (crates-io))

(define-public crate-uplink-sys-0.1.0 (c (n "uplink-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "16ba32if6b41cwn0qb88cy848d4sm2d9b69wg218429r0nwa244m")))

(define-public crate-uplink-sys-0.1.1 (c (n "uplink-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "0jgsz61f8cd5l19gi5n3yq7fykm6rsikczzmyry7a4mahq97lpv5")))

(define-public crate-uplink-sys-0.2.0 (c (n "uplink-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "1ax0w83lc58pr0n52r2jd7kb88sznv632n22cbn7hsnd5s639xxf") (l "uplink")))

(define-public crate-uplink-sys-0.3.0 (c (n "uplink-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0lvrr2cyx1dpw57wkm80aqsf9hx3d4zj8396qqjfp8an07hkc6i8") (l "uplink")))

(define-public crate-uplink-sys-0.4.0 (c (n "uplink-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1x4lrflny02f1p59s5n6wy82ijlh9xxn22hk33q3pmjbmhx9zf54") (l "uplink")))

(define-public crate-uplink-sys-0.5.0 (c (n "uplink-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1mplqxg61asgfsbn5r83njb9g6bvd2s2xcgf1iw4h7g1wprqrnxp") (l "uplink")))

(define-public crate-uplink-sys-0.5.1 (c (n "uplink-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0dz3i4vnqhhb21ypdhnibhyfnwx9hzrzdzg8xp96im4g4a0xsdwh") (l "uplink")))

(define-public crate-uplink-sys-0.5.2 (c (n "uplink-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0ar8s31q9bvbvpxxmzv8j4g7y5inww590zj1aibh6n33fkpmdprk") (l "uplink")))

(define-public crate-uplink-sys-0.6.0 (c (n "uplink-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "02zci8jz3s9x7782mhdfxbwbza4rxnfg45fgh7q12brrimvas6xh") (l "uplink")))

(define-public crate-uplink-sys-0.6.1 (c (n "uplink-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "0lnzbs6rpaj6kl5yr6jammp2lhrdaw2cldk830f9i4wdj28vfdb8") (l "uplink")))

