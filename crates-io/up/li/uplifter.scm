(define-module (crates-io up li uplifter) #:use-module (crates-io))

(define-public crate-uplifter-0.1.0 (c (n "uplifter") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17i9n8bmdci9n2ga70797jnxhbnms2iflxj81cj0f6904bxb301m") (r "1.57.0")))

