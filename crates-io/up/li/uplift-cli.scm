(define-module (crates-io up li uplift-cli) #:use-module (crates-io))

(define-public crate-uplift-cli-0.1.0 (c (n "uplift-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "time" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0lfwpfqnpkr79whpwd9199k1040wwkif7g5hqgxxpyyrpjdbs7xh")))

