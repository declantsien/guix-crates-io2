(define-module (crates-io up db updb) #:use-module (crates-io))

(define-public crate-updb-0.1.0 (c (n "updb") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "imbl") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0dw4s9xiz0zj37brnkymav5mmrahh0v5hbawglsvx88a4a36b3sl")))

