(define-module (crates-io up lo uplock) #:use-module (crates-io))

(define-public crate-uplock-0.1.0 (c (n "uplock") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1wwp9fc06z0m07zslhra0clw47a67q8jrsp533jswn2si0zdlyik")))

(define-public crate-uplock-0.1.1 (c (n "uplock") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0vwx0ry1n89d1hpp1bcfdbpprvcjmkcplbw7l61qcpvl0vrzk9fp") (y #t)))

(define-public crate-uplock-0.1.2 (c (n "uplock") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "158dmc22vdvfd513b8kpsh9311h3griv21g05ishyi454y9khiik")))

(define-public crate-uplock-0.1.3 (c (n "uplock") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0xbkz7rsw0812caz9b7m97vdhhh1cyykpn8cfj7cfx70nmd5dx23") (y #t)))

(define-public crate-uplock-0.1.4 (c (n "uplock") (v "0.1.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1pv215frhzlagpdip183v4l5nb9qmisxfs3qhvi3vf6gakah4s68")))

(define-public crate-uplock-0.1.5 (c (n "uplock") (v "0.1.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "096xmh1n6qn8y8rqz5sdfa2lm7812zrs5xyi3f4s6zvmgp6b69vp")))

