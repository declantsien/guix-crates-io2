(define-module (crates-io up lo uploader_canister) #:use-module (crates-io))

(define-public crate-uploader_canister-0.1.0 (c (n "uploader_canister") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.146") (d #t) (k 0)))) (h "1c4jm14gz9h78bnccrssyccxjcxjvh6vn0yjwdd6mcv4z61d0fi7")))

(define-public crate-uploader_canister-0.1.1 (c (n "uploader_canister") (v "0.1.1") (d (list (d (n "candid") (r "^0.8.3") (d #t) (k 0)) (d (n "canistergeek_ic_rust") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.146") (d #t) (k 0)))) (h "1k2z5p09085ki3p5h9csba52hn9kgx82qrgbgnmn3lqndmx49m1z")))

(define-public crate-uploader_canister-0.2.0 (c (n "uploader_canister") (v "0.2.0") (d (list (d (n "candid") (r "^0.9.1") (d #t) (k 0)) (d (n "canistergeek_ic_rust") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.146") (d #t) (k 0)))) (h "0d7fmrz0aar38ng315h467qdn1h2ibji1p60ijf5jljjbjjgs53z")))

