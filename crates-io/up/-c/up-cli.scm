(define-module (crates-io up -c up-cli) #:use-module (crates-io))

(define-public crate-up-cli-0.1.0 (c (n "up-cli") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1v23yr8mbn46m4iyw45ar54lmjyq9p2ldw35gr3wwylwinywzr1d")))

(define-public crate-up-cli-0.1.1 (c (n "up-cli") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pvydi3wcdvjjkqd7cpc0517ilq2k0s100wi8glq01lx2r2x99g7")))

(define-public crate-up-cli-0.1.2 (c (n "up-cli") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1z519gg24nngysarixya5x9p83c8hjhi0xq7qf50byahd9fhwmli")))

