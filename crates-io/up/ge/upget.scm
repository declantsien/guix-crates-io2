(define-module (crates-io up ge upget) #:use-module (crates-io))

(define-public crate-upget-0.1.0 (c (n "upget") (v "0.1.0") (h "1kbb1a68v80vc5fnkbvyd149wyvms1b9kaz0va39j0sx3qvr7yin")))

(define-public crate-upget-0.1.1 (c (n "upget") (v "0.1.1") (h "1vmagsdr5q22qh0rg6756n4apq4mf5z529w55rfn2nk0a2s725ld")))

