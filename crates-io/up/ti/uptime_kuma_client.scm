(define-module (crates-io up ti uptime_kuma_client) #:use-module (crates-io))

(define-public crate-uptime_kuma_client-0.1.2 (c (n "uptime_kuma_client") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0qa2yqdpspy4m320l2i748ac9wvbwh727dpwaapksc7xfizrbk2v")))

(define-public crate-uptime_kuma_client-0.1.3 (c (n "uptime_kuma_client") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "14vkngikb20pax2rans4qblgk61xq2gyzkcxf9b4izraii6129f9")))

(define-public crate-uptime_kuma_client-0.1.4 (c (n "uptime_kuma_client") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1w4c8sbanx3y347ny2sbah0mb54c8b6q5gb9r9kszl1bpv4ajsjs")))

(define-public crate-uptime_kuma_client-0.1.5 (c (n "uptime_kuma_client") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1fi1z1g1qzq49g4zrzmba9b6drz27cy4qgwcjnq80i21dmb2ffih")))

(define-public crate-uptime_kuma_client-0.1.6 (c (n "uptime_kuma_client") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0f6f8r8iqpzhw2wjchgxknyag9f7di43x14r0nx3zhln59j37bis")))

(define-public crate-uptime_kuma_client-0.1.7 (c (n "uptime_kuma_client") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0109rr9ky129q4brxnzvy6hhbjlxfdwx61cm462zxjn0mkc4kagn")))

(define-public crate-uptime_kuma_client-0.1.8 (c (n "uptime_kuma_client") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0bj4yjdjyw5y2vg3h8jz18fh8xd01hhh2nivk9i8g2yj4a2mcpsa")))

(define-public crate-uptime_kuma_client-0.1.9 (c (n "uptime_kuma_client") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "14rgsnpdxq6m79swzzw81jvnmd4xn6kyiphm4j9jijqimba0nl38")))

(define-public crate-uptime_kuma_client-0.1.10 (c (n "uptime_kuma_client") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1jnz8p1jl05p2chz9gnb8rh2rjz6cnjigkyqddl7nciaa1jbmxqj")))

