(define-module (crates-io up ti uptime_lib) #:use-module (crates-io))

(define-public crate-uptime_lib-0.0.0 (c (n "uptime_lib") (v "0.0.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1qqz5b0jc622bs8pbkk9z0qipqa0x37s70mx3ldbkldfni6jzik5")))

(define-public crate-uptime_lib-0.1.0 (c (n "uptime_lib") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "03ndbk1fqc3nbdslg7633jq0bwfla062pk54c3xsh0s4z43n0kh7")))

(define-public crate-uptime_lib-0.2.0 (c (n "uptime_lib") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0m46mq640k7yycraxjdb3vz2x8wvm5scm7p8i47f1264hin6crd0")))

(define-public crate-uptime_lib-0.2.1 (c (n "uptime_lib") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (t "cfg(not(windows))") (k 0)))) (h "043gfwbq3qjyl3fb2js5lq43di7c98ldl9i32ack5sqxwid7lq6w")))

(define-public crate-uptime_lib-0.2.2 (c (n "uptime_lib") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.126") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_System_SystemInformation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zr365bl8g00hsh2nr9lmprd8x8p81qrxb7fpkwngy91p9cxn2fb")))

(define-public crate-uptime_lib-0.3.0 (c (n "uptime_lib") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_System_SystemInformation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wq046ny55nm7sb9c8ikzn8vnsv09vd6l7c2h5c6v1gdxzdivrzl")))

