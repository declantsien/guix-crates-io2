(define-module (crates-io up ca upcast-arithmetic) #:use-module (crates-io))

(define-public crate-upcast-arithmetic-0.0.0 (c (n "upcast-arithmetic") (v "0.0.0") (h "07jvqi5w53ywlm5gqm99bmvjm5cqhvcx40w9pl169xrldyl85jxl")))

(define-public crate-upcast-arithmetic-0.0.1 (c (n "upcast-arithmetic") (v "0.0.1") (h "10w0dms9x3flg6xh99zi9anmr6q9v0v4y9fadz2x38c6wal4g81z")))

(define-public crate-upcast-arithmetic-0.0.2 (c (n "upcast-arithmetic") (v "0.0.2") (h "1h7jv44s6wr0jf595nd1cjxfr1fny11nqqcknqpna689bl45vqg5")))

(define-public crate-upcast-arithmetic-0.1.0 (c (n "upcast-arithmetic") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "069x4y3f4iwcxz6hvryynb1vrlwl7av735imldzkxfdhk3api6jy")))

(define-public crate-upcast-arithmetic-0.1.1 (c (n "upcast-arithmetic") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "09cs2p5vfizda398bl27laf8z9annvm8wvww9sj0x7791rfprb9q") (f (quote (("default" "const") ("const"))))))

(define-public crate-upcast-arithmetic-0.1.2 (c (n "upcast-arithmetic") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "08frlikx5a8lmjh1hnj17i358rbv9d5jrxg9v42ii317nrkrd8v3") (f (quote (("default" "const") ("const"))))))

