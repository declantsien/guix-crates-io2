(define-module (crates-io up ai upair) #:use-module (crates-io))

(define-public crate-upair-0.1.0 (c (n "upair") (v "0.1.0") (h "06jr583ip8w537lr1w6x7dnk1gd8xaz0gr2gkqkwlvs4y8clp3n4")))

(define-public crate-upair-0.1.1 (c (n "upair") (v "0.1.1") (h "18wb2pjmc9jbrhw1cpspj0cm3fzl5ma2sqh2w466jy6jdb9j585w")))

