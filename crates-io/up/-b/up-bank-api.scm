(define-module (crates-io up -b up-bank-api) #:use-module (crates-io))

(define-public crate-up-bank-api-0.1.0 (c (n "up-bank-api") (v "0.1.0") (d (list (d (n "restson") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)))) (h "00hswl871sadffnfybww7x92azzjrcvm64y9rl0zwz0fng221ly4")))

(define-public crate-up-bank-api-0.1.1 (c (n "up-bank-api") (v "0.1.1") (d (list (d (n "restson") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)))) (h "0cnpxyykqqpqf8nxh3v1yp4bgjqs7883w6c4f1p6fagkn6zp5v7k")))

(define-public crate-up-bank-api-0.1.2 (c (n "up-bank-api") (v "0.1.2") (d (list (d (n "restson") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 2)))) (h "1mljnzgj7r0vgmqn7slc68zix04y5g73zvv7q7s3jzgsqyniqwqb")))

