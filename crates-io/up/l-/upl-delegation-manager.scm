(define-module (crates-io up l- upl-delegation-manager) #:use-module (crates-io))

(define-public crate-upl-delegation-manager-0.1.0 (c (n "upl-delegation-manager") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "19y44anlg7ihy9b03n71cmgz0rgswgqqnyjgmsqaffgy9nysz8px") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-upl-delegation-manager-0.1.1 (c (n "upl-delegation-manager") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "0kqsqg4h77azfa7jc5l11bvxna0ah652i8pqh6jzw1hz4rmffywh") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

