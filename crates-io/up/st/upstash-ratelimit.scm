(define-module (crates-io up st upstash-ratelimit) #:use-module (crates-io))

(define-public crate-upstash-ratelimit-0.1.0 (c (n "upstash-ratelimit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "redis") (r "^0.23.0") (d #t) (k 0)))) (h "0l39y9l7wl2zrgignjn6c8xakg1mbcdw1g9imxfmlmwyyfyiqxib")))

