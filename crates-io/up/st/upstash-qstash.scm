(define-module (crates-io up st upstash-qstash) #:use-module (crates-io))

(define-public crate-upstash-qstash-0.1.0 (c (n "upstash-qstash") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "165klcwkgvsnbd0rnr4nxaifpfinpm1ii4mycavvbjr9z9szzrgi")))

(define-public crate-upstash-qstash-0.1.1 (c (n "upstash-qstash") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0ri1c3x2nl2xvw9zg1bprawfmnw8fpywzfsnbkvnq688vpr8p45s")))

(define-public crate-upstash-qstash-0.1.2 (c (n "upstash-qstash") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0rvamvxal3nd82kflzznlxi7nswmbi27awdw4hglx17pnzqdalvy")))

(define-public crate-upstash-qstash-0.2.0 (c (n "upstash-qstash") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "103apqdvwlc4w7qgzya5gp7lqclw3bybf1i20yff37bh2sgp5p7b")))

(define-public crate-upstash-qstash-0.3.0 (c (n "upstash-qstash") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "05sd0q7a9652a21ha36byjqf0x0jgdzw579v19022ma5wj115pmq")))

