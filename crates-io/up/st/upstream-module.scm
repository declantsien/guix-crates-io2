(define-module (crates-io up st upstream-module) #:use-module (crates-io))

(define-public crate-upstream-module-0.2.0 (c (n "upstream-module") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "module-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "pingora-core") (r "^0.2.0") (d #t) (k 0)) (d (n "pingora-proxy") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0wc3bdn6jjdzyykgplrpnsshr0mqpawhx6g9s0q7dmavxrj0wrvi")))

