(define-module (crates-io up ve upversion) #:use-module (crates-io))

(define-public crate-upversion-0.1.0 (c (n "upversion") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "insta") (r "^1") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mockito") (r "^0.31") (d #t) (k 2)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^1") (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yql4jz7jbhpgyxq47ijn0ingy9haixxd8dqvfq25xkfria4kxz6")))

