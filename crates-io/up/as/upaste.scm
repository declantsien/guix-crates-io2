(define-module (crates-io up as upaste) #:use-module (crates-io))

(define-public crate-upaste-0.1.0 (c (n "upaste") (v "0.1.0") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1v69jlaxq8yb5fwmdmsgaxhxmbzcshf1q7grygzci43q4n550fxf")))

(define-public crate-upaste-0.2.0 (c (n "upaste") (v "0.2.0") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1nm90dmynar2syx91drh8m3mx4xv4wym7y7r29d38cvmjbkpyav1")))

(define-public crate-upaste-0.2.1 (c (n "upaste") (v "0.2.1") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0b3i8fihrn83xcs3gr1jy0abfhx2y415wd2cmhznhi1k96nyb3vq")))

(define-public crate-upaste-0.2.2 (c (n "upaste") (v "0.2.2") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "18a8nxwrd3ncvpq6xk2a97p0ca14s3d1hi4khhqk4vl3c0ifsr0d")))

(define-public crate-upaste-0.2.3 (c (n "upaste") (v "0.2.3") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0pdz7bkf05z06bklyxkm6hwlfgzax6mij8n2df8d04pqq4x4q65i")))

(define-public crate-upaste-0.2.4 (c (n "upaste") (v "0.2.4") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0m2qk21my473vfa74jzpv39nhcxhf1d6v4mdmkcggxr53azhg51d")))

(define-public crate-upaste-0.2.5 (c (n "upaste") (v "0.2.5") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "158kc0wp4a9d115d2if78x7j5nz4c8myklrbncwwnnc27wzz7vkw")))

(define-public crate-upaste-0.2.6 (c (n "upaste") (v "0.2.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0wfzpi39l4vk5smfqcd5wiw2cd0bs79xg81cavlayp4klkr5mxli")))

(define-public crate-upaste-0.2.7 (c (n "upaste") (v "0.2.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1x69z5pbs093g9yb5ngqr8wnnjgvgwbn82qq7qna2h34a481cma8")))

(define-public crate-upaste-0.3.0 (c (n "upaste") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0kinnnjgglnp3kzqb98l919n52xppalaj26brvnaa0fm8xbkzmap")))

