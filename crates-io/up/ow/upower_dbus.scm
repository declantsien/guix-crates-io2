(define-module (crates-io up ow upower_dbus) #:use-module (crates-io))

(define-public crate-upower_dbus-0.1.0 (c (n "upower_dbus") (v "0.1.0") (d (list (d (n "dbus") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1dx9kafy328dgr286cb30v81gsmm65xpy55vnj7p9zg6bxain4mq")))

(define-public crate-upower_dbus-0.2.0 (c (n "upower_dbus") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.0.0") (d #t) (k 0)))) (h "1y6xdqzmq2hsmb0lngpx4328s9fcry8zp3rr1df34x6fqnfg7skw")))

(define-public crate-upower_dbus-0.3.0 (c (n "upower_dbus") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "zbus") (r "^3.2.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.7.1") (d #t) (k 0)))) (h "12xjf45x30q7crv75rjzcrna42iyhwq0gkqdwlsf7yp3x849d8x3")))

(define-public crate-upower_dbus-0.3.1 (c (n "upower_dbus") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 2)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "zbus") (r "^3.6.1") (d #t) (k 0)))) (h "1lxm0hbms0f4rxx69lycvk6w52fszdrhaqgv27zy5lvf7g3rmnia")))

(define-public crate-upower_dbus-0.3.2 (c (n "upower_dbus") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.10") (d #t) (k 0)) (d (n "zbus") (r "^3.7.0") (d #t) (k 0)))) (h "133g244g12acdzqhgkawkv2czvn4p12v18jzgwcfbclax5yxgcdh")))

