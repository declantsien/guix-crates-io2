(define-module (crates-io up m_ upm_lib) #:use-module (crates-io))

(define-public crate-upm_lib-0.2.0 (c (n "upm_lib") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0cg4208280b7s5p1jvds41m0xrk36557s2v99zl4vg8bi704asww")))

(define-public crate-upm_lib-0.3.0 (c (n "upm_lib") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0a74ca6rvrh02f8fhiv418xjxxd68d1nfmy3r0rsibcxcs09w9ld")))

