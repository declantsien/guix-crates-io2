(define-module (crates-io up to upto) #:use-module (crates-io))

(define-public crate-upto-0.1.3 (c (n "upto") (v "0.1.3") (h "17wjl5nwmx5r34wgjhsj2jpjly8d0zmdlcsy9jjysqdrm32sp5db")))

(define-public crate-upto-0.1.4 (c (n "upto") (v "0.1.4") (h "1m6lpnwxd1m4cw0kc8lr5pyrlpmh4iq4bfjlwl47nxs6hl4xx0jl")))

(define-public crate-upto-0.1.5 (c (n "upto") (v "0.1.5") (h "054m736x6jxqmpsp1r199lx20yz7jh9a66b4hd9iqwc5n9wq0mxg")))

