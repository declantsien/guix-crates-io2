(define-module (crates-io up to uptown_funk_macro) #:use-module (crates-io))

(define-public crate-uptown_funk_macro-0.1.1 (c (n "uptown_funk_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing" "clone-impls" "fold"))) (d #t) (k 0)))) (h "1ph1xnpk1yim1kvrxq6brcsysh67g58xd12ibng1l1b1zjb2xr4p") (f (quote (("vm-wasmtime") ("vm-wasmer"))))))

(define-public crate-uptown_funk_macro-0.1.2 (c (n "uptown_funk_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing" "clone-impls" "fold"))) (d #t) (k 0)))) (h "04903fgmjxj40aq7zll4wdfbwzv34y8kidf9gz5cppmdzs902z44") (f (quote (("vm-wasmtime") ("vm-wasmer"))))))

