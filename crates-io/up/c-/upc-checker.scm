(define-module (crates-io up c- upc-checker) #:use-module (crates-io))

(define-public crate-upc-checker-0.1.1 (c (n "upc-checker") (v "0.1.1") (h "01n0i4v26ldcphyyk34pqlilz4m6b5589k1fgpfnpfhb7mbwrq0x")))

(define-public crate-upc-checker-0.1.2 (c (n "upc-checker") (v "0.1.2") (h "1gq4bw1lzhqrs362n2py3g3jrl86rkcbxpbsqpx53nk3ci41ap9s")))

(define-public crate-upc-checker-0.1.4 (c (n "upc-checker") (v "0.1.4") (h "1mh7fr4iqmiilh78sihlpb63j92dg6j918g9bvk602is6810ziln")))

(define-public crate-upc-checker-0.1.5 (c (n "upc-checker") (v "0.1.5") (h "16b8x6qwh320szd8sydapchcblj78qigbkgp9akixvmfjqgnr1rh")))

