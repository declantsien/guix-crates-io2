(define-module (crates-io up dp updpkg) #:use-module (crates-io))

(define-public crate-updpkg-0.0.0 (c (n "updpkg") (v "0.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "05dyr2d7rwghws84zm6r7zqh6bvbal7fd3xjshyl7w146g2jd01c") (r "1.70.0")))

(define-public crate-updpkg-0.1.0 (c (n "updpkg") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "0i7fp418yc90qpmxp5v00c5a5xqnj9bci2r7lsrl2rqq8xd1ma3q") (r "1.70.0")))

(define-public crate-updpkg-0.2.0 (c (n "updpkg") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1ih53dixxkx3pgc8vwvslfyvg2xvda7y3n15pg7zqa3kslwdz4gk") (f (quote (("sd") ("default" "sd")))) (r "1.70.0")))

(define-public crate-updpkg-0.2.1 (c (n "updpkg") (v "0.2.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "0q383078rvy2svg09vwlwxf3jz9jppig4d4hdzny6wgy52ac11x0") (f (quote (("sd") ("default" "sd")))) (r "1.70.0")))

(define-public crate-updpkg-0.2.2 (c (n "updpkg") (v "0.2.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "0xkvlqvbkdqs702qyp73s4yaivan6sajyiwwkhzpv9qvxz8yddn8") (f (quote (("sd") ("default" "sd")))) (r "1.70.0")))

