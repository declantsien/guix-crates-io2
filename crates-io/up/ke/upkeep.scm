(define-module (crates-io up ke upkeep) #:use-module (crates-io))

(define-public crate-upkeep-1.0.0 (c (n "upkeep") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1ycydsbvp8320dcqkmipxyq7xwhqhhn57vr2m2hpi9d7kf94mc9x")))

