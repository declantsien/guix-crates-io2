(define-module (crates-io up gr upgrayedd-macros) #:use-module (crates-io))

(define-public crate-upgrayedd-macros-0.1.1-rc.2 (c (n "upgrayedd-macros") (v "0.1.1-rc.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1mppbr339jwf3v6dcghz6p4zsjh8aqffq7z9k47p6awfxzyfi2ck")))

(define-public crate-upgrayedd-macros-0.1.1-rc.3 (c (n "upgrayedd-macros") (v "0.1.1-rc.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1mng9g0s6lhjdbd4l8kdjfrpv5v4ilcvl0lk7ib4849d80964z02")))

(define-public crate-upgrayedd-macros-0.1.1-rc.4 (c (n "upgrayedd-macros") (v "0.1.1-rc.4") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1hcgp4ql986gpmcn0rbgdg2vvwsw0rif8iqm5fq58ygra8a3b66p")))

(define-public crate-upgrayedd-macros-0.1.1 (c (n "upgrayedd-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0xgns14rm7wx1fcsvsk8x7wqg0j5ll4xchiy10bzkxqnlbpprn9k")))

(define-public crate-upgrayedd-macros-0.1.2 (c (n "upgrayedd-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0rh224nkry0j0w3wx1p0a4c915s7v1ahrbkjsr0hrn1xc9gpb0lj")))

