(define-module (crates-io up gr upgradedb4pg) #:use-module (crates-io))

(define-public crate-upgradedb4pg-0.1.0 (c (n "upgradedb4pg") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rustyline") (r "^2.1.0") (d #t) (k 0)))) (h "1j8wk97ibfmjhws35lafpyqbcx2srmky5s51s3x9793x9hxgv2rp")))

