(define-module (crates-io up gr upgrayedd) #:use-module (crates-io))

(define-public crate-upgrayedd-0.1.1-rc.2 (c (n "upgrayedd") (v "0.1.1-rc.2") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "upgrayedd-macros") (r "^0.1.1-rc.2") (d #t) (k 0)))) (h "0r9ypf8kc0h7c0a5xpvcnyak4qcafhnqp8qb1szqfzr8c29nwai0")))

(define-public crate-upgrayedd-0.1.1-rc.3 (c (n "upgrayedd") (v "0.1.1-rc.3") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "upgrayedd-macros") (r "^0.1.1-rc.3") (d #t) (k 0)))) (h "0h1gzszk1zrjv56hcz8rcpxn0s4xgjq1c1qvi47gn7zy806d3q16")))

(define-public crate-upgrayedd-0.1.1-rc.4 (c (n "upgrayedd") (v "0.1.1-rc.4") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "upgrayedd-macros") (r "^0.1.1-rc.4") (d #t) (k 0)))) (h "0ja1dvz7xc0qdqjn794xlvcx991fbzzx40iy1717mjjf3fplab06")))

(define-public crate-upgrayedd-0.1.1 (c (n "upgrayedd") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "upgrayedd-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1xlqamf1vv6r5xhab2lfmdfwsjjh5hgki7c4mcksycw3pwh2ag3f")))

(define-public crate-upgrayedd-0.1.2 (c (n "upgrayedd") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "upgrayedd-macros") (r "^0.1.2") (d #t) (k 0)))) (h "1phahjyrlxqdzswwhdwgmgmjjvg8hcbk8sbcpjzgnciwr9khisrf")))

