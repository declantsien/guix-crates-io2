(define-module (crates-io up gr upgrade) #:use-module (crates-io))

(define-public crate-upgrade-0.1.0 (c (n "upgrade") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "~0.8") (d #t) (k 0)) (d (n "windows") (r "~0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Kernel" "Win32_System_Memory"))) (d #t) (k 0)))) (h "0xv210ykznkygqy5lzggxd0fsy4lmljb0x5h5jgfk9mkm1bjz5gm")))

(define-public crate-upgrade-0.2.0 (c (n "upgrade") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "~0.8") (d #t) (k 0)) (d (n "windows") (r "~0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Kernel" "Win32_System_Memory"))) (d #t) (k 0)))) (h "1yfzbrxiy2ws3wga434d7ln0i2qkjg1hy6yqcjlb9lxs57w79v9k") (y #t)))

(define-public crate-upgrade-0.2.1 (c (n "upgrade") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "~0.8") (d #t) (k 0)) (d (n "windows") (r "~0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Kernel" "Win32_System_Memory"))) (d #t) (k 0)))) (h "0z1bbyn0wg1cm3x0pfi9ax1x50hhylb2d6bsbqhhv3dfkn9zv99m") (y #t)))

(define-public crate-upgrade-0.2.2 (c (n "upgrade") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "encoding_rs") (r "~0.8") (d #t) (k 0)) (d (n "windows") (r "~0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Kernel" "Win32_System_Memory"))) (d #t) (k 0)))) (h "0sdm3wyh4q1ljx738njdk5l18i3b9fi24f1ig83hy2z7jrrp78x2") (y #t)))

(define-public crate-upgrade-0.3.0 (c (n "upgrade") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "encoding_rs") (r "~0.8") (d #t) (k 0)) (d (n "windows") (r "~0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Kernel" "Win32_System_Memory"))) (d #t) (k 0)))) (h "16bi1dz5zkx2dj7mk7rx6hfj02ni9gqa4b4z130i44zr526ky8bc") (y #t)))

(define-public crate-upgrade-0.3.1 (c (n "upgrade") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "self-replace") (r "~1.3") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows-sys") (r "~0") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)))) (h "0ady7v3kdxk0mrmhxlnsg0gy2nwhwvzn0nimc5hxzrrrmjsh3ryp") (y #t)))

(define-public crate-upgrade-0.3.2 (c (n "upgrade") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "self-replace") (r "~1.3") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows-sys") (r "~0") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)))) (h "0ra6j4347w2bpwr89nh5l0afmr8ipwjgzzr9iifpkb5d4xa3i6gm")))

(define-public crate-upgrade-0.4.0 (c (n "upgrade") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "self-replace") (r "~1.3") (d #t) (k 0)))) (h "1jqzb5skdh8lpjhbx0bnzc1d5ch79p457nwxrf3khv29aj2z5ljj")))

(define-public crate-upgrade-1.0.0 (c (n "upgrade") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "self-replace") (r "^1") (d #t) (k 0)))) (h "13b07dyx8b79vppli5pc593ixqqgnhi5dinv9wdcrlbp65brrw5c")))

(define-public crate-upgrade-1.1.0 (c (n "upgrade") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reportme") (r "~0.2") (d #t) (k 1)) (d (n "self-replace") (r "^1") (d #t) (k 0)))) (h "1xdydd1h99l6r9vr0w58ya8q93kqrzf9yk9lf6vz9sscqmfy956z")))

(define-public crate-upgrade-1.1.1 (c (n "upgrade") (v "1.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reportme") (r "~0.2") (d #t) (k 1)) (d (n "self-replace") (r "^1") (d #t) (k 0)))) (h "1gw6hvs756q1xdsi4h0lvrwq08ldxnfa7ar48gps01850zrc5qnk")))

(define-public crate-upgrade-2.0.0 (c (n "upgrade") (v "2.0.0") (d (list (d (n "reportme") (r ">=0.2") (d #t) (k 1)) (d (n "self-replace") (r "^1") (d #t) (k 0)))) (h "05lylyc807cfdh0vpr9j6ynqgb08p3mvygvc0b1w9skh7ahcvalf")))

