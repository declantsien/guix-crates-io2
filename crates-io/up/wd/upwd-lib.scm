(define-module (crates-io up wd upwd-lib) #:use-module (crates-io))

(define-public crate-upwd-lib-0.1.0 (c (n "upwd-lib") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1v0yb94xxzkds1b2qp7myz30g5plhk05sh83qmn3p146729132f8")))

(define-public crate-upwd-lib-0.1.1 (c (n "upwd-lib") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0nrkcrs7pb1ih44zl8h9ixgfxrpvdfdqavqi39ab68izrqglpvvm")))

(define-public crate-upwd-lib-0.1.2 (c (n "upwd-lib") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0zzm93cyncxzb8sm88gi2s97qipadwxdisqjsvjj4qqxzm4hhz1k")))

