(define-module (crates-io up te uptest) #:use-module (crates-io))

(define-public crate-uptest-0.1.0 (c (n "uptest") (v "0.1.0") (h "1066fps87iqz89w4xqx7iqcf9dpszhxp29rzax7qb9g6mapf48hj")))

(define-public crate-uptest-0.1.1 (c (n "uptest") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_mangen") (r "^0.2.7") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "libuptest") (r "^0.1.1") (f (quote ("metadatadecode"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1m4cvxflg826fk5finr5rgiymn7v3n1w8vrdyn2zxdah4hk2vx47")))

(define-public crate-uptest-0.1.4 (c (n "uptest") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_mangen") (r "^0.2.7") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "libuptest") (r "^0.1.4") (f (quote ("metadatadecode"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1h5s7zkm0c5jwzflmixmqfvy89s4s4ywd2ywnms9q2v9m34gz0vi")))

