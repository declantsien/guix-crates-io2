(define-module (crates-io up -a up-api) #:use-module (crates-io))

(define-public crate-up-api-0.1.0 (c (n "up-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "00sflbv5iwhfnnvgp1pg1p5mkhwc9a9d1rqk650jpqkqif7k0nc8")))

(define-public crate-up-api-0.1.1 (c (n "up-api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "12bfbr8dvvr188cri3pc616xp9q02finpsshsng3ipgl9aclrng9")))

(define-public crate-up-api-0.1.2 (c (n "up-api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1l6a27ymfzqd72l58p8v6087skxl63rgr1kbvx7k4a9m1izj24nd")))

(define-public crate-up-api-0.1.3 (c (n "up-api") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1a016k9b5cc9lzird9185vx5hp0nhdq6qf2bmb3kwyn6frylk1bn")))

