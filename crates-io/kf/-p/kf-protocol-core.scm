(define-module (crates-io kf -p kf-protocol-core) #:use-module (crates-io))

(define-public crate-kf-protocol-core-0.1.0 (c (n "kf-protocol-core") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)))) (h "083cw2x2a2m59s6qspnhxhknisgdxazz6sk0ilb0ff9cd94nbsbz")))

(define-public crate-kf-protocol-core-0.1.1 (c (n "kf-protocol-core") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)))) (h "00qcng0q3wnlgp3g5511b19ld9zx59j0nw1h5bbqskrppk5ng8xp")))

(define-public crate-kf-protocol-core-0.1.2 (c (n "kf-protocol-core") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x93za7sbm8nlmicyvql4gnrjjca6v92m1a5bj71kfcd8dfr278q")))

