(define-module (crates-io kf -p kf-protocol-derive) #:use-module (crates-io))

(define-public crate-kf-protocol-derive-0.1.0 (c (n "kf-protocol-derive") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1k94fjqlsy4bnxg25v6q5mf3kb53hjadkypp4v68ra4z79adrjj9")))

(define-public crate-kf-protocol-derive-0.1.1 (c (n "kf-protocol-derive") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1gibrhf7633403z84ag0gn2695njk7rbas2cd553hb9vbls5bmq4")))

(define-public crate-kf-protocol-derive-0.2.0 (c (n "kf-protocol-derive") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "067l5jw9q9fkzbh7jw2chzzw8hkygmx0lga9iji056zbdxq54ppi")))

