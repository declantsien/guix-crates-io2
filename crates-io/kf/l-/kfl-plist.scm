(define-module (crates-io kf l- kfl-plist) #:use-module (crates-io))

(define-public crate-kfl-plist-0.0.0 (c (n "kfl-plist") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "kfl") (r "^0") (f (quote ("default" "chrono"))) (d #t) (k 0)))) (h "09fdmdyw049np3afihac09mdxsb11ndir1ymq328nnvw2z12rk4k") (r "1.62.0")))

