(define-module (crates-io kf c_ kfc_crazy_thus) #:use-module (crates-io))

(define-public crate-kfc_crazy_thus-1.0.0 (c (n "kfc_crazy_thus") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "teloxide") (r "^0.10.1") (f (quote ("macros" "auto-send" "ctrlc_handler"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "05191s8l109hra60lnlwssjm6w9r7cf66fp187x6k0m3cq35g7pj") (y #t)))

(define-public crate-kfc_crazy_thus-1.0.1 (c (n "kfc_crazy_thus") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "teloxide") (r "^0.10.1") (f (quote ("macros" "auto-send" "ctrlc_handler"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1hyz59m1h74zisgnjr10h70ff3m5n07bzpxmrvbw3kyx71pfclj7")))

(define-public crate-kfc_crazy_thus-1.0.2 (c (n "kfc_crazy_thus") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "teloxide") (r "^0.10.1") (f (quote ("macros" "auto-send" "ctrlc_handler"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "17ksi01g9y6p3qllqdqinjfl8pqsgiak55q5459yd2xvb6wf483l")))

(define-public crate-kfc_crazy_thus-1.0.3 (c (n "kfc_crazy_thus") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "teloxide") (r "^0.10.1") (f (quote ("macros" "auto-send" "ctrlc_handler"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1xx42gq21q1qf83c9sj58g251480mjzvfwbmllf9xfpim1vgglhx")))

