(define-module (crates-io cw mp cwmp) #:use-module (crates-io))

(define-public crate-cwmp-0.2.1 (c (n "cwmp") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1j89xwpq4ipflikxxbhn8s51gvy0p6llsijsfy8qfj0qj65fz0ms")))

(define-public crate-cwmp-0.2.2 (c (n "cwmp") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0hazq1zdqqszfpzqq1qf0ag193p081mpx5jmhihj4jc8jpyvg9d8")))

