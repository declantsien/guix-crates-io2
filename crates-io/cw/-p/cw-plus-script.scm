(define-module (crates-io cw -p cw-plus-script) #:use-module (crates-io))

(define-public crate-cw-plus-script-0.1.0 (c (n "cw-plus-script") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cosm-script") (r "^0.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "cw20") (r "^0.13") (d #t) (k 0)) (d (n "cw20-base") (r "^0.13") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "tokio") (r "^1.19.1") (d #t) (k 2)))) (h "1ym8ksyr55apnq639js1vhm1gmlxrhr3r0gbavd5iqdbg95wlfs6")))

