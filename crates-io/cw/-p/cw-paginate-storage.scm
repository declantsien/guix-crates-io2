(define-module (crates-io cw -p cw-paginate-storage) #:use-module (crates-io))

(define-public crate-cw-paginate-storage-0.1.0 (c (n "cw-paginate-storage") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.13") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1i0fyj5a7n508vjc7g9v5sxl65vkn0ds4q8v5phdwmy50wz86m5q")))

(define-public crate-cw-paginate-storage-2.1.0 (c (n "cw-paginate-storage") (v "2.1.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "06l9p6n9kljq6k0c5qda5i4cg3ywkps73ysf1dsbcn34rpgvnwk2") (y #t)))

(define-public crate-cw-paginate-storage-2.1.2 (c (n "cw-paginate-storage") (v "2.1.2") (d (list (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "126dp7z80gz73czpgzvy7px0hlycwvrkpp2gsakzn014j24wp4b4") (y #t)))

(define-public crate-cw-paginate-storage-2.1.3 (c (n "cw-paginate-storage") (v "2.1.3") (d (list (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1ma6sh9l49gqg2sf7c483njpdwvkn22183c5s8yy1j4djhh5fksv") (y #t)))

(define-public crate-cw-paginate-storage-2.1.5 (c (n "cw-paginate-storage") (v "2.1.5") (d (list (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "162xpn3fxmkwcxparjk3cwrkbwgrq9plgynyrky13gh0ambb5dvq") (y #t)))

(define-public crate-cw-paginate-storage-2.2.0 (c (n "cw-paginate-storage") (v "2.2.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1wd9abpzgw1mznwrpq7bw6fyqwyb2vsyms5xw815as36ljm68s8g")))

(define-public crate-cw-paginate-storage-2.3.0 (c (n "cw-paginate-storage") (v "2.3.0") (d (list (d (n "cosmwasm-std") (r "^1.2") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.2") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.17") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "012w59crbljmrg5wa6bycp0rm1afk416gf7nnk2qcpr7vjlbm4w6")))

