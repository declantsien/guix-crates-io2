(define-module (crates-io cw -p cw-pause-once) #:use-module (crates-io))

(define-public crate-cw-pause-once-0.1.0 (c (n "cw-pause-once") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1myad9n0qw757zs2jsb3zgf9471c9z25awzf0ji4m6lr6l3f2jw3")))

