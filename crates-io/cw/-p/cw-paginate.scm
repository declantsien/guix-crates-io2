(define-module (crates-io cw -p cw-paginate) #:use-module (crates-io))

(define-public crate-cw-paginate-0.1.0 (c (n "cw-paginate") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0ivz7wjjnp77zr3c3v5dx47yhpvqxjk8rwbjjw8l72lzmkwvla55")))

(define-public crate-cw-paginate-0.2.0 (c (n "cw-paginate") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1vckm1x8i4kg0yp43nhj3yclvigvzsk3xqbdls7zd5s3f52g57nc")))

(define-public crate-cw-paginate-0.2.1 (c (n "cw-paginate") (v "0.2.1") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0kjcd283l9r0wjlwakdvv2czhi7xpw7yp0f76ldbwlb2gxhpilmd")))

