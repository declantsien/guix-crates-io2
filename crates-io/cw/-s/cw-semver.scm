(define-module (crates-io cw -s cw-semver) #:use-module (crates-io))

(define-public crate-cw-semver-1.0.14 (c (n "cw-semver") (v "1.0.14") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1xnkzyx8h3a1jav5paqiqvsy7lc6wvk8mrj0yiip7qz2hdwpzzj5") (f (quote (("std") ("default" "std")))) (r "1.31")))

