(define-module (crates-io cw -s cw-stake-tracker) #:use-module (crates-io))

(define-public crate-cw-stake-tracker-2.1.0 (c (n "cw-stake-tracker") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-wormhole") (r "^2.1.0") (d #t) (k 0)))) (h "1hjn986avlaxi0cqi2m3wdpz52954s13fv5n4shbnhfs1da47wk9") (y #t)))

(define-public crate-cw-stake-tracker-2.1.3 (c (n "cw-stake-tracker") (v "2.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-wormhole") (r "^2.1.3") (d #t) (k 0)))) (h "14hq0d90fglqb1bggipgh912xi662fwkidsknjjjakz8ffrapvmx") (y #t)))

(define-public crate-cw-stake-tracker-2.1.5 (c (n "cw-stake-tracker") (v "2.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-wormhole") (r "^2.1.5") (d #t) (k 0)))) (h "0rxn798grj6kks9fajhck775hkm52wcdjlnb0llf5a7mi95r0f3q") (y #t)))

(define-public crate-cw-stake-tracker-2.2.0 (c (n "cw-stake-tracker") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-wormhole") (r "^2.2.0") (d #t) (k 0)))) (h "0bvpnbyd0pfazapc19rqnbl6d6m48bf43v16jj5ksmyn6q6bz4nn")))

(define-public crate-cw-stake-tracker-2.3.0 (c (n "cw-stake-tracker") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-wormhole") (r "^2.3.0") (d #t) (k 0)))) (h "1w3dddqb030v3wsdjnny7hqd9qzqv1lii9jzza8xly8dpalrxs8p")))

