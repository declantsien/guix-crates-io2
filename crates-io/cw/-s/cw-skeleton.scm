(define-module (crates-io cw -s cw-skeleton) #:use-module (crates-io))

(define-public crate-cw-skeleton-0.1.0 (c (n "cw-skeleton") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.5") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16.4") (d #t) (k 2)) (d (n "cw-skeleton-pkg") (r "^0.1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "cw2") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "175s0bn6sb77366i830zqk3f92sgkz9vg5qdb6cs6jn8n99njx3i") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

