(define-module (crates-io cw -s cw-storage-macro) #:use-module (crates-io))

(define-public crate-cw-storage-macro-0.14.0 (c (n "cw-storage-macro") (v "0.14.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (k 2)) (d (n "cw-storage-plus") (r "<=0.14.0, >=0.13.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 2)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0kcpn0arar122bkp2xj06ic8g1qal0ni2qi91756l964m00fm7g8")))

(define-public crate-cw-storage-macro-0.15.0 (c (n "cw-storage-macro") (v "0.15.0") (d (list (d (n "cosmwasm-std") (r "^1.1.0") (k 2)) (d (n "cw-storage-plus") (r "<=0.15.0, >=0.14.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 2)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "154xwwxpjd576bq42rqxqvi5b787jvj3m9r9ksk3vazfp85ph4a9")))

(define-public crate-cw-storage-macro-0.15.1 (c (n "cw-storage-macro") (v "0.15.1") (d (list (d (n "cosmwasm-std") (r "^1.1.0") (k 2)) (d (n "cw-storage-plus") (r "<=0.15.1, >=0.14.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 2)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0i4l2y07y3b3lvx9h2p6qypsgg6kiq4ymyhnijpyaxhs8cz5gcy9")))

(define-public crate-cw-storage-macro-0.16.0 (c (n "cw-storage-macro") (v "0.16.0") (d (list (d (n "cosmwasm-std") (r "^1.1.0") (k 2)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 2)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1a6i8gw8cx6i20vxrifhpk2dlw442vw672q2wyh4klk423h3sypc")))

(define-public crate-cw-storage-macro-1.0.0 (c (n "cw-storage-macro") (v "1.0.0") (d (list (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0vnnxfvnfg00vj1rc454d8mpwpa4kxlil85bhyjjm255ghs85wxw")))

(define-public crate-cw-storage-macro-1.0.1 (c (n "cw-storage-macro") (v "1.0.1") (d (list (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "11jipabm5a09p06bd7q6ykyhww59fg2ih77k36qj3xbl7hj2477p")))

(define-public crate-cw-storage-macro-1.1.0 (c (n "cw-storage-macro") (v "1.1.0") (d (list (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1xxfswziz1wspj72qj7659cd5vvw6ivjm2kzl1k9s2aaihypqm4m")))

(define-public crate-cw-storage-macro-1.2.0 (c (n "cw-storage-macro") (v "1.2.0") (d (list (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1gjd7wyia0y6sb7bwj0q7am0rvp7b3j9wlapvjh2wm10dnzqwg45")))

(define-public crate-cw-storage-macro-2.0.0-beta.0 (c (n "cw-storage-macro") (v "2.0.0-beta.0") (d (list (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n87x062zd9xi1j4fksidrwjyhz9abfyd9682iyhr8w4780dwx7r")))

(define-public crate-cw-storage-macro-2.0.0-rc.0 (c (n "cw-storage-macro") (v "2.0.0-rc.0") (d (list (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "154szwmlzvlsag47jcw13i62f083h8pydapcblflzr8j6jjlix9g")))

(define-public crate-cw-storage-macro-2.0.0 (c (n "cw-storage-macro") (v "2.0.0") (d (list (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0416fffki9sjv9dzmzdj97lm4dsij018p3qp8dbw8x81x8l7h3pr")))

