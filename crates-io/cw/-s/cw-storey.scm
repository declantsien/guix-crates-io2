(define-module (crates-io cw -s cw-storey) #:use-module (crates-io))

(define-public crate-cw-storey-0.1.0 (c (n "cw-storey") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^2") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "storey") (r "^0.1.0") (d #t) (k 0)))) (h "0wdgmfx7wwzzs8qxhvvj0x7csr7rng6f9sxqpymw3lqbb9njygvn")))

(define-public crate-cw-storey-0.2.0 (c (n "cw-storey") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^2") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "storey") (r "^0.2") (d #t) (k 0)))) (h "0bsjrkh7s59lr9dabc6x99z4xmggwispzw6w1dwkg657l49d8zy6")))

