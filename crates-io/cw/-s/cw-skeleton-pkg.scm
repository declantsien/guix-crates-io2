(define-module (crates-io cw -s cw-skeleton-pkg) #:use-module (crates-io))

(define-public crate-cw-skeleton-pkg-0.1.0 (c (n "cw-skeleton-pkg") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "0clpcpzk3lv198n100p5fb23v1cfhxd9w5sxj4x9ginx8zz4lpgy")))

