(define-module (crates-io cw -f cw-fable) #:use-module (crates-io))

(define-public crate-cw-fable-0.1.0 (c (n "cw-fable") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.3") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "test-case") (r "^1") (d #t) (k 2)))) (h "18zx8kkp22d0xplm5k929w91sm8kvv06qlmmm3vb22n24p1j6g55")))

