(define-module (crates-io cw -f cw-fee-config) #:use-module (crates-io))

(define-public crate-cw-fee-config-0.1.0-rc.1 (c (n "cw-fee-config") (v "0.1.0-rc.1") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.3") (d #t) (k 0)) (d (n "cw-address-like") (r "^1.0.4") (d #t) (k 0)) (d (n "cw-asset") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0lsw2x279mvgqsv5x1wpql8b44mvnsmfc0ljmipa735ci9rxmbdq")))

(define-public crate-cw-fee-config-0.1.0-rc.2 (c (n "cw-fee-config") (v "0.1.0-rc.2") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.3") (d #t) (k 0)) (d (n "cw-address-like") (r "^1.0.4") (d #t) (k 0)) (d (n "cw-asset") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "1j44pzsp50z2l7czdrvl9kx9hfy7v055whjxyqiznvqahi518im1")))

(define-public crate-cw-fee-config-0.1.0-rc.3 (c (n "cw-fee-config") (v "0.1.0-rc.3") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.3") (d #t) (k 0)) (d (n "cw-address-like") (r "^1.0.4") (d #t) (k 0)) (d (n "cw-asset") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0q9mdx7bypcz3gag961q4ps5lsxdzhq7rbf40y1ycagr07f7is09")))

(define-public crate-cw-fee-config-0.1.0 (c (n "cw-fee-config") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.3") (d #t) (k 0)) (d (n "cw-address-like") (r "^1.0.4") (d #t) (k 0)) (d (n "cw-asset") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "1zznkbqx9nc86by09drnwks3y2q8rwqjpq7yr8blanvrmk8zf2jb") (y #t)))

(define-public crate-cw-fee-config-0.1.1 (c (n "cw-fee-config") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.4") (d #t) (k 0)) (d (n "cw-address-like") (r "^1.0.4") (d #t) (k 0)) (d (n "cw-asset") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "14zn4pk3gx2dpv2fw2vp5j7g6ypidr0ndmys0cilhiqkn2y4k3hl")))

(define-public crate-cw-fee-config-0.1.2-rc.1 (c (n "cw-fee-config") (v "0.1.2-rc.1") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.4") (d #t) (k 0)) (d (n "cw-address-like") (r "^1.0.4") (d #t) (k 0)) (d (n "cw-asset") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0cd8i426xrmnhmfqkschidr52hjgyjr8cfvp8biq742bb5lwlam5")))

(define-public crate-cw-fee-config-0.1.2 (c (n "cw-fee-config") (v "0.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.4") (d #t) (k 0)) (d (n "cw-address-like") (r "^1.0.4") (d #t) (k 0)) (d (n "cw-asset") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0m2y4q4j187hxywfn9drh5il4s32ly8l2i5ih1fng8fr2pa7hp1h")))

(define-public crate-cw-fee-config-0.1.3 (c (n "cw-fee-config") (v "0.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.4") (d #t) (k 0)) (d (n "cw-address-like") (r "^1.0.4") (d #t) (k 0)) (d (n "cw-asset") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "09dxrymfb7h19i77j8sxz2jwb01smhv09ci3pmq7hgnsc118p4mq")))

