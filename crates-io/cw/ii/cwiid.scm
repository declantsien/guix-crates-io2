(define-module (crates-io cw ii cwiid) #:use-module (crates-io))

(define-public crate-cwiid-0.1.0 (c (n "cwiid") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "12i2nxbzcz1jhhsapxlkyhwcydh4cflcck5b13jhmdlasvm8dh2v")))

(define-public crate-cwiid-0.1.1 (c (n "cwiid") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "190fgrirdbsv0q2gw9n6my98mgafl4j3fg0rhj754h11b025i37m")))

(define-public crate-cwiid-0.1.2 (c (n "cwiid") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1wvmas8xnr5lxzz9z8akpzfskl4nqd37q81pq28vs7nywdan6i2r")))

(define-public crate-cwiid-0.1.3 (c (n "cwiid") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1dfi8rfpvqn82ni1zzj6j40cm4yb0qqf41i7zj8j46gjvnh38xh8")))

(define-public crate-cwiid-0.1.4 (c (n "cwiid") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1wr1xy7gjqcs3vj8i84kw518navszidmkynmhcwk7k60m3wfd3bg")))

(define-public crate-cwiid-0.1.5 (c (n "cwiid") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1x42vdyjqhwh23gsqj6slr402d2f8cr8zmr91k1x1kq3jdw8p428")))

(define-public crate-cwiid-0.1.6 (c (n "cwiid") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1ppk0rl2k80l5chp2n5d0fwh34phc58gvyra814wk1kqjx3ykfa4")))

(define-public crate-cwiid-0.1.7 (c (n "cwiid") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "04jvy6vybclsifdp9099bb0an6bz7y7qbaksxl4hl4xah33wr3ml")))

(define-public crate-cwiid-0.1.8 (c (n "cwiid") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "19l6nsdli4ikhpppmpnfrj1in5k87i7gci67jds84lpk28ghwlxv")))

(define-public crate-cwiid-0.1.9 (c (n "cwiid") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0qdnf7lllk2cyjv7xafyd6yzgwf12i8l0sv8vld8c1g2vm00x559")))

(define-public crate-cwiid-0.1.10 (c (n "cwiid") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1cic71g7qvxycaxadf1j7wchbnipakp2jsyjc1y2i8lbvv2i7lix")))

(define-public crate-cwiid-0.1.11 (c (n "cwiid") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0rcpwhgv4r15746s7jh05hxm41l8v3khxmyp2ak68yyh538ggpy8")))

(define-public crate-cwiid-0.1.12 (c (n "cwiid") (v "0.1.12") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "17lyisd7989li3ya1c58jqc7hcbqp7kk4vscbp767q26d2rjqrk4")))

(define-public crate-cwiid-0.1.13 (c (n "cwiid") (v "0.1.13") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0z8l93d1kzymvqnk0zpxq39gcr44xhpkjwdw31frlsgfg4phq2d3")))

(define-public crate-cwiid-0.1.14 (c (n "cwiid") (v "0.1.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0vykavmyia2f6hp9r7lvsvg9fv53m6j59iqkwn38gd3k4al58flm")))

(define-public crate-cwiid-0.1.15 (c (n "cwiid") (v "0.1.15") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libcwiid-sys") (r "^0.1.18") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1akr3xm3y24ca9xkkh9p7bbmrkhmf0plywxaz73xnq6x2i9lqgba")))

