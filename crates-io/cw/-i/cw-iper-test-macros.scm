(define-module (crates-io cw #{-i}# cw-iper-test-macros) #:use-module (crates-io))

(define-public crate-cw-iper-test-macros-0.1.0 (c (n "cw-iper-test-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "01n9g3wkm7vylv3hkkfizmfgq33w8kjsvin0vd9n7f0v8nhsc2gj") (f (quote (("internal") ("default"))))))

