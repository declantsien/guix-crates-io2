(define-module (crates-io cw #{-i}# cw-item-set) #:use-module (crates-io))

(define-public crate-cw-item-set-0.1.0 (c (n "cw-item-set") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.14") (f (quote ("iterator"))) (d #t) (k 0)))) (h "11flrhasc9328swc4xji7raig9jbhga5f9w58h9cf9k26v4fy14h")))

(define-public crate-cw-item-set-0.2.0 (c (n "cw-item-set") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13") (d #t) (k 0)))) (h "0af36iibba00w7s2yvf4qns65xi0nv7bm1yipvdfmy1cwh0plbb9") (f (quote (("iterator" "cw-storage-plus/iterator") ("default" "iterator"))))))

(define-public crate-cw-item-set-0.3.0 (c (n "cw-item-set") (v "0.3.0") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13") (d #t) (k 0)))) (h "0hlngllwvxq7c6fr9csnlfjxvjyyjvq308xprq2s5asnm3wylzq4") (f (quote (("iterator" "cw-storage-plus/iterator") ("default" "iterator"))))))

(define-public crate-cw-item-set-0.4.0 (c (n "cw-item-set") (v "0.4.0") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13") (d #t) (k 0)))) (h "1i9fsq1m2kq61gnb1ay2pk1zzw3r53j4ahzis5z1jd4k3bi04rkb") (f (quote (("iterator" "cw-storage-plus/iterator") ("default" "iterator"))))))

(define-public crate-cw-item-set-0.5.0 (c (n "cw-item-set") (v "0.5.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.15") (d #t) (k 0)))) (h "1hxx4pyqw3ynqf65p0mnrabbpmqv1bbhzh11r6v01pwdz5qi68dy") (f (quote (("iterator" "cw-storage-plus/iterator") ("default" "iterator"))))))

(define-public crate-cw-item-set-0.6.0 (c (n "cw-item-set") (v "0.6.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.16") (d #t) (k 0)))) (h "131nr8df3qkg8qgmxh0fgd5h3zjaix9di62mpf2bg6kf5vfdknh5") (f (quote (("iterator" "cw-storage-plus/iterator") ("default" "counter" "iterator") ("counter"))))))

(define-public crate-cw-item-set-0.7.0 (c (n "cw-item-set") (v "0.7.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)))) (h "17d3kyfzmr3b2p5531fk24hi990wyv36jrp43ssccqbc12lj5zp2") (f (quote (("iterator" "cw-storage-plus/iterator") ("default" "counter" "iterator") ("counter"))))))

(define-public crate-cw-item-set-0.7.1 (c (n "cw-item-set") (v "0.7.1") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)))) (h "1cnfplk3ygnnm17lgg1q3xzmj32blrw42lbax7dvxfk7plrs59fy") (f (quote (("iterator" "cw-storage-plus/iterator") ("default" "counter" "iterator") ("counter"))))))

