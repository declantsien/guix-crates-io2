(define-module (crates-io cw #{-i}# cw-ica-controller-derive) #:use-module (crates-io))

(define-public crate-cw-ica-controller-derive-0.4.1 (c (n "cw-ica-controller-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vcqmi51a4iqrwwy7277nlxza9sm9npndwca97wjaf4v9ip8jw8x")))

(define-public crate-cw-ica-controller-derive-0.4.2 (c (n "cw-ica-controller-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lcijxs1ih540hsrby1p8d4zjwlxa77rdmbwbw4n94mgwx43jpwh")))

