(define-module (crates-io cw #{-i}# cw-i2-creator-pkg) #:use-module (crates-io))

(define-public crate-cw-i2-creator-pkg-0.1.0 (c (n "cw-i2-creator-pkg") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "0h7yfbij6ni6ianlifaxcqdl0if6r60nbdbpdpwyln2gv8lhpflw")))

(define-public crate-cw-i2-creator-pkg-0.1.1-rc.1 (c (n "cw-i2-creator-pkg") (v "0.1.1-rc.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "15i2y2529paqa04q75p6a7n88i0l8h4i7vnk938yhzhhr6w6x97g")))

