(define-module (crates-io cw #{-i}# cw-ibc-query) #:use-module (crates-io))

(define-public crate-cw-ibc-query-0.1.0 (c (n "cw-ibc-query") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "cw-osmo-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.12.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0vqankkky5r684cf97fnzllg9fbkq0x3hiad8k0gxqbgxdkfq89g") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

