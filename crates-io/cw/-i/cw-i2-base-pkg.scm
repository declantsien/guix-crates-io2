(define-module (crates-io cw #{-i}# cw-i2-base-pkg) #:use-module (crates-io))

(define-public crate-cw-i2-base-pkg-0.1.0 (c (n "cw-i2-base-pkg") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "0p1mr3hzmqv27y3r6fk570nz3bn9vrnrly4dai1k03sg4cjg9rhl")))

