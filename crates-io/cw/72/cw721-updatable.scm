(define-module (crates-io cw #{72}# cw721-updatable) #:use-module (crates-io))

(define-public crate-cw721-updatable-1.0.0 (c (n "cw721-updatable") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "0jr61p28p2gmc3jdyzss2vnssn2mx36jkknvnxxcsl03hrw8nnld")))

(define-public crate-cw721-updatable-1.0.1 (c (n "cw721-updatable") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "0i2hy2qr3xa27s7k9bxqvf9dbl49380cd1iwvgsjgg02kpnmhb1x")))

(define-public crate-cw721-updatable-1.0.2 (c (n "cw721-updatable") (v "1.0.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "0mp9xa82zd2511znpk93s0hlycv932zp546ljwcv3vwmmbj4bkxz") (y #t)))

(define-public crate-cw721-updatable-1.0.3 (c (n "cw721-updatable") (v "1.0.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "03js8w8ijbsbfhimcrs89cs3bcswvalx86ibwdmpi7z35rgc7fjw")))

(define-public crate-cw721-updatable-1.0.4 (c (n "cw721-updatable") (v "1.0.4") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "1rlxgs7vp5a3jigci63g079k331wqi7rgv1mgzy3f6ng3x3dsx74")))

(define-public crate-cw721-updatable-1.0.5 (c (n "cw721-updatable") (v "1.0.5") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "1zvlzyw2wrpli9rldrqp9ln2d0rzmdvk1jhd1an4jw19vx9fiqdl")))

