(define-module (crates-io cw #{72}# cw721-controllers) #:use-module (crates-io))

(define-public crate-cw721-controllers-2.1.0 (c (n "cw721-controllers") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1farabvl9vppby17jxxp69sp8g0awfznx7baxny1q91pi2bnh9xi") (y #t)))

(define-public crate-cw721-controllers-2.1.2 (c (n "cw721-controllers") (v "2.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0rszv0b6x0g47430lsf00l1xg0w2hagcjkzhc0r5143494rm7bjv") (y #t)))

(define-public crate-cw721-controllers-2.1.3 (c (n "cw721-controllers") (v "2.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0hq3s2cxc2mdvmr0049p80irawwqlif2sm5qv17cncji1p5wps0l") (y #t)))

(define-public crate-cw721-controllers-2.1.5 (c (n "cw721-controllers") (v "2.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0d0gbncvx7xjrf57yq1c6lyiahhyjp3bqr1idpvf7r7x7473avdw") (y #t)))

(define-public crate-cw721-controllers-2.2.0 (c (n "cw721-controllers") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1vrpap8449k5753qhzwfpsy87six46qqmhxaw8nk132r159ki425")))

(define-public crate-cw721-controllers-2.3.0 (c (n "cw721-controllers") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zz2jb126kvqd4afizlf2kd9n2h3gp3kw0vpfqr0yz7xmr948cm1")))

