(define-module (crates-io cw #{72}# cw721-soulbound) #:use-module (crates-io))

(define-public crate-cw721-soulbound-1.0.0 (c (n "cw721-soulbound") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "~1.5.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.14") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "0jbq7w50hgvbjsp7srafbql327adipy0n1pd7kd1r2z75nf07fbs")))

(define-public crate-cw721-soulbound-1.0.1 (c (n "cw721-soulbound") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "~1.5.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.14") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "0ry2zb20jnxamlf4hnjh6kk8cd3hz69ysjk0zxg5zpc82nsciimz")))

(define-public crate-cw721-soulbound-1.0.2 (c (n "cw721-soulbound") (v "1.0.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.14") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "0630p44fridvnw59sf8avxvd0pdifmcp42r1jrm0zxiylsrd488z")))

