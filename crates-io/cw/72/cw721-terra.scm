(define-module (crates-io cw #{72}# cw721-terra) #:use-module (crates-io))

(define-public crate-cw721-terra-0.9.2 (c (n "cw721-terra") (v "0.9.2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "18l502lq524xisbvpd6d8jqi8k75mb71jl7qz4fm7y401k3zy29v")))

(define-public crate-cw721-terra-0.9.3 (c (n "cw721-terra") (v "0.9.3") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1l9spqvxx0fw0hzylq39g6pcavx1166nz8ayyx2wmzdajv8d93dz")))

