(define-module (crates-io cw #{72}# cw721-basic) #:use-module (crates-io))

(define-public crate-cw721-basic-0.9.2 (c (n "cw721-basic") (v "0.9.2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 0)) (d (n "cw0") (r "^0.9.1") (d #t) (k 0)) (d (n "cw2") (r "^0.9.1") (d #t) (k 0)) (d (n "cw721") (r "^0.9.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1qwx5d3b2x02dqw5w84dj3x30sydl043iir4gx1sghd3p4986qa4") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

