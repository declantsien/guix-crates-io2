(define-module (crates-io cw #{72}# cw721-marketplace-utils) #:use-module (crates-io))

(define-public crate-cw721-marketplace-utils-0.1.5 (c (n "cw721-marketplace-utils") (v "0.1.5") (d (list (d (n "cosmwasm-std") (r "~1.5.0") (d #t) (k 0)) (d (n "cw20") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bsbfzna5lmgnnql5srs3l9wfgxwf937a3jfhm6ki0p7grx11pj1")))

