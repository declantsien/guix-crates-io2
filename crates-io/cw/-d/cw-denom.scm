(define-module (crates-io cw -d cw-denom) #:use-module (crates-io))

(define-public crate-cw-denom-0.1.0 (c (n "cw-denom") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.13") (d #t) (k 2)) (d (n "cw20") (r "^0.13") (d #t) (k 0)) (d (n "cw20-base") (r "^0.13") (d #t) (k 2)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0shqaz75fc8db09xqpgk0ybr1byx8kpnvhzbkznxhwxgpl2yfxb0")))

(define-public crate-cw-denom-2.0.1 (c (n "cw-denom") (v "2.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw20") (r "^0.16") (d #t) (k 0)) (d (n "cw20-base") (r "^0.16") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0qiyhriap3540yp5s3czxdzx38bwqjhw90nr6n8wmndqgjjvnicd")))

(define-public crate-cw-denom-2.0.2 (c (n "cw-denom") (v "2.0.2") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw20") (r "^0.16") (d #t) (k 0)) (d (n "cw20-base") (r "^0.16") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "01axljzs7j62h159lrnkrz01jki8cnb1d1h0la23v9631fd452wc")))

(define-public crate-cw-denom-2.1.3 (c (n "cw-denom") (v "2.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw20") (r "^0.16") (d #t) (k 0)) (d (n "cw20-base") (r "^0.16") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0nin2fcd2834m5w2kqxrap11hzkhlgsz2gg7lp4f51rhl402vypp") (y #t)))

(define-public crate-cw-denom-2.1.5 (c (n "cw-denom") (v "2.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw20") (r "^0.16") (d #t) (k 0)) (d (n "cw20-base") (r "^0.16") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ijp9b95pvj3m8bhmapk3lgjsjyq8200rj5y9l9xxx1ccxyfkdpm") (y #t)))

(define-public crate-cw-denom-2.2.0 (c (n "cw-denom") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16") (d #t) (k 2)) (d (n "cw20") (r "^0.16") (d #t) (k 0)) (d (n "cw20-base") (r "^0.16") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1fzcg6s9bad9pvfd8rgxjj0fjrqhbdcxs0rr7bc11z36myq9kwv2")))

(define-public crate-cw-denom-2.3.0 (c (n "cw-denom") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.17") (d #t) (k 2)) (d (n "cw20") (r "^1.1") (d #t) (k 0)) (d (n "cw20-base") (r "^1.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1323bv35bh869wnfk1rkip1727vy0zadi0q2phspai3zdihaj6gj")))

