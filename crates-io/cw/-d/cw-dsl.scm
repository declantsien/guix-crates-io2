(define-module (crates-io cw -d cw-dsl) #:use-module (crates-io))

(define-public crate-cw-dsl-0.1.0 (c (n "cw-dsl") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "unicode-ident") (r "^1.0.1") (d #t) (k 0)))) (h "1s4c0bh6rykxhyk9d8gj3aa9cb6rp2df4mdvb9g020v87ksr40x7") (f (quote (("default"))))))

(define-public crate-cw-dsl-0.1.1 (c (n "cw-dsl") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "unicode-ident") (r "^1.0.1") (d #t) (k 0)))) (h "1hh98zr4pdsnnfznhksipkv3gqjswa2aqlpdhdj61hvkxywapqgz") (f (quote (("default"))))))

(define-public crate-cw-dsl-0.2.0 (c (n "cw-dsl") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.1") (d #t) (k 0)))) (h "0saak6ly0g8l9wvnzin9x10k87dm5gfd4jc736dsaa5h7yryi7gn") (f (quote (("default"))))))

(define-public crate-cw-dsl-0.2.1 (c (n "cw-dsl") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.1") (d #t) (k 0)))) (h "1zqwfzyhn91zyb9ar3wy6w2dima5qpkxa069xbxpdc2pk2dachd9") (f (quote (("default"))))))

(define-public crate-cw-dsl-0.2.2 (c (n "cw-dsl") (v "0.2.2") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.1") (d #t) (k 0)))) (h "0qzyslyznwc5m3blc774x77n4l77psy17yyclr73has2sqhc3g68") (f (quote (("default"))))))

(define-public crate-cw-dsl-0.3.0 (c (n "cw-dsl") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.1") (d #t) (k 0)))) (h "12z6ss05f53fn8mbkr3sr2pf4783rg08p8mlqiflnvyss9qcqsck") (f (quote (("default"))))))

(define-public crate-cw-dsl-0.4.0 (c (n "cw-dsl") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.1") (d #t) (k 0)))) (h "1ic4qh93q75lb85hlpxq3bz8fmd7qd4wl4a1mffbajlyygy68dj3") (f (quote (("default"))))))

(define-public crate-cw-dsl-0.4.1 (c (n "cw-dsl") (v "0.4.1") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.1") (d #t) (k 0)))) (h "1vk7h1vkf1bq71zs4y33r806n2jwm245hp6284dznbw6n7sfdjgv") (f (quote (("default"))))))

