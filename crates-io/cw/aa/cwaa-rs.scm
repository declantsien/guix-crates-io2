(define-module (crates-io cw aa cwaa-rs) #:use-module (crates-io))

(define-public crate-cwaa-rs-0.1.0 (c (n "cwaa-rs") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "05ypi42rhgj9qbq03zhkryb8j22ng9dkijdwd2zf2imijbar55z8")))

