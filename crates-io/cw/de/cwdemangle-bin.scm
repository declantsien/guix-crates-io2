(define-module (crates-io cw de cwdemangle-bin) #:use-module (crates-io))

(define-public crate-cwdemangle-bin-1.0.0 (c (n "cwdemangle-bin") (v "1.0.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "cwdemangle") (r "^1.0.0") (d #t) (k 0)))) (h "1599hxbszrapq5rd0zb84l0l33yc0i1rjvjifcffl3jr8q7bmi7m") (r "1.58")))

