(define-module (crates-io cw de cwdemangle) #:use-module (crates-io))

(define-public crate-cwdemangle-0.1.1 (c (n "cwdemangle") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)))) (h "0s5ixbv6ga1wss13hfia2y5phgr8llafavrvscp5y654ccxx5l6g")))

(define-public crate-cwdemangle-0.1.2 (c (n "cwdemangle") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)))) (h "0gvr4nxgjxx1yfpqxvvj36jc778x5wnw99vv10kpxq5dywjnp26r")))

(define-public crate-cwdemangle-0.1.3 (c (n "cwdemangle") (v "0.1.3") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)))) (h "0s0yp0fqr5h0bcjwrkj8cx0vv3k1gzm4bj9pnsysabq7xnjfy4g4")))

(define-public crate-cwdemangle-0.1.4 (c (n "cwdemangle") (v "0.1.4") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)))) (h "13j9sd60fl9hl2xs4hnjpwcjl24admwi3rwd4iflxcakcsgn7ny6")))

(define-public crate-cwdemangle-0.1.5 (c (n "cwdemangle") (v "0.1.5") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)))) (h "0wy37vfmm0c9i9m4f21kpncjw3waxskfsfnhpx70mziwl2ik93dm") (r "1.58")))

(define-public crate-cwdemangle-0.1.6 (c (n "cwdemangle") (v "0.1.6") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)))) (h "0qj15x62lg6mqannbks4amxmmsy2rxakk6vvbk43syrpadavqlf2") (r "1.58")))

(define-public crate-cwdemangle-1.0.0 (c (n "cwdemangle") (v "1.0.0") (h "11d27rpivcrqkg697bqqnb5k6cgnkyacprdiis4kqjk3rsdnzq62") (r "1.58")))

