(define-module (crates-io cw -w cw-wormhole) #:use-module (crates-io))

(define-public crate-cw-wormhole-2.1.0 (c (n "cw-wormhole") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0pywk0ff970q3ci42j320kfgmdzv3jdadjdnx2qs0s8vbi9wcgzs") (y #t)))

(define-public crate-cw-wormhole-2.1.3 (c (n "cw-wormhole") (v "2.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0srbca1rscblbvbx0i4w1b4f2vdddh5784pqfnmic64br6ybn1b0") (y #t)))

(define-public crate-cw-wormhole-2.1.5 (c (n "cw-wormhole") (v "2.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1v2lwf3s2g7ggmaz1kyd99q798v7b9dm83my77xd0vg3ljqq60l5") (y #t)))

(define-public crate-cw-wormhole-2.2.0 (c (n "cw-wormhole") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0kdmbdyx05ngv5xrh4vms3cwk4dr4w47gygygwrnn2frz3lg0bc4")))

(define-public crate-cw-wormhole-2.3.0 (c (n "cw-wormhole") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1w7b8mbrgy4dwirm20azljb3zkwg6bydmx4kdgnlgsicaj174jjb")))

