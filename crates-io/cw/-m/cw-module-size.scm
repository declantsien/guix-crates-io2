(define-module (crates-io cw -m cw-module-size) #:use-module (crates-io))

(define-public crate-cw-module-size-1.0.0 (c (n "cw-module-size") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cosmwasm") (r "^0.7.2") (d #t) (k 0)) (d (n "cosmwasm-vm") (r "^1.0.0") (d #t) (k 0)) (d (n "loupe") (r "^0.1.3") (d #t) (k 0)) (d (n "wasmer") (r "^2.2.1") (f (quote ("cranelift" "universal" "singlepass"))) (k 0)))) (h "0wik34dcv8482dkmlcfsdxmfdndhj0x37fhzbxf1vrs02rgypdyb") (y #t)))

