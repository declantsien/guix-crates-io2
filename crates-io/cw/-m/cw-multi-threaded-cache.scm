(define-module (crates-io cw -m cw-multi-threaded-cache) #:use-module (crates-io))

(define-public crate-cw-multi-threaded-cache-1.0.0 (c (n "cw-multi-threaded-cache") (v "1.0.0") (d (list (d (n "cosmwasm") (r "^0.7.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (k 0)) (d (n "cosmwasm-vm") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1356d9w6ynq9nhkri2vafsxzgssn6ar9drgv012bs81w6l1cgsr5") (y #t)))

