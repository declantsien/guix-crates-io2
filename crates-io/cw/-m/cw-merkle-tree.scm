(define-module (crates-io cw -m cw-merkle-tree) #:use-module (crates-io))

(define-public crate-cw-merkle-tree-0.1.0 (c (n "cw-merkle-tree") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.3") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.15.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0gqjsaa0f386jgpkcm1812m9gjrbzvawvl1fi8bccl047qvwkllh")))

(define-public crate-cw-merkle-tree-0.2.0 (c (n "cw-merkle-tree") (v "0.2.0") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.3") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.15.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0lsz4nq531y9i74a91j9b4c3s4fp6cymvd30hmfs8656g67hl2hk")))

(define-public crate-cw-merkle-tree-0.3.0 (c (n "cw-merkle-tree") (v "0.3.0") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.2.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0mlw80jan111rmsc2jki0vcca2imqsknfspx148lzhjl7qs4iyhr")))

