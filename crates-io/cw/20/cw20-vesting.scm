(define-module (crates-io cw #{20}# cw20-vesting) #:use-module (crates-io))

(define-public crate-cw20-vesting-0.4.1 (c (n "cw20-vesting") (v "0.4.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.1") (d #t) (k 0)) (d (n "cw2") (r "^0.13.1") (d #t) (k 0)) (d (n "cw20") (r "^0.13.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "wynd-utils") (r "^0.4.1") (d #t) (k 0)))) (h "119mwvhc5ywmn8ag3wvbgc8d1jv01vz390yj933p6vvs8i6hx27i") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

