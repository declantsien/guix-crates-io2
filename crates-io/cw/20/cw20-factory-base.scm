(define-module (crates-io cw #{20}# cw20-factory-base) #:use-module (crates-io))

(define-public crate-cw20-factory-base-0.1.0 (c (n "cw20-factory-base") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.5.2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.2.0") (d #t) (k 0)) (d (n "cw20") (r "^1.1.2") (d #t) (k 0)) (d (n "cw20-base") (r "^1.1.2") (f (quote ("library"))) (d #t) (k 0)) (d (n "cw20-factory-pkg") (r "^0.1.0") (d #t) (k 0)) (d (n "rhaki-cw-plus") (r "^1.0.3") (d #t) (k 0)))) (h "0sg9kcv9vq8pkg3nyh15151ihyvilhpi00wyb6m8cbvwm542jzgs")))

