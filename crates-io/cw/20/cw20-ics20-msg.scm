(define-module (crates-io cw #{20}# cw20-ics20-msg) #:use-module (crates-io))

(define-public crate-cw20-ics20-msg-0.0.1 (c (n "cw20-ics20-msg") (v "0.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0crlq57jdxnw7gjdbkygnzp222qns9g2zy30zaj21wd6l2njryds")))

(define-public crate-cw20-ics20-msg-0.0.2 (c (n "cw20-ics20-msg") (v "0.0.2") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0gxi8l58yvyjap8v7ingq2f63jar53wv8r57ix1alk1l6swn8h70")))

(define-public crate-cw20-ics20-msg-0.0.3 (c (n "cw20-ics20-msg") (v "0.0.3") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "oraiswap") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0lcmkrp9w9f1737df2a1mm52fcawpx0l0jbr6vkh8czp4krglqzg")))

