(define-module (crates-io cw #{20}# cw20-icg-pkg) #:use-module (crates-io))

(define-public crate-cw20-icg-pkg-0.1.0 (c (n "cw20-icg-pkg") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20-base") (r "^1.0.1") (d #t) (k 0)) (d (n "gate-pkg") (r "^0.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "09rx1av08fvgvvhbh4kxpj8clhpps56yiq3qgi2pi6i709w2jyn8")))

