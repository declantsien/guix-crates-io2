(define-module (crates-io cw #{20}# cw20-factory-osmosis) #:use-module (crates-io))

(define-public crate-cw20-factory-osmosis-0.1.0 (c (n "cw20-factory-osmosis") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.5.2") (d #t) (k 0)) (d (n "cw20") (r "^1.1.2") (d #t) (k 0)) (d (n "cw20-base") (r "^1.1.2") (f (quote ("library"))) (d #t) (k 0)) (d (n "cw20-factory-base") (r "^0.1.0") (d #t) (k 0)) (d (n "cw20-factory-pkg") (r "^0.1.0") (d #t) (k 0)) (d (n "osmosis-std") (r "^0.22.0") (d #t) (k 0)) (d (n "rhaki-cw-plus") (r "^1.0.3") (d #t) (k 0)))) (h "0ajhs1l60i9bdlxyqq68bl3gp1h9pa12cr6b67fj7szyp3yhaqxv") (f (quote (("library"))))))

