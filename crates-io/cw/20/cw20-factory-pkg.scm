(define-module (crates-io cw #{20}# cw20-factory-pkg) #:use-module (crates-io))

(define-public crate-cw20-factory-pkg-0.1.0 (c (n "cw20-factory-pkg") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.2") (f (quote ("staking" "stargate"))) (d #t) (k 0)) (d (n "cw20") (r "^1.1.2") (d #t) (k 0)) (d (n "cw20-base") (r "^1.1.2") (f (quote ("library"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "156lpp7ksjrzakvwyr8m39r4sd2scv8y82jjdy9zzdgyr0z6dyvw")))

