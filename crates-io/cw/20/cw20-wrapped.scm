(define-module (crates-io cw #{20}# cw20-wrapped) #:use-module (crates-io))

(define-public crate-cw20-wrapped-0.1.0 (c (n "cw20-wrapped") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.8.0") (d #t) (k 0)) (d (n "cw2") (r "^0.8.0") (d #t) (k 0)) (d (n "cw20") (r "^0.8.0") (d #t) (k 0)) (d (n "cw20-legacy") (r "^0.2.0") (f (quote ("library"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "188hf3i3mlp1ziv9ffm0chrvlp0hmjyz2z99nmrbv69j604f8hx6") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

