(define-module (crates-io cw #{20}# cw20) #:use-module (crates-io))

(define-public crate-cw20-0.1.0 (c (n "cw20") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.9.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.9.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0vqz66p51q0pwvx9ixnf09f184b2by227qy7ch4kd6azwpl4qgd1")))

(define-public crate-cw20-0.1.1 (c (n "cw20") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "^0.10.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "094k1yjygfvqgba24wsnik7p9dw2iq0nc953z6jgssgh8rkpqf17")))

(define-public crate-cw20-0.2.0 (c (n "cw20") (v "0.2.0") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cw0") (r "^0.2.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "03h43zns7k2j0lv17xk9m702bycxg51idsphqish5ry6smcpcvah")))

(define-public crate-cw20-0.2.1 (c (n "cw20") (v "0.2.1") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cw0") (r "^0.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1x6h50b6vpy06d5lk00wa32xhssh8yw2b012vk5yg68ysri732n6")))

(define-public crate-cw20-0.2.2 (c (n "cw20") (v "0.2.2") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cw0") (r "^0.2.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0yykpq368whqwc80g7vr2877wfia9cj7a7n242xjcy4mriw4xry4")))

(define-public crate-cw20-0.2.3 (c (n "cw20") (v "0.2.3") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cw0") (r "^0.2.3") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "05d9cfmnvhy2skcii9i1vc1aw396bnc54gnikn4w3x9bmkf5vn7z")))

(define-public crate-cw20-0.3.0 (c (n "cw20") (v "0.3.0") (d (list (d (n "cosmwasm-schema") (r "^0.11.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.0") (d #t) (k 0)) (d (n "cw0") (r "^0.3.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1glc2izcfiminjy2ajvnsmmc8dn7pk31b6rx9sg69d78wicy3ns6")))

(define-public crate-cw20-0.3.1 (c (n "cw20") (v "0.3.1") (d (list (d (n "cosmwasm-schema") (r "^0.11.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "cw0") (r "^0.3.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1fa29rc1l455qw3bqrv31kswzmaxp1ycr8wd36nv481n9fwkwa2y")))

(define-public crate-cw20-0.3.2 (c (n "cw20") (v "0.3.2") (d (list (d (n "cosmwasm-schema") (r "^0.11.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "cw0") (r "^0.3.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1iy44i17v117i975qcmsbwxb7cx3n9jq40ghxxafk1zd3axdbsph")))

(define-public crate-cw20-0.4.0 (c (n "cw20") (v "0.4.0") (d (list (d (n "cosmwasm-schema") (r "^0.12.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.12.2") (d #t) (k 0)) (d (n "cw0") (r "^0.4.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "10lbdnrlg7370spadzrx0hcqh9ck1prxa7x3nchsp6pih5vxmzsj")))

(define-public crate-cw20-0.5.0 (c (n "cw20") (v "0.5.0") (d (list (d (n "cosmwasm-schema") (r "^0.13.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.13.2") (d #t) (k 0)) (d (n "cw0") (r "^0.5.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0757ifz1nhklcca6c15ky323fvz87id9kgawlj2gzq8mp6bjsvq2")))

(define-public crate-cw20-0.6.0-alpha1 (c (n "cw20") (v "0.6.0-alpha1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-alpha1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1hi41sz24izqdmff9xrlylln4k4sglj33hqmv5rr9wpgagpl0iqd")))

(define-public crate-cw20-0.6.0-alpha2 (c (n "cw20") (v "0.6.0-alpha2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-alpha2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0ajr6c7ps3d5bmyfgwsml09xl7wih47l202k7cqrg75zfw3vsl56")))

(define-public crate-cw20-0.6.0-alpha3 (c (n "cw20") (v "0.6.0-alpha3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-alpha3") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "154nw5zc2fjnhjcbmml1a8i8jkdp7x2bj2bi35zfhlzqzlpp22fl")))

(define-public crate-cw20-0.6.0-beta1 (c (n "cw20") (v "0.6.0-beta1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1fdk05534wi45l36gfca6mrbvmimkpgasv06552zgav2qwxj4vsh")))

(define-public crate-cw20-0.6.0-beta2 (c (n "cw20") (v "0.6.0-beta2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-beta2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "10frkix9vcs9l16xyrv4psqpyak1jizq7dm6pq1iq28zc126b2lp")))

(define-public crate-cw20-0.6.0-beta3 (c (n "cw20") (v "0.6.0-beta3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta5") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "16sphixz98fcglzv8f2b4ny5wssn1dfvnj66xndyr9b6dmcm8l9w")))

(define-public crate-cw20-0.6.0 (c (n "cw20") (v "0.6.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "17sz2nihgm6q271niwn58gcy66i43yrjwrqribnf10xvxj2vmlaq")))

(define-public crate-cw20-0.6.1 (c (n "cw20") (v "0.6.1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw0") (r "^0.6.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1bb446wvf03nwp4qk8cl583nlkv24bb38p633f254cbfzl8vpgxc")))

(define-public crate-cw20-0.6.2 (c (n "cw20") (v "0.6.2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw0") (r "^0.6.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1h6dw1q4zsp85327ymhvq80385zansmxz2f3m4sdj9lxm0b8n66f")))

(define-public crate-cw20-0.7.0 (c (n "cw20") (v "0.7.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.15.0") (d #t) (k 0)) (d (n "cw0") (r "^0.7.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1s3n4xzx6x4pmlhf0s002r79kdbg7mgf2fcnqma4rks6aidgp694")))

(define-public crate-cw20-0.8.0-rc1 (c (n "cw20") (v "0.8.0-rc1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0-rc5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0-rc5") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0-rc1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "18if5v8xd8sfy7l2gmzi1p7mndbabw5rzgg2h1dsnmrw9cbg9q4x")))

(define-public crate-cw20-0.8.0-rc2 (c (n "cw20") (v "0.8.0-rc2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0-rc2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1jxjii36bzbmnz56dab1arl4k9xys5r1qqipx0d4innzdlb1mxcp")))

(define-public crate-cw20-0.8.0-rc3 (c (n "cw20") (v "0.8.0-rc3") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0-rc3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1yfkbk49l4cvwqmlh1lkm97pcj5cddnm17zjaq8ck584nk479m90")))

(define-public crate-cw20-0.8.0 (c (n "cw20") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1vr9nmgppwg517s2777npws0ppqmsq60p2mzgrd5vnb3c9apjizj")))

(define-public crate-cw20-0.8.1 (c (n "cw20") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "10xhbgg6y0chhbjz0bp5zg7g4ng5qqmzc8skxnsgan12spdjl6m1")))

(define-public crate-cw20-0.9.0 (c (n "cw20") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.9.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0bm7g1qm0al9cpv88za7vsp69bwc2mvvdf96id4xa4c1jh4q3y4s")))

(define-public crate-cw20-0.10.0-soon (c (n "cw20") (v "0.10.0-soon") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "15y4vjplamhdw5r8ysnb8zsxhb4044lbxg5q8vn60m22ayygn673")))

(define-public crate-cw20-0.9.1 (c (n "cw20") (v "0.9.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "005ii2kq4ma563imwngvbka7yvd1r6n2c6fci3cmyd8yr89v0jdc")))

(define-public crate-cw20-0.10.0-soon2 (c (n "cw20") (v "0.10.0-soon2") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0hb8vg2krvn0h9j58d1rpb1wk9c8qrxzmpl364zvh51yfls08y3d")))

(define-public crate-cw20-0.10.0-soon3 (c (n "cw20") (v "0.10.0-soon3") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "122l3jrma84y5wgawm8c4zyyd5b900mbldhjjzr1093qm3zdg8yc")))

(define-public crate-cw20-0.10.0-soon4 (c (n "cw20") (v "0.10.0-soon4") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon2") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "04ay29py5c9b4yhdlr3mvx51k2w7n92kixq52aav782qid80n89k")))

(define-public crate-cw20-0.10.0 (c (n "cw20") (v "0.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0kab5hhp2jfkfjhr7cslkhlc2xxh5rfaq6w45hr7rjpa04mwrk8y")))

(define-public crate-cw20-0.10.1 (c (n "cw20") (v "0.10.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "035bxf30fcd4djq0ldqhib98jz620dh65z29vc574nd54hcfil5x")))

(define-public crate-cw20-0.10.2 (c (n "cw20") (v "0.10.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1nwd1kq10915jax5fq2f8vgmbk99vymlcm3462craybbj5b8rxyi")))

(define-public crate-cw20-0.10.3 (c (n "cw20") (v "0.10.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0hi16sfpy05nlww49hdzhniabsnwfl1mq6hhxcdq0rdkp5a9vz2n")))

(define-public crate-cw20-0.11.0 (c (n "cw20") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-utils") (r "^0.11.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0s4ip59ir81wpyribx2pk0jz4bw1i6h44d5x1v98f3jqgly449ss")))

(define-public crate-cw20-0.11.1 (c (n "cw20") (v "0.11.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-utils") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0807p81czj0644p3kxyl5knyq0gmwcz4f7igbfpsq22nxznxfwcn")))

(define-public crate-cw20-0.12.0 (c (n "cw20") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-utils") (r "^0.12.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0ghcrqrag3i1sqfalbwbsgmjg11y502vsm8pqbr771j4rjlnxrqc")))

(define-public crate-cw20-0.12.1 (c (n "cw20") (v "0.12.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.12.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1whmr3rbm9a7vwsy9bask769r4rij4pkb0na6yp0c0bbgw1i2fa1")))

(define-public crate-cw20-0.13.0 (c (n "cw20") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1iwb68dg9fa5212m7f9bmn3yiv2mlskk19aiasww351l5kcn1pib")))

(define-public crate-cw20-0.13.1 (c (n "cw20") (v "0.13.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "10p6n06x5sm795cwsyjvrh17c9zinvz4xgnjjw4bpz3kdj0qzhd8")))

(define-public crate-cw20-0.13.2 (c (n "cw20") (v "0.13.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta8") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0h1zc0xdzmr8alaa7iigk98yzmi5p22x807a8hspdzn50933cv9m")))

(define-public crate-cw20-0.13.3 (c (n "cw20") (v "0.13.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "02v7amn0jlf3fbjd57fj93j2mj32qgwc046g9hmc11vwifsxz80i")))

(define-public crate-cw20-0.13.4 (c (n "cw20") (v "0.13.4") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "01gcizk5q2m5innv8cmydg0rx97vbz9gxg6vnm79m08hy6w85dsc")))

(define-public crate-cw20-0.14.0 (c (n "cw20") (v "0.14.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0dxpidvyl8c2n83195q32xvfw1davkwcgxmrp1daryqrqmcnyi2g")))

(define-public crate-cw20-0.15.0 (c (n "cw20") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "14gfm39hn6hqj4z1l6pwz8jfvhxzbahyx8skw22198y0hm58x92n")))

(define-public crate-cw20-0.15.1 (c (n "cw20") (v "0.15.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.15.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "16hxbr80arl1k0qvm3qf113cdkc2d5hf9wr19jbkwq3fzdv540pn")))

(define-public crate-cw20-0.16.0 (c (n "cw20") (v "0.16.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0qpfrnr8inz3ns11dk5daq46kszcgcmfbbjcydmbccyxlna8fnm4")))

(define-public crate-cw20-1.0.0 (c (n "cw20") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1pnbbx3bj03wwac5hzqnazrmi5hljzvx73y0fx82kql0k6ddd957")))

(define-public crate-cw20-1.0.1 (c (n "cw20") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.0.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0286sqvixhqkdz8b8w4vqw8gqkhi51d6bpwlzzaqs35lqyk6srli")))

(define-public crate-cw20-1.1.0 (c (n "cw20") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "16rsr9szqk2d0l4b86k5flplzxh64m855znl69fvs0421y94a701")))

(define-public crate-cw20-1.1.1 (c (n "cw20") (v "1.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "14jlqn3m8rg73f2vv2rryv86bby1hh9yhqr4rp777x1px6jrsvkq")))

(define-public crate-cw20-1.1.2 (c (n "cw20") (v "1.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "1gba4ial1x9yp8lmflsy9qlxmx1q01zp51h3rnhjakjk42xkjvjj")))

(define-public crate-cw20-2.0.0-rc.0 (c (n "cw20") (v "2.0.0-rc.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^2.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "1d2jg6d3cin6qhqzw6c6cib9gh82fz8amli4xm2m59ikmwv3spdl")))

(define-public crate-cw20-2.0.0 (c (n "cw20") (v "2.0.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^2.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "0z72s01jkqhbjggxbc1xbprj6iw94739fdkljfkdvfr9pyv148m4")))

