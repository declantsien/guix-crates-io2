(define-module (crates-io cw -a cw-address-like) #:use-module (crates-io))

(define-public crate-cw-address-like-1.0.0 (c (n "cw-address-like") (v "1.0.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)))) (h "0ml87smjwvibrl963ffl4afgkbwsm4rnh917hqkfryzs6wypzvql")))

(define-public crate-cw-address-like-1.0.1 (c (n "cw-address-like") (v "1.0.1") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)))) (h "0dj5yb8cn8q5s6f4brl8na3rwckl4y9mrz5gm3qqic10wbmra36a")))

(define-public crate-cw-address-like-1.0.2 (c (n "cw-address-like") (v "1.0.2") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)))) (h "1gcksaa3c9yb7gjszxbhacp2rn48klkv56b282c76x9ih2q6cs6v")))

(define-public crate-cw-address-like-1.0.3 (c (n "cw-address-like") (v "1.0.3") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)))) (h "08c6p74pvqz6imnjg1l0y27y7kb2n3npmfhvxj8zbsnlgqpsfdxi")))

(define-public crate-cw-address-like-1.0.4 (c (n "cw-address-like") (v "1.0.4") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)))) (h "106k67zp6a6sp0igzs3wkdkxck4xkrwqi2hacg0a721s128lc6j5")))

