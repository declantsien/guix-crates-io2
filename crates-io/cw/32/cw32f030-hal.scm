(define-module (crates-io cw #{32}# cw32f030-hal) #:use-module (crates-io))

(define-public crate-cw32f030-hal-0.1.0 (c (n "cw32f030-hal") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (d #t) (k 0)) (d (n "svd2rust") (r "^0.30.2") (d #t) (k 2)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "07n7zqx9gnci1rdyi0y0w12fc3dcrx9q11r25f8pw2zryp26c063")))

(define-public crate-cw32f030-hal-0.1.1 (c (n "cw32f030-hal") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (d #t) (k 0)) (d (n "svd2rust") (r "^0.30.2") (d #t) (k 2)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0pmhxq0j4kmsr6gf6ba9b5ns14si3rfpd5wl1xb85smm4jvh93qn")))

