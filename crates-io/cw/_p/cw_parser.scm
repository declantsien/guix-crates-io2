(define-module (crates-io cw _p cw_parser) #:use-module (crates-io))

(define-public crate-cw_parser-0.1.0 (c (n "cw_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "recap") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qrhk0ghnc50zf41hx5h25iva7pc38sizj4l7fxgzhlc1bknrj7n")))

(define-public crate-cw_parser-0.1.1 (c (n "cw_parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "recap") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.47.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rxnx505y9fdwf8qjcfx4apipwvqqbshysbrykxvivkc520zbnh7")))

(define-public crate-cw_parser-0.1.2 (c (n "cw_parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "recap") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.47.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l1y2mk19xzmrw6xny0plvbg65n7af2shhs6h0ch9glkg0qnympj")))

(define-public crate-cw_parser-0.1.3 (c (n "cw_parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "recap") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.47.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p4yqbw9wydw4qxx4vihfchfhll0z79pbf84f9zgg3fkcrgglsii")))

