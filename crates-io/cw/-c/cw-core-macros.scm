(define-module (crates-io cw -c cw-core-macros) #:use-module (crates-io))

(define-public crate-cw-core-macros-0.1.0 (c (n "cw-core-macros") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k4xcvvr1549x571lpp65zqqijmxdhdngfdh5k0s3j1dkm47f2pj")))

