(define-module (crates-io cw -c cw-core-interface) #:use-module (crates-io))

(define-public crate-cw-core-interface-0.1.0 (c (n "cw-core-interface") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-core-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "cw2") (r "^0.13") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1pl8dnx50qw2qrs1gj6mg4znb492mzqgkjibxpmpfds78m4nhgn9")))

