(define-module (crates-io cw -c cw-check-contract) #:use-module (crates-io))

(define-public crate-cw-check-contract-1.0.0 (c (n "cw-check-contract") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cosmwasm") (r "^0.7.2") (d #t) (k 0)) (d (n "cosmwasm-vm") (r "^1.0.0") (d #t) (k 0)))) (h "1snmrsixv6c6vn667ryykdnf76hvjklm5iadnwq79n4dirv85dzi") (y #t)))

(define-public crate-cw-check-contract-1.0.1 (c (n "cw-check-contract") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cosmwasm-vm") (r "^1.0.0") (d #t) (k 0)))) (h "15dy501mcdmlfhfm3bm2zb07mb414fq4j0pwp57h00c6wxr90jhl") (y #t)))

