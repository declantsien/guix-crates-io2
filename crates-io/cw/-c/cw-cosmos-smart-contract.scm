(define-module (crates-io cw -c cw-cosmos-smart-contract) #:use-module (crates-io))

(define-public crate-cw-cosmos-smart-contract-0.1.0 (c (n "cw-cosmos-smart-contract") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.5.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("cosmwasm_1_3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.17.0") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "cw2") (r "^1.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0lsck70rzjf1y43mxhyn1bg9mahinn0kgisjxy29gpxnrzbg4w2x") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

