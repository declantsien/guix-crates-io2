(define-module (crates-io cw -c cw-clearable) #:use-module (crates-io))

(define-public crate-cw-clearable-0.1.0 (c (n "cw-clearable") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.5.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.2.0") (d #t) (k 2)))) (h "0ii8f1xcp3l0nbzmy2dyd3014a2gz0va6000qbmaiab83xyzbwmk")))

(define-public crate-cw-clearable-0.1.1 (c (n "cw-clearable") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.5.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.2.0") (d #t) (k 2)))) (h "0xn3yvadj5g6lhrcawr01nb7qly5jrpknz6zskzlgi4rxg4vacyy")))

(define-public crate-cw-clearable-0.1.2 (c (n "cw-clearable") (v "0.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.5.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.2.0") (d #t) (k 2)))) (h "1qp2wb4b93c2absxp0aai73a9jc8idr2yl5fi2vrjmd5s10qj4cf")))

