(define-module (crates-io cw -b cw-band) #:use-module (crates-io))

(define-public crate-cw-band-0.1.0-alpha.1 (c (n "cw-band") (v "0.1.0-alpha.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "obi") (r "^0.0.2") (d #t) (k 0)))) (h "0ak8ikjv98c7dw6rn4fzm9xjlc4z68bw90sy37s8xqcnkb011wl6")))

(define-public crate-cw-band-0.1.0-alpha.2 (c (n "cw-band") (v "0.1.0-alpha.2") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "obi") (r "^0.0.2") (d #t) (k 0)))) (h "1nvxhk4mdp9nvch7snb1bz3if1xmn34liqr74bww20nv9kn32wqd")))

(define-public crate-cw-band-0.1.0 (c (n "cw-band") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "obi") (r "^0.0.2") (d #t) (k 0)) (d (n "base64") (r "^0.13.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1xiwm8ry4ls9pnqksl18kb8x93chh0hacma4klgzq6h4v9i92wx4") (y #t)))

(define-public crate-cw-band-0.1.1 (c (n "cw-band") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "obi") (r "^0.0.2") (d #t) (k 0)) (d (n "base64") (r "^0.13.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0h9cc3j0dbs3jph5471w4a9xf9g1ivsh4k05cjq4n6mq5blsl4m6")))

