(define-module (crates-io cw -b cw-bigint) #:use-module (crates-io))

(define-public crate-cw-bigint-0.4.3 (c (n "cw-bigint") (v "0.4.3") (d (list (d (n "arbitrary") (r "^1") (o #t) (k 0)) (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-integer") (r "^0.1.42") (f (quote ("i128"))) (k 0)) (d (n "num-traits") (r "^0.2.11") (f (quote ("i128"))) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "09v0syfs81vjhbifah6xrrbc5cqfjq25bwysycxsgrpdkivywa27") (f (quote (("std" "num-integer/std" "num-traits/std") ("default" "std"))))))

