(define-module (crates-io cw #{11}# cw1155) #:use-module (crates-io))

(define-public crate-cw1155-0.6.0-beta1 (c (n "cw1155") (v "0.6.0-beta1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "064y68xsx0p2nvip6y3qnb51g2f4prx97d8cbi6mxj2y6yz9cn6b")))

(define-public crate-cw1155-0.6.0-beta2 (c (n "cw1155") (v "0.6.0-beta2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-beta2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "15fv0hjhl2bxfbsndln318xa2p6khfa5r73f334r83flzfxsr9zi")))

(define-public crate-cw1155-0.6.0-beta3 (c (n "cw1155") (v "0.6.0-beta3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta5") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0bzaghk00phazk0ikrk1zpp5n3ly9pfn4fx46smvqvz7sxwwfj63")))

(define-public crate-cw1155-0.6.0 (c (n "cw1155") (v "0.6.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0vizsr6jh8a9nklm645mx5ffxcdybh707271mjbcfvjqn3hgxnjf")))

(define-public crate-cw1155-0.6.1 (c (n "cw1155") (v "0.6.1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw0") (r "^0.6.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0ybdwwbr3i96kmznlz3z5dgzd5iiy4k4i4si340dqi67c57b5m40")))

(define-public crate-cw1155-0.6.2 (c (n "cw1155") (v "0.6.2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw0") (r "^0.6.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0rpszx3j1sawhfw3mi1kz1jz0jnqrfikqlzssn8ggxdl4h9bdz8l")))

(define-public crate-cw1155-0.7.0 (c (n "cw1155") (v "0.7.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.15.0") (d #t) (k 0)) (d (n "cw0") (r "^0.7.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "02cjzqpg8xhvdkzwczimlwpdzfpglqgz1hnhslz7yhdaky1yir44")))

(define-public crate-cw1155-0.8.0-rc1 (c (n "cw1155") (v "0.8.0-rc1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0-rc5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0-rc5") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0-rc1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "176x970xw1hmy4aspqnmq30ri588bnd5vsfbiv30d1z3wblhcgvp")))

(define-public crate-cw1155-0.8.0-rc2 (c (n "cw1155") (v "0.8.0-rc2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0-rc2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0hgppr9xlrcm1zrhb5i1w7vgxxhfb9i4vh98c0f0l8qlpbdkj6jx")))

(define-public crate-cw1155-0.8.0-rc3 (c (n "cw1155") (v "0.8.0-rc3") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0-rc3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1721ndg9wkmhkhcmjypyrkgbwdp7s343nwvd28xa8p6pp5nqz3np")))

(define-public crate-cw1155-0.8.0 (c (n "cw1155") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1wi5wkhsap06ffsg9d6fwf0039apa2a51xjhcqi9yalbmh3w6hm7")))

(define-public crate-cw1155-0.8.1 (c (n "cw1155") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "14rl2fijsf8fy8i3gml5mbv2i1wsjxqnxrm1fypl1a76a3i17vbs")))

(define-public crate-cw1155-0.9.0 (c (n "cw1155") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.9.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1xqvv4619k4ccp1c4hn9rk4ig1580mqv4978518a4gddbfc09krb")))

(define-public crate-cw1155-0.10.0-soon (c (n "cw1155") (v "0.10.0-soon") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1rbc5v07k7ryrjbs7lp1a9sn5smzvakdj3viwyd8mblhzl55lk2g")))

(define-public crate-cw1155-0.9.1 (c (n "cw1155") (v "0.9.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1srn4mfld8my0jzbn1an021xi5vmnmapr8k0qaafimw9m0728zym")))

(define-public crate-cw1155-0.10.0-soon2 (c (n "cw1155") (v "0.10.0-soon2") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1ipz7fc92mg7vmrksj1dhgmiagnhcqn1blk0nv3k64m27m8kifz8")))

(define-public crate-cw1155-0.10.0-soon3 (c (n "cw1155") (v "0.10.0-soon3") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "063dnfq8x3dyqwf5r9fq17ks8p0xrc7nfg17qlmz3vmybpzqavg4")))

(define-public crate-cw1155-0.10.0-soon4 (c (n "cw1155") (v "0.10.0-soon4") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon2") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0aqcvw115xhxnxqn7j3agq7z6m15czs65ybjlsidhw3xa3nlh8ck")))

(define-public crate-cw1155-0.10.0 (c (n "cw1155") (v "0.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1dk1yn68vafyg7xcys1jkkqvwbb83naggd3h6jpxi24dnljcwrba")))

(define-public crate-cw1155-0.10.1 (c (n "cw1155") (v "0.10.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0bv14n525d1n0yavr90hzhqja8rqsxvrfr2is4kpfg9sfaa6zarr")))

(define-public crate-cw1155-0.10.2 (c (n "cw1155") (v "0.10.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1gxlkk6gxlywvibfd5abzax6p4mbn9fybckrrw13c87v40140ksw")))

(define-public crate-cw1155-0.10.3 (c (n "cw1155") (v "0.10.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0cmdf0rm6918znisl1qpbwiw9lkxfblg8863hrb6wkqp5zlm5nvk")))

(define-public crate-cw1155-0.11.0 (c (n "cw1155") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-utils") (r "^0.11.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1q9ggjf08azdz82wwfzr9r2hhln5sgn5pdrwhkkr8mygzvhs565c")))

(define-public crate-cw1155-0.11.1 (c (n "cw1155") (v "0.11.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-utils") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0jywd2kb25k2hkkzs15f79kwi20xakkgn78rf7d8cxb0fr49v62x")))

(define-public crate-cw1155-0.12.0 (c (n "cw1155") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-utils") (r "^0.12.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1087vlskxfks9fij405k12fgvvmfcqhhxyx0r69rh1a0ay5lcapk")))

(define-public crate-cw1155-0.12.1 (c (n "cw1155") (v "0.12.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.12.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0w6j8ah07lfi54kwfwzr5hbhczfldksw6v3mzp6migc9c28y81nv")))

(define-public crate-cw1155-0.13.0 (c (n "cw1155") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0gi9l5vxvryf63hhs24120lc8ckm0jpliigid9mh20icmgx0dhhq")))

(define-public crate-cw1155-0.13.1 (c (n "cw1155") (v "0.13.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0zxznmqaqvk9wcam8pbmlkj2b2kjl8jix1h5rkyva5pfad52gzbq")))

(define-public crate-cw1155-0.13.2 (c (n "cw1155") (v "0.13.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta8") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "01fk81k4bnr8qbxrnk103rkmxp1dhkjy3ppj32ah3pv78pymghdr")))

(define-public crate-cw1155-0.13.3 (c (n "cw1155") (v "0.13.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1j7gqxjjwd688b0dpd7aj7bn8wq5mdl7syr8fkh7pd10ava69d3m")))

(define-public crate-cw1155-0.13.4 (c (n "cw1155") (v "0.13.4") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1g4vixx44gljh4vplcl9h9bqdr2p87r2b480fwqnai6q1yd7q6a2")))

(define-public crate-cw1155-0.14.0 (c (n "cw1155") (v "0.14.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1bwfaydjsqc2gpqi9v29vknw1bhdn9bb3q6vxis8h7bzmwvcqjqf")))

(define-public crate-cw1155-0.15.0 (c (n "cw1155") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "05v0qbb84fqb8611fg1zdsghf6bwcb4msw375z2cbym4afgsm0hy")))

(define-public crate-cw1155-0.15.1 (c (n "cw1155") (v "0.15.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.15.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1nbkwhmblv8ah22cr66iam9p8k2sykg7l81q8ldmmr9v8jd7vn6v")))

(define-public crate-cw1155-0.16.0 (c (n "cw1155") (v "0.16.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1icpd0lm16767mq2q4m119ml3igy1bqvq3x0iqzhxg2sv6sfpq2b")))

