(define-module (crates-io cw -o cw-orch-contract-derive) #:use-module (crates-io))

(define-public crate-cw-orch-contract-derive-0.11.0-alpha.1 (c (n "cw-orch-contract-derive") (v "0.11.0-alpha.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hn586cyqfv8z95d0apq48wvxcpg1hbhrg6cwhsgi5ab4b7fr9mg") (f (quote (("propagate_daemon"))))))

(define-public crate-cw-orch-contract-derive-0.11.0-alpha.2 (c (n "cw-orch-contract-derive") (v "0.11.0-alpha.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zggvbx8bi9nkv0dpyqdyfajd5kfw8ljg078w6ji3b17nasf4f1y") (f (quote (("propagate_daemon"))))))

(define-public crate-cw-orch-contract-derive-0.11.0-alpha.3 (c (n "cw-orch-contract-derive") (v "0.11.0-alpha.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "056xs5lb0g3i63rphg655l5vh6ak10r9xla30nppkh3k7ahiw5w9") (f (quote (("propagate_daemon"))))))

(define-public crate-cw-orch-contract-derive-0.11.0 (c (n "cw-orch-contract-derive") (v "0.11.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pl0g003gnck47prwwwrhlq7b3pjccw737x3ppclwapf5s67d73n") (f (quote (("propagate_daemon"))))))

(define-public crate-cw-orch-contract-derive-0.11.1 (c (n "cw-orch-contract-derive") (v "0.11.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ihhwf7da4syyj7801djqhnn1mi3xx65mhz6xr5iyjwk5ikdwjgz") (f (quote (("propagate_daemon"))))))

(define-public crate-cw-orch-contract-derive-0.11.2 (c (n "cw-orch-contract-derive") (v "0.11.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r4xnfkzh4jcy3xhz6ybdj1n0m1bf0mw32wbzxdzh1wimny7zyjy") (f (quote (("propagate_daemon"))))))

(define-public crate-cw-orch-contract-derive-0.11.3 (c (n "cw-orch-contract-derive") (v "0.11.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wma5qd7zm0ma0cfb73qfz9q38mgxj608bsrqipz8dkrj82bx11s") (f (quote (("propagate_daemon"))))))

(define-public crate-cw-orch-contract-derive-0.11.4 (c (n "cw-orch-contract-derive") (v "0.11.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yirj38zy50rbvgm8nnqvcjg3nanmywqq7vvrgsg92whyi8qghpr") (f (quote (("propagate_daemon"))))))

(define-public crate-cw-orch-contract-derive-0.12.0 (c (n "cw-orch-contract-derive") (v "0.12.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17kili60ag98csdnvlgadslcd8blrwg0zwn1931kcimqbdl1fpc0")))

(define-public crate-cw-orch-contract-derive-0.12.1 (c (n "cw-orch-contract-derive") (v "0.12.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d8xc98jfxjyp2xpsm2sman4wy1biwppdp8klm5c7mkzp0k22i0n")))

(define-public crate-cw-orch-contract-derive-0.13.0 (c (n "cw-orch-contract-derive") (v "0.13.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jdsyz37hk9vyqmqncq4pp8f45bp340nsbnmlrrw0gn5bf4r57nj")))

(define-public crate-cw-orch-contract-derive-0.13.1 (c (n "cw-orch-contract-derive") (v "0.13.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yjmpxvirn24f3y6z53pa69lwv758fsiy78p19xxxkda1hkcqdq6")))

(define-public crate-cw-orch-contract-derive-0.13.2 (c (n "cw-orch-contract-derive") (v "0.13.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p0yrjsay5jkyzax3vgh2r43blwda8n4n0q0ccs5zpm9nk9lk2la")))

(define-public crate-cw-orch-contract-derive-0.13.3 (c (n "cw-orch-contract-derive") (v "0.13.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10caxk5a7vlx3qr9qkp1ci2bkkcvfgspdyy1hfwyyk8b2s9wkzj5")))

(define-public crate-cw-orch-contract-derive-0.14.0 (c (n "cw-orch-contract-derive") (v "0.14.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zp1sm6n69a1wn0688m44yww6bdjb3vg5mz3cxywahr11bxipwkv")))

(define-public crate-cw-orch-contract-derive-0.15.0 (c (n "cw-orch-contract-derive") (v "0.15.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gma28kvc752m6fsdz4krjg2vywjmzms2iglbk323x9j3jv6pd71")))

(define-public crate-cw-orch-contract-derive-0.17.0 (c (n "cw-orch-contract-derive") (v "0.17.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v2blvg0j8jfrn6q6qk8785lcm1692kw66gbkkjdlimi5rv7qh5r")))

(define-public crate-cw-orch-contract-derive-0.18.0 (c (n "cw-orch-contract-derive") (v "0.18.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "085a8v3qsp4nc4zd0mnf1n6hmqrrnp337qaf46dgq200m3z8hbqm")))

(define-public crate-cw-orch-contract-derive-0.20.0 (c (n "cw-orch-contract-derive") (v "0.20.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00xf0sbxxx99cnawr6jkicpqz7cmvmrlxb2ljv9hy2hr62mmvv4g")))

(define-public crate-cw-orch-contract-derive-0.20.1 (c (n "cw-orch-contract-derive") (v "0.20.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09lphjdqj6rcykpnxfwx2rnzm0vznkyba7ys0xnw4495dvjc7scl")))

(define-public crate-cw-orch-contract-derive-0.21.0 (c (n "cw-orch-contract-derive") (v "0.21.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hsmwchc6llk5winp8kf9s7772i0vk9gly0wx4qbvirgd5svmj2v")))

