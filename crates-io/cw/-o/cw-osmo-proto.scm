(define-module (crates-io cw -o cw-osmo-proto) #:use-module (crates-io))

(define-public crate-cw-osmo-proto-0.1.0 (c (n "cw-osmo-proto") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta5") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)))) (h "0hxcqddzfhj956rd0yx9w5z185dfy7v1b3h8jxi1fpkqbpil91w6")))

