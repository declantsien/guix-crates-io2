(define-module (crates-io cw -o cw-orch-fns-derive) #:use-module (crates-io))

(define-public crate-cw-orch-fns-derive-0.11.0-alpha.1 (c (n "cw-orch-fns-derive") (v "0.11.0-alpha.1") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1r4spc9k9s73ra41jsvnxdvm8vb9jq9w3ljz4h4bhicf8i2yb402")))

(define-public crate-cw-orch-fns-derive-0.11.0-alpha.2 (c (n "cw-orch-fns-derive") (v "0.11.0-alpha.2") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0kvq85s6jkh31xb59827a8x70axnx4lim3y6db5ymvi9xdivssnk")))

(define-public crate-cw-orch-fns-derive-0.11.0-alpha.3 (c (n "cw-orch-fns-derive") (v "0.11.0-alpha.3") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "09mr888cxl5sr7np176s8j0hj8cc3jbsh3i88c02ihb9xmsllij0")))

(define-public crate-cw-orch-fns-derive-0.11.0-alpha.4 (c (n "cw-orch-fns-derive") (v "0.11.0-alpha.4") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1xn320wg41fmcx36c6m7xgfdw0sh584rcp930qdrr0w0ch4242xs")))

(define-public crate-cw-orch-fns-derive-0.11.0 (c (n "cw-orch-fns-derive") (v "0.11.0") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "03vwhqlb5k5s15zxq0q42vhmv96kp00h1s7v6im9x177rd2k6xzy")))

(define-public crate-cw-orch-fns-derive-0.11.1 (c (n "cw-orch-fns-derive") (v "0.11.1") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1w52rcqcaih6z8raf6ybha6m7nw8wanl7l19n8mingmalm8f6912")))

(define-public crate-cw-orch-fns-derive-0.11.2 (c (n "cw-orch-fns-derive") (v "0.11.2") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "00wrqva9w70zq9k6wblif3k2d0m8rmkwgrix567hd1ifghd27siw")))

(define-public crate-cw-orch-fns-derive-0.11.3 (c (n "cw-orch-fns-derive") (v "0.11.3") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1wk0avlx5mgv0nihh2x9rx0ckmbdjiklj5qxkxvlrkv69snh98ag")))

(define-public crate-cw-orch-fns-derive-0.11.4 (c (n "cw-orch-fns-derive") (v "0.11.4") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "113warac0waq02a1skd28i7kdkfscbvhdkyq8yc7q3his89m5mky")))

(define-public crate-cw-orch-fns-derive-0.12.0 (c (n "cw-orch-fns-derive") (v "0.12.0") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "160cgym7jjcmhyqizhjbc5zrxkfh4f4qs4vh3l7a4vdx4aya4dgk")))

(define-public crate-cw-orch-fns-derive-0.12.1 (c (n "cw-orch-fns-derive") (v "0.12.1") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "05v1pi6bs7nad37yyd42q28j8ylapq64z72v168szl2v9jjgvwak")))

(define-public crate-cw-orch-fns-derive-0.13.0 (c (n "cw-orch-fns-derive") (v "0.13.0") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "034yg1xk6xfl8r9pdsw3wscfgnyh54g5y6rym2z2fdml7xmr77qj")))

(define-public crate-cw-orch-fns-derive-0.13.1 (c (n "cw-orch-fns-derive") (v "0.13.1") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "11w0p8w3zvvckakd1mbmmmgn43pzbf54hkiy4vg7qqsfrdb7z53d")))

(define-public crate-cw-orch-fns-derive-0.13.2 (c (n "cw-orch-fns-derive") (v "0.13.2") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0bssgnfa1fazkyfpsnzbjk07gfk82rw3w3q1mpxr556qwzp2sc91")))

(define-public crate-cw-orch-fns-derive-0.13.3 (c (n "cw-orch-fns-derive") (v "0.13.3") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0f5s66wdsx8gwm0333vxdv93k2gj5mc1nl72rh33hzjbgjxkz6z7")))

(define-public crate-cw-orch-fns-derive-0.14.0 (c (n "cw-orch-fns-derive") (v "0.14.0") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "07cplwaza6mhnlc0n62gpgp8a7l3w7sjkny86amlsw6bs89djn8b")))

(define-public crate-cw-orch-fns-derive-0.15.0 (c (n "cw-orch-fns-derive") (v "0.15.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1gna3l9r10v7wbs7b7wpkap86fmpjyjadq6c42yqygxqc6l6xvag")))

(define-public crate-cw-orch-fns-derive-0.16.0 (c (n "cw-orch-fns-derive") (v "0.16.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1gjzb812md489jq4in5xk8p20jigwczwh6zly52mfchhlsxn2ny8")))

(define-public crate-cw-orch-fns-derive-0.16.1 (c (n "cw-orch-fns-derive") (v "0.16.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0kswbfirqrl6p7kad2h55y8p4l9mfqxhdy11hp5nlhyh3anl4wnv")))

(define-public crate-cw-orch-fns-derive-0.17.0 (c (n "cw-orch-fns-derive") (v "0.17.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1x1dwvj03jwaygfa0z65qj3k8la1frdm8x29syrzf4vwjl9wpxsz")))

(define-public crate-cw-orch-fns-derive-0.18.0 (c (n "cw-orch-fns-derive") (v "0.18.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1zab4463qg3xrjvcalynilgs66x9hfdb9qai7c10c93l12vx23b5")))

(define-public crate-cw-orch-fns-derive-0.18.1 (c (n "cw-orch-fns-derive") (v "0.18.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "08fv0mcmglpcl8y8wikad5n8z3xxk6difam9q1ya0jls7j2ws8v4")))

(define-public crate-cw-orch-fns-derive-0.19.0 (c (n "cw-orch-fns-derive") (v "0.19.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0g81yarxfqggzamklacck3s2mqgsfjdvjnxpdd2yr814ggxwcyh0") (y #t)))

(define-public crate-cw-orch-fns-derive-0.19.1 (c (n "cw-orch-fns-derive") (v "0.19.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0qvlaal815fkbij7xsmnzwg3r21rn7zryaiivymm5k7sbfhvgb79")))

