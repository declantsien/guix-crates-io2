(define-module (crates-io cw -o cw-ownable-derive) #:use-module (crates-io))

(define-public crate-cw-ownable-derive-0.1.0 (c (n "cw-ownable-derive") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 2)) (d (n "cw-utils") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vxp9sslv58vjy82sryfgg1kca3qpyrqnaaz0j6crid870pb01v3")))

(define-public crate-cw-ownable-derive-0.1.1 (c (n "cw-ownable-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1xixnw8xqnnxj2aidc15p6f2bis71v6k2h9hxwvfbm2kq8cg4rwd")))

(define-public crate-cw-ownable-derive-0.2.0 (c (n "cw-ownable-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14mm7iq8r9c0mrzg0a9kicvp5n29bnnd4c6dx62p3x56z805fh85")))

(define-public crate-cw-ownable-derive-0.4.0 (c (n "cw-ownable-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02bwj0pygiqcibrdd6ginnnx9qzl05mdx3gpkpbh39w824pa186r")))

(define-public crate-cw-ownable-derive-0.5.0 (c (n "cw-ownable-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1xkpglj4xh0rglb2lfgbn4nhf3033i2kcm1vcv11ivhladdpsi5c")))

(define-public crate-cw-ownable-derive-0.5.1 (c (n "cw-ownable-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1bw9m104pwbr3vwi3ig07jykywfg64fl8z8d236bc6rl1wpbzlx1")))

