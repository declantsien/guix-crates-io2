(define-module (crates-io cw -o cw-orch-traits) #:use-module (crates-io))

(define-public crate-cw-orch-traits-0.16.0 (c (n "cw-orch-traits") (v "0.16.0") (d (list (d (n "cosmrs") (r "^0.14.0") (d #t) (k 0)) (d (n "cw-orch-core") (r "^0.16.0") (d #t) (k 0)))) (h "11k0rxl749m2f5jzq89vpk1ykjnkbdjd1p9r4rb8arvpcdmai7m4") (y #t)))

(define-public crate-cw-orch-traits-0.16.1 (c (n "cw-orch-traits") (v "0.16.1") (d (list (d (n "cw-orch-core") (r "^0.16.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)))) (h "1nssxv46bfagvaiisi7ys9mibv1wiz9vdrmw97rwq1z9mgs0914g")))

(define-public crate-cw-orch-traits-0.16.2 (c (n "cw-orch-traits") (v "0.16.2") (d (list (d (n "cw-orch-core") (r "^0.16.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)))) (h "1z9f2l5qnbdznhzk1vv4pn9zfnyh0bnpc36apbn9xr03qrma70as")))

(define-public crate-cw-orch-traits-0.17.0 (c (n "cw-orch-traits") (v "0.17.0") (d (list (d (n "cw-orch-core") (r "^0.17.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)))) (h "1nlkh4mhxmxybj2s2y1bahxvkz5wzgn31cw5v1yb3ymm00ylp70i")))

(define-public crate-cw-orch-traits-0.18.0 (c (n "cw-orch-traits") (v "0.18.0") (d (list (d (n "cw-orch-core") (r "^0.18.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)))) (h "1v61q7s6yvrq1qhz4dsvylcajcvbfrl70ss1rrrkd7882qkxk9g3")))

(define-public crate-cw-orch-traits-0.19.0 (c (n "cw-orch-traits") (v "0.19.0") (d (list (d (n "cw-orch-core") (r "^0.19.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1rqsmq04h5q34jqlpjqb8lvpjr4wdiysd56vidplcm4pimfjk1pp")))

(define-public crate-cw-orch-traits-0.20.0 (c (n "cw-orch-traits") (v "0.20.0") (d (list (d (n "cw-orch-core") (r "^0.20.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)))) (h "11m08wmyhcn07cs7v63q0pqf429hdlijmch7h62v1jw88j10h6qd")))

(define-public crate-cw-orch-traits-0.19.1 (c (n "cw-orch-traits") (v "0.19.1") (d (list (d (n "cw-orch-core") (r "^0.19.1") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)))) (h "04p7hgz7p5s77y33b9zkrz8r4cng4vsf7s68rwjmcq205kdjvbxv")))

(define-public crate-cw-orch-traits-0.21.0 (c (n "cw-orch-traits") (v "0.21.0") (d (list (d (n "cw-orch-core") (r "^0.21.1") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)))) (h "1y8w0m3d03zr7bkb4k77ba90h4m1z9l09n23aqhxg7778whg31nz")))

(define-public crate-cw-orch-traits-0.22.0-rc (c (n "cw-orch-traits") (v "0.22.0-rc") (d (list (d (n "cw-orch-core") (r "^1.0.0-rc") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)))) (h "0qp9h634jh4746a74k4apd7vbzr102nazw6pz5x2ns5kj1j402a5")))

(define-public crate-cw-orch-traits-0.22.0 (c (n "cw-orch-traits") (v "0.22.0") (d (list (d (n "cw-orch-core") (r "^1.0.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)))) (h "0fkwy2bv1qnvrchb4aaypzclvabfffia0cvrnja2b9fqx4lwwnar")))

