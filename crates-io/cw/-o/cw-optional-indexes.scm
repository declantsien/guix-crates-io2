(define-module (crates-io cw -o cw-optional-indexes) #:use-module (crates-io))

(define-public crate-cw-optional-indexes-0.1.0 (c (n "cw-optional-indexes") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1kfhlysxackg6izaz1ndpdjs6rgj0znjb3jn31dfqrd23sm63x36")))

(define-public crate-cw-optional-indexes-0.1.1 (c (n "cw-optional-indexes") (v "0.1.1") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1ghhzghpnfszcg37sqrgnbg6dy8bd3n9smya9bk53qz54ndk5b3r")))

