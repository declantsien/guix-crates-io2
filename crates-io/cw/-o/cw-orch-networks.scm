(define-module (crates-io cw -o cw-orch-networks) #:use-module (crates-io))

(define-public crate-cw-orch-networks-0.15.0 (c (n "cw-orch-networks") (v "0.15.0") (d (list (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0m7a3h2897y6z96nb2abl7fqcmpf2hrg7zzd3ic8frd7xgsbrqal")))

(define-public crate-cw-orch-networks-0.16.0 (c (n "cw-orch-networks") (v "0.16.0") (d (list (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0krzxr06kd26yz4v7bgaq8qhnwhxji126yqvcgv5lkfs9cjsv96m")))

(define-public crate-cw-orch-networks-0.17.0 (c (n "cw-orch-networks") (v "0.17.0") (d (list (d (n "cw-orch-core") (r "^0.17.0") (d #t) (k 0)) (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0963gahg3bhzf05id052hffnnizcqfh2sinvaai05qwxalf6fbn9")))

(define-public crate-cw-orch-networks-0.18.0 (c (n "cw-orch-networks") (v "0.18.0") (d (list (d (n "cw-orch-core") (r "^0.18.0") (d #t) (k 0)) (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1r3yh0alhhs3zcp2qpjjdp44ay7k8b4lzd3lm31iayjgxlpynbl6")))

(define-public crate-cw-orch-networks-0.18.1 (c (n "cw-orch-networks") (v "0.18.1") (d (list (d (n "cw-orch-core") (r "^0.18.0") (d #t) (k 0)) (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0di3afkclm25d107hsl3yay309gwfnk3dxwaj2ylm1s5klv58cdc")))

(define-public crate-cw-orch-networks-0.19.0 (c (n "cw-orch-networks") (v "0.19.0") (d (list (d (n "cw-orch-core") (r "^0.19.0") (d #t) (k 0)) (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1ds8lgpxcaf5l4gnff29s4jh9sn38k2c2qf6c12zfkip7j0sn23a")))

(define-public crate-cw-orch-networks-0.20.0 (c (n "cw-orch-networks") (v "0.20.0") (d (list (d (n "cw-orch-core") (r "^0.20.1") (d #t) (k 0)) (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0sg955g9ixgzjpwf3isxkw3jx1xzqyaz4vnsf1gjk8mmjz4l4jgs") (y #t)))

(define-public crate-cw-orch-networks-0.20.1 (c (n "cw-orch-networks") (v "0.20.1") (d (list (d (n "cw-orch-core") (r "^0.20.1") (d #t) (k 0)) (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0snxzym8g5qvamq4swjndj56zqsn5mkhjkwjznd7f44xa89gbamz")))

(define-public crate-cw-orch-networks-0.20.2 (c (n "cw-orch-networks") (v "0.20.2") (d (list (d (n "cw-orch-core") (r "^0.21.1") (d #t) (k 0)) (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1axkzi3g5qzc8c1ljb1ciakq6qfgdj0l453v3vz3pv6szs8gj08m") (y #t)))

(define-public crate-cw-orch-networks-0.20.3 (c (n "cw-orch-networks") (v "0.20.3") (d (list (d (n "cw-orch-core") (r "^0.21.1") (d #t) (k 0)) (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "17zvkvqvygp022z60gclgxd9vfz0ppf12skf3m4nxy0bmksra3gx") (y #t)))

(define-public crate-cw-orch-networks-0.20.4 (c (n "cw-orch-networks") (v "0.20.4") (d (list (d (n "cw-orch-core") (r "^0.21.1") (d #t) (k 0)) (d (n "ibc-chain-registry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1m1x9mgc85jzzpsc2bllg135whh5m7p61ffs7h2r0bhwkakp7x05")))

(define-public crate-cw-orch-networks-0.22.0-rc (c (n "cw-orch-networks") (v "0.22.0-rc") (d (list (d (n "cw-orch-core") (r "^1.0.0-rc") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1p2bwss2cdm9gi5x8h29rqdi785r59f6wyv11nrzb03gprdpq40j")))

(define-public crate-cw-orch-networks-0.22.0 (c (n "cw-orch-networks") (v "0.22.0") (d (list (d (n "cw-orch-core") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1m55qbdwjz96ms2d6fi8wyjb8gvvn8jfr39msi4bzaj3n9pxhmvm")))

