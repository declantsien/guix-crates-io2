(define-module (crates-io cw -h cw-hooks) #:use-module (crates-io))

(define-public crate-cw-hooks-2.0.1 (c (n "cw-hooks") (v "2.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "18fzlcd7wjf69s1znr58lfyfjjkqszp6zvkn9pwphbxvvar27cf1")))

(define-public crate-cw-hooks-2.1.3 (c (n "cw-hooks") (v "2.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "14w2llfmhm681wd3h80ng0dl6y551aiclx798adnnb5cajzixcnz") (y #t)))

(define-public crate-cw-hooks-2.1.5 (c (n "cw-hooks") (v "2.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zpa6yky7cq07pglw4lxk0a04fi56ssadlhyi1clr0xl6lw9w3pi") (y #t)))

(define-public crate-cw-hooks-2.2.0 (c (n "cw-hooks") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0k1hlpcnr6n2vildfsvhq2r16asd4kb30ppjyjlkv4izyzgyg6vb")))

(define-public crate-cw-hooks-2.3.0 (c (n "cw-hooks") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04kgxkwampxzs9bgx1jj04x8b260rd1d03511pcd321sykbsn4cz")))

