(define-module (crates-io qu ee queens-rock) #:use-module (crates-io))

(define-public crate-queens-rock-0.1.0 (c (n "queens-rock") (v "0.1.0") (d (list (d (n "image") (r "^0.20.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)))) (h "0rsdawn9qdjh8g5kls3fdbil9ffpaadhbjnynb5fbaw02472q3zk") (f (quote (("unstable"))))))

(define-public crate-queens-rock-0.1.1 (c (n "queens-rock") (v "0.1.1") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)))) (h "1aqm9g7azk7pagw5zmxzv0qz9jc3wcmi76pdp7ivpi6rqhgbisyy") (f (quote (("unstable"))))))

