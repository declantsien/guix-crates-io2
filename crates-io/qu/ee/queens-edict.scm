(define-module (crates-io qu ee queens-edict) #:use-module (crates-io))

(define-public crate-queens-edict-0.1.0 (c (n "queens-edict") (v "0.1.0") (d (list (d (n "owo-colors") (r "^3.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1d4ir7v5z26qgav8qkb9j22cc9l7qpxp6wmjnzi8pm8mn9kki3v2")))

(define-public crate-queens-edict-0.1.1 (c (n "queens-edict") (v "0.1.1") (d (list (d (n "owo-colors") (r "^3.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "18qkiw0hlxn51y5x1pnbwb0p8l3qkskvfq8ixd9i0a8sbhql7j9q")))

