(define-module (crates-io qu ee queen-log) #:use-module (crates-io))

(define-public crate-queen-log-0.1.0 (c (n "queen-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "16gdpa0afn2pd7c07l50vvzi63pjhzkm9mr0c87kjayyvfplmn4n")))

(define-public crate-queen-log-0.1.1 (c (n "queen-log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "17lyi2fxp4104qjm56gg4w02f16ip63685rwqfrjpn2ayjalh16n")))

(define-public crate-queen-log-0.1.2 (c (n "queen-log") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1xmhk0qniczibznvfmjxw6mjp50xqsrkwdhjbrkd62ds17bmp95r")))

(define-public crate-queen-log-0.2.0 (c (n "queen-log") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1y1kzggqlrxdybmlq1smi603c9lkqihw1bnljach650h29xwif8s")))

(define-public crate-queen-log-0.2.1 (c (n "queen-log") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0vvci4idd4344s55ch9w3v4rafa5gdv7wchac395hfphj3ymgd65")))

(define-public crate-queen-log-0.2.2 (c (n "queen-log") (v "0.2.2") (d (list (d (n "humantime") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1drbdh7gc76rwzkdq1ih6sqv85imh1fd8pnsdvn2l479syl4n3b7")))

(define-public crate-queen-log-0.3.0 (c (n "queen-log") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1q7h5ailran3vc95zy8xrw2lxcb4iw4b634zsdifsl1j2gaphll7") (y #t)))

(define-public crate-queen-log-0.3.1 (c (n "queen-log") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "047ijswi2w5i1fdyscd7rlksbx198yq41n85si120n3y2xcb1pb9")))

(define-public crate-queen-log-0.4.0 (c (n "queen-log") (v "0.4.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "081cyzyagd4fjvjw840dyqqaxyv0pdj6f6sf3qgcw5zw1f5sgcyk")))

