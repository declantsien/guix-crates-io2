(define-module (crates-io qu ee queen-core) #:use-module (crates-io))

(define-public crate-queen-core-0.1.0 (c (n "queen-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "nson") (r "^0.1") (d #t) (k 0)) (d (n "queen-io") (r "^0.1") (d #t) (k 0)))) (h "0jlin9v7amg9z5kyph4b69nnhlvqn8qkl50d9gf96yyys5j0i87b")))

