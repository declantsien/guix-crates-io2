(define-module (crates-io qu ee quee) #:use-module (crates-io))

(define-public crate-quee-0.1.0 (c (n "quee") (v "0.1.0") (d (list (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.8.0") (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.1") (d #t) (k 1)))) (h "03bis5li1lknsk86prkxyyg89ddgkpf4f5ldxqdk3rgjj8qfiakl")))

