(define-module (crates-io qu ee queen-io) #:use-module (crates-io))

(define-public crate-queen-io-0.1.0 (c (n "queen-io") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ypyll4kn8j5fz7nfy595h13gf8ghfdq5sd2jg6iqhc18mg90dfw")))

(define-public crate-queen-io-0.1.1 (c (n "queen-io") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p146fhwi5g74qp9brx5b5xw41vwd7476m14ffy05lz5dcvc8rfa")))

(define-public crate-queen-io-0.1.5 (c (n "queen-io") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "060z4dim803gyflmflvknkdap6lys8y1ggd0a0npvl9r2b8844ap")))

(define-public crate-queen-io-0.1.6 (c (n "queen-io") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g5b3033q6q8hab8g51afik4phybqgr3c6pzdrmy862qid0kipjx")))

(define-public crate-queen-io-0.2.0 (c (n "queen-io") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mk4162hqpbjr369n255vkmsscj64iq8fjzj6l5878fg5x3cgfbm")))

(define-public crate-queen-io-0.2.1 (c (n "queen-io") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s00ywwida361lrfxvk9h1r1yqgxbdvr2xccp4arpr8cly893his")))

(define-public crate-queen-io-0.2.2 (c (n "queen-io") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10970y8hj37wrcww6bzlxjik4xp8kkpjnak7x01dsfwhr06r2b45")))

(define-public crate-queen-io-0.3.0 (c (n "queen-io") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15h24mdh0nnw07zn33qh1cqbs5wpy973vp2387cl9z1kv86hbkxq")))

(define-public crate-queen-io-0.3.1 (c (n "queen-io") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0695h5lxngb4fxc9bcry9alg0qbggkvh28rdi8rxjir0scwxwzxb")))

(define-public crate-queen-io-0.3.2 (c (n "queen-io") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07sl7ikfxj06ffcl0bka8sc48r9wg3pc6w03clrnv95552sf4ky9")))

(define-public crate-queen-io-0.4.0 (c (n "queen-io") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "007d37f42rk7by3clw24rhfap3cwv2mahps12wafsj7mbk8rd4qn")))

(define-public crate-queen-io-0.4.1 (c (n "queen-io") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rf96csf4a5a11x97bf6wwlq8k79mpn6jsi5sz4fsnhwa5k5n93d")))

(define-public crate-queen-io-0.4.2 (c (n "queen-io") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zqg2b747hr9g1afh8nn0a99lcfx9dkbflmjmw1bq1jscyp16zgf")))

(define-public crate-queen-io-0.4.3 (c (n "queen-io") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hxw6jlndyvg9yjy3zbx5vxjf7r9mafsimvaqrqsbz7jygfn4pxk")))

(define-public crate-queen-io-0.4.4 (c (n "queen-io") (v "0.4.4") (d (list (d (n "indexmap") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cis5yk6l4yswb1mp7q3jmx2zp70kx3lsa9vnav6ljnxhkcvi2vf")))

(define-public crate-queen-io-0.4.5 (c (n "queen-io") (v "0.4.5") (d (list (d (n "indexmap") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1742hlv6ap8i4hrwm7bnk01zcz65mj66rddmpg16jgw1m6cyg223")))

(define-public crate-queen-io-0.4.6 (c (n "queen-io") (v "0.4.6") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i0839i144q1mzcn7qwqsvkw7d5acvaqq7w6c5azpfi6bkvzsdh3")))

(define-public crate-queen-io-0.4.7 (c (n "queen-io") (v "0.4.7") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19gjbpl5hksp8na02nlfrnl7p2av3agcliysfhvgyhi36zjn6ig4")))

(define-public crate-queen-io-0.4.8 (c (n "queen-io") (v "0.4.8") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05vb9vz2cky04wbj5gfxiaxm2vavshqzpbh50zvb57y7qagal22n")))

(define-public crate-queen-io-0.5.0 (c (n "queen-io") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kn8lvwxq7vcsl97vslydr8szm0913j7hzz2vvq60fqgg0dvz5p9")))

(define-public crate-queen-io-0.5.1 (c (n "queen-io") (v "0.5.1") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hgjz43m0pc4vpk030njkh1mqncz1a6ar8js14wlxas9c2iwr65s")))

(define-public crate-queen-io-0.6.0 (c (n "queen-io") (v "0.6.0") (d (list (d (n "indexmap") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w92fkcd4xk6dc4gp7zm34i938zn91x04bxwxh7kzln74p526ypl")))

(define-public crate-queen-io-0.6.1 (c (n "queen-io") (v "0.6.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hy9q2df7xw1hbymmrz22azhc8bfypc9cdpqrc43gfj4v8qhq9hx")))

