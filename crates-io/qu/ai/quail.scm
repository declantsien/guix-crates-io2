(define-module (crates-io qu ai quail) #:use-module (crates-io))

(define-public crate-quail-0.1.0 (c (n "quail") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^4.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0v1xjxcjxyznrbjghi54pg8njsw913kkjhlla0wp6pysgby3vfza")))

