(define-module (crates-io qu ai quaigh) #:use-module (crates-io))

(define-public crate-quaigh-0.0.1 (c (n "quaigh") (v "0.0.1") (d (list (d (n "cat_solver") (r "^3.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "volute") (r "^1.0.3") (d #t) (k 0)))) (h "1aq98mjiifsg8s3inx8gszqwyrkdrd68sy7jjncvna0fj1rzyksv")))

(define-public crate-quaigh-0.0.2 (c (n "quaigh") (v "0.0.2") (d (list (d (n "cat_solver") (r "^3.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "volute") (r "^1.0.3") (d #t) (k 0)))) (h "1lh1mvfrmhhqb2l6f6jixr3gz6i1p6qii8l908yvqm54l8hf6qv5")))

(define-public crate-quaigh-0.0.3 (c (n "quaigh") (v "0.0.3") (d (list (d (n "cat_solver") (r "^3.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "kdam") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "volute") (r "^1.0.3") (d #t) (k 0)))) (h "02c3wp1l2ycadsv0lff5hzqcwc8njjygxsm8ly1pk35ywq9cd0z0")))

(define-public crate-quaigh-0.0.4 (c (n "quaigh") (v "0.0.4") (d (list (d (n "cat_solver") (r "^3.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "kdam") (r "^0.5") (f (quote ("template"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "volute") (r "^1.0.3") (d #t) (k 0)))) (h "103afk3bx4zvhvfd64wcad1wqf55scjh9xb6nhfyrf0yc9pla957")))

(define-public crate-quaigh-0.0.5 (c (n "quaigh") (v "0.0.5") (d (list (d (n "cat_solver") (r "^3.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "kdam") (r "^0.5") (f (quote ("template"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "volute") (r "^1.1.3") (d #t) (k 0)))) (h "1sfqp9psjz1gjhd5rra62f7llki85if5gdph73kcy3i734rh1x5f")))

