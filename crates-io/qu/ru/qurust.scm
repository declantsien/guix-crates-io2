(define-module (crates-io qu ru qurust) #:use-module (crates-io))

(define-public crate-qurust-0.1.0 (c (n "qurust") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.9.5") (d #t) (k 2)))) (h "0swcgwmlw6fcbfshyj57f1bb12yb1yfilp02ql1b8cydva943cqp")))

(define-public crate-qurust-0.2.0 (c (n "qurust") (v "0.2.0") (d (list (d (n "antlr-rust") (r "^0.3.0-beta") (d #t) (k 0)) (d (n "indent") (r "^0.1.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.5") (d #t) (k 2)))) (h "0d5inqwm7sia9k0zzjg0r9s3bkz7yyljf090wv38wdw22p5fmgcd")))

