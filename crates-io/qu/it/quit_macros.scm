(define-module (crates-io qu it quit_macros) #:use-module (crates-io))

(define-public crate-quit_macros-0.1.0 (c (n "quit_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15rivqi8q8h4rpnw10v81s5j32bqk7f6zcl3qdnkr04sav4yhcq0")))

(define-public crate-quit_macros-0.2.0 (c (n "quit_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zyfmcsb2lgjimzv0lvhvnl9r3j9nicdqffrip2n4a9cppl2km3r")))

(define-public crate-quit_macros-0.2.1 (c (n "quit_macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18gvz82asx10jqmyzrkgqlavirf6lv35rjkl1si43dp7jlfx4zb9")))

(define-public crate-quit_macros-1.0.0 (c (n "quit_macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dqnk84krc3q5wxvp8qiys1qbiagyxawh3m04vwg2lzzkw6pn10w")))

(define-public crate-quit_macros-1.1.0 (c (n "quit_macros") (v "1.1.0") (h "0alhvaq4f84fn0ys1j61pn88d1aaq9zx4paiqlqpm6mmirn91qyw")))

(define-public crate-quit_macros-1.1.1 (c (n "quit_macros") (v "1.1.1") (h "1zyqd56fg7ydwy9fyx1a1w37wpp2hrfb11wk4av8r0834awdhqk7")))

(define-public crate-quit_macros-1.1.2 (c (n "quit_macros") (v "1.1.2") (h "0khvdv0h48hd43bflsvqf21z438vzyssgx5bmah9gbwg2gj26sgz")))

(define-public crate-quit_macros-1.1.3 (c (n "quit_macros") (v "1.1.3") (h "1g3zbjjxlfqrjfxz2jxgmq5wb03lsk28rqarz7yp15hnny24vsr6")))

(define-public crate-quit_macros-1.1.4 (c (n "quit_macros") (v "1.1.4") (h "1bhvg3z60r4msg0x1d2sai7578sypacrij3906f17qyw8xmn24d4")))

(define-public crate-quit_macros-1.2.0 (c (n "quit_macros") (v "1.2.0") (h "0hj2c9hbymjkrj93ash4zhsa63f7mx0hjpg8zvp4iqvz5krz877g") (r "1.60.0")))

(define-public crate-quit_macros-2.0.0 (c (n "quit_macros") (v "2.0.0") (h "0r3myfghk6rwgbgk42yyx87c7j8f6vxm4yfiy9xas22xvnh2fjqx") (r "1.64.0")))

