(define-module (crates-io qu it quitters) #:use-module (crates-io))

(define-public crate-quitters-0.1.0 (c (n "quitters") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (f (quote ("std" "perf"))) (k 0)) (d (n "semver") (r "^1.0.14") (d #t) (k 0)))) (h "055hdmm9b78jpdirsbfl7bi2zi1zjwiskjxma2r1a8adv3kwv378")))

