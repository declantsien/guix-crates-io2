(define-module (crates-io qu it quit) #:use-module (crates-io))

(define-public crate-quit-0.1.0 (c (n "quit") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quit_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1w0pws2h7xdlaqs20m69q2j92qp5qaqvs4gvhwc7cb95y9wc32n2")))

(define-public crate-quit-0.2.0 (c (n "quit") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quit_macros") (r "^0.2.0") (d #t) (k 0)))) (h "16rsl1nsw14zi24m6spzbcymp03fcl881qq7dcqkdagi47kx7607")))

(define-public crate-quit-0.2.1 (c (n "quit") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quit_macros") (r "= 0.2.1") (d #t) (k 0)))) (h "09r7gf890lrg0a7zqwd1l11cw2x85jl7azd34y01nphq8qygqh7d")))

(define-public crate-quit-1.0.0 (c (n "quit") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quit_macros") (r "= 1.0.0") (d #t) (k 0)))) (h "0j8fxsisnz6igjsib5zqrym0qg462vqba0fy4n9kqaq74yqv2n3j")))

(define-public crate-quit-1.1.0 (c (n "quit") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quit_macros") (r "=1.1.0") (d #t) (k 0)))) (h "1b5jg3hnzdbwji7cncp09hhbkv6lamfrgc90ryi1ym3cc0aan0yb")))

(define-public crate-quit-1.1.1 (c (n "quit") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quit_macros") (r "=1.1.1") (d #t) (k 0)))) (h "0j401dzrzmn9h292xb11gzwmmh8xvs4blphl11qf2gy78zxv3n65")))

(define-public crate-quit-1.1.2 (c (n "quit") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quit_macros") (r "=1.1.2") (d #t) (k 0)))) (h "0mh7hhhps8pn6s6lv0sya3vk76rb4kvblq5f6nxm5715lhvbpx4l")))

(define-public crate-quit-1.1.3 (c (n "quit") (v "1.1.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quit_macros") (r "=1.1.3") (d #t) (k 0)))) (h "1fvx1qwb4ijsvfprj65ha416jviq086bdks0j8pnij2qalimdzfb")))

(define-public crate-quit-1.1.4 (c (n "quit") (v "1.1.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quit_macros") (r "=1.1.4") (d #t) (k 0)))) (h "17g4d23yhvp3y2fdn4j0wzn0lvzda0qpxygsgxc5h2znhvy19lxs")))

(define-public crate-quit-1.2.0 (c (n "quit") (v "1.2.0") (d (list (d (n "quit_macros") (r "=1.2.0") (d #t) (k 0)))) (h "0482h1jg745g29nmny7mys2xnglk0qm7792s1i8hz4fbfvvlq265") (r "1.60.0")))

(define-public crate-quit-2.0.0 (c (n "quit") (v "2.0.0") (d (list (d (n "quit_macros") (r "=2.0.0") (d #t) (k 0)))) (h "16v6imwrw0niqf925zrxcc7a2q3ca0scy677j1mvv09x4c4cd460") (f (quote (("__unstable_tests")))) (r "1.64.0")))

