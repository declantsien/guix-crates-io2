(define-module (crates-io qu il quilt_graph) #:use-module (crates-io))

(define-public crate-quilt_graph-0.1.1 (c (n "quilt_graph") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)))) (h "1q4nzif6vjrjjn33nwny4i2wfkv9rlfhb0ih0rgvhaabhm9lm659")))

