(define-module (crates-io qu il quilt-lang) #:use-module (crates-io))

(define-public crate-quilt-lang-0.1.0 (c (n "quilt-lang") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1sf7l0mapx9nq9p0r84v0q6vlk57379mgha5xrdlw37fh4xvbh8j")))

(define-public crate-quilt-lang-0.1.1 (c (n "quilt-lang") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1i4syqn0hihxcrv1ncrqhn3l457hmalk1783ldawcmq5d0zhhysx")))

