(define-module (crates-io qu il quill-prototype) #:use-module (crates-io))

(define-public crate-quill-prototype-0.0.0 (c (n "quill-prototype") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0a2vax6xvi8138dgsvavzssv4bfnfx10ig8crccphr9mbsvsw437")))

(define-public crate-quill-prototype-0.0.0-prototype.1 (c (n "quill-prototype") (v "0.0.0-prototype.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "08py2n9p07fydjhrbmrwg85qjla2ykq141bfams7mh39iyw37akv")))

