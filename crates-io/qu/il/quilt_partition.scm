(define-module (crates-io qu il quilt_partition) #:use-module (crates-io))

(define-public crate-quilt_partition-0.1.1 (c (n "quilt_partition") (v "0.1.1") (d (list (d (n "quilt_multimap") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0p23f55g9rxmlbdq1p3wi3rnw4308xn6yy54z51iv162gsarba0i")))

