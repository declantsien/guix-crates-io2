(define-module (crates-io qu il quilt_multimap) #:use-module (crates-io))

(define-public crate-quilt_multimap-0.1.1 (c (n "quilt_multimap") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 2)))) (h "18q50zfpqlhi4glf4hp74ins4niwkafpmfy163wfms8ixgmzcjji")))

