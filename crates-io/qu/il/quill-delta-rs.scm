(define-module (crates-io qu il quill-delta-rs) #:use-module (crates-io))

(define-public crate-quill-delta-rs-1.0.0 (c (n "quill-delta-rs") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cb60z7j0c24zmyhdkz1f2h9ligk4c54y2l2k4hjk1bp232fb803")))

(define-public crate-quill-delta-rs-1.1.0 (c (n "quill-delta-rs") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a58wlrc0dvh5mg7330vyrzmklvy52h175bhvyhsx77x5w2viay9")))

(define-public crate-quill-delta-rs-1.1.1 (c (n "quill-delta-rs") (v "1.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d1ry7lw19f1gdp1gdjxz6bk6jpxz7x03irdrmc337lkj9sb5sx8")))

