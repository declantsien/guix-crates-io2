(define-module (crates-io qu il quilt_wasm) #:use-module (crates-io))

(define-public crate-quilt_wasm-0.1.1 (c (n "quilt_wasm") (v "0.1.1") (d (list (d (n "console_log") (r "^0.1") (d #t) (k 0)) (d (n "libquilt") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quilt_graph") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1xpr9f0i5v526l0qsp271fd44c3l8migfwjyk8mj7f864af66wpk")))

