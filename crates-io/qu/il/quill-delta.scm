(define-module (crates-io qu il quill-delta) #:use-module (crates-io))

(define-public crate-quill-delta-0.0.0 (c (n "quill-delta") (v "0.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l2phxscvl05icvv5q20msnl0l17ml7d95psf92amdj5q4cvn2j5")))

(define-public crate-quill-delta-0.0.1 (c (n "quill-delta") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m15283b9mzjszpw1k1v8rhasv1fdgf3p78i10f6p8pl48b93czq")))

