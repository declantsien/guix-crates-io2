(define-module (crates-io qu il quilkin-macros) #:use-module (crates-io))

(define-public crate-quilkin-macros-0.1.0 (c (n "quilkin-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1nk2nzw16nazpy420c8w5y9phky290wlmgxcwc4vp5s8907yrhxn")))

(define-public crate-quilkin-macros-0.2.0-dev (c (n "quilkin-macros") (v "0.2.0-dev") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1fv2mzg3crsi369g2pqgqvlj3h9158y7ccnnh53i97z6jm7m66zv") (y #t)))

(define-public crate-quilkin-macros-0.2.0 (c (n "quilkin-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1z54r850v7mp1aymkc1krpq4m94yzbjl2xsp4kkmcn7x37vhas3v")))

(define-public crate-quilkin-macros-0.3.0 (c (n "quilkin-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "080jpm20v43vq1llcvhgnjl6inwg1g2491g8sw027fa562wig5dj")))

(define-public crate-quilkin-macros-0.4.0 (c (n "quilkin-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "0kw8mvprh96jfvyajml1sxq151dwizbxgqhdfa8930v2frf5z925")))

(define-public crate-quilkin-macros-0.5.0 (c (n "quilkin-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1va7apazcdi5axzq80xgnp19b380dai0gkyhwcsq3lb48960hn28")))

(define-public crate-quilkin-macros-0.6.0 (c (n "quilkin-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1fizqqp9qjn9d6hbdr3sa8jpd941h6r7rls4ky0q3bd5i968r8fw")))

(define-public crate-quilkin-macros-0.7.0 (c (n "quilkin-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1dqgzw1kz71akh9hpqncmn6w2jh6rididr40n1c04gk88n00pcgv")))

(define-public crate-quilkin-macros-0.8.0 (c (n "quilkin-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "14v8ycsab72y5v2xj4kqvh2bnhrp787rqlwy76f15f7f2bialahb")))

