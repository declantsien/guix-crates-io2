(define-module (crates-io qu il quil) #:use-module (crates-io))

(define-public crate-quil-0.1.0 (c (n "quil") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)))) (h "0621rk1lm5mn0z5vk4rk9ysy9fg99h88pj22czxlzgyfjkjd854a")))

(define-public crate-quil-0.2.0 (c (n "quil") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)))) (h "0y3bl1l8bhmgifamnbmiqph60ga3g2pmw9pkf2p77xxbz4arjs1m")))

(define-public crate-quil-0.3.0 (c (n "quil") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)))) (h "0njxzxh9ipmsyspx3y9d4yyhapyzmxdfdx463fhmdxpydp53mxr8")))

(define-public crate-quil-0.3.1 (c (n "quil") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)))) (h "0hjv7mpnxz31yj8qgl13z9ri79vh9z5ng5nhmz89pjm2yj64zv4f")))

(define-public crate-quil-0.3.2 (c (n "quil") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)))) (h "01mb7nwn1nfv7l8gm1g3sf2xi3v1nb1vxx3f53bay1kfg9gm6l9i")))

(define-public crate-quil-0.4.0 (c (n "quil") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)))) (h "10nxzgspr61dajda1sgxi16ygk59hcp32ns5wcii5k3rzabbxjl8")))

(define-public crate-quil-0.4.1 (c (n "quil") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)))) (h "0fbw33yd9shwdaphp3xmprdrh59w975j40dh9gcvpbm168xsip1d")))

