(define-module (crates-io qu il quilt) #:use-module (crates-io))

(define-public crate-quilt-0.1.0 (c (n "quilt") (v "0.1.0") (d (list (d (n "askama_escape") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.10") (d #t) (k 0)) (d (n "libquilt") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quilt_diff") (r "^0.1.0") (d #t) (k 0)) (d (n "quilt_graph") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1vjy3nhyqz8jvi6ixfxd9a498l0ny8vg2jkndf7sck1pkwinqips")))

