(define-module (crates-io qu do qudoku) #:use-module (crates-io))

(define-public crate-qudoku-0.1.0 (c (n "qudoku") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("getrandom"))) (k 2)) (d (n "secp") (r "^0.2.0") (f (quote ("k256" "num-traits"))) (k 0)) (d (n "sha2") (r "^0.10.8") (k 0)))) (h "0indgjlz71bhqn5n7d5psg3dnvxhf4wc15rhw6bpmpwlzfp6dj72") (f (quote (("default")))) (s 2) (e (quote (("rand" "dep:rand" "secp/rand"))))))

(define-public crate-qudoku-0.1.1 (c (n "qudoku") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("getrandom"))) (k 2)) (d (n "secp") (r "^0.2.0") (f (quote ("k256" "num-traits"))) (k 0)) (d (n "sha2") (r "^0.10.8") (k 0)))) (h "0g8h218pnp46ahx6vv1838x16ha8gi08cf070xsjrxcvf7k8bvr7") (f (quote (("default")))) (s 2) (e (quote (("rand" "dep:rand" "secp/rand"))))))

