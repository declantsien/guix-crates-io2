(define-module (crates-io qu ol quoll) #:use-module (crates-io))

(define-public crate-quoll-0.1.1 (c (n "quoll") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "home") (r "^0.3.4") (d #t) (k 0)) (d (n "libappindicator") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)))) (h "1msnz4898ss1yk96axj7g3gkgbspnrvrlbznwz038ki6zcf320g4")))

(define-public crate-quoll-0.1.2 (c (n "quoll") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "gtk") (r "^0.8.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "libappindicator") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "1r6lnn2nf9swc137gzw9kldfb4bywfv3yxwkzkfxh7v516fr0xvc")))

(define-public crate-quoll-0.2.0 (c (n "quoll") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "gtk") (r "^0.14.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "libayatana-appindicator") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "1l3w9bh3dcga595w5j2av8rmqc0hmz4x8f1qd9c9brprpgrfg9cq")))

