(define-module (crates-io qu iv quiverquant) #:use-module (crates-io))

(define-public crate-quiverquant-0.1.0 (c (n "quiverquant") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0pkyda0y6y78d925vmbmy7bp1k747xc9jkhj9gk3bbka4pbp4y6k")))

(define-public crate-quiverquant-0.2.0 (c (n "quiverquant") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "162hdlh5hdr4b5048xlhkzfkv62r8prvn9rwyz7d7nm66p33v2bw")))

(define-public crate-quiverquant-0.2.1 (c (n "quiverquant") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0yv1a9rj9vgdd3xdghy9kgv0wnrxj6y1znwir5vb5fbisws66rx8")))

(define-public crate-quiverquant-0.2.2 (c (n "quiverquant") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hmrvj9qxiqb1pbj4avvszii1k8xqvxkbf4am52q55nd6kv3iq5p")))

