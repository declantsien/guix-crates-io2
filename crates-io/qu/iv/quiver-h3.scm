(define-module (crates-io qu iv quiver-h3) #:use-module (crates-io))

(define-public crate-quiver-h3-0.1.0 (c (n "quiver-h3") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "quiche-tokio") (r "^0.1") (d #t) (k 0)) (d (n "quiver-qpack") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (d #t) (k 0)) (d (n "tokio-bitstream-io") (r "^0.0.7") (d #t) (k 0)))) (h "1apimraxgm644r0721rlmb8g9way8jfb9bd3scmb380h75pgnhl9")))

