(define-module (crates-io qu ar quark_ecs) #:use-module (crates-io))

(define-public crate-quark_ecs-0.1.0 (c (n "quark_ecs") (v "0.1.0") (d (list (d (n "hecs") (r "^0.6.3") (d #t) (k 0)))) (h "067j3jqi3ya8hw00yg3bxx359la3vvsl72bbf989cbcbdj4z97rd")))

(define-public crate-quark_ecs-0.1.1 (c (n "quark_ecs") (v "0.1.1") (d (list (d (n "hecs") (r "^0.6.3") (d #t) (k 0)))) (h "06jswiaajsf0z624x6fsp117d22bh0jp1ggpsk3fnwm5pb6r39fv")))

