(define-module (crates-io qu ar quartz_nbt_macros) #:use-module (crates-io))

(define-public crate-quartz_nbt_macros-0.1.0 (c (n "quartz_nbt_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k1jjgjgl58zh224h8bcmql3mkpgzkvy94qk45vnzd5i3m7mly62")))

(define-public crate-quartz_nbt_macros-0.1.1 (c (n "quartz_nbt_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vfsx0bggm6s7qmif61higs9l3lyqbwafa755l6q87sdi86am6r8")))

