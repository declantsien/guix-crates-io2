(define-module (crates-io qu ar quartz_commands_macros) #:use-module (crates-io))

(define-public crate-quartz_commands_macros-0.1.0 (c (n "quartz_commands_macros") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fkk8yh6hdrjwvfcb2rnvkmabnvvkyp7ljsalpf5bsf175dp60xk")))

