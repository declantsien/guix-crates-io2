(define-module (crates-io qu ar quarkrs) #:use-module (crates-io))

(define-public crate-quarkrs-0.1.0 (c (n "quarkrs") (v "0.1.0") (h "1j2jqkg1y5nbwdmsdyj71r02i7nci5kcg27c5c9kd6f0162c6pgx")))

(define-public crate-quarkrs-0.1.1 (c (n "quarkrs") (v "0.1.1") (h "1469gd7as6zqd2z9sviapiz9n63vlxf44pqbh6r5x0y4zyx0f3gw")))

