(define-module (crates-io qu ar quark) #:use-module (crates-io))

(define-public crate-quark-0.0.0 (c (n "quark") (v "0.0.0") (h "1nq8g9a43m8l0lbz11h833x5nhvy980lbi8n9bbvhswpgjahiv58") (y #t)))

(define-public crate-quark-1.0.0 (c (n "quark") (v "1.0.0") (d (list (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "10jjm10hj84xn97ly9ifalhq6zackg8wd9lblbpbzfv789kvvdcj")))

(define-public crate-quark-1.1.0 (c (n "quark") (v "1.1.0") (d (list (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1xr6afzc3023djjw0qgsmh87s9dll6qm9rgyc86p0qvckkxw7xxy")))

