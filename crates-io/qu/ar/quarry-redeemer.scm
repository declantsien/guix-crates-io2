(define-module (crates-io qu ar quarry-redeemer) #:use-module (crates-io))

(define-public crate-quarry-redeemer-1.9.0 (c (n "quarry-redeemer") (v "1.9.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "vipers") (r "^1.2.1") (d #t) (k 0)))) (h "0gl2zcy7xscnm63a81315q7jdb69zjb6fx4pwvfkhfiyxcw526z7") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.10.0 (c (n "quarry-redeemer") (v "1.10.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "vipers") (r "^1.3.0") (d #t) (k 0)))) (h "1sdajq9j41q0gxicwgzf3ig58y3p50b7n9x85506gfhrd6nm1xzx") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.10.5 (c (n "quarry-redeemer") (v "1.10.5") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.3") (d #t) (k 0)))) (h "09xk0s4amraac9q0ni7gmps1n72dy4d993fkskv2rr7rdmic3fvb") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.10.6 (c (n "quarry-redeemer") (v "1.10.6") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.3") (d #t) (k 0)))) (h "0jz2aix7m2df6lm80iyjz9x0w7fb0nwqnwq641xg2bbw7yjkadjl") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.11.0 (c (n "quarry-redeemer") (v "1.11.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1wndxy2k01g9i1qdw9j47y0l40hzcbmczm7sijsayzgpyp2qdhwf") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.11.1 (c (n "quarry-redeemer") (v "1.11.1") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "18halaycjgqphavgzjsyhf4lq6q9v1fn1vjqkyvymj7q8nqnz6qw") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.11.3 (c (n "quarry-redeemer") (v "1.11.3") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "0mp4jwfm8yhr5mwhjl7ccgy9nr0xr07anlw68jgc61dq6rpm87cn") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.11.6 (c (n "quarry-redeemer") (v "1.11.6") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1973p3dj82bb8bs738w6knky897yngjzvc0czagxq6dgf0d6nrh1") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.11.8 (c (n "quarry-redeemer") (v "1.11.8") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "0pk10a1yvr6ck4lwxhyq8n2ixp1savjgg6q6hp3c6v19a56n90dn") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.11.9 (c (n "quarry-redeemer") (v "1.11.9") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "14x66p7lb6axdlj6rnj3mhv0c3028dvn83j8b9cmx91i2vkd7ihx") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.11.10 (c (n "quarry-redeemer") (v "1.11.10") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.7") (d #t) (k 0)))) (h "0r69ddpmb9mfqdcraxclinl4fimdpfnr84x0kap5cn081vr1i09w") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-1.11.11 (c (n "quarry-redeemer") (v "1.11.11") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.7") (d #t) (k 0)))) (h "0knkq3969w4xh6c8l47ah2fi3xchqdvr9acnp5bsqckj22askmbp") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-2.0.1 (c (n "quarry-redeemer") (v "2.0.1") (d (list (d (n "anchor-lang") (r ">=0.21") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.21") (d #t) (k 0)) (d (n "vipers") (r "^1.6") (d #t) (k 0)))) (h "15402afj1sl9xx18vr4z567dy7gs6lf4rx9rc84w5six75mr8gkm") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-3.0.0 (c (n "quarry-redeemer") (v "3.0.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1rx0w9g347nyzlngg4dv7379sk826zpb5aywh8aqbr41mxk7fyny") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-4.0.0 (c (n "quarry-redeemer") (v "4.0.0") (d (list (d (n "anchor-lang") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0jdvk5wl6hivl3hmsp1ph3srkjx5wdgh1asqd1ba7nk629nykm0g") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-4.1.0 (c (n "quarry-redeemer") (v "4.1.0") (d (list (d (n "anchor-lang") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0vcjf582pj9gimgwizqb8a63f1l42cpx9pzmxay7g8na1z81pjia") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-4.2.0 (c (n "quarry-redeemer") (v "4.2.0") (d (list (d (n "anchor-lang") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0mbxf45d66q217vvqk90vbzqfwn5a8f4b4km1595rv6m63izal0f") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-4.2.1 (c (n "quarry-redeemer") (v "4.2.1") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0igg2a6szwyd1h4hm2ygmyl7fbr40f7jlzpaj2gj4m6sqm791c1w") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-5.0.0 (c (n "quarry-redeemer") (v "5.0.0") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1wgahwprxpzb1lsvxf7ifx39nd4dn7kcgsd2dp9hz25zf2f6yazz") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-5.0.1 (c (n "quarry-redeemer") (v "5.0.1") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1ynflzadvkarz3wljlzrgv68xjrwd4x88k48nsg7v7aw767h8dxy") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-5.0.2 (c (n "quarry-redeemer") (v "5.0.2") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0a4l3jgl79ib8njvs2kfm4y400j33gfmpa4hw2x3fi8f7r3c2l52") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-5.1.0 (c (n "quarry-redeemer") (v "5.1.0") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0v95wllj8z4k1d9x6g7bqnrm41gr1aajs2xywl8443r0mm632hw3") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-redeemer-5.2.0 (c (n "quarry-redeemer") (v "5.2.0") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.25") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.25") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "16h5ymkblwb4cz1whqbpk3qkw8wb46h0w0z2bx8w021sbjsid6ck") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

