(define-module (crates-io qu ar quartz_commands) #:use-module (crates-io))

(define-public crate-quartz_commands-0.1.0 (c (n "quartz_commands") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quartz_commands_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "01wafcj332j5gigmc3lq7bcxcvghq85qvq7jzz851hfkvgn07r7w")))

