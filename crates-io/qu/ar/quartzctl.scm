(define-module (crates-io qu ar quartzctl) #:use-module (crates-io))

(define-public crate-quartzctl-0.1.0 (c (n "quartzctl") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "libquartz") (r "^0") (d #t) (k 0)))) (h "0j2zgb7iga482gicp3yq3az344yhnkg007p34b3g55jvpdixp2ms")))

(define-public crate-quartzctl-0.2.0 (c (n "quartzctl") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "libquartz") (r "^0") (d #t) (k 0)))) (h "1wc9hmi1nj5yfqvzv5w044fk95bj1vvxxl28linjyspj31n5wnnw")))

(define-public crate-quartzctl-0.3.0 (c (n "quartzctl") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0) (p "painted")) (d (n "libquartz") (r "^0") (d #t) (k 0)))) (h "1l64wrf6qc2vlyab17bv3m24my4rc8v12c1sd1izg01zz8fwdhgj")))

