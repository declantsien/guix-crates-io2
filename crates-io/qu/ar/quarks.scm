(define-module (crates-io qu ar quarks) #:use-module (crates-io))

(define-public crate-quarks-0.1.0 (c (n "quarks") (v "0.1.0") (d (list (d (n "quark_app") (r "^0.1.0") (d #t) (k 0)) (d (n "quark_ecs") (r "^0.1.0") (d #t) (k 0)))) (h "1rr1md9g8pc7x0jnikmgfj4fc2zjwpcfj4qbg192fpdk8bvjz8si")))

(define-public crate-quarks-0.1.1 (c (n "quarks") (v "0.1.1") (d (list (d (n "quark_app") (r "^0.1.0") (d #t) (k 0)) (d (n "quark_ecs") (r "^0.1.0") (d #t) (k 0)))) (h "0vza1f8ihixk67lnc3hpq2w4989q4hjc5vw2hbrmjn917vsr9lb9")))

