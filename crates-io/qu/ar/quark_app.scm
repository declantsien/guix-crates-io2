(define-module (crates-io qu ar quark_app) #:use-module (crates-io))

(define-public crate-quark_app-0.1.0 (c (n "quark_app") (v "0.1.0") (h "107s7wbvj5qv8a5idn16qgw2pfvrh7iharl1mg8w8dswa3r6nwd8")))

(define-public crate-quark_app-0.1.1 (c (n "quark_app") (v "0.1.1") (h "0w275ah7ykcs2fc3p56zq3lgwky557sv70zcy4yb7cgj987pwn0p")))

