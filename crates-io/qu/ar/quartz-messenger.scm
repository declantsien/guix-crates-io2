(define-module (crates-io qu ar quartz-messenger) #:use-module (crates-io))

(define-public crate-quartz-messenger-0.1.0 (c (n "quartz-messenger") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libquartz") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "07d0syq81084p15l082747g1mz18yl1afnrl5z8jgghqqymvsn53")))

(define-public crate-quartz-messenger-0.1.1 (c (n "quartz-messenger") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libquartz") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1m0b2dlils7pvygkqpwcbl2fgsnxam83nmpkhn2m8148k2l1iz7v")))

(define-public crate-quartz-messenger-0.2.0 (c (n "quartz-messenger") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "libquartz") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1ahvh9y8yfsakalcgpsy84avlgy2y2qgfvmcxcnaqpniwm63dn90")))

(define-public crate-quartz-messenger-0.2.1 (c (n "quartz-messenger") (v "0.2.1") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "libquartz") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1nh3y4n763a60xr0r9hyyv1qfw02aabqr6z71krb3m17hs251189")))

(define-public crate-quartz-messenger-0.3.0 (c (n "quartz-messenger") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "colored") (r "^1.0") (d #t) (k 0) (p "painted")) (d (n "libquartz") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1s610j32nl3wq0gkjhcywpdvqvi8a16kdmxmjkmgad4fa6d8annc")))

