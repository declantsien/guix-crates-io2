(define-module (crates-io qu ar quartet) #:use-module (crates-io))

(define-public crate-quartet-0.1.0 (c (n "quartet") (v "0.1.0") (h "0lvzirbc5i5wfh7j36iy2iq8dldnr3zm3lca4gfxnpmrcz3hpvgh")))

(define-public crate-quartet-0.2.0 (c (n "quartet") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "0b9w1hkbzhilg7kfb6vkncrd1b5x9xzflfyxm3nzrdkzkxqf65jw")))

(define-public crate-quartet-0.2.1 (c (n "quartet") (v "0.2.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "0vjdijsrg1sfg5c8dvxn5nd8da4qwsjxpa29f2ry7sh6qrxq7xxz")))

