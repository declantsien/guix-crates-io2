(define-module (crates-io qu ar quartz-server) #:use-module (crates-io))

(define-public crate-quartz-server-0.1.1 (c (n "quartz-server") (v "0.1.1") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json" "tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "16i69f11xnfi922j0c64b9jwfhzxiij8lhygqp6swdbc72z8c2yz")))

(define-public crate-quartz-server-0.2.0 (c (n "quartz-server") (v "0.2.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json" "tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1x1i3xw35sgb61bvyw9bmga08pjb9ww8fygqhvwlas16ss39l1s2")))

(define-public crate-quartz-server-0.3.0 (c (n "quartz-server") (v "0.3.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json" "tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ccfb9z54qmlbp1v4wv34ws4h58k8534q5ch1rm63vbc3rkq1hzq")))

