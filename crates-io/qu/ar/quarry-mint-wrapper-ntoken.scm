(define-module (crates-io qu ar quarry-mint-wrapper-ntoken) #:use-module (crates-io))

(define-public crate-quarry-mint-wrapper-ntoken-5.0.2 (c (n "quarry-mint-wrapper-ntoken") (v "5.0.2") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1vfaj41qkspbrj1p0gnvc4cc4yx3lp390i3v2r8vk1gi0g50va1s") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-quarry-mint-wrapper-ntoken-5.0.3 (c (n "quarry-mint-wrapper-ntoken") (v "5.0.3") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1rcnial7zw4fpqfqrzikhxhmfcdv2cl41b94njgvkgsjclhj03ad") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

