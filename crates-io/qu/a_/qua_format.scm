(define-module (crates-io qu a_ qua_format) #:use-module (crates-io))

(define-public crate-qua_format-0.1.0 (c (n "qua_format") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0vh151n4z4cg9lymg9nw9kfpizxj1lnilj30q17wkkn45lbvjdad")))

(define-public crate-qua_format-0.1.1 (c (n "qua_format") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0xbcw8k9z1s6qbj10rp0zv970id7qx1dr3833diwfcv46ksmc496")))

(define-public crate-qua_format-0.1.2 (c (n "qua_format") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0wn804f579891id7mhmpnrrqgn8rjxwmbcff4wam9f1ivjays85f")))

(define-public crate-qua_format-0.1.3 (c (n "qua_format") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0bny52v611fblf00q4hps9skvlspnfwbvry03msc5y2wb94lpykl")))

(define-public crate-qua_format-0.1.4 (c (n "qua_format") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1klaskf1p7zhyv0g09dcd0dvwh5rv2hxfhdzy87cpjnq00qkq3pl")))

(define-public crate-qua_format-0.1.5 (c (n "qua_format") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0c6hf8dqgbfi2kf3g7hmabdig4190g0rapjila423qrv1xhd3avh")))

(define-public crate-qua_format-0.1.6 (c (n "qua_format") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0n7816r9ayy9p29h8psk0j2nr8y0zxpz776i1gkp5qa3y8zwpyns")))

(define-public crate-qua_format-0.1.7 (c (n "qua_format") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0fd1g8gp09f5w840g7lvlx91iqg4kbn55zjfyshrjgmy47dzznnq")))

