(define-module (crates-io qu eu queue-ext) #:use-module (crates-io))

(define-public crate-queue-ext-0.1.0 (c (n "queue-ext") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1pi0j01rzjl2wx7g3ixjfqb3hbgvdb3ph2x155i2r108yigh56kq") (r "1.38")))

(define-public crate-queue-ext-0.2.0 (c (n "queue-ext") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1f0bxnsnzlp8nv7bapqyrqqb3j02rdp3idbpwvbqcdfnrapam7gd") (y #t) (r "1.38")))

(define-public crate-queue-ext-0.2.1 (c (n "queue-ext") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1b9xqhppl3dxxcwjwvgzbfqxblgv9khn9l3wp5vc4azq4cj72bl2") (r "1.38")))

(define-public crate-queue-ext-0.3.0 (c (n "queue-ext") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0pwms2rbd5d548l7qxffcwczsf7wj54a22ksjn0g1ps06sprsm4s") (r "1.38")))

(define-public crate-queue-ext-0.4.0 (c (n "queue-ext") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "04czgzxvqzqvaznczrsy70p27pwvrkbg40jkjivqgk44wqdpqay2") (r "1.47")))

(define-public crate-queue-ext-0.4.1 (c (n "queue-ext") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1j14arcdp4j9b6xf5zq97zwz0lz5n4qi372wi6rx210wd5rhn2qj") (r "1.47")))

