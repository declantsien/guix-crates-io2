(define-module (crates-io qu eu queued-client-rs) #:use-module (crates-io))

(define-public crate-queued-client-rs-0.1.0 (c (n "queued-client-rs") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (d #t) (k 0)) (d (n "serde_with") (r "^3.7.0") (d #t) (k 0)))) (h "0i4wnkfg60jzc4pdi4fa30rlgdycaqn6r0yfqsb27vv5ldsbpa0k")))

(define-public crate-queued-client-rs-0.1.1 (c (n "queued-client-rs") (v "0.1.1") (d (list (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (d #t) (k 0)) (d (n "serde_with") (r "^3.7.0") (d #t) (k 0)))) (h "0wsqnz58acdd2989yk657bxr89bxizdiypy8ywma77hd7gb8gbg1")))

