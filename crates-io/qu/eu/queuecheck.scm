(define-module (crates-io qu eu queuecheck) #:use-module (crates-io))

(define-public crate-queuecheck-0.1.0 (c (n "queuecheck") (v "0.1.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "0wl57c347kml408jnvmwc410l8aay2v9szyv6bwm7l2w62hsdip1")))

(define-public crate-queuecheck-0.1.1 (c (n "queuecheck") (v "0.1.1") (h "17d941j52sr2rbscd20xwnkajfydqfkscsnj66hn6b75kaihkpwa")))

