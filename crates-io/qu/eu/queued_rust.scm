(define-module (crates-io qu eu queued_rust) #:use-module (crates-io))

(define-public crate-queued_rust-0.1.1 (c (n "queued_rust") (v "0.1.1") (h "1zsg7ny0wl1j0y8fimlbqdsyfx93ndgfxwaqh1m26nlp54jkp8yq")))

(define-public crate-queued_rust-0.3.1 (c (n "queued_rust") (v "0.3.1") (h "0mkqkbsngngg8hdbvzi0h6dmjjh8qnykd39lryzsfsw6k157rs8n") (y #t)))

(define-public crate-queued_rust-0.3.5 (c (n "queued_rust") (v "0.3.5") (h "1wcq0c6cb0m895v75gs5qpifcg43w19xs8vj5jv444653nankdg4") (y #t)))

(define-public crate-queued_rust-0.4.0 (c (n "queued_rust") (v "0.4.0") (h "1hy84rl6v95sk8jafdc7ag3qi89ym5xhgr6196jpwjl0irh42h1a") (y #t)))

(define-public crate-queued_rust-0.4.1 (c (n "queued_rust") (v "0.4.1") (h "03iakjwbx9cc691zqhqxivizq3hsqnf0x1p2jibmh7ln30gr1w75") (y #t)))

(define-public crate-queued_rust-0.4.2 (c (n "queued_rust") (v "0.4.2") (h "1bzzrr033hb4ar47msvbkjcsna0b60vfz035h317pmsbxcsgid5b") (y #t)))

(define-public crate-queued_rust-0.4.3 (c (n "queued_rust") (v "0.4.3") (h "07x10kr1007jlm8aql9iibkjm7g0bi78gy9hiv3dmpnkfyk6vi36") (y #t)))

(define-public crate-queued_rust-0.4.4 (c (n "queued_rust") (v "0.4.4") (h "1rmwz1w9jf4w93v0zpmzf3qvbn2mcm1c60rzmpf13x57i24nl0l3") (y #t)))

(define-public crate-queued_rust-0.4.5 (c (n "queued_rust") (v "0.4.5") (h "1k898vnjdin161r7gv7ppv1f5w3icn982iph6jqhzq2fxvwk7cm6") (y #t)))

(define-public crate-queued_rust-0.4.6 (c (n "queued_rust") (v "0.4.6") (h "09zb2ynwlphk5qza4zm3irg4pp8jpfr240x0v97075r4d5bi21x3") (y #t)))

(define-public crate-queued_rust-0.4.7 (c (n "queued_rust") (v "0.4.7") (h "0qvldfd93iss95rwrgcc573br6cm0kxa1wp96dni07rkrcyb5m9v") (y #t)))

(define-public crate-queued_rust-0.4.8 (c (n "queued_rust") (v "0.4.8") (h "11bnngwnfm27klkrm6v28zzbx4gj7g2xj9p71p8kysifh3rxcjry") (y #t)))

(define-public crate-queued_rust-0.5.0 (c (n "queued_rust") (v "0.5.0") (d (list (d (n "snafu") (r "^0.8.2") (d #t) (k 0)))) (h "12llp4prnbvb9psfia0ysdrlwx56fmrnnq4mvf59ps1p43503m8r") (y #t)))

(define-public crate-queued_rust-0.5.3 (c (n "queued_rust") (v "0.5.3") (d (list (d (n "snafu") (r "^0.8.2") (d #t) (k 0)))) (h "18h1wvcpgh15firk4hxd33imxw1ls8ahzi1a7rgk3n6h2cf39a5i")))

(define-public crate-queued_rust-0.6.0 (c (n "queued_rust") (v "0.6.0") (d (list (d (n "snafu") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "16gbwpak5agwa6d26nk9bz12miv69z2a0a22p1hxchpzai0mmp20")))

(define-public crate-queued_rust-0.6.1 (c (n "queued_rust") (v "0.6.1") (d (list (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1n4gsqlkdfzc7dvig1f4x860fvlb4dwcwlmg6zkb8ckzpn1pmad8")))

(define-public crate-queued_rust-0.7.0 (c (n "queued_rust") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1bss5pdlgads26bf88vgv0ahy012jl9dbl98r6gxjanw7y9wnaw6")))

(define-public crate-queued_rust-0.7.1 (c (n "queued_rust") (v "0.7.1") (d (list (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0f27fypq42xyw967bgynkbs0h9wfd80axcamhncbmh6z2kii6yjk")))

(define-public crate-queued_rust-0.7.2 (c (n "queued_rust") (v "0.7.2") (d (list (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0f4y9y8mdi9njrbynigca9691nvrd2cswia476jbppv7dd3w4kps")))

(define-public crate-queued_rust-0.7.3 (c (n "queued_rust") (v "0.7.3") (d (list (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "05sjlv6i1gp305a25fmg4b18pnl7hl1zs7nr6927ghg388vy0466")))

(define-public crate-queued_rust-0.7.4 (c (n "queued_rust") (v "0.7.4") (d (list (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0q23rbh31ddj2i4z1cdj3lcr0acn655n16kzd41kzd4v26rjwmgw")))

