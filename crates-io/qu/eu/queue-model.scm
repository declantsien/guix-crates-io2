(define-module (crates-io qu eu queue-model) #:use-module (crates-io))

(define-public crate-queue-model-0.1.0 (c (n "queue-model") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "safe-modular-arithmetic") (r "^0.2.3") (d #t) (k 0)))) (h "1pzp6a93m80fn6lgkw26c1xqgb0v62553pnwngb7158k6b80r6gs") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-queue-model-0.1.1 (c (n "queue-model") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "safe-modular-arithmetic") (r "^0.2.4") (d #t) (k 0)))) (h "1cp3px20zk654z13dql6pd4fpz7ysy1kbld2qq5fnvd0b0zyd6x1") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-queue-model-0.1.2 (c (n "queue-model") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "safe-modular-arithmetic") (r "^0.2.4") (d #t) (k 0)))) (h "1fahy722ig5jw5ccm8nws0iads169nik0x5b70naw2x162a2n8zm") (f (quote (("default" "alloc") ("alloc"))))))

