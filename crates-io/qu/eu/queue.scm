(define-module (crates-io qu eu queue) #:use-module (crates-io))

(define-public crate-queue-0.1.0 (c (n "queue") (v "0.1.0") (h "04affigjrc0hfm3xpmdnwavw7wcgm947qpylp5kq749khfpyxl2i")))

(define-public crate-queue-0.1.1 (c (n "queue") (v "0.1.1") (h "0pjrjm9j9896vp0s1qgjc9w8hnhs4kq3v6gil3ph6djfvr7hfp0q")))

(define-public crate-queue-0.2.0 (c (n "queue") (v "0.2.0") (h "1ips6myppzijp30c2h66maq6qk0lhwcpxn7dqd2k71vfwsl281z7")))

(define-public crate-queue-0.3.0 (c (n "queue") (v "0.3.0") (h "0yw9m0jp6p9yb989gvvyvyp06jb30la0xh4qwb9na08b0907ndp8")))

(define-public crate-queue-0.3.1 (c (n "queue") (v "0.3.1") (h "0j6fha3bh8z450lilxsfbn54d5f9q70i65bp40bhhji9cbyygxvg")))

(define-public crate-queue-0.3.2-final (c (n "queue") (v "0.3.2-final") (h "1lb08mmcflp19pysd3klx1y4cqnlk7i6wcjb4npnrk9kfqbipx3z")))

