(define-module (crates-io qu eu queues) #:use-module (crates-io))

(define-public crate-queues-1.0.0 (c (n "queues") (v "1.0.0") (h "0l3017g40aic9x9vy0da38ff0b3hfzql9zm5zi4dbrk2484r2q7z")))

(define-public crate-queues-1.0.1 (c (n "queues") (v "1.0.1") (h "0xxjx9k7wwwjgwx638vb4if1xxavgpgs783zh98k4ambbzsl8wyd")))

(define-public crate-queues-1.0.2 (c (n "queues") (v "1.0.2") (h "060xrxvhjr11b5csnbfs25dp0ki8slrricvnn5cqrhxs1qxzrz4y")))

(define-public crate-queues-1.1.0 (c (n "queues") (v "1.1.0") (h "1w2ic7sw629crlbwh864izaabw0407icyfpyj22rkm4a9ypanx8l")))

