(define-module (crates-io qu eu queue-queue) #:use-module (crates-io))

(define-public crate-queue-queue-0.0.1 (c (n "queue-queue") (v "0.0.1") (h "15x2cm30g5m76bxd0bdidvbxhhbw136wxc3lrlw8dkx2bxrn8nwh")))

(define-public crate-queue-queue-0.1.0 (c (n "queue-queue") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0ysxc9h33skraq5kh74n8y7jfvp47n42m0529903qyqkhw21l7k1")))

(define-public crate-queue-queue-0.1.1 (c (n "queue-queue") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1hsqhc0x9yplm0hp4rbl7wd0js5zaz3wvvvz11l3i3lrd4a57q9c")))

(define-public crate-queue-queue-0.1.2 (c (n "queue-queue") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0vcrjpl753zf5q3gkxnz9rcnxkxzjj94mq8zl436p9jha25z3id6")))

(define-public crate-queue-queue-0.1.3 (c (n "queue-queue") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1az61fqi0s9jhzsyjjfp4a9fiampnn9smcfg97c58lpjz3c6k7f6")))

(define-public crate-queue-queue-0.1.4 (c (n "queue-queue") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1v2clnm8qxpcxwypv01xsnzcd14xan4bcgh72ri9m0kfz1jc79yl")))

(define-public crate-queue-queue-0.2.0 (c (n "queue-queue") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1an5c6vszm97fnzh3di94lv9c036i6favjc9jfsp7gjarv7bp600")))

