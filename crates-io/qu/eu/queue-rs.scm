(define-module (crates-io qu eu queue-rs) #:use-module (crates-io))

(define-public crate-queue-rs-0.0.10 (c (n "queue-rs") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "127c9jswzgcka7vczhmqcbxkcrv0jx21n9hw0fh5v1vlc95shqqm")))

(define-public crate-queue-rs-0.0.13 (c (n "queue-rs") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1jdvdjjg6294iw4znnj4vs5z6r3i3zagmazqyjblax799ijyrgyi")))

(define-public crate-queue-rs-0.0.14 (c (n "queue-rs") (v "0.0.14") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1bi2bbhzw33rmry7clp9z4g2x689pak9xx627gwvcfgm9hxb4qxz")))

(define-public crate-queue-rs-0.0.15 (c (n "queue-rs") (v "0.0.15") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "11xbcpf1lhfk216rk2k9v1c3nvpx043bz2j38xahpxvyi89g1j10")))

(define-public crate-queue-rs-0.0.16 (c (n "queue-rs") (v "0.0.16") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "17xqs13j8237d40lkkqyd4b461l8i875285p9vaz7nayjzgb1zp4")))

(define-public crate-queue-rs-0.0.17 (c (n "queue-rs") (v "0.0.17") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1ihjzrn9kh6y9msi4ggrk50c5j6sii61fvhd277j1ykmj2l50287")))

(define-public crate-queue-rs-0.0.5 (c (n "queue-rs") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0crv1am5kqszjlspwlmd2q8pnni6hi3g5iszlnm0m6mjk91pa5rr")))

(define-public crate-queue-rs-0.0.21 (c (n "queue-rs") (v "0.0.21") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1wahrs2c4ixbjqhinzwxr0dsb8kjrz7vvy5l8yqwyysibldjllic")))

(define-public crate-queue-rs-0.0.23 (c (n "queue-rs") (v "0.0.23") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1kr8arzdv1c8s7xrnm6ngjr0smc3jqvicv4xlj1wadq3mgfnsvcf")))

(define-public crate-queue-rs-0.0.24 (c (n "queue-rs") (v "0.0.24") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0qjilhd8ypxyj86lsig1gxajkkrk8sikn4hpd76hfn08s228c0fb")))

(define-public crate-queue-rs-0.0.25 (c (n "queue-rs") (v "0.0.25") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1ib1zl8fymkcl5krfxa456rjzpvgzpbfv87bc4856xibb3pmf8ni")))

(define-public crate-queue-rs-0.0.26 (c (n "queue-rs") (v "0.0.26") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "08gly1g3bzzdjqg4wlbwk4lqsxyniimwvpjhas0j39jvdpkhrq8l")))

(define-public crate-queue-rs-0.0.28 (c (n "queue-rs") (v "0.0.28") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0cw0q54659z2ssm3z5fv7qr53xk026xpsgzj2jrig18jp7zwd70z")))

(define-public crate-queue-rs-0.0.29 (c (n "queue-rs") (v "0.0.29") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "061b22nx1swp94710pagrij37qlik1vr06ava19nlymijlpi2q5z")))

(define-public crate-queue-rs-0.0.30 (c (n "queue-rs") (v "0.0.30") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fp_rust") (r "^0.3.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "19xjzj96qgdijkw82fhrbxw2r1ynz8nfq81z67dyza9diblyf1ng")))

