(define-module (crates-io qu eu queueue) #:use-module (crates-io))

(define-public crate-queueue-0.1.0 (c (n "queueue") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "0d3d8qkvk4m9g3qmxjwl083ikr0skv1ccmj2wa5ywrn91nj8an8q") (f (quote (("std") ("default"))))))

(define-public crate-queueue-0.1.1-beta.1 (c (n "queueue") (v "0.1.1-beta.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "1v9pjzh8zak6bbaq66sads7rmz9lcxlxmijmxkpw3a96896018ks") (f (quote (("std") ("default"))))))

(define-public crate-queueue-0.1.1-beta.2 (c (n "queueue") (v "0.1.1-beta.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "12g6m7d2qdm16ixrjn4h51sg6pf8my2zd24nfjwkk4in13jh5mzi") (f (quote (("std") ("default"))))))

(define-public crate-queueue-0.1.1-beta.3 (c (n "queueue") (v "0.1.1-beta.3") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "1s608h5r9zf15cvyzfqih2yc38kd9cflmk4a6225swq1nap4lwcb") (f (quote (("std") ("default"))))))

