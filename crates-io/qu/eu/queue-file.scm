(define-module (crates-io qu eu queue-file) #:use-module (crates-io))

(define-public crate-queue-file-1.0.0 (c (n "queue-file") (v "1.0.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "164s6s4h86gnag6b7cxa87ab7hjyzza5ai17d7rxdim92n3jykxz")))

(define-public crate-queue-file-1.0.1 (c (n "queue-file") (v "1.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "00sgfl3gbwj53szizh22pha66l2vqbmqx75w7s412x5l3vslxcbv")))

(define-public crate-queue-file-1.0.2 (c (n "queue-file") (v "1.0.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1cxzca20am1i88akz974s7hjkzm0qpn03ha767gg9si1w96iap1w")))

(define-public crate-queue-file-1.1.0 (c (n "queue-file") (v "1.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0lblr811xl45d6k35hzhw50ajhg027f47kqcj12bcjwfcw4w5bc2")))

(define-public crate-queue-file-1.2.0 (c (n "queue-file") (v "1.2.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "1s62hbrj3s0d248a7j9rkf884j3w4fffvxpi0ykbd301p9a0fb13")))

(define-public crate-queue-file-1.3.0 (c (n "queue-file") (v "1.3.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "1bfrg04ir81a387k2zkvkiqfwa3dgxga5jn7mb78f6gwjvqf2ahp")))

(define-public crate-queue-file-1.4.0 (c (n "queue-file") (v "1.4.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "1kyha4hc9rsxshi4frpvzk4zm9h9y09ra4wp95fs4xkjznq9a1r4")))

(define-public crate-queue-file-1.4.1 (c (n "queue-file") (v "1.4.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "0czfdfzv4pnqp1zfapab1y9ch4lam8zhys3v2avqyjf9az5iiz4f")))

(define-public crate-queue-file-1.4.2 (c (n "queue-file") (v "1.4.2") (d (list (d (n "auto-delete-path") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "0gxim2rmpsyb8f9flwxifk1wddrfakz3kk3qs8ig3z652fz3qnhw")))

(define-public crate-queue-file-1.4.3 (c (n "queue-file") (v "1.4.3") (d (list (d (n "auto-delete-path") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "13ybrj6wp4pg7wxj11aswjyw5rqn6gmxrclb7acbdc262bvjbdhj")))

(define-public crate-queue-file-1.4.4 (c (n "queue-file") (v "1.4.4") (d (list (d (n "auto-delete-path") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "1lc6v0k6wbx6bwcdn7ay51m6d7lg5zrh639d54nxacx290layw7j") (y #t)))

(define-public crate-queue-file-1.4.5 (c (n "queue-file") (v "1.4.5") (d (list (d (n "auto-delete-path") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "0k4jkjgg3j2cvv7bz6j5i1w339bk9hr4m5avfcv3r4npg4n96cw8")))

(define-public crate-queue-file-1.4.6 (c (n "queue-file") (v "1.4.6") (d (list (d (n "auto-delete-path") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "08r3mf3k3x52kb4i0g3qpvz7v4zf8fnqn83f0lknhnp55qz8vsdh")))

(define-public crate-queue-file-1.4.7 (c (n "queue-file") (v "1.4.7") (d (list (d (n "auto-delete-path") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "17m3p1v4qcv2sd2vvl7xnci0148d5y94ssw2qcwqwrgzsgqgc042")))

(define-public crate-queue-file-1.4.8 (c (n "queue-file") (v "1.4.8") (d (list (d (n "auto-delete-path") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "0wvpjdj8j8gy8wcn5sf64xij6qyfb77skl2sp9p5qqy21gs7mi8s")))

(define-public crate-queue-file-1.4.9 (c (n "queue-file") (v "1.4.9") (d (list (d (n "auto-delete-path") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "1vgc0qn4i9cqbazs2r0visxy2a1gahqx6kib9sf066b5wyy588c4") (r "1.58.1")))

(define-public crate-queue-file-1.4.10 (c (n "queue-file") (v "1.4.10") (d (list (d (n "auto-delete-path") (r "^0.2.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "104aascxag9m4ddsxfmzy6zvyv2316iiz9clk3fmvm03dv37vvb9") (r "1.58.1")))

