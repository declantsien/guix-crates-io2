(define-module (crates-io qu ad quad-url) #:use-module (crates-io))

(define-public crate-quad-url-0.1.0 (c (n "quad-url") (v "0.1.0") (d (list (d (n "egui") (r "^0.10.0") (d #t) (k 2)) (d (n "sapp-jsutils") (r "^0.1.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"windows\", target_os = \"macos\", target_os = \"android\"))") (k 0)))) (h "0mfpnwaxrl18xnbabfrfcai55d884pn3p1mjxqwc6g19srqz6zp3")))

(define-public crate-quad-url-0.1.1 (c (n "quad-url") (v "0.1.1") (d (list (d (n "egui") (r "^0.10.0") (d #t) (k 2)) (d (n "egui-macroquad") (r "^0.1.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.0-alpha.16") (d #t) (k 2)) (d (n "sapp-jsutils") (r "^0.1.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"windows\", target_os = \"macos\", target_os = \"android\"))") (k 0)))) (h "00szlb7zxcfw6caj1y43yk6pbka7ns4207j3r80mv7cf47kbfx76")))

