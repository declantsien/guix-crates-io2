(define-module (crates-io qu ad quad-svg) #:use-module (crates-io))

(define-public crate-quad-svg-0.1.0 (c (n "quad-svg") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (d #t) (k 0)))) (h "0d7hhnp975bgqpygq50ir6kppkda6zhrvm4l6s906ywaj1d6qj6h")))

(define-public crate-quad-svg-0.1.1 (c (n "quad-svg") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (d #t) (k 0)))) (h "080dv64z66jlwnmba3mrvmk9yahrh4cr4vr35mzvzwvz3hmrs62w")))

(define-public crate-quad-svg-0.1.2 (c (n "quad-svg") (v "0.1.2") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (d #t) (k 0)))) (h "0zvkw84cj7namxpszzd9c8hpyjr8qb9bgjsa7xwjwlrwnq20dwv1")))

