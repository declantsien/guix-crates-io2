(define-module (crates-io qu ad quad-gif) #:use-module (crates-io))

(define-public crate-quad-gif-0.2.0 (c (n "quad-gif") (v "0.2.0") (d (list (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "gif-dispose") (r "^3.1.1") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.20") (d #t) (k 0)) (d (n "rgb") (r "^0.8.33") (d #t) (k 0)))) (h "14kbalvyxk7nrsvgd6r64lsnwlgq8ym41yi0l78yiwjb0k2ilj7s")))

