(define-module (crates-io qu ad quadprogpp-sys) #:use-module (crates-io))

(define-public crate-quadprogpp-sys-0.1.0 (c (n "quadprogpp-sys") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "18vy0dq7w3plfzq9ixcfy8zsn4brlbax8f0cvd69kj5lj4xbsm5b") (f (quote (("trace-solver"))))))

(define-public crate-quadprogpp-sys-0.1.1 (c (n "quadprogpp-sys") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "01sj4ppxxckyblrmwyb269ksmrja534nsa9h95jkp1yx849wh9dv") (f (quote (("trace-solver"))))))

