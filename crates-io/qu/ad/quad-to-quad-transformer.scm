(define-module (crates-io qu ad quad-to-quad-transformer) #:use-module (crates-io))

(define-public crate-quad-to-quad-transformer-0.1.0 (c (n "quad-to-quad-transformer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.2") (d #t) (k 0)))) (h "12qjm17a41wbi6nxjzs652hdf49fbgivlhr2farmxbiiny1npxdp")))

(define-public crate-quad-to-quad-transformer-0.1.1 (c (n "quad-to-quad-transformer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.2") (d #t) (k 0)))) (h "09s5gbcdgpbmwa6b1l20a9r7vc15diclbbj67d2lwnlc2xgby8mn")))

