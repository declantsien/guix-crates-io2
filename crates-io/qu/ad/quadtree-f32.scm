(define-module (crates-io qu ad quadtree-f32) #:use-module (crates-io))

(define-public crate-quadtree-f32-0.1.0 (c (n "quadtree-f32") (v "0.1.0") (h "1hq4zbshw9vk46y93r9fi5d0jrmr6x4mxp75zx3m3yxvm1vlgbyw")))

(define-public crate-quadtree-f32-0.2.0 (c (n "quadtree-f32") (v "0.2.0") (h "0wn8jzpsqlq4rvggagxn4zb6191l9fpy0krbglj7vwdaykqsjrvp")))

(define-public crate-quadtree-f32-0.2.1 (c (n "quadtree-f32") (v "0.2.1") (h "0m82vfkhgyix2cksbrzn0v7kqncys3ai6jaqfa214z4qgjqrdqx6")))

(define-public crate-quadtree-f32-0.2.2 (c (n "quadtree-f32") (v "0.2.2") (h "1xh4rp767xbr7f5kcdzszdz1cs3gzfvxhy61qh5v64q93sqbylna")))

(define-public crate-quadtree-f32-0.3.0 (c (n "quadtree-f32") (v "0.3.0") (h "1pd594hxw4k8rwzpqlx2zqbrimmap5f5wzl5fynb6vn37ppd53c5")))

(define-public crate-quadtree-f32-0.3.2 (c (n "quadtree-f32") (v "0.3.2") (h "1zw88zv9bhpxa9w0kcd72kn7sqa1ixxjpdc15j9851fxvpqhap0s")))

(define-public crate-quadtree-f32-0.3.3 (c (n "quadtree-f32") (v "0.3.3") (h "0kh5m55m2ss69rrg5ccq9n41kmvyw7gcb8vazrsl631lmw06ikaa")))

(define-public crate-quadtree-f32-0.3.4 (c (n "quadtree-f32") (v "0.3.4") (h "1yicq968p64rg1263agvhjwz5kmd35vl0yfqka2wxw01808whirs")))

(define-public crate-quadtree-f32-0.4.0 (c (n "quadtree-f32") (v "0.4.0") (h "0m4yq1nbma2w5h5j3vl98kcqzd5b2mbghipyndxv98b3ppqyflkk")))

