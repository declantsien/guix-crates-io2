(define-module (crates-io qu ad quadtree_rs) #:use-module (crates-io))

(define-public crate-quadtree_rs-0.1.1 (c (n "quadtree_rs") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)) (d (n "derive_builder") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0i2d2i9fhvbplk2nwagnkfy7i1zlk4bak07gf3cxjj0pfpq9mjw0")))

(define-public crate-quadtree_rs-0.1.2 (c (n "quadtree_rs") (v "0.1.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)) (d (n "derive_builder") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1zczxldmb2cx2ylr42jvghm0y1678rkdyd5xfgrczxf00dlm1hbf")))

(define-public crate-quadtree_rs-0.1.3 (c (n "quadtree_rs") (v "0.1.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)) (d (n "derive_builder") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a8r4337v608k9d6pyv7alqfbrhsng1li3k44nqyg2lvnqikjdms") (s 2) (e (quote (("serde" "dep:serde"))))))

