(define-module (crates-io qu ad quad-alsa-sys) #:use-module (crates-io))

(define-public crate-quad-alsa-sys-0.3.1 (c (n "quad-alsa-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.56") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "16q19ac7ixd9426yksp9l8fq3kqzclmb9w48w5lzpg477nl3rgfy") (f (quote (("use-bindgen" "bindgen")))) (y #t) (l "alsa")))

(define-public crate-quad-alsa-sys-0.3.2 (c (n "quad-alsa-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.56") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1dflvkcczw4pr7cqrc2p3jjjsl0x4pf5mn3kg53r6qlllq22yv66") (f (quote (("use-bindgen" "bindgen")))) (l "alsa")))

