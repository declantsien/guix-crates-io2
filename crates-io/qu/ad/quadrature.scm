(define-module (crates-io qu ad quadrature) #:use-module (crates-io))

(define-public crate-quadrature-0.1.0 (c (n "quadrature") (v "0.1.0") (h "1h9l0q7pbcm67v5iz8w1a79d2fciaa0j0zb38xyn3d9k6sxayhcd")))

(define-public crate-quadrature-0.1.1 (c (n "quadrature") (v "0.1.1") (h "1xaijdc06im26f74n7bbsnz2dzbh79g0kw9bmjbd49hz5prk9hzr")))

(define-public crate-quadrature-0.1.2 (c (n "quadrature") (v "0.1.2") (h "1drlcsb10yn44kndagq065inlqvil6h3ld0yr0mwnks55yqcqm10")))

