(define-module (crates-io qu ad quad-cargo-apk) #:use-module (crates-io))

(define-public crate-quad-cargo-apk-0.1.0 (c (n "quad-cargo-apk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "cargo") (r "^0.53.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "multimap") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1dsnq9srmfx4dhh6a6wh65vx0iwjvpk922mx39gicxz8rcb682ba") (y #t)))

