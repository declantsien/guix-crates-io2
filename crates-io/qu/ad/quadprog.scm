(define-module (crates-io qu ad quadprog) #:use-module (crates-io))

(define-public crate-quadprog-0.0.0 (c (n "quadprog") (v "0.0.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "06p8ra4sx8ra35n2q8gigkajvag6qrwzrxyc9vm2zpbyal1wqqf2")))

(define-public crate-quadprog-0.0.1 (c (n "quadprog") (v "0.0.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0fqxkdxqbay1ci4pigvniadl1jrj4bgzr1g3m5s4676v80lwgmns")))

