(define-module (crates-io qu ad quad) #:use-module (crates-io))

(define-public crate-quad-0.1.0 (c (n "quad") (v "0.1.0") (h "1y1h2rgas4wrwpj81j692d021ip3iq7yava0y2sr3c34wi33i6yj")))

(define-public crate-quad-0.1.1 (c (n "quad") (v "0.1.1") (d (list (d (n "GSL") (r "^6.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.6") (d #t) (k 0)))) (h "0f4cq4xhq1slh7iq9rylwwpaazasxhg1691n65rgkpssg8x7w5dn")))

(define-public crate-quad-0.1.2 (c (n "quad") (v "0.1.2") (d (list (d (n "GSL") (r "^6.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.6") (d #t) (k 0)))) (h "1vq4wwwnymzhcqkhd9qi57bmli3hxdasv4wn55gq0z1gpck7rh3m")))

