(define-module (crates-io qu ad quad-rs) #:use-module (crates-io))

(define-public crate-quad-rs-0.1.0 (c (n "quad-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0lj30k5hi1h8hjwbv0p3p8x54kyh0kpmn76p3hkspl4fppxwcy46")))

(define-public crate-quad-rs-0.1.1 (c (n "quad-rs") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1yqf9wkbwpmkvqpp6hmf1vwav7krfiw6wlvmjz23zjsnc58v90ap")))

(define-public crate-quad-rs-0.1.2 (c (n "quad-rs") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0brwdxbjjnzgw14p52y2d524hy6l9llklxw0z797gr0ya9h3w4ww")))

