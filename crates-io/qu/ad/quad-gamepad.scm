(define-module (crates-io qu ad quad-gamepad) #:use-module (crates-io))

(define-public crate-quad-gamepad-0.2.0-alpha (c (n "quad-gamepad") (v "0.2.0-alpha") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winerror" "xinput"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0b2hz0shaykwcnqy294sl29byzc19n9fr0x8f8mfdvwvn1zaqnzh")))

