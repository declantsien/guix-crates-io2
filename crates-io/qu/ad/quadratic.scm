(define-module (crates-io qu ad quadratic) #:use-module (crates-io))

(define-public crate-quadratic-0.1.0 (c (n "quadratic") (v "0.1.0") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "0fagfj467aiq4hwjnfbb2gyda7w19p13d57w6427k4m1fbgaybmb")))

(define-public crate-quadratic-0.2.0 (c (n "quadratic") (v "0.2.0") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "0m5a2y85qsqd64x9yrc4jwlq39973l99r5sy2x40w7nyigrcbi8g")))

(define-public crate-quadratic-0.3.0 (c (n "quadratic") (v "0.3.0") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "0h44rj140ivjvpzi3sq1766m44fpr2s0w8aq69qa5ny04lm3js4v")))

(define-public crate-quadratic-0.3.1 (c (n "quadratic") (v "0.3.1") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "08dbxiw3nxdszas57rg26yckiabbzasf9wfj1q9062dzyfq5mkl3")))

