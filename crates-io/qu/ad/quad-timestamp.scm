(define-module (crates-io qu ad quad-timestamp) #:use-module (crates-io))

(define-public crate-quad-timestamp-0.1.0 (c (n "quad-timestamp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "16414p9zmi5v9r3l47z898skb0xq1p0b1501fcsg2iakvwfrxa4l")))

(define-public crate-quad-timestamp-0.1.1 (c (n "quad-timestamp") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0vzc53kxnv85cws92mjy6z8gigll7pssppjxalfzpdiv2kh9dmn7")))

(define-public crate-quad-timestamp-0.1.2 (c (n "quad-timestamp") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1gni7v6csqrnjrmxg427n28l44m2zp9aksyxicwph54g7xg02fc7")))

