(define-module (crates-io qu ad quad-rand) #:use-module (crates-io))

(define-public crate-quad-rand-0.1.0 (c (n "quad-rand") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1sx1h7wiaw4mx8j6wc2iss3dk0hk6xv23qlzxcp8fjvfq0pv0y35")))

(define-public crate-quad-rand-0.1.1 (c (n "quad-rand") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1igyf6wjglyf756rv44aj8xw1p4dfs2hkwn8p91qmdfrn6wqwgpc")))

(define-public crate-quad-rand-0.2.0 (c (n "quad-rand") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0xf2shr78pxm9vj7n65az957wnzvhhk9gn4h27k9smgj3rzmn2yw")))

(define-public crate-quad-rand-0.2.1 (c (n "quad-rand") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (o #t) (k 0)))) (h "125bw7b295khgwk7bnb6vkcdjyki1xbfzrcygh2mzk54yzxa33v5")))

