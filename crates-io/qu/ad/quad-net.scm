(define-module (crates-io qu ad quad-net) #:use-module (crates-io))

(define-public crate-quad-net-0.1.1 (c (n "quad-net") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.0-alpha") (d #t) (k 2)) (d (n "nanoserde") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 2)) (d (n "qws") (r "^0.7.9") (f (quote ("ssl"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sapp-jsutils") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "ureq") (r "^2.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "147i88gabyzipinb78khrdmbx6f9xkc0myi2i06a9f3w0pj7ysab") (f (quote (("default" "nanoserde"))))))

