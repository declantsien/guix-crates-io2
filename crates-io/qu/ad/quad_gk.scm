(define-module (crates-io qu ad quad_gk) #:use-module (crates-io))

(define-public crate-quad_gk-0.1.0 (c (n "quad_gk") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "0fxwj1yjs21gnj3hw22fpa0sjy5w5bh64633m2640c5mkljcv291")))

(define-public crate-quad_gk-0.1.1 (c (n "quad_gk") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "0g5bmfdd5shi6dd9hq2sl5z1p4f7g5fdsidp4xzfzkr0w04ian9r")))

(define-public crate-quad_gk-1.0.0 (c (n "quad_gk") (v "1.0.0") (d (list (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1ww099jq9yzfz21d959gaqj73jdsz87bdd2kzcqjvcvxr2v4d49n")))

