(define-module (crates-io qu ad quadrature-encoder) #:use-module (crates-io))

(define-public crate-quadrature-encoder-0.1.0 (c (n "quadrature-encoder") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.19") (k 0)) (d (n "quadrature-decoder") (r "^0.1.0") (k 0)))) (h "0nxx9bmxsjqay0l17aadh2yc41d0n5id1r2w3a6s41qn7cljf0xf") (r "1.74.0")))

