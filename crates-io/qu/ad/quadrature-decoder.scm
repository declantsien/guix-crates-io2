(define-module (crates-io qu ad quadrature-decoder) #:use-module (crates-io))

(define-public crate-quadrature-decoder-0.1.0 (c (n "quadrature-decoder") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.19") (k 0)))) (h "1spafy9dh80nzibqkqd1sp37dh4p5si1rmvcil8p30xb32h3q484") (r "1.74.0")))

