(define-module (crates-io qu ad quad-gl) #:use-module (crates-io))

(define-public crate-quad-gl-0.1.0 (c (n "quad-gl") (v "0.1.0") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.2") (d #t) (k 0)))) (h "02f8d7y56m4f6yy6r1zqx2m6ma78s9xx197p8r0n35zb1b65qbyb")))

(define-public crate-quad-gl-0.1.1 (c (n "quad-gl") (v "0.1.1") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.2") (d #t) (k 0)))) (h "162l64h6xl37zrkyri3ic8g1yhcn3f4fnjcr47psjy38xp33c25i")))

(define-public crate-quad-gl-0.1.2 (c (n "quad-gl") (v "0.1.2") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.2") (d #t) (k 0)))) (h "1xb5kgi5iwqai3rc6qmy6hfldphxlh8vfqlw263pil0b3ksyj06d")))

(define-public crate-quad-gl-0.1.3 (c (n "quad-gl") (v "0.1.3") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.2") (d #t) (k 0)))) (h "1pih67vzsqwrw9gwfacbp993qd2nrzgxp6070718k8w6wnshqndw")))

(define-public crate-quad-gl-0.2.0 (c (n "quad-gl") (v "0.2.0") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "= 0.3.0-alpha.5") (d #t) (k 0)))) (h "14pbvbsx2v6qhqhzyca2jc99xs7kn4snq72f0jz2aad4k6i727dn")))

(define-public crate-quad-gl-0.2.1 (c (n "quad-gl") (v "0.2.1") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "144pkxc97hnbcybjn7p544fw5rmhf6i8w6jxsi24xg2508v12z7s")))

(define-public crate-quad-gl-0.2.2 (c (n "quad-gl") (v "0.2.2") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0pcjkfhcgq27jsakh9pqn5kamn6h5qfwx95fqb8fs2y6fad6zmsf")))

(define-public crate-quad-gl-0.2.3 (c (n "quad-gl") (v "0.2.3") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1xgv5zk4wybb9bb8yysxmf41z20acgd2zy9h7j86hk1lgr7w7ad6")))

(define-public crate-quad-gl-0.2.4 (c (n "quad-gl") (v "0.2.4") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0gz8izki0kwqabhp7y4ybhaqpyi69disgk582bj32xviljsfkpk7")))

(define-public crate-quad-gl-0.2.5 (c (n "quad-gl") (v "0.2.5") (d (list (d (n "glam") (r "^0.9") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.15") (d #t) (k 0)))) (h "0rkjz2pwygxvn9sxzgz2048pd1fghmy9y1wwr07s18jxa4ck25zr")))

(define-public crate-quad-gl-0.2.7 (c (n "quad-gl") (v "0.2.7") (d (list (d (n "glam") (r "^0.9") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.19") (d #t) (k 0)))) (h "03zi77h72xs86n1k9bv5c4y8lcwi1wrh6p1yza154sj0ca97cg0f")))

(define-public crate-quad-gl-0.2.8 (c (n "quad-gl") (v "0.2.8") (d (list (d (n "glam") (r "^0.9") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.19") (d #t) (k 0)))) (h "1cjqy8lc8kpnkmm6nax45hs1wr6m5sr087dxllaggpb8fychn0p7")))

(define-public crate-quad-gl-0.2.9 (c (n "quad-gl") (v "0.2.9") (d (list (d (n "glam") (r "^0.9") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.21") (d #t) (k 0)))) (h "1awh40d3h5qhrdw3r12yqr66cryyl8rpfv8zjgwivb7k4gsv7lkz")))

(define-public crate-quad-gl-0.2.10 (c (n "quad-gl") (v "0.2.10") (d (list (d (n "glam") (r "^0.9") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.22") (d #t) (k 0)))) (h "0valn0xahbp3f2mvdr4zv5z4ysl7n0i2g0n32k1w0wv981xksx2w")))

(define-public crate-quad-gl-0.2.11 (c (n "quad-gl") (v "0.2.11") (d (list (d (n "glam") (r "^0.9") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.22") (d #t) (k 0)))) (h "1a4ms0wah912rgpyxxl1n2nxw6aqxm5sbsfwbdj3v0sbnsxjv6v2")))

(define-public crate-quad-gl-0.2.12 (c (n "quad-gl") (v "0.2.12") (d (list (d (n "glam") (r ">=0.9.0, <0.10.0") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r ">=0.22.0, <0.23.0") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r ">=0.3.0-alpha.22, <0.4.0") (d #t) (k 0)))) (h "1v5q6mflhq7jy8n7l1pm41zgskar17c4v0m4c96zpz1j6n1hckji")))

(define-public crate-quad-gl-0.2.13 (c (n "quad-gl") (v "0.2.13") (d (list (d (n "glam") (r "^0.10") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec"))) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.22") (d #t) (k 0)))) (h "1dg8dbcmk0878snbypy71vmzxh4gpsik3g6cx5gsg1vmj6jcgp2a")))

