(define-module (crates-io qu ad quadprogpp) #:use-module (crates-io))

(define-public crate-quadprogpp-0.1.0 (c (n "quadprogpp") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "quadprogpp-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0s2c7ql4ishscwryddd9afv5ggmrdhijc6sq67sfv5h3hp28afny")))

