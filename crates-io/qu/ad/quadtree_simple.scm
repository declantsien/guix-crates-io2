(define-module (crates-io qu ad quadtree_simple) #:use-module (crates-io))

(define-public crate-quadtree_simple-0.1.0 (c (n "quadtree_simple") (v "0.1.0") (h "0l8ihssm68rvyn1856p3j871aqlh75xls18gjif4fs66nj6cxa54") (y #t)))

(define-public crate-quadtree_simple-0.1.1 (c (n "quadtree_simple") (v "0.1.1") (h "0xdcikwbn49m31lsrk89j6340shgvigvfrigf7qxf2jyfgisc1pr") (y #t)))

(define-public crate-quadtree_simple-0.1.2 (c (n "quadtree_simple") (v "0.1.2") (h "0z3wg0nr77afgj7izks767w3m8iwy0hx23akn0583x391iizxaf5") (y #t)))

(define-public crate-quadtree_simple-0.1.3 (c (n "quadtree_simple") (v "0.1.3") (h "0h2i2yfszmfdffkz9wlimhk6npy16dmyphbp9406wn0nracand15") (y #t)))

(define-public crate-quadtree_simple-0.1.4 (c (n "quadtree_simple") (v "0.1.4") (h "1lyilc23yshf7kxcmfzc9akfkdf4csr96jp0s51733r4p75fal8c") (y #t)))

(define-public crate-quadtree_simple-0.1.5 (c (n "quadtree_simple") (v "0.1.5") (h "16i7yxgkzafn68q2h1n86sqxpr8z857avd891v08hns91kgq04b3") (y #t)))

(define-public crate-quadtree_simple-0.1.6 (c (n "quadtree_simple") (v "0.1.6") (d (list (d (n "macroquad") (r "^0.4.5") (d #t) (k 2)))) (h "1k5zb99i2mn4hkhhiz8k1kc9whaih4giraxps713xy19zbvajhxc") (y #t)))

(define-public crate-quadtree_simple-0.1.7 (c (n "quadtree_simple") (v "0.1.7") (d (list (d (n "macroquad") (r "^0.4.5") (d #t) (k 2)))) (h "0vk4afh7njpgpszbvgp4lasyipya699ciipm0zdaglzcw354rxwv")))

