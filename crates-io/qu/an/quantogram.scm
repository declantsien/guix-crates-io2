(define-module (crates-io qu an quantogram) #:use-module (crates-io))

(define-public crate-quantogram-0.1.0 (c (n "quantogram") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 2)) (d (n "skiplist") (r "^0.4") (d #t) (k 0)) (d (n "zw-fast-quantile") (r "^0.2") (d #t) (k 2)))) (h "10nbplkygmndssrb8phmj6vxbvfjf7mp4qqxlkpjiqjw5rbqn1bp")))

(define-public crate-quantogram-0.2.0 (c (n "quantogram") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 2)) (d (n "skiplist") (r "^0.4") (d #t) (k 0)) (d (n "zw-fast-quantile") (r "^0.2") (d #t) (k 2)))) (h "0vsps21p65i32s0lkz10a9v30jll7akcziqjz2xz2f6wbiflyyjm")))

(define-public crate-quantogram-0.3.0 (c (n "quantogram") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 2)) (d (n "skiplist") (r "^0.4") (d #t) (k 0)) (d (n "zw-fast-quantile") (r "^0.2") (d #t) (k 2)))) (h "0vha50p8sidalg3asqwdi2640xsi7li5iqpbnlb7xm1ga1lqqxxm")))

(define-public crate-quantogram-0.4.0 (c (n "quantogram") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 2)) (d (n "skiplist") (r "^0.4") (d #t) (k 0)) (d (n "zw-fast-quantile") (r "^0.2") (d #t) (k 2)))) (h "02c78pdffp3cdpkqgnp9z4b6fb7aarzx4nay3yd1pnfxb6vhzp9r")))

(define-public crate-quantogram-0.4.1 (c (n "quantogram") (v "0.4.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 2)) (d (n "skiplist") (r "^0.4") (d #t) (k 0)) (d (n "zw-fast-quantile") (r "^0.2") (d #t) (k 2)))) (h "0ayj6mxlr01c9qzmfl7vxc1iywldrwrbc9d1mh8hgx69jifpkaxn")))

(define-public crate-quantogram-0.4.2 (c (n "quantogram") (v "0.4.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 2)) (d (n "skiplist") (r "^0.4") (d #t) (k 0)) (d (n "zw-fast-quantile") (r "^0.2") (d #t) (k 2)))) (h "0b7b417sk7wk549ngpw07ancfpff3caya3siwc65hd4srv842k5l")))

(define-public crate-quantogram-0.4.3 (c (n "quantogram") (v "0.4.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 2)) (d (n "skiplist") (r "^0.4") (d #t) (k 0)) (d (n "zw-fast-quantile") (r "^0.2") (d #t) (k 2)))) (h "1ab21lhl69vlrl5lshv6j12bm48q7q6xxw0qlrn34fjqil4bjdfg")))

(define-public crate-quantogram-0.4.4 (c (n "quantogram") (v "0.4.4") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "float_extras") (r "^0.1") (d #t) (k 0)) (d (n "quantiles") (r "^0.7") (d #t) (k 2)) (d (n "skiplist") (r "^0.4") (d #t) (k 0)) (d (n "zw-fast-quantile") (r "^0.2") (d #t) (k 2)))) (h "0hxk6ns7fhxbg0mcblkxkq7f534w5n5rmrnapl5887b6i5pizmkp")))

