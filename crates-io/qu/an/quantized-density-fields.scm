(define-module (crates-io qu an quantized-density-fields) #:use-module (crates-io))

(define-public crate-quantized-density-fields-0.1.0 (c (n "quantized-density-fields") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0lhyvmvdsv1b08j8x941jkpn5vz2hj1b92f1r0m9s1yhnd01a6hd")))

(define-public crate-quantized-density-fields-0.1.1 (c (n "quantized-density-fields") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0jx8xmnb72d6ixf9bp4m4dk1b79f5qw46i25rkjmffn1gcl8h3w3")))

(define-public crate-quantized-density-fields-0.1.2 (c (n "quantized-density-fields") (v "0.1.2") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "11qshsy7a78yl9ii0pb5k22mrd7xaz93b7b4j9spqfbnpqwkrswf")))

(define-public crate-quantized-density-fields-0.1.3 (c (n "quantized-density-fields") (v "0.1.3") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ckhgjpajdjixgaa2wb9bk0v8m4c5vx2j9mh0l5v13lgz2yy7479")))

(define-public crate-quantized-density-fields-0.1.4 (c (n "quantized-density-fields") (v "0.1.4") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1xa2dmmnss4qfj90143yah5i0ffxgbjg2zsx9jgfdaagf2vbdyxf")))

(define-public crate-quantized-density-fields-0.1.5 (c (n "quantized-density-fields") (v "0.1.5") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0gvxsqmxw625jqj8aqwjdh9lyfr5ria8giihbza49mh8nr2yg2bk")))

(define-public crate-quantized-density-fields-0.1.6 (c (n "quantized-density-fields") (v "0.1.6") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1v1lh0ldjmjr8h348f0np7iw2dlgplr4c93xk98ncfqp11j7z9qx")))

(define-public crate-quantized-density-fields-0.1.7 (c (n "quantized-density-fields") (v "0.1.7") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "130i6p0iarwg42s31dirmr2ylqiqgagb5zlgbsbnbd4nhwsps07l")))

(define-public crate-quantized-density-fields-0.1.8 (c (n "quantized-density-fields") (v "0.1.8") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1287g4x6h2zif79qhn73qkzk99zaqxgjj5fq0v6fgh8gnfj07kvc")))

(define-public crate-quantized-density-fields-0.1.9 (c (n "quantized-density-fields") (v "0.1.9") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0308x277alqyah4waiwa535dl1dd4c9lbf6q60550kxx3mahadkn")))

(define-public crate-quantized-density-fields-0.2.0 (c (n "quantized-density-fields") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "11ppswqnkafj30jyqjni0n3ddd42lnsig7gj9mk2y0n3abq40bhl")))

(define-public crate-quantized-density-fields-0.2.2 (c (n "quantized-density-fields") (v "0.2.2") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1shxaxwcn0pv7g5kxxyy19i6wjsszccb5ci0m21b0jg190qi2yhf")))

(define-public crate-quantized-density-fields-0.2.3 (c (n "quantized-density-fields") (v "0.2.3") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0jdq2lx2rg8shx1f1fy04159hjjid9a2vsa28jd1xgj3chpwig7k")))

