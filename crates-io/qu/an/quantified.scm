(define-module (crates-io qu an quantified) #:use-module (crates-io))

(define-public crate-quantified-0.1.0 (c (n "quantified") (v "0.1.0") (h "166gl4gz4qyshqcw44w5z3f5f5lyxn7ybg9zcp3f22wnws8vpr4q")))

(define-public crate-quantified-0.1.1 (c (n "quantified") (v "0.1.1") (h "0wqrbhk2m6b44m4vy95a4rp1z3fm5b68q5bq38sns8fjfqf6yw7h")))

