(define-module (crates-io qu an quant-suite) #:use-module (crates-io))

(define-public crate-quant-suite-0.1.1 (c (n "quant-suite") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "ctrader-fix") (r "^0.5") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "logger") (r "^0.4") (d #t) (k 2)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07rzggkxvqw3z95r7achh63pk0wiw4j4rn9dagnwf05m2xkczb26")))

