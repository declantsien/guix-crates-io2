(define-module (crates-io qu an quandl-v3) #:use-module (crates-io))

(define-public crate-quandl-v3-0.1.0 (c (n "quandl-v3") (v "0.1.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "has") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "0axjll74q5kgz965rbbmcrgq4ayadyz1a578ds6ygnmkydcrqjh7")))

(define-public crate-quandl-v3-0.1.1 (c (n "quandl-v3") (v "0.1.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "has") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "00f0zil6999xcjnd2v6brx1kyykb609pxqnvfnk5sijpwq8vmals")))

(define-public crate-quandl-v3-1.0.0 (c (n "quandl-v3") (v "1.0.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "has") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "094js6g0ij4w121in0iijnlbqdynva0lg1l8dsnnvvr8wn6vvf50")))

(define-public crate-quandl-v3-1.1.0 (c (n "quandl-v3") (v "1.1.0") (d (list (d (n "csv") (r "^1.0.0-beta.3") (d #t) (k 0)) (d (n "has") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "1mkyji0ssjq2bsmjcj983nllc4ha2g4vcqmkw0hizv7rnp4ld0y8")))

(define-public crate-quandl-v3-1.2.0 (c (n "quandl-v3") (v "1.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "has") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "102z1j2ybyfwcmq5g1piixq3qlk7ph5fcarp5119imymhq3jh83n")))

