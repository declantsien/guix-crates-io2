(define-module (crates-io qu an quantleaf-test) #:use-module (crates-io))

(define-public crate-quantleaf-test-0.1.0 (c (n "quantleaf-test") (v "0.1.0") (h "1wg8h8gb7m8vpk1sy6j7a12m3w2zh276am91xmjk4ch0v9y3mwqn") (y #t)))

(define-public crate-quantleaf-test-0.1.1 (c (n "quantleaf-test") (v "0.1.1") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1750l8kqmnladr2wrm9n3wvv04vvfirnha95rh7n2k8xjwyl2m7h") (y #t)))

(define-public crate-quantleaf-test-0.1.2 (c (n "quantleaf-test") (v "0.1.2") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0lsj9bq0ym0r5cs6gbaigpskz6043fcbl3hx3j5ndsyh0aklrvyy") (y #t)))

(define-public crate-quantleaf-test-0.2.0 (c (n "quantleaf-test") (v "0.2.0") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mf1347531ahr5ml7s7syg82454lv68mpz6y6maci53dvndzrb09") (y #t)))

