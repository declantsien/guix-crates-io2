(define-module (crates-io qu an quant) #:use-module (crates-io))

(define-public crate-quant-0.0.1 (c (n "quant") (v "0.0.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0lkvivyj36cp0ikssldiyqb5jmzch98dfiwkrmsivvdmv64nk4ix") (y #t)))

(define-public crate-quant-0.0.2 (c (n "quant") (v "0.0.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "06vhj4kv6mcpnqnwshlc49h7x2wnsld0hjai3dkd151plvir0bv9") (y #t)))

(define-public crate-quant-0.0.3 (c (n "quant") (v "0.0.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1kadfraz8q90qjljrqymhnnnxzm6axck0g6fchc26vsjif3ny267") (y #t)))

(define-public crate-quant-0.0.4 (c (n "quant") (v "0.0.4") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0579rz6vsvy9fgacd2f670wv62c9fs0hk53amln6kv3hlg36cz23") (y #t)))

