(define-module (crates-io qu an quantmath) #:use-module (crates-io))

(define-public crate-quantmath-0.1.0 (c (n "quantmath") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.15.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tagged") (r "^0.2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.9.0") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1pcfg8bxvk9yfrb0j6pkxcfxkafp42h5q4fh2mg0pgh02z849k89")))

