(define-module (crates-io qu an quantifiable-derive) #:use-module (crates-io))

(define-public crate-quantifiable-derive-0.1.0 (c (n "quantifiable-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "synstructure") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "1c3bb1khx38rnlk8rl9qlz7yydf75gj931ri331k6mv05w7krrsi")))

