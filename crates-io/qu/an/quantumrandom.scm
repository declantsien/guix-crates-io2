(define-module (crates-io qu an quantumrandom) #:use-module (crates-io))

(define-public crate-QuantumRandom-0.1.0 (c (n "QuantumRandom") (v "0.1.0") (d (list (d (n "atoi") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)))) (h "16cskllimgnrzhj4yfa53y3wjxa6ils5ghrgank281r9jq8kq2fj")))

(define-public crate-QuantumRandom-0.1.1 (c (n "QuantumRandom") (v "0.1.1") (d (list (d (n "atoi") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)))) (h "1sgdfkxddida5rvlib45517hg5s1ma4q2wnwzirsdhcch207jvis")))

(define-public crate-QuantumRandom-0.1.2 (c (n "QuantumRandom") (v "0.1.2") (d (list (d (n "atoi") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)))) (h "1k6i71n0wkp5xkzcladx4nv28cpwg319c6xqvbwpzyasbn5cr7wc")))

(define-public crate-QuantumRandom-0.2.0 (c (n "QuantumRandom") (v "0.2.0") (d (list (d (n "atoi") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)))) (h "1fb7sh7wzdijrwkhfa6q8ss8xl3jpd342h9sfslml4zfknghkp60")))

