(define-module (crates-io qu an quantizr) #:use-module (crates-io))

(define-public crate-quantizr-1.4.0 (c (n "quantizr") (v "1.4.0") (h "05nkp6875sy3znm4qnld1g1yzhvkip7v8nmcsdd3a7b4gv72lb1l") (f (quote (("capi"))))))

(define-public crate-quantizr-1.4.1 (c (n "quantizr") (v "1.4.1") (h "090cbbdyxj7ybg8j82mf7cm62cmv5l4lashl5ri9yffcf1bzwwfs") (f (quote (("capi"))))))

(define-public crate-quantizr-1.4.2 (c (n "quantizr") (v "1.4.2") (h "0fcd2yx1h99219fc5i048a9ljrvrhd38y3h8iz7wbiz6sj7ggql4") (f (quote (("capi"))))))

