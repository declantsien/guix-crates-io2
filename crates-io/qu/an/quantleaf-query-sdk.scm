(define-module (crates-io qu an quantleaf-query-sdk) #:use-module (crates-io))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.0 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.0") (h "1jxp72qrcjvb2daqmfhwslbbydw3kranqjnl183dpdhaxkd7skk9")))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.1 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dqv16zr1gb5ggi9gp675h888yih53kwx5kwnyvw4pgybjzdd6a4")))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.2 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1s9dxlcrpwn2dav15np7931qr0b60arw862lbg82gcs4krxcrfjv")))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.3 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1d6xgq77mhychck7bvambsr2dhfadmm46k5pnafws5bc9vh4slic")))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.5 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.5") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jcr5ambq9668dzawa8v158bvl5nz40293aidl8g6gnjlv5r7lja")))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.6 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.6") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0s6sy4yf2b7f803g56gxrcja5xj4gmhr38iq5z7fcj43nh91c27c")))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.7 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.7") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0521f4lyqnwgzdc17zpv6ad167bbwdjld7qbypmi9fkdv20pz036")))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.8 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.8") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04k3xf2irdyj4qgmijbqjqrh14bfk2q1xzxmfj5fali6gws3hkzp")))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.9 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.9") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "19dq6i8dfayi6nb33wx92sz6p207rqf4gi6n3rirxqw83m7pb0fi")))

(define-public crate-quantleaf-query-sdk-0.0.1-alpha.11 (c (n "quantleaf-query-sdk") (v "0.0.1-alpha.11") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17zba7195qcqv04yqp80cbq7ks0y3c2aa4312xa3gxilpnmkmqnp")))

