(define-module (crates-io qu an quantum-entangler) #:use-module (crates-io))

(define-public crate-quantum-entangler-0.1.0 (c (n "quantum-entangler") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ifihsiyczga403bzrxb94963cali2w2zkp1cx6hyqi4d1ig8rcq")))

(define-public crate-quantum-entangler-0.1.1 (c (n "quantum-entangler") (v "0.1.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1vq9g2c3d1jrwlaq8ya0y01v32xvjisw6x1k30f55m8kagcxcxq0")))

