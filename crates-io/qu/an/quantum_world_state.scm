(define-module (crates-io qu an quantum_world_state) #:use-module (crates-io))

(define-public crate-quantum_world_state-0.1.0 (c (n "quantum_world_state") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "maybe-owned") (r "^0.3.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)))) (h "1q860w74q91m3nhxkap2rif5f9nq16x0xy7sdpc1x0pqw982122m")))

