(define-module (crates-io qu an quantum) #:use-module (crates-io))

(define-public crate-quantum-0.1.0 (c (n "quantum") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1bzga49j08m1c9h8knrykgml1s8x17hdy9x67canci45pl733lzw") (f (quote (("optimize"))))))

(define-public crate-quantum-0.1.2 (c (n "quantum") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1p5n4gsxxa77c8a5v5q2ikg4vp9bdg9aqhqa7sbg7vjg2bhiiqan") (f (quote (("optimize"))))))

