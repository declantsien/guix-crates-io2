(define-module (crates-io qu an quantum2) #:use-module (crates-io))

(define-public crate-quantum2-0.1.3 (c (n "quantum2") (v "0.1.3") (d (list (d (n "float-cmp") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1yl6ik67nkki3p2yy15n9fja61m3grl4rvjqw97479dw3q0j02ci") (f (quote (("optimize"))))))

