(define-module (crates-io qu an quantum_state_sim) #:use-module (crates-io))

(define-public crate-quantum_state_sim-0.1.0 (c (n "quantum_state_sim") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (d #t) (k 0)))) (h "00g03mq16jpk9dcmqdcj0xw50zam48adbzhydlg519y6n9kr24hs") (y #t) (r "1.56")))

(define-public crate-quantum_state_sim-0.1.1 (c (n "quantum_state_sim") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (d #t) (k 0)))) (h "0yk72b1b0j172y74k3jfx97spmiqp98ap3mr1dz8pd6awi8ml591") (r "1.67")))

