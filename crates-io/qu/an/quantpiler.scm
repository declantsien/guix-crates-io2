(define-module (crates-io qu an quantpiler) #:use-module (crates-io))

(define-public crate-quantpiler-0.1.0 (c (n "quantpiler") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "18phdgshgkk8bciwh122qjzyixxcsl43a0i77n5l6q9cwkj51cwg")))

