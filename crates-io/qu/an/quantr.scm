(define-module (crates-io qu an quantr) #:use-module (crates-io))

(define-public crate-quantr-0.1.0 (c (n "quantr") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1z7psvyibswvw5rwirg7zxc3rar7ry3f7av8cdl0i2bsypqkdknm")))

(define-public crate-quantr-0.1.1 (c (n "quantr") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05fl8zawr98gwjyah1i5hvpzwwyx22sf04rri231c5y3mfsihm85")))

(define-public crate-quantr-0.1.2 (c (n "quantr") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ir0s70r9614778f9fw6ywk8fkin13ids7dwvsrinqklh6d6sbp3")))

(define-public crate-quantr-0.2.0 (c (n "quantr") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1sqz6qd3n4gabqmn4xnk7nfdzvgz2wggyv5hl7f9xq83c78wcpzg")))

(define-public crate-quantr-0.2.1 (c (n "quantr") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11kl0nns07fv15jz5jf2bqxkfy8p1rllc69i3nrxnz9gzznshfrn")))

(define-public crate-quantr-0.2.2 (c (n "quantr") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "082j3m10qbknlx93k22crdsh8f8mmpcwswpd7br74qs3a0x7vv7n")))

(define-public crate-quantr-0.2.3 (c (n "quantr") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zlwdky4r10zw8qzpklizdj34p64w7fx9bawv7b4bgcnj5pmyvy4")))

(define-public crate-quantr-0.2.4 (c (n "quantr") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x7aaf53ixmc6f8fmfspk8p7l44w8wi4b73la0a1ivcx0dg5b9hb")))

(define-public crate-quantr-0.2.5 (c (n "quantr") (v "0.2.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xdaqy0kgfh0j0jzdl2gh382665g7fpw4dx7adzvx5g8lma70kv6")))

(define-public crate-quantr-0.3.0 (c (n "quantr") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fwl7x8haphn6fy5hm3jdqsxl6lv4n595srcfza8y4ss639n8z4z")))

(define-public crate-quantr-0.4.0 (c (n "quantr") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08b5isahj8lrqsrmqs80wmc3a40pf0jp576j6n3k1jkc1mdag7hk")))

(define-public crate-quantr-0.4.1 (c (n "quantr") (v "0.4.1") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)))) (h "0mhliv44riqcc37k0hnl7mwmr4lkgg7r771qcnzpnkv8y45y239m")))

(define-public crate-quantr-0.5.0 (c (n "quantr") (v "0.5.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)))) (h "170xhm5k652l2h8bnmgavrcqlmmapfzxaszmnbw0lfjyljwhdzy7")))

(define-public crate-quantr-0.5.1 (c (n "quantr") (v "0.5.1") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)))) (h "0vjmpv4js4mzakxsdsiabk2n2nkw79l4nx6qgarx1a2y3sdy7809")))

(define-public crate-quantr-0.5.2 (c (n "quantr") (v "0.5.2") (d (list (d (n "fastrand") (r "^2.1.0") (d #t) (k 0)))) (h "12wrr0hvlfd8x4wysmfprla8ziiz0aqkfm9whx5hwhr8b9928nva")))

