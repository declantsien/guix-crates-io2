(define-module (crates-io qu an quantum-mc) #:use-module (crates-io))

(define-public crate-quantum-mc-0.1.0 (c (n "quantum-mc") (v "0.1.0") (d (list (d (n "libic") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xmltojson") (r "^0") (d #t) (k 0)))) (h "136pi16hkxwsidcsyyn6izdwrch9flw1yivz14bhf5jcsgnw1lq8")))

