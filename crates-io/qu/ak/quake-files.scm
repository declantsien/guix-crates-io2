(define-module (crates-io qu ak quake-files) #:use-module (crates-io))

(define-public crate-quake-files-0.1.0 (c (n "quake-files") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0mwlrd23r22cy1hb5641fvrp1n93saibjacdh72qkzdx8mcfyh1q") (f (quote (("nightly" "clippy"))))))

