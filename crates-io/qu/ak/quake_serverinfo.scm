(define-module (crates-io qu ak quake_serverinfo) #:use-module (crates-io))

(define-public crate-quake_serverinfo-0.1.0 (c (n "quake_serverinfo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1rf9nzv858fdsw53rfzkdqdh4gdf7birwm2xzvz37ll5bqm9p7fz")))

(define-public crate-quake_serverinfo-0.2.0 (c (n "quake_serverinfo") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1zl9qirhlwgajjw0iyq1vh1a83vry8kp99317gdhp2a2cydmsjdx")))

(define-public crate-quake_serverinfo-0.2.1 (c (n "quake_serverinfo") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1b2xddrgka50wk05b3bj9q004zn1ysl41c03vz9bqr0r9v77qp95")))

(define-public crate-quake_serverinfo-0.3.0 (c (n "quake_serverinfo") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1b2q3y69wnaa9va5xw7n70xj3z0l2xzhr7rf8x3mcjwhw0v3xh3n")))

(define-public crate-quake_serverinfo-0.4.0 (c (n "quake_serverinfo") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "quake_infostring") (r "^0.1.0") (d #t) (k 0)))) (h "193qd4a36ry0ziava2dvcal4acldbx3jgfdlv13mnz03ymvpslhx")))

