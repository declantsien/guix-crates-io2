(define-module (crates-io qu ak quake_text) #:use-module (crates-io))

(define-public crate-quake_text-0.1.0 (c (n "quake_text") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1mk76ksfzgml527midk0bifpw2q0djdlg2qwxs1429s4xgv18sav")))

(define-public crate-quake_text-0.1.1 (c (n "quake_text") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1zjg9gh5pplkyhi6mf7z8fdjjp7qqx27misx11hh16hgyk5ykni0")))

