(define-module (crates-io qu ak quake_microsoft_todo) #:use-module (crates-io))

(define-public crate-quake_microsoft_todo-0.1.0 (c (n "quake_microsoft_todo") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0vfv1kj01pzblvlbmazabh7a4ycjnjhfsn6k772xkpswk5f1baay")))

(define-public crate-quake_microsoft_todo-0.1.1 (c (n "quake_microsoft_todo") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0njb3bvd4g3a6ljxdvj60m5p2fc94ag1jq2sfvh0q287xxjqim5c")))

