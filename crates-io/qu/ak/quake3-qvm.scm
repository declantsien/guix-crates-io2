(define-module (crates-io qu ak quake3-qvm) #:use-module (crates-io))

(define-public crate-quake3-qvm-0.1.0 (c (n "quake3-qvm") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1403dy3yv80r9nqbhpz929yzd6g7p717w1vcpn4mf0j0vdzrs51i")))

(define-public crate-quake3-qvm-0.1.1 (c (n "quake3-qvm") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "09l5ip8xbkifagq31xwfxbsbmdpfv529didyinscqg2f48qm0mfy")))

(define-public crate-quake3-qvm-0.2.0 (c (n "quake3-qvm") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0j2njkmdvgsgvc82xi457nmjw1la0nwc9xail652qszv2wjkh336")))

(define-public crate-quake3-qvm-0.3.0 (c (n "quake3-qvm") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "12ni4llr3b0dgkf55h7qzrl55cmq7qqgrpkzj6xgw9fhcgkx0ab5")))

(define-public crate-quake3-qvm-0.4.0 (c (n "quake3-qvm") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "10lwwzz21jvqpk0ph0h5gr7pjwxik748pxr8p8yrz6ndd6ai31f6") (y #t)))

(define-public crate-quake3-qvm-0.4.2 (c (n "quake3-qvm") (v "0.4.2") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1wxxa0hlmsy4y44k9b7xdp9h8vpm4ghgr0yg8mag5jb46c531cwk")))

(define-public crate-quake3-qvm-0.4.3 (c (n "quake3-qvm") (v "0.4.3") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0sk2qhhjxq604zd7zzp3l781y0x5vsnrm0s8ls56fvcxppyc8y99")))

(define-public crate-quake3-qvm-0.4.4 (c (n "quake3-qvm") (v "0.4.4") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0bzm9c8vm8qxx5knsaca0kiviarr81da7lfzryhkrhq1za9ab06y")))

(define-public crate-quake3-qvm-0.5.0 (c (n "quake3-qvm") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1p39anixhs9pawncydbcsjyj1rgzv0wnmwnr80nxsx6my1mj9arv")))

(define-public crate-quake3-qvm-0.6.0 (c (n "quake3-qvm") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1l3nffs6py3dbw9z0mdvcrl1w354552zg889g5j3jkgm7xi5l3kk")))

