(define-module (crates-io qu ak quake3_loader) #:use-module (crates-io))

(define-public crate-quake3_loader-0.1.0 (c (n "quake3_loader") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)))) (h "05hrbwvn7hhhphcqg8rdhcaswm9ldyzibbgx9xgfzk8imzx5lwf4")))

(define-public crate-quake3_loader-0.2.0 (c (n "quake3_loader") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)))) (h "03pdj0wzni08p2w1kzd90365x3z8ak44c2qb9jdkq2q1cq50jsm3")))

