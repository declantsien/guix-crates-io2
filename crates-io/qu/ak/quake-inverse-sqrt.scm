(define-module (crates-io qu ak quake-inverse-sqrt) #:use-module (crates-io))

(define-public crate-quake-inverse-sqrt-0.1.0 (c (n "quake-inverse-sqrt") (v "0.1.0") (h "1qq764rm32qs6dv8dgd3gdf1v0kp5y2bb1kvgb64925xfcxd723n")))

(define-public crate-quake-inverse-sqrt-0.1.1 (c (n "quake-inverse-sqrt") (v "0.1.1") (h "0n491vxn6ri9jya0971r7vjh3x88xifh14298p691janaiv11p57")))

(define-public crate-quake-inverse-sqrt-0.1.2 (c (n "quake-inverse-sqrt") (v "0.1.2") (h "0c1047hs3pqbznzd0fwkrjabmxb4fcki1appm2qw7v2i7wfwmhyj")))

(define-public crate-quake-inverse-sqrt-0.1.3 (c (n "quake-inverse-sqrt") (v "0.1.3") (h "1ssz0q7mbi2lar1sxdnq04px5nacsnyqcyb4w45rql41xnckp724")))

