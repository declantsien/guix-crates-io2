(define-module (crates-io qu ak quakeworld-protocol-macros) #:use-module (crates-io))

(define-public crate-quakeworld-protocol-macros-0.0.1 (c (n "quakeworld-protocol-macros") (v "0.0.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16ycvgdc3x096wpsipkp6ic19axvmk06icd514al838mlnqav091")))

(define-public crate-quakeworld-protocol-macros-0.0.2 (c (n "quakeworld-protocol-macros") (v "0.0.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05gjwd117wcixmkpl66s6hhpn3axg5m0h3yspzbvjh61x17n3y38")))

