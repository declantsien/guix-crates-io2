(define-module (crates-io qu ak quake_tui) #:use-module (crates-io))

(define-public crate-quake_tui-0.2.0 (c (n "quake_tui") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "quake_core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "00qzp3bdypgv1f86xfnddpyzqsbmn6kixvyi6qlszc8crb9896yf")))

(define-public crate-quake_tui-0.3.0 (c (n "quake_tui") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "quake_core") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0xaxm021xrksxm00zc1ag0y8dnsxs8hawlkqqjbfzn747j281d08")))

(define-public crate-quake_tui-0.4.0 (c (n "quake_tui") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "quake_core") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1s54ddy93r5n0spjblbbb6ckfx2mzpkxswkb8n34dyzam1pjpham")))

(define-public crate-quake_tui-0.5.0 (c (n "quake_tui") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "quake_core") (r "^0.5.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1kcsgqlk3r6ik9js5m3h97hra3yq9zdcj9aafh7flxbb371bifq8")))

