(define-module (crates-io qu ak quake_clientinfo) #:use-module (crates-io))

(define-public crate-quake_clientinfo-0.1.0 (c (n "quake_clientinfo") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "quake_infostring") (r "^0.1.0") (d #t) (k 0)))) (h "0n95afi2ghf1k4xpiccq6pixnw6cwp1qjxxsasf1k5lv4ws0xv69")))

(define-public crate-quake_clientinfo-0.2.0 (c (n "quake_clientinfo") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "quake_infostring") (r "^0.1.0") (d #t) (k 0)))) (h "0vag79qyf57gz97f1y37yk3rfhnnd8lvfg6ha12bn5fhrspx5465")))

