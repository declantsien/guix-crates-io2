(define-module (crates-io qu ak quake_wasm) #:use-module (crates-io))

(define-public crate-quake_wasm-0.4.0 (c (n "quake_wasm") (v "0.4.0") (d (list (d (n "quake_core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1fb653dbmr5ymh2wp8ik8drcmys0r55xwmfqq5h128bs2ivl2p78")))

