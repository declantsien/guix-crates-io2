(define-module (crates-io qu al qualify-derive) #:use-module (crates-io))

(define-public crate-qualify-derive-0.1.0 (c (n "qualify-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "03gs3hrajzwy5ysbacqjvhialzf7h0181q1jwaz6dsfx2c5lx8ny")))

(define-public crate-qualify-derive-0.1.1 (c (n "qualify-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "1xnj8by06mmf4x3qrg4cci2iwhjb1mj6haqgsbl9wnqq2rbfg5gc")))

(define-public crate-qualify-derive-0.1.2 (c (n "qualify-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "0hl782xms96dwhzk7ia3sfrx1h1fk91f2002mvbdjkkv80mvgyby")))

