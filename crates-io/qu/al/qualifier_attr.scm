(define-module (crates-io qu al qualifier_attr) #:use-module (crates-io))

(define-public crate-qualifier_attr-0.1.0 (c (n "qualifier_attr") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "07hqdnw3x1zrc9q07jn5scwycyd62qgvi4qzvc1r3162dyfc3y2h") (r "1.56.1")))

(define-public crate-qualifier_attr-0.1.1 (c (n "qualifier_attr") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0y14gz4ral44q0b4psm5gb6bj9sknjqcn78harrsvwzx3rasiij1") (r "1.56.1")))

(define-public crate-qualifier_attr-0.1.2 (c (n "qualifier_attr") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "11wmfnxmnm2zajq0vfajcdb72fm2yjhchzldn340gl7v5c6mdblf") (r "1.56.1")))

(define-public crate-qualifier_attr-0.1.3 (c (n "qualifier_attr") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "05jjjl3q6f7g1sdnl61kjc80h53rnbw63h39w7xhra3i4s3wyvsn") (r "1.56.1")))

(define-public crate-qualifier_attr-0.1.4 (c (n "qualifier_attr") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1b1qhshar5qz4vggvhs2mpbzmh725xb6jrx96y09sghijfpw02iz") (r "1.56.1")))

(define-public crate-qualifier_attr-0.1.5 (c (n "qualifier_attr") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0jnbkn2nglkn7ayyi7xdz5f9bxb78ixy1hz9ij4gnk6jg85hizgd") (r "1.56.1")))

(define-public crate-qualifier_attr-0.1.6 (c (n "qualifier_attr") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "078bc1f23bak69i3hjzbq8hacbbbhmcfyqqiv1pajipdjgwax5m6") (r "1.56.1")))

(define-public crate-qualifier_attr-0.2.0 (c (n "qualifier_attr") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1gz2m1982a6qxmnrsysg7hwrp8nbqvij7qrn56sr5c2bi5ff8ich") (f (quote (("legacy_attrs") ("default" "legacy_attrs")))) (r "1.56.1")))

(define-public crate-qualifier_attr-0.2.1 (c (n "qualifier_attr") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "11bzh97zv5jy5dfj3jb77a2d4iq8zp3rkq57v5gdzfq47jyihsv5") (f (quote (("legacy_attrs") ("default" "legacy_attrs")))) (r "1.56.1")))

(define-public crate-qualifier_attr-0.2.2 (c (n "qualifier_attr") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1clglb26yazyjs6vdalp9xqzgzyymlml525f7ivlvcpmfbp2ably") (f (quote (("legacy_attrs") ("default" "legacy_attrs")))) (r "1.56.1")))

