(define-module (crates-io qu al qualomat_schema) #:use-module (crates-io))

(define-public crate-qualomat_schema-0.1.0 (c (n "qualomat_schema") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "06dn1nlf6pvb61km59f1q29kwiks322r71v04i7zs54v0yapf8v1")))

