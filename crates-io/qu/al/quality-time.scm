(define-module (crates-io qu al quality-time) #:use-module (crates-io))

(define-public crate-quality-time-0.1.0 (c (n "quality-time") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "git-repository") (r "^0.33.0") (d #t) (k 0)) (d (n "rust-code-analysis") (r "^0.0.24") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1340znyb1kqb0kwxdc9p12zqnygahpplgd94x3qznsfcgy5yljld")))

