(define-module (crates-io qu ri quriust) #:use-module (crates-io))

(define-public crate-quriust-0.1.0 (c (n "quriust") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "105hfs7z6fshy1prasf4q9y2161cs2yyza4wlzcgg58j0cyj765h")))

(define-public crate-quriust-0.1.2 (c (n "quriust") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pfcmxc770f4qidrc53dkgzm19abwbiizwikpzqga32a9qp5zzqy")))

(define-public crate-quriust-0.1.3 (c (n "quriust") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "147ibnqz0qhfmird43x70jh4jy2sw25vhl9sjajfqahk0xap3r69")))

(define-public crate-quriust-0.1.4 (c (n "quriust") (v "0.1.4") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0j68p16z22awg0cs69hi9zjij4b6s4plm6is5skidjxwhn93m3jv")))

(define-public crate-quriust-0.1.5 (c (n "quriust") (v "0.1.5") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1idmrawca91zq43ghxxkk492ir6ljsdzz51cij5q9yz8sdabfx2h")))

(define-public crate-quriust-0.1.6 (c (n "quriust") (v "0.1.6") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18lag84fwml2vcsxsmy1fqv1hxz7j7gj854632khwxkc27mncnf5")))

(define-public crate-quriust-0.1.7 (c (n "quriust") (v "0.1.7") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xnvwchmkz3clww7j7z24fyi0x0pijf5mlm7a1rcxyysn3zwhims")))

(define-public crate-quriust-0.1.8 (c (n "quriust") (v "0.1.8") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qfnm53zxxpdv77i246anqabsgr9ad2ss3na5nc3zngbi6h76hri")))

(define-public crate-quriust-0.1.9 (c (n "quriust") (v "0.1.9") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05wmd9201l9cvb15d3dh0j9rdrd0fl6lwlyfdccmprrcaprmcg81")))

(define-public crate-quriust-0.2.0 (c (n "quriust") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "048rwd560hdcmbdcrgky7c8bz48z2r82ic09x9w5i2kxbdc2jjcq")))

