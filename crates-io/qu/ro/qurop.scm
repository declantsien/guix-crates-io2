(define-module (crates-io qu ro qurop) #:use-module (crates-io))

(define-public crate-qurop-0.1.0 (c (n "qurop") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "confy") (r "^0.5") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("std" "env-filter"))) (d #t) (k 0)) (d (n "x11rb") (r "^0.12") (d #t) (k 0)))) (h "1fb214g82l144mggga8w21wf93wg5k7b8p3kk0nj5viqzh7igws2")))

