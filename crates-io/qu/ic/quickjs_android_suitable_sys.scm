(define-module (crates-io qu ic quickjs_android_suitable_sys) #:use-module (crates-io))

(define-public crate-quickjs_android_suitable_sys-0.1.0 (c (n "quickjs_android_suitable_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18fp46sx97wh7z1n80bs2z48fivibm01q2vivgg9mx8kixky4k55") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.1.1 (c (n "quickjs_android_suitable_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1bl7jjpvy7rbg70j6fs138kzpraaza0c55h61sdc3fws1grjzdnh") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.1 (c (n "quickjs_android_suitable_sys") (v "0.20201108.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0m0h60fcacx461xlxk20nxpg1ci6hm5qbq92i2h4f8970bjy4zzx") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.2 (c (n "quickjs_android_suitable_sys") (v "0.20201108.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1yfaham9v82f75x8i9pzqckadp4z2jrca2vhwrm3i882xgr4gcpy") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.3 (c (n "quickjs_android_suitable_sys") (v "0.20201108.3") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "10d2gjpgpmqcsa3nf1r2cipd8hnq6ic8x5ak7wwc7fpwq5qkm0vq") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.4 (c (n "quickjs_android_suitable_sys") (v "0.20201108.4") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0r5x27ybfr8428b8iha8cgc51nlnwdfz7frb69lbwpkrvxxhil9q") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.5 (c (n "quickjs_android_suitable_sys") (v "0.20201108.5") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sbfyjn10q7jsg9k3spcpyvxdigkyxf9rdr79xkwx8m2glz096a5") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.6 (c (n "quickjs_android_suitable_sys") (v "0.20201108.6") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1yy07cxcnm666mhxhlj5sjhxi0n37ry5z2rx3fqh6m9c7ycn2307") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.7 (c (n "quickjs_android_suitable_sys") (v "0.20201108.7") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1radscnbyhbcmy0lbp71aldpp0vysjvmr6m7k3iqjgikhsazf48d") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.8 (c (n "quickjs_android_suitable_sys") (v "0.20201108.8") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "17hb9c3gfrlhlmwf4kxd6py1y3dygr0378g1zdggv0v95nifzkwn") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.9 (c (n "quickjs_android_suitable_sys") (v "0.20201108.9") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0q23awrj4jl14mllswl4paa8ab6rlgpj2i6abhmzh41mnn1804zl") (f (quote (("dump_leaks") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.10 (c (n "quickjs_android_suitable_sys") (v "0.20201108.10") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0m48z3q92rq9cgdwki4l5l3is08gg0zxawn7flc0q9ppfmz0fvd7") (f (quote (("dump_leaks") ("dump_free") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.11 (c (n "quickjs_android_suitable_sys") (v "0.20201108.11") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0kfjbjcdmdx4dfi6z8pyny7qi46flqkh6hzz961wmxd2bn25zypj") (f (quote (("dump_leaks") ("dump_free") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20201108.12 (c (n "quickjs_android_suitable_sys") (v "0.20201108.12") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0clkk38ijv6sn42dh21kr7d9wv437d8vmc2ildail8si7q4jzbbj") (f (quote (("dump_leaks") ("dump_free") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20220306.0 (c (n "quickjs_android_suitable_sys") (v "0.20220306.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "17fpccv91y3bbz83kdgf90ndcdi5cinm17xpwm0ffs9aq5xwhsf6") (f (quote (("dump_leaks") ("dump_free") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20220306.1 (c (n "quickjs_android_suitable_sys") (v "0.20220306.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04n0xbwkbxrdz2r20fq291dxv80vk594yyv579785wv58f8nhbpl") (f (quote (("dump_leaks") ("dump_free") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20220306.2 (c (n "quickjs_android_suitable_sys") (v "0.20220306.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0lwpwmc4lw9kqab2dr1nnfapk3nrnxy17if4pb3161s5nzdrivvx") (f (quote (("dump_leaks") ("dump_free") ("default" "bignum") ("bignum"))))))

(define-public crate-quickjs_android_suitable_sys-0.20220306.3 (c (n "quickjs_android_suitable_sys") (v "0.20220306.3") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0p1q0wck5cw9pn9sbmh5jxls02zx1lbmqd6r5bazd10g9bkq7iqy") (f (quote (("dump_leaks") ("dump_free") ("default" "bignum") ("bignum"))))))

