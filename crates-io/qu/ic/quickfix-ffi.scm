(define-module (crates-io qu ic quickfix-ffi) #:use-module (crates-io))

(define-public crate-quickfix-ffi-0.1.0 (c (n "quickfix-ffi") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)))) (h "02xq83cl9qbz1pslz907chazj0h7biqa1x08s1rqqwxah2rcq8yk") (f (quote (("default") ("build-with-postgres") ("build-with-mysql")))) (r "1.70.0")))

(define-public crate-quickfix-ffi-0.1.1 (c (n "quickfix-ffi") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)))) (h "0zf0mbjxf51mir278pzv5ym96c9zbkh6apsz0chih526i2mdx6hk") (f (quote (("default") ("build-with-postgres") ("build-with-mysql")))) (r "1.70.0")))

(define-public crate-quickfix-ffi-0.1.2 (c (n "quickfix-ffi") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)))) (h "1nwicq2rvya38srya4zkn0gzgg2cph4abg901395cqmh7yc929dg") (f (quote (("default") ("build-with-postgres") ("build-with-mysql")))) (y #t) (r "1.70.0")))

(define-public crate-quickfix-ffi-0.1.3 (c (n "quickfix-ffi") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)))) (h "1widp7i52bi7ln4709y5iklliv2ssl0wq3d7cd2y7pc34ph7k0mg") (f (quote (("default") ("build-with-postgres") ("build-with-mysql")))) (r "1.70.0")))

(define-public crate-quickfix-ffi-0.1.4 (c (n "quickfix-ffi") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)))) (h "0hpz0qimfh9ddcbz0rjjk2ff7d72is9m8098v1nkk8v1ig3gg5f1") (f (quote (("default") ("build-with-postgres") ("build-with-mysql")))) (r "1.70.0")))

