(define-module (crates-io qu ic quickapi) #:use-module (crates-io))

(define-public crate-quickapi-0.0.0 (c (n "quickapi") (v "0.0.0") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1w1fvl69ks7aqdiwdd8m49r5150cjpgic08fx2b5wmdhhjzr23ac")))

