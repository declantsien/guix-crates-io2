(define-module (crates-io qu ic quickwit-cluster) #:use-module (crates-io))

(define-public crate-quickwit-cluster-0.1.0 (c (n "quickwit-cluster") (v "0.1.0") (h "08zm94x5zsnz3xi4a19f54y1c5xa265yk0s7fs3hxrfcaaj1qgpq")))

(define-public crate-quickwit-cluster-0.3.0 (c (n "quickwit-cluster") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chitchat") (r "^0.4") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "quickwit-common") (r "^0.3.0") (d #t) (k 0)) (d (n "quickwit-config") (r "^0.3.0") (d #t) (k 0)) (d (n "quickwit-proto") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0zh5yks6hgwgawkivw8gdbhg32nn9qql72ha933pb0h927b0h9jq")))

