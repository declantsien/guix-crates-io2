(define-module (crates-io qu ic quickscope) #:use-module (crates-io))

(define-public crate-quickscope-0.1.0 (c (n "quickscope") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "14q64n2swh1r1if3xpm3asw5sqhszqhfrf3m5fving848brcnn9w")))

(define-public crate-quickscope-0.1.1 (c (n "quickscope") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "0cv5rhsanan974gaa7aan1558pxrz2s0jb12nciqn81zxmv9izxg")))

(define-public crate-quickscope-0.1.2 (c (n "quickscope") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "09szfi6mglcpxprgkffqlllfgwkndh8mpg9h8ja7d5d17bwjfc5y")))

(define-public crate-quickscope-0.1.3 (c (n "quickscope") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "0zikp91v5m7ix9p8wwmvj9p208vlxdh4z6ynjd34if4vm59cp1fq")))

(define-public crate-quickscope-0.1.4 (c (n "quickscope") (v "0.1.4") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "1aagndlswrsjcvrnn5w1a679lyya64x51v475p8j9839vsln2y93")))

(define-public crate-quickscope-0.1.5 (c (n "quickscope") (v "0.1.5") (d (list (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "1wb2bz290p65sdii648q1layi32khav99kfjl9brxhy06mpvxdad")))

(define-public crate-quickscope-0.1.6 (c (n "quickscope") (v "0.1.6") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1rlbhmsapwh0x4jbad51qfas4n7l6fnlv11029bpps62ggl7r5an")))

(define-public crate-quickscope-0.2.0 (c (n "quickscope") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "0srv37b93pz8jair63wafjrynnpvdlms0f4kry4hb18k7vybqiqx")))

