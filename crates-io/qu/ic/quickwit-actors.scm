(define-module (crates-io qu ic quickwit-actors) #:use-module (crates-io))

(define-public crate-quickwit-actors-0.1.0 (c (n "quickwit-actors") (v "0.1.0") (h "0rclvhg29dd848jq4gkhj8bdpaqbrzaid7gq75nikms7a0ryb6pa")))

(define-public crate-quickwit-actors-0.3.0 (c (n "quickwit-actors") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "quickwit-common") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "0ihfc9y1hd0n08k2cyvfmdh11h17r3jscz1xfki8v54bl3d6lbsx")))

