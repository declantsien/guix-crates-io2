(define-module (crates-io qu ic quickexif) #:use-module (crates-io))

(define-public crate-quickexif-0.1.0 (c (n "quickexif") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18s3mwkrvwcph9rmdilmc5hx1mp5gy78n2km77a0x569cij67amx")))

(define-public crate-quickexif-0.1.1 (c (n "quickexif") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gpx5ryg3s0fjqs3l51agcxfmndkmg1hjnyg7xny5qrzg5mx6ljh")))

(define-public crate-quickexif-0.1.2 (c (n "quickexif") (v "0.1.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bh5v1viglr35sfi78n4gnpxwj3pydnjmqnahq307l7fxhhfd6g4")))

(define-public crate-quickexif-0.1.3 (c (n "quickexif") (v "0.1.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0kylbi4811slgc71dw1rgxbil692c804c703k2gasl276mwa0ifd")))

(define-public crate-quickexif-0.1.4 (c (n "quickexif") (v "0.1.4") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pifql5sfrib2mlrjysj5cvbz1iqip2gslq9svpkn2jlllbzgla8")))

(define-public crate-quickexif-0.1.5 (c (n "quickexif") (v "0.1.5") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "139iahrb67v0gvjv946blk1xywh9b2c0zkknnad2bsvra9agiq80")))

(define-public crate-quickexif-0.2.0-alpha.2 (c (n "quickexif") (v "0.2.0-alpha.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "erreport") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bvznmavs9jp11kqlyqcca2m9klaii8zrwbl16bia5nij38m5ikc")))

(define-public crate-quickexif-0.2.0-alpha.3 (c (n "quickexif") (v "0.2.0-alpha.3") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "erreport") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1py0lnmfd07wmc539j7idi8nbimx0icw6clc8j9jly7bw2xplgsr")))

(define-public crate-quickexif-0.2.0-alpha.4 (c (n "quickexif") (v "0.2.0-alpha.4") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "erreport") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "050fdalczxhw6q06qx5g3vm4v029wljm78vnfyzd2x9xwfd4mzsk")))

(define-public crate-quickexif-0.2.0-alpha.5 (c (n "quickexif") (v "0.2.0-alpha.5") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "erreport") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18855k0v99js7cn6l011x8j4xljih8m4ng0ix775r1cjidmpq09g")))

(define-public crate-quickexif-0.2.0-alpha.6 (c (n "quickexif") (v "0.2.0-alpha.6") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "erreport") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zckdsc2ql71yh7b0i24dshbmhfhmglyrgiblidyjdng9qc1cdpr")))

(define-public crate-quickexif-0.2.0-alpha.7 (c (n "quickexif") (v "0.2.0-alpha.7") (d (list (d (n "erreport") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03lrbn2rk38xnncqfk533d02bd213as9qhbxxnmdqfnk682gqjkx")))

(define-public crate-quickexif-0.2.0-alpha.8 (c (n "quickexif") (v "0.2.0-alpha.8") (d (list (d (n "erreport") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cizl9d9vixwvzd8faf0dwiriny0njphig464p2r7ylyiln67qp8")))

(define-public crate-quickexif-0.2.0-alpha.9 (c (n "quickexif") (v "0.2.0-alpha.9") (d (list (d (n "erreport") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mlca6zsvv14ak24hknv1qv2riz322v4jwhf447hn2fn9al2pf0b")))

