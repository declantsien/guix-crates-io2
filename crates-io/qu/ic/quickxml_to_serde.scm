(define-module (crates-io qu ic quickxml_to_serde) #:use-module (crates-io))

(define-public crate-quickxml_to_serde-0.1.0 (c (n "quickxml_to_serde") (v "0.1.0") (d (list (d (n "minidom") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n8ii3aanqg4a2s6dq6zfxnisjsjrjg3gapzk2hrjiv1qlb1dsp7")))

(define-public crate-quickxml_to_serde-0.2.0 (c (n "quickxml_to_serde") (v "0.2.0") (d (list (d (n "minidom") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pcf6lv0hbr331g0v1jcgay6l1qkqa3jgkxsp09hk3rhhxfa6xyx")))

(define-public crate-quickxml_to_serde-0.2.1 (c (n "quickxml_to_serde") (v "0.2.1") (d (list (d (n "minidom") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b02v1kajxg33s778x1izdslfxj5di0cyp4s1q7p4xfs4ygdjz9k")))

(define-public crate-quickxml_to_serde-0.3.0 (c (n "quickxml_to_serde") (v "0.3.0") (d (list (d (n "minidom") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a3gkq8cq6v4gsal42aw4m9qv7vgz3jibbxfrwm30f4bmvv0msw6")))

(define-public crate-quickxml_to_serde-0.3.1 (c (n "quickxml_to_serde") (v "0.3.1") (d (list (d (n "minidom") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16vprb3zynyv091x8z23cmxvi8jsc6cyw0a9kz8hm9276klf43qi")))

(define-public crate-quickxml_to_serde-0.3.2 (c (n "quickxml_to_serde") (v "0.3.2") (d (list (d (n "minidom") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ds5isifa55bdc50ff7gz0sqia71w3hz1mr1mqashy700q57kjvz")))

(define-public crate-quickxml_to_serde-0.4.1 (c (n "quickxml_to_serde") (v "0.4.1") (d (list (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r8mby51nrfjkc5n18pqfl5kn0ywc6hdczv56c9kdhb7kxjnb7nk")))

(define-public crate-quickxml_to_serde-0.4.3 (c (n "quickxml_to_serde") (v "0.4.3") (d (list (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03j0rmgmm0ldm06vcbd4nwcbckz4ip2wqrv1yxv93c9wx7vbl2l4") (f (quote (("json_types"))))))

(define-public crate-quickxml_to_serde-0.5.0 (c (n "quickxml_to_serde") (v "0.5.0") (d (list (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i726pdcrw098cnch1rrs5hvvvgfhf82hi1lyrrgv02lnc953wr6") (f (quote (("json_types"))))))

(define-public crate-quickxml_to_serde-0.6.0 (c (n "quickxml_to_serde") (v "0.6.0") (d (list (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zj07hy3av98lxy0czbp5vh3n2lfc86y262jmkv6yd8bl33hasr8") (f (quote (("regex_path" "json_types") ("json_types"))))))

