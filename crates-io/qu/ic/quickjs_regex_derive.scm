(define-module (crates-io qu ic quickjs_regex_derive) #:use-module (crates-io))

(define-public crate-quickjs_regex_derive-0.1.0 (c (n "quickjs_regex_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quickjs_regex") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0dkzjhsmn27yqbgdcgkqv846npwpgdkg604j3jl9njdccbbmld4a")))

(define-public crate-quickjs_regex_derive-0.1.1 (c (n "quickjs_regex_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quickjs_regex") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1mcngqk11l46lxcf26mhjvxz2pyz3xcbwff1p1xkb2ffqv4l5b9f")))

(define-public crate-quickjs_regex_derive-0.1.2 (c (n "quickjs_regex_derive") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quickjs_regex_backend") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1vf6l08rcyczvj136rirb7hwhkpcwbz6armwbwmdgzpm8194yfl9")))

