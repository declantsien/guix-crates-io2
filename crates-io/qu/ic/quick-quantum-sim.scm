(define-module (crates-io qu ic quick-quantum-sim) #:use-module (crates-io))

(define-public crate-quick-quantum-sim-0.2.0 (c (n "quick-quantum-sim") (v "0.2.0") (d (list (d (n "mimalloc") (r "^0.1.27") (o #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("rayon" "matrixmultiply-threading"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0668nacmyxvjfhy1xd2npbank34g71gyzyiqmbm03gcikb31g3kf") (f (quote (("default" "mimalloc")))) (y #t)))

(define-public crate-quick-quantum-sim-0.2.1 (c (n "quick-quantum-sim") (v "0.2.1") (d (list (d (n "mimalloc") (r "^0.1.27") (o #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("rayon" "matrixmultiply-threading"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "06wn5rim66kc9fad9fzc1p4kflhlasbdalf7ijs0y0ymshwp67hd") (f (quote (("default" "mimalloc")))) (y #t)))

(define-public crate-quick-quantum-sim-0.2.2 (c (n "quick-quantum-sim") (v "0.2.2") (h "0bcf4zbq91rjqlkab9h222lc7d9257bxj448ynj1yz2a5ay9y2gg") (y #t)))

