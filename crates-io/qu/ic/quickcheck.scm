(define-module (crates-io qu ic quickcheck) #:use-module (crates-io))

(define-public crate-quickcheck-0.1.2 (c (n "quickcheck") (v "0.1.2") (h "127s1fbbjynymw3slzxm495msscgfplbf3iclrhyz4awgayncff4")))

(define-public crate-quickcheck-0.1.3 (c (n "quickcheck") (v "0.1.3") (h "1a48x2yz0351ynal5b6x9g8vikqc0jbrsgwlpb73r97qrr85jz9k")))

(define-public crate-quickcheck-0.1.4 (c (n "quickcheck") (v "0.1.4") (h "0wkpig9b18n9carydrsz1nf2z1ap4584m434wqznvarl58vjr6iy")))

(define-public crate-quickcheck-0.1.5 (c (n "quickcheck") (v "0.1.5") (h "02phklm96fwq6kx6cxn1vwwmyi03y5h2lifcq7if6n5z26axbswj")))

(define-public crate-quickcheck-0.1.6 (c (n "quickcheck") (v "0.1.6") (h "1zx9pvnd04qab3z57mf91dj8zc4bfchmx8a0ljsxja1n4wfjh2p6")))

(define-public crate-quickcheck-0.1.7 (c (n "quickcheck") (v "0.1.7") (h "1gzw348b9w7pjm5w7d89584maqz3aap7gcc1pihx2kvll2rp1mak")))

(define-public crate-quickcheck-0.1.8 (c (n "quickcheck") (v "0.1.8") (h "0082kbr1sd1yav3zw5qbv0qjks931r3rgxl4hfhja2prv60cpbai")))

(define-public crate-quickcheck-0.1.11 (c (n "quickcheck") (v "0.1.11") (d (list (d (n "collect") (r "*") (d #t) (k 0)))) (h "1f3grylc354wv26x0b2lljffmizinwfi28d28l3vdabfq10bc7rb")))

(define-public crate-quickcheck-0.1.19 (c (n "quickcheck") (v "0.1.19") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)))) (h "1r1dkj230g2bfwld02aaqxgzknyhqi45acbd1nh55lszwckazsjv")))

(define-public crate-quickcheck-0.1.20 (c (n "quickcheck") (v "0.1.20") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)))) (h "15gaijl8f2g78cfb3iqp1cwvvja9bj6zklpjd163zxn16sc2jfv3")))

(define-public crate-quickcheck-0.1.21 (c (n "quickcheck") (v "0.1.21") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)))) (h "1hp7mqnbx1a874nhzc4z94i1x1cawhwbpvsy9v1zla6njak08sx9")))

(define-public crate-quickcheck-0.1.22 (c (n "quickcheck") (v "0.1.22") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)))) (h "0qr1crzk8iygppa142g2yaj9m4pqi6pq88ilvk28g8y921ncbgss")))

(define-public crate-quickcheck-0.1.23 (c (n "quickcheck") (v "0.1.23") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)))) (h "03s30y5nz645dp755ncpksmxf8q2lab36lm11l1f6n61m8arii43")))

(define-public crate-quickcheck-0.1.24 (c (n "quickcheck") (v "0.1.24") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0mxk0v193y1ss3xyprqizrr773ixjbrkgzd8q1f7vvmkb0ir44z0")))

(define-public crate-quickcheck-0.1.25 (c (n "quickcheck") (v "0.1.25") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1b2jq18qpnh4v5lhmgjv9hp1gnqy4snwmirx7qrb3lfnrfnkyl3y")))

(define-public crate-quickcheck-0.1.26 (c (n "quickcheck") (v "0.1.26") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0k3wvl9v7n37cyzgax3h7d2ql413gzgih2pq1ydjli1jsv4jrc5v")))

(define-public crate-quickcheck-0.1.27 (c (n "quickcheck") (v "0.1.27") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1fv53sp7h8zjcvkmpy0h8n94kaa3f6wxr5yn9d2xh87s8f8d5nlc")))

(define-public crate-quickcheck-0.1.28 (c (n "quickcheck") (v "0.1.28") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0if8db71725pj3jlx849kh5j1an92azfl7mfylj42hv0vlw4mb5k")))

(define-public crate-quickcheck-0.1.29 (c (n "quickcheck") (v "0.1.29") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "16r0nj61p01px3v1pbaxv22yspw0l8jflk9b38si1d5rgvw00yvm") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.30 (c (n "quickcheck") (v "0.1.30") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1wh2y847mpiiz0jdv25x534r4wvspidhyma211s1aks9d1dzxz7y") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.31 (c (n "quickcheck") (v "0.1.31") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0cgbkqgznk2zdin7m9c4kshm8kdpbliv1wjpvhw1i48mc5cljqa4") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.32 (c (n "quickcheck") (v "0.1.32") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0swzgxlcs4fb16hcyd8bnlpz4053frszn4spx0zid1s37gag2jfv") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.33 (c (n "quickcheck") (v "0.1.33") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "12h54fi7lylflfb0sqkmhrhyq67s050630ygqz04f8shaip3hkhs") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.35 (c (n "quickcheck") (v "0.1.35") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0b1g2a274q8khrfqzclk2a0xhwh33ziix0fqsfm7g7lnp0ss5k7q") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.36 (c (n "quickcheck") (v "0.1.36") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1c32x8zwjazzzfmfv91jxpyw5fridkcd968qxn960mk7v8ixnr2y") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.37 (c (n "quickcheck") (v "0.1.37") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "06p4qxvmgd368l8yq8fk73i44h54h2pdcxkxlb95idyl5lx2npbk") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.38 (c (n "quickcheck") (v "0.1.38") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1iq47ky0v0l4661srakm6ysl6pyxg5m62w7crd3inv03qlz3jspm") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.39 (c (n "quickcheck") (v "0.1.39") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1h2dmgqdnf17yafxsqyhcfmjnb8pbgs70crka8ynsnznb1kf6zwx") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.40 (c (n "quickcheck") (v "0.1.40") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1w2svpi28z3bbikk9jk0jkl7q7mcchab3ccl6ag6b3qg78bqr57c") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.1.41 (c (n "quickcheck") (v "0.1.41") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1hfs7472b44i21py3n83vas2m858hl1rqadi4wxn1nx8qkzad2yg") (f (quote (("collect_impls" "collect")))) (y #t)))

(define-public crate-quickcheck-0.2.0 (c (n "quickcheck") (v "0.2.0") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "16zkrb756famzmp09piwsfq5l659lihlw411913j7r7fisc6ffr1") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.1 (c (n "quickcheck") (v "0.2.1") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "00bc2bapxqnrcvd3j1ddnr7hv1qhn2ppdhq32b8cmnd2wmikpdzr") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.2 (c (n "quickcheck") (v "0.2.2") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0f1hw3zm62r4fv7r258c9hmpd05zg1c2zhpb5n83dr9qy3ilcnfy") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.3 (c (n "quickcheck") (v "0.2.3") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0zaizc6m97rpy5f8wvs83kdga9i6hdyqpybar7s799bahd20yd4w") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.4 (c (n "quickcheck") (v "0.2.4") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0gyv5yimsy3s72bdrssf4ll576jc4vxqb0v3cij65yr6bhplg0pb") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.5 (c (n "quickcheck") (v "0.2.5") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "17m9m7wdzhdizi6pgkizqlfjv6c12wnqaplm2n54sfw1xrh2y806") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.6 (c (n "quickcheck") (v "0.2.6") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1pcdc2dj81ns7q77wi47x8rq94ak1195vm8by5n644dx1mgc2sxp") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.8 (c (n "quickcheck") (v "0.2.8") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1b568grwi83zl7s58pgd6zdfdixxxm36f0k1b2zgdkjpkg8kdpzn") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.9 (c (n "quickcheck") (v "0.2.9") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "05gzx36r6l7ybm7pmrpx8sknsyz2lclf1xraf1f17mf4skrci4ag") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.10 (c (n "quickcheck") (v "0.2.10") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0ndycfyz4f1v1bsrjqpa4injsvhkqicfdydwi5bhy67a5iaibzcj") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.11 (c (n "quickcheck") (v "0.2.11") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0mns1fbpyy7j6250ma08fqf6cswjmimbqhhxrlnlrxcixr4m2jfg") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.12 (c (n "quickcheck") (v "0.2.12") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "02xvzkys1z5jzjfy9m49b4h11sdag954s6ffw6rhq8hagg95mjvh") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.13 (c (n "quickcheck") (v "0.2.13") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1rvgps2izipmn89wx0i3nzs969vxzgzgy7zfc8njs2p13dk3pm06") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.14 (c (n "quickcheck") (v "0.2.14") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "024pw1b902hk1rr4rzhdammslm9m9w28jiy3yap4d0g42hm3sypp") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.15 (c (n "quickcheck") (v "0.2.15") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "09rjddh61g9q5wnr6g8g24h3h1d2hz8dsrijjg33gkmis538a5k8") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.16 (c (n "quickcheck") (v "0.2.16") (d (list (d (n "collect") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "19fb3pc70084cx74km9jpjip8zgz6s5sarm46hk9akh26jd1qdzk") (f (quote (("collect_impls" "collect"))))))

(define-public crate-quickcheck-0.2.17 (c (n "quickcheck") (v "0.2.17") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0b4i33a4alf68wc5h4ffd7iz4jfh41qsbawsqzliay6mll7abn3j")))

(define-public crate-quickcheck-0.2.18 (c (n "quickcheck") (v "0.2.18") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0x6d0l6g3px2r7005bl7shrp8gglsqkra57wbf7mngs2vpqcmv93")))

(define-public crate-quickcheck-0.2.19 (c (n "quickcheck") (v "0.2.19") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1f6j6x2vgmyd655fmrg8iaz9xh6bi0m9v1l9wa4ma5zl98v7xm6j")))

(define-public crate-quickcheck-0.2.20 (c (n "quickcheck") (v "0.2.20") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "017wp6dii0p8kdh02w7zvn1wmhd03linq5yv4hdp9q7lm8n7jl9w")))

(define-public crate-quickcheck-0.2.21 (c (n "quickcheck") (v "0.2.21") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0g7c2p68lgcmgb6kjqi4y89fg3d7pcli1lnrrl01h7cpk4jk4m0r")))

(define-public crate-quickcheck-0.2.22 (c (n "quickcheck") (v "0.2.22") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1z4g8razlnjm9v7vchmxmhjyl3p20jvq4h4cpv34gid14xhncwk0")))

(define-public crate-quickcheck-0.2.23 (c (n "quickcheck") (v "0.2.23") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1fkvlc7zisam0q2dpzv0inrlmlbzfsjwal0mz5iksp9jq033k1lx")))

(define-public crate-quickcheck-0.2.24 (c (n "quickcheck") (v "0.2.24") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0sszv43kvgnlmiii90dv8snl3cacc42dhrm6lw6v6hibs86gvh6s")))

(define-public crate-quickcheck-0.2.25 (c (n "quickcheck") (v "0.2.25") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0k9d8i068xcjwxi7f1b8zwlxh7qb9fal5p97q6ajdsgvycxy3b3c") (f (quote (("unstable"))))))

(define-public crate-quickcheck-0.2.26 (c (n "quickcheck") (v "0.2.26") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0nmybd8l4jry2v98zvsgigghhrdvry6shv8hhbsdjsskk4cm9hni") (f (quote (("unstable"))))))

(define-public crate-quickcheck-0.2.27 (c (n "quickcheck") (v "0.2.27") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1vb4acppaavlnchzc1jmn5wlkgir9x9gmhgp97bavyxxqxgsg1nh") (f (quote (("unstable"))))))

(define-public crate-quickcheck-0.3.0 (c (n "quickcheck") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ngrjfjrmbkl7k0m9fvxv8x30rl8b975dhsylhai1fvd615n82sr") (f (quote (("unstable"))))))

(define-public crate-quickcheck-0.3.1 (c (n "quickcheck") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1jkl4l7lmyfrcpc78hn6d0vgjcp1hjlbz9i8l7jskrr3x3aljipf") (f (quote (("unstable"))))))

(define-public crate-quickcheck-0.3.2 (c (n "quickcheck") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "01a6s6lmnjld9lahbl54qp7h7x2hnkkzhcyr2gdhbk460sj3scqb") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("default" "use_logging"))))))

(define-public crate-quickcheck-0.4.0 (c (n "quickcheck") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19lcw7ckp1mw2k81wvwp2vijxlipl9ngyvgs3zqx3ckcjchppw9j") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("default" "use_logging"))))))

(define-public crate-quickcheck-0.4.1 (c (n "quickcheck") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "01hligcv1h4pvc8ykch65qjzi7jgcq2s462v69j27slc84fl3hh2") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("default" "use_logging"))))))

(define-public crate-quickcheck-0.4.2 (c (n "quickcheck") (v "0.4.2") (d (list (d (n "env_logger") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16symxywrj4qh431wjhl7zvzv4n9hp111dmn9qnj1kqq830r9wiy") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("default" "use_logging")))) (y #t)))

(define-public crate-quickcheck-0.5.0 (c (n "quickcheck") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1jzm1ygfbn4igaq14b9nipc8yvsn6c8panpgd1qiy5r2insjllyd") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("default" "use_logging"))))))

(define-public crate-quickcheck-0.5.1 (c (n "quickcheck") (v "0.5.1") (d (list (d (n "env_logger") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0bwkjp5pg1l9fxf2j7m2b1yxyzjq5y91w6c208142v3680k165r9") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("i128" "rand/i128_support") ("default" "use_logging")))) (y #t)))

(define-public crate-quickcheck-0.6.0 (c (n "quickcheck") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0x2gna5g8ci593fvzw8csw6h7jv5zvgmag4s9cffn1ma7l6ldx0k") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("i128" "rand/i128_support") ("default" "use_logging"))))))

(define-public crate-quickcheck-0.6.1 (c (n "quickcheck") (v "0.6.1") (d (list (d (n "env_logger") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0c700jmlhc35nmcmgpwmnlxzpcgqbkfn0xy5cf2jxdqikb0a5k8m") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("i128" "rand/i128_support") ("default" "use_logging"))))))

(define-public crate-quickcheck-0.6.2 (c (n "quickcheck") (v "0.6.2") (d (list (d (n "env_logger") (r "^0.5") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1dyazm2fcq0v9fscq1a7597zsvdl9f0j8c2bfj1jm2nlzz2sn6y0") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("i128" "rand/i128_support") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.7.0 (c (n "quickcheck") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.5") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.2.1") (d #t) (k 0)))) (h "1f825nrs29sqh9hkmk8y6lbg0cv5s6a20rsfiw4f9v3y2srqzvgy") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("i128" "rand/i128_support") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.7.1 (c (n "quickcheck") (v "0.7.1") (d (list (d (n "env_logger") (r "^0.5") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.2.1") (d #t) (k 0)))) (h "0dys540fdyz61gf84phlspbag3bvs91p977fd1n5nf8gc6mih0dp") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("i128" "rand/i128_support") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.7.2 (c (n "quickcheck") (v "0.7.2") (d (list (d (n "env_logger") (r "^0.5") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.2.1") (d #t) (k 0)))) (h "05pqzja6fwdyrs1za5vmxb9ifb993knmpdsrs1fs2wyz9qz7slyl") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("i128" "rand/i128_support") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.8.0 (c (n "quickcheck") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.5") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (d #t) (k 0)))) (h "085pcawcnpwgg3m24kd41g0v4i8py0xhjgm5v6n93q4nywrzcsfx") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.8.1 (c (n "quickcheck") (v "0.8.1") (d (list (d (n "env_logger") (r "^0.5") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.0") (d #t) (k 0)))) (h "1j55v55xdy8wwh8vjxh7a9dhi2h1abwid1w2dxd4ncrdds1j9kwv") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.8.2 (c (n "quickcheck") (v "0.8.2") (d (list (d (n "env_logger") (r "^0.6.0") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.4.0") (d #t) (k 0)))) (h "1p0p1vvyxy99m2prcxkb94yxrymcyn5pfqph3gvyx3s215aaws1m") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.8.3 (c (n "quickcheck") (v "0.8.3") (d (list (d (n "env_logger") (r "^0.6.0") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.4.0") (d #t) (k 0)))) (h "12ps1jrr5a8z13nkpniqhap8f9v9va26swlx3b9rhywjg9npnild") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.8.4 (c (n "quickcheck") (v "0.8.4") (d (list (d (n "env_logger") (r "^0.6.0") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.4.0") (d #t) (k 0)))) (h "1pxgmsfgbpd048ki9ci3yd0bgk44dja7vi0y08wr6n6kyhis51sf") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.8.5 (c (n "quickcheck") (v "0.8.5") (d (list (d (n "env_logger") (r "^0.6.0") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.4.0") (d #t) (k 0)))) (h "0mkl4wnvvjk4m32aq3an4ayfyvnmbxnzcybfm7n3fbsndb1xjdcw") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.9.0 (c (n "quickcheck") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.6.0") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0qi67dg7cf50i23ac7n5qhg4vhhsm6xznhpl2wsqv86s5x551jnm") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.9.1 (c (n "quickcheck") (v "0.9.1") (d (list (d (n "env_logger") (r "^0.7.0") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "1cdm35la1y5azs2p1dr9n64vzqxf36xdndah6q9r3yxwyg4ikng8") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-0.9.2 (c (n "quickcheck") (v "0.9.2") (d (list (d (n "env_logger") (r "^0.7.0") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0pwl7j21wmf843kpa9gr0byb40hg975ghjrwp0yxcym99bkq6j54") (f (quote (("use_logging" "log" "env_logger") ("unstable") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-1.0.0 (c (n "quickcheck") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.8.2") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng" "std"))) (k 0)))) (h "1rpqx2s4z3jkpa15n23brk757zlv4k56khrvgyrrg0nwdffm1ivh") (f (quote (("use_logging" "log" "env_logger") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-1.0.1 (c (n "quickcheck") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.8.2") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng"))) (k 0)))) (h "1j67v8rq6rw7dpi9a6ipv90fj8r2d5wnd3bqzjmxwnzs4sr0ih7g") (f (quote (("use_logging" "log" "env_logger") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-1.0.2 (c (n "quickcheck") (v "1.0.2") (d (list (d (n "env_logger") (r "^0.8.2") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng"))) (k 0)))) (h "1qv16kwi4x32k0ybmfywmxcl017rvd5kl65cp170wdqdfx30bq83") (f (quote (("use_logging" "log" "env_logger") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

(define-public crate-quickcheck-1.0.3 (c (n "quickcheck") (v "1.0.3") (d (list (d (n "env_logger") (r "^0.8.2") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng"))) (k 0)))) (h "1mjhkfqwrb8mdyxdqr4zzbj1rm5dfx25n9zcc25lb6fxwiw673sq") (f (quote (("use_logging" "log" "env_logger") ("regex" "env_logger/regex") ("default" "regex" "use_logging"))))))

