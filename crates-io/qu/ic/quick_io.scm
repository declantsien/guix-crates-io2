(define-module (crates-io qu ic quick_io) #:use-module (crates-io))

(define-public crate-quick_io-1.0.0 (c (n "quick_io") (v "1.0.0") (h "1v8zilm5lx9s4yl3j9n9v0d47vck6srcaq9lcjngmnrlqrzn9k3w")))

(define-public crate-quick_io-1.0.1 (c (n "quick_io") (v "1.0.1") (h "08f64vp5q8fh3h6j8zchy9m01xa3hk7lid13mai83gp93w92irkx")))

(define-public crate-quick_io-2.0.0 (c (n "quick_io") (v "2.0.0") (h "19xggk4bp4rhcqn8dsj15v9f8aq5w2nf5r2z03ihf6kffnvxb03q")))

