(define-module (crates-io qu ic quickcheck_seedable) #:use-module (crates-io))

(define-public crate-quickcheck_seedable-0.3.1 (c (n "quickcheck_seedable") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1cgdlj0argfmi6si7wa1m38sk68zn2acb7ljklb3kqb0cki1a6l6") (f (quote (("unstable"))))))

