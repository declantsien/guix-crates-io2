(define-module (crates-io qu ic quickfix-msg43) #:use-module (crates-io))

(define-public crate-quickfix-msg43-0.1.1 (c (n "quickfix-msg43") (v "0.1.1") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "1gak2a7vabm7c2qdcvyrp7nisrzvy0pj6hljqq8hmj4mc4ly5d0n") (r "1.70.0")))

(define-public crate-quickfix-msg43-0.1.3 (c (n "quickfix-msg43") (v "0.1.3") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "09ycb1xn7r7h2bj0gzzdpva3kd11mah663yi8ki134q7jca0cgnv") (r "1.70.0")))

(define-public crate-quickfix-msg43-0.1.4 (c (n "quickfix-msg43") (v "0.1.4") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "164rki2hwbwkv8kyzkxs6l8xdqfg1ldmvmkzsdysi34zaf6z4hh8") (r "1.70.0")))

