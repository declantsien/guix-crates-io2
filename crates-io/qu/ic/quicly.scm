(define-module (crates-io qu ic quicly) #:use-module (crates-io))

(define-public crate-quicly-0.0.0 (c (n "quicly") (v "0.0.0") (h "081lqzawiqq8hpfjikzyay12lkqkrsb895w28dy6lflm1g2k3qdj")))

(define-public crate-quicly-0.0.1 (c (n "quicly") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0dwz01h00y21j63yhq0k8j715slmbz93d2vlq795gcix56i5kjk0")))

