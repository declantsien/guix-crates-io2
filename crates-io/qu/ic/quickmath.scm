(define-module (crates-io qu ic quickmath) #:use-module (crates-io))

(define-public crate-quickmath-0.2.3 (c (n "quickmath") (v "0.2.3") (d (list (d (n "evalexpr") (r "^11.0.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "18nznxiy87956kh6m5ik6jpynxqxa3r007i0lxnfvddfgw0d0pc1")))

