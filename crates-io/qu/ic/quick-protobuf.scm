(define-module (crates-io qu ic quick-protobuf) #:use-module (crates-io))

(define-public crate-quick-protobuf-0.1.0 (c (n "quick-protobuf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)))) (h "03fz4ahhssqv9jbh0v301sk6676z2j7bp7mrb669pvp0fh67053h")))

(define-public crate-quick-protobuf-0.2.0 (c (n "quick-protobuf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)))) (h "0ah04dbrd1q014b9vd85dbwgcj2h03vv2vqc047k725ni4bcgfg4")))

(define-public crate-quick-protobuf-0.3.0 (c (n "quick-protobuf") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)))) (h "0sxlh0vmw94mmqzjc2bbvyycgr5j7q4q75jikphwdkf45kgv9nab")))

(define-public crate-quick-protobuf-0.4.0 (c (n "quick-protobuf") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)))) (h "1s3v3r91zfvm6hkx7cg84dvzn312myjm819g65wgqzik9c5wincq")))

(define-public crate-quick-protobuf-0.5.0 (c (n "quick-protobuf") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)))) (h "01j09akz38z3lmfhjz184q27z3igm5nxjwd304p7ys1pi1d1y4vd")))

(define-public crate-quick-protobuf-0.6.0 (c (n "quick-protobuf") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.10") (d #t) (k 2)))) (h "1b8rwrwf45gqs0amcdar1l4d7jm5dzbm4f6y7cj70c44m94yczrz")))

(define-public crate-quick-protobuf-0.6.1 (c (n "quick-protobuf") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.10") (d #t) (k 2)))) (h "0nfa5kqngcwi3g8skc4yvqv55x6m0ivf1ry12z4vvfcqhag3sj3k")))

(define-public crate-quick-protobuf-0.6.2 (c (n "quick-protobuf") (v "0.6.2") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.10") (d #t) (k 2)))) (h "10vg9kahc7lbszy7ybyjvqr10ml1h74nly2qflr9ryim96p3k9j8")))

(define-public crate-quick-protobuf-0.6.3 (c (n "quick-protobuf") (v "0.6.3") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.10") (d #t) (k 2)))) (h "1p1nc3y8gpbvrx198v3170qwwfm76zyj74dbr1x2w17x25p2kkvv")))

(define-public crate-quick-protobuf-0.6.4 (c (n "quick-protobuf") (v "0.6.4") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.10") (d #t) (k 2)))) (h "1hin36bg7mnh9ccf1jbai80f9ryv5dlwjq2vpkzbrsycg9414l1m")))

(define-public crate-quick-protobuf-0.7.0 (c (n "quick-protobuf") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0jwbv96fj1pmf9zmy5ph7d5ak4jcbndj4c0n56q6kshp7jld92g4") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-quick-protobuf-0.8.0 (c (n "quick-protobuf") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1frd357rn036aw8m27ij5gr7hqk7qzdqlsx0rhqrx1mc0y9679kc") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-quick-protobuf-0.8.1 (c (n "quick-protobuf") (v "0.8.1") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "07rf1pdzq9l5rsv0qg96487ijvi73rp2zfh1ksc2lwh4q96ahvcx") (f (quote (("std" "byteorder/std") ("default" "std"))))))

