(define-module (crates-io qu ic quick-merkle) #:use-module (crates-io))

(define-public crate-quick-merkle-0.1.0 (c (n "quick-merkle") (v "0.1.0") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sha3") (r "^0.10.2") (d #t) (k 2)))) (h "06w13g11zsmm0xvmym2z577644s1pjd30alk4ghizra1bj439crp")))

