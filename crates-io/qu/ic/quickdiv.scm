(define-module (crates-io qu ic quickdiv) #:use-module (crates-io))

(define-public crate-quickdiv-0.1.0 (c (n "quickdiv") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "0ghrhdba563l3r73yw42drl8d0lb1amfjxfpmqk7h8x8j96gphpy")))

(define-public crate-quickdiv-0.1.1 (c (n "quickdiv") (v "0.1.1") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "10wlshqlbsn9hscb2dpf03qskplpmdp0kn94rpyhdw72jj385yxg")))

