(define-module (crates-io qu ic quic) #:use-module (crates-io))

(define-public crate-quic-0.0.1 (c (n "quic") (v "0.0.1") (h "1nx8lxwpvfs4jgnpp2id6yzdcd1bkd9si1k18w3vds3ng1g4q0kv")))

(define-public crate-quic-0.0.2 (c (n "quic") (v "0.0.2") (h "01hz9wm1dqcfcmkpzb141kf6r493zawgfwvlmlmz1cb4563v39qh")))

(define-public crate-quic-0.0.3 (c (n "quic") (v "0.0.3") (h "00lhb13yrk4igmz9ldhy5gqrvff4anq7prvpby15sp1s94mc6ivf")))

