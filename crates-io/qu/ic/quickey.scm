(define-module (crates-io qu ic quickey) #:use-module (crates-io))

(define-public crate-quickey-0.1.0 (c (n "quickey") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15z1q1wkm99cg8yzanv1263j4475gfi4fsbzi8njq9z0alb39ghc") (f (quote (("default" "base58")))) (s 2) (e (quote (("base58" "dep:bs58"))))))

(define-public crate-quickey-0.2.0 (c (n "quickey") (v "0.2.0") (d (list (d (n "bs58") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vmfg9mzwj6mr3n55pbqgbdps2gfjrqqwbdfxxvncfplaa7dnal7") (f (quote (("default" "base58")))) (s 2) (e (quote (("base58" "dep:bs58"))))))

(define-public crate-quickey-0.3.0 (c (n "quickey") (v "0.3.0") (d (list (d (n "bs58") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0cgfs46h0rnbcizwxqr7p1r4izhrfdprczyd2ardnsi1jsfyxlaw") (f (quote (("default" "base58" "hex")))) (s 2) (e (quote (("hex" "dep:hex") ("base58" "dep:bs58"))))))

(define-public crate-quickey-0.4.0 (c (n "quickey") (v "0.4.0") (d (list (d (n "base64") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "bs58") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rqwpmq9xrgbab7wcrbwvq0z6kcm5zd9ix9gwmwf6l9m8wrkyi1c") (f (quote (("default" "base58" "base64" "hex")))) (s 2) (e (quote (("hex" "dep:hex") ("base64" "dep:base64") ("base58" "dep:bs58"))))))

(define-public crate-quickey-0.4.1 (c (n "quickey") (v "0.4.1") (d (list (d (n "base64") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "bs58") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w6ya6w2z3qin3vfblknyk00l0pp0mj35qpyy2pd028j03zfsdjl") (f (quote (("default" "base58" "base64" "hex")))) (s 2) (e (quote (("hex" "dep:hex") ("base64" "dep:base64") ("base58" "dep:bs58"))))))

