(define-module (crates-io qu ic quicktok) #:use-module (crates-io))

(define-public crate-quicktok-0.1.1 (c (n "quicktok") (v "0.1.1") (d (list (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "0wiaapiiq66sjsmv4jgi2jfm3zhpfc90v6gd0das99mcsyzp913v")))

