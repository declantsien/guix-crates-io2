(define-module (crates-io qu ic quickerr) #:use-module (crates-io))

(define-public crate-quickerr-0.1.0 (c (n "quickerr") (v "0.1.0") (h "000m6ysqq4cyi4lcrm7h5qsfhfwjhi1cfcnb9xkcq52iqvgx34lr") (y #t)))

(define-public crate-quickerr-0.1.1 (c (n "quickerr") (v "0.1.1") (h "14jf9km56x11040c4xz6m9hqynawa43wlbyyg571y2d57jdqprg7")))

(define-public crate-quickerr-0.2.0 (c (n "quickerr") (v "0.2.0") (h "0l3rbyh0bpxq8z3rni8pq16wy7hrgcz48n1jaw6qfdfdcnp6pbdb")))

(define-public crate-quickerr-0.3.0 (c (n "quickerr") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1cgpqv9p7iif92xj61m78024rzk5qg15fz35g8iwznsnhwb8pxzp")))

(define-public crate-quickerr-0.3.1 (c (n "quickerr") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1n8v2mkvddp60l9vawfsdnz2ll09pbn9q5i5q1hhyzfivblzxc9s")))

(define-public crate-quickerr-0.3.2 (c (n "quickerr") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1ja85szqiyfm991w9hxcd9iavi4iw4q8blpx11swqlginq3fqc9z")))

