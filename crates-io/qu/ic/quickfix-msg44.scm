(define-module (crates-io qu ic quickfix-msg44) #:use-module (crates-io))

(define-public crate-quickfix-msg44-0.1.1 (c (n "quickfix-msg44") (v "0.1.1") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "1yr4wkr77mc82mx1nkiav45jbvk7pay5gr1clrymk844ybxrpfhs") (r "1.70.0")))

(define-public crate-quickfix-msg44-0.1.3 (c (n "quickfix-msg44") (v "0.1.3") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "1ws1cj322ssv65h4pawhcv8af8yhr5gdj4mdrykq6gfb05ip8m4c") (r "1.70.0")))

(define-public crate-quickfix-msg44-0.1.4 (c (n "quickfix-msg44") (v "0.1.4") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "0ffrhb7yv2rxzgywakyhffhxgznrc8jrsgdqyxx354kcfg5cm0i3") (r "1.70.0")))

