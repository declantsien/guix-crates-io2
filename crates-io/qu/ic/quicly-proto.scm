(define-module (crates-io qu ic quicly-proto) #:use-module (crates-io))

(define-public crate-quicly-proto-0.0.0 (c (n "quicly-proto") (v "0.0.0") (h "0hvlr8x0qkdv8f9sahfnlfn2snv5rz8xypslly08p4gbcp7w2y8q")))

(define-public crate-quicly-proto-0.0.1 (c (n "quicly-proto") (v "0.0.1") (h "0xpyljbiq9wc4xj3887k5mkbc4ic91z4sj2pzp9ms9agz8rh9yc3")))

