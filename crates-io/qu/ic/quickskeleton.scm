(define-module (crates-io qu ic quickskeleton) #:use-module (crates-io))

(define-public crate-quickskeleton-0.4.7 (c (n "quickskeleton") (v "0.4.7") (d (list (d (n "handlebars") (r "^0.25.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "text_io") (r "^0.1.5") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.4") (d #t) (k 0)))) (h "1acclvnz0saf9rya87v3c8h8vrp1pqnn2hnlf9v31yjgm8r15v2a")))

(define-public crate-quickskeleton-0.4.8 (c (n "quickskeleton") (v "0.4.8") (d (list (d (n "handlebars") (r "^0.25.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "text_io") (r "^0.1.5") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.4") (d #t) (k 0)))) (h "0p8f046f3704518m8i9yl1rbc847hw8b68lzinifq5jvd3rsjdni")))

