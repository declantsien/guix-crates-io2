(define-module (crates-io qu ic quickmaths) #:use-module (crates-io))

(define-public crate-quickmaths-0.1.0 (c (n "quickmaths") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1dxd1am2a0kf03brg1c5qwiqc6n5ywb7cri3pfpdm09695gynnma")))

(define-public crate-quickmaths-0.1.1 (c (n "quickmaths") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "190pn1sq1x5k9h5fmisqqcr0j6q34c7mgrjhbqvrxh77y4gvjy6c") (f (quote (("std") ("default" "std"))))))

(define-public crate-quickmaths-0.1.2 (c (n "quickmaths") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03wk11d4070ks4q76bqkrg7i8likl6m9gg81svfd31x1l32bnx3j") (f (quote (("std") ("default" "std"))))))

(define-public crate-quickmaths-0.1.3 (c (n "quickmaths") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03pmx2fhamqbli7jz19qbrnwbvspm17vry6szwjj1qih21qcmwi7") (f (quote (("std") ("default" "std"))))))

