(define-module (crates-io qu ic quickselect) #:use-module (crates-io))

(define-public crate-quickselect-0.1.0 (c (n "quickselect") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0bgmz6hf6f6fjcf44wapjb7bk09lf2b8b63fwrp8cdzbkx942z35")))

(define-public crate-quickselect-0.1.1 (c (n "quickselect") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "01dh6cn1z0z6fshkdf0cw8l6iv79d47df9z4qy5byb37sbf91xjg")))

(define-public crate-quickselect-0.1.2 (c (n "quickselect") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "174qiqz98p6jkx7dr47n4hlpm89kfwnkaqp74r0a6knbabacbz06")))

(define-public crate-quickselect-0.1.3 (c (n "quickselect") (v "0.1.3") (h "0d4k68w78acpgv9xmspy67ii0vvpbrr324zpdakvbz2pffjygvid")))

