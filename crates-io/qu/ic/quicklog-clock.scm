(define-module (crates-io qu ic quicklog-clock) #:use-module (crates-io))

(define-public crate-quicklog-clock-0.1.1 (c (n "quicklog-clock") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "quanta") (r "^0.10.1") (d #t) (k 0)))) (h "1vgd3rjqvyj0z4kk5gk4fj1xwv0n8a91vns4l464gh4s7is36fk3")))

(define-public crate-quicklog-clock-0.1.2 (c (n "quicklog-clock") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "quanta") (r "^0.10.1") (d #t) (k 0)))) (h "143jndzi38w3977qc0335098a32cxlvspp0bixck0mvmxpd2mliz")))

(define-public crate-quicklog-clock-0.1.3 (c (n "quicklog-clock") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "quanta") (r "^0.10.1") (d #t) (k 0)))) (h "0lkyznhx1l83rp7zm87s6f062yzjmql0zw70fgl17vs9hpa7z7hl")))

