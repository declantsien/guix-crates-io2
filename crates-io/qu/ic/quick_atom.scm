(define-module (crates-io qu ic quick_atom) #:use-module (crates-io))

(define-public crate-quick_atom-0.1.0 (c (n "quick_atom") (v "0.1.0") (d (list (d (n "atom_syndication") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1c19i8vrg28nzz07pmqyi0lgxkpgcl59xj89x86m5yj83rxc3h4g")))

