(define-module (crates-io qu ic quickcheck2) #:use-module (crates-io))

(define-public crate-quickcheck2-2.0.0 (c (n "quickcheck2") (v "2.0.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng"))) (k 0)))) (h "1bisdxpr8nnid96jyks9yhndvk3aqxkmzld7fc2dam6hhk2xh4rv") (y #t)))

