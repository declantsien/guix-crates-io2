(define-module (crates-io qu ic quickbooks-rust) #:use-module (crates-io))

(define-public crate-quickbooks-rust-0.1.0 (c (n "quickbooks-rust") (v "0.1.0") (d (list (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (d #t) (k 0)) (d (n "validator") (r "^0.18.1") (d #t) (k 0)))) (h "0wiy69h51bx5a1s363v2mfh7pdhzrgq0lbih813fyivrnq1b8f2y")))

(define-public crate-quickbooks-rust-0.1.1 (c (n "quickbooks-rust") (v "0.1.1") (d (list (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (d #t) (k 0)) (d (n "validator") (r "^0.18.1") (d #t) (k 0)))) (h "0llzvdkbaa2sf4jpg07fs7imz16qms7mrlpa4x780ilrj6kj1wy6")))

