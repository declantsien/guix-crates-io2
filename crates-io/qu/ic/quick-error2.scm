(define-module (crates-io qu ic quick-error2) #:use-module (crates-io))

(define-public crate-quick-error2-2.0.0 (c (n "quick-error2") (v "2.0.0") (h "0vpx6yl2lpskf2lqg5irgmm2hmx3yk241rgr8ma55y2nbz2r17vl")))

(define-public crate-quick-error2-2.0.1 (c (n "quick-error2") (v "2.0.1") (h "1c60wpbdaawks52gh30nzxk4q0da2rpgvyslnvsk0f4p6907g5h1")))

(define-public crate-quick-error2-2.1.0 (c (n "quick-error2") (v "2.1.0") (h "00magw1y5jhm8mw09520qqjirazd60wn4qfg42ivk6i1pkryc8ny")))

