(define-module (crates-io qu ic quick) #:use-module (crates-io))

(define-public crate-quick-0.1.0-alpha.1 (c (n "quick") (v "0.1.0-alpha.1") (h "1hcqdp3x9z4j477c3aa81xl3jsc6jynpnpwv4il82cy69k4d7085") (y #t)))

(define-public crate-quick-0.1.0-alpha.2 (c (n "quick") (v "0.1.0-alpha.2") (d (list (d (n "libquickjs-sys") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "18ndgp442a7w180jbb1zkanynmif2ggx0jw98m3yhiwpvpd9i1wf") (y #t)))

(define-public crate-quick-0.1.0-alpha.3 (c (n "quick") (v "0.1.0-alpha.3") (d (list (d (n "libquickjs-sys") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "12gx2lgs5xml0w663b5958fxlq6fahzamnkba4wlz0rpx4gzw50k") (y #t)))

(define-public crate-quick-0.1.0-alpha.4 (c (n "quick") (v "0.1.0-alpha.4") (d (list (d (n "libquickjs-sys") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "0ah63si9v4f4lp3ardfyqqasn4cr4fmgdh0z0zj4py9hm4slmjg6") (y #t)))

(define-public crate-quick-0.1.0-alpha.5 (c (n "quick") (v "0.1.0-alpha.5") (h "02mrw5ch306gsgqsdvdbch3hcg6c47qbvg0swcfzr9jd59whcsw2")))

