(define-module (crates-io qu ic quicksort) #:use-module (crates-io))

(define-public crate-quicksort-1.0.0 (c (n "quicksort") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0pglmav4x4kxnv9fn5hjx2ha6s198jk2rmly3nrhy4vq8p8i9mkl")))

(define-public crate-quicksort-1.1.0 (c (n "quicksort") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "060r1w64l18nsn0v9wfk305jh1nfaha3mldm9f23bvsy31f8s4i2")))

