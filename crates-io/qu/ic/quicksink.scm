(define-module (crates-io qu ic quicksink) #:use-module (crates-io))

(define-public crate-quicksink-0.1.0 (c (n "quicksink") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "160ly9df8zrsq5hqqgm2x9q08k902ckdjrn7vg579dwjfla4z0qd")))

(define-public crate-quicksink-0.1.1 (c (n "quicksink") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "0c3m28f1dm6kln1nxcrf7hdi7f94wyf65l6wv1rgsqaz8kviwim8")))

(define-public crate-quicksink-0.1.2 (c (n "quicksink") (v "0.1.2") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "0n7qgajwz01ksx33n4jywcx09whxh2b2fnf674ahn5jsbs0krpkp")))

