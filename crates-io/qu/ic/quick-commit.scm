(define-module (crates-io qu ic quick-commit) #:use-module (crates-io))

(define-public crate-quick-commit-0.1.0 (c (n "quick-commit") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)))) (h "025kyf20wss21p1vmn3q9pvhrsifzmikwk9rnmj4bh754x4wvr7r")))

(define-public crate-quick-commit-0.1.1 (c (n "quick-commit") (v "0.1.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)))) (h "0zrma7mbfmwwz4rfa0d4h5kfibmxm9m2q4asjwghb95a5jplfc8s")))

(define-public crate-quick-commit-0.1.2 (c (n "quick-commit") (v "0.1.2") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)))) (h "06smk2r93sfr7z74jf61da12npvqd8h64gjkhp112ld6b6ql4nwj")))

