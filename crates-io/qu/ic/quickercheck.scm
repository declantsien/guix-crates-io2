(define-module (crates-io qu ic quickercheck) #:use-module (crates-io))

(define-public crate-quickercheck-0.1.0 (c (n "quickercheck") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10vcl916p4ii0s5061zifgcnzlxzpa0qy866f87v6c39lzglxsi0") (f (quote (("no_function_casts") ("default"))))))

(define-public crate-quickercheck-0.1.1 (c (n "quickercheck") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0zribdf4c3f02zqbvskchys745702cgxjpb5bnbpv81ddv2i5n0z") (f (quote (("no_function_casts") ("default"))))))

(define-public crate-quickercheck-0.1.2 (c (n "quickercheck") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "094djkb28lcnpnys8dlrc8ws04s83ldxyx9h1dinjiw0wf4zc49a") (f (quote (("no_function_casts") ("default"))))))

(define-public crate-quickercheck-0.1.3 (c (n "quickercheck") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1pf4vaqnb3i3v9nrydzhss5dibq55xmxv0nf5si32rl85xm9ll13") (f (quote (("no_function_casts") ("default"))))))

(define-public crate-quickercheck-0.2.0 (c (n "quickercheck") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0smszyjps9nznmvsmg3f0yizc5d6v6qvcgjmd04jpl36397kz1dw") (f (quote (("no_function_casts") ("default"))))))

