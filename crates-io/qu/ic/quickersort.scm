(define-module (crates-io qu ic quickersort) #:use-module (crates-io))

(define-public crate-quickersort-1.0.0 (c (n "quickersort") (v "1.0.0") (d (list (d (n "num") (r "^0.1.25") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "1kgakmqdi2kgcmgm3lbzkjzrb6pyqj0k797rdjir1nwyg00z20hc") (f (quote (("float" "num" "unreachable") ("default" "float")))) (y #t)))

(define-public crate-quickersort-1.0.1 (c (n "quickersort") (v "1.0.1") (d (list (d (n "itertools") (r "^0.3.23") (d #t) (k 2)) (d (n "num") (r "^0.1.25") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "1618qy3060qs859ppvbrvqnpgc8jjml2637vv8a4lrpwvy62x3fv") (f (quote (("float" "num") ("default" "float")))) (y #t)))

(define-public crate-quickersort-2.0.0 (c (n "quickersort") (v "2.0.0") (d (list (d (n "itertools") (r "^0.3.23") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.32") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "1qvgrid3pbwvic2whvccqinlacqjvmd3ky27n0inmy2ypz6r4xcv") (f (quote (("float" "num-traits") ("default" "float")))) (y #t)))

(define-public crate-quickersort-2.0.1 (c (n "quickersort") (v "2.0.1") (d (list (d (n "itertools") (r "^0.3.23") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.32") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "17g89hjfvj6xjbl4nzx6qi78l7w2hw4mpzxxarw6ykwhk35vfl2r") (f (quote (("float" "num-traits") ("default" "float")))) (y #t)))

(define-public crate-quickersort-2.1.0 (c (n "quickersort") (v "2.1.0") (d (list (d (n "itertools") (r "^0.3.23") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.32") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "0lcnqf7vs5v6xqd9vdwi7z0jpkxcmyw4mg04c1iq2916k5vfllp9") (f (quote (("float" "num-traits") ("default" "float")))) (y #t)))

(define-public crate-quickersort-2.1.1 (c (n "quickersort") (v "2.1.1") (d (list (d (n "itertools") (r "^0.3.23") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.32") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "0jgkapbmiqigcvr1rx04ks7qsmchb6pv6k8lcxlmhkgm5rgvi3p9") (f (quote (("float" "num-traits") ("default" "float")))) (y #t)))

(define-public crate-quickersort-2.2.0 (c (n "quickersort") (v "2.2.0") (d (list (d (n "itertools") (r "^0.3.23") (d #t) (k 2)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (k 0)))) (h "1iahhj2447wmrbxi69368ra8h5x8am5m4khi0fyqmirqghv8zbhl") (f (quote (("unstable" "nodrop/use_union") ("float" "num-traits") ("default" "float") ("assert_working_compare")))) (y #t)))

(define-public crate-quickersort-3.0.0 (c (n "quickersort") (v "3.0.0") (d (list (d (n "itertools") (r "^0.3.23") (d #t) (k 2)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (k 0)))) (h "151z4hj5ck83anqkypqiidvrxv1isg4zh096j0fgnqsn02dpjf8p") (f (quote (("unstable" "nodrop/use_union") ("assert_working_compare")))) (y #t)))

(define-public crate-quickersort-3.0.1 (c (n "quickersort") (v "3.0.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 2)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "unreachable") (r "^1") (k 0)))) (h "02r7wplnw7m1dhjah8p89inxn9g4nwb7ga7xdn95cffwpwn88z1h") (f (quote (("unstable") ("assert_working_compare")))) (y #t)))

