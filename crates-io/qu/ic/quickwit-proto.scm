(define-module (crates-io qu ic quickwit-proto) #:use-module (crates-io))

(define-public crate-quickwit-proto-0.1.0 (c (n "quickwit-proto") (v "0.1.0") (h "1y58w6vh04jnfvwvm5c2qq3zx2w961i9dkwcv8cnrzr3fz9r4s2d")))

(define-public crate-quickwit-proto-0.3.0 (c (n "quickwit-proto") (v "0.3.0") (d (list (d (n "prost") (r "^0.10.0") (f (quote ("prost-derive"))) (k 0)) (d (n "prost-build") (r "^0.10.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.0") (d #t) (k 1)))) (h "11wq9f8jcvzb5fq3mh0qk74xxwg07gxig0k3nmq8hqygy15yq1la")))

