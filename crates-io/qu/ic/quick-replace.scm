(define-module (crates-io qu ic quick-replace) #:use-module (crates-io))

(define-public crate-quick-replace-0.1.0 (c (n "quick-replace") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "0b6wz3wxj4p3gxmkjzka7nnmgn39k63sjsifczr8m89fllby9xbs")))

(define-public crate-quick-replace-0.1.1 (c (n "quick-replace") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "0scavrqqk9l6wqf461dn82nx4yvicrs6n1i0my6v1ms513vslcvz")))

