(define-module (crates-io qu ic quic-socket) #:use-module (crates-io))

(define-public crate-quic-socket-0.1.0 (c (n "quic-socket") (v "0.1.0") (d (list (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "161a28k2nindp5dzyj3dz1xcqx8ifsc8h2ynyyyckab8cwlwy0nb")))

(define-public crate-quic-socket-0.1.1 (c (n "quic-socket") (v "0.1.1") (d (list (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "0yqiyn87yr0ib0lrlq5097ic1d771jiw0zk0ak9441ria9i9w0fc")))

(define-public crate-quic-socket-0.2.1 (c (n "quic-socket") (v "0.2.1") (d (list (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1d5giswfbb1qmfawbsxjp5mkhl2g0s93j46czqsdshlknb8ih6n5")))

(define-public crate-quic-socket-0.2.2 (c (n "quic-socket") (v "0.2.2") (d (list (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "0kkafjslm4iqg7mdnydl0j29mni5j8139sif831igrn17cvs82wy")))

(define-public crate-quic-socket-0.3.2 (c (n "quic-socket") (v "0.3.2") (d (list (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "0vicn4mj28d4vw2ln1y0xx1fvnwx5h8s1fd6m18kb08gg4q8ksmi")))

(define-public crate-quic-socket-0.3.3 (c (n "quic-socket") (v "0.3.3") (d (list (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1vyrq6cxnn9308fy94z381imc19a4fxrwkvs819xbc55c9xr5n94")))

(define-public crate-quic-socket-0.4.0 (c (n "quic-socket") (v "0.4.0") (d (list (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1lrlph6jz7jbn03ssk4xkp2swmnmbyqdxaymkab0j6gcc3l563iw")))

(define-public crate-quic-socket-0.4.1 (c (n "quic-socket") (v "0.4.1") (d (list (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "16kkw1pf0x8x57zx2x99xx5gd0w6g3vf3d5ragdn7brkkia3pzmi")))

(define-public crate-quic-socket-0.4.2 (c (n "quic-socket") (v "0.4.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1kf0078jcbj9gpr064fjbyaqmv8wh5ahhm4zb5sd19y9ajw9w418")))

(define-public crate-quic-socket-0.4.3 (c (n "quic-socket") (v "0.4.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "05c0mzmdk8wxvdfllwdhy4qmld9frs29l3l7qfwlv70jv0805n23")))

(define-public crate-quic-socket-0.4.4 (c (n "quic-socket") (v "0.4.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1sr7l0xbbiyv1lbc1rm35vi3z3rl9nkvy1sinv2c22nd5k77yil5")))

(define-public crate-quic-socket-0.4.5 (c (n "quic-socket") (v "0.4.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1rn544mi7sbcpysym6dyalyxmrpfs7c95qs45aj8jj95fjwxyam3")))

(define-public crate-quic-socket-0.5.0 (c (n "quic-socket") (v "0.5.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "04hlg1y5rgdapxzslh5zwkh3jpmh41an0wz0mmlvlph9vx7d7k42")))

(define-public crate-quic-socket-0.5.1 (c (n "quic-socket") (v "0.5.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "03c0g9aprwyw7h8yc8l89xli7b0b89iq5yfaf1q0l3mpwz8h9z1b")))

(define-public crate-quic-socket-0.5.2 (c (n "quic-socket") (v "0.5.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "06hihikvrydm1yqc21c5qwwm0j7fm7hm750gvd2g6sc5k6l39627")))

(define-public crate-quic-socket-0.5.3 (c (n "quic-socket") (v "0.5.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1nrx3g8xq647vxk6axm8ms3j3fr68nb6misvlj9n1hdfk44kwl1j")))

(define-public crate-quic-socket-0.5.4 (c (n "quic-socket") (v "0.5.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "19px1lkmhbvm9lpnifxjd0kg4lrkn5s0sc4pfd5gr53v70jdxb39")))

(define-public crate-quic-socket-0.5.5 (c (n "quic-socket") (v "0.5.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1vgqmk12bpdq5h4qmhpxkxam4ms875lbkhnhrkydznv22c9y4fn5")))

(define-public crate-quic-socket-0.5.6 (c (n "quic-socket") (v "0.5.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1agqpv0yb695b0zgwkn12n870m4b35hrab4ifwh59lcxwblzq8z5")))

(define-public crate-quic-socket-0.5.7 (c (n "quic-socket") (v "0.5.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1vj1km2xakvlgn4wdf6cs4bqshv1b3fl74r4l096y02f0gz99g7v")))

(define-public crate-quic-socket-0.5.8 (c (n "quic-socket") (v "0.5.8") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1jfmb19sw2ifmhg84mmy27svknr4bcxikrwc36j9ilpx8r1kiynx")))

(define-public crate-quic-socket-0.5.9 (c (n "quic-socket") (v "0.5.9") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "027f1swyzv9zfi9jldjvfn947k2p699cny9cb1k67b51wigj2rmj")))

(define-public crate-quic-socket-0.6.0 (c (n "quic-socket") (v "0.6.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1j17s6jj2h9k56y1djd29hm1rvixlqpw641vncyc53jbc85cz6w4")))

(define-public crate-quic-socket-0.6.1 (c (n "quic-socket") (v "0.6.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "14zv0rf4mcxnc5g0vyw55l0jdmf52nam997mhckx3a5si5i5pb15")))

(define-public crate-quic-socket-0.6.2 (c (n "quic-socket") (v "0.6.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "06rhv4vwc5ngsmlpzr0fccnafxnjs70xa0lhp0qwxwgn7bh742aa")))

(define-public crate-quic-socket-0.6.3 (c (n "quic-socket") (v "0.6.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1wfi73p6kgx22l0pl4b3g5zzk1pbdpkwils4ajwgvakshfgzsh21")))

(define-public crate-quic-socket-0.6.4 (c (n "quic-socket") (v "0.6.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "1rrdw9rbmrfk3p8607yam2493bzyys9w1rqi6vpd4x5im3q2q363")))

(define-public crate-quic-socket-0.6.5 (c (n "quic-socket") (v "0.6.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "09zwp0bkxwslqlm0xznmb3s4n7mzk8lmpx0yn5x7gsnm6m2pdqag")))

(define-public crate-quic-socket-0.6.6 (c (n "quic-socket") (v "0.6.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "02bdfafnzsa1sgqbrymdw7vzchipm8af4wdsrzhhv5zma364bjrd")))

(define-public crate-quic-socket-0.6.7 (c (n "quic-socket") (v "0.6.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "07pqivy0p7jx1iz06fnx87alh338rarqjhc4gnmvzpy3c8isyx4p")))

(define-public crate-quic-socket-0.6.8 (c (n "quic-socket") (v "0.6.8") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "0kds4dsis95jv9qr94rs61g878g7l2na9whcml54sigsaj4iwis6")))

(define-public crate-quic-socket-0.6.9 (c (n "quic-socket") (v "0.6.9") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "quiche") (r "^0.10.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net"))) (d #t) (k 0)))) (h "0h82lr258yd2yxi5mbpkkvfb0qa3pk71gbpyxjd5ix7kh81pihkf")))

(define-public crate-quic-socket-0.7.0 (c (n "quic-socket") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.14") (d #t) (k 0)) (d (n "rustls") (r "^0.20.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1hdyw7vn93pfn0rd63xmdjl4pkk5630iqf5vjm213d0pxq7gk5q3")))

(define-public crate-quic-socket-0.7.1 (c (n "quic-socket") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.14") (d #t) (k 0)) (d (n "rustls") (r "^0.20.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1likvbhhxka8h6fndzp423niy7cy5djycg44cjxb324rl2w01xs1")))

(define-public crate-quic-socket-0.7.2 (c (n "quic-socket") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.14") (d #t) (k 0)) (d (n "rustls") (r "^0.20.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1g3mkihic27k75zp1v3d6ik4c7xzy97xcq0laph6b2cgkwrxvdqa")))

(define-public crate-quic-socket-0.7.3 (c (n "quic-socket") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.14") (d #t) (k 0)) (d (n "rustls") (r "^0.20.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0w7pk3c7389ngyipk8spsa97vs22hafbyskl3di9f40baszab76a")))

(define-public crate-quic-socket-0.7.4 (c (n "quic-socket") (v "0.7.4") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.14") (d #t) (k 0)) (d (n "rustls") (r "^0.20.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "116mpczij96jh24ladqg61akazc7ahbv50prxn76jq70jpqjp5xr")))

(define-public crate-quic-socket-0.7.5 (c (n "quic-socket") (v "0.7.5") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.14") (d #t) (k 0)) (d (n "rustls") (r "^0.20.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1xyyp5wjfa8h8gvpb4zb8yfjij7bjf5jnqv9y50c66mc23n20jmb")))

(define-public crate-quic-socket-0.7.6 (c (n "quic-socket") (v "0.7.6") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.14") (d #t) (k 0)) (d (n "rustls") (r "^0.20.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0nga70034x2ljk3mpbgrxbk5jpm7h8vx91fxm7bsnm6rjsnk2r8g")))

(define-public crate-quic-socket-0.7.7 (c (n "quic-socket") (v "0.7.7") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.14") (d #t) (k 0)) (d (n "rustls") (r "^0.20.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "09jwdk34walf4lna232qxiddriagdafa8zixzfh5rirwk0bv2cj1")))

