(define-module (crates-io qu ic quickwit-directories) #:use-module (crates-io))

(define-public crate-quickwit-directories-0.1.0 (c (n "quickwit-directories") (v "0.1.0") (h "1qrvq13q2smjfiyh9l1q7svxscjdr55rb5hmksgiixn8ijkrj13x")))

(define-public crate-quickwit-directories-0.3.0 (c (n "quickwit-directories") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "quickwit-storage") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tantivy") (r "^0.18.0") (f (quote ("mmap" "lz4-compression" "quickwit"))) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0ibsw9i2xkkk8lzwa7qdbjynvzdr4lvfs2137p9cd9301jbn6dw6")))

