(define-module (crates-io qu ic quick-hash-cache) #:use-module (crates-io))

(define-public crate-quick-hash-cache-1.0.1 (c (n "quick-hash-cache") (v "1.0.1") (d (list (d (n "hashbrown") (r "^0.13") (f (quote ("inline-more" "raw"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "quanta") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1cz40siq76gp7r6gc4pw9h6q1f0jlynlr2h9cbc615vf2mx35sv3")))

