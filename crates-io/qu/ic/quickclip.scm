(define-module (crates-io qu ic quickclip) #:use-module (crates-io))

(define-public crate-quickclip-0.1.0 (c (n "quickclip") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "1qxlygfdfja2ysq0v51za89mvg33rniksf8qgwsqfkb3k9nw3mnk")))

(define-public crate-quickclip-0.1.1 (c (n "quickclip") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "1778zp7i03kbig3mindcg9wnl7400icxqdzms0r2m9998sf7mmiv")))

(define-public crate-quickclip-0.1.2 (c (n "quickclip") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "0gsrbf1rbvyxyiywvdl0jmxqqi6q1nmnn4gjhf8bl7bckvza7nkf")))

(define-public crate-quickclip-0.1.3 (c (n "quickclip") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "173hfgh45rl2fpq6qsi1dxx4b53ph9gbr1sh28mbbkhh7n13a9f2")))

(define-public crate-quickclip-0.1.4 (c (n "quickclip") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "02af4sakz9zkhzl3jmlb739p547wfsc4ankjmxn3qjhhaqhvc61w")))

