(define-module (crates-io qu ic quickwit-telemetry) #:use-module (crates-io))

(define-public crate-quickwit-telemetry-0.1.0 (c (n "quickwit-telemetry") (v "0.1.0") (h "1574w6h5m02pdm7jbawxrn1ijgci6d650slclp5r77ji1nhw884l")))

(define-public crate-quickwit-telemetry-0.3.0 (c (n "quickwit-telemetry") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "=0.8.29") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "username") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "17g8hcmzgfdgfgk0d1xpr748kvbibij4dfp0s0q5fqa06ncd49dw")))

