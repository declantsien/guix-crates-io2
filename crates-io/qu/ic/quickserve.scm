(define-module (crates-io qu ic quickserve) #:use-module (crates-io))

(define-public crate-quickserve-0.1.1 (c (n "quickserve") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "17zwxrnm6bcb4hq7vv56al8gr8dymi528vmia77x81hy9yvmnm79")))

(define-public crate-quickserve-0.1.2 (c (n "quickserve") (v "0.1.2") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "1gdd7rmxjgykanybak99qq1il34m4s0b4bp0y7hkcxavs1qbjf6c")))

(define-public crate-quickserve-0.2.0 (c (n "quickserve") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "08397s9ddia87500fpz689zdpgjzgd5fh4fn8fi04vsyrkd9imdy")))

(define-public crate-quickserve-0.3.0 (c (n "quickserve") (v "0.3.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "0vg6q58m79mn4m8kxq9xvxqykgni8f0i180b0xqx9l28q7slkny6")))

(define-public crate-quickserve-0.3.1 (c (n "quickserve") (v "0.3.1") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "07a8dlsmwy5xpj6qgiibiibbjgn6n5ri62w3xlx1j39kz8lp8m1y")))

(define-public crate-quickserve-0.3.2 (c (n "quickserve") (v "0.3.2") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "1m4diplilmwvyffsy5qan94gbr97dgaw1y3i2w65av6dj4sj76h5")))

(define-public crate-quickserve-0.4.0 (c (n "quickserve") (v "0.4.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "1scjab7amlsjxqdnq09cncwfw5sklb1gd1mifri3fjrk62ll3zj7")))

(define-public crate-quickserve-0.5.0 (c (n "quickserve") (v "0.5.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "1jx3im6s5x9da9b16q63kxzdlxb1pg4j63q7gacq5z18cdhf71ay")))

(define-public crate-quickserve-0.5.2 (c (n "quickserve") (v "0.5.2") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "1n0dphyrcajvp9ialhyvjq97c2rnz2lx5wn3130ysr5d6kdmhmnw")))

