(define-module (crates-io qu ic quickwit-config) #:use-module (crates-io))

(define-public crate-quickwit-config-0.1.0 (c (n "quickwit-config") (v "0.1.0") (h "1qgwzlf5i6sad0zcaybl3swq2dfmpjg1x1r82virwwmpjwiawk4a")))

(define-public crate-quickwit-config-0.3.0 (c (n "quickwit-config") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byte-unit") (r "^4") (f (quote ("serde"))) (k 0)) (d (n "json_comments") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "quickwit-common") (r "^0.3.0") (d #t) (k 0)) (d (n "quickwit-doc-mapper") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "0kpfjicd5g7difk171g2raxpr2bknqik2mkv6qila8l3pn6gmjhr")))

