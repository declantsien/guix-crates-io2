(define-module (crates-io qu ic quickdeck) #:use-module (crates-io))

(define-public crate-quickdeck-0.1.0 (c (n "quickdeck") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.12") (d #t) (k 0)))) (h "1zcd6r5czsvvc0d9jhdjc2c72f92l7j74syilnpxnvv2jyabvwyi")))

(define-public crate-quickdeck-0.1.1 (c (n "quickdeck") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.12") (d #t) (k 0)))) (h "0wr8fz4jf7f75xb2d8qs3xmm5bz6cgigfzjbd3bxm4pa04i2wcm6")))

