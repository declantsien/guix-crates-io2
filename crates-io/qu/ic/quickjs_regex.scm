(define-module (crates-io qu ic quickjs_regex) #:use-module (crates-io))

(define-public crate-quickjs_regex-0.1.0 (c (n "quickjs_regex") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0695ypvc7wcqqk22pqzn5h4206ppgsq71xrq8047xxmjif0nng2r")))

(define-public crate-quickjs_regex-0.2.0 (c (n "quickjs_regex") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0n80347ipn2pd7ijvzpwfbmldvdwm4rk9dv6hxxii6026pk7i4m9")))

(define-public crate-quickjs_regex-0.2.1 (c (n "quickjs_regex") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0mwc44y40v1fv2yiarsjji2i8nagbp21d66i5596684dird0rry2")))

(define-public crate-quickjs_regex-0.2.2 (c (n "quickjs_regex") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1v4cnqxvz7r42vphlf4vc1v53zg2nisgcjik0dj4q8qm6n8v03hm")))

(define-public crate-quickjs_regex-0.2.3 (c (n "quickjs_regex") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickjs_regex_backend") (r "^0.1.0") (d #t) (k 0)) (d (n "quickjs_regex_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.6.25") (d #t) (k 2)))) (h "0zpc8nspkizfkmnswl7h6kckhpj04dbxrr3i7acnd3vf5ca8pfd2")))

