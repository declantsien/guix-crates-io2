(define-module (crates-io qu ic quickhttp) #:use-module (crates-io))

(define-public crate-quickhttp-0.1.0 (c (n "quickhttp") (v "0.1.0") (h "122nglsrqnqmq07syybawd2inq9kd7jlzh0bzycqnhyzkbca0a3w")))

(define-public crate-quickhttp-0.1.1 (c (n "quickhttp") (v "0.1.1") (h "1p7dggwd0brxnbr42wcd2l30bh1ahdc06jmqy29h4qwdms587kpb")))

