(define-module (crates-io qu ic quickcheck_async) #:use-module (crates-io))

(define-public crate-quickcheck_async-0.1.0 (c (n "quickcheck_async") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0dxwrc5n2q080fakyws3d78z82p26hqi97y74ibh8v2ccrxjn5bz")))

(define-public crate-quickcheck_async-0.1.1 (c (n "quickcheck_async") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "blocking"))) (d #t) (k 2)))) (h "1ymj0bl2gfwgss4vvpzy3frg2s3kj5r5f0d5apia84qkjiqzcz94")))

