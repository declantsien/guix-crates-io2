(define-module (crates-io qu ic quickfix-msg-gen) #:use-module (crates-io))

(define-public crate-quickfix-msg-gen-0.1.1 (c (n "quickfix-msg-gen") (v "0.1.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-spec-parser") (r "^0.1.0") (d #t) (k 0)))) (h "0cj6rc2jcn04j2sr0siy4iz4vxgaj1q6jxldgjv6ps2b67961dik") (r "1.70.0")))

(define-public crate-quickfix-msg-gen-0.1.3 (c (n "quickfix-msg-gen") (v "0.1.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-spec-parser") (r "^0.1.0") (d #t) (k 0)))) (h "0przvrfilhm1w3wxfplzyr82naagnszlk4lj60fm03a5xdh4zwl0") (r "1.70.0")))

(define-public crate-quickfix-msg-gen-0.1.4 (c (n "quickfix-msg-gen") (v "0.1.4") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-spec-parser") (r "^0.1.0") (d #t) (k 0)))) (h "13x58v5sqdrz2bn02wfvwzk36rv52j42zvpvh7c1zjqx99n8gys7") (r "1.70.0")))

