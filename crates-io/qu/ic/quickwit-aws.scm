(define-module (crates-io qu ic quickwit-aws) #:use-module (crates-io))

(define-public crate-quickwit-aws-0.3.0 (c (n "quickwit-aws") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.47") (f (quote ("rustls"))) (k 0)) (d (n "rusoto_kinesis") (r "^0.47") (f (quote ("rustls"))) (o #t) (k 0)) (d (n "rusoto_s3") (r "^0.47") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.18") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1i16ac6rly0z3cykd57240lbz5b7rs9a0w5pvqs4jpp88dyb6kf1") (f (quote (("kinesis" "rusoto_kinesis"))))))

