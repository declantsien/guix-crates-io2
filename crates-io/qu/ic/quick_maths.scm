(define-module (crates-io qu ic quick_maths) #:use-module (crates-io))

(define-public crate-quick_maths-0.1.0 (c (n "quick_maths") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0s9jfprg2ylcak46n53zmlrhcfhbyvzgl5h37pfr196pzrjssxnp") (f (quote (("default_f64"))))))

(define-public crate-quick_maths-0.1.1 (c (n "quick_maths") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0357is2f1zjl9ldmyns3qr4mjk29vq837hl9625i9277afghq3n9") (f (quote (("default_f64"))))))

(define-public crate-quick_maths-0.1.2 (c (n "quick_maths") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05acmw7l0r3rl96ghw69p1slvhbspq33als4lw45hsa7hav1vcds") (f (quote (("default_f64"))))))

(define-public crate-quick_maths-0.1.3 (c (n "quick_maths") (v "0.1.3") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1flfmy7j2sd34dlzjz2zy64dw8iq5cqgb4ciza92wr28zbf3cf4z") (f (quote (("default_f64"))))))

(define-public crate-quick_maths-0.1.5 (c (n "quick_maths") (v "0.1.5") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sc89kkmw5i82rq1agmw30vffbabmx9wcg3npbfg5bwxaj7wmv90") (f (quote (("default_f64"))))))

(define-public crate-quick_maths-0.1.6 (c (n "quick_maths") (v "0.1.6") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "047p9k6603f2smch1r8k7fa22fgbhkx958vscdplpfa5kl031kr3") (f (quote (("default_f64"))))))

(define-public crate-quick_maths-0.1.7 (c (n "quick_maths") (v "0.1.7") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kjdviihfw46pgni1y27wkfjx62c4zwd6mdsx74y337g92kwrp8m") (f (quote (("default_f64"))))))

(define-public crate-quick_maths-0.1.8 (c (n "quick_maths") (v "0.1.8") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1padyb79gqq6hcrxid58pdbylpczb5dm7jnymz26c5xqi2n159q2") (f (quote (("default_f64"))))))

(define-public crate-quick_maths-0.2.0 (c (n "quick_maths") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kmgfqkw5dd5vyr5ja934k2ryf4hwfhw8gr9yy29mahnac12c3mh") (f (quote (("stokes") ("mueller") ("masked") ("default_f64"))))))

(define-public crate-quick_maths-0.2.1 (c (n "quick_maths") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yki6dy3srdwcrckba7amn0dyyp7gf3zwaix3fj7g72l530lp2in") (f (quote (("stokes") ("mueller") ("masked") ("default_f64") ("autodiff"))))))

