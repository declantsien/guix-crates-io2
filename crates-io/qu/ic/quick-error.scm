(define-module (crates-io qu ic quick-error) #:use-module (crates-io))

(define-public crate-quick-error-0.1.0 (c (n "quick-error") (v "0.1.0") (h "0xmd2mr4dqqa1kzyfqcbw7v3wbkmymimpx2apmyjhy2izy5ik80h")))

(define-public crate-quick-error-0.1.1 (c (n "quick-error") (v "0.1.1") (h "1gw0y9gzc29h4gkj2rz98fbhm91lyfzl9bn8cypvw9f80m548zvi")))

(define-public crate-quick-error-0.1.2 (c (n "quick-error") (v "0.1.2") (h "00965cnbiy0iphzp97vj5n2c9ma66ay6fzhifbf8g9msryi1cbpg")))

(define-public crate-quick-error-0.1.3 (c (n "quick-error") (v "0.1.3") (h "1pjwry3gq0ggin09sgyjm85izxn8z02ib9cfpgwxfwl7dg2mlxjm")))

(define-public crate-quick-error-0.1.4 (c (n "quick-error") (v "0.1.4") (h "07fals7bq9j777nrgrg4hwm1rnpkniddnn75xb1bkg3vvgwcrdjz")))

(define-public crate-quick-error-0.2.0 (c (n "quick-error") (v "0.2.0") (h "11kxyva5zx8kc1y8vzdnfzahml6rx8a17k73rzppl6b1xhj8izq4")))

(define-public crate-quick-error-0.2.1 (c (n "quick-error") (v "0.2.1") (h "1nw0y01pds074x5xrzjf6imgvsmfjj88x14b5daiwhsl3bkw0aqc")))

(define-public crate-quick-error-0.2.2 (c (n "quick-error") (v "0.2.2") (h "1m059qldabkm48r3yrmdfk3cyzq6843d0gsy394di3839smr1jbs")))

(define-public crate-quick-error-1.0.0 (c (n "quick-error") (v "1.0.0") (h "0l6a97vggdlkpb7qjy4nayvjn6hd0n0mh3r9idywmmnfvi3y85f4")))

(define-public crate-quick-error-1.1.0 (c (n "quick-error") (v "1.1.0") (h "0757jd5lbcpi7kkhjh8j0vj2k26f4swg9wdx5ni7vdkzilz61b8a")))

(define-public crate-quick-error-1.2.0 (c (n "quick-error") (v "1.2.0") (h "0sd4xzxamscraqfz7rp7qfjj6yam791f1c92wki1psvq95yrhdiw")))

(define-public crate-quick-error-1.2.1 (c (n "quick-error") (v "1.2.1") (h "1i5spa4864dj7sfkfhr1kdv02r07m9m210dph6y64vlpf6dzx9gd")))

(define-public crate-quick-error-1.2.2 (c (n "quick-error") (v "1.2.2") (h "1w6kgwwv7p7zr0yyg5rb315lkk24bimywklwx7fsvsbwi10bjx4j")))

(define-public crate-quick-error-1.2.3 (c (n "quick-error") (v "1.2.3") (h "1q6za3v78hsspisc197bg3g7rpc989qycy8ypr8ap8igv10ikl51")))

(define-public crate-quick-error-2.0.0 (c (n "quick-error") (v "2.0.0") (h "1njwdphwqfjmfqbgn3v9gw6vzqf78sy0j6g6n84w2vvp288kpirs")))

(define-public crate-quick-error-2.0.1 (c (n "quick-error") (v "2.0.1") (h "18z6r2rcjvvf8cn92xjhm2qc3jpd1ljvcbf12zv0k9p565gmb4x9")))

