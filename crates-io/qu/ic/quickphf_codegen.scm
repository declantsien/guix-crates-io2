(define-module (crates-io qu ic quickphf_codegen) #:use-module (crates-io))

(define-public crate-quickphf_codegen-0.1.0 (c (n "quickphf_codegen") (v "0.1.0") (d (list (d (n "quickdiv") (r "^0.1.1") (d #t) (k 0)) (d (n "quickphf") (r "^0.1.0") (d #t) (k 0)))) (h "0rpl62q609xv7rs4dz8mms7pd9wwhvg93kqy2n053b33r8n5mqyi") (r "1.56")))

(define-public crate-quickphf_codegen-0.1.1 (c (n "quickphf_codegen") (v "0.1.1") (d (list (d (n "quickdiv") (r "^0.1.1") (d #t) (k 0)) (d (n "quickphf") (r "^0.1.0") (d #t) (k 0)))) (h "1jc7v94pydims8ala2ybpv5lwxxhv32inyzjhkss15dgrz5mnkj0") (r "1.56")))

