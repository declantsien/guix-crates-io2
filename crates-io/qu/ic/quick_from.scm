(define-module (crates-io qu ic quick_from) #:use-module (crates-io))

(define-public crate-quick_from-0.1.0 (c (n "quick_from") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hpi43l31plh403w4zg0rqmycvw2lwigmzhx5aml29qzhb0q9i89")))

(define-public crate-quick_from-0.2.0 (c (n "quick_from") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fszhixwycf0b6hqjyzvbvfkb9nzdls8b5yh677ak0vv6fh3ryl1")))

