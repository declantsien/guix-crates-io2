(define-module (crates-io qu ic quickwit-common) #:use-module (crates-io))

(define-public crate-quickwit-common-0.1.0 (c (n "quickwit-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "053nd7n0krjwn7ppl2l7d2zkqhf5bw8cxa6b9dv921ccqqj7j5n9")))

(define-public crate-quickwit-common-0.3.0 (c (n "quickwit-common") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (f (quote ("process"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("fs" "macros" "rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0wlzr1g4dh0bbgzz0034jbg4j432lkql1c6s89hfncgywdcgfaq0")))

