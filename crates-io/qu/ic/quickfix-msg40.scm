(define-module (crates-io qu ic quickfix-msg40) #:use-module (crates-io))

(define-public crate-quickfix-msg40-0.1.1 (c (n "quickfix-msg40") (v "0.1.1") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "0dx1zvnvsz3alhp8rk89frg5j9asmzg85jgq3dqxnk9jg4qqb8m2") (r "1.70.0")))

(define-public crate-quickfix-msg40-0.1.3 (c (n "quickfix-msg40") (v "0.1.3") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "1q684da2fb03077l9jd723fq2vb82sqvw8zgn8x0155jk3skicnv") (r "1.70.0")))

(define-public crate-quickfix-msg40-0.1.4 (c (n "quickfix-msg40") (v "0.1.4") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "1029zcaqdfb3ssc3l9nh6g37rrlw92ry8162mkivw0x101rbm40l") (r "1.70.0")))

