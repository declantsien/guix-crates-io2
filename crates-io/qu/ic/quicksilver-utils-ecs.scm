(define-module (crates-io qu ic quicksilver-utils-ecs) #:use-module (crates-io))

(define-public crate-quicksilver-utils-ecs-0.1.0 (c (n "quicksilver-utils-ecs") (v "0.1.0") (d (list (d (n "instant") (r "^0.1.2") (f (quote ("now"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "platter") (r "^0.1.4") (d #t) (k 2)) (d (n "quicksilver") (r "= 0.4.0-alpha0.2") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.4.0") (d #t) (k 0)) (d (n "specs") (r "^0.15") (d #t) (k 0)) (d (n "specs-derive") (r "^0.4.0") (d #t) (k 0)))) (h "01napq46w9dj8nlsc754ddpkngf7ghk8613prgddhlc1hb655md3") (f (quote (("stdweb" "quicksilver/stdweb" "instant/stdweb"))))))

