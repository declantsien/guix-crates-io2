(define-module (crates-io qu ic quickrandr) #:use-module (crates-io))

(define-public crate-quickrandr-0.3.0 (c (n "quickrandr") (v "0.3.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.0") (d #t) (k 0)))) (h "181j8b9qxj56mc8b5m8wqmlp0asxqhsxvzwczghnjx6641bfbv4n")))

(define-public crate-quickrandr-0.3.1 (c (n "quickrandr") (v "0.3.1") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.0") (d #t) (k 0)))) (h "0wcqnxgqzygi1nxw4sn3aix58xr0vdnpksfk08qnlwmhdj51y4cd")))

(define-public crate-quickrandr-0.3.2 (c (n "quickrandr") (v "0.3.2") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.0") (d #t) (k 0)))) (h "0k3djnam24gh6pbr1zzid7nc566nmdnv7z65v5276sdj2h2l2vr0")))

