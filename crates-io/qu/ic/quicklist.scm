(define-module (crates-io qu ic quicklist) #:use-module (crates-io))

(define-public crate-quicklist-0.0.1 (c (n "quicklist") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "1h0xslv8h3713339vaqwrihkjvi67xwgpbnxnisd8802pizkvsm0")))

