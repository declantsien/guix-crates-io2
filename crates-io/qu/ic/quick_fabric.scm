(define-module (crates-io qu ic quick_fabric) #:use-module (crates-io))

(define-public crate-quick_fabric-0.1.0 (c (n "quick_fabric") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.41") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0245knz70qlp81rfffygh80gjdsfjldv51m0s20wk3jfrmlncwdg") (y #t)))

(define-public crate-quick_fabric-0.1.1 (c (n "quick_fabric") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.41") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wfzqh5iqhmdxvxph9rclzs65zqa2b2d3ljd9gpzn7h52g5f1y7z")))

(define-public crate-quick_fabric-0.1.2 (c (n "quick_fabric") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.41") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "119pvr1ay0x7g93sf75iqq2y4mq3ym25jnak5ln4zjiamlrb8cp9")))

