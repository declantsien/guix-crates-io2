(define-module (crates-io qu ic quick-storer) #:use-module (crates-io))

(define-public crate-quick-storer-1.0.0 (c (n "quick-storer") (v "1.0.0") (d (list (d (n "brotli") (r "^3.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mla") (r "^1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0ww1lsq3apn88vi12dk15bjgvnl8xmqaamliiy6l0nssd4vyg1xi")))

