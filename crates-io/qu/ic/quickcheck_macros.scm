(define-module (crates-io qu ic quickcheck_macros) #:use-module (crates-io))

(define-public crate-quickcheck_macros-0.1.2 (c (n "quickcheck_macros") (v "0.1.2") (d (list (d (n "quickcheck") (r "~0.1.2") (d #t) (k 0)))) (h "0dyzdmashfv36xm7j51dvipd4mvy9q7nhsd5p8snc6naxl01v8bz")))

(define-public crate-quickcheck_macros-0.1.3 (c (n "quickcheck_macros") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.1.3") (d #t) (k 0)))) (h "1dqzx8k7ryvmvn15966qf8z1y1pwqif5yvj26ddv8yyv78vhzyf0")))

(define-public crate-quickcheck_macros-0.1.4 (c (n "quickcheck_macros") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.1.4") (d #t) (k 0)))) (h "06ka7ywkh697z2khfnqi6im87cx8l6w80rhyg7ymbf5rn00bmjmn")))

(define-public crate-quickcheck_macros-0.1.5 (c (n "quickcheck_macros") (v "0.1.5") (d (list (d (n "quickcheck") (r "^0.1.5") (d #t) (k 0)))) (h "1xnii2nfnph91qqm5llk2xal5lw040x7bhxp9710bfgx1rsxa7qa")))

(define-public crate-quickcheck_macros-0.1.6 (c (n "quickcheck_macros") (v "0.1.6") (d (list (d (n "quickcheck") (r "^0.1.6") (d #t) (k 0)))) (h "1i31ydgcg7id4x5lswa2cvp9n3ghjqs6a9bwmfny8p4kip825gvd")))

(define-public crate-quickcheck_macros-0.1.7 (c (n "quickcheck_macros") (v "0.1.7") (d (list (d (n "quickcheck") (r "^0.1.7") (d #t) (k 0)))) (h "15bqqkcq10b5vg9lxjfg1kdzmnh3n34kzlavfwd42hwvqwc4gxqj")))

(define-public crate-quickcheck_macros-0.1.8 (c (n "quickcheck_macros") (v "0.1.8") (d (list (d (n "quickcheck") (r "^0.1.8") (d #t) (k 0)))) (h "1k6m3cgn0mzi7j1svp856nvpm2jig7cflnyih3jq7za2fx9zj1nd")))

(define-public crate-quickcheck_macros-0.1.11 (c (n "quickcheck_macros") (v "0.1.11") (d (list (d (n "quickcheck") (r "^0.1.11") (d #t) (k 0)))) (h "0hf8i576sqfajscshpjzsm0b8r92fzzrnmxfl1aqgg2i1l9jcx11")))

(define-public crate-quickcheck_macros-0.1.19 (c (n "quickcheck_macros") (v "0.1.19") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1g9gciwf1j9ky78cfgn1pr2msws3mqhapc914v4mvdijzbvg22r9")))

(define-public crate-quickcheck_macros-0.1.20 (c (n "quickcheck_macros") (v "0.1.20") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0ycx374wd3h0qhvnj1h5ljqj4pxw567zflazfnxd96czjrj497rw")))

(define-public crate-quickcheck_macros-0.1.21 (c (n "quickcheck_macros") (v "0.1.21") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1ar74c58gl49fk8z975mjkpyi5d5hw5mgaihv1g4gimw710ga5k0")))

(define-public crate-quickcheck_macros-0.1.22 (c (n "quickcheck_macros") (v "0.1.22") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1i808cqljjdrfirk57jdsnnrqbph7z02qi4jl084n8bysrcwxs76")))

(define-public crate-quickcheck_macros-0.1.23 (c (n "quickcheck_macros") (v "0.1.23") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1ws11f62xb7ln5l2r9p230ad4fskarbyq5af4rp4az08vpva5yp2")))

(define-public crate-quickcheck_macros-0.1.24 (c (n "quickcheck_macros") (v "0.1.24") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "049f2b278r1imj1m3hi1krnlny3w6h9wblc62m23wfzsrm7fyf31")))

(define-public crate-quickcheck_macros-0.1.25 (c (n "quickcheck_macros") (v "0.1.25") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1sw64zp0mwh3zcifjs335hx8yhs88v06lz840p02cr9rjscpbrf7")))

(define-public crate-quickcheck_macros-0.1.26 (c (n "quickcheck_macros") (v "0.1.26") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "08wrl8i311ip0i5l4s9bidnvmlb6xvr8nj9ipl0g4iqvb1y32ilz")))

(define-public crate-quickcheck_macros-0.1.27 (c (n "quickcheck_macros") (v "0.1.27") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "13b2mnx1pyy05h1ns2q6z3drh0pa8ib40n9l48g52k0li9kh7bw3")))

(define-public crate-quickcheck_macros-0.1.28 (c (n "quickcheck_macros") (v "0.1.28") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "064d4q75sx4mf66ns2gibb4gf7j6skk0m8xvfa3fqnw344bsxlzw")))

(define-public crate-quickcheck_macros-0.1.29 (c (n "quickcheck_macros") (v "0.1.29") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0d9i8jzm2qmvs8miacmlkq4n34v5ms6n1ba9vjyg90fp56faqs3h")))

(define-public crate-quickcheck_macros-0.1.30 (c (n "quickcheck_macros") (v "0.1.30") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1gbx796ips9gp949b7df6yiq57qmb6d8lfsr2sw7mvi3idv47gz3")))

(define-public crate-quickcheck_macros-0.1.31 (c (n "quickcheck_macros") (v "0.1.31") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "12zg4a76byj7yp33ialbx4cd0v0d3q8qr66h3w86d1dhav6igkns")))

(define-public crate-quickcheck_macros-0.1.32 (c (n "quickcheck_macros") (v "0.1.32") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0phrwymdrgpbj2whkrymyydgdqb67xrz3kkrf6pc0h9p24knpnw0")))

(define-public crate-quickcheck_macros-0.1.33 (c (n "quickcheck_macros") (v "0.1.33") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "07ljn0ysq75iyh53rbaxpd48b9xd594m5w1rj9ygzl5ab1n8zi4n")))

(define-public crate-quickcheck_macros-0.1.34 (c (n "quickcheck_macros") (v "0.1.34") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0dbkksyzyhgzlz8qh6vr53pfkfyhfl8nv87iqvjs478dsz25wbys")))

(define-public crate-quickcheck_macros-0.1.35 (c (n "quickcheck_macros") (v "0.1.35") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0p6bk571765ykpkfljmpyass165pzrwra76d0590ffvds07lll2g")))

(define-public crate-quickcheck_macros-0.1.36 (c (n "quickcheck_macros") (v "0.1.36") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1z4kzx3zzhzqn3my40l4wcxv2agxj6nqbaq4ab5jrhn58iabsm57")))

(define-public crate-quickcheck_macros-0.1.37 (c (n "quickcheck_macros") (v "0.1.37") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0hhxaj8fc9mqdx782phi3rv1lmy1lzfcdnbnrpjs2jgxryn5iwgw")))

(define-public crate-quickcheck_macros-0.1.38 (c (n "quickcheck_macros") (v "0.1.38") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "06sdvnhn5f4f5pacyh7ynk0x1qvz0zria265sxq8pj46d6cnfkbw")))

(define-public crate-quickcheck_macros-0.1.39 (c (n "quickcheck_macros") (v "0.1.39") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1f0ppf79h2lqjrvsnkz1bxghzjq3w7mfwswhy1kz3kr2x354j9qp")))

(define-public crate-quickcheck_macros-0.1.40 (c (n "quickcheck_macros") (v "0.1.40") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1xr2z71gd6mipbbgx6wh8nh8sfkpmn0b7kb7lyd9x7qiivsyd71m")))

(define-public crate-quickcheck_macros-0.2.0 (c (n "quickcheck_macros") (v "0.2.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1v6xbxywvy8cam3a0c8p65fgc0ylfbbdix9y90hrqfasgn0bf498")))

(define-public crate-quickcheck_macros-0.2.1 (c (n "quickcheck_macros") (v "0.2.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0jbl77x0dj397094333a2ym2sayggin68znpvvhgisv1fvmnmrb1")))

(define-public crate-quickcheck_macros-0.2.2 (c (n "quickcheck_macros") (v "0.2.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "11fv2kjf0jzc3zp815bp4kdrkmb19q59rgjyjj2spr94f9p481hj")))

(define-public crate-quickcheck_macros-0.2.3 (c (n "quickcheck_macros") (v "0.2.3") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1kfvcixzrgd8arjmqc2zf0yznvcsahwm21a267p0xcsx8czn4dyc")))

(define-public crate-quickcheck_macros-0.2.4 (c (n "quickcheck_macros") (v "0.2.4") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "16i439xa9ivvfwx6yhim06g5v7sidvmwa8zlsvqb6il7p6sxvnh1")))

(define-public crate-quickcheck_macros-0.2.5 (c (n "quickcheck_macros") (v "0.2.5") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1j1x7nkjbg5j00rxp27dfcw36cnq89317sf142vdkfswxndxlw8i")))

(define-public crate-quickcheck_macros-0.2.6 (c (n "quickcheck_macros") (v "0.2.6") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "03bf6ldzw9rz32skf8ga9wrnc63bxq04ia4f8yzdi2fhs2crgri5")))

(define-public crate-quickcheck_macros-0.2.8 (c (n "quickcheck_macros") (v "0.2.8") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0sdaqnvnb9mjg5bx9z7kdldb1dv9d6l0cf9akpyknhyvd459i34v")))

(define-public crate-quickcheck_macros-0.2.9 (c (n "quickcheck_macros") (v "0.2.9") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "17x1l7hmj1a46bziadngdazrnsyi2d21zfad2c1mlbkr5kbiyfng")))

(define-public crate-quickcheck_macros-0.2.10 (c (n "quickcheck_macros") (v "0.2.10") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1g8n0mz605p7y6zpzfbcgg6j5v3n34sxjx1wz38kbikdnzw47l2j")))

(define-public crate-quickcheck_macros-0.2.11 (c (n "quickcheck_macros") (v "0.2.11") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1g47ma79zrqjrvgb23cibanjic2kjhcicr9sk1b89iwqb1bnz73f")))

(define-public crate-quickcheck_macros-0.2.12 (c (n "quickcheck_macros") (v "0.2.12") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "11s7bay8cqgyz71vl6djaz1hxini0mqx6k4m60kixfc1b6mjfmz7")))

(define-public crate-quickcheck_macros-0.2.13 (c (n "quickcheck_macros") (v "0.2.13") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "002vpxycwj41ximqpqzjkpp0g2qrnfy2d88s14iwf4dhsq7d5hbb")))

(define-public crate-quickcheck_macros-0.2.14 (c (n "quickcheck_macros") (v "0.2.14") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "073v8k2bpvz31ms37m6nmpl5cxarjwzyaznh8ghpdvsbwblkhgpv")))

(define-public crate-quickcheck_macros-0.2.15 (c (n "quickcheck_macros") (v "0.2.15") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1fiy6jjm6p3x9w0n181f2qk0jxjc25gx2cmr0011cbvbf0h74f83")))

(define-public crate-quickcheck_macros-0.2.16 (c (n "quickcheck_macros") (v "0.2.16") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1247bipqyxxkm1s9p0wdnx5fmydc5jcwy1wh2jvdm0nrayyx7g5z")))

(define-public crate-quickcheck_macros-0.2.17 (c (n "quickcheck_macros") (v "0.2.17") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "03c8ffyzybwifqnm9ahnc6rqr1xl5hqzzsp9mcnzia2f783dxl6f")))

(define-public crate-quickcheck_macros-0.2.18 (c (n "quickcheck_macros") (v "0.2.18") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0s209imxxm9fq9hbinyiil19whbjh5k60b9n06j8j2zmg6g5ir7d")))

(define-public crate-quickcheck_macros-0.2.19 (c (n "quickcheck_macros") (v "0.2.19") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1nkgyzqhxshbf337z9pl22ss1z0q1p2869cyy2cxr24s2yxas8ks")))

(define-public crate-quickcheck_macros-0.2.20 (c (n "quickcheck_macros") (v "0.2.20") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1hp12kcqrmd8jn4s221lgy2rggpr9af2b8qvrmf2bj0iby5kx095")))

(define-public crate-quickcheck_macros-0.2.21 (c (n "quickcheck_macros") (v "0.2.21") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "1vyq71cr00niakjlymi9l4c88dfvybf2cmanx36a23xfjmlcix5z")))

(define-public crate-quickcheck_macros-0.2.22 (c (n "quickcheck_macros") (v "0.2.22") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)))) (h "0spaf30knzbcdpim4r9d3d3911cs83g4391i1xx8qdywiq9vbkkf")))

(define-public crate-quickcheck_macros-0.2.23 (c (n "quickcheck_macros") (v "0.2.23") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 0)))) (h "04mhxwygpplfaws3m35qlb1xmjas42nmzai9467hw900mz2731xb")))

(define-public crate-quickcheck_macros-0.2.24 (c (n "quickcheck_macros") (v "0.2.24") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 0)))) (h "1cnrhq5vspnqji7w78jkiqbss1gikrwnn832j0qwhvw6bmyn6hfi")))

(define-public crate-quickcheck_macros-0.2.25 (c (n "quickcheck_macros") (v "0.2.25") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 0)))) (h "1bq2faayzpzcjgcy4gjp61m6qb4r8rn240hsm4qica1nxr8f2qpy")))

(define-public crate-quickcheck_macros-0.2.26 (c (n "quickcheck_macros") (v "0.2.26") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 0)))) (h "1qyms8xy4dp8b1amikyxalrmzpvczw9vif0zrzw48xbqzgkgdvkh")))

(define-public crate-quickcheck_macros-0.2.27 (c (n "quickcheck_macros") (v "0.2.27") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 0)))) (h "0jl4vivxq4diprqvj001lzg4mbyradmyisc8bpp47nkz0dik32yw")))

(define-public crate-quickcheck_macros-0.2.28 (c (n "quickcheck_macros") (v "0.2.28") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "10k963syjjx2svsapgs94hl8vnbdy546fqjjrvs3y6xghg0zlh4r")))

(define-public crate-quickcheck_macros-0.2.29 (c (n "quickcheck_macros") (v "0.2.29") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "1fdcv882b81q64fsy3d77kqd4r9cimd5w15w15p6zphys07zhphx")))

(define-public crate-quickcheck_macros-0.4.0 (c (n "quickcheck_macros") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "10y7dk8v5s57g1gr2ly50crp18ksdq1iawkphnaa6kjqfg2k1z0h")))

(define-public crate-quickcheck_macros-0.4.1 (c (n "quickcheck_macros") (v "0.4.1") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 0)))) (h "0wn6kw9i1yjzp3n67bqy1mdl7wqiz9s954mza51bb2rzb8pj4fx9")))

(define-public crate-quickcheck_macros-0.4.2 (c (n "quickcheck_macros") (v "0.4.2") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 0)))) (h "1qk5x9ag4sqyzkdxr6bzrcx1d793isvzkls15ayhfzybhi25r32w")))

(define-public crate-quickcheck_macros-0.5.0 (c (n "quickcheck_macros") (v "0.5.0") (d (list (d (n "quickcheck") (r "^0.5") (d #t) (k 0)))) (h "1mr79pyd213vqx8s6hx8cim6sd0dzlgm5hk919dlq16zr70kz568")))

(define-public crate-quickcheck_macros-0.5.1 (c (n "quickcheck_macros") (v "0.5.1") (d (list (d (n "quickcheck") (r "^0.5") (d #t) (k 0)))) (h "1ff0chivx1ninlnnrvj71kw6knfzqjqqlwh90a91v7p5xxqih69q") (y #t)))

(define-public crate-quickcheck_macros-0.6.0 (c (n "quickcheck_macros") (v "0.6.0") (d (list (d (n "quickcheck") (r "^0.5") (d #t) (k 0)))) (h "0kvs436la92c24qr9qjjr9xfp8ny63yayc1013z3s4x72c0wm34d")))

(define-public crate-quickcheck_macros-0.6.1 (c (n "quickcheck_macros") (v "0.6.1") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 0)))) (h "0njmban351dcrzzcw9x1p6ivg7ygr0bz3bvlw22si64s43fdwjjm")))

(define-public crate-quickcheck_macros-0.6.2 (c (n "quickcheck_macros") (v "0.6.2") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 0)))) (h "04zwb03v45wikc5qkf82zqikz6v7wik1fhlnmg6f66kbl96bybif")))

(define-public crate-quickcheck_macros-0.7.0 (c (n "quickcheck_macros") (v "0.7.0") (d (list (d (n "quickcheck") (r "^0.7.1") (d #t) (k 0)))) (h "194nir3yxp1idj6bj985w6yvvdrh5h1xs87wla9aqdwzxw10w12c")))

(define-public crate-quickcheck_macros-0.8.0 (c (n "quickcheck_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0b3mhn0xcrdd3fkbkx3rghhivwzwil8w991ngp6gaj70l72c3pyp")))

(define-public crate-quickcheck_macros-0.9.0 (c (n "quickcheck_macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "16hnknvfcp3v7gq78vsalm45xq7hbblwf0mz0z3yc7kdcwxyjy8k")))

(define-public crate-quickcheck_macros-0.9.1 (c (n "quickcheck_macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zsb9b4jpg7qvbiym4v8y9pgqk7p1g4f5hn9gp0fnzz9v1pib330")))

(define-public crate-quickcheck_macros-1.0.0 (c (n "quickcheck_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s8nh0fmmzq3fd7928qcp2syvymlyv1pmww6fxcaj5np48r6jamj")))

