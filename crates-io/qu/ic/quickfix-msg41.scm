(define-module (crates-io qu ic quickfix-msg41) #:use-module (crates-io))

(define-public crate-quickfix-msg41-0.1.1 (c (n "quickfix-msg41") (v "0.1.1") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "0myyhnmyajjhhvrkfx642y7s7mhylv0vrwib4ap3ykj6vlb07p9z") (r "1.70.0")))

(define-public crate-quickfix-msg41-0.1.3 (c (n "quickfix-msg41") (v "0.1.3") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "19sbfg6w66pn899ncmmyin6drn72wy6hlcx8wnnlbgrravidllgz") (r "1.70.0")))

(define-public crate-quickfix-msg41-0.1.4 (c (n "quickfix-msg41") (v "0.1.4") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "10fdyqpqwfdans18h3ry6id3ckkqjb88ws02n2d4g938kgjwhv9y") (r "1.70.0")))

