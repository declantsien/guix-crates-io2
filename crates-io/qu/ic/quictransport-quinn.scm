(define-module (crates-io qu ic quictransport-quinn) #:use-module (crates-io))

(define-public crate-quictransport-quinn-0.8.0 (c (n "quictransport-quinn") (v "0.8.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "quinn") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "webtransport-generic") (r "^0.8") (d #t) (k 0)))) (h "160kw3nqakwinbbijnqnrlcvhab89s9a48wpps5z8d8drn6fcrad")))

(define-public crate-quictransport-quinn-0.9.0 (c (n "quictransport-quinn") (v "0.9.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "quinn") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "webtransport-generic") (r "^0.9") (d #t) (k 0)))) (h "1yg3w4xbs12rsnwws3w3qhkfc5b3k80asyrncvy0yx1d2ikpj8h4")))

