(define-module (crates-io qu ic quick-protobuf-codec) #:use-module (crates-io))

(define-public crate-quick-protobuf-codec-0.1.0 (c (n "quick-protobuf-codec") (v "0.1.0") (d (list (d (n "asynchronous-codec") (r "^0.6") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (f (quote ("asynchronous_codec"))) (d #t) (k 0)))) (h "12rxwada5d9zzspylq0w1hka3hb9c607gdhh5zmkcr028mii34qn") (r "1.60.0")))

(define-public crate-quick-protobuf-codec-0.2.0 (c (n "quick-protobuf-codec") (v "0.2.0") (d (list (d (n "asynchronous-codec") (r "^0.6") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (f (quote ("asynchronous_codec"))) (d #t) (k 0)))) (h "163fynrgzw3n584z0ynmqwvfgyrrf46dslad4hkiclvqrnqyvvgq") (r "1.65.0")))

(define-public crate-quick-protobuf-codec-0.3.0 (c (n "quick-protobuf-codec") (v "0.3.0") (d (list (d (n "asynchronous-codec") (r "^0.7.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.8.0") (f (quote ("asynchronous_codec"))) (d #t) (k 0)))) (h "0q5w8izzrchpyg3c1k39r5dmvp4dymr50izs49r7j9i9r5bijiy0") (r "1.73.0")))

(define-public crate-quick-protobuf-codec-0.3.1 (c (n "quick-protobuf-codec") (v "0.3.1") (d (list (d (n "asynchronous-codec") (r "^0.7.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "quick-protobuf") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.8.0") (f (quote ("std"))) (d #t) (k 0)))) (h "0x14vq4v829j58avwcc9fcbcl9ljjsxb57d3sx2rf5ibnc55i80m") (r "1.73.0")))

