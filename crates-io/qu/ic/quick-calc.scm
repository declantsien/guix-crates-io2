(define-module (crates-io qu ic quick-calc) #:use-module (crates-io))

(define-public crate-quick-calc-0.2.0 (c (n "quick-calc") (v "0.2.0") (h "02j9l16r9ybzw2qbankhw9jr2z46l7x3k1j0q600vs19cljkj2rg")))

(define-public crate-quick-calc-0.2.1 (c (n "quick-calc") (v "0.2.1") (h "18wkhqkv4ic0z9vzmdr8bsdbd4n4fkq4yrlszp5djmxc48wm05ig")))

(define-public crate-quick-calc-0.3.0 (c (n "quick-calc") (v "0.3.0") (h "1qf58hmlr2n6yp2xhll4yblpkczlql6w6al5hla8ys3vvcxqrd1h")))

