(define-module (crates-io qu ic quick-csv) #:use-module (crates-io))

(define-public crate-quick-csv-0.1.0 (c (n "quick-csv") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0zk49ds42p931ryyfcyz4qcmjm8w2m64ymnw0yb9g44zrqr7lg9h")))

(define-public crate-quick-csv-0.1.2 (c (n "quick-csv") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0dyn4v60hkg24b23c3dkz431hcn3wc38xmsf7crl2pl2dzib53dc")))

(define-public crate-quick-csv-0.1.3 (c (n "quick-csv") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "17bacc20gz3nkpq5cpyywk9d43n0h6h598vpab1z7wfshhyhral6")))

(define-public crate-quick-csv-0.1.4 (c (n "quick-csv") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0lhplwi4kj3l1w8g6s62crhq8sjhscby7wmsq83m0dql0jgww9qc")))

(define-public crate-quick-csv-0.1.5 (c (n "quick-csv") (v "0.1.5") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13pg0lxsx2n4r4mp2nli2808r0k2x1fxg3k4m367vl1cl6w0a8j9")))

(define-public crate-quick-csv-0.1.6 (c (n "quick-csv") (v "0.1.6") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "131k8zzlplk2h62wz813jbvm0sk7v3mixwhhq34y9lmp3mqbgx7d")))

