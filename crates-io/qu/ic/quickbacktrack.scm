(define-module (crates-io qu ic quickbacktrack) #:use-module (crates-io))

(define-public crate-quickbacktrack-0.1.0 (c (n "quickbacktrack") (v "0.1.0") (h "1qhm5v66jph9n34z0b8ww1mwxwds9181b82arm6vsmvvbm0m3byz")))

(define-public crate-quickbacktrack-0.1.1 (c (n "quickbacktrack") (v "0.1.1") (h "10csqa62vas6g5lb4pzf67cgfillsdmmjh56vd9g079d0k5ajzdb")))

(define-public crate-quickbacktrack-0.2.0 (c (n "quickbacktrack") (v "0.2.0") (h "18yd9xaxaa0zacmymsq4dha4wddzcczsa1imn1wf6dxrcqw56d18")))

(define-public crate-quickbacktrack-0.3.0 (c (n "quickbacktrack") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "0a962jkw44q31j6r76yf29r3pv6grycpw1frnlfc09dqvmwxkk1y")))

(define-public crate-quickbacktrack-0.4.0 (c (n "quickbacktrack") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "1kvgjm823igrrpnnriwmp2d1820khn5v3cmyw7wpk24c35sf432a")))

(define-public crate-quickbacktrack-0.5.0 (c (n "quickbacktrack") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "0xqb2b1lhqvn2xrvwc8h37s7ylw34nykycpqqx0ljnpmxfy2m756")))

(define-public crate-quickbacktrack-0.6.0 (c (n "quickbacktrack") (v "0.6.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "1v56r8zp7wy7fgih9l79mj60r4a1s3j3q59d0c9nahisyl6s58bf")))

(define-public crate-quickbacktrack-0.7.0 (c (n "quickbacktrack") (v "0.7.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1fmd0smc11qbx2ld620n2l4szfnm4z62h5dmxsa28mv34a0bjnd5")))

