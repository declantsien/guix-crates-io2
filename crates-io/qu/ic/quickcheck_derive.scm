(define-module (crates-io qu ic quickcheck_derive) #:use-module (crates-io))

(define-public crate-quickcheck_derive-0.1.0 (c (n "quickcheck_derive") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1w94hxgki091nl8mkjjgc8aygcvwrbqan37f24pawhha8c3a94l6")))

(define-public crate-quickcheck_derive-0.1.1 (c (n "quickcheck_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (d #t) (k 0)))) (h "1dxff7rbgi88c9mjh90xvs51kcak3hjmhy7zp9krhjrd60xdx7ja")))

(define-public crate-quickcheck_derive-0.1.2 (c (n "quickcheck_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (d #t) (k 0)))) (h "0j7s12l63rmcl36m9hrjnlfn3cl5ia28nrznklnycq779vk5n4h2")))

(define-public crate-quickcheck_derive-0.1.3 (c (n "quickcheck_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (d #t) (k 0)))) (h "0l53y5ngksarch3imgnjrmfdk8m4yipdql5sl1sziq7r05kc7a9f")))

(define-public crate-quickcheck_derive-0.2.0 (c (n "quickcheck_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (d #t) (k 0)))) (h "109jv1nvi1zyq08nryi3449kij17qi9ji94kscq6k0jnmivl3hla")))

(define-public crate-quickcheck_derive-0.2.1 (c (n "quickcheck_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (d #t) (k 0)))) (h "1s6i9ljrswqgnx9h11w5xm3aa0ps66hz27j6gx5dcxs0p8sw26n2")))

(define-public crate-quickcheck_derive-0.3.0 (c (n "quickcheck_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1ssdd7h0qiplnjzi85f8jca3w83qvgy2ccrxc5d3wnlf42qncmb9")))

