(define-module (crates-io qu ic quickxorhash) #:use-module (crates-io))

(define-public crate-quickxorhash-0.1.0 (c (n "quickxorhash") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "11p63zcikn780p2v0fi904lmkzidbpnc571knwgsyjs08qcf1lhl")))

