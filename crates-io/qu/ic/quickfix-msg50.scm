(define-module (crates-io qu ic quickfix-msg50) #:use-module (crates-io))

(define-public crate-quickfix-msg50-0.1.3 (c (n "quickfix-msg50") (v "0.1.3") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "06jppk1c2f1c9bj69vr239glpn34j47fddck6r3ww3qq2vbnwi4g") (r "1.70.0")))

(define-public crate-quickfix-msg50-0.1.4 (c (n "quickfix-msg50") (v "0.1.4") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "1618i99rvax9rwrnq2211nl76cmhkhr2gmx38rh5z6bzpmq7y79d") (r "1.70.0")))

