(define-module (crates-io qu ic quick_file) #:use-module (crates-io))

(define-public crate-quick_file-0.1.0 (c (n "quick_file") (v "0.1.0") (h "1bm5kc31g40k23534633s8yscp57nmhjr3wnxl81wpkm9snc69r2")))

(define-public crate-quick_file-0.2.0 (c (n "quick_file") (v "0.2.0") (h "1cjg8s7zws58bmy1fc17hfg25l77yz7gpmk987m357z3lia2x0pl")))

