(define-module (crates-io qu ic quickfix-spec-parser) #:use-module (crates-io))

(define-public crate-quickfix-spec-parser-0.1.0 (c (n "quickfix-spec-parser") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "17yrhxwr41qnwp6id9w81b1gzmy1alp1ixqy0p15fpmjlxgd1ab4") (r "1.70.0")))

(define-public crate-quickfix-spec-parser-0.1.1 (c (n "quickfix-spec-parser") (v "0.1.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "17hxm7wcpcaqir9km4dzr62l25kyw3kfsdnpx3k24fl7x804f7zw") (r "1.70.0")))

(define-public crate-quickfix-spec-parser-0.1.3 (c (n "quickfix-spec-parser") (v "0.1.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1drvvz28a1ijky9r0ibwyrpicgph61gj3hsf901mgc67pk8y06g3") (r "1.70.0")))

(define-public crate-quickfix-spec-parser-0.1.4 (c (n "quickfix-spec-parser") (v "0.1.4") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0clkb705mdhmhy404nqhj7gyd0a4ags7lrjnabsx04c0niazymad") (r "1.70.0")))

