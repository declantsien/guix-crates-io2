(define-module (crates-io qu ic quic_rust) #:use-module (crates-io))

(define-public crate-quic_rust-0.1.0 (c (n "quic_rust") (v "0.1.0") (h "12sy8yqxddc0lxb36kgh4cyv1362pl49xwr8v15qv1ljn8g755ry")))

(define-public crate-quic_rust-0.1.1 (c (n "quic_rust") (v "0.1.1") (h "0x25c49k0991x2m4rfjpps5xrh02nzibysbr8p721l70q7ih1gjl")))

