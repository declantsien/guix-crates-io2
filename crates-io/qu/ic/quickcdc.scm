(define-module (crates-io qu ic quickcdc) #:use-module (crates-io))

(define-public crate-quickcdc-1.0.0 (c (n "quickcdc") (v "1.0.0") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "time") (r "^0.1.41") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "09dal5cdxjjh59ljys688v6r2j208crn9ggx4mh8y8zw0i0jz0pm")))

