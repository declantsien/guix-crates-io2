(define-module (crates-io qu ic quicklz) #:use-module (crates-io))

(define-public crate-quicklz-0.1.0 (c (n "quicklz") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "0g1nc6n3zhdm1h95ykg5jz1882aryspir2q9xyspfyn7bahr535c")))

(define-public crate-quicklz-0.2.0 (c (n "quicklz") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1649fd00y442g8yz4mzcjgpn911bb94xa9izsqnrx19pgn3awa9b") (f (quote (("nightly"))))))

(define-public crate-quicklz-0.3.0 (c (n "quicklz") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07qms07851v5hpq0a26rmy459h96prf3mbr51dj6sxckg2lscvgj") (f (quote (("nightly"))))))

(define-public crate-quicklz-0.3.1 (c (n "quicklz") (v "0.3.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cgvwp38q4fhyvji7qbkm3bhdk3jy4mj4ara9w3cv816b8kan3km") (f (quote (("nightly"))))))

