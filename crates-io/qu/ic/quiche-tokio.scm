(define-module (crates-io qu ic quiche-tokio) #:use-module (crates-io))

(define-public crate-quiche-tokio-0.1.0 (c (n "quiche-tokio") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "quiche") (r "^0.18.0") (f (quote ("qlog"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("net" "macros" "rt-multi-thread" "time" "sync" "fs" "io-util"))) (d #t) (k 0)))) (h "0n2lvaqbl4j8xj78z62jw5l09sigbmxk204v00l0fnip4pciwadp")))

