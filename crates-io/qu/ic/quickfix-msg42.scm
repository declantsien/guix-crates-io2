(define-module (crates-io qu ic quickfix-msg42) #:use-module (crates-io))

(define-public crate-quickfix-msg42-0.1.1 (c (n "quickfix-msg42") (v "0.1.1") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "1d80px42h9kf2g6m559kkwm7yggjfbj03jdw7x9d9gpv0fabrvc0") (r "1.70.0")))

(define-public crate-quickfix-msg42-0.1.3 (c (n "quickfix-msg42") (v "0.1.3") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "13czm93593mhjbs4f0ixrg0ybr34bjgamc8ybwzpnk7j6b295ccq") (r "1.70.0")))

(define-public crate-quickfix-msg42-0.1.4 (c (n "quickfix-msg42") (v "0.1.4") (d (list (d (n "quickfix") (r "^0.1.0") (d #t) (k 0)) (d (n "quickfix-msg-gen") (r "^0.1.0") (d #t) (k 1)))) (h "01bkzd3x1wbbrwbjr4711fxwy3fsy3frmm8s9dc0dm2xzlf0za60") (r "1.70.0")))

