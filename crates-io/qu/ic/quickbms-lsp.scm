(define-module (crates-io qu ic quickbms-lsp) #:use-module (crates-io))

(define-public crate-quickbms-lsp-0.0.1 (c (n "quickbms-lsp") (v "0.0.1") (d (list (d (n "antlr-rust") (r "=0.2") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^17.0.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.89.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "05zyjw07rzwlz5ji0w00j52daival5nziy89imb1ig9jjacg5762")))

(define-public crate-quickbms-lsp-0.1.0 (c (n "quickbms-lsp") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "jsonrpc-core") (r "^17.0.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.89.0") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "04j5wiijjv936v6wdg2521ir2hbpaipj44lvbzpm944akn0r6njq")))

(define-public crate-quickbms-lsp-0.1.1 (c (n "quickbms-lsp") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "jsonrpc-core") (r "^17.0.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.89.0") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.19.3") (d #t) (k 0)))) (h "1msid7lrviiqb5hxsrxzfxqz08jg222fj0qp49fbsxv9c0x20m4c")))

