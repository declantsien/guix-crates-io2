(define-module (crates-io qu ic quickdry) #:use-module (crates-io))

(define-public crate-quickdry-0.1.0 (c (n "quickdry") (v "0.1.0") (h "0j6rd9hlkx0plrm51vmv69fj1ghrkl5qqslz1g6bxrnmcj3vlbdi")))

(define-public crate-quickdry-0.1.1 (c (n "quickdry") (v "0.1.1") (h "04npnkm103sa2maaq930gpwkzxyzir7d2cqpfshpyc0pchdq4pp6")))

(define-public crate-quickdry-0.1.2 (c (n "quickdry") (v "0.1.2") (h "1w4b9raamid544wim28cip6sr4pm7gbacl6sy4kb78y1m136qgvg")))

