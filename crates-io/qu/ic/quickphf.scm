(define-module (crates-io qu ic quickphf) #:use-module (crates-io))

(define-public crate-quickphf-0.1.0 (c (n "quickphf") (v "0.1.0") (d (list (d (n "quickdiv") (r "^0.1.1") (d #t) (k 0)) (d (n "wyhash") (r "^0.5.0") (d #t) (k 0)))) (h "0r5yrbr8g97mnmn95ghlvdfk8552xpydgd1s5psg2xizzl0xv178") (r "1.56")))

