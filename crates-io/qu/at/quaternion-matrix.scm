(define-module (crates-io qu at quaternion-matrix) #:use-module (crates-io))

(define-public crate-quaternion-matrix-0.0.1 (c (n "quaternion-matrix") (v "0.0.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "184gimd45r8r6xix6zw9rhqm9c5aaklhbvk7z3s03qlzfl5c631j") (y #t)))

(define-public crate-quaternion-matrix-0.0.2 (c (n "quaternion-matrix") (v "0.0.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "03zqb8f13hnv95ddb0sg0f378irsx3kid2pzhha8qcla4zypzhcb") (y #t)))

(define-public crate-quaternion-matrix-0.0.3 (c (n "quaternion-matrix") (v "0.0.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "115p09hj7p3mnycx3hwdsypj2ggmknrhds8ikfzv1ywzwxm77wr6") (y #t)))

(define-public crate-quaternion-matrix-0.0.4 (c (n "quaternion-matrix") (v "0.0.4") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0cz73vg84b6xnnxdq3rky911cn913dzabf7h6fjblz9v5d3jlb6l") (y #t)))

(define-public crate-quaternion-matrix-0.0.5 (c (n "quaternion-matrix") (v "0.0.5") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1yg7q4vfzfzbipp8p9zz0vv9skz6w9bv8r99njn4pbbbryx8lsi2") (y #t)))

(define-public crate-quaternion-matrix-0.0.6 (c (n "quaternion-matrix") (v "0.0.6") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "12lqxdi1q5fhaavydj150a2lxr80qizllgvx2bqrr2392lf943k6") (y #t)))

(define-public crate-quaternion-matrix-0.0.7 (c (n "quaternion-matrix") (v "0.0.7") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "15b7mqqsvvdfp2d5m3n25zrlcdvsic8rwz6y126gy082rnz5fbg9") (y #t)))

(define-public crate-quaternion-matrix-0.0.8 (c (n "quaternion-matrix") (v "0.0.8") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0zgn2jfp876f5nl7q88nf2ni5vbxwfps4hn38c0v8cncwqclpysl") (y #t)))

(define-public crate-quaternion-matrix-0.1.1 (c (n "quaternion-matrix") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1js1hi3hzgnfalhxpbhgawvqxngqmrj0r667fkv19n62y6agy9vh") (y #t)))

(define-public crate-quaternion-matrix-0.1.2 (c (n "quaternion-matrix") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0j1r4p1jbfi0gyizqaxrs6l0wdhzif362cnmgcj4m9mja1b6bsbc") (y #t)))

(define-public crate-quaternion-matrix-0.1.3 (c (n "quaternion-matrix") (v "0.1.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1rhnq3yblr5h9pnnwf6iqabfi3mjg5wgpgr9icsg8npahs4x02x0") (y #t)))

(define-public crate-quaternion-matrix-0.1.4 (c (n "quaternion-matrix") (v "0.1.4") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1spv13cjl5r19saag72vfa16zf8d6vivs35c4rbj0w5hczdp358m")))

