(define-module (crates-io qu at quaternion) #:use-module (crates-io))

(define-public crate-quaternion-0.0.0 (c (n "quaternion") (v "0.0.0") (d (list (d (n "vecmath") (r "^0.0.3") (d #t) (k 0)))) (h "13c1flpsw9zwr4rhnnns8kcrlmf8f7k4cv8fvvwj862skn8h5nc8")))

(define-public crate-quaternion-0.0.1 (c (n "quaternion") (v "0.0.1") (d (list (d (n "vecmath") (r "^0.0.3") (d #t) (k 0)))) (h "0dgvxs8shgvx0yci02mxqh1ilfa4i3f980ywmbg6lh92qg2fj3j3")))

(define-public crate-quaternion-0.0.2 (c (n "quaternion") (v "0.0.2") (d (list (d (n "vecmath") (r "^0.0.5") (d #t) (k 0)))) (h "0lh1jla1qblswcizyrf0zjc492dn56bxpxh2jf3mrs4b13m2niw0")))

(define-public crate-quaternion-0.0.5 (c (n "quaternion") (v "0.0.5") (d (list (d (n "vecmath") (r "^0.0.5") (d #t) (k 0)))) (h "1snmvjqfl5vmssqzkhwf5s8vmqxz0qai5kii10y67x08q4szc8sb")))

(define-public crate-quaternion-0.0.6 (c (n "quaternion") (v "0.0.6") (d (list (d (n "vecmath") (r "^0.0.22") (d #t) (k 0)))) (h "1kfjcz6g669m4js26dwwia22r88q0hnkldyn04ihk9d370n73f20")))

(define-public crate-quaternion-0.0.7 (c (n "quaternion") (v "0.0.7") (d (list (d (n "vecmath") (r "^0.0.23") (d #t) (k 0)))) (h "10sj4ipsl9gq65f4y70h956c7d00r2mw7wgilcpic5874b6b77rv")))

(define-public crate-quaternion-0.1.0 (c (n "quaternion") (v "0.1.0") (d (list (d (n "vecmath") (r "^0.1.0") (d #t) (k 0)))) (h "1d7q8ns95cybb27xcgz8yilss675hpbcbw5305k0mr6ijajli8jj")))

(define-public crate-quaternion-0.2.0 (c (n "quaternion") (v "0.2.0") (d (list (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "11s0y0370krddgl3f2999g85nwkacxwaa0qdsqa0vc64v34xgaw0")))

(define-public crate-quaternion-0.2.1 (c (n "quaternion") (v "0.2.1") (d (list (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "0k5y57ykc4d9gs38wqdwmmds4avd7gjrfjabm27xb18b272syv6g")))

(define-public crate-quaternion-0.3.0 (c (n "quaternion") (v "0.3.0") (d (list (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0nbq4yqz5p73cc9i6h7ax3gxpp74yyphpbg8a93b4q32i6n0gdg1")))

(define-public crate-quaternion-0.3.1 (c (n "quaternion") (v "0.3.1") (d (list (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0xg66jyp83xawdsk7i52k40id0w14ffzcm9rwl060vkpw9l82ngm")))

(define-public crate-quaternion-0.4.0 (c (n "quaternion") (v "0.4.0") (d (list (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "19prb9xdml2j9amz3nd4x3a3xdhivz6f0p4w2fxbdnzm864dk74y")))

(define-public crate-quaternion-0.4.1 (c (n "quaternion") (v "0.4.1") (d (list (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "05n2v25v3w4jhv3hfadmk5vgrndrbhlp865a4n2kci8ldlil7109")))

(define-public crate-quaternion-1.0.0 (c (n "quaternion") (v "1.0.0") (d (list (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "1cy06x3sysk953pfj6c3xp5xfayz4czvf663ip4pckrxrysgsa7j")))

