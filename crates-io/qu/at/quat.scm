(define-module (crates-io qu at quat) #:use-module (crates-io))

(define-public crate-quat-0.1.0 (c (n "quat") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec4") (r "^0.1") (d #t) (k 0)))) (h "0fs40kx4fjlk472kcaw9jxzxrf1fk4ib1sl96akif2hn1hpii4fh")))

(define-public crate-quat-0.1.1 (c (n "quat") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec4") (r "^0.1") (d #t) (k 0)))) (h "1jw3qfxn3n7c4k90h6v99b4s6wyfvjzf07nhjy42j3h0dsh8spp6")))

(define-public crate-quat-0.1.2 (c (n "quat") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec4") (r "^0.1") (d #t) (k 0)))) (h "1zl583nihi6j3ff6mx5cynlcbq09y2xfpn96nfa11jxc682xnlxr")))

(define-public crate-quat-0.2.0 (c (n "quat") (v "0.2.0") (d (list (d (n "cast_trait") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec4") (r "^0.2") (d #t) (k 0)))) (h "1lpn08blsdch3d04yf7gay4nbaifb054mgrlfqfj4hbai6dq3mjc")))

(define-public crate-quat-0.2.1 (c (n "quat") (v "0.2.1") (d (list (d (n "cast_trait") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec4") (r "^0.2") (d #t) (k 0)))) (h "1lp2xl2v87arp3xfjyn1sy8ig336j44ya1c1k87wspc9gd5c7ams")))

