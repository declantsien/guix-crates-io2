(define-module (crates-io qu at quaternion-core) #:use-module (crates-io))

(define-public crate-quaternion-core-0.1.0 (c (n "quaternion-core") (v "0.1.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "09clgij883f3zjvc6bbnh9kgz27ljqrmqhrrpi8hlf1mz47izgbi") (f (quote (("std" "num-traits/std") ("simd") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.2.0 (c (n "quaternion-core") (v "0.2.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "14dw6wm5z7d4zjx7vd92484837mrv1205xl98v2a7iz3n6r372j2") (f (quote (("std" "num-traits/std") ("simd") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.2.1 (c (n "quaternion-core") (v "0.2.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0f4fav3q0n8m90q0accpf5dcfw6rdb18l0ahybvricz73f7hrwll") (f (quote (("std" "num-traits/std") ("simd") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3.0 (c (n "quaternion-core") (v "0.3.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0n3yl1isvdkz4sl41ndj7rz94hci9jnzvdj4sha90piv7jyfxf5z") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3.1 (c (n "quaternion-core") (v "0.3.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "04vax1svsbyrifqks1hxrjy350srg7ax5219ld6zw0pp31mvmziy") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3.2 (c (n "quaternion-core") (v "0.3.2") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1i9jr4yj03b40djkwv3q46mivfk3wqhghrv0z3qqw2kfb3cjcv2k") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3.3 (c (n "quaternion-core") (v "0.3.3") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1xnqz7akfxkp2cc4l05v0d555yx0xbi5njm3ysn1bgray2n0zl4f") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3.4 (c (n "quaternion-core") (v "0.3.4") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0k9z5n8srv4s0nx3vrwm9496pv3hn8wp2zi91q8jz2f1j4q7zlp4") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3.5 (c (n "quaternion-core") (v "0.3.5") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1syismbzr36bwdqyhlv69aal938f03l082gkz82shvn80v7l8lcv") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.4.0 (c (n "quaternion-core") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0hmiygsii334gwnp33n8bh252y1r3shv1rgjqsy2qxi7g2nlxnmb") (f (quote (("std" "num-traits/std") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std")))) (y #t)))

(define-public crate-quaternion-core-0.4.1 (c (n "quaternion-core") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1bbzdxnqk2pcz1yr0p2fhr7wan9b0jzvjm85qwij3162mzyy8vh4") (f (quote (("std" "num-traits/std") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std")))) (y #t)))

(define-public crate-quaternion-core-0.4.2 (c (n "quaternion-core") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0s9wp8fg8bdz6584avffdlv98qkk4s3f9by497war30g8xwxmsk5") (f (quote (("std" "num-traits/std") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.4.3 (c (n "quaternion-core") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2.17") (k 0)))) (h "02dry8bwhbmm1rqrxgqkiyss8bm2vgisxp3i6zmy1bbscnkmgfpr") (f (quote (("std" "num-traits/std") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.5.0 (c (n "quaternion-core") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2.17") (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k8yj588wz10li3pbwz45ng0l50kjkhxbsk6rrjsy78ym7scqs4g") (f (quote (("std" "num-traits/std") ("serde-serialize" "serde") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

