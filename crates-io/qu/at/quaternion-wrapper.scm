(define-module (crates-io qu at quaternion-wrapper) #:use-module (crates-io))

(define-public crate-quaternion-wrapper-0.1.0 (c (n "quaternion-wrapper") (v "0.1.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quaternion-core") (r "^0.1.0") (k 0)))) (h "05chj6vcm2yg1n9i0fq3b3x2jz27vj6h0m9n7q3h8jf75kxb7gz7") (f (quote (("std" "quaternion-core/std" "num-traits/std") ("simd" "quaternion-core/simd") ("libm" "quaternion-core/libm" "num-traits/libm") ("fma" "quaternion-core/fma") ("default" "std"))))))

(define-public crate-quaternion-wrapper-0.1.1 (c (n "quaternion-wrapper") (v "0.1.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quaternion-core") (r "^0.1.0") (k 0)))) (h "19fb37rr9yxdymqsfkmhvrm4dnw03n0w9hb19x42whba637ff989") (f (quote (("std" "quaternion-core/std" "num-traits/std") ("simd" "quaternion-core/simd") ("libm" "quaternion-core/libm" "num-traits/libm") ("fma" "quaternion-core/fma") ("default" "std"))))))

(define-public crate-quaternion-wrapper-0.2.0 (c (n "quaternion-wrapper") (v "0.2.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "quaternion-core") (r "^0.3.0") (k 0)))) (h "0n7kj6s8gic7cxi9f6d00079wv00lxs48514lrg9rq8ybbmzzx30") (f (quote (("std" "quaternion-core/std" "num-traits/std") ("libm" "quaternion-core/libm" "num-traits/libm") ("fma" "quaternion-core/fma") ("default" "std"))))))

(define-public crate-quaternion-wrapper-0.3.0 (c (n "quaternion-wrapper") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "quaternion-core") (r "^0.4.2") (k 0)))) (h "1h6r93nzr2d1lvl909jg42z0mzp8qhi1khr43q8656c9fjvvdya5") (f (quote (("std" "quaternion-core/std" "num-traits/std") ("norm-sqrt" "quaternion-core/norm-sqrt") ("libm" "quaternion-core/libm" "num-traits/libm") ("fma" "quaternion-core/fma") ("default" "std"))))))

