(define-module (crates-io qu at quaternion_averager) #:use-module (crates-io))

(define-public crate-quaternion_averager-0.1.0 (c (n "quaternion_averager") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "12qxh7kmj16gr25dmnqg7ak8klvinfwmk35g4j8w36gblld6mvd1")))

(define-public crate-quaternion_averager-0.1.1 (c (n "quaternion_averager") (v "0.1.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "1w1d9zmylyidd2sxrzbhzlmznn8cj5vni8pir8pyjy8hgfjnjcrc")))

(define-public crate-quaternion_averager-0.1.2 (c (n "quaternion_averager") (v "0.1.2") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)))) (h "0sd6j7cn46j8mcfhczb2kw7b65shyild1bg0cmc11jb0h79vp6rn")))

(define-public crate-quaternion_averager-0.1.3 (c (n "quaternion_averager") (v "0.1.3") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)))) (h "1g5215miffwygpnvk1fvylpgwm5nwzafjm62zaix91fcz8j4jn92")))

