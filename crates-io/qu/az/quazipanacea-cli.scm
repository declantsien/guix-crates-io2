(define-module (crates-io qu az quazipanacea-cli) #:use-module (crates-io))

(define-public crate-quazipanacea-cli-0.1.0 (c (n "quazipanacea-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "1iqxpxzbvh6012sypj7cw8ccinm5wrjfqjyv2vzr3r9mlzsdam7c")))

(define-public crate-quazipanacea-cli-0.1.1 (c (n "quazipanacea-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0mfyz6mfg3sj71nr3bzz9z6a49ygn48khds4lf32hk7fj3bwd08a")))

(define-public crate-quazipanacea-cli-0.1.2 (c (n "quazipanacea-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0agvihjvlrh8sx43kyzmjiqq65dj8xdg76cq440rrb6l82bk9rmc")))

