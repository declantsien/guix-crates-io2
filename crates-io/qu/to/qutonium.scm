(define-module (crates-io qu to qutonium) #:use-module (crates-io))

(define-public crate-qutonium-0.1.0 (c (n "qutonium") (v "0.1.0") (h "01cmji54jacx5n65wqfly9f50dapfxxbad7h6z4h7qbrvcm589ad")))

(define-public crate-qutonium-0.0.0 (c (n "qutonium") (v "0.0.0") (h "0h8wyc461q20schvr3987sazizv9wdrzchjjxlp038gd9g720mw3")))

(define-public crate-qutonium-0.1.1 (c (n "qutonium") (v "0.1.1") (d (list (d (n "miniserde") (r "^0.1.13") (d #t) (k 0)) (d (n "qute") (r "^0.0.11") (d #t) (k 0)))) (h "03dz574z43iyy3siba96i6s73wf05ri4cy5l6ggrkl1irx373pgz")))

(define-public crate-qutonium-0.1.2 (c (n "qutonium") (v "0.1.2") (d (list (d (n "miniserde") (r "^0.1.13") (d #t) (k 0)) (d (n "qute") (r "^0.0.11") (d #t) (k 0)))) (h "0ci5ibyrjk43xw2iq9krliw6pm51izf9yzwh0ppij3hhw1dg4af4")))

(define-public crate-qutonium-0.1.3 (c (n "qutonium") (v "0.1.3") (d (list (d (n "miniserde") (r "^0.1.13") (d #t) (k 0)) (d (n "qute") (r "^0.0.11") (d #t) (k 0)))) (h "0jfqrjx18r5ssj24bl499ppkgnrcrqk50vw38hsx583gax44pfaj")))

(define-public crate-qutonium-0.1.4 (c (n "qutonium") (v "0.1.4") (d (list (d (n "miniserde") (r "^0.1.13") (d #t) (k 0)) (d (n "qute") (r "^0.0.11") (d #t) (k 0)))) (h "0aykg8v6b3mh3m2vpaya1xjlfmvbhdaf1w432fyikik1vsvgjnpq")))

(define-public crate-qutonium-0.1.5 (c (n "qutonium") (v "0.1.5") (d (list (d (n "miniserde") (r "^0.1.13") (d #t) (k 0)) (d (n "qute") (r "^0.0.11") (d #t) (k 0)))) (h "1jlb09qdpmkbf7vs7sxhpjlnzanv8c2z275nxay39xi3mx5zv65z")))

(define-public crate-qutonium-0.1.6 (c (n "qutonium") (v "0.1.6") (d (list (d (n "miniserde") (r "^0.1.13") (d #t) (k 0)) (d (n "qute") (r "^0.0.11") (d #t) (k 0)))) (h "199bkhf534z4yslvzss4xm0w5q2m6ylbhb6qdzjmxlsm572m54ww")))

