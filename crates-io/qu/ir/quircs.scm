(define-module (crates-io qu ir quircs) #:use-module (crates-io))

(define-public crate-quircs-0.9.0 (c (n "quircs") (v "0.9.0") (d (list (d (n "image") (r "^0.23.4") (d #t) (k 2)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0yg2pvpqdnppdz9d9bap0snwx6wmlblm6rfrx7b45ygriw10zafg")))

(define-public crate-quircs-0.9.1 (c (n "quircs") (v "0.9.1") (d (list (d (n "image") (r "^0.23.4") (d #t) (k 2)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "05hzac6blrj9y7m2j14qx7vdx7i2h8ab1rqwl8ba1rl9bvi03wx8")))

(define-public crate-quircs-0.10.0 (c (n "quircs") (v "0.10.0") (d (list (d (n "image") (r "^0.23.4") (d #t) (k 2)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0jlzcw46kvzyq18jpg74afkq7fly0z3vyzc6a1h1hxw2p27d3308")))

(define-public crate-quircs-0.10.1 (c (n "quircs") (v "0.10.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "19gc2ygbh3kck9r50zb6s0dz7yn3invxp6gfwh6g29g5kyma5936")))

(define-public crate-quircs-0.10.2 (c (n "quircs") (v "0.10.2") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "094x6dxbnyfw6h7zkwqbgyj7ha0m9m0d8ki4ls99c4bszj8fqgzc")))

