(define-module (crates-io qu ir quirs) #:use-module (crates-io))

(define-public crate-quirs-0.1.0 (c (n "quirs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "lodepng") (r "^2.4.2") (d #t) (k 2)))) (h "0860s6xainf3ya2v4q111cdzmwlk1lph5sd8hqbc1kf80425l44b")))

(define-public crate-quirs-0.1.1 (c (n "quirs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "lodepng") (r "^2.4.2") (d #t) (k 2)))) (h "1minbmax9wxfjbmj0xz0mfiyigrxmcl2l8p5ghi5978m8q6ljgd6")))

