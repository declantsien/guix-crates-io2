(define-module (crates-io qu ir quiren) #:use-module (crates-io))

(define-public crate-quiren-0.2.0 (c (n "quiren") (v "0.2.0") (d (list (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "main_error") (r "^0.1.1") (d #t) (k 0)) (d (n "osstrtools") (r "^0.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dkiss6xlnh86d5cqjg2dwx0gc7zic50sz5yjngarxp3y30qavz4")))

