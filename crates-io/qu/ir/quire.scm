(define-module (crates-io qu ir quire) #:use-module (crates-io))

(define-public crate-quire-0.1.0 (c (n "quire") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jnkpk79vl127kqlxdhg1bxxk543xqz5m9w6l4i94gjwhlgsz7zn")))

(define-public crate-quire-0.1.1 (c (n "quire") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0v7i5mv40zw3pccdfvw5l6zzg317jnspahdgcbmiw2882hsc17qn")))

(define-public crate-quire-0.1.2 (c (n "quire") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^0.1.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "14nymx1zcipnk01zs2kd2fns0szg00ns6b8kd8ic1bjipdb93gc5") (y #t)))

(define-public crate-quire-0.1.3 (c (n "quire") (v "0.1.3") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^0.1.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1fac0gxlgva3w969klvvr0dwy9bs236srmhn4hl6b5b5icshswdh")))

(define-public crate-quire-0.1.4 (c (n "quire") (v "0.1.4") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^0.1.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0g8zqzkx9dx82zzwzx1m08f6qmhi9zd51r84y06s0k6i2wm57v53")))

(define-public crate-quire-0.1.5 (c (n "quire") (v "0.1.5") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1jqaf8llhyp4n9a9gw56hldzaw2wkxia8mg6c64rwi0nljsgpyj1")))

(define-public crate-quire-0.1.6 (c (n "quire") (v "0.1.6") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0yp7dblmbviqns34jk20jwa9s8224n4nrgmmxf78yha45yrvbgvl") (y #t)))

(define-public crate-quire-0.1.7 (c (n "quire") (v "0.1.7") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1nhr69cfxj9xg4nqlszasv66v3dg0c1k5x3am3323f4ih92vzmly")))

(define-public crate-quire-0.1.8 (c (n "quire") (v "0.1.8") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "019m7jmhz5gm3ild8r0md9q95xb1673pjql7m7l14sagc6iipvcl")))

(define-public crate-quire-0.2.0 (c (n "quire") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "humannum") (r "^0.1.0") (d #t) (k 0)) (d (n "humantime") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1rl8jbsyvaaqvcd45vflsa26wmqbacwm8mxr65s9k0abq0j0hrwc")))

(define-public crate-quire-0.2.1 (c (n "quire") (v "0.2.1") (d (list (d (n "humannum") (r "^0.1.0") (d #t) (k 0)) (d (n "humantime") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0d2nz1qvprrajgfhh22nc8y3879yqnnx0443zc4fzhqjs22msgwv")))

(define-public crate-quire-0.2.2 (c (n "quire") (v "0.2.2") (d (list (d (n "humannum") (r "^0.1.0") (d #t) (k 0)) (d (n "humantime") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0gh5p07rkcdngpaybv01fmxn79jj1j2pg33xc48nh65vb53sanxg")))

(define-public crate-quire-0.2.3 (c (n "quire") (v "0.2.3") (d (list (d (n "humannum") (r "^0.1.0") (d #t) (k 0)) (d (n "humantime") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1zn368xslzvbwgjh5xin22xdswrclz2acx6y8ic817wjm3jnaibx")))

(define-public crate-quire-0.3.0 (c (n "quire") (v "0.3.0") (d (list (d (n "humannum") (r "^0.1.0") (d #t) (k 0)) (d (n "humantime") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 2)))) (h "19v4m04gx0r9cf011p9v00rgvdqsiirqiw32yacsihy1j17m5vxv") (f (quote (("regex_expressions" "regex") ("default" "regex_expressions"))))))

(define-public crate-quire-0.3.1 (c (n "quire") (v "0.3.1") (d (list (d (n "humannum") (r "^0.1.0") (d #t) (k 0)) (d (n "humantime") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 2)))) (h "196nsanr1ygzcacaj6j3glsip98ks7fnsyw05gl9w8ks0c51s8mb") (f (quote (("regex_expressions" "regex") ("default" "regex_expressions"))))))

(define-public crate-quire-0.4.0 (c (n "quire") (v "0.4.0") (d (list (d (n "humannum") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 2)) (d (n "serde-transcode") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 2)))) (h "077s59sbr42akkz6rzrvc7sv24c3qjdrknp66npfsyc4s26sm1g9")))

(define-public crate-quire-0.4.1 (c (n "quire") (v "0.4.1") (d (list (d (n "humannum") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 2)) (d (n "serde-transcode") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 2)))) (h "0n0r12gqivj2fxwim7rqddv9mfq4km4qqgz3j3bhk77h2hdva2gz")))

