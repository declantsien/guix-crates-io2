(define-module (crates-io qu ac quackie) #:use-module (crates-io))

(define-public crate-quackie-0.1.0 (c (n "quackie") (v "0.1.0") (d (list (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)))) (h "10rzfb2imb3nc9d6icl9fgga9xavlfg96l3iklxjlmklxdv77w3p")))

(define-public crate-quackie-0.2.0 (c (n "quackie") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)))) (h "0qmqmifk5j93llzd3di4mfzwx4dvrnyjcvy6wkcnv58gvwnwmff3") (y #t)))

(define-public crate-quackie-0.2.1 (c (n "quackie") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)))) (h "0cj3wn37nbl5m1197bkyf5zms2akkj8v0nyjcbfykbb1pj8jxq5c")))

