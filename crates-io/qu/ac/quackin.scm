(define-module (crates-io qu ac quackin) #:use-module (crates-io))

(define-public crate-quackin-0.1.0 (c (n "quackin") (v "0.1.0") (d (list (d (n "csv") (r "^0.15.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "0bcg966kdmah7a8m01jbxm34zyix3bhy0adsqgnkq1jj4lgrs4pk") (y #t)))

(define-public crate-quackin-0.1.1 (c (n "quackin") (v "0.1.1") (d (list (d (n "csv") (r "^0.15.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "sprs") (r "^0.4.0") (d #t) (k 0)))) (h "1dfggahppznjrkhs9vzmapgar8xxq48zlg4xf0blbbgh0w6h1a1z")))

