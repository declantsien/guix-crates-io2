(define-module (crates-io qu ac quack-gh) #:use-module (crates-io))

(define-public crate-quack-gh-0.1.0 (c (n "quack-gh") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0cxz2py8npp33ybd52v5g7fbckndilk8jvx8qgaawpalzzxs0bp6")))

(define-public crate-quack-gh-0.1.1 (c (n "quack-gh") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0bnsnr9dpsj8qchxvavc741l6015g9cx7jpsgr0fqnjp4asmwfja")))

(define-public crate-quack-gh-0.1.2 (c (n "quack-gh") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1ra4cc7pv8s1mlbhrin35fcnds6jig2rp3acjw1fx0jyss4lhj59")))

(define-public crate-quack-gh-0.1.3 (c (n "quack-gh") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0wlc75sj2dmxkbwh5av9a75la3xwqyzpfxsbpw3dx5ph5qvfzm33")))

