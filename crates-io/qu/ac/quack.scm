(define-module (crates-io qu ac quack) #:use-module (crates-io))

(define-public crate-quack-0.0.0 (c (n "quack") (v "0.0.0") (h "0b10kgysqi8kr70j89njg359zxpyk4l5azd147dljcly60cvmii6")))

(define-public crate-quack-0.0.1 (c (n "quack") (v "0.0.1") (h "0xmc322mnj2zmykb9xr0ayqqxia5b4nzywimhk7c8al51l29i6cj")))

(define-public crate-quack-0.0.2 (c (n "quack") (v "0.0.2") (h "026zvsxw2hyga93p7p7knh2y11rpsa13l0y4abs0gjgiia7sy89b")))

(define-public crate-quack-0.0.3 (c (n "quack") (v "0.0.3") (h "1c3jg920swsib6ly70mq46wy7daj58j9cmq0jggfqqbynw0phlcx")))

(define-public crate-quack-0.0.4 (c (n "quack") (v "0.0.4") (h "0hkby7q42rcggngri6sy3fpa52ys1hwx96z8v23dw4gli40m0xlw")))

(define-public crate-quack-0.0.5 (c (n "quack") (v "0.0.5") (h "04cxhvkkmd1is4qn317ls4qgw3li06hah5vgwcx8x4hnn7myy77n")))

(define-public crate-quack-0.0.6 (c (n "quack") (v "0.0.6") (h "05aixq5sanp1c26rscqd268ic5js85q0rrvrr38gy8qjxyyc0qs8")))

(define-public crate-quack-0.0.7 (c (n "quack") (v "0.0.7") (h "08a4lh3dmz766xcwlvjv7mi6vijh6fqc3pwwmls8sg4nbbn3n182")))

(define-public crate-quack-0.0.8 (c (n "quack") (v "0.0.8") (h "1b5jxw9996pskfm70hx72mdrydzv47aya67wasp38xp9p3jbc4ns")))

(define-public crate-quack-0.0.9 (c (n "quack") (v "0.0.9") (h "0j41h9fmis2374296l037xxjg790akv23n3ggyi6hbiwdfp377bm")))

(define-public crate-quack-0.0.10 (c (n "quack") (v "0.0.10") (h "0s4cxxgc3yk2v7h8g43fhdpirpkgh9ysxpk2g0q3dd61qgsmgkyc")))

(define-public crate-quack-0.0.11 (c (n "quack") (v "0.0.11") (h "0cs6h0dr3x8xhl1v2r1vrjryb0c36xqjjgfchy90hblqglggv4h2")))

(define-public crate-quack-0.0.12 (c (n "quack") (v "0.0.12") (h "0vm8hlp961zlhib4w4wvjz969j9xq55sik0bankiwm5l1y3hhlm2")))

(define-public crate-quack-0.0.13 (c (n "quack") (v "0.0.13") (h "0lqnmfkmlwqfg6jlxq4n1q8my9p8mwzb025dq29d22s6dbvhyvbf")))

(define-public crate-quack-0.1.0 (c (n "quack") (v "0.1.0") (h "157hyk4nxi0nl6q1nxpfj7jd6yf5yq5nrcrdqjvlqkjaf61gnxsg")))

