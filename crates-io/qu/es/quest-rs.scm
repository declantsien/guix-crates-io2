(define-module (crates-io qu es quest-rs) #:use-module (crates-io))

(define-public crate-quest-rs-0.2.0 (c (n "quest-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1gqq95nh915rf8wgb80c6rny6ri9pwrcxhzbxfpagk3y6qbd53r2")))

(define-public crate-quest-rs-0.2.5 (c (n "quest-rs") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0yifsc1xsahax72anf2hgdjc6scjhnw1aykmz2blcx31m9whqx20")))

(define-public crate-quest-rs-0.2.7 (c (n "quest-rs") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0sh39s2ql1zqrvpm5m8gh7xpjd4rrridm52gfihdb0d1sl98ky9n")))

(define-public crate-quest-rs-0.2.8 (c (n "quest-rs") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "066cqf6j3nwnix7ds7kr46kly8wyjmpgvmr384qnxzz32cfm5p79")))

