(define-module (crates-io qu es question) #:use-module (crates-io))

(define-public crate-question-0.1.0 (c (n "question") (v "0.1.0") (h "0z4ndngc8fk518y0bb37xgj4jryh5nbskhq8077ing7www80c7h8")))

(define-public crate-question-0.2.0 (c (n "question") (v "0.2.0") (h "19xg6rzrj56i2gslh4b11dsdmagr47blgm0yrjqdzsn8rdl8yrdm")))

(define-public crate-question-0.2.1 (c (n "question") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0.169") (o #t) (d #t) (k 0)))) (h "0ik15ydw8dni9b2jbfg9b6avl3az20fljklrdcj3sg0x2zvidkfq") (f (quote (("strict" "clippy") ("default"))))))

(define-public crate-question-0.2.2 (c (n "question") (v "0.2.2") (d (list (d (n "clippy") (r "^0.0.169") (o #t) (d #t) (k 0)))) (h "13gsclz5649g5cjd8h0ky9c1n020rzr3fikiksw8m6lggbg3xfxc") (f (quote (("strict" "clippy") ("default"))))))

