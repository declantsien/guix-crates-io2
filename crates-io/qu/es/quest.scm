(define-module (crates-io qu es quest) #:use-module (crates-io))

(define-public crate-quest-0.1.0 (c (n "quest") (v "0.1.0") (d (list (d (n "rpassword") (r "^2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "12v2619bkdq5kx69pwnzh2yi0cxvafn1bjlxmlrinzdbm2gcih15")))

(define-public crate-quest-0.1.1 (c (n "quest") (v "0.1.1") (d (list (d (n "rpassword") (r "^2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "19hz7jb4y48iwll2qw1y8n7b8bnxra221d0nn0h2313z5zxp77sy")))

(define-public crate-quest-0.2.0 (c (n "quest") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "rpassword") (r "^2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mf7dy4088rw6c4bw4ywmkprksqhjsjwak76601nvxq9vf1mf4zm")))

(define-public crate-quest-0.3.0 (c (n "quest") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "rpassword") (r "^2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1n2rzql4dcchdp2wrhqvapk7i5wglf0yalspyh9yx8jkr7szasjm")))

