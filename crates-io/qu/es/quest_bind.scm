(define-module (crates-io qu es quest_bind) #:use-module (crates-io))

(define-public crate-quest_bind-0.3.0 (c (n "quest_bind") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xf651v3kynvhn21hawfrswjdx11zymswxc102rbi5pfsgxiwcrp") (f (quote (("mpi") ("gpu") ("f32"))))))

(define-public crate-quest_bind-0.3.1 (c (n "quest_bind") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "016gxkfisx3ifxl5glkk2b53g170gjc0hwzgrg4icw8iii5gxwy7") (f (quote (("mpi") ("gpu") ("f32"))))))

(define-public crate-quest_bind-0.3.2 (c (n "quest_bind") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bxzmcncgjqjzv9bbx44rhxl8y4j67n18x6099vwpnmslkipxw4p") (f (quote (("mpi") ("gpu") ("f32"))))))

(define-public crate-quest_bind-0.3.3 (c (n "quest_bind") (v "0.3.3") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "001k4zbvqz0jcbj95krn39cagxcwvibwi14vsnrwzmbd7qf6pd3a") (f (quote (("mpi") ("gpu") ("f32"))))))

(define-public crate-quest_bind-0.3.4 (c (n "quest_bind") (v "0.3.4") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yvks33y3m4xm5vqk88vfn0ndz49b7x3nb95mpy4as6x0pvnpnaw") (f (quote (("mpi") ("gpu") ("f32"))))))

(define-public crate-quest_bind-0.3.5 (c (n "quest_bind") (v "0.3.5") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hny4zbl2n38dd1cz2g4ddwgapwpcp9hgbk3k20fjkhka9694kxv") (f (quote (("mpi") ("gpu") ("f32"))))))

(define-public crate-quest_bind-0.3.6 (c (n "quest_bind") (v "0.3.6") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15g5m2ry1nnx64sca13csq2ilh5hrcv0jyf822vndsgnaxxrhmqn") (f (quote (("mpi") ("gpu") ("f32"))))))

(define-public crate-quest_bind-0.3.7 (c (n "quest_bind") (v "0.3.7") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y8wbs1sk3n3v72647s0yqy9fw7i4lqcj3n6l8dmg4liswr61ggk") (f (quote (("mpi") ("gpu") ("f32"))))))

