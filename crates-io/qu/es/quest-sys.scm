(define-module (crates-io qu es quest-sys) #:use-module (crates-io))

(define-public crate-quest-sys-0.1.0 (c (n "quest-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1avf45f0gz4p6i24r9jyqrsa6f667wzrgvprfcqwsm0zcc6r6cgx") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.1.1 (c (n "quest-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1wsfv06mxg6sw49i97fxkbf05d500qydz7a6jay4720ra74q5f4v") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.2.0 (c (n "quest-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "14468qcna00pdh3q0mzpfvn88ibj83j65zm3jgpwi2n8znlarx1z") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.3.0 (c (n "quest-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "02vrmlgczcrz11vr3gjdxn32as56p5d3r34vx86sn2qrazsvjmza") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.4.0 (c (n "quest-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1q9zpf8pqvfbrsl8k1zc7ljsms9kfwmcf8jwnsr7ky5507qhc5x6") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.5.0 (c (n "quest-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1hk9jmyhl34b8ilanxg2jh757hwfp1v2arzi5xp4c0vipf3xs3n1") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.6.0 (c (n "quest-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0fbcswj7lg0gqz7yh0b1nrxn27km63afz6xkdv2hb55cb08v9qdi") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.6.1 (c (n "quest-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0v87iy9h3ia4x4l5za8qwfxr3w6n0lh2xqji4zw9s4w0r0xz428i") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.7.0 (c (n "quest-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1ah43rr50xm0vj6978f0wlim1pqaxgcikjcdxmmac559fabwp5sf") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.7.1 (c (n "quest-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0f5yv8hvpvcn0g7vi6472h0hf24052dyxgcxh2a9d8b407y4dka7") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.7.2 (c (n "quest-sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1lngbgaywrpr54fsqsfnfaf1xcfbq9hxjy8xlk72l0n8vkxm5bmp") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.7.3 (c (n "quest-sys") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1jbl0dnfhp6xykgfq1d5w5frygyrkbiblxmwjhk0zg7bcr9b5dg2") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.7.4 (c (n "quest-sys") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1s7c7f6jj214c9hipbz59h40jvjfhxvhzl0ppv1ldr9fzxcchmp0") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.8.0-beta.1 (c (n "quest-sys") (v "0.8.0-beta.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "05v1ykcmxbw4yn6bqh6rw80dsjl54b34s8rszmmkc1n2hzrk96wj") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.8.0-beta.2 (c (n "quest-sys") (v "0.8.0-beta.2") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1hl9zdwqpl4yc76nsgdllrmky30nvhg6jxjaaz035yyrckr7rz4y") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.8.0-beta.3 (c (n "quest-sys") (v "0.8.0-beta.3") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1faan2jigch3jl3jdxgnl92i3qwms6iypgd4x56vyng2vwswyvi1") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.8.0-beta.4 (c (n "quest-sys") (v "0.8.0-beta.4") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0a31mfpyalngnw8yhpp8mafk7si0k07prshr09vnbn0zvg798qb7") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.8.0-beta.5 (c (n "quest-sys") (v "0.8.0-beta.5") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1fv1z46bzapv7br7wj78khmna99vxyfyh1ggazqfdl45rjc6v06x") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.8.0 (c (n "quest-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1v17rgqh9mlhsydx0aa34pz15j0sy9mkfhg1sxbiji81ffak7cgx") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.8.1 (c (n "quest-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "151ff891ppnf2r5fwimd99z2dbciyjq7w12ybh687mixq1v833j4") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.9.0-alpha.1 (c (n "quest-sys") (v "0.9.0-alpha.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1rdj2y3ky2p6g3z24kl3mdy2gq18lj3qqh71v4xhkz2vn96gl6y5") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.9.0-alpha.2 (c (n "quest-sys") (v "0.9.0-alpha.2") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0zpagjyczyl3nxm70mngi7155pgk5nxrkwjlp1qf9i3i2fm0kgd0") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.8.2 (c (n "quest-sys") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0mjm54sflqxhh5225w65v0yzw8r05mxxa7s4p44600by33mnr8y1") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.9.0 (c (n "quest-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1z30y6747iadsswrc01agrysys5disgj7jgn5xp6rak9jmac2hv4") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.9.1 (c (n "quest-sys") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "02r9czs5nxvrkgwkn8d1j6329krhyk5hcnhky4fpkdfg735b2j6w") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.10.0 (c (n "quest-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "18xpghdq35y0qj98r4m530g9xwm1s0sls6pil55b34n8dj7b8pkw") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.10.1 (c (n "quest-sys") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "19k5w4zv2dqckx63prcsv5lfjy885xwrbmlbq675abhcrckb38yh") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.11.0 (c (n "quest-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1jrmhjbfs2am102lr5cw3j466syqac2303f8b81hfqjlxkili7r6") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.11.1 (c (n "quest-sys") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1lkpql0hp1w0z71igb22vw2ilsvwxdci2xwy727hd9ffcb0d4kiz") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.11.2 (c (n "quest-sys") (v "0.11.2") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "06b57gswvhv07d1b9pxivpk4qhbypq3rdz9lj8kc8rh7qa7347cv") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.11.3-alpha.1 (c (n "quest-sys") (v "0.11.3-alpha.1") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "15arf4069fdpp2c98sm5kwq4aanjyy1fmr52sdnsx5a0y9vww2y7") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.11.3 (c (n "quest-sys") (v "0.11.3") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0315hl6sqm5kq0a2dn5l93bh2098s6vbg2kjpf0f7kg5cab6my5q") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default")))) (r "1.56")))

(define-public crate-quest-sys-0.12.0-alpha.0 (c (n "quest-sys") (v "0.12.0-alpha.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1qq717bsnryrchbrx387zq0s6x23w2yiyx055qnga41snw99nyhi") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default") ("cuquantum") ("cuda")))) (r "1.60")))

(define-public crate-quest-sys-0.12.0 (c (n "quest-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1sxwivyiwk6nlqmhxafd6ci35srqymdszcmnm49l35wiphhpxrlw") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default") ("cuquantum") ("cuda")))) (r "1.60")))

(define-public crate-quest-sys-0.12.1 (c (n "quest-sys") (v "0.12.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "00y7xbwfz9jxiwqhpiv3zk23ba7xxdvbcw8nc1gvjnv9jydghmyw") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default") ("cuquantum") ("cuda")))) (r "1.60")))

(define-public crate-quest-sys-0.12.2 (c (n "quest-sys") (v "0.12.2") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0hhs5f8lm5zzpzj99c6w9xckbwn33in64qnw0azk2kxnzhz6bll2") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default") ("cuquantum") ("cuda")))) (r "1.60")))

(define-public crate-quest-sys-0.12.3 (c (n "quest-sys") (v "0.12.3") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0c0pqaj24bmqqwx4jh4i6z78bwfqqa7zs56370qkcpyc80gj8cix") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default") ("cuquantum") ("cuda")))) (r "1.60")))

(define-public crate-quest-sys-0.12.4 (c (n "quest-sys") (v "0.12.4") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1wb2pzryjkh5sg2hy7igl0iqz0kjvxwajzf0wxp1507zi6mdkn5f") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default") ("cuquantum") ("cuda")))) (r "1.60")))

(define-public crate-quest-sys-0.13.0 (c (n "quest-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "0hnl7dqlaxvfsva2g8h4pgn9nprbpkk6g0pg8sjj7aa8f89722pw") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default") ("cuquantum") ("cuda")))) (r "1.60")))

(define-public crate-quest-sys-0.13.1 (c (n "quest-sys") (v "0.13.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1sf8iacn3yb41yih251z7z5nrq5bdp9q4c6yy8q00n5ld36qnh61") (f (quote (("rebuild" "bindgen") ("openmp" "openmp-sys") ("default") ("cuquantum") ("cuda")))) (r "1.60")))

