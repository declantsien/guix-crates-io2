(define-module (crates-io qu es quest-tui) #:use-module (crates-io))

(define-public crate-quest-tui-0.1.0 (c (n "quest-tui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm"))) (k 0)))) (h "1sczn2m2r5fnnazjwdwfc3qp1i2imp59xdna57203zqhxh10mzmd")))

(define-public crate-quest-tui-0.1.1 (c (n "quest-tui") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm"))) (k 0)))) (h "097fc2ign1dmda99ldmlp8mzi0jjf6ia1nyz6ids76z8d29aqpjy")))

(define-public crate-quest-tui-0.1.2 (c (n "quest-tui") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm"))) (k 0)))) (h "1hdqkhrwdbrr7vah284szc7jnbbg5x510dq17r0glr2kss3am6pq")))

(define-public crate-quest-tui-0.1.3 (c (n "quest-tui") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm"))) (k 0)))) (h "0jbmcgdgn7sy96apdlsj087648p3a7lk3y97jh4j77180bh3mm48")))

(define-public crate-quest-tui-0.2.0 (c (n "quest-tui") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.19.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm" "serde"))) (k 0)))) (h "08gr1bf92in9bfqhv4nkkv79q7s2b42vydhl1r3czj94g2r733ha")))

