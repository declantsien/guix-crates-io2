(define-module (crates-io qu es questdb) #:use-module (crates-io))

(define-public crate-questdb-0.1.0 (c (n "questdb") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0d23m2ba1h7926vncksjiv4iyh8irff82dqq8af1xqc72knffrbc")))

(define-public crate-questdb-0.1.1 (c (n "questdb") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0qv92zsicmpsx0nhmmsvi3n6pwi4jfycg9rdwxs23dv55s928813")))

(define-public crate-questdb-0.1.2 (c (n "questdb") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1nizyf8hr2zs0s5na56x2ixcj4alql9ixhcjyqp15sykn88wbjz6")))

(define-public crate-questdb-0.1.3 (c (n "questdb") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1wk4wqbmw2hpdmyjd2fljl1p48kyw0mayi1a3maabiq9l8vhvpz2")))

