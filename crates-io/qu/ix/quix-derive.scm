(define-module (crates-io qu ix quix-derive) #:use-module (crates-io))

(define-public crate-quix-derive-0.0.1 (c (n "quix-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1q2pzgzwjfsn3jx09cvf3lvvx7razajmllyafj3iydl4ld30kf6j")))

(define-public crate-quix-derive-0.0.2 (c (n "quix-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1m0d5237dbhglbc45rz2v1sk47sh0j4dc0mycgvpya6zr2qh5zg4")))

(define-public crate-quix-derive-0.0.3 (c (n "quix-derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "072cg36dn5vhwj86bczxydrkasjnrp2rj9q1ixhm5hlvcs3gqcij")))

(define-public crate-quix-derive-0.0.4 (c (n "quix-derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "123m23g4gzzscgbsxig9gcmncp3p0cmnwz0zsr4ajc8d2n8xmjx2")))

(define-public crate-quix-derive-0.0.5 (c (n "quix-derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "15fh4vsn06byw59zlgksq1dqwpd49gl82ay8jm975712bbimv4wg")))

(define-public crate-quix-derive-0.0.6 (c (n "quix-derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1p7vd4fbn3qv7wfx91hzx176y1jqdjbqlyx9ai3v7ci2k76scnwk")))

