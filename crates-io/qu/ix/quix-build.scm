(define-module (crates-io qu ix quix-build) #:use-module (crates-io))

(define-public crate-quix-build-0.0.3 (c (n "quix-build") (v "0.0.3") (d (list (d (n "prost-build") (r "^0.6.1") (d #t) (k 0)))) (h "0zd2sx2lff3qp43p39xwi9dxhczm06fkm956qhdbr81dqmkbp49i")))

(define-public crate-quix-build-0.0.4 (c (n "quix-build") (v "0.0.4") (d (list (d (n "crc64") (r "^1.0.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 0)))) (h "11msi6pbzcpn745cikd2y85yxc772rr430c2kwh40d0gjv5xr9yz")))

(define-public crate-quix-build-0.0.5 (c (n "quix-build") (v "0.0.5") (d (list (d (n "crc64") (r "^1.0.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (d #t) (k 0)))) (h "0c02qvf210n0cqdx0k3kiyr4q26xi98asb0ba0vk3w4rs7gfdrd5")))

(define-public crate-quix-build-0.0.6 (c (n "quix-build") (v "0.0.6") (d (list (d (n "crc64") (r "^1.0.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (d #t) (k 0)))) (h "06fvifli58drlrdj6gyc4wf7sfqiyaynjm52b5gd40g8ijy3gf5b")))

