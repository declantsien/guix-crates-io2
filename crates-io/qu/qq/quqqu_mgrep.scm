(define-module (crates-io qu qq quqqu_mgrep) #:use-module (crates-io))

(define-public crate-QuqqU_mgrep-0.1.0 (c (n "QuqqU_mgrep") (v "0.1.0") (h "1v5hhb7pg725bnrml12svrn43ni2qscdpfbbnvlszhp151vi10ax")))

(define-public crate-QuqqU_mgrep-0.1.1 (c (n "QuqqU_mgrep") (v "0.1.1") (h "0f0bclhfwgbc7fm1j4i4r4yhwkrn8q7yfbcd571x838lagxp85xb")))

