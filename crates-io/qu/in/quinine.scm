(define-module (crates-io qu in quinine) #:use-module (crates-io))

(define-public crate-quinine-0.1.0 (c (n "quinine") (v "0.1.0") (h "0m2i9j2a6b5h6316pd33sa1481l13dv9q27fhcn6a3hy3897ya21")))

(define-public crate-quinine-0.2.0 (c (n "quinine") (v "0.2.0") (h "19jqrzghy1p2ld5b3bspah023dp6npqmsx1160zjh8bkb89v2yyj") (f (quote (("std") ("default"))))))

