(define-module (crates-io qu in quincho) #:use-module (crates-io))

(define-public crate-quincho-0.0.1 (c (n "quincho") (v "0.0.1") (d (list (d (n "base64-compat") (r "^1.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.29.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.26.0") (f (quote ("recovery"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "10v80w3493ai1lx1vrb2g9328i7wczsqgi75j9k82y9rdmgb1cpl")))

(define-public crate-quincho-0.0.2 (c (n "quincho") (v "0.0.2") (d (list (d (n "base64-compat") (r "^1.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.29.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.26.0") (f (quote ("recovery"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1jshiwniiv2psc3rjlvnfnsfnybm8mhs30mn2np5lzixq714i2im")))

