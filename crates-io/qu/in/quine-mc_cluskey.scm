(define-module (crates-io qu in quine-mc_cluskey) #:use-module (crates-io))

(define-public crate-quine-mc_cluskey-0.1.0 (c (n "quine-mc_cluskey") (v "0.1.0") (h "0jzaxzi5hmwd9rgv94w3ny48hs74xr07vxv34ydb14cgb2njlnwl")))

(define-public crate-quine-mc_cluskey-0.1.1 (c (n "quine-mc_cluskey") (v "0.1.1") (h "0ax2b83aadrch2vj7xmphnwbgpm2x31hp2fy1z66zksaxmj6lzdg")))

(define-public crate-quine-mc_cluskey-0.2.0 (c (n "quine-mc_cluskey") (v "0.2.0") (h "1il5v8xlsnjvk12sjrcl9lxznw2kxsb7k7sc6vl6qvalzfl0kig1")))

(define-public crate-quine-mc_cluskey-0.2.1 (c (n "quine-mc_cluskey") (v "0.2.1") (h "0c2014mhj1p6pzqjij2pjnq2rbrvcpw9csvl0cjxg5jg80nw1q8h")))

(define-public crate-quine-mc_cluskey-0.2.2 (c (n "quine-mc_cluskey") (v "0.2.2") (h "0jmp1sdrmcvkhfg9jg6nhqzpsly19001z11mlnqi626q4c73ns56")))

(define-public crate-quine-mc_cluskey-0.2.3 (c (n "quine-mc_cluskey") (v "0.2.3") (d (list (d (n "quickcheck") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0n9nkk2d7zx8r7f9syn5wv6f9bvwrlmjbfrjkwsfvghrjdgagyg8")))

(define-public crate-quine-mc_cluskey-0.2.4 (c (n "quine-mc_cluskey") (v "0.2.4") (d (list (d (n "quickcheck") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0iazdlwffhrlksb8yhhs1prgwpa68rwjwqm4v26hr9hrswarcn07")))

