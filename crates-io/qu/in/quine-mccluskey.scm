(define-module (crates-io qu in quine-mccluskey) #:use-module (crates-io))

(define-public crate-quine-mccluskey-1.0.0 (c (n "quine-mccluskey") (v "1.0.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1i6yllraw6k1gvxan88dcz7gmfybpj5p7baf1366imv1nd6f20my")))

