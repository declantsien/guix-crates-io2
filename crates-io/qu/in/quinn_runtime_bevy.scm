(define-module (crates-io qu in quinn_runtime_bevy) #:use-module (crates-io))

(define-public crate-quinn_runtime_bevy-0.0.0 (c (n "quinn_runtime_bevy") (v "0.0.0") (h "07djfp1xvvkr3wyc4glfdnc2cw4y4b323zp155cn8czycmjk1yrs")))

(define-public crate-quinn_runtime_bevy-0.1.0 (c (n "quinn_runtime_bevy") (v "0.1.0") (d (list (d (n "async-executor") (r "^1.4.1") (d #t) (k 0)) (d (n "async-io") (r "^1.9.0") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.9.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "quinn") (r "^0.9.1") (f (quote ("native-certs" "tls-rustls"))) (k 0)) (d (n "quinn-proto") (r "^0.9") (k 0)) (d (n "quinn-udp") (r "^0.3.0") (k 0)))) (h "1gp30qhqpnbp2drnv3d6y6piky7zhww3rm2agm4i8zqsfr00qk8a")))

(define-public crate-quinn_runtime_bevy-0.2.0 (c (n "quinn_runtime_bevy") (v "0.2.0") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "async-io") (r "^1.9") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.10") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "quinn") (r "^0.10") (f (quote ("native-certs" "tls-rustls"))) (k 0)) (d (n "quinn-proto") (r "^0.10") (k 0)) (d (n "quinn-udp") (r "^0.4") (k 0)))) (h "1073bmwgk7ypxawbn4pj3nzzalv8idiz3mgh4gl57bf98q43wj07")))

