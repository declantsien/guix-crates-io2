(define-module (crates-io qu er querystring_tiny) #:use-module (crates-io))

(define-public crate-querystring_tiny-0.1.0 (c (n "querystring_tiny") (v "0.1.0") (d (list (d (n "ebacktrace") (r "^0.5") (d #t) (k 0)))) (h "18d8br8rlamkmxrvpdyhrxrwqawywd67jv18yfc669yzj6mc3566") (f (quote (("default"))))))

(define-public crate-querystring_tiny-0.2.0 (c (n "querystring_tiny") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vna54pbd3mqw6s274yy2gl7ihpjc5pw1vldyaw8ch1v8ww8h85w") (f (quote (("default"))))))

(define-public crate-querystring_tiny-0.2.1 (c (n "querystring_tiny") (v "0.2.1") (h "0n4xfp8zxzdrvc3n1v6ccgqm5mip08674rdg0y0k4jqh5iqard48") (f (quote (("default"))))))

