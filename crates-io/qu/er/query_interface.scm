(define-module (crates-io qu er query_interface) #:use-module (crates-io))

(define-public crate-query_interface-0.1.0 (c (n "query_interface") (v "0.1.0") (h "05q2fq1wkrfh721lv2q9bzpm3198zygprhr7farihsgzvv9hlc0y")))

(define-public crate-query_interface-0.2.0 (c (n "query_interface") (v "0.2.0") (h "1in2v47jzhv2vmm945a034z570qry2hc7q2khapy2f4apih1xblh")))

(define-public crate-query_interface-0.3.0 (c (n "query_interface") (v "0.3.0") (h "0kmrhan4bjqszm4vyh5cymij4x21pz45in821h5xyxrg8qzfp8k5")))

(define-public crate-query_interface-0.3.1 (c (n "query_interface") (v "0.3.1") (h "18c7ly1sbm8dnxnamqslh4324l59p6hpmlr5gp5sxmdn18s2a1nd")))

(define-public crate-query_interface-0.3.2 (c (n "query_interface") (v "0.3.2") (h "0830xa776n7j10i2yj4a9q14sbm7h16qqnc2nvs1bxjnpnzy1mxh")))

(define-public crate-query_interface-0.3.3 (c (n "query_interface") (v "0.3.3") (h "1395x2p2dx1izijn1cimxg90x1c4pc0cm0hdk1wqkn0m509vaw7g")))

(define-public crate-query_interface-0.3.4 (c (n "query_interface") (v "0.3.4") (h "1wbqw276r5pb10zzvjldifc10a2fxhqxzgz59ml21lc1jyfm3rvp")))

(define-public crate-query_interface-0.3.5 (c (n "query_interface") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.0") (o #t) (d #t) (k 0)))) (h "14rn7i6jr8zf3h103jhmivw39429gmkzk4wgns3bpvl4c82g1h3q") (f (quote (("dynamic" "lazy_static"))))))

