(define-module (crates-io qu er query-params-serialize) #:use-module (crates-io))

(define-public crate-query-params-serialize-0.0.1 (c (n "query-params-serialize") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "01m3cpii9zsvxxn396fwbjf14y44if6f6216k5kjvfm2p6imjcgc")))

(define-public crate-query-params-serialize-0.0.2 (c (n "query-params-serialize") (v "0.0.2") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "1cgj89fv2afimf9p4mjcmjs5nxq26s04q411ix5765i46rkxcbp4")))

