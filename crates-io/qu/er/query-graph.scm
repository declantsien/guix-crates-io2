(define-module (crates-io qu er query-graph) #:use-module (crates-io))

(define-public crate-query-graph-0.1.0 (c (n "query-graph") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1phn5n1dw3p4klxgn4hhmy2v0s34svjnga20xydrf9r2r2hyx32z")))

(define-public crate-query-graph-0.1.1 (c (n "query-graph") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0v9zxn3k3qk33vifiiiqgchssx2lr1ns0kvsvv034ig3a1cn9im5")))

