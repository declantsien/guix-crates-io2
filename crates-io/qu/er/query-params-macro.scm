(define-module (crates-io qu er query-params-macro) #:use-module (crates-io))

(define-public crate-query-params-macro-0.0.2 (c (n "query-params-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 2)))) (h "0bian6l1q5kjvxv8lyg5ajr5lqwrvccqh04x45a7jmili51fwxmm") (y #t)))

(define-public crate-query-params-macro-0.0.3 (c (n "query-params-macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0f548d9x401lc3dfk465gp930ciylk8gvxg9v95krq432bwjn7dz")))

(define-public crate-query-params-macro-0.0.4 (c (n "query-params-macro") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "110xb3mqz09kzn0yh2d9qqg0am4vp5gslr6y8cg894fzkp62jn9g")))

