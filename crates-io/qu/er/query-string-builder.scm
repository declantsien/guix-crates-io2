(define-module (crates-io qu er query-string-builder) #:use-module (crates-io))

(define-public crate-query-string-builder-0.1.0 (c (n "query-string-builder") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)))) (h "1pw285cl125x1vmm4db7xdm3n5l5l3ci9q2hhl4g6nddj0bhhpgy")))

(define-public crate-query-string-builder-0.2.0 (c (n "query-string-builder") (v "0.2.0") (d (list (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)))) (h "1h1bbhzrk2c7rz0k2fp8d2kxy0xnfvfjmlmldaygfy8xnj1k723z")))

(define-public crate-query-string-builder-0.3.0 (c (n "query-string-builder") (v "0.3.0") (d (list (d (n "percent-encoding") (r "^2.3.0") (d #t) (k 0)))) (h "027bsc9akpky4a8r1b7n7av9nxl6q9daf2scrnddd3dvv51dms99")))

(define-public crate-query-string-builder-0.4.0 (c (n "query-string-builder") (v "0.4.0") (d (list (d (n "percent-encoding") (r "^2.3.0") (f (quote ("std"))) (k 0)))) (h "0w09c3c72rb4gv1pjvjbk087qbza8kb7fyfma5jdqwl67c55xbr1")))

(define-public crate-query-string-builder-0.4.1 (c (n "query-string-builder") (v "0.4.1") (d (list (d (n "percent-encoding") (r "^2.3.0") (f (quote ("std"))) (k 0)))) (h "0ipgv2m5najx2n83z1kyxkcq5sq0hvgdv2pyghh5cyv8fghl9arj")))

(define-public crate-query-string-builder-0.4.2 (c (n "query-string-builder") (v "0.4.2") (d (list (d (n "percent-encoding") (r "^2.3.0") (f (quote ("std"))) (k 0)))) (h "0h3np00yqdr9ks5wr6l5cd69spzdyhx3gbf8yak24cwywzsfs8fz")))

(define-public crate-query-string-builder-0.5.0 (c (n "query-string-builder") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3.0") (f (quote ("std"))) (k 0)))) (h "0ckqmy9rfphf9wi06h3g282dgz2pimmvfnbpy845rzg0h1kz2vpn")))

(define-public crate-query-string-builder-0.5.1 (c (n "query-string-builder") (v "0.5.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3.0") (f (quote ("std"))) (k 0)))) (h "1cfmq9qlxlf16x7drqyxfrs9xd9cyvz2i5sqf57fdcq2ydasn55v")))

