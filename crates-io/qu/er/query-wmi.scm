(define-module (crates-io qu er query-wmi) #:use-module (crates-io))

(define-public crate-query-wmi-1.0.0 (c (n "query-wmi") (v "1.0.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (k 0)))) (h "049qxpn34s45a9x0bzli6n84jaq8644zd060pfd1v0vgbf7dkmfb") (y #t)))

(define-public crate-query-wmi-1.1.0 (c (n "query-wmi") (v "1.1.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (k 0)))) (h "0sj9k9fi9dkxv6hcya258g8jf0wzb7sy89i73vfbj3rzd544aliy") (y #t)))

(define-public crate-query-wmi-1.1.1 (c (n "query-wmi") (v "1.1.1") (d (list (d (n "paste") (r "^1.0.11") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "14h8p3dgymg9s3z7v4d09vqqvry8prw6wfcggpc8rj1iqlcmpgf4") (y #t)))

(define-public crate-query-wmi-1.1.2 (c (n "query-wmi") (v "1.1.2") (d (list (d (n "paste") (r "^1.0.11") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1zqgvwip4azz7984l1b63w5ds082z33asr6fd4fnf5a6g61yw4ig") (y #t)))

(define-public crate-query-wmi-1.1.3 (c (n "query-wmi") (v "1.1.3") (d (list (d (n "paste") (r "^1.0.11") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "11r2d3gr8vnkaavsgach8rwjvbr2mpm9j9mz3yx0dnd57w5kzvrj")))

