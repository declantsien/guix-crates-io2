(define-module (crates-io qu er querun_derive) #:use-module (crates-io))

(define-public crate-querun_derive-0.0.1 (c (n "querun_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01gw6v78haxnwp7zblvm3m46dm97zmj9j5kgv3ygay9v4650w2fn")))

