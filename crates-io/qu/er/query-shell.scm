(define-module (crates-io qu er query-shell) #:use-module (crates-io))

(define-public crate-query-shell-0.1.0 (c (n "query-shell") (v "0.1.0") (d (list (d (n "sysinfo") (r "^0.15.9") (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0psd61i07bw32k4yxymr3i5kdmshrk0sz4xc80cglzs5icllzznr")))

(define-public crate-query-shell-0.2.0 (c (n "query-shell") (v "0.2.0") (d (list (d (n "sysinfo") (r "^0.23") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ixmcjani969kli8nq1s8ma8vlfll91isxkhnss5vbv5ykabs5p7")))

(define-public crate-query-shell-0.3.0 (c (n "query-shell") (v "0.3.0") (d (list (d (n "sysinfo") (r "^0.27") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09gfvyj85qz3rfxgzw8zy6gr86zks1d9sr388nrcx3k3x631q39h")))

