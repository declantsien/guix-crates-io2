(define-module (crates-io qu er querable) #:use-module (crates-io))

(define-public crate-querable-0.0.1 (c (n "querable") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)))) (h "1z622lm0xbwd3nw7mspq6nr0q4aadx9gp03psaax33iqijlxdqfr")))

(define-public crate-querable-0.0.2 (c (n "querable") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)))) (h "10rzrr4yfrd0xagcgz25l8ph06b7hpaawrmlz84gyjcz22q2xxqv")))

