(define-module (crates-io qu er queriable_storage) #:use-module (crates-io))

(define-public crate-queriable_storage-0.2.1 (c (n "queriable_storage") (v "0.2.1") (h "1v2xv7c5qbgc8h1qd119cpsgs7ikdq6gfx1n95zqds4j1zd9lppj")))

(define-public crate-queriable_storage-0.3.0 (c (n "queriable_storage") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "iter-set") (r "^2.0.0") (d #t) (k 0)))) (h "1hamrnq8jlbxrdfv6pi9anbll2vwh7d51wvbl3amsl1b578ci3d1")))

