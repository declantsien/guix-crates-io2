(define-module (crates-io qu er queryst-prime) #:use-module (crates-io))

(define-public crate-queryst-prime-2.0.0 (c (n "queryst-prime") (v "2.0.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0zlc0ld69kzn3zspzp9i8ggvhr204cl273h6p2m0y3ib4qw3mmaj")))

