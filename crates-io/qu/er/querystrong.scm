(define-module (crates-io qu er querystrong) #:use-module (crates-io))

(define-public crate-querystrong-0.1.0 (c (n "querystrong") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0h65w9q7ix71sr95hxrzgz6xljc7y5mdxv85lmrazsvarcvaqz72")))

(define-public crate-querystrong-0.1.1 (c (n "querystrong") (v "0.1.1") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1n25h2b9yc7rgh2y7d515rpmfkx1vzzwcnhr7g6hy08wsq1kacjk")))

(define-public crate-querystrong-0.2.0 (c (n "querystrong") (v "0.2.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1klxkin9sifavw55ryg4wnqckw68sazq1g7gsps6dca6cf8xwyyh")))

(define-public crate-querystrong-0.3.0 (c (n "querystrong") (v "0.3.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0xshrbkp6nzc1idl4d4wl5ddxm55z5phqq5syhjbqrddf7ybl3pv")))

