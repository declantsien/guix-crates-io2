(define-module (crates-io qu er querydown-cli) #:use-module (crates-io))

(define-public crate-querydown-cli-0.0.1 (c (n "querydown-cli") (v "0.0.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "querydown") (r "^0.0.1") (d #t) (k 0)))) (h "0qp1w4zi9hmkrhpk27v0mgwc03aci9cnsr1iks2hrc7zirb6df8g")))

