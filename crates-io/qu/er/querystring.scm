(define-module (crates-io qu er querystring) #:use-module (crates-io))

(define-public crate-querystring-0.1.0 (c (n "querystring") (v "0.1.0") (h "1bgw3css39padmjr0qz9wadvlcl7yawnpba27a0nmggkcnyd85z4")))

(define-public crate-querystring-1.0.0 (c (n "querystring") (v "1.0.0") (h "151awyimvmn6zm6dcg40ah832b8cdqcns2vnnw578gqqzjpgi4pw")))

(define-public crate-querystring-1.1.0 (c (n "querystring") (v "1.1.0") (h "11wifcpji4c8fmxn47zx3wknfq8b5sw7hgjsll9av6krik8fl64k")))

