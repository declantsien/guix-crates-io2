(define-module (crates-io qu er queryparse) #:use-module (crates-io))

(define-public crate-queryparse-0.1.0 (c (n "queryparse") (v "0.1.0") (h "0yr7m025ivdmiws7j0l4bkvd4d1pbnais82dcr8ldk9p8sgp9qbz")))

(define-public crate-queryparse-0.1.1 (c (n "queryparse") (v "0.1.1") (h "1zf9ska69j1gamzpp1wi95rq2bjxz3xv9gmpln1b2rpc26wgxh9c")))

