(define-module (crates-io qu er querylib) #:use-module (crates-io))

(define-public crate-querylib-0.1.0 (c (n "querylib") (v "0.1.0") (d (list (d (n "bson") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)))) (h "019ar2mpv822vbjfqsh9mwfwskr65yfllamsljaclvx4hfr43zz2")))

(define-public crate-querylib-0.1.1 (c (n "querylib") (v "0.1.1") (d (list (d (n "bson") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)))) (h "1kg2jvw3x5zaqafal8bdc6n9034d6y83as86j12kpc65a007j8bz")))

(define-public crate-querylib-0.1.2 (c (n "querylib") (v "0.1.2") (d (list (d (n "bson") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1gqwgdajm246liwss5a8dzal7wn47xgva1piil79r0bzq6nz9bpy") (f (quote (("mongo" "bson") ("default"))))))

(define-public crate-querylib-0.2.0 (c (n "querylib") (v "0.2.0") (d (list (d (n "bson") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.16") (o #t) (d #t) (k 0)))) (h "03wq630dl117y1wg9m1vkv1lcdj5dh6nlagjdvrrrng740qfbvfd") (f (quote (("parse" "lexer") ("mongo" "bson") ("default" "lexer"))))))

(define-public crate-querylib-0.2.1 (c (n "querylib") (v "0.2.1") (d (list (d (n "bson") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.16") (o #t) (d #t) (k 0)))) (h "1q3hbgkq26zbzwqc38iprjx50p95cd0wxi9yd9m7kxh0plkmj0a5") (f (quote (("parse" "lexer") ("mongo" "bson") ("default" "lexer"))))))

(define-public crate-querylib-0.2.2 (c (n "querylib") (v "0.2.2") (d (list (d (n "bson") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.16") (o #t) (d #t) (k 0)))) (h "15ris6z5nh5lb9b1yixsa9h129irzf8q6qwyalsvqz1k1bijdhzz") (f (quote (("parse" "lexer") ("mongo" "bson") ("default"))))))

(define-public crate-querylib-0.2.3 (c (n "querylib") (v "0.2.3") (d (list (d (n "bson") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.16") (o #t) (d #t) (k 0)))) (h "1fm87dzpyva87wrizk47mxssfzi63q1z0yig491ii73czzscpp32") (f (quote (("parse" "lexer") ("mongo" "bson") ("default"))))))

(define-public crate-querylib-0.3.0 (c (n "querylib") (v "0.3.0") (d (list (d (n "bson") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.16") (o #t) (d #t) (k 0)))) (h "0c68pjrjmszx650vw90zsizgqxzgcmj1n3spw5x21xxmp38a5iwn") (f (quote (("parse" "lexer") ("mongo" "bson") ("default"))))))

(define-public crate-querylib-0.3.1 (c (n "querylib") (v "0.3.1") (d (list (d (n "bson") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.16") (o #t) (d #t) (k 0)))) (h "091a0js7ixnxlk26dc0433anzx1m073gs4wv4207kkjmml1drjs1") (f (quote (("parse" "lexer") ("mongo" "bson") ("default"))))))

(define-public crate-querylib-0.3.2 (c (n "querylib") (v "0.3.2") (d (list (d (n "bson") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.16") (o #t) (d #t) (k 0)))) (h "04b3wd9i2vmcy77sv6qvv55gi11cfya17krnxps071c3mcimhrly") (f (quote (("parse" "lexer") ("mongo" "bson") ("default"))))))

(define-public crate-querylib-0.3.3 (c (n "querylib") (v "0.3.3") (d (list (d (n "bson") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.16") (o #t) (d #t) (k 0)))) (h "02x1vw2vs0g6f1zcj9x42d66dalpbnras4pldgklwya52l0rxhbg") (f (quote (("parse" "lexer") ("mongo" "bson") ("default"))))))

(define-public crate-querylib-0.3.4 (c (n "querylib") (v "0.3.4") (d (list (d (n "bson") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.18") (o #t) (d #t) (k 0)))) (h "1lcks327z356p5fraa5czxyxy56pm3vlwxwn3jmd5q7zypvxhclq") (f (quote (("parse" "lexer") ("mongo" "bson") ("default"))))))

(define-public crate-querylib-0.4.0 (c (n "querylib") (v "0.4.0") (d (list (d (n "bson") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.18") (o #t) (d #t) (k 0)))) (h "0jdszx6iq2kdv7pcp3qvmqlb2bg440w45x91x06f39d9w4kch03q") (f (quote (("parse" "lexer") ("mongo" "bson") ("default"))))))

(define-public crate-querylib-0.5.0 (c (n "querylib") (v "0.5.0") (d (list (d (n "bson") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "lexer") (r "^0.1.18") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0v1zz7bfckzgbcmzl3lfy9bwiqifbnix37rvk6656nxhxrswddg7") (f (quote (("postgres") ("parse" "lexer") ("mongo" "bson") ("default"))))))

