(define-module (crates-io qu er querylizer) #:use-module (crates-io))

(define-public crate-querylizer-0.1.0 (c (n "querylizer") (v "0.1.0") (d (list (d (n "dtoa") (r "^1.0.2") (d #t) (k 0)) (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10k1snsvdbffwf8jnws9g4368p17jqmcj8yh2v3kiidwdkgawy6c")))

(define-public crate-querylizer-0.1.1 (c (n "querylizer") (v "0.1.1") (d (list (d (n "dtoa") (r "^1.0.2") (d #t) (k 0)) (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0lgv8ak6qy2swbn3zdn2jrjjpyj36aix95lfskp0f2q61d7hgcbs")))

(define-public crate-querylizer-0.1.2 (c (n "querylizer") (v "0.1.2") (d (list (d (n "dtoa") (r "^1.0.2") (d #t) (k 0)) (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0jzg68lvrh0l4f4b0m6jip7c74x2vly4n86wwp04iwyhjmryd11v")))

(define-public crate-querylizer-0.1.3 (c (n "querylizer") (v "0.1.3") (d (list (d (n "dtoa") (r "^1.0.2") (d #t) (k 0)) (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0nsrh41yb8a0d352z4x700n4v0sp119lfkqs6y53i7d35xnvgzsr")))

(define-public crate-querylizer-0.1.4 (c (n "querylizer") (v "0.1.4") (d (list (d (n "dtoa") (r "^1.0.2") (d #t) (k 0)) (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1bpwkc8xi2ybljdw9i6kg1qjpi36chglmbnsxhfmncsvaspqrpgm")))

(define-public crate-querylizer-0.2.0 (c (n "querylizer") (v "0.2.0") (d (list (d (n "dtoa") (r "^1.0.2") (d #t) (k 0)) (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xgsb3i4ds5ag6p9fq3jwkssfasn9l51ammpa3jdi2dmbqp7ljjb")))

