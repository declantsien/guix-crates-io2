(define-module (crates-io qu er querio_derive) #:use-module (crates-io))

(define-public crate-querio_derive-0.0.1 (c (n "querio_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01q191ny2kqdlm3ff96y31f67hc8k9g898s6q1n2kzfbpcd2kpi1") (f (quote (("variables") ("native_output") ("native_input") ("default")))) (s 2) (e (quote (("flatten" "dep:regex"))))))

