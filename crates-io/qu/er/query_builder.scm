(define-module (crates-io qu er query_builder) #:use-module (crates-io))

(define-public crate-query_builder-0.1.0 (c (n "query_builder") (v "0.1.0") (h "02xxysly5zin0kn865fb36x1almdvv0cy1mdw3hwpf85fys2cfqr")))

(define-public crate-query_builder-0.1.1 (c (n "query_builder") (v "0.1.1") (h "1lr53b8pgc37w3h445h5pp0acm7g7ymi9jwarg1kvm1jg0sc71v0")))

(define-public crate-query_builder-0.1.2 (c (n "query_builder") (v "0.1.2") (h "0sv6mfgv1z8gjccc1s3asksfxz24rzr3yckdjg8z5g5i1lfngx5y")))

(define-public crate-query_builder-0.1.3 (c (n "query_builder") (v "0.1.3") (h "15321yvvczvccv91yl8hsc2xpbb7ln84bikddybcfwinfmlyd9j9")))

(define-public crate-query_builder-0.2.0 (c (n "query_builder") (v "0.2.0") (h "06qgvia1mslcysjyfbxi0pvlnh10n145bydp84fwwh10mps32j4s")))

(define-public crate-query_builder-0.3.1 (c (n "query_builder") (v "0.3.1") (h "1v8imaf4i8z6l78czq1r4nji316hh1qpxfcs3x2g9dhbv9w4s2yq")))

