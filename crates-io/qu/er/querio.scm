(define-module (crates-io qu er querio) #:use-module (crates-io))

(define-public crate-querio-0.0.1 (c (n "querio") (v "0.0.1") (d (list (d (n "intuple") (r "^0.1.0") (d #t) (k 0)) (d (n "querio_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "strung") (r "^0.1.3") (d #t) (k 0)))) (h "04zwvmqycra4i6wkbrwhsxy9yrs1i3y1phcr2fzgiphh1vxxr5y2") (f (quote (("variables" "querio_derive/variables") ("native_output" "querio_derive/native_output") ("native_io" "native_input" "native_output") ("native_input" "querio_derive/native_input") ("flatten" "querio_derive/flatten") ("default" "variables"))))))

