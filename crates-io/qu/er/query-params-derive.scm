(define-module (crates-io qu er query-params-derive) #:use-module (crates-io))

(define-public crate-query-params-derive-0.0.1 (c (n "query-params-derive") (v "0.0.1") (d (list (d (n "query-params-trait") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1gj4d9dvs6j7c6rjj7y82dhkrx78fsd032y309d66fx0i08fc288")))

(define-public crate-query-params-derive-0.1.0 (c (n "query-params-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "query-params-trait") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0w59n9wksdanqidssnzlbajf8bz8pm18aq6irgjf94dg3sa5abvr")))

(define-public crate-query-params-derive-0.1.1 (c (n "query-params-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "query-params-trait") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1yldwnds766l0mzv15ww4j8gk2z1ssm0h76698g4p5sjgbn72j7y")))

