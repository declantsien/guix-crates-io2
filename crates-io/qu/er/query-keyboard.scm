(define-module (crates-io qu er query-keyboard) #:use-module (crates-io))

(define-public crate-query-keyboard-1.0.0 (c (n "query-keyboard") (v "1.0.0") (d (list (d (n "device_query") (r "^1.1.1") (d #t) (k 0)))) (h "00zfiw4fy7s5w7hf7hj47f0f07mbb5bdz3p3p9ylsx6l0rs01by9")))

(define-public crate-query-keyboard-1.1.0 (c (n "query-keyboard") (v "1.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "device_query") (r "^1.1.1") (d #t) (k 0)))) (h "0vn9wmnqhxr82wfd221rnlcywk6s0s9w2k44iiqd0m4rknfl2g14")))

