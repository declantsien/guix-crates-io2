(define-module (crates-io qu er query-parser) #:use-module (crates-io))

(define-public crate-query-parser-0.1.0 (c (n "query-parser") (v "0.1.0") (h "0b8dk8681b8vl2b861kljvmfwfkb7i4x4q8kggsh5crzar742yi8")))

(define-public crate-query-parser-0.2.0 (c (n "query-parser") (v "0.2.0") (h "1i65jccb5vl0mabzqxm9z8pvgivb6cr80zzwgzfvvw6q739ivi6s")))

