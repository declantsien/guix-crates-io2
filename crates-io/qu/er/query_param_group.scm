(define-module (crates-io qu er query_param_group) #:use-module (crates-io))

(define-public crate-query_param_group-0.1.0 (c (n "query_param_group") (v "0.1.0") (d (list (d (n "rocket") (r "^0.2.6") (d #t) (k 0)))) (h "0f06lqgr6qy0z6lyjq9ka268db24cirs42j3q93gyk8fpgbxbgvj")))

(define-public crate-query_param_group-0.1.1 (c (n "query_param_group") (v "0.1.1") (d (list (d (n "rocket") (r "^0.2.6") (d #t) (k 0)))) (h "16l03dmyk2siaqiprlzbxf3ra7fiwfs14p8xnp3mv56r9gdg0j8m")))

