(define-module (crates-io qu er query) #:use-module (crates-io))

(define-public crate-query-0.1.0 (c (n "query") (v "0.1.0") (d (list (d (n "flate2") (r "^0.2.20") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.18") (d #t) (k 0)))) (h "14x7v5i8rnb0ph22dr3drniw6c3dwyacqv0hjk1zrfl0pknd0c1g") (y #t)))

(define-public crate-query-0.1.1 (c (n "query") (v "0.1.1") (d (list (d (n "flate2") (r "^0.2.20") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.18") (d #t) (k 0)))) (h "1hdh0296f12sv1jzcar69m0hmfylkpsai87g6364nhxai987vdi9") (y #t)))

(define-public crate-query-0.1.2 (c (n "query") (v "0.1.2") (d (list (d (n "flate2") (r "^0.2.20") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.18") (d #t) (k 0)))) (h "1sx5x1jk91p47lbcygsb9nddvrkf8734qj720307c9iymig62j0r") (y #t)))

