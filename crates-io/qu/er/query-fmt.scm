(define-module (crates-io qu er query-fmt) #:use-module (crates-io))

(define-public crate-query-fmt-1.6.0 (c (n "query-fmt") (v "1.6.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "tree-sitter-query") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tar") (r "^0.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "zip") (r "^0.5") (d #t) (t "cfg(windows)") (k 0)))) (h "1ilriw5np74f58bc4y7f6ikf4qny8pxj8m62pfamxw6w3ahl9575")))

