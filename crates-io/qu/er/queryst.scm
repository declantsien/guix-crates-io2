(define-module (crates-io qu er queryst) #:use-module (crates-io))

(define-public crate-queryst-0.1.0 (c (n "queryst") (v "0.1.0") (d (list (d (n "url") (r "~0.2.0") (d #t) (k 0)))) (h "03w09m25yiqawqawz2nyyfvqgsbpxd86xgabhb125k8gmbgi4pfr")))

(define-public crate-queryst-0.1.1 (c (n "queryst") (v "0.1.1") (d (list (d (n "url") (r "~0.2.0") (d #t) (k 0)))) (h "17271r9fsd5jrfry7i3dn41asiyvnvkj7nqxgwpjjjiws7xnly33")))

(define-public crate-queryst-0.1.2 (c (n "queryst") (v "0.1.2") (d (list (d (n "url") (r "~0.2.0") (d #t) (k 0)))) (h "167l9rb51cby4d7hkcmr0kcvazp8x0j5pzkfr8nrfa92nf4fzhik")))

(define-public crate-queryst-0.1.3 (c (n "queryst") (v "0.1.3") (d (list (d (n "url") (r "~0.2.0") (d #t) (k 0)))) (h "0wfmma4spcy907pvqf5npg4l61r9jlgy3lgxyixlvjyk5w0fxqfk")))

(define-public crate-queryst-0.1.4 (c (n "queryst") (v "0.1.4") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "061vliba3wh9r96nzrqc0a7xcfkb8hspvizz3dfacyficsfv50nw")))

(define-public crate-queryst-0.1.6 (c (n "queryst") (v "0.1.6") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1im6wd9a51mvlsmy61pcl0iw51cy7rqcr32w1icj9visl7fki6k8")))

(define-public crate-queryst-0.1.7 (c (n "queryst") (v "0.1.7") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "14hby7d323yj8rinx2ymqwf2w1jwrmyljc6acnglb8fh5mhiv3ax")))

(define-public crate-queryst-0.1.8 (c (n "queryst") (v "0.1.8") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "01b3f30klp498jrj3qnpvpbrqayscdj6hfvcaklka8vi53nazbda")))

(define-public crate-queryst-0.1.9 (c (n "queryst") (v "0.1.9") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0byspy7370fvx51di6idcymxbl27r6k3amb7mf5rvigpqkrq45yd")))

(define-public crate-queryst-0.1.10 (c (n "queryst") (v "0.1.10") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "01d43d6y2nvwxkgasx7jljcry4w9lsf3vdaa4a6wzbpmprn2ad71")))

(define-public crate-queryst-0.1.11 (c (n "queryst") (v "0.1.11") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "04zjbvbjblnpy6n3fdrsjg44cn9372zz02183qsvn7mr0y2iqlxi")))

(define-public crate-queryst-0.1.12 (c (n "queryst") (v "0.1.12") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0hdp8dpg5b94b4g5kppv6ghq626zp1q3hwkfz03r9ajk3lwkl3ff")))

(define-public crate-queryst-0.1.13 (c (n "queryst") (v "0.1.13") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1dnnz4d8c63jdrlyfkh06anni87fg0z55wlcyv0jfw610bjgk7wf")))

(define-public crate-queryst-0.1.14 (c (n "queryst") (v "0.1.14") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "01a9mqdc5sw7gl56lczngfx0a78gpkql7ri9fq0vy99sw3lpa1fw")))

(define-public crate-queryst-0.1.15 (c (n "queryst") (v "0.1.15") (d (list (d (n "lazy_static") (r "^0.1.10") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1vznz4b43dsap9f1fba62hiqp5j2625kn6s259i9an0xz49spn8n")))

(define-public crate-queryst-0.1.16 (c (n "queryst") (v "0.1.16") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0hrjmmkvlh7vijbmw4hsybqklvgr4baxsqbgy1hq5f3m6cakxnxc")))

(define-public crate-queryst-1.0.0 (c (n "queryst") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "03xs34api0w3c6pgv02n01df7a1fygh9mspz3ivyigb3smaaiydk")))

(define-public crate-queryst-1.0.1 (c (n "queryst") (v "1.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "09rsi4y1m9swlyx2nyk886cg6jzxxgb5mwjg77a6zwlhzbmv3d6p")))

(define-public crate-queryst-2.0.0 (c (n "queryst") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "00flhdbf4shdrk4y8n6rdm0zagiw68761r02zkwipy5ylnf7icgw")))

(define-public crate-queryst-2.0.1 (c (n "queryst") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1d7jaw8riajjjcwgx2c01c4bz35rfhdc3br9y82alsb4g7dlkwy4") (f (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

(define-public crate-queryst-2.0.2 (c (n "queryst") (v "2.0.2") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1c2q8vmfbfyn58v3dbx1zcwk8969a8nnw9r5l81qma06vvgqyr7f") (f (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

(define-public crate-queryst-2.1.0 (c (n "queryst") (v "2.1.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "00nrbkvnzfdrxr66ndfs68idpmxjnwkmq96xhd50xznj2xzwhaby") (f (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

(define-public crate-queryst-3.0.0 (c (n "queryst") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ndrng57h94mg4n4zbr8bw9kp1sgd2f6vmm23hhaypb9misypjy1") (f (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

