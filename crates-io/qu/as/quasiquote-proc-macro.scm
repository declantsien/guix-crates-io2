(define-module (crates-io qu as quasiquote-proc-macro) #:use-module (crates-io))

(define-public crate-quasiquote-proc-macro-0.0.1 (c (n "quasiquote-proc-macro") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1jpradh9paw3phgwh8y7yjjg1z6zr6fmwyg4l10q6ycws3la8fwl")))

