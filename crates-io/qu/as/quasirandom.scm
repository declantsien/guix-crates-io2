(define-module (crates-io qu as quasirandom) #:use-module (crates-io))

(define-public crate-quasirandom-0.1.0 (c (n "quasirandom") (v "0.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1d61f5574pnx0mx9s0bhp9m3k9dlrf0c1g4sifv89k81syxvllr7")))

(define-public crate-quasirandom-0.2.0 (c (n "quasirandom") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0c30yzi1zrcldb8is4k9p15qbs67j3bc0s6fqbphh0b2knlc9i94")))

(define-public crate-quasirandom-0.2.1 (c (n "quasirandom") (v "0.2.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1pyb7rswd2yw40s6mfgdcxsfgbl3mz3g0fnwpnpf2fbfqj6chd3k")))

(define-public crate-quasirandom-0.3.0 (c (n "quasirandom") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0sqi65rss0h3xrig7077541fc41j72q07jn3la35aknspbbw5k6s")))

