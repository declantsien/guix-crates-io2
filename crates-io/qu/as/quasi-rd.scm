(define-module (crates-io qu as quasi-rd) #:use-module (crates-io))

(define-public crate-quasi-rd-0.1.0 (c (n "quasi-rd") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1yf73lfkjrlg27l1qqr7svk11b1rvfp6rlnfkkh2vzbp6k2bmzz8")))

