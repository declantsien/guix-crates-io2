(define-module (crates-io qu as quasi) #:use-module (crates-io))

(define-public crate-quasi-0.1.0 (c (n "quasi") (v "0.1.0") (d (list (d (n "quasi_macros") (r "^0.1.0") (d #t) (k 2)))) (h "0aggmr9v1bk1l6qcih2w809lg998mazb0nnmbmpma02gyfm2q13d")))

(define-public crate-quasi-0.1.1 (c (n "quasi") (v "0.1.1") (d (list (d (n "quasi_macros") (r "^0.1.1") (d #t) (k 2)))) (h "1aa5c1q005wr4z7s9v2bm6ynlg9xjpmz73g5f5rbp3nrh38jw25s")))

(define-public crate-quasi-0.1.3 (c (n "quasi") (v "0.1.3") (d (list (d (n "quasi_macros") (r "^0.1.3") (d #t) (k 2)))) (h "1a5kpgdiirx8smh98szbmkblbs1dsjgb1hh0j1mms9v6f2zplbqb")))

(define-public crate-quasi-0.1.4 (c (n "quasi") (v "0.1.4") (d (list (d (n "quasi_macros") (r "*") (d #t) (k 2)))) (h "11adzdmj9vg1pmi5b89adby55j3ad0406xs1pg4w52dj86hx8i74")))

(define-public crate-quasi-0.1.5 (c (n "quasi") (v "0.1.5") (d (list (d (n "quasi_macros") (r "*") (d #t) (k 2)))) (h "11jmnw6qas01c705mnq5wldg1q0wq3p6s7zmigrwws57avgwk872")))

(define-public crate-quasi-0.1.6 (c (n "quasi") (v "0.1.6") (d (list (d (n "quasi_macros") (r "*") (d #t) (k 2)))) (h "18wk2h5isfblhz1n0w299974bw49wxbasg1wjlxgak0g4s7hsrxa")))

(define-public crate-quasi-0.1.7 (c (n "quasi") (v "0.1.7") (d (list (d (n "quasi_macros") (r "^0.1.7") (d #t) (k 2)))) (h "1c5c9qms23hzdn262gai1nl2fiqbmy5rin5lx4idsk5sw0s49gqn")))

(define-public crate-quasi-0.1.8 (c (n "quasi") (v "0.1.8") (d (list (d (n "quasi_macros") (r "^0.1.7") (d #t) (k 2)))) (h "112szlzxw96mn5jqx2kyxxs80zvjqkl0xfmn1knlarn3ay44gyi6")))

(define-public crate-quasi-0.1.9 (c (n "quasi") (v "0.1.9") (d (list (d (n "quasi_macros") (r "^0.1.9") (d #t) (k 2)))) (h "066fr7nn584y6xdy4z57dapz39nfyw1v2lcnh10xfkbqxxidqg7s")))

(define-public crate-quasi-0.1.10 (c (n "quasi") (v "0.1.10") (d (list (d (n "quasi_macros") (r "*") (d #t) (k 2)))) (h "1r7xnc5c4c40kvjznhlbp1071rza98iy2y2fanzh81h8d9c6zrld")))

(define-public crate-quasi-0.2.0 (c (n "quasi") (v "0.2.0") (d (list (d (n "aster") (r "*") (d #t) (k 2)) (d (n "quasi_macros") (r "*") (d #t) (k 2)))) (h "01xnwm97ry7gx1mw0arcc6i4a4qmd05pkvamz19l94q9igz3k7cd")))

(define-public crate-quasi-0.3.0 (c (n "quasi") (v "0.3.0") (d (list (d (n "syntex_syntax") (r "*") (o #t) (d #t) (k 0)))) (h "18n7iyyxq9xlxwv9bp1c1xkh4r8ncnn1rv2z85hwly6w2w22j9v3") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.1 (c (n "quasi") (v "0.3.1") (d (list (d (n "syntex_syntax") (r "*") (o #t) (d #t) (k 0)))) (h "0v9882yn6s6v7k4xa14i6p4lg2mdk55jzg8l960fp13iv71y1gjb") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.2 (c (n "quasi") (v "0.3.2") (d (list (d (n "syntex_syntax") (r ">= 0.13.0") (o #t) (d #t) (k 0)))) (h "1l3z59a0flddhm17avx232l849ymq9iwd8zwsz2pw04p06r5dv2z") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.4 (c (n "quasi") (v "0.3.4") (d (list (d (n "syntex_syntax") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "03skkgv7d5794ci2k1n2jy8d5hfzj7bdvr4khy20jhlfsi469kk6") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.5 (c (n "quasi") (v "0.3.5") (d (list (d (n "syntex_syntax") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "1l9imxhfwspbz8mp5a5sswqghrabbkhknnv2qqk13ypyjzmp79hi") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.6 (c (n "quasi") (v "0.3.6") (d (list (d (n "syntex_syntax") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "15722d3p9b3r5c1lxgp2w2mkwzjhm4jk08nk4v11b0m7ddi6k5n8") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.7 (c (n "quasi") (v "0.3.7") (d (list (d (n "syntex_syntax") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "0z3nn8yrl5k5sxhcfb5xc1prarghl116hwvnskdpvzqcxjabizzg") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.8 (c (n "quasi") (v "0.3.8") (d (list (d (n "syntex_syntax") (r "^0.22.0") (o #t) (d #t) (k 0)))) (h "0kdxz8wf2rx46ncdj19599lvpwai1s40z61b70vm0rs3ixrhrsdl") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.10 (c (n "quasi") (v "0.3.10") (d (list (d (n "syntex_syntax") (r "^0.23.0") (o #t) (d #t) (k 0)))) (h "1qjw7vdygvscl8rjkp8yjvi2lvfcirq7mc6sfs7a0224iab31aqj") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.11 (c (n "quasi") (v "0.3.11") (d (list (d (n "syntex_syntax") (r "^0.24.0") (o #t) (d #t) (k 0)))) (h "16x391db06sgpdc3jhwq0i0lxn87nqp7r1rvq78x3l8j6qyacj1s") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.3.12 (c (n "quasi") (v "0.3.12") (d (list (d (n "syntex_syntax") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "0vf25ckhfkcqc7lqnhczrjkps7s8244lb6zajc92wb72dav5cmca") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.4.0 (c (n "quasi") (v "0.4.0") (d (list (d (n "syntex_syntax") (r "^0.26.0") (o #t) (d #t) (k 0)))) (h "01g91gfjl35jmjhdxlhansx2rp86zysnc7kz0psvmq9xj7br1js5") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.5.0 (c (n "quasi") (v "0.5.0") (d (list (d (n "syntex_syntax") (r "^0.27.0") (o #t) (d #t) (k 0)))) (h "1vx8fbqnwmphg82lgybysn7a5swz3cl0rb9vn6fia3jc0jmkpfnm") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.6.0 (c (n "quasi") (v "0.6.0") (d (list (d (n "syntex_syntax") (r "^0.28.0") (o #t) (d #t) (k 0)))) (h "016fgawrvwa94jlfnhg2ad8px7hkksj50hlmc4nhpk0rwjf0ra9d") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.7.0 (c (n "quasi") (v "0.7.0") (d (list (d (n "syntex_syntax") (r "^0.29.0") (o #t) (d #t) (k 0)))) (h "0agynrys4xjw0ayv0p6na83z0jnm71biywvx73bnjx6gvaxzg2lb") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.8.0 (c (n "quasi") (v "0.8.0") (d (list (d (n "syntex_syntax") (r "^0.30.0") (o #t) (d #t) (k 0)))) (h "0z44npbvhs9c0xhahxw32am6fbl2h6qr4pa8vhcrxinbzw13a5i5") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.9.0 (c (n "quasi") (v "0.9.0") (d (list (d (n "syntex_syntax") (r "^0.31.0") (o #t) (d #t) (k 0)))) (h "1kxvbp629wdcrfgcbw7i0vgwykw8ilzhpbnj5va37lrf5pgwddl5") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.10.0 (c (n "quasi") (v "0.10.0") (d (list (d (n "syntex_syntax") (r "^0.32.0") (o #t) (d #t) (k 0)))) (h "195jzfb3jm273gm2n4sl585bavx750zpfhc7p7npkv86ci7h9akh") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.11.0 (c (n "quasi") (v "0.11.0") (d (list (d (n "syntex_syntax") (r "^0.33.0") (o #t) (d #t) (k 0)))) (h "0y557ks3m1k6mfl5bcgiql2sj41hf7xzlz7cyf3pskkhcfsl6rdj") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.12.0 (c (n "quasi") (v "0.12.0") (d (list (d (n "syntex_syntax") (r "^0.35.0") (o #t) (d #t) (k 0)))) (h "1cmqs4ca0drlm8cis0y7mwf4lhsh2jcx9kqw9r4jidpaq7s60472") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-quasi-0.13.0 (c (n "quasi") (v "0.13.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.36.0") (o #t) (d #t) (k 0)))) (h "1m7j32w7xkmb6qdsx9qgk96zzw05jhxbixl51lba0zbvvz0w39ks") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.14.0 (c (n "quasi") (v "0.14.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.37.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.37.0") (o #t) (d #t) (k 0)))) (h "0r1ajxs9fz1wbb5nswlzl4xlk3rf60vp3bdmf8r9s9x1f86d07k2") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.15.0 (c (n "quasi") (v "0.15.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.38.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38.0") (o #t) (d #t) (k 0)))) (h "0q7r6hh5gwrwzvyqsjvrxa1ww9pjgfr22wp8k1y9hyw6bggc6pb1") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.16.0 (c (n "quasi") (v "0.16.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)))) (h "0qf1rgbvi62dw39gi236g35jrav6irgb3yh93xdimxwswplmckii") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.17.0 (c (n "quasi") (v "0.17.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.41.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.41.0") (o #t) (d #t) (k 0)))) (h "0l03hgixbg6k76nzdd7pmgq5bng20ndvfq60gq4sbf23xsmj0p4g") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.18.0 (c (n "quasi") (v "0.18.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.42.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42.0") (o #t) (d #t) (k 0)))) (h "0k63n6qkgg6hna1xvsy11218gxf40aawjsrkz9qaad544vrawznb") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.19.0 (c (n "quasi") (v "0.19.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.43.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.43.0") (o #t) (d #t) (k 0)))) (h "0wyink4h21hk3r2fgpkihczv009bhxjsvkih1rycmav22cwn1cgr") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.20.0 (c (n "quasi") (v "0.20.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.44.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.44.0") (o #t) (d #t) (k 0)))) (h "11c9ralnddac58jznk0z3by778irvd7zmvp8ma3aa4hz2jgkzn2h") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.20.1 (c (n "quasi") (v "0.20.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.44.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.44.0") (o #t) (d #t) (k 0)))) (h "1lrll5c27ggifv4fw3icpjv3bvjaw07hzr5jln1l86lk7d2k59cl") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.21.0 (c (n "quasi") (v "0.21.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.44.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.44.0") (o #t) (d #t) (k 0)))) (h "1mnrn1sdf7klxdc1vk06z2p08aj1zvyf6zsh3cvzs3s69010yllq") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.22.0 (c (n "quasi") (v "0.22.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.45.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.45.0") (o #t) (d #t) (k 0)))) (h "1jck3zfdln9zyzkfapmb3bixpnq3dlx531c219mvf7wxva36pvbb") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.23.0 (c (n "quasi") (v "0.23.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.46.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.46.0") (o #t) (d #t) (k 0)))) (h "1xf762x131fdy109s8qmcqn8wx5v4l6rmj8sdjzv9msg6y7r56wa") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.24.0 (c (n "quasi") (v "0.24.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.47.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.47.0") (o #t) (d #t) (k 0)))) (h "00z41vcxpwc6kzbc93bhbgg430s6hcbdby82cjhwkgax7g0h8dql") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.25.0 (c (n "quasi") (v "0.25.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.48.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.48.0") (o #t) (d #t) (k 0)))) (h "0cxig9pmlm5z2dp64dc8r06azz2jjy8a2xc87hhayl8y3g738qy2") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.25.1 (c (n "quasi") (v "0.25.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.48.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.48.0") (o #t) (d #t) (k 0)))) (h "0ngdlrny2nnrmrwqx9l9n1rv5kaahkawavmc55gz3lzhmnynka60") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.26.0 (c (n "quasi") (v "0.26.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.50.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.50.0") (o #t) (d #t) (k 0)))) (h "1cbx4n354in3db0phjxanc0csrgn1c4lkp3gxpqwanxz40lrkdwa") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.27.0 (c (n "quasi") (v "0.27.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.51.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.51.0") (o #t) (d #t) (k 0)))) (h "0k4mxyg7sg4bbn3v7y8bnph49ijhbzlgrhcwy3048q6wnbq3kk50") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.28.0 (c (n "quasi") (v "0.28.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.52.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.52.0") (o #t) (d #t) (k 0)))) (h "01n0x7rraqxg9z9ddzf7n5l2baj28q5w251vi47i72arwymwp2gb") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.29.0 (c (n "quasi") (v "0.29.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.54.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.54.0") (o #t) (d #t) (k 0)))) (h "1j8hsipzvh40gi1hk3hn1g8hidkimalgk21dqzxhlsnw8ra83gyw") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.30.0 (c (n "quasi") (v "0.30.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.55.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.55.0") (o #t) (d #t) (k 0)))) (h "1fn5d9f9gacl3ixhg9j1r6gr2pwzyb3d4fj69yrf6i0z3x2h63av") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.31.0 (c (n "quasi") (v "0.31.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.57") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.57") (o #t) (d #t) (k 0)))) (h "0vbwc93fih0z1snr99dxn3icka95mak2nnajm2cfmsihkq93ygfr") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

(define-public crate-quasi-0.32.0 (c (n "quasi") (v "0.32.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.58") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58") (o #t) (d #t) (k 0)))) (h "1csqqgz3aw85q570ywmhb34r3sqgi1sprf8xadfwzlfnai45ri0q") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy"))))))

