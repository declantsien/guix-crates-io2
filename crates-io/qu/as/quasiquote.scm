(define-module (crates-io qu as quasiquote) #:use-module (crates-io))

(define-public crate-quasiquote-0.0.0 (c (n "quasiquote") (v "0.0.0") (h "0sglr35rs2yyblpc6wihhr9glyx05hn2sag48964pz5ic24s2c8q") (y #t)))

(define-public crate-quasiquote-0.0.1 (c (n "quasiquote") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 2)) (d (n "quasiquote-proc-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "syn") (r "^2.0.12") (d #t) (k 2)))) (h "1b4a3qfp4bx71f87f6yd3mizyi78lza3s1sr2g4wn10wkb1c7cn6")))

