(define-module (crates-io qu as quasi_macros) #:use-module (crates-io))

(define-public crate-quasi_macros-0.1.0 (c (n "quasi_macros") (v "0.1.0") (d (list (d (n "syntax_ast_builder") (r "^0.1.0") (d #t) (k 0)))) (h "1iy3mg2k5pvlki2jyv8i9w0d61m2hn8xyckwz56rwksrxbaywnc2")))

(define-public crate-quasi_macros-0.1.1 (c (n "quasi_macros") (v "0.1.1") (d (list (d (n "aster") (r "^0.1.2") (d #t) (k 0)))) (h "05dkl0bgil690c5ij1p3jcxazcsk5l4hjdgip7msbkwhw9zbsxwj")))

(define-public crate-quasi_macros-0.1.3 (c (n "quasi_macros") (v "0.1.3") (d (list (d (n "aster") (r "^0.1.3") (d #t) (k 0)))) (h "1ravp7sawk73pzj50qynmxxc4r6aq6q617874hnfm5my9sd9igqy")))

(define-public crate-quasi_macros-0.1.5 (c (n "quasi_macros") (v "0.1.5") (d (list (d (n "aster") (r "*") (d #t) (k 0)))) (h "1s33jqcs3lc444jqn86lvawj36rh1d3hyr0liymrj5x2db6kk1ld")))

(define-public crate-quasi_macros-0.1.6 (c (n "quasi_macros") (v "0.1.6") (d (list (d (n "aster") (r "*") (d #t) (k 0)))) (h "1jy25c4g9ml0hjzrk4bkvsvqld8fakz4q8ac8r9z29vxpbl95qz8")))

(define-public crate-quasi_macros-0.1.7 (c (n "quasi_macros") (v "0.1.7") (d (list (d (n "aster") (r "*") (d #t) (k 0)))) (h "15w9c17h6mvw6qpa34pj8xmgcghg5183zl6ws6wgg8dvm2n7gz5m")))

(define-public crate-quasi_macros-0.1.8 (c (n "quasi_macros") (v "0.1.8") (d (list (d (n "aster") (r "*") (d #t) (k 0)))) (h "1v1qipy99cm5mn56s4f81k8903rvmw9lcg7wbw3c9rkbd97yjjf3")))

(define-public crate-quasi_macros-0.1.9 (c (n "quasi_macros") (v "0.1.9") (d (list (d (n "aster") (r "*") (d #t) (k 0)))) (h "1g07ypi8kw4bfbn7sll6f6kngi13pcx3zipvxpzi7zf5355n92kg")))

(define-public crate-quasi_macros-0.1.10 (c (n "quasi_macros") (v "0.1.10") (d (list (d (n "aster") (r "*") (d #t) (k 0)))) (h "0j5fjnifqac6gyb023mdc36ahyzfgpf5fihnk6sfizr9x4kyjpkz")))

(define-public crate-quasi_macros-0.2.0 (c (n "quasi_macros") (v "0.2.0") (d (list (d (n "aster") (r "*") (d #t) (k 0)))) (h "0j2wpgjb2zcdxvpglkd0l0lqfixi6zsq9xcz4jkc5fcw0w0vbcsx")))

(define-public crate-quasi_macros-0.3.0 (c (n "quasi_macros") (v "0.3.0") (d (list (d (n "aster") (r "*") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (d #t) (k 0)))) (h "06yw1zfsmd24g7gz745r15c9xf0ywkd27k4y9rjz7cygvxcg934v")))

(define-public crate-quasi_macros-0.3.1 (c (n "quasi_macros") (v "0.3.1") (d (list (d (n "aster") (r "*") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "04cpzin9gpmhd026gs7q3srnk36w6mmhzh4dp1fa4x1vx6dd8pc7")))

(define-public crate-quasi_macros-0.3.2 (c (n "quasi_macros") (v "0.3.2") (d (list (d (n "aster") (r "*") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "0nnhahf4r4qxmld9rbr0r4g70yd3c7hnlcjq417rg37hgvc6rpzy")))

(define-public crate-quasi_macros-0.3.4 (c (n "quasi_macros") (v "0.3.4") (d (list (d (n "aster") (r ">= 0.4.10") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "0f7k5h4r1gg24yhnpmgc06hsbrxd8s6y1xq85b6pzqb43sav3yc6")))

(define-public crate-quasi_macros-0.3.5 (c (n "quasi_macros") (v "0.3.5") (d (list (d (n "aster") (r "^0.5.0") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "1c5hdwkx5p7j3bvif4d9cmb3y9vm9fsgwnhnyzazddhz5ndiic0z")))

(define-public crate-quasi_macros-0.3.6 (c (n "quasi_macros") (v "0.3.6") (d (list (d (n "aster") (r "^0.6.0") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "14987p6801hpdf0l4gryjyj1513cp3mdkk1xc7d9ph2873g4l3z5")))

(define-public crate-quasi_macros-0.3.7 (c (n "quasi_macros") (v "0.3.7") (d (list (d (n "aster") (r "^0.7.0") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "1qi2i92d305fwqqd8md9f351xw80i8rnbbd88j77269ys3yklz1p")))

(define-public crate-quasi_macros-0.3.8 (c (n "quasi_macros") (v "0.3.8") (d (list (d (n "aster") (r "^0.8.0") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "01xpmhww4jdpw4kv6sb38d5pvr782j5qy7n8yg7laacac8chj863")))

(define-public crate-quasi_macros-0.3.9 (c (n "quasi_macros") (v "0.3.9") (d (list (d (n "aster") (r "^0.8.0") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "18lpirlr840a35davvqjlicd41slnfa6yj0qwzx5khgjz3sa47ww")))

(define-public crate-quasi_macros-0.3.10 (c (n "quasi_macros") (v "0.3.10") (d (list (d (n "aster") (r "^0.9.0") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "0ckdgv3ghlwimb5b5v6k72kraf38kiksbmgrp6n42j944zyylvbz")))

(define-public crate-quasi_macros-0.3.11 (c (n "quasi_macros") (v "0.3.11") (d (list (d (n "aster") (r "^0.9.2") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "12n3sqfx771ks67iflbs7i5bj85kcy899ym4xrspg3qjnr39v5l4")))

(define-public crate-quasi_macros-0.3.12 (c (n "quasi_macros") (v "0.3.12") (d (list (d (n "aster") (r "^0.9.3") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "*") (k 0)))) (h "0v5dcvj7796j2vmdnq3g8f94wji60iml76irzvfmv9qmljaqm86b")))

(define-public crate-quasi_macros-0.4.0 (c (n "quasi_macros") (v "0.4.0") (d (list (d (n "aster") (r "^0.10.0") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.4.0") (k 0)))) (h "13rh6wphq6fsss4xyv1faqh3b223frzvd649q8gv7g3y4a55ch0r")))

(define-public crate-quasi_macros-0.3.13 (c (n "quasi_macros") (v "0.3.13") (d (list (d (n "aster") (r "^0.9.3") (d #t) (k 2)) (d (n "quasi") (r "*") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.3.12") (k 0)))) (h "11bf06phz1wkbxmzd6qxwpzkawb9mgfxkpfij85zn2mgsk98ip83")))

(define-public crate-quasi_macros-0.5.0 (c (n "quasi_macros") (v "0.5.0") (d (list (d (n "aster") (r "^0.11.0") (d #t) (k 2)) (d (n "quasi") (r "^0.5.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.5.0") (k 0)))) (h "0ap5w8dn10fca284cp4j5zx34kbyr9c0p95zq1j68s60zphvnpcp")))

(define-public crate-quasi_macros-0.6.0 (c (n "quasi_macros") (v "0.6.0") (d (list (d (n "aster") (r "^0.12.0") (d #t) (k 2)) (d (n "quasi") (r "^0.6.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.6.0") (k 0)))) (h "10h005vpjd5499gb1f93jsgl78m2khr92w319njp792mn8xa2q7n")))

(define-public crate-quasi_macros-0.7.0 (c (n "quasi_macros") (v "0.7.0") (d (list (d (n "aster") (r "^0.13.0") (d #t) (k 2)) (d (n "quasi") (r "^0.7.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.7.0") (k 0)))) (h "0qv4paz2qmzjpxi9gsdrinrff4idm3qcwg87hfg9sdwrw3h81b3l")))

(define-public crate-quasi_macros-0.8.0 (c (n "quasi_macros") (v "0.8.0") (d (list (d (n "aster") (r "^0.14.0") (d #t) (k 2)) (d (n "quasi") (r "^0.8.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.8.0") (k 0)))) (h "08krgxbbdavfai7931c2qwr0jk1p21n3aymdbk5bm3rf0pjrrql5")))

(define-public crate-quasi_macros-0.9.0 (c (n "quasi_macros") (v "0.9.0") (d (list (d (n "aster") (r "^0.15.0") (d #t) (k 2)) (d (n "quasi") (r "^0.9.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.9.0") (k 0)))) (h "15jmgpbgbydcp2vaq1af1ylx77k84jq83fy3bzsxvrm65d7i4wdb")))

(define-public crate-quasi_macros-0.10.0 (c (n "quasi_macros") (v "0.10.0") (d (list (d (n "aster") (r "^0.16.0") (d #t) (k 2)) (d (n "quasi") (r "^0.10.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.10.0") (k 0)))) (h "1fy2v4jk7mzm6qhw66y9mzzd5ybxix27ya0hh80l12a4bcm528ni")))

(define-public crate-quasi_macros-0.11.0 (c (n "quasi_macros") (v "0.11.0") (d (list (d (n "aster") (r "^0.17.0") (d #t) (k 2)) (d (n "quasi") (r "^0.11.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.11.0") (k 0)))) (h "0lai7w2kssadz7mnkii0q0x7fzfd1b0bzq3lz063w6vbqpl8szyk")))

(define-public crate-quasi_macros-0.12.0 (c (n "quasi_macros") (v "0.12.0") (d (list (d (n "aster") (r "^0.18.0") (d #t) (k 2)) (d (n "quasi") (r "^0.12.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.12.0") (k 0)))) (h "0bkp5rgi1x6a8ayjzils0y1bkbknb4mbsvyibwx33ni1qpcaf2qh")))

(define-public crate-quasi_macros-0.13.0 (c (n "quasi_macros") (v "0.13.0") (d (list (d (n "aster") (r "^0.19.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.13.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.13.0") (k 0)))) (h "11gdzqy6244avf1mvf5x6vhhsihl9qblrb1afrqp8whmxqh575m0") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.14.0 (c (n "quasi_macros") (v "0.14.0") (d (list (d (n "aster") (r "^0.20.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.14.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.14.0") (k 0)))) (h "024klvixl50748dz44bpffjff2qrg0nxcvsls24wh61blqnq8jsn") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.15.0 (c (n "quasi_macros") (v "0.15.0") (d (list (d (n "aster") (r "^0.21.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.15.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.15.0") (k 0)))) (h "0vmdb4115ggmwy6vrnga7c29s0z959j4xzhp3n4pnb8ksd342dgb") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.16.0 (c (n "quasi_macros") (v "0.16.0") (d (list (d (n "aster") (r "^0.22.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.16.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.16.0") (k 0)))) (h "1if9fz160qfp8c4izslgdbphw3plax3cc29rspj3hskvymzhnlwv") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.17.0 (c (n "quasi_macros") (v "0.17.0") (d (list (d (n "aster") (r "^0.24.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.17.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.17.0") (k 0)))) (h "122g1fydxx2x0rba08m0i7iz16p6gmmi6gm3glh66274iqifpwa0") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.18.0 (c (n "quasi_macros") (v "0.18.0") (d (list (d (n "aster") (r "^0.25.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.18.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.18.0") (k 0)))) (h "190h63zy0gqfsnrby8l25vmi5jwgmsc6v92s4gn7wr9yxqlbvxhz") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.19.0 (c (n "quasi_macros") (v "0.19.0") (d (list (d (n "aster") (r "^0.26.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.19.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.19.0") (k 0)))) (h "0rg71pizis4zk2zrc0pwp34shswszph5i9rr6kcv7gykgfxbzppl") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.20.0 (c (n "quasi_macros") (v "0.20.0") (d (list (d (n "aster") (r "^0.27.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.20.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.20.0") (k 0)))) (h "0pviydldkmkf31yjwsznljmwby3z4sb3iy37wbj58ppahmib7hmd") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.21.0 (c (n "quasi_macros") (v "0.21.0") (d (list (d (n "aster") (r "^0.29.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.21.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.21.0") (k 0)))) (h "1s1iziyws68hhgq52zvbph3dmzzsi4jg0dj7bfnhdmpx38jb4k2s") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.22.0 (c (n "quasi_macros") (v "0.22.0") (d (list (d (n "aster") (r "^0.30.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.22.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.22.0") (k 0)))) (h "1wrvmv654sslb5vkg6s8rx800dx8xbmfklssf6vk6lspjwpsqdh5") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.23.0 (c (n "quasi_macros") (v "0.23.0") (d (list (d (n "aster") (r "^0.31.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.23.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.23.0") (k 0)))) (h "18d9vnwx94g3lqxjid3866s3q9z0vjgscyma07q77269ys9rkbky") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.24.0 (c (n "quasi_macros") (v "0.24.0") (d (list (d (n "aster") (r "^0.32.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.24.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.24.0") (k 0)))) (h "0hrnsr00gk37k8zdrg9radrslvraka034kcspy0wfb9n4m2a9f6v") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.25.0 (c (n "quasi_macros") (v "0.25.0") (d (list (d (n "aster") (r "^0.33.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.25.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.25.0") (k 0)))) (h "0c017a0qb55ipxflm8iwabl357ywn3b4hfkdnl23fx5qxwrknicw") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.25.1 (c (n "quasi_macros") (v "0.25.1") (d (list (d (n "aster") (r "^0.33.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.25") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.25") (k 0)))) (h "15qwhjv9l94mli4v6qpyb4w396mnq22fml9szz4g3829j4psbsqh") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.26.0 (c (n "quasi_macros") (v "0.26.0") (d (list (d (n "aster") (r "^0.34.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.26") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.26") (k 0)))) (h "157iv31xv9d80dq5sl6zcva5qwzg1gr70cpyd1gngbkc608gz9s2") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.27.0 (c (n "quasi_macros") (v "0.27.0") (d (list (d (n "aster") (r "^0.35.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.27") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.27") (k 0)))) (h "1mav89l6kd3q376fbisxnbdgnr2j9ajgsvvndbyqjza6l2jwknnz") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.28.0 (c (n "quasi_macros") (v "0.28.0") (d (list (d (n "aster") (r "^0.36.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.28") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.28") (k 0)))) (h "0lqzdxpxq0pnl627g52cw2daslsx2dwas91v9cxy4wbvfs9ys063") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.29.0 (c (n "quasi_macros") (v "0.29.0") (d (list (d (n "aster") (r "^0.38.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.29.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.29.0") (k 0)))) (h "01i7k8v062jda0lcw4z5qak7i5pcjfdz198l31p1abhgxw876pvk") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.30.0 (c (n "quasi_macros") (v "0.30.0") (d (list (d (n "aster") (r "^0.39.0") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.30.0") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.30.0") (k 0)))) (h "0srq2wlmqz5nismnnwdw50mjshs45qmhvxpipq03cw6g0izwvrsc") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.31.0 (c (n "quasi_macros") (v "0.31.0") (d (list (d (n "aster") (r "^0.40") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.31") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.31") (k 0)))) (h "16vlmgyrr04pdc25w4qj3fi8aqilyfnhakcn9jibv8h4zq3iqq44") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

(define-public crate-quasi_macros-0.32.0 (c (n "quasi_macros") (v "0.32.0") (d (list (d (n "aster") (r "^0.41") (d #t) (k 2)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quasi") (r "^0.32") (d #t) (k 2)) (d (n "quasi_codegen") (r "^0.32") (k 0)))) (h "1p825s96wa9xcc01pm5f4nlb01nx0pah50qnwkbncrw1q9xwiki9") (f (quote (("unstable-testing" "clippy" "quasi/unstable-testing" "quasi_codegen/unstable-testing"))))))

