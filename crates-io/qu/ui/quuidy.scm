(define-module (crates-io qu ui quuidy) #:use-module (crates-io))

(define-public crate-quuidy-0.1.0 (c (n "quuidy") (v "0.1.0") (d (list (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0y1vc7la2by6yhxmrs0xh3rkrbvsil13qq4i2myj1209xxpjzrqd")))

(define-public crate-quuidy-0.1.1 (c (n "quuidy") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "002isjqm7g2mwwkyli4w74g63y2nbfiv9idjjlpkq37h2zgs8rhh")))

(define-public crate-quuidy-0.1.2 (c (n "quuidy") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "v7" "fast-rng"))) (d #t) (k 0)))) (h "1f115q0jp8wdxij9kiy26hmhbz4ibjv8vpjbgvk6hxnbq8iw9sx3")))

