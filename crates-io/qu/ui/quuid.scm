(define-module (crates-io qu ui quuid) #:use-module (crates-io))

(define-public crate-quuid-0.1.0 (c (n "quuid") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "14hvpml5zcyh6s0gfy297w12zrryji96f7czhgf4hwggsw3kpnxc")))

