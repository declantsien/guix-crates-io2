(define-module (crates-io qu -d qu-derive) #:use-module (crates-io))

(define-public crate-qu-derive-0.1.0 (c (n "qu-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "11d7lgk53fhp26410nbad4cs5yxh3bb7bwaacvcn1kxigzhp022z")))

(define-public crate-qu-derive-0.2.0 (c (n "qu-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ccn1721j1m6hl04h4q7wz2bqjib9apq8dc30j84096921565acj")))

(define-public crate-qu-derive-0.2.1 (c (n "qu-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r9h7j1b7nbj9camk1qfpf72c2c2kd3w40ka6gsn4d78w6kcz0sz")))

(define-public crate-qu-derive-0.3.0 (c (n "qu-derive") (v "0.3.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rqnc0ph38wllhl2l422wfjn0siqafcadlpwahvqz9nq4sg5qi14")))

(define-public crate-qu-derive-0.3.1 (c (n "qu-derive") (v "0.3.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vi76vkm97cixxpdpwlzh4z97k35dfqw791q0wpshz3il80kcliy")))

(define-public crate-qu-derive-0.4.2 (c (n "qu-derive") (v "0.4.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yglb9mmndmfl43c113ygy2dxa4n7i1hzg7xinvg1hgc1cdq456y")))

(define-public crate-qu-derive-0.3.2 (c (n "qu-derive") (v "0.3.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cfqgs65kwh7maq8d0zaw91s5q10mf36n19pbyhzcyk5snbvdr7x")))

(define-public crate-qu-derive-0.3.3 (c (n "qu-derive") (v "0.3.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n564k78k2ykbry4wambv1n0wjs158k49a97cv8vrlhi89ns31zq")))

(define-public crate-qu-derive-0.3.4 (c (n "qu-derive") (v "0.3.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lrkkbysjnj4knhvaawh2i49afwrjag1m1sbflsc5455mxha0j95")))

(define-public crate-qu-derive-0.6.0 (c (n "qu-derive") (v "0.6.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01nl4857rlkq54h7055gndnv82gkhih5px1h3rmbpx3348dbxagx")))

