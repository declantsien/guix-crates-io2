(define-module (crates-io qu ec quec) #:use-module (crates-io))

(define-public crate-quec-0.1.0 (c (n "quec") (v "0.1.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "05x02xwm27iy96ib3awd7j6vkhcgykjj93pzj39cwz4xbgkjckag")))

(define-public crate-quec-0.1.1 (c (n "quec") (v "0.1.1") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "0hbmyjk8f398ncqbn8xym35ykpsbf3829pkz13k3nqjld16jgxpf")))

(define-public crate-quec-0.1.2 (c (n "quec") (v "0.1.2") (d (list (d (n "console") (r "^0.15.7") (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1ax5gkgmyxn02yyrxhx4ykl8yvi6xy1x3b0mhs58qj9m2gxz1a35")))

(define-public crate-quec-0.1.3 (c (n "quec") (v "0.1.3") (d (list (d (n "console") (r "^0.15.7") (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "13scbx8bdj9mnmwnbdckimgngd9db2wd0pvh8017sck01cppbgs4")))

(define-public crate-quec-0.1.4 (c (n "quec") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream" "windows"))) (t "cfg(not(unix))") (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "0v34n2fwwrgn458v306zl5m5c54pi2nkgl4vycwrji1mxap7nh2n")))

(define-public crate-quec-0.1.5 (c (n "quec") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream" "windows"))) (t "cfg(not(unix))") (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1x1mljyjza8h4izhs4jbn14garymwwy7w3bdmza2z2nsmappssyx")))

