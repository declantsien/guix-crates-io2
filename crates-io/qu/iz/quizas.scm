(define-module (crates-io qu iz quizas) #:use-module (crates-io))

(define-public crate-quizas-0.1.0 (c (n "quizas") (v "0.1.0") (h "0j85if73zz6x17sqrqys0swmk79w66gawjcl6dgvy2hwdfff46px")))

(define-public crate-quizas-0.1.1 (c (n "quizas") (v "0.1.1") (h "0skw5z2jnq7a73nbm28rw0rpbjm2zjalqvps6h09gb573wrfad21")))

(define-public crate-quizas-0.1.2 (c (n "quizas") (v "0.1.2") (h "1ds1k6pkjz4bycw687ydm66g5wc7szsrifidvgdrdc23f2alfgjj")))

(define-public crate-quizas-0.1.2-1 (c (n "quizas") (v "0.1.2-1") (h "1wp1q7ws1z1wwxdbz3dpigqnbliwh91k8203h23483w7vya62idr")))

(define-public crate-quizas-0.1.3 (c (n "quizas") (v "0.1.3") (h "1vqs6ln8wcgw3m30j3rn41s7493rfz9id301xl6kgxfzssqmj7km")))

(define-public crate-quizas-0.1.4 (c (n "quizas") (v "0.1.4") (h "117hz36rv5jccffk9lnmw2ah2b6c0jpxxyx8shmy5b4x7vqy24q0")))

(define-public crate-quizas-0.1.5 (c (n "quizas") (v "0.1.5") (h "1jxizl15piy81dg86bmrc9gp9lk5f4i0i4kziy1fgv8lfgxnd7p0")))

(define-public crate-quizas-0.1.6 (c (n "quizas") (v "0.1.6") (h "11vdqa3hfy3v03l5023xq7aqqaiwa12wlr4jqq64l1mlpgs2bn4d")))

(define-public crate-quizas-0.1.7 (c (n "quizas") (v "0.1.7") (h "0120j52s45a6kynajv5n21jxbggq8x04ns4llyf9b0v10507shh1")))

(define-public crate-quizas-0.1.8 (c (n "quizas") (v "0.1.8") (h "0s47h6s1g0xmli0n9fffmcndmsdy70wm5dj2b9w98fk85ax8vi63")))

(define-public crate-quizas-0.1.9 (c (n "quizas") (v "0.1.9") (h "0g9jw9xic7dy8pl8kfikx155a86yy2hdkib89348a1srfrn9m0z5")))

