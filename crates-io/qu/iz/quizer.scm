(define-module (crates-io qu iz quizer) #:use-module (crates-io))

(define-public crate-quizer-1.0.0 (c (n "quizer") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1zai94cl6vywsjpim15lfjsva2jb09skzr0hczgyd8d9ny8kd963") (y #t)))

