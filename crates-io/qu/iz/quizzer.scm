(define-module (crates-io qu iz quizzer) #:use-module (crates-io))

(define-public crate-quizzer-1.0.0 (c (n "quizzer") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "17k7z95rbbbs30ys0f1np3162g003hh376bxj26ydgyj4w30cnqs")))

(define-public crate-quizzer-1.0.1 (c (n "quizzer") (v "1.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0cqmqg8a9rfbbpr0v5fd4ga11l11mq6az81zss45ngb6sj0lv758")))

(define-public crate-quizzer-1.0.2 (c (n "quizzer") (v "1.0.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0vcwh863h1lyk5crdjykw0hywbycsjnrbb4vbmcbniqn2jk2mabj")))

(define-public crate-quizzer-1.1.2 (c (n "quizzer") (v "1.1.2") (d (list (d (n "bincode") (r "^1.3.3") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (o #t) (d #t) (k 0)))) (h "1ysyr7sqa1n0fip5pnyy5c9pvm4g5g47qq5vyl7j91k2fpslibgz") (s 2) (e (quote (("json" "dep:serde_json") ("default" "dep:bincode"))))))

