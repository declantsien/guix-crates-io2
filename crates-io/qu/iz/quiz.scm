(define-module (crates-io qu iz quiz) #:use-module (crates-io))

(define-public crate-quiz-0.0.1 (c (n "quiz") (v "0.0.1") (d (list (d (n "figures-rs") (r ">=0.0.1, <0.0.2") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)))) (h "1wi0q5y695fqi6k1g9g0mvb9xnlz5790cpdrdvp5sm5369zvmh82")))

(define-public crate-quiz-0.0.2 (c (n "quiz") (v "0.0.2") (d (list (d (n "figures-rs") (r ">=0.0.1, <0.0.2") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)))) (h "04ksk5mjfw2qbiw55dys7qnywxg1amn582cjs0z5c3r2p9p9p3wf")))

(define-public crate-quiz-0.0.3 (c (n "quiz") (v "0.0.3") (d (list (d (n "figures-rs") (r ">=0.0.1, <0.0.2") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)))) (h "1zf7ka4qz2nd306nl1vhs75zyzjbsm88mazqwvpyzj50hkfp2qap")))

