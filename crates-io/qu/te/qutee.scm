(define-module (crates-io qu te qutee) #:use-module (crates-io))

(define-public crate-qutee-0.1.0 (c (n "qutee") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)))) (h "1lbnn435mpjay3jbhw7vk974izmmxdqyk8qi5ingf8xkvvb8lb6l")))

(define-public crate-qutee-0.1.1 (c (n "qutee") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)))) (h "0wn7gsxnsdsydq9xib2g4iywxzwwg8xxc0v90l7qi7hqysyf7bj5")))

(define-public crate-qutee-0.2.0 (c (n "qutee") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)))) (h "19qinmhx1019cijnqc59i2m46r39jyih02r4bj06m9a74fkca4m7")))

(define-public crate-qutee-0.2.1 (c (n "qutee") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "test-case") (r "^3.0") (d #t) (k 2)))) (h "1szmqfrqb0ry3z8skkbvbd4a3amw2n15yhfpkr5na3qsfz5vp92w")))

