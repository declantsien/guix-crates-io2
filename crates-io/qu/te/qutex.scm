(define-module (crates-io qu te qutex) #:use-module (crates-io))

(define-public crate-qutex-0.0.1 (c (n "qutex") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "03y7da9k4kcv4zgji4a6l4drrfay1lrvh4lxry5z84pg7qrdnx3x")))

(define-public crate-qutex-0.0.2 (c (n "qutex") (v "0.0.2") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0m02k3ypmn33bipihv2cavj4hglylq91v8xvyganvkmivjy8n5x0")))

(define-public crate-qutex-0.0.3 (c (n "qutex") (v "0.0.3") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1bqy12s8g1j75rdb85iga6z10d9360vfwv53ada7zb47hj0yivhl")))

(define-public crate-qutex-0.0.4 (c (n "qutex") (v "0.0.4") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1i3pqcya1qh2bqc71nh7yqqcn6y9vld295zqq8iybh61wcd8a9kg")))

(define-public crate-qutex-0.0.5 (c (n "qutex") (v "0.0.5") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1ivdc3hgvwjl53r7z2lbczvxifb0hrm9zrkqh2kpq2vwizkjaxmg")))

(define-public crate-qutex-0.0.6 (c (n "qutex") (v "0.0.6") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0r066hq1lqv5mmnb0plq7nd06bnfmda3lnjipkm6q4hn6xvq6bfk")))

(define-public crate-qutex-0.0.7 (c (n "qutex") (v "0.0.7") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "15y47iljig28chbgzxkyi3cqr6lf2njc7i1bmhrn73bqp15al5k3")))

(define-public crate-qutex-0.0.8 (c (n "qutex") (v "0.0.8") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0lrayh86zpz5w9ir80g5m09d6p5pgh0la826cwxlpp9f8w57n70h")))

(define-public crate-qutex-0.0.9 (c (n "qutex") (v "0.0.9") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1c4y5v8igdzzf26vwv4dh10iwnriqiwcmr4q7b47bsgwnpbc0haq")))

(define-public crate-qutex-0.1.0 (c (n "qutex") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "05r3317nqyvcmisvgccn6ryw055jadqcli11a36drhm14c5j8vx4")))

(define-public crate-qutex-0.1.1 (c (n "qutex") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0mrikgk8k1xy3mvyim46ricgzjaa6jx8823x8y6islfxi7s1fqwh")))

(define-public crate-qutex-0.2.0 (c (n "qutex") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0f46gdfm67kvj417saxk5sy37n4hl9fhlmv2v22si2baf8hpfcrw")))

(define-public crate-qutex-0.2.1 (c (n "qutex") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1rzhyxaszb47p4n3pjp88npigywlgisnh3lg3ssyin67x6agwssx")))

(define-public crate-qutex-0.2.2 (c (n "qutex") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "1xfgxfzqb15l8cnlr425al6dqny2w6gl304m4zahlcwlaqcsjwmm")))

(define-public crate-qutex-0.2.3 (c (n "qutex") (v "0.2.3") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "188by93n3k5scyy6ng3ccv8ssfsqk53imgi5qbj26b4zzgwjahq8")))

(define-public crate-qutex-0.2.4 (c (n "qutex") (v "0.2.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures03") (r "^0.3.0-alpha.14") (f (quote ("compat"))) (o #t) (d #t) (k 0) (p "futures-preview")))) (h "0gw8164yaacz9hxjc5cbc3d8vbbpj0inn2j5z6bc2wyplcdsb96d") (f (quote (("default") ("async_await" "futures03"))))))

(define-public crate-qutex-0.2.5 (c (n "qutex") (v "0.2.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures03") (r "^0.3.0-alpha.14") (f (quote ("compat"))) (o #t) (d #t) (k 0) (p "futures-preview")))) (h "1kg283sbyp4qsd9sc4npyrn5gfy5x3m5z0c7zf03wkwnqxhz7fvk") (f (quote (("default") ("async_await" "futures03"))))))

(define-public crate-qutex-0.2.6 (c (n "qutex") (v "0.2.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures03") (r "^0.3.0-alpha.14") (f (quote ("compat"))) (o #t) (d #t) (k 0) (p "futures-preview")))) (h "0mqp7a3yrr9cicww5k89i2qcvx8yw1lxqgq3cb5f7c6qwww84xqi") (f (quote (("default") ("async_await" "futures03"))))))

