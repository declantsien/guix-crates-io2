(define-module (crates-io qu te qute) #:use-module (crates-io))

(define-public crate-qute-0.0.0 (c (n "qute") (v "0.0.0") (h "1f3cdfhan3drz8qwp40md849dqcyx2a0ss6mscgil5mjgvp8vasx")))

(define-public crate-qute-0.0.1 (c (n "qute") (v "0.0.1") (h "18b0ci4kkdkzp0r2gr4qzlhpylysxw89b0s49idm6dbgi9l6j8qw")))

(define-public crate-qute-0.0.2 (c (n "qute") (v "0.0.2") (h "1x96296ciq9wg8asafq71rcz4hkgwck6xz2ad6nxjni0a6r272rb")))

(define-public crate-qute-0.0.3 (c (n "qute") (v "0.0.3") (h "1fih3sm71vzdgjbq362asns9lsrkd3lbb41i7zph4g4s9y9f3hn3")))

(define-public crate-qute-0.0.4 (c (n "qute") (v "0.0.4") (h "0jl4i4c0hgl6dwwwgqcrvnhxbdgmhln142b5al9fbizzva8nfpl1")))

(define-public crate-qute-0.0.5 (c (n "qute") (v "0.0.5") (h "1ilf933n0kf0x3rpl5in219cwcxn2zm3waa16gzmr6jh9vzjp0vc")))

(define-public crate-qute-0.0.6 (c (n "qute") (v "0.0.6") (h "1nry3ksv07kj377wwiw750a8my5wp0lbvd3wg3w3iv4k8iwqjrm0")))

(define-public crate-qute-0.0.7 (c (n "qute") (v "0.0.7") (h "0b0n09iysk682hk1wy95q8pxicc8pldgsabg3r6lip8ydacwgz34")))

(define-public crate-qute-0.0.8 (c (n "qute") (v "0.0.8") (h "0ki3hwhblycmgvb4nx1grbzk8gfdpv4xgibayiv88ncjcsmnakkd")))

(define-public crate-qute-0.0.9 (c (n "qute") (v "0.0.9") (h "1aq1dr5j2j45zbxv9g2xzphsc50x7x4ayr9bfxj6ggykca04nfzv")))

(define-public crate-qute-0.0.10 (c (n "qute") (v "0.0.10") (h "06wlc48ykp53h6gjbckrzb3kzb9nnbw18m4szm9rvjcsgrqpx2f5")))

(define-public crate-qute-0.0.11 (c (n "qute") (v "0.0.11") (h "16qq59lpkgm0a3af71ajk4d5xwspxmdggm6kg7lsikmxz9v2xl3a")))

(define-public crate-qute-0.0.12 (c (n "qute") (v "0.0.12") (h "1licih9ab1mgs93dbjyqqfl2rakbnx8qcbp8kyjrbm0p8bwq1pa1")))

(define-public crate-qute-0.0.13 (c (n "qute") (v "0.0.13") (h "1cab4py22a6pa7h548x57zd3gi1b76fx45r0hl2mf9946js69vvq")))

