(define-module (crates-io qu ot quote-use-macros) #:use-module (crates-io))

(define-public crate-quote-use-macros-0.7.2 (c (n "quote-use-macros") (v "0.7.2") (d (list (d (n "derive-where") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bzlqg6hdi2b0hcamcfw2qyy25w5311bnid2frx025hgwb3l9slp") (f (quote (("prelude_std" "prelude_core") ("prelude_core") ("prelude_2021" "prelude_core") ("namespace_idents") ("default"))))))

(define-public crate-quote-use-macros-0.8.0 (c (n "quote-use-macros") (v "0.8.0") (d (list (d (n "derive-where") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "proc-macro-utils") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (k 0)))) (h "1vw067blmwxxk4ym0sprnid3a0fxwk7l1hp2h1bj034hhwipgn3i")))

