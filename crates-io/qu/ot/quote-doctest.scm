(define-module (crates-io qu ot quote-doctest) #:use-module (crates-io))

(define-public crate-quote-doctest-0.1.0 (c (n "quote-doctest") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "temp-env") (r "^0.2") (d #t) (k 2)))) (h "1g0lcgslx4b9vr4dqbcya7y6xjnn7q29dn5pg0p77wsl0qkfx2mq")))

(define-public crate-quote-doctest-0.1.1 (c (n "quote-doctest") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "temp-env") (r "^0.2") (d #t) (k 2)))) (h "1vg867sgjjfahzwgxmccwpa3ffwk20ds6dlvbrvgr9ncc2b07qfa")))

(define-public crate-quote-doctest-0.1.2 (c (n "quote-doctest") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "temp-env") (r "^0.2") (d #t) (k 2)))) (h "0863b2ymx6di388n0fn5ffi0aqal1c7sq1739hdcwwr0i94zwhm3")))

(define-public crate-quote-doctest-0.2.0 (c (n "quote-doctest") (v "0.2.0") (d (list (d (n "prettyplease") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing"))) (k 0)) (d (n "temp-env") (r "^0.2") (d #t) (k 2)))) (h "06bgd8135dqi7x8dmafk9xnknpcix4bf8ddyr4rz6j9jyfl510p7") (f (quote (("default" "prettyplease"))))))

(define-public crate-quote-doctest-0.2.1 (c (n "quote-doctest") (v "0.2.1") (d (list (d (n "prettyplease") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing"))) (k 0)) (d (n "temp-env") (r "^0.2") (d #t) (k 2)))) (h "0nw04rn6801mr49jm7nin61vfvz73nx4xrwj3mmk25rgn357pagp") (f (quote (("default" "prettyplease"))))))

(define-public crate-quote-doctest-0.2.2 (c (n "quote-doctest") (v "0.2.2") (d (list (d (n "prettyplease") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing"))) (k 0)) (d (n "temp-env") (r "^0.2") (d #t) (k 2)))) (h "0m08y0kf48s9jf4app4c90hxi51h51d0iyh1vg8im0v1g74kw790") (f (quote (("default" "prettyplease"))))))

(define-public crate-quote-doctest-0.3.0 (c (n "quote-doctest") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3") (f (quote ("post_process" "token_stream"))) (d #t) (k 0)))) (h "185m381323znk4gnm9zn8zzwdf52rqf6rpqnx9jkyk7j91ln0r63") (f (quote (("pretty_please" "rust-format/pretty_please") ("default" "pretty_please"))))))

(define-public crate-quote-doctest-0.3.1 (c (n "quote-doctest") (v "0.3.1") (d (list (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.2") (f (quote ("post_process" "token_stream"))) (d #t) (k 0)))) (h "1hy55ma6pynq88hyn9whqyixb7946kqk54xmvwb834pp0fa3ajvw") (f (quote (("pretty_please" "rust-format/pretty_please") ("default" "pretty_please"))))))

(define-public crate-quote-doctest-0.3.2 (c (n "quote-doctest") (v "0.3.2") (d (list (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (f (quote ("post_process" "token_stream"))) (d #t) (k 0)))) (h "0hwq453cfax2fxcw9g2c2028s3yqfn391y6bh02484axqq5nb7lj") (f (quote (("pretty_please" "rust-format/pretty_please") ("default" "pretty_please"))))))

