(define-module (crates-io qu ot quoth-macros) #:use-module (crates-io))

(define-public crate-quoth-macros-0.1.0 (c (n "quoth-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ajxs86vqwibnknq9mihznb4w5an0smwn045h5y703wqysn7gs20")))

(define-public crate-quoth-macros-0.1.1 (c (n "quoth-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0wjqd9vn7j9qg8my6hqdzvqprbpa702ypwx50wix3sibl7d2xix2")))

(define-public crate-quoth-macros-0.1.2 (c (n "quoth-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ankn8prs7h3ivaz0j6ibmvfq6dk0kn0zgbdxmybm0fk4aqpaqfr")))

(define-public crate-quoth-macros-0.1.3 (c (n "quoth-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "076p4zx79a6pdwidmgb3n3r9r897zdc6vh62ayyhbwymj4z3rbvh")))

(define-public crate-quoth-macros-0.1.4 (c (n "quoth-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bj85zvaw49qg84shvfs35i8c09idccvm82290kic7v6i8knhczl")))

(define-public crate-quoth-macros-0.1.5 (c (n "quoth-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1k9n1v5fi5hbjkbhmqwqhmbq3rxl9f7gsc6v4xhdckl0qaqxmyph")))

(define-public crate-quoth-macros-0.1.6 (c (n "quoth-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "01gwzycjp01jci7l6c3lmv943zi5jay47zj6afn12k4642pixlwp")))

