(define-module (crates-io qu ot quotient-filter) #:use-module (crates-io))

(define-public crate-quotient-filter-0.1.0 (c (n "quotient-filter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1qrq6p5vgq0m6607brjyjgbpx7w70a457l771110hr2c31x02k7k")))

(define-public crate-quotient-filter-0.1.1 (c (n "quotient-filter") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0ny1j4pdc95hdrm60dppgrvp025cmps97mq2496h45m2bnjx9fcf")))

(define-public crate-quotient-filter-0.1.2 (c (n "quotient-filter") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "const-murmur3") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1awdb5l190w8f8akb263i3fmq6jxh9y8qipzmlqjqy6w5kkwjcgp")))

(define-public crate-quotient-filter-0.1.3 (c (n "quotient-filter") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "const-murmur3") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1vmvpl30czwmqa1h3p45incm9vyv17clhwkibqrhs5h8mz08gxzp")))

(define-public crate-quotient-filter-0.2.0 (c (n "quotient-filter") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "const-murmur3") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1r13y8q1siwyyyfyy7lrvn9dxxww00apxvyr14yq1msspskgclr0")))

(define-public crate-quotient-filter-0.2.1 (c (n "quotient-filter") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "const-murmur3") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1x9smfm913i53qh901krpacrba6vnnkkn4nx1ah1kszak6k9w0lg")))

(define-public crate-quotient-filter-0.2.2 (c (n "quotient-filter") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "const-murmur3") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0s54l21b37xnhk6rxsyzfl846f96dhb6fps810hmkg42nbydqrbl")))

(define-public crate-quotient-filter-0.2.3 (c (n "quotient-filter") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "const-murmur3") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "05s228ahp73ma4gb9gmy6ap3kvmdwrxsfknrm60n5ag3q60f9kyr")))

