(define-module (crates-io qu ot quote_into_macro) #:use-module (crates-io))

(define-public crate-quote_into_macro-0.1.0 (c (n "quote_into_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "15fygycmgbhw32mds7wafgpxg9d275xn88sivvvkacsn56a0f9w8")))

(define-public crate-quote_into_macro-0.2.0 (c (n "quote_into_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "099myyik43bvxd65yn39wqr1864k9qcpa724zrd2nia0ijcjif1n")))

