(define-module (crates-io qu ot quoted_printable) #:use-module (crates-io))

(define-public crate-quoted_printable-0.1.0 (c (n "quoted_printable") (v "0.1.0") (h "1lz7p79sk68kjwysf136plff947vd04c7f7c3ghkccrlq3jdgr55")))

(define-public crate-quoted_printable-0.1.1 (c (n "quoted_printable") (v "0.1.1") (h "0ff746i5dx4kdf1br1vj1gh1lkvyw7bnpwljjfsimilvigih5xp3")))

(define-public crate-quoted_printable-0.2.0 (c (n "quoted_printable") (v "0.2.0") (h "1ja6db34kc8dcwqf9a42aww2gshj1vxl52gs27lsbcj3ba0kr8cr")))

(define-public crate-quoted_printable-0.3.0 (c (n "quoted_printable") (v "0.3.0") (h "0y1i652faalx3agb6aanbij0ls9zxm9cbp7yq3xdv3g1s5b59dgb")))

(define-public crate-quoted_printable-0.3.1 (c (n "quoted_printable") (v "0.3.1") (h "09ajzwxzndbwqaxv0ql57azyh0qklkjksa068irs5yy5fg2pbfyx")))

(define-public crate-quoted_printable-0.3.2 (c (n "quoted_printable") (v "0.3.2") (h "1xvl9h0lag39wk9kfgsm3hyn6jj4812wh14rvhqhnrz3ljcc37yl")))

(define-public crate-quoted_printable-0.3.3 (c (n "quoted_printable") (v "0.3.3") (h "17x8d0i8f3amz0fvz96jnf7mhx8920q9sdr34qjaxv5y2wz0c6pc")))

(define-public crate-quoted_printable-0.4.0 (c (n "quoted_printable") (v "0.4.0") (h "0njv8zd40qaxnf66mpq3v1cqfq8xfaban94slbk6dcfpqscgl9j1")))

(define-public crate-quoted_printable-0.4.1 (c (n "quoted_printable") (v "0.4.1") (h "1667lg7bxdhyl2gdw3f45333zckb1c9ynnxqgds2x29828rxzkl6")))

(define-public crate-quoted_printable-0.4.2 (c (n "quoted_printable") (v "0.4.2") (h "1qihd6w90kb3148k9n388xlkcpgw1jz39gcwqwm2k6v3vg2q1c27")))

(define-public crate-quoted_printable-0.4.3 (c (n "quoted_printable") (v "0.4.3") (h "1ig9b5nr2i2f72smsjkkx8pwpxp9vz2090dhi7n4jdlj15mjaf0j")))

(define-public crate-quoted_printable-0.4.4 (c (n "quoted_printable") (v "0.4.4") (h "0814jrw7hfs4nbw26l3jnxss7gn16805i9589pa93y112n76nm46") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-quoted_printable-0.4.5 (c (n "quoted_printable") (v "0.4.5") (h "03rc6d5ym6glgvqahlrdammf0566aijpcb1qwcc3997pb772vviz") (f (quote (("std") ("default" "std"))))))

(define-public crate-quoted_printable-0.4.6 (c (n "quoted_printable") (v "0.4.6") (h "1jxm775bbpfdw4skl02f4jp5shicm5x6m66wbvyfxjqq343lxw90") (f (quote (("std") ("default" "std"))))))

(define-public crate-quoted_printable-0.4.7 (c (n "quoted_printable") (v "0.4.7") (h "1dniisibj9djfa498rj5hcgskypbq7wdvp4hri9mha6q4zv3jh52") (f (quote (("std") ("default" "std"))))))

(define-public crate-quoted_printable-0.4.8 (c (n "quoted_printable") (v "0.4.8") (h "0jcyi10gh2xzjvxls8r58gwc9yavw3iighkgnb1jwrjij8hncf2s") (f (quote (("std") ("default" "std"))))))

(define-public crate-quoted_printable-0.5.0 (c (n "quoted_printable") (v "0.5.0") (h "1c4fkkm95ff59vjmk6qi6p7sawlfschw3rcgq5l4nhvvi0p2iv3r") (f (quote (("std") ("default" "std"))))))

