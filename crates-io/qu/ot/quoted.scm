(define-module (crates-io qu ot quoted) #:use-module (crates-io))

(define-public crate-quoted-0.1.0 (c (n "quoted") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "11frqm9azc4z9aj9v5nizry47v9afk8hi64y571ga7dby44lf44c")))

(define-public crate-quoted-0.1.1 (c (n "quoted") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "116i6y862rwkzlx4szb2d470qigxdxnkcjdapvymhgwx40h13bcn")))

(define-public crate-quoted-0.1.2 (c (n "quoted") (v "0.1.2") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1z1xrsigwzj6p27gw68cz8m2bpz8m3bc525d5i7v1yvgr06p0sr7")))

