(define-module (crates-io qu ot quoth) #:use-module (crates-io))

(define-public crate-quoth-0.0.1 (c (n "quoth") (v "0.0.1") (h "05zsw8vnm0lld97sw96pfi8d2ijxn7sx334akciz06yxplfy852y")))

(define-public crate-quoth-0.1.0 (c (n "quoth") (v "0.1.0") (d (list (d (n "quoth-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "148mc7rdhrs0s54sy7ymrkc9fk11pdyszhmxw1a1sdp5zjy8p3ya")))

(define-public crate-quoth-0.1.1 (c (n "quoth") (v "0.1.1") (d (list (d (n "quoth-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1y713igv78y99k6j8pcjn3bqafprm4g121lqw9wjqnmw4szxljxb")))

(define-public crate-quoth-0.1.2 (c (n "quoth") (v "0.1.2") (d (list (d (n "quoth-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0v8gjw5r19i2f4j0p9qyb65xnvhz9vlgk7mn0hbpfwi1zm45l3b5")))

(define-public crate-quoth-0.1.3 (c (n "quoth") (v "0.1.3") (d (list (d (n "quoth-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1h6zin2v0c3l4piwijipfs4dn2sbq8bwcfqd9l2pmlpp1bksc1pj")))

(define-public crate-quoth-0.1.4 (c (n "quoth") (v "0.1.4") (d (list (d (n "quoth-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1h5hzi5x26xg2l21rr77bcsii2szkgpmynhlmzf9z1f3g9gdbyvs")))

(define-public crate-quoth-0.1.5 (c (n "quoth") (v "0.1.5") (d (list (d (n "quoth-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0aa2raqi6ccv2madf5b9f1kc1kf11bvvnpmzd2884n36q826ncn4")))

(define-public crate-quoth-0.1.6 (c (n "quoth") (v "0.1.6") (d (list (d (n "quoth-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "safe-string") (r "^0.1.10") (d #t) (k 0)))) (h "03mq6whxhvzlgpqhmvd8fq5zif7c8d7qplzdaj22aw4fv3zja4w5")))

