(define-module (crates-io qu ot quote_into) #:use-module (crates-io))

(define-public crate-quote_into-0.1.0 (c (n "quote_into") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "quote_into_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 2)))) (h "015sdssrnk3jfipnfvv62419zf8x0afmdfd78y5j09jy8k8w3szl")))

(define-public crate-quote_into-0.2.0 (c (n "quote_into") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "quote_into_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 2)))) (h "1fcrfvqfbvzm4k7nja47a6i15prn6zx6mln41yb5v2i2dzanx24k")))

