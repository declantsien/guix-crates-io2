(define-module (crates-io qu ot quote2) #:use-module (crates-io))

(define-public crate-quote2-0.1.0 (c (n "quote2") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote2-macros") (r "^0.1") (d #t) (k 0)))) (h "0l3ks24hcv73jb1n7r951cq54cz3zxm2l45s299kr4yhnxppcpaf")))

(define-public crate-quote2-0.2.0 (c (n "quote2") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote2-macros") (r "^0.2") (d #t) (k 0)))) (h "1ir1s8fz5yak4xq927lnr08sm9b4fq988hnw3v60wddvbbwk06a1") (y #t)))

(define-public crate-quote2-0.3.0 (c (n "quote2") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "quote2-macros") (r "^0.3") (d #t) (k 0)))) (h "1cg2gbnz330kdk0m69ykg23xp6fc39svgx7mdb5xpl89rv3cqckl") (y #t)))

(define-public crate-quote2-0.5.0 (c (n "quote2") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "quote2-macros") (r "^0.5") (d #t) (k 0)))) (h "0q5z1dvf88vzvc43xd6a9q695c4r9f55vgkm4z2q1bdn37xjgm59") (y #t)))

(define-public crate-quote2-0.7.0 (c (n "quote2") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "quote2-macros") (r "^0.7") (d #t) (k 0)))) (h "0c1n4ngab2flgyi8s6470qzmjs13c3pmc365qv49amvydyw761cp")))

