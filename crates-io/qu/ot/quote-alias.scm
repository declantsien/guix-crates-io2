(define-module (crates-io qu ot quote-alias) #:use-module (crates-io))

(define-public crate-quote-alias-1.0.0 (c (n "quote-alias") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "18nhivf25a5am926vzmf50q1khxk8d8qhiqm81r5pdm0ycvv721i")))

(define-public crate-quote-alias-1.0.1 (c (n "quote-alias") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1pdb833gp2y13z3881ys7m91rn3f57w96pg0549gp0dwfwrzcwr4")))

(define-public crate-quote-alias-1.0.2 (c (n "quote-alias") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0zvfiwhn3jfb47fbf4qqs2yv61d2rn4vqmwzbnr206mmqh9x98cf")))

(define-public crate-quote-alias-1.0.3 (c (n "quote-alias") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "134ckjpd57ndkky5py2z3q4vm7qlcr45lkp8fqwm2rwdlp4f19mv")))

