(define-module (crates-io qu ot quoted-string-parser) #:use-module (crates-io))

(define-public crate-quoted-string-parser-0.1.0 (c (n "quoted-string-parser") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1sf5dywn5giwa69m3k44vzlmbcz8fjgplrnby40x0ldlrmwm7iqd")))

