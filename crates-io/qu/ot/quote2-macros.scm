(define-module (crates-io qu ot quote2-macros) #:use-module (crates-io))

(define-public crate-quote2-macros-0.1.0 (c (n "quote2-macros") (v "0.1.0") (h "17cblwndi482fiwhxh1mcn7hmw5nd1f7anmmkgmyzgygrxnvg8ps")))

(define-public crate-quote2-macros-0.2.0 (c (n "quote2-macros") (v "0.2.0") (h "1dq4z90b5aqa15ivb5sdn6p4adh4w0b4wcw3hhib5ihyp5akxcfh")))

(define-public crate-quote2-macros-0.3.0 (c (n "quote2-macros") (v "0.3.0") (h "0j7slclg1qsmk4865q9avsfq5inn8mba7h06v2s2gq7y22d21npi")))

(define-public crate-quote2-macros-0.5.0 (c (n "quote2-macros") (v "0.5.0") (h "1hvjqvq131s3bcw5y09f610c8a1yalracyfc571cpwggj1mp9qfm")))

(define-public crate-quote2-macros-0.7.0 (c (n "quote2-macros") (v "0.7.0") (h "0xqrdr3al15k258hv3c72ir7s3mvd6k0vllsc8l0m1rdgg1qjjsz")))

