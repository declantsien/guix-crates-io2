(define-module (crates-io qu ot quote) #:use-module (crates-io))

(define-public crate-quote-0.1.0 (c (n "quote") (v "0.1.0") (h "1cs98kdjg7hz2y6rzs9k6bzxi4wwagg9q9ihrg1056rml9f7d0dj")))

(define-public crate-quote-0.1.1 (c (n "quote") (v "0.1.1") (h "10a1qqskap9yj2xmm4g03gx4mrr4y5831q6pnk3g99r9dbbrihg9")))

(define-public crate-quote-0.1.2 (c (n "quote") (v "0.1.2") (h "0ywkqd4qb4gr08jk1dcy35irv3xvx7qbdhk87b97kzhlnfdhz39j")))

(define-public crate-quote-0.1.3 (c (n "quote") (v "0.1.3") (h "0gw9h4zgzrcqh70aam0mx0czr75i7sgnz2k71n7i3ic4r3n681rd")))

(define-public crate-quote-0.1.4 (c (n "quote") (v "0.1.4") (h "05vx9l61mvmf2259zg2y5hz3qqgz5plgiygp10dx88gzj1yq9wdh")))

(define-public crate-quote-0.2.0 (c (n "quote") (v "0.2.0") (h "1yyv83rqc2rld2m3n4rqi96l547w9dfl188f2cq7xp9p8fp86aah")))

(define-public crate-quote-0.2.1 (c (n "quote") (v "0.2.1") (h "00bh0k5zyhqcwj1hg54nkz2qpbiqc5cj0wcli135zzw19bmawwsj")))

(define-public crate-quote-0.2.2 (c (n "quote") (v "0.2.2") (h "148rkfydb5zwl95av78560mmgnpbz2lvd7f523qh5c396ylxzx9b")))

(define-public crate-quote-0.2.3 (c (n "quote") (v "0.2.3") (h "0145c92zdx3gr32nhq1q68b5zbmx7p922wanfb6dn1hhzrwg8p2c")))

(define-public crate-quote-0.3.0-rc1 (c (n "quote") (v "0.3.0-rc1") (h "1rwczv6pjijxn7mkr497pwhy502m58ysfk8v5bqj0964lyxriygl")))

(define-public crate-quote-0.3.0-rc2 (c (n "quote") (v "0.3.0-rc2") (h "0fa8xgm9rfbbbgda88h1dzwz8vwj2x83f8h6y37xdxjxv8zvl9xx")))

(define-public crate-quote-0.3.0 (c (n "quote") (v "0.3.0") (h "0nxk80fa0xpf9bbhzz8p6yimvdr1f8n5j4yxhbj8idw08klp2l2s")))

(define-public crate-quote-0.3.1 (c (n "quote") (v "0.3.1") (h "1ksmsjapvglhmqf016326gfnw1f5ibn1hvvqdibwzk6q60sqfvcb")))

(define-public crate-quote-0.3.2 (c (n "quote") (v "0.3.2") (h "1dlfv7z14ggf4736qlcg3a1vz4lwx0v3irdb012f9cbayz2fq6aj")))

(define-public crate-quote-0.3.3 (c (n "quote") (v "0.3.3") (h "0vn5jcdjkjb06hj64ldqsq62zddcs1bgi7y8nv4by56jfargy8hb")))

(define-public crate-quote-0.3.4 (c (n "quote") (v "0.3.4") (h "1j6lj182r71xw6sj560f9x2szcv0vdxjfv871q1a0ihvrq8qbzm3")))

(define-public crate-quote-0.3.5 (c (n "quote") (v "0.3.5") (h "1cx4k88ywb0kfm9vvxb2dr2sbh87h8rnxawsac6sdc5zqsdhq7pa")))

(define-public crate-quote-0.3.6 (c (n "quote") (v "0.3.6") (h "0whw68vbb56l5nxpdfdjn674ra45bn3yg7j5jjzgvs7r2hwibi4p")))

(define-public crate-quote-0.3.7 (c (n "quote") (v "0.3.7") (h "0z2mcpkdli49zxx14kc7gp8infvwwr11c8pafavcjcpv79f29qwl")))

(define-public crate-quote-0.3.8 (c (n "quote") (v "0.3.8") (h "1kwsnh6kih8l7fa5wg8f5qfvvwc6nqq5p0irw8mjqahgwjcwf3wz")))

(define-public crate-quote-0.3.9 (c (n "quote") (v "0.3.9") (h "0xrhvh21798jg7bj3mwg0jfgzk9lv3blkhcyscgf72949kw2d0am")))

(define-public crate-quote-0.3.10 (c (n "quote") (v "0.3.10") (h "1gl3zhdbbdwpd037ry6vl93qrwbinj3390n1qyzp3hn9cckf6ck7")))

(define-public crate-quote-0.3.11 (c (n "quote") (v "0.3.11") (h "1h1kxm9yzp5qr062bkkzwmjxffs9wbigj7bm7qk2kpz2dlsn3ya8")))

(define-public crate-quote-0.3.12 (c (n "quote") (v "0.3.12") (h "1989r41npdqkyhfj2bl1bjamwvlh6jcib1w1qp0q72xj7pc4zd77")))

(define-public crate-quote-0.3.13 (c (n "quote") (v "0.3.13") (h "0raqyxicckmjk67gv4ancrds75j4wfx3qi2hwihkzy3hwq93zph8")))

(define-public crate-quote-0.3.14 (c (n "quote") (v "0.3.14") (h "14jm6f4afngkbfayr2xlkbsxw4csigsl576x33yyi4jasdxcyxbk")))

(define-public crate-quote-0.3.15 (c (n "quote") (v "0.3.15") (h "0yhnnix4dzsv8y4wwz4csbnqjfh73al33j35msr10py6cl5r4vks")))

(define-public crate-quote-0.4.0 (c (n "quote") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)))) (h "09pnch6p0hzv4nnf1v2yj2hcdn95bf3wr4wknj2bhiibdc08qn6r")))

(define-public crate-quote-0.4.1 (c (n "quote") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)))) (h "07142jpg95wlwy144g2farc3sfm2yj703j53kcg9hz86kz8fdahr")))

(define-public crate-quote-0.4.2 (c (n "quote") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)))) (h "02648jf53xpa7qxai36bhn1wyz986aibbgxnnhp704md4z3i9jhy")))

(define-public crate-quote-0.5.0 (c (n "quote") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.3") (k 0)))) (h "1lr44sirwncibxb75j6hsj7qazj7s2cickrh37n0g8ippnr60zlm") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.5.1 (c (n "quote") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.3") (k 0)))) (h "0aixw7xp4fxhv0gqx9nvl1g7rrx2xa7jjm6i7ysw33gjh89ga3vv") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.5.2 (c (n "quote") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.3") (k 0)))) (h "1s01fh0jl8qv4xggs85yahw0h507nzrxkjbf7vay3zw8d3kcyjcr") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.0 (c (n "quote") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4") (k 0)))) (h "1qvzkvxjjm5jpcyzky3jjpdw22vnd3pb71rgda7x6wk21b4yc9dr") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.1 (c (n "quote") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^0.4") (k 0)))) (h "051spqk5fgrfk99f0hznwp6nivlq0mi834r06x6cfswqg7adbxrh") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.2 (c (n "quote") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^0.4") (k 0)))) (h "0kh7yaydcwbbkrvb039zl7yhs7fivszdjrnygl2qpgfx0zdfwlwy") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.3 (c (n "quote") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^0.4.4") (k 0)))) (h "0dgh7baj7y9i0vzls6mg24nr257p47i63dc33kvrkpacvjh52ip4") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.4 (c (n "quote") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^0.4.4") (k 0)))) (h "06lz8782832j1nn4hyak0bnq7z1b9sfvx231q2lmlmambmbry7xp") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.5 (c (n "quote") (v "0.6.5") (d (list (d (n "proc-macro2") (r "^0.4.4") (k 0)))) (h "1a9bhkvnvppn1dwcih2w466w6dq1xavd2armwafajdkbfqsxqwik") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.6 (c (n "quote") (v "0.6.6") (d (list (d (n "proc-macro2") (r "^0.4.4") (k 0)))) (h "0z0yfcljx2iqgg8nbz2al619qbzslh2f289gjwqzc3aj2c4nazgd") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.7 (c (n "quote") (v "0.6.7") (d (list (d (n "proc-macro2") (r "^0.4.4") (k 0)))) (h "1ghvl0b08n2agy6f0vkdg0cyp178s6n2h22m680i207k0779c8ka") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.8 (c (n "quote") (v "0.6.8") (d (list (d (n "proc-macro2") (r "^0.4.13") (k 0)))) (h "19fyz1p8r6g8fbmdglcsy4c7myik5miq7i22024sycvwjqjn8qyx") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.9 (c (n "quote") (v "0.6.9") (d (list (d (n "proc-macro2") (r "^0.4.13") (k 0)))) (h "0nzsx3g21rpssa4ch070bdkqh4wiq5js3cwkxv7kh9zm8j985db3") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.10 (c (n "quote") (v "0.6.10") (d (list (d (n "proc-macro2") (r "^0.4.21") (k 0)))) (h "0p1bnq7hyxijadf3y1sv6b87lrx2v03n50bs5lvzkl2bk6hj5yjk") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.11 (c (n "quote") (v "0.6.11") (d (list (d (n "proc-macro2") (r "^0.4.21") (k 0)))) (h "1q8i2ilb3yyy2rh60p5yppj3n0yb9x4rsih6ni106bn5v55y1n6d") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.12 (c (n "quote") (v "0.6.12") (d (list (d (n "proc-macro2") (r "^0.4.21") (k 0)))) (h "1nw0klza45hf127kfyrpxsxd5jw2l6h21qxalil3hkr7bnf7kx7s") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-0.6.13 (c (n "quote") (v "0.6.13") (d (list (d (n "proc-macro2") (r "^0.4.21") (k 0)))) (h "1qgqq48jymp5h4y082aanf25hrw6bpb678xh3zw993qfhxmkpqkc") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.0 (c (n "quote") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0rypyiyb612rizizp8za7p2jhc1y1kqym0pvnlk29j7iwvmkifbs") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.1 (c (n "quote") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "18whpq15y4m3hx9h7k774pd87aycxd71mhcl2gsg4rw7r90prms9") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.2 (c (n "quote") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zkc46ryacf2jdkc6krsy2z615xbk1x8kp1830rcxz3irj5qqfh5") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.3 (c (n "quote") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zwd6fp74xfg4jnnnwj4v84lkzif2giwj4ch1hka9g35ghc6rp1b") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.4 (c (n "quote") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "19rj891yvmlc8w5pg7shwfjbdz9cwpz6n4q5nz7fiiazz874n7sc") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.5 (c (n "quote") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0pmsqhalqhf27q4clxijwxmz9kqgiwf5bn0n78kkn3dbr34lp4s2") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.6 (c (n "quote") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1sj4prziz4zdbwad2krk5rlzixlg757ij44m1ihnzbajlr91i8jl") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.7 (c (n "quote") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0drzd6pq7whq7qhdvvs8wn6pbb0hhc12pz8wv80fb05ixhbksmma") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.8 (c (n "quote") (v "1.0.8") (d (list (d (n "proc-macro2") (r "^1.0.20") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pr8dz4pyfbbsqpqw6ygin8m4sz61iir7nl23233cgwsa71k254r") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.9 (c (n "quote") (v "1.0.9") (d (list (d (n "proc-macro2") (r "^1.0.20") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "19rjmfqzk26rxbgxy5j2ckqc2v12sw2xw8l4gi8bzpn2bmsbkl63") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-quote-1.0.10 (c (n "quote") (v "1.0.10") (d (list (d (n "proc-macro2") (r "^1.0.20") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "01ff7a76f871ggnby57iagw6499vci4bihcr11g6bqzjlp38rg1q") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.11 (c (n "quote") (v "1.0.11") (d (list (d (n "proc-macro2") (r "^1.0.20") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "0dmjj62xv7amg4nk6m8q52l1vs50qdjxv3jc15smnjlx6d9rizrx") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.12 (c (n "quote") (v "1.0.12") (d (list (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "188a4qd642n44hcf0d1vzldhd5ilbxazr0xav6rm9wwccidrv112") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.13 (c (n "quote") (v "1.0.13") (d (list (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "1zf2z9wzi6hlgj1kx5v7n6apg4laiibrjm466vlnz35yssm39yl2") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.14 (c (n "quote") (v "1.0.14") (d (list (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zf823y56wqwxkcp3rf3ik9zashpmx9700q0fmqz3np4gi281aj7") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.15 (c (n "quote") (v "1.0.15") (d (list (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "0id1q0875pvhkg0mlb5z8gzdm2g2rbbz76bfzhv331lrm2b3wkc6") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.16 (c (n "quote") (v "1.0.16") (d (list (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "0mysqcqshvcvwr720lffp6bvks5c4mqg31p80zgzqcs5f722xbxl") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.17 (c (n "quote") (v "1.0.17") (d (list (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "0n1z06kngl0h24d4n6f1cv690js8rlbb92za6dps6x7qyyzh4bb3") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.18 (c (n "quote") (v "1.0.18") (d (list (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "1lca4xnwdc2sp76bf4n50kifmi5phhxr9520w623mfcksr7bbzm1") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.19 (c (n "quote") (v "1.0.19") (d (list (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "03fy24h9cdh5a8wsqjkm907ymqabrpr7z7p0fzvacsd72v7whggm") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.20 (c (n "quote") (v "1.0.20") (d (list (d (n "proc-macro2") (r "^1.0.40") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "015qrb5jf9q0pajx38mfn431gfqn0hv2kc1ssarbqvvpx49g5k9v") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.21 (c (n "quote") (v "1.0.21") (d (list (d (n "proc-macro2") (r "^1.0.40") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "0yai5cyd9h95n7hkwjcx8ig3yv0hindmz5gm60g9dmm7fzrlir5v") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.22 (c (n "quote") (v "1.0.22") (d (list (d (n "proc-macro2") (r "^1.0.40") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1j4j48x5w47wxpswcyf18kv6xz5dbq6j1p3p3qk9bs20m53hyvam") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.23 (c (n "quote") (v "1.0.23") (d (list (d (n "proc-macro2") (r "^1.0.40") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ywwzw5xfwwgq62ihp4fbjbfdjb3ilss2vh3fka18ai59lvdhml8") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.24 (c (n "quote") (v "1.0.24") (d (list (d (n "proc-macro2") (r "^1.0.52") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1v68nnv39p1mhhjd8d1aa60qc9q2k42zwb9v8lfns4y44406ws2h") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.25 (c (n "quote") (v "1.0.25") (d (list (d (n "proc-macro2") (r "^1.0.52") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "117y035vad1a67md2r9fk9663i5cvbax1ykc998f3hr9hwhfh22k") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (y #t) (r "1.31")))

(define-public crate-quote-1.0.26 (c (n "quote") (v "1.0.26") (d (list (d (n "proc-macro2") (r "^1.0.52") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1z521piwggwzs0rj4wjx4ma6af1g6f1h5dkp382y5akqyx5sy924") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.27 (c (n "quote") (v "1.0.27") (d (list (d (n "proc-macro2") (r "^1.0.52") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "004mdlsn61k3f9lqv4yk8ghbzq6x1r2m9in7hg2c2pi68p8jjkwg") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.28 (c (n "quote") (v "1.0.28") (d (list (d (n "proc-macro2") (r "^1.0.52") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "122lh886x0p5xh87015wbknl0dfimsjg273g00cxzn6zxb3vk6hv") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.29 (c (n "quote") (v "1.0.29") (d (list (d (n "proc-macro2") (r "^1.0.63") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "019ij5fwp56ydww6zr46dhmzsf078qkdq9vz6mw1cri7mgl1ac2p") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.30 (c (n "quote") (v "1.0.30") (d (list (d (n "proc-macro2") (r "^1.0.63") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fw4h7j524dq7dk1fciahzkhx9ngjxy6w3qp2n5ll9bpqavs21sr") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-quote-1.0.31 (c (n "quote") (v "1.0.31") (d (list (d (n "proc-macro2") (r "^1.0.66") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1w27xrr106vi73x7wr9wnafjbfr6nmm86pxv9qc0h26xd5fsds2z") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-quote-1.0.32 (c (n "quote") (v "1.0.32") (d (list (d (n "proc-macro2") (r "^1.0.66") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0rarx33n4sp7ihsiasrjip5qxh01f5sn80daxc6m885pryfb7wsh") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-quote-1.0.33 (c (n "quote") (v "1.0.33") (d (list (d (n "proc-macro2") (r "^1.0.66") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1biw54hbbr12wdwjac55z1m2x2rylciw83qnjn564a3096jgqrsj") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-quote-1.0.34 (c (n "quote") (v "1.0.34") (d (list (d (n "proc-macro2") (r "^1.0.72") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "02jqkbsbd1gi3p076mdyafc8gpkqa9dnaikar10d2pmg4s9pr8r2") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-quote-1.0.35 (c (n "quote") (v "1.0.35") (d (list (d (n "proc-macro2") (r "^1.0.74") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1vv8r2ncaz4pqdr78x7f138ka595sp2ncr1sa2plm4zxbsmwj7i9") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.56")))

(define-public crate-quote-1.0.36 (c (n "quote") (v "1.0.36") (d (list (d (n "proc-macro2") (r "^1.0.74") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "19xcmh445bg6simirnnd4fvkmp6v2qiwxh5f6rw4a70h76pnm9qg") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.56")))

