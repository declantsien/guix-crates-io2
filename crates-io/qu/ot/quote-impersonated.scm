(define-module (crates-io qu ot quote-impersonated) #:use-module (crates-io))

(define-public crate-quote-impersonated-0.1.0 (c (n "quote-impersonated") (v "0.1.0") (d (list (d (n "proc-macro2-impersonated") (r "^0.1") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1jbd563950ha5fzcghhl3mxbpk4adlhfn0bznxar1jahdw74agiy") (f (quote (("proc-macro" "proc-macro2-impersonated/proc-macro") ("default" "proc-macro"))))))

