(define-module (crates-io qu ot quote-next) #:use-module (crates-io))

(define-public crate-quote-next-0.0.0 (c (n "quote-next") (v "0.0.0") (h "197py0vrrxmy94a70fxb6605h3nzpyc06ll3nq2sd3llxna8mkkh") (y #t)))

(define-public crate-quote-next-1.0.0-rc1 (c (n "quote-next") (v "1.0.0-rc1") (d (list (d (n "proc-macro2-next") (r "^1.0.0-rc1") (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16fly90s0aq4d74mrxr4m02a1ixwmn5yxnzzdyy4kngf04wlcadd") (f (quote (("proc-macro" "proc-macro2-next/proc-macro") ("default" "proc-macro")))) (y #t)))

(define-public crate-quote-next-1.0.0-rc2 (c (n "quote-next") (v "1.0.0-rc2") (d (list (d (n "proc-macro2-next") (r "^1.0.0-rc1") (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0a8zazc62fv8q7v9p4gwdssgr6apgkajrqg0jli25yszfpc2acbd") (f (quote (("proc-macro" "proc-macro2-next/proc-macro") ("default" "proc-macro")))) (y #t)))

(define-public crate-quote-next-1.0.0-rc3 (c (n "quote-next") (v "1.0.0-rc3") (d (list (d (n "proc-macro2-next") (r "^1.0.0-rc2") (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nh0yzb7949g0rczqwi3cd1hi4sayb6baylc98xw4ni1fpgsg2ra") (f (quote (("proc-macro" "proc-macro2-next/proc-macro") ("default" "proc-macro")))) (y #t)))

