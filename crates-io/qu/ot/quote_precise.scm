(define-module (crates-io qu ot quote_precise) #:use-module (crates-io))

(define-public crate-quote_precise-0.0.2 (c (n "quote_precise") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0b89i3r39m082n62mksp1605pyp4m03qdrzpbrcc94h79ks3ii88") (y #t)))

(define-public crate-quote_precise-0.0.3 (c (n "quote_precise") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0044c4kczjix73h6gs16kvyx6k2wmwbccv8a8sq4xy5ap53vxpcy") (y #t)))

(define-public crate-quote_precise-0.0.4 (c (n "quote_precise") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1iwg2m1m678n4xliz18hli1gjr4lz2l6jix0gsbyb71nbv8inx3q") (y #t)))

(define-public crate-quote_precise-0.0.5 (c (n "quote_precise") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1p3ygqr9mdrpvbj9ylbwy4c8w0i0d18yxbcpfyh0i9skgam8lknk") (y #t)))

(define-public crate-quote_precise-0.0.6 (c (n "quote_precise") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1820bx1xhbd4b909wh1rjpxlsack8g2cpagam0sdwq30ppsnfwvl") (y #t)))

(define-public crate-quote_precise-0.0.7 (c (n "quote_precise") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0csagf75qh5an118bn03sxqxvasj3ygbdn2q2rbk7d5yzvf9bvw9") (y #t)))

(define-public crate-quote_precise-0.0.8 (c (n "quote_precise") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "04q1lncpfl3yyn5dx5w2y3yax02lcl3hswqc94q28ly1nz2ynyrr") (y #t)))

(define-public crate-quote_precise-0.0.9 (c (n "quote_precise") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "00glvvf60dp9ypfl06pvzcfgbhl01ijsmxyvj7fxslwdqll6k8n9") (y #t)))

