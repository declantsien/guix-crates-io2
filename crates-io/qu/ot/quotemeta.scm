(define-module (crates-io qu ot quotemeta) #:use-module (crates-io))

(define-public crate-quotemeta-0.1.0 (c (n "quotemeta") (v "0.1.0") (h "05n4ckg6ca1cnv1f1hd3cnakihxbrxq5697a9f765b3mw7zqyzbj") (f (quote (("default" "clippy-insane") ("clippy-insane"))))))

(define-public crate-quotemeta-0.1.1 (c (n "quotemeta") (v "0.1.1") (h "10wycb42nbr42hnh75gwgif5vg52sh5czqcd0n7qlc2q7f4md0jj") (f (quote (("unstable-doc-cfg") ("default") ("clippy-insane"))))))

(define-public crate-quotemeta-0.1.2 (c (n "quotemeta") (v "0.1.2") (h "188il4hhd0p392zcbymy2j2pbf0a3ndys7vybfim0pnj51hw373d") (f (quote (("default") ("clippy-insane"))))))

