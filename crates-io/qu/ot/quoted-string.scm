(define-module (crates-io qu ot quoted-string) #:use-module (crates-io))

(define-public crate-quoted-string-0.2.0 (c (n "quoted-string") (v "0.2.0") (d (list (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "13rz22qs3k79p8vqpzrifw6mqwvr50ajmbmx41gs8ld4bk8ni32y")))

(define-public crate-quoted-string-0.2.1 (c (n "quoted-string") (v "0.2.1") (d (list (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "0xz2ip2pbxpmwr96sbifajjymr8jb16aqf6y4fnr3mp1x933s7y2")))

(define-public crate-quoted-string-0.2.2 (c (n "quoted-string") (v "0.2.2") (d (list (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)))) (h "0rzmypcqf776qffic8ghil0il8mmkh53863cg671v51rgy3gi1lm")))

(define-public crate-quoted-string-0.3.0 (c (n "quoted-string") (v "0.3.0") (h "0f47ijxyraizczgivmqmffyn2cvb9wk92hazjkf1sf881qv4wipp") (y #t)))

(define-public crate-quoted-string-0.6.0 (c (n "quoted-string") (v "0.6.0") (h "0k0mbid7klw24m8kgh7kd0bfzgkfwf5dri2dd8p05hkzgmdi58df")))

(define-public crate-quoted-string-0.6.1 (c (n "quoted-string") (v "0.6.1") (h "0bi7p9ckjkrsq90narxgj0jk8pnnnkh2xnp7809rs61prqq6l82s")))

