(define-module (crates-io qu or quork-proc) #:use-module (crates-io))

(define-public crate-quork-proc-0.1.0 (c (n "quork-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "12pffkl9wqh4874277sc236hlrc0j4sas21vcxn8vbn5lfxd2d1l")))

(define-public crate-quork-proc-0.1.1 (c (n "quork-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1jszihh40gkqk9kdq4rb1cjq5g80jn88pf9iagg8zwlhvbcry5am")))

(define-public crate-quork-proc-0.1.2 (c (n "quork-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vxd898rdbwj9d6bg9jjrc95qv3is92qdjlqh901dx19ybn3cghn")))

(define-public crate-quork-proc-0.2.0 (c (n "quork-proc") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g95wly2vdf91w9bkm8gq9bakp5nscyd0k0dl5n4nvzxgkjn8iai")))

(define-public crate-quork-proc-0.3.0 (c (n "quork-proc") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lbzsph325ryq5wxsi503gc0x6vzc0dqz26jxwk5r1gm39qjzkxv")))

(define-public crate-quork-proc-0.3.1 (c (n "quork-proc") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07mbqw8pbavk2xq3vhhynqv4n7nspkx0yvf9dcnm5nc5gqr3kiqi")))

