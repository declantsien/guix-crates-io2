(define-module (crates-io qu ra quranize) #:use-module (crates-io))

(define-public crate-quranize-0.1.0 (c (n "quranize") (v "0.1.0") (h "06gdqi4ilqczm5dvcgs0ylap3mbgbb27pavx7fwhyfcynid9vrwj") (f (quote (("quran-simple-plain"))))))

(define-public crate-quranize-0.1.1 (c (n "quranize") (v "0.1.1") (h "1sld55nf533fj5y3j6dadccym7xzkzdlwglm165zb5mvblhg6lx2") (f (quote (("quran-simple-plain"))))))

(define-public crate-quranize-0.1.2 (c (n "quranize") (v "0.1.2") (h "0708ymnvp39zzki0yn7d64ahbvpp2x3jc5idqxd8x767arfna3nq") (f (quote (("quran-simple-plain"))))))

(define-public crate-quranize-0.2.0 (c (n "quranize") (v "0.2.0") (h "1xlwyqzwwmp9x1zbs27jk8hvznr069ls30210g82vkxs43cki0ri") (f (quote (("quran-simple-plain"))))))

(define-public crate-quranize-0.3.0 (c (n "quranize") (v "0.3.0") (h "161x4kdsl80hssb4msf4dx2ply9qgavkfwcvwh67wxz8s0n31lhn") (f (quote (("quran-simple-plain"))))))

(define-public crate-quranize-0.4.0 (c (n "quranize") (v "0.4.0") (h "0jnkjp9qkxllc89p60jj4gg8l38f1yfqyxa9d5ajydbbyxg28fcv") (f (quote (("quran-simple-plain"))))))

(define-public crate-quranize-0.4.1 (c (n "quranize") (v "0.4.1") (h "0mhqzhfij8qlk2ni6rr6nacv7nzf0wzfw370f8685vlpzvwcskrp") (f (quote (("quran-simple-plain"))))))

(define-public crate-quranize-0.5.0 (c (n "quranize") (v "0.5.0") (h "1l5ncj9m0cbhbb4092apn1lm08a1kiib7d9jmaqqxraq1la6vdlg") (f (quote (("quran-simple-plain"))))))

(define-public crate-quranize-0.5.1 (c (n "quranize") (v "0.5.1") (h "0ayqax3bxdaksbc7czwfp2k5rkzc27c3n6ngxkq6r3ys3zlbkvb6") (f (quote (("quran-simple-plain"))))))

(define-public crate-quranize-0.6.0 (c (n "quranize") (v "0.6.0") (h "0g4w56kiaviyrz0b20n1ar93z9m14n6rxpqc6lrnp39xv09i4wgg")))

(define-public crate-quranize-0.6.1 (c (n "quranize") (v "0.6.1") (h "1xyj5g3dsinpqqm71b1w6d0waacvmqid1p9i3ggy5v8fpyrf4mhk")))

(define-public crate-quranize-0.6.2 (c (n "quranize") (v "0.6.2") (h "0y2iff5mihzqp23wkpykxpyfrszinx8ry5931jh024zh9cn6z5zh")))

(define-public crate-quranize-0.6.3 (c (n "quranize") (v "0.6.3") (h "1pvzprp7ih5smfqg6lb3g97i12vark1qiij61sflm4w79m4wm1qy")))

(define-public crate-quranize-0.7.0 (c (n "quranize") (v "0.7.0") (h "1m16fpnm1rq48lzwbk8jrhv1vdslav4szbjhgfr5vf70xmzbfisb")))

(define-public crate-quranize-0.7.1 (c (n "quranize") (v "0.7.1") (h "0bva0529dvyhfj0hxzjwznr2gcc9zdhsgh82pwssyl3cdnzbzr4s")))

(define-public crate-quranize-0.7.2 (c (n "quranize") (v "0.7.2") (h "1aymmacic8x0m49gn0nks34gmbcw3ldhgw6nl7h9jadnaxifqm6w")))

(define-public crate-quranize-0.7.3 (c (n "quranize") (v "0.7.3") (h "1y8lf2ya1mg6csvkzmvlpjvg257hgw8l14k0npgz3ancjlw12wz3")))

(define-public crate-quranize-0.8.0 (c (n "quranize") (v "0.8.0") (h "1n13p3nw336xwjbh03383kid5g4gzb98k24siqa227w49zny2j7r")))

(define-public crate-quranize-0.8.1 (c (n "quranize") (v "0.8.1") (h "1sxipb19ynlnifp0b3rmsag0pnqwg1pl84wizgl6pd50wr5i9kfl")))

(define-public crate-quranize-0.8.2 (c (n "quranize") (v "0.8.2") (h "0gaqaazi7nnbxgvqd6dxn907daafdcb31dyrvlm52f54ny25rii4")))

(define-public crate-quranize-0.9.0 (c (n "quranize") (v "0.9.0") (h "1wdwmyxv5b4y2mfaiksyzwx61nmm1d3wrb9xzlc0ifrdjbda7srq")))

(define-public crate-quranize-0.10.0 (c (n "quranize") (v "0.10.0") (h "0knj8s9xs4h3lkkgfj4g1mqamzf592cvkvvxwwfwf9p1w5aqyljh")))

(define-public crate-quranize-0.10.1 (c (n "quranize") (v "0.10.1") (h "0y8rk7y404r417xsxmzx138fdxczrh1giq6whadraprf4qk27d0s")))

(define-public crate-quranize-0.10.2 (c (n "quranize") (v "0.10.2") (h "0xyvw1jrqrg96xcm0m0y3ijq7h1ckffd0b711x3f7fyw3pyx8vxs")))

(define-public crate-quranize-0.10.3 (c (n "quranize") (v "0.10.3") (h "0k7n0s2gax58claf9qzrsmnr0qpd8w9nhlv0ljs7rr3jfq0dz7di")))

(define-public crate-quranize-0.10.4 (c (n "quranize") (v "0.10.4") (h "01smck47qi1xk0f36lr128pwmq79q9jwydrggbd99dyb7ql437xj")))

(define-public crate-quranize-0.10.5 (c (n "quranize") (v "0.10.5") (h "05dlhibvbrrjqhsm6qgg2y1jp6ylpgpwsxs0qgw05n6d666knsby")))

(define-public crate-quranize-0.10.6 (c (n "quranize") (v "0.10.6") (h "0fyw0cs8wf655yvlhli0rsyd1qqnhal7iqq42bzl0r969681vzfs")))

(define-public crate-quranize-0.10.7 (c (n "quranize") (v "0.10.7") (h "13csqf0pfbrqbck6b8lvxja1a2bck8pqhx4fvyyb6zi94macxs8y")))

(define-public crate-quranize-0.10.8 (c (n "quranize") (v "0.10.8") (h "1dc0gjxl3qyzpwxd81m098p65hl9bc9la67vks9h41ba8jvc0qvz")))

(define-public crate-quranize-0.11.0 (c (n "quranize") (v "0.11.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "032dfhn9zgmc9n0zx7wsxw64pbdv32b3jxj09xrfzv0x67rw58nc")))

(define-public crate-quranize-0.11.1 (c (n "quranize") (v "0.11.1") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "15hrrypnqnss04k90iy4wspfli764v2ycikp1ssw21pcr58g00xa")))

(define-public crate-quranize-0.11.2 (c (n "quranize") (v "0.11.2") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0cx1llkjmpc717bifdd2s2m4da27582zy41jzpvzf7pn22ffbvh3")))

(define-public crate-quranize-0.11.3 (c (n "quranize") (v "0.11.3") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "09rg65zyr709y99bsb8xz2s0gqqk71m0fri1jkscqnsfq07xyz3z")))

(define-public crate-quranize-0.11.4 (c (n "quranize") (v "0.11.4") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "19w7kbgazdi8992yccwmsklcmlqv7prpla1i9nl91nlc7yccijln")))

(define-public crate-quranize-0.11.5 (c (n "quranize") (v "0.11.5") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "16nlhaa165nwwnwybmxck25qsp1ybxrjlaagmgsk6imrwnwyh711")))

(define-public crate-quranize-0.12.0 (c (n "quranize") (v "0.12.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0saswsrlk6hh9cdf4y6yq13cn5x18q1ly7k7h207rba2d2j2cc7f")))

(define-public crate-quranize-0.12.1 (c (n "quranize") (v "0.12.1") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "091iip62fdvfrn6h0gaky4f6lxfh2ih8in4kvvkgcvkcv2s2bm12")))

(define-public crate-quranize-0.12.2 (c (n "quranize") (v "0.12.2") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1big8v4qsg21z7iaph3wfssr98ci65b3hy15xa2d912sdma0y3r6")))

