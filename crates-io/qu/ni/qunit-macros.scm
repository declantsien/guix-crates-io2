(define-module (crates-io qu ni qunit-macros) #:use-module (crates-io))

(define-public crate-qunit-macros-0.1.0-beta.1 (c (n "qunit-macros") (v "0.1.0-beta.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0y3vv9wp0pfd5z0k11i903d20czcglh64564yi09z3ph4bbkws27")))

(define-public crate-qunit-macros-0.1.0-beta.2 (c (n "qunit-macros") (v "0.1.0-beta.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0l7j107120mh23jq2w12hv386j9i48r6rkpn7ad5nikj5s7cvj7y")))

(define-public crate-qunit-macros-0.1.0-beta.3 (c (n "qunit-macros") (v "0.1.0-beta.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1hz4viwdk21612bxzms1n86kd1cc3cxbgbh14h3q9ghzkrgp515w")))

(define-public crate-qunit-macros-0.1.0-beta.4 (c (n "qunit-macros") (v "0.1.0-beta.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0yc29iyfvi45j66zqbi2550ik93csqbj8d2mirkz418qabd189fh")))

(define-public crate-qunit-macros-0.1.0-beta.5 (c (n "qunit-macros") (v "0.1.0-beta.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1ab0rw7kwj2cdy627cymfl4jjh8f4ypyfqnqngyk76frqp0zfc94") (f (quote (("matrix_pat"))))))

(define-public crate-qunit-macros-0.1.0 (c (n "qunit-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1xsxwkjpd2xvkyqzvhgiybkpwyxfcqj15ng16xqd1kw0dz0665b5") (f (quote (("matrix_pat"))))))

(define-public crate-qunit-macros-0.1.1 (c (n "qunit-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1nnc3nx36z0b9n5ip06554q6wnib2xxy61mw6m6bpm9axjy7fvlz") (f (quote (("matrix_pat"))))))

