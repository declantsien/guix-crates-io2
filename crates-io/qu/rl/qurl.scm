(define-module (crates-io qu rl qurl) #:use-module (crates-io))

(define-public crate-qurl-0.1.0 (c (n "qurl") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "mockall") (r "^0.8.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1frwjasy3qp3rx6azmiww5kpwhg20df5y00rrddswxs8rr54p9gi")))

(define-public crate-qurl-0.2.0 (c (n "qurl") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "httpmock") (r "^0.5") (d #t) (k 2)) (d (n "mockall") (r "^0.8") (d #t) (k 2)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0inbg5vs8b9wzw297inmhaf70l7b9gknsn3np1zp8mh902wrh5yk")))

