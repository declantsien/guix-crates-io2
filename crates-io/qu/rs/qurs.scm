(define-module (crates-io qu rs qurs) #:use-module (crates-io))

(define-public crate-qurs-0.1.0 (c (n "qurs") (v "0.1.0") (h "1h4sglxm5zfym5b24nwnbw9cz80xfwi7yqas9l9qvdiwvysdf44y")))

(define-public crate-qurs-0.1.1 (c (n "qurs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "01cfcx6hnm5kaykjp2v27nn8811rdw9z5vw66v57a1hdv66d3dav")))

(define-public crate-qurs-0.2.0 (c (n "qurs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 0)))) (h "126ii810g1w6a4diw2w4hklx53iq94r1fmpldwpvy50qyspb8asp")))

