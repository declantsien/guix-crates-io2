(define-module (crates-io jw k- jwk-set) #:use-module (crates-io))

(define-public crate-jwk-set-0.0.0 (c (n "jwk-set") (v "0.0.0") (h "1kv76vkggl6c37h2qkrdx4xzwjwd7prqzxw0949h33704cm6y7kc")))

(define-public crate-jwk-set-0.1.0 (c (n "jwk-set") (v "0.1.0") (d (list (d (n "jsonwebkey") (r "^0.3") (k 0)) (d (n "jsonwebtoken") (r "^7.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 2)))) (h "1lqnk1ivfgw52rgw72jhrlczy304c8pfszlspjwqvcw6wm3f0080") (f (quote (("with-decrypt" "jsonwebkey/jwt-convert" "jsonwebtoken") ("default" "with-decrypt") ("_priv_with_serde_json" "serde_json"))))))

(define-public crate-jwk-set-0.1.1 (c (n "jwk-set") (v "0.1.1") (d (list (d (n "jsonwebkey") (r "^0.3.5") (k 0)) (d (n "jsonwebtoken") (r "^8.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 2)))) (h "1y8d2n9wapnhkbi5p0i03sx5gqiavicvn93g54ywfn5599aa0dpi") (f (quote (("with-decrypt" "jsonwebkey/jwt-convert" "jsonwebtoken") ("default" "with-decrypt") ("_priv_with_serde_json" "serde_json"))))))

