(define-module (crates-io jw t_ jwt_boiler) #:use-module (crates-io))

(define-public crate-jwt_boiler-0.1.0 (c (n "jwt_boiler") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "jwt") (r "^0.16.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "124sj6qigc1xd21jr5h5gxsawdr3a7wmf2wwlzhv2acis94yx4p1")))

(define-public crate-jwt_boiler-0.1.1 (c (n "jwt_boiler") (v "0.1.1") (d (list (d (n "digest") (r "^0.10.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "jwt") (r "^0.16.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lv4gdc21nr570s5y211rz1cdfchb931f139ahizmlv8sawb8gj9")))

