(define-module (crates-io jw t_ jwt_rs) #:use-module (crates-io))

(define-public crate-jwt_rs-0.1.0 (c (n "jwt_rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "17vdvhdaid997qbd342zd22yndnbmrpm7q65in2xqs577wzkxfmj")))

