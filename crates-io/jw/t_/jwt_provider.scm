(define-module (crates-io jw t_ jwt_provider) #:use-module (crates-io))

(define-public crate-jwt_provider-0.1.0 (c (n "jwt_provider") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.11") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "weld-codegen") (r "^0.6") (d #t) (k 1)))) (h "10mbwfmbn4mg5ncyffxb7d7bin4j34dwzvpc08wk6dmk7j280nr0")))

(define-public crate-jwt_provider-1.0.13 (c (n "jwt_provider") (v "1.0.13") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.11") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "weld-codegen") (r "^0.6") (d #t) (k 1)))) (h "1va03cg8b20b4k41brxg21393mhy9z0kfbzpm9hibxq56rqyqw2c")))

(define-public crate-jwt_provider-1.0.14 (c (n "jwt_provider") (v "1.0.14") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.11") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "weld-codegen") (r "^0.6") (d #t) (k 1)))) (h "01n0hv8rvzlln68sw7429iqqkhmbpkdwia0f4gng17mcgshjz0pm")))

(define-public crate-jwt_provider-1.0.15 (c (n "jwt_provider") (v "1.0.15") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.11") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "weld-codegen") (r "^0.6") (d #t) (k 1)))) (h "11471jvvypaklm5zci0yj6zmsrv02i9qzk3q2rlygyk2k5mp2pgh")))

