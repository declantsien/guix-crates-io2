(define-module (crates-io jw t_ jwt_hmac) #:use-module (crates-io))

(define-public crate-jwt_hmac-0.1.0 (c (n "jwt_hmac") (v "0.1.0") (d (list (d (n "base64-url") (r "^1.4.10") (d #t) (k 0)) (d (n "hmac") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1myilp0165s59jw9z1p2y4kghqp04xpi4dh2y80ffsgzshxfvpsk")))

(define-public crate-jwt_hmac-0.2.0 (c (n "jwt_hmac") (v "0.2.0") (d (list (d (n "base64-url") (r "^1.4.13") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0rz9na82baxh3sffbi2lz54np7a4lh9zgcf3wlz2pags9zfi5i4r")))

