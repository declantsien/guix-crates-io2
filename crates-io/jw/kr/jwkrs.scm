(define-module (crates-io jw kr jwkrs) #:use-module (crates-io))

(define-public crate-jwkrs-0.1.0 (c (n "jwkrs") (v "0.1.0") (d (list (d (n "evmap") (r "^10") (d #t) (k 0)) (d (n "evmap-derive") (r "^0.2") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "19hjvyabjx1fwaqkdkg99j3habjm58603kn01a692xasyxppk906")))

(define-public crate-jwkrs-0.1.1 (c (n "jwkrs") (v "0.1.1") (d (list (d (n "evmap") (r "^10") (d #t) (k 0)) (d (n "evmap-derive") (r "^0.2") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1vlzlp2by63493mnlfjzx23ihd7036i4xcwphpya2kf3hxxy9xah")))

(define-public crate-jwkrs-0.1.2 (c (n "jwkrs") (v "0.1.2") (d (list (d (n "evmap") (r "^10") (d #t) (k 0)) (d (n "evmap-derive") (r "^0.2") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0mzdh55cf7hva4669490189fb33zmimilv06r8wq1shsbmv9myri")))

(define-public crate-jwkrs-0.1.3 (c (n "jwkrs") (v "0.1.3") (d (list (d (n "evmap") (r "^10") (d #t) (k 0)) (d (n "evmap-derive") (r "^0.2") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "11pxqrw4i3vigj0f0x7a75hwx45fvnlxvb6sgf8f207ynzb9hsrp")))

(define-public crate-jwkrs-0.1.4 (c (n "jwkrs") (v "0.1.4") (d (list (d (n "evmap") (r "^10") (d #t) (k 0)) (d (n "evmap-derive") (r "^0.2") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0lllpn1s7685531063701qcil73q5jc7qhlqgdd3f9xrqbpqyg66")))

(define-public crate-jwkrs-0.1.5 (c (n "jwkrs") (v "0.1.5") (d (list (d (n "evmap") (r "^10") (d #t) (k 0)) (d (n "evmap-derive") (r "^0.2") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1gqx66qqpvyaxr3kmgmhqzfixbysxr91lvrmhhsrd58278arklch")))

(define-public crate-jwkrs-0.1.6 (c (n "jwkrs") (v "0.1.6") (d (list (d (n "evmap") (r "^10") (d #t) (k 0)) (d (n "evmap-derive") (r "^0.2") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0iq10yfbvy6zrn1ivjl5wbf4fwqa08dpmy88ybrm2zl79dpq6acv")))

(define-public crate-jwkrs-0.2.0 (c (n "jwkrs") (v "0.2.0") (d (list (d (n "evmap") (r "^10") (d #t) (k 0)) (d (n "evmap-derive") (r "^0.2") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1x5p1qbghaw6nqln3dwr88z617sw632mz5zgrcvpz5clv62ih2iy")))

