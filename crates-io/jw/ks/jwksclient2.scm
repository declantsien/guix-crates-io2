(define-module (crates-io jw ks jwksclient2) #:use-module (crates-io))

(define-public crate-jwksclient2-0.2.0 (c (n "jwksclient2") (v "0.2.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json" "rustls-tls" "rustls"))) (k 0)) (d (n "ring") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0iivd89lkp4ripvyjkimbkmf6421j3bj5648xkiyslssq691gn0w")))

