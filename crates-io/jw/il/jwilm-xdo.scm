(define-module (crates-io jw il jwilm-xdo) #:use-module (crates-io))

(define-public crate-jwilm-xdo-0.1.0 (c (n "jwilm-xdo") (v "0.1.0") (d (list (d (n "foreign-types") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.11") (d #t) (k 0)) (d (n "x11") (r "^2.1.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1ldvn8a52bgi3ibw202ynw0rydy3ppqv4q99lxbvgyr4n3dj7lil")))

(define-public crate-jwilm-xdo-0.1.1 (c (n "jwilm-xdo") (v "0.1.1") (d (list (d (n "foreign-types") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.11") (d #t) (k 0)) (d (n "x11") (r "^2.1.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0ml79xabm6f428gny1mxi6j3zjlmf14kz5vnk6fiva8x3481h5md")))

(define-public crate-jwilm-xdo-0.1.2 (c (n "jwilm-xdo") (v "0.1.2") (d (list (d (n "foreign-types") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.11") (d #t) (k 0)) (d (n "x11") (r "^2.1.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0zl7sg6bs3k2nsxam8k46nydwv1nk6m72zq46n1sb8g04dbsgq6q")))

