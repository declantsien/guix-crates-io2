(define-module (crates-io jw ts jwts) #:use-module (crates-io))

(define-public crate-jwts-0.1.0 (c (n "jwts") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1i7rnck6mfap7ic2mjizkm8mp1znr536imnx5rwsbjd7pl8imdmc")))

(define-public crate-jwts-0.2.0 (c (n "jwts") (v "0.2.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0bada8i7d3gnz0sp1m4a2sij6li084f6dq6lk09zrk0yjs0l8xv7")))

(define-public crate-jwts-0.2.1 (c (n "jwts") (v "0.2.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1ah685c37898lc96lj0gv9symv41m1w8icq34c1f80sqgsikwkd1")))

(define-public crate-jwts-0.2.2 (c (n "jwts") (v "0.2.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1n5hhpfiz3wd5s8c5cfskflvw5szd3ypnzsz3y02hdyx4hcwrylc")))

(define-public crate-jwts-0.2.3 (c (n "jwts") (v "0.2.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "124vkvz8cas7yx0qpcr5lngfsr00bsy0bk3w22765iiifdhm11j3")))

(define-public crate-jwts-0.2.4 (c (n "jwts") (v "0.2.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "1snl0s40l7hzfbwnviv5aq5qmqddc4in5vkwbz5nwxs3rdpkjqvr")))

(define-public crate-jwts-0.3.0 (c (n "jwts") (v "0.3.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "04p4kh89iaqvga5z6wgp0z968y4a1va962h1y9i5si4kzw0829a6") (y #t)))

(define-public crate-jwts-0.4.0 (c (n "jwts") (v "0.4.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "1893rj0pddc1i47ky85z8n4cywbjglxykxjm0khsghbhva5h1426")))

(define-public crate-jwts-0.5.0 (c (n "jwts") (v "0.5.0") (d (list (d (n "aws-lc-rs") (r "^1.6.4") (f (quote ("aws-lc-sys"))) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "17k26aj285darjg5v3hzxxc86ap7ibzyf1k5jn4m7dj4cyrbv5n2")))

(define-public crate-jwts-0.5.1 (c (n "jwts") (v "0.5.1") (d (list (d (n "aws-lc-rs") (r "^1.6.4") (f (quote ("aws-lc-sys"))) (k 0)) (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0xlnkr8kdifd3rab84d0vrrwil4y9hsiw3196r5dj1qqaharr5qf")))

