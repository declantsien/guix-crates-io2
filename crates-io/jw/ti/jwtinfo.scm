(define-module (crates-io jw ti jwtinfo) #:use-module (crates-io))

(define-public crate-jwtinfo-0.1.0 (c (n "jwtinfo") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rnm68lcb81pr7ny31ngy7xs83l4dfhibcwd4cnscln4x2xwgjzn")))

(define-public crate-jwtinfo-0.1.1 (c (n "jwtinfo") (v "0.1.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a0pd4aimkphg3s17dji77y9lxj7210d4q252m0ac66dbbmj5cqq")))

(define-public crate-jwtinfo-0.1.2 (c (n "jwtinfo") (v "0.1.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "040hpywgqkadcv0cmshy2596jbswmy9wizhmfjipw913qg541k98")))

(define-public crate-jwtinfo-0.1.3 (c (n "jwtinfo") (v "0.1.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kyq09v09g90615dms40vjyjxx47842drqvbv34yq2zds2bs9g87")))

(define-public crate-jwtinfo-0.1.4 (c (n "jwtinfo") (v "0.1.4") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jczjr9mdr3sbjygx2lpjrka4gns4zqds19laj0wh7qcvbwknjkq")))

(define-public crate-jwtinfo-0.1.5 (c (n "jwtinfo") (v "0.1.5") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1725crivllwisqkikpfvgizc35iqmhavdqgz9ss9gvbcspxr8yrv")))

(define-public crate-jwtinfo-0.1.6 (c (n "jwtinfo") (v "0.1.6") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09xshrbhady6v5gqsp8v7iw9yfwwgln2q3yxsi1b814nway0j8kg")))

(define-public crate-jwtinfo-0.1.7 (c (n "jwtinfo") (v "0.1.7") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ax9sq4pqshz6fw3j0ga8nb78z05c4ckcn5a9mldjwq7di9dy9b4")))

(define-public crate-jwtinfo-0.1.8 (c (n "jwtinfo") (v "0.1.8") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03kp3rdr2lxh71c77scxxp3vaqxp3i86xf846zs1mladvablf5fi")))

(define-public crate-jwtinfo-0.1.9 (c (n "jwtinfo") (v "0.1.9") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "135my52w3spl1zwsrw11riri6zrkiv0lh82n4iaqapi6s80xa8mc")))

(define-public crate-jwtinfo-0.1.10 (c (n "jwtinfo") (v "0.1.10") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n9n2hxa17y4chp98xv4d5yd9qb5rahi8x35syncmkcf0jz07xxs")))

(define-public crate-jwtinfo-0.2.0 (c (n "jwtinfo") (v "0.2.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07xd2wspsl57bahy3yp01dcbyll0agx5dsqkzwhzk36gn88dllmp")))

(define-public crate-jwtinfo-0.2.1 (c (n "jwtinfo") (v "0.2.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qr9hfn2i0csv66fk433n8nq6m89338jqysfqwy2klsiagnr95wr")))

(define-public crate-jwtinfo-0.2.2 (c (n "jwtinfo") (v "0.2.2") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b0da2077y3z9d25ykz27w7xxa5yy8n97y34i6r0lmjq9dz73nd0")))

(define-public crate-jwtinfo-0.2.3 (c (n "jwtinfo") (v "0.2.3") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xljz8d980li021kz1j9707cmh29kdg17rv6snabg6qhhpkpyycq")))

(define-public crate-jwtinfo-0.3.0 (c (n "jwtinfo") (v "0.3.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ybki0frilxbha7zpgnnv2pvd3fj277lsx9zdcw00wxilkjj674x")))

(define-public crate-jwtinfo-0.4.0 (c (n "jwtinfo") (v "0.4.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j5yc7lvlypwabia48kvli4z1gs48bj5ad027260mpay1fk051sj")))

(define-public crate-jwtinfo-0.4.1 (c (n "jwtinfo") (v "0.4.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vj2h0q6na1xb7jdn5c1n5hy9ih1hhdzxc4wrsml18qcmh6caq7p")))

(define-public crate-jwtinfo-0.4.2 (c (n "jwtinfo") (v "0.4.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p8bsyvpmh09qh2p9v8xiym4vbays1rqx1bil5052pv3zn6n06yh")))

(define-public crate-jwtinfo-0.4.3 (c (n "jwtinfo") (v "0.4.3") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iynckb99qf4ji9igq2yywbl59bh3h46s7iv21nch0l89bx1wvyc")))

