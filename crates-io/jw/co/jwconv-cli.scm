(define-module (crates-io jw co jwconv-cli) #:use-module (crates-io))

(define-public crate-jwconv-cli-0.0.1 (c (n "jwconv-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.20.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "jwconv") (r "^0.0.1") (d #t) (k 0)))) (h "0mpmxyash16lm4sxcp1zfdc5si7ws99z0ksjrwbrxiaaxlsm5vid")))

