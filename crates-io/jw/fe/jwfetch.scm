(define-module (crates-io jw fe jwfetch) #:use-module (crates-io))

(define-public crate-jwfetch-0.1.0 (c (n "jwfetch") (v "0.1.0") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ycffhckxf9si4sqb2nspw0hk8bxcsz7wjgbnkczvj8lwfr01k8q")))

(define-public crate-jwfetch-0.1.1 (c (n "jwfetch") (v "0.1.1") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rn7mbqqcw0x3yyw8rpgzxr3rawcdilmw7chwpdhm6dndabnv2kl") (y #t)))

(define-public crate-jwfetch-0.1.2 (c (n "jwfetch") (v "0.1.2") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cyniswznviscnq84zlgmw4jvdla2swxbnhvavbkyxh8rvmd4316")))

(define-public crate-jwfetch-0.1.3 (c (n "jwfetch") (v "0.1.3") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "097gkqwzzh0dfxc6cnhbk53wjxg5wwjfx2kam5873jzkcqp8dzcz")))

(define-public crate-jwfetch-0.1.4 (c (n "jwfetch") (v "0.1.4") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wyvxdzrq66rcg9j4m8sr9j0cl3lflcq75pz6990fqzm6skw02rv")))

(define-public crate-jwfetch-0.1.5 (c (n "jwfetch") (v "0.1.5") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "018jr7b7vhalpikhgq8q08in8dpszsa71kzv3idy181smh7cc3m3")))

(define-public crate-jwfetch-0.1.6 (c (n "jwfetch") (v "0.1.6") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1si3kx5kn39argyryp6j0hnhggk907cgw5x291bw9jaibvnsq9yw")))

(define-public crate-jwfetch-0.1.7 (c (n "jwfetch") (v "0.1.7") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0v9wljh89jr18dxabcssig8f1rdpjknfr3p3cqhz12gzy1a7hwkp")))

(define-public crate-jwfetch-0.1.8 (c (n "jwfetch") (v "0.1.8") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0j5m2m978n573iawqfmcpg9bg5vf8ql4asbbgd9pqzynll1kkxgw")))

(define-public crate-jwfetch-0.1.9 (c (n "jwfetch") (v "0.1.9") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kjww7x3zkdhab7pja8w4mqz2fpip70x7f8axwbvaxh0xv9gdjg8")))

(define-public crate-jwfetch-0.2.0 (c (n "jwfetch") (v "0.2.0") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xrxr8nh8k0mzkf9p2zkcl0i9dxcpl80cl7jnhy6xxv56vpvkn4q")))

(define-public crate-jwfetch-0.3.0 (c (n "jwfetch") (v "0.3.0") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0r35gmrcs52h5dgz7pkffnnzkhza2gb1q5x6z4x7y727krg93dqk")))

(define-public crate-jwfetch-0.3.1 (c (n "jwfetch") (v "0.3.1") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1d7rlp8fnhifjmxhkl38d0hba6pl8lll14b3d6wnhqpmqyqv8irj")))

(define-public crate-jwfetch-0.3.2 (c (n "jwfetch") (v "0.3.2") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "096n547rdvx2sf17wsa56mdsl18lv69ddrhncv3zbys4q8wn0z82")))

(define-public crate-jwfetch-0.3.3 (c (n "jwfetch") (v "0.3.3") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ygf0kkj1gjkdx5cswmyxfjq64qkj0w15zqkw6z75fb85pf92kfi")))

