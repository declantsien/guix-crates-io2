(define-module (crates-io jw te jwtexp) #:use-module (crates-io))

(define-public crate-jwtexp-0.1.0 (c (n "jwtexp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m3h64lwy5qcf26h8s5mxq45hi61baxd3lv6d0p46cy8sn3bmq3q")))

(define-public crate-jwtexp-0.1.1 (c (n "jwtexp") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "095ksybhhh71iz1p50v1x0f7g7vgp3by9dyzadwjzq2sgqhh8n3a")))

(define-public crate-jwtexp-0.1.2 (c (n "jwtexp") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iwjdg606v41q29swg95cn9l4hdvl7c6h97idfq7d9x586v62kw8")))

