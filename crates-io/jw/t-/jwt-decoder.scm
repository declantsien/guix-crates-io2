(define-module (crates-io jw t- jwt-decoder) #:use-module (crates-io))

(define-public crate-jwt-decoder-0.1.0 (c (n "jwt-decoder") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.30") (d #t) (k 0)))) (h "1rcnhyjxrdkaxr13w4ba69a79s2a0y68zr7jm17jr4v294bqpmwk")))

(define-public crate-jwt-decoder-0.2.0 (c (n "jwt-decoder") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.30") (d #t) (k 0)))) (h "1fjp9ln7bqgifjm14s9dj61vdxglj969gkvhc5fp06vkngs2152j")))

(define-public crate-jwt-decoder-0.2.1 (c (n "jwt-decoder") (v "0.2.1") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.30") (d #t) (k 0)))) (h "0jcaz2ar4zf7v1gnsvzl3yx9lz2sxpmf20r52qvk7shazsb8yl0d")))

