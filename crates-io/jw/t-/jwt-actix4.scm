(define-module (crates-io jw t- jwt-actix4) #:use-module (crates-io))

(define-public crate-jwt-actix4-0.1.0 (c (n "jwt-actix4") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.2") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-utils") (r "^3.0") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "jwks-client") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n0i2p32pc7gf5l1yz9gl6gvjm1ghj96g8vqrvrinkj6pqiasx4y")))

(define-public crate-jwt-actix4-0.2.0 (c (n "jwt-actix4") (v "0.2.0") (d (list (d (n "actix-rt") (r "^2.2") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-utils") (r "^3.0") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "jwks-client") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wj61jms7dkcgm265d12cxwpsqn1zpa65hikfq6nrrj72hr4wnxp")))

(define-public crate-jwt-actix4-0.3.0 (c (n "jwt-actix4") (v "0.3.0") (d (list (d (n "actix-rt") (r "^2.7") (d #t) (k 2)) (d (n "actix-service") (r "^2.0.2") (d #t) (k 0)) (d (n "actix-utils") (r "^3.0") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "jwks-client") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bpr6fyafs8074y6xxgibb3wyaah909ij6n2nvm8j4vfxk36dnwg")))

