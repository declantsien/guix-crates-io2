(define-module (crates-io jw t- jwt-tonic-user-uuid) #:use-module (crates-io))

(define-public crate-jwt-tonic-user-uuid-0.0.1 (c (n "jwt-tonic-user-uuid") (v "0.0.1") (d (list (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "10mp2mga45lgyar04rc3qc90w86181kdzp8gfld17jx4gh4kl5ah")))

(define-public crate-jwt-tonic-user-uuid-0.0.2 (c (n "jwt-tonic-user-uuid") (v "0.0.2") (d (list (d (n "jsonwebtoken") (r "^8.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0jdmb1ndach01xx96ihz6q8b0pwnrm93mjijbsh2qwny4a6x65la")))

