(define-module (crates-io jw t- jwt-tui) #:use-module (crates-io))

(define-public crate-jwt-tui-0.1.0 (c (n "jwt-tui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "impl_new") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "ratatui-textarea") (r "^0.3") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (f (quote ("local-offset"))) (k 0)))) (h "07baq188vjwfva5fd77ibhq1f0y900zq4izd4kk0zi569lh4w2vm") (y #t)))

