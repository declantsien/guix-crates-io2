(define-module (crates-io jw t- jwt-debugger) #:use-module (crates-io))

(define-public crate-jwt-debugger-0.1.0 (c (n "jwt-debugger") (v "0.1.0") (d (list (d (n "jwt") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1k868b1b3096xzrb18jf3j2180kaw56v4w1fy2v4dxbc2z4mjfs4") (y #t)))

(define-public crate-jwt-debugger-0.1.1 (c (n "jwt-debugger") (v "0.1.1") (d (list (d (n "jwt") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1vmy5r9b0al70fxldi3pfdcjv0m5yxqf3ydal35hvzvsv92sa97p")))

