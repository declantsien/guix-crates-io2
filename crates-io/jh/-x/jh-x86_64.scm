(define-module (crates-io jh -x jh-x86_64) #:use-module (crates-io))

(define-public crate-jh-x86_64-0.1.0 (c (n "jh-x86_64") (v "0.1.0") (d (list (d (n "block-buffer") (r "^0.3.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 0)))) (h "0da09dd81mji5bxdh78p5andlc4xdivqblmfg2dg8mhbfwr1z3x0")))

(define-public crate-jh-x86_64-0.1.1 (c (n "jh-x86_64") (v "0.1.1") (d (list (d (n "block-buffer") (r "^0.6") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 0)))) (h "0q7a4dsjbficnigzs159qcpiqbvlgjhk6azmxxmxlr7w3h6bh1mh")))

(define-public crate-jh-x86_64-0.1.2 (c (n "jh-x86_64") (v "0.1.2") (d (list (d (n "block-buffer") (r "^0.6") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "digest") (r "^0.7.1") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "1k68id5n4s41y9c7y7jppwvak5wcxwfz0zfrfb4wik7x0ndr4zcl")))

(define-public crate-jh-x86_64-0.2.0 (c (n "jh-x86_64") (v "0.2.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "digest") (r "^0.8") (d #t) (k 0)))) (h "1h7y1y7xqv55kjpnlyi6mnjnfy4dj6ba1nmdhzrzgww82ynlnf5x")))

(define-public crate-jh-x86_64-0.2.1 (c (n "jh-x86_64") (v "0.2.1") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 0)))) (h "1i8hp6j5gkrlsvg3fxmbwa9fs564kpcak6scis0gap0anzx5d3m0") (f (quote (("std") ("default" "std") ("avx2"))))))

(define-public crate-jh-x86_64-0.2.2 (c (n "jh-x86_64") (v "0.2.2") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "crypto-simd") (r "^0.1") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "packed_simd_crate") (r "^0.3.2") (o #t) (d #t) (k 0) (p "packed_simd")) (d (n "ppv-null") (r "^0.1") (d #t) (k 0)) (d (n "simd") (r "^0.1") (o #t) (d #t) (k 0) (p "ppv-lite86")))) (h "02j9s44bi7fw81dbh9zcxzvn95xxjvqbzbiha5cc2vp6lplhx5z1") (f (quote (("std" "lazy_static") ("packed_simd" "packed_simd_crate" "crypto-simd/packed_simd") ("default" "std" "simd") ("avx2" "simd"))))))

