(define-module (crates-io jh as jhash) #:use-module (crates-io))

(define-public crate-jhash-0.1.0 (c (n "jhash") (v "0.1.0") (h "16q9zkpw7gbzwbgnqb75wvda3apvp27il83s9fh1z3wbbfdzkr16")))

(define-public crate-jhash-0.1.1 (c (n "jhash") (v "0.1.1") (h "1dhmzkadmkxf9krs5vlb3ffgmv5vkglvj5i8n62hddg5i212p01s")))

