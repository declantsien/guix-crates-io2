(define-module (crates-io jh #{71}# jh7110-vf2-12a-pac) #:use-module (crates-io))

(define-public crate-jh7110-vf2-12a-pac-0.1.0 (c (n "jh7110-vf2-12a-pac") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0fkr3pjng1dp9ky433a4mz9rhdks5zsia372fc3anw32lc566j13") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-jh7110-vf2-12a-pac-0.1.1 (c (n "jh7110-vf2-12a-pac") (v "0.1.1") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1pixnrghgjfbbh4dvb0kn4r8j2hmazcaaykfay1wrp8minqg88sf") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-jh7110-vf2-12a-pac-0.2.0 (c (n "jh7110-vf2-12a-pac") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "17png4f1xbw6xal0rqpii13bfhib360w4nih2larbm0k78lp36yx") (f (quote (("rt" "riscv-rt") ("default" "critical-section")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

