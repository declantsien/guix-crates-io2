(define-module (crates-io jh #{71}# jh7110-vf2-13b-pac) #:use-module (crates-io))

(define-public crate-jh7110-vf2-13b-pac-0.1.0 (c (n "jh7110-vf2-13b-pac") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1nr1xfa3m7z77mi0q6yhjlrcd57jwwl6knbj1z2yv84d9a51hi7k") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-jh7110-vf2-13b-pac-0.1.1 (c (n "jh7110-vf2-13b-pac") (v "0.1.1") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vazp5p8w9hjd38ar9bifvj8ikxn0dbr164l83cl84g8kgxp4g2k") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-jh7110-vf2-13b-pac-0.2.0 (c (n "jh7110-vf2-13b-pac") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1va4hyx6ndzfph3gsjky9pslyng3dnk7blwlka4356ijrd0i5lha") (f (quote (("rt" "riscv-rt") ("default" "critical-section")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

