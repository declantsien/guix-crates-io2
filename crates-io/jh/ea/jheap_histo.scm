(define-module (crates-io jh ea jheap_histo) #:use-module (crates-io))

(define-public crate-jheap_histo-0.1.0 (c (n "jheap_histo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "combine") (r "^4.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "05r731kx8lh44c5nspfcjv27gz8c8j0cq38i0jllpk52f8z4qysc")))

