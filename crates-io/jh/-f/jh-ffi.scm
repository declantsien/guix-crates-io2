(define-module (crates-io jh -f jh-ffi) #:use-module (crates-io))

(define-public crate-jh-ffi-0.0.1 (c (n "jh-ffi") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0x0kgam8z161v8wbhxd45dl96cms4y73b0qn7pmlskpg2ngxa4ff")))

(define-public crate-jh-ffi-0.0.2 (c (n "jh-ffi") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0l920zvmbp3z8nxvjh63knkchlvazrybmjvycxc7qpg9wyww1kff")))

(define-public crate-jh-ffi-0.5.0 (c (n "jh-ffi") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.26") (d #t) (k 1)))) (h "1hacbzslymcazadjjmnygc9c6b97gp03gdy5ai30x4qb6yjfxkhv")))

