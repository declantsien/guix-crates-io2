(define-module (crates-io af so afsort) #:use-module (crates-io))

(define-public crate-afsort-0.1.0 (c (n "afsort") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "1rd3wbazsy9x66xsy7371dxfwvaaml55did45rv8zsljph4140pc")))

(define-public crate-afsort-0.1.1 (c (n "afsort") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "0ira351yjfcxhqiknym3fzbchy2xbmqkj0alr6n71bpnw27zyyy4")))

(define-public crate-afsort-0.2.0 (c (n "afsort") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "1srxd5am9bh9kwjvfxwa0bnaw8w0xfrjhgpq8f89da1l4vwcacag")))

(define-public crate-afsort-0.3.0 (c (n "afsort") (v "0.3.0") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "1qrfzscqpn902s8m8j5k13ffnz514vi66bk5bwipgnnhpjbyywq2")))

(define-public crate-afsort-0.3.1 (c (n "afsort") (v "0.3.1") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "0kfv5mxgmi1k0l4bcwrq09sgmghs7f8r5kj7iwz219dm6dx0ijjm")))

