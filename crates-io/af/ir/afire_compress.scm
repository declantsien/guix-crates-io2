(define-module (crates-io af ir afire_compress) #:use-module (crates-io))

(define-public crate-afire_compress-0.1.0 (c (n "afire_compress") (v "0.1.0") (d (list (d (n "afire") (r "^0.3.0") (d #t) (k 0)) (d (n "afire") (r "^0.3.0") (d #t) (k 2)) (d (n "brotli2") (r "^0.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "libflate") (r "^1.1.2") (d #t) (k 0)))) (h "0q1796xf8jdf7v9j7das75qcagq98bsvnc5n8fsx5nqparsgl4k8")))

(define-public crate-afire_compress-0.1.1-alpha (c (n "afire_compress") (v "0.1.1-alpha") (d (list (d (n "afire") (r "^0.3.0") (d #t) (k 0)) (d (n "afire") (r "^0.3.0") (d #t) (k 2)) (d (n "brotli2") (r "^0.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "libflate") (r "^1.1.2") (d #t) (k 0)))) (h "1xm5cxkly5cw2xz7013ac69gxpw335k6pmv9hsvg60dd1s6r1a7q")))

