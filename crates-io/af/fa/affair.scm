(define-module (crates-io af fa affair) #:use-module (crates-io))

(define-public crate-affair-0.0.0 (c (n "affair") (v "0.0.0") (h "067rb864rxhb1yj7xn62fvvnxgp806dc2mwy3f62x584kisnf78v")))

(define-public crate-affair-0.1.0 (c (n "affair") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "19lwysivpwrjnc8scbwl3hvd7wyvjfw5v8vy4ag8hrhshr5lvnnb")))

(define-public crate-affair-0.1.1 (c (n "affair") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "06csm04y0cm9ymf848iqwfxxp29za3ba258vl2g6k4kwy3nsqb9x")))

(define-public crate-affair-0.1.2 (c (n "affair") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "0qz1jidj3wjf35yjpvkgbrhydbhihb5j0whahlgq75c0shb8qjkd")))

