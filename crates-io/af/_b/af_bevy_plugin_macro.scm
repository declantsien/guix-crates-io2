(define-module (crates-io af _b af_bevy_plugin_macro) #:use-module (crates-io))

(define-public crate-af_bevy_plugin_macro-0.1.0 (c (n "af_bevy_plugin_macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "19nip66smmapjmp9qyjyn2k19m1v4ynqcx728biws9796xy7dlzs")))

(define-public crate-af_bevy_plugin_macro-0.1.1 (c (n "af_bevy_plugin_macro") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0m77h29yb6k6prd1wv6v2w8jkazqkk4iql3lvq2hn75915i9gghn")))

(define-public crate-af_bevy_plugin_macro-0.1.2 (c (n "af_bevy_plugin_macro") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1fkj5q6yp4i8hxil3lqffdy2abj3idc9rhsa8xl7a2rgcs7ss7vf")))

