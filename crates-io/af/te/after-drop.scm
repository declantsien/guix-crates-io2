(define-module (crates-io af te after-drop) #:use-module (crates-io))

(define-public crate-after-drop-1.0.0 (c (n "after-drop") (v "1.0.0") (h "0pq0b75x6f40fj2y4drf3nblykq4l88knmhjn5pq0zs243b8m0lh") (f (quote (("std") ("default" "std"))))))

(define-public crate-after-drop-1.0.1 (c (n "after-drop") (v "1.0.1") (h "0nlsg5r2jsjvby1vgkf6nlvb4375gjazs41i99hfw9s2qdvyf82g") (f (quote (("std") ("default" "std"))))))

