(define-module (crates-io af te aftermath-rs) #:use-module (crates-io))

(define-public crate-aftermath-rs-0.1.0 (c (n "aftermath-rs") (v "0.1.0") (d (list (d (n "sysreq") (r "^0") (d #t) (k 1)))) (h "0ch7b05nqkiihs3pch3ii8g9z6n7797pvigc5zfz7ypl8yqknwm8")))

(define-public crate-aftermath-rs-0.1.1 (c (n "aftermath-rs") (v "0.1.1") (d (list (d (n "sysreq") (r "^0") (d #t) (k 1)))) (h "1qwpzy5pvkvixz0n4shvp0xr7p3x1wqbc4rx9466j4hr3ask3xx5") (y #t)))

(define-public crate-aftermath-rs-0.1.2 (c (n "aftermath-rs") (v "0.1.2") (d (list (d (n "sysreq") (r "^0") (d #t) (k 1)))) (h "0fidlj9sigkpsqbl6l8xvz1g6kd3f01is7wh51d817ba9hli61mw")))

(define-public crate-aftermath-rs-0.1.3 (c (n "aftermath-rs") (v "0.1.3") (d (list (d (n "sysreq") (r "^0") (d #t) (k 1)))) (h "13km993cx5d9nd1w93lpzjg2bz7m54dblrgqvsy72lfm6amsr7ns")))

