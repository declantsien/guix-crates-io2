(define-module (crates-io af te aftermath) #:use-module (crates-io))

(define-public crate-aftermath-0.1.0 (c (n "aftermath") (v "0.1.0") (h "19n8cjsqgv4fjmxg5i5xss4zm35ly09jd4l1qlzhfgidrsab9sp0")))

(define-public crate-aftermath-1.0.0 (c (n "aftermath") (v "1.0.0") (d (list (d (n "bumpalo") (r "^3.11.1") (f (quote ("collections"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "1g2ksqkfix6wfnmfwl5gllhviav3f5ln9vh8h6p2p7hgys8w88hc")))

(define-public crate-aftermath-1.1.0 (c (n "aftermath") (v "1.1.0") (d (list (d (n "bumpalo") (r "^3.11.1") (f (quote ("collections"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "08a2dxwclr2cw2h22x5jqz0im891m3avc119fj5fw0jm5bs6ljp4") (f (quote (("reduce") ("differentiation") ("default" "differentiation" "reduce"))))))

(define-public crate-aftermath-1.1.1 (c (n "aftermath") (v "1.1.1") (d (list (d (n "bumpalo") (r "^3.11.1") (f (quote ("collections"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "1qxq3x9yzys2m3g2xlvciga59fjzdf7v73i19phmpvw89nf465qq") (f (quote (("reduce") ("differentiation") ("default" "differentiation" "reduce"))))))

(define-public crate-aftermath-1.1.2 (c (n "aftermath") (v "1.1.2") (d (list (d (n "bumpalo") (r "^3.11.1") (f (quote ("collections"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "1alx7qkbdxm35d23fnvljkcybypwch8xjhl8m0an0366fjw9z0ag") (f (quote (("reduce") ("differentiation") ("default" "differentiation" "reduce"))))))

(define-public crate-aftermath-1.1.3 (c (n "aftermath") (v "1.1.3") (d (list (d (n "bumpalo") (r "^3.11.1") (f (quote ("collections"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "15fx3dydv1mh6ybhcj1k1r5vnlpl7403f4i2r6ax40qdk4w3yq48") (f (quote (("reduce") ("differentiation") ("default" "differentiation" "reduce"))))))

