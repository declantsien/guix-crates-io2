(define-module (crates-io af te aftershock) #:use-module (crates-io))

(define-public crate-aftershock-0.0.9 (c (n "aftershock") (v "0.0.9") (d (list (d (n "lodepng") (r "^3.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.19.1") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 2)))) (h "00v7yr8m465333gazp521diprsmf51nqhd79514ic467x6z1jnqw")))

(define-public crate-aftershock-0.0.10 (c (n "aftershock") (v "0.0.10") (d (list (d (n "lodepng") (r "^3.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.19.1") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 2)))) (h "0yiv4wcny1f2kl7hjwx7qvk460jj1plnxbjlwz1f3akixyjjw3my")))

(define-public crate-aftershock-0.0.11 (c (n "aftershock") (v "0.0.11") (d (list (d (n "lodepng") (r "^3.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.19.1") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 2)))) (h "0yvlzd91q1mhsxd2l4fh66vv9f8kqyg5ninba4m7avxysw1mn8q6")))

(define-public crate-aftershock-0.0.12 (c (n "aftershock") (v "0.0.12") (d (list (d (n "lodepng") (r "^3.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.19.1") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 2)))) (h "0j5r1vdn0jk30yxay02n95g6c4mpbpnf4cfpmmv6mmdmbi4khjjx")))

(define-public crate-aftershock-0.0.13 (c (n "aftershock") (v "0.0.13") (d (list (d (n "framebuffer") (r "^0.3.0") (d #t) (k 2)) (d (n "lodepng") (r "^3.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.20.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.0") (d #t) (k 2)))) (h "0ka72vh9zv18lhaiqflf4g84c9k3xa0nyfa2pvm3ckl52hlspx5q")))

(define-public crate-aftershock-0.0.16 (c (n "aftershock") (v "0.0.16") (d (list (d (n "framebuffer") (r "^0.3.0") (d #t) (k 2)) (d (n "lodepng") (r "^3.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.0") (d #t) (k 2)) (d (n "squares-rng") (r "^0.2.0") (d #t) (k 2)))) (h "1gd6k542j3bl51qb5mvxcz1l3mc54lf3p1bkqb5jkrw69f6is4hv")))

(define-public crate-aftershock-0.0.17 (c (n "aftershock") (v "0.0.17") (d (list (d (n "framebuffer") (r "^0.3.0") (d #t) (k 2)) (d (n "lodepng") (r "^3.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.0") (d #t) (k 2)) (d (n "squares-rng") (r "^0.2.0") (d #t) (k 2)))) (h "1m2ynwrflph8mxzq1d7brj4cbimz72kzimhsmnny3jjv0hccpinv")))

(define-public crate-aftershock-0.0.18 (c (n "aftershock") (v "0.0.18") (d (list (d (n "lodepng") (r "^3.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "alea") (r "^0.2.2") (d #t) (k 2)) (d (n "device_query") (r "^1.1.2") (d #t) (k 2)) (d (n "minifb") (r "^0.23.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 2)))) (h "062ldq97zb69j4zgbw1lchldvgs4azml1lnhdgbdd6waqficqrlh")))

