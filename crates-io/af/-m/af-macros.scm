(define-module (crates-io af -m af-macros) #:use-module (crates-io))

(define-public crate-af-macros-0.1.0 (c (n "af-macros") (v "0.1.0") (d (list (d (n "af-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0mz09cg5h1vjv64ywsgi9azmp158qp0pcwbbsh9h2m5l5nhb7qk3") (f (quote (("logger" "af-proc-macros/logger"))))))

(define-public crate-af-macros-0.1.3 (c (n "af-macros") (v "0.1.3") (d (list (d (n "af-proc-macros") (r "^0.1") (d #t) (k 0)))) (h "0halcxikcp43mksnarhlbc5dcs5r0bs02lq7qqqp9qx3qf4hlfb9") (f (quote (("logger" "af-proc-macros/logger"))))))

