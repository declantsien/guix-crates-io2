(define-module (crates-io af o3 afo3c4hnlo78d_publish_test_a) #:use-module (crates-io))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1-alpha.0 (c (n "afo3c4hnlo78d_publish_test_a") (v "0.0.1-alpha.0") (h "09lpgmnlnj8hqzyryq8nvsa0gh62j6k05bxy5zca258jwv22p80d") (y #t) (r "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1-alpha.2 (c (n "afo3c4hnlo78d_publish_test_a") (v "0.0.1-alpha.2") (h "0pvylp2yigdb1ddgr5623izxfz4bszxlq0fy6pklw53xqx30s80l") (r "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1-alpha.3 (c (n "afo3c4hnlo78d_publish_test_a") (v "0.0.1-alpha.3") (h "03lnvaj9qp868ckfkg2r5760d162nwdkqq3sz07j4v71lifdlrm6") (r "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1-alpha.5 (c (n "afo3c4hnlo78d_publish_test_a") (v "0.0.1-alpha.5") (h "1jlgs8w3cd6ch26pj2lxb0jxfs375b40b0jamfqk91dry3g017lx") (r "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1-alpha.6 (c (n "afo3c4hnlo78d_publish_test_a") (v "0.0.1-alpha.6") (h "0vkvcn16jjs9rcpxb00kfn9m5blppq1gp5fh4l2j9nzk7j7mh46g") (r "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1-alpha.7 (c (n "afo3c4hnlo78d_publish_test_a") (v "0.0.1-alpha.7") (h "09hyx9ah0bljgw7a3vl4sz3p8rjxfx2mmma4ynk2p0r05046fdwc") (r "1.69")))

