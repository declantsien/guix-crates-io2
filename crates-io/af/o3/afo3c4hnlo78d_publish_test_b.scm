(define-module (crates-io af o3 afo3c4hnlo78d_publish_test_b) #:use-module (crates-io))

(define-public crate-afo3c4hnlo78d_publish_test_b-0.0.1-alpha.0 (c (n "afo3c4hnlo78d_publish_test_b") (v "0.0.1-alpha.0") (h "05wj2g0p455l4i3a1n96x2kxv4m9bladllpj2g1mwmm3dqckjwk2") (y #t) (r "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_b-0.0.1-alpha.7 (c (n "afo3c4hnlo78d_publish_test_b") (v "0.0.1-alpha.7") (d (list (d (n "afo3c4hnlo78d_publish_test_a") (r "^0.0.1-alpha.7") (d #t) (k 0)))) (h "06yb2ls182ir7vsj8cchxwrgvmxnz1ccjwa9izbfkrwk7djpqqsn") (r "1.69")))

