(define-module (crates-io af ib afibex) #:use-module (crates-io))

(define-public crate-afibex-0.1.0 (c (n "afibex") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "0qw35ajhyw35axn5j69dd1ypcfw770pvnp6wg1cpv87shr4h9njm")))

(define-public crate-afibex-0.2.0 (c (n "afibex") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "07vddrld068cna9l1ly4c9qpf0nh2qa1ddds8f21803w1grzp2sx")))

(define-public crate-afibex-0.2.1 (c (n "afibex") (v "0.2.1") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "1w7yi9h59fpcj65065jznvmwfbz2wq0bvdfvx00wgi8gmxsab3yj")))

(define-public crate-afibex-0.3.0 (c (n "afibex") (v "0.3.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "0jc8viy1br1m6gjkyfspw0y3lj75mp12z03d8sz9z0x6n8165iq4")))

(define-public crate-afibex-0.3.1 (c (n "afibex") (v "0.3.1") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "0h4wmanpxcjcwawynh0mg2bj5vc7i8i6z619m45d97b20lgnc2pk")))

(define-public crate-afibex-0.3.2 (c (n "afibex") (v "0.3.2") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "1ff13wwvaa01sl160cjssjv435qsails9z0wgygjyarcc0dyacja")))

(define-public crate-afibex-0.4.0 (c (n "afibex") (v "0.4.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "0cdkfbhgbvsgqqsxllklhzslg4nn14m5g3vx2q66riqc0wzpdhni")))

(define-public crate-afibex-0.5.0 (c (n "afibex") (v "0.5.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "1mvq8x2ip4kgs1kp36hb7kmfssz6648qbmb6p2v62f7fnndl634k")))

(define-public crate-afibex-0.6.0 (c (n "afibex") (v "0.6.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "1qi8h6k12vfhnq8brv6may132q6zqk20dvlvxbjkn4kpbr055cas")))

(define-public crate-afibex-0.6.1 (c (n "afibex") (v "0.6.1") (d (list (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "0m2hk3h0wf0vvs65rvaidbplzp5vcbyw9ll1237nafsz8ad71hl9")))

(define-public crate-afibex-0.6.2 (c (n "afibex") (v "0.6.2") (d (list (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "18hdzyhbavy3vq5i629frk1p2manx4cvsrqalvrqr5m2021s26c4")))

(define-public crate-afibex-0.7.0 (c (n "afibex") (v "0.7.0") (d (list (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "16wg5bphalkl8xwrscn4vfddzmp0199a94qbkgyiqsnnlf7ihs2y")))

(define-public crate-afibex-0.8.0 (c (n "afibex") (v "0.8.0") (d (list (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "0flf4lz7x3r86jkmb0cjp8skcmq72ykci36fzxhw3rp1rxc9mj6b")))

(define-public crate-afibex-0.9.0 (c (n "afibex") (v "0.9.0") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "1gsn6mi38jf0sgvxkl4n6yb0isbp8qj7mrfwm4a5an96j8v47lg8")))

(define-public crate-afibex-0.10.0 (c (n "afibex") (v "0.10.0") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kmia1gv5mzhnaq9b1cpqn8qx7i2ip1r1gcpfacwvqb70215xpn4")))

(define-public crate-afibex-0.10.1 (c (n "afibex") (v "0.10.1") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sqdw6pa1q6yqpvhhgf3p9xvw5kc7b6m8z35hv3ym1idycmw6qj6")))

(define-public crate-afibex-0.11.0 (c (n "afibex") (v "0.11.0") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vgcr8avzv06hbz547h58wxqgnlrpf31f5jrgaac3ywnm8n9hr5d")))

(define-public crate-afibex-0.11.1 (c (n "afibex") (v "0.11.1") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gq3sk28hq0rwnswk9nynkm1sl2jix5xrfykmy4spzhn7aspwqm2")))

