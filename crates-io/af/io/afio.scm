(define-module (crates-io af io afio) #:use-module (crates-io))

(define-public crate-afio-0.1.0 (c (n "afio") (v "0.1.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "mio") (r "^0.6.1") (d #t) (k 0)))) (h "0lwlmgdy9pinf6m6qan7f0hj88h1jnl02v17aybj0v0q61jg7ynq")))

