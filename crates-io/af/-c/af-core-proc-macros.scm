(define-module (crates-io af -c af-core-proc-macros) #:use-module (crates-io))

(define-public crate-af-core-proc-macros-0.1.0 (c (n "af-core-proc-macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lllk63h3px0cr3xkgckrmi4kw9a1xsi4ghfxxy5y08pija93p0q") (f (quote (("logger"))))))

