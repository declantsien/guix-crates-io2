(define-module (crates-io af -c af-cuda-interop) #:use-module (crates-io))

(define-public crate-af-cuda-interop-3.7.1 (c (n "af-cuda-interop") (v "3.7.1") (d (list (d (n "arrayfire") (r "^3.7") (d #t) (k 0)) (d (n "cuda-runtime-sys") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustacuda") (r "^0.1") (d #t) (k 2)) (d (n "rustacuda_core") (r "^0.1") (d #t) (k 2)))) (h "02775cz3c7r7cnhxqb8xyp9h6wnk7cl5azr8jmx8922zaxvmddyp")))

