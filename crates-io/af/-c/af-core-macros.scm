(define-module (crates-io af -c af-core-macros) #:use-module (crates-io))

(define-public crate-af-core-macros-0.1.0 (c (n "af-core-macros") (v "0.1.0") (d (list (d (n "af-core-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1hrpmk20gak3v205gl5ayqf3965j0374qrvvn8lv7w8906x84g6d") (f (quote (("logger" "af-core-proc-macros/logger"))))))

