(define-module (crates-io af la aflak_imgui-sys) #:use-module (crates-io))

(define-public crate-aflak_imgui-sys-0.18.1 (c (n "aflak_imgui-sys") (v "0.18.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gfx") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.20") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nz529bwngfwd1hp7l6ah5a88nnl746yig5rn5zw7f9cdyw035j9")))

