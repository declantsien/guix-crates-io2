(define-module (crates-io af la aflak_imgui_glium_support) #:use-module (crates-io))

(define-public crate-aflak_imgui_glium_support-0.0.3 (c (n "aflak_imgui_glium_support") (v "0.0.3") (d (list (d (n "glium") (r "^0.22") (f (quote ("glutin"))) (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-glium-renderer") (r "^0.0.21") (d #t) (k 0)))) (h "1ncc2rlfpqg6yhrsm5x0wxqd9zi8hi94k3whql8xvgrlxx1cjr3k")))

