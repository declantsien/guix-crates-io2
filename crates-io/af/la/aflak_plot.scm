(define-module (crates-io af la aflak_plot) #:use-module (crates-io))

(define-public crate-aflak_plot-0.0.1 (c (n "aflak_plot") (v "0.0.1") (d (list (d (n "aflak_imgui") (r "^0.18.1") (d #t) (k 0)) (d (n "aflak_imgui-glium-renderer") (r "^0.18.1") (d #t) (k 0)) (d (n "glium") (r "^0.20") (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)))) (h "02xwwldx79d2vqbffsbsbjjhjvjzi9x94wvry99idhwgbifxfw0a")))

(define-public crate-aflak_plot-0.0.3 (c (n "aflak_plot") (v "0.0.3") (d (list (d (n "aflak_imgui_glium_support") (r "^0.0.3") (d #t) (k 2)) (d (n "glium") (r "^0.22") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-glium-renderer") (r "^0.0.21") (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)))) (h "141xvyvpn191zm8wz643sqm0jf5z73vdrvqqlqkhjszwcw90xv57")))

