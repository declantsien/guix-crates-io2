(define-module (crates-io af la aflak_cake) #:use-module (crates-io))

(define-public crate-aflak_cake-0.0.1 (c (n "aflak_cake") (v "0.0.1") (d (list (d (n "boow") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "variant_name") (r "^0.0.1") (d #t) (k 0)) (d (n "variant_name_derive") (r "^0.0.1") (d #t) (k 2)))) (h "18mw4k0sqmxyqhcxgkwiws48pazqskhgr9097zcjgkfm5wfk296q")))

(define-public crate-aflak_cake-0.0.3 (c (n "aflak_cake") (v "0.0.3") (d (list (d (n "boow") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "variant_name") (r "^0.0.1") (d #t) (k 0)) (d (n "variant_name_derive") (r "^0.0.1") (d #t) (k 2)))) (h "185imf2nyzc01gqs5nwhn24igx8y6gmnib7lcn5xkqc24fng5fyg")))

