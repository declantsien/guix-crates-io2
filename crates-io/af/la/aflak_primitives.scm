(define-module (crates-io af la aflak_primitives) #:use-module (crates-io))

(define-public crate-aflak_primitives-0.0.1 (c (n "aflak_primitives") (v "0.0.1") (d (list (d (n "aflak_cake") (r "^0.0.1") (d #t) (k 0)) (d (n "fitrs") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "variant_name") (r "^0.0.1") (d #t) (k 0)) (d (n "variant_name_derive") (r "^0.0.1") (d #t) (k 0)))) (h "0yxn26vvw9a2qw44vywjkg7z2dihjxq3bh8s2aw6ijgg39a3y95p")))

(define-public crate-aflak_primitives-0.0.3 (c (n "aflak_primitives") (v "0.0.3") (d (list (d (n "aflak_cake") (r "^0.0.3") (d #t) (k 0)) (d (n "fitrs") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "variant_name") (r "^0.0.1") (d #t) (k 0)) (d (n "variant_name_derive") (r "^0.0.1") (d #t) (k 0)))) (h "0lr1dgxmggfcr2jmsdqjijjihwjpw8r7bsxabw68qv90hznl6nyd")))

