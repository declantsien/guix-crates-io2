(define-module (crates-io af la aflak_imgui_file_explorer) #:use-module (crates-io))

(define-public crate-aflak_imgui_file_explorer-0.0.1 (c (n "aflak_imgui_file_explorer") (v "0.0.1") (d (list (d (n "aflak_imgui") (r "^0.18.1") (d #t) (k 0)) (d (n "aflak_imgui-glium-renderer") (r "^0.18.1") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.20") (d #t) (k 2)))) (h "08rjm1fyrwqy8aza9iz6aanlyaw2nj4l0nicj5rbp6maagdj90y1")))

