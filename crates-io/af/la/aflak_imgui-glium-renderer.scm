(define-module (crates-io af la aflak_imgui-glium-renderer) #:use-module (crates-io))

(define-public crate-aflak_imgui-glium-renderer-0.18.1 (c (n "aflak_imgui-glium-renderer") (v "0.18.1") (d (list (d (n "aflak_imgui") (r "^0.18.1") (d #t) (k 0)) (d (n "aflak_imgui-sys") (r "^0.18.1") (f (quote ("glium"))) (d #t) (k 0)) (d (n "glium") (r "^0.20") (f (quote ("glutin"))) (d #t) (k 0)))) (h "0d7xqrnf31grkfabk85kzfa5fixqd28izc9y8ypzilba2vif9g0s")))

