(define-module (crates-io af -o af-opencl-interop) #:use-module (crates-io))

(define-public crate-af-opencl-interop-3.7.1 (c (n "af-opencl-interop") (v "3.7.1") (d (list (d (n "arrayfire") (r "^3.7") (d #t) (k 0)) (d (n "cl-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ocl-core") (r "^0.11.2") (d #t) (k 2)))) (h "1n00dm9hbgf4nhbbd2zdqm8ypnh8nzdagq1yc95lnn4f00vdx9x5")))

