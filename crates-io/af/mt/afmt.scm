(define-module (crates-io af mt afmt) #:use-module (crates-io))

(define-public crate-afmt-0.1.0 (c (n "afmt") (v "0.1.0") (d (list (d (n "devise") (r "^0.2.0") (d #t) (k 0)))) (h "1qcbmz3j4yazwnhvhl12gjy5y0rqsl3ldnm82pki72aawamf9xzv")))

(define-public crate-afmt-0.1.1 (c (n "afmt") (v "0.1.1") (d (list (d (n "devise") (r "^0.2.0") (d #t) (k 0)))) (h "1gk8shbwb1m89y0jk1c1h4nlm6g6pysagmnmc1qpc2rg9049l87s")))

