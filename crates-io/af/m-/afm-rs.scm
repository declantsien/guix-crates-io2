(define-module (crates-io af m- afm-rs) #:use-module (crates-io))

(define-public crate-afm-rs-0.1.0 (c (n "afm-rs") (v "0.1.0") (h "0asf0f5kwn6ckcyg3mysxj8v0hh8rml98v5p2qph12sjfqagaqnz")))

(define-public crate-afm-rs-0.1.1 (c (n "afm-rs") (v "0.1.1") (h "1x1qr25lkxxnnhf1ysp6wliyypqkjbgr31il9sqhha2bmlzlhwhq")))

