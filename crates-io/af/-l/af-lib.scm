(define-module (crates-io af -l af-lib) #:use-module (crates-io))

(define-public crate-af-lib-0.1.1 (c (n "af-lib") (v "0.1.1") (d (list (d (n "af-core") (r "^0.1") (d #t) (k 0)) (d (n "af-postgres") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "af-sentry") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "af-slack") (r "^0.1") (o #t) (d #t) (k 0)))) (h "006sw6igz1ardf2b9sfma2ypwmkwpd3985iyi5myp0mfzjq0zyp7") (f (quote (("tokio" "af-core/tokio") ("test-runner" "af-core/test-runner") ("slack" "af-slack") ("sentry" "af-sentry") ("postgres" "af-postgres") ("logger" "af-core/logger") ("default" "logger"))))))

