(define-module (crates-io af -s af-slack) #:use-module (crates-io))

(define-public crate-af-slack-0.1.0 (c (n "af-slack") (v "0.1.0") (d (list (d (n "af-core") (r "^0.1") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1wrpacrxkmv1x352yzcy5rz0f7vv9v0cfh5yj0nm3i5dhpk5p84d")))

