(define-module (crates-io af -s af-sentry) #:use-module (crates-io))

(define-public crate-af-sentry-0.1.0 (c (n "af-sentry") (v "0.1.0") (d (list (d (n "af-core") (r "^0.1") (d #t) (k 0)) (d (n "sentry") (r "^0.21") (d #t) (k 0)))) (h "007avyixd5l81mplszl6c72swm3kxjvy3jf9g9hvq3i87va23gg2")))

(define-public crate-af-sentry-0.1.1 (c (n "af-sentry") (v "0.1.1") (d (list (d (n "af-core") (r "^0.1") (d #t) (k 0)) (d (n "sentry") (r "^0.21") (d #t) (k 0)))) (h "14l7m6vq2vbl9vs1fz04pahzb7rvx9l5qadszlsbsmnxd9k5i62p")))

