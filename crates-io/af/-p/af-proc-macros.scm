(define-module (crates-io af -p af-proc-macros) #:use-module (crates-io))

(define-public crate-af-proc-macros-0.1.0 (c (n "af-proc-macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pard3527q7mw6cc17z5bmbgq2sxvvgak2pnfx227myrnq4p48in") (f (quote (("logger"))))))

(define-public crate-af-proc-macros-0.1.1 (c (n "af-proc-macros") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m0ifc3b1mkrbp8bvl32375ihyhqrplr6mr7vqpdgfshxqyalv25") (f (quote (("logger"))))))

