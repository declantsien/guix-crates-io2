(define-module (crates-io af ea afeather) #:use-module (crates-io))

(define-public crate-afeather-0.0.1 (c (n "afeather") (v "0.0.1") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)) (d (n "extend-lifetime") (r "^0.2.0") (d #t) (k 0)))) (h "1rylnm399xykx39jmm202xaifvlgwsxyw928zrkn1q3bff9d8an9")))

