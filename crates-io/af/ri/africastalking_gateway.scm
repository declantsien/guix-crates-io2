(define-module (crates-io af ri africastalking_gateway) #:use-module (crates-io))

(define-public crate-africastalking_gateway-0.1.0 (c (n "africastalking_gateway") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08hprfxbgxk8104xgvkr2j2i08mc74h58hskjysji65vhi4g1j3j")))

