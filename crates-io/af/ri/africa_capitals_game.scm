(define-module (crates-io af ri africa_capitals_game) #:use-module (crates-io))

(define-public crate-africa_capitals_game-1.0.0 (c (n "africa_capitals_game") (v "1.0.0") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "1mpz87az3ig2w087hilxc3zm7j8qv0qisw3x02k8l7nsdfd2y91i")))

(define-public crate-africa_capitals_game-1.1.0 (c (n "africa_capitals_game") (v "1.1.0") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "0qg8kymjz5jr1czvkg85h08pkbh8njkks01kwpfnyjqj917l9sxg")))

(define-public crate-africa_capitals_game-1.2.0 (c (n "africa_capitals_game") (v "1.2.0") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "1w8vxnzdw85ziab41kchwfl2g2rxzjlgv2fq0jlr28i6jhqi2dl3")))

(define-public crate-africa_capitals_game-1.2.1 (c (n "africa_capitals_game") (v "1.2.1") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "11iybga9fafwmrrhgz3s691wms7q6sirayp91nwlflb6s1grb7z3")))

