(define-module (crates-io af ri afrim-memory) #:use-module (crates-io))

(define-public crate-afrim-memory-0.3.2 (c (n "afrim-memory") (v "0.3.2") (h "0h7h3irzbb8nnqhznhgg05mpy6mdysqh13pmznrwkz0vgsq05705")))

(define-public crate-afrim-memory-0.4.0 (c (n "afrim-memory") (v "0.4.0") (h "006x3vnxrfhawr7kf54r80r9n22ywlghp83dfzymsryqqc65x5a9")))

(define-public crate-afrim-memory-0.4.1 (c (n "afrim-memory") (v "0.4.1") (h "16lfl2ils6a3cy2r2g60xg9dlgfxmkcr63i4i4r9ql8vyx0d6fgi") (y #t)))

(define-public crate-afrim-memory-0.4.2 (c (n "afrim-memory") (v "0.4.2") (h "1q2i0m1nz21cz8h10sis7lzi3dqb1sfpq27rrb7z7kvfqhw1shs4")))

