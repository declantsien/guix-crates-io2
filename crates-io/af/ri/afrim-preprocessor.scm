(define-module (crates-io af ri afrim-preprocessor) #:use-module (crates-io))

(define-public crate-afrim-preprocessor-0.5.0 (c (n "afrim-preprocessor") (v "0.5.0") (d (list (d (n "afrim-memory") (r "^0.3.2") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.7.0") (d #t) (k 0)))) (h "0k3ixd5xlywwf6n291ki5fhs12pkmaym6pk6gyzdmn4cbznlh56d") (f (quote (("inhibit") ("default"))))))

(define-public crate-afrim-preprocessor-0.5.1 (c (n "afrim-preprocessor") (v "0.5.1") (d (list (d (n "afrim-memory") (r "^0.3.2") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.7.0") (d #t) (k 0)))) (h "1gx3w309akz9c0vb4h41pwbvlv3sn3b5bvys6pmlbci1ihis82zr") (f (quote (("inhibit") ("default"))))))

(define-public crate-afrim-preprocessor-0.6.0 (c (n "afrim-preprocessor") (v "0.6.0") (d (list (d (n "afrim-memory") (r "^0.4.0") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.7.0") (k 0)) (d (n "keyboard-types") (r "^0.7.0") (f (quote ("webdriver"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zij6cq34y5nqbgxb8hsmlxd84fqyb208im1dhva6davjwl1x6s0") (f (quote (("inhibit") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "keyboard-types/serde"))))))

(define-public crate-afrim-preprocessor-0.6.1 (c (n "afrim-preprocessor") (v "0.6.1") (d (list (d (n "afrim-memory") (r "^0.4.2") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.7.0") (k 0)) (d (n "keyboard-types") (r "^0.7.0") (f (quote ("webdriver"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wf0lqwlkw6f9r9p30sdzvrqqsqmgs2n52y1ixgvgqikifra193b") (f (quote (("inhibit") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "keyboard-types/serde"))))))

