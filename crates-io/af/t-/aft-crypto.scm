(define-module (crates-io af t- aft-crypto) #:use-module (crates-io))

(define-public crate-aft-crypto-1.2.2 (c (n "aft-crypto") (v "1.2.2") (d (list (d (n "aes-gcm") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (k 0)) (d (n "scrypt") (r "^0.11") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (d #t) (k 0)))) (h "19wjj2bnjy1zyd4w849b2g1c6m1l1dx6fagdll036gm24lys1sfd")))

