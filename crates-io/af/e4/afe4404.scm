(define-module (crates-io af e4 afe4404) #:use-module (crates-io))

(define-public crate-afe4404-0.1.0 (c (n "afe4404") (v "0.1.0") (d (list (d (n "codegen") (r "^0.2.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)))) (h "0m0gp29aw49sc8hdvdr7jcc2970p4c2kymql1jfd79q798x4bwvg")))

(define-public crate-afe4404-0.2.0 (c (n "afe4404") (v "0.2.0") (d (list (d (n "codegen") (r "^0.2.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)))) (h "0b539c3pimzlrb5h47rrxvcrf6jfzzl103m3yrzkfq5q1jrhcm2f")))

(define-public crate-afe4404-0.2.1 (c (n "afe4404") (v "0.2.1") (d (list (d (n "codegen") (r "^0.2.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)))) (h "14knk4bzj5ihp7lr6xa6kw5r7kvx4g6qyjw84v3y47hzngpfp7ck")))

(define-public crate-afe4404-0.2.2 (c (n "afe4404") (v "0.2.2") (d (list (d (n "codegen") (r "^0.2.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)))) (h "0vm8zv6jd4vz6fdcmihhbsb47cc0wr2p4g3p7q2bfap9gx60fn13")))

(define-public crate-afe4404-0.2.3 (c (n "afe4404") (v "0.2.3") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)) (d (n "codegen") (r "^0.2.0") (d #t) (k 1)))) (h "1lg6vxda72dll40qzkakbqp70va6l28fajl4afw69h4chwcpc4v4")))

(define-public crate-afe4404-0.2.4 (c (n "afe4404") (v "0.2.4") (d (list (d (n "codegen") (r "^0.2.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)))) (h "1ckp4zscnqhbx6gq9s9l1h9gf68bb6vgyqgxx0s5xrg9fsk4qlqw")))

