(define-module (crates-io af e4 afe4400) #:use-module (crates-io))

(define-public crate-afe4400-0.1.0 (c (n "afe4400") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "0ph9pvyhhab7w1m51vcp0hnpa24wzgip3smd73k08f3v5b8sqp6i")))

(define-public crate-afe4400-0.2.0 (c (n "afe4400") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.1.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1xgwxv96jzd66zja7wk3kng584ky7a0pxw3i8x8a36yljii97p00")))

(define-public crate-afe4400-0.3.0 (c (n "afe4400") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.1.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "0jrylij9s4ra8ka1k4nz5d71x5rndm5904pjbr0a5c9n92pc483v")))

(define-public crate-afe4400-0.3.1 (c (n "afe4400") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "0z950jv5l2z6iq4y87vjvk595alpb8hf17mkjq08dqsdp571wdbf")))

(define-public crate-afe4400-0.4.0 (c (n "afe4400") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "0208mgn14zwry977hiva0m4aqzwbh9g78lgzj7g60c4lzn1zzjyw")))

