(define-module (crates-io af ch afch-logger) #:use-module (crates-io))

(define-public crate-afch-logger-0.1.0-alpha.0 (c (n "afch-logger") (v "0.1.0-alpha.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06m5ps6g469hww29av9hyk8czgd8pvdzzpik0x5ib2i9d44l9nsp") (y #t)))

(define-public crate-afch-logger-0.1.0 (c (n "afch-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0k384612yc91in52z4mqpzbwwnsvlaxg7ims3x185692jh5li9kq") (y #t)))

(define-public crate-afch-logger-0.2.0 (c (n "afch-logger") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0grjanwvcqciy4d06vsfkdyy3xcpqxawwkz0lbz4vp5w9bgi3gpz")))

