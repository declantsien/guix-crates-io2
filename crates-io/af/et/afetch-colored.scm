(define-module (crates-io af et afetch-colored) #:use-module (crates-io))

(define-public crate-afetch-colored-2.0.1 (c (n "afetch-colored") (v "2.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "05b4hiii9y7q51776a1ly7npb6mgmv9d5cy4vs1rg7wbmcnkgks6") (f (quote (("no-color"))))))

(define-public crate-afetch-colored-2.0.2 (c (n "afetch-colored") (v "2.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "1cyfl72y7np8xsnqi46ynp13a0kpnywy5p56xays4758bzk6cz3l") (f (quote (("no-color"))))))

(define-public crate-afetch-colored-2.0.3 (c (n "afetch-colored") (v "2.0.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "0y5amynh09hlmbk8kkqykjfg9yn5hk0d4lq4s2jmmhz5q1xminxp") (f (quote (("no-color"))))))

(define-public crate-afetch-colored-2.0.4 (c (n "afetch-colored") (v "2.0.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12zsk0rsm492w0j2l4f13a82cy6jwl0z9lwr2mcrlsh8s9hxn51n") (f (quote (("no-color")))) (r "1.70")))

