(define-module (crates-io af fi affine_transforms) #:use-module (crates-io))

(define-public crate-affine_transforms-0.1.0 (c (n "affine_transforms") (v "0.1.0") (h "12q62h1j28yx9q6haxf289fcapyyw13ncj5h2773iwi82qcscwy1")))

(define-public crate-affine_transforms-0.1.1 (c (n "affine_transforms") (v "0.1.1") (h "0aig592sj6iph0n439z5ncznk8392axijnmx7fzkw6184jhy81ip")))

(define-public crate-affine_transforms-0.2.0 (c (n "affine_transforms") (v "0.2.0") (h "0ap9zsq213p66fyr0px1blkyxbmxlxwqn6a4vjm93zfb4v195la8")))

(define-public crate-affine_transforms-0.3.0 (c (n "affine_transforms") (v "0.3.0") (d (list (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)))) (h "0p5jg91pdbsfd6kxr4pfj36cj49av0izhi2pa3nf0mgzdkps0wvl")))

