(define-module (crates-io af fi affinityprop) #:use-module (crates-io))

(define-public crate-affinityprop-0.1.1 (c (n "affinityprop") (v "0.1.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "15gjn5qb118jpqnr2rhp4fsi176kwjpc6fy038ccs4p4w59hypn8")))

(define-public crate-affinityprop-0.2.0 (c (n "affinityprop") (v "0.2.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0qiw24a733dvx1z621gc873ydylva5zmcy91sb0lmcn2kxaikr9g")))

