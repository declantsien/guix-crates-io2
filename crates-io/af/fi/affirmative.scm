(define-module (crates-io af fi affirmative) #:use-module (crates-io))

(define-public crate-affirmative-0.1.0 (c (n "affirmative") (v "0.1.0") (h "1my5nkbjisbn2ja83z64w5wzrib9sxgxqvzprm6623scsnf9csyw")))

(define-public crate-affirmative-0.1.1 (c (n "affirmative") (v "0.1.1") (h "0cqyywxb8vmsvlrq2d5hf15515w4xajnaxf4wfkp4gpcdqq2x9pm")))

