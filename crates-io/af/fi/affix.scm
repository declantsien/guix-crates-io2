(define-module (crates-io af fi affix) #:use-module (crates-io))

(define-public crate-affix-0.1.0 (c (n "affix") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "04pwk4hyli49hjljc1vrg82hymj080ql8fa7xg1ryr5qg67nljfn") (y #t) (r "1.31")))

(define-public crate-affix-0.1.1 (c (n "affix") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "03av802hv19c5hclzx6qcvypd4xyll0qh356sc6zjfgaxfnaph1m") (r "1.31")))

(define-public crate-affix-0.1.2 (c (n "affix") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1g6lslkr3xc4yfnqy6546n8q95la845rlhpqaprhj87ssf2fmrsh") (r "1.31")))

