(define-module (crates-io af _p af_packet) #:use-module (crates-io))

(define-public crate-af_packet-0.1.0 (c (n "af_packet") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dn0cd9xm44hyg3ghlsgwq31333k5f2ad07bxrinl2d1qcwd7b9s")))

(define-public crate-af_packet-0.1.1 (c (n "af_packet") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vv21l3xvpxm6da10i1ydldp5xwq6pjdywgi8m3m0llmavpk59zc")))

(define-public crate-af_packet-0.1.3 (c (n "af_packet") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "113h1hcb5bfca6za93yzwbjmn9qax2a544yww8f5l5szgdyppmg8")))

(define-public crate-af_packet-0.1.4 (c (n "af_packet") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "144qx8n6cy90vqb1x3xp3m0v15da2g08a2rl1rxkr6xch9ks89dx")))

(define-public crate-af_packet-0.1.5 (c (n "af_packet") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0mb7xgi98qvm3awwqwjjjir9ydaqraardls49zannwljvwm9r37f")))

(define-public crate-af_packet-0.1.6 (c (n "af_packet") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1qciyd1g7l1mych6m05zfdn0zgasg3xna2ny413m27wzmhw608fm")))

(define-public crate-af_packet-0.2.0 (c (n "af_packet") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0mnxgvf5c02ggphcm5v79pfy4zpwkm4476x8l3srby9wxksazgy1")))

(define-public crate-af_packet-0.2.1 (c (n "af_packet") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "06bch6gn0rkzj2vljl6ffnl2qgg1xwqv727rx5cvmz9hkm5pnnxk")))

(define-public crate-af_packet-0.2.2 (c (n "af_packet") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1ncyji18h0nl6hcvpmppc9nxwidf8i69djqzs6bzp5fz5xz3b4hl")))

(define-public crate-af_packet-0.2.3 (c (n "af_packet") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "05m2s6am39j919dh3mxfybp043n72cfhif1v52hihhn3xlzzqd6g")))

(define-public crate-af_packet-0.2.5 (c (n "af_packet") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "09abw0nx21fkpzjklfwxbidj9826yr67lhx2a1dlic4ivgddz1v3")))

(define-public crate-af_packet-0.2.6 (c (n "af_packet") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0knixj74vslas82mmdi7zjv92wjl620yhfxhaffjwb17zh5kgzc7")))

(define-public crate-af_packet-0.2.7 (c (n "af_packet") (v "0.2.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0dwd0f5m9fm0n7pd4lrycwmj2xzdlya6s1bhr7psg5d1x1njq2v9")))

(define-public crate-af_packet-0.3.0 (c (n "af_packet") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1fwnj1nqhjg33l30sa8if4zc21l3a0d2hk21i7b1vz1rlx89m9hv")))

(define-public crate-af_packet-0.3.1 (c (n "af_packet") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)))) (h "10z298qd1iw3w7w87d179s2zh9rgjyz7yplkrng7a3fvnl6cxffn")))

