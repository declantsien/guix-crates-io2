(define-module (crates-io af l- afl-plugin) #:use-module (crates-io))

(define-public crate-afl-plugin-0.1.0 (c (n "afl-plugin") (v "0.1.0") (h "1iggk5zz1q99f7pn6w813xk7g26s88zds2sh6sry0g8188ap2mjp") (y #t)))

(define-public crate-afl-plugin-0.1.1 (c (n "afl-plugin") (v "0.1.1") (h "0hfyfn06mjxsfivyd1v7r0w5fva76kqji6wkprycprn5dfnizakm") (y #t)))

(define-public crate-afl-plugin-0.1.2 (c (n "afl-plugin") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "quale") (r "^1.0") (d #t) (k 1)))) (h "0sgygrz4843i28f98a6bnj00fl53wh4vly73kw0nzb6c5x4ryw4g") (y #t)))

(define-public crate-afl-plugin-0.1.3 (c (n "afl-plugin") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "quale") (r "^1.0") (d #t) (k 1)))) (h "16hpqlm4ychgjyqfb935v8mszp89bs4hyz8wfvsv72fnayv4jsgn") (y #t)))

(define-public crate-afl-plugin-0.1.4 (c (n "afl-plugin") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "quale") (r "^1.0") (d #t) (k 1)))) (h "04nd4kgx7r5i9c03zn4k7xwpklflmr0r9xpdzxkvikk9y7cdzvr3") (y #t)))

(define-public crate-afl-plugin-0.1.5 (c (n "afl-plugin") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "quale") (r "^1.0") (d #t) (k 1)))) (h "103ba3r1y285w7vmqfnsya5wdq2v8jlsc9wbrl6hbxsp8z9spkab") (y #t)))

