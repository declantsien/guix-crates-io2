(define-module (crates-io af l- afl-sys) #:use-module (crates-io))

(define-public crate-afl-sys-0.1.2 (c (n "afl-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18riri4194dd8pg373f67bkj3325x54687mb4kv5r47bpw7qrqy3") (y #t)))

(define-public crate-afl-sys-0.1.3 (c (n "afl-sys") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m17866lyyymfnwiiqagkkwiq9pl6109pq58lnznw3mqfzi7sabm") (y #t)))

(define-public crate-afl-sys-0.1.4 (c (n "afl-sys") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d21vyy51rnvn1azmm0y6ln1a84bcf8fdbl1jj5cvxp3qd2pvvvd") (y #t)))

(define-public crate-afl-sys-0.1.5 (c (n "afl-sys") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kipfjhbypflv0bbrvrccm0jan0jampl13d2f68pjxj1ymhcwpid") (y #t)))

