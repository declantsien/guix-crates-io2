(define-module (crates-io cm dl cmdline_words_parser) #:use-module (crates-io))

(define-public crate-cmdline_words_parser-0.0.2 (c (n "cmdline_words_parser") (v "0.0.2") (h "0wrwmi3s9cmxmks4wgmkgdflv2pz0kxyqrni7kaxrg39i1ci0srl") (f (quote (("no_std"))))))

(define-public crate-cmdline_words_parser-0.1.0 (c (n "cmdline_words_parser") (v "0.1.0") (h "1sh944s6cjjs6cdz0v8rzgipznim5xbv9lp54f26y2jsnzcx90ig") (f (quote (("no_std"))))))

(define-public crate-cmdline_words_parser-0.2.0 (c (n "cmdline_words_parser") (v "0.2.0") (h "1qxlgbs4kn66m6bv4n7wxl0749zyyvdlnzh0jc80738ma14br7dg") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-cmdline_words_parser-0.2.1 (c (n "cmdline_words_parser") (v "0.2.1") (h "0adxna3j9yycy3jjr204aa4lmsl0qr4fz89lppc77xns0f7hgn3m") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

