(define-module (crates-io cm d2 cmd2zip) #:use-module (crates-io))

(define-public crate-cmd2zip-1.0.0 (c (n "cmd2zip") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "wild") (r "^2.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0zxybkmvbslv3z9884vmqsiyvr5l16y0j2qj0g3nnhk53zjc862d")))

(define-public crate-cmd2zip-1.1.0 (c (n "cmd2zip") (v "1.1.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "wild") (r "^2.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1qw3ifi5rnsi3mz5lkdkpls369yxdwihf01skavx3yk21cc72182")))

