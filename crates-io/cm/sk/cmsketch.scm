(define-module (crates-io cm sk cmsketch) #:use-module (crates-io))

(define-public crate-cmsketch-0.1.0 (c (n "cmsketch") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand_mt") (r "^4.2.1") (d #t) (k 2)))) (h "1x5avf1d8zs01nlqpyxdyp4vw1kk0f1mcphlibm99na2g8fg101l")))

(define-public crate-cmsketch-0.1.1 (c (n "cmsketch") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand_mt") (r "^4.2.1") (d #t) (k 2)))) (h "17l80j4s0hqh3rsa8cbyjz8hdavcmza6bgwbiq4hwhjy7i2m3js9")))

(define-public crate-cmsketch-0.1.2 (c (n "cmsketch") (v "0.1.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand_mt") (r "^4.2.1") (d #t) (k 2)))) (h "1vs10a7vmj7v9vpiza5yiyc19szhgnazgg01q9kngd4z4ss3mj2j")))

(define-public crate-cmsketch-0.1.3 (c (n "cmsketch") (v "0.1.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand_mt") (r "^4.2.1") (d #t) (k 2)))) (h "16ar5r68y5fkkp4j5m3a95dm5nv3kg0ysqs3px4j07sj1pqw66h4")))

(define-public crate-cmsketch-0.1.4 (c (n "cmsketch") (v "0.1.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand_mt") (r "^4.2.1") (d #t) (k 2)))) (h "1w62djvj6z1p7xnbs8r00iqdcnlw9r9wshzbkazm6578hw2lczj6")))

(define-public crate-cmsketch-0.1.5 (c (n "cmsketch") (v "0.1.5") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand_mt") (r "^4.2.1") (d #t) (k 2)))) (h "03nn4sy06l9l177hcn7542yz7flxyzwkcymi18jyldvwp2c0awck")))

(define-public crate-cmsketch-0.2.0 (c (n "cmsketch") (v "0.2.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand_mt") (r "^4.2.1") (d #t) (k 2)))) (h "1zvg5gzwhp2ijy4jwwqn4w806qh9c0fvn3wnhnmacifabm6lpa8p")))

