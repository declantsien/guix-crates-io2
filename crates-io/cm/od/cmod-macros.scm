(define-module (crates-io cm od cmod-macros) #:use-module (crates-io))

(define-public crate-cmod-macros-0.0.1 (c (n "cmod-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "062lmzkkc18f438n21sny38xnmzpiikdq2zqg57j4zxgn6pkgs1v") (f (quote (("python") ("lua"))))))

(define-public crate-cmod-macros-0.0.2 (c (n "cmod-macros") (v "0.0.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0nd5grrpis2gh6v8if5piwx19hap46c03vcbrc8y5wyzshsl0rfw") (f (quote (("ffi_py") ("ffi_lua"))))))

(define-public crate-cmod-macros-0.1.0 (c (n "cmod-macros") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0cdwz2pjzvdh61z9kax9mhyvkjwv45zfrs4z3ickyjimpd2ydn7f") (f (quote (("ffi_wasm") ("ffi_py") ("ffi_lua"))))))

(define-public crate-cmod-macros-0.1.3 (c (n "cmod-macros") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0kpbhhd956qhl5kbrm4vmdf9kc033l6nr285axkjaixyhkzm4v1s") (f (quote (("ffi_wasm") ("ffi_py_asyncio" "ffi_py") ("ffi_py") ("ffi_lua"))))))

(define-public crate-cmod-macros-0.1.4 (c (n "cmod-macros") (v "0.1.4") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0qghdw5ni29nxn1y978l434d1gknr8jxgvg532viqdf5xgi6ldjl") (f (quote (("ffi_wasm") ("ffi_py_asyncio" "ffi_py") ("ffi_py") ("ffi_lua"))))))

(define-public crate-cmod-macros-0.2.1 (c (n "cmod-macros") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1flxpms8nxm9y9v737hdqiijqvs38bnwbqj8zhzk9iknvfixqhza") (f (quote (("ffi_wasm") ("ffi_py_asyncio" "ffi_py") ("ffi_py") ("ffi_lua"))))))

(define-public crate-cmod-macros-0.3.0 (c (n "cmod-macros") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "cmod-core") (r "^0") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "06rv6wbcvd90k6kxpap0whbkc8nwd0rn4xgssv31n5ahncs17a60") (f (quote (("ffi_wasm" "cmod-core/ffi_wasm") ("ffi_py_asyncio" "ffi_py" "cmod-core/ffi_py_asyncio") ("ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-core/ffi_lua"))))))

(define-public crate-cmod-macros-0.3.1 (c (n "cmod-macros") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0xpdqdqibnh6n059mqvj3g79798la1hlq6hc2rdkwyck9baqcq8w") (f (quote (("ffi_wasm") ("ffi_py_asyncio" "ffi_py") ("ffi_py") ("ffi_lua"))))))

(define-public crate-cmod-macros-0.3.2 (c (n "cmod-macros") (v "0.3.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1aaa020nda84j2yw18xqsgpvhpib84ddk0c6x6pdlxmx2wjjjqqv") (f (quote (("ffi_wasm") ("ffi_py_asyncio" "ffi_py") ("ffi_py") ("ffi_lua"))))))

(define-public crate-cmod-macros-0.3.5 (c (n "cmod-macros") (v "0.3.5") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0m0bny6xldvjd613kzknv3jqnkzcaxx9q4lybp661r2nd45czcxw") (f (quote (("ffi_wasm") ("ffi_py_asyncio" "ffi_py") ("ffi_py") ("ffi_lua"))))))

