(define-module (crates-io cm od cmod) #:use-module (crates-io))

(define-public crate-cmod-0.0.1 (c (n "cmod") (v "0.0.1") (d (list (d (n "cmod-core") (r "^0") (d #t) (k 0)) (d (n "cmod-macros") (r "^0") (d #t) (k 0)))) (h "060zsp2c0g7yn9zaalax2klkqw8s3ykrq63vxhkrlzav2a6j1z4b")))

(define-public crate-cmod-0.0.2 (c (n "cmod") (v "0.0.2") (d (list (d (n "cmod-core") (r "^0") (d #t) (k 0)) (d (n "cmod-macros") (r "^0") (d #t) (k 0)))) (h "191kqxsxwis9gcy2pncad1sdl1hqfb1ppzdbb7yz5hi4ixxzl0j6") (f (quote (("ffi_py" "cmod-macros/ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-macros/ffi_lua" "cmod-core/ffi_lua") ("default"))))))

(define-public crate-cmod-0.1.0 (c (n "cmod") (v "0.1.0") (d (list (d (n "cmod-core") (r "^0") (d #t) (k 0)) (d (n "cmod-macros") (r "^0") (d #t) (k 0)))) (h "0ix9lg8phr35mn7f7dxd1qks20j0s4n6babrdn0ahfrv3zl872wy") (f (quote (("ffi_wasm" "cmod-macros/ffi_wasm" "cmod-core/ffi_wasm") ("ffi_py" "cmod-macros/ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-macros/ffi_lua" "cmod-core/ffi_lua") ("default"))))))

(define-public crate-cmod-0.1.3 (c (n "cmod") (v "0.1.3") (d (list (d (n "cmod-core") (r "^0") (d #t) (k 0)) (d (n "cmod-macros") (r "^0") (d #t) (k 0)))) (h "1h6gkhn4a6jz1h54s959c60qswimwklnsn9hfylsc6wscxq4vspg") (f (quote (("ffi_wasm" "cmod-macros/ffi_wasm" "cmod-core/ffi_wasm") ("ffi_py_asyncio" "cmod-macros/ffi_py_asyncio" "cmod-core/ffi_py_asyncio") ("ffi_py" "cmod-macros/ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-macros/ffi_lua" "cmod-core/ffi_lua") ("default"))))))

(define-public crate-cmod-0.1.4 (c (n "cmod") (v "0.1.4") (d (list (d (n "cmod-core") (r "^0.1") (d #t) (k 0)) (d (n "cmod-macros") (r "^0.1") (d #t) (k 0)))) (h "1a0ifpbjiy8jg1nf3jkl0j5n2ma1ji1by8x5dacwa1dpl19dvcxg") (f (quote (("ffi_wasm" "cmod-macros/ffi_wasm" "cmod-core/ffi_wasm") ("ffi_py_asyncio" "cmod-macros/ffi_py_asyncio" "cmod-core/ffi_py_asyncio") ("ffi_py" "cmod-macros/ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-macros/ffi_lua" "cmod-core/ffi_lua") ("default"))))))

(define-public crate-cmod-0.2.1 (c (n "cmod") (v "0.2.1") (d (list (d (n "cmod-core") (r "^0.2") (d #t) (k 0)) (d (n "cmod-macros") (r "^0.2") (d #t) (k 0)))) (h "05ls7in7sv6v032nxwcx0g70zycmiwxi5pvfsbi624xp1s2x3ibb") (f (quote (("ffi_wasm" "cmod-macros/ffi_wasm" "cmod-core/ffi_wasm") ("ffi_py_asyncio" "cmod-macros/ffi_py_asyncio" "cmod-core/ffi_py_asyncio") ("ffi_py" "cmod-macros/ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-macros/ffi_lua" "cmod-core/ffi_lua") ("default"))))))

(define-public crate-cmod-0.3.0 (c (n "cmod") (v "0.3.0") (d (list (d (n "cmod-core") (r "^0") (k 0)) (d (n "cmod-macros") (r "^0") (k 0)))) (h "0gh4bbrrnvh05829lwa6v96xzx6qr0dr0fgj48iyyb4d7hk4ik1q") (f (quote (("ffi_wasm" "cmod-macros/ffi_wasm" "cmod-core/ffi_wasm") ("ffi_py_asyncio" "cmod-macros/ffi_py_asyncio" "cmod-core/ffi_py_asyncio") ("ffi_py" "cmod-macros/ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-macros/ffi_lua" "cmod-core/ffi_lua") ("default"))))))

(define-public crate-cmod-0.3.1 (c (n "cmod") (v "0.3.1") (d (list (d (n "cmod-core") (r "^0") (k 0)) (d (n "cmod-macros") (r "^0") (k 0)))) (h "1p07wp6wqady0ry8qk31pxwakmxa99i7iixa3nn6achya46w3i1n") (f (quote (("ffi_wasm" "cmod-macros/ffi_wasm" "cmod-core/ffi_wasm") ("ffi_py_asyncio" "cmod-macros/ffi_py_asyncio" "cmod-core/ffi_py_asyncio") ("ffi_py" "cmod-macros/ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-macros/ffi_lua" "cmod-core/ffi_lua") ("default"))))))

(define-public crate-cmod-0.3.2 (c (n "cmod") (v "0.3.2") (d (list (d (n "cmod-core") (r "^0") (k 0)) (d (n "cmod-macros") (r "^0") (k 0)))) (h "1jg1k4f00zdyjdfhdiffhhhxagyka1qds23z8sjqxhfdkc4va9a2") (f (quote (("ffi_wasm" "cmod-macros/ffi_wasm" "cmod-core/ffi_wasm") ("ffi_py_asyncio" "cmod-macros/ffi_py_asyncio" "cmod-core/ffi_py_asyncio") ("ffi_py" "cmod-macros/ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-macros/ffi_lua" "cmod-core/ffi_lua") ("default"))))))

(define-public crate-cmod-0.3.5 (c (n "cmod") (v "0.3.5") (d (list (d (n "cmod-core") (r "^0") (k 0)) (d (n "cmod-macros") (r "^0") (k 0)))) (h "098hdd87m003s6283y17xqgpjwjz73g8278drn67aikzrayh392p") (f (quote (("ffi_wasm" "cmod-macros/ffi_wasm" "cmod-core/ffi_wasm") ("ffi_py_asyncio" "cmod-macros/ffi_py_asyncio" "cmod-core/ffi_py_asyncio") ("ffi_py" "cmod-macros/ffi_py" "cmod-core/ffi_py") ("ffi_lua" "cmod-macros/ffi_lua" "cmod-core/ffi_lua") ("default"))))))

