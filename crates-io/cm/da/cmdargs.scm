(define-module (crates-io cm da cmdargs) #:use-module (crates-io))

(define-public crate-cmdargs-0.1.0 (c (n "cmdargs") (v "0.1.0") (d (list (d (n "cmdargs-macros") (r "^0.1.0") (d #t) (k 0)))) (h "008gywhj8pqjd4w33xwllc50vjjgp92r6hgk90crrvwpgdkw9hn8")))

(define-public crate-cmdargs-0.1.1 (c (n "cmdargs") (v "0.1.1") (d (list (d (n "cmdargs-macros") (r "^0.1.1") (d #t) (k 0)))) (h "10jxf0a0ak86msyvhl5cn15k7znl96bhbj1hck4adislp6px8879")))

