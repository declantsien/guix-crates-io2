(define-module (crates-io cm os cmos) #:use-module (crates-io))

(define-public crate-cmos-0.1.0 (c (n "cmos") (v "0.1.0") (d (list (d (n "cpuio") (r "^0.2.0") (d #t) (k 0)))) (h "1m9gj7kbrvhq6qdq1zi6jbzs2hddsiphni3wkkacrvvkpn43zb7g")))

(define-public crate-cmos-0.1.1 (c (n "cmos") (v "0.1.1") (d (list (d (n "cpuio") (r "^0.2.0") (d #t) (k 0)))) (h "15a0lsswn4hc74savchfbp35y2n4jabda68d8sk2xqa4aslpflqa")))

(define-public crate-cmos-0.1.2 (c (n "cmos") (v "0.1.2") (d (list (d (n "cpuio") (r "^0.3") (d #t) (k 0)))) (h "1mcwaghvgml42x721w56gcpkvqisrhbjydm57riw3jk52qi0p7z5")))

