(define-module (crates-io cm os cmos-rtc) #:use-module (crates-io))

(define-public crate-cmos-rtc-0.1.0 (c (n "cmos-rtc") (v "0.1.0") (d (list (d (n "x86_64") (r "^0.13.0") (d #t) (k 0)))) (h "1mvlf6z9agsh8svcp12l2d4hfp90klf2vdlls61pf2iahb8fcrrn")))

(define-public crate-cmos-rtc-0.1.1 (c (n "cmos-rtc") (v "0.1.1") (d (list (d (n "x86_64") (r "^0.14.10") (d #t) (k 0)))) (h "107cyylllwmzrfj958fh28qa0l50qgvifz0rs81q5zcdw3k3cn4y")))

(define-public crate-cmos-rtc-0.1.2 (c (n "cmos-rtc") (v "0.1.2") (d (list (d (n "x86_64") (r "^0.14.10") (d #t) (k 0)))) (h "0wmmp3azzj5y68869mgby8ggk7jqg0i5xxr1rqh793qhpbc0klgx")))

