(define-module (crates-io cm d_ cmd_cachier) #:use-module (crates-io))

(define-public crate-cmd_cachier-0.2.0 (c (n "cmd_cachier") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "0p14rpr7l6vjzkk0xa07wvn85s2ps5y1k1b64fmvf4r5n3mlh62i") (y #t)))

(define-public crate-cmd_cachier-0.2.1 (c (n "cmd_cachier") (v "0.2.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "1yz5r64dxjr6jd6br3rrl6jkar6yiaqg7qa3vd5kqmb2rysg7iq6") (y #t)))

(define-public crate-cmd_cachier-0.2.2 (c (n "cmd_cachier") (v "0.2.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "1p2vzsi5afqvg11pj68afg0qbvdn7wg74lc7jnnv0ajwqcda7qvd") (y #t)))

(define-public crate-cmd_cachier-0.2.3 (c (n "cmd_cachier") (v "0.2.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "10nmcfgabjniyrjxza9a6lf3ywnx6cjrqs22cw2l30zkfrgpnzp4") (y #t)))

(define-public crate-cmd_cachier-0.2.4 (c (n "cmd_cachier") (v "0.2.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "1qpyj16x05y20g0gffgy0fr69fdpz6kcqh4rdfrlrv0z9adzch5l") (y #t)))

(define-public crate-cmd_cachier-0.2.5 (c (n "cmd_cachier") (v "0.2.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "111yayvjd8azjpqv7ryydi80wi7v31v2czpla7lqbv6sgyq79i1n") (y #t)))

(define-public crate-cmd_cachier-0.2.6 (c (n "cmd_cachier") (v "0.2.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "0zffv6ar3ccf03wjipwb70pdfhgb5aizr3d3rxx7wkfk42m7gp1y") (y #t)))

(define-public crate-cmd_cachier-0.2.7 (c (n "cmd_cachier") (v "0.2.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "1q18xz06fb2l7gcp3gdlp0rk87hz5f205dm0clriwlbii8d19ca1")))

(define-public crate-cmd_cachier-0.3.0 (c (n "cmd_cachier") (v "0.3.0") (d (list (d (n "colored") (r "=2.0") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "1k8b3s2rm6ajidv8abyqvyz75gvh0948bh420zi196q4niyx3001")))

(define-public crate-cmd_cachier-0.3.1 (c (n "cmd_cachier") (v "0.3.1") (d (list (d (n "colored") (r "=2.0") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "1f2sdfbjwajqsiiny244v25s05pr95js8p8bb77krzp8m00x8zdd")))

(define-public crate-cmd_cachier-0.3.2 (c (n "cmd_cachier") (v "0.3.2") (d (list (d (n "colored") (r "=2.0") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "0w29x7yxzdygd758n3g263mrfx2qjnw7bd2898b867vp4yhh7i0z")))

(define-public crate-cmd_cachier-0.3.3 (c (n "cmd_cachier") (v "0.3.3") (d (list (d (n "colored") (r "=2.0") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "09vhqqbwaridfywdlfvlwqda2jw9r6k778chzs7aakwi3w7sjl5d") (y #t)))

(define-public crate-cmd_cachier-0.3.4 (c (n "cmd_cachier") (v "0.3.4") (d (list (d (n "colored") (r "=2.0") (d #t) (k 0)) (d (n "redis") (r "^0.23") (d #t) (k 0)))) (h "1ndj8fjc0zwhhkpriibzhcxmwxmf30kikkggbrnbx8zmywj15z82")))

