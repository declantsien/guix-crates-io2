(define-module (crates-io cm d_ cmd_wrk_parser) #:use-module (crates-io))

(define-public crate-cmd_wrk_parser-0.0.1 (c (n "cmd_wrk_parser") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "1vhyxp72xyf0pvb6b4a2z58nz5bwfpwn9g6msrmhh0cc2p67lsxz") (f (quote (("std-parsers" "regex") ("primitive-parsers") ("default" "bundle-primitive" "bundle-std") ("bundle-std" "std-parsers") ("bundle-primitive" "primitive-parsers"))))))

