(define-module (crates-io cm d_ cmd_lib_cf_macros) #:use-module (crates-io))

(define-public crate-cmd_lib_cf_macros-1.3.1 (c (n "cmd_lib_cf_macros") (v "1.3.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04qdm03qdr6ch8qax4n9wi9v67r0vrbfwggvgr4116drji1qq5fg")))

