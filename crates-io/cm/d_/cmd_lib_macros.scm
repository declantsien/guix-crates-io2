(define-module (crates-io cm d_ cmd_lib_macros) #:use-module (crates-io))

(define-public crate-cmd_lib_macros-0.1.0 (c (n "cmd_lib_macros") (v "0.1.0") (d (list (d (n "cmd_lib_core") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1ck2bggr444mr5h9sv0chywj3ycc4jlc0f4im25r5n18zh62n81d")))

(define-public crate-cmd_lib_macros-0.2.0 (c (n "cmd_lib_macros") (v "0.2.0") (d (list (d (n "cmd_lib_core") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pcsspfj85050x32gicwhyxd3v7vxz4fiamjz25x4gwjwmchzsj6")))

(define-public crate-cmd_lib_macros-0.3.0 (c (n "cmd_lib_macros") (v "0.3.0") (d (list (d (n "cmd_lib_core") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "016cdq4m8fd4fv3nq9a3gk6d6nccizm3h9ynn2rxbz9j9wp4y9vc")))

(define-public crate-cmd_lib_macros-0.4.0 (c (n "cmd_lib_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bkz7s7gxgg3kkpyj69j26xn9xxnyxyh8yic9dpmyl7s9brkkfgh")))

(define-public crate-cmd_lib_macros-0.5.0 (c (n "cmd_lib_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s0si5r0la1n35kyrqqi7f0n7xf46ivpi08l37wiv2hj31g30zj2")))

(define-public crate-cmd_lib_macros-0.6.0 (c (n "cmd_lib_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fg2vicvx1lj7wv3fqnchv8hqk0c62iq5nnzmffcnngjpg0fiql8")))

(define-public crate-cmd_lib_macros-0.7.0 (c (n "cmd_lib_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13c6bad1ybavd5nw7zv1lxgr080mibcv79zpwq7mjj29khkwwhal")))

(define-public crate-cmd_lib_macros-0.8.0 (c (n "cmd_lib_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "134hmd9kjcgr75s74wddm72bf5k4d7729wx4qd10ryyj12c2mn9n")))

(define-public crate-cmd_lib_macros-0.9.0 (c (n "cmd_lib_macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cxm0ip9jyjl6rfp5a3pafmi6d9g4r2rgbpz9k68xpn0kwnbahbd")))

(define-public crate-cmd_lib_macros-0.10.0 (c (n "cmd_lib_macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c7flahrbbg5567g4dy42axfffq9gym817n4rlbkf4a878xapqyq")))

(define-public crate-cmd_lib_macros-0.11.0 (c (n "cmd_lib_macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y5p5kaxcj13wqs6yilrzd75jpp6n0y4mnflh09krhs16jgb7qyr")))

(define-public crate-cmd_lib_macros-0.11.4 (c (n "cmd_lib_macros") (v "0.11.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s92bm6shjg8173cdbcpcsyfrd2g332dgss44pwmds52pp9ridmy")))

(define-public crate-cmd_lib_macros-0.11.5 (c (n "cmd_lib_macros") (v "0.11.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19nj74jxyn5l436pfxad7c47yyxp9mf3whx89ghb18hmgszz271m")))

(define-public crate-cmd_lib_macros-0.11.6 (c (n "cmd_lib_macros") (v "0.11.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nds5vrngy5qynmwdp5w4mmjbg2nqxy41bcizj5s2qz9msjq63zr")))

(define-public crate-cmd_lib_macros-0.12.0-rc1 (c (n "cmd_lib_macros") (v "0.12.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13xzwyn5i9cq94wsn197b8jgs6xc22d79ixb0vncvs7sjprm4vl4")))

(define-public crate-cmd_lib_macros-0.12.0 (c (n "cmd_lib_macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fxj0dpzpkzsplcaxj3a2dq2i8q2zk6vh3inr6ix5s5m4kbnzg0d")))

(define-public crate-cmd_lib_macros-0.12.1 (c (n "cmd_lib_macros") (v "0.12.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bbxs41hr9fci0hxbmvyhlv2zl8xfvmhm06x0whhn20m0w4z3zld")))

(define-public crate-cmd_lib_macros-0.12.2 (c (n "cmd_lib_macros") (v "0.12.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h3dwrxcan145mg4lp8n19vi08p4x6d4s7558c9rp6am2hvkdqkc")))

(define-public crate-cmd_lib_macros-0.12.3 (c (n "cmd_lib_macros") (v "0.12.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05nbnaswqlyckph42an4c0r2mr2f1mkfz8d5jcc4qs3f3d8s405z")))

(define-public crate-cmd_lib_macros-0.12.4 (c (n "cmd_lib_macros") (v "0.12.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "182fvs8dfwv19i2b9rm1dx1rh16pcriism73dymybkqgjk13i4zs")))

(define-public crate-cmd_lib_macros-0.12.5 (c (n "cmd_lib_macros") (v "0.12.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a7zrlfa7b5ai8l5kmcnqxh90zrd1mgiagn1k18kqjmcxl8xndyh")))

(define-public crate-cmd_lib_macros-0.12.6 (c (n "cmd_lib_macros") (v "0.12.6") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xyqjab84g5svjch5ppmgmcmkfq6bm70cxdhy57sb4xlq3b5rnfj")))

(define-public crate-cmd_lib_macros-0.14.0 (c (n "cmd_lib_macros") (v "0.14.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fl8a73v4r4xlwvir9wp398ildvzkz4i98vbcf9bn6qm9jjax636")))

(define-public crate-cmd_lib_macros-0.14.1 (c (n "cmd_lib_macros") (v "0.14.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "058qi3fa1v0fbbpic9dh4h6gm07v2n4wyzwrn0pq1w18nf93gp3g")))

(define-public crate-cmd_lib_macros-0.14.2 (c (n "cmd_lib_macros") (v "0.14.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ydx9zp2vacq6w5bn132yi7yawy1s93hrkyb718nh5ky2c79jm6l")))

(define-public crate-cmd_lib_macros-0.14.3 (c (n "cmd_lib_macros") (v "0.14.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ygpc9b120nrcnmsd16bhvmgwnbz8q62w9bvqb77kk7r9bffnc66")))

(define-public crate-cmd_lib_macros-0.14.4 (c (n "cmd_lib_macros") (v "0.14.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wrnsc6ly3h2jnjyjqhg5yh7q1pm5i7c9gsz6kyy1cibnglbyd9m")))

(define-public crate-cmd_lib_macros-0.14.5 (c (n "cmd_lib_macros") (v "0.14.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "130h8wmvbz2d4v36j5gl9mc3p44ln1h78p7kv00nwhx9267x3cw6")))

(define-public crate-cmd_lib_macros-0.14.6 (c (n "cmd_lib_macros") (v "0.14.6") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ci04cxdwwwb66q7hqaramqi5mjqp7msh5scqcdwsm2n69dgnf83")))

(define-public crate-cmd_lib_macros-0.15.0 (c (n "cmd_lib_macros") (v "0.15.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cafnl5hax0d6gj9pvcd3s1pvfs9w03gdh2i6inpbdcbksyzq7fq")))

(define-public crate-cmd_lib_macros-0.15.1 (c (n "cmd_lib_macros") (v "0.15.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z1gpwwgy4hrk0v0d9qp21sl01ashbbp9ns0rvh60673565lql9c")))

(define-public crate-cmd_lib_macros-1.0.0 (c (n "cmd_lib_macros") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08bcl6wwqpp0zz9akdm7170s9wa4izb2x5kn7d1s2q8x209vm2m7")))

(define-public crate-cmd_lib_macros-1.0.1 (c (n "cmd_lib_macros") (v "1.0.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jmdgx0sxn8ni22nygkcwmwnxbpvw8r5lgjpa0kr1fgkgjc8q2zz")))

(define-public crate-cmd_lib_macros-1.0.2 (c (n "cmd_lib_macros") (v "1.0.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07f28049scx5w9xpz400grw3axzvhyyz6pqfqnhibjmq7flj7axc")))

(define-public crate-cmd_lib_macros-1.0.3 (c (n "cmd_lib_macros") (v "1.0.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fibhfx8idc4kg46z022b4izhzcxp79j9qcx21drxxv06c034xmm")))

(define-public crate-cmd_lib_macros-1.0.4 (c (n "cmd_lib_macros") (v "1.0.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wby0yb0sm66jqwylyaw1w1xnamqhwsrqiyp8i24afblgh37w0jg")))

(define-public crate-cmd_lib_macros-1.0.5 (c (n "cmd_lib_macros") (v "1.0.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1icp9ddfag2xradvdn8h1sbsib7p1i5n24c5njis9j51b2wpbh1k")))

(define-public crate-cmd_lib_macros-1.0.6 (c (n "cmd_lib_macros") (v "1.0.6") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xxj0jy819k5c152s1hc8i7ham5ikp2c7adxn553d0gj83yy3yh8")))

(define-public crate-cmd_lib_macros-1.0.7 (c (n "cmd_lib_macros") (v "1.0.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a78af41di2mfgh6zbzx7iny3dghym0hq5bmjw9dzxmwg1bmbpd2")))

(define-public crate-cmd_lib_macros-1.0.8 (c (n "cmd_lib_macros") (v "1.0.8") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w2h0i9a16p5r4zg8y8rnnbphjl6pppn04gjjmzz1sa8336sjpm8")))

(define-public crate-cmd_lib_macros-1.0.9 (c (n "cmd_lib_macros") (v "1.0.9") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ldqmxi3yya8avbadk8s70b3wga2rzyd15sra97pggjxz1i13fjd")))

(define-public crate-cmd_lib_macros-1.0.10 (c (n "cmd_lib_macros") (v "1.0.10") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fs333i9mpap1whlrm8xgmf9s9hmm0sxkv5scjmfykr86c9i3dmx")))

(define-public crate-cmd_lib_macros-1.0.11 (c (n "cmd_lib_macros") (v "1.0.11") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12l4jr5q3p4p9czr9z546r0q1fir2z551hgzss7rby4dpymqfw9s")))

(define-public crate-cmd_lib_macros-1.0.12 (c (n "cmd_lib_macros") (v "1.0.12") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jwzxcl6ncfhpp9dag1s5bvp0dbfjbfh9f6px1c0gdz74r19v9bb")))

(define-public crate-cmd_lib_macros-1.0.13 (c (n "cmd_lib_macros") (v "1.0.13") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ga57bz103bhvz34aga8f4r0879zpdis80d2jab3msx65glciiap")))

(define-public crate-cmd_lib_macros-1.1.0 (c (n "cmd_lib_macros") (v "1.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lwd1w4schvcvzm78lwar8ib2jj1l8gf1m16m0qffcx4x5adcvrm")))

(define-public crate-cmd_lib_macros-1.2.0 (c (n "cmd_lib_macros") (v "1.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n70jd1mv5g3farzw77pm7q5fqjnmmydbanv46d1475mww3hfm1d")))

(define-public crate-cmd_lib_macros-1.2.1 (c (n "cmd_lib_macros") (v "1.2.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s35fabrq6svlhk1j3j2107ziwaa6dgi0fjjsj2mdami98zkniqh")))

(define-public crate-cmd_lib_macros-1.2.2 (c (n "cmd_lib_macros") (v "1.2.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nlpkrxxihan9qgn0l6p8bggnzacqd03p5g6vlmfcscrxzhx8s9l")))

(define-public crate-cmd_lib_macros-1.2.3 (c (n "cmd_lib_macros") (v "1.2.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10l829c1468cmfrbjqpn84zdas384s05x7fn4rpm8vv2d436wcss")))

(define-public crate-cmd_lib_macros-1.2.4 (c (n "cmd_lib_macros") (v "1.2.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11jzxjxlwyq2x760mww1k9c5fjc5590i6hby8bkinyrj6qw4cy57")))

(define-public crate-cmd_lib_macros-1.3.0 (c (n "cmd_lib_macros") (v "1.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h1d25y4j0zknmcr6ng0h0cdqqizdjp02ri4w0vnwv7zj9860rly")))

(define-public crate-cmd_lib_macros-1.4.0 (c (n "cmd_lib_macros") (v "1.4.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07k4qyhg930gs0mv1mj9i363dqbh96vysmqz4y8cdyv723zk0bd3")))

(define-public crate-cmd_lib_macros-1.5.0 (c (n "cmd_lib_macros") (v "1.5.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b9y02vjw9379686r6rgmicfz72paxrryxdbl0fh5p0xn0fb3v0p")))

(define-public crate-cmd_lib_macros-1.6.0 (c (n "cmd_lib_macros") (v "1.6.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lly2ga0gpwpdwbqlv1cf1iidrmz7q1rc4dqpaz6x1rd3k0r70sl")))

(define-public crate-cmd_lib_macros-1.6.1 (c (n "cmd_lib_macros") (v "1.6.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1slliadv7gnpx8zj9svrl5113xjca2a0620kwhlpd521hpvgx69f")))

(define-public crate-cmd_lib_macros-1.8.0 (c (n "cmd_lib_macros") (v "1.8.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zgjdl5pvyvqy04ap74i468ff4vipya7jh8m48pw8nkb60ywspxc")))

(define-public crate-cmd_lib_macros-1.8.1 (c (n "cmd_lib_macros") (v "1.8.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "114qiyk1klmra7gzyshg1q4bi3bf6k103yp2wr5xzsgyjcw5lrch")))

(define-public crate-cmd_lib_macros-1.9.0 (c (n "cmd_lib_macros") (v "1.9.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ybv4lzvg4bgm7klwzcg17vi603yc1a9mxpwc8w1rd1373yvhysh")))

(define-public crate-cmd_lib_macros-1.9.1 (c (n "cmd_lib_macros") (v "1.9.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iicpxs6lq5jch12qn5v8fhdsipqs0gz9jf4mis7fpfxdf612kpl")))

(define-public crate-cmd_lib_macros-1.9.2 (c (n "cmd_lib_macros") (v "1.9.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5qp0amhdsv2ffv0h9cw1vnrcmr37mcl17n261gqll1dqvnky1g")))

(define-public crate-cmd_lib_macros-1.9.3 (c (n "cmd_lib_macros") (v "1.9.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1psz7yjg44c3j1gv9s6nl4qk0afgb2aw1cgh3vwhk972yxh1k25f")))

(define-public crate-cmd_lib_macros-1.9.4 (c (n "cmd_lib_macros") (v "1.9.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g3a0qnsqacy2z1h54769lzvb44v1j8xvpxmf1xfjbyibv0gm04s")))

