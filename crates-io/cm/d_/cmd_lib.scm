(define-module (crates-io cm d_ cmd_lib) #:use-module (crates-io))

(define-public crate-cmd_lib-0.1.0 (c (n "cmd_lib") (v "0.1.0") (h "0gv3dln5gsnkpv7cijszbnx8pyi4p24j1vd00v0fq4a7bjs2x786")))

(define-public crate-cmd_lib-0.1.1 (c (n "cmd_lib") (v "0.1.1") (h "0ig1i8av8xgnx4rg6fa9i8g10xzixw7nf92n45f0jfmws2jsa598")))

(define-public crate-cmd_lib-0.1.2 (c (n "cmd_lib") (v "0.1.2") (h "11i4x0qpdj5j9w79fss0sak84rzvz5mdc7gpyxszgg3xc9np4x7h")))

(define-public crate-cmd_lib-0.1.3 (c (n "cmd_lib") (v "0.1.3") (h "13pppw3y3pi7s6fwrpskrqjjjilkgwv34wyk289z21asvd57a253")))

(define-public crate-cmd_lib-0.1.4 (c (n "cmd_lib") (v "0.1.4") (h "0pif3y6m3j5r639c72ialg27ls6nhw8a3140xhwbfplc92krpa4r")))

(define-public crate-cmd_lib-0.1.5 (c (n "cmd_lib") (v "0.1.5") (h "0j65grld2f1h5c858s6yqjr0l3zmzqglv48ki96f4vjv94p2j7lr")))

(define-public crate-cmd_lib-0.2.0 (c (n "cmd_lib") (v "0.2.0") (h "16gqabm849i8m07kxqvv9gmyrd6ng8gzq3ychlj868wf84smvafv")))

(define-public crate-cmd_lib-0.2.1 (c (n "cmd_lib") (v "0.2.1") (h "0wmxc5ixcdzfafs4c8myyd98cadvvhyjfdpriirwkxk8gkf4vayl")))

(define-public crate-cmd_lib-0.2.2 (c (n "cmd_lib") (v "0.2.2") (h "00lqrqv12k0qqm54aznj6p919yvpszqfp5b7miyrdjq6f91rv45x")))

(define-public crate-cmd_lib-0.2.3 (c (n "cmd_lib") (v "0.2.3") (h "0q91b8ig8msbydb39x93wsl69ybrgysd2mv5ca0g7l819c0dxm9g")))

(define-public crate-cmd_lib-0.2.4 (c (n "cmd_lib") (v "0.2.4") (h "1d7s1a7kgp33szmskxfd843ihg97f3d0a424ii6acjn177y5m2dq")))

(define-public crate-cmd_lib-0.2.5 (c (n "cmd_lib") (v "0.2.5") (h "0v0knrs4hiqvapl3lmdg7gwkgmknv15qys25fl4gva71lkqdjl7x")))

(define-public crate-cmd_lib-0.2.6 (c (n "cmd_lib") (v "0.2.6") (h "0mb8h19zgh7iyk3m4msaln3f5rbp4njxab7i8x9kc2j93jnicqnp")))

(define-public crate-cmd_lib-0.3.0 (c (n "cmd_lib") (v "0.3.0") (h "0fhvkggbp07ggghaig5gnaknkmkly0l36l7z7y9z4qqx8prlqbjv")))

(define-public crate-cmd_lib-0.3.1 (c (n "cmd_lib") (v "0.3.1") (h "1hx5vvwr81sc7163r3jviip2k1cj6qm9x48rv93xz4hp6i772j09")))

(define-public crate-cmd_lib-0.3.2 (c (n "cmd_lib") (v "0.3.2") (h "1s5g5inhn86cajwp8iqnz8sxi1k1f3pja99rbxkkz2mbsak6zfd2")))

(define-public crate-cmd_lib-0.6.0 (c (n "cmd_lib") (v "0.6.0") (h "05bnxp8j49pnrcypzkl3j0badqk0mfpyjfy7b90m0z7snbmkimdv")))

(define-public crate-cmd_lib-0.6.4 (c (n "cmd_lib") (v "0.6.4") (h "17fqv873d9jdbx1h4dbpbrm2m83rqkxngp3xhf9xib2lv2xa5nhh")))

(define-public crate-cmd_lib-0.6.5 (c (n "cmd_lib") (v "0.6.5") (h "1fgajlvfh86g72y506j9hsrsdizmx51qr12sbhsl63pmxdphh88a")))

(define-public crate-cmd_lib-0.6.6 (c (n "cmd_lib") (v "0.6.6") (h "1xcrr1c410i1j47i1vw946ayi5q6g9j5aki27igvz9wc9rbr9ivc")))

(define-public crate-cmd_lib-0.6.7 (c (n "cmd_lib") (v "0.6.7") (h "1lmrfp4kh0mqp2s8ghhwkpf8njpj3wrgxb391nf7br7223k2196p")))

(define-public crate-cmd_lib-0.6.8 (c (n "cmd_lib") (v "0.6.8") (h "1zdnm7hxyypb5cg91jiqy434k00v9p5mcngmxc8ajbf3wa09nkky")))

(define-public crate-cmd_lib-0.6.9 (c (n "cmd_lib") (v "0.6.9") (h "1mcnd9n9hnii75ll85m8q7ja3ripr0lrh1vn32wd1xg6p1mshvqp")))

(define-public crate-cmd_lib-0.7.0 (c (n "cmd_lib") (v "0.7.0") (h "1l2dfyjyc46l5hblm66r47303idcak4dw9j6c7lq5zfba4p8i1hv")))

(define-public crate-cmd_lib-0.7.1 (c (n "cmd_lib") (v "0.7.1") (h "1jyblmnw0gwq05gpjmhacpk0f7m8lifk402zfk85wxhk2434l8p7")))

(define-public crate-cmd_lib-0.7.2 (c (n "cmd_lib") (v "0.7.2") (h "04icrcssdld3d153cpxmm5v231xy3val60fb9advcirq6k5lsv0m")))

(define-public crate-cmd_lib-0.7.3 (c (n "cmd_lib") (v "0.7.3") (h "11vnmnda3ak6crgsv84gyz97fha71gph8m1il97mzh47brmhh0w0")))

(define-public crate-cmd_lib-0.7.4 (c (n "cmd_lib") (v "0.7.4") (h "18b0n3gnkhcv90jv8gaypnv7h9590gs1m83g9hyfzf12yna0fb9g")))

(define-public crate-cmd_lib-0.7.5 (c (n "cmd_lib") (v "0.7.5") (h "0vvc5y1f0xar9r17n8429wfm6ivspljg2zhrxavxpqr64lzfql9c")))

(define-public crate-cmd_lib-0.7.6 (c (n "cmd_lib") (v "0.7.6") (h "0x30faha3qzmajxka5b2hi04p5vgl2cxxv6a4z2yfw50xrhvrsi8")))

(define-public crate-cmd_lib-0.7.7 (c (n "cmd_lib") (v "0.7.7") (h "1k31jdcb2mabrfw01mh4kggsmyaf7q6m1ici6i290bz27qpjl472")))

(define-public crate-cmd_lib-0.7.8 (c (n "cmd_lib") (v "0.7.8") (h "0x52v6068wdizxwvnck568wypfmwhbl4b5xmdf7yvd40fsmdfns7")))

(define-public crate-cmd_lib-0.8.0 (c (n "cmd_lib") (v "0.8.0") (h "0wwqs2x23c1f0jiid0q7wga5n17i0f8xfs3xgkvz8h19hcr50z54")))

(define-public crate-cmd_lib-0.8.1 (c (n "cmd_lib") (v "0.8.1") (h "0bwbp6sa1s1gv1y27fnb9dm25zwy63kl2wiymn13p9b5qr354jz3")))

(define-public crate-cmd_lib-0.8.2 (c (n "cmd_lib") (v "0.8.2") (h "1q8cfrd8qjwyjfnf498ph3f8v35rjqqpnvqzan11a949iqwvbzmr")))

(define-public crate-cmd_lib-0.8.3 (c (n "cmd_lib") (v "0.8.3") (h "1rmxbr3m51cn9f33dcwxqljl06phq6hh41kn8w6b3gd2sfcima9k")))

(define-public crate-cmd_lib-0.8.4 (c (n "cmd_lib") (v "0.8.4") (d (list (d (n "cmd_lib_core") (r "^0.1") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.1") (d #t) (k 0)))) (h "1dva2yda69q5kh83vac2fbdy60fnkn1xg4mcwhs6bjg7xg38g870")))

(define-public crate-cmd_lib-0.8.5 (c (n "cmd_lib") (v "0.8.5") (d (list (d (n "cmd_lib_core") (r "^0.1") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.1") (d #t) (k 0)))) (h "1n9bky7iskd1sskvrdbcxzhpcc5ijsx3yj5ldxs03x1h9mj3ni22")))

(define-public crate-cmd_lib-0.9.0 (c (n "cmd_lib") (v "0.9.0") (d (list (d (n "cmd_lib_core") (r "^0.2") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.2") (d #t) (k 0)))) (h "176kkpczhrxlj3dgqqhrsy6nfgqf2xc86x4p3fh028rcw9p241cl")))

(define-public crate-cmd_lib-0.9.1 (c (n "cmd_lib") (v "0.9.1") (d (list (d (n "cmd_lib_core") (r "^0.2") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.2") (d #t) (k 0)))) (h "13s7wxw41kf3rb0y8cdqcdc5qzm4s6m7gj377ilbj6k77jhkf65g")))

(define-public crate-cmd_lib-0.9.2 (c (n "cmd_lib") (v "0.9.2") (d (list (d (n "cmd_lib_core") (r "^0.2") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.2") (d #t) (k 0)))) (h "1p9yc3k8p0n3dm7ppihza0l03s3sqgsgry9nmrfxrcx1r6l6a7vr")))

(define-public crate-cmd_lib-0.9.3 (c (n "cmd_lib") (v "0.9.3") (d (list (d (n "cmd_lib_core") (r "^0.2") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.2") (d #t) (k 0)))) (h "0l9vyrdc8dm90xj7akfr9vi92ldinfjr7l0y55iwzy787x4q3cms")))

(define-public crate-cmd_lib-0.9.4 (c (n "cmd_lib") (v "0.9.4") (d (list (d (n "cmd_lib_core") (r "^0.2") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.2") (d #t) (k 0)))) (h "0mi2qg89qwqcasys6v5g4m1r6cak5pcb6ncf88ds7s3xydinpjwx")))

(define-public crate-cmd_lib-0.9.5 (c (n "cmd_lib") (v "0.9.5") (d (list (d (n "cmd_lib_core") (r "^0.2") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.2") (d #t) (k 0)))) (h "1g30zgss6rlh8ijaiqm49xswph352ir7v3gqvrzdkijsqg4xc73s")))

(define-public crate-cmd_lib-0.9.6 (c (n "cmd_lib") (v "0.9.6") (d (list (d (n "cmd_lib_core") (r "^0.3") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.3") (d #t) (k 0)))) (h "0nks8jjia3pfbi89x3hlrhrg947n2vs6ymk7c2hz9xr0lmhlcgsw")))

(define-public crate-cmd_lib-0.9.7 (c (n "cmd_lib") (v "0.9.7") (d (list (d (n "cmd_lib_core") (r "^0.4") (d #t) (k 0)) (d (n "cmd_lib_macros") (r "^0.4") (d #t) (k 0)))) (h "02mdw0j8kfl6ys20pfi5z2rvb0zgfphnf4ckgr63z3p7ndrahl8d")))

(define-public crate-cmd_lib-0.10.0 (c (n "cmd_lib") (v "0.10.0") (d (list (d (n "cmd_lib_macros") (r "^0.5") (d #t) (k 0)))) (h "1jazwmdczx4k6fl66mdc7y9f639na11x47x872ccd6hwgqsxv6rn")))

(define-public crate-cmd_lib-0.10.1 (c (n "cmd_lib") (v "0.10.1") (d (list (d (n "cmd_lib_macros") (r "^0.6") (d #t) (k 0)))) (h "1wv5s18fzmb7f319hrhcng8rfv9qvhgmwxgjb4dv8hif0lhaff72")))

(define-public crate-cmd_lib-0.10.2 (c (n "cmd_lib") (v "0.10.2") (d (list (d (n "cmd_lib_macros") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "00g8ac4msdpjk7y4vm3ki7iv874saldyfmb7qzdpammvl0li83rf")))

(define-public crate-cmd_lib-0.10.3 (c (n "cmd_lib") (v "0.10.3") (d (list (d (n "cmd_lib_macros") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0awlhm7kngz243hnl25r72i6zm8w9162lim195wyjavdim7kgkmp")))

(define-public crate-cmd_lib-0.10.4 (c (n "cmd_lib") (v "0.10.4") (d (list (d (n "cmd_lib_macros") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hlq7civpjx2yxsdvj18rj6b1sz6bw2r0gysdnaqval15aqkwsmz")))

(define-public crate-cmd_lib-0.10.5 (c (n "cmd_lib") (v "0.10.5") (d (list (d (n "cmd_lib_macros") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1scaqvnhwhfwbdwrn3ixw968mdgkbw7055njbv8agl3ahf1zid76")))

(define-public crate-cmd_lib-0.11.0 (c (n "cmd_lib") (v "0.11.0") (d (list (d (n "cmd_lib_macros") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0l8hwhx7k24ywc98fv2q1ccnkbfljvppsjnavpbrzgihksi1yhfd")))

(define-public crate-cmd_lib-0.11.1 (c (n "cmd_lib") (v "0.11.1") (d (list (d (n "cmd_lib_macros") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1bg3q3m9ashq7f864i16wz19di984pbm5f7qwm3yhpp6y7hlg125")))

(define-public crate-cmd_lib-0.11.2 (c (n "cmd_lib") (v "0.11.2") (d (list (d (n "cmd_lib_macros") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "045g83hkcg7bz0cwmawzh13q5mv177krnpn42mrvy3vs1k6midn7")))

(define-public crate-cmd_lib-0.11.3 (c (n "cmd_lib") (v "0.11.3") (d (list (d (n "cmd_lib_macros") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "19bq41s9yrkwn416jmccl3d18av71wjwvlsk98d43klb8whkvsgw")))

(define-public crate-cmd_lib-0.11.4 (c (n "cmd_lib") (v "0.11.4") (d (list (d (n "cmd_lib_macros") (r "^0.11.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "03s7zkhh9vx6rc0vhcnydkw562kkbp5wv3ld8zlidyc00ibm95yl")))

(define-public crate-cmd_lib-0.11.5 (c (n "cmd_lib") (v "0.11.5") (d (list (d (n "cmd_lib_macros") (r "^0.11.5") (d #t) (k 0)))) (h "1rq3h7br1y769qkq1m35vqi3xavz0fv2lf43mmb86qqmkvbf3zsn")))

(define-public crate-cmd_lib-0.11.6 (c (n "cmd_lib") (v "0.11.6") (d (list (d (n "cmd_lib_macros") (r "^0.11.6") (d #t) (k 0)))) (h "1zscm0fs8hgnq72y15bwzb0p5vz73i2n0h0xs5g5iwzkzhjhysxx")))

(define-public crate-cmd_lib-0.12.0-rc1 (c (n "cmd_lib") (v "0.12.0-rc1") (d (list (d (n "cmd_lib_macros") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "07kla433i9wq7c5hipr5q8cyrk27barwh7a5fxgxv9rargxqqxja")))

(define-public crate-cmd_lib-0.12.0 (c (n "cmd_lib") (v "0.12.0") (d (list (d (n "cmd_lib_macros") (r "^0.12.0") (d #t) (k 0)))) (h "09lc6dvlkl5x3xmbpj4kkrscqmf44fp26qxgyw0q87dd7m87yrwj")))

(define-public crate-cmd_lib-0.12.1 (c (n "cmd_lib") (v "0.12.1") (d (list (d (n "cmd_lib_macros") (r "^0.12.1") (d #t) (k 0)))) (h "1gip5156vcdxxc9vkrhhnj9iw4gd3rzi1smyxcg5bhs3bhlzyahb")))

(define-public crate-cmd_lib-0.12.2 (c (n "cmd_lib") (v "0.12.2") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.12.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "16hq4v576dqhxzrm2nvdhvs7lmz6awl51z0rnp266623n2yb61cs")))

(define-public crate-cmd_lib-0.12.3 (c (n "cmd_lib") (v "0.12.3") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.12.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1cxjvc7r0l6m6rlmx1bm0k0dg1lgyvw0v4wfyg4dg0y7dfscak0x")))

(define-public crate-cmd_lib-0.12.4 (c (n "cmd_lib") (v "0.12.4") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0a4dh4nxvd6p12a27lsgmy9sz9p5khb0mj98iqvr0wfyr1ga325x")))

(define-public crate-cmd_lib-0.12.5 (c (n "cmd_lib") (v "0.12.5") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.12.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1yxcq3ysfw1rvwwhpdl1dp20axqi1wb7yfyxm01wc7zyr600c11q")))

(define-public crate-cmd_lib-0.12.6 (c (n "cmd_lib") (v "0.12.6") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.12.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1bw6qgj3v0bdfv9snkigyhdzsp1ynj0jvrl6xqwqw34jkgkdgqsq")))

(define-public crate-cmd_lib-0.14.0 (c (n "cmd_lib") (v "0.14.0") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1b7r2spns8hxkj8a1f5z8yq77yk2ifsisxj6nppjpmbxrz0j9qg5")))

(define-public crate-cmd_lib-0.14.1 (c (n "cmd_lib") (v "0.14.1") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.14.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1jb7sfd5kc0d1md82f5xzpifkafjyadrhgh35a8zx7gzpq1phxnw")))

(define-public crate-cmd_lib-0.14.2 (c (n "cmd_lib") (v "0.14.2") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "07ywzxfcs35j4zkpwsmfxkj3d60fphqxymprb90z1a9x1xk4ihyv")))

(define-public crate-cmd_lib-0.14.3 (c (n "cmd_lib") (v "0.14.3") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.14.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0cr5pk2lxjwq89jzcix5qi33sgkrcf3xlmgi27y01xds597g2445")))

(define-public crate-cmd_lib-0.14.4 (c (n "cmd_lib") (v "0.14.4") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.14.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "02lm9ma99sm451bjz14x6pq6s7wv0wvch0fijagzwdphjghjykzl")))

(define-public crate-cmd_lib-0.14.5 (c (n "cmd_lib") (v "0.14.5") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.14.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1vmj6d6fjpy7sjjkp7jlbqpxj4ipgkn63r51cswjpsxb7c672qzi")))

(define-public crate-cmd_lib-0.14.6 (c (n "cmd_lib") (v "0.14.6") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.14.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "10y3b65zlmgkfjsmkizzz3wrvrspklidc7b6pjikz6xycrqx6fcp")))

(define-public crate-cmd_lib-0.15.0 (c (n "cmd_lib") (v "0.15.0") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "06rz8inxz5bwz38jrkczyhr31vcp0ikd77wh5q6gj439vrjn8a09")))

(define-public crate-cmd_lib-0.15.1 (c (n "cmd_lib") (v "0.15.1") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^0.15.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "105nnjr9pppw69ia55jbbgp31dhi29scffxqy3272pr3rkdfldg0")))

(define-public crate-cmd_lib-1.0.0 (c (n "cmd_lib") (v "1.0.0") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0aicpmhsnrpn1zmaxjhflpcapzah0ih15l4887b5xhldfqvh12i8")))

(define-public crate-cmd_lib-1.0.1 (c (n "cmd_lib") (v "1.0.1") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0p9kp4prc7yc3bfa2cdshkm3dl24ff0840zmgsbibndgd1kkycd4")))

(define-public crate-cmd_lib-1.0.2 (c (n "cmd_lib") (v "1.0.2") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0ap98jnq5k4bsj9lrx4qdc4lz9s0l1xb4l0xi72diwmdmjl297dq")))

(define-public crate-cmd_lib-1.0.3 (c (n "cmd_lib") (v "1.0.3") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1i9hfk3jcbsl37rxlfplvv2ffkf13lm3jz9mawd6h74q9hh19cxr")))

(define-public crate-cmd_lib-1.0.4 (c (n "cmd_lib") (v "1.0.4") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0h3llw00gqwjzl7i8hfw40b8c015xmj89v4cgksbfhfj9qys3ghv")))

(define-public crate-cmd_lib-1.0.5 (c (n "cmd_lib") (v "1.0.5") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.5") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0i0mjaxs6k8nbl5g5xh3p7kjbs1zgvg81knq12k4qp1allqjmzq2")))

(define-public crate-cmd_lib-1.0.6 (c (n "cmd_lib") (v "1.0.6") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.6") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "06bc2mr0ryajh99w78i3aigvaf5njwvxdr0znvb85ch7r343li2v")))

(define-public crate-cmd_lib-1.0.7 (c (n "cmd_lib") (v "1.0.7") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.7") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0arbpvwr7w4nis3nvf7dyhyx8yzp6w4j53rg8bx250b5imdiqx0i")))

(define-public crate-cmd_lib-1.0.8 (c (n "cmd_lib") (v "1.0.8") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.8") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "195yxalfn3gm8pr0wg21psk2ci725rni17jngqd5az2i1rb3iwc0")))

(define-public crate-cmd_lib-1.0.9 (c (n "cmd_lib") (v "1.0.9") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.9") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "05mnx3ipz5jgv9b8rgssc1clhq7piyfkdd36cicaxdn3byzklz4c")))

(define-public crate-cmd_lib-1.0.10 (c (n "cmd_lib") (v "1.0.10") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.10") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0sklci6dv4q5fzs4hgazxdjqkwvj8h9dp18r252q9z3vh8fskx7v")))

(define-public crate-cmd_lib-1.0.11 (c (n "cmd_lib") (v "1.0.11") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.11") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "15w2s8b31ipvlc9jfci93rby89pp998bvnjszb5f9vh6ci94nav3")))

(define-public crate-cmd_lib-1.0.12 (c (n "cmd_lib") (v "1.0.12") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.12") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "183ypsk6ili274ck7r09yralm2b0acldghmzvk29qmmz97lk9jvq")))

(define-public crate-cmd_lib-1.0.13 (c (n "cmd_lib") (v "1.0.13") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.0.13") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1zg8rcp8xpkkznymkfq675vvpfrca9r16ciadwrrls345yv4avv6")))

(define-public crate-cmd_lib-1.1.0 (c (n "cmd_lib") (v "1.1.0") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.1.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "00b5ia6k30pnyydx8yx6x1c1bg4i2k4dgdspn2grzc49z754n4f7")))

(define-public crate-cmd_lib-1.2.0 (c (n "cmd_lib") (v "1.2.0") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.2.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0iywvii1916msg7231nmzqavp5hl5rfqmnm7d1cmajbqsnbccfhl")))

(define-public crate-cmd_lib-1.2.1 (c (n "cmd_lib") (v "1.2.1") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.2.1") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0s30m4j5n3f8izhraiz6yjg50mvv9lhxnwq87fifakdmng793acl")))

(define-public crate-cmd_lib-1.2.2 (c (n "cmd_lib") (v "1.2.2") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.2.2") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1f3kix4v3nxxy2pvl8imn4aiiyml9ywbnd4bfc6hzd05708m2m0j")))

(define-public crate-cmd_lib-1.2.3 (c (n "cmd_lib") (v "1.2.3") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.2.3") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0nz7rqirk8plkam5i9ka9dxik5zgl5kl7p07zg1zj4m50x21cg2z")))

(define-public crate-cmd_lib-1.2.4 (c (n "cmd_lib") (v "1.2.4") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.2.4") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "102yaj5dswlq8aj8zx780zzb7x23yhm20p5gx8lfn3wd831jkv3c")))

(define-public crate-cmd_lib-1.3.0 (c (n "cmd_lib") (v "1.3.0") (d (list (d (n "byte-unit") (r "^4.0") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.3.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0qhgh03azm1rqgrilk150lclcyvs4wpj99dghmzx71kkfw9z980b")))

(define-public crate-cmd_lib-1.4.0 (c (n "cmd_lib") (v "1.4.0") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "05ajjrzggpid5g2mckyziicgamm4gqic9azyfg1b7a71rfji3bik")))

(define-public crate-cmd_lib-1.5.0 (c (n "cmd_lib") (v "1.5.0") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "03svxwfrlb6w6g651fdk38zp6r4msggkklsvgwlhh55hw42ilz5s")))

(define-public crate-cmd_lib-1.6.0 (c (n "cmd_lib") (v "1.6.0") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0bf1s4v1qg09ni2pjpw5psz94v6b8h7hhg80g53f70iadkamsib6")))

(define-public crate-cmd_lib-1.6.1 (c (n "cmd_lib") (v "1.6.1") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0fapc981f05gcf2lg7lgl9lki1231qihzgxzjq2vr1a00793g7sx")))

(define-public crate-cmd_lib-1.8.0 (c (n "cmd_lib") (v "1.8.0") (d (list (d (n "byte-unit") (r "^4.0.19") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "07lljcfb3dk19ygfm6qd9x43nd433mg62pgjgl0qfihs6q0vlp3c")))

(define-public crate-cmd_lib-1.8.1 (c (n "cmd_lib") (v "1.8.1") (d (list (d (n "byte-unit") (r "^4.0.19") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1zpnxab3db5dykwnzh70pjvpsqnx6qbhsmav2wvym81sb2gcspbm")))

(define-public crate-cmd_lib-1.9.0 (c (n "cmd_lib") (v "1.9.0") (d (list (d (n "byte-unit") (r "^4.0.19") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1s15scid1nv9ahprb38a9w05n6jddw6vkjss0xp91mpggnnm4d00")))

(define-public crate-cmd_lib-1.9.1 (c (n "cmd_lib") (v "1.9.1") (d (list (d (n "byte-unit") (r "^4.0.19") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.9.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1jlbfx29mdmlnk6pl8rhbwr0lq1g42va75zmqj140czmpi4090br")))

(define-public crate-cmd_lib-1.9.2 (c (n "cmd_lib") (v "1.9.2") (d (list (d (n "byte-unit") (r "^4.0.19") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.9.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0rik1mvckb24jpwj2np15r89vf98jq0r5v6kaxs2idkckngcvv8r")))

(define-public crate-cmd_lib-1.9.3 (c (n "cmd_lib") (v "1.9.3") (d (list (d (n "byte-unit") (r "^4.0.19") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.9.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0h0d3yqnzjbgda1440insbh45skj8375xj0rbdf67jjimgfcpx75")))

(define-public crate-cmd_lib-1.9.4 (c (n "cmd_lib") (v "1.9.4") (d (list (d (n "byte-unit") (r "^4.0.19") (d #t) (k 2)) (d (n "cmd_lib_macros") (r "^1.9.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "os_pipe") (r "^1.1.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1s7zavyvwzwhckkn7gwkzfy8qsaqmr803np9ixj4s7pr19hpg3vi")))

