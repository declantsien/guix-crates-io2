(define-module (crates-io cm d_ cmd_cache) #:use-module (crates-io))

(define-public crate-cmd_cache-0.1.0 (c (n "cmd_cache") (v "0.1.0") (d (list (d (n "chrono") (r ">= 0.4.11") (d #t) (k 0)) (d (n "rand") (r ">= 0.7.3") (d #t) (k 2)) (d (n "rust-crypto") (r ">= 0.2.36") (d #t) (k 0)) (d (n "tempfile") (r ">= 3.1.0") (d #t) (k 0)))) (h "0p0dzvn3rwl5g493fpwnx414z5vksd4p8bxr0ilpkzangmaivnl7")))

(define-public crate-cmd_cache-0.1.1 (c (n "cmd_cache") (v "0.1.1") (d (list (d (n "chrono") (r ">=0.4.11") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3") (d #t) (k 2)) (d (n "rust-crypto") (r ">=0.2.36") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0") (d #t) (k 0)))) (h "0km1scc56l8bw91lsfcas4cpfgwwsbpp5p92ppnrz1xcz8s10pd9")))

