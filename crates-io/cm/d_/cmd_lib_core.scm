(define-module (crates-io cm d_ cmd_lib_core) #:use-module (crates-io))

(define-public crate-cmd_lib_core-0.1.0 (c (n "cmd_lib_core") (v "0.1.0") (h "1j3zggn7il4xcqbf3j3g22wl78bf2haj1wmgmraj923fafhm0365")))

(define-public crate-cmd_lib_core-0.2.0 (c (n "cmd_lib_core") (v "0.2.0") (h "0x5ipf4lv20can0jjfkivg6nvrkjw82slk3iz676bq0qc3sr8i9y")))

(define-public crate-cmd_lib_core-0.3.0 (c (n "cmd_lib_core") (v "0.3.0") (h "0p1k6zh5i4vswyx8ipllchc8yav01aqzgkx6mkbx87n7561kqdd6")))

(define-public crate-cmd_lib_core-0.4.0 (c (n "cmd_lib_core") (v "0.4.0") (h "0kiagpvhzpi3ynsimwdcm6g1mjvcaksks3w1pypnsd3i91dn9c99")))

