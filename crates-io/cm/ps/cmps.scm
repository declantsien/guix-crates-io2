(define-module (crates-io cm ps cmps) #:use-module (crates-io))

(define-public crate-cmps-0.1.0 (c (n "cmps") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "07dw96v2px5wrd48l792i5rj9zflyrmjzq2gxg7ab3d4igihp0kj")))

(define-public crate-cmps-0.1.1 (c (n "cmps") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0wda9v8j88kmb6k9my0dfxy9wilj8hhcmsfhpc5pcjnr49snssqv")))

(define-public crate-cmps-0.2.0 (c (n "cmps") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.0") (d #t) (k 0)))) (h "17rd2nzxncwdx4qphssbjmslqvdm354l99zsjnwddp4ksjczmnhn")))

(define-public crate-cmps-0.2.1 (c (n "cmps") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.0") (d #t) (k 0)))) (h "094fk8j7lgf3i2a14hs60ib4aad9sp6lxqfhi8xd4gccfs38pixx")))

(define-public crate-cmps-0.2.2 (c (n "cmps") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.0") (d #t) (k 0)))) (h "01rv8r3vrrwj3dd0ab6qjs6kcifwiklgfk4sabiwg9qsyc9qcx16")))

(define-public crate-cmps-0.3.0 (c (n "cmps") (v "0.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "09jdaw2p4vddqp16mly0awnfzc4fyip8lh4k0ni4rpah678q00vn")))

(define-public crate-cmps-0.3.1 (c (n "cmps") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.0") (d #t) (k 0)))) (h "09in85knqjxhjjl4r6gsywdqzkv5ah8lp91h4bqba74bfjvjimn8")))

(define-public crate-cmps-0.3.2 (c (n "cmps") (v "0.3.2") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.0") (d #t) (k 0)))) (h "001769mgasz29z311wgvsx6p6pb02p593n3nb8yb6nnhcc09112d")))

(define-public crate-cmps-0.3.3 (c (n "cmps") (v "0.3.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)))) (h "1v4b9041hzalapg8jf6rrv0qc6w7hrv63c1gjsp8dgcsmskqnqnk")))

(define-public crate-cmps-0.3.4 (c (n "cmps") (v "0.3.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)))) (h "0ki93bdpdxsskh8qii0jch9z2kawmpd8s1jy542xanlxqrjqmak8")))

(define-public crate-cmps-0.4.0 (c (n "cmps") (v "0.4.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)))) (h "0c0irsp1r0ayfwikr7457b7f1rgljl2n7fvzkd0qqh453fqsw64k")))

