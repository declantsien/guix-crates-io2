(define-module (crates-io cm s_ cms_tools) #:use-module (crates-io))

(define-public crate-cms_tools-0.1.0 (c (n "cms_tools") (v "0.1.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1281n8pb501nh3lgn8xy6ix0qy0gl9zih23ixvgmqh7zavkbg66g")))

