(define-module (crates-io cm s_ cms_notifs) #:use-module (crates-io))

(define-public crate-cms_notifs-0.1.0 (c (n "cms_notifs") (v "0.1.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "web-view") (r "^0.7") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1yjr5kg62mx7app52rvs5kihjdnd6spmncc9mn8y0qvrzc0gxxhw")))

(define-public crate-cms_notifs-0.1.1 (c (n "cms_notifs") (v "0.1.1") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "web-view") (r "^0.7") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1jlrc2m00n1gx8p0phabg7sdsbjmmpg01i0z164xxhjl2jfs8yhh")))

(define-public crate-cms_notifs-0.1.2 (c (n "cms_notifs") (v "0.1.2") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)) (d (n "web-view") (r "^0.7") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "02jnkmrdjxhlnly12nc0lw5jigg42058mhkwbcry6dq5979g4ysa")))

