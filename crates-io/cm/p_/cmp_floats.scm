(define-module (crates-io cm p_ cmp_floats) #:use-module (crates-io))

(define-public crate-cmp_floats-0.1.0 (c (n "cmp_floats") (v "0.1.0") (h "1chn8awsbb8615mkff9qcfb55clpawl70djlvcp83ajli8a95sfp") (y #t)))

(define-public crate-cmp_floats-0.1.1 (c (n "cmp_floats") (v "0.1.1") (h "0r339gi02v6cqqd64bzrknkm71an0albyj1fzpad50n6q4i3hpry") (y #t)))

(define-public crate-cmp_floats-0.1.2 (c (n "cmp_floats") (v "0.1.2") (h "0c4z7dxfj2pjszmzw131hirq03zj0z4dsaz9g9f17gbm43pqkr2b") (y #t)))

(define-public crate-cmp_floats-0.1.3 (c (n "cmp_floats") (v "0.1.3") (h "1w3pd83mkkmp6g8i4lj5lv83s23b81cvnv88xawyaga2f26w83n5") (y #t)))

(define-public crate-cmp_floats-0.1.4 (c (n "cmp_floats") (v "0.1.4") (h "0dc157as9khcgrh4k6ql8gamzgr0sby66ql0c19k968n7dpbacj0") (y #t)))

(define-public crate-cmp_floats-0.1.5 (c (n "cmp_floats") (v "0.1.5") (h "0ml4g994gl8miiwayrl2c910svwf32rkd364bx096hycaqbhfhbq") (y #t)))

(define-public crate-cmp_floats-0.1.6 (c (n "cmp_floats") (v "0.1.6") (h "1z8p9nvi1dk8b6hllrfqf5a5mbkh6lxxqjbdslvlr7jr6nypcp0r") (y #t)))

(define-public crate-cmp_floats-0.1.7 (c (n "cmp_floats") (v "0.1.7") (h "0cgczq7jhqxnydk311k7llmzp216n61abqf136ag8xafv0yxasz9") (y #t)))

