(define-module (crates-io cm p_ cmp_json) #:use-module (crates-io))

(define-public crate-cmp_json-0.1.0 (c (n "cmp_json") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1ayw5jw84xdqs74gwvklzkhpf7rkpv016v8ly1n77xnr4szns63x")))

