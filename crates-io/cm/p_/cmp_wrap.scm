(define-module (crates-io cm p_ cmp_wrap) #:use-module (crates-io))

(define-public crate-cmp_wrap-0.1.0 (c (n "cmp_wrap") (v "0.1.0") (h "1ljs7wvmavvzih4l340r94gksn2ysj45zrmx6jazjr3lfgv48h8r")))

(define-public crate-cmp_wrap-0.1.1 (c (n "cmp_wrap") (v "0.1.1") (h "0rf47rxdi9hgmh0g96d6i1lz2gzgqlibqzx6j5x3v7nnz2wn0iln")))

(define-public crate-cmp_wrap-0.1.2 (c (n "cmp_wrap") (v "0.1.2") (h "0fjaaflmwb4283jkjvyjvar9x1pcggjb0il0bmswr4f8xpyc8lx5")))

(define-public crate-cmp_wrap-0.2.1 (c (n "cmp_wrap") (v "0.2.1") (h "1bm9f4a241169fl522byw1752z55km2caknq1j5js9491qbw5v4i")))

(define-public crate-cmp_wrap-0.2.2 (c (n "cmp_wrap") (v "0.2.2") (h "16kpijkw5sxjx73zyiyi2xjzncgsqvbykl582s6q6f59f81bmj5x")))

