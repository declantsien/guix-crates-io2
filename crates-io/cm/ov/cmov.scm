(define-module (crates-io cm ov cmov) #:use-module (crates-io))

(define-public crate-cmov-0.0.0 (c (n "cmov") (v "0.0.0") (h "0ajjmwxbj9624y06h300pqgfnsxs72xqsicp7sgzzls6s7f7qh5x") (y #t)))

(define-public crate-cmov-0.1.0 (c (n "cmov") (v "0.1.0") (h "0i61ddshvgz3swilg1fh7qz7m0yxdhwhiyvvjp07r9xm3rqhc4xb") (r "1.59")))

(define-public crate-cmov-0.1.1 (c (n "cmov") (v "0.1.1") (h "0r8chjx5hz1i05z50jsi1fhxzggy1chn3v2mlx5rfmr2jrzk966n") (r "1.59")))

(define-public crate-cmov-0.2.0 (c (n "cmov") (v "0.2.0") (h "0747j7al6d5spyp1b0vf41kj31n0l3g09vafyph3jwkgzgwr0j15") (r "1.60")))

(define-public crate-cmov-0.3.0 (c (n "cmov") (v "0.3.0") (h "0aw28007dkvs0l5112n6vsq3ybdsi4y01ssa0pyxf41amg3pipxa") (r "1.60")))

(define-public crate-cmov-0.3.1 (c (n "cmov") (v "0.3.1") (h "1avg148f883m979gicxy6yh1icqwxjl4s9drglk47mbmp9hcj78v") (r "1.60")))

