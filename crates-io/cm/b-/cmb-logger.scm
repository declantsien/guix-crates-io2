(define-module (crates-io cm b- cmb-logger) #:use-module (crates-io))

(define-public crate-cmb-logger-0.1.0 (c (n "cmb-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)))) (h "0dpk0wpmfwcz8cqhjfbla6cnfyn2x0lbzm1aw1835j89003q9z4i")))

(define-public crate-cmb-logger-0.1.1 (c (n "cmb-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)))) (h "139ijdpxlsihj2751rpz0mchg5x0ybxh3p63bzcalxsdisl1cfyk")))

