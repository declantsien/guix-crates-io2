(define-module (crates-io cm pr cmprss) #:use-module (crates-io))

(define-public crate-cmprss-0.0.1 (c (n "cmprss") (v "0.0.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)))) (h "0ndni6w762dwbggq910080p45qsnnq8m8lzxg65bj81jfbs0h3qy")))

(define-public crate-cmprss-0.1.0 (c (n "cmprss") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0.2") (d #t) (k 2)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (d #t) (k 0)))) (h "1dg69fck3ri2mgk8ag6ylfbq8pjyj45z8vb0giv830d8njgmy761")))

(define-public crate-cmprss-0.2.0 (c (n "cmprss") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.12") (d #t) (k 2)) (d (n "bzip2") (r "^0.4.4") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0.2") (d #t) (k 2)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (d #t) (k 0)))) (h "1br3skk14gwky00i021djpmqwrpw7gb6wlnax1bbzz8dm8jjm8ba")))

