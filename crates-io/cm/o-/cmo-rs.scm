(define-module (crates-io cm o- cmo-rs) #:use-module (crates-io))

(define-public crate-cmo-rs-0.1.0 (c (n "cmo-rs") (v "0.1.0") (d (list (d (n "sma-rs") (r ">=0.1.1, <0.2.0") (d #t) (k 0)) (d (n "ta-common") (r ">=0.1.2, <0.2.0") (d #t) (k 0)))) (h "0wdiqhyikznhkslzlrmmcifphkwnbl84hb9s365gs7l1lnrvwla6")))

