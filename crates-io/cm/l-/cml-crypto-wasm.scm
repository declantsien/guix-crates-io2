(define-module (crates-io cm l- cml-crypto-wasm) #:use-module (crates-io))

(define-public crate-cml-crypto-wasm-0.1.0 (c (n "cml-crypto-wasm") (v "0.1.0") (d (list (d (n "cbor_event") (r "^2.2.0") (d #t) (k 0)) (d (n "cml-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0zagd87sv1cw0a3blz6b6prhyqq7pmfv5fslw0zknbmgbhhqlsvn")))

(define-public crate-cml-crypto-wasm-4.0.2 (c (n "cml-crypto-wasm") (v "4.0.2") (d (list (d (n "cbor_event") (r "^2.2.0") (d #t) (k 0)) (d (n "cml-crypto") (r "^4.0.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0hhwki38qrd4k58ja5a3c9g0z440mqlqvzh65pk4732kp0nkr7mg")))

(define-public crate-cml-crypto-wasm-5.0.0 (c (n "cml-crypto-wasm") (v "5.0.0") (d (list (d (n "cbor_event") (r "^2.2.0") (d #t) (k 0)) (d (n "cml-crypto") (r "^5.0.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0idy1f8iwhpdn9m5757n2sfsp6rnjpk1fpx2l7k2f9hvd17k55xj")))

(define-public crate-cml-crypto-wasm-5.1.0 (c (n "cml-crypto-wasm") (v "5.1.0") (d (list (d (n "cbor_event") (r "^2.2.0") (d #t) (k 0)) (d (n "cml-crypto") (r "^5.1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "08y081a1bs28kii7ayv6pn56xz99l7fwf2y16r401r9wqy49lpn7")))

(define-public crate-cml-crypto-wasm-5.2.0 (c (n "cml-crypto-wasm") (v "5.2.0") (d (list (d (n "cbor_event") (r "^2.2.0") (d #t) (k 0)) (d (n "cml-crypto") (r "^5.2.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0292z09544ypzqihnc932isvnb8nybikrlz5faagpgg3vijfcz67")))

(define-public crate-cml-crypto-wasm-5.3.0 (c (n "cml-crypto-wasm") (v "5.3.0") (d (list (d (n "cbor_event") (r "^2.2.0") (d #t) (k 0)) (d (n "cml-crypto") (r "^5.3.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0q7qadx4qlmfvvzzq7ng0frncfgn67gvj0psc97slsi69krdvchl")))

(define-public crate-cml-crypto-wasm-5.3.1 (c (n "cml-crypto-wasm") (v "5.3.1") (d (list (d (n "cbor_event") (r "^2.2.0") (d #t) (k 0)) (d (n "cml-crypto") (r "^5.3.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.83") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0vs7qyxildbr4fwm4mhyppprbhih0di0ghyy2k5p56z4aq2drskn")))

