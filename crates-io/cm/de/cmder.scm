(define-module (crates-io cm de cmder) #:use-module (crates-io))

(define-public crate-cmder-0.1.0 (c (n "cmder") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1awd1cphj5ghr1y2d7yjfqd4zjz40pd3vbiii690vkfqk5p2nskm") (y #t)))

(define-public crate-cmder-0.2.0 (c (n "cmder") (v "0.2.0") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0hpwk8228m3qcz3msvhbamznpm5ad1jlxk5bx42ri8c7s812gjgb") (y #t)))

(define-public crate-cmder-0.3.0 (c (n "cmder") (v "0.3.0") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0n0dk1lm8yy3mrzfdks8iynvarr5plp6gyzp87mnxv0fq23m8wfk")))

(define-public crate-cmder-0.4.0 (c (n "cmder") (v "0.4.0") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "082mripifhy0l5k1mv1cfipd3wk5z73wg19bky0f2nqafxy5pdj7")))

(define-public crate-cmder-0.4.1 (c (n "cmder") (v "0.4.1") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1v1cnvv68dgbk6hpqws2wl70f54i19hlvp33qfwnhdshbn943db0")))

(define-public crate-cmder-0.5.0 (c (n "cmder") (v "0.5.0") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0bsmy0x6xkx1342psx88sqig33b11sj1i7n8s8nphnh2qh35rqrm")))

(define-public crate-cmder-0.5.1 (c (n "cmder") (v "0.5.1") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "17bnzi4cqhzlpvr6fp71x5ya3w156zp23ivpkcypj6as4gilhf4j")))

(define-public crate-cmder-0.5.2 (c (n "cmder") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0w6qw220zwk2s5q6v09g360x7w07x2zhyz7vgqrw3ja4p128wp0n")))

(define-public crate-cmder-1.0.0-beta-1 (c (n "cmder") (v "1.0.0-beta-1") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0r0k3w0yd9cvjb3cj4iw6gkajap5db9m7xvylbcia1l2c41dxa5q")))

(define-public crate-cmder-0.6.0 (c (n "cmder") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0ywsllpbzpfqvmnc3h4ik47fqx0yz7x2cxrmlni9bm295wpv2kks")))

(define-public crate-cmder-0.6.1 (c (n "cmder") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "029yfgnn11zk6cv31l9g5sndpakd2d5w2i897djd5445gdzfg3rz")))

