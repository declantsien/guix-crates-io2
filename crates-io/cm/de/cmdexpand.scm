(define-module (crates-io cm de cmdexpand) #:use-module (crates-io))

(define-public crate-cmdexpand-0.2.0 (c (n "cmdexpand") (v "0.2.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0m9m2im38f3kijc25q8aj2vp3ymn9qjlqwa1p4qjgqg6nn4vjg75")))

(define-public crate-cmdexpand-0.2.1 (c (n "cmdexpand") (v "0.2.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i3f592k9yg8g0g5bg00vkl8na4pyriqkx49wbimrjj37y6j6jjz")))

(define-public crate-cmdexpand-0.2.2 (c (n "cmdexpand") (v "0.2.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sd5zdl41n4c29cysil0f2fk6a0q9xf361g929fn0z68xsl759ym")))

(define-public crate-cmdexpand-0.3.0 (c (n "cmdexpand") (v "0.3.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ikqx7c1ri1wlzsfrl4w48bqf845wy8rx507wvr9vnq8zaxh8av3")))

(define-public crate-cmdexpand-0.3.1 (c (n "cmdexpand") (v "0.3.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mpx0q19h8iniykcrg47wrhhhnn7i39yfp1rhjg4mbbv6lpggdd7")))

(define-public crate-cmdexpand-0.3.2 (c (n "cmdexpand") (v "0.3.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07g2byvs9sw0l9lq811i89ag641wywrrly95cajlgrb8ngazwrzv")))

