(define-module (crates-io cm de cmdex) #:use-module (crates-io))

(define-public crate-cmdex-0.1.0 (c (n "cmdex") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1w978x0cw042k9y5wgk6b16m1nhkkkmakw83wbb070b3a64f0il2")))

(define-public crate-cmdex-0.1.1 (c (n "cmdex") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "10wlgj1gbi9vsk1r1nckxr8rvb5pma10c6plgv6gilk438vgnq2b")))

(define-public crate-cmdex-0.1.2 (c (n "cmdex") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1rnkkbl0s0pmiybz110rq59528giw2za2hyadaq7i1lkm18lfigx")))

(define-public crate-cmdex-0.1.3 (c (n "cmdex") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0ih7krwn53a7z142r6fwws396ag5vr4jci84vv6n5n2r7z8bpgr2")))

(define-public crate-cmdex-0.1.4 (c (n "cmdex") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1kl8fdfldfz6fwhna3hwqg7030jyyiwahfbfq5zvyv4nql327cb0")))

(define-public crate-cmdex-0.1.5 (c (n "cmdex") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0srgr4rvs6m1jv77hliqarqs0pprbndm1p6fas2dj22ichwciwzv")))

(define-public crate-cmdex-0.1.6 (c (n "cmdex") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1srkyn916bvjmfjm9db3rr4mc72jl5q6cgqhi7lr11mj7zl4lk4d")))

(define-public crate-cmdex-0.1.7 (c (n "cmdex") (v "0.1.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1v0bdb5j4jimjlzxfhpsk8v0qkp4kzx1m4idvv0hbchpjf6mabg5")))

(define-public crate-cmdex-0.1.8 (c (n "cmdex") (v "0.1.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0vnsqxdf7dzdf21x1za29lnwgqr34g0gpq8i55xkgjpcgxf9l496")))

(define-public crate-cmdex-0.1.9 (c (n "cmdex") (v "0.1.9") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "13yhjmi8jy414r609wrd6nc1saf1j262jxfqjvhn9lvqslg716q4")))

(define-public crate-cmdex-0.1.10 (c (n "cmdex") (v "0.1.10") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1k2wjnpchcsnpllpbilqdx64741hckxdlqk3k7b00cwjsazw2ml0")))

(define-public crate-cmdex-0.1.11 (c (n "cmdex") (v "0.1.11") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1zjhw60m966wybxb6zp3yyfik8jyx85hxnv4ypgfbkbqc64i1hrk")))

(define-public crate-cmdex-0.1.12 (c (n "cmdex") (v "0.1.12") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0ybdhr3xzm55r75g3c6nrpjqihddlri5z33c89mi4ggnmvcgh9jf")))

(define-public crate-cmdex-0.2.1 (c (n "cmdex") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0amqpqzflxl12vp51v0znf2hsh7y3z9i9s5dl3wjd3na5za0j4gz")))

(define-public crate-cmdex-0.2.3 (c (n "cmdex") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "041riik0zx7r365ka3i9cg794p5242lh007m965yp3iz3s3ycar2")))

(define-public crate-cmdex-0.2.4 (c (n "cmdex") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1mjkw7q75napr44ms1fkxwhv8d79hbzrqyv2bch57av9raz05d0f")))

(define-public crate-cmdex-0.2.5 (c (n "cmdex") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0npp8jc1s1xdwv5ahv54r20crafvjc52v0dm11xrs9w038vp8b6k")))

(define-public crate-cmdex-0.2.6 (c (n "cmdex") (v "0.2.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "121s46vc9s6akjvhk1brqa1qn3p5xz72vi34mxr2zpjk5dfq5f61")))

(define-public crate-cmdex-0.2.7 (c (n "cmdex") (v "0.2.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1a7lirx1014gvxif0pcjx970glchpxhx6nmkzncw363al146ywl3")))

