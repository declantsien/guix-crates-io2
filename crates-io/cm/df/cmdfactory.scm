(define-module (crates-io cm df cmdfactory) #:use-module (crates-io))

(define-public crate-cmdfactory-0.3.2 (c (n "cmdfactory") (v "0.3.2") (d (list (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9.2") (d #t) (k 0)) (d (n "rust-embed") (r "^5.6.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1scj7cbmnjg1b0wy0vv8y2c738srg1z8gw511ifq3fkrf6a48q8y") (y #t)))

(define-public crate-cmdfactory-0.3.3 (c (n "cmdfactory") (v "0.3.3") (h "1fa9jkb2zk7invmrc5zsa6fyjixm9x98lmb3z2rb69k731cg0w0p") (y #t)))

