(define-module (crates-io cm as cmasfo-dev) #:use-module (crates-io))

(define-public crate-cmasfo-dev-0.0.1 (c (n "cmasfo-dev") (v "0.0.1") (h "1l8gszab0lvanfnzl0d8yf55i3hll9h14f3729lirw5hsyvgpjd5") (y #t)))

(define-public crate-cmasfo-dev-0.0.2 (c (n "cmasfo-dev") (v "0.0.2") (h "1fcg3vbnhwbrcx9gdr8xw7ydsc5g5m466nvma4i708gyh9mi76wr") (y #t)))

(define-public crate-cmasfo-dev-0.0.3 (c (n "cmasfo-dev") (v "0.0.3") (h "16kl79rqws712n3qxcbp3b1ifgbaz031i93w4wdiikg8371fiaaw") (y #t)))

(define-public crate-cmasfo-dev-0.0.31 (c (n "cmasfo-dev") (v "0.0.31") (h "1dsdz9spg0vx4cychzajfjaysjgjddnymwr40sjybjwsn9hfqxh6") (y #t)))

(define-public crate-cmasfo-dev-0.0.32 (c (n "cmasfo-dev") (v "0.0.32") (h "05c83r8z6l718gxmin6yr0isgr260zfafnhjvmgblchycp8pg4dr") (y #t)))

(define-public crate-cmasfo-dev-0.0.33 (c (n "cmasfo-dev") (v "0.0.33") (h "1jvv4ccwnm5xjnmgam5jl9hpg5jydm4wq5dwvjhck0dw8yfvl6ym") (y #t)))

(define-public crate-cmasfo-dev-0.0.34 (c (n "cmasfo-dev") (v "0.0.34") (h "0v4ndnlbglh1ky8g1h5p0bgcqynlcnrzj433l1yw2nw3xm9yyv6y") (y #t)))

(define-public crate-cmasfo-dev-0.1.0 (c (n "cmasfo-dev") (v "0.1.0") (d (list (d (n "rand") (r "^0") (d #t) (k 0)))) (h "0hhdcqzp96knm6qh3vck1gzxy6433d1wgj3cknfvzzx9kykn9m33") (y #t)))

(define-public crate-cmasfo-dev-0.1.1 (c (n "cmasfo-dev") (v "0.1.1") (d (list (d (n "rand") (r "^0") (d #t) (k 0)))) (h "06lr8zx4787qi9q6qby7bhzl9nrp7g5pfkxa9jlw6dhpbddaqd8n") (y #t)))

(define-public crate-cmasfo-dev-0.1.11 (c (n "cmasfo-dev") (v "0.1.11") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "wry") (r "^0") (d #t) (k 0)))) (h "0v0s3gd16k7zq3by3xbbdhvca5vqkg5viaqj7pv1i7ak6s7335yr") (y #t)))

(define-public crate-cmasfo-dev-0.1.12 (c (n "cmasfo-dev") (v "0.1.12") (d (list (d (n "image") (r "^0") (d #t) (k 0)) (d (n "wry") (r "^0") (d #t) (k 0)))) (h "0kdg5ha5qwxgsyhibqn4c8ar3iwmbmz544n6c8sbi96q04c9d60q") (y #t)))

(define-public crate-cmasfo-dev-0.1.13 (c (n "cmasfo-dev") (v "0.1.13") (h "1gbsxlq06n0nh4q4dzg5hyalaahdg3vfdib11lv82ksybwxngd27") (y #t)))

(define-public crate-cmasfo-dev-0.1.14 (c (n "cmasfo-dev") (v "0.1.14") (h "0fa35ph0arb3mwc7b6r20848ym5zzlkbinhwq0aqmfwflwsfczm9")))

(define-public crate-cmasfo-dev-0.1.15 (c (n "cmasfo-dev") (v "0.1.15") (h "0xk01v625j591jxi5wh3s0p7hfxyavlrm4lzrwi3bxk7a8m3sfz9")))

(define-public crate-cmasfo-dev-0.1.16 (c (n "cmasfo-dev") (v "0.1.16") (h "15hlrimpzxj4vpbyvd010504nh2w71d707kd7km54f4fnpxa7na5")))

