(define-module (crates-io cm us cmus-status-line) #:use-module (crates-io))

(define-public crate-cmus-status-line-1.0.0 (c (n "cmus-status-line") (v "1.0.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0mfcddlkvwjb8fng4a7k08hd16svgla6ma0clvsjgfqjw8an1ab5")))

(define-public crate-cmus-status-line-1.0.1 (c (n "cmus-status-line") (v "1.0.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0ff074q8csap9f0xdq2r2cf45lvfnf66v1c6n73c70qg4xr28ga0")))

(define-public crate-cmus-status-line-1.0.2 (c (n "cmus-status-line") (v "1.0.2") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0c629paah4ycsdskfah7s0qrc5b7yf5dw7wz9bnd15f5znf1h07a")))

(define-public crate-cmus-status-line-1.1.0 (c (n "cmus-status-line") (v "1.1.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "02l7bqkyizkxx1j61jj4nq5qiyl8b25d275f7g6wvdj3rvxl598a") (y #t)))

(define-public crate-cmus-status-line-1.1.1 (c (n "cmus-status-line") (v "1.1.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0p2z4vdryckacdd0a9pm6dn8vr1cm1an51qp39bnxlr0dr3dk5z5")))

