(define-module (crates-io cm us cmus-rpc-rs) #:use-module (crates-io))

(define-public crate-cmus-rpc-rs-0.1.0 (c (n "cmus-rpc-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.1") (d #t) (k 0)))) (h "0grj1jxz2lvl09n03xswghqb0n81a9ravbcvlqd7hvchjigncsn1")))

(define-public crate-cmus-rpc-rs-0.1.1 (c (n "cmus-rpc-rs") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.1") (d #t) (k 0)))) (h "1mc5k9hdcga0fy4hkw5fc6h1phr3iwiq1m0fg5hzj07bbfyswnyi")))

(define-public crate-cmus-rpc-rs-0.1.2 (c (n "cmus-rpc-rs") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.1") (d #t) (k 0)))) (h "1zzgbswdkfgml84d8043rx5r9cb25i9ipmrkr7mqpnhmdfifgg5q")))

(define-public crate-cmus-rpc-rs-0.1.3 (c (n "cmus-rpc-rs") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.1") (d #t) (k 0)))) (h "00hmnwj9c7mr32m21zgx67j5zprfcyxj4prabwfvfi7iqdwl0qdv")))

(define-public crate-cmus-rpc-rs-0.2.0 (c (n "cmus-rpc-rs") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.3") (d #t) (k 0)))) (h "0kq0r807ds315hz89i4k6ppd566d5ypnmqgavs1k7jhgdzhf2pn9")))

