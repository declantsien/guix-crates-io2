(define-module (crates-io cm _c cm_contract_sdk) #:use-module (crates-io))

(define-public crate-cm_contract_sdk-0.0.1 (c (n "cm_contract_sdk") (v "0.0.1") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.29") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1cb1w5vni1pfqvjziqsbbq71r3hcabpa1zhfiw3dhn0gg7q177mw") (y #t)))

(define-public crate-cm_contract_sdk-0.0.2 (c (n "cm_contract_sdk") (v "0.0.2") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.29") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1l91ns7fdipmfajnzz9fjx927c1jqc2haj335q6j47i1j3pl1hg4")))

