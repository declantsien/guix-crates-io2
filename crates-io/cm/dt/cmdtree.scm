(define-module (crates-io cm dt cmdtree) #:use-module (crates-io))

(define-public crate-cmdtree-0.0.1 (c (n "cmdtree") (v "0.0.1") (h "029r09a18p2hw2yrn8rqhlgvj89i3n1lfgnr1yradbgca0g2097d")))

(define-public crate-cmdtree-0.1.0 (c (n "cmdtree") (v "0.1.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)))) (h "182r3rjlkpl29xl850wqd7h3rr7x1rhiq5k3p5f1hb66cp4bgykp")))

(define-public crate-cmdtree-0.2.0 (c (n "cmdtree") (v "0.2.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)))) (h "0z94lwfcswz6hwckz5lb1yxa7rhmcffv8c1lw7z3492j5mcy3qd4")))

(define-public crate-cmdtree-0.3.0 (c (n "cmdtree") (v "0.3.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)))) (h "0qm37pbadnik3f4yzmcg811sg1hw85dchj4dwv9j6x6p5vgng5z3")))

(define-public crate-cmdtree-0.4.0 (c (n "cmdtree") (v "0.4.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)))) (h "1nrikg06b6zx0lm943nmw069c0r9a48bffkji7pad44xqiyjhcw9")))

(define-public crate-cmdtree-0.4.1 (c (n "cmdtree") (v "0.4.1") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)))) (h "0dz35wzw5r77bgiqdjl86a7a11h7ah3wn2gi9g1zd15wf90lhs3p")))

(define-public crate-cmdtree-0.4.2 (c (n "cmdtree") (v "0.4.2") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)))) (h "0lny040mp68dmqy998mhbhby3mv907hxyrishp33rc5n1hadjh72")))

(define-public crate-cmdtree-0.5.0 (c (n "cmdtree") (v "0.5.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)))) (h "1dcrla3549yq5drypdisa97rijnhpyn4cnkc0dmx462yc8hakmaa")))

(define-public crate-cmdtree-0.6.0 (c (n "cmdtree") (v "0.6.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)))) (h "0d51liq8cxiybnh0nmywf5ipmr52prm1x8my650y6nj702i0nk6n")))

(define-public crate-cmdtree-0.6.1 (c (n "cmdtree") (v "0.6.1") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)))) (h "1fr029ypc6dr06rxfak2lapcym8nsijq8485f6ywimcnag093jjq")))

(define-public crate-cmdtree-0.7.0 (c (n "cmdtree") (v "0.7.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)))) (h "1yb3lbv4iz81gryidyyms3pk52mh0aljzpw4db0wl9glp1gw24z4")))

(define-public crate-cmdtree-0.8.0 (c (n "cmdtree") (v "0.8.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "linefeed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "12qws36lq1m7ywfvx6448zd4gzxp3rs9ig23r15dvnqs637y7869") (f (quote (("runnable" "linefeed") ("default" "runnable"))))))

(define-public crate-cmdtree-0.9.0 (c (n "cmdtree") (v "0.9.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "linefeed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0w08xq31vdzk3hg8wgpxhipnqsp3fl3fh0xqf3kx33v6712yd7sk") (f (quote (("runnable" "linefeed") ("default" "runnable"))))))

(define-public crate-cmdtree-0.10.0 (c (n "cmdtree") (v "0.10.0") (d (list (d (n "colored") (r "^1.8") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linefeed") (r "^0.6") (o #t) (k 0)))) (h "1v54b1i2npcbwqp0n7bzgxqrxjbzx20vq4fi74wgyszs76hg0miy") (f (quote (("runnable" "linefeed") ("default" "runnable"))))))

(define-public crate-cmdtree-0.10.1 (c (n "cmdtree") (v "0.10.1") (d (list (d (n "colored") (r "^2") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linefeed") (r "^0.6") (o #t) (k 0)))) (h "05rhcjgkr7g2wf6720gax091387ym6gczwph9sv06r5xchvbbyal") (f (quote (("runnable" "linefeed") ("default" "runnable"))))))

