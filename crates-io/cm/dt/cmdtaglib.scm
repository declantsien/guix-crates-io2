(define-module (crates-io cm dt cmdtaglib) #:use-module (crates-io))

(define-public crate-cmdtaglib-0.1.0 (c (n "cmdtaglib") (v "0.1.0") (d (list (d (n "taglib") (r "^0.1.0") (d #t) (k 0)))) (h "1aza67y2xdv86359aizwp5dkavkaa8i430k2l3hy3xbwf7dmwirr")))

(define-public crate-cmdtaglib-0.1.1 (c (n "cmdtaglib") (v "0.1.1") (d (list (d (n "taglib") (r "^0.1.0") (d #t) (k 0)))) (h "1lcm5qr6mjma7grfp61qgam0hircc7hq8irrvqp01bi66d31a2si")))

(define-public crate-cmdtaglib-0.1.2 (c (n "cmdtaglib") (v "0.1.2") (d (list (d (n "taglib") (r "^0.1.0") (d #t) (k 0)))) (h "0x9whgc19sl1977xivqg590zvgxans80jc4fkr2ffvcjv5p3jwrg")))

(define-public crate-cmdtaglib-0.1.3 (c (n "cmdtaglib") (v "0.1.3") (d (list (d (n "taglib") (r "^0.1.0") (d #t) (k 0)))) (h "01jcybhwvg27giw216i9a2jf6fyxbnksq5dim60z8dh942k9xc66")))

(define-public crate-cmdtaglib-0.1.4 (c (n "cmdtaglib") (v "0.1.4") (d (list (d (n "taglib") (r "^0.1.0") (d #t) (k 0)))) (h "0i1whq2wcaxyb9lhd3yanm322gsb8graa8g6nyhh72ygql0w2dz2")))

(define-public crate-cmdtaglib-0.1.5 (c (n "cmdtaglib") (v "0.1.5") (d (list (d (n "taglib") (r "^0.1.0") (d #t) (k 0)))) (h "1f8s6fjjc0nh5b9qxrxinlc1vzidr92v9r0i3mlh0gipc0g0r03l")))

(define-public crate-cmdtaglib-0.2.0 (c (n "cmdtaglib") (v "0.2.0") (d (list (d (n "taglib") (r "^0.1.0") (d #t) (k 0)))) (h "12bndy3fvpkn200nz317kcicchz93b13nc68bcf24iyl3shnxbfv")))

(define-public crate-cmdtaglib-0.2.1 (c (n "cmdtaglib") (v "0.2.1") (d (list (d (n "taglib") (r "^0.1.0") (d #t) (k 0)))) (h "1g5qkxvzpvr454fxc0p780j7gd4zx6ws16rd1rizn3l99rh0wrga")))

(define-public crate-cmdtaglib-0.2.2 (c (n "cmdtaglib") (v "0.2.2") (d (list (d (n "taglib") (r "^0.1.0") (d #t) (k 0)))) (h "10343wsi1sx7vxcsvak376vlqyddspar4sgcn1da4qf7qvrrk9v5")))

