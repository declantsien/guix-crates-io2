(define-module (crates-io cm ic cmice) #:use-module (crates-io))

(define-public crate-cmice-0.1.0 (c (n "cmice") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mice") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "13v5mfs585vna8llf3kla4yp9aw792ybj8rhgwf83vw3hg4pxxlh")))

