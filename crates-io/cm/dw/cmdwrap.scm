(define-module (crates-io cm dw cmdwrap) #:use-module (crates-io))

(define-public crate-cmdwrap-0.1.0 (c (n "cmdwrap") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h7narj2mq56xfmkqsdjirv4nln503mi0ndg9x5pairimhdqjjjb")))

(define-public crate-cmdwrap-0.1.1 (c (n "cmdwrap") (v "0.1.1") (d (list (d (n "async-stream") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kvj55h1pyj66j6xfz9drag1d3rcgv9414dxzvpywybdlfd20ql8")))

