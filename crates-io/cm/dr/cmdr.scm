(define-module (crates-io cm dr cmdr) #:use-module (crates-io))

(define-public crate-cmdr-0.1.0 (c (n "cmdr") (v "0.1.0") (d (list (d (n "cmdr_macro") (r "^0.1") (d #t) (k 0)))) (h "1v5gb3b7n0kyi06wa3cbnfyjx4hx75qvrfvcp7m0kk50y7dangpz")))

(define-public crate-cmdr-0.1.1 (c (n "cmdr") (v "0.1.1") (d (list (d (n "cmdr_macro") (r "^0.1") (d #t) (k 0)))) (h "1nw3z1rgdlgymxh6nnvkbhg7lhlqyj7r2qfpkkrzd7yz8nc11qbw")))

(define-public crate-cmdr-0.1.2 (c (n "cmdr") (v "0.1.2") (d (list (d (n "cmdr_macro") (r "^0.1") (d #t) (k 0)))) (h "160fws467wympvjqyzn5ad1c9g23i3p2y07b43ndnzi8ywp8f8zl")))

(define-public crate-cmdr-0.1.3 (c (n "cmdr") (v "0.1.3") (d (list (d (n "cmdr_macro") (r "^0.1") (d #t) (k 0)))) (h "17vanamxv0pkzz935yd20nsfljpqg0yfwfnisp8kvy5kqxyd9yi9")))

(define-public crate-cmdr-0.2.0 (c (n "cmdr") (v "0.2.0") (d (list (d (n "cmdr_macro") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^3") (d #t) (k 0)))) (h "0hz9azjspv23nh0cap049x6chm29c3c441ishxigvnmnry5qi5m3")))

(define-public crate-cmdr-0.2.1 (c (n "cmdr") (v "0.2.1") (d (list (d (n "cmdr_macro") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^3") (d #t) (k 0)))) (h "0cjwzds95z7yv6461h4wxkv19l5q2ir5s6v1cf483ykf8cpzzkdl")))

(define-public crate-cmdr-0.3.0 (c (n "cmdr") (v "0.3.0") (d (list (d (n "cmdr_macro") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^3") (d #t) (k 0)))) (h "1qhwqpzfwj764a1ycq7y3bq187mimzg1a4xyars5nw3v3sdaiv05")))

(define-public crate-cmdr-0.3.1 (c (n "cmdr") (v "0.3.1") (d (list (d (n "cmdr_macro") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^3") (d #t) (k 0)))) (h "1bmdp8zhq1p13n9vs3y6f6i43ypgqa6n8j21ly47c8xzhabb8xgq")))

(define-public crate-cmdr-0.3.2 (c (n "cmdr") (v "0.3.2") (d (list (d (n "cmdr_macro") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^3") (d #t) (k 0)))) (h "1vzlyin2yflkdm3ninl1zl89bnsdj4fnskxb3q3i3cadf68fq983")))

(define-public crate-cmdr-0.3.3 (c (n "cmdr") (v "0.3.3") (d (list (d (n "cmdr_macro") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^3") (d #t) (k 0)))) (h "1ycp9a9f777bi4jqzj33yfnh6wn8q68ixj6gp5m28qkqs786630l")))

(define-public crate-cmdr-0.3.4 (c (n "cmdr") (v "0.3.4") (d (list (d (n "cmdr_macro") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^3") (d #t) (k 0)))) (h "1yk4dn5p7pgx65wdf72r1yprnhjp00sm4pjm46rb2zsxzyridgdx")))

(define-public crate-cmdr-0.3.5 (c (n "cmdr") (v "0.3.5") (d (list (d (n "cmdr_macro") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^4") (d #t) (k 0)))) (h "08whidcsz19j4hkihswdmkyi575n2iji0rzzmpn0v6g7xx30ff0d")))

(define-public crate-cmdr-0.3.6 (c (n "cmdr") (v "0.3.6") (d (list (d (n "cmdr_macro") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^4") (d #t) (k 0)))) (h "18z4qdmh5f9xm8jsrncdgwmav2dqm8s89mci6vg731mxnp01g097")))

(define-public crate-cmdr-0.3.7 (c (n "cmdr") (v "0.3.7") (d (list (d (n "cmdr_macro") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^4") (d #t) (k 0)))) (h "0i21hxqym0lc38sd3mfzzd43pzfyd0b9wf08pgm3yxnfnz7hbmrp")))

(define-public crate-cmdr-0.3.8 (c (n "cmdr") (v "0.3.8") (d (list (d (n "cmdr_macro") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^4") (d #t) (k 0)))) (h "00wc9sb3iyf5d5vvqsz7iyb0q2bl9b4abz7ybbxqhcw1qvvdps9d")))

(define-public crate-cmdr-0.3.9 (c (n "cmdr") (v "0.3.9") (d (list (d (n "cmdr_macro") (r "^0.3.9") (d #t) (k 0)) (d (n "rustyline") (r "^4.0.0") (d #t) (k 0)))) (h "05apdv61f31kjwa19ld8q2ckb4anmsj47ryvy3rxqny5ay87skwl")))

(define-public crate-cmdr-0.3.10 (c (n "cmdr") (v "0.3.10") (d (list (d (n "cmdr_macro") (r "^0.3.9") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "1231sxw9qj3ib8dl4wafg3qwayl1nv4mdvcw1p6h32vyi5g896c6")))

(define-public crate-cmdr-0.3.11 (c (n "cmdr") (v "0.3.11") (d (list (d (n "cmdr_macro") (r "^0.3.11") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.3") (d #t) (k 0)))) (h "1bkhw18n7yi4k4vcgfcjsa3p620g7na4qmpr3caffksyv23d0alk")))

(define-public crate-cmdr-0.3.12 (c (n "cmdr") (v "0.3.12") (d (list (d (n "cmdr_macro") (r "^0.3.12") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "11fbr9p11hnnv10nf09n6pg9a0kiai29kvwfln71gcnbniyiarb8")))

