(define-module (crates-io cm dr cmdr_macro) #:use-module (crates-io))

(define-public crate-cmdr_macro-0.1.0 (c (n "cmdr_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "12wja8c358zc9jmhx2wfgl4rnwi6s22j9bns8nqrrvmh0kmmqkyj")))

(define-public crate-cmdr_macro-0.1.1 (c (n "cmdr_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1rnnx69pqv6wz7cgsvcyr5mrzy9l2aqihnkh5i2jjvlaf13zs62b")))

(define-public crate-cmdr_macro-0.1.2 (c (n "cmdr_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1idnshmf1nkkqb9k98k8i6y272aiank6qp427djp7qp4dmjfc7ix")))

(define-public crate-cmdr_macro-0.1.3 (c (n "cmdr_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1lbhpa0dx44n02m2sr3wfsx44dv5r28n91fcln49f41izbnsl6c1")))

(define-public crate-cmdr_macro-0.2.0 (c (n "cmdr_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1i2q3hxyg18w782phch3mi6bshh6jdlkfbxym7qzmz1bczfldm9p")))

(define-public crate-cmdr_macro-0.2.1 (c (n "cmdr_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1sl9hrr40nf3sn4czybmvagj1qyfb30r96kdc77l4zw37bga376f")))

(define-public crate-cmdr_macro-0.3.0 (c (n "cmdr_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1impzpfm42yhgqnx71rnwfpgs5h92s2j3qnsshdg90bv7h1cidac")))

(define-public crate-cmdr_macro-0.3.1 (c (n "cmdr_macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0fqrwa8lwybk4n84slk0m0i7mgv5vl1dmrzdy7v7sl1vf7fa61ga")))

(define-public crate-cmdr_macro-0.3.2 (c (n "cmdr_macro") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0613w09phhgz735cv816mdaqg0ik25293922hm0mxi5vk3vm6wkq")))

(define-public crate-cmdr_macro-0.3.3 (c (n "cmdr_macro") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1vgr11jjc7r7ld7zhpzvy3clawd10ljcxf9zvaxbnxzqa9pnz83a")))

(define-public crate-cmdr_macro-0.3.4 (c (n "cmdr_macro") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1m2ifps1l9ah3001s5y5vp1hg4khzw43rss3bggyi94v0gh6sax3")))

(define-public crate-cmdr_macro-0.3.5 (c (n "cmdr_macro") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "00s3fq1acg1mgbx0fsch2s5zzbk0wv0xbj3cgwk0cccqvxvqmcyz")))

(define-public crate-cmdr_macro-0.3.6 (c (n "cmdr_macro") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0cw9bajngzl0jxi9h085dl30sxrn44zcv5v9jiphyynr9l4nimgv")))

(define-public crate-cmdr_macro-0.3.7 (c (n "cmdr_macro") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1nimgv8i3h00lmd29if4d3apiqphcm676phh7lmcn5v2q641n883")))

(define-public crate-cmdr_macro-0.3.8 (c (n "cmdr_macro") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1sydnv0y12909rbrs9i3xcp17iyg4j6d054z6k3vpvcmvlk21i8j")))

(define-public crate-cmdr_macro-0.3.9 (c (n "cmdr_macro") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1p8q3y4yc3f45yawasdwlk7pjrjqm84ndc3fqb630bkfv6mlkx6c")))

(define-public crate-cmdr_macro-0.3.10 (c (n "cmdr_macro") (v "0.3.10") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1p5kw8casqi990i9cwn4jx76hi26ggbmfg8nl1vdgy3x2gv10cvw")))

(define-public crate-cmdr_macro-0.3.11 (c (n "cmdr_macro") (v "0.3.11") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "19r1csi8da1f6f7y6ziynvrlcv4v09jmxdvcdfs6yknhx6gywn53")))

(define-public crate-cmdr_macro-0.3.12 (c (n "cmdr_macro") (v "0.3.12") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1nqqhz3x3543n2i13hfv1q8z6ifk1xw7wlmxyy0m5ny51rpvnfkp")))

