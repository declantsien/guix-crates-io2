(define-module (crates-io cm ar cmark2jira) #:use-module (crates-io))

(define-public crate-cmark2jira-0.1.0 (c (n "cmark2jira") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.0.15") (k 0)))) (h "0hxm7mjyayzb3fw20syhvc8gi2l44xv748y779pjf95f29293qql")))

(define-public crate-cmark2jira-0.2.0 (c (n "cmark2jira") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.0.15") (k 0)))) (h "1bx2bzjvqi9pb5ca9jdqjfl6jwp6k42i773x582nvb422dgrlpyx")))

(define-public crate-cmark2jira-0.3.0 (c (n "cmark2jira") (v "0.3.0") (d (list (d (n "pulldown-cmark") (r "^0.0.15") (k 0)))) (h "01cz388pf0ngsjlqaq75q8nw68nw0l4y973bhwqvzgl8q0v7c99q")))

(define-public crate-cmark2jira-0.4.0 (c (n "cmark2jira") (v "0.4.0") (d (list (d (n "pulldown-cmark") (r "^0.0.15") (k 0)))) (h "1qb4wdr1p7xddpzmy8crn652zs2h9fz1srwsx1g3a9x5p0s6yz09")))

