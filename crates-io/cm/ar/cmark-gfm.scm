(define-module (crates-io cm ar cmark-gfm) #:use-module (crates-io))

(define-public crate-cmark-gfm-0.1.0 (c (n "cmark-gfm") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "cmark-gfm-sys") (r "^0.1.0") (d #t) (k 0)))) (h "032nz43alw4qdc57rphlr7i7hhy1id1f3zvkdhidkzwr0rsd0hpq") (y #t)))

(define-public crate-cmark-gfm-0.1.1 (c (n "cmark-gfm") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "cmark-gfm-sys") (r "^0.29.0") (d #t) (k 0)))) (h "0d5abwhp1nhsmjsjabm12s0g0lqbn9nw8zzmffjvgm1c6vjjicq1")))

