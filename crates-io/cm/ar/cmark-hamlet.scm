(define-module (crates-io cm ar cmark-hamlet) #:use-module (crates-io))

(define-public crate-cmark-hamlet-0.0.1 (c (n "cmark-hamlet") (v "0.0.1") (h "059r54p047ax2x1j874lj7jvjm6acgk8m55k8izfsr6anz8sbs4d")))

(define-public crate-cmark-hamlet-0.0.2 (c (n "cmark-hamlet") (v "0.0.2") (d (list (d (n "hamlet") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "0h1c4n133vwj831bdjxfhpm7sv6n6bykizpdlwk4gnlz2ykq334g")))

