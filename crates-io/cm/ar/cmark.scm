(define-module (crates-io cm ar cmark) #:use-module (crates-io))

(define-public crate-cmark-0.0.1 (c (n "cmark") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0q9la99rqqr1ddhjavbwcp7v33yb9nbb2qi1payy7ag0wlz4q10x")))

(define-public crate-cmark-0.0.2 (c (n "cmark") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0lz4184z9bnglydh1ac1713drxpyd6c4vjw4bv2caby8lyycj9r8")))

