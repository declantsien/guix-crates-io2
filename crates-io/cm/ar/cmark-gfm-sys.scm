(define-module (crates-io cm ar cmark-gfm-sys) #:use-module (crates-io))

(define-public crate-cmark-gfm-sys-0.1.0 (c (n "cmark-gfm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1mi01pf83awxdshky5lsw1jg0d4vnb0glad50kj727kfmhnsmdmj") (y #t)))

(define-public crate-cmark-gfm-sys-0.29.0 (c (n "cmark-gfm-sys") (v "0.29.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1lykcm9b6v8h31fsm85kjbx53md0fcn12xx9a6bmzndii1r82vpd")))

