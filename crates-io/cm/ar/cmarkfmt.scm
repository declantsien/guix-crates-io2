(define-module (crates-io cm ar cmarkfmt) #:use-module (crates-io))

(define-public crate-cmarkfmt-0.1.0 (c (n "cmarkfmt") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)))) (h "1mz0gvm57x5xcf8649c65g78i60ahldhivwqk12zn55qryxa1fic")))

(define-public crate-cmarkfmt-0.1.1 (c (n "cmarkfmt") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)))) (h "0ii21a249j5457way2qf9s750y0niqp64b5jxbsc16mvk7bi6y1f")))

(define-public crate-cmarkfmt-0.1.2 (c (n "cmarkfmt") (v "0.1.2") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)))) (h "10aqdil76isrqx59sh00vprgv2sg5496k1853a5nsb8h95c837ch")))

