(define-module (crates-io cm ar cmark-syntax) #:use-module (crates-io))

(define-public crate-cmark-syntax-0.1.0 (c (n "cmark-syntax") (v "0.1.0") (d (list (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)))) (h "0b4jsf53jyyhv8cx8ffxakgha0gcfa76dpiybmp9p5dg5zc78ga6")))

(define-public crate-cmark-syntax-0.1.1 (c (n "cmark-syntax") (v "0.1.1") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "ramhorns") (r "^0.13") (d #t) (k 2)))) (h "14yqi0zc4icah6lhr9s9z09z114yxl8ng4a8d7cfbb9nc4xdvhdv")))

(define-public crate-cmark-syntax-0.1.2 (c (n "cmark-syntax") (v "0.1.2") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "ramhorns") (r "^0.13") (d #t) (k 2)))) (h "1m5b2xdh03k0pracr8cqcnmrj5lwimryjc0hii4zviq5fyvgmfn9")))

(define-public crate-cmark-syntax-0.2.0 (c (n "cmark-syntax") (v "0.2.0") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "ramhorns") (r "^0.13") (d #t) (k 2)))) (h "0ivsl05s2sh3c18vcvbg82fzbih2p0wzrg0zr3kzxhmk5g3f119b")))

(define-public crate-cmark-syntax-0.2.1 (c (n "cmark-syntax") (v "0.2.1") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "ramhorns") (r "^0.13") (d #t) (k 2)))) (h "0xy9ghmnfsw4rpjkgvynn1p4xdvla2mlf399xycrvwqvhn1nkh1y")))

(define-public crate-cmark-syntax-0.2.2 (c (n "cmark-syntax") (v "0.2.2") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "ramhorns") (r "^0.13") (d #t) (k 2)))) (h "08zj46mmqfgz0bcm1mxb6i5iv773ary7mfbylkj9kjqds0j5kj5d")))

(define-public crate-cmark-syntax-0.2.3 (c (n "cmark-syntax") (v "0.2.3") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "ramhorns") (r "^0.13") (d #t) (k 2)))) (h "15irrg04ckmkq1aa46aad44vgb6476mrwpszkjgy2gd0xrsv6l64")))

(define-public crate-cmark-syntax-0.2.4 (c (n "cmark-syntax") (v "0.2.4") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "ramhorns") (r "^0.13") (d #t) (k 2)))) (h "08cz47jv0mwb19lis88459cwx35ddy788n727d5wa2w110llldvv")))

(define-public crate-cmark-syntax-0.3.0 (c (n "cmark-syntax") (v "0.3.0") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 0)))) (h "0p5yca3kl97sp5r6gdd7j2ifplbzmj97d4fl3m6h1nn5ck9259qf")))

(define-public crate-cmark-syntax-0.3.1 (c (n "cmark-syntax") (v "0.3.1") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (k 0)))) (h "0yarifv67m2grzybc6vywfd2bnmmy76816zzkpbvnvdrp1ah1953")))

(define-public crate-cmark-syntax-0.4.0 (c (n "cmark-syntax") (v "0.4.0") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (k 0)))) (h "0qbzq30kp0alp47d8qasq24pnnhhy2nnqi0n88p2scnqbyrw0gab") (y #t)))

(define-public crate-cmark-syntax-0.4.1 (c (n "cmark-syntax") (v "0.4.1") (d (list (d (n "latex2mathml") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (k 0)))) (h "1dgp899nq00afily4aixsz8wfhh9g55jmkqk6309l66zf5jjwza2")))

