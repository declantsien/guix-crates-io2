(define-module (crates-io cm im cmim) #:use-module (crates-io))

(define-public crate-cmim-0.1.0 (c (n "cmim") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)))) (h "18si3r1f5c2bc7299gf5g7cda8w2vllxcycb4fkl85y304byac9a")))

(define-public crate-cmim-0.1.1 (c (n "cmim") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)))) (h "0s8yxdf4vzjw7zs6v5slxgprq7fcf1vv5wbllwq8npy5mkl98fgv")))

(define-public crate-cmim-0.1.2 (c (n "cmim") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)))) (h "1fhxdhgj88cdcaqqn17950w8wlzm475kmwznnxqsgzpys2qniybm")))

(define-public crate-cmim-0.1.3 (c (n "cmim") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "nrf52832-hal") (r "^0.8") (f (quote ("rt"))) (d #t) (k 2)))) (h "12jpvk7c27n4szam648z6f9ak5zxmapafh4d3v54228iaa6r6knz")))

(define-public crate-cmim-0.2.0 (c (n "cmim") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "dwm1001") (r "^0.3.0") (f (quote ("dev" "rt"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "heapless") (r "^0.5.1") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)))) (h "1pkwxywfgvivjj04z92qd28s82jpn836smn9zz5n4z77pmsnlnrx")))

(define-public crate-cmim-0.2.1 (c (n "cmim") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "dwm1001") (r "^0.3.0") (f (quote ("dev" "rt"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "heapless") (r "^0.5.1") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)))) (h "0rw9j3pj3vpsqyhb6n3c423zkzw2fbzc0a2s0vvd8zs098cri1ny")))

