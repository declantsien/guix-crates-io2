(define-module (crates-io cm dp cmdparse) #:use-module (crates-io))

(define-public crate-cmdparse-0.1.0 (c (n "cmdparse") (v "0.1.0") (d (list (d (n "cmdparse-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "18gb8rwl34zsxbx1321fc89gfhqzal6x4nzk6dzdly2drbbyza5x")))

(define-public crate-cmdparse-0.1.1 (c (n "cmdparse") (v "0.1.1") (d (list (d (n "cmdparse-derive") (r "=0.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "043mpk7h16qjpvddli9z0r2ydnb968sm5mkw0i7wji95l54kxdsx")))

