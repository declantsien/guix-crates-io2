(define-module (crates-io cm dp cmdparse-derive) #:use-module (crates-io))

(define-public crate-cmdparse-derive-0.1.0 (c (n "cmdparse-derive") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "extra-traits" "full"))) (d #t) (k 2)))) (h "0j4k9v9gks80kfbffnhk4his433faaviyim9p6lnsw3z7rd9nvdp")))

(define-public crate-cmdparse-derive-0.1.1 (c (n "cmdparse-derive") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "extra-traits" "full"))) (d #t) (k 2)))) (h "121a4p6c9fhvnvlg3jd71qhzv9dx1nhbpsclphfpbmnw24bkcifq")))

