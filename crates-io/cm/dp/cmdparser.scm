(define-module (crates-io cm dp cmdparser) #:use-module (crates-io))

(define-public crate-cmdparser-0.1.0 (c (n "cmdparser") (v "0.1.0") (h "07fqfphcs94p71j11ja7r9309lm87hr4s5nrrlc2v9775ipz6hb4")))

(define-public crate-cmdparser-0.1.1 (c (n "cmdparser") (v "0.1.1") (h "0nq4vbc3gi13byrpgzz8cm5wfdpa0jbimdjj5c6apnh0lbh82m8r")))

(define-public crate-cmdparser-0.2.0 (c (n "cmdparser") (v "0.2.0") (h "0jfv8s8wfpd75dn04is907hvw979dmyhl3k8l411mhswsbglwnx8")))

