(define-module (crates-io cm dp cmdpack) #:use-module (crates-io))

(define-public crate-cmdpack-0.1.0 (c (n "cmdpack") (v "0.1.0") (d (list (d (n "extlog") (r "^0.1.4") (d #t) (k 0)))) (h "199s3spba4m21y6clggh7n29pc6khk3dic7hwghw68sm6frgwwav") (r "1.59.0")))

(define-public crate-cmdpack-0.1.2 (c (n "cmdpack") (v "0.1.2") (d (list (d (n "extlog") (r "^0.1.4") (d #t) (k 0)))) (h "15rswvaiymivapmw2l68qjxyg1lz44b00b6fc6rxj20mrrld6114") (r "1.59.0")))

