(define-module (crates-io cm dp cmdpiped) #:use-module (crates-io))

(define-public crate-cmdpiped-0.1.0 (c (n "cmdpiped") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-test") (r "^0.1") (d #t) (k 2)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "actix-web-actors") (r "^4") (d #t) (k 2)) (d (n "actix-ws") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("process" "io-std"))) (d #t) (k 0)) (d (n "tokio-process-stream") (r "^0.3.0") (d #t) (k 0)))) (h "02bzp94wah6r6b7rsb1yw0pmycydkk51zvxprikph75fvlbi6hw5")))

