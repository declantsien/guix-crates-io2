(define-module (crates-io cm ds cmdshelf) #:use-module (crates-io))

(define-public crate-cmdshelf-2.0.1 (c (n "cmdshelf") (v "2.0.1") (d (list (d (n "is_executable") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "057yxw35mkamww4irr80if8zrq1cz941wp91jl23pxbscc8x2321")))

(define-public crate-cmdshelf-2.0.2 (c (n "cmdshelf") (v "2.0.2") (d (list (d (n "is_executable") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "03scgm54xmyfbzv6lgy5cbd29k3lwx7hj4h6ywpqik2icklcb1d9")))

