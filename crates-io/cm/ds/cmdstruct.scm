(define-module (crates-io cm ds cmdstruct) #:use-module (crates-io))

(define-public crate-cmdstruct-1.0.0 (c (n "cmdstruct") (v "1.0.0") (d (list (d (n "cmdstruct-macros") (r "^1.0.0") (d #t) (k 0)))) (h "0282crcg7ksb84100k3rqmpiwqb2p7m3y38yxyym5dp91qb5xcvb")))

(define-public crate-cmdstruct-1.1.0 (c (n "cmdstruct") (v "1.1.0") (d (list (d (n "cmdstruct-macros") (r "^1.0.0") (d #t) (k 0)))) (h "0a0vmmm224dabfqclg2dwhbp2mwj88a8c3i7xbfxphznmz49ixgk")))

(define-public crate-cmdstruct-1.2.0 (c (n "cmdstruct") (v "1.2.0") (d (list (d (n "cmdstruct-macros") (r "^1.1.0") (d #t) (k 0)))) (h "10ni04fvwclh4khq7ygr9zw23b7qh5s6k7gs6iqw57igghfp5jf4")))

(define-public crate-cmdstruct-2.0.0 (c (n "cmdstruct") (v "2.0.0") (d (list (d (n "cmdstruct-macros") (r "^2.0.0") (d #t) (k 0)))) (h "1md4m1rilh9pk3jfqb0inljqzvp6dvngrr1vzl7m1qjqskbwfghi")))

(define-public crate-cmdstruct-2.0.1 (c (n "cmdstruct") (v "2.0.1") (d (list (d (n "cmdstruct-macros") (r "^2.0.0") (d #t) (k 0)))) (h "138k12ld7jykvi4xs0zkmmd7kmjmsq1m1mvqmcpfx0b0gbsqsfcw")))

