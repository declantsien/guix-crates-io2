(define-module (crates-io cm ds cmdstruct-macros) #:use-module (crates-io))

(define-public crate-cmdstruct-macros-1.0.0 (c (n "cmdstruct-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0kciv1ydcqp4190rkladank0kw3isa1h4dn664748x9r1w0r8657")))

(define-public crate-cmdstruct-macros-1.1.0 (c (n "cmdstruct-macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "17f31cmslyrmqjrf34b41hfnsbf0f0ah1k9w8ig4yiyhbnwxx8hc")))

(define-public crate-cmdstruct-macros-1.2.0 (c (n "cmdstruct-macros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "18ksg4zb7h14pjb9wraph4dsxfk01r0vq6mf81sbz36v6s7kk88k")))

(define-public crate-cmdstruct-macros-2.0.0 (c (n "cmdstruct-macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "03jxbdsg0p4a51r9hp9fcaziml7mhdhrrak5brz3q0bkc02pivys")))

(define-public crate-cmdstruct-macros-2.0.1 (c (n "cmdstruct-macros") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1w9k0xn6pwlszcsbcxzjqyqwvji50lpghv53g9fnhwp0xq2myvkj")))

