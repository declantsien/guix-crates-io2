(define-module (crates-io cm si cmsis_dsp_sys_pregenerated) #:use-module (crates-io))

(define-public crate-cmsis_dsp_sys_pregenerated-0.1.0 (c (n "cmsis_dsp_sys_pregenerated") (v "0.1.0") (d (list (d (n "ureq") (r "^2.0.1") (d #t) (k 1)) (d (n "zip") (r "^0.5.9") (f (quote ("deflate"))) (k 1)))) (h "02nj19fk25srpg36h3mi4dhqv7vmk9if3ylw4hjm8vmimha0g3n4") (f (quote (("dsp-instructions") ("double-precision-fpu") ("cortex-m7")))) (l "arm_cortex_math")))

