(define-module (crates-io cm si cmsis-dsp-sys) #:use-module (crates-io))

(define-public crate-cmsis-dsp-sys-0.1.0 (c (n "cmsis-dsp-sys") (v "0.1.0") (h "17pxwm0d7sqb4jjgzw2hklhn4v9ygrhj04axh665siagrcyyzh5s") (l "cmsis-dsp")))

(define-public crate-cmsis-dsp-sys-0.2.0 (c (n "cmsis-dsp-sys") (v "0.2.0") (h "0f4s5q50khavcsfch7179k27j2knd2h2fnh7wwrkbnd0krz73941") (l "cmsis-dsp")))

(define-public crate-cmsis-dsp-sys-0.3.0 (c (n "cmsis-dsp-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "default-tls"))) (k 1)) (d (n "zip") (r "^0.5.12") (f (quote ("deflate"))) (k 1)))) (h "0ap7y96bazjdq3ci20ynw9bvrip3x4gb4l8s8x5g4kx0w97ikb4i") (l "cmsis-dsp")))

(define-public crate-cmsis-dsp-sys-0.3.1 (c (n "cmsis-dsp-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "default-tls"))) (k 1)) (d (n "zip") (r "^0.5.12") (f (quote ("deflate"))) (k 1)))) (h "1jappvdldrib1zzqn7vk8xyiskj14jgz80w75g1f09778nc0lzj5") (l "cmsis-dsp")))

