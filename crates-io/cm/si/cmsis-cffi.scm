(define-module (crates-io cm si cmsis-cffi) #:use-module (crates-io))

(define-public crate-cmsis-cffi-0.4.0 (c (n "cmsis-cffi") (v "0.4.0") (d (list (d (n "cmsis-pack") (r "^0.4.0") (d #t) (k 0)) (d (n "ctor") (r "^0.1.15") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)))) (h "0mc9a7f5my5jnfhpzx7ir2nmwk349p7iqq74a8scy0097swsl70y")))

(define-public crate-cmsis-cffi-0.5.0 (c (n "cmsis-cffi") (v "0.5.0") (d (list (d (n "cmsis-pack") (r "^0.5.0") (d #t) (k 0)) (d (n "ctor") (r "^0.1.15") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "044wvbvxqvglhq77dfavl79qz1sih3yf9m2rly4psxx90nkgfzr1")))

(define-public crate-cmsis-cffi-0.6.0 (c (n "cmsis-cffi") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.56") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "cmsis-pack") (r "^0.6.0") (d #t) (k 0)) (d (n "ctor") (r "^0.1.15") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "1wddds43jz1b3aba3fbwiiskk20p9skhj6vqgcnpvzybxbqci940")))

(define-public crate-cmsis-cffi-0.6.1 (c (n "cmsis-cffi") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.56") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "cmsis-pack") (r "^0.6.1") (d #t) (k 0)) (d (n "ctor") (r "^0.1.15") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (f (quote ("termcolor"))) (k 0)))) (h "1yszcmvs97sykl538qm8j8z5mz5bixjdr0x1g533ivsnldq6hzhx")))

(define-public crate-cmsis-cffi-0.6.2 (c (n "cmsis-cffi") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.56") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "cmsis-pack") (r "^0.6.2") (d #t) (k 0)) (d (n "ctor") (r "^0.1.15") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (f (quote ("termcolor"))) (k 0)))) (h "105jwyvbfq7hkdjbynv20fclj8m851b8il27byyi0r69xb5pw2zd")))

(define-public crate-cmsis-cffi-0.6.3 (c (n "cmsis-cffi") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0.56") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "cmsis-pack") (r "^0.6.2") (d #t) (k 0)) (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (f (quote ("termcolor"))) (k 0)))) (h "108ybsbysq06k22n99xklj4j80z9pkpsls6bfwnm8r260x7pf9v9")))

(define-public crate-cmsis-cffi-0.7.0 (c (n "cmsis-cffi") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.56") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "cmsis-pack") (r "^0.7.0") (d #t) (k 0)) (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (f (quote ("termcolor"))) (k 0)))) (h "039mii2ylrzgc6q6pwrx07a569hd4qgfrspapvrr0830pa7kgfhy")))

