(define-module (crates-io cm si cmsis_dsp) #:use-module (crates-io))

(define-public crate-cmsis_dsp-0.1.0 (c (n "cmsis_dsp") (v "0.1.0") (d (list (d (n "cmsis_dsp_sys_pregenerated") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.2.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "micromath") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.3.0") (k 0)))) (h "094x094a4qhln2fnpdcbwifnf5p94z0imb9b9i1ix2ylm2grhkq7") (f (quote (("dsp-instructions" "cmsis_dsp_sys_pregenerated/dsp-instructions") ("double-precision-fpu" "cmsis_dsp_sys_pregenerated/double-precision-fpu") ("cortex-m7" "cmsis_dsp_sys_pregenerated/cortex-m7"))))))

