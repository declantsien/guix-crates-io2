(define-module (crates-io cm pb cmpb) #:use-module (crates-io))

(define-public crate-cmpb-2.3.2 (c (n "cmpb") (v "2.3.2") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)))) (h "0sdafc5x9s3inknvhrmp023sibnjgrjh0jb2xwyxv4fcp9f716kg")))

