(define-module (crates-io cm ak cmake_tui) #:use-module (crates-io))

(define-public crate-cmake_tui-1.0.0 (c (n "cmake_tui") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cassowary") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.10.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1yrj1260677nmb7w8s1gra79d978j30n8lb8klcwa5sww4hfqf26")))

