(define-module (crates-io cm ak cmake-parser-derive) #:use-module (crates-io))

(define-public crate-cmake-parser-derive-0.1.0-alpha.1 (c (n "cmake-parser-derive") (v "0.1.0-alpha.1") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1gn1khs9svg4z1dlhqb829ldj9fqvxc42yq2h4lddy21ihf2n1dj")))

