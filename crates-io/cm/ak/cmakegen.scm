(define-module (crates-io cm ak cmakegen) #:use-module (crates-io))

(define-public crate-cmakegen-0.1.0 (c (n "cmakegen") (v "0.1.0") (h "18qv1x36ypfl2gv3y1x9xv4v5wy0by2s458zkr607sdhkb33jgq5")))

(define-public crate-cmakegen-0.1.1 (c (n "cmakegen") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1kj5r3qa27qgqz2x01i2j4i55njigv5lmjs47vigqygghl25v5cq")))

