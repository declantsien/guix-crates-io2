(define-module (crates-io cm ak cmake-parser) #:use-module (crates-io))

(define-public crate-cmake-parser-0.1.0-alpha (c (n "cmake-parser") (v "0.1.0-alpha") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0f0lbk17nmm1sa9gnv0qi7hs7vdxs3pwwagxinfhyq2crbxxjrnx")))

(define-public crate-cmake-parser-0.1.0-alpha.1 (c (n "cmake-parser") (v "0.1.0-alpha.1") (d (list (d (n "check_keyword") (r "^0.2") (d #t) (k 2)) (d (n "cmake-parser-derive") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18bglsy9bh77hw7ilbhhy0c6xfv2zzhw1mxwh3n86nb016i3a159")))

