(define-module (crates-io cm ak cmakefmt) #:use-module (crates-io))

(define-public crate-cmakefmt-0.1.0-alpha.0 (c (n "cmakefmt") (v "0.1.0-alpha.0") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "0nsda6xndmqxy5n3nk9b3dxxk442lvh1ldh98gdzknbdmgx3391z")))

(define-public crate-cmakefmt-0.1.0 (c (n "cmakefmt") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "1s0w0mh53jpllyqr69gm6i5rplyqmip3hkfh3vvww4737zhslpkp")))

(define-public crate-cmakefmt-0.1.1 (c (n "cmakefmt") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "022zjf6yrqdvxz3v3hbvs14v1kd105py67nxz89gxnlppfsgrkfx")))

(define-public crate-cmakefmt-0.1.2 (c (n "cmakefmt") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "0ndg1rvwxfclfyaprygkdjgzh9cqn57xkddn5sf3g7zr0k9ir4x5")))

(define-public crate-cmakefmt-0.1.3 (c (n "cmakefmt") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "1lq3qf74jjk0jjp2c2b73whkngbqmf02c1gl6qqxf7zksyb8mlqw")))

(define-public crate-cmakefmt-0.1.4 (c (n "cmakefmt") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "17nn9ibfng78zcnaariywx339za3s1l2ghs97h4gc80fdgjxk8z6")))

(define-public crate-cmakefmt-0.1.5 (c (n "cmakefmt") (v "0.1.5") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "1dir3if3marilk4570x2rpjzy3nsxz5178kcl1rxp2hj4vhx1zp2")))

(define-public crate-cmakefmt-0.1.6 (c (n "cmakefmt") (v "0.1.6") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "0fph52gz3a65h3kh2p9hmzg44nfdypl4gkm7bxx3qb0awva9i0cq")))

(define-public crate-cmakefmt-0.1.7 (c (n "cmakefmt") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "0sg6i6ynxshxzs0pp6pxybs1293la07giq0cxpgni5vzha9qp3s4")))

(define-public crate-cmakefmt-0.1.8 (c (n "cmakefmt") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "0qjki1bgsjbd01gp0iisvkgw33pn0fzx8ras5fkjb70ri0wbrfv1")))

(define-public crate-cmakefmt-0.1.9 (c (n "cmakefmt") (v "0.1.9") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "0lwm343nslbkqq1i51gb9hasls9xdllf9c45pp56nl8bl699jbw4")))

(define-public crate-cmakefmt-0.1.10 (c (n "cmakefmt") (v "0.1.10") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "1nyq6xfcj5lrc3kcrhfc4azy1ijgyhf4pgd1qrbsn8sbw9ks9q8c")))

(define-public crate-cmakefmt-0.1.11 (c (n "cmakefmt") (v "0.1.11") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty") (r "^0.12.3") (d #t) (k 0)))) (h "1hjhnis26dhzszihgls23q0khvg7ax6bz31czmfshbxi1a20fgl5")))

