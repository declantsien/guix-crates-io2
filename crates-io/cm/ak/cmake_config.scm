(define-module (crates-io cm ak cmake_config) #:use-module (crates-io))

(define-public crate-cmake_config-0.1.0 (c (n "cmake_config") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0hd1zy83g162wprr30fsnh5z0k3xnhqyswz9fzvp1mr0hp5g18nr")))

(define-public crate-cmake_config-0.1.1 (c (n "cmake_config") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1qnsi2m4rb1rw0piz44sj9zx4sa2c602g3d5xmf25wl1jxzz0w71")))

