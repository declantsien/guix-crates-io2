(define-module (crates-io cm ak cmake) #:use-module (crates-io))

(define-public crate-cmake-0.1.0 (c (n "cmake") (v "0.1.0") (h "1xnva9w22bysrri4pscrav3f4avgdj6jh47l10d997n4pia5gj5q")))

(define-public crate-cmake-0.1.1 (c (n "cmake") (v "0.1.1") (h "0brhjr93q84ffd9prmm0clcgx62ir923d6xqv7i624lb7sf9mhd3")))

(define-public crate-cmake-0.1.2 (c (n "cmake") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.11") (d #t) (k 0)))) (h "0frl1c2vizkhpz7kjp9rhsrxsc19rwk8ngzrb70mi62x4s0n0yy5")))

(define-public crate-cmake-0.1.3 (c (n "cmake") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3.11") (d #t) (k 0)))) (h "002kgi4091mabzdrx4w2cbmshvms2bllrpmbfwifvqiwcxm7y0sw")))

(define-public crate-cmake-0.1.4 (c (n "cmake") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3.11") (d #t) (k 0)))) (h "0a76rw8y1va6r0mr0wknzy0sqwrh2l79g84gl33w4k6xl4c6hsvv")))

(define-public crate-cmake-0.1.5 (c (n "cmake") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "18cjnnfgw60i92fv0fv02k2k72cc6lsn567igkdmicdcfcmi10yq")))

(define-public crate-cmake-0.1.6 (c (n "cmake") (v "0.1.6") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "1gvlj15i8mn7wnfp1wrfz3pvxll33f3s98z1w18flr3h02yfz7ya")))

(define-public crate-cmake-0.1.7 (c (n "cmake") (v "0.1.7") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "19wbxfdc0di35z2wc8rhp502843n8i53pkfw3iq7r1nwbgspfwjb")))

(define-public crate-cmake-0.1.8 (c (n "cmake") (v "0.1.8") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "1chvbjq2xcb9g3wzvcsf41jsgb5ah2739x2x915nq028wvv2sd2r")))

(define-public crate-cmake-0.1.9 (c (n "cmake") (v "0.1.9") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "1fbaq3wpj6fqmq4k99k1qzxngrkyiq0cg6w5qncybpsjvzpyngi0")))

(define-public crate-cmake-0.1.10 (c (n "cmake") (v "0.1.10") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "151h87if31rjn2z23lfj0i7rqmvch9r4i803yh2bprrr3z69zvpv")))

(define-public crate-cmake-0.1.11 (c (n "cmake") (v "0.1.11") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "145mk3xq7hmcscf66zwialq4j78fhdysp1wly9wia897w1x7cimn")))

(define-public crate-cmake-0.1.12 (c (n "cmake") (v "0.1.12") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "0wxzb7mml552qwv70l58zh68h6ri747swl94pj2iwx0sn15qjyxb")))

(define-public crate-cmake-0.1.13 (c (n "cmake") (v "0.1.13") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "1bi4cfvm91xy9smyzyb8f903w8rhf2ssycj22bzzbp70scq47c6a")))

(define-public crate-cmake-0.1.14 (c (n "cmake") (v "0.1.14") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "0dsbyja9c12ixly9d94ihin83mh5a0dw6jgsc9wb9aarzl944540")))

(define-public crate-cmake-0.1.15 (c (n "cmake") (v "0.1.15") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "1a0cwxy4xhjcixn6rjqwvh3p90ynfc6hfprrjmbp0lsgssm5aryj")))

(define-public crate-cmake-0.1.16 (c (n "cmake") (v "0.1.16") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "0ch5cpmn0n0y0zjgvh1mwfj5rpcbqhz94yf55bshm3jznv9shqgb")))

(define-public crate-cmake-0.1.17 (c (n "cmake") (v "0.1.17") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "0y29v47g6gdmps30vqmp41wfkpybkng50hm0iqxrbvsnrv75pkyz")))

(define-public crate-cmake-0.1.18 (c (n "cmake") (v "0.1.18") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "10zjplmhk2sxlimqll782sjxgwwdv576ads4vz0q98cpw0kwynqf")))

(define-public crate-cmake-0.1.19 (c (n "cmake") (v "0.1.19") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "0am8c8ns1h6b1a5x9z2r1m3rszvya5nccl2pzszzjv5aiiaydgcf")))

(define-public crate-cmake-0.1.20 (c (n "cmake") (v "0.1.20") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "01q4gxdwwac26jql3b37ir68dyyn7bh856nd3dy7w24myrfq19m3")))

(define-public crate-cmake-0.1.21 (c (n "cmake") (v "0.1.21") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "098vjjgpggs0iykcrqnzc2x891d20sbx1xgr72pjfiki7y5cdb71")))

(define-public crate-cmake-0.1.22 (c (n "cmake") (v "0.1.22") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 0)))) (h "1b3n7san1c4b0ghag97fh98ny1x4pxckd4g7qgf1diflgsc6i3fi")))

(define-public crate-cmake-0.1.23 (c (n "cmake") (v "0.1.23") (d (list (d (n "gcc") (r "^0.3.46") (d #t) (k 0)))) (h "08hif3z30kvpbk8phbkziyv90r1spfhhgrw9zifggj0jjjvqw9wj")))

(define-public crate-cmake-0.1.24 (c (n "cmake") (v "0.1.24") (d (list (d (n "gcc") (r "^0.3.48") (d #t) (k 0)))) (h "07xg162qzf9k871hmashsdjv4yfb3bg373qnjyad176wscsvpsxq")))

(define-public crate-cmake-0.1.25 (c (n "cmake") (v "0.1.25") (d (list (d (n "gcc") (r "^0.3.53") (d #t) (k 0)))) (h "0gcvi8iag0khg5zjqa4hjzr6v9m5078y5bjgvv9p5kavlm0nb2hc")))

(define-public crate-cmake-0.1.26 (c (n "cmake") (v "0.1.26") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "17bqdgdjplnv2bmr0fpnzif307qs3s8bbvf1jckp75gwl7khfz1m")))

(define-public crate-cmake-0.1.27 (c (n "cmake") (v "0.1.27") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "017nxg8j1zwanr7l3hn7ns2yvfd1gam08v8nywklw0y2rdiw03lv")))

(define-public crate-cmake-0.1.28 (c (n "cmake") (v "0.1.28") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1b2wnrin88rkc47h2lmk8gzz46p93s1lprbpas86lb5wgidd2k71")))

(define-public crate-cmake-0.1.29 (c (n "cmake") (v "0.1.29") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1fr8rrrnnl58zw0sf9ywjc2ni3vkykgvfdkbs3v7grb9gbm43msn")))

(define-public crate-cmake-0.1.30 (c (n "cmake") (v "0.1.30") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0z6s76ndw5m9914h25ys90d6sq7kbi338fnb02045ppdxg77ixjw")))

(define-public crate-cmake-0.1.31 (c (n "cmake") (v "0.1.31") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1cv72kd746my5ihjjmdzc7avjr8yvhd447rfpxr6swhwqcsh4iwm")))

(define-public crate-cmake-0.1.32 (c (n "cmake") (v "0.1.32") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "033mgrb9hbpwx1dqbjfbav67ynlcj829bx64y3sam8x1iy9j2s5m")))

(define-public crate-cmake-0.1.33 (c (n "cmake") (v "0.1.33") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1yzfqpa71m5nmadxpklr80pqvw514kmdnmdjmyqam78lnlxvykvh")))

(define-public crate-cmake-0.1.34 (c (n "cmake") (v "0.1.34") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "03n7yqx9hkbb8smd7mi0az7542906i75bi9873hz0j0glx7332w4")))

(define-public crate-cmake-0.1.35 (c (n "cmake") (v "0.1.35") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0jk1ph1yxbvzkll84jdsay395m3y8n9kdllia0rnzlf9z7j5xikf")))

(define-public crate-cmake-0.1.36 (c (n "cmake") (v "0.1.36") (d (list (d (n "cc") (r "^1.0.32") (d #t) (k 0)))) (h "155s0s33affg76p9l9hp9qsbpmxlgkqhxf0cj65ld8ybkvjj5i4k")))

(define-public crate-cmake-0.1.37 (c (n "cmake") (v "0.1.37") (d (list (d (n "cc") (r "^1.0.32") (d #t) (k 0)))) (h "01ickz92mxwan15n4zzmic305glrvb34hwjgf01frmi6y3xpj6y1")))

(define-public crate-cmake-0.1.38 (c (n "cmake") (v "0.1.38") (d (list (d (n "cc") (r "^1.0.32") (d #t) (k 0)))) (h "1j6fggcv675nwncflv6sbwh81ajf89lkg9jj0kygphsgagn0w8cn")))

(define-public crate-cmake-0.1.39 (c (n "cmake") (v "0.1.39") (d (list (d (n "cc") (r "^1.0.32") (d #t) (k 0)))) (h "0dk5m0vkqs7z83qspfkiwibqkgqq7m4dpxv4l0flbcbw88imbz6r")))

(define-public crate-cmake-0.1.40 (c (n "cmake") (v "0.1.40") (d (list (d (n "cc") (r "^1.0.32") (d #t) (k 0)))) (h "1w0zgqdbbhl9w6px7avc6d5p43clglrmjfdn2n26mdsli5n3i91c")))

(define-public crate-cmake-0.1.41 (c (n "cmake") (v "0.1.41") (d (list (d (n "cc") (r "^1.0.32") (d #t) (k 0)))) (h "0a9j1fch0w97mh0qavm5i9njmv3p8qjz9qsq3xwdc9givjbcb11w")))

(define-public crate-cmake-0.1.42 (c (n "cmake") (v "0.1.42") (d (list (d (n "cc") (r "^1.0.41") (d #t) (k 0)))) (h "0qkwibkvx5xjazvv9v8gvdlpky2jhjxvcz014nrixgzqfyv2byw1")))

(define-public crate-cmake-0.1.43 (c (n "cmake") (v "0.1.43") (d (list (d (n "cc") (r "^1.0.41") (d #t) (k 0)))) (h "1ajc2c9i8sn0ljc8zl9vsfb5q2n1hdn7xzd33b2sy1kr2ri7bya9")))

(define-public crate-cmake-0.1.44 (c (n "cmake") (v "0.1.44") (d (list (d (n "cc") (r "^1.0.41") (d #t) (k 0)))) (h "1fv346ipxmvff6qrnh78rild0s8k72ilfjkdsrk869562y62cmhf")))

(define-public crate-cmake-0.1.45 (c (n "cmake") (v "0.1.45") (d (list (d (n "cc") (r "^1.0.41") (d #t) (k 0)))) (h "0m8868gi7wrfszjk6qfmb8bgawywqsd5fbm1rnjgn78p6yv10qpb")))

(define-public crate-cmake-0.1.46 (c (n "cmake") (v "0.1.46") (d (list (d (n "cc") (r "^1.0.41") (d #t) (k 0)))) (h "1290ilzm0i09xva4wz8n24l5sp5flh4m4jmdmrjfdvv329a5if5p")))

(define-public crate-cmake-0.1.47 (c (n "cmake") (v "0.1.47") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1hh79zj121g44x49cg8kxzz5fwvjn0ph4s8myv5dbf7m2027cl1r")))

(define-public crate-cmake-0.1.48 (c (n "cmake") (v "0.1.48") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0fj4wxqdk8zzh1l0zrywpcx50a6jcj0j1wwxp1l7piaa23pqrbg8")))

(define-public crate-cmake-0.1.49 (c (n "cmake") (v "0.1.49") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0z3bqqi2gj9vx97zhcjnacwi41bi906zj5dj5rgp4c0b21p9ad6v")))

(define-public crate-cmake-0.1.50 (c (n "cmake") (v "0.1.50") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0c3i3548mqbizpgbff94jjgkcd2p6q9fxjjh89zzf5dqcfaph753")))

