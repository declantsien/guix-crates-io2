(define-module (crates-io cm ud cmudict) #:use-module (crates-io))

(define-public crate-cmudict-0.1.0 (c (n "cmudict") (v "0.1.0") (d (list (d (n "cmudict_core") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "indexed-line-reader") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0syzx4md6v5vdn95sy1z7vrp36aprlqhhb0s4cd3my24v3l3bif6")))

(define-public crate-cmudict-0.2.0 (c (n "cmudict") (v "0.2.0") (d (list (d (n "cmudict_core") (r "^0.1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "indexed-line-reader") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0nr5gjjp2gfz3y2g03n1jmqvpic3hb35rr5ci86w96sdzvhw6l9g")))

(define-public crate-cmudict-0.2.1 (c (n "cmudict") (v "0.2.1") (d (list (d (n "cmudict_core") (r "^0.1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "indexed-line-reader") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0i55cqxgibfs1c6i31iwz0fmw5nqv3yxx3a6093zinm7plrsj66m")))

(define-public crate-cmudict-0.3.0 (c (n "cmudict") (v "0.3.0") (d (list (d (n "cmudict_core") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "indexed-line-reader") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0rmhgdz9bshv05gr5i490f9skf6pl8xdr6b4bvvna4madlxah2s4")))

(define-public crate-cmudict-0.3.1 (c (n "cmudict") (v "0.3.1") (d (list (d (n "cmudict_core") (r "^0.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "indexed-line-reader") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "06852lnpiw7dnj72f6nayzv1zh4xh864j3kx5zqx4zvaanxk89iz")))

(define-public crate-cmudict-0.3.2 (c (n "cmudict") (v "0.3.2") (d (list (d (n "cmudict_core") (r "^0.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "indexed-line-reader") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0dlix55pq0ncpmjb2kvgswbva69p6gwqi88llb317z2sgw1p01y4")))

