(define-module (crates-io cm ud cmudict-fast) #:use-module (crates-io))

(define-public crate-cmudict-fast-0.4.0 (c (n "cmudict-fast") (v "0.4.0") (d (list (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "05x1h6fpky1167syf193vflpdhnx2ilg4m3mgaa0dd1pcmvhc9kg")))

(define-public crate-cmudict-fast-0.4.1 (c (n "cmudict-fast") (v "0.4.1") (d (list (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "19g1l2b6016l7y6xqx6zj69wig6ddqwv1fmxqp67ibqkm5pil9fv")))

(define-public crate-cmudict-fast-0.5.0 (c (n "cmudict-fast") (v "0.5.0") (d (list (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0zcfrnx4chhr1nbkdlj5zy2c781wr7pxphyczxdjkdy8p6k5g9k4")))

(define-public crate-cmudict-fast-0.6.0 (c (n "cmudict-fast") (v "0.6.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1870yv1h77dg40w6y1yr4jkcqhfys8xqrcr71r9i3cyc7izj3j55")))

(define-public crate-cmudict-fast-0.7.0 (c (n "cmudict-fast") (v "0.7.0") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "12vwjhsfh2qg859krw1n6j9h72fjc697snn1j52rfabm853538yc") (f (quote (("serialization" "serde"))))))

(define-public crate-cmudict-fast-0.7.1 (c (n "cmudict-fast") (v "0.7.1") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "13sj4m6f5q91yck7bgpnvany6wy0lyz60cc58gbp1gbh5kvhn80d") (f (quote (("serialization" "serde"))))))

(define-public crate-cmudict-fast-0.7.2 (c (n "cmudict-fast") (v "0.7.2") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1nd4hw2yvr8az62pb6mch5yh7w3vx1b1n0391lxq3gy2crdyvz98") (f (quote (("serialization" "serde"))))))

(define-public crate-cmudict-fast-0.8.0 (c (n "cmudict-fast") (v "0.8.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "00nwfq18cjab12phhpr9ad0ngj0j5dnl1mvz7rnd93lj9q0777rc") (f (quote (("serialization" "serde"))))))

