(define-module (crates-io cm ud cmudict_core) #:use-module (crates-io))

(define-public crate-cmudict_core-0.1.0 (c (n "cmudict_core") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)))) (h "1lmkcd1zkxw3wn7i0rraa8m1iryfj727k0q1xilzm68wjlawj31w")))

(define-public crate-cmudict_core-0.1.1 (c (n "cmudict_core") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)))) (h "0wvylfpdsldvbapdk29ggyh9nn9kf576ikg45zcgbk359avmpdaq")))

(define-public crate-cmudict_core-0.2.0 (c (n "cmudict_core") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0581s8kpjmrsfwlhf5nyq67z8jid40xnnrykf3lmgrvgnwy0j10f")))

(define-public crate-cmudict_core-0.2.1 (c (n "cmudict_core") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0n17f3ch30vsvqlf8jscr9ccr5hb3jl217pll4015g5vv9cr51gz")))

(define-public crate-cmudict_core-0.2.2 (c (n "cmudict_core") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1rjsnwgb05m3hdn58x7q1gbiw6k47zxwnfz6r90zw5q298v9l882")))

