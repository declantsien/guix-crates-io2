(define-module (crates-io cm d- cmd-derive) #:use-module (crates-io))

(define-public crate-cmd-derive-0.1.0 (c (n "cmd-derive") (v "0.1.0") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ala52acap4a2rq8nak0lckb36ji1lr9k0zjx33jv8j4lcrig197")))

(define-public crate-cmd-derive-0.1.1 (c (n "cmd-derive") (v "0.1.1") (d (list (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "derive" "extra-traits"))) (k 0)))) (h "0h7ajwq2rl90phawf3hda37q45aqx46rzrxj5i85svw486gl6bpp")))

