(define-module (crates-io cm d- cmd-proc-macro) #:use-module (crates-io))

(define-public crate-cmd-proc-macro-0.1.2 (c (n "cmd-proc-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1a7lh923z6qgc2hgs3pwph2a6pi462a31lym31dyc96jsxx180d0") (y #t)))

(define-public crate-cmd-proc-macro-0.1.3 (c (n "cmd-proc-macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.20") (f (quote ("full"))) (d #t) (k 0)))) (h "11vfkibspws0nvwpqrm0gm890bx8nhr52a38axris3z57rlf5rsz") (y #t)))

(define-public crate-cmd-proc-macro-0.1.4 (c (n "cmd-proc-macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.20") (f (quote ("full"))) (d #t) (k 0)))) (h "17nbskny5hsprb00v69vjm816npwmpvf121diy8n1pgd9406fr90")))

