(define-module (crates-io cm d- cmd-args) #:use-module (crates-io))

(define-public crate-cmd-args-0.1.0 (c (n "cmd-args") (v "0.1.0") (h "0md7bi1qh5gkswbmmzmc4z7nx8qfx4ki14frxkchnhywlaagscwh")))

(define-public crate-cmd-args-0.2.0 (c (n "cmd-args") (v "0.2.0") (h "15rwdfwh7xayc4rm4x7brmi97paj1faj4cp27bxibnxr42rrgyf2")))

