(define-module (crates-io cm d- cmd-wrapped) #:use-module (crates-io))

(define-public crate-cmd-wrapped-0.2.0 (c (n "cmd-wrapped") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "01ir1lkd0jz6r1mvsrnzcw8di03rqpl1vf68b29gwlfkys4nlvbs")))

(define-public crate-cmd-wrapped-0.3.0 (c (n "cmd-wrapped") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dl0yzxwzkdlvi2zs16qkd9k849kx0x8d7yajly7pfh8zn35gsv1")))

