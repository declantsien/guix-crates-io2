(define-module (crates-io cm d- cmd-macro) #:use-module (crates-io))

(define-public crate-cmd-macro-0.1.0 (c (n "cmd-macro") (v "0.1.0") (d (list (d (n "cmd-derive") (r "^0.1.0") (d #t) (k 0)))) (h "14a2im90apdms2m6a7s6gc8i5d9qyanhvydg2j35pnh3kwfdpi98")))

(define-public crate-cmd-macro-0.1.1 (c (n "cmd-macro") (v "0.1.1") (d (list (d (n "cmd-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1pkfhc4dffx87pllynyivp52z6n7q5rhkwbg2k09c2ghz1j9bn6n")))

(define-public crate-cmd-macro-0.1.2 (c (n "cmd-macro") (v "0.1.2") (d (list (d (n "cmd-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wq7qx5j2jmhh8yar6kyzir1y6pfqvvjdc3sr762k9cyrlbrgm7s")))

