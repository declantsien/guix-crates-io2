(define-module (crates-io cm d- cmd-pandoc) #:use-module (crates-io))

(define-public crate-cmd-pandoc-0.1.0 (c (n "cmd-pandoc") (v "0.1.0") (h "10785n6ik9fj12lnf6sh3j9qjlh7nmpxl2fhl9dykr1pvs2ccrs5")))

(define-public crate-cmd-pandoc-0.2.0 (c (n "cmd-pandoc") (v "0.2.0") (h "0lkp7zk4gj2vl0zqqm2i3zg0704i2iq4796d3jd7bqkb65ggdl8n")))

