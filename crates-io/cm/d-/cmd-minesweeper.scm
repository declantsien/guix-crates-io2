(define-module (crates-io cm d- cmd-minesweeper) #:use-module (crates-io))

(define-public crate-cmd-minesweeper-0.1.0 (c (n "cmd-minesweeper") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1g00vh3jvyh51vqvdv7spf80wchjm46n9p1hpm6bm0cd63b6nq3r")))

(define-public crate-cmd-minesweeper-0.1.1 (c (n "cmd-minesweeper") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "09zc4k2mklp1nnyvy8969by43pkv6sa6zwfsr8l6cp18pi2gk8vd")))

(define-public crate-cmd-minesweeper-0.1.2 (c (n "cmd-minesweeper") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1xdnfxszlrw72jw303dw6dvmagwkqnn7vr5nhwp5mng3ibysgcw3")))

(define-public crate-cmd-minesweeper-0.1.3 (c (n "cmd-minesweeper") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1b933md5x75gr0n53w7yra3l8676izln7xg9xvkvyymw84wg04b6")))

