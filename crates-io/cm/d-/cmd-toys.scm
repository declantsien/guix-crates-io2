(define-module (crates-io cm d- cmd-toys) #:use-module (crates-io))

(define-public crate-cmd-toys-0.1.0 (c (n "cmd-toys") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11znr6r6n85wxjzvjs3d007k0jlj5rgdz80q9x9lsmnh99hs6ad1")))

(define-public crate-cmd-toys-0.1.1 (c (n "cmd-toys") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1pfwimpsms2qf6sfcpga7mmwziisl6640gcf3bv1z0wjg32fmc33")))

(define-public crate-cmd-toys-0.1.2 (c (n "cmd-toys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1sacr4nbvp4v0bc6jlncqbdv1hqyma8knna01v60miz25833nm72")))

(define-public crate-cmd-toys-0.1.3 (c (n "cmd-toys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0680scb3x2k45f0qak5l4phcx7v4vzfya0iqsw59virb89fs1mv6")))

(define-public crate-cmd-toys-0.1.6 (c (n "cmd-toys") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wh4m86va1m053y24pnkdmq8dndxjxnvzswd61kskjhw0s4cgmds")))

