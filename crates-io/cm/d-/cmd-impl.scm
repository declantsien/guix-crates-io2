(define-module (crates-io cm d- cmd-impl) #:use-module (crates-io))

(define-public crate-cmd-impl-0.9.0-rc13 (c (n "cmd-impl") (v "0.9.0-rc13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s3xq6k8rnj8f42b2nq3j7mrk8080a36qxx705mcmmk0pk44vkyc") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.0-rc14 (c (n "cmd-impl") (v "0.9.0-rc14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "030dscn9zv823vjrjnc3ysx8z7byv1yva84mskkay5iv4nlrzaf0") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.0-rc15 (c (n "cmd-impl") (v "0.9.0-rc15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19vwchg8q7d2nxrbw79x20dgz35inwbpb4vrrzgpxaphm5jh8c9c") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.0-rc16 (c (n "cmd-impl") (v "0.9.0-rc16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0la7x0l2iab61396ycsvwnxnpzxkv288iy4lwdj2vf1cvpvld79l") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.0-rc17 (c (n "cmd-impl") (v "0.9.0-rc17") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0imr05fbxyi5kz332wbx7qhh5c0wr2kdwfbg7xc5mdjv08fm6aj2") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.0-rc18 (c (n "cmd-impl") (v "0.9.0-rc18") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "136hs2sy7298rxkra235pl4d40273amyp7rsp785gk1319k262i6") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.0-rc19 (c (n "cmd-impl") (v "0.9.0-rc19") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0as3kp2hflw2by09sj5aan6kl250hngp2rsn8zi8klp38v81r6z8") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.0-rc20 (c (n "cmd-impl") (v "0.9.0-rc20") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y0zywkxqi2gf0b86a1k3k71qhwqn8afs4inc61b4q1r5frca7vx") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.0 (c (n "cmd-impl") (v "0.9.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pvlnaz59ls0xcqljvkcdn5yf02adzh2k5cl0skhwb4nal2y0n03") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.1 (c (n "cmd-impl") (v "0.9.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jinzr85g6gskjry573i49vq9bai334ixh5c4q7n67qh08kf5ps3") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.2 (c (n "cmd-impl") (v "0.9.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p38idcr8nm552kj7paz9gg5zlpk9rxb42x9iwpbiga424wxy09l") (f (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9.3 (c (n "cmd-impl") (v "0.9.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0alsiq8r7qhb2r72jn11z81zq7r3q4wvfdhscyvnjwfnphj60qwm") (f (quote (("debug" "syn/extra-traits"))))))

