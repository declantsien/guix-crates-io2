(define-module (crates-io cm d- cmd-utils) #:use-module (crates-io))

(define-public crate-cmd-utils-0.1.0 (c (n "cmd-utils") (v "0.1.0") (h "1i6ab5m9g7v9677yw8wys3kqy509rsw8r57ra7am11xq5krv0phd")))

(define-public crate-cmd-utils-0.2.0 (c (n "cmd-utils") (v "0.2.0") (h "16bx3mdf390m40hmyl0whkwnqbd1jw4dy1vfxxj05m3kck24gjjp")))

(define-public crate-cmd-utils-0.3.0 (c (n "cmd-utils") (v "0.3.0") (h "0260m39zsbsdix482q1yrn36765l72qi94pss94sfav5ajymyr46")))

(define-public crate-cmd-utils-0.3.1 (c (n "cmd-utils") (v "0.3.1") (h "1fvxbqq3vdcd4s25s6lqaj539vnisn2i69wccjnhcs552ah6a55n")))

