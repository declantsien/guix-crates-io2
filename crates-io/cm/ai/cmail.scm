(define-module (crates-io cm ai cmail) #:use-module (crates-io))

(define-public crate-cmail-0.1.0 (c (n "cmail") (v "0.1.0") (d (list (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "chan-signal") (r "^0.1") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cara2ar893lk8irbfpm000jlxi05wpfp6jx6xi5y9kyjad49asl")))

(define-public crate-cmail-0.1.1 (c (n "cmail") (v "0.1.1") (d (list (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "chan-signal") (r "^0.1") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "03wvq3nhnmx7fqi1vlhfr1ks30rcqxdh1yn17ligaan7lzjgzfsb")))

(define-public crate-cmail-0.2.0 (c (n "cmail") (v "0.2.0") (d (list (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "chan-signal") (r "^0.1") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "159xy57z2v2r6k76ii73252c5pb8w1dij571x8m9zicq1jw2mjj8")))

(define-public crate-cmail-0.3.0 (c (n "cmail") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)))) (h "1s8b3nmnxaxzddwaic01fxf7bzklj74nbji6v5bfxdn9xc5pj0n5")))

