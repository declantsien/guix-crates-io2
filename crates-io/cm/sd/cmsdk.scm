(define-module (crates-io cm sd cmsdk) #:use-module (crates-io))

(define-public crate-cmsdk-0.0.1 (c (n "cmsdk") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cmpb") (r "^2.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "p256") (r "^0.13.2") (f (quote ("arithmetic" "ecdsa" "pem"))) (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0pk0smvd5f7rflcfsd3spq8z90bzivahk9srjl9gqcb12pi47gp9")))

