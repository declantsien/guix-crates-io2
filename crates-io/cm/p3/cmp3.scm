(define-module (crates-io cm p3 cmp3) #:use-module (crates-io))

(define-public crate-cmp3-1.0.0 (c (n "cmp3") (v "1.0.0") (h "1pq497syc54hfvlqq1zs77101kfi93qy1bypsggxa7axplnmnk0n") (y #t)))

(define-public crate-cmp3-1.0.1 (c (n "cmp3") (v "1.0.1") (h "1dl10h4v2k92ifybdaj2x8xjf3558ar1lz4363d59qzws6ws0lfv") (y #t)))

