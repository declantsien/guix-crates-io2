(define-module (crates-io cm pv cmpv2) #:use-module (crates-io))

(define-public crate-cmpv2-0.0.0 (c (n "cmpv2") (v "0.0.0") (h "1xl0j1did7fmq6aq4mxaf4c6c7lqs0j54nmrnijkr9brhwnjkv3b")))

(define-public crate-cmpv2-0.2.0-pre.0 (c (n "cmpv2") (v "0.2.0-pre.0") (d (list (d (n "const-oid") (r "^0.9") (f (quote ("db"))) (d #t) (k 2)) (d (n "crmf") (r "=0.2.0-pre.0") (d #t) (k 0)) (d (n "der") (r "^0.7") (f (quote ("alloc" "derive" "flagset" "oid"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "spki") (r "^0.7") (d #t) (k 0)) (d (n "x509-cert") (r "=0.2.0-pre.0") (k 0)))) (h "1mi2x9kpwjhba04p87di8p6vm772pfkdmxm5a6idp0s7nnlm2nsn") (f (quote (("std" "der/std" "spki/std") ("pem" "alloc" "der/pem") ("alloc" "der/alloc")))) (r "1.65")))

(define-public crate-cmpv2-0.2.0 (c (n "cmpv2") (v "0.2.0") (d (list (d (n "const-oid") (r "^0.9") (f (quote ("db"))) (d #t) (k 2)) (d (n "crmf") (r "^0.2") (d #t) (k 0)) (d (n "der") (r "^0.7") (f (quote ("alloc" "derive" "flagset" "oid"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "spki") (r "^0.7") (d #t) (k 0)) (d (n "x509-cert") (r "^0.2") (k 0)))) (h "0fbvg5fh7d7n5cdgnkmqvbih2r3fil91klli21dfw9bfcrd9a6wn") (f (quote (("std" "der/std" "spki/std") ("pem" "alloc" "der/pem") ("alloc" "der/alloc")))) (r "1.65")))

