(define-module (crates-io wc -g wc-grab) #:use-module (crates-io))

(define-public crate-wc-grab-0.1.0 (c (n "wc-grab") (v "0.1.0") (d (list (d (n "clap") (r "^2.22.0") (d #t) (k 0)) (d (n "dbus") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)))) (h "00pjibwh5c7cnydlb7pmv279vlmrh6ny2bw38g92zqnml0p0w6dg")))

(define-public crate-wc-grab-0.3.0 (c (n "wc-grab") (v "0.3.0") (d (list (d (n "clap") (r "^2.22.0") (d #t) (k 0)) (d (n "dbus") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)))) (h "0sij2qkmb8nvxgdzbisv0n7pmi9dl7kfwkc266rk9f0qwacwj327")))

