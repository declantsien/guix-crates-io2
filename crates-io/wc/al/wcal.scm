(define-module (crates-io wc al wcal) #:use-module (crates-io))

(define-public crate-wcal-0.1.0 (c (n "wcal") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1yxgc0dl1r6h6cb6w59rf27qcky034spnvv2z0rz1yshhjdy4fv5")))

(define-public crate-wcal-0.2.0 (c (n "wcal") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0a1l62vsi21jd6jzc9azmcy6r2ygqn2vig7mspdwdw9b4crz165y")))

