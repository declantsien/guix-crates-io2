(define-module (crates-io wc st wcstr) #:use-module (crates-io))

(define-public crate-wcstr-0.1.0 (c (n "wcstr") (v "0.1.0") (h "1m198b4zj66f29sjv8p9s3lds536xma32r0zy4n5zkjjn371ml0r")))

(define-public crate-wcstr-0.1.1 (c (n "wcstr") (v "0.1.1") (h "0hcv2xg0x6yr30imc0d18b8y5c0a1a3da9jwy7y340lffjyfnw1l")))

(define-public crate-wcstr-0.1.2 (c (n "wcstr") (v "0.1.2") (h "1c4fx6a3rry2fsd2z6a3zb4225r8wszhkai5pz4k5s5rhj0aahqb")))

(define-public crate-wcstr-0.1.3 (c (n "wcstr") (v "0.1.3") (h "0vd47l9ra4i6rbfmd1snbymishp0krkgjs0zhc624ip2z9wnxj31")))

(define-public crate-wcstr-0.1.4 (c (n "wcstr") (v "0.1.4") (h "0w57398l1fx0dlwkdx0lyqlfjhlbd5224n5kwg20wh8p99vzf9ry")))

