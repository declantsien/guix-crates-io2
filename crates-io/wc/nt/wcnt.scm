(define-module (crates-io wc nt wcnt) #:use-module (crates-io))

(define-public crate-wcnt-0.4.0 (c (n "wcnt") (v "0.4.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "id-arena") (r "^2.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1dxxhcb10m19k23f8cazqcvycdpw8sdwf854xy5ldacdygrrs9nk")))

