(define-module (crates-io wc wi wcwidth) #:use-module (crates-io))

(define-public crate-wcwidth-1.0.0 (c (n "wcwidth") (v "1.0.0") (h "0gwyfri8xqka7m22j1q4vyjpphf52xwfgpki431hvw3dnxyvas6m") (y #t)))

(define-public crate-wcwidth-1.0.1 (c (n "wcwidth") (v "1.0.1") (h "13i7vpiys24vz1lz83ycmxa37mf2bwxcr8f00ssd2a1n40yjmwyv") (y #t)))

(define-public crate-wcwidth-1.12.1 (c (n "wcwidth") (v "1.12.1") (h "159h4nc4s7qxqj7h3rjgwk29szia6kac3wii4a920ws1iqg9n1ji") (y #t)))

