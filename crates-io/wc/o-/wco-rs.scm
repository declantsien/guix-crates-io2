(define-module (crates-io wc o- wco-rs) #:use-module (crates-io))

(define-public crate-wco-rs-0.1.0 (c (n "wco-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0pn6qf0z60jxlzv2m5c62n6a4g7ymcxppal5m6bkfwg60j2iybwq")))

(define-public crate-wco-rs-0.1.1 (c (n "wco-rs") (v "0.1.1") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "091i87py4i13ksh6sklldha5i63jxfkpjrsrnk1k5f0j42jgnvr0")))

(define-public crate-wco-rs-0.1.2 (c (n "wco-rs") (v "0.1.2") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h8451fi4z61kab4cf362lcszlbbj3b6bcdizc5vq4h080zw63rx")))

