(define-module (crates-io wc an wcanvas) #:use-module (crates-io))

(define-public crate-wcanvas-0.0.1 (c (n "wcanvas") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.17.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "1s7la3b0ajinc0c2fqgz7wvgf8i59m2r22h54ih94dbz1h641gjr")))

