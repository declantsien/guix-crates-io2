(define-module (crates-io wc ur wcursorgen) #:use-module (crates-io))

(define-public crate-wcursorgen-0.1.0 (c (n "wcursorgen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "riff-ani") (r "^0.1.0") (d #t) (k 0)))) (h "0y6qk3lnsh39j8hdzdz9pkpn2s86i8863zq7w2a7ziraysfvhpfh")))

(define-public crate-wcursorgen-0.1.1 (c (n "wcursorgen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "riff-ani") (r "^0.1.0") (d #t) (k 0)))) (h "10hb46sk85z7sg4kliwl473zbxx1i6ynn42vppgb8jf1ngd0ssq6")))

