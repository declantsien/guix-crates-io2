(define-module (crates-io wc xh wcxhead) #:use-module (crates-io))

(define-public crate-wcxhead-0.1.0 (c (n "wcxhead") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "ntdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1llbiw2s5y35ss87c5spa7v4qxww4h86jwsmf0q0ysjphlc43hr6")))

