(define-module (crates-io wc ol wcolor) #:use-module (crates-io))

(define-public crate-wcolor-0.1.0 (c (n "wcolor") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "stringapiset" "winnls" "impl-default" "dcomp" "d2d1" "d2d1_2" "d3d11" "dxgi1_2" "dxgi1_3"))) (d #t) (k 0)) (d (n "wio") (r "^0.2") (d #t) (k 0)))) (h "1jj7ls31kf17axw727nbq88xnyp7w8pbnwkl1wnj363va24v5a1m")))

(define-public crate-wcolor-0.1.1 (c (n "wcolor") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "stringapiset" "winnls" "impl-default" "dcomp" "d2d1" "d2d1_2" "d3d11" "dxgi1_2" "dxgi1_3"))) (d #t) (k 0)) (d (n "wio") (r "^0.2") (d #t) (k 0)))) (h "1a3b91n5x8k85faynzxqnpky0q0dql4ws51vylhs2rkgcyjzq688")))

(define-public crate-wcolor-0.1.2 (c (n "wcolor") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "stringapiset" "winnls" "impl-default" "dcomp" "d2d1" "d2d1_2" "d3d11" "dxgi1_2" "dxgi1_3"))) (d #t) (k 0)) (d (n "wio") (r "^0.2") (d #t) (k 0)))) (h "17fy72zivbb0l2mp4553qglslzhqi85xlrv996f641bi3fc88f76")))

(define-public crate-wcolor-0.1.3 (c (n "wcolor") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "stringapiset" "winnls" "impl-default" "dcomp" "d2d1" "d2d1_2" "d3d11" "dxgi1_2" "dxgi1_3"))) (d #t) (k 0)) (d (n "wio") (r "^0.2") (d #t) (k 0)))) (h "0gxhlcbl77dg188csflqryvm5crpqjwgx73krrqj3pa87l5qzk32")))

(define-public crate-wcolor-0.1.4 (c (n "wcolor") (v "0.1.4") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "stringapiset" "winnls" "impl-default" "dcomp" "d2d1" "d2d1_2" "d3d11" "dxgi1_2" "dxgi1_3"))) (d #t) (k 0)) (d (n "wio") (r "^0.2") (d #t) (k 0)))) (h "0h1jjz8j5d91csc6d474wiglfzrz1bkxlzn5j0c9f3kdzxcnbnxk")))

(define-public crate-wcolor-0.1.5 (c (n "wcolor") (v "0.1.5") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "stringapiset" "winnls" "impl-default" "dcomp" "d2d1" "d2d1_2" "d3d11" "dxgi1_2" "dxgi1_3"))) (d #t) (k 0)) (d (n "wio") (r "^0.2") (d #t) (k 0)))) (h "1aqr1xbfh1zmpwq0yiaqh3mk8x38yryjr6wlynld7brx6mr0c1hj")))

