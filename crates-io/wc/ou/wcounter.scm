(define-module (crates-io wc ou wcounter) #:use-module (crates-io))

(define-public crate-wcounter-0.0.1 (c (n "wcounter") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1sddz6xwgn3x66fr4qsa65rp98g94pgwlfs68njqn3wigjwc1fzj")))

(define-public crate-wcounter-0.0.2 (c (n "wcounter") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "raw_sync") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "shared_memory") (r "^0.11.1") (d #t) (k 0)))) (h "0icj7iz9i4n34jsqn28h6ch6vdkpas0gmh5w1aivv5n8rljri1z8")))

(define-public crate-wcounter-0.2.1 (c (n "wcounter") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "raw_sync") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "shared_memory") (r "^0.11.1") (d #t) (k 0)))) (h "0zgrfrvl7ya3hybs9p4m2s05b4rch7qrgikqxw11fdfmw8x59rqc")))

(define-public crate-wcounter-0.2.2 (c (n "wcounter") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "fslock") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0mmk8ax8brla34yqajnndn4i1alq3df705vw54i58p5k6bgf9dzq")))

(define-public crate-wcounter-0.2.3 (c (n "wcounter") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "fslock") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0h1w8g09qjirhwk30vhgnv7jz804f4c8hywqiibwggl8j5jf0ag6")))

(define-public crate-wcounter-0.2.4 (c (n "wcounter") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "cluFlock") (r "^1.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "05h5myx5i4riqyf5x1fpjdxh389nn168nqmklckmj8hd2kp9ff3k")))

