(define-module (crates-io wc rs wcrs) #:use-module (crates-io))

(define-public crate-wcrs-0.1.0 (c (n "wcrs") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "13s6930hqp042fi70jz4n2f064j0ws5cc1kvpgz3dl3rfx9h1cja")))

(define-public crate-wcrs-0.2.0 (c (n "wcrs") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xn5vg3mkldj07hv9gzci51qi4y355f680094kr8bbyc1lq4zcxa")))

