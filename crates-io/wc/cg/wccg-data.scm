(define-module (crates-io wc cg wccg-data) #:use-module (crates-io))

(define-public crate-wccg-data-0.1.0 (c (n "wccg-data") (v "0.1.0") (d (list (d (n "bson") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.5.0") (f (quote ("write"))) (o #t) (d #t) (k 0)))) (h "07kk1frfyhqqwmqm3ba9xlqz5p079ikpk5x63grlv91ca8bpqnca") (f (quote (("write" "log" "bson" "wread-data-mongodb") ("read" "log" "bson" "wread-data-mongodb") ("models" "serde") ("default" "models"))))))

(define-public crate-wccg-data-0.1.1 (c (n "wccg-data") (v "0.1.1") (d (list (d (n "bson") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.5.0") (f (quote ("write"))) (o #t) (d #t) (k 0)))) (h "1bkbmiqyggdcllgwby5m2zdbpdxlbmqd3hwm8w8jxb41ai991jsl") (f (quote (("write" "log" "bson" "wread-data-mongodb") ("read" "log" "bson" "wread-data-mongodb") ("models" "serde") ("default" "models"))))))

(define-public crate-wccg-data-0.2.0 (c (n "wccg-data") (v "0.2.0") (d (list (d (n "bson") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "wccg-models") (r "^0.1.0") (f (quote ("activity"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.5.0") (f (quote ("write"))) (o #t) (d #t) (k 0)))) (h "1rkbwc5fi89xbmprw2mj4wikkjz80k0nkbcvmsix13w68jmihssf") (f (quote (("write" "log" "bson" "wread-data-mongodb") ("read" "log" "bson" "wread-data-mongodb") ("default" "read" "write"))))))

(define-public crate-wccg-data-0.2.1 (c (n "wccg-data") (v "0.2.1") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.2.1") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.5.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "0qah38pl8102a13i6viq0hz8dvbbx2hbkr266kczrpld1b9s8s3c") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.3.0 (c (n "wccg-data") (v "0.3.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.2.3") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.5.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "13prwp5vvjf64a5zznq9fb37syz771m6z3zrx159bl95g2ly5hgr") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.4.0 (c (n "wccg-data") (v "0.4.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.2.3") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.6.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1z86v14ykg055a42wpv8l26iq8ssrmyhah4m4nrl9y552jc96y92") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.4.1 (c (n "wccg-data") (v "0.4.1") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.2.3") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.6.1") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "0x974q14m3wim0zbris74pj0hgd4dbs4kfinrddl3hp3jlr2vv49") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.4.2 (c (n "wccg-data") (v "0.4.2") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.2.6") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.6.1") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "18pmcpg32vhvjjl8wdv20yd88sw5x9sjrcij59sxg287ll534gcw") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.4.3 (c (n "wccg-data") (v "0.4.3") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.2.6") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.7.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "15pwaxf5xbj0a1iy40vdi3a9k3pr7f358xqnd46h2f2f676gx8dc") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.4.4 (c (n "wccg-data") (v "0.4.4") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.2.6") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.7.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "14h4iwqsa5b203w39sxky6mp2iag4zqssxkydw2zhxy44nnbrv2g") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.5.0 (c (n "wccg-data") (v "0.5.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.1") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.7.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1jgbgl5nz1v9r0y6mvwrbhi034m56smavgdyvamkrd6v7w827ybw") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.5.1 (c (n "wccg-data") (v "0.5.1") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.1") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.7.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "0l9gp3i5aff55hl3dk5sgdp32986a83af088xrixcaizxjybfc10") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.5.2 (c (n "wccg-data") (v "0.5.2") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.1") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.7.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1c4fppfbj8jbiwqj99khdlzl7dnw3w6j4vrxfczcqmvp63l8xfjy") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.5.3 (c (n "wccg-data") (v "0.5.3") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.1") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.7.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "18lp8r0dq3dxrdnkyhhmw9ynyszirbnyvnzlagng8xf0yfgixf2x") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.5.4 (c (n "wccg-data") (v "0.5.4") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.2") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.7.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1m2nhy79n95f7xn1caqsj8vry8vybx994xf9vgpaw9g0byjqmzkz") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.5.5 (c (n "wccg-data") (v "0.5.5") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.2") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.7.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "03d723zq8866cjif47xsx5sklm470x95mcl8sc3vvx3gh5mlnw4c") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.0 (c (n "wccg-data") (v "0.6.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.2") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.8.0") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "184blg51g3mnlw6y0y5hgjw1rfg8b175lwj9fan2zffi7w4mv82w") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.1 (c (n "wccg-data") (v "0.6.1") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.2") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.8.1") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1b6bmcybzc88qq8d7nb5rhl4l9riwbw53d8kl51c1s1afch04hfb") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.2 (c (n "wccg-data") (v "0.6.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.3") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.2") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1wl1rlnlpxy5vh8zbwf7l4cyx1wgynxfhq5w8kk7m2vkrl5vqf6p") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.3 (c (n "wccg-data") (v "0.6.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.3") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.2") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "13zczjc0imfyi445balskcl7fhins9sz1lgzb273gf79rs7npzs2") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.4 (c (n "wccg-data") (v "0.6.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.4") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.2") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "16zcqv07iqzrc4lkv9w1gnjq5g9mwr3jala6rflfklxhjl9lzfjl") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.5 (c (n "wccg-data") (v "0.6.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.5") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.2") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "0a3m8cg78xbc8a9bagq37kcslkjr1pjwk3190qkv9cnsaip7qfij") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.6 (c (n "wccg-data") (v "0.6.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.5") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.2") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1d3b2n6iqlycr0dfkly5c680slclhmgv5iwi8sy5ss26wadjlfyr") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.7 (c (n "wccg-data") (v "0.6.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.5") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.2") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "01k5f6rrqrqwlpwfk8fzzgmbv23npdava6xpzgvr0in4irn696i1") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.8 (c (n "wccg-data") (v "0.6.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.6") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.3") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "0ix4mlpi3504fm3g79dl08laniispqzs1zmw7nbhvcfrz63arxib") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.9 (c (n "wccg-data") (v "0.6.9") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.8") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.3") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "0vm7h14y3ik9mbpy992kxd4f6sglkzbm1lp1n2152b533646c3ly") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.10 (c (n "wccg-data") (v "0.6.10") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.9") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.3") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1a95h72sbvrr96kcfdfrjz7ddvvywyrjqsfyd7ca5s560v48wqsy") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.6.11 (c (n "wccg-data") (v "0.6.11") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.3.10") (f (quote ("activity" "strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.3") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1hv9xkq09lkksb6bjyh1gq8iw4s7lv8jasvd10hdjk1f1bn07xlj") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.7.0 (c (n "wccg-data") (v "0.7.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.4.0") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.3") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1fh3syyl9ca5zrkyym6lqb019r9swywmjx43d8qrqbxxc86ssj96") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.7.1 (c (n "wccg-data") (v "0.7.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.4.0") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.3") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "1a40l3iwlcfxp42gj3wbpisjfbh3nk0sxyqdiwrdmg2a46h2swqd") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.7.2 (c (n "wccg-data") (v "0.7.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.4.4") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.3") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "0gpa4dq5m81f2g5c1s41iy2ypapvaa2vqx4zifyx58rdddl9wqnx") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.7.3 (c (n "wccg-data") (v "0.7.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.2") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.3") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "0xxz9sfaac25dz1jkzjj539pmn33nslrhilr45ral24kccbkp3f8") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.7.5 (c (n "wccg-data") (v "0.7.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.3") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.12.3") (f (quote ("read" "write"))) (d #t) (k 0)))) (h "15806ppbs7vx2y60r32bz0js46fvnsnbirzzblfa5qxvaq0j522r") (f (quote (("strava") ("default") ("activity"))))))

(define-public crate-wccg-data-0.8.0 (c (n "wccg-data") (v "0.8.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.4") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "1qach2gbkcp748474dzakkkbyd8m00gvm7ajp0mlhfvb6fxgp4xs")))

(define-public crate-wccg-data-0.8.1 (c (n "wccg-data") (v "0.8.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.5") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "11cqr8kzigkkkj6f31427s4jzvz4gf636bs6jfgj7hb6xnfg7b7f")))

(define-public crate-wccg-data-0.8.2 (c (n "wccg-data") (v "0.8.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.5") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "1x9a55zmpzs9k14ckl69l7wjj58i0lxjc0779w53f73nr0vibkjq")))

(define-public crate-wccg-data-0.8.3 (c (n "wccg-data") (v "0.8.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.5") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "0zasm9bl9xagdy0ls52f1w86vf9g2xxqv85ay64nr1ffjjzj0xgf")))

(define-public crate-wccg-data-0.8.4 (c (n "wccg-data") (v "0.8.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.5") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "0xyyxjzvyqg41dbvq0hczikd1s78zma79vwsrlppibfr3l1fapr6")))

(define-public crate-wccg-data-0.8.5 (c (n "wccg-data") (v "0.8.5") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.6") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "08r1p0hgv2ax29inim6dk44kc61cnc77qazz9aflpfwscdx8gzs8")))

(define-public crate-wccg-data-0.8.6 (c (n "wccg-data") (v "0.8.6") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.6") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "0x581vwhy2nybngwp1rm2js1dl0j4q5a6an3za1jr16vbrzv053n")))

(define-public crate-wccg-data-0.8.7 (c (n "wccg-data") (v "0.8.7") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.6") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "08h2cd49fydk92gbsyz48nkhy2qzx67si8mqs2dq3rhb6j5zk3hq")))

(define-public crate-wccg-data-0.8.8 (c (n "wccg-data") (v "0.8.8") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.5.6") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "1fkg8mqrzrkkkq7nkv87byfcarfc94bgq0mljmjq2qcnbbdvxmyp")))

(define-public crate-wccg-data-0.9.0 (c (n "wccg-data") (v "0.9.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.6.3") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "1nxamhzrwjr03mm8fjci72bhi93bp8kqm1jpdncwpbg8z9c342ky")))

(define-public crate-wccg-data-0.9.1 (c (n "wccg-data") (v "0.9.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.6.4") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "04x9gzfxghb29w5p1jicriy0jkg6ijb1a2j48y0ql8w4a3sh59sn")))

(define-public crate-wccg-data-0.9.2 (c (n "wccg-data") (v "0.9.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.6.5") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-data-mongodb") (r "^0.13.0") (d #t) (k 0)))) (h "030k6a2ibrvag27czacn5rk076zza01vigkj8zxxjz097pii6cvn")))

(define-public crate-wccg-data-0.10.0 (c (n "wccg-data") (v "0.10.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.7.0") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-mongodb") (r "^0.14.0") (d #t) (k 0)))) (h "05d935rx4b6gl9ddw4rd9qvcn35fm0r5im34w9390dvc9fnyqf5q")))

(define-public crate-wccg-data-0.10.1 (c (n "wccg-data") (v "0.10.1") (d (list (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "wccg-models") (r ">=0.7.1, <0.8.0") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-mongodb") (r ">=0.14.0, <0.15.0") (d #t) (k 0)))) (h "1rlkf4kcdrvl4yrajjfx8v99m0zabwh1ghyxy04mlk23gwah0pmj")))

(define-public crate-wccg-data-0.10.2 (c (n "wccg-data") (v "0.10.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wccg-models") (r "^0.7.2") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-mongodb") (r "^0.14.0") (d #t) (k 0)))) (h "0xnzkj86prkzmx689wa4ma027c5ahzrvwf4g88pq746jdzvzinr2")))

(define-public crate-wccg-data-0.11.0 (c (n "wccg-data") (v "0.11.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "razorpay") (r "^0.2.4") (d #t) (k 0)) (d (n "wccg-models") (r "^0.8.0") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-mongodb") (r "^0.14.0") (d #t) (k 0)))) (h "146zjrm644qb2bh3lwjq0klhzyca5wp6afaj4k3b22yamkn404vm")))

(define-public crate-wccg-data-0.11.1 (c (n "wccg-data") (v "0.11.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "razorpay") (r "^0.2.4") (d #t) (k 0)) (d (n "wccg-models") (r "^0.8.1") (f (quote ("strava"))) (d #t) (k 0)) (d (n "wread-mongodb") (r "^0.14.0") (d #t) (k 0)))) (h "0vakc6ah7ajixq60zdgpmbbjx6idm048m1j4r9xw26sj0xwkkr9l")))

