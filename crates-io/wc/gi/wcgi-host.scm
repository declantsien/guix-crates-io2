(define-module (crates-io wc gi wcgi-host) #:use-module (crates-io))

(define-public crate-wcgi-host-0.1.0 (c (n "wcgi-host") (v "0.1.0") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (k 0)) (d (n "wasmparser") (r "^0.98") (d #t) (k 0)) (d (n "wcgi") (r "^0.1.0") (d #t) (k 0)))) (h "1hk3hqw31ljl8rv2779s9z4vc88njd5vipklw18jw98ffi4izn4j") (f (quote (("schema" "schemars")))) (r "1.64.0")))

(define-public crate-wcgi-host-0.1.1 (c (n "wcgi-host") (v "0.1.1") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (k 0)) (d (n "wasmparser") (r "^0.95.0") (d #t) (k 0)) (d (n "wcgi") (r "^0.1.1") (d #t) (k 0)))) (h "1p64vyy2399ah9ispl7sdwprpdrx9v1n1ivgcjvlxc3nd8q14bp5") (f (quote (("schema" "schemars")))) (r "1.64.0")))

(define-public crate-wcgi-host-0.1.2 (c (n "wcgi-host") (v "0.1.2") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (k 0)) (d (n "wasmparser") (r "^0.95.0") (d #t) (k 0)) (d (n "wcgi") (r "^0.1.2") (d #t) (k 0)))) (h "1k4mnrikqni8838cyf7rpvwc1961k2in67arzfia52fk1qmwyqm7") (f (quote (("schema" "schemars")))) (r "1.64.0")))

