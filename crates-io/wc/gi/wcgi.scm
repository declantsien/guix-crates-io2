(define-module (crates-io wc gi wcgi) #:use-module (crates-io))

(define-public crate-wcgi-0.1.0 (c (n "wcgi") (v "0.1.0") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0lpyns36x8cxbi03l0wfl9zsm49jbv6gj8bq8467v8zj9yxp36an") (r "1.64.0")))

(define-public crate-wcgi-0.1.1 (c (n "wcgi") (v "0.1.1") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "02cxrl5fpnar5ndrw270av23kxcxw31xvwijmqkd1vbgzjbffm9w") (r "1.64.0")))

(define-public crate-wcgi-0.1.2 (c (n "wcgi") (v "0.1.2") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0xxzwaiia3yjmkqi3vhrsa8qcns19z3i59i5pnbq2fpc9qrqzjir") (r "1.64.0")))

