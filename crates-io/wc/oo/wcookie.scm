(define-module (crates-io wc oo wcookie) #:use-module (crates-io))

(define-public crate-wcookie-0.1.0 (c (n "wcookie") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1ns383rsari0hw1d903fibq854n50slkvjmi8k5nds424amqi5w5")))

(define-public crate-wcookie-0.1.1 (c (n "wcookie") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1a49vi041fh21s9df0zxqf088bqcp4kcijl2a31v03d4xcsal36b")))

(define-public crate-wcookie-0.1.2 (c (n "wcookie") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "003gimycvw6xhkgzqnmaz0zyzmq0k4137wffq2mfkqyz5zkz1aai")))

(define-public crate-wcookie-0.1.3 (c (n "wcookie") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1dgdcwrrw9lsmgj4gkv2rk7rpzhdkzvrvc38rspwvs1h4y75jc3h")))

