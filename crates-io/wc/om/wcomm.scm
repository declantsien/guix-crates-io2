(define-module (crates-io wc om wcomm) #:use-module (crates-io))

(define-public crate-wcomm-0.1.0 (c (n "wcomm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "fundsp") (r "^0.6.4") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "0v9n9qcws8g9g08m4wk77mnng8m3lmfzi6a3sr2sgqw0p92pylqg")))

(define-public crate-wcomm-0.1.1 (c (n "wcomm") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "fundsp") (r "^0.6.4") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "1hvalx5wgb4qrb099k8abxgkdnajr51vpx54g231sqfzmsy4f2a6")))

(define-public crate-wcomm-0.1.2 (c (n "wcomm") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "fundsp") (r "^0.6.4") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "1wlam1h9j15v7h8hk7zshm58ljml1ckcmhyp4xmnwd728j1hddgw")))

(define-public crate-wcomm-0.1.3 (c (n "wcomm") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "fundsp") (r "^0.6.4") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "0g3z4f2ya47avdl04knl77z2sbij4lgz6i3max3zachs4wx6dwlx")))

