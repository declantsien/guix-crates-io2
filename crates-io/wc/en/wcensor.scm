(define-module (crates-io wc en wcensor) #:use-module (crates-io))

(define-public crate-wcensor-0.1.0 (c (n "wcensor") (v "0.1.0") (d (list (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)) (d (n "wtools") (r "~0.1") (d #t) (k 0)))) (h "05m9kgzibfhdivyfl4k3qj01ck001k9rwnfy49wjvmqn0kyzj5qv")))

(define-public crate-wcensor-0.1.1 (c (n "wcensor") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "wtools") (r "~0.2") (d #t) (k 0)))) (h "1qmjb6saak1bd3r8ymj55q41wvhpycjjdxbackvpw69fywm591z0") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

