(define-module (crates-io wc or wcore) #:use-module (crates-io))

(define-public crate-wcore-0.2.0 (c (n "wcore") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0zc7l0vwgmawzw46ff2jvzi0r0xrk2qmc3v07p7cgaqd94jyppw4")))

(define-public crate-wcore-0.2.1 (c (n "wcore") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0b9lzrm0a0ywxkq7hlrzg24lx6cv8sv8rw98ijd112p7d9sa8g0c")))

(define-public crate-wcore-0.3.0 (c (n "wcore") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0zdfj37z432jj54hcyhydvfb8hqxmrgbmbsfgcfw5rpnp138vjbc")))

(define-public crate-wcore-0.3.1 (c (n "wcore") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0h874f79ky86mrnc6p1wv4xgfwzcv2zyd29zv8vk46id7l1y8965")))

(define-public crate-wcore-0.3.2 (c (n "wcore") (v "0.3.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "14n4kh1b8kz0zdi817v63n095w57vrxc9xvmqq59rldpplhc6sgg")))

(define-public crate-wcore-0.3.3 (c (n "wcore") (v "0.3.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0cyy1bc6y44ksw9vggjwabcsjvikr56syg92465qcq0kif5pjf29") (y #t)))

(define-public crate-wcore-0.3.4 (c (n "wcore") (v "0.3.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0rs9x303zj4kamdrx79rxhsw649rrwyq60ihmq2v7q6cwzlwyrxv")))

(define-public crate-wcore-0.3.5 (c (n "wcore") (v "0.3.5") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "1x8iydfzag1vk6g8jmxsmvplf9sxk6039hl357425hps1v9fkrrm")))

(define-public crate-wcore-0.4.0 (c (n "wcore") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "1lsqz756p5q6adwp32j3w4kdfjmbqmc3yss449li0yaadn1vcwan")))

(define-public crate-wcore-0.4.1 (c (n "wcore") (v "0.4.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "1da02xp15j5lrhq3kiwlcwrgav0y7l75dpisbw1fih9nj9rb0qk5")))

(define-public crate-wcore-0.4.2 (c (n "wcore") (v "0.4.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0jgmqdfpj16qh1cq9cjivqvrxb537v70h925czm7n3kwirpmmzsc")))

(define-public crate-wcore-0.4.3 (c (n "wcore") (v "0.4.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0f3cmq7bh8ha237fpagy11ikvd9aah1vs644m8fvlrf3ckv9db6j")))

(define-public crate-wcore-0.4.4 (c (n "wcore") (v "0.4.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0dpkaq1dj9km4m7znv4lia53j1yi19nw7ramcc8qr2m02jln6y6y")))

(define-public crate-wcore-0.5.0 (c (n "wcore") (v "0.5.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "1l4vhpy1wcw7wbqyy2pzh7rm1x7nf24rm73kp4dgcr80nmkjz5gy")))

(define-public crate-wcore-0.5.1 (c (n "wcore") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "1q4qll01p58dk5qpj4jdbl8mzrnvwhdj3ql4mch84a5wzs36by0y")))

