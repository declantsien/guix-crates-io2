(define-module (crates-io wc -b wc-bg) #:use-module (crates-io))

(define-public crate-wc-bg-0.3.0 (c (n "wc-bg") (v "0.3.0") (d (list (d (n "byteorder") (r ">= 0.3, < 0.6") (d #t) (k 0)) (d (n "clap") (r "^2.23.2") (d #t) (k 0)) (d (n "image") (r "^0.10.3") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 0)) (d (n "way-cooler-client-helpers") (r "^0.1") (d #t) (k 0)) (d (n "wayland-client") (r "^0.9.0") (f (quote ("cursor" "dlopen"))) (d #t) (k 0)) (d (n "wayland-scanner") (r "^0.9.1") (d #t) (k 1)) (d (n "wayland-sys") (r "^0.9.0") (f (quote ("client" "dlopen"))) (d #t) (k 0)))) (h "1k8v5914ir9hfj9686989m72vdq2185vblbykvr7g6p5kydki10x")))

