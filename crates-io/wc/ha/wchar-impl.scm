(define-module (crates-io wc ha wchar-impl) #:use-module (crates-io))

(define-public crate-wchar-impl-0.5.0 (c (n "wchar-impl") (v "0.5.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1ny6pzf3ml9n65f7idb0aiz48s9rq05gyfbvnnqrzl3w1qhzmj06")))

(define-public crate-wchar-impl-0.6.0 (c (n "wchar-impl") (v "0.6.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "116x2qs92sza4h7gbiws3qplchwz2kmy3z26ljzrky03jcmr4dgi")))

(define-public crate-wchar-impl-0.7.0 (c (n "wchar-impl") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1hcvlczjwf5z407dr0zqbax8d2fbw7q2chc95yccw80vwv27p91k")))

(define-public crate-wchar-impl-0.8.0 (c (n "wchar-impl") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "14pjs4jaiaca2brzwcr63s8wnq3fqas17pyy7f8hgy9l9jzj416j")))

(define-public crate-wchar-impl-0.9.0 (c (n "wchar-impl") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0rkjraii259v9ydgfy2v6rmgmsv3d27w97ra444d6wkxsg34n3zp") (f (quote (("nightly" "proc-macro2/nightly") ("default")))) (y #t)))

(define-public crate-wchar-impl-0.10.0 (c (n "wchar-impl") (v "0.10.0") (d (list (d (n "libc") (r "^0.2.94") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "16z36dfq9vkiyvp4p03y7mk6yxk04jzmrgq5d07nfv3rcgy313p9") (f (quote (("unstable" "proc-macro2/nightly") ("default"))))))

(define-public crate-wchar-impl-0.11.0 (c (n "wchar-impl") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "099xdbq7kfcbyz0sh6f969sbdzfh0hbfixbsnpdgj8gddwar6p07") (f (quote (("unstable" "proc-macro2/nightly") ("default"))))))

