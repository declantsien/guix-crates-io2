(define-module (crates-io wc ha wchar) #:use-module (crates-io))

(define-public crate-wchar-0.1.0 (c (n "wchar") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing"))) (k 0)))) (h "067qlllwn9558gw77bdfypqfvnjjx2z3xiih3qlxv0xhffc3j0a4")))

(define-public crate-wchar-0.1.1 (c (n "wchar") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing"))) (k 0)))) (h "0m9jgl29qxgh50gk8wa0bn18hw2v8ry6661qvjs8xqs9npvdhhsa")))

(define-public crate-wchar-0.2.0 (c (n "wchar") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing"))) (k 0)))) (h "1qmin0lnsw3bnlfdnqmb24fxkhm1nqsqjfn4rfbdj6wqyq9x5mhv")))

(define-public crate-wchar-0.5.0 (c (n "wchar") (v "0.5.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "wchar-impl") (r "^0.5.0") (d #t) (k 0)))) (h "0zxafwf0c3kypns2j9mink8s2bq20s16y656ifwrclbkq75hrkh8")))

(define-public crate-wchar-0.6.0 (c (n "wchar") (v "0.6.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "wchar-impl") (r "^0.6.0") (d #t) (k 0)))) (h "1rkg6sd6mb34x2hxcjngmfkky1al2zpqlikfdf5lb22vkmbzaksq")))

(define-public crate-wchar-0.6.1 (c (n "wchar") (v "0.6.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "wchar-impl") (r "^0.6") (d #t) (k 0)))) (h "1py274q4bcs8qnfq13dl77jqwkv7vmvwglxpn11gjsb5y45h2kf7")))

(define-public crate-wchar-0.7.0 (c (n "wchar") (v "0.7.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wchar-impl") (r "=0.7.0") (d #t) (k 0)))) (h "0b49cab9mr9ijgfynbipcn4c41k86601rlsc58687y28h56pylif")))

(define-public crate-wchar-0.8.0 (c (n "wchar") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wchar-impl") (r "^0.8.0") (d #t) (k 0)))) (h "18klicm338ws8gksjxz7kjrl4vhy6gb8k761hbrfxxy6fjgcrig6")))

(define-public crate-wchar-0.9.0 (c (n "wchar") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wchar-impl") (r "^0.9.0") (d #t) (k 0)))) (h "0bwjf9gdxg3jq1h9p86mj47g9p6ar98z4wqdppl2sn616gw2bizv") (f (quote (("nightly" "wchar-impl/nightly") ("default")))) (y #t)))

(define-public crate-wchar-0.10.0 (c (n "wchar") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.94") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wchar-impl") (r "^0.10.0") (d #t) (k 0)))) (h "14jcdyzyvfspmrh5ig2c35yqc94a259yhc4lf1x1imlifl17bcff") (f (quote (("unstable" "wchar-impl/unstable") ("default"))))))

(define-public crate-wchar-0.10.1 (c (n "wchar") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.94") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wchar-impl") (r "^0.10.0") (d #t) (k 0)))) (h "06dzd8hbrc6hmblfcrdvfjbdnln9d5rw53dkshd5zxh73lasg6rj") (f (quote (("unstable" "wchar-impl/unstable") ("default"))))))

(define-public crate-wchar-0.11.0 (c (n "wchar") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.94") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wchar-impl") (r "^0.11.0") (d #t) (k 0)))) (h "133xrpsm9j518l483av0i59h9vsin7plfvdaqdqn9rqp0fl6xjp1") (f (quote (("unstable" "wchar-impl/unstable") ("default"))))))

