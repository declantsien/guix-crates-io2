(define-module (crates-io rq ui rquickjs-cli) #:use-module (crates-io))

(define-public crate-rquickjs-cli-0.1.0 (c (n "rquickjs-cli") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "rquickjs") (r "^0.5.1") (d #t) (k 0)))) (h "1p88j3jjlqycwhj8l3y6c5jj9xpl53bqvczvydwr5ay2slydlf99")))

(define-public crate-rquickjs-cli-0.1.1 (c (n "rquickjs-cli") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "rquickjs") (r "^0.5.1") (f (quote ("bindgen"))) (d #t) (k 0)))) (h "17lx2avsdvcl2x926cmpmnb26sqf6pvn0lx4rmn07phbjv3xx2j5")))

