(define-module (crates-io rq _d rq_derive) #:use-module (crates-io))

(define-public crate-rq_derive-0.1.0 (c (n "rq_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "1lpzkkazwsrlb2yj3nkb1mj2r36jpvaz1kj7iczaks7a80gj1zsm")))

(define-public crate-rq_derive-0.1.2 (c (n "rq_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "0q49z4dz67dli9zb2kpfa8k9dmhbp3nyq4rjyfr0dipzga0gmyj0")))

