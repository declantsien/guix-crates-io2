(define-module (crates-io rq tt rqtt) #:use-module (crates-io))

(define-public crate-rqtt-0.1.0 (c (n "rqtt") (v "0.1.0") (d (list (d (n "async-std") (r "^1.2.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "bitreader_async") (r "^0.2.0") (d #t) (k 0)) (d (n "cargo-edit") (r "^0.3.3") (d #t) (k 0)) (d (n "mqtt_5") (r "^0.1.1") (d #t) (k 0)) (d (n "packattack") (r "^0.1.0") (d #t) (k 0)))) (h "033ssja64612rng0ski25ywlpzmzhdnxyaqx7iv9ngl5p3biv4kp")))

