(define-module (crates-io rq li rqlite) #:use-module (crates-io))

(define-public crate-rqlite-0.1.0 (c (n "rqlite") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3.0") (d #t) (k 0)))) (h "1vqah0nci7gdmlajmqfq9rkmrlz32cw3x9d44254ngrmzx4jsma2")))

