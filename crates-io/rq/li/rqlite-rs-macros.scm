(define-module (crates-io rq li rqlite-rs-macros) #:use-module (crates-io))

(define-public crate-rqlite-rs-macros-0.2.0 (c (n "rqlite-rs-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rqlite-rs-core") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1zm3f0w9byzvxmg8mf2ajvw6sa03y975d13qdjx7gcai48j9d6av")))

(define-public crate-rqlite-rs-macros-0.2.1 (c (n "rqlite-rs-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rqlite-rs-core") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0ap00ijsvqx3w5c4nxkrrb1mzs2vx2cispdd0sil2djkckhw07ff")))

(define-public crate-rqlite-rs-macros-0.2.2 (c (n "rqlite-rs-macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rqlite-rs-core") (r "^0.2.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "019sxj6k2j54w9mch7l74q6wyxal551n48aw2vigp3h9p9z75gxc")))

(define-public crate-rqlite-rs-macros-0.2.3 (c (n "rqlite-rs-macros") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rqlite-rs-core") (r "^0.2.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "19lw3n5jssjjxqn68scr55hzqw29z9fwm8lh2zf2al6mawawi5qr")))

(define-public crate-rqlite-rs-macros-0.2.4 (c (n "rqlite-rs-macros") (v "0.2.4") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rqlite-rs-core") (r "^0.2.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1j1n5kr1ilvnsf6wr1r2y5lhdl7pkrryk2m56b4dwv1fk0xwb0wy")))

(define-public crate-rqlite-rs-macros-0.2.5 (c (n "rqlite-rs-macros") (v "0.2.5") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rqlite-rs-core") (r "^0.2.6") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0qk8xgn1qbgbcnpds97lbjfqk94d674f1s665jcvpg787fgn6scb")))

(define-public crate-rqlite-rs-macros-0.2.6 (c (n "rqlite-rs-macros") (v "0.2.6") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "rqlite-rs-core") (r "^0.2.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0lwixhyfsmrla6hpfx14lsqd9v39pw5z67kzxyrl2appzc1sdq2i")))

(define-public crate-rqlite-rs-macros-0.2.7 (c (n "rqlite-rs-macros") (v "0.2.7") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "rqlite-rs-core") (r "^0.2.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0af1043a6v3xvr38h4f1qbiiksf36bxbakf18dxqs2iv2kgfqc1c")))

