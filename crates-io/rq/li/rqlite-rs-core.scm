(define-module (crates-io rq li rqlite-rs-core) #:use-module (crates-io))

(define-public crate-rqlite-rs-core-0.1.0 (c (n "rqlite-rs-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0yanj1818jlxnw8lv3yhx1bvzvid77hmqd86lrbfylg5x8i837jr")))

(define-public crate-rqlite-rs-core-0.2.0 (c (n "rqlite-rs-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "13cn3iz740ck8sc0b13h6632sgw269wjm070zs87c4fvrb52hhqx")))

(define-public crate-rqlite-rs-core-0.2.1 (c (n "rqlite-rs-core") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "19y69clwiq7shkrnm951qmqicsv6hdzlw3l89msbx9dmgrc7ld5j")))

(define-public crate-rqlite-rs-core-0.2.2 (c (n "rqlite-rs-core") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "046mqzdwbhk7ya8haf8yafksfv9iblvg1mk63jn4r8nc2k6csvvh")))

(define-public crate-rqlite-rs-core-0.2.3 (c (n "rqlite-rs-core") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1186pfk2liasjp7i924psyxjicz1cp55z5pjn9890wb9bd3x4dcl")))

(define-public crate-rqlite-rs-core-0.2.4 (c (n "rqlite-rs-core") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "01a3m90jqm19w6csklk6k56sm08skglscr2dmh16s7vcbjmsjk0s")))

(define-public crate-rqlite-rs-core-0.2.5 (c (n "rqlite-rs-core") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "1xj76ya9rl0i3n0548kdfnxjsip052vim7ljl6q7y97272qz1578")))

(define-public crate-rqlite-rs-core-0.2.6 (c (n "rqlite-rs-core") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0nrc1xqixyxk5xbibvh6xygqv4ywvj59y9q42r1bmck6pzy17sn1")))

(define-public crate-rqlite-rs-core-0.2.7 (c (n "rqlite-rs-core") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0rl0injxra3k5hrap1xab37gl3dincdhcqzxqj2c3wzlpvy0rk7n")))

(define-public crate-rqlite-rs-core-0.2.8 (c (n "rqlite-rs-core") (v "0.2.8") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "07v77s6yv3cr63r1xwnkn6kxmwximn50xihz5q1gygakg2q7461j")))

