(define-module (crates-io rq rr rqrr) #:use-module (crates-io))

(define-public crate-rqrr-0.1.0 (c (n "rqrr") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "g2p") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0rclvzbv2lmg3pjwfii0khkgawb3m0315kab5v8lw1aqymjvslmm") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.1.1 (c (n "rqrr") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "g2p") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1dv7lgws6zz88cnjq3p4pcgib848ipcpzdwjllvrj9xl7msav1qd") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.1.2-beta.1 (c (n "rqrr") (v "0.1.2-beta.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "g2p") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.1") (d #t) (k 0)))) (h "1aba9hxxls743n9c3c7l7gcfdpp2cxkkfl0mvnw5qkc8z8gmw0s6") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.2.0 (c (n "rqrr") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "g2p") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.1") (d #t) (k 0)))) (h "13vk3zggq6q6h5fxblprr4qgg55v11w46j7ah05qbvyikwq81afs") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.2.1 (c (n "rqrr") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "g2p") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.1") (d #t) (k 0)))) (h "0jfgn7bcwn67bj3pl4j327rf1k00i0f642g0ncybgj392pb0c47c") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.3.0 (c (n "rqrr") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "g2p") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)))) (h "16bsxz1k3n9fb6z3is8azcmyx59lc442pfmhln7fh6zdw6gc19wq") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.3.1 (c (n "rqrr") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "g2p") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)))) (h "094bvb48fjizp345kd8id9972pjlcx4r1gs7lns81sqildiak676") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.3.2 (c (n "rqrr") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "g2p") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)))) (h "1k84hjqnsjpzmz0lqlldmmqabignnbz88c3fwzdihvsf07d672p8") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.4.0 (c (n "rqrr") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "g2p") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)))) (h "0fynsjy87c0kg7q2syalv2llyx7xs0ldh8x316wss81vym3rk9vg") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.5.0 (c (n "rqrr") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "g2p") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.24.0") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)))) (h "0xsylhj2mzh0qzk9xz992gkihnl101yn96ivbyffdxxh5fn74q6x") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.5.1 (c (n "rqrr") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "g2p") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.24.0") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)))) (h "1qbrj04kfd31pbcmc0xsnz9f4yq1kbf5pk2px2pqciscp1lr9db0") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.5.2 (c (n "rqrr") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "g2p") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.24.0") (o #t) (d #t) (k 0)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)))) (h "0adic7s4jcz5fgbar6sfn6ly5bj4v90vvcwl208wsn981nqfavq9") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.6.0 (c (n "rqrr") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "g2p") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (o #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "lru") (r "^0.9") (d #t) (k 0)))) (h "02wa9gnjrzxzpkcpry6ginvb45wn7wqgs83yqykb36znz78qg2sa") (f (quote (("img" "image") ("default" "img"))))))

(define-public crate-rqrr-0.7.0 (c (n "rqrr") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "g2p") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.25") (o #t) (k 0)) (d (n "image") (r "^0.25") (d #t) (k 2)) (d (n "lru") (r "^0.12") (d #t) (k 0)))) (h "09i85nc5fkkr7kd8aqns8zjlphah7p22278wfba25vp337y9s9as") (f (quote (("img" "image") ("default" "img")))) (r "1.67.1")))

(define-public crate-rqrr-0.7.1 (c (n "rqrr") (v "0.7.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "g2p") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.25") (o #t) (k 0)) (d (n "image") (r "^0.25") (d #t) (k 2)) (d (n "lru") (r "^0.12") (d #t) (k 0)))) (h "0xc58l2l4rj8q2n6z0ylp6yckwywbsxz32jcma32zsvb5r1x035d") (f (quote (("img" "image") ("default" "img")))) (r "1.74.0")))

