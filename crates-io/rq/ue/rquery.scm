(define-module (crates-io rq ue rquery) #:use-module (crates-io))

(define-public crate-rquery-0.1.0 (c (n "rquery") (v "0.1.0") (d (list (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "1vb95n8avbp50n8grxkm7shqr5nw5gfgcn122ia9asxnkvk8m0is")))

(define-public crate-rquery-0.1.1 (c (n "rquery") (v "0.1.1") (d (list (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "1716ri4dgxyzf02z77idgfpq8vn2gcwk0h4m3x29dxxpip7s8pxn")))

(define-public crate-rquery-0.1.2 (c (n "rquery") (v "0.1.2") (d (list (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "1s6pw4a0gzfy8h4hi47j13c2fc0qncbfjvr7lxicp5vsf65npxqf")))

(define-public crate-rquery-0.2.0 (c (n "rquery") (v "0.2.0") (d (list (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "0p1nd2sisall2xadvg5b8n3xnsv3a70wi4z8805kciy8wzi80m1l")))

(define-public crate-rquery-0.2.1 (c (n "rquery") (v "0.2.1") (d (list (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "163nvvzbqivpjz6lhdcq0jjihxp210akix2nzhs89f70vi7y16fs")))

(define-public crate-rquery-0.3.0 (c (n "rquery") (v "0.3.0") (d (list (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "185j7lzvhd9ygzz5kcjraai0bgvfcaad90scqyip0dnlyl0zvpg0")))

(define-public crate-rquery-0.3.1 (c (n "rquery") (v "0.3.1") (d (list (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1wk82lxshkgg8sj4lw97bzmlbrv3p4rhqw66x9fqjb8pgirfba7h")))

(define-public crate-rquery-0.3.2 (c (n "rquery") (v "0.3.2") (d (list (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1bdfb1gi2idl4vx7fin9nlc882g86v3p1xkwlqrgm4czalqpgb3s")))

(define-public crate-rquery-0.4.0 (c (n "rquery") (v "0.4.0") (d (list (d (n "xml-rs") (r "^0.4") (d #t) (k 0)))) (h "00npdr15x1ayvyrara6b7kyk59y3c10nzamfq92m4adqbb5cwks6")))

(define-public crate-rquery-0.4.1 (c (n "rquery") (v "0.4.1") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "0lnd4gsmpw5c8wbm7lbnfjdzhbwqldi1y677plgxa0nynbg8m1ap")))

