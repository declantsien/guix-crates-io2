(define-module (crates-io rq pu rqpush) #:use-module (crates-io))

(define-public crate-rqpush-0.4.0 (c (n "rqpush") (v "0.4.0") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0852cql3rh0m6s91kzim8n580ybmj7kvkjrk9k7ivd4qsdj6v61q")))

(define-public crate-rqpush-0.4.1 (c (n "rqpush") (v "0.4.1") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1w250laqjdr88g972763nzspzqi6j8vyh8gkv8ym2rmkj27iq7zm")))

(define-public crate-rqpush-0.4.2 (c (n "rqpush") (v "0.4.2") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0226x06167xj3ksyfhi3qivz6rwagd0j7dk4xgpjflh6fcj5ia1i")))

(define-public crate-rqpush-0.4.3 (c (n "rqpush") (v "0.4.3") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1r5bd23l99k3aqnzfw7m2hws347px972psjvhjdh6906bss04lvb")))

(define-public crate-rqpush-0.4.4 (c (n "rqpush") (v "0.4.4") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1057ji4qkmh9krilbayqhps13lff2hn4mkqmvjiysr1568smmgqc")))

(define-public crate-rqpush-0.4.5 (c (n "rqpush") (v "0.4.5") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1r4rkyjk0ni36avsifzipqf56d259ckhnw4rwbd4y1adi8qy688a")))

(define-public crate-rqpush-0.4.6 (c (n "rqpush") (v "0.4.6") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0mdg3hxa8hxrxb2p08x5307zx07sznyykz8gpvi84dkk0hv8gm67")))

(define-public crate-rqpush-0.4.7 (c (n "rqpush") (v "0.4.7") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1anxpmy6g2cj3219rc2ykbhdpfddyj1x9px4hfq0d6fwmm7s6km5")))

(define-public crate-rqpush-0.4.8 (c (n "rqpush") (v "0.4.8") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "10ckmm7f4by5gs8lil6n15v4rm7rgcbmjmz2dfad3gdzcph0jbfz")))

(define-public crate-rqpush-0.4.9 (c (n "rqpush") (v "0.4.9") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1srdsv427n54j0jlfv7lag989mbfi0vn41s6sfkazwfgpakbw6yc")))

