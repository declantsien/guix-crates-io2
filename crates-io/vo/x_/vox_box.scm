(define-module (crates-io vo x_ vox_box) #:use-module (crates-io))

(define-public crate-vox_box-0.1.0 (c (n "vox_box") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "hound") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^1.0") (d #t) (k 0)) (d (n "sample") (r "^0.2") (d #t) (k 0)))) (h "019b3wb6x5687rvl7i8fs35rxyb95za2pzh46hw632hn8fz2q0a1")))

(define-public crate-vox_box-0.2.0 (c (n "vox_box") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^1.0") (d #t) (k 0)) (d (n "sample") (r "^0.6.1") (d #t) (k 0)))) (h "08zk0906n61qli3lgj9vk96zxp5gr4vm6r7q2h2xy4ggpx435mhz")))

(define-public crate-vox_box-0.2.1 (c (n "vox_box") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^1.0") (d #t) (k 0)) (d (n "sample") (r "^0.6.1") (d #t) (k 0)))) (h "15gh9zglll194bwnawv907m8zmb5b6dimf0n5wai2y6wlp9h6qs1") (f (quote (("nightly"))))))

(define-public crate-vox_box-0.2.2 (c (n "vox_box") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^1.0") (d #t) (k 0)) (d (n "sample") (r "^0.6.1") (d #t) (k 0)))) (h "0w12i64khwnjbsrbxyrh5y9cg09bh4b26rg6kx8k11hmia5647wg") (f (quote (("nightly"))))))

(define-public crate-vox_box-0.2.3 (c (n "vox_box") (v "0.2.3") (d (list (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^1.0") (d #t) (k 0)) (d (n "sample") (r "^0.6.1") (d #t) (k 0)))) (h "0lk3w1px6s2m8xgriilay9wajca1n4gp9xciigxckwkqrgiwkpl7") (f (quote (("nightly"))))))

(define-public crate-vox_box-0.2.4 (c (n "vox_box") (v "0.2.4") (d (list (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^1.0") (d #t) (k 0)) (d (n "sample") (r "^0.6.1") (d #t) (k 0)))) (h "0plpsw3g89ggyycqm1qkvkdv3pqqkhd6q20mc6ymk4x6yzb8155h") (f (quote (("nightly"))))))

(define-public crate-vox_box-0.3.0 (c (n "vox_box") (v "0.3.0") (d (list (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^1.0") (d #t) (k 0)) (d (n "sample") (r "^0.9") (d #t) (k 0)))) (h "1y1s212xjywwb1xhsnlcp9acx8z32p912qczvgcb8nqjn258wg0h") (f (quote (("nightly"))))))

