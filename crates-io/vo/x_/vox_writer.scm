(define-module (crates-io vo x_ vox_writer) #:use-module (crates-io))

(define-public crate-vox_writer-0.1.0 (c (n "vox_writer") (v "0.1.0") (h "0ngv6lqqmiskk6v272ca0m3pxpmps09nfda8q8831k15x5hqpkh7") (y #t)))

(define-public crate-vox_writer-0.1.1 (c (n "vox_writer") (v "0.1.1") (h "1gxkfl1d3c2vckp52jmlvn242lxsfcan4rvpy5b7r6i80k111l4r") (y #t)))

(define-public crate-vox_writer-0.1.2 (c (n "vox_writer") (v "0.1.2") (h "1h2gklyrf8sw53zv3xjrvh75bkcq83hnmk5c9d0l40n6jfhcymfv") (y #t)))

(define-public crate-vox_writer-0.1.3 (c (n "vox_writer") (v "0.1.3") (h "00s7q3nhl6a0yvm3lfkqg43jm162cnj3dgqqhi80n89iibba4mgr") (y #t)))

(define-public crate-vox_writer-0.1.4 (c (n "vox_writer") (v "0.1.4") (h "19dkh5hnamnql1rs4rfbhnmcs25wrk827p31j2yzx4janad8npm5") (y #t)))

(define-public crate-vox_writer-0.1.5 (c (n "vox_writer") (v "0.1.5") (h "0fppaf1adr1l2aahg1k3915bgkzpys4skg9qxg00zv7nhws0pwb1") (y #t)))

(define-public crate-vox_writer-0.1.6 (c (n "vox_writer") (v "0.1.6") (h "1jm5ka8wkaab2zl89wzvg6xp7sr3b9wr6c3whzz6sjg3x37dfczk")))

(define-public crate-vox_writer-0.1.7 (c (n "vox_writer") (v "0.1.7") (h "11hpzv55klzz91sw1wvhw9lnb6qlmdis9gg5f0sgrvmynscjghml") (y #t)))

(define-public crate-vox_writer-0.1.8 (c (n "vox_writer") (v "0.1.8") (h "0pyqrs1w8rpp5vdnh1pxqlx68zp1v07qkm4g5qsyj4dhks3gd2zv")))

(define-public crate-vox_writer-0.1.9 (c (n "vox_writer") (v "0.1.9") (h "10ccvxmw0d0rdy9g93yv1fqrmhvs3ji4gw8r0bfy1hv8q6ypfc4p")))

