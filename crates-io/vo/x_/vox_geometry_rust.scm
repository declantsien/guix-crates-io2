(define-module (crates-io vo x_ vox_geometry_rust) #:use-module (crates-io))

(define-public crate-vox_geometry_rust-0.1.0 (c (n "vox_geometry_rust") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1c81ap7zj4m4i40k7rx2r9qzwsprvv4v14d1lcx3dkcfprlx61nx")))

(define-public crate-vox_geometry_rust-0.1.1 (c (n "vox_geometry_rust") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tobj") (r "^3.0.0") (d #t) (k 0)))) (h "15s9zli1kzyjmln9md4ymlgf7fmlv2jgjk6kcg3vdwbhprxs6zzy")))

(define-public crate-vox_geometry_rust-0.1.2 (c (n "vox_geometry_rust") (v "0.1.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tobj") (r "^3.0.0") (d #t) (k 0)))) (h "00nbibxwbdyygyp2vs6pgv0sl88iqjgpp459xdr4p6mcj6jpv5dg")))

