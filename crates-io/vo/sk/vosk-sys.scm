(define-module (crates-io vo sk vosk-sys) #:use-module (crates-io))

(define-public crate-vosk-sys-0.1.0 (c (n "vosk-sys") (v "0.1.0") (h "10vybx0pxlgk4q6xvwh2a5sjfkzw95sllbrxanrrx9yp2krwrqna")))

(define-public crate-vosk-sys-0.1.1 (c (n "vosk-sys") (v "0.1.1") (h "0acx17sy9r6qy7sv8jngzpy32cf9i0d520bc3kbq550hra9sy6a1")))

