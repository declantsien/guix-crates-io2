(define-module (crates-io vo sk vosk) #:use-module (crates-io))

(define-public crate-vosk-0.1.0 (c (n "vosk") (v "0.1.0") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "dasp") (r "^0.11") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vosk-sys") (r "^0.1") (d #t) (k 0)))) (h "00idnymlflzi5dq5dnhyb8wd4mw8nd0njgajqhmx0cnhp9q4p2sc")))

(define-public crate-vosk-0.2.0 (c (n "vosk") (v "0.2.0") (d (list (d (n "cpal") (r "^0.14") (d #t) (k 2)) (d (n "dasp") (r "^0.11") (d #t) (k 2)) (d (n "hound") (r "^3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vosk-sys") (r "^0.1") (d #t) (k 0)))) (h "1v37rm7xskq8fgbzrzzfch6ifsw98034d6wj8vm6mxwdhvfvh91y")))

