(define-module (crates-io vo rb vorbis-encoder) #:use-module (crates-io))

(define-public crate-vorbis-encoder-0.1.0 (c (n "vorbis-encoder") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vorbis-sys") (r "^0.0.8") (d #t) (k 0)))) (h "056n1ibaly5y6v9vsf354ncc91d6wi5a1wrs78ywi044y87dz1ba")))

(define-public crate-vorbis-encoder-0.1.1 (c (n "vorbis-encoder") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vorbis-sys") (r "^0.0.8") (d #t) (k 0)))) (h "04kxdcz50an6qz2wiwl9h9ffq1wczvfbcwxr2ilbyxmqqglznqi7")))

(define-public crate-vorbis-encoder-0.1.2 (c (n "vorbis-encoder") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vorbis-sys") (r "^0.0") (d #t) (k 0)))) (h "0nf2iybn6fwhrc2i6dfljqk7mv3agplc7kyip9ff58crwh7zqnwl")))

(define-public crate-vorbis-encoder-0.1.3 (c (n "vorbis-encoder") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vorbis-sys") (r "^0.0") (d #t) (k 0)))) (h "0vwlymi629p3q31br8xzfsj3kl03cwhbvz7aqfvf0vzzskxynhg1")))

(define-public crate-vorbis-encoder-0.1.4 (c (n "vorbis-encoder") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vorbis-sys") (r "^0.0") (d #t) (k 0)))) (h "1g4iafccg2j20gjcxxniibz7i5r6l5lndf0vk4qd4v85vv6npdiz")))

