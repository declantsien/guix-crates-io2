(define-module (crates-io vo rb vorbisfile-sys) #:use-module (crates-io))

(define-public crate-vorbisfile-sys-0.0.1 (c (n "vorbisfile-sys") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)))) (h "1bgdpqdqiiri78jhnml97in9fgxv6bcc74iplqsnf0xqj6d6z5v6")))

(define-public crate-vorbisfile-sys-0.0.2 (c (n "vorbisfile-sys") (v "0.0.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)))) (h "060ss5rj43spysvc3ynvyr5mz24bajada3slzkwzpx71zlxavxc4")))

(define-public crate-vorbisfile-sys-0.0.3 (c (n "vorbisfile-sys") (v "0.0.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)))) (h "04dxyr2w469gbz5gls908cdhmawahhrf8arixw7hkigmm01fnpnd")))

(define-public crate-vorbisfile-sys-0.0.5 (c (n "vorbisfile-sys") (v "0.0.5") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)))) (h "0l61rlpwd9s9m3d7i515cwfm8s2n3f0zqhcjp7wgcnr3zh0yfc8f")))

(define-public crate-vorbisfile-sys-0.0.6 (c (n "vorbisfile-sys") (v "0.0.6") (d (list (d (n "gcc") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.2") (d #t) (k 1)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)))) (h "04w26bdgf2g3q4mb9m2c9xikvhph7x3nrbpd20p00226mvqf77xr")))

(define-public crate-vorbisfile-sys-0.0.7 (c (n "vorbisfile-sys") (v "0.0.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)))) (h "0rzw85ac65rdhq390m6mv4w6q3i5ribwf59m0169y1272zplrz0l")))

(define-public crate-vorbisfile-sys-0.0.8 (c (n "vorbisfile-sys") (v "0.0.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)))) (h "1la2j2zbzdjd93byz21ij58c540bfn1r9pi0bssrjimcw7bhchsg")))

