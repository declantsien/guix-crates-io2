(define-module (crates-io vo rb vorbis-sys) #:use-module (crates-io))

(define-public crate-vorbis-sys-0.0.1 (c (n "vorbis-sys") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 0)))) (h "1xff673awskipqr7sgcvcx8rv321b2v22nq8jwd1yhbsp7vpzhgr")))

(define-public crate-vorbis-sys-0.0.2 (c (n "vorbis-sys") (v "0.0.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "010xwpbsvcrbbgnvsfpyymqwb1pdlqmw2djkmiavadvv1wj7bpsx")))

(define-public crate-vorbis-sys-0.0.3 (c (n "vorbis-sys") (v "0.0.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "07bs7hmd6dh0b2mz38ll7491aaq542qlg87snrxhra0hwh4xk2mc")))

(define-public crate-vorbis-sys-0.0.4 (c (n "vorbis-sys") (v "0.0.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0pk8wgajbjipfl7ap451smiwa481x7lxjyjhysqf71vqgxw5qvdj")))

(define-public crate-vorbis-sys-0.0.5 (c (n "vorbis-sys") (v "0.0.5") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0642ykcz3h1rzjxdyy8a2qnhj2vfp6sfyn68yslvkpkqich73542")))

(define-public crate-vorbis-sys-0.0.6 (c (n "vorbis-sys") (v "0.0.6") (d (list (d (n "gcc") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.2") (d #t) (k 1)))) (h "1by8va17dsmj042skjbrpq5csgvd787njghkg0hcbbd0d1a3zj70")))

(define-public crate-vorbis-sys-0.0.7 (c (n "vorbis-sys") (v "0.0.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07by0smc74h3ximraxni81a8cml9hh1vrm5kwd50682rk65ih2kw")))

(define-public crate-vorbis-sys-0.0.8 (c (n "vorbis-sys") (v "0.0.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17aqqnrq54gl1za6jfx50dsp0i7g88fwv84ws7kb8l2q74aiz7kj")))

(define-public crate-vorbis-sys-0.1.0 (c (n "vorbis-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i170dca74psmkxkf3r5hgngk0qza3xavc443pd4hdri6iq8s2is") (l "vorbis")))

(define-public crate-vorbis-sys-0.1.1 (c (n "vorbis-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zgv7lwa4b2z091g25h83zil8bawk4frc1f0ril5xa31agpxd7mx") (l "vorbis")))

