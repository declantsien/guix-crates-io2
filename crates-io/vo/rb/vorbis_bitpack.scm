(define-module (crates-io vo rb vorbis_bitpack) #:use-module (crates-io))

(define-public crate-vorbis_bitpack-0.1.0 (c (n "vorbis_bitpack") (v "0.1.0") (d (list (d (n "acid_io") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "15j2vpdrvkcpdanclkxbg1jgbdrghfx3y50y601b0ihrfi0jr4jf") (f (quote (("no-std" "acid_io" "libm")))) (r "1.61")))

(define-public crate-vorbis_bitpack-0.1.1 (c (n "vorbis_bitpack") (v "0.1.1") (d (list (d (n "acid_io") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "15w9q9y67pg3hvi5aryx7jvgsvmkgmi9v1bd964ch53zzfqlyc4i") (f (quote (("no-std" "acid_io" "libm")))) (r "1.61")))

