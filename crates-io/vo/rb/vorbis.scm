(define-module (crates-io vo rb vorbis) #:use-module (crates-io))

(define-public crate-vorbis-0.0.1 (c (n "vorbis") (v "0.0.1") (d (list (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "0249sii5f01vhkfzmyy73l5wk5jpgjb7i6k787zvzs5l7j8q2jg1")))

(define-public crate-vorbis-0.0.2 (c (n "vorbis") (v "0.0.2") (d (list (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "1qvk6zz9p8w363q34n01f87lf9ai23lagrwbacrfy28vprl5w2hz")))

(define-public crate-vorbis-0.0.3 (c (n "vorbis") (v "0.0.3") (d (list (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "1jmlanyscpmpq71ww77cjh2m72c7avqc5p4rfm2qkrd9frdxr8r9")))

(define-public crate-vorbis-0.0.4 (c (n "vorbis") (v "0.0.4") (d (list (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "0yw3g2cpw7qg36aq1n5vwxzdx9f5gfp9v23734bijln244fh9744")))

(define-public crate-vorbis-0.0.5 (c (n "vorbis") (v "0.0.5") (d (list (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "1yj9199dgv75yjxzjx12rr95chlbw5wz9cwqfzmxxdb1ra7dcnnn")))

(define-public crate-vorbis-0.0.6 (c (n "vorbis") (v "0.0.6") (d (list (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "0dmg8dkcky59pn0mbq2c7zywc081545m0bdf335pb5vvlfd97x5s")))

(define-public crate-vorbis-0.0.7 (c (n "vorbis") (v "0.0.7") (d (list (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "0rd9zy9bn2kkz1gkgpbcww8dhdd13pd3bg0wzqzi3namflf3ff5q")))

(define-public crate-vorbis-0.0.8 (c (n "vorbis") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "0fgarq0pc8idn3vls1k8hv9imxw9xlfnyngzzcp3f5ppg4in5zbi")))

(define-public crate-vorbis-0.0.9 (c (n "vorbis") (v "0.0.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "16zb5ibda4cr359wjk8szfqghd2zmwbh6s9k4njiya7bazsz8zp4")))

(define-public crate-vorbis-0.0.10 (c (n "vorbis") (v "0.0.10") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "0ba1gm6crs1zdq0nd7ygklss1b55ccw7xfn4kg5rw8nz3zkmamap")))

(define-public crate-vorbis-0.0.11 (c (n "vorbis") (v "0.0.11") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "1a1rmpqhi91shk9yj0f21d0xfn60q763hc7fzayr3im4fq7jipjx")))

(define-public crate-vorbis-0.0.12 (c (n "vorbis") (v "0.0.12") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "0hi9daw5a97n0571q7brmb7qf1zljlg2ljwxagbq0hfm6x0izz4c")))

(define-public crate-vorbis-0.0.13 (c (n "vorbis") (v "0.0.13") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "04j20vgfgswkjf52ip0ilj2jxwvwx2xarz81pi3kkac4qraaa04j")))

(define-public crate-vorbis-0.0.14 (c (n "vorbis") (v "0.0.14") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "ogg-sys") (r "*") (d #t) (k 0)) (d (n "vorbis-sys") (r "*") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "*") (d #t) (k 0)))) (h "0xn7diq8qz2zmsmwzg3rcsxmpmm2gj7wgnl2gdan0lq7ax21k2jy")))

(define-public crate-vorbis-0.1.0 (c (n "vorbis") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1") (d #t) (k 0)) (d (n "vorbis-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "vorbisfile-sys") (r "^0.0.8") (d #t) (k 0)))) (h "0mkn538sz9fc3jpcicwdy6licbrwfyfg1bypsj48n4i48pjr62bn")))

