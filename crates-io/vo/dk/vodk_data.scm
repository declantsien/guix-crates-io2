(define-module (crates-io vo dk vodk_data) #:use-module (crates-io))

(define-public crate-vodk_data-0.0.1 (c (n "vodk_data") (v "0.0.1") (h "16plkawj5x412z17wlrfljw33qrbaa5w5pcqwv7g1c9zvcdvak07")))

(define-public crate-vodk_data-0.0.2 (c (n "vodk_data") (v "0.0.2") (h "14gyfspjlb8zaxml5z4ik2clv48lxk31mmq06rs2kxzrb7c0azbx")))

