(define-module (crates-io vo dk vodk_math) #:use-module (crates-io))

(define-public crate-vodk_math-0.0.1 (c (n "vodk_math") (v "0.0.1") (h "05wqfvbay8q40i52gmdmzhb3kzmg1nvj24vakdhh0l704vwhznv4")))

(define-public crate-vodk_math-0.0.2 (c (n "vodk_math") (v "0.0.2") (h "02nwfdzj77fc4alw8b0l5ypr7rl60qkbi3y2s52r3hz69jxp7p3j")))

(define-public crate-vodk_math-0.0.3 (c (n "vodk_math") (v "0.0.3") (h "1c655r1mp3iwygw8yfzyig6f302a735rzsbpaqyl1wdn8rc0m17j")))

