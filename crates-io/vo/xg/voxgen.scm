(define-module (crates-io vo xg voxgen) #:use-module (crates-io))

(define-public crate-voxgen-0.1.0 (c (n "voxgen") (v "0.1.0") (d (list (d (n "enterpolation") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "line_drawing") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "034b41jman7s7z25djlivf8hyvs7iz8xg17ym4ip5wqn2rlzn3zf")))

(define-public crate-voxgen-0.2.0 (c (n "voxgen") (v "0.2.0") (d (list (d (n "enterpolation") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "line_drawing") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "1643wkbcn5g2nzifj68jsn57slvvi67syr92bvj9xw0nlmgjl4jr")))

(define-public crate-voxgen-0.3.0 (c (n "voxgen") (v "0.3.0") (d (list (d (n "enterpolation") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "line_drawing") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "1ssgckws5s4hgzqgyw4x79vzqxzn96aldajb9a6zgqzrgb51815w")))

(define-public crate-voxgen-0.4.0 (c (n "voxgen") (v "0.4.0") (d (list (d (n "enterpolation") (r "^0.2.0") (d #t) (k 0)) (d (n "line_drawing") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "19hvzcc6nh4dz5x4r7vhg424wmrwz2skx4j9jj615w8qscq8pn5h")))

