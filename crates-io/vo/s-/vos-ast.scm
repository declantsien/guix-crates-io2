(define-module (crates-io vo s- vos-ast) #:use-module (crates-io))

(define-public crate-vos-ast-0.0.0 (c (n "vos-ast") (v "0.0.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "unicode-ident") (r "^1.0.5") (d #t) (k 0)) (d (n "vos-error") (r "^0.1.1") (f (quote ("num" "bigdecimal" "peginator"))) (d #t) (k 0)))) (h "0f367cfn5dqq06dmy6689g3a4yafpfs99ymj06f6nnhq1vmlwq3a") (f (quote (("default"))))))

(define-public crate-vos-ast-0.1.0 (c (n "vos-ast") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "unicode-ident") (r "^1.0.5") (d #t) (k 0)) (d (n "vos-error") (r "^0.1.1") (f (quote ("bigdecimal" "peginator"))) (d #t) (k 0)))) (h "17bhbhd7a9ajkw9697l6vf4cn72cs8gvdanpzc3khl5qvg0s0nqy") (f (quote (("default"))))))

