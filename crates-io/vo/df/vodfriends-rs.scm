(define-module (crates-io vo df vodfriends-rs) #:use-module (crates-io))

(define-public crate-vodfriends-rs-0.1.0 (c (n "vodfriends-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "m3u8-rs") (r "^5.0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)))) (h "1h2jggykf5dq845ln5y1znjpachxfadhsd7zm80f74prngdymczm")))

