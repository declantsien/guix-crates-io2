(define-module (crates-io vo x2 vox2pmf) #:use-module (crates-io))

(define-public crate-vox2pmf-0.1.0 (c (n "vox2pmf") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dot_vox") (r "^4.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "libpolymesh") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04vqdlgvkgw5wa4dhqb97qq1mxa6pn7851130bhnf7bdm8wzrjg6")))

