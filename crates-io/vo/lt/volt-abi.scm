(define-module (crates-io vo lt volt-abi) #:use-module (crates-io))

(define-public crate-volt-abi-0.1.0 (c (n "volt-abi") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.24.2") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.8.14") (d #t) (k 0)))) (h "16dg411g4bigkv313pw2nqahaspwczcck3x9h0i1y1qg3ka53lpn") (f (quote (("no-idl") ("no-entrypoint") ("mainnet") ("default" "mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-volt-abi-0.2.0 (c (n "volt-abi") (v "0.2.0") (d (list (d (n "anchor-lang") (r "=0.25.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.10.29") (d #t) (k 0)))) (h "1rf1yfk0b8r7hqpd7182wac643bjdwz66mih599r1alnqnh1lr9w") (f (quote (("no-idl") ("no-entrypoint") ("mainnet") ("default" "mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-volt-abi-0.10.0 (c (n "volt-abi") (v "0.10.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.10.29") (d #t) (k 0)))) (h "0ipdlpy78lva4w7kvh1kl4nzh1vycznbmr93rnagp22h686f6pm1") (f (quote (("no-idl") ("no-entrypoint") ("mainnet") ("default" "mainnet") ("cpi" "no-entrypoint"))))))

(define-public crate-volt-abi-0.11.0 (c (n "volt-abi") (v "0.11.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.10.29") (d #t) (k 0)))) (h "1kzpdcgazaz0i7znlm8r6zj5fq39w008dzfa821yb0ybg74fskm7") (f (quote (("no-idl") ("no-entrypoint") ("mainnet") ("default" "mainnet") ("cpi" "no-entrypoint"))))))

