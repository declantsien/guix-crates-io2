(define-module (crates-io vo lt voltorb) #:use-module (crates-io))

(define-public crate-voltorb-0.0.0 (c (n "voltorb") (v "0.0.0") (h "0f0yh8nisr05452z3zjzb1lq71ry2fmlf3g87p3kddh62abpwyql")))

(define-public crate-voltorb-0.1.0 (c (n "voltorb") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "boxy") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pcqmmk9n10m06kv1h6iyq02hs87aqk50hsgg1mxn74l5854vrz0")))

