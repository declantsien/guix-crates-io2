(define-module (crates-io vo lt voltdb) #:use-module (crates-io))

(define-public crate-voltdb-0.1.0 (c (n "voltdb") (v "0.1.0") (h "0rwxcqrj42sif0y81993zs8ylhygkj1xqdm332ch30bd2kngmn16")))

(define-public crate-voltdb-0.1.1 (c (n "voltdb") (v "0.1.1") (h "1a4n3ck4jvwy9031pv14r3gbz8ndfnaac35ppccd633vh3ms9jkc")))

