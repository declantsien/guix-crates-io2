(define-module (crates-io vo lt volt_parse) #:use-module (crates-io))

(define-public crate-volt_parse-0.2.2 (c (n "volt_parse") (v "0.2.2") (h "1dxwdqfd1j3qbrzf9mpbaw9yk65jnwss78i53i2gvxkml10yfva0")))

(define-public crate-volt_parse-0.3.0 (c (n "volt_parse") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "0wjghw29c6gv20jqq9xk3gwapw7w6cx89xilm9zi415c3sjf9qp3")))

(define-public crate-volt_parse-0.4.0 (c (n "volt_parse") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "0ynwrpbhqcv1rp8f98fxsigliyqkzqdcycnbdj33q5132jhrbd04")))

(define-public crate-volt_parse-0.5.0 (c (n "volt_parse") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "0b88sgyi4nczh773gmpcsrpnpd9ks1sryvwzxnv98s7kn8qxgq84")))

