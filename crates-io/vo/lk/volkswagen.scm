(define-module (crates-io vo lk volkswagen) #:use-module (crates-io))

(define-public crate-volkswagen-0.1.0 (c (n "volkswagen") (v "0.1.0") (d (list (d (n "built") (r "^0.3") (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n0ha74ss9d78fsk500p8jdavxs35izlf91k2aasx13w2pgf1asq")))

