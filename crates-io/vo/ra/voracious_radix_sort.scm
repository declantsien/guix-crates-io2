(define-module (crates-io vo ra voracious_radix_sort) #:use-module (crates-io))

(define-public crate-voracious_radix_sort-0.1.0 (c (n "voracious_radix_sort") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1kskbv6d10ggkhx6hfdkrl1nn5b1d15ip5jjj6r32xx4lk5pcv31")))

(define-public crate-voracious_radix_sort-1.0.0 (c (n "voracious_radix_sort") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)) (d (n "rayon") (r "^1.4.0") (d #t) (k 0)))) (h "04cj2yqdjb3ldsfr4jpmr68yk2jwcr7pwzd1jy4za221z5lkabvk")))

(define-public crate-voracious_radix_sort-1.1.0 (c (n "voracious_radix_sort") (v "1.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)))) (h "0pjk9mqbqkqhydhw9rrmpbz2xikasw13xvdk6lkkrv5bvfwfmdxs") (f (quote (("voracious_multithread" "rayon"))))))

(define-public crate-voracious_radix_sort-1.1.1 (c (n "voracious_radix_sort") (v "1.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)))) (h "0mxsgmvwy6lx9l0x1ylq95bw1znpqkw76r9wcnvrbanqi57wr9vw") (f (quote (("voracious_multithread" "rayon"))))))

(define-public crate-voracious_radix_sort-1.2.0 (c (n "voracious_radix_sort") (v "1.2.0") (d (list (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 2)))) (h "16a8j7hm35j3y8rah8mi4j2c8wavxvr1xrgpbb872yn2nvy7yvj4") (f (quote (("voracious_multithread" "rayon"))))))

