(define-module (crates-io vo te vote-hooks) #:use-module (crates-io))

(define-public crate-vote-hooks-0.1.0 (c (n "vote-hooks") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "indexable-hooks") (r "^0.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0hdi5wz1ma9gas7q48zwxiya41n4zj5ljwnf7f55y6fxgv8plqgg")))

