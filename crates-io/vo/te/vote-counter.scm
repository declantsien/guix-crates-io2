(define-module (crates-io vo te vote-counter) #:use-module (crates-io))

(define-public crate-vote-counter-0.1.0 (c (n "vote-counter") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "0w754x268hk4d513ka0dyd4cb2lyazkifpdwl91sqlngkvqa036s")))

(define-public crate-vote-counter-0.1.1 (c (n "vote-counter") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1cgslrgcws994kjf8h8qzfaa1nvdl1z1y3ck7cqzif509i6ga3r8")))

(define-public crate-vote-counter-0.2.0 (c (n "vote-counter") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "0cm5x766znj62licq2lh7vlp7cc2fi0h885r44kz518w8m0m38v1")))

(define-public crate-vote-counter-0.2.1 (c (n "vote-counter") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "12qhdxinqix5nvxy2l04za11qdv00w8cfg5l0f2rbys19nn7q92p")))

