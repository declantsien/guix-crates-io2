(define-module (crates-io vo te vote) #:use-module (crates-io))

(define-public crate-vote-0.1.0 (c (n "vote") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0sgbyawbwhs0m70bxid7fbcwhhksw042dzsr49y81kql0v467adv")))

(define-public crate-vote-0.1.1 (c (n "vote") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "03i5lwbkygawc823iz35g9cigx65hyfj2wr844lf0lk694jx19bg")))

(define-public crate-vote-0.1.2 (c (n "vote") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "193prgajmqp1mif5zjghw0yrinqdms9fpvc2nxv0k56870dw15gb")))

(define-public crate-vote-0.1.3 (c (n "vote") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0q72vcjyc43gfk23i8847drmk4q0a72q06gh68bgdq92pjicmry3")))

