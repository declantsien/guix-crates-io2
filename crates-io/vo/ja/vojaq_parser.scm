(define-module (crates-io vo ja vojaq_parser) #:use-module (crates-io))

(define-public crate-vojaq_parser-0.1.0 (c (n "vojaq_parser") (v "0.1.0") (h "0i11zad1vydb1nwm6zrmi91v4g8vzfznapclszd36z01ffb4rpzs")))

(define-public crate-vojaq_parser-0.1.1 (c (n "vojaq_parser") (v "0.1.1") (h "0h06lm0c7c9sxyd3d8yjsj89y0lsnysqpmwh84ihsxz45bg7wwyn")))

(define-public crate-vojaq_parser-0.1.2 (c (n "vojaq_parser") (v "0.1.2") (h "1qr16k4ps6a7484wk7n8sm9mpbjndwfvihjm6jsjxjjy5cldw46r")))

(define-public crate-vojaq_parser-0.2.0 (c (n "vojaq_parser") (v "0.2.0") (h "1z24l1h9vhv85wkg92yqly7whhmw3v16xqzwz2pxp0ana4fkbq20")))

(define-public crate-vojaq_parser-0.2.1 (c (n "vojaq_parser") (v "0.2.1") (h "04vp9h2r2wskjac2lgp0cj55ahbaalgpazisa94g098g4b4brbv6")))

(define-public crate-vojaq_parser-0.3.0 (c (n "vojaq_parser") (v "0.3.0") (h "1iqvl836lgsc4cjr9fxmhpjcr4551crf1s59bw9xln3iz5a59ldi")))

(define-public crate-vojaq_parser-0.4.0 (c (n "vojaq_parser") (v "0.4.0") (h "18wlfmi649b9c9jq75fa2ykp7h8wsrknxwf362x9v7zfgvzif6q1")))

(define-public crate-vojaq_parser-0.5.0 (c (n "vojaq_parser") (v "0.5.0") (h "0c1640y3y0i324sdf0g50xnnr7v4scrpm0wvzmfk6k4zc8k2a850")))

(define-public crate-vojaq_parser-0.5.1 (c (n "vojaq_parser") (v "0.5.1") (h "0xyj37ngikkh1hn01kqix4iyyb1wb4d3j9czgqdgkdpkklp82ybw")))

