(define-module (crates-io vo ml voml-error) #:use-module (crates-io))

(define-public crate-voml-error-0.1.0 (c (n "voml-error") (v "0.1.0") (d (list (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1cx72n12d2m1w64y3wm0pyl6559nxkvfihvf2wgv2zhzixxsw9vb") (f (quote (("default"))))))

(define-public crate-voml-error-0.1.1 (c (n "voml-error") (v "0.1.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "diagnostic") (r "^0.3.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "13197l9pb7w4pls32rjisrlsl5rlspqlf7cx5yzd27mr2n3w25vh") (f (quote (("default"))))))

