(define-module (crates-io vo ml voml-types) #:use-module (crates-io))

(define-public crate-voml-types-0.0.0 (c (n "voml-types") (v "0.0.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "diagnostic") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "peginator") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "voml-collection") (r "^0.3.0") (d #t) (k 0)))) (h "102lfvxhjry9ps88zph950gr2wamqwrn132vd9lhm6w0zzvzfbqp") (f (quote (("default"))))))

(define-public crate-voml-types-0.1.1 (c (n "voml-types") (v "0.1.1") (d (list (d (n "diagnostic") (r "^0.4.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "peginator") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "voml-collection") (r "^0.3.1") (f (quote ("bigdecimal"))) (d #t) (k 0)))) (h "12cxgvj0dkv86wf98mp5286gfy7iq8ss55xaddsqhcwqyz3annb2") (f (quote (("default"))))))

