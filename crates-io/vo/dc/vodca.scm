(define-module (crates-io vo dc vodca) #:use-module (crates-io))

(define-public crate-vodca-0.1.1 (c (n "vodca") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "derive" "proc-macro"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1a34bi5yjw5qpxr1n65jadfiz5lj6d5vh3kjay2zwrb5bs5v4wxj")))

