(define-module (crates-io vo id voidbox) #:use-module (crates-io))

(define-public crate-voidbox-0.1.0 (c (n "voidbox") (v "0.1.0") (h "0ly6hfjj4gjajsc0p5w23482g36r8b1hqkj94ldd5hsd5wxp9jrl") (y #t)))

(define-public crate-voidbox-0.2.0 (c (n "voidbox") (v "0.2.0") (h "1a31q347xjdj091z1wqzkxgx8sylicd3f26f7y9fwsdkpiqd6pw6") (y #t)))

