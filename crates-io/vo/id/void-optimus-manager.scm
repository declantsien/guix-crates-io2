(define-module (crates-io vo id void-optimus-manager) #:use-module (crates-io))

(define-public crate-void-optimus-manager-0.1.0 (c (n "void-optimus-manager") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "02qzmqjwsr808svhf090isz910p69f5g4kx6n5c5izmgnfd3bnng")))

