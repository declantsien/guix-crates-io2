(define-module (crates-io vo id voidy) #:use-module (crates-io))

(define-public crate-voidy-0.1.0 (c (n "voidy") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)))) (h "1sv47da3adffqsdkgrcnrjcs9bfmsnwqqjvimlb2jmzzb4qd4j0w") (y #t)))

(define-public crate-voidy-0.2.0 (c (n "voidy") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)))) (h "1sa2xg09abf86afr6nmbgayvl8zzj78v8ni9bbka4yv9q6rx3ypz") (y #t)))

