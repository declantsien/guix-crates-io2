(define-module (crates-io vo id void) #:use-module (crates-io))

(define-public crate-void-0.0.1 (c (n "void") (v "0.0.1") (h "1rh70ysa2l8hwlbm6vpbd19z3c5ya3sjh1vbg43q5d7ksyf1aksn")))

(define-public crate-void-0.0.2 (c (n "void") (v "0.0.2") (h "156s945792fpzyddix658cmxsrqijcdyp3h1219y1jpa4f9436cj")))

(define-public crate-void-0.0.3 (c (n "void") (v "0.0.3") (h "0l7pp7kx4s31i6cknv98j2hi7pqxcz0qi4jjrwd92cnr4hh8x07p")))

(define-public crate-void-0.0.4 (c (n "void") (v "0.0.4") (h "04s40jrbf79bmkgarxzyc4x132rqylh0jmz6dna81azq9kd8nwfx")))

(define-public crate-void-0.0.5 (c (n "void") (v "0.0.5") (h "02l02z7rf7higfac598m8wv6xx4173j1ia6h5hn77z4sxnlwvdn6")))

(define-public crate-void-1.0.0 (c (n "void") (v "1.0.0") (h "0b5pvqmx8lhfijam7j1y4dqsjl30i68gbzva4f8d54vbrkyx944i") (f (quote (("std") ("default" "std"))))))

(define-public crate-void-1.0.1 (c (n "void") (v "1.0.1") (h "1lnjz2nrjnxg7v9ky82wm8brkcz2128wcsll0m5n8zd3yp5326mz") (f (quote (("std") ("default" "std"))))))

(define-public crate-void-1.0.2 (c (n "void") (v "1.0.2") (h "0zc8f0ksxvmhvgx4fdg0zyn6vdnbxd2xv9hfx4nhzg6kbs4f80ka") (f (quote (("std") ("default" "std"))))))

