(define-module (crates-io vo id void-app-api) #:use-module (crates-io))

(define-public crate-void-app-api-0.0.1 (c (n "void-app-api") (v "0.0.1") (h "1800k7773rb9j6nyas9kkg89ys792s94v07j5lzc6sg9mrdq2mdx")))

(define-public crate-void-app-api-0.0.2 (c (n "void-app-api") (v "0.0.2") (h "1552ai104y6gmilm3a57cyg6izkg4s74xklym74qckwgi5dhbv0r")))

(define-public crate-void-app-api-0.0.3 (c (n "void-app-api") (v "0.0.3") (h "0mzzm7zwqf0vkj541vsryqymkgnrn52i2hj0h1jmwp8rj9hl4f91")))

(define-public crate-void-app-api-0.0.4 (c (n "void-app-api") (v "0.0.4") (h "068lw0phy13p5b17yb18ais7szin4paxlmv0xjamx2mcrp5bsvgw")))

(define-public crate-void-app-api-0.0.5 (c (n "void-app-api") (v "0.0.5") (h "1yj1rypzs0vv89xrxc7b355gb5wxw6mrb736lwvjbbz7w1h7d0l7")))

(define-public crate-void-app-api-0.0.6 (c (n "void-app-api") (v "0.0.6") (h "009r3npq4m3ygii6hx46xah75ggmawlqh350ig2j5yc96d4vibbh")))

(define-public crate-void-app-api-0.0.7 (c (n "void-app-api") (v "0.0.7") (h "19g6mpfg2d80i0jc4q1y2qidbg88745h5dgaxfx4ilsa0k8fc320")))

(define-public crate-void-app-api-0.0.8 (c (n "void-app-api") (v "0.0.8") (h "0953w2plq8bkpi1hnv6mkfz8fm1v2nw4qmc1mjp6iz1zgpxd3mif")))

(define-public crate-void-app-api-0.0.9 (c (n "void-app-api") (v "0.0.9") (h "153wfc7g46ddpfk1xcbifp6fvs65d1hb7y6396nskvc6fb4wk99y")))

(define-public crate-void-app-api-0.0.10 (c (n "void-app-api") (v "0.0.10") (d (list (d (n "egui") (r "^0.27.2") (d #t) (k 0)))) (h "0f5qz27kr2snwcs69qf254mym15y5g0wnnqjd4ikznlzmnjl6has")))

