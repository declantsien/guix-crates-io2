(define-module (crates-io vo lu volu) #:use-module (crates-io))

(define-public crate-volu-0.1.0 (c (n "volu") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.6") (d #t) (k 0)))) (h "1qx8wlqazfqa8r2nsw0k1zmf00xq8gzkp9pv6qypdi3nl4iw1mwq")))

(define-public crate-volu-0.1.1 (c (n "volu") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.6") (d #t) (k 0)))) (h "1hrc4hnazarzy3p1ksr895w14i9jy287f3dqsx5i75ncl513xdhn")))

(define-public crate-volu-0.1.2 (c (n "volu") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.6") (d #t) (k 0)))) (h "08y2n6mzw17dp4wvc9gk6618r6h4ijmm85y99inprgqyff0p40kh")))

(define-public crate-volu-0.2.0 (c (n "volu") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.6") (d #t) (k 0)))) (h "0f7d3jf1qzk05cmgcz2g6jlhlcvmgcah0ybn657py0dm4c3bpq05")))

(define-public crate-volu-0.2.1 (c (n "volu") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.6") (d #t) (k 0)))) (h "00di7i8s58yf5r8rlkjh9mv06wrmaj1161yal5agpci8r9xdlxs8")))

