(define-module (crates-io vo lu volute) #:use-module (crates-io))

(define-public crate-volute-0.1.0 (c (n "volute") (v "0.1.0") (h "038dv9bxg3y739w64sicqkvhn0cbdkvb5xfxf8x76plpa8hbssig")))

(define-public crate-volute-0.1.1 (c (n "volute") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0w6ypasqv5dv7dcwhqkacj3kkb60wsj8nryx05k1h5g45dji798i")))

(define-public crate-volute-1.0.0 (c (n "volute") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r ">=0.5.0") (o #t) (d #t) (k 0)))) (h "09kgv6987r249wsj8ikp4vbqfarlfcga3840wxl79ql0j8kxyrq8") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-volute-1.0.1 (c (n "volute") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "198s2hw3zpdzygdhv6346pz4ic0warbgdnic5xix8r5s17s68cb4") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-volute-1.0.2 (c (n "volute") (v "1.0.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "1zsgfsw428iiwpm1wmxgklllxdrrfds4xwls3agafkvjxxaxl39g") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-volute-1.0.3 (c (n "volute") (v "1.0.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0vzfnn0xwvqk8k3a7gil8f1smjz74jm1dk4v97wp8i5dh32ilmn1") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-volute-1.0.4 (c (n "volute") (v "1.0.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "11m9y3l5f3z2ns9zarg6d3yyx7g2jw7ck4q6pd73vkzgdlvvl2lw") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-volute-1.0.5 (c (n "volute") (v "1.0.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "1saji7f07cw02xww90jw4mghw8v24k7rlm01c3qmyy9ksm75rkg0") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-volute-1.0.6 (c (n "volute") (v "1.0.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "good_lp") (r "^1.7") (f (quote ("highs"))) (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "1gmhii684lb3sxqazpypc8fap80kzdrzbsc1hk4kcvwcm0zkr9vr") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand") ("optim-mip" "dep:good_lp"))))))

(define-public crate-volute-1.1.0 (c (n "volute") (v "1.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "good_lp") (r "^1.7") (f (quote ("highs"))) (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "03gygdig0d2a4lsrxpkkps6j56yk87bqlq9qvfi4digzy604fih0") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand") ("optim-mip" "dep:good_lp"))))))

(define-public crate-volute-1.1.1 (c (n "volute") (v "1.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "good_lp") (r "^1.7") (f (quote ("highs"))) (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rustsat") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rustsat-kissat") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1ddgvhigcbzzhhg8b69ymyk6g1c4qpjwgakr90caa0kjklmsyy89") (f (quote (("default" "rand")))) (y #t) (s 2) (e (quote (("rand" "dep:rand") ("optim-sat" "dep:rustsat" "dep:rustsat-kissat") ("optim-mip" "dep:good_lp"))))))

(define-public crate-volute-1.1.2 (c (n "volute") (v "1.1.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "good_lp") (r "^1.7") (f (quote ("highs"))) (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rustsat") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rustsat-kissat") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1i8lfk8f6c8gb5jzss3vn142krfnnr2877wb6ikmsz2gfll42x99") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand") ("optim-sat" "dep:rustsat" "dep:rustsat-kissat") ("optim-mip" "dep:good_lp"))))))

(define-public crate-volute-1.1.3 (c (n "volute") (v "1.1.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "good_lp") (r "^1.7") (f (quote ("highs"))) (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rustsat") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rustsat-kissat") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1x3vy2m6kkn7ridgsly83gnnig4s7v5llmrbqrxkfzfzxfbrj335") (f (quote (("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand") ("optim-sat" "dep:rustsat" "dep:rustsat-kissat") ("optim-mip" "dep:good_lp"))))))

(define-public crate-volute-1.1.4 (c (n "volute") (v "1.1.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "good_lp") (r "^1.7") (f (quote ("highs"))) (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rustsat") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "rustsat-kissat") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0jpwqal90zl9szjn1wcrgvhl0gc0k674rihp6lq8xn6p2kdnqqkn") (f (quote (("default" "rand")))) (y #t) (s 2) (e (quote (("rand" "dep:rand") ("optim-sat" "dep:rustsat" "dep:rustsat-kissat") ("optim-mip" "dep:good_lp"))))))

