(define-module (crates-io vo lu volume) #:use-module (crates-io))

(define-public crate-volume-0.1.0 (c (n "volume") (v "0.1.0") (d (list (d (n "dsp-chain") (r "^0.5.4") (d #t) (k 0)))) (h "0k0da86pd0ivw0x2s3wdilbw4gx2mqb8nbqhzisr6si9vclhig35")))

(define-public crate-volume-0.1.1 (c (n "volume") (v "0.1.1") (d (list (d (n "dsp-chain") (r "^0.5.4") (d #t) (k 0)) (d (n "time_calc") (r "^0.10.0") (d #t) (k 0)))) (h "13l6jh7pjc3ygknw5zbcpc1fcvwg728snmmjc8bqrhqaaxcwdhcp")))

(define-public crate-volume-0.2.0 (c (n "volume") (v "0.2.0") (d (list (d (n "dsp-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.10.0") (d #t) (k 0)))) (h "0yakmr0bf4wfdcw426d96jm3k288lvm7mgi0m2gmbb118li8pqhx")))

(define-public crate-volume-0.2.1 (c (n "volume") (v "0.2.1") (d (list (d (n "dsp-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.10.0") (d #t) (k 0)))) (h "1ibaw3g2zkjjhk67swplbp3jnb934ybny8hvcp16zpbr7wnhasmc")))

(define-public crate-volume-0.2.2 (c (n "volume") (v "0.2.2") (d (list (d (n "dsp-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.10.0") (d #t) (k 0)))) (h "0y8rh360d0azzcvaav24hwbrcgmc1jgdbklvybv6gs1gsny2bdwb")))

(define-public crate-volume-0.3.0 (c (n "volume") (v "0.3.0") (d (list (d (n "dsp-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 0)))) (h "1xm28wf4shylql05sgg99anhqbab7j5q1q86j2cgg198gd0zwx0r")))

(define-public crate-volume-0.4.0 (c (n "volume") (v "0.4.0") (d (list (d (n "dsp-chain") (r "^0.13.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.11.0") (d #t) (k 0)))) (h "1dsyzccxm33lny1gw8z6q51lqd826pfdq8nn5n0n56x5jyp9j3ih")))

