(define-module (crates-io vo lu voluntary) #:use-module (crates-io))

(define-public crate-voluntary-0.1.0 (c (n "voluntary") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "jemallocator") (r "^0.5.0") (d #t) (k 0)) (d (n "rlua") (r "^0.19.4") (d #t) (k 0)))) (h "07kmj8z3mjb8lg4ir5nszxlw1rjwz30gbvqb51bs52x0sgdrx8s1") (y #t)))

(define-public crate-voluntary-0.1.1 (c (n "voluntary") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "rlua") (r "^0.19.4") (d #t) (k 0)))) (h "1frywpjfx4s3pl40h9c8bal6jm6q2fvqb2vcn77gzk604lzzp07l") (y #t)))

