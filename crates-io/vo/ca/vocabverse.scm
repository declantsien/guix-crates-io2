(define-module (crates-io vo ca vocabverse) #:use-module (crates-io))

(define-public crate-vocabverse-0.1.0 (c (n "vocabverse") (v "0.1.0") (h "1a698r804bg5y0brxi2dgkrh4bf6b2k02i9wc5q6b38i9bv35mfl") (y #t)))

(define-public crate-vocabverse-0.1.1 (c (n "vocabverse") (v "0.1.1") (h "0i5qg95xb9sxydivjnrr9vlr5r01njhv3i9j67k6dqb449nlaf6z") (y #t)))

(define-public crate-vocabverse-0.1.2 (c (n "vocabverse") (v "0.1.2") (h "0wyiy07lyzqn5gim2i6ixhzzkns9algldnq4f00h0771qn3g176z") (y #t)))

(define-public crate-vocabverse-0.1.3 (c (n "vocabverse") (v "0.1.3") (h "0kx6m2r0rhl8im8hxmdadiaxfcdibsrcay5qpn1b16x0nwv176c1")))

