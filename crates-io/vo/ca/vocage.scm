(define-module (crates-io vo ca vocage) #:use-module (crates-io))

(define-public crate-vocage-1.0.0 (c (n "vocage") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1gz8yxzj3ih9m09il108awmak3d99l0s2xvrzimxsv4qix35k7fw")))

(define-public crate-vocage-1.0.1 (c (n "vocage") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0hzn4zsp5nnqnlfvzplfss1889liragczbpa71c0w3221bk556wp")))

(define-public crate-vocage-1.1.0 (c (n "vocage") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1kgkz3gsn6bkxqb7w4w360j1k0rscc9lamjbv0lkf6x1kp8k0d7z")))

