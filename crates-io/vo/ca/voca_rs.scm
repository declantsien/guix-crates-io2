(define-module (crates-io vo ca voca_rs) #:use-module (crates-io))

(define-public crate-voca_rs-0.1.0 (c (n "voca_rs") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0zyk19blcv89djcan0s0k0sjii5qfazlkljryvbg7nm1i3qn4h9s")))

(define-public crate-voca_rs-0.2.0 (c (n "voca_rs") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0fsw78yyls9kmsbbhyai49mi85nlvi5l2vg9ypc7ljvxvrhicsfm")))

(define-public crate-voca_rs-0.3.0 (c (n "voca_rs") (v "0.3.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0lgfyl7m54si68zfa2wvr2w1bkhgdlnfwgr84bviaj0nbfi6j11w")))

(define-public crate-voca_rs-0.4.0 (c (n "voca_rs") (v "0.4.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "1365dafnnlv4invmcc27qxypax57srxl21rv2x30f05j81pm6r7l")))

(define-public crate-voca_rs-0.5.0 (c (n "voca_rs") (v "0.5.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "1ddrry0i2alhnj94lvvb4wyr068a6d7n40a0idvmnhms4pvd1fb1")))

(define-public crate-voca_rs-1.0.0 (c (n "voca_rs") (v "1.0.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0ckppl2inmhvlgwrlbhrc92srrj6fyhg0n7nhz27zp0y6rrjin7x")))

(define-public crate-voca_rs-1.1.0 (c (n "voca_rs") (v "1.1.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0njyixvam0sdlyv6imhzgi5fny35k9gbwl2z4vij69bd5giag9ib")))

(define-public crate-voca_rs-1.2.0 (c (n "voca_rs") (v "1.2.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1zy9hh1pz8lxni4vhy1p9i1ry776jmd3d0j91fmb6fc0igghmj13")))

(define-public crate-voca_rs-1.3.0 (c (n "voca_rs") (v "1.3.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "11bs4f3rv22b8c899n81d8fbwqyycdvrh8pjchv5kbf1fzjlrpif")))

(define-public crate-voca_rs-1.4.0 (c (n "voca_rs") (v "1.4.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1pc6laxi1ahi444d4hj00daq0rccr6kjxhsmzdr050m5pp8grl4i")))

(define-public crate-voca_rs-1.5.0 (c (n "voca_rs") (v "1.5.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0hhcgyjy0vvdhgap6svpnbhhzmwm0rskg3zpjx2xjrn65gkwzb94")))

(define-public crate-voca_rs-1.6.0 (c (n "voca_rs") (v "1.6.0") (d (list (d (n "dissolve") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "060ph3xai741iylx0b7r3kp95r4pg8wxg1jgcly5np1qbkz3v52q")))

(define-public crate-voca_rs-1.6.1 (c (n "voca_rs") (v "1.6.1") (d (list (d (n "dissolve") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0j846v6cmbw0bnaf02lzdmxbwjkvrqcclrgwnsc0rdkp4j35dzqr")))

(define-public crate-voca_rs-1.7.0 (c (n "voca_rs") (v "1.7.0") (d (list (d (n "dissolve") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1vgi4krjw9yxv3zb5350b665lk99bzpwak6khc6ggis3ilcfvvdr")))

(define-public crate-voca_rs-1.8.0 (c (n "voca_rs") (v "1.8.0") (d (list (d (n "dissolve") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1874gs855ziig51vzy3dy7kfxsw6riqnpbvlgz50v9047r76ypvk")))

(define-public crate-voca_rs-1.9.0 (c (n "voca_rs") (v "1.9.0") (d (list (d (n "dissolve") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0fz9iwcjmamyb5rsd6bj0arrwcamak45r3ddrisimx0j9fl9a7hx")))

(define-public crate-voca_rs-1.9.1 (c (n "voca_rs") (v "1.9.1") (d (list (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1x0ygbmvvfjzda0q8q3hksb3wam0apwwi5ym33wwh6v2wpvdfwpz")))

(define-public crate-voca_rs-1.10.0 (c (n "voca_rs") (v "1.10.0") (d (list (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "01d6ijs0z6vmkh05r2m3zqklyb8dl6cqg0mpy5855l58siq6xvab")))

(define-public crate-voca_rs-1.10.1 (c (n "voca_rs") (v "1.10.1") (d (list (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "14631vyvfbkgbpddf0dwyk98cfh2azw4ydyfmymjjp3yy3lmya3m")))

(define-public crate-voca_rs-1.11.0 (c (n "voca_rs") (v "1.11.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0503pra5yzq7g8mr9bvnayycfk8i60mw786hbmi8mfy77m8iq18x")))

(define-public crate-voca_rs-1.12.0 (c (n "voca_rs") (v "1.12.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "156k1pzffmcnyjzahi2ks5f3qkkwnqiq08j137akd7a8aqi96ibz")))

(define-public crate-voca_rs-1.13.0 (c (n "voca_rs") (v "1.13.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1z4qr4bsl0h260kq27dr0i6g7g2grfjvwaw5ja85g8akq90cwagc")))

(define-public crate-voca_rs-1.13.1 (c (n "voca_rs") (v "1.13.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "12anw7q6jk6mi45f9s0dy9srml2k92alkp3jfslnzadqxxk2z03i")))

(define-public crate-voca_rs-1.14.0 (c (n "voca_rs") (v "1.14.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "154dyfkfji68z6y729qvs343y5k2jpwlj6mwqfzqjfi89iwzhij6")))

(define-public crate-voca_rs-1.15.0 (c (n "voca_rs") (v "1.15.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "07k64qd6dwz5x3i9f8f3czc6wi7x32894c981g77ccvrw3xyckb5")))

(define-public crate-voca_rs-1.15.1 (c (n "voca_rs") (v "1.15.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "086ypl10b8gpwhfiqcmcq3y410jxhc5v8bz234l9xsfv7c1jpasw")))

(define-public crate-voca_rs-1.15.2 (c (n "voca_rs") (v "1.15.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "stfu8") (r "^0.2.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "064f2ln4qxm4y1g8yc9dlznb7ly3mkz488njxkanh9z34nzyyi1y")))

