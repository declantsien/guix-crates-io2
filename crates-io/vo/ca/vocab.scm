(define-module (crates-io vo ca vocab) #:use-module (crates-io))

(define-public crate-vocab-0.1.0 (c (n "vocab") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.3") (f (quote ("sqlite"))) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "06mz7vry0hql1pxbgkgszl4d6s1m725vihlrlsk1lq6a7bsgn5i9")))

(define-public crate-vocab-0.1.1 (c (n "vocab") (v "0.1.1") (d (list (d (n "diesel") (r "^1.4.3") (f (quote ("sqlite"))) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0dn82n2miyhbhjiwaxyqh5b0ysdlrgx7qs9jwwwkwx56flbksgkw")))

(define-public crate-vocab-0.2.0 (c (n "vocab") (v "0.2.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("sqlite"))) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0h0a37v0sf663ig6gicfwkmmh5bhmkxb7827cbrm2rfn6ir8b9w9")))

