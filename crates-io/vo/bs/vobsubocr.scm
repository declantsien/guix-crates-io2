(define-module (crates-io vo bs vobsubocr) #:use-module (crates-io))

(define-public crate-vobsubocr-0.1.0 (c (n "vobsubocr") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "leptess") (r "^0.13.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "scoped-tls-hkt") (r "^0.1.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "subparse") (r "^0.7.0") (d #t) (k 0)) (d (n "vobsub") (r "^0.2.3") (d #t) (k 0)))) (h "0ifkbcr1n248042y8aqgv63b0f0cml0znja72gf5wwda7vc81iiq")))

