(define-module (crates-io vo xp voxport) #:use-module (crates-io))

(define-public crate-voxport-0.1.0 (c (n "voxport") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "collada_io") (r "^0.1.0") (d #t) (k 0)) (d (n "dot_vox") (r "^4.1.0") (d #t) (k 0)) (d (n "stl_io") (r "^0.5.2") (d #t) (k 0)))) (h "0725f6iw0d0gf007npv9ra17symzzg40cdvgwhhclmdjxwxm26vh")))

