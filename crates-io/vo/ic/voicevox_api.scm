(define-module (crates-io vo ic voicevox_api) #:use-module (crates-io))

(define-public crate-voicevox_api-0.14.4 (c (n "voicevox_api") (v "0.14.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "04jvdp65ygim8mnz6mhvgp8m3zadm7mnv236hq43jpfdljkpkppy")))

