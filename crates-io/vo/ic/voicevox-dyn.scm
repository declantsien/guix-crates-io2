(define-module (crates-io vo ic voicevox-dyn) #:use-module (crates-io))

(define-public crate-voicevox-dyn-0.1.0 (c (n "voicevox-dyn") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "ouroboros") (r "^0.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1yy97h40cjy2kdsiisq0pvxljc2bga91sgykkm01xphlwf2dicar")))

(define-public crate-voicevox-dyn-0.2.0 (c (n "voicevox-dyn") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "ouroboros") (r "^0.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1yxbd7wgshsa922qhdjfyrhfdq9224g05n6y1h28mz89kflw2jgh")))

(define-public crate-voicevox-dyn-0.3.0 (c (n "voicevox-dyn") (v "0.3.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "ouroboros") (r "^0.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)))) (h "1svrj6pizy0sxpg04bpy44s8fg0r71vqa8imxm28ifrn7k279df3")))

