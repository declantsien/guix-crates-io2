(define-module (crates-io vo ic voicecode) #:use-module (crates-io))

(define-public crate-voicecode-0.1.0 (c (n "voicecode") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1f7aimnh43gwdpjdv5nfh8w7jx1zd1acx77vgz2f72b2hxma70h8") (y #t)))

(define-public crate-voicecode-0.2.0 (c (n "voicecode") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "070rw38z0x0jn6y21864mz08zmhjm9vw8jmzfvjbhgh84add0m6y")))

