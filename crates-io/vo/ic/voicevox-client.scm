(define-module (crates-io vo ic voicevox-client) #:use-module (crates-io))

(define-public crate-voicevox-client-0.1.0 (c (n "voicevox-client") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json" "rustls"))) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1iv61v4yysxhvlbxnq33myqr2piy2f99icb6xds4pf53ijvni7lj")))

(define-public crate-voicevox-client-0.1.1 (c (n "voicevox-client") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json" "rustls"))) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0z0qnjcp042kvw0vzjviqx6gx44ipbb443cps8vzwypx62gnj38h")))

