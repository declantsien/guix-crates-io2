(define-module (crates-io vo rm vorm) #:use-module (crates-io))

(define-public crate-vorm-0.1.0 (c (n "vorm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mongodb") (r "^2.3.0") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (d #t) (k 2)))) (h "03zg248avswxnc3ybzbmyp3a540m6niqp98z53i8raaxryq83nqw")))

(define-public crate-vorm-0.2.0 (c (n "vorm") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mongodb") (r "^2.3.0") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (d #t) (k 2)))) (h "1zvyqbybzd6svr3xs8i5hfk2c22h40aq78j81k0idrrdfb00bbcp")))

(define-public crate-vorm-0.2.1 (c (n "vorm") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mongodb") (r "^2.3.0") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (d #t) (k 2)))) (h "0p4ma70rf8lljiy3hcya2rbnph339w2bc7kmp7hk5afj584zaci3")))

