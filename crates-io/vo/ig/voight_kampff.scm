(define-module (crates-io vo ig voight_kampff) #:use-module (crates-io))

(define-public crate-voight_kampff-0.1.0 (c (n "voight_kampff") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1756milq0y1cna6sasr5a5l9jw8qs9886xn0fif5cp0np3pn2hnc")))

(define-public crate-voight_kampff-0.1.1 (c (n "voight_kampff") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "05i2vmnmh3gf9xbrhl6qd9wyi9p31j6jha87kf7isnqqqny1jrps")))

(define-public crate-voight_kampff-0.1.2 (c (n "voight_kampff") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1pjp82bkhz1gn32syxphqfaydhj2q69lnw8lbl1s98xis36z57hn")))

