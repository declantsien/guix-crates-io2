(define-module (crates-io vo c- voc-perturb) #:use-module (crates-io))

(define-public crate-voc-perturb-0.1.0 (c (n "voc-perturb") (v "0.1.0") (d (list (d (n "clap") (r "~2.28.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "imageproc") (r "^0.13") (d #t) (k 0)) (d (n "quick-xml") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ssf80y2arfhpls0wvc911478xdmdk0lcwhxkrm4mv2wkiy0mr2c")))

(define-public crate-voc-perturb-0.1.1 (c (n "voc-perturb") (v "0.1.1") (d (list (d (n "clap") (r "~2.28.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "imageproc") (r "^0.13") (d #t) (k 0)) (d (n "quick-xml") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1pjpa6j05kxi4fsz66kxmqcapz2nhyxl27d8ijx6fn7iahpka7bs")))

(define-public crate-voc-perturb-0.1.2 (c (n "voc-perturb") (v "0.1.2") (d (list (d (n "clap") (r "~2.28.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "imageproc") (r "^0.13") (d #t) (k 0)) (d (n "quick-xml") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0i842d07d7gjvq5k8iyvfm4jxijx37129332iysvm8f7p89jz1ym")))

