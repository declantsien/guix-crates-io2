(define-module (crates-io vo xt voxtree) #:use-module (crates-io))

(define-public crate-voxtree-0.1.0 (c (n "voxtree") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)))) (h "1q09g4hf1511d2m0b2ppwzcqs176ig1shrjn0b5pva1czj0g3rsy") (f (quote (("no_std"))))))

(define-public crate-voxtree-0.1.1 (c (n "voxtree") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)))) (h "0y92d3gkrrl7s82pg6vki5003wqnwmzvp83cmhk435hpagql0h8z") (f (quote (("no_std"))))))

(define-public crate-voxtree-0.1.2 (c (n "voxtree") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "1yzcdwavn3yf9m2fd25hzfxp43fkp3l8jaqqy54l5idmrp7dcljj") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:cgmath"))))))

(define-public crate-voxtree-0.1.3 (c (n "voxtree") (v "0.1.3") (d (list (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "09ixg84djkbl5apwm9kzr5jzvb4ddg18h5bqcs1r3qrsc9h42whq") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:cgmath"))))))

(define-public crate-voxtree-0.1.4 (c (n "voxtree") (v "0.1.4") (d (list (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "0ans44af5mrvs9nmkmk0w4lsagx32g9scz1i0da3wczwwl338pq0") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:cgmath"))))))

(define-public crate-voxtree-0.1.5 (c (n "voxtree") (v "0.1.5") (d (list (d (n "cgmath") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "1cfs7wr2jqab5dkqpb2q69gskjf0h6253k8ybfp7jk51mpkmnp02") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:cgmath"))))))

