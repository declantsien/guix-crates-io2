(define-module (crates-io vo x- vox-tool) #:use-module (crates-io))

(define-public crate-vox-tool-0.1.0 (c (n "vox-tool") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vox-format") (r "^0.1.0") (f (quote ("image"))) (d #t) (k 0)))) (h "1z82wbjcvvgqlf7zj7ccjsd10sv6wxcar7i3srnx9mri97zsk9aj")))

