(define-module (crates-io vo ll vollerei) #:use-module (crates-io))

(define-public crate-vollerei-0.1.0 (c (n "vollerei") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "0wb4mav5mdg6nhcf487dirncqlxysalay35c05zq7z0jsldbgjbf")))

(define-public crate-vollerei-0.1.1 (c (n "vollerei") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "0dk890nj20bjhgp56n0vixf5i1bzj6hmf73a8wdyh542p10xswbx") (y #t)))

(define-public crate-vollerei-0.1.2 (c (n "vollerei") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "0xcifnmpsv3dg2jcz5agxdy7z0r0rgv43xp6wrhsak4vlxspppjc") (y #t)))

(define-public crate-vollerei-0.1.3 (c (n "vollerei") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "1ar4nfpdj5sfnw3kif2phsh7whvswp5pbcqxky8bwsgf8jgysi59") (y #t)))

(define-public crate-vollerei-0.1.4 (c (n "vollerei") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "1y94ly2fk101yb5bkib83dabcvs5qk49mhg383n4wfcwabp85lgc")))

(define-public crate-vollerei-0.1.5 (c (n "vollerei") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "0r2f6wk831x8z3wymqd990zfi77lzblsr9zqksgpafp8nj2w7j4k")))

(define-public crate-vollerei-0.1.7 (c (n "vollerei") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "01mj8l6w3avjqzikr4dyjippwi45sq36iagm78mcwvdffck05zfm")))

