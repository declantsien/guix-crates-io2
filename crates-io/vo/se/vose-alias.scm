(define-module (crates-io vo se vose-alias) #:use-module (crates-io))

(define-public crate-vose-alias-1.0.0 (c (n "vose-alias") (v "1.0.0") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0i3yf8ah5q1ln46k3nnyvc8zllcr48jnz617wyjc60qpjl0qa179")))

