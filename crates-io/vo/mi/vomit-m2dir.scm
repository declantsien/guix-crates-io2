(define-module (crates-io vo mi vomit-m2dir) #:use-module (crates-io))

(define-public crate-vomit-m2dir-0.1.0 (c (n "vomit-m2dir") (v "0.1.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lettre") (r "^0.11") (d #t) (k 2)) (d (n "mail-parser") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1wzb6z09xgfyzrgfspgpc3q44xnzcaamqkb79rhw51l3nqs6nawk")))

(define-public crate-vomit-m2dir-0.1.1 (c (n "vomit-m2dir") (v "0.1.1") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lettre") (r "^0.11") (d #t) (k 2)) (d (n "mail-parser") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0032byzwdfq8ap65g4c31f97lgxsqa1hv8c2dh9bz9iwx9sh4zfp")))

