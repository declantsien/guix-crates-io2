(define-module (crates-io vo mi vomit-config) #:use-module (crates-io))

(define-public crate-vomit-config-0.1.0 (c (n "vomit-config") (v "0.1.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0vw3f2k0mg22fqwgwv59wxdl5gy097whwybqqmd8kz6cq0abxsjl")))

(define-public crate-vomit-config-0.2.0 (c (n "vomit-config") (v "0.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "10p4nmhl6q8wlynw71abir3kx1lwqwm4aph70xi84zg5lsyc531c")))

(define-public crate-vomit-config-0.2.1 (c (n "vomit-config") (v "0.2.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0y6c82an35i0skfjz616pxbhqyrx4yyik29cc49k20i44qfbgvs1")))

