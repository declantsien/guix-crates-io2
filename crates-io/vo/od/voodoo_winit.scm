(define-module (crates-io vo od voodoo_winit) #:use-module (crates-io))

(define-public crate-voodoo_winit-0.1.0 (c (n "voodoo_winit") (v "0.1.0") (d (list (d (n "voodoo") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.8") (d #t) (k 0)))) (h "00yp10jv68llnsinpdg3pdx4jkwk6qcbysxwffncqnh67f7wbfnh")))

(define-public crate-voodoo_winit-0.1.1 (c (n "voodoo_winit") (v "0.1.1") (d (list (d (n "voodoo") (r "^0.3") (d #t) (k 0)) (d (n "winit") (r "^0.10") (d #t) (k 0)))) (h "0hxka02x507v2bvxp885hhqv9xvagvg85h6qyq4m59x2bm7r11wb")))

