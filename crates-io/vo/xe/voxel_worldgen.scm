(define-module (crates-io vo xe voxel_worldgen) #:use-module (crates-io))

(define-public crate-voxel_worldgen-0.1.0 (c (n "voxel_worldgen") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.5") (d #t) (k 0)) (d (n "noise") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0p09mpf9a8c3vf6zysi2x080bxn7jnil9890apjsp4p5jyv6fb4w")))

