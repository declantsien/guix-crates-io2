(define-module (crates-io vo xe voxel-tile-raycast) #:use-module (crates-io))

(define-public crate-voxel-tile-raycast-0.1.0 (c (n "voxel-tile-raycast") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)))) (h "1piczf350k2ambgizadzhhmczhy88dqr6kmnm7z8pbqi8m7s7h4f") (f (quote (("voxel") ("tile") ("f64") ("f32") ("default" "f64" "tile" "voxel"))))))

