(define-module (crates-io vo xe voxelizer) #:use-module (crates-io))

(define-public crate-voxelizer-1.0.0 (c (n "voxelizer") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0y9k5nz4gyjfynyy6pgn2jlhjf3jcsj92s6v5rpmngdh90h6r3m4")))

