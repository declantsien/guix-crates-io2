(define-module (crates-io vo xe voxelify) #:use-module (crates-io))

(define-public crate-voxelify-0.1.0 (c (n "voxelify") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gltf") (r "^1.4.1") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0if50lwl2gyyb3qp010b5jr27rfsqccm7pm09jp4f116ba7lifzj")))

