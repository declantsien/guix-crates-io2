(define-module (crates-io vo xe voxel_chunk) #:use-module (crates-io))

(define-public crate-voxel_chunk-0.0.1 (c (n "voxel_chunk") (v "0.0.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1y2in78pyamdzilgz4xkydpibc1q92r482k4kwsk1fxc1k1akglc")))

(define-public crate-voxel_chunk-0.0.2 (c (n "voxel_chunk") (v "0.0.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0j2h4j4isj4z0wdlyg4zn5a0h343yrfgh64wdx8jjvdvkfkzdw18")))

