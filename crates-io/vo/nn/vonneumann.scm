(define-module (crates-io vo nn vonneumann) #:use-module (crates-io))

(define-public crate-vonneumann-1.0.0 (c (n "vonneumann") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "winnt" "errhandlingapi" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1qmhpjg3iga989pad4ssnv8zxy3flvwp3synh7wqxgvzxwhwnnjf") (f (quote (("nightly"))))))

(define-public crate-vonneumann-1.0.1 (c (n "vonneumann") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "winnt" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02yi2j5z85171za94x6nxbd52fpdr3fnwcbp50xhvqdlwaz9zcic") (f (quote (("nightly"))))))

(define-public crate-vonneumann-1.0.2 (c (n "vonneumann") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "winnt" "sysinfoapi"))) (t "cfg(windows)") (k 0)))) (h "1nylmayfssfpiczxpj091i668nzi7spsb7kfm1w39amzzd78d2vm") (f (quote (("nightly"))))))

