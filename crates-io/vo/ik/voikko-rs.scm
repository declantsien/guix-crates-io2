(define-module (crates-io vo ik voikko-rs) #:use-module (crates-io))

(define-public crate-voikko-rs-1.0.2 (c (n "voikko-rs") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "05w7h6wb6jij11c6qq7ybwbflwnlkmh63nap3m7r1c88bf4jx9qq")))

(define-public crate-voikko-rs-1.0.3 (c (n "voikko-rs") (v "1.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "0jbnclmg06454l8xsp18mhh1izfjl3wspvm97ifbd7xa1livnmbi")))

(define-public crate-voikko-rs-1.0.4 (c (n "voikko-rs") (v "1.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "0rslkxcf7rc9rq2npa1ayyzlw50ncsh4jj2dyilgzysjzx99mvpc")))

(define-public crate-voikko-rs-1.0.5 (c (n "voikko-rs") (v "1.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "15bbi0483q6k3xp24rqhqd672v17vb2d23rcbp97nvs06kp2kjx3")))

(define-public crate-voikko-rs-1.0.6 (c (n "voikko-rs") (v "1.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 0)))) (h "060rsws4ppy7g1kir09w26j40856r885fhlfwzn5jzx0d0yi4bc1")))

(define-public crate-voikko-rs-1.0.7 (c (n "voikko-rs") (v "1.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (k 0)))) (h "0xz26fsir87s5b8r1wbbmpfy0zp8r0kyk770fka3f4r4n1pkxbn6")))

(define-public crate-voikko-rs-1.1.0 (c (n "voikko-rs") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (k 0)))) (h "0hd4ri8qp1acbx6cpqg0lw5idpgiapffiivfql4a2z3p89vv6rcg")))

