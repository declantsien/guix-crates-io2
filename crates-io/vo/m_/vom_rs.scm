(define-module (crates-io vo m_ vom_rs) #:use-module (crates-io))

(define-public crate-vom_rs-0.1.0 (c (n "vom_rs") (v "0.1.0") (d (list (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0lc0hmrjjskd8bzdacc4c9q0a9v45ypzakl11sfg7yy5wzcha2sc")))

(define-public crate-vom_rs-0.1.1 (c (n "vom_rs") (v "0.1.1") (d (list (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1g3v9xafplmv25l4bcm7bwkr8v7j2v7q2fh36173p1hi52adlk76")))

(define-public crate-vom_rs-0.2.0 (c (n "vom_rs") (v "0.2.0") (d (list (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "05lx6qpif42v426xij9m3i1ljxkn2vf4b3zp356pxk94mdkn8433")))

(define-public crate-vom_rs-0.3.0 (c (n "vom_rs") (v "0.3.0") (d (list (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "06h1l05g5zn7xvd9jcab374gdwgddl81ps16mrdq4azsxvpvp8hn")))

(define-public crate-vom_rs-0.4.0 (c (n "vom_rs") (v "0.4.0") (d (list (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "094cc5n7ijkracm9s68niax5yg4rp5x2axjnlw90357b27zzyn55")))

(define-public crate-vom_rs-0.5.0 (c (n "vom_rs") (v "0.5.0") (d (list (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "158pkq50il9hcmv97dd5syms004b7zp041zl0i2hbd6kiw5my8as")))

(define-public crate-vom_rs-0.5.1 (c (n "vom_rs") (v "0.5.1") (d (list (d (n "decorum") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1bvi52laqi0cyz7bz1s4h8601gb4bwcdmfxrajqb4f6v3m63kkla")))

