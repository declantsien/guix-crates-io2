(define-module (crates-io vo lo volo_macro) #:use-module (crates-io))

(define-public crate-volo_macro-0.1.0 (c (n "volo_macro") (v "0.1.0") (h "043plvi0z78m1bk1ydy5vclv4g12kig9hdib9l1dz6fmpy50aziq")))

(define-public crate-volo_macro-0.1.1 (c (n "volo_macro") (v "0.1.1") (h "0pfsksnhphh67b1yacjl8p7n5swcqfdhgkxdwzqdj40m26nm6rnf")))

(define-public crate-volo_macro-0.1.2 (c (n "volo_macro") (v "0.1.2") (h "102nf9090m2va51gn45whnrmh9xpv9na7fhs69dfskyxb8qlnhgn")))

