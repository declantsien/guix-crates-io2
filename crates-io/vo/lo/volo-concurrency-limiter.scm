(define-module (crates-io vo lo volo-concurrency-limiter) #:use-module (crates-io))

(define-public crate-volo-concurrency-limiter-0.1.0 (c (n "volo-concurrency-limiter") (v "0.1.0") (d (list (d (n "volo") (r "^0.8") (d #t) (k 0)) (d (n "volo-grpc") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "volo-thrift") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0jf9bizjdrxlvlrmaw1h4jqdj89jsv62vw6y09r6hdzqbsxyy9z6") (s 2) (e (quote (("volo-thrift" "dep:volo-thrift") ("volo-grpc" "dep:volo-grpc"))))))

