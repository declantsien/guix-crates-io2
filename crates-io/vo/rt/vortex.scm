(define-module (crates-io vo rt vortex) #:use-module (crates-io))

(define-public crate-vortex-0.1.0 (c (n "vortex") (v "0.1.0") (d (list (d (n "actix-http") (r "^1.0.1") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1.1") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_regex") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "045y4an95s0p80lamcbmcyxb677r93qhdbxg8fd71m1zkbpw78a4")))

