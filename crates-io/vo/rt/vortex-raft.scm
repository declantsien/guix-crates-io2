(define-module (crates-io vo rt vortex-raft) #:use-module (crates-io))

(define-public crate-vortex-raft-0.1.0 (c (n "vortex-raft") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "ulid") (r "^1.1.0") (d #t) (k 0)))) (h "0cvvf7839wi4dn8yysk359m5jqzrhhgxis1sx48v5g9iqj4bchqp")))

