(define-module (crates-io vo uc voucherify_rs) #:use-module (crates-io))

(define-public crate-voucherify_rs-0.1.0 (c (n "voucherify_rs") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1mqqpgil7xjysx90y34gvp6hjhngx8qhhp8p62xr18bymf67dlz8")))

(define-public crate-voucherify_rs-0.1.1 (c (n "voucherify_rs") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1qb2dm80mgrz4rz4nizyhf6wnr35pzs964w3dhlxgybzl3cvgn0q")))

(define-public crate-voucherify_rs-1.0.0 (c (n "voucherify_rs") (v "1.0.0") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1nzmh98x7cj9wbfhy5vbq9diqds4w2jyvbdxhwjnyf4kbkn1b7pm")))

