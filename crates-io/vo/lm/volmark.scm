(define-module (crates-io vo lm volmark) #:use-module (crates-io))

(define-public crate-volmark-0.1.0 (c (n "volmark") (v "0.1.0") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "0zk1qgrw913h1whr64mnqh5dp3rz54kismsghjcdy5nb10bwcay2")))

