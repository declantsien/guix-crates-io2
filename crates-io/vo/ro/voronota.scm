(define-module (crates-io vo ro voronota) #:use-module (crates-io))

(define-public crate-voronota-0.1.0 (c (n "voronota") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "17wwarjmqbzkyvac4g3zk0cp3l0620my59d9pziqpbnp6id503yf")))

(define-public crate-voronota-0.1.1 (c (n "voronota") (v "0.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "11d805z858hgw5pa18fsdqbpr84w0bf12mpi5dj569bll6j6ss0p")))

