(define-module (crates-io vo ro voronoi) #:use-module (crates-io))

(define-public crate-voronoi-0.1.0 (c (n "voronoi") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "0sayqvg21fb5wh4va8f3ypmx0l96l8gfb6d5gv1vdcnf18ypkpgd")))

(define-public crate-voronoi-0.1.1 (c (n "voronoi") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "12viam2m8piiayzfmhi5ybpnn9l5axni3164cs0misnr9mnggarv")))

(define-public crate-voronoi-0.1.2 (c (n "voronoi") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16iaynm52mc89mkv2aspvvkzp1y7nhi57zl5zb50ary3vvg8wrmp")))

(define-public crate-voronoi-0.1.3 (c (n "voronoi") (v "0.1.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "11v87cbzyw8yya72ycvjqb93xz8ihsnn56slknsvv186198g95bi")))

(define-public crate-voronoi-0.1.4 (c (n "voronoi") (v "0.1.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "106hdklqdxsphj8vimk0svnc85aapz089ian495w1fh9k6jyqmn5")))

