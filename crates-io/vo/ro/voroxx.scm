(define-module (crates-io vo ro voroxx) #:use-module (crates-io))

(define-public crate-voroxx-0.1.0 (c (n "voroxx") (v "0.1.0") (d (list (d (n "cpp") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "cpp_build") (r ">=0.5.0, <0.6.0") (d #t) (k 1)))) (h "06mi1h77rh7i3skka1cjkshzvwfq48nzzvlpa93jp3ymcm64xq7x")))

(define-public crate-voroxx-0.1.1 (c (n "voroxx") (v "0.1.1") (d (list (d (n "cpp") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "cpp_build") (r ">=0.5.0, <0.6.0") (d #t) (k 1)))) (h "145sq9ybkxfim5nzwp9mxpddwmlix3m3fzzv7m5qhrwczdv1phs3")))

