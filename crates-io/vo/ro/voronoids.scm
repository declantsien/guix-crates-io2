(define-module (crates-io vo ro voronoids) #:use-module (crates-io))

(define-public crate-voronoids-0.1.0 (c (n "voronoids") (v "0.1.0") (d (list (d (n "kiddo") (r "^4.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "plotter") (r "^0.1.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)))) (h "17511adn875i6gxvrkwyqix9hq19zb88p385c46cgzaddrcxgyw8")))

