(define-module (crates-io vo ro voronator) #:use-module (crates-io))

(define-public crate-voronator-0.1.0 (c (n "voronator") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "plotters") (r "^0.2.15") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.54") (d #t) (k 2)))) (h "0zp752avf1l902v4sgx6csljgdci76bj0pvrzxw03fwcd5m8lnv9")))

(define-public crate-voronator-0.1.2 (c (n "voronator") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "plotters") (r "^0.2.15") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.54") (d #t) (k 2)) (d (n "svg") (r "^0.9.0") (d #t) (k 2)))) (h "0kkyciz9rsq8v1q40g20xj5my002mz17ccgzydk98k6rjvr4imn3")))

(define-public crate-voronator-0.2.0 (c (n "voronator") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "plotters") (r "^0.2.15") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.54") (d #t) (k 2)) (d (n "svg") (r "^0.9.0") (d #t) (k 2)))) (h "1xzcy80ymfkkn8pjg7wf735dwn5nckrygkgn9v04ppg8mgqiw3sv")))

(define-public crate-voronator-0.2.1 (c (n "voronator") (v "0.2.1") (d (list (d (n "maybe_parallel_iterator") (r "^0.12.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "plotters") (r "^0.2.15") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.54") (d #t) (k 2)) (d (n "svg") (r "^0.9.0") (d #t) (k 2)))) (h "1vhh5h4jkp9qkxfcshkssb6vb2mhyiy35nja5qzl8fwv9gzfdn07") (f (quote (("rayon" "maybe_parallel_iterator/rayon") ("default" "rayon"))))))

