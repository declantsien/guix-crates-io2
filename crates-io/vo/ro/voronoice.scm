(define-module (crates-io vo ro voronoice) #:use-module (crates-io))

(define-public crate-voronoice-0.0.1 (c (n "voronoice") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "delaunator") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "1cl8pvqzznpjmz16p9j4lp0g41hdsidb03y5k9028lrrhi8519fv")))

(define-public crate-voronoice-0.1.0 (c (n "voronoice") (v "0.1.0") (d (list (d (n "approx") (r "~0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "delaunator") (r "~0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "0zsmvdk1yinpxczaj9sj8afzdf7zw8d7zqg4lfxk7pw6vhp350fn")))

(define-public crate-voronoice-0.1.1 (c (n "voronoice") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "delaunator") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "1p063xczflyb9jpyydnjv1xj7lpdkzvczkx6x5yilywri97mpnnn")))

(define-public crate-voronoice-0.2.0 (c (n "voronoice") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "delaunator") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)) (d (n "robust") (r "^0.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "103xnbixxadlbfim4wy85w9v37w12mnb84bxkpzn4pv4lzicbb13")))

