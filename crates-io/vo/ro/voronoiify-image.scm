(define-module (crates-io vo ro voronoiify-image) #:use-module (crates-io))

(define-public crate-voronoiify-image-1.0.0 (c (n "voronoiify-image") (v "1.0.0") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)) (d (n "clap") (r "2.32.*") (d #t) (k 0)) (d (n "image") (r "0.19.*") (d #t) (k 0)) (d (n "num_cpus") (r "1.0.*") (d #t) (k 0)) (d (n "rand") (r "0.5.*") (d #t) (k 0)) (d (n "scoped-pool") (r "1.0.*") (d #t) (k 0)) (d (n "separator") (r "0.3.*") (d #t) (k 0)))) (h "0wqhzy47cszgbia9lgikfv0dja5a3nqh89xl0a8v7bz2w565pld1")))

(define-public crate-voronoiify-image-1.0.1 (c (n "voronoiify-image") (v "1.0.1") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)) (d (n "clap") (r "2.32.*") (d #t) (k 0)) (d (n "image") (r "0.19.*") (d #t) (k 0)) (d (n "num_cpus") (r "1.0.*") (d #t) (k 0)) (d (n "rand") (r "0.5.*") (d #t) (k 0)) (d (n "scoped-pool") (r "1.0.*") (d #t) (k 0)) (d (n "separator") (r "0.3.*") (d #t) (k 0)))) (h "1milnzry9gv6sl7bl63s2h2vpzzx59k82v4ig8rryjggqdl19nmx") (y #t)))

(define-public crate-voronoiify-image-1.0.2 (c (n "voronoiify-image") (v "1.0.2") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)) (d (n "clap") (r "2.32.*") (d #t) (k 0)) (d (n "image") (r "0.19.*") (d #t) (k 0)) (d (n "num_cpus") (r "1.0.*") (d #t) (k 0)) (d (n "rand") (r "0.5.*") (d #t) (k 0)) (d (n "scoped-pool") (r "1.0.*") (d #t) (k 0)) (d (n "separator") (r "0.3.*") (d #t) (k 0)))) (h "19rrz7ga2dzv9slr6qgv5k014mjdg831vrzdwlg0yy085askmkkn")))

