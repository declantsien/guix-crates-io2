(define-module (crates-io vo la volatile-ptr) #:use-module (crates-io))

(define-public crate-volatile-ptr-0.1.0 (c (n "volatile-ptr") (v "0.1.0") (h "1sh7iyvra0cmmpad3ixjnffg794sffbpmrjkg8wi3b9ffzcd3g09")))

(define-public crate-volatile-ptr-0.1.1 (c (n "volatile-ptr") (v "0.1.1") (h "08diq90aszdzsm1acknr61w5r4677gzlmyw7pr6xgmqnpid8209i")))

