(define-module (crates-io vo la volatile-macro) #:use-module (crates-io))

(define-public crate-volatile-macro-0.5.3-rc.1 (c (n "volatile-macro") (v "0.5.3-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pl44ijfw3akzfihn0h0vqqkgb1siv361w53ydbhkm6mp05rm8j1")))

(define-public crate-volatile-macro-0.5.3-rc.2 (c (n "volatile-macro") (v "0.5.3-rc.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00vps1r7yg3mn87faixzks0zxl3vazgc67jdqgdj598r35cwqvbk")))

(define-public crate-volatile-macro-0.5.3 (c (n "volatile-macro") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06lqv0iq2gwk50f8asbyd0ps8zl2hrpznhqh2za1qr48yz16wbkx")))

(define-public crate-volatile-macro-0.5.4 (c (n "volatile-macro") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0i8qr3qwamz28bkq4fivd2drbfqhjmrsw7xp5ap80snpx0w3ql60")))

