(define-module (crates-io vo la volatile_unique_id) #:use-module (crates-io))

(define-public crate-volatile_unique_id-0.1.0 (c (n "volatile_unique_id") (v "0.1.0") (h "136dk2v1nkq9yic6618x0h3mfqpsqjq1kr2awi4rdv0fm839zgkf")))

(define-public crate-volatile_unique_id-0.1.1 (c (n "volatile_unique_id") (v "0.1.1") (h "1wqbbdh1rv0lgcj8s1yrnmb3rrnbpdm7hq22kiwk36vdmkl66wny")))

