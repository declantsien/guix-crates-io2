(define-module (crates-io vo la volatile-register) #:use-module (crates-io))

(define-public crate-volatile-register-0.1.0 (c (n "volatile-register") (v "0.1.0") (h "1sdjkgz8s7wn4052ach5x0v13363906if0m5qb5pa84bfh8xp1a3")))

(define-public crate-volatile-register-0.1.1 (c (n "volatile-register") (v "0.1.1") (h "0fq91xxai6v34mqmv3w4kd9pjjndip8miaf0fhl81cqsghmhfkza")))

(define-public crate-volatile-register-0.1.2 (c (n "volatile-register") (v "0.1.2") (h "0jfm9wc2v4qsv32n9zp51snfhqsfh86djg6l7f4svlzjm2d8hw54")))

(define-public crate-volatile-register-0.2.0 (c (n "volatile-register") (v "0.2.0") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11ij0qrab4b1bc78bjm6k2s0hwgr9y22igfnn46r96yr2r3cnrqd")))

(define-public crate-volatile-register-0.2.1 (c (n "volatile-register") (v "0.2.1") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1dh8x7z1ywjzyziz3jyjj39imp37s05c46whf2pkyablkngz3s4y")))

(define-public crate-volatile-register-0.2.2 (c (n "volatile-register") (v "0.2.2") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1k0rkm81qyhn4r8f03z0sch2kyikkgjjfalpaami9c08c8m7whyy")))

