(define-module (crates-io vo la voladdress) #:use-module (crates-io))

(define-public crate-voladdress-0.2.0 (c (n "voladdress") (v "0.2.0") (d (list (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0j13s749ilbkca050m4xwbl7kq3yqc14pmf2pxnfq9nrj7n8zf94")))

(define-public crate-voladdress-0.2.1 (c (n "voladdress") (v "0.2.1") (d (list (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0mk1l2a6jg9ac0rm7msidgd1y1qlm6nv3p3j1dlzk50njk4nxirk")))

(define-public crate-voladdress-0.2.2 (c (n "voladdress") (v "0.2.2") (d (list (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1mw39warm086p80j4q0ibl6s3lvrkhcvkkxkjw8897rxb9m02n6j")))

(define-public crate-voladdress-0.2.3 (c (n "voladdress") (v "0.2.3") (d (list (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1wn4mnkdvmvhhv78k0m1fp4y7z85lm1l5glz7kpwcaklgczknvil")))

(define-public crate-voladdress-0.2.4 (c (n "voladdress") (v "0.2.4") (d (list (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1psbgashvr4hbmdai1224vra3j78fnic97b3l8jb5j8vvmiwb0m8")))

(define-public crate-voladdress-0.2.5 (c (n "voladdress") (v "0.2.5") (d (list (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0381z323ziivdzymzkihsbayyrqiyh6kxcr8crwp67552jmqa0i4")))

(define-public crate-voladdress-0.3.0 (c (n "voladdress") (v "0.3.0") (h "0hri3dc0j7wmg6f23xwxs28l4h10c9kack3p4ls7nn89765hlyci")))

(define-public crate-voladdress-0.4.0 (c (n "voladdress") (v "0.4.0") (h "14fhrlszn5gyj42dakx793k5vkgif0cw4x6vwiah2f9yj5cj1vd9") (y #t)))

(define-public crate-voladdress-0.4.1 (c (n "voladdress") (v "0.4.1") (h "13218fy52lp2klzslsg0y67n8wi9b9932c2yn9cb25jl7x2q0cnj") (y #t)))

(define-public crate-voladdress-0.4.2 (c (n "voladdress") (v "0.4.2") (h "0fdas9j3qmamd08dz5fyckw5ymjjpm0iijd4b55539gs34l9my19") (y #t)))

(define-public crate-voladdress-0.4.3 (c (n "voladdress") (v "0.4.3") (h "1bjvg0p9fp16krx27ib9wy16giy24107j3fwwfb9gkgnighk4h5i") (y #t)))

(define-public crate-voladdress-0.4.4 (c (n "voladdress") (v "0.4.4") (h "15czmrji6lhiss8rzvhcxp5qb29pmnjq6nicbcyzwd5smbif5g9h") (y #t)))

(define-public crate-voladdress-0.4.5 (c (n "voladdress") (v "0.4.5") (h "1bi70p9ydbp2fzfcii752d402an90b47ljxac756fnzs2nrh072y") (y #t)))

(define-public crate-voladdress-1.0.0 (c (n "voladdress") (v "1.0.0") (h "02zsnci428lfrzwgq59dqxr5jlhckjn4is8zprg4mmnd5wwl0g8z") (y #t)))

(define-public crate-voladdress-1.0.1 (c (n "voladdress") (v "1.0.1") (h "0rn9pzx4kfbwgv1v5b579c5f1q3f4a4r5p9xj4b9mi1d9nmiq1db") (f (quote (("experimental_volregion") ("default" "experimental_volregion")))) (y #t)))

(define-public crate-voladdress-1.0.2 (c (n "voladdress") (v "1.0.2") (h "02470s6lwjyrms384pcnfpl690vwplv7qf7dz8412b7gkx0x0mr1") (f (quote (("experimental_volregion")))) (y #t)))

(define-public crate-voladdress-1.1.0 (c (n "voladdress") (v "1.1.0") (h "12md01az49lffg52dc6wrdblgzawf8v8wa7q8l379yhxdjq82q0i") (f (quote (("experimental_volregion") ("experimental_volmatrix")))) (y #t)))

(define-public crate-voladdress-1.2.0 (c (n "voladdress") (v "1.2.0") (h "0a49sk39lzhj4fxazm6c549jpc69l4k9qc1bdsr579nd02c5xlc0") (f (quote (("experimental_volregion") ("experimental_volmatrix")))) (y #t)))

(define-public crate-voladdress-1.2.1 (c (n "voladdress") (v "1.2.1") (h "1m7sbycz7z6inv167l61y0cpbm7dsr37jwginqf608ng5j164900") (f (quote (("experimental_volregion") ("experimental_volmatrix")))) (y #t)))

(define-public crate-voladdress-1.2.2 (c (n "voladdress") (v "1.2.2") (h "113phshklrfwsvrf83wn6gjwdw94v5j7xhnib4nwcigm0dxvqvsp") (f (quote (("experimental_volregion") ("experimental_volmatrix"))))))

(define-public crate-voladdress-1.2.3 (c (n "voladdress") (v "1.2.3") (h "0aryfabv1wnm45nlhj46fgwzzw7lzc9h6hjyn71xgfwbm9bdpnfw") (f (quote (("experimental_volregion") ("experimental_volmatrix"))))))

(define-public crate-voladdress-1.3.0 (c (n "voladdress") (v "1.3.0") (h "1k2y1ffhk7f250zjxmc3c7ws3iv6g78bsdh96pwd75f9ggpkzgvz")))

(define-public crate-voladdress-1.4.0 (c (n "voladdress") (v "1.4.0") (h "113hn9h40y2bm369zv0p87dikc1gs525sh72j8cm85z7qykskx3m")))

