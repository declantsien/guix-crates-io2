(define-module (crates-io vo la volatile_cell) #:use-module (crates-io))

(define-public crate-volatile_cell-0.0.1 (c (n "volatile_cell") (v "0.0.1") (d (list (d (n "expectest") (r "*") (o #t) (d #t) (k 0)) (d (n "rust-libcore") (r "^0.0.1") (d #t) (t "thumbv6m-none-eabi") (k 0)) (d (n "rust-libcore") (r "^0.0.1") (d #t) (t "thumbv7em-none-eabi") (k 0)) (d (n "rust-libcore") (r "^0.0.1") (d #t) (t "thumbv7m-none-eabi") (k 0)))) (h "1jlfd8glgz5m2jka4fgp3n1rq5phr55qmays9lx2cs7vj4bmcajv") (f (quote (("replayer" "expectest"))))))

(define-public crate-volatile_cell-1.0.0 (c (n "volatile_cell") (v "1.0.0") (d (list (d (n "expectest") (r "*") (o #t) (d #t) (k 0)) (d (n "rust-libcore") (r "> 0.0.1") (d #t) (t "thumbv6m-none-eabi") (k 0)) (d (n "rust-libcore") (r "> 0.0.1") (d #t) (t "thumbv7em-none-eabi") (k 0)) (d (n "rust-libcore") (r "> 0.0.1") (d #t) (t "thumbv7m-none-eabi") (k 0)))) (h "0wms3c3s08bkwvzz9zclja90chwcqjjs6lnbzk2gczn4dk2l9778") (f (quote (("replayer" "expectest"))))))

