(define-module (crates-io vo la volatile) #:use-module (crates-io))

(define-public crate-volatile-0.1.0 (c (n "volatile") (v "0.1.0") (h "158abvy2chj6x4vflzk0qmc3xklq9ryp3i0fq654zfj01xndgi9p")))

(define-public crate-volatile-0.2.0 (c (n "volatile") (v "0.2.0") (h "1msds03wya7kdp95097kbg3zr6axn4v3i36dkaiz23xvawsh4v87")))

(define-public crate-volatile-0.2.1 (c (n "volatile") (v "0.2.1") (h "14ngacf4adwjci14840f5nsvmpbaigfgyfkmrfj5sn7j21hjikm7")))

(define-public crate-volatile-0.2.2 (c (n "volatile") (v "0.2.2") (h "170g06kghxnqdg2ai64hm6siva2qm9zyn35iv8f48kw1sxc15m0s")))

(define-public crate-volatile-0.2.3 (c (n "volatile") (v "0.2.3") (h "1h5h6xjvxia0nq6bmw02yx3r85zqx5f2cwl2nj98yn8n1wjwacv6") (f (quote (("const_fn"))))))

(define-public crate-volatile-0.2.4 (c (n "volatile") (v "0.2.4") (h "1s1zia2z9l67x2m2fq2gb7lbgrsffm85kyblm1253mpj5lx39m2l") (f (quote (("const_fn"))))))

(define-public crate-volatile-0.2.5 (c (n "volatile") (v "0.2.5") (h "002xq7gwz0jfa7m0ka4jf8jhkprnq50fp2rgbjflg3knalf3kjnr") (f (quote (("const_fn"))))))

(define-public crate-volatile-0.2.6 (c (n "volatile") (v "0.2.6") (h "0abzpkm4mh2kppvwxbr1y20capnng16j8n8iqlgw7b7snksyvw3a") (f (quote (("const_fn"))))))

(define-public crate-volatile-0.2.7 (c (n "volatile") (v "0.2.7") (h "0i8rlhllyd70n07q4hb3szzflfdlpp6lgmb96mqz3zh6xp9nmc7n") (f (quote (("const_fn"))))))

(define-public crate-volatile-0.3.0 (c (n "volatile") (v "0.3.0") (h "1rk0mviczy8n4hjl2f1qw088qqwh44ixmpyjcvhrcgzh12p6zrzq") (f (quote (("const_fn"))))))

(define-public crate-volatile-0.4.0 (c (n "volatile") (v "0.4.0") (h "0akpn3vjzzaqzk4pq5myn78pamvnga44hczz7s9hhs8lv5nxyygs") (f (quote (("unstable"))))))

(define-public crate-volatile-0.4.1 (c (n "volatile") (v "0.4.1") (h "1j1lncxa03wyjw4zc62331qgr76kivqvsdx62xjc94n7li4sj070") (f (quote (("unstable"))))))

(define-public crate-volatile-0.4.2 (c (n "volatile") (v "0.4.2") (h "13x0ry290q7x4yf5va72fcxvcgiidx62zn6iv6lw17r71xxd8lwb") (f (quote (("unstable"))))))

(define-public crate-volatile-0.4.3 (c (n "volatile") (v "0.4.3") (h "042fizq8zykq084xxvfc6dgz3ra7g1pz4v1m6j7y4lw3p8ww97ca") (f (quote (("unstable"))))))

(define-public crate-volatile-0.4.4 (c (n "volatile") (v "0.4.4") (h "1w34yiy99a7cl6n51y0dk5ci030g6xz20vky6mrkkddq9vadphp4") (f (quote (("unstable"))))))

(define-public crate-volatile-0.4.5 (c (n "volatile") (v "0.4.5") (h "033cdyvs12j7jph3nv8yw3xs9mmlgw6djkz0fjp612nskls9ijp3") (f (quote (("unstable"))))))

(define-public crate-volatile-0.4.6 (c (n "volatile") (v "0.4.6") (h "14x7bvfjgk1xnl07n5f9i433vrw7gix08b8rdhs9p0rc7z38fa24") (f (quote (("unstable"))))))

(define-public crate-volatile-0.5.0 (c (n "volatile") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "07n3gw4rlrlqnml997a6s7yacx4h8bkk2j3hprb0hdaqbzh7dv4m") (f (quote (("very_unstable" "unstable") ("unstable"))))))

(define-public crate-volatile-0.5.1 (c (n "volatile") (v "0.5.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0pnjd4cscrb7agbk34fjwq1xkb6cb1gb289ysk6h1bmpv7wwkaly") (f (quote (("very_unstable" "unstable") ("unstable"))))))

(define-public crate-volatile-0.5.2 (c (n "volatile") (v "0.5.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0dnrf5y4qq4h54wmdb07alg8sakka65jcmpjs94n18ds1i4fc55j") (f (quote (("very_unstable" "unstable") ("unstable"))))))

(define-public crate-volatile-0.5.3-rc.1 (c (n "volatile") (v "0.5.3-rc.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "volatile-macro") (r "=0.5.3-rc.1") (o #t) (d #t) (k 0)))) (h "0chj6296mma87zs1il3qsppry9whsy74w1kv13dygv1n8m1g5jj5") (f (quote (("very_unstable" "unstable") ("unstable")))) (s 2) (e (quote (("derive" "dep:volatile-macro"))))))

(define-public crate-volatile-0.5.3-rc.2 (c (n "volatile") (v "0.5.3-rc.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "volatile-macro") (r "=0.5.3-rc.2") (o #t) (d #t) (k 0)))) (h "0vbbqx0fixf8a0xvdhiz10ycllp0wn02lm3ici6pmilnq0mvjwal") (f (quote (("very_unstable" "unstable") ("unstable")))) (s 2) (e (quote (("derive" "dep:volatile-macro"))))))

(define-public crate-volatile-0.5.3 (c (n "volatile") (v "0.5.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "volatile-macro") (r "=0.5.3") (o #t) (d #t) (k 0)))) (h "13n884k00l771vdchpnh3v6b90jf7n9lzzwcx591vhm7pjagc5gg") (f (quote (("very_unstable" "unstable") ("unstable")))) (s 2) (e (quote (("derive" "dep:volatile-macro"))))))

(define-public crate-volatile-0.5.4 (c (n "volatile") (v "0.5.4") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "volatile-macro") (r "=0.5.4") (o #t) (d #t) (k 0)))) (h "0xryc4klhzfaca5nyng6awfray4jf9dcrzr0v50fzi2qs8vm6nph") (f (quote (("very_unstable" "unstable") ("unstable")))) (s 2) (e (quote (("derive" "dep:volatile-macro"))))))

