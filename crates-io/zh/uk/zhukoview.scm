(define-module (crates-io zh uk zhukoview) #:use-module (crates-io))

(define-public crate-zhukoview-0.0.1 (c (n "zhukoview") (v "0.0.1") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "pingus") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0gh1wrby94y8g8wpnn342a1vy8cbz422gmig2c5xlvn8fhckm8p5") (s 2) (e (quote (("rng" "dep:rand") ("png" "dep:pingus"))))))

