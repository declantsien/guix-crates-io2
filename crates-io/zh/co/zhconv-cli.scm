(define-module (crates-io zh co zhconv-cli) #:use-module (crates-io))

(define-public crate-zhconv-cli-0.1.0-alpha (c (n "zhconv-cli") (v "0.1.0-alpha") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "zhconv") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "1d04fdyfi9md73am9lyciwiazx0k3ag990z4pl1sxwqr21v75fgy")))

(define-public crate-zhconv-cli-0.1.0-beta (c (n "zhconv-cli") (v "0.1.0-beta") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "zhconv") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "1chpq3xvyf5jgi31kgmis86m82vayc4lsn7i0ic3jlbma4wq8i8k")))

(define-public crate-zhconv-cli-0.1.0-rc (c (n "zhconv-cli") (v "0.1.0-rc") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "zhconv") (r "^0.1.0-rc3") (d #t) (k 0)))) (h "020mwiqnk3rflkdqzcvr8kl52cdmzaayrh01125jz0ghab5l5h8c")))

(define-public crate-zhconv-cli-0.1.0 (c (n "zhconv-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "zhconv") (r "^0.1.0") (d #t) (k 0)))) (h "1gmiyfv386kg4qvswvj2mry64308p6vbrq3726lni30zdxk5w267")))

(define-public crate-zhconv-cli-0.2.0-beta (c (n "zhconv-cli") (v "0.2.0-beta") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "zhconv") (r "^0.2.0-beta") (d #t) (k 0)))) (h "1yjirvybj6f0y3fv7vck03bqf93k8nc649wczdys9hq0h0bs21is")))

(define-public crate-zhconv-cli-0.3.0-beta (c (n "zhconv-cli") (v "0.3.0-beta") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "zhconv") (r "^0.3.0-beta") (d #t) (k 0)))) (h "03g8yjfg7mvx9b6v5nm898a9hzkvr1bfms22990cdhb3glxwc3a6")))

(define-public crate-zhconv-cli-0.3.0 (c (n "zhconv-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "zhconv") (r "^0.3.0") (d #t) (k 0)))) (h "11ffga9y0jjw5ialms1j14jv23zx213fia732vksfy5bsdn76ssi")))

(define-public crate-zhconv-cli-0.3.1 (c (n "zhconv-cli") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "zhconv") (r "^0.3.1") (f (quote ("opencc"))) (d #t) (k 0)))) (h "01hvd2bki0jc6jkw8z6y3ddshnbxf3yrsy71ahd6nj014xzymr1m")))

