(define-module (crates-io zh ou zhou) #:use-module (crates-io))

(define-public crate-zhou-0.1.0 (c (n "zhou") (v "0.1.0") (h "18cdsqwvwq8xrg4kwvmmwk19q9awv5z36hghykn5lkmv82641yv5") (y #t)))

(define-public crate-zhou-0.1.1 (c (n "zhou") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "httpdate") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting" "macros" "parsing"))) (d #t) (k 0)))) (h "1qjqjijib7wnfyjg648m3xz41b1ibm40x1ngvavzd22f82gcrxv1") (y #t)))

(define-public crate-zhou-0.1.2 (c (n "zhou") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "httpdate") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting" "macros" "parsing"))) (d #t) (k 0)))) (h "1f3vaa967wphvyjapvha97zksja8pcchfj5xf6m4d9x93ljx7xfb")))

