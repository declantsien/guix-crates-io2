(define-module (crates-io zh ih zhihu-link) #:use-module (crates-io))

(define-public crate-zhihu-link-0.0.0 (c (n "zhihu-link") (v "0.0.0") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "htmler") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1biwvrnkncaqag4qxzfd2aqg1ymihcbj3hfrafrjfpvg2495fxn7") (f (quote (("default"))))))

