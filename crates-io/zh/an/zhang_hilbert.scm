(define-module (crates-io zh an zhang_hilbert) #:use-module (crates-io))

(define-public crate-zhang_hilbert-0.1.0 (c (n "zhang_hilbert") (v "0.1.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 2)))) (h "0j2ns4qarvw8cms1vnlmv7w73xwh54flvm5q77731p1l2nrbvzjb")))

(define-public crate-zhang_hilbert-0.1.1 (c (n "zhang_hilbert") (v "0.1.1") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 2)))) (h "1hkbirs5j7bakp73awga8i7frfgcx1ihkmfbm9wzkjiyh1hgxnsw")))

