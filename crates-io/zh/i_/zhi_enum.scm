(define-module (crates-io zh i_ zhi_enum) #:use-module (crates-io))

(define-public crate-zhi_enum-0.1.0 (c (n "zhi_enum") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 2)) (d (n "zhi_enum_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0r8jd5brrc2rh1a3ri7lhpxzvb40lswdwcjmya6v6zn27h06cnw9") (y #t)))

(define-public crate-zhi_enum-0.1.1 (c (n "zhi_enum") (v "0.1.1") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 2)) (d (n "zhi_enum_derive") (r "^0.1.0") (d #t) (k 0)))) (h "00y9wf736imv0316jzrbhqsgw5rnhcmkl4xp0znd85pan2mai9jr")))

(define-public crate-zhi_enum-0.1.2 (c (n "zhi_enum") (v "0.1.2") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 2)) (d (n "zhi_enum_derive") (r "^0.1.2") (d #t) (k 0)))) (h "08ywaxz6iqrkvadd5p6ql1gbl0hszwwicvpmi4nzbr19mhawvpq9")))

