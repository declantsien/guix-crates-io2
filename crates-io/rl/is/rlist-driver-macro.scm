(define-module (crates-io rl is rlist-driver-macro) #:use-module (crates-io))

(define-public crate-rlist-driver-macro-0.1.1 (c (n "rlist-driver-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "08ijfi5p2hnwx3v5sdaff3s06laiz2aa7812bi5jcwifdmh35s9m")))

(define-public crate-rlist-driver-macro-0.1.2 (c (n "rlist-driver-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "13a8xsc1xdz476gncrq8ki66ydpyg1p1ffylbyh88r5gjhvja1rq")))

(define-public crate-rlist-driver-macro-0.1.3 (c (n "rlist-driver-macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1csv9ycfmqncl9d97gmsjjhyrbl5bdkffxg2157hynm0kzyfpmri")))

(define-public crate-rlist-driver-macro-0.1.4 (c (n "rlist-driver-macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "11f8xbgzrii6h8k2rb58147lxz8wxvh351ar2ii2li6767di30sx")))

(define-public crate-rlist-driver-macro-0.1.5 (c (n "rlist-driver-macro") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "0c84vkjpdymqkbf3wy7ljdvhawjcf7xi73l8p8iirdhi680g13lw")))

(define-public crate-rlist-driver-macro-0.1.6 (c (n "rlist-driver-macro") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "0hfviysv70gib96zj46w63crc7csxrhz1cpx8l72gdhq5w0l874b")))

(define-public crate-rlist-driver-macro-0.1.7 (c (n "rlist-driver-macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "0zc1pqjqaa10zdxgs1cr2imgziq0zdyrqn3nf64nmjvzqw7mflc9")))

(define-public crate-rlist-driver-macro-0.1.8 (c (n "rlist-driver-macro") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "19gl1ljjk4dmw7lr34zvv4dwwxaw6s5k4jf344l52aq4j5gjig7b")))

(define-public crate-rlist-driver-macro-0.1.9 (c (n "rlist-driver-macro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "07yy0km46hk4jw4yd7gnx14kqzlh6nq481yz9blnvrn6c5yxzq41")))

(define-public crate-rlist-driver-macro-0.1.10 (c (n "rlist-driver-macro") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rlist_vfs") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "09i5g35rdn6d5z2yky52mjqgn2hxcsqzkpgq61y22qcdzqjkiy6k")))

