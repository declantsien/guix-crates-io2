(define-module (crates-io rl is rlisp) #:use-module (crates-io))

(define-public crate-rlisp-0.1.0 (c (n "rlisp") (v "0.1.0") (h "1y87yh9isbb8aqlk3j7c3rjaafz1p4yirdzvd37pifxvn46bi8z2")))

(define-public crate-rlisp-0.2.0 (c (n "rlisp") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1jwhwdk3991k2r72imwdnii4lll4vbc9i9dp05y9nl0d5d3j35il")))

