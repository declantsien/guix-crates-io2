(define-module (crates-io rl p- rlp-compress) #:use-module (crates-io))

(define-public crate-rlp-compress-0.1.0 (c (n "rlp-compress") (v "0.1.0") (d (list (d (n "elastic-array") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.4.5") (d #t) (k 0)))) (h "13142zva6cmcvhyy9qy3757h11fskvid0sna9j6ckq0wgpwcw5jy")))

