(define-module (crates-io rl p- rlp-iter) #:use-module (crates-io))

(define-public crate-rlp-iter-0.2.0 (c (n "rlp-iter") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)))) (h "0zxwsl8fibzsawsnz9idvy8603q4hvdwykbgsimawfipp2j4bqqd")))

(define-public crate-rlp-iter-0.2.1 (c (n "rlp-iter") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)))) (h "0ag1svl9bhkj8zb2c5xqkf0k4w6hr6s65pkbhnl8w373yvccgjw6")))

