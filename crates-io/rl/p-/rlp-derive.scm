(define-module (crates-io rl p- rlp-derive) #:use-module (crates-io))

(define-public crate-rlp-derive-0.1.0 (c (n "rlp-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "0ak73xi7zpw0zs5n30w6j2jmwfqdyb9hfagyjy3hsd0cpqm7ngg3")))

