(define-module (crates-io rl ib rlibproject) #:use-module (crates-io))

(define-public crate-rlibproject-0.1.0 (c (n "rlibproject") (v "0.1.0") (h "1bm1wwrbs7paqcj294jyxjxjmz3dnz105b656rp3pcr4m64d6d06") (y #t)))

(define-public crate-rlibproject-0.1.1 (c (n "rlibproject") (v "0.1.1") (h "0qjh3jkplq3fpyr3xzf3mbwpil4fikb2xx9qqfsv6ydf03jw96p4") (y #t)))

