(define-module (crates-io rl ib rlibcex) #:use-module (crates-io))

(define-public crate-rlibcex-0.1.0 (c (n "rlibcex") (v "0.1.0") (h "17za30djzjivph29iyhnrjk2ab6glp9x38by19sb7hwm58rrc8is")))

(define-public crate-rlibcex-0.1.2 (c (n "rlibcex") (v "0.1.2") (h "1kjazzkpb2qjv51n7apd1xr722pln85jm5jf29h0fq0mah12shs7")))

