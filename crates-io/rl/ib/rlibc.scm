(define-module (crates-io rl ib rlibc) #:use-module (crates-io))

(define-public crate-rlibc-0.0.1 (c (n "rlibc") (v "0.0.1") (h "1ln88v17xyy7nz7iyv0pj7wzfpx7281qcfiz2xnpsz467jr9sdf5")))

(define-public crate-rlibc-0.0.2 (c (n "rlibc") (v "0.0.2") (h "0sdx0rp9p645cqxsvaqvpn3n3d33wv5fly2njk56zmw89az99a1s")))

(define-public crate-rlibc-0.1.0 (c (n "rlibc") (v "0.1.0") (h "0iqg09mys6w6gf2ispck683vq0l758a5r068bh3srl11lcq5knni")))

(define-public crate-rlibc-0.1.1 (c (n "rlibc") (v "0.1.1") (h "0aa8518dcv2w1b4xsyxkbaj8sc2gx5029rr1wfzmxayd42n021q2")))

(define-public crate-rlibc-0.1.2 (c (n "rlibc") (v "0.1.2") (h "0rgcf5xcvyh795p4l4hsyyna6q7rvicmg5p4xbvkvx67wmlbzlhy")))

(define-public crate-rlibc-0.1.3 (c (n "rlibc") (v "0.1.3") (h "0l2kjc608shqvi06lkdwk02v4l10qf7l1czcp6vgrv6z09g3h0vm")))

(define-public crate-rlibc-0.1.4 (c (n "rlibc") (v "0.1.4") (h "0157rcxqvm911bi092h8qlk0m1v2ai2f6rjynaqmbqdnh3aif9f2")))

(define-public crate-rlibc-0.1.5 (c (n "rlibc") (v "0.1.5") (h "0jxh6g7a3fs40r6yxwlpimnbnadnasqsvwb1kj34mv80psh5ywjg")))

(define-public crate-rlibc-1.0.0 (c (n "rlibc") (v "1.0.0") (h "1zh8xs0j9s799vnj3n9a1r881as52al66rzijbbi9w35fw94p1zw")))

