(define-module (crates-io rl da rldap) #:use-module (crates-io))

(define-public crate-rldap-0.1.0 (c (n "rldap") (v "0.1.0") (d (list (d (n "ldap3") (r "^0.11.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (f (quote ("vendored"))) (d #t) (k 0)))) (h "0k6lyhx4xfr3mk3481f1mlvbflp616dwvics86khmyr3xss802h3")))

