(define-module (crates-io rl e- rle-decode-helper) #:use-module (crates-io))

(define-public crate-rle-decode-helper-1.0.0-alpha (c (n "rle-decode-helper") (v "1.0.0-alpha") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0jh8h4rqkmckchp40grzkxgkxbjbrgk2gm8fd2fzcdj9qanvdyvd") (y #t)))

(define-public crate-rle-decode-helper-1.0.0-alpha2 (c (n "rle-decode-helper") (v "1.0.0-alpha2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1rhvi778dpz2kvm6g6in5ilbialy8g337hbrxyz1mjcb34awkizw") (y #t)))

