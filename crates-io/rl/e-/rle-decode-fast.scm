(define-module (crates-io rl e- rle-decode-fast) #:use-module (crates-io))

(define-public crate-rle-decode-fast-1.0.0 (c (n "rle-decode-fast") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0xpxkbnj2rz7066hhb46wiq67w5lky9k4qyaar8j0n881l4y9ldg") (f (quote (("bench" "criterion"))))))

(define-public crate-rle-decode-fast-1.0.1 (c (n "rle-decode-fast") (v "1.0.1") (d (list (d (n "criterion") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1b4h7qs4mssc5dnlhs3f91ya8pb40bv72zzshl18gify2jllzgna") (f (quote (("bench" "criterion"))))))

(define-public crate-rle-decode-fast-1.0.2 (c (n "rle-decode-fast") (v "1.0.2") (d (list (d (n "criterion") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0647n1p09237j62y1qfz4lqsxaib09661x5wz3vrf0fc4l7khvj6") (f (quote (("bench" "criterion"))))))

(define-public crate-rle-decode-fast-1.0.3 (c (n "rle-decode-fast") (v "1.0.3") (d (list (d (n "criterion") (r "^0.2") (o #t) (d #t) (k 0)))) (h "08kljzl29rpm12fiz0qj5pask49aiswdvcjigdcq73s224rgd0im") (f (quote (("bench" "criterion"))))))

