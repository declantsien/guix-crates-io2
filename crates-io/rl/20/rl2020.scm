(define-module (crates-io rl #{20}# rl2020) #:use-module (crates-io))

(define-public crate-rl2020-0.1.0 (c (n "rl2020") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)))) (h "15y123bz2p0ny0bggvxw37lrk0k6vlmz30h8nr580dprb672zmzm")))

