(define-module (crates-io rl p2 rlp2) #:use-module (crates-io))

(define-public crate-rlp2-0.2.0 (c (n "rlp2") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "08pnac68m5fmvssmfdzf3wqqg2w2p78rfjk910fmc0ihdcn4fnv1")))

