(define-module (crates-io rl vm rlvm) #:use-module (crates-io))

(define-public crate-rlvm-0.0.1 (c (n "rlvm") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "1l3mh40b9nxricbifjksis9n4wp8n64008b7sa6y7p3kwwbif3d8")))

(define-public crate-rlvm-0.0.2 (c (n "rlvm") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "1m2b7jy4kq4fhj72dwwbkf1zg40bs2ibxha76ds668zz74y9bm1c")))

