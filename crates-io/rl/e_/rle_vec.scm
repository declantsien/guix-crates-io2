(define-module (crates-io rl e_ rle_vec) #:use-module (crates-io))

(define-public crate-rle_vec-0.1.0 (c (n "rle_vec") (v "0.1.0") (h "1s2sjapi8l5n4miimhf4az6k1w6w6p3f59lp2c49f78rp2p2dm8m")))

(define-public crate-rle_vec-0.2.0 (c (n "rle_vec") (v "0.2.0") (h "08rnr8043irivjnvq94hlqaz35nmz8qd7zqcya088ar7r48v7dni")))

(define-public crate-rle_vec-0.2.1 (c (n "rle_vec") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "06723czj3awsdl0amrg2b2fv44hnp6gqaj6asl5q8zd0xym07yfi")))

(define-public crate-rle_vec-0.2.2 (c (n "rle_vec") (v "0.2.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13b3mgx311kgmsi1pq8yh2m9gfk77pm5kncpdj1pp3msgavj1qbi")))

(define-public crate-rle_vec-0.3.0 (c (n "rle_vec") (v "0.3.0") (h "18qlfrwnfxc4p2cacdkla4rr042ngvdvvdlcxad3h0k659n42xbr")))

(define-public crate-rle_vec-0.3.1 (c (n "rle_vec") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0yqvvw9qvg836h2wlp5daw2ynv98kja4d13dsf5wiiga3yx3ic0v")))

(define-public crate-rle_vec-0.4.0 (c (n "rle_vec") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0z8p9nfi7szssjx604kcy0l3jqh7swyla1iyvdgvcgd70bfl3sxk") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-rle_vec-0.4.1 (c (n "rle_vec") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0084hqsvd5y5224h7vlvgjry6w7bf6azzlwk88jpgyff4hpn453s") (f (quote (("serialize" "serde" "serde_derive"))))))

