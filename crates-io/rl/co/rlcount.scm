(define-module (crates-io rl co rlcount) #:use-module (crates-io))

(define-public crate-rlcount-0.1.0 (c (n "rlcount") (v "0.1.0") (h "152jdk4mdi5gbxmnz3y9mf33mdr7sa51mj8fy46frd3i0lx4qqxs")))

(define-public crate-rlcount-0.1.1 (c (n "rlcount") (v "0.1.1") (h "1jbz1z068carb6x5c0i6kjrjiyvrspkhq6j6lpwixfgsszd3y0s1")))

(define-public crate-rlcount-0.2.0 (c (n "rlcount") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("suggestions" "color"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1akl695y39jjrjszdc028klfds6r2vzap6mfh4p9j36rrl167bj7")))

(define-public crate-rlcount-0.2.1 (c (n "rlcount") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("suggestions" "color"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1xdvljvgdvf7hs7dahc1fv846dpg0n7y48swl982kq6dmh35z31f")))

