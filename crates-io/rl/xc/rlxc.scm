(define-module (crates-io rl xc rlxc) #:use-module (crates-io))

(define-public crate-rlxc-0.1.0 (c (n "rlxc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lxc-sys") (r ">=0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0apcwi56awssylax2p2vm7ii3pl03fbcz487idi39zr4gf2g3al5")))

