(define-module (crates-io rl ua rlua-async) #:use-module (crates-io))

(define-public crate-rlua-async-0.1.0 (c (n "rlua-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)))) (h "01gmmbxqd8dvs5kbp24m5h4g1a39vvqiz1s8qn12b7ryqq7wriib")))

(define-public crate-rlua-async-0.1.1 (c (n "rlua-async") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)))) (h "0ahvf17358i2a3x32d9dhidhw94j5mf2nr9s3li936p3q1kawqdx")))

(define-public crate-rlua-async-0.2.0 (c (n "rlua-async") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)))) (h "00vsmh54s3bv19q63rxkv4z2v6vpmkxaj1afa9apipvywzjqn7jg")))

(define-public crate-rlua-async-0.3.0 (c (n "rlua-async") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)))) (h "0r21xpaqq1s44f3sgkkj2ims6lmakka0shva28f1lf44qgbzcivd")))

(define-public crate-rlua-async-0.4.0 (c (n "rlua-async") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)))) (h "0jxshmr7iycgwj4m426mx08vdldclhvi9wg3qd43z4qqns10jdf2")))

(define-public crate-rlua-async-0.5.0 (c (n "rlua-async") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "rlua") (r "^0.19.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)))) (h "1gq24zwcrqa2w9bisw7xbr6ac272j3s36b26rsxs21v0vywnz08q")))

