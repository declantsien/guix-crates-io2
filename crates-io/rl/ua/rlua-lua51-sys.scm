(define-module (crates-io rl ua rlua-lua51-sys) #:use-module (crates-io))

(define-public crate-rlua-lua51-sys-0.1.0 (c (n "rlua-lua51-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0zh2bjrla75n7qd89xqczxyrwid036vifz286jilkkqg48fk1zvw") (f (quote (("static") ("lua51-pkg-config") ("default")))) (l "lua5.1")))

(define-public crate-rlua-lua51-sys-0.1.1 (c (n "rlua-lua51-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1q9wd8llc3i4sw2v88b8jp2y7m6jlgp4322la2slj4rki004br0q") (f (quote (("static") ("lua51-pkg-config") ("default")))) (l "lua5.1")))

(define-public crate-rlua-lua51-sys-0.1.2 (c (n "rlua-lua51-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "143vkr6p9kcy5jlfmda20yg37gd47mcabihr0brr70mp750vvslc") (f (quote (("static") ("lua51-pkg-config") ("default")))) (l "lua5.1")))

(define-public crate-rlua-lua51-sys-0.1.3 (c (n "rlua-lua51-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1qz8vdmq48xyf9mii18qmqzchl8j2i002fmchk6715j88kw0fnxy") (f (quote (("static") ("lua51-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.1")))

(define-public crate-rlua-lua51-sys-0.1.5 (c (n "rlua-lua51-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1490lax2pj5qqjq0flfm5wcn03690684g4rxcab322pcxs37m2a8") (f (quote (("static") ("lua51-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.1")))

(define-public crate-rlua-lua51-sys-0.1.6 (c (n "rlua-lua51-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0lpxii46w6d2jb069g6n7dz7mv1w6gcz25a42mf4p99zn0v4d3sw") (f (quote (("static") ("luajit-pkg-config") ("lua51-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.1")))

