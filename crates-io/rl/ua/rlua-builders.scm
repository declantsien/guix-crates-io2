(define-module (crates-io rl ua rlua-builders) #:use-module (crates-io))

(define-public crate-rlua-builders-0.1.0 (c (n "rlua-builders") (v "0.1.0") (d (list (d (n "rlua") (r "^0.17") (d #t) (k 0)))) (h "19khd42zf9k60qndq24ar0hchnh03a0rp78chdg49bd77c7xyxk1")))

(define-public crate-rlua-builders-0.1.1 (c (n "rlua-builders") (v "0.1.1") (d (list (d (n "rlua") (r "^0.17") (d #t) (k 0)))) (h "10g0grpq7pncv1y0wrhcqzw2wpw9yc2zksnm53fpqyj967zy09d6")))

(define-public crate-rlua-builders-0.1.2 (c (n "rlua-builders") (v "0.1.2") (d (list (d (n "rlua") (r "^0.17") (d #t) (k 0)))) (h "1j7al4x5a7sgmy1b9zarfqv0qwra820wsh2f2rvam2imqklsq2p5")))

(define-public crate-rlua-builders-0.1.3 (c (n "rlua-builders") (v "0.1.3") (d (list (d (n "rlua") (r "^0.17") (d #t) (k 0)) (d (n "rlua-builders-derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "14aq8p50mlpf0k8ldgzkp6hdg742y2gn278h2far2cq7w99zsbrq") (f (quote (("derive" "rlua-builders-derive") ("default" "derive"))))))

(define-public crate-rlua-builders-0.1.4 (c (n "rlua-builders") (v "0.1.4") (d (list (d (n "rlua") (r "^0.17") (d #t) (k 0)) (d (n "rlua-builders-derive") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0kinyiwv0dkav1h17nyaybs61svv03x7r82rllilbg69k0lv0cq4") (f (quote (("derive" "rlua-builders-derive") ("default" "derive"))))))

