(define-module (crates-io rl ua rlua-table-derive) #:use-module (crates-io))

(define-public crate-rlua-table-derive-0.1.0 (c (n "rlua-table-derive") (v "0.1.0") (d (list (d (n "quote") (r "0.3.*") (d #t) (k 0)) (d (n "rlua") (r "0.10.*") (d #t) (k 0)) (d (n "syn") (r "^0.11.0") (d #t) (k 0)))) (h "0blch459qwlxiia2fa5kqvnnbhbxhx3bygh8gnnhmxvw354w6z7c")))

