(define-module (crates-io rl ua rlua-lua54-sys) #:use-module (crates-io))

(define-public crate-rlua-lua54-sys-0.1.0 (c (n "rlua-lua54-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1bysi0m3d79a18jcxycwskwagm78r76n7qglsfj4hkkqkb5zl2db") (f (quote (("static") ("lua54-pkg-config") ("default")))) (l "lua5.4")))

(define-public crate-rlua-lua54-sys-0.1.1 (c (n "rlua-lua54-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0w4khn5lr11r1i384ncbh7bfpxaldc3wa9pyn6hwyyjzdbygvqdl") (f (quote (("static") ("lua54-pkg-config") ("default")))) (l "lua5.4")))

(define-public crate-rlua-lua54-sys-0.1.2 (c (n "rlua-lua54-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0dwimlqlzgrsjsg5kl6hmhrsigjdkdqvw4byrrfi2bbqlqckr8lw") (f (quote (("static") ("lua54-pkg-config") ("default")))) (l "lua5.4")))

(define-public crate-rlua-lua54-sys-0.1.3 (c (n "rlua-lua54-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0a7hkkxz37zagk64kmn1kxds25l4w8q8znjz40ngnxiygiwlibi3") (f (quote (("static") ("lua54-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.4")))

(define-public crate-rlua-lua54-sys-0.1.4 (c (n "rlua-lua54-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0d3rf2cz593vikiajcw0i6xvr31zjs6nyhr8vz2vrc5fnl125x4k") (f (quote (("static") ("lua54-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.4")))

(define-public crate-rlua-lua54-sys-0.1.5 (c (n "rlua-lua54-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1ah53sy4kkm4j2vzrp7r6mzxpsqick8xi0jx014qiw9yy5yszc98") (f (quote (("static") ("lua54-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.4")))

(define-public crate-rlua-lua54-sys-0.1.6 (c (n "rlua-lua54-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0x34x6mws16l5vmdpkzrsasz8sylydznv9c1psib8p49w6pspbvs") (f (quote (("static") ("lua54-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.4")))

