(define-module (crates-io rl ua rluau) #:use-module (crates-io))

(define-public crate-rluau-0.1.0 (c (n "rluau") (v "0.1.0") (d (list (d (n "luau-src") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0fwsfm78g3d179x4wwm17lqf399dvfprh15vms9v0z8bwbi5rwr8")))

(define-public crate-rluau-0.1.1 (c (n "rluau") (v "0.1.1") (d (list (d (n "luau-src") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.3") (d #t) (k 0)))) (h "1f8262vsl4rpdlpga2cpyjdnmm5slfg3zmlq4hshafzpv78f3256")))

