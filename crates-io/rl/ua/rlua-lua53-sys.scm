(define-module (crates-io rl ua rlua-lua53-sys) #:use-module (crates-io))

(define-public crate-rlua-lua53-sys-0.1.0 (c (n "rlua-lua53-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "16ry9plgvaq276c8dvqvcrjis14k6bsl28fxizcaz758dx28xjra") (f (quote (("static") ("lua53-pkg-config") ("default")))) (l "lua5.3")))

(define-public crate-rlua-lua53-sys-0.1.1 (c (n "rlua-lua53-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0cvq2jx6bfcm3zyfpbshahfhkmxravfz3gj4qa1m4fczpc4bpwxa") (f (quote (("static") ("lua53-pkg-config") ("default")))) (l "lua5.3")))

(define-public crate-rlua-lua53-sys-0.1.2 (c (n "rlua-lua53-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1jykh5r84xax4d4lsdypzjkvv2j3njsx71p1yx49nmmyl2vran3s") (f (quote (("static") ("lua53-pkg-config") ("default")))) (l "lua5.3")))

(define-public crate-rlua-lua53-sys-0.1.3 (c (n "rlua-lua53-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1pkl3xn3xzrjwsq37xa8i8fx1wl1prpzasxg8vgyc80vhq8q3hbs") (f (quote (("static") ("lua53-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.3")))

(define-public crate-rlua-lua53-sys-0.1.5 (c (n "rlua-lua53-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "11k9ml1a100apffmh3jd2fbqvxba6lcwibapjrlzqlr4r6hlm5ys") (f (quote (("static") ("lua53-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.3")))

(define-public crate-rlua-lua53-sys-0.1.6 (c (n "rlua-lua53-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "197lp2vc1myp2r1qkzdhnwfp3vga82jwy5w6xiyjfl0l4kgcjc69") (f (quote (("static") ("lua53-pkg-config") ("lua-no-oslib") ("default")))) (l "lua5.3")))

