(define-module (crates-io rl ua rlua_serde) #:use-module (crates-io))

(define-public crate-rlua_serde-0.1.0 (c (n "rlua_serde") (v "0.1.0") (d (list (d (n "rlua") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "02fd9p656mh932gas9gsdnzzjvc6sg1vh772lyd21x4k1v6dqz3h")))

(define-public crate-rlua_serde-0.2.0 (c (n "rlua_serde") (v "0.2.0") (d (list (d (n "rlua") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1gq348pzfpg2qdbiz0j0mcf83dkiccj7p4w1a46dax5inqzrzfkq")))

(define-public crate-rlua_serde-0.3.0 (c (n "rlua_serde") (v "0.3.0") (d (list (d (n "rlua") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0gq47jq069isi5f6bl2ikzsf76cbwzinncipavk85jls45x5s1kh")))

(define-public crate-rlua_serde-0.3.1 (c (n "rlua_serde") (v "0.3.1") (d (list (d (n "rlua") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "09a71dm52naylgx9zjqj6xgzy11h6s1z0vr0fbrkqxdxlrp2l1ng")))

(define-public crate-rlua_serde-0.4.0 (c (n "rlua_serde") (v "0.4.0") (d (list (d (n "rlua") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0sr51mj8xy9v17a8blg6x4y0hp3a1mm0rbyx8s5g3igwgf67rra6")))

