(define-module (crates-io rl if rlife) #:use-module (crates-io))

(define-public crate-rlife-0.0.7 (c (n "rlife") (v "0.0.7") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1bhvabsk8ys3wvhxsbl70046l8yyrdj3g9sx3aakjkl6g1y6vr0n")))

(define-public crate-rlife-0.1.0 (c (n "rlife") (v "0.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "04kpyxn6yz904aqhwwra5y8yf7hl0w6fkajjp5l7ddldac712a23")))

