(define-module (crates-io rl ir rliron) #:use-module (crates-io))

(define-public crate-rliron-0.0.1 (c (n "rliron") (v "0.0.1") (h "0ghz2z3kcrn7h12wz0q9pp76jddhm465y769df4fqmwkg90klad8")))

(define-public crate-rliron-0.0.2 (c (n "rliron") (v "0.0.2") (h "0ch9x78ki0qm4ibhiq1h5pppimwc3i5jhly74v7rf0rx3xh0z0x1")))

(define-public crate-rliron-0.0.3 (c (n "rliron") (v "0.0.3") (h "0glf5r8v5sdymsxck8mh1nj6jr4dlr5pv4n3q23kjrfi0wwzngf8")))

(define-public crate-rliron-0.0.4 (c (n "rliron") (v "0.0.4") (h "1m65aq1fgpbvj61qb0kjhc34f4gnkdyg0rw4bby96fj09cfsfnmr")))

(define-public crate-rliron-0.0.5 (c (n "rliron") (v "0.0.5") (h "0aq8vyplg4h75gq2nbj0zlk23j6dkgi3lqdjw30jq29d5zriv7qp")))

(define-public crate-rliron-0.0.6 (c (n "rliron") (v "0.0.6") (h "0jyw4j3s5r9rrksmdnvvqh161xpl0wsgcc5kimmr4418lzphm9kk")))

