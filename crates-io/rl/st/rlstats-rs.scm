(define-module (crates-io rl st rlstats-rs) #:use-module (crates-io))

(define-public crate-rlstats-rs-0.1.0 (c (n "rlstats-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "1vscppipfk5n1vz514qw13lq8ll5n0mgaflk5y8z57hvv2nk42kf")))

(define-public crate-rlstats-rs-0.1.1 (c (n "rlstats-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "1765d7b49ndp8lkw5gljmp47n7l8hj0mxrpqsw33vn649f93b9j4")))

(define-public crate-rlstats-rs-0.1.2 (c (n "rlstats-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1477synmhhlhx1di14fv1hfmyipfzzpjw20pab373dkcad8f03fp")))

