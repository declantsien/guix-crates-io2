(define-module (crates-io rl og rlogs) #:use-module (crates-io))

(define-public crate-rlogs-0.1.0 (c (n "rlogs") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "04nx4kzivp3lwa88bl4dyd2vvgjf21n9jifylpxbw56cpsa8cr8d")))

(define-public crate-rlogs-0.1.1 (c (n "rlogs") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1ssb8wcqpc0qrnlk002pjxv93szcipxzs6ls0cwllcaijbgiq091")))

(define-public crate-rlogs-0.1.2 (c (n "rlogs") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "14ficasvnwavm2bkmh21gzjpnkngvrny77372dafvl2bxhjmn9fc")))

