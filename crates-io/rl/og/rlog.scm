(define-module (crates-io rl og rlog) #:use-module (crates-io))

(define-public crate-rlog-0.1.0 (c (n "rlog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "016dj9smjc0nfl1sp9iigyzsdpifm6cwbx79s1s2jdswwhbw2swm")))

(define-public crate-rlog-0.1.1 (c (n "rlog") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0l2wa095h2njnb434bcq9vi7hhkajy7kraqsik41clsk72iy0xm3")))

(define-public crate-rlog-0.1.2 (c (n "rlog") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1jabadx8y2hwsyks8jils4f6lxgfc1lbvcb6sh3vd8m4lmfv5gb6")))

(define-public crate-rlog-0.1.3 (c (n "rlog") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1n6zgh4r3bx2b6s0zyi345d0m3dn8spd4pzivaj200lp2d4d1d6g")))

(define-public crate-rlog-0.1.4 (c (n "rlog") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "004m5jisk7ajqx0ygprqhplw3sfrladmp7i1sfglzr58m2a49vj3")))

(define-public crate-rlog-0.2.0 (c (n "rlog") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0ijh5734d2m3kxq94g7if1wmp8gy4kkvqjfwfaq816g5lyiqnvw2")))

(define-public crate-rlog-1.0.0 (c (n "rlog") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1kvp52a1n6wffqxyjfzgiv4yfa51wivfkx26jmaxczb9dliyky8v")))

