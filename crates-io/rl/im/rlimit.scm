(define-module (crates-io rl im rlimit) #:use-module (crates-io))

(define-public crate-rlimit-0.1.0 (c (n "rlimit") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11hdm982jri2abb4prc15p4703gd5sccy1imgld203qdgjjkdnym") (y #t)))

(define-public crate-rlimit-0.2.0 (c (n "rlimit") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05ivlm5p0a85rxn2ndi221zpk87y2nq71fakvlbmr69h4cy7l7x9") (y #t)))

(define-public crate-rlimit-0.2.1 (c (n "rlimit") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1czk6g94qb81da8dky4kkvcdrl6z4krf81ps5gf3h553micjz3q0") (y #t)))

(define-public crate-rlimit-0.3.0 (c (n "rlimit") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s7ac6fi4q4f2djg5ljbch7ab3shyh9fq2gjqll2c7iax1jnxlp6") (y #t)))

(define-public crate-rlimit-0.4.0 (c (n "rlimit") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dhm9jm1bvm4fk3839jw95gxs99yg0cwl9awwkyaclw3qdi2vc29") (y #t)))

(define-public crate-rlimit-0.5.0 (c (n "rlimit") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kpibg8glj8rnnd954lk2m2niy1i5z553gkdyxm9by4jxwalpm4q")))

(define-public crate-rlimit-0.5.1 (c (n "rlimit") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "034a4aazwr0xf6ksk40x6qm9ar04nyxliryiqjfx7sagzmpqwgc5")))

(define-public crate-rlimit-0.5.2 (c (n "rlimit") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05j3kqz4awl63ncw504fxsd7y8a9i715k6jf9rcmjlddyp3smrra")))

(define-public crate-rlimit-0.5.3 (c (n "rlimit") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0izqnfkc2wrbahnj2d149m650zarwfr03cyjj51z0la9gdslhwbf")))

(define-public crate-rlimit-0.5.4 (c (n "rlimit") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0807zkwsch3dxniv3w7nh3xvbxxm3b3r483wi7b4km5yxl1yvac1")))

(define-public crate-rlimit-0.6.0 (c (n "rlimit") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dgxssi98sq1n4fm7w09kq6m2nv1l01i760gqiz81282j5vy9cp6") (y #t)))

(define-public crate-rlimit-0.6.1 (c (n "rlimit") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qh687bm6xa2vv9ha2gjcw3bwqmmas2vmay6b90l9fcih4fl2zgf") (y #t)))

(define-public crate-rlimit-0.6.2 (c (n "rlimit") (v "0.6.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09hpr6dfrx7xzlps7g25akqp2p32190vhcj3ymid6vrpaiaz42yc")))

(define-public crate-rlimit-0.7.0 (c (n "rlimit") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02s8a8zas1vlmrf951fl5g3kiphx4fz5fh8ljgkg3ba7msjh6xrl")))

(define-public crate-rlimit-0.8.0 (c (n "rlimit") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06rm11nzb7iqzz99k1dj61adbgjsb7pqylidzmm090vrgqj3s6c2") (y #t)))

(define-public crate-rlimit-0.8.1 (c (n "rlimit") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bgqchjb7qi6k82bg9fdcc9kikaa5pxx1s3d183a9410czwwdg0g") (y #t)))

(define-public crate-rlimit-0.8.2 (c (n "rlimit") (v "0.8.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xdcvrr8hr2d9cv7gl9h490vgc2ryb915vf775vma4aln7kbc6vq")))

(define-public crate-rlimit-0.8.3 (c (n "rlimit") (v "0.8.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18vsz3fdj4s8yjp96wwq7wvrlc3vzzsqki8mfpha9m5zr0g8l9zp")))

(define-public crate-rlimit-0.9.0 (c (n "rlimit") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 2)))) (h "09996hcr9hjn95w8hz8nmdfdkncy1ar4w21crxgypgz2wph7fp7a") (r "1.59.0")))

(define-public crate-rlimit-0.9.1 (c (n "rlimit") (v "0.9.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 2)))) (h "13mb3ligflqb4h7m76pkyc8z5pswpc38fcl6qm1lvp2jls3rv8pq") (r "1.59.0")))

(define-public crate-rlimit-0.10.0 (c (n "rlimit") (v "0.10.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 2)))) (h "16xnqyl72p4hzalgja9v6jr7jd26lfrkda4g9z931xhfpkh8nnwv") (r "1.60.0")))

(define-public crate-rlimit-0.10.1 (c (n "rlimit") (v "0.10.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 2)))) (h "1n6346gms599n33wgav7jr46pzj0fyh7il0ys08nvwd0607zfq1m") (r "1.60.0")))

