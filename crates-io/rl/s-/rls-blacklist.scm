(define-module (crates-io rl s- rls-blacklist) #:use-module (crates-io))

(define-public crate-rls-blacklist-0.1.0 (c (n "rls-blacklist") (v "0.1.0") (h "0gcpcqw2fndm8fi1rz021l3pjs7kxyqvszr7rzxqifah9277pysn")))

(define-public crate-rls-blacklist-0.1.1 (c (n "rls-blacklist") (v "0.1.1") (h "0ppyrfrpz00k823ljkqn2pba5dw08xn51l2d81yykcjh2yffwv0m")))

(define-public crate-rls-blacklist-0.1.2 (c (n "rls-blacklist") (v "0.1.2") (h "1lppa0538svndfckqpqd4w9fr9irh1xh9zjv6mdy1dyc8ljwrag4")))

(define-public crate-rls-blacklist-0.1.3 (c (n "rls-blacklist") (v "0.1.3") (h "04g4043x6ww6pp649845p6ds6mzzw6ab31zz2x38q4ryq3d1zkmq")))

