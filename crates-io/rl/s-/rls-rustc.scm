(define-module (crates-io rl s- rls-rustc) #:use-module (crates-io))

(define-public crate-rls-rustc-0.1.0 (c (n "rls-rustc") (v "0.1.0") (h "0dym56x3a0wvdsa46rgwv2ngbifaz5siiw7ggi161m27sp4mg9sz")))

(define-public crate-rls-rustc-0.1.1 (c (n "rls-rustc") (v "0.1.1") (h "1llrfa004qr8c018wvdjn6vh9pic4qhbj6xzka96j5dzx59aj7mj")))

(define-public crate-rls-rustc-0.2.0 (c (n "rls-rustc") (v "0.2.0") (h "1kbas803vqixjfz5aqdpxs8yy9699l8gskhjdwsq0ls73m2vvm8a")))

(define-public crate-rls-rustc-0.2.1 (c (n "rls-rustc") (v "0.2.1") (h "1wh18x4ki4jsb5qhp49dqh64fbj7ia7h0f3pwjiksccyw7fvkkw5")))

(define-public crate-rls-rustc-0.2.2 (c (n "rls-rustc") (v "0.2.2") (h "1sj4vgzmiz98dwhardx97i0wfn4m98yh6bmhrdr0ahjp4ywncpw8")))

(define-public crate-rls-rustc-0.3.0 (c (n "rls-rustc") (v "0.3.0") (h "0kazh0aalrdxn5p00yv27r6nnawhr03ga2k00hy6ds5v5cmk9mcf")))

(define-public crate-rls-rustc-0.4.0 (c (n "rls-rustc") (v "0.4.0") (h "16dyn1kb7n4k82hcv9f6sbj256rgaz5y2zxiljm7p272g88hk31c")))

(define-public crate-rls-rustc-0.5.0 (c (n "rls-rustc") (v "0.5.0") (h "1l5yqyvh3q01jgh8mfhhm0n1jr9n3rq2k130aflyyyj2j1rvm79g")))

