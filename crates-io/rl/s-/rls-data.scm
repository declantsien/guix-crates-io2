(define-module (crates-io rl s- rls-data) #:use-module (crates-io))

(define-public crate-rls-data-0.1.0 (c (n "rls-data") (v "0.1.0") (d (list (d (n "rls-span") (r "^0.1") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0kasfy3cblkzsyp2d0gs0in6f0z71ldi7bxrxmw7pzc907qgy7dg")))

(define-public crate-rls-data-0.2.0 (c (n "rls-data") (v "0.2.0") (d (list (d (n "rls-span") (r "^0.1") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0iji66mr0lxzil9cwmyn4a6d61c7xds4qrv7fxl8rj3wdasfhx5m")))

(define-public crate-rls-data-0.3.0 (c (n "rls-data") (v "0.3.0") (d (list (d (n "rls-span") (r "^0.2") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1559lr0hrxm83aidghggk7sm2lxmjq18ncgcp0hvi5f1dn2ihy8i")))

(define-public crate-rls-data-0.3.1 (c (n "rls-data") (v "0.3.1") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ivxybifcmw4jyhvlp5b3ch2ajl438c5n5733yqmdx2p7k77fhpw")))

(define-public crate-rls-data-0.4.0 (c (n "rls-data") (v "0.4.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "10rc8x5ia5b397i397siasgbwdc7lwf6v8c12sb34sr2lvpsg133")))

(define-public crate-rls-data-0.4.1 (c (n "rls-data") (v "0.4.1") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "07pmpm2p8ihs3cbqmq9l3nc0xzaavmwl186qpykq21nc66nqyjip")))

(define-public crate-rls-data-0.4.2 (c (n "rls-data") (v "0.4.2") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ampxwi5xlf7xqk87nd7yqwnqigsg8gjnf8g7qpi59ba9fqf82aq")))

(define-public crate-rls-data-0.5.0 (c (n "rls-data") (v "0.5.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1yvivg3qga7xcj1998mzy4m4mhnmm229jzjmxpv8g7vfdijaar4q")))

(define-public crate-rls-data-0.6.0 (c (n "rls-data") (v "0.6.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1d929nkz6h5h3vxqqn9wmvdqa4gbbmv7c93cm2n4zhv4fd3qf84y")))

(define-public crate-rls-data-0.7.0 (c (n "rls-data") (v "0.7.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ac7bd94pi3hrchsvr7ikbi8kvvq08nw61i5k0z04pn3kdksq0p5") (f (quote (("borrows"))))))

(define-public crate-rls-data-0.9.0 (c (n "rls-data") (v "0.9.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1h3hl0621mswqqnpibvpqa9i47am1c3dx6zy04kdqdv3jrzyqx7j") (f (quote (("borrows"))))))

(define-public crate-rls-data-0.10.0 (c (n "rls-data") (v "0.10.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0d5m3x4c71i1nc1zr32ay2mdlaqbqj1hzpijh16yfcwfi3qkklqi") (f (quote (("serialize-serde" "serde" "serde_derive") ("borrows"))))))

(define-public crate-rls-data-0.11.0 (c (n "rls-data") (v "0.11.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1zqm7mp1n8k95z3iir619acbym79x9qjqprrzjqc20lpls7k5ska") (f (quote (("serialize-serde" "serde" "serde_derive") ("borrows"))))))

(define-public crate-rls-data-0.12.0 (c (n "rls-data") (v "0.12.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1rhz98h2zzgkkj18z1iwd9zi06qh4r1grlm87hdf1hi3vvm7q9a8") (f (quote (("serialize-serde" "serde" "serde_derive") ("borrows"))))))

(define-public crate-rls-data-0.13.0 (c (n "rls-data") (v "0.13.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0i1b71w0c291d25whr7hrqama3y1w45djc2mg8h697xg1lxbp1gz") (f (quote (("serialize-serde" "serde" "serde_derive") ("borrows"))))))

(define-public crate-rls-data-0.14.0 (c (n "rls-data") (v "0.14.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0g7ccz157gsrhnf0x39gs0za7f9i992qsahymsj0lbd7mkzg2940") (f (quote (("serialize-serde" "serde" "serde_derive") ("borrows"))))))

(define-public crate-rls-data-0.15.0 (c (n "rls-data") (v "0.15.0") (d (list (d (n "rls-span") (r "^0.4") (f (quote ("serialize-rustc"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0435vr1mvvrjpjsd2k0lhbq2y66vnzn7x0wrhjki4mabx5i4985y") (f (quote (("serialize-serde" "serde" "serde_derive") ("borrows"))))))

(define-public crate-rls-data-0.16.0 (c (n "rls-data") (v "0.16.0") (d (list (d (n "rls-span") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "127r4ifpx4p8yl8pb08aggx8lky49bxaij44afafh2n6w5ihglix") (f (quote (("serialize-serde" "serde" "serde_derive" "rls-span/serialize-serde") ("serialize-rustc" "rustc-serialize" "rls-span/serialize-rustc") ("default" "serialize-rustc") ("borrows"))))))

(define-public crate-rls-data-0.17.0 (c (n "rls-data") (v "0.17.0") (d (list (d (n "rls-span") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0hxxnq74k7d4jg7ds12r841pqa56z01pczrjvxnqfff8kygyg1ni") (f (quote (("serialize-serde" "serde" "serde_derive" "rls-span/serialize-serde") ("serialize-rustc" "rustc-serialize" "rls-span/serialize-rustc") ("default" "serialize-rustc") ("borrows"))))))

(define-public crate-rls-data-0.18.0 (c (n "rls-data") (v "0.18.0") (d (list (d (n "rls-span") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fgsjn9fkg4zl8m3m63f40f493gk6v97z4626gnk0s7zxhwfi0ag") (f (quote (("serialize-serde" "serde" "serde_derive" "rls-span/serialize-serde") ("serialize-rustc" "rustc-serialize" "rls-span/serialize-rustc") ("default" "serialize-rustc") ("borrows"))))))

(define-public crate-rls-data-0.18.1 (c (n "rls-data") (v "0.18.1") (d (list (d (n "rls-span") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bngfbnl3f53xmwz34zg8daky6hcvjm7nskqw35i6a5mdgj9q81s") (f (quote (("serialize-serde" "serde" "serde_derive" "rls-span/serialize-serde") ("serialize-rustc" "rustc-serialize" "rls-span/serialize-rustc") ("default" "serialize-rustc") ("borrows"))))))

(define-public crate-rls-data-0.18.2 (c (n "rls-data") (v "0.18.2") (d (list (d (n "rls-span") (r "^0.4.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0p766rga39cgkrfiik8d1irn2l1vj5s27wgpzapjcbmka52vi02z") (f (quote (("serialize-serde" "serde" "serde_derive" "rls-span/serialize-serde") ("serialize-rustc" "rustc-serialize" "rls-span/serialize-rustc") ("default" "serialize-rustc") ("borrows"))))))

(define-public crate-rls-data-0.19.0 (c (n "rls-data") (v "0.19.0") (d (list (d (n "rls-span") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "14f4ymf04ndgvwmgf7w3jfqg5sayvkmmgc8b57vfanq4gsljxivn") (f (quote (("derive" "serde_derive" "rls-span/derive") ("default"))))))

(define-public crate-rls-data-0.19.1 (c (n "rls-data") (v "0.19.1") (d (list (d (n "rls-span") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "16dvvr0hwlw3xiff4dp6gbsmd2z71qpijy9pldwk4flz0gmkb0d5") (f (quote (("derive" "serde/derive" "rls-span/derive") ("default" "derive"))))))

