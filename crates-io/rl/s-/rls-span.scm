(define-module (crates-io rl s- rls-span) #:use-module (crates-io))

(define-public crate-rls-span-0.1.0 (c (n "rls-span") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1nrqph7lqgm2i6jzj4kffqr5mcdv8731hhzr9qhgp1dca2wgfml6") (f (quote (("serialize-serde" "serde" "serde_derive") ("serialize-rustc" "rustc-serialize") ("default"))))))

(define-public crate-rls-span-0.2.0 (c (n "rls-span") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0vlih3c0cy12plcqvxi1mcnq3h3s0s4pgfanm4s9sisf6hwi97by") (f (quote (("serialize-serde" "serde" "serde_derive") ("serialize-rustc" "rustc-serialize") ("default"))))))

(define-public crate-rls-span-0.3.0 (c (n "rls-span") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1mhfjkfwywkyddlvlk163pm15z7xw5i67p4ilxnlzixbkr3pnyps") (f (quote (("serialize-serde" "serde" "serde_derive") ("serialize-rustc" "rustc-serialize") ("default"))))))

(define-public crate-rls-span-0.4.0 (c (n "rls-span") (v "0.4.0") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0skshsszcnbjjv721kmr658yg0j3vd32fc7d0apg54kavi370z2x") (f (quote (("serialize-serde" "serde" "serde_derive") ("serialize-rustc" "rustc-serialize") ("default"))))))

(define-public crate-rls-span-0.4.1 (c (n "rls-span") (v "0.4.1") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01c8s3f0ki6ah5v6k30xnvmzd60n2d22a5lz0ac5rkbcdhfnzmik") (f (quote (("serialize-serde" "serde" "serde_derive") ("serialize-rustc" "rustc-serialize") ("default"))))))

(define-public crate-rls-span-0.5.0 (c (n "rls-span") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1myy9igfmfjjg8zbafr99gsjwiqaxd486gb17pppxy9vz803wsaq") (f (quote (("derive" "serde_derive") ("default"))))))

(define-public crate-rls-span-0.5.1 (c (n "rls-span") (v "0.5.1") (d (list (d (n "rustc-serialize") (r "^0.3.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1cryagvpfmk7hlhskafm32h5h8bq3hpniwyg7kj2r38d86a4djzi") (f (quote (("serialize-rustc" "rustc-serialize") ("derive" "serde_derive") ("default"))))))

(define-public crate-rls-span-0.5.2 (c (n "rls-span") (v "0.5.2") (d (list (d (n "rustc-serialize") (r "^0.3.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0d6rwya5zsyw6vmrj8d8g3fgvic0xyp1lvfhv62vswk2dzavxsgj") (f (quote (("serialize-rustc" "rustc-serialize") ("nightly") ("derive" "serde_derive") ("default"))))))

(define-public crate-rls-span-0.5.3 (c (n "rls-span") (v "0.5.3") (d (list (d (n "rustc-serialize") (r "^0.3.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "11jc7rg3fr56rzg4wcbhjwgbi8bk45hkccmhf5gy21pwg22abvph") (f (quote (("serialize-rustc" "rustc-serialize") ("nightly") ("derive" "serde/derive") ("default"))))))

(define-public crate-rls-span-0.5.4 (c (n "rls-span") (v "0.5.4") (d (list (d (n "rustc-serialize") (r "^0.3.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1lgn9szjjn6k6xpgc6lfpc92dyf636pjkc79pw87kcyl99hhzs5n") (f (quote (("serialize-rustc" "rustc-serialize") ("nightly") ("derive" "serde/derive") ("default"))))))

