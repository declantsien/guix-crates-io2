(define-module (crates-io rl nk rlnk) #:use-module (crates-io))

(define-public crate-rlnk-0.1.0 (c (n "rlnk") (v "0.1.0") (h "1j6raha2k1b6wa8ddn68lyf0b9hg5jj7f8v6k6rza82b30bhmaan") (y #t)))

(define-public crate-rlnk-0.1.1 (c (n "rlnk") (v "0.1.1") (h "01x1hzkwivjq8jidxn9f4chsyab99fa9100isyz1y4bpl3garz13") (y #t)))

(define-public crate-rlnk-0.1.2 (c (n "rlnk") (v "0.1.2") (h "1lcdrbk887y4pxkpywxqsliyn83yfkb90g5rcj7zyscnm8lbbidp") (y #t)))

(define-public crate-rlnk-0.1.3 (c (n "rlnk") (v "0.1.3") (h "199kqcah9yx0wj9nhgvkw63waad6knafwpnhky0kf69hm6f491kv") (y #t)))

(define-public crate-rlnk-0.1.4 (c (n "rlnk") (v "0.1.4") (h "037idhl73arh0bpsw8a8vfc9q9wwpia8fm9z0vq96207npyl6grj") (y #t)))

(define-public crate-rlnk-0.1.5 (c (n "rlnk") (v "0.1.5") (h "0dqm738dn0hq7b63y4i0x5j1llinzb8jbpp00a4n0393aggqcn3i") (y #t)))

(define-public crate-rlnk-0.1.6 (c (n "rlnk") (v "0.1.6") (h "14zrb0nd65rw80g0mzx7n4313sbmi48ppw5zwmsk7l91pgm0iqzk") (y #t)))

(define-public crate-rlnk-0.1.7 (c (n "rlnk") (v "0.1.7") (h "01371r48kqw2pvgkn6mladf315b4gwc77h7rzkckxh18ypgjld8w") (y #t)))

