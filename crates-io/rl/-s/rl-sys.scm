(define-module (crates-io rl -s rl-sys) #:use-module (crates-io))

(define-public crate-rl-sys-0.0.1 (c (n "rl-sys") (v "0.0.1") (h "0v7a0scwxbixchwm6w1cvihjd612nbdncdxp5j00ckdj53d9b9pm")))

(define-public crate-rl-sys-0.0.2 (c (n "rl-sys") (v "0.0.2") (h "00fr46ccq9fy47z54nk55ckw3y8x07v3868bhvwj1cg1il8dbr6d")))

(define-public crate-rl-sys-0.0.3 (c (n "rl-sys") (v "0.0.3") (h "16i16n6y105fcnqc364zjrw3zrbzgwn8rjn0h0pf3wippvn973pa")))

(define-public crate-rl-sys-0.0.4 (c (n "rl-sys") (v "0.0.4") (h "14y9v7zy3msz47awbza1l74ndmd27fs523svh6cnkpv7scd5wh0w")))

(define-public crate-rl-sys-0.0.5 (c (n "rl-sys") (v "0.0.5") (h "00cppf84im5a1inb0lr02dzg7mdbnh104xnsdcanzkaavak74n34")))

(define-public crate-rl-sys-0.1.0 (c (n "rl-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0x0f7ryih6631qyd417clzpdzwnq3nqdqhsx9nzzl52rs01krn51")))

(define-public crate-rl-sys-0.1.1 (c (n "rl-sys") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1mjzbi8wzpfn9mjddndpr8kxrzmyd394iba0v9szlaa2xgb2spcp")))

(define-public crate-rl-sys-0.1.2 (c (n "rl-sys") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0zxg6n2lgqvw6fxkgjbmb8qc88gxhz1nppm3h5ih9kg0hmcr0kn5")))

(define-public crate-rl-sys-0.2.0 (c (n "rl-sys") (v "0.2.0") (d (list (d (n "libc") (r "~0.2.2") (d #t) (k 0)))) (h "18xx694zcj8i1pvx2jcrg57kvyg83nkfqyw4qw8d57fjvs3p0ryk")))

(define-public crate-rl-sys-0.2.1 (c (n "rl-sys") (v "0.2.1") (d (list (d (n "libc") (r "~0.2.2") (d #t) (k 0)))) (h "0hy6lyk8qqmzq2dcpydyf8069npg96kzr4v8jjfr9lk9b0y7ascx")))

(define-public crate-rl-sys-0.2.2 (c (n "rl-sys") (v "0.2.2") (d (list (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)))) (h "14lmn10ljzamzkvjj9cj8v1zgif8jhf1ldqpskf30rpqdi8wdjn7")))

(define-public crate-rl-sys-0.3.0 (c (n "rl-sys") (v "0.3.0") (d (list (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "sodium-sys") (r "~0.0.4") (d #t) (k 2)) (d (n "vergen") (r "~0.0.16") (d #t) (k 1)))) (h "1xxl5whjcj9k2vbqhmibbdnkwjsazxxwp65f5ag25r4a0lvh9s84")))

(define-public crate-rl-sys-0.4.0 (c (n "rl-sys") (v "0.4.0") (d (list (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "sodium-sys") (r "~0.0.4") (d #t) (k 2)) (d (n "vergen") (r "~0.0.16") (d #t) (k 1)))) (h "1wwqi7p11zb25k16sgzwcn8qj4jdq0jqp9krhmq98w5c5q3hb82f")))

(define-public crate-rl-sys-0.4.1 (c (n "rl-sys") (v "0.4.1") (d (list (d (n "blastfig") (r "~0.3.3") (d #t) (k 0)) (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "errno") (r "~0.1.5") (d #t) (k 0)) (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)) (d (n "sodium-sys") (r "~0.0.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "0h50n672936v5scfybknzjmrlviwzbm6zlhpafh1kf6j4s7n69l9") (f (quote (("latest") ("default"))))))

(define-public crate-rl-sys-0.5.0 (c (n "rl-sys") (v "0.5.0") (d (list (d (n "bitflags") (r "~0.7.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "errno") (r "~0.1.5") (d #t) (k 0)) (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)) (d (n "sodium-sys") (r "~0.0.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "13a5pqc5lgzaq29j6nyh9r50xb56a93a5zwsvcaifpqrmwpc905j") (f (quote (("lint" "clippy") ("default"))))))

(define-public crate-rl-sys-0.5.1 (c (n "rl-sys") (v "0.5.1") (d (list (d (n "bitflags") (r "~0.7.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.76") (o #t) (d #t) (k 0)) (d (n "errno") (r "~0.1.5") (d #t) (k 0)) (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)) (d (n "sodium-sys") (r "~0.0.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "17ayfznv7j0pk76xiacwdkiknsj7wmsfaaccj2vmy1x8296znphw") (f (quote (("lint" "clippy") ("default"))))))

(define-public crate-rl-sys-0.5.2 (c (n "rl-sys") (v "0.5.2") (d (list (d (n "bitflags") (r "~0.7.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.76") (o #t) (d #t) (k 0)) (d (n "errno") (r "~0.1.5") (d #t) (k 0)) (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)) (d (n "sodium-sys") (r "~0.0.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "13ipd15gj2x00k29zzs2nq1bmrjxz50v0c5jvbnrd0gwxskcmara") (f (quote (("lint" "clippy") ("default"))))))

