(define-module (crates-io rl fs rlfsc) #:use-module (crates-io))

(define-public crate-rlfsc-0.1.0 (c (n "rlfsc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "016whw04lfymzwv7vzckycifhad6kb8m09cab6rlsz2mkxbdww6r")))

(define-public crate-rlfsc-0.1.1 (c (n "rlfsc") (v "0.1.1") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "06zrrhldlp5h1rzn3j67xz3zg6r6hd6da5472d2aj903cwx3l5g7")))

(define-public crate-rlfsc-0.1.2 (c (n "rlfsc") (v "0.1.2") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0sx4dfrb4jr3vcz53nlbmx8zn5jqms4sl7z715bps21mhac4z0a1")))

(define-public crate-rlfsc-0.1.3 (c (n "rlfsc") (v "0.1.3") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "13i3qdif8q1pjifc6fbq0ni2n7cm0ib7mmnn3p8rxsvgbf7dwcbz")))

(define-public crate-rlfsc-0.1.4 (c (n "rlfsc") (v "0.1.4") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "14ydjbkvd36w30r0imv8yzvxyp4xbqgbi2dz2yglcq3gl57dyi15")))

(define-public crate-rlfsc-0.1.5 (c (n "rlfsc") (v "0.1.5") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0v46g7zh2pa6sqw4k74iqjix6b23qjqzhjczxsmwhdikkdb3mqhw")))

(define-public crate-rlfsc-0.1.6 (c (n "rlfsc") (v "0.1.6") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "057ndvkniqbp8m1yq5bga78c21l2v1py21ipyhi46yxxkq4ij02i")))

(define-public crate-rlfsc-0.2.0 (c (n "rlfsc") (v "0.2.0") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "12ln8325ds4z0zxahazcnil3kxyvj66l4042aiyz0sjyasr4r81z")))

(define-public crate-rlfsc-0.2.1 (c (n "rlfsc") (v "0.2.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0g1fn4rsgnd8qggbqcfa6qlq0fx1p5wz0rwykgaw1ifjlwd6b77z")))

(define-public crate-rlfsc-0.2.2 (c (n "rlfsc") (v "0.2.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rlimit") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1iz16rrmk8zjyk8kkpm8f9rg8lx7nc4wp4ga8jcqhx8539pl629m")))

