(define-module (crates-io rl m2 rlm2c) #:use-module (crates-io))

(define-public crate-rlm2c-0.1.0 (c (n "rlm2c") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cpal") (r "^0.12.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "interception") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vigem") (r "^0.9.1") (d #t) (k 0)))) (h "0r5yw6j3zbygrpxbl7c7dr09a5ck1c6n8mva8xk1qny1hbah1l54")))

