(define-module (crates-io rl ay rlay-backend) #:use-module (crates-io))

(define-public crate-rlay-backend-0.3.0 (c (n "rlay-backend") (v "0.3.0") (d (list (d (n "ambassador") (r "^0.2.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "cid") (r "^0.3.1") (o #t) (d #t) (k 0) (p "cid_fork_rlay")) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "rlay_ontology") (r "^0.2.6") (f (quote ("web3_compat"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1yb31nk1fqby9fngvlq4mr60lx4xb3g82c1dw4xn6m6wiv4c0srj") (f (quote (("rpc" "cid"))))))

