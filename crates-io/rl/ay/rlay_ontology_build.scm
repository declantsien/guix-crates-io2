(define-module (crates-io rl ay rlay_ontology_build) #:use-module (crates-io))

(define-public crate-rlay_ontology_build-0.2.3 (c (n "rlay_ontology_build") (v "0.2.3") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01v2ww3dfa6xwyyzzb2b18lls7jl4b2igz21b57xhbgapxz49fdm") (f (quote (("std" "serde_json") ("default" "std"))))))

(define-public crate-rlay_ontology_build-0.2.5 (c (n "rlay_ontology_build") (v "0.2.5") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x3qkw6v0p07yd132pswsfzgl4q178ln3i8g8s4b9s2yr43g29yq") (f (quote (("std" "serde_json") ("default" "std"))))))

