(define-module (crates-io rl ay rlay-plugin-interface) #:use-module (crates-io))

(define-public crate-rlay-plugin-interface-0.3.0 (c (n "rlay-plugin-interface") (v "0.3.0") (d (list (d (n "ambassador") (r "^0.2.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "rlay-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "rlay_ontology") (r "^0.2.6") (f (quote ("web3_compat"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13p123hwdg9c98kbp85z2cjqckjxp5rsxr1bk6rmg4vs4g07bkll")))

