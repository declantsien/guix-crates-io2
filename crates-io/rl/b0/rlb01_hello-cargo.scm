(define-module (crates-io rl b0 rlb01_hello-cargo) #:use-module (crates-io))

(define-public crate-rlb01_hello-cargo-0.1.0 (c (n "rlb01_hello-cargo") (v "0.1.0") (h "1lwrxbwh4fc8sshgzqkm0vf1kyzc7dw6hc19rjcn4mkq7qdgvxgn")))

(define-public crate-rlb01_hello-cargo-0.1.1 (c (n "rlb01_hello-cargo") (v "0.1.1") (h "0cw6c9c8bvawich0df808pdrjvl9kyvczzcwc36yik2xvssp11yi")))

(define-public crate-rlb01_hello-cargo-0.2.0 (c (n "rlb01_hello-cargo") (v "0.2.0") (h "01jmhd4bciq2a72h9xswm5wdmpnbhhyvbg8r7zr0kggbjqqxdy2a")))

