(define-module (crates-io rl oc rlocc) #:use-module (crates-io))

(define-public crate-rlocc-0.2.1 (c (n "rlocc") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)))) (h "1s1iqghrb9mfja72n1ida6hmsk9afp21yl376digjvcvdwsrndja")))

