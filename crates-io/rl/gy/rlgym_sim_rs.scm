(define-module (crates-io rl gy rlgym_sim_rs) #:use-module (crates-io))

(define-public crate-rlgym_sim_rs-0.3.0-alpha (c (n "rlgym_sim_rs") (v "0.3.0-alpha") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocketsim_rs") (r "^0.22.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "15lafjdm66gviblkzhj064q689x0y3hvvyg5j66mfra8cm8v5s5j")))

(define-public crate-rlgym_sim_rs-0.4.0-alpha (c (n "rlgym_sim_rs") (v "0.4.0-alpha") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rocketsim_rs") (r "^0.22.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1n11vq02bjayjmbdardmj09shc869sznx5ys1fz7qj97fvvj7hv9")))

(define-public crate-rlgym_sim_rs-0.5.0-alpha (c (n "rlgym_sim_rs") (v "0.5.0-alpha") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rocketsim_rs") (r "^0.22.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "0swdjx7wi91dsim5v7gv2pw6azm9gnf8r0d4d7rxz9x7cv0kgnkp")))

(define-public crate-rlgym_sim_rs-0.6.0-alpha (c (n "rlgym_sim_rs") (v "0.6.0-alpha") (d (list (d (n "memmap2") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rocketsim_rs") (r "^0.22.3") (f (quote ("bin"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1m9g51lcl53cgnnzh93n1h86aa610prb4d55kybpy7pic7ygvv34")))

(define-public crate-rlgym_sim_rs-0.7.0-alpha (c (n "rlgym_sim_rs") (v "0.7.0-alpha") (d (list (d (n "memmap2") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rocketsim_rs") (r "^0.23.0") (f (quote ("bin"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "0gmqm82a9hxkb2qwcxr9gfd5683k359ah02i9ihimsd7kfbqr3zl")))

