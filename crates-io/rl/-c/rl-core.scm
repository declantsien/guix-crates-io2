(define-module (crates-io rl -c rl-core) #:use-module (crates-io))

(define-public crate-rl-core-0.0.0 (c (n "rl-core") (v "0.0.0") (d (list (d (n "serde_derive") (r "^1.0.132") (o #t) (d #t) (k 0)))) (h "15ir2065z1l2rariwmjfgshf218wsz7hw29f71hkhb1b4kms498n") (f (quote (("serde" "serde_derive"))))))

(define-public crate-rl-core-0.1.0 (c (n "rl-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (o #t) (d #t) (k 0)))) (h "070yid5b2xdvsx8fs314c4d8295ic3hrscc8qnf340al9f9k25cs") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.2.0 (c (n "rl-core") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (o #t) (d #t) (k 0)))) (h "0zz9kzk7273qcbjkrljknqmi6pawx4l8f4nabyqc4bzw05ja6zak") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.3.0 (c (n "rl-core") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (o #t) (d #t) (k 0)))) (h "0a2ngl4nc04mjsngs9dmbv1p85dcy3094hbjbkzx2k7s4p5wnwf0") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.4.0 (c (n "rl-core") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0s3l2a859xnh56jb69206yszcm2r7ia7g9w1i0q3gl22a09ywzv1") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.7.0 (c (n "rl-core") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "14lv3vdwslyjfwdk9kfsyv9p4l0hiklhbcw0irpxmhnp2xziw03h") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.9.0 (c (n "rl-core") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "154p0h8xs83y6fblrsk32lkgifvq67k68lschzd027j45y6dqmbz") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.10.0 (c (n "rl-core") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "03w5bq2b8dasmx60ygj4398d34c4snmk7kvigscywlvs5sc65gdy") (f (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.11.0 (c (n "rl-core") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0srfb6k0v9zlaxnhf9asxyqh4z6kbz352zqdk5ixydg449c21hhn")))

(define-public crate-rl-core-0.13.0 (c (n "rl-core") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0wzv6590l4dc6l2kzv9idp8nhbpxk4hrnfnqdj6cbzhs6fsgmw11")))

(define-public crate-rl-core-0.14.0 (c (n "rl-core") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "069049vc10ds9qs75xam4k1j07r7js0pzbi1sx0rdg441z5yavwh")))

