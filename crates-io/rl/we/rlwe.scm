(define-module (crates-io rl we rlwe) #:use-module (crates-io))

(define-public crate-rlwe-0.1.0 (c (n "rlwe") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typenum") (r "^1.15.0") (d #t) (k 0)))) (h "0ab6a2amb4mc1q8b24sfvclhl8zq14dn1kb07w0wxqxlns77l6c2")))

