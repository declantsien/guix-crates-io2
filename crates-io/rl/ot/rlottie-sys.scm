(define-module (crates-io rl ot rlottie-sys) #:use-module (crates-io))

(define-public crate-rlottie-sys-0.1.0 (c (n "rlottie-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1qwp4f7kpfyrvxd9paf83awg6c30b6wvgmlrypxd6yxvj5b4wngk") (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.0 (c (n "rlottie-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1y3w61qzjv9gxszzhvnjm2vb88p8mwyb25pwppccwpza5bp3jif4") (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.1 (c (n "rlottie-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1024xxcqmv29sa2kyzl5dyd4xmi8brxmrj1q1x1kaj55jm38p5qh") (y #t) (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.2 (c (n "rlottie-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.60.1") (f (quote ("runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1m180wbrb21ybfgcr9mdqpq5pwbh89snxvz7d4yd87k25i8iq94y") (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.3 (c (n "rlottie-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.60.1") (f (quote ("runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "18bx80wgmh4163qain88l7idrz69aaamqkar0z7mgzwbrix294js") (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.4 (c (n "rlottie-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.61.0") (f (quote ("runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "0whqdqbfin8j1fqr33gps9iyv99jjcsbpn8838irdpamvxgqjjzg") (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.5 (c (n "rlottie-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "10ivjha1smz14kfkqmj7ycxhrdr4y35gahi911x02a5ihcfk7gnb") (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.6 (c (n "rlottie-sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.64.0") (f (quote ("runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1qcw803dcf7jm7d850yf260lpmn6gh0853cvfjicspqipw2iaxq0") (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.7 (c (n "rlottie-sys") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.65.1") (f (quote ("runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "0zl80cyni97f41j4w49icxf21vl0rakbc50jv6qngw14x9s24sln") (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.8 (c (n "rlottie-sys") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.66.1") (f (quote ("prettyplease" "runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "00zyk6rp4ma6hckdwbz2mq4mvr4844pi7lk2ys0b8bhlq6dmhsjr") (l "rlottie") (r "1.56")))

(define-public crate-rlottie-sys-0.2.9 (c (n "rlottie-sys") (v "0.2.9") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("prettyplease" "runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1jrxmjnjs5bcvgd4zj2zq6kjr6mqq32j9y6j7y0j83bzlxz3bvr4") (l "rlottie") (r "1.56")))

