(define-module (crates-io rl ot rlottie) #:use-module (crates-io))

(define-public crate-rlottie-0.1.0 (c (n "rlottie") (v "0.1.0") (d (list (d (n "rlottie-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1z9prh6f5hrpd1ff8p0f4m4cs185cxflyaxhimic9fwblhwrk9hx") (r "1.56")))

(define-public crate-rlottie-0.2.0 (c (n "rlottie") (v "0.2.0") (d (list (d (n "rlottie-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1v79c5yiz6dj5a8l46bf3ngcbp4iqnxv3v7pxbm555xkd4zms15w") (r "1.56")))

(define-public crate-rlottie-0.2.1 (c (n "rlottie") (v "0.2.1") (d (list (d (n "rlottie-sys") (r "^0.1.0") (d #t) (k 0)))) (h "19wkp7bybkmcv9znrl2cr7qs0mhr6dm34g625c39pamqmcs2s1sn") (y #t) (r "1.56")))

(define-public crate-rlottie-0.2.2 (c (n "rlottie") (v "0.2.2") (d (list (d (n "rlottie-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0331xs7q6s2d4plpax7rhxbn7l96m8dmrp2bzxv5wi7xjnd83n1h") (r "1.56")))

(define-public crate-rlottie-0.3.0 (c (n "rlottie") (v "0.3.0") (d (list (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1g1x4naavjia4lbnimc7xj5cqf84nkfd06hl8mx4cn2d12l2iba0") (r "1.56")))

(define-public crate-rlottie-0.3.1 (c (n "rlottie") (v "0.3.1") (d (list (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0g5yj5n14z1bbb8889rvpzcqmc8m6i33g5l2301fsh1symbdxwy9") (r "1.56")))

(define-public crate-rlottie-0.4.0 (c (n "rlottie") (v "0.4.0") (d (list (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0sfyby3566795crfhhs149v9sl3i45m9c7lg36j10m06fncs5vw9") (r "1.56")))

(define-public crate-rlottie-0.4.1 (c (n "rlottie") (v "0.4.1") (d (list (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1pkb2440ayga70vgkkms3akxb7rhnvfwvg8hs97h9yz2y783dza1") (r "1.56")))

(define-public crate-rlottie-0.5.0 (c (n "rlottie") (v "0.5.0") (d (list (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie-sys") (r "^0.2") (d #t) (k 0)))) (h "147p8h7ijnaxyr62a8n2j3dpc9hd3m0xzibifkwxlhgnfc17bn4b") (r "1.56")))

(define-public crate-rlottie-0.5.1 (c (n "rlottie") (v "0.5.1") (d (list (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie-sys") (r "^0.2") (d #t) (k 0)))) (h "0wl6axzbk6ky8bg4bb2gf14flfjxc3qnb26pybx22p079ddzy3qb") (r "1.56")))

(define-public crate-rlottie-0.5.2 (c (n "rlottie") (v "0.5.2") (d (list (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie-sys") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "093ls2snr1ydks5s0mfvb6a9ca43d8mapc7p3wa05g2k5da3vcdp") (s 2) (e (quote (("serde" "dep:serde" "rgb/serde")))) (r "1.60")))

