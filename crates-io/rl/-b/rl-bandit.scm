(define-module (crates-io rl -b rl-bandit) #:use-module (crates-io))

(define-public crate-rl-bandit-1.0.0 (c (n "rl-bandit") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "14mr4a15ci9bvn7cqa6xri3ypmg5w334hc179d0p0pm39il2ppnr")))

(define-public crate-rl-bandit-1.0.1 (c (n "rl-bandit") (v "1.0.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0vc9vxvqayfxvhba254yka236i0lxqkkv9win5i06yngc787839b")))

(define-public crate-rl-bandit-1.0.2 (c (n "rl-bandit") (v "1.0.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0dkpk86a0dsdyxn6wg3p1vk49q2jb8fvwp1cd1rkzsrns9wc82mb")))

