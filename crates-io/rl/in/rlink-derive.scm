(define-module (crates-io rl in rlink-derive) #:use-module (crates-io))

(define-public crate-rlink-derive-0.1.0 (c (n "rlink-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dxc2zpxv337alcd53wsxq9sc3x2gq7ds4rg5k48h5k39z7223pd")))

(define-public crate-rlink-derive-0.1.1 (c (n "rlink-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07jabipc30bsxc7qh2n2j52p1m3h8dqjnm6qzrqn75pn3c4rhr71")))

(define-public crate-rlink-derive-0.2.0-alpha.1 (c (n "rlink-derive") (v "0.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kvqacg2lhbpv8ybzjl37rb6fnfkq3zp00fqdc19vj9fgids12ij")))

(define-public crate-rlink-derive-0.2.0-alpha.2 (c (n "rlink-derive") (v "0.2.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v6d5f971f94x899pa7kpyw8zj3pgb3i9bcydb4zpn883v2mvh6a")))

(define-public crate-rlink-derive-0.2.0-alpha.3 (c (n "rlink-derive") (v "0.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mlxk9m3qsajgqgsx1fmx6fs0d0my184bzk563qmzgyjp43qlqxp")))

(define-public crate-rlink-derive-0.2.0-alpha.4 (c (n "rlink-derive") (v "0.2.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qh8bams77dlbwy1vhk6zgywqplddh5qbg24b7xfgz3b7fkjpjdk")))

(define-public crate-rlink-derive-0.2.0-alpha.5 (c (n "rlink-derive") (v "0.2.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13q1wknj7vq8wf9c7s0q4chh675a0jd2fsbplgmz2xajy3xymwlw")))

(define-public crate-rlink-derive-0.2.0 (c (n "rlink-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jnl84rdnpwsfn7w8bikb8q4sh5k0vdj1fw0nlyrmhfk1hkpbqnr")))

(define-public crate-rlink-derive-0.2.1-alpha.0 (c (n "rlink-derive") (v "0.2.1-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lq8c85fgqh8jgv5svqr9fad0vscx2ll59qd4xj99qs5p9ybl8vv")))

(define-public crate-rlink-derive-0.2.1-alpha.1 (c (n "rlink-derive") (v "0.2.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mmmw1g08kb3a7xac150ph1vmv9ggjf741wpqki78p18x8lgswgv")))

(define-public crate-rlink-derive-0.2.1-alpha.2 (c (n "rlink-derive") (v "0.2.1-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xqw3dhpnsf4llg147fnd46hqp82659ikqvwx9yd7aavq1pj4i06")))

(define-public crate-rlink-derive-0.2.1-alpha.3 (c (n "rlink-derive") (v "0.2.1-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jgbamhr9pf26f9vngrxlwlp0lh7kglxp34b31ahi9hsp8d7dpkv")))

(define-public crate-rlink-derive-0.2.1-alpha.4 (c (n "rlink-derive") (v "0.2.1-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05m7bc33lplm6ca175pldrhwq4838hfpqgqpgkc5r6qd43hl28js")))

(define-public crate-rlink-derive-0.2.1 (c (n "rlink-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05yxfwdy0h98nrnw4p6slzql9h8ajaknm5w8xdzkxgl23dn5s3xa")))

(define-public crate-rlink-derive-0.2.2-alpha.0 (c (n "rlink-derive") (v "0.2.2-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09r2pvxzi7sdxpgd14ssn1sm5cskb0cymkx04aaij78j110ak2g1")))

(define-public crate-rlink-derive-0.2.2-alpha.1 (c (n "rlink-derive") (v "0.2.2-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p8lpgbd6ll0vw7c4cmcax8844y5kl9lx5avd0d9h6s8yy39rndf")))

(define-public crate-rlink-derive-0.2.2-alpha.2 (c (n "rlink-derive") (v "0.2.2-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07ybpxwvmmqnj1n6fy4p52gpgb26rmsvwpaldpvfd724v4dc2g0k")))

(define-public crate-rlink-derive-0.2.2-alpha.3 (c (n "rlink-derive") (v "0.2.2-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p7i988x000ayhdhqrs2xzpq42915sxv8z1mr7vpqr0z059wdynm")))

(define-public crate-rlink-derive-0.2.2-alpha.4 (c (n "rlink-derive") (v "0.2.2-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ry59jisyc31bgap4s8nnff6yczb8938lg7fg3ir3y0l7qfgjnpq")))

(define-public crate-rlink-derive-0.2.2-alpha.5 (c (n "rlink-derive") (v "0.2.2-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x2ypldk6hdmp2hgbgvv51ns9klg5icr0vlf4a1ngcmhh5xbzwkx")))

(define-public crate-rlink-derive-0.2.2-alpha.6 (c (n "rlink-derive") (v "0.2.2-alpha.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0llx67zjyy9b0p70yfl9lj0shvmavazpva8ny26pznhd486dvgx6")))

(define-public crate-rlink-derive-0.2.2-alpha.7 (c (n "rlink-derive") (v "0.2.2-alpha.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k3i37dkm6z5dvchihivnl82q9j7ca38hdj2n99gfpa14dlz19gb")))

(define-public crate-rlink-derive-0.2.2 (c (n "rlink-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lj324d8hj2jjlgqvqr0mm39ymg9gj1cgxvqvz7838my27bbk7mk")))

(define-public crate-rlink-derive-0.2.3-alpha.1 (c (n "rlink-derive") (v "0.2.3-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gny4ivw433g0xk1g5zswhzrw7792f8v6fqxaa6cmwzl34mjnpgj")))

(define-public crate-rlink-derive-0.2.3-alpha.2 (c (n "rlink-derive") (v "0.2.3-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mya01gbbhn5h1asdp033zbzhw8s0i1hl154shkhrzzr3yzkz63j")))

(define-public crate-rlink-derive-0.2.3-alpha.3 (c (n "rlink-derive") (v "0.2.3-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d56rmccydhkh5md4k257q9zvwhqhyy8hbs7rfniz9wb7618gd09")))

(define-public crate-rlink-derive-0.2.3-alpha.4 (c (n "rlink-derive") (v "0.2.3-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gs88pq3ycjja6jnqlvqj1mry211v1rpwk62nzk0yd9k265sac88")))

(define-public crate-rlink-derive-0.2.3-alpha.5 (c (n "rlink-derive") (v "0.2.3-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y27shcir0kkjfi146sndsvnhkrmbpszkf3309wj99czji7ir748")))

(define-public crate-rlink-derive-0.2.3-alpha.6 (c (n "rlink-derive") (v "0.2.3-alpha.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01cm1a2jhj3ad9f3jgbl20hlhjmcnxcvj33xw3y49bwdqhhddpnl")))

(define-public crate-rlink-derive-0.2.3 (c (n "rlink-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "190zw8avfax7mcgiqa8yky2p4z4kklvjkmg6pia4jz2l0gpq6iqf")))

(define-public crate-rlink-derive-0.3.0 (c (n "rlink-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y9dfh38k55sbb7iq8y5b93k3sbnb54g1n65agrv2az0fppgszk0")))

