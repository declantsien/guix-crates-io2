(define-module (crates-io rl in rlink-buffer-gen) #:use-module (crates-io))

(define-public crate-rlink-buffer-gen-0.6.2 (c (n "rlink-buffer-gen") (v "0.6.2") (h "0s52fmiamqh2jnca5pf9zf6nq0vhis05k7rwb5lyl51vcb1xmphq") (y #t)))

(define-public crate-rlink-buffer-gen-0.1.0 (c (n "rlink-buffer-gen") (v "0.1.0") (h "18ibbnwn3n8bsrf0c4s1wcm4mxldbc0lhzcch8pk45c18hjcx1nx") (y #t)))

