(define-module (crates-io rl in rline) #:use-module (crates-io))

(define-public crate-rline-0.0.0 (c (n "rline") (v "0.0.0") (h "1vz40l71w5r6d1z98d2r4szdfp8sz1c3yg3i8mnfwc0kp3l4qb7k")))

(define-public crate-rline-0.1.0 (c (n "rline") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uid") (r "^0.1") (d #t) (k 0)))) (h "1q1g6qh6szd6d5dyv0clc78147nbzxwzdaxnxb93lb0915vhj4qm")))

(define-public crate-rline-0.1.1 (c (n "rline") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uid") (r "^0.1") (d #t) (k 0)))) (h "1fqyy9m5ykilg790iqfkbiwhv1ra1pyz2ki180pm7yc9l4rvl8v7")))

(define-public crate-rline-0.1.2 (c (n "rline") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uid") (r "^0.1") (d #t) (k 0)))) (h "12gqh7gmb1wnk16028pv4sjnvmk2v0kmmim5wj4ki8wmdhqkhwrd")))

(define-public crate-rline-0.1.3 (c (n "rline") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "uid") (r "^0.1") (d #t) (k 0)))) (h "121gljp1w4npbr88p0038j6x7qgx7j7x766dn9wvzzr9mk2dirl0")))

(define-public crate-rline-0.1.4 (c (n "rline") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "uid") (r "^0.1") (d #t) (k 0)))) (h "1rjzzwgk5bw3jn3a60qxzpa4frd1v5r2g3mlpsmx8mx0kdq0j28b")))

(define-public crate-rline-0.2.0 (c (n "rline") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "uid") (r "^0.1") (d #t) (k 0)))) (h "0igpp5nbw5y91ckn4p8grvmkhl0vxbpa85zlnrsw94sfzfx77msq") (l "readline")))

(define-public crate-rline-0.2.1 (c (n "rline") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "uid") (r "^0.1") (d #t) (k 0)))) (h "16118ji667bvqckyv2xl22cypiqbhbd7szhpq8cdjf1hv4anxys1") (l "readline")))

(define-public crate-rline-0.3.0 (c (n "rline") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "uid") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 2)))) (h "1rhkjb2rhp6sgvsmvsm9rv462rbbcfns9j7diyvf4rkrmh7s7hin") (l "readline")))

(define-public crate-rline-0.3.1 (c (n "rline") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)) (d (n "uid") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 2)))) (h "1r35p2hwgfvl28n0gc4sxvf7b3jqhcjg3r0ympxi4rbyjyvi7mdm") (l "readline")))

(define-public crate-rline-0.3.2 (c (n "rline") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)) (d (n "uid") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 2)))) (h "13mq0v87nzw9iwqism1bj0ky6jl8hfa2s5y089q6h2kw5akw8xb6") (f (quote (("static")))) (l "readline")))

(define-public crate-rline-0.3.3 (c (n "rline") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 2)) (d (n "uid") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 2)))) (h "111848kz7hg6pizvk5mh9v04apmvssp4mcqdwwn0ka4iklvii9xk") (f (quote (("static")))) (l "readline")))

