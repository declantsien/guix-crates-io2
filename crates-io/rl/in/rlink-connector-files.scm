(define-module (crates-io rl in rlink-connector-files) #:use-module (crates-io))

(define-public crate-rlink-connector-files-0.3.0 (c (n "rlink-connector-files") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^3.0") (d #t) (k 0)) (d (n "rlink") (r "^0.3.0") (d #t) (k 0)) (d (n "rlink-derive") (r "^0.2.3") (d #t) (k 0)))) (h "1vh9q7gfg6grh3jmkpdxysvc9rwsvaxbxjpynlj2w0zpmxrf9x55")))

(define-public crate-rlink-connector-files-0.4.0 (c (n "rlink-connector-files") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^3.0") (d #t) (k 0)) (d (n "rlink") (r "^0.4") (d #t) (k 0)) (d (n "rlink-derive") (r "^0.2.3") (d #t) (k 0)))) (h "0ynq7z8arbgqaydchi2lb633dlvr87w5pvsgynn3s3v7rc26da6w")))

(define-public crate-rlink-connector-files-0.5.0 (c (n "rlink-connector-files") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^4.0") (d #t) (k 0)) (d (n "rlink") (r "^0.5") (d #t) (k 0)) (d (n "rlink-derive") (r "^0.3") (d #t) (k 0)))) (h "17hfzr49yi5rg281arz13ny5hvn629rqd68fr2n14aif8wa6ds7b")))

