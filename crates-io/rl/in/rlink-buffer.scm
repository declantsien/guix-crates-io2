(define-module (crates-io rl in rlink-buffer) #:use-module (crates-io))

(define-public crate-rlink-buffer-0.1.0 (c (n "rlink-buffer") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0jvy56vk7wzzzqc0gv09k64mhcdyyayjk6kkalpfn5hqka7gl5hp") (y #t)))

