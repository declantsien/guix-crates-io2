(define-module (crates-io rl in rline_macro) #:use-module (crates-io))

(define-public crate-rline_macro-1.0.0 (c (n "rline_macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rline_api") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.40") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i01zyqrh7vybglphww4af8h2fc4i9b2fw56fl3cf9z5faxpbk84")))

