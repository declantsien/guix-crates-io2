(define-module (crates-io rl in rline_api) #:use-module (crates-io))

(define-public crate-rline_api-1.0.0 (c (n "rline_api") (v "1.0.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1z16fv8fqjm5yild73ywpwwfs3spgjbr7hsyvv7pndy2jknlnhhd")))

