(define-module (crates-io rl in rlink-files-connector) #:use-module (crates-io))

(define-public crate-rlink-files-connector-0.2.2-alpha.7 (c (n "rlink-files-connector") (v "0.2.2-alpha.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^3.0") (d #t) (k 0)) (d (n "rlink") (r "^0.2.2-alpha.7") (d #t) (k 0)) (d (n "rlink-derive") (r "^0.2.2-alpha.7") (d #t) (k 0)))) (h "1jc31aqr8nsyw9rf5c9pwwhcbvvzs44g4ds43n4p1h2f0cvan91q") (y #t)))

(define-public crate-rlink-files-connector-0.2.2 (c (n "rlink-files-connector") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^3.0") (d #t) (k 0)) (d (n "rlink") (r "^0.2.2") (d #t) (k 0)) (d (n "rlink-derive") (r "^0.2.2") (d #t) (k 0)))) (h "0mc3glawzvfwwvj1xni65bz3l4af8xz4mda6v0pkvick8gg2qr2k") (y #t)))

(define-public crate-rlink-files-connector-0.2.3 (c (n "rlink-files-connector") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^3.0") (d #t) (k 0)) (d (n "rlink") (r "^0.2.3") (d #t) (k 0)) (d (n "rlink-derive") (r "^0.2.3") (d #t) (k 0)))) (h "1h7ai885g2gj54izflann37ba5s0p03bnn1z0v4fvidy28rw15kk") (y #t)))

