(define-module (crates-io rl _l rl_localtime) #:use-module (crates-io))

(define-public crate-rl_localtime-0.1.0 (c (n "rl_localtime") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)))) (h "0cngrscrgy6jxz6k0p7j6v5z1qj7jdi52ybzgpj6hcjr0qx95hd9")))

(define-public crate-rl_localtime-0.1.1 (c (n "rl_localtime") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)))) (h "0a4czv44dc19f0khc2i9nlslpk2n35igmnjhxm70p8ir9hhbzn90")))

(define-public crate-rl_localtime-0.1.2 (c (n "rl_localtime") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)))) (h "0pvp9qjrhmbg1vcnbb3hwm9gm77sgy0x6dz42116snkn8b2ghx7q")))

