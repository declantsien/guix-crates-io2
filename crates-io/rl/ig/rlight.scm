(define-module (crates-io rl ig rlight) #:use-module (crates-io))

(define-public crate-rlight-0.0.1 (c (n "rlight") (v "0.0.1") (h "0flwbq80b1cwkyx63xamgqi2j0r7k3znzcyjsbcszkkwpal3z8rz")))

(define-public crate-rlight-0.1.0 (c (n "rlight") (v "0.1.0") (d (list (d (n "at_debug") (r "^0.1.0") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron_conf"))) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)) (d (n "v4l") (r "^0.14.0") (f (quote ("libv4l"))) (k 0)))) (h "14kaq82015zzx5rs597xwcjly024cx9ihp1mz6k4z95m6pji7awy")))

(define-public crate-rlight-0.1.1 (c (n "rlight") (v "0.1.1") (d (list (d (n "at_debug") (r "^0.1.0") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron_conf"))) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)) (d (n "v4l") (r "^0.14.0") (f (quote ("libv4l"))) (k 0)))) (h "0ygn66r8zwdqmdjb1vmypm8ghq710wdgwxy6c8ka0xxy5pp9sq5y")))

(define-public crate-rlight-0.2.0 (c (n "rlight") (v "0.2.0") (d (list (d (n "brightness") (r "^0.5.0") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron_conf"))) (k 0)) (d (n "nokhwa") (r "^0.10.4") (f (quote ("input-native"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)))) (h "0rqnk41nfq7n8sra03m08bmvf2zq7cc71xdhylniyd94kbqy306b")))

