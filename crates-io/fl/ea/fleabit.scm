(define-module (crates-io fl ea fleabit) #:use-module (crates-io))

(define-public crate-fleabit-0.1.0 (c (n "fleabit") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)))) (h "0yjswvq90ml5l57j6dvk9vcaw9cf56cic65q6vxlg274lzmpskci") (y #t)))

(define-public crate-fleabit-0.1.1 (c (n "fleabit") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)))) (h "0yc6kfkxxbf6n017j2ppkcq3z9ha2f0klq2gqwaqb5brjqv211c2") (y #t)))

(define-public crate-fleabit-0.1.2 (c (n "fleabit") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)))) (h "1cd50i52qy3la07cdyib399x40cq47yyfz5nfrj4b0nz6kl6z3vm") (y #t)))

