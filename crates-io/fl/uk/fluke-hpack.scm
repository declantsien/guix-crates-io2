(define-module (crates-io fl uk fluke-hpack) #:use-module (crates-io))

(define-public crate-fluke-hpack-0.3.0 (c (n "fluke-hpack") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "02mlw5xcnnxa2fdsfqa5db98rj2d24sfg1dzylaz03q8fksm2dc7") (f (quote (("interop-tests"))))))

(define-public crate-fluke-hpack-0.3.1 (c (n "fluke-hpack") (v "0.3.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "13mx40zcf9cf51x67i8grli01dd7h0z5rf30lf8gf2hamg48sq30") (f (quote (("interop-tests"))))))

