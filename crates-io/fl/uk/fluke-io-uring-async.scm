(define-module (crates-io fl uk fluke-io-uring-async) #:use-module (crates-io))

(define-public crate-fluke-io-uring-async-0.1.0 (c (n "fluke-io-uring-async") (v "0.1.0") (d (list (d (n "io-uring") (r "^0.6.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("rt" "net"))) (d #t) (k 0)))) (h "1cn4kkl81jz887wnmjfcz0lqmh3i150q43v1yy5fxjg7r1szxw9j") (r "1.75")))

