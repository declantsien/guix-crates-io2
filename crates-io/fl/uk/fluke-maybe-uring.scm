(define-module (crates-io fl uk fluke-maybe-uring) #:use-module (crates-io))

(define-public crate-fluke-maybe-uring-0.1.0 (c (n "fluke-maybe-uring") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("extern_crate_std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "sync" "io-util"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "08f23pc8nm5xj4mdpk05c833dxh47002zi0128gb8axn3dwmfkkx") (f (quote (("net" "tokio/net") ("default" "tokio-uring"))))))

(define-public crate-fluke-maybe-uring-0.1.1 (c (n "fluke-maybe-uring") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("extern_crate_std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "sync" "io-util"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1izqyb9b6ncsw2dz4cwfgqjl5ygrmlqwcnk4aiampz09yb77m8ra") (f (quote (("net" "tokio/net") ("default" "tokio-uring"))))))

