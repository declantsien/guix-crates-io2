(define-module (crates-io fl tr fltrs) #:use-module (crates-io))

(define-public crate-fltrs-0.1.0 (c (n "fltrs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^2.0") (d #t) (k 2)))) (h "0b79wci229yyj25sqwnyg87bwzshk3jqwfr6y76wd9aaz2x1grw3") (s 2) (e (quote (("regex" "dep:regex"))))))

(define-public crate-fltrs-0.2.0 (c (n "fltrs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^2.0") (d #t) (k 2)))) (h "1jqa54frjg5hykmmimglagnx22smnc1yg7n0vj03yihfaz1i4nr9") (s 2) (e (quote (("regex" "dep:regex"))))))

