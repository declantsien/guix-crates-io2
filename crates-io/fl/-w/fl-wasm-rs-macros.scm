(define-module (crates-io fl -w fl-wasm-rs-macros) #:use-module (crates-io))

(define-public crate-fl-wasm-rs-macros-0.1.0 (c (n "fl-wasm-rs-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "06xnis1ix6g0r1i43alxbnbm257z3h4xgrirky921lwmvpx8ip4j") (r "1.65")))

