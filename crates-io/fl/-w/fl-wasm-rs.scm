(define-module (crates-io fl -w fl-wasm-rs) #:use-module (crates-io))

(define-public crate-fl-wasm-rs-0.1.0 (c (n "fl-wasm-rs") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "fl-wasm-rs-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00z2lhy5dqgcasf228l5xik1i2xk2ydd126fbzq71vzb59hvcp7n") (r "1.65")))

(define-public crate-fl-wasm-rs-0.1.1 (c (n "fl-wasm-rs") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "fl-wasm-rs-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1grba551z67n9rg1b0gl1f94jj6k8ifhbgmwwin4whnaxlwdj8ys") (r "1.65")))

