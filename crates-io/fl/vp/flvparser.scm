(define-module (crates-io fl vp flvparser) #:use-module (crates-io))

(define-public crate-flvparser-0.0.1 (c (n "flvparser") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "networkio") (r "^0.1.23") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (k 0)) (d (n "tokio-util") (r "^0.6.5") (f (quote ("codec"))) (d #t) (k 0)))) (h "1nr6p3mpsrw6wx847irl0mdp3j0b0f4cbkavsz0i5jj940wsjpk7")))

