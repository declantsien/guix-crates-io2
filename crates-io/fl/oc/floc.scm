(define-module (crates-io fl oc floc) #:use-module (crates-io))

(define-public crate-floc-0.1.0 (c (n "floc") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1b3q7fnzxybhdnk2gam2l5k4kmqy4qzrqn8w98nfdrfs4d8qx8nm")))

