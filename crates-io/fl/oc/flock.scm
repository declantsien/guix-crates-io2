(define-module (crates-io fl oc flock) #:use-module (crates-io))

(define-public crate-flock-0.1.0 (c (n "flock") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.2") (d #t) (k 0)))) (h "0hadlbyjfjv7ajwv0qaviwcyvrd6kpiczb9wv9lbjww8r7grv345")))

(define-public crate-flock-0.2.0 (c (n "flock") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "109sy23gbs7hkr3b2ymnhl8dzm5kwy4jfwfnjwx8hiip96bk77vz")))

