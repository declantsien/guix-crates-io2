(define-module (crates-io fl oc floccus-proc) #:use-module (crates-io))

(define-public crate-floccus-proc-0.2.5 (c (n "floccus-proc") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0y4phdsx815csq71mg34sd8jq03j52z6g8159m5zp3wbcxpyfzca")))

(define-public crate-floccus-proc-0.3.0 (c (n "floccus-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16k0carq65lzrwk8h444ilypalzimf7rrcnjb005a24fmh4y9vzl")))

