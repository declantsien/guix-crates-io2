(define-module (crates-io fl y_ fly_catcher) #:use-module (crates-io))

(define-public crate-fly_catcher-0.1.0 (c (n "fly_catcher") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ly6sff145nppn23d35qxv5x6sr9rkhjsgqq5l7vrv37bpligm6q") (y #t)))

