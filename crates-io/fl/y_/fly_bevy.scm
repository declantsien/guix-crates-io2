(define-module (crates-io fl y_ fly_bevy) #:use-module (crates-io))

(define-public crate-fly_bevy-0.1.0 (c (n "fly_bevy") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "1dm7f93hp8qrcphkr4a642rd4ikxyr6394q4zvn6ngif5sqhng2j")))

(define-public crate-fly_bevy-0.10.2 (c (n "fly_bevy") (v "0.10.2") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "0bx1fj9wm8v85mh4rn956g7y24xfwj2j5fq9262dsqsgchlspbky")))

