(define-module (crates-io fl si flsignal) #:use-module (crates-io))

(define-public crate-flsignal-0.5.0 (c (n "flsignal") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "flnet") (r "^0.5") (d #t) (k 0)) (d (n "flnet-libc") (r "^0.5") (d #t) (k 0)) (d (n "flutils") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n1smcx34pk2427vnasl8b4n2l2l7h1bgry06dam2ckmnnsibb43")))

