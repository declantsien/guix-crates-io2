(define-module (crates-io fl ow flowerbloom) #:use-module (crates-io))

(define-public crate-flowerbloom-0.1.0 (c (n "flowerbloom") (v "0.1.0") (d (list (d (n "bloomfilter") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1i6f7fpybaiqi9ijqrpk7znxy8iaz66j4njykdvc24c3yn4x1701")))

