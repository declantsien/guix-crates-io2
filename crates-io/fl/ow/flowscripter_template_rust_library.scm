(define-module (crates-io fl ow flowscripter_template_rust_library) #:use-module (crates-io))

(define-public crate-flowscripter_template_rust_library-0.1.0 (c (n "flowscripter_template_rust_library") (v "0.1.0") (d (list (d (n "deno_bindgen") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z4pfmynqbmwm4jvp677wpyqa41ckz870lpfzv8dviwiwcg3g6bv")))

(define-public crate-flowscripter_template_rust_library-1.0.0 (c (n "flowscripter_template_rust_library") (v "1.0.0") (h "1gbjmvcx3lwg2rp8s406cqxvl4xh1nq2vvvky7bwdd4p8g0jr1jx")))

(define-public crate-flowscripter_template_rust_library-1.0.1 (c (n "flowscripter_template_rust_library") (v "1.0.1") (h "1wcgfd7135q9asn4yaa3a4bnxq6al2p29v63sy6w7h9z4g5yr6di")))

(define-public crate-flowscripter_template_rust_library-1.0.2 (c (n "flowscripter_template_rust_library") (v "1.0.2") (h "1cywvfpjlgm4m0b5z0mll2jbbp7fjkilhf21aah13dl3fznbcyvk")))

(define-public crate-flowscripter_template_rust_library-1.0.3 (c (n "flowscripter_template_rust_library") (v "1.0.3") (h "04wbmq8y0inb726amxj9ba6r3vdw8n66cgaivmb615xp7xfljyss")))

(define-public crate-flowscripter_template_rust_library-1.1.0 (c (n "flowscripter_template_rust_library") (v "1.1.0") (h "1wmylsjlajlhda3gzvlbmy6c2p9ml04valv492nv1wy93nn4324b")))

(define-public crate-flowscripter_template_rust_library-1.1.1 (c (n "flowscripter_template_rust_library") (v "1.1.1") (h "19snfc3hyv2nc6fv455az1lbvy09rybnskzi436chcacvss578yy")))

(define-public crate-flowscripter_template_rust_library-1.1.2 (c (n "flowscripter_template_rust_library") (v "1.1.2") (h "10bxy4sxz2jl77v4hpy1skpx913k94nkr4rwn5naqkghkbfjmppr")))

(define-public crate-flowscripter_template_rust_library-1.1.3 (c (n "flowscripter_template_rust_library") (v "1.1.3") (h "0fv1xivhnqv0isv34i353hlg0rsvjbr7gqs6n2xx7xd5lgikcyz2")))

(define-public crate-flowscripter_template_rust_library-1.1.4 (c (n "flowscripter_template_rust_library") (v "1.1.4") (h "1ivxxxj5xr8zmkmkkj3v8r44lv9sijj4mhb1p69wflrynj9356a4")))

(define-public crate-flowscripter_template_rust_library-1.1.5 (c (n "flowscripter_template_rust_library") (v "1.1.5") (h "0qphznymggfmc8jl1sm73y2ylmmn6cqwsb7w78md0rrd9f9ascap")))

(define-public crate-flowscripter_template_rust_library-1.1.6 (c (n "flowscripter_template_rust_library") (v "1.1.6") (h "14bm2fiw8l1qajrwqpffggin1h5j937myknqdkm3zk9vkp9afr63")))

(define-public crate-flowscripter_template_rust_library-1.1.7 (c (n "flowscripter_template_rust_library") (v "1.1.7") (h "1zr7rmh3dxfpzw1pqaz5jvm4rcss93znisji4aw69mdd4z8k8niy")))

