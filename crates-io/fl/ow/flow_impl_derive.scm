(define-module (crates-io fl ow flow_impl_derive) #:use-module (crates-io))

(define-public crate-flow_impl_derive-0.8.0 (c (n "flow_impl_derive") (v "0.8.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "0ggfb3hpsg7wqblscxkzi0rl5azi8rb46klsj6iv792gia3jbyzr")))

(define-public crate-flow_impl_derive-0.8.1 (c (n "flow_impl_derive") (v "0.8.1") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "0zml6snibrrzzdgwsg5j48c0mjjl8wjjswks9lf70px1n6a56d96")))

(define-public crate-flow_impl_derive-0.8.2 (c (n "flow_impl_derive") (v "0.8.2") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "1p9w79avpc3vfdd7hxir1d7nxssk250dbz4wmfxhwmgwngcd9kwy")))

(define-public crate-flow_impl_derive-0.8.3 (c (n "flow_impl_derive") (v "0.8.3") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "0zaxdj0as8pjxsj4k57cgvbihldnpfg3vaxxazrdmw3rclk1sp4a")))

(define-public crate-flow_impl_derive-0.8.4 (c (n "flow_impl_derive") (v "0.8.4") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0n3lmhsph9c83k6g87krzmr8alhxars2krfqv7w7k5af36agglsj")))

(define-public crate-flow_impl_derive-0.8.5 (c (n "flow_impl_derive") (v "0.8.5") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1w737jfk4sj39y4isz1rm0a169gr2llj389m0vl47jcb9qby28x9")))

(define-public crate-flow_impl_derive-0.9.0 (c (n "flow_impl_derive") (v "0.9.0") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1bxydzcm9rb9gaabwxfx0dlhvp1w3bnimw2434w8hwf5cmgcw5mf")))

(define-public crate-flow_impl_derive-0.10.0 (c (n "flow_impl_derive") (v "0.10.0") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0mvyf2292baxmk8nd48diprk3fwiawh085yf62617nkwqp4b6kjm")))

(define-public crate-flow_impl_derive-0.21.0 (c (n "flow_impl_derive") (v "0.21.0") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1848sakwz1smsx43i3m0pw2gqs5fb9inhay24f70rjmklxfjndp2")))

(define-public crate-flow_impl_derive-0.34.7 (c (n "flow_impl_derive") (v "0.34.7") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "08i4ph64gf6v69xmmj8a39jysqx75za2f98vc5kmzm4rv06hhnns")))

(define-public crate-flow_impl_derive-0.40.0 (c (n "flow_impl_derive") (v "0.40.0") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0y57wzagmnng08nvgh9lh1x9qjpi3kpzr9256iql2syd7b0nx88q")))

(define-public crate-flow_impl_derive-0.40.1 (c (n "flow_impl_derive") (v "0.40.1") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1c0qmy4q19pzkzi75n3y9dia5asrpyswlykw8jc553q8vxz2fp9b")))

