(define-module (crates-io fl ow flow-graph) #:use-module (crates-io))

(define-public crate-flow-graph-0.21.0 (c (n "flow-graph") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "test-logger") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "wick-config") (r "^0.26.0") (d #t) (k 2)) (d (n "wick-logger") (r "^0.2.0") (d #t) (k 2)))) (h "1bxydjwm5iybnvg6x44qp2q3a3hbyzhjsbjwqfpjyzca2vamwpjy")))

(define-public crate-flow-graph-0.22.0 (c (n "flow-graph") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "181aj8vadm2f183x67r4pmszj356922i6rc29gwq38s8rvgfr45m")))

(define-public crate-flow-graph-0.23.0 (c (n "flow-graph") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1njqb1j43m4m70wgihvy8ni4vr10nmywba5gindncbcksnp6gpv7")))

