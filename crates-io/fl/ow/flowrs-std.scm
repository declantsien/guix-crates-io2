(define-module (crates-io fl ow flowrs-std) #:use-module (crates-io))

(define-public crate-flowrs-std-0.1.0 (c (n "flowrs-std") (v "0.1.0") (d (list (d (n "flowrs") (r "^0.1.0") (d #t) (k 0)) (d (n "flowrs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0a56cwh5lnd0yzd3r98dpb8iynhqqpp1k76fakz9gf9hp6lfm0ya")))

(define-public crate-flowrs-std-0.2.0 (c (n "flowrs-std") (v "0.2.0") (d (list (d (n "flowrs") (r "^0.1.0") (d #t) (k 0)) (d (n "flowrs-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1n16cs5f2a55cb7052z3a6lgyz6agkbcbjdldw5dr4zx3aqlf7qm")))

