(define-module (crates-io fl ow flowruntime) #:use-module (crates-io))

(define-public crate-flowruntime-0.8.6 (c (n "flowruntime") (v "0.8.6") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "flow_impl") (r "= 0.8.5") (d #t) (k 0)) (d (n "flowrlib") (r "= 0.8.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simplog") (r "~1.2") (d #t) (k 0)) (d (n "url") (r "~2.1") (d #t) (k 0)))) (h "1xvh856fdc5g8d1yvafj83bskyd14pz8ds1rg6csb93sxlcygfna")))

(define-public crate-flowruntime-0.8.8 (c (n "flowruntime") (v "0.8.8") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "flow_impl") (r "= 0.8.5") (d #t) (k 0)) (d (n "flowrlib") (r "= 0.8.8") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simplog") (r "~1.2") (d #t) (k 0)) (d (n "url") (r "~2.1") (d #t) (k 0)))) (h "1lp976r2yvsppzk6vqzkwjiws4qn84zjf5z5g5l55ls10hcvjh3a")))

