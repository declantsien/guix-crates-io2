(define-module (crates-io fl ow flowr-utilities) #:use-module (crates-io))

(define-public crate-flowr-utilities-0.100.0 (c (n "flowr-utilities") (v "0.100.0") (h "1qxq47n9jyxb2na494q8pavxi5wgv8az29kplxrzk248d9rca9y6")))

(define-public crate-flowr-utilities-0.101.0 (c (n "flowr-utilities") (v "0.101.0") (h "0fs9jp4d3sz6d7jzs9k5m9ayc8kw31ylgspfqrj67sqjj1llffvb")))

(define-public crate-flowr-utilities-0.107.0 (c (n "flowr-utilities") (v "0.107.0") (h "0mrb5z6c5118v6qy8c9nk2d65n74y3h8dgw3jk5r6wqg4i0hkpg1")))

(define-public crate-flowr-utilities-0.108.0 (c (n "flowr-utilities") (v "0.108.0") (h "0wqp5znyx94ykbjqa90sbkjrd7rs8lw8yvx9k29z30fwmhc6qzw8")))

(define-public crate-flowr-utilities-0.109.0 (c (n "flowr-utilities") (v "0.109.0") (h "0rhxnv7wkmhp7kz0i8nhkkfww0ajjh9bapyb6cxk8vhjp90s2f15")))

(define-public crate-flowr-utilities-0.110.0 (c (n "flowr-utilities") (v "0.110.0") (h "0mh3wy82fixciy6ca5rr6dk2kqdra7s685lwc9day5lyn85h7vjh")))

(define-public crate-flowr-utilities-0.112.0 (c (n "flowr-utilities") (v "0.112.0") (h "15mjn6mz5j7252ihyqd99plhdmir0ym7qq2nqnxmqr5xbmnly4sd")))

(define-public crate-flowr-utilities-0.113.0 (c (n "flowr-utilities") (v "0.113.0") (h "0mc6dbxwy6n9ibzff1qnka306zwl18j3dq8d0ml857y82qs0zzyy")))

(define-public crate-flowr-utilities-0.114.0 (c (n "flowr-utilities") (v "0.114.0") (h "1kdjpxdhgcx1v8l9l6n5356xa5whcrcpycw0a7gzxc0lrk8jxz8x")))

(define-public crate-flowr-utilities-0.115.0 (c (n "flowr-utilities") (v "0.115.0") (h "0qvbw2w2kwsdasy3w4qdyf7cc5p9cywvnjd7gr1gm9xmjx3zjf13")))

(define-public crate-flowr-utilities-0.116.0 (c (n "flowr-utilities") (v "0.116.0") (h "120jn20qpsikqn8w28n0vgisz0q2p51278p6k1ll6wmprm3hzl4z")))

(define-public crate-flowr-utilities-0.117.0 (c (n "flowr-utilities") (v "0.117.0") (h "1ap4qxh7f76rrf3ldr65rv4k9kpiaiiga2z03xv8wln1qkrzpw23")))

(define-public crate-flowr-utilities-0.120.0 (c (n "flowr-utilities") (v "0.120.0") (h "0phgfw1x16c112d4j9vyyf07lhzfpb5wbqwg015nnd8qkx4va8sl")))

(define-public crate-flowr-utilities-0.121.0 (c (n "flowr-utilities") (v "0.121.0") (h "0dc6z87m0k1yzarkfr5il38whqpig5jsfdcigdyi1qagryn6nyd4")))

(define-public crate-flowr-utilities-0.124.0 (c (n "flowr-utilities") (v "0.124.0") (h "05m1ncrgfmh9zn8zd6lsfdcxsrfp46djl3bzba1vq68hx2ghbs6a")))

(define-public crate-flowr-utilities-0.125.0 (c (n "flowr-utilities") (v "0.125.0") (h "1mlmhvhvszvzh484q207jwf8i0vh6gcvildiq282spwrf1h9fhsj")))

(define-public crate-flowr-utilities-0.126.0 (c (n "flowr-utilities") (v "0.126.0") (h "14hgb4xrsfi5wami1hbinhvy16499x8s0pwyl154fxd8r6im469b")))

(define-public crate-flowr-utilities-0.127.0 (c (n "flowr-utilities") (v "0.127.0") (h "1ccrmzls2zbpk9rw44nhw82jd6w0sfwcv5p6cv9iiybr914rrbsa")))

(define-public crate-flowr-utilities-0.128.0 (c (n "flowr-utilities") (v "0.128.0") (h "1fq0wprvbjv8985pz8m3kpsymrdl0vbkzhbb6ppkvci88q6852i5")))

(define-public crate-flowr-utilities-0.129.0 (c (n "flowr-utilities") (v "0.129.0") (h "0a8qjzbwwiqm66h2lw0k4kfc35np0n7qm2wlvis3ri7a4db4l365")))

(define-public crate-flowr-utilities-0.130.0 (c (n "flowr-utilities") (v "0.130.0") (h "12dk695b6ky1r5ph9qx14znnnh59q2jpnwssmzmpwhfzylahh2y2")))

(define-public crate-flowr-utilities-0.131.0 (c (n "flowr-utilities") (v "0.131.0") (h "0bzrfh36kzgbbv0xz3vlmgv7z2bhqq6dy3r637n8plhpvzprslpd")))

(define-public crate-flowr-utilities-0.132.0 (c (n "flowr-utilities") (v "0.132.0") (h "0ay3klnfwqcjcw69hb0a0drg2xyd2vknslcbl3mbqcrw8s3hlnjn")))

(define-public crate-flowr-utilities-0.133.0 (c (n "flowr-utilities") (v "0.133.0") (h "05w9sssnksxxxq97641avq5ivwkqrj001grrafvzfjs7qhgjrm7k")))

(define-public crate-flowr-utilities-0.134.0 (c (n "flowr-utilities") (v "0.134.0") (h "0p8f0svikl6zyzsr8grib7wyfc7p4zjvssj75vhnkys48p2xaczb")))

(define-public crate-flowr-utilities-0.135.0 (c (n "flowr-utilities") (v "0.135.0") (h "186rk781s0mdhq43l5nn3vim0cm9pnm5ahqnlh5zpqcz8lpkkvbc")))

(define-public crate-flowr-utilities-0.136.0 (c (n "flowr-utilities") (v "0.136.0") (h "0zaqjjkljh4lagk6bh5287j1mp1v4ai310s6gxg1zq09n28jdfmg")))

(define-public crate-flowr-utilities-0.137.0 (c (n "flowr-utilities") (v "0.137.0") (h "1mhk0sp9rkfrnlwmfwlmkvr2m0a31w5sf6vff32qwz2b25mhcvvf")))

(define-public crate-flowr-utilities-0.138.0 (c (n "flowr-utilities") (v "0.138.0") (h "1w94ycs2abgsn5dnfpcz7hq3scxfwx69xyn8vlna7i1lamk417a6")))

(define-public crate-flowr-utilities-0.139.0 (c (n "flowr-utilities") (v "0.139.0") (h "08i62dqla4l8420d66ykswqvmm23dg4bvygq8a28ssqgc0nzpdqi")))

(define-public crate-flowr-utilities-0.140.0 (c (n "flowr-utilities") (v "0.140.0") (h "1p6q4kd8qb1z9naf2rmjzkiki5zq7vh71fqgk70ra3pd17fi5r48")))

(define-public crate-flowr-utilities-0.141.0 (c (n "flowr-utilities") (v "0.141.0") (h "1hvsig3h90qip0fwpdbcqh2m99f94x544afpmfjli13c1vl5j30l")))

(define-public crate-flowr-utilities-0.142.0 (c (n "flowr-utilities") (v "0.142.0") (h "1wgamwc88wih4y6vmywblgv98yp3aywy3q07iazll5d0zw8jmhf8")))

