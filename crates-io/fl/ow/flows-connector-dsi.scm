(define-module (crates-io fl ow flows-connector-dsi) #:use-module (crates-io))

(define-public crate-flows-connector-dsi-0.1.0 (c (n "flows-connector-dsi") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "19h1da3f60r54ch488vjxphhaf1a84bkvn8l6rpyclvl237xl3lw") (y #t)))

(define-public crate-flows-connector-dsi-0.1.1 (c (n "flows-connector-dsi") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1746m0xnnadgsyfq7n0sjlqpp8bl6bzhrhkf4nkia845rl6qd9vr") (y #t)))

(define-public crate-flows-connector-dsi-0.1.2 (c (n "flows-connector-dsi") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1m21l2k7vd5davmmrj239b1v2xr22fkw4ybf2c3ll8hwdg3g7ixn") (y #t)))

(define-public crate-flows-connector-dsi-0.1.3 (c (n "flows-connector-dsi") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0g1sk0nzk2pclfdchzv2c3xx7wg6ad2lyy7dmhznyk2wc5qsfg1d")))

(define-public crate-flows-connector-dsi-0.1.4 (c (n "flows-connector-dsi") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "12rf6r2y2c8a5iq0d3m4rabrpy2ipmcgrf4i4dk8sdg24rn8ch94")))

(define-public crate-flows-connector-dsi-0.1.5 (c (n "flows-connector-dsi") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11s70vnvfy2h96i3yp1cqfp2msdpz1p1p0izz8kd0h4qb9v33kjl")))

(define-public crate-flows-connector-dsi-0.1.6 (c (n "flows-connector-dsi") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "086jpy47r9qlfqh9j4nyxn4zp49wax6xz8dpnw4iv9agbgb60ynd")))

(define-public crate-flows-connector-dsi-0.1.7 (c (n "flows-connector-dsi") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rrdipmz50mi028al3rm5g4kw9v7m0pq407g9l8r5bpnm8il6lkr")))

(define-public crate-flows-connector-dsi-0.1.8 (c (n "flows-connector-dsi") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1dcg174gc21644afyf2mm7ngqy7ln96p25pngwk1v9g5bmcmibfj")))

(define-public crate-flows-connector-dsi-0.1.9 (c (n "flows-connector-dsi") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1dff0302yb84a9m7ihcj41nj3vgv0w8ravh7jbnldpaaw3wqn7y2")))

(define-public crate-flows-connector-dsi-0.1.10 (c (n "flows-connector-dsi") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1vlh1p83smxx3yjankiplvzaqklvmxq07j3ylsqkylh7mqvqf9y6")))

(define-public crate-flows-connector-dsi-0.1.11 (c (n "flows-connector-dsi") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0k22jxajwh6hypf0spm3ywczarrqsba5rz4gqigy3s9rsn1cl219")))

