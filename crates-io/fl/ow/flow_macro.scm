(define-module (crates-io fl ow flow_macro) #:use-module (crates-io))

(define-public crate-flow_macro-0.40.1 (c (n "flow_macro") (v "0.40.1") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1v6qn4flbi6a911673qpsvyhp956qxvaqxgxnq2d0banx2dw34yh")))

(define-public crate-flow_macro-0.42.0 (c (n "flow_macro") (v "0.42.0") (d (list (d (n "flowcore") (r "^0.42.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0va3kxnknbfz6hf1kanlk9w7rs8ag4pxlpwhg3azwyg2sqr75g0r")))

