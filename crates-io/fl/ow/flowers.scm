(define-module (crates-io fl ow flowers) #:use-module (crates-io))

(define-public crate-flowers-0.0.1 (c (n "flowers") (v "0.0.1") (d (list (d (n "getopts") (r "^0.1.4") (d #t) (k 0)))) (h "01gpfm62dxzpzv23pb3lmnw92fmwvsa7av1w28vb6f4r19z8dnsb") (y #t)))

(define-public crate-flowers-0.0.2 (c (n "flowers") (v "0.0.2") (d (list (d (n "getopts") (r "^0.1.4") (d #t) (k 0)))) (h "0kp0alg0iak4gsa4d4n7d7fg2bm6m6akyz9imy8pzrpqybqj0d8z") (y #t)))

(define-public crate-flowers-0.0.3 (c (n "flowers") (v "0.0.3") (d (list (d (n "getopts") (r "^0.1.4") (d #t) (k 0)))) (h "1547i2p52wgx0dalnsm6ynas6vig33hkxgrrfz0lskdwihwkqv90") (y #t)))

(define-public crate-flowers-0.0.4 (c (n "flowers") (v "0.0.4") (d (list (d (n "getopts") (r "^0.1.4") (d #t) (k 0)))) (h "0wy5bmldr299cikr86kgydkk8wxxainmjigkb6p595wy5gr9111b") (y #t)))

(define-public crate-flowers-0.0.5 (c (n "flowers") (v "0.0.5") (h "0400mxr6hb82vyfsar827bvbpkfvx00pz254xfvd8nhnxxj3icri") (y #t)))

