(define-module (crates-io fl ow flowsamples) #:use-module (crates-io))

(define-public crate-flowsamples-0.24.0 (c (n "flowsamples") (v "0.24.0") (d (list (d (n "flowc") (r "^0.23.0") (d #t) (k 1)) (d (n "flowr") (r "^0.24.0") (f (quote ("debugger" "metrics" "checks"))) (d #t) (k 0)))) (h "0vs3yxmcnpkf1axr5jj21rzc8x8j3abm2yzp8aj6ivgqyhkxw330") (y #t)))

(define-public crate-flowsamples-0.25.0 (c (n "flowsamples") (v "0.25.0") (d (list (d (n "flowc") (r "^0.25.0") (d #t) (k 1)) (d (n "flowr") (r "^0.25.0") (f (quote ("debugger" "metrics" "checks"))) (d #t) (k 0)))) (h "1pb3f736y8jwrn1rdxzragc4g0fr0m1iapq2988hc8abngf690c2") (y #t)))

(define-public crate-flowsamples-0.29.1 (c (n "flowsamples") (v "0.29.1") (d (list (d (n "flowc") (r "^0.25.0") (d #t) (k 1)) (d (n "flowr") (r "^0.25.0") (f (quote ("debugger" "metrics" "checks"))) (d #t) (k 0)) (d (n "simpath") (r "~1.4") (d #t) (k 0)) (d (n "simpath") (r "~1.4") (d #t) (k 1)))) (h "0ld37dl8wzn28a1j4ndsa6m5mnyr3vi9za61wd6dqfy5g54xcxb5") (y #t)))

(define-public crate-flowsamples-0.29.2 (c (n "flowsamples") (v "0.29.2") (d (list (d (n "flowc") (r "^0.25.0") (d #t) (k 1)) (d (n "flowr") (r "^0.25.0") (f (quote ("debugger" "metrics" "checks"))) (d #t) (k 0)) (d (n "simpath") (r "~1.4") (d #t) (k 0)) (d (n "simpath") (r "~1.4") (d #t) (k 1)))) (h "1wk72ysfc2h3cvk3c55086yxarnkvgd20wsrhc3bgy7qn8hrpvi8") (y #t)))

(define-public crate-flowsamples-0.29.3 (c (n "flowsamples") (v "0.29.3") (d (list (d (n "flowc") (r "^0.25.0") (d #t) (k 1)) (d (n "flowr") (r "^0.25.0") (f (quote ("debugger" "metrics" "checks"))) (d #t) (k 0)) (d (n "simpath") (r "~1.4") (d #t) (k 0)) (d (n "simpath") (r "~1.4") (d #t) (k 1)))) (h "114f901msrl3hq8jri8fzxq0hf4pp7k89i7kxii3danahaz6g7jq") (y #t)))

(define-public crate-flowsamples-0.30.0 (c (n "flowsamples") (v "0.30.0") (d (list (d (n "flowc") (r "^0.25.0") (d #t) (k 1)) (d (n "flowr") (r "^0.25.0") (f (quote ("debugger" "metrics" "checks"))) (d #t) (k 0)) (d (n "simpath") (r "~1.4") (d #t) (k 0)) (d (n "simpath") (r "~1.4") (d #t) (k 1)))) (h "1c607zrksqg0sldvwz0xsqcasn9hpx7mk5hj3jqsai4kbcv6c2yi") (y #t)))

(define-public crate-flowsamples-0.34.4 (c (n "flowsamples") (v "0.34.4") (d (list (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)) (d (n "simpath") (r "~2.1") (f (quote ("urls"))) (d #t) (k 0)) (d (n "simpath") (r "~2.1") (f (quote ("urls"))) (d #t) (k 1)))) (h "1jfgn8hl97fr7mnpr6c445f079k8chjk9r505riqi2ahp8ikqxga") (y #t)))

(define-public crate-flowsamples-0.34.7 (c (n "flowsamples") (v "0.34.7") (d (list (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)) (d (n "simpath") (r "~2.2") (f (quote ("urls"))) (d #t) (k 0)) (d (n "simpath") (r "~2.2") (f (quote ("urls"))) (d #t) (k 1)))) (h "01f4f84wy2lvn73cv1xhzqzwd4lrnbzh77dsm02zp0lwvvdbckia") (y #t)))

(define-public crate-flowsamples-0.34.10 (c (n "flowsamples") (v "0.34.10") (d (list (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)) (d (n "simpath") (r "~2.2") (f (quote ("urls"))) (d #t) (k 0)) (d (n "simpath") (r "~2.2") (f (quote ("urls"))) (d #t) (k 1)))) (h "1m4pvjrhm1660c6azcv560ajr3vgxgyjzr77970c0h0p16jh7cay") (y #t)))

(define-public crate-flowsamples-0.44.0 (c (n "flowsamples") (v "0.44.0") (d (list (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "0vghzn966314m4zg2prbycsgz4bph53l9kiabgw3ggvk12mfyg0a") (y #t)))

(define-public crate-flowsamples-0.46.0 (c (n "flowsamples") (v "0.46.0") (d (list (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "1csggi3bxixky3i0585ncv503n533mksjvnpb03cg4cbsp22sarg") (y #t)))

(define-public crate-flowsamples-0.47.0 (c (n "flowsamples") (v "0.47.0") (d (list (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "0xgxz26y855fgzm4s74f44zh59b3ab3icixc7yhz3cpqj1fr3mwq") (y #t)))

(define-public crate-flowsamples-0.50.0 (c (n "flowsamples") (v "0.50.0") (d (list (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "02vdy33whqfxjdn9iqx6wanpj0gyxkw11vayh524swxyx8387mzh") (y #t)))

(define-public crate-flowsamples-0.60.0 (c (n "flowsamples") (v "0.60.0") (d (list (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "1p0dplxs1zg009i74qxd827m8wh2gk843c98zvzfa5rwzk6fhyk8") (y #t)))

(define-public crate-flowsamples-0.70.0 (c (n "flowsamples") (v "0.70.0") (d (list (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "0vi4vqwi3ysk2mdr1s45kxjlc3wy3rc7la3vnqrqhr2cgr43pn8g") (y #t)))

(define-public crate-flowsamples-0.80.0 (c (n "flowsamples") (v "0.80.0") (d (list (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "0a2awrhixcc335h8ji2wm6fi3flfvshqs4r52090dgys8k8zag5i") (y #t)))

(define-public crate-flowsamples-0.90.0 (c (n "flowsamples") (v "0.90.0") (d (list (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "1sjlly4fxk2h1nc3m91xgcfgmsa7nkh6dxirxmvk89bhdg8ki839") (y #t)))

(define-public crate-flowsamples-0.91.0 (c (n "flowsamples") (v "0.91.0") (d (list (d (n "serial_test") (r "^0.10.0") (d #t) (k 2)))) (h "0rhb4pdihws3jzqcn6bkyd51f13l3f358b7jzqp29gb0wnlvgbxg") (y #t)))

(define-public crate-flowsamples-0.92.0 (c (n "flowsamples") (v "0.92.0") (d (list (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "177vvj0ib31hr7vy8pwjb3lwjj97cyadgxxwf6qdqcnhq8pfpwg1") (f (quote (("wasm") ("default")))) (y #t)))

(define-public crate-flowsamples-0.95.0 (c (n "flowsamples") (v "0.95.0") (d (list (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1a2pawa7wqs8q12h9lw24vghgllyii1j0a0ik8l4mx1h42mx8pb7") (f (quote (("default")))) (y #t)))

