(define-module (crates-io fl ow flow_arena) #:use-module (crates-io))

(define-public crate-flow_arena-0.4.0 (c (n "flow_arena") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1dwr7404pc9c5l0ry3fdw9phy9gq64m80sqy83m54s0dpn0kng6p") (f (quote (("serde1" "serde") ("default" "serde1"))))))

(define-public crate-flow_arena-0.4.1 (c (n "flow_arena") (v "0.4.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0jkriiqfi7lk2j0kxyskh1zmhiycphizhj0x03kjsjzb7g0xsbp2") (f (quote (("serde1" "serde") ("default" "serde1"))))))

(define-public crate-flow_arena-0.4.2 (c (n "flow_arena") (v "0.4.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1s5yxk0f4df5nz7r1x6wcamqdzrxxfxdbwlncgsb2vlxcwd6gxx6") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-flow_arena-0.4.3 (c (n "flow_arena") (v "0.4.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1gzv52g4594c5l80cvb31h56ars190jyllj67z9gzb82cs56wzhi") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-flow_arena-0.4.4 (c (n "flow_arena") (v "0.4.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "02f45b9v8sbmav76zvc3p2c1jg403wsd2i7lfi1qa3hp7321rf64") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

