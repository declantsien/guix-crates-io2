(define-module (crates-io fl ow flowutils) #:use-module (crates-io))

(define-public crate-flowutils-0.1.0 (c (n "flowutils") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "07v1ny3c9c2f0i8rxpzcpdgr84cs4hdi58zys9125ll0m4bimja6")))

