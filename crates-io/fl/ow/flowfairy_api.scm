(define-module (crates-io fl ow flowfairy_api) #:use-module (crates-io))

(define-public crate-flowfairy_api-0.1.0 (c (n "flowfairy_api") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0n3ya3nfayq2hhpjyc23yrqqjx9x7a3w5vjib6najx0q9igzd32c")))

(define-public crate-flowfairy_api-0.2.0 (c (n "flowfairy_api") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01759ys82l3280rqv1lxcs71wwhizdza5grkka8f8fdinz1addm4")))

(define-public crate-flowfairy_api-0.2.2 (c (n "flowfairy_api") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0wzhr74cncnl0mgx95yldgy4bkc701pn50nvldkxcvz4wgbj71sc")))

