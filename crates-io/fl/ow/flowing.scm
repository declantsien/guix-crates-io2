(define-module (crates-io fl ow flowing) #:use-module (crates-io))

(define-public crate-flowing-0.1.0 (c (n "flowing") (v "0.1.0") (h "030nc1nax6mj0ds2v1sa77vgn8gv6y8fcdkayikmrac40r37xnpg")))

(define-public crate-flowing-0.2.0 (c (n "flowing") (v "0.2.0") (h "1z8lp4xvlp29gym5wjrbm8hnlbscnwwwv6x63dndg5mgb3z6d0vj")))

(define-public crate-flowing-0.2.1 (c (n "flowing") (v "0.2.1") (h "1wk6dz6w53ql9dgk78r6yldz8pwg8q414qlfgahhkszas45cacqy")))

