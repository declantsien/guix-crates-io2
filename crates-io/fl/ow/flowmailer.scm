(define-module (crates-io fl ow flowmailer) #:use-module (crates-io))

(define-public crate-flowmailer-0.1.0 (c (n "flowmailer") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gq1pgg6kh5v9schzirblzhxardgzvvaylhsdqakapj5vc4m0bh5")))

(define-public crate-flowmailer-0.1.1 (c (n "flowmailer") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r1jvni5k07lb6d0yvx1cqnxx78npr1lmixbph2jficmf9w9ksfa")))

