(define-module (crates-io fl ow flowrs-wasm) #:use-module (crates-io))

(define-public crate-flowrs-wasm-0.1.0 (c (n "flowrs-wasm") (v "0.1.0") (d (list (d (n "flowrs") (r "^0.1.0") (d #t) (k 0)) (d (n "flowrs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flowrs-std") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "0f0m9vz5blncbnlvx73ms9l2pasw4zf1gdbrciwswxcdd1hq2v8a")))

