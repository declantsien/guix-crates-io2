(define-module (crates-io fl ow flow-free) #:use-module (crates-io))

(define-public crate-flow-free-0.0.0 (c (n "flow-free") (v "0.0.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rs-graph") (r "^0.20.1") (d #t) (k 0)))) (h "1lcv36kmys8l09q1gzdp96rrxbr3a8r5cqy9zvh5f6az99n4xl2w") (f (quote (("default"))))))

