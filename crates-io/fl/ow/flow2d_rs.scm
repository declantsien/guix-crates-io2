(define-module (crates-io fl ow flow2d_rs) #:use-module (crates-io))

(define-public crate-flow2d_rs-0.1.0 (c (n "flow2d_rs") (v "0.1.0") (d (list (d (n "iced") (r "^0.10") (f (quote ("canvas" "tokio"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3.3") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "10ybqx1fiisvawfinx6gslcvb4vwclizb2yj92gw9nyy0dj0nb2x")))

