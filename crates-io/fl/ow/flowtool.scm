(define-module (crates-io fl ow flowtool) #:use-module (crates-io))

(define-public crate-flowtool-1.0.0 (c (n "flowtool") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "pmd_flow") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "15zsh6dw2sacixvhnzn9lpyhmmbz136c9xblk798qmnxdddmjayp")))

