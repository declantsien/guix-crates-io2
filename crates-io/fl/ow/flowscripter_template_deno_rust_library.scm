(define-module (crates-io fl ow flowscripter_template_deno_rust_library) #:use-module (crates-io))

(define-public crate-flowscripter_template_deno_rust_library-1.0.6 (c (n "flowscripter_template_deno_rust_library") (v "1.0.6") (d (list (d (n "deno_bindgen") (r "^0.5.1") (d #t) (k 0)) (d (n "flowscripter_template_rust_library") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kcs89gfi5sc7dicy2ypy0nilmr073rkb0na8dgsxqpir3p7640d")))

(define-public crate-flowscripter_template_deno_rust_library-1.0.7 (c (n "flowscripter_template_deno_rust_library") (v "1.0.7") (d (list (d (n "deno_bindgen") (r "^0.5.1") (d #t) (k 0)) (d (n "flowscripter_template_rust_library") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dfixg3l5gc5z2n74hga9nqh8jwflazsiccfh2784a19zxh4650k")))

(define-public crate-flowscripter_template_deno_rust_library-1.0.8 (c (n "flowscripter_template_deno_rust_library") (v "1.0.8") (d (list (d (n "deno_bindgen") (r "^0.5.1") (d #t) (k 0)) (d (n "flowscripter_template_rust_library") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "17q8br5yvhpvf2i3b1ys7gapz75q22x4y2s1wh5gksja4bvdiwk7")))

(define-public crate-flowscripter_template_deno_rust_library-1.0.9 (c (n "flowscripter_template_deno_rust_library") (v "1.0.9") (d (list (d (n "deno_bindgen") (r "^0.6.0") (d #t) (k 0)) (d (n "flowscripter_template_rust_library") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "06fp3davwqm3mw300iv5dbnlvq1qndw3l8sv25lfqm3inf5ba94q")))

(define-public crate-flowscripter_template_deno_rust_library-1.0.10 (c (n "flowscripter_template_deno_rust_library") (v "1.0.10") (d (list (d (n "deno_bindgen") (r "^0.6.0") (d #t) (k 0)) (d (n "flowscripter_template_rust_library") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "15c76a43bllk6p3gl5nhb6jq67y5z1ndi2azzza4n05bh3nq482v")))

(define-public crate-flowscripter_template_deno_rust_library-1.0.11 (c (n "flowscripter_template_deno_rust_library") (v "1.0.11") (d (list (d (n "deno_bindgen") (r "^0.6.0") (d #t) (k 0)) (d (n "flowscripter_template_rust_library") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zg88s8arm990apyll739qsry4ldxbj6mpk7c92wnciwn1vldhbp")))

(define-public crate-flowscripter_template_deno_rust_library-1.0.13 (c (n "flowscripter_template_deno_rust_library") (v "1.0.13") (d (list (d (n "deno_bindgen") (r "^0.6.0") (d #t) (k 0)) (d (n "flowscripter_template_rust_library") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z33l7cqh302spr1a8841b200jx87annx7q6w4wvxpp810wjs02g")))

(define-public crate-flowscripter_template_deno_rust_library-1.0.14 (c (n "flowscripter_template_deno_rust_library") (v "1.0.14") (d (list (d (n "deno_bindgen") (r "^0.6.0") (d #t) (k 0)) (d (n "flowscripter_template_rust_library") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v6qxdl9mxy51srmdmx92akz6wv7z1428hvf8hbzcqk4hmx36q8p")))

(define-public crate-flowscripter_template_deno_rust_library-1.0.15 (c (n "flowscripter_template_deno_rust_library") (v "1.0.15") (d (list (d (n "deno_bindgen") (r "^0.6.0") (d #t) (k 0)) (d (n "flowscripter_template_rust_library") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f7chzp8w88qr0digx82bjpqvaqlr4917z68as3i3wqrp1p73ik8")))

