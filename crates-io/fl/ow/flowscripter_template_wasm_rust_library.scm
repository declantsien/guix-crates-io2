(define-module (crates-io fl ow flowscripter_template_wasm_rust_library) #:use-module (crates-io))

(define-public crate-flowscripter_template_wasm_rust_library-1.0.3 (c (n "flowscripter_template_wasm_rust_library") (v "1.0.3") (d (list (d (n "flowscripter_template_rust_library") (r "^1.0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 2)))) (h "1h1mrlbmhnqvgkhz5v0dishcz36jl7ph11y1djbhqsm7isla8g3n")))

(define-public crate-flowscripter_template_wasm_rust_library-1.0.4 (c (n "flowscripter_template_wasm_rust_library") (v "1.0.4") (d (list (d (n "flowscripter_template_rust_library") (r "^1.0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.30") (d #t) (k 2)))) (h "0q2p6pi8nh3c4mid72llpnqjzflknqsgrws938agxyfnmc874a99")))

(define-public crate-flowscripter_template_wasm_rust_library-1.0.5 (c (n "flowscripter_template_wasm_rust_library") (v "1.0.5") (d (list (d (n "flowscripter_template_rust_library") (r "^1.0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.30") (d #t) (k 2)))) (h "1nnnz5y4sv6fqj7vy45rspr5hffm368n7bffp02zyis1yvr29xyw")))

(define-public crate-flowscripter_template_wasm_rust_library-1.1.0 (c (n "flowscripter_template_wasm_rust_library") (v "1.1.0") (d (list (d (n "flowscripter_template_rust_library") (r "^1.0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.30") (d #t) (k 2)))) (h "1r7nj2w95k5vzz0ymd1wsjs2qm3hl9niacxk8psna4ma97gs444i")))

(define-public crate-flowscripter_template_wasm_rust_library-1.1.2 (c (n "flowscripter_template_wasm_rust_library") (v "1.1.2") (d (list (d (n "flowscripter_template_rust_library") (r "^1.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.30") (d #t) (k 2)))) (h "163c1jlf11vjr222fa44342zgq7yhzg8xbhdb1240vsdp3y24vdp")))

(define-public crate-flowscripter_template_wasm_rust_library-1.1.3 (c (n "flowscripter_template_wasm_rust_library") (v "1.1.3") (d (list (d (n "flowscripter_template_rust_library") (r "^1.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.30") (d #t) (k 2)))) (h "1m1vvxjsnmv8jbmv3yld65lk3zz2xrwiqjqcj12v84qh7dprsdv7")))

(define-public crate-flowscripter_template_wasm_rust_library-1.1.5 (c (n "flowscripter_template_wasm_rust_library") (v "1.1.5") (d (list (d (n "flowscripter_template_rust_library") (r "^1.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.30") (d #t) (k 2)))) (h "13mf9nn1vq7qg5c3vhd3rrsqb0731hvlzja4h0hbdl07gfbidmw3")))

(define-public crate-flowscripter_template_wasm_rust_library-1.1.6 (c (n "flowscripter_template_wasm_rust_library") (v "1.1.6") (d (list (d (n "flowscripter_template_rust_library") (r "^1.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.30") (d #t) (k 2)))) (h "08rdgvk8ngsfc38b403c16cq953dfka2pr9w24kqh5h9giz6g4pj")))

(define-public crate-flowscripter_template_wasm_rust_library-1.1.7 (c (n "flowscripter_template_wasm_rust_library") (v "1.1.7") (d (list (d (n "flowscripter_template_rust_library") (r "^1.1.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.32") (d #t) (k 2)))) (h "1ww7brl1r68w6hg3l58s28cjhikgmahkbjzy0zsib5as44abxkx2")))

(define-public crate-flowscripter_template_wasm_rust_library-1.1.8 (c (n "flowscripter_template_wasm_rust_library") (v "1.1.8") (d (list (d (n "flowscripter_template_rust_library") (r "^1.1.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (k 2)))) (h "1syy9jl3cbi1sycbz1s2m3l432nnbxns67nv7f35pcp3agc6jm7z")))

(define-public crate-flowscripter_template_wasm_rust_library-1.1.9 (c (n "flowscripter_template_wasm_rust_library") (v "1.1.9") (d (list (d (n "flowscripter_template_rust_library") (r "^1.1.6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (k 2)))) (h "11y17scs5hd7lkav6ca9ahxqm9l86ygimj3kc46rbcyq9p5kpr53")))

