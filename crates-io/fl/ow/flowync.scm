(define-module (crates-io fl ow flowync) #:use-module (crates-io))

(define-public crate-flowync-0.1.0 (c (n "flowync") (v "0.1.0") (h "120cnmzs2vslcyqpxnbns8l41brgsmvfnl9q4b1387lmyclxgxxv") (y #t)))

(define-public crate-flowync-0.1.5 (c (n "flowync") (v "0.1.5") (h "0ab6jwkm7jrwis3x61yp9ldll627pap1myfnms41db1qmm9jwjnx")))

(define-public crate-flowync-0.2.0 (c (n "flowync") (v "0.2.0") (h "0givn0vnnqs0s5f50y36kny1w5dpgb2b7bqlm5kzilmj2bxwaprj")))

(define-public crate-flowync-1.0.0 (c (n "flowync") (v "1.0.0") (h "1bihyllv8c8xj4i54h3lfhls65vfv6i40rqgbyj19c59s2ij1in7") (y #t)))

(define-public crate-flowync-1.8.0 (c (n "flowync") (v "1.8.0") (h "0cfqk35b0sn00wq1yzcxia87wj4g7y29gfhkgz90kpi6hm9lhz9v") (y #t)))

(define-public crate-flowync-1.8.1 (c (n "flowync") (v "1.8.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "093bfqdadaw36rxxx0f8gdica94i9c3vrwx894lar57mk2bkssyp") (y #t)))

(define-public crate-flowync-1.8.9 (c (n "flowync") (v "1.8.9") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1sn6q5j2nqp5xg4mzjaasha1851xbklqbhivxffgss10f8kyb6mf") (f (quote (("parking-lot" "parking_lot") ("default"))))))

(define-public crate-flowync-2.0.0 (c (n "flowync") (v "2.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "usync") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1l0j2h1jvhay30840qq39wm44sf9yj1j7flmnidj7hzhxrf92zhs") (f (quote (("use-usync" "usync") ("default")))) (y #t)))

(define-public crate-flowync-2.0.1 (c (n "flowync") (v "2.0.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "usync") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1akg8mm519pv3hxiyj9165yrsbk4fvclhvjagkvin1wcqbi3p4dc") (f (quote (("use-usync" "usync") ("default")))) (y #t)))

(define-public crate-flowync-2.0.2 (c (n "flowync") (v "2.0.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "usync") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0l88fnkj17i1hlz7l54bvgzdg4qxff2m1hxd57kk6zi33niyx3hm") (f (quote (("use-usync" "usync") ("default")))) (y #t)))

(define-public crate-flowync-2.0.5 (c (n "flowync") (v "2.0.5") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0fnfkrbpyyilfqvfpnysbla99p0kiiqwahdndfzqrs1vihir2i4q") (f (quote (("parking-lot" "parking_lot") ("default")))) (y #t)))

(define-public crate-flowync-2.0.6 (c (n "flowync") (v "2.0.6") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "05yyj8kza7mkxi7kwdxjxm18icm9jxihmj4fjkw14xgvc83vjnkk") (f (quote (("parking-lot" "parking_lot") ("default")))) (y #t)))

(define-public crate-flowync-2.0.7 (c (n "flowync") (v "2.0.7") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "18113ks0jwnljwv3adylh6rjfnh794g5vvvx6imq587lv5xz7mx3") (f (quote (("parking-lot" "parking_lot") ("default"))))))

(define-public crate-flowync-3.0.0 (c (n "flowync") (v "3.0.0") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0v530dij9fkrn2yzrfhgdlz5jh5hpcwd44jh1l7sdfccmp2blc9g") (f (quote (("parking-lot" "parking_lot") ("default"))))))

(define-public crate-flowync-4.0.0 (c (n "flowync") (v "4.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "06d8aizanxal3krpgcghq043nr3gnywmish5hd9i9z5fd7xhj0bi") (y #t)))

(define-public crate-flowync-4.0.1 (c (n "flowync") (v "4.0.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "16hmb7micam6i0mwy904hr65hkk4rkm7mjl6220wpn9l5g4wy3cv") (y #t)))

(define-public crate-flowync-4.0.2 (c (n "flowync") (v "4.0.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "12x2vka6v36zxxvddi48sn4jwcqh2zc2cgb4b9fg8v4x2d6q9qb6") (y #t)))

(define-public crate-flowync-4.6.0 (c (n "flowync") (v "4.6.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1pcjs6zdc7qnhi0zz89wq8m57k12lz2x23vbs9bkqv5cbizvncnh") (y #t)))

(define-public crate-flowync-4.6.1 (c (n "flowync") (v "4.6.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "143j74qs04zfkpzyw1gf2l0qjy2r7apfr0j15jjbr1r3wgbbxm9k") (y #t)))

(define-public crate-flowync-4.6.2 (c (n "flowync") (v "4.6.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "10s0f42z3y6sw9rfql6blmr9kvshhg61npv6xbj5gy6br8wkdvmq") (y #t)))

(define-public crate-flowync-4.6.3 (c (n "flowync") (v "4.6.3") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "09sqrc61d3q5c6n7dcdx9qmry30jfic7zfgfixl9svnfxhh49nim") (y #t)))

(define-public crate-flowync-4.8.0 (c (n "flowync") (v "4.8.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1i4dzhg302gddzm4492drhflxlfz61iv0afy8y507wzfwysfa1dx")))

(define-public crate-flowync-5.0.0 (c (n "flowync") (v "5.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "06hgda0iwi33xaxhf3ij6nsqji4rvsrnxc61nbhy70s7k4y7ds5z") (y #t)))

(define-public crate-flowync-5.0.1 (c (n "flowync") (v "5.0.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "06d7sh3k20rkavwvqrbsfgk32nz4hgmxznlqwriqgw5vzswikcml") (f (quote (("compact")))) (y #t)))

(define-public crate-flowync-5.1.0 (c (n "flowync") (v "5.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "08r2im77pdw5d9y766v7pp7c4s91f1l36z71ryiyaz6jn1k855i2") (f (quote (("compact"))))))

