(define-module (crates-io fl ow flow-cli) #:use-module (crates-io))

(define-public crate-flow-cli-0.1.0 (c (n "flow-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "07yjh2dwsgxz70kaa33mrggvvd6jcwv13c2y12d03w0ylzd6s1nr")))

(define-public crate-flow-cli-0.1.1 (c (n "flow-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0qj8f8m5dgcnsyh1bldl90j9v068vh0pq08hrlh6l7brpkpd3lsn")))

