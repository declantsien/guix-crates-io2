(define-module (crates-io fl ow flowsnet-platform-sdk) #:use-module (crates-io))

(define-public crate-flowsnet-platform-sdk-0.1.0 (c (n "flowsnet-platform-sdk") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "163kynpr1951vv6a8a6lpyh5s5ldw0p0333f9vk9wbk0r1kcxbqg")))

(define-public crate-flowsnet-platform-sdk-0.1.1 (c (n "flowsnet-platform-sdk") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0660cynrj5wvsfb6pdkhpjjkwflyyx9wz5w9hnasfzg7x7z8jl38")))

(define-public crate-flowsnet-platform-sdk-0.1.2 (c (n "flowsnet-platform-sdk") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n9vjyql4jhjbf4d1ys8zbyzmm1vzsghrwm4z9x7l4kxcj18ykrv")))

(define-public crate-flowsnet-platform-sdk-0.1.3 (c (n "flowsnet-platform-sdk") (v "0.1.3") (d (list (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1ggfn6qs93nrg06gkmxw8ybk4kg0b4nspz1g5i9fmz05q3sjvvv5")))

(define-public crate-flowsnet-platform-sdk-0.1.4 (c (n "flowsnet-platform-sdk") (v "0.1.4") (d (list (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1dqp3l26gipphi4bisq2bsmylj8crg89683ja6f5r2sh517151j3")))

(define-public crate-flowsnet-platform-sdk-0.1.5 (c (n "flowsnet-platform-sdk") (v "0.1.5") (d (list (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1jab47a71fnakrigckng1r1afp4cg2g2h42hlg3h3ra9i5a29cdb")))

(define-public crate-flowsnet-platform-sdk-0.1.6 (c (n "flowsnet-platform-sdk") (v "0.1.6") (d (list (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1sjv0pxsv60bqfc8vra3z96kchrsl0jvk1mcii8rrgaj4gizhpvv")))

