(define-module (crates-io fl ow flowrs) #:use-module (crates-io))

(define-public crate-flowrs-0.1.0 (c (n "flowrs") (v "0.1.0") (d (list (d (n "flowrs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "0wvyv21rbl0l0xq7sblql4vj7wyn4wyx3k29jn810laq5yvk4yy8")))

(define-public crate-flowrs-0.2.0 (c (n "flowrs") (v "0.2.0") (d (list (d (n "flowrs-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1lmm6jsgr1dy52dgm7fj0w38bvcc8z4g9h8vinpixjj72myd6158")))

