(define-module (crates-io fl ow flowcom) #:use-module (crates-io))

(define-public crate-flowcom-0.1.0 (c (n "flowcom") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "nucleo") (r "^0.4.0") (d #t) (k 0)))) (h "1b69x1dwl1klcq01dn2qkqm3sn9qpfvf307cx595nnqy1d8r6dbm")))

