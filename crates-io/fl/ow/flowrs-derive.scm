(define-module (crates-io fl ow flowrs-derive) #:use-module (crates-io))

(define-public crate-flowrs-derive-0.1.0 (c (n "flowrs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0qn4jmxm5b97a9ic24frfnfrav8avm4qrbr7kr48hvcxhxirqf1s")))

(define-public crate-flowrs-derive-0.2.0 (c (n "flowrs-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1643lhhc6np9a0f3zmar8y0n6p58mxbijpr8ap05lhn59cfrhbls")))

(define-public crate-flowrs-derive-0.3.0 (c (n "flowrs-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0q46b7dg0nwlys8r0198vy9wp65y312gbpdy3570vgdlarbfvi35")))

