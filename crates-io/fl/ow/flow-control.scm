(define-module (crates-io fl ow flow-control) #:use-module (crates-io))

(define-public crate-flow-control-0.0.0 (c (n "flow-control") (v "0.0.0") (h "093m2rrrlqb75cdlbd563y4whilv6xy9qvajd36g8mmqwg6dif5q")))

(define-public crate-flow-control-0.1.0 (c (n "flow-control") (v "0.1.0") (h "1zvypymfykjcyqr0j23in6c9csnxxqdn3mbvmgpmpfacmi4fcchw")))

(define-public crate-flow-control-0.1.1 (c (n "flow-control") (v "0.1.1") (h "1005mmss2b24rwndxbmzfps5nkwxnv2mh816y1k6989703zpv8x1")))

