(define-module (crates-io fl ow flow_impl) #:use-module (crates-io))

(define-public crate-flow_impl-0.8.0 (c (n "flow_impl") (v "0.8.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h6c7v180mgfnlljs55499nxni3nsvw1gm8j0nsi6bklg8h9bn1d")))

(define-public crate-flow_impl-0.8.1 (c (n "flow_impl") (v "0.8.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cnnj52xxwbmhr4nhs5h462s1jnrvyjmx4247fia5p20kqrif9x5")))

(define-public crate-flow_impl-0.8.2 (c (n "flow_impl") (v "0.8.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "029jkzkhr3a74v1mfj8m1hgjs9ywzcj6f7jzq1964dmf4pgvjs89")))

(define-public crate-flow_impl-0.8.3 (c (n "flow_impl") (v "0.8.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yq84f3mzybkkl366xc75xgy16iq5ms0cg5y2i1rkrjb032j1za3")))

(define-public crate-flow_impl-0.8.4 (c (n "flow_impl") (v "0.8.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vzlnglks4vadjwkckxdbq7w5dpwmg8r391f530zp105dk41ns98")))

(define-public crate-flow_impl-0.8.5 (c (n "flow_impl") (v "0.8.5") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g14mrdwqjxyl532nbg4cdscfhb0czbv1sdp8zbfs3dz9p8bx2a8")))

(define-public crate-flow_impl-0.8.9 (c (n "flow_impl") (v "0.8.9") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07fwv1ibawwh08fliwdyr4zk6xnhb0q8m3d3zs7zwb3av4vwqy53")))

(define-public crate-flow_impl-0.9.0 (c (n "flow_impl") (v "0.9.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19vg4bskh755mfk1iasz6yh90a4qhgykzv0arv549d1hvl80zrrg")))

(define-public crate-flow_impl-0.10.0 (c (n "flow_impl") (v "0.10.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f83n4bdlzmczpg4cf8zv9xdvbjibawnp2lkfc3hng4pj2bpn3sd")))

(define-public crate-flow_impl-0.11.0 (c (n "flow_impl") (v "0.11.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02dkbb9zz9b0jrvhi8lhd3nlsdiccqryp343nmx4pk42hn1bfq9q")))

(define-public crate-flow_impl-0.12.0 (c (n "flow_impl") (v "0.12.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l4b8npnjka7rdfs48dbwl3yjc8xnqm1fl1l0x55fdh7plkvcvz0")))

(define-public crate-flow_impl-0.14.0 (c (n "flow_impl") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bh7b692alqzcgd0rvyq5x9n48xg1w5fdnw4izks4xvm5zs0pypq")))

(define-public crate-flow_impl-0.21.0 (c (n "flow_impl") (v "0.21.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l634jjbmqsr36h9mmy7r0bq19wl2zm4cj8d2bz220ihpqjcxz3h")))

