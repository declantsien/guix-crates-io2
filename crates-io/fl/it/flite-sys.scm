(define-module (crates-io fl it flite-sys) #:use-module (crates-io))

(define-public crate-flite-sys-0.1.0 (c (n "flite-sys") (v "0.1.0") (h "1gnpbinik716ypgx5a17j3c0p8vmnsriqhcx8487nj6272dg11hp")))

(define-public crate-flite-sys-0.1.1 (c (n "flite-sys") (v "0.1.1") (h "1f5nji6w6rwsbdfq54xg8s768g5vmjjirav009lkm7g7j2kqwp36")))

(define-public crate-flite-sys-0.1.2 (c (n "flite-sys") (v "0.1.2") (h "12g9rmnvvkgk26w8f6f97799x0k5rvn0sbn322djpyzz4p0j6nmp")))

