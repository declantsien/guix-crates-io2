(define-module (crates-io fl it flit) #:use-module (crates-io))

(define-public crate-flit-0.1.0 (c (n "flit") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.10") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)))) (h "1s1ic3pmaywv6cz1xadj7hc6nh4gl5z489cgyn986vcijvgvsbrh")))

(define-public crate-flit-0.1.1 (c (n "flit") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)))) (h "1jhz664c9yawwnk7hdrwvwb18x519cqkxja0jiv1d8d7bhkqnrwy")))

(define-public crate-flit-0.1.2 (c (n "flit") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "06a6bbf7585152pmakr1cibqzggp2h4gzq1is3wjsqhpj7p5qf82")))

