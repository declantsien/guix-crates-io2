(define-module (crates-io fl it flite) #:use-module (crates-io))

(define-public crate-flite-0.0.1 (c (n "flite") (v "0.0.1") (d (list (d (n "flite-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mjz5dcp9hw5q2gqv0x7b2lz5alzgkxbhriyvhj0jvb7h56az8hl")))

(define-public crate-flite-0.0.2 (c (n "flite") (v "0.0.2") (d (list (d (n "flite-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1myb563xjg8f4gp9nkjdxxcw663zvc1953i6v3s444h6p8fj495k")))

(define-public crate-flite-0.0.3 (c (n "flite") (v "0.0.3") (d (list (d (n "flite-sys") (r "^0.1") (d #t) (k 0)))) (h "1d8vdxvjf4kli9iq8189a28gd49v3dfhrhqa7ra7w4mj5hihd0gr")))

