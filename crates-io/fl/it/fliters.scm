(define-module (crates-io fl it fliters) #:use-module (crates-io))

(define-public crate-fliters-0.1.0 (c (n "fliters") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("wav"))) (k 0)))) (h "093q1nd9g4lkvm2qasqzlpvgk3qb0m66y447c5xpmjf603fw592k")))

(define-public crate-fliters-0.1.1 (c (n "fliters") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("wav"))) (k 0)))) (h "1j6q52n65mlg569l9sxkw4g80yspl9bna8kljn3nv1xf9xjsiab7") (y #t)))

(define-public crate-fliters-0.1.2 (c (n "fliters") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("wav"))) (k 0)))) (h "0jnazyh1vkmkkr9hmpvm410xjjgr96x1mh37lam0mbrkgfrw6xgk") (y #t)))

(define-public crate-fliters-0.1.3 (c (n "fliters") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("wav"))) (k 0)))) (h "1nd1m8176xjiqbhs96syjr188chxfxl6q1qgmyznm1kiczpf61jf") (y #t)))

(define-public crate-fliters-0.1.4 (c (n "fliters") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("wav"))) (k 0)))) (h "19ap1xms7qg810l5n5mm86v0mi296yid9n7c8pal55az80vb06zl") (y #t)))

(define-public crate-fliters-0.1.5 (c (n "fliters") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("wav"))) (k 0)))) (h "07s0432v7mn3x2fc1lkbd5df86c9bmfivasnjdzz4wa80g6984ry") (y #t)))

(define-public crate-fliters-0.1.7 (c (n "fliters") (v "0.1.7") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("wav"))) (k 0)))) (h "1wh7iqfn854hbwihzxwz0c3xz932yyvf96amcniy9gzyfsqzn5v1")))

