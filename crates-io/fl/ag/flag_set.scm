(define-module (crates-io fl ag flag_set) #:use-module (crates-io))

(define-public crate-flag_set-0.1.0 (c (n "flag_set") (v "0.1.0") (h "1nx8hvfaw0x8p5d2sk3xxnnk1qyf8rnqslrglfbsg19rz2kp5mpd")))

(define-public crate-flag_set-0.1.1 (c (n "flag_set") (v "0.1.1") (h "1wpfjxh6q5rjkfhjkwxq88nwawgvdj2cn4sxq11v1jji39f17qm3")))

(define-public crate-flag_set-0.1.2 (c (n "flag_set") (v "0.1.2") (h "1d73m05mcazlrjssci9nv10bis264md3s8bppxhchv0jqwqbv8i3")))

(define-public crate-flag_set-0.1.3 (c (n "flag_set") (v "0.1.3") (h "0hfz7z9jqsrcn7p0q6f8lb5g8mw50cwy4ajywd4y2bkp2hhcypqi")))

(define-public crate-flag_set-0.1.4 (c (n "flag_set") (v "0.1.4") (h "15fhx52vgxzmf2i17xjj8i4b62ixh50lnnw47mshvqwlgpkp1z1b")))

(define-public crate-flag_set-0.1.5 (c (n "flag_set") (v "0.1.5") (h "0c5q5dg58fpcbqz4ag8j1dmscqsfgi558y5df6ap8jm4ycxk77gw")))

(define-public crate-flag_set-0.1.6 (c (n "flag_set") (v "0.1.6") (h "0k2353bwv2jnh32pnw9793k86r5dl2ys96fpfm7vlyxaxzdww8y8")))

