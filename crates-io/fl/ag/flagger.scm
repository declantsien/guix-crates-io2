(define-module (crates-io fl ag flagger) #:use-module (crates-io))

(define-public crate-flagger-0.1.0 (c (n "flagger") (v "0.1.0") (d (list (d (n "flagger-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "should") (r "^0.4.2") (d #t) (k 2)))) (h "0jwmadks2a17mgr5ssj0d61gjr15ya0lyclly1043mirmdfqyj5i")))

(define-public crate-flagger-0.2.0 (c (n "flagger") (v "0.2.0") (d (list (d (n "flagger-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "should") (r "^0.4.2") (d #t) (k 2)))) (h "1n19vrc36nkmbpfgadqz89nqsnv8zah1h0k9kqms16cyksxifi8m")))

(define-public crate-flagger-0.3.0 (c (n "flagger") (v "0.3.0") (d (list (d (n "flagger-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "should") (r "^0.4.2") (d #t) (k 2)))) (h "1099blf2v384rh1yzdvlc661iw4ik11zckmrvag8m5wjhk1aq70b")))

(define-public crate-flagger-0.4.0 (c (n "flagger") (v "0.4.0") (d (list (d (n "flagger-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "should") (r "^0.4.2") (d #t) (k 2)))) (h "00gf6hg1cgksn5l11hhwc57v65ad1985wclkx4v8wpd670y2iz6s")))

(define-public crate-flagger-0.5.0 (c (n "flagger") (v "0.5.0") (d (list (d (n "flagger-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "should") (r "^0.4.2") (d #t) (k 2)))) (h "06xn9sgl2s3s9cdyb2d7f1lkmww7fkfddg0f5sgyd70fzv4s9yh1")))

(define-public crate-flagger-0.6.0 (c (n "flagger") (v "0.6.0") (d (list (d (n "flagger-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "should") (r "^0.4.2") (d #t) (k 2)))) (h "0ryckjyfrkiv76v3davlhqmf4pj4g70n0k0ww4bipx3pbyyjh557")))

