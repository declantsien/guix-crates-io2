(define-module (crates-io fl ag flaggy-values) #:use-module (crates-io))

(define-public crate-flaggy-values-0.1.0 (c (n "flaggy-values") (v "0.1.0") (h "17nwzcdr7palvj0brf5zv36khnrw3crnyi1li4lgx95wmyzk5ykq")))

(define-public crate-flaggy-values-0.1.1 (c (n "flaggy-values") (v "0.1.1") (h "1r1iqdiscmf66qzf80hd7033y310zxknfdajfk54xpc5b1hxxvrk")))

(define-public crate-flaggy-values-0.2.0 (c (n "flaggy-values") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14gbhrhzw87vid7p6whsr99dv0pxc9ymbfsdkwz69ga323gv9pr9")))

(define-public crate-flaggy-values-0.2.1 (c (n "flaggy-values") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18f1c51rmlzsvvf0l61h7w4hwp83930kq5yimaw3ax35ilq63fjw")))

