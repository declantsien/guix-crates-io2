(define-module (crates-io fl ag flag_by_code) #:use-module (crates-io))

(define-public crate-flag_by_code-0.1.0 (c (n "flag_by_code") (v "0.1.0") (h "0g84jl2nwvkdmr6j740sf7xx25paxwyflis5jfk1cd98kccs0p56")))

(define-public crate-flag_by_code-0.1.1 (c (n "flag_by_code") (v "0.1.1") (h "1ibani0jh78kbdhigcybqzlxpi6y4l4iczapj7bj2pyapaabnxq3")))

