(define-module (crates-io fl ag flagtory) #:use-module (crates-io))

(define-public crate-flagtory-0.1.0 (c (n "flagtory") (v "0.1.0") (h "1m6nl3jzrlhm6hzk3ygmcxkwigwbc7agw6skb4imw1jlr8rm2fbn")))

(define-public crate-flagtory-0.2.0 (c (n "flagtory") (v "0.2.0") (h "14p2841w8jc1dc5gbg5kqvcm90ffh49773wzhzlfc2qs088f8m2y")))

(define-public crate-flagtory-0.2.1 (c (n "flagtory") (v "0.2.1") (h "1v5hm52hxjwsr217ndd9l87bgxagyy7pkc5bxr1s3ipdsqbi32am")))

