(define-module (crates-io fl ag flagset) #:use-module (crates-io))

(define-public crate-flagset-0.1.0 (c (n "flagset") (v "0.1.0") (h "16v4qj5mfql0833rjygijxxwv2y09l7rgbn4kmhxgw0nx0ywr9jq")))

(define-public crate-flagset-0.2.0 (c (n "flagset") (v "0.2.0") (h "07kmrhdd7w8mdz01adlqpl10l0dm9hvz2maa3nnh6fmlvdg6w38v")))

(define-public crate-flagset-0.3.0 (c (n "flagset") (v "0.3.0") (h "1ld60mp3nyz1fwxwkz5wv74cpmspfbf9nm441w1qw3qd5illj0w9")))

(define-public crate-flagset-0.4.0 (c (n "flagset") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 2)))) (h "1bgfqq1pr5ywrylnblna77542fq05qas5yllpr520adwj6lmq073")))

(define-public crate-flagset-0.4.1 (c (n "flagset") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 2)))) (h "07f6icw6dpqq6zd90c1czzd5hzqayc1xycbayrb68b28h6naz1k4")))

(define-public crate-flagset-0.4.2 (c (n "flagset") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 2)))) (h "1kw3xhkagcikisjjqdl1rn3hd9zjklf7ij9zk9c0827204z3j1qj")))

(define-public crate-flagset-0.4.3 (c (n "flagset") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 2)))) (h "16alqal1jyg1pibd72drdq2ax5cb1fs09f54ghpw043qg75579nd")))

(define-public crate-flagset-0.4.4 (c (n "flagset") (v "0.4.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 2)))) (h "0yb7rqf0im05n3gps32klzfg7zmavkdavngiwq9hh182h907wanm") (f (quote (("std")))) (r "1.65")))

(define-public crate-flagset-0.4.5 (c (n "flagset") (v "0.4.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 2)))) (h "18faiabgk7gh7fjhqxlh4c23zf6prshcyq7h2yyamyawx6jkmsyd") (f (quote (("std")))) (r "1.65")))

