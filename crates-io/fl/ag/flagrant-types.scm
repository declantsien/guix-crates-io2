(define-module (crates-io fl ag flagrant-types) #:use-module (crates-io))

(define-public crate-flagrant-types-0.0.2 (c (n "flagrant-types") (v "0.0.2") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_valid") (r "^0.20.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("sqlite" "macros" "runtime-tokio-rustls"))) (d #t) (k 0)))) (h "06z9dpbx4fj8qw27px3jmgswx2pw7wfny7apk80xsqpbqrymkfmb")))

