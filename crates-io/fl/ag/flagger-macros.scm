(define-module (crates-io fl ag flagger-macros) #:use-module (crates-io))

(define-public crate-flagger-macros-0.1.0 (c (n "flagger-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "078lq8qwshnsp01ws8nvi38lkwrng4r9kijqcsk551p4gc5p6rm2")))

(define-public crate-flagger-macros-0.2.0 (c (n "flagger-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1nsfjbsjj4wq55slbazbkcs4b4gg7jb57fw3d0s18a3iri8szcki")))

(define-public crate-flagger-macros-0.3.0 (c (n "flagger-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "14y8510z01x28ypkrfdvw2xrc7z5wg80m4ynmxnl6mghh7kbxy6f")))

(define-public crate-flagger-macros-0.4.0 (c (n "flagger-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1q9wbhbvmrac34av8yhy6sv3l3gjn3afxm7f2n2zgq03qashj5xv")))

(define-public crate-flagger-macros-0.5.0 (c (n "flagger-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0ss9c4ywh7xyrapjikf1l31ni9zkm57alns3qy1258cnw3cq9sf4")))

(define-public crate-flagger-macros-0.6.0 (c (n "flagger-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0jikdxklb5spijs9m2ibd3a3irajx479sgs73iwpkjw8m3kf1v6c")))

