(define-module (crates-io fl ag flaggy) #:use-module (crates-io))

(define-public crate-flaggy-0.1.0 (c (n "flaggy") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flaggy-codegen") (r "^0.1") (d #t) (k 0)) (d (n "flaggy-values") (r "^0.1") (d #t) (k 0)))) (h "1xmyddps9j5xr4b4d84hhjxl2nafs4axk094q3a6n5f7k4kqvbpf")))

(define-public crate-flaggy-0.1.1 (c (n "flaggy") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flaggy-codegen") (r "^0.1") (d #t) (k 0)) (d (n "flaggy-values") (r "^0.1") (d #t) (k 0)))) (h "1vdp0myb6170hz0asl3fx6s98kyqkbrms89b0raf2yyvgd73z516")))

(define-public crate-flaggy-0.2.0 (c (n "flaggy") (v "0.2.0") (d (list (d (n "flaggy-codegen") (r "^0.1") (d #t) (k 0)) (d (n "flaggy-values") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b3b25wv22ajl4m9yf9pgrh9vbfprbsx6hcaxw6snbd0p312gl08")))

(define-public crate-flaggy-0.2.1 (c (n "flaggy") (v "0.2.1") (d (list (d (n "flaggy-codegen") (r "^0.1") (d #t) (k 0)) (d (n "flaggy-values") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k6d279kx5bwclxzxvdy22lfqas0b851n110lryvhgvfijkbxsmm")))

(define-public crate-flaggy-0.2.2 (c (n "flaggy") (v "0.2.2") (d (list (d (n "flaggy-codegen") (r "^0.1") (d #t) (k 0)) (d (n "flaggy-values") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1x79y0i6inl1lg5qjvqzfz795lp50rq99nwvi5g4dsz5pi4xipdb")))

