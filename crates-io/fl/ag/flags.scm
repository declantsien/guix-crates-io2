(define-module (crates-io fl ag flags) #:use-module (crates-io))

(define-public crate-flags-0.0.0 (c (n "flags") (v "0.0.0") (h "0lydvnqfr9ridkbbf7xngrfw4x0y441lhw45yardbfg22ca6yf0i")))

(define-public crate-flags-0.1.0 (c (n "flags") (v "0.1.0") (h "1akzp2c3v7n1yfsvicl2mj3nxv0myig8l40scpd92s4i6agrw676") (f (quote (("example_generated") ("default" "example_generated"))))))

(define-public crate-flags-0.1.1 (c (n "flags") (v "0.1.1") (h "1ip1x9c6770zrfwq2d399gqgzdk6jspka0n4bfdk13qd1087gxz3") (f (quote (("example_generated") ("default" "example_generated"))))))

(define-public crate-flags-0.1.2 (c (n "flags") (v "0.1.2") (h "1770xsskyiyy670q897ddnncinlhj8z18vp8q9ad7m2kkj0ff2rx") (f (quote (("example_generated") ("default" "example_generated"))))))

(define-public crate-flags-0.1.3 (c (n "flags") (v "0.1.3") (h "05ygkp2ppnak32sg3p1y2xr1l1af79dmsyqm02a5mgnnw1sc9mxg") (f (quote (("example_generated") ("default"))))))

(define-public crate-flags-0.1.4 (c (n "flags") (v "0.1.4") (h "19z8prz0fcq6v7prdyv6fqb1ij8n77f9zwgx1slnjvvwybiwhw14") (f (quote (("example_generated") ("default"))))))

(define-public crate-flags-0.1.5 (c (n "flags") (v "0.1.5") (h "0h63fkg54w4zix1i4hhggk228kb4bmrhm74j3gmq54jmsm0lvm0f") (f (quote (("example_generated") ("default"))))))

