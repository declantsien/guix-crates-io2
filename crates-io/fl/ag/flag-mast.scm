(define-module (crates-io fl ag flag-mast) #:use-module (crates-io))

(define-public crate-flag-mast-0.1.0 (c (n "flag-mast") (v "0.1.0") (d (list (d (n "flag-mast-derive") (r "^0.1.0") (d #t) (k 0)))) (h "106inxds802cxha1v0ycw7y48a62jhjjmilybmhppzysfsyf5yw7")))

(define-public crate-flag-mast-0.1.1 (c (n "flag-mast") (v "0.1.1") (d (list (d (n "flag-mast-derive") (r "^0.1.1") (d #t) (k 0)))) (h "08azp69lzrzll9p5k7yzg6qg84rnddwlw2a2ffhi1g9lf1vpwikv")))

(define-public crate-flag-mast-0.1.2 (c (n "flag-mast") (v "0.1.2") (d (list (d (n "flag-mast-derive") (r "^0.1.2") (d #t) (k 0)))) (h "08xpvcx11pkgiz0xklssjgfiqw7qjz6cl5cgdhvlzpqxx2n33m1p")))

(define-public crate-flag-mast-0.2.0 (c (n "flag-mast") (v "0.2.0") (d (list (d (n "flag-mast-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0aw3q1xq9liis7w4i5zkkc9p3yj0lxrg7dhx3lgpwg1kdifgg591")))

(define-public crate-flag-mast-0.2.1 (c (n "flag-mast") (v "0.2.1") (d (list (d (n "flag-mast-derive") (r "^0.2.1") (d #t) (k 0)))) (h "1vzrmindw372gms837maxdk3r5bcy6chnn6a3w4s26mp5syc8djr")))

