(define-module (crates-io fl ag flaggy-codegen) #:use-module (crates-io))

(define-public crate-flaggy-codegen-0.1.0 (c (n "flaggy-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0n6psn2z387vv0j7jql8m4skjzl1563r81wciy1v69kp685qy8bv")))

(define-public crate-flaggy-codegen-0.1.1 (c (n "flaggy-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "109ya5rhhri2wa36bm6j9mni8gms3mcp0pk81rdhfhzqdscvdgnr")))

(define-public crate-flaggy-codegen-0.1.2 (c (n "flaggy-codegen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x9kwydalrjkdmv9zkg2zdl1rlj7ia450lg6knsg2i5zbx8c5b6a")))

