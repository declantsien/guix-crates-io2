(define-module (crates-io fl ag flag-mast-derive) #:use-module (crates-io))

(define-public crate-flag-mast-derive-0.1.0 (c (n "flag-mast-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1kiy2xp2zcx0dqs8j5fnb51six3agk23ycym4smn1a2in0j07m4p")))

(define-public crate-flag-mast-derive-0.1.1 (c (n "flag-mast-derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0w6iiwqw0b3xbry8qwra9iis1sgi6di8md4qagms9yirmcp7bv9l")))

(define-public crate-flag-mast-derive-0.1.2 (c (n "flag-mast-derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0j7z4isyl62v8x7f5g136n3fzw81s9733pp8cfhpsw13g7jpx5y7")))

(define-public crate-flag-mast-derive-0.2.0 (c (n "flag-mast-derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jbg89lbh98grr4w6yzrr5nqq5kv5fchy5w0pym445d4z4bh63zg")))

(define-public crate-flag-mast-derive-0.2.1 (c (n "flag-mast-derive") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0z4z4ja6gycbv2jmlqpnb8m0jqwam7a6lzbbcdfp9s9wdpqk9ipk")))

