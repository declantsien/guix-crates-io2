(define-module (crates-io fl ag flagrant-client) #:use-module (crates-io))

(define-public crate-flagrant-client-0.0.2 (c (n "flagrant-client") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "flagrant-types") (r "^0.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ahzdfbd3hqldlxpc13303l22mjgwhxl5vwfbwv8pxxnvsn25nqg")))

