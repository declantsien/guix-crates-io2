(define-module (crates-io fl ag flags-macro) #:use-module (crates-io))

(define-public crate-flags-macro-0.1.0 (c (n "flags-macro") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 2)) (d (n "enumflags") (r "^0.4.1") (d #t) (k 2)) (d (n "enumflags_derive") (r "^0.4.1") (d #t) (k 2)))) (h "1pqhb5hvm8y93j7nv0jfvzpplj9wa4035c8bdkpgxg5rlk9ak0nk")))

(define-public crate-flags-macro-0.1.1 (c (n "flags-macro") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 2)) (d (n "enumflags") (r "^0.4.1") (d #t) (k 2)) (d (n "enumflags_derive") (r "^0.4.1") (d #t) (k 2)))) (h "1layzw8na4k53h8pslbj2qg61lyqx98r8vv8y1j7x5kirz4m78g9")))

(define-public crate-flags-macro-0.1.2 (c (n "flags-macro") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 2)) (d (n "enumflags") (r "^0.4.1") (d #t) (k 2)) (d (n "enumflags_derive") (r "^0.4.1") (d #t) (k 2)))) (h "0bd392ik9ypnrd83fh384bjp8pcb184v0wfvjrcmzrcfqfl67ks8")))

(define-public crate-flags-macro-0.1.3 (c (n "flags-macro") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 2)) (d (n "enumflags") (r "^0.4.1") (d #t) (k 2)) (d (n "enumflags_derive") (r "^0.4.1") (d #t) (k 2)))) (h "048vf6kam6m4pjzw7pirk55fyayj8hckzwqk60kqpjiibmywvhbb")))

(define-public crate-flags-macro-0.1.4 (c (n "flags-macro") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 2)) (d (n "enumflags") (r "^0.4.1") (d #t) (k 2)) (d (n "enumflags_derive") (r "^0.4.1") (d #t) (k 2)))) (h "1m7f4m98mxmrwlw5cjd87v4ka974xqkxxvm4c6dqg8n64dkc5qrd")))

