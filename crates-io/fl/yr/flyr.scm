(define-module (crates-io fl yr flyr) #:use-module (crates-io))

(define-public crate-flyr-0.4.0 (c (n "flyr") (v "0.4.0") (d (list (d (n "binread") (r "^1.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.1") (f (quote ("png"))) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)))) (h "10fflwaickdw4gwv1b6fg24qsmkl910bvl7bq45zxawx3w1hziwc")))

(define-public crate-flyr-0.4.1 (c (n "flyr") (v "0.4.1") (d (list (d (n "binread") (r "^1.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.1") (f (quote ("png"))) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)))) (h "0cifdz7c741rb9yly8v72kwixl2wz3c8hsv04pmyd43w2nrqqm9v")))

(define-public crate-flyr-0.4.2 (c (n "flyr") (v "0.4.2") (d (list (d (n "binread") (r "^1.1.1") (d #t) (k 0)) (d (n "image") (r "^0.23.1") (f (quote ("png"))) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)))) (h "1q0d6r3f5yaj78g8ywfwrbcpl60zyba2wcf7pxa9j29jxdnnxiqw")))

(define-public crate-flyr-0.5.0 (c (n "flyr") (v "0.5.0") (d (list (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.1") (f (quote ("png" "jpeg"))) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)))) (h "1cp09f0938fdr65x69w1705mqjfwn7mrr48w3j9rdj7bjdxppg6x")))

