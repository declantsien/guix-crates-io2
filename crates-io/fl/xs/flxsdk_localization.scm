(define-module (crates-io fl xs flxsdk_localization) #:use-module (crates-io))

(define-public crate-flxsdk_localization-1.0.0 (c (n "flxsdk_localization") (v "1.0.0") (d (list (d (n "language-objects") (r "^1") (d #t) (k 0)) (d (n "message-locator") (r "^1") (d #t) (k 0)))) (h "14i53zx8yxn1fa9vkzyjh0vmxwxyrijzw15giad93sb78xlnac07") (y #t)))

