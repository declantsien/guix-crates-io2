(define-module (crates-io fl ca flcard) #:use-module (crates-io))

(define-public crate-flcard-1.0.1 (c (n "flcard") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19xb27imvvf326yvdascxl8bn1y18fdh5435gp4v2i7hwhsj0fgw")))

