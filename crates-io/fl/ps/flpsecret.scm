(define-module (crates-io fl ps flpsecret) #:use-module (crates-io))

(define-public crate-flpsecret-2.0.0 (c (n "flpsecret") (v "2.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0w15wrfpy1p75s4nxhq6ywzb25fp4mhyxad2d3n7am726m4llfvm")))

