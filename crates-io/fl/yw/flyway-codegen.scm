(define-module (crates-io fl yw flyway-codegen) #:use-module (crates-io))

(define-public crate-flyway-codegen-0.1.0 (c (n "flyway-codegen") (v "0.1.0") (d (list (d (n "flyway-sql-changelog") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "01wmfdg2bh6b1x691aas531wzgzwzwv7z39ip0h87ab9x7lxivix")))

(define-public crate-flyway-codegen-0.1.1 (c (n "flyway-codegen") (v "0.1.1") (d (list (d (n "flyway-sql-changelog") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0yv8jjkb8s0ncpx16kxxx07h61w940cb86m8i28c5rn69s7wsm12")))

(define-public crate-flyway-codegen-0.2.2 (c (n "flyway-codegen") (v "0.2.2") (d (list (d (n "flyway-sql-changelog") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "00956ca39ca7xz47f7qs5biz5rghi4crn8kiyk4m1b90qciwbn9r") (f (quote (("debug_mode"))))))

(define-public crate-flyway-codegen-0.3.0 (c (n "flyway-codegen") (v "0.3.0") (d (list (d (n "flyway-sql-changelog") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0q0iz8vavp73jyx95vx4qv8fj4lhkf5294q86cm6nrn71cwvrwqv") (f (quote (("debug_mode"))))))

(define-public crate-flyway-codegen-0.3.1 (c (n "flyway-codegen") (v "0.3.1") (d (list (d (n "flyway-sql-changelog") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1q3s6nry75kidgc4a4fxf0wwhrnnhz2dvz6f9v6vz2gx3b7k95l6") (f (quote (("debug_mode"))))))

