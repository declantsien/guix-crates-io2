(define-module (crates-io fl yw flywheel) #:use-module (crates-io))

(define-public crate-flywheel-0.1.0 (c (n "flywheel") (v "0.1.0") (d (list (d (n "piston_window") (r "^0.29.0") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0rq4wcz6m00dkdzr8z0q9fkidyjl8ix5gcn1qpdb3vx2cf2psv4w")))

(define-public crate-flywheel-0.1.1 (c (n "flywheel") (v "0.1.1") (d (list (d (n "piston_window") (r "^0.29.0") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "10m5fv5qc07sy991mglcv41aawmihgfcxg3yf0wjhc79s3xni7bw")))

