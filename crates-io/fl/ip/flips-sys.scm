(define-module (crates-io fl ip flips-sys) #:use-module (crates-io))

(define-public crate-flips-sys-0.1.0 (c (n "flips-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1wgaqn027np42yfyb9ldab051gg212af5gp3p3ga9dm1jbwpy1di")))

(define-public crate-flips-sys-0.2.0 (c (n "flips-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "crc32fast") (r "^1.2.0") (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1fir8bj1d9n11f08lfz4s8d7lapcz9pifwvf266a04x3r9fahhsn") (f (quote (("std" "crc32fast/std") ("default" "std"))))))

(define-public crate-flips-sys-0.2.1 (c (n "flips-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "crc32fast") (r "^1.2.0") (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0vphyfzskkjqb51j88fx4kgs9hbiycwgkyfhzyc86ihmsi4gfkm9") (f (quote (("std" "crc32fast/std") ("default" "std"))))))

