(define-module (crates-io fl ip flipperzero-alloc) #:use-module (crates-io))

(define-public crate-flipperzero-alloc-0.1.0 (c (n "flipperzero-alloc") (v "0.1.0") (d (list (d (n "flipperzero-sys") (r "^0.1.0") (d #t) (k 0)))) (h "14nq9rqsv519745aaiqmr51ipg1i9a94832248jfli6nslm025jd")))

(define-public crate-flipperzero-alloc-0.2.0 (c (n "flipperzero-alloc") (v "0.2.0") (d (list (d (n "flipperzero-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1bid92s6w00fqrvkmmfzy7s5gjgxqfgkjfazalyh81p100hiy6j0") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.3.0-alpha (c (n "flipperzero-alloc") (v "0.3.0-alpha") (d (list (d (n "flipperzero-sys") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1kwgnh94dgjgb1b11fqr96r79bnnk2vj5rhf32gw1y8hqpf43y7l") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.3.0 (c (n "flipperzero-alloc") (v "0.3.0") (d (list (d (n "flipperzero-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0zpgc6zxy12d0m6zaxlhfm3ybkwf0h8h5s05p7wyiai8pb4j24y9") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.3.1 (c (n "flipperzero-alloc") (v "0.3.1") (d (list (d (n "flipperzero-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0aj8m6kj1c1a3gmr1529w5i8y2kg3nl7zmqipxcfnjwng2xjh975") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.4.0 (c (n "flipperzero-alloc") (v "0.4.0") (d (list (d (n "flipperzero-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1h42506pk0fd3jh5n59cyjzp5qckrsg0gm4vv1wdd9h6n7wwrn5p") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.4.1 (c (n "flipperzero-alloc") (v "0.4.1") (d (list (d (n "flipperzero-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0rq0cx7pj7advvf2z4fzg846858yhdkdijwvk8r4m749d4wcf74h") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.5.0 (c (n "flipperzero-alloc") (v "0.5.0") (d (list (d (n "flipperzero-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0djk57kazbxcn6zvj803lzrld6csx5l63rsjxyaanp987j1iybvq") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.6.0 (c (n "flipperzero-alloc") (v "0.6.0") (d (list (d (n "flipperzero-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0rgi0yyhnfv51p0xah5rr9hc32f7lw2v3j0hlrl9ijr7pabyw7rn") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.7.0 (c (n "flipperzero-alloc") (v "0.7.0") (d (list (d (n "flipperzero-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1fx4s3jkv30b19m4sa1m17sj932h0d78bgq6klv8h0a6rwhdbzcz") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.7.1 (c (n "flipperzero-alloc") (v "0.7.1") (d (list (d (n "flipperzero-sys") (r "^0.7.1") (d #t) (k 0)))) (h "14vq2gg8znmz7p1rvg63agkkjpgzyccpi1dlblz9xfaj0nrp6dlh") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.7.2 (c (n "flipperzero-alloc") (v "0.7.2") (d (list (d (n "flipperzero-sys") (r "^0.7.2") (d #t) (k 0)))) (h "00413v446p25mgb21hlfh64vrrz8c314rbi3sv9wj7dzj7ma0rzv") (r "1.64.0")))

(define-public crate-flipperzero-alloc-0.8.0 (c (n "flipperzero-alloc") (v "0.8.0") (d (list (d (n "flipperzero-sys") (r "^0.8.0") (d #t) (k 0)))) (h "02wgr9q4r4qcfhmixgfy351d1pl620ghsqdbnjy5zc8m7szck3x2") (r "1.68.0")))

(define-public crate-flipperzero-alloc-0.9.0 (c (n "flipperzero-alloc") (v "0.9.0") (d (list (d (n "flipperzero-sys") (r "^0.9.0") (d #t) (k 0)))) (h "1s1i7b8qw3y2ifan796ikh2vay8y0fk9qxcil2zjk2x3f1pgmbxv") (r "1.70.0")))

(define-public crate-flipperzero-alloc-0.10.0 (c (n "flipperzero-alloc") (v "0.10.0") (d (list (d (n "flipperzero-sys") (r "^0.10.0") (d #t) (k 0)))) (h "1fa5n05s0b5k9mgv56icb5fj6r46qa822rdrcpcbdqqyn5ahqlyp") (r "1.70.0")))

(define-public crate-flipperzero-alloc-0.11.0 (c (n "flipperzero-alloc") (v "0.11.0") (d (list (d (n "flipperzero-sys") (r "^0.11.0") (d #t) (k 0)))) (h "04b1b651r7fz49ib1hipd5xycwd46mjr1bzhcqgnc58sg8pz4496") (r "1.70.0")))

