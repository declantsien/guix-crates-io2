(define-module (crates-io fl ip flipperbit) #:use-module (crates-io))

(define-public crate-flipperbit-0.1.0 (c (n "flipperbit") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "17wbnml62iwvrkg1apvg7m424wa94rgmvja9l5fcrmdj6ylchg3i")))

(define-public crate-flipperbit-0.1.1 (c (n "flipperbit") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "debugoff") (r "^0.1.1") (f (quote ("obfuscate"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "1x9hqf49d1986dh4a3kv107qzpwnhv4163wrrpwlcg27x14lcga7") (f (quote (("dbgoff" "debugoff"))))))

(define-public crate-flipperbit-0.1.2 (c (n "flipperbit") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "debugoff") (r "^0.2.1") (f (quote ("obfuscate" "syscallobf"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "1isgkf85j91gs63w85m1b85v6yi0dg83xq8i18kpqw8l4v592l73") (f (quote (("dbgoff" "debugoff"))))))

(define-public crate-flipperbit-0.1.3 (c (n "flipperbit") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "debugoff") (r "^0.2.2") (f (quote ("obfuscate" "syscallobf"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "14qw5p080m4paf7mrdcizjy0dhsm06ab455mbz5lla66ir5qvla7") (f (quote (("dbgoff" "debugoff"))))))

