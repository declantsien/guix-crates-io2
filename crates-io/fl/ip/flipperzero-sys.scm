(define-module (crates-io fl ip flipperzero-sys) #:use-module (crates-io))

(define-public crate-flipperzero-sys-0.1.0 (c (n "flipperzero-sys") (v "0.1.0") (h "1nwwbi8mcds3djkw4f9646012gc5m39m5jss8gndrflzqgvbpxak")))

(define-public crate-flipperzero-sys-0.2.0 (c (n "flipperzero-sys") (v "0.2.0") (h "19zz8r3cs1yn4s0l3gw4ynkq7kxzhc5ympaidkidfiq0rp7smkxb") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.3.0-alpha (c (n "flipperzero-sys") (v "0.3.0-alpha") (h "1w1ssrph683dasqyjpjasgi92pf930l1yw71kyk3dw49gw539ns4") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.3.0 (c (n "flipperzero-sys") (v "0.3.0") (h "02qvs8ifv3mg49q6ycq7ibp1ziymhlsyzh22dfhfs70b0r9n3jyz") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.3.1 (c (n "flipperzero-sys") (v "0.3.1") (h "07b3jnwva9h31n8qb59gm2fahld6d8a5vghhrkyp0kgcrq81w9aq") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.4.0 (c (n "flipperzero-sys") (v "0.4.0") (h "0ka93bqbfwn8z08h4mcwl34gml20pl7rc2vjir8gd00yp8p1jfzn") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.4.1 (c (n "flipperzero-sys") (v "0.4.1") (h "1qqvpair41f70za9y1bh5ivh7ph814nwvd7jvfg0k5hz4wjkkz1a") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.5.0 (c (n "flipperzero-sys") (v "0.5.0") (h "08v3s8731bmh4a9af6gxzrga85rxmp9lrpzhx0mp9g6m108j56p8") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.6.0 (c (n "flipperzero-sys") (v "0.6.0") (h "103qb5c9h0xgqi38wrgxnx8n9ih2hp6lq5l51ansy71azrkvi3mg") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.7.0 (c (n "flipperzero-sys") (v "0.7.0") (h "1f1814zx98x5vhnh3z0dnxzgql9wvad47hvkx3vh6p75prjcj4pw") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.7.1 (c (n "flipperzero-sys") (v "0.7.1") (h "13i01b18qc9lpgfynw0bm6rirc3xqz16xn4zgrzprmnlrb5b1xid") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.7.2 (c (n "flipperzero-sys") (v "0.7.2") (h "0fn3iy09q1h38d83wmhg17v8in8g692fh8jyxafzdia7g3jvh593") (r "1.64.0")))

(define-public crate-flipperzero-sys-0.8.0 (c (n "flipperzero-sys") (v "0.8.0") (d (list (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "1q5bs3cymkqjdqzidb2kndk1i8w980i11wihq5081g73zqk8f7yc") (r "1.68.0")))

(define-public crate-flipperzero-sys-0.9.0 (c (n "flipperzero-sys") (v "0.9.0") (d (list (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "0g2zknpgyfkwvz8vf3fpzh4hnchy8nqxz8msjv6m0987336svmk4") (r "1.70.0")))

(define-public crate-flipperzero-sys-0.10.0 (c (n "flipperzero-sys") (v "0.10.0") (d (list (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "1jqahg1ji8b0ynxvj32j7d21408ai4whmiis1lr8jji0f8dbz25b") (r "1.70.0")))

(define-public crate-flipperzero-sys-0.11.0 (c (n "flipperzero-sys") (v "0.11.0") (d (list (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "1q71azj9qwgqz9azaw3kmywyd4m3lv0936xbw12jgjbcyz50w2s5") (r "1.70.0")))

