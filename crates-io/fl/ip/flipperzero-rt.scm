(define-module (crates-io fl ip flipperzero-rt) #:use-module (crates-io))

(define-public crate-flipperzero-rt-0.3.0-alpha (c (n "flipperzero-rt") (v "0.3.0-alpha") (d (list (d (n "flipperzero-sys") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0jk0fh4p2cc75mm43y9rd2krmd3lkq4ff3mw8i08wc1dx4y48a18") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.3.0 (c (n "flipperzero-rt") (v "0.3.0") (d (list (d (n "flipperzero-sys") (r "^0.3.0") (d #t) (k 0)))) (h "15vvnkiahj8h71khwvlqwnxy45wv40cyb22kh4cdx580dfyzvmqv") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.3.1 (c (n "flipperzero-rt") (v "0.3.1") (d (list (d (n "flipperzero-sys") (r "^0.3.1") (d #t) (k 0)))) (h "00w1ljawnv6ylnkhkfp9ffmsqi1hkd6b6cl219v4rgzhhmhsm218") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.4.0 (c (n "flipperzero-rt") (v "0.4.0") (d (list (d (n "flipperzero-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1scsmmx99lqzbc5wlpl2ssjc8ksb0wl4mwixb0slh74ncyww6zws") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.4.1 (c (n "flipperzero-rt") (v "0.4.1") (d (list (d (n "flipperzero-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1rjr529i5npkjkqaa15ji2lbgqx1wrinffdvysiqvh41dbw8y7jd") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.5.0 (c (n "flipperzero-rt") (v "0.5.0") (d (list (d (n "flipperzero-sys") (r "^0.5.0") (d #t) (k 0)))) (h "01yl0z1nsyqm0jayq2qbiccwxsdw5fb9ymc6h2dd639lqrzlqvk9") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.6.0 (c (n "flipperzero-rt") (v "0.6.0") (d (list (d (n "flipperzero-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0421ybvnlvdagkrn917f93zx7hikw7wzz9f1964333bdf88v6lra") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.7.0 (c (n "flipperzero-rt") (v "0.7.0") (d (list (d (n "flipperzero-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0q87cfn2c384jny0i4n2sxf425h4y5p1bb8nysyaqrmbkqhs5gv0") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.7.1 (c (n "flipperzero-rt") (v "0.7.1") (d (list (d (n "flipperzero-sys") (r "^0.7.1") (d #t) (k 0)))) (h "0r9p0ysprh6sg0fyhkhhbd3agvkrbqnjzjd03ck9xi7qxkajfssk") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.7.2 (c (n "flipperzero-rt") (v "0.7.2") (d (list (d (n "flipperzero-sys") (r "^0.7.2") (d #t) (k 0)))) (h "0lcmx1nvzw17hramc6aqirkr8z0wbya4jpbs1y89n72mgzavdj34") (r "1.64.0")))

(define-public crate-flipperzero-rt-0.8.0 (c (n "flipperzero-rt") (v "0.8.0") (d (list (d (n "flipperzero-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0ilijd1z178jbasgrd9vcnb9flrxkry0a0p7mcmbm6jrlfzxijvw") (r "1.68.0")))

(define-public crate-flipperzero-rt-0.9.0 (c (n "flipperzero-rt") (v "0.9.0") (d (list (d (n "flipperzero-sys") (r "^0.9.0") (d #t) (k 0)))) (h "1wsb75xh9h6pqqyl4ac6x8wlal5argkcp5kx798nfwy1bz9d90zx") (r "1.70.0")))

(define-public crate-flipperzero-rt-0.10.0 (c (n "flipperzero-rt") (v "0.10.0") (d (list (d (n "flipperzero-sys") (r "^0.10.0") (d #t) (k 0)))) (h "07v87qpbsrc4n9zm3zfn27hwnlny9grwh1hpd7yhmx252f81fr78") (r "1.70.0")))

(define-public crate-flipperzero-rt-0.11.0 (c (n "flipperzero-rt") (v "0.11.0") (d (list (d (n "flipperzero-sys") (r "^0.11.0") (d #t) (k 0)))) (h "0569g1vsw4cyf0izxy6dah8i1ad3pprwwnl63w8p4j524yvyf5v9") (r "1.70.0")))

