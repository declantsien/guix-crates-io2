(define-module (crates-io fl ip flipdot-serial) #:use-module (crates-io))

(define-public crate-flipdot-serial-0.6.0 (c (n "flipdot-serial") (v "0.6.0") (d (list (d (n "flipdot-core") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "0p2wmfaqd13ivzm9qhca46sghbdix769xgbwdn2jgky2f7d232aq")))

(define-public crate-flipdot-serial-0.7.0 (c (n "flipdot-serial") (v "0.7.0") (d (list (d (n "flipdot-core") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "10ms9kdnrq8w449r7a3gqbkp5ymxggklj3l3lks27xw7vvyxy619")))

(define-public crate-flipdot-serial-0.7.1 (c (n "flipdot-serial") (v "0.7.1") (d (list (d (n "flipdot-core") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "05pz6acc2c0wxkr5684dqfwclzmdqaayhkdmh05q3a9l6ww0p5vz")))

