(define-module (crates-io fl ip flip-ui) #:use-module (crates-io))

(define-public crate-flip-ui-0.1.0 (c (n "flip-ui") (v "0.1.0") (d (list (d (n "flip-ui-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "flipperzero") (r "^0.11.0") (d #t) (k 0)) (d (n "flipperzero_rt") (r "^0.11.0") (d #t) (k 2) (p "flipperzero-rt")) (d (n "flipperzero_test") (r "^0.11.0") (d #t) (k 2) (p "flipperzero-test")))) (h "1nn7dfaw5mjc0q10hvwra5gn8dlpzjzvl6kq9r92iarzdsyc8dx0") (r "1.70.0")))

(define-public crate-flip-ui-0.1.1 (c (n "flip-ui") (v "0.1.1") (d (list (d (n "flip-ui-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "flipperzero") (r "^0.11.0") (d #t) (k 0)) (d (n "flipperzero_rt") (r "^0.11.0") (d #t) (k 2) (p "flipperzero-rt")) (d (n "flipperzero_test") (r "^0.11.0") (d #t) (k 2) (p "flipperzero-test")))) (h "0jm2i9ym2mlyhpfm7752qnlysj72azvd6x37iqfnw0w85zn7nv4q") (r "1.70.0")))

(define-public crate-flip-ui-0.1.2 (c (n "flip-ui") (v "0.1.2") (d (list (d (n "flip-ui-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "flipperzero") (r "^0.11.0") (d #t) (k 0)) (d (n "flipperzero_rt") (r "^0.11.0") (d #t) (k 2) (p "flipperzero-rt")) (d (n "flipperzero_test") (r "^0.11.0") (d #t) (k 2) (p "flipperzero-test")))) (h "0has95qd0h3wbk11i50a6m8wmw3caa137mp6hc3j5lg38gszjdv7") (r "1.70.0")))

