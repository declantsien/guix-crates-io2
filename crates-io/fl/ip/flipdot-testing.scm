(define-module (crates-io fl ip flipdot-testing) #:use-module (crates-io))

(define-public crate-flipdot-testing-0.6.0 (c (n "flipdot-testing") (v "0.6.0") (d (list (d (n "flipdot-core") (r "^0.6.0") (d #t) (k 0)) (d (n "flipdot-serial") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "14mawfam4ycwlss4hsnqll9gd4aps79vflzmdswjyfd80rkvk9kw")))

(define-public crate-flipdot-testing-0.7.0 (c (n "flipdot-testing") (v "0.7.0") (d (list (d (n "flipdot-core") (r "^0.7.0") (d #t) (k 0)) (d (n "flipdot-serial") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0p2ylm4r2j1m3w17l4z5yv3ygnlnlqi4kgs0qch79nm7xvibc8cq")))

(define-public crate-flipdot-testing-0.7.1 (c (n "flipdot-testing") (v "0.7.1") (d (list (d (n "flipdot-core") (r "^0.7.1") (d #t) (k 0)) (d (n "flipdot-serial") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "01b0vzy58k9fvnz8yah7477v8523cnscx94phsrm5ma533c03j1f")))

