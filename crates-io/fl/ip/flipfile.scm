(define-module (crates-io fl ip flipfile) #:use-module (crates-io))

(define-public crate-flipfile-0.1.0 (c (n "flipfile") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1knvz4nnvwiqln2v4c7m48chsi1y685xs0p02762kr1bcn3qmwgd")))

(define-public crate-flipfile-0.2.0 (c (n "flipfile") (v "0.2.0") (d (list (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0z9c225p79vf2866cj5zj0sqg5qsc1q9yb36lj6rm92694bf0d2p")))

(define-public crate-flipfile-0.3.0 (c (n "flipfile") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "16px4fm63nfqc261rsg4cdhl4ims4cz930grwprnhw2m0ib9cvyj")))

(define-public crate-flipfile-0.4.0 (c (n "flipfile") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "13pilv4xqniv2bwl09lc808harg7bk4wzq9yff0d6ji1dfcvz193")))

(define-public crate-flipfile-0.5.0 (c (n "flipfile") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1dbkzxai8fznbsdfw8zr0j2myxh001ramynn3dxxca26s9h3zky1")))

(define-public crate-flipfile-0.6.0 (c (n "flipfile") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "01h2vc8iamcwr9p6qn3358pi57b4284mawyziirdv96h8vj29z7x")))

(define-public crate-flipfile-0.7.0 (c (n "flipfile") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1xpavjrcs0v8hl06hbb8cz7js61idaz0kklpjk7i7431v5qldiyz")))

(define-public crate-flipfile-0.7.1 (c (n "flipfile") (v "0.7.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0l1ialscck5ca3fpmnbjzx6cf8fsba56zll58bxizbrzrns1ggcp")))

(define-public crate-flipfile-0.7.2 (c (n "flipfile") (v "0.7.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1f9c3wbshqm7qm5mdi80ykhn5r49r9flmwxk8xdf7jzfxyxcd94i")))

