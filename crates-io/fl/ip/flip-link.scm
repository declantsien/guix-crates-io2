(define-module (crates-io fl ip flip-link) #:use-module (crates-io))

(define-public crate-flip-link-0.1.0 (c (n "flip-link") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "object") (r "^0.21.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0b6fwrnplinwkl01bjy6hkb2k62jf0588amb53wp6wgng17m7bvq")))

(define-public crate-flip-link-0.1.1 (c (n "flip-link") (v "0.1.1") (d (list (d (n "anyhow") (r ">=1.0.32, <2.0.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.7.1, <0.8.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "object") (r ">=0.21.1, <0.22.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 0)))) (h "13pc2jyxxvky2bqbfxjppd25dyyajkhx2a22ffv9paclhmvfxqsj")))

(define-public crate-flip-link-0.1.2 (c (n "flip-link") (v "0.1.2") (d (list (d (n "anyhow") (r ">=1.0.32, <2.0.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.7.1, <0.8.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "object") (r ">=0.21.1, <0.22.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 0)))) (h "04fyxk4338qlc2bk9m896zbwl3y8wqc1gbjyal8saywnz1b4zjdw")))

(define-public crate-flip-link-0.1.3 (c (n "flip-link") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "object") (r "^0.23.0") (f (quote ("read_core" "elf" "std"))) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0hky08dwn7iwldh0jqacv4yx8vqzik7mg3jj466jas6cw58lpjm4")))

(define-public crate-flip-link-0.1.4 (c (n "flip-link") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "object") (r "^0.24.0") (f (quote ("read_core" "elf" "std"))) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "062p5341r75x2smfdw95rbms8n5kxjipj38isnzyvd9x2g6q3p4f")))

(define-public crate-flip-link-0.1.5 (c (n "flip-link") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "object") (r "^0.26") (f (quote ("read_core" "elf" "std"))) (k 0)) (d (n "rstest") (r "^0.11") (d #t) (k 2)))) (h "1z8iqgi15kl6nchhjbpfp4lf32cr6ksa6mixjbq467xzgqrz8ks4")))

(define-public crate-flip-link-0.1.6 (c (n "flip-link") (v "0.1.6") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "object") (r "^0.26") (f (quote ("read_core" "elf" "std"))) (k 0)) (d (n "rstest") (r "^0.11") (d #t) (k 2)))) (h "0k7g3cbqpdmwigw9xb0azivmcidy7zfb3a15km1grykcw037q103")))

(define-public crate-flip-link-0.1.7 (c (n "flip-link") (v "0.1.7") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "object") (r "^0.31") (f (quote ("read_core" "elf" "std"))) (k 0)) (d (n "rstest") (r "^0.18") (k 2)))) (h "1dclcbjym8cb3x1brvlkb9qlj50claayqa1pbmmss25q9karavv1")))

(define-public crate-flip-link-0.1.8 (c (n "flip-link") (v "0.1.8") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "object") (r "^0.33") (f (quote ("read_core" "elf" "std"))) (k 0)) (d (n "rstest") (r "^0.18") (k 2)))) (h "0w7n2s4xx84784lhrvzj8yrb00phidgp8f1jwxcznahx1in2vgmy")))

