(define-module (crates-io fl ip flipperzero-test-macros) #:use-module (crates-io))

(define-public crate-flipperzero-test-macros-0.1.0 (c (n "flipperzero-test-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0np0d0qgys1g2gllv6z6xq2q09pj7bn5hvx8i5rz8v69ilyfnxb9") (r "1.68.0")))

(define-public crate-flipperzero-test-macros-0.9.0 (c (n "flipperzero-test-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16kzh9rjf6r51dfassgn8rmvbl1l27537xxxz9fh7zw6lw70qdjv") (r "1.70.0")))

(define-public crate-flipperzero-test-macros-0.10.0 (c (n "flipperzero-test-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0scq5i14zlc5x3lzidfmkwz24mmwnf6b664gpg0scsh4arfkk4rw") (r "1.70.0")))

(define-public crate-flipperzero-test-macros-0.11.0 (c (n "flipperzero-test-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1szm2nag02ic9l7mfhjb8k5cl6qqs66i7s9rn7zc2y8qkv0cc3pg") (r "1.70.0")))

