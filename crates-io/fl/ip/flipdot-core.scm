(define-module (crates-io fl ip flipdot-core) #:use-module (crates-io))

(define-public crate-flipdot-core-0.6.0 (c (n "flipdot-core") (v "0.6.0") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "1slaz25cx6xcsppw5xvz95nph7bijv4mdavwfy0s07l4f8fwicx4")))

(define-public crate-flipdot-core-0.7.0 (c (n "flipdot-core") (v "0.7.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "177lw0zlwkz0vkj2qaz621f6f39k3nd6hxsmviqnj9bs31cmgw8b")))

(define-public crate-flipdot-core-0.7.1 (c (n "flipdot-core") (v "0.7.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "03cdd90vfiqzfwbsa96ryrg46g0k11mppja5zqphhhp5wja72bns")))

