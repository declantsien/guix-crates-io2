(define-module (crates-io fl ip flipper0-build-cfg) #:use-module (crates-io))

(define-public crate-flipper0-build-cfg-0.1.0 (c (n "flipper0-build-cfg") (v "0.1.0") (h "10b9sfghnxlxza11dshh5hfkwa3n7xi7vg8i9igyp0mjr0jkqf8n")))

(define-public crate-flipper0-build-cfg-0.1.1 (c (n "flipper0-build-cfg") (v "0.1.1") (h "0g4i1gn4iy7b0yiq32jjybh69p7q0rpkfg5rsa1ribws87shiycg")))

(define-public crate-flipper0-build-cfg-0.1.2 (c (n "flipper0-build-cfg") (v "0.1.2") (h "1v44jymldv7y0zd9gm9lg5lmsq43gq7z8im3nkdg985i99v6k7fk")))

(define-public crate-flipper0-build-cfg-0.1.3 (c (n "flipper0-build-cfg") (v "0.1.3") (h "1n1wnhlbzpnk3v0r61dd9g86sd0p2xm62yi40rsmp2fc851d8g40")))

