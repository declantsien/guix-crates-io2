(define-module (crates-io fl ip flip) #:use-module (crates-io))

(define-public crate-flip-0.1.0 (c (n "flip") (v "0.1.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "06k34gv6csii1ryvj7ppcabdk8ydbvnjbkyvdc50gf69a1rhp1cd")))

