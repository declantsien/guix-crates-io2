(define-module (crates-io fl ip fliplru) #:use-module (crates-io))

(define-public crate-fliplru-0.1.0 (c (n "fliplru") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "polonius-the-crab") (r "^0.3.1") (d #t) (k 0)))) (h "17dzfn3c5pflyikvcbvjbxnazknkkzacgarh6137z7z36q83vypr")))

(define-public crate-fliplru-0.1.1 (c (n "fliplru") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "polonius-the-crab") (r "^0.3.1") (d #t) (k 0)))) (h "12668k7lx819mciz649fvyd77xy0ci72rhx6iij7np8czm2x4h6s")))

(define-public crate-fliplru-0.1.2 (c (n "fliplru") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "polonius-the-crab") (r "^0.3.1") (d #t) (k 0)))) (h "1c36j3bj6fwd756yg24563lzwm97m9pf28iyr4favz44bwy6vngx")))

(define-public crate-fliplru-0.1.3 (c (n "fliplru") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "polonius-the-crab") (r "^0.3.1") (d #t) (k 0)))) (h "1by2z9b2mgp6c63rsqbrsnjhqpd4hi58a7gsvapaw51glacs2p00")))

(define-public crate-fliplru-0.1.4 (c (n "fliplru") (v "0.1.4") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "polonius-the-crab") (r "^0.3.1") (d #t) (k 0)))) (h "14m0r9rxclg72ilz95dklpsv68352z9hbsd2v5ygspknk8rza7lc")))

(define-public crate-fliplru-0.1.5 (c (n "fliplru") (v "0.1.5") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "polonius-the-crab") (r "^0.3.1") (d #t) (k 0)))) (h "07s0bsi54k6dr0z793jcdlda0j3nlz3byy1chmijcd07nvs27wxg")))

(define-public crate-fliplru-0.1.6 (c (n "fliplru") (v "0.1.6") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "polonius-the-crab") (r "^0.3.1") (d #t) (k 0)))) (h "0xsw0hszgck0qdvasb12l4gyph24mzr9hgslw4syf98gkyah3qcs")))

