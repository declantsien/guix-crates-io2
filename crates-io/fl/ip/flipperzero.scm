(define-module (crates-io fl ip flipperzero) #:use-module (crates-io))

(define-public crate-flipperzero-0.1.0 (c (n "flipperzero") (v "0.1.0") (d (list (d (n "flipperzero-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0b4bl7f6nxp9z2qnzcla2zvfpvibj22n61p4acd4p15kmmfn7ll2")))

(define-public crate-flipperzero-0.2.0 (c (n "flipperzero") (v "0.2.0") (d (list (d (n "flipperzero-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0pm5z1pr9w7g47hbgplg1c2isllggyhiai8mgvby6ybmjda1ibg5") (r "1.64.0")))

(define-public crate-flipperzero-0.3.0-alpha (c (n "flipperzero") (v "0.3.0-alpha") (d (list (d (n "flipperzero-sys") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "1ck8f86bzn6ab0bsmzqc7xw51il5b9kzkk4wn8dcbkdpb413gznd") (r "1.64.0")))

(define-public crate-flipperzero-0.3.0 (c (n "flipperzero") (v "0.3.0") (d (list (d (n "flipperzero-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0v7s1jcns74aihd1lm4jyfwyi1wccglclbq9d1aivdhy223x5kdv") (r "1.64.0")))

(define-public crate-flipperzero-0.3.1 (c (n "flipperzero") (v "0.3.1") (d (list (d (n "flipperzero-rt") (r "^0.3.1") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0y8zqrcs580njblnxc42ddrgdk118kn9pfdcnwsqfvkq22clsq1k") (r "1.64.0")))

(define-public crate-flipperzero-0.4.0 (c (n "flipperzero") (v "0.4.0") (d (list (d (n "flipperzero-rt") (r "^0.4.0") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0ykbxfyacs4sfs85wqxrifj48q7k2bz8n9s5y2j9ljp2p1jbgvxx") (r "1.64.0")))

(define-public crate-flipperzero-0.4.1 (c (n "flipperzero") (v "0.4.1") (d (list (d (n "flipperzero-rt") (r "^0.4.1") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1xnjwc1w0qqx20zn9srjpps6kiqf68kc1d2prc03cncc70x45b43") (r "1.64.0")))

(define-public crate-flipperzero-0.5.0 (c (n "flipperzero") (v "0.5.0") (d (list (d (n "flipperzero-rt") (r "^0.5.0") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1s1f28fa8sassmgp2ji1x9frj8gsaa9qsjcr2n7vikc231ra189l") (r "1.64.0")))

(define-public crate-flipperzero-0.6.0 (c (n "flipperzero") (v "0.6.0") (d (list (d (n "flipperzero-rt") (r "^0.6.0") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.6.0") (d #t) (k 0)))) (h "135p7h3kz4w4mbxdmh7da0zd7l2gfrclnmp47hhg10ni6nh2wsvl") (f (quote (("alloc")))) (r "1.64.0")))

(define-public crate-flipperzero-0.7.0 (c (n "flipperzero") (v "0.7.0") (d (list (d (n "flipperzero-rt") (r "^0.7.0") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.7.0") (d #t) (k 0)))) (h "18wzakamfkw9701jb6myvd85fkxigms6rl3i91hxwxj9jlnvbpi4") (f (quote (("alloc")))) (r "1.64.0")))

(define-public crate-flipperzero-0.7.1 (c (n "flipperzero") (v "0.7.1") (d (list (d (n "flipperzero-rt") (r "^0.7.1") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.7.1") (d #t) (k 0)))) (h "169dkfa4y8j2g6sdj861xyvd9hi2k1bpzl2xiki27if6j83bf72z") (f (quote (("alloc")))) (r "1.64.0")))

(define-public crate-flipperzero-0.7.2 (c (n "flipperzero") (v "0.7.2") (d (list (d (n "flipperzero-rt") (r "^0.7.2") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.7.2") (d #t) (k 0)))) (h "1lrjmf13d4rrmq320sqi7yb2q1q47yw4mycyi4y30a950xii0rm3") (f (quote (("alloc")))) (r "1.64.0")))

(define-public crate-flipperzero-0.8.0 (c (n "flipperzero") (v "0.8.0") (d (list (d (n "flipperzero-alloc") (r "^0.8.0") (d #t) (k 2)) (d (n "flipperzero-rt") (r "^0.8.0") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "1hz7phi9d12xi0vwgz5yry64lpqncvy3gklzldjkbp3rivl06b8w") (f (quote (("alloc")))) (r "1.68.0")))

(define-public crate-flipperzero-0.9.0 (c (n "flipperzero") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1") (k 2)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "flipperzero-alloc") (r "^0.9.0") (d #t) (k 2)) (d (n "flipperzero-rt") (r "^0.9.0") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "flipperzero-test") (r "^0.9.0") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "1hd467q2136p7cn4zr9q82zrwgzrwgsfb6y22hh2fg9kh1jbzykv") (f (quote (("alloc")))) (r "1.70.0")))

(define-public crate-flipperzero-0.10.0 (c (n "flipperzero") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1") (k 2)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "flipperzero-alloc") (r "^0.10.0") (d #t) (k 2)) (d (n "flipperzero-rt") (r "^0.10.0") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "flipperzero-test") (r "^0.10.0") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "0jc7nas5g1yd0yapd6f0pnbyd2w2g1492plslxss4x3p6v1xd01k") (f (quote (("alloc")))) (r "1.70.0")))

(define-public crate-flipperzero-0.11.0 (c (n "flipperzero") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1") (k 2)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "flipperzero-alloc") (r "^0.11.0") (d #t) (k 2)) (d (n "flipperzero-rt") (r "^0.11.0") (d #t) (k 2)) (d (n "flipperzero-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "flipperzero-test") (r "^0.11.0") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "159by9nfpdwx4ix3w2p33gry8vbzs393i8vmn6wsij03ykrnn5f4") (f (quote (("alloc")))) (r "1.70.0")))

