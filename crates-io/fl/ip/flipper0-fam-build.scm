(define-module (crates-io fl ip flipper0-fam-build) #:use-module (crates-io))

(define-public crate-flipper0-fam-build-0.1.0 (c (n "flipper0-fam-build") (v "0.1.0") (d (list (d (n "crate-metadata") (r "^0.1") (d #t) (k 0)) (d (n "fam") (r "^0.1") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ryladpnr593y7qjx5rxd8qh0wdsbwv0pjp0ixi7whn12bx0lb79")))

(define-public crate-flipper0-fam-build-0.1.1 (c (n "flipper0-fam-build") (v "0.1.1") (d (list (d (n "crate-metadata") (r "^0.1") (d #t) (k 0)) (d (n "fam") (r "^0.1.1") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xdpmdkb2bpw00xd43dh04q4biqplqcfwgxbx6lg56syyx0r1rii")))

(define-public crate-flipper0-fam-build-0.1.2 (c (n "flipper0-fam-build") (v "0.1.2") (d (list (d (n "crate-metadata") (r "^0.1") (d #t) (k 0)) (d (n "fam") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "0xl1w5d4xqy4xp9w5pg3mz6bcnjj69fsmp92pjma0z8z3fhnlqc9")))

(define-public crate-flipper0-fam-build-0.1.3 (c (n "flipper0-fam-build") (v "0.1.3") (d (list (d (n "crate-metadata") (r "^0.1") (d #t) (k 0)) (d (n "fam") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "0syr7c4nsagilq2px8flarj1xfp6gmv4qf5zmap0kihqdk5z2w5k")))

(define-public crate-flipper0-fam-build-0.1.4 (c (n "flipper0-fam-build") (v "0.1.4") (d (list (d (n "crate-metadata") (r "^0.1") (d #t) (k 0)) (d (n "fam") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "1b5wifgm2kjkj0gj2h23nmb68jc2crp3m00iy8jh5ldjyz7i1iwi") (f (quote (("optional_entry_point") ("default"))))))

(define-public crate-flipper0-fam-build-0.1.5 (c (n "flipper0-fam-build") (v "0.1.5") (d (list (d (n "crate-metadata") (r "^0.1") (d #t) (k 0)) (d (n "fam") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "07s0wcq4f00ljh2kmm9cgbqcqs04v2c9qpfsz8jg14ahga88ww4l") (f (quote (("optional_entry_point") ("default"))))))

(define-public crate-flipper0-fam-build-0.1.6 (c (n "flipper0-fam-build") (v "0.1.6") (d (list (d (n "crate-metadata") (r "^0.1") (d #t) (k 0)) (d (n "fam") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "0pfhgw8xj5f88y094r2lyw9krnghj6xdrq5y09km5r26na37907g") (f (quote (("optional_entry_point") ("default"))))))

