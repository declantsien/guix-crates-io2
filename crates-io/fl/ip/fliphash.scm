(define-module (crates-io fl ip fliphash) #:use-module (crates-io))

(define-public crate-fliphash-0.1.0 (c (n "fliphash") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 2)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 2)) (d (n "statrs") (r "^0.16.0") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.8") (f (quote ("const_xxh3" "xxh3"))) (o #t) (k 0)))) (h "122b8kpa8560a14d678hrhpjn4v6ns0dfab92arywlzi0vpz3i9m") (f (quote (("xxh3" "xxhash-rust"))))))

