(define-module (crates-io fl ip flips) #:use-module (crates-io))

(define-public crate-flips-0.1.0 (c (n "flips") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "flips-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1fd6rfn9i8krxi1il5cnb326jqvc9mhbsf8n8sl6kafm2n6hph5n") (f (quote (("std" "err-derive") ("default" "std") ("_doc" "std"))))))

(define-public crate-flips-0.2.0 (c (n "flips") (v "0.2.0") (d (list (d (n "err-derive") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "flips-sys") (r "^0.2.0") (k 0)))) (h "13yqzfgmha10jpxqgmb0bzz5b0w0qf2fd7jmj374l7253jyid0aw") (f (quote (("std" "err-derive" "flips-sys/std") ("default" "std") ("_doc" "std"))))))

(define-public crate-flips-0.2.1 (c (n "flips") (v "0.2.1") (d (list (d (n "err-derive") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "flips-sys") (r "^0.2.1") (k 0)))) (h "15qcj6v4i00xql5ld626ssghnb61j28gl4naick89wz78pxnvi26") (f (quote (("std" "err-derive" "flips-sys/std") ("default" "std") ("_doc" "std"))))))

