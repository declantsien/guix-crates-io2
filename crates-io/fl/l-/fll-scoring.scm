(define-module (crates-io fl l- fll-scoring) #:use-module (crates-io))

(define-public crate-fll-scoring-0.1.0 (c (n "fll-scoring") (v "0.1.0") (d (list (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "bson") (r "^1.0.0") (d #t) (k 0)) (d (n "configparser") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "handlebars") (r "^3.1.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "00rvk7dji2mmc7z2wiz8j5gy8as1f3rdmcg2nqjh6ir3xg2xvsqw")))

