(define-module (crates-io fl l- fll-rs) #:use-module (crates-io))

(define-public crate-fll-rs-0.1.0 (c (n "fll-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ev3dev-lang-rust") (r "^0.12") (f (quote ("screen"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (k 0)) (d (n "imageproc") (r "^0.23") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)))) (h "0xvzywzpcl3nxnrjlj62wzpd3b79j1xrgbc1vr9fwvzr29r8kjrq")))

(define-public crate-fll-rs-0.1.1 (c (n "fll-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ev3dev-lang-rust") (r "^0.12") (f (quote ("screen"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (k 0)) (d (n "imageproc") (r "^0.23") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)))) (h "0iscplyifkvzxyjnl4q6yyw8ww38hdznixyfdj6b7kyw64ik42r1")))

(define-public crate-fll-rs-0.1.2 (c (n "fll-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ev3dev-lang-rust") (r "^0.12") (f (quote ("screen"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (k 0)) (d (n "imageproc") (r "^0.23") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)))) (h "00xja2hcns4n0dc4pinv76na675gwa0pnm7862dj3wb9kzap6wy2")))

(define-public crate-fll-rs-0.1.3 (c (n "fll-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ev3dev-lang-rust") (r "^0.12") (f (quote ("screen"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (k 0)) (d (n "imageproc") (r "^0.23") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)))) (h "0va69h6c2yxj7cbyz6rmw083ikr89rg7hkcxdcsb62af4fzv252q")))

(define-public crate-fll-rs-0.1.4 (c (n "fll-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ev3dev-lang-rust") (r "^0.12") (f (quote ("screen"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (k 0)) (d (n "imageproc") (r "^0.23") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)))) (h "0ramprb4jb4cx6p4phim1bb2as0ll6plccz32kasslmw0sj1sj7b")))

