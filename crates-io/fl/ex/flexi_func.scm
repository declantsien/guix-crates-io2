(define-module (crates-io fl ex flexi_func) #:use-module (crates-io))

(define-public crate-flexi_func-0.1.0 (c (n "flexi_func") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0p6wqa21im4arzz9xzjw72lhknkk4ahmxdzs29lvf5wjs3bp872y")))

(define-public crate-flexi_func-0.2.0 (c (n "flexi_func") (v "0.2.0") (d (list (d (n "flexi_func_declarative") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0zwccswci53sw11ryk2vzhix958bcf2fhlqfvfjyhmk8nrnvrfck")))

(define-public crate-flexi_func-0.2.1 (c (n "flexi_func") (v "0.2.1") (d (list (d (n "flexi_func_declarative") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0avc1qkifjyjvjl6cw7g5pzndzkx5szcwmsmxrm3cjvdir2qh1n7")))

(define-public crate-flexi_func-0.2.2 (c (n "flexi_func") (v "0.2.2") (d (list (d (n "flexi_func_declarative") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0yipd8bxmlp31aywlnyxdvva37b9fzbzw6v6q05bk01blgh9wj9f")))

(define-public crate-flexi_func-0.2.3 (c (n "flexi_func") (v "0.2.3") (d (list (d (n "flexi_func_declarative") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1gfwkrcf1l454l97d5ibip7qglr0fn3c9dxik6cjb4dcpbldjwd9")))

(define-public crate-flexi_func-0.2.4 (c (n "flexi_func") (v "0.2.4") (d (list (d (n "flexi_func_declarative") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0y0dg8xxd9w5k0ph7ix2bdpm16w4i5nr1jyya7y3c78mz52syz19")))

(define-public crate-flexi_func-0.2.5 (c (n "flexi_func") (v "0.2.5") (d (list (d (n "flexi_func_declarative") (r "^0.2.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0zhms1f8kvnfz461rnffkpmi23irfl98xslxpi48f00q3cap0dr3")))

(define-public crate-flexi_func-0.2.6 (c (n "flexi_func") (v "0.2.6") (d (list (d (n "flexi_func_declarative") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1b8jl20ia98viyqsm8ykvchzikykjrd8aj3i4lh14wz8h4ghx7qz")))

(define-public crate-flexi_func-0.2.7 (c (n "flexi_func") (v "0.2.7") (d (list (d (n "flexi_func_declarative") (r "^0.2.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1fav9wkbj9i5kqmzw6q4l42nhb3a6yvq24kkcxv56ipjbrzsqdlr")))

