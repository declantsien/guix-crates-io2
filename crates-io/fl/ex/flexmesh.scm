(define-module (crates-io fl ex flexmesh) #:use-module (crates-io))

(define-public crate-flexmesh-0.1.0 (c (n "flexmesh") (v "0.1.0") (h "16sldxmdg4772dd7dilja93q052gfia58x67599ivhdqbcb4qifk")))

(define-public crate-flexmesh-0.1.1 (c (n "flexmesh") (v "0.1.1") (h "12rkp733yd1a9sqrd4qlqyjw1ydpdlpbjbplh851ddn9xwyaqbs9")))

(define-public crate-flexmesh-0.1.2 (c (n "flexmesh") (v "0.1.2") (h "0173sm2fymhh6lxy5fryhi6aa3hb2narnvg2hcrvgjshp4mc045f")))

(define-public crate-flexmesh-0.1.3 (c (n "flexmesh") (v "0.1.3") (h "1qwpghdbx0x5hari6fnxy05dr4l0gd248kbbgr92n8bdya35qz6b")))

