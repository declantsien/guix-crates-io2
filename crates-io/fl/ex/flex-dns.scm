(define-module (crates-io fl ex flex-dns) #:use-module (crates-io))

(define-public crate-flex-dns-0.1.0 (c (n "flex-dns") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0") (o #t) (k 0)) (d (n "heapless") (r "^0") (o #t) (k 0)) (d (n "simple-dns") (r "^0") (d #t) (k 2)))) (h "13j5j7krjz28yhv2kmr6d0q7h5zsbwf53qalm6yf6gyj1vg5d7xn") (f (quote (("vec") ("default" "arrayvec" "heapless"))))))

(define-public crate-flex-dns-1.0.0 (c (n "flex-dns") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0") (o #t) (k 0)) (d (n "heapless") (r "^0") (o #t) (k 0)) (d (n "simple-dns") (r "^0") (d #t) (k 2)))) (h "1737rnkgqnwr8nysrd0wjz5ggs2bsqnyi9z3m324vgw424jl2mdp") (f (quote (("vec") ("default" "arrayvec" "heapless"))))))

(define-public crate-flex-dns-1.0.1 (c (n "flex-dns") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0") (o #t) (k 0)) (d (n "heapless") (r "^0") (o #t) (k 0)) (d (n "simple-dns") (r "^0") (d #t) (k 2)))) (h "0ngz2dh2s15zbpzzrymkf08w696if6hncn0hqakpgggn1c7ax2qk") (f (quote (("vec") ("default" "arrayvec" "heapless"))))))

