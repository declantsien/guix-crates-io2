(define-module (crates-io fl ex flexvg) #:use-module (crates-io))

(define-public crate-flexvg-0.1.0 (c (n "flexvg") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "taffy") (r "^0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hgf6zrlnz7436w345hgh7jcy159bxkbya29favap3k30qgfwa6h") (s 2) (e (quote (("serde" "dep:serde" "dep:regex" "dep:lazy_static" "dep:convert_case"))))))

