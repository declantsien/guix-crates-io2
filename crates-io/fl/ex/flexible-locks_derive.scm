(define-module (crates-io fl ex flexible-locks_derive) #:use-module (crates-io))

(define-public crate-flexible-locks_derive-0.1.0 (c (n "flexible-locks_derive") (v "0.1.0") (d (list (d (n "quote") (r ">= 0.4, < 0.6") (d #t) (k 0)) (d (n "syn") (r ">= 0.12, < 0.14") (d #t) (k 0)))) (h "0a3lxi42gv1xd6xbvwg0dpkb21kg0scqb67rc160mgnrlajva7dq")))

