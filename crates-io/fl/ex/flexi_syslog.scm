(define-module (crates-io fl ex flexi_syslog) #:use-module (crates-io))

(define-public crate-flexi_syslog-0.1.0 (c (n "flexi_syslog") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.22") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0sl5ii03vhgkgvmzglyy7cyv2s47wfmq1dsvn8bhbp4l4kyd6vdf")))

(define-public crate-flexi_syslog-0.2.0 (c (n "flexi_syslog") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.22") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0") (d #t) (k 0)))) (h "1y217nam7caawxbc801jd1gr5xg243h02h09rg7kslz9m83w8pbb")))

(define-public crate-flexi_syslog-0.2.1 (c (n "flexi_syslog") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.22") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0") (d #t) (k 0)))) (h "0gjby7sfx5mmmb8fndwqra6yc34gnilg9phq5n6sd350x6kidxw0")))

(define-public crate-flexi_syslog-0.2.2 (c (n "flexi_syslog") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.22") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0") (d #t) (k 0)))) (h "0fggm8qnb0skrdiplrlsl5ycxmxmdlhxiscbrpa4pynnzf361dcn")))

(define-public crate-flexi_syslog-0.3.0 (c (n "flexi_syslog") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.22") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0") (d #t) (k 0)))) (h "1m08z9njgm6n22yzwkrbyd5y75v9wak3lqi72qdgam567ijw7c8x")))

(define-public crate-flexi_syslog-0.4.0 (c (n "flexi_syslog") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.22") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("local-offset" "formatting" "macros" "parsing"))) (d #t) (k 0)))) (h "11nn62krasp5n6kklx8q0qq5d4y3fick3af4wa4mi64b1l64d57i")))

(define-public crate-flexi_syslog-0.4.1 (c (n "flexi_syslog") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.22") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("local-offset" "formatting" "macros" "parsing"))) (d #t) (k 0)))) (h "1ab8cbz0hddzdgzqgis5q3b75zhc0yaynf90xil56k480qnpk9ha")))

(define-public crate-flexi_syslog-0.5.0 (c (n "flexi_syslog") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.24") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0") (d #t) (k 0)))) (h "16vqliwr4ypph00mhvg4hav4mb07m6gxjm40nqyvmmm9ap3xks9p")))

(define-public crate-flexi_syslog-0.5.1 (c (n "flexi_syslog") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.24") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0") (d #t) (k 0)))) (h "0v8i25ngfql22f86law5j3l52kbrsmbx6vzdgl9w0vyl390c1dpf")))

(define-public crate-flexi_syslog-0.5.2 (c (n "flexi_syslog") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.25") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0") (d #t) (k 0)))) (h "188shqvppdkmcqz7hany6bvaza36hdqyhlv6m4hp61n8y2wq4bnc")))

(define-public crate-flexi_syslog-0.5.3 (c (n "flexi_syslog") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.27.4") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^6.1") (d #t) (k 0)))) (h "126vpjfb48xnsxxq5hrlilynj5pjw41pv9wmf05ixfdnv7q9fnjf")))

