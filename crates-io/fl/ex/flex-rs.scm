(define-module (crates-io fl ex flex-rs) #:use-module (crates-io))

(define-public crate-flex-rs-0.1.0 (c (n "flex-rs") (v "0.1.0") (h "1k276yz6xak7daa7z8aibyxpp7md6b253cdqqb0rr1y7ksjbip4p") (y #t)))

(define-public crate-flex-rs-0.1.1 (c (n "flex-rs") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "no-comment") (r "^0.0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ixc6kyvj3f3p38g66bgh8gjl140a62jayn0nl3p4cl30sh0vywa") (y #t)))

