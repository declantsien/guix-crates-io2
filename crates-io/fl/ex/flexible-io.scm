(define-module (crates-io fl ex flexible-io) #:use-module (crates-io))

(define-public crate-flexible-io-0.1.0 (c (n "flexible-io") (v "0.1.0") (h "1g4p68y7n36h5drw3v0dvlj188xx42b1zwlsy5wbyz8gnj5j84js")))

(define-public crate-flexible-io-0.1.1 (c (n "flexible-io") (v "0.1.1") (h "0aw6wziq253q916f2z009wh2jip2fjjmp73nkd8myb3lqvanlxrb")))

(define-public crate-flexible-io-0.2.0 (c (n "flexible-io") (v "0.2.0") (h "05s42d2i7lrnmc4l33psadykpdkr655lrwssf2q3q23v5y49f5c3") (f (quote (("unstable_with_metadata_of" "unstable_set_ptr_value") ("unstable_set_ptr_value"))))))

