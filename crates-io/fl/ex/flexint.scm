(define-module (crates-io fl ex flexint) #:use-module (crates-io))

(define-public crate-flexint-0.1.0 (c (n "flexint") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "serde_with") (r "^3.7.0") (o #t) (d #t) (k 0)) (d (n "trait-tactics") (r "^0.1.0") (f (quote ("num-traits"))) (d #t) (k 0)))) (h "0gzk8915dv2h1jyry2lmwnrwi90r6rr3h852pkqlqccc1ciiy0fj") (s 2) (e (quote (("serde" "dep:serde_with"))))))

