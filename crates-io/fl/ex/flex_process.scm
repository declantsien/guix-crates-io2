(define-module (crates-io fl ex flex_process) #:use-module (crates-io))

(define-public crate-flex_process-0.1.0 (c (n "flex_process") (v "0.1.0") (d (list (d (n "edn-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "edn-format") (r "^3.3.0") (d #t) (k 0)) (d (n "edn-rs") (r "^0.17.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kjylmhrpimmsdn1q8azmgh487gmxdhwqykk15qfa238l526zqjg")))

