(define-module (crates-io fl ex flexar) #:use-module (crates-io))

(define-public crate-flexar-0.1.0 (c (n "flexar") (v "0.1.0") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "0myz847p08nbkh9pchpp1dx548ffm4kikhmwvwximbzwikljpr49")))

(define-public crate-flexar-0.1.1 (c (n "flexar") (v "0.1.1") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "023hg030ihd7ka5fbs78r6bwcmhqa7996i75xk5iq1mj9dqsa6p5")))

(define-public crate-flexar-0.2.0 (c (n "flexar") (v "0.2.0") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "0qr53k5i2cnbsx6wy669vpvsyqskmhz3wqhix9r84sgf20icw0lx")))

(define-public crate-flexar-0.2.1 (c (n "flexar") (v "0.2.1") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "0vfvaglc5my1jiyqd9ifl5whwjk2z6v0pq6hma0v13frhhmnrxjr")))

(define-public crate-flexar-0.2.2 (c (n "flexar") (v "0.2.2") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "07nrchzk9z5fnb3dmnqmfsa2ffk3rmqwyd2cpspm5vh88ijz5pc3")))

(define-public crate-flexar-0.2.3 (c (n "flexar") (v "0.2.3") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "1c21nzm95v81dvylskg7j41hchvxvfqdp62aqyp5p12flb105r0q")))

(define-public crate-flexar-0.3.1 (c (n "flexar") (v "0.3.1") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "0iz1cbfjl4a450b2wwv475vi8wpfwmxrzch5n00krrfdn3p4c62r")))

(define-public crate-flexar-0.3.3 (c (n "flexar") (v "0.3.3") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "1zfx0gbpdv3nr14fwqnbgggkwc4jj9bi06h76ms5marri7nmxdw1")))

(define-public crate-flexar-0.3.4 (c (n "flexar") (v "0.3.4") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "1517rjyzckfpl17nzp37in6ccqpm7vc11d8rvdxhsx6p0jx5xcqp")))

(define-public crate-flexar-0.3.5 (c (n "flexar") (v "0.3.5") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "1rbijf46npi4qjjw1fd5xcdr7iv74jh6cgdzizjc87lbrc7vmlmj")))

(define-public crate-flexar-0.3.6 (c (n "flexar") (v "0.3.6") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "1pxb16qjk1cya38zkbqmmrd928b6p9rqxylb7d20bxvm4sfbyf4q")))

(define-public crate-flexar-0.3.7 (c (n "flexar") (v "0.3.7") (d (list (d (n "soulog") (r "^1.1.0") (d #t) (k 0)))) (h "0km6l11a6dz564vd3mx54hjnz0dghxx78vmshiwmc73gva7l8fmg")))

(define-public crate-flexar-0.3.8 (c (n "flexar") (v "0.3.8") (d (list (d (n "soulog") (r "^1.2.1") (d #t) (k 0)))) (h "1ym617vg1xhsvg2s1vm6a24s4ik8jvr1a9h9c4a0i2d3b6f1hbrg")))

(define-public crate-flexar-0.3.9 (c (n "flexar") (v "0.3.9") (d (list (d (n "soulog") (r "^1.2.1") (d #t) (k 0)))) (h "1s3d69jxjpgzkqijbyv2ycrjn5ygc0qjicgrjl6cnrgg1cnip5bq")))

(define-public crate-flexar-0.3.10 (c (n "flexar") (v "0.3.10") (h "1k1lqjmxzhxb6gn0xzkr7avir8l9dcsqhbd42cam1071af2yagyx")))

(define-public crate-flexar-0.3.11 (c (n "flexar") (v "0.3.11") (h "04fxfdbl820jydr1k2mb3f7wq3mdwvxjd70631zs1ixq496qdkhv")))

(define-public crate-flexar-0.4.0 (c (n "flexar") (v "0.4.0") (h "07xmim4abm15hks555hipg50anb3sa2gld1n7sjalv5cx91p404z")))

(define-public crate-flexar-0.4.1 (c (n "flexar") (v "0.4.1") (h "05sgh17acm8c769zaibfs7gwhvbyrmqms7pk2pp1x2slrqqbmap0")))

(define-public crate-flexar-0.4.2 (c (n "flexar") (v "0.4.2") (h "02g8r4qm8vdq009x1x2ykpz07rxvhlnrm6rfkwxfqw5rdqpbl89b")))

(define-public crate-flexar-0.4.3 (c (n "flexar") (v "0.4.3") (h "0ph55d24d83f30g84pl8bglaimcax98ar2j1vrg63dz5fkf6qp5w")))

(define-public crate-flexar-1.0.0 (c (n "flexar") (v "1.0.0") (h "014dmvqgy1syc5087yzr2d0s3il2mfjgkqlw0va4jj74innyfyxl")))

(define-public crate-flexar-1.0.1 (c (n "flexar") (v "1.0.1") (h "177cjpv6c76wp9dqs7ypvwjcx39iplpy47hsqvb5pbirwglb33r8")))

(define-public crate-flexar-1.0.2 (c (n "flexar") (v "1.0.2") (h "1rh3fzz396rkv1wg303m33l149h7jz4srcakalhyfxaml29jv86f")))

(define-public crate-flexar-1.1.0 (c (n "flexar") (v "1.1.0") (h "1m3z4y8xlwc8v9ip8cxjw2cbwik0884acwjws64ppzh6vjc78vzm")))

(define-public crate-flexar-1.1.1 (c (n "flexar") (v "1.1.1") (h "1qbs2j8bwliaz5n3hrkjbfq7dnk7jyczwpv2rshls8h3s1i3k7rd")))

(define-public crate-flexar-1.1.2 (c (n "flexar") (v "1.1.2") (h "10n4738zz6n599j4jiq6bb0qcnj0przqahmhjy0isairhh3qw0yc")))

(define-public crate-flexar-1.1.3 (c (n "flexar") (v "1.1.3") (h "1934m7f15vh7ayv0iz9xq6dqvibh6anl7pp7cj6q3if47vfn1l4x")))

(define-public crate-flexar-1.1.4 (c (n "flexar") (v "1.1.4") (h "06z04bvl1b04k2mb4hja34mh4dpbn4wsmmah11b6gy4lm1dfrdc5")))

(define-public crate-flexar-1.2.0 (c (n "flexar") (v "1.2.0") (h "0hna2crlnamk3kibzm4z9lv7bai0y6vgdjs18qw2lassqi3lndy1")))

(define-public crate-flexar-1.2.1 (c (n "flexar") (v "1.2.1") (h "1gqgakr28yf4996b5m6b3lswcmafqk8sghwp591azpcw0c028i6n")))

(define-public crate-flexar-1.2.2 (c (n "flexar") (v "1.2.2") (h "0gl7q9nfpdxj6rz1jbpdmn3vakm2gh57vlsspwgqvzamx4p4gfps")))

(define-public crate-flexar-1.2.3 (c (n "flexar") (v "1.2.3") (h "0m05b16gnf27ffd37fimzv21pkl865qlm2dgmipaphgiwr6m5h1m")))

(define-public crate-flexar-1.2.4 (c (n "flexar") (v "1.2.4") (h "1cfn6sh95x0m5cmdx91hq7md9z5qbwnacpcdqpjasrywxhyah4lq")))

(define-public crate-flexar-1.2.5 (c (n "flexar") (v "1.2.5") (h "1gwccrkkflkfxbpb0la39d8f3abphp74calcd6kn8y9ky3h02hxm")))

(define-public crate-flexar-1.2.6 (c (n "flexar") (v "1.2.6") (h "0079khl5q68qcczwvflm9p91kdg5fic26qnb4mhjqjccjrc3b306")))

