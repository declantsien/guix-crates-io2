(define-module (crates-io fl ex flexible-locks) #:use-module (crates-io))

(define-public crate-flexible-locks-0.1.0 (c (n "flexible-locks") (v "0.1.0") (d (list (d (n "flexible-locks_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "parking_lot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("synchapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v9qw1n4x3z1lm2hri49f4h8cy6b8w8vc3ihjhmmh5zkwdpwnvxq") (f (quote (("std") ("default" "std"))))))

