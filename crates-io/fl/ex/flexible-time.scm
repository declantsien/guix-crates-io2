(define-module (crates-io fl ex flexible-time) #:use-module (crates-io))

(define-public crate-flexible-time-0.1.0 (c (n "flexible-time") (v "0.1.0") (d (list (d (n "time") (r "^0.3") (f (quote ("parsing" "macros"))) (d #t) (k 0)))) (h "1av63r0lnrdsi563zzahnv1jhak3ay600xhmm9m9d2hj1w4m530i") (r "1.65")))

(define-public crate-flexible-time-0.1.1 (c (n "flexible-time") (v "0.1.1") (d (list (d (n "time") (r "^0.3") (f (quote ("parsing" "macros"))) (d #t) (k 0)))) (h "1k57sxp4w0pa4q9arb4s9q9in7if2ka81l5p9jb69lh63pf516ha") (r "1.67")))

