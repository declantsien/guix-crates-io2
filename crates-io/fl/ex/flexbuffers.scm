(define-module (crates-io fl ex flexbuffers) #:use-module (crates-io))

(define-public crate-flexbuffers-0.1.0 (c (n "flexbuffers") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0qyzqw46rhb08zg3f7pxzy41h51mmwb6s35vwx2nj1h6i22ph7j8")))

(define-public crate-flexbuffers-0.1.1 (c (n "flexbuffers") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1cr1b2kwcp0dlxqrgj9q9fwgxbqa9l5vf9vhzwis2jwf3ijma8mz")))

(define-public crate-flexbuffers-0.2.1 (c (n "flexbuffers") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0dyrz6q3j3nag6iwfdsjy12pc5alyfpg4vga4cwyw7mvrqys5yp8") (f (quote (("serialize_human_readable") ("deserialize_human_readable"))))))

(define-public crate-flexbuffers-0.2.2 (c (n "flexbuffers") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.119") (d #t) (k 0)))) (h "11x18qx6rv5psqn11xjdhkvxzrm13awy1792153q2wdnsphcfmim") (f (quote (("serialize_human_readable") ("deserialize_human_readable"))))))

(define-public crate-flexbuffers-2.0.0 (c (n "flexbuffers") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.119") (d #t) (k 0)))) (h "1s7pw2zxc0hpvdxnvplyf4cdmydhx48vxzjvwy6801b4y0l43l8m") (f (quote (("serialize_human_readable") ("deserialize_human_readable"))))))

