(define-module (crates-io fl ex flexer) #:use-module (crates-io))

(define-public crate-flexer-0.1.0 (c (n "flexer") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "097il283l35lda9miap7rriryn599jyy7vj16pgxpbyqayh49vp9") (y #t)))

(define-public crate-flexer-0.1.1 (c (n "flexer") (v "0.1.1") (d (list (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "1x6ibqhvz5h28jqqy3my2rcifdy1nplw96bhxban252lqmkg1raa") (y #t)))

