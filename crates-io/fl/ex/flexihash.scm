(define-module (crates-io fl ex flexihash) #:use-module (crates-io))

(define-public crate-flexihash-0.1.0 (c (n "flexihash") (v "0.1.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "0vsv2w2n5c8i9s4p8pklqcdb9l15kw183bjfkf5zln83hlk0mzgn")))

(define-public crate-flexihash-0.1.1 (c (n "flexihash") (v "0.1.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "059sgqwaczapcf0r2qvvp81x40rmb6y10hmyin0xam880awjz73l")))

(define-public crate-flexihash-0.1.2 (c (n "flexihash") (v "0.1.2") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "1m1i3wz3w56yz7cq6imlyh5nk7vzms63fdpn565z05lcb2fhvbkz")))

(define-public crate-flexihash-0.1.3 (c (n "flexihash") (v "0.1.3") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "0lwifxa69yvdysnx7vli8vfsivyi1vb3iha48zr9rcqf1wxdig55")))

(define-public crate-flexihash-0.1.4 (c (n "flexihash") (v "0.1.4") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "1nqfwkxvpa3r1yifzs0yyl0xs6dlcqbh0wdkk3cvr4ka7dnlhw6h")))

(define-public crate-flexihash-0.1.5 (c (n "flexihash") (v "0.1.5") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "1jlfg3cb5v0rxl8gxqc5hzpp83w9mmfx1k72hzdhdn31s6rypq86")))

(define-public crate-flexihash-0.1.6 (c (n "flexihash") (v "0.1.6") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "1hq85806ns0qz3yzcrij7z3acm46ib1wi3bffa7mqdckbh562iiq")))

(define-public crate-flexihash-0.1.7 (c (n "flexihash") (v "0.1.7") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "0qq2kh17pivwzr62h223hndvbv2lk50kk0d6k8zgmbzyf7l7s507")))

(define-public crate-flexihash-0.1.8 (c (n "flexihash") (v "0.1.8") (d (list (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "0w7xrci7ipgcnyanx3gg28lp7l5bdavk0500r6w89a9hil32jiyc")))

