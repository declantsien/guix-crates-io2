(define-module (crates-io fl ex flexstr) #:use-module (crates-io))

(define-public crate-flexstr-0.1.0 (c (n "flexstr") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "08ky79ympid0y9fbb6nx0ygn6m3l0fcm10ijqjl49ba4wqisik9f")))

(define-public crate-flexstr-0.2.0 (c (n "flexstr") (v "0.2.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0bvzw3zzp201z85i1c4ar2c3863bps9j1f7v0ql391pjw6c090dc")))

(define-public crate-flexstr-0.2.1 (c (n "flexstr") (v "0.2.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0r2ifpyn3j61s9qbbh3aqm6ygx502bksnjkl3fg72xp7f5l55rrq")))

(define-public crate-flexstr-0.3.0 (c (n "flexstr") (v "0.3.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "09ab4zziph0mrgca27q8s36wc9d1wv34j1zcdzrbdrmal406wk38")))

(define-public crate-flexstr-0.4.0 (c (n "flexstr") (v "0.4.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "103slkp2mfyaahxbznnwywsj7mkp53jsycbbwmqsxx03a9rf73lx")))

(define-public crate-flexstr-0.4.1 (c (n "flexstr") (v "0.4.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16s1c3v5y73gmqgkkvpshqqxg978b40s7vzc3adzhsvd5f28gir8")))

(define-public crate-flexstr-0.4.2 (c (n "flexstr") (v "0.4.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0i6j5vbhk26lkia4n7l5626r6cbjsg9z9wq97zawwfc2krk341fj")))

(define-public crate-flexstr-0.4.3 (c (n "flexstr") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01q5k6y7mii8f6047xnid5gb3h5kk9w7igvmbx3hwj0jmjzhm0j9")))

(define-public crate-flexstr-0.4.4 (c (n "flexstr") (v "0.4.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0p096dfi6wy5z38whl6wmcwjc6idbxwi1pg044q5sr8i65rsvkah")))

(define-public crate-flexstr-0.4.5 (c (n "flexstr") (v "0.4.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1iynwzplzdw8nni6jbi9f0qp9v95rb8jldyw8sywh63a7qydm43y")))

(define-public crate-flexstr-0.4.6 (c (n "flexstr") (v "0.4.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0sdbn7hhq2amhjmjxv27lacxhs05p8g6p9b5yvk519mjlfjpz0b2")))

(define-public crate-flexstr-0.5.0 (c (n "flexstr") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mp1sqrq4h0jaf24hbnam9ba65xbvqx205dp4ihv99xbkibbp7ms")))

(define-public crate-flexstr-0.5.1 (c (n "flexstr") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0sc64r5sabmx5nv9xaja9hxmqr1f0m8fz9c0mcw3jw8xkm1fn0z1")))

(define-public crate-flexstr-0.5.2 (c (n "flexstr") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dbvir0p876lc43bjpc53ar1viqp9pqi5d3addm5bc1j9l0br785")))

(define-public crate-flexstr-0.5.3 (c (n "flexstr") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vfmwfggnvh302fyia0sj56wb224v62bw96b495zb8dl3v8669z4")))

(define-public crate-flexstr-0.5.4 (c (n "flexstr") (v "0.5.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wyplksxsbrv40d2hakwhnac8c30y14bjjfrj4d308qwaf542s4p")))

(define-public crate-flexstr-0.6.0 (c (n "flexstr") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18gvcfy99n9a2azkwm0kvv03mncfb9rkz2z9gv161f46sq7wri82")))

(define-public crate-flexstr-0.6.1 (c (n "flexstr") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vcvmmfp3x3qk3v5farv6jlknxan8f286r5j6a9v9cmqfgq6qiww")))

(define-public crate-flexstr-0.6.2 (c (n "flexstr") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kmgms4n616n54d22v0xpyqnldika72niiqgv5cpv839kwb29ywy")))

(define-public crate-flexstr-0.7.0 (c (n "flexstr") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gig98jpsazkv3fwqsq6yxk2jgxkfxkqpqcnig9fws4p8hja07ll")))

(define-public crate-flexstr-0.7.1 (c (n "flexstr") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03s2xf6c9qqp7xiqf282fsxbnabw0ycym9bwk6slvjnb3i8bipgb")))

(define-public crate-flexstr-0.7.2 (c (n "flexstr") (v "0.7.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0d150brb5ka2084wcmsw668435y7jgsi2p1cbiqiqga3kyiqn4wq")))

(define-public crate-flexstr-0.8.0 (c (n "flexstr") (v "0.8.0") (d (list (d (n "itoa") (r "^1") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1qmcxp3rfikanxh4bi81v2id9nxmpz1m45lxjkrf5j6pakjrhi54") (f (quote (("int_convert" "itoa") ("fp_convert" "ryu") ("fast_format" "ufmt" "ufmt-write"))))))

(define-public crate-flexstr-0.8.1 (c (n "flexstr") (v "0.8.1") (d (list (d (n "itoa") (r "^1") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0zmr5jh7hrxkn5jy0h31221rjpa3x2bkvp8lh3lxmj0wkaw4y6rs") (f (quote (("int_convert" "itoa") ("fp_convert" "ryu") ("fast_format" "ufmt" "ufmt-write")))) (y #t)))

(define-public crate-flexstr-0.9.0 (c (n "flexstr") (v "0.9.0") (d (list (d (n "itoa") (r "^1") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (o #t) (d #t) (k 0)))) (h "17qjp6rskjsr1x3391wd4i6az4gwwihxxa9f7bd09ix9v2pwciq7") (f (quote (("std") ("int_convert" "itoa") ("fp_convert" "ryu") ("fast_format" "ufmt" "ufmt-write") ("default" "std"))))))

(define-public crate-flexstr-0.9.1 (c (n "flexstr") (v "0.9.1") (d (list (d (n "itoa") (r "^1") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1wccq60j4h9xv8543wy5yrfvik7iflylx4njj1dpw3jib2npjqnr") (f (quote (("std") ("int_convert" "itoa") ("fp_convert" "ryu") ("fast_format" "ufmt" "ufmt-write") ("default" "std"))))))

(define-public crate-flexstr-0.9.2 (c (n "flexstr") (v "0.9.2") (d (list (d (n "itoa") (r "^1") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0rggqwm8j77rv6pydrx1n4vr1crfjlnmjnna9yjkdlqr8vqswl2d") (f (quote (("std") ("int_convert" "itoa") ("fp_convert" "ryu") ("fast_format" "ufmt" "ufmt-write") ("default" "std"))))))

