(define-module (crates-io fl ex flexi_config) #:use-module (crates-io))

(define-public crate-flexi_config-0.0.1 (c (n "flexi_config") (v "0.0.1") (d (list (d (n "clap") (r "^2.3.0") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "toml") (r "^0.1.28") (d #t) (k 0)))) (h "0cz32p3v1yb4a3rgdyjkkz0gmhrr71hqrysrdwwyn7d9b6r9cj94")))

(define-public crate-flexi_config-0.1.0 (c (n "flexi_config") (v "0.1.0") (d (list (d (n "clap") (r "^2.3.0") (d #t) (k 2)) (d (n "flexi_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "toml") (r "^0.1.28") (d #t) (k 0)))) (h "1l23jk0vlfgnk4p4k7q29rs52jdhc4nr5vhnxxnx0cwrzs81a6f8")))

