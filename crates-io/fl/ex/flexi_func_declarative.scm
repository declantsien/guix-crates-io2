(define-module (crates-io fl ex flexi_func_declarative) #:use-module (crates-io))

(define-public crate-flexi_func_declarative-0.1.0 (c (n "flexi_func_declarative") (v "0.1.0") (h "1nhvqdvz8izcnmyhk2xrbhmwdndlsrmcrnczq4j3sp0kdgww2fqm")))

(define-public crate-flexi_func_declarative-0.2.2 (c (n "flexi_func_declarative") (v "0.2.2") (h "0i582zxr71wsp129bgnkfbf49psaygkb9vdmvzhgqb7rcd0a32q7")))

(define-public crate-flexi_func_declarative-0.2.4 (c (n "flexi_func_declarative") (v "0.2.4") (h "0fpmyb9csjgbxwabzk8as9rr76vdvmqn5534il5nlf8ammfbcv4h")))

(define-public crate-flexi_func_declarative-0.2.5 (c (n "flexi_func_declarative") (v "0.2.5") (h "1y948icbpdmn840xkpjspbjm4z89xg93mz1zg5qmn5zsxihz25xc")))

(define-public crate-flexi_func_declarative-0.2.6 (c (n "flexi_func_declarative") (v "0.2.6") (h "1l94nk8179kzwwbl438mhh0gsy9hdfm1376aa5n144xcamvgivxy")))

(define-public crate-flexi_func_declarative-0.2.7 (c (n "flexi_func_declarative") (v "0.2.7") (h "0nq3s4piay3cyp9y6gz38wmdzmigjg4cdqbnrlafbhlqfxm8hd60")))

