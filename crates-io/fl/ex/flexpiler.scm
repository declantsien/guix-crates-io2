(define-module (crates-io fl ex flexpiler) #:use-module (crates-io))

(define-public crate-flexpiler-0.1.0 (c (n "flexpiler") (v "0.1.0") (h "0sx8i83has1w208qi0rafzj45acy2304qkkcbk81ajp0jp8dyv3n")))

(define-public crate-flexpiler-0.1.1 (c (n "flexpiler") (v "0.1.1") (d (list (d (n "flexpiler_derive") (r "^0.1.1") (d #t) (k 0)))) (h "117h5qdm2aax3fj2wxyymfdl71f41nazmm570skhnbm5565vvvlv")))

(define-public crate-flexpiler-0.2.0 (c (n "flexpiler") (v "0.2.0") (d (list (d (n "flexpiler_derive") (r "^0.2.0") (d #t) (k 0)))) (h "01s21wpn0j478m38hyay6kq253w4rdd88xqgnx2jaccrmi6piybi")))

(define-public crate-flexpiler-0.2.1 (c (n "flexpiler") (v "0.2.1") (d (list (d (n "flexpiler_derive") (r "^0.2.1") (d #t) (k 0)))) (h "00ladmjln67dhv6qs97wpjxlys5ha9kl6hvbfp2gr12ji3hs99fb")))

