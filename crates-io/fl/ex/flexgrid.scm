(define-module (crates-io fl ex flexgrid) #:use-module (crates-io))

(define-public crate-flexgrid-0.1.0 (c (n "flexgrid") (v "0.1.0") (h "12rv4jghlihjbxvz4zghai03qsbv5sp5a3r48df8cy6s1mf6aicb")))

(define-public crate-flexgrid-0.1.1 (c (n "flexgrid") (v "0.1.1") (h "1l0lvhmjfp9kyqlld9f6d39wb20fsq7qj5mjw4wl71am5d1i5c3d")))

