(define-module (crates-io fl ex flexiber_derive) #:use-module (crates-io))

(define-public crate-flexiber_derive-0.1.0 (c (n "flexiber_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "09pqsagj7f5cwgjvy8c6g7hcc3f9ggrxg7bn40bp2kp78dzi832h")))

