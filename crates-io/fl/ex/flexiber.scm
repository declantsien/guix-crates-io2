(define-module (crates-io fl ex flexiber) #:use-module (crates-io))

(define-public crate-flexiber-0.1.0 (c (n "flexiber") (v "0.1.0") (d (list (d (n "delog") (r "^0.1.0") (d #t) (k 0)) (d (n "flexiber_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "1r9qg4bsdhl7p9hxn435lahhnnlpk65kin28b0zv1izfcqa5ppz3") (f (quote (("std" "alloc") ("log-warn") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("derive" "flexiber_derive") ("alloc"))))))

(define-public crate-flexiber-0.1.1 (c (n "flexiber") (v "0.1.1") (d (list (d (n "delog") (r "^0.1.0") (d #t) (k 0)) (d (n "flexiber_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "1kpzbi8mczi1xdd9x34ds778ps0k5crajnc62qrdpwx3mni2hb7p") (f (quote (("std" "alloc") ("log-warn") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("derive" "flexiber_derive") ("alloc"))))))

