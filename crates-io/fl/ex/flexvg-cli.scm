(define-module (crates-io fl ex flexvg-cli) #:use-module (crates-io))

(define-public crate-flexvg-cli-0.1.0 (c (n "flexvg-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "flexvg") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0vkpvzi12sprvgzx48nm1igibclwbd24c1ym89sackc0245jgphi")))

