(define-module (crates-io fl ex flexible-transcript) #:use-module (crates-io))

(define-public crate-flexible-transcript-0.1.1 (c (n "flexible-transcript") (v "0.1.1") (d (list (d (n "blake2") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "merlin") (r "^3") (o #t) (d #t) (k 0)))) (h "00m0cmq08m4q6lmzhq27zf5ivss9gik2is4zjlmgxccjh3224q5l") (f (quote (("recommended" "blake2")))) (s 2) (e (quote (("merlin" "dep:merlin"))))))

(define-public crate-flexible-transcript-0.1.2 (c (n "flexible-transcript") (v "0.1.2") (d (list (d (n "blake2") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "merlin") (r "^3") (o #t) (d #t) (k 0)))) (h "0z4nzjknnl37fr8yqq40khsxhhbflfvcwr000cj3ywm0pj5dpdwy") (f (quote (("recommended" "blake2")))) (s 2) (e (quote (("merlin" "dep:merlin"))))))

(define-public crate-flexible-transcript-0.1.3 (c (n "flexible-transcript") (v "0.1.3") (d (list (d (n "blake2") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "merlin") (r "^3") (o #t) (d #t) (k 0)))) (h "199kqk7r0byf71g5qcdq2lbgnwlzk8a811184cabymd9d8z9142n") (f (quote (("recommended" "blake2")))) (s 2) (e (quote (("merlin" "dep:merlin"))))))

(define-public crate-flexible-transcript-0.2.0 (c (n "flexible-transcript") (v "0.2.0") (d (list (d (n "blake2") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "merlin") (r "^3") (o #t) (d #t) (k 0)))) (h "0c004a1fv2j08z17way3ak23z7iqk71pqzns06pjayj354ppswrw") (f (quote (("recommended" "blake2")))) (s 2) (e (quote (("merlin" "dep:merlin"))))))

(define-public crate-flexible-transcript-0.3.0 (c (n "flexible-transcript") (v "0.3.0") (d (list (d (n "blake2") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "blake2") (r "^0.10") (d #t) (k 2)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "merlin") (r "^3") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)))) (h "0zjr32m7xjlx4s8sx0kmbmd8axq2hgq4dqww02vzgqvz5nl4n85s") (f (quote (("tests") ("recommended" "blake2")))) (s 2) (e (quote (("merlin" "dep:merlin"))))))

(define-public crate-flexible-transcript-0.3.1 (c (n "flexible-transcript") (v "0.3.1") (d (list (d (n "blake2") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "blake2") (r "^0.10") (d #t) (k 2)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "merlin") (r "^3") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)))) (h "1pfvmqimpd8rwlykjg4riq9258r5kc5jnn6s7d8prm8q44hvsqjy") (f (quote (("tests") ("recommended" "blake2")))) (s 2) (e (quote (("merlin" "dep:merlin"))))))

(define-public crate-flexible-transcript-0.3.2 (c (n "flexible-transcript") (v "0.3.2") (d (list (d (n "blake2") (r "^0.10") (o #t) (k 0)) (d (n "blake2") (r "^0.10") (k 2)) (d (n "digest") (r "^0.10") (f (quote ("core-api"))) (k 0)) (d (n "merlin") (r "^3") (o #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "zeroize") (r "^1.5") (k 0)))) (h "16rm98c3hyadgs15brqxxz089aikj4k2mb95dqfpcfsycw7fgzxh") (f (quote (("tests") ("recommended" "blake2")))) (s 2) (e (quote (("merlin" "dep:merlin")))) (r "1.66")))

