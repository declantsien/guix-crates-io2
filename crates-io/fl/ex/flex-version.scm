(define-module (crates-io fl ex flex-version) #:use-module (crates-io))

(define-public crate-flex-version-0.1.0 (c (n "flex-version") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "123qhhx9krqmpp4r8nzmdn2l6swm8cc5g8pvkxx49djzqbldn2j4")))

(define-public crate-flex-version-0.1.1 (c (n "flex-version") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ln1gcf2p54nbz9cv612d0qc29z48jjh8p22xnvbr4nri8mfidb9")))

(define-public crate-flex-version-0.1.2 (c (n "flex-version") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "048h3jh3rxrsd7wr7j8ara29ls0jqpz4sxz5md0mnsy77jiz5l19")))

(define-public crate-flex-version-0.2.0 (c (n "flex-version") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18hbvwjj6pyd2ddq5d8n8a7i3skgq8ncydj20dbjn51vby13r9p6")))

(define-public crate-flex-version-0.2.1 (c (n "flex-version") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "09ixgb8c8iry324h36xj2a4sc59axqjf1bhb43kz4vlzskk67467")))

(define-public crate-flex-version-0.3.0 (c (n "flex-version") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1hwh970mipsbbc51j5a9igkyaz7pmlzbpphm1v5w3cz21xfb5dld")))

