(define-module (crates-io fl ex flex-algo) #:use-module (crates-io))

(define-public crate-flex-algo-0.1.0 (c (n "flex-algo") (v "0.1.0") (h "1572ib67gjyngkdwdfhcysnwq0j8nl3lxh0aw2clascbrk3hgw4c")))

(define-public crate-flex-algo-0.2.0 (c (n "flex-algo") (v "0.2.0") (h "15rmv3s2nm7fr07dvngxa0jk7phyix9a4g0grlwdnf554h7021xy")))

(define-public crate-flex-algo-0.3.0 (c (n "flex-algo") (v "0.3.0") (h "1jb24r8d6rzzlb3417lqrm7k964701r2ssfrnmgf8x9n6hdnxyhm")))

(define-public crate-flex-algo-0.4.0 (c (n "flex-algo") (v "0.4.0") (h "1mkwq60jfpmj0n3gkvk0zq6x1ydhl0pyhv63q8sgyq8ch011c6g6")))

(define-public crate-flex-algo-0.5.0 (c (n "flex-algo") (v "0.5.0") (h "17qpqf1kn2sfg2sf95g417c4j9060mfs9rpcl2lcj8a9k86dlj5k")))

(define-public crate-flex-algo-0.5.1 (c (n "flex-algo") (v "0.5.1") (h "1j0q0fpnhah54fa0w8qcmnv9jn2w2z2zjn9c6285c1fvqrwq531k")))

(define-public crate-flex-algo-0.6.0 (c (n "flex-algo") (v "0.6.0") (h "05knc54nyvzy0rnl47my9zsfv9qb175nxwq4ci9hgwj34cg31qxa")))

(define-public crate-flex-algo-0.7.0 (c (n "flex-algo") (v "0.7.0") (h "0i051wdsl0pgvr65yr7rbd19q8clnsmkvzihnyhnxv2pbjg0h6lb")))

