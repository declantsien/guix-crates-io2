(define-module (crates-io fl ex flexver-rs) #:use-module (crates-io))

(define-public crate-flexver-rs-0.1.0 (c (n "flexver-rs") (v "0.1.0") (h "0g43ld4k0cnlgpawqbbcjynvqrac8qdj1lxc1p5wr3brd5b3hfr7")))

(define-public crate-flexver-rs-0.1.1 (c (n "flexver-rs") (v "0.1.1") (h "1b6vwj1nwq2flsfwhrhnvndvq6nis782n7aan82br3072jkm7j7v")))

(define-public crate-flexver-rs-0.1.2 (c (n "flexver-rs") (v "0.1.2") (h "16jxg6wd43yb09hzd4gw3qpcisk6ip4gvc3na50w3plimjz54lbm")))

