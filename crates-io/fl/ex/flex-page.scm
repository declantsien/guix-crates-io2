(define-module (crates-io fl ex flex-page) #:use-module (crates-io))

(define-public crate-flex-page-0.1.0 (c (n "flex-page") (v "0.1.0") (d (list (d (n "page-lock") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)))) (h "0l5m64b72v7faqapyr9gq8fz3x92nfd50ds45fyra971lph2mph7")))

(define-public crate-flex-page-0.1.1 (c (n "flex-page") (v "0.1.1") (d (list (d (n "page-lock") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)))) (h "14rfcqb9avy8rbpqkcz7azwz15w2rn030mxfs47y3adhdhr6mmlg")))

(define-public crate-flex-page-0.2.0 (c (n "flex-page") (v "0.2.0") (d (list (d (n "page-lock") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)))) (h "0mzky93zy53smsq20anyay5djbpc18cfkf6q0hylw0kgv8nj3dc8")))

(define-public crate-flex-page-0.3.0 (c (n "flex-page") (v "0.3.0") (d (list (d (n "page-lock") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)))) (h "0r8pl5hbsak1d5008yxlxvc3sm9b98zmbvfxynvxxy7g8q71lynk")))

(define-public crate-flex-page-0.3.1 (c (n "flex-page") (v "0.3.1") (d (list (d (n "page-lock") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)))) (h "0fqzd4cwcf53gj8jz71lmngqxvszlpqyzflz6r30fgi271drj11m")))

(define-public crate-flex-page-0.4.0 (c (n "flex-page") (v "0.4.0") (d (list (d (n "page-lock") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)))) (h "0inbvjv73pl7srcw4l3n0pc5kryym4bpymc0lx71ykhj5vj8phds")))

(define-public crate-flex-page-0.5.0 (c (n "flex-page") (v "0.5.0") (d (list (d (n "page-lock") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1a1z5cdhvawg0dldq17ddhd1hjp0kcb85kxgz91g20y50k39w8a4")))

(define-public crate-flex-page-0.5.1 (c (n "flex-page") (v "0.5.1") (d (list (d (n "page-lock") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "04vzishfs0nf41ydfzbay1n7nn79xm16ccmcwwlwjj3fbk7l5flj")))

(define-public crate-flex-page-0.6.0 (c (n "flex-page") (v "0.6.0") (d (list (d (n "page-lock") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0la9rwj6v97i0v4hs4nrzqajfz07x7jhkiaq9imp79xp5yxwz52n")))

(define-public crate-flex-page-0.6.1 (c (n "flex-page") (v "0.6.1") (d (list (d (n "page-lock") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "13ki8bpkrzwfi39rchl08im4vpwb17jbzg07rblycrnlxqfxfs4b")))

(define-public crate-flex-page-0.7.0 (c (n "flex-page") (v "0.7.0") (d (list (d (n "page-lock") (r "^3.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ilnmh9v0p93ycdbzivbqa0phi35nvdh8d5zg2mpgphlsas7y1bb")))

(define-public crate-flex-page-0.8.0 (c (n "flex-page") (v "0.8.0") (d (list (d (n "page-lock") (r "^3.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1zkyx41zz24n39z8lci8vscjys3jdcdf961f26bfm79bmnnffvkb")))

(define-public crate-flex-page-0.9.0 (c (n "flex-page") (v "0.9.0") (d (list (d (n "page-lock") (r "^3.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1ykp7q8ribicrn7ch72xjiq2sdr7kwqg59gx91mms7frjcrzp57v")))

(define-public crate-flex-page-1.0.0 (c (n "flex-page") (v "1.0.0") (d (list (d (n "page-lock") (r "^3.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0wrs8x0rnkfvrcjzmsr6n7zc8qv44j9cgvzpqm3ksl98y0rxcfs1")))

(define-public crate-flex-page-2.0.0 (c (n "flex-page") (v "2.0.0") (h "17xwb40c7k2pyf5nw2fys4qp89hh5w7rbjxxzl548jp7fv1dgv50")))

(define-public crate-flex-page-2.1.0 (c (n "flex-page") (v "2.1.0") (h "0phxdw2syg4kdmn7b755xbd289vrb2qbhzl2ickdszafjdmyqj06")))

