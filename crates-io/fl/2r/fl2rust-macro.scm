(define-module (crates-io fl #{2r}# fl2rust-macro) #:use-module (crates-io))

(define-public crate-fl2rust-macro-0.5.5 (c (n "fl2rust-macro") (v "0.5.5") (d (list (d (n "fl2rust") (r "^0.5") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1") (d #t) (k 0)))) (h "0hmrv9v7d2xc6pi60jv9j2cpsaj1y896bcpz1vvc85cd0ma5lh2v") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.6 (c (n "fl2rust-macro") (v "0.5.6") (d (list (d (n "fl2rust") (r "^0.5") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1") (d #t) (k 0)))) (h "0mczk4hj65m33grlgisw14vr8mw73zsjf2fzs4khwbl5hw002cjj") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.7 (c (n "fl2rust-macro") (v "0.5.7") (d (list (d (n "fl2rust") (r "^0.5.7") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.9") (d #t) (k 0)))) (h "05yxskmiyy7kwg4ld9wq6kj5ml8b55pwz50qnf015ciyw7y9ic5h") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.8 (c (n "fl2rust-macro") (v "0.5.8") (d (list (d (n "fl2rust") (r "^0.5.8") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.9") (d #t) (k 0)))) (h "0wwz2cgcjkqxk6dijw0acr7l1r5asm8ayfgca1dj36fw4a4fk70q") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.10 (c (n "fl2rust-macro") (v "0.5.10") (d (list (d (n "fl2rust") (r "^0.5.10") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.10") (d #t) (k 0)))) (h "1dk7c8fpkc2m84zmf4zhch66lzqw4a0325jk6cmg9gdl21lli6xw") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.11 (c (n "fl2rust-macro") (v "0.5.11") (d (list (d (n "fl2rust") (r "^0.5.11") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.12") (d #t) (k 0)))) (h "0b09vbgddfbc338v9g889ps5bhmvj5sah48j3hsl6w9gv249b3x9") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.12 (c (n "fl2rust-macro") (v "0.5.12") (d (list (d (n "fl2rust") (r "^0.5.12") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.12") (d #t) (k 0)))) (h "1m97npnpyjh7qcwpiynqa9zwb7afg3qzylsgas1jrbinncl0adkv") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.13 (c (n "fl2rust-macro") (v "0.5.13") (d (list (d (n "fl2rust") (r "^0.5.13") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.12") (d #t) (k 0)))) (h "1k1yhqqi90rm660cjfrgz22mifyddkc42k1qrhpcr0d3nv4bv094") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.14 (c (n "fl2rust-macro") (v "0.5.14") (d (list (d (n "fl2rust") (r "^0.5.14") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "1l189ryr0iq83pr3rw83cl2llywsvl6a4jzzd5drrwv19vxjxx9i") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.15 (c (n "fl2rust-macro") (v "0.5.15") (d (list (d (n "fl2rust") (r "^0.5.15") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "0ihqck0h3x2as9dmfb21kdzsp2njkk4sdzch16i1r21176pwgig1") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.16 (c (n "fl2rust-macro") (v "0.5.16") (d (list (d (n "fl2rust") (r "^0.5.16") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "192dk5cif8l28cpra95lwdqmkzi6j7hzlhni6v7iqwgmcqjdllxg") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.17 (c (n "fl2rust-macro") (v "0.5.17") (d (list (d (n "fl2rust") (r "^0.5.17") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "12iljp75ipxfaviawjglbm61kl23zwj0k7h7bgv3vkqrcdx8q3fs") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.18 (c (n "fl2rust-macro") (v "0.5.18") (d (list (d (n "fl2rust") (r "^0.5.18") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "1i92h3q2ix5y848xhcnc6gc32ibw3igjk6pzkzzrvjw3pabgdgd9") (r "1.63")))

(define-public crate-fl2rust-macro-0.5.19 (c (n "fl2rust-macro") (v "0.5.19") (d (list (d (n "fl2rust") (r "^0.5.19") (d #t) (k 0)) (d (n "fluid-parser") (r "^0.1.15") (d #t) (k 0)))) (h "13476kjg81jkpn3cp0synzr1gf87m8d1brkzjkfd7mqsrrnr5ln9") (r "1.63")))

