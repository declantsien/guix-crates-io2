(define-module (crates-io fl #{2r}# fl2rust) #:use-module (crates-io))

(define-public crate-fl2rust-0.1.0 (c (n "fl2rust") (v "0.1.0") (h "05ipyk8xsdx4c1xm0k3ipjdc5b9skkbkgdh9kaxxf6ab80bq9s6c")))

(define-public crate-fl2rust-0.1.1 (c (n "fl2rust") (v "0.1.1") (h "1ylgzp4qbll5akzq7i39bz03m3yp409124pkn5fqvyjxsifmh7r6")))

(define-public crate-fl2rust-0.1.2 (c (n "fl2rust") (v "0.1.2") (h "1fnqj8h2l86hrxkisl4ksq4yma4i482d5gw666bdk14f78araij4")))

(define-public crate-fl2rust-0.1.5 (c (n "fl2rust") (v "0.1.5") (h "1cywf3xj5iqa60j5pvdsln80dgs9x5gzsrpf2qarndimki7dgwgl")))

(define-public crate-fl2rust-0.1.6 (c (n "fl2rust") (v "0.1.6") (h "080i55nz4kal0xrc7hgpqd6lw27l91lg7ry6fvhl3drxvxh76m0l")))

(define-public crate-fl2rust-0.1.7 (c (n "fl2rust") (v "0.1.7") (h "10fd2szvpcw0cr60ywvhwykcr672a4naa2rq7hh5fxpjcg749lvc")))

(define-public crate-fl2rust-0.1.8 (c (n "fl2rust") (v "0.1.8") (h "0bnayx9w8x5bql0jzbjhmgy1fc4x19xaw1v2jal2qz1q23frhrzn")))

(define-public crate-fl2rust-0.1.9 (c (n "fl2rust") (v "0.1.9") (h "1b6w8clf0g1phg40f3fyznd73awpfcz1i48psa631raxfvn75xrl")))

(define-public crate-fl2rust-0.1.10 (c (n "fl2rust") (v "0.1.10") (h "08hpa0rjrfn20n3pf2656s6a0nnwddxwix73wfl1k9xr8j9phwik")))

(define-public crate-fl2rust-0.1.11 (c (n "fl2rust") (v "0.1.11") (h "0w11bv4vkxlj6vwakhy4qknyp2aywvld9lpkjn0ararz4z47w6qa")))

(define-public crate-fl2rust-0.1.12 (c (n "fl2rust") (v "0.1.12") (h "0xnw737dk3y172siwlw3vzwrhzr97bl0fkk9qn5xhrhbxv34ziz8")))

(define-public crate-fl2rust-0.1.13 (c (n "fl2rust") (v "0.1.13") (h "0is8w6313xbqhm6p82s6hphz5cm8a3k9n4d8p2437z5xcx8gj4zk")))

(define-public crate-fl2rust-0.1.14 (c (n "fl2rust") (v "0.1.14") (h "15sgysravds9p97a8lkbpzqgl41b4am9d372cws9lrhldmmvnzhr")))

(define-public crate-fl2rust-0.2.0 (c (n "fl2rust") (v "0.2.0") (h "0pmmj30ibsy894di0xi518n4g4gb3s4pwv3njr1rfz3cqh45z7r7")))

(define-public crate-fl2rust-0.2.1 (c (n "fl2rust") (v "0.2.1") (h "0vkd6bx86ppfqrv6ig5v3dyrd9x7g4h68g22y1npbl7q8d8f1b67")))

(define-public crate-fl2rust-0.2.2 (c (n "fl2rust") (v "0.2.2") (h "17k70g1s4fd3ln3k6jrja0mknrpkc4kp87mib9d614wlxnf5vpql")))

(define-public crate-fl2rust-0.2.3 (c (n "fl2rust") (v "0.2.3") (h "04sfin2w0qy4xr5zllhwffwinzmbfnm1zrin73snj3ajjm3wdqhz")))

(define-public crate-fl2rust-0.2.4 (c (n "fl2rust") (v "0.2.4") (h "0b3bva13d6k69bxyiwkfpaz1n9l5asw67rn0rmiirs7ns8sqs3kj")))

(define-public crate-fl2rust-0.2.5 (c (n "fl2rust") (v "0.2.5") (h "00lps74qf45p89n3ksk2z4pk7nyk0450hrnhipx8n4wpfbfwb1xl")))

(define-public crate-fl2rust-0.2.6 (c (n "fl2rust") (v "0.2.6") (h "1q10vvbpsmwzh0cn578k450d2j0g1g9l8vnwkcqz3madxglj5mli")))

(define-public crate-fl2rust-0.2.7 (c (n "fl2rust") (v "0.2.7") (h "0mpc6lsxs6zf9lpvx479pkg9dmid8akazmhp3yc0rh6818r9apg6")))

(define-public crate-fl2rust-0.2.8 (c (n "fl2rust") (v "0.2.8") (h "1sjkcxf8gxa3ny1yz6777f6pa312pw83ahdvm6dyqkw33wlvy6sk")))

(define-public crate-fl2rust-0.2.9 (c (n "fl2rust") (v "0.2.9") (h "1vig1b9fmv8pnkc2a0xwwwrkmnn8hbnm6zslbikhfpz70sx769pp")))

(define-public crate-fl2rust-0.3.0 (c (n "fl2rust") (v "0.3.0") (h "1qkjias50yyhkan4d2hd54rx88bv1017w71r2dgl7y7hwrcisfvi")))

(define-public crate-fl2rust-0.3.1 (c (n "fl2rust") (v "0.3.1") (d (list (d (n "fltk") (r "^0.14") (d #t) (k 2)))) (h "0nwwwmr1sj7x587cvk0l9hbbldfr1l6v2knr0fngg0mzb5p792ib")))

(define-public crate-fl2rust-0.3.2 (c (n "fl2rust") (v "0.3.2") (d (list (d (n "fltk") (r "^0.15") (d #t) (k 2)))) (h "0m1rbaf22r1ahbgsnlc18581kypd17zdw8637w1y48k7sjczp4fk")))

(define-public crate-fl2rust-0.4.0 (c (n "fl2rust") (v "0.4.0") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "1apfpbi3vlm2ygpqpmliimb9z3db4vzn8d52hllm4fk2d3c9z3fk")))

(define-public crate-fl2rust-0.4.1 (c (n "fl2rust") (v "0.4.1") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "1037ik8zsiijqhryrgl3rmzfiyksqhfsvldr92pyh7i532pibaij")))

(define-public crate-fl2rust-0.4.2 (c (n "fl2rust") (v "0.4.2") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "001vlhxh5sff498c821ihzdj67ghfr4l46lmz0v6rjn6xf0dqrgk")))

(define-public crate-fl2rust-0.4.3 (c (n "fl2rust") (v "0.4.3") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "1kllj71f5xwah9d58xx3b2z9pcpwhf93bfhgg3a5n6p40sa9iafq")))

(define-public crate-fl2rust-0.4.4 (c (n "fl2rust") (v "0.4.4") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "05f2pbpb65ag9ghnfpssy9q4c0gnsb85wkmccqgfg8cvjhv33xv3")))

(define-public crate-fl2rust-0.4.5 (c (n "fl2rust") (v "0.4.5") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "16fls3k19zqap2fx0k8zdj1rms1dm2l3vi9y7lf0mylm61mggmfn")))

(define-public crate-fl2rust-0.4.6 (c (n "fl2rust") (v "0.4.6") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "1sws0wa8qwwl4qjswg3qribank4z6izqk7jfd2l7klh3dm6y0vyf")))

(define-public crate-fl2rust-0.4.7 (c (n "fl2rust") (v "0.4.7") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "09nb7ay4ksv1nmrmmqf0bwl580qpk84bjb7x0kmaw6c8n62bqyxi")))

(define-public crate-fl2rust-0.4.8 (c (n "fl2rust") (v "0.4.8") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "1rw6mwx93f4zwc712g2x8shm10lxhwwhj216kxslg8x472hk7lyh")))

(define-public crate-fl2rust-0.4.9 (c (n "fl2rust") (v "0.4.9") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "16285g3zchb19sbi451fpjirjpxp53b3aa1acq8gil3gb8fppjj3")))

(define-public crate-fl2rust-0.4.10 (c (n "fl2rust") (v "0.4.10") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "121563y99j7dv0d0kq9ncdhncqnx0yc6fixljw38ljxhc6paxs5m")))

(define-public crate-fl2rust-0.4.11 (c (n "fl2rust") (v "0.4.11") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "0cn25zn4yphfm1zzhqs1cq6zvym06f5zd4r9vbznrqaikyk55kw2")))

(define-public crate-fl2rust-0.4.12 (c (n "fl2rust") (v "0.4.12") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)))) (h "0c8xl2h0m5rsr94dfjbipq2z12p5warq8pwrh73jh8g95rc16phl")))

(define-public crate-fl2rust-0.4.13 (c (n "fl2rust") (v "0.4.13") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)) (d (n "fltk-form") (r "^0.1") (d #t) (k 2)) (d (n "fltk-form-derive") (r "^0.1") (d #t) (k 2)))) (h "1b04s7f09njjcbasyinlkm65f6wvjz7ww9j9y77zfw3nf8ryr4px")))

(define-public crate-fl2rust-0.4.14 (c (n "fl2rust") (v "0.4.14") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)) (d (n "fltk-form") (r "^0.1") (d #t) (k 2)) (d (n "fltk-form-derive") (r "^0.1") (d #t) (k 2)))) (h "1y00r382z9lypnn83y6d69z8sjy3bccmw5k8n9bmrjcfv2ad0z5i")))

(define-public crate-fl2rust-0.4.15 (c (n "fl2rust") (v "0.4.15") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)) (d (n "fltk-form") (r "^0.1") (d #t) (k 2)) (d (n "fltk-form-derive") (r "^0.1") (d #t) (k 2)))) (h "0m4d4dbvzshanx9kl2icjjkvk5crv1k60fsf8lgd5inyhr2m6shb")))

(define-public crate-fl2rust-0.4.16 (c (n "fl2rust") (v "0.4.16") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)) (d (n "fltk-form") (r "^0.1") (d #t) (k 2)) (d (n "fltk-form-derive") (r "^0.1") (d #t) (k 2)))) (h "1vmnnmhpxxxxl1s91pmwwd5fqca15gnzv0m17689x5yz21i867vz")))

(define-public crate-fl2rust-0.4.17 (c (n "fl2rust") (v "0.4.17") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)) (d (n "fltk-form") (r "^0.1") (d #t) (k 2)) (d (n "fltk-form-derive") (r "^0.1") (d #t) (k 2)))) (h "043f677s73bvfrjgyq2hshazz9qf8xs41bahvlm6pcgga02531wf")))

(define-public crate-fl2rust-0.4.18 (c (n "fl2rust") (v "0.4.18") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)) (d (n "fltk-form") (r "^0.1") (d #t) (k 2)) (d (n "fltk-form-derive") (r "^0.1") (d #t) (k 2)))) (h "1lc9zjnkl01mp38yybi8vgy2mkcszzyxb27bph11b0z4kw9r32qy")))

(define-public crate-fl2rust-0.4.19 (c (n "fl2rust") (v "0.4.19") (d (list (d (n "fltk") (r "^1") (d #t) (k 2)) (d (n "fltk-form") (r "^0.1") (d #t) (k 2)) (d (n "fltk-form-derive") (r "^0.1") (d #t) (k 2)))) (h "16c05xzw1mx1mhrag3mmfq10b93v2mzib6wkq7ib39ail4bf5bl2")))

(define-public crate-fl2rust-0.5.0 (c (n "fl2rust") (v "0.5.0") (d (list (d (n "fluid-parser") (r "^0.1.6") (d #t) (k 0)))) (h "1j0sd26n822r8w2l06348f32jqjnqxa37fc8bx7ksssb6n6z1m0j") (y #t) (r "1.63")))

(define-public crate-fl2rust-0.5.1 (c (n "fl2rust") (v "0.5.1") (d (list (d (n "fluid-parser") (r "^0.1.6") (d #t) (k 0)))) (h "0384cd2mx5ylk1rjgpyhnhw7v7v2gah737a1yfkiazr9qgs5cnnd") (y #t) (r "1.63")))

(define-public crate-fl2rust-0.5.2 (c (n "fl2rust") (v "0.5.2") (d (list (d (n "fluid-parser") (r "^0.1.6") (d #t) (k 0)))) (h "1plr9r1gvqk4zqiq2hr1ia7vm5zw5rw62hb1qmdrlhm8i3nbkx41") (y #t) (r "1.63")))

(define-public crate-fl2rust-0.5.3 (c (n "fl2rust") (v "0.5.3") (d (list (d (n "fluid-parser") (r "^0.1.6") (d #t) (k 0)))) (h "0ciwc82irck97my3k8v8cxvnixkxmcvjz3qra415la5ba8s6sihy") (y #t) (r "1.63")))

(define-public crate-fl2rust-0.5.4 (c (n "fl2rust") (v "0.5.4") (d (list (d (n "fluid-parser") (r "^0.1.6") (d #t) (k 0)))) (h "0mnmbhybw2pjj11gh8zgafxvvvjqjf5jc8n64kfrxpn37s3jssxs") (y #t) (r "1.63")))

(define-public crate-fl2rust-0.5.5 (c (n "fl2rust") (v "0.5.5") (d (list (d (n "fluid-parser") (r "^0.1.6") (d #t) (k 0)))) (h "1pkyjb8klsz4f9g9kbs97q9sicwnmdrx6sdcgh9y9d47gd56f5aj") (y #t) (r "1.63")))

(define-public crate-fl2rust-0.5.6 (c (n "fl2rust") (v "0.5.6") (d (list (d (n "fluid-parser") (r "^0.1.6") (d #t) (k 0)))) (h "0i1c1a6hzxa0jlri1sk4939ki99jwd7db5gkcsbfyvx5vzrix040") (r "1.63")))

(define-public crate-fl2rust-0.5.7 (c (n "fl2rust") (v "0.5.7") (d (list (d (n "fluid-parser") (r "^0.1.9") (d #t) (k 0)))) (h "0l2pvg2vwf04i0xhg6h1y52hmj368c6mwxrcnlcq3g7wjc8qrvdn") (r "1.63")))

(define-public crate-fl2rust-0.5.8 (c (n "fl2rust") (v "0.5.8") (d (list (d (n "fluid-parser") (r "^0.1.9") (d #t) (k 0)))) (h "1342iwvlbvmwaqh8a0053i47jqc9hcqd5d73xfzqqas7fn14mgs5") (r "1.63")))

(define-public crate-fl2rust-0.5.9 (c (n "fl2rust") (v "0.5.9") (d (list (d (n "fluid-parser") (r "^0.1.9") (d #t) (k 0)))) (h "0jz0jwshmih5hzqh6bm8ib0jxcza5bfqbq3r6a1ncljyimhyp7kp") (r "1.63")))

(define-public crate-fl2rust-0.5.10 (c (n "fl2rust") (v "0.5.10") (d (list (d (n "fluid-parser") (r "^0.1.10") (d #t) (k 0)))) (h "1pwj8cv4dbcq3wb5ralvyv8cv9adi226px50g3ncw2bi9lwl62mk") (r "1.63")))

(define-public crate-fl2rust-0.5.11 (c (n "fl2rust") (v "0.5.11") (d (list (d (n "fluid-parser") (r "^0.1.12") (d #t) (k 0)))) (h "0wklzhj8rciwvpal9c2bvkb44lpraivj4fhviiwkvhpmgs6krclf") (r "1.63")))

(define-public crate-fl2rust-0.5.12 (c (n "fl2rust") (v "0.5.12") (d (list (d (n "fluid-parser") (r "^0.1.12") (d #t) (k 0)))) (h "0fvzvsgyv7l9hpbn6q7ar5cjv6mgbl2sr79acp3jw48k5gavp388") (r "1.63")))

(define-public crate-fl2rust-0.5.13 (c (n "fl2rust") (v "0.5.13") (d (list (d (n "fluid-parser") (r "^0.1.12") (d #t) (k 0)))) (h "0ssprb20scc8869k3ffrxbq9wx8y1y7zqgxmvl32js351vyva0vz") (r "1.63")))

(define-public crate-fl2rust-0.5.14 (c (n "fl2rust") (v "0.5.14") (d (list (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "14hdym18mgxr478m95x0pg4608dqq8y82g7x4syrfnj815dwf82f") (r "1.63")))

(define-public crate-fl2rust-0.5.15 (c (n "fl2rust") (v "0.5.15") (d (list (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "1nii2kpxi9wwmjrckfzb8v0apb56h9d4zpz9higs9hyv4i4nlj2v") (r "1.63")))

(define-public crate-fl2rust-0.5.16 (c (n "fl2rust") (v "0.5.16") (d (list (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "10641k778gql4zsfhr6pks90pns4l1fs2vmv4dl64mdsqsah43y1") (r "1.63")))

(define-public crate-fl2rust-0.5.17 (c (n "fl2rust") (v "0.5.17") (d (list (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "03qx0hb180wwhz7zy5gqvrk1l5ag5naahxvbg3dk86pw2m7dbx5p") (r "1.63")))

(define-public crate-fl2rust-0.5.18 (c (n "fl2rust") (v "0.5.18") (d (list (d (n "fluid-parser") (r "^0.1.14") (d #t) (k 0)))) (h "0vn0hcgkk3987522vhrpvp1a0863mhlgqphclk1lpynqsv87skyc") (r "1.63")))

(define-public crate-fl2rust-0.5.19 (c (n "fl2rust") (v "0.5.19") (d (list (d (n "fluid-parser") (r "^0.1.15") (d #t) (k 0)))) (h "0psr015kk8rf8a2vl6pqp89z8nyibw4a1biw9y2yww1fmq294rzn") (r "1.63")))

