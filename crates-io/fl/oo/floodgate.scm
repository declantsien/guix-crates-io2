(define-module (crates-io fl oo floodgate) #:use-module (crates-io))

(define-public crate-floodgate-0.1.0 (c (n "floodgate") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync" "rt"))) (o #t) (d #t) (k 0)))) (h "0brhr9lrmjnjz9sj240hmbszgfg9q3xf5cl40naj3cv7nghkp0ns") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-floodgate-0.1.1 (c (n "floodgate") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync" "rt"))) (o #t) (d #t) (k 0)))) (h "1f6wwcmshhhnmai52yrknyxif45kdwzya75hbbdjawn3fm31axjd") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-floodgate-0.2.0 (c (n "floodgate") (v "0.2.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)))) (h "1bckf2djzf1yxgjiflgbfn48ayxhcmg94dhqys85z7n44zds866p")))

(define-public crate-floodgate-0.2.1 (c (n "floodgate") (v "0.2.1") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)))) (h "0j5qnbb9zzxd1ra8119pdfi16p42p6s87601vffvrbjxz7zz0n04")))

(define-public crate-floodgate-0.2.2 (c (n "floodgate") (v "0.2.2") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)))) (h "0ygbvbiiqiw4jd57j3nlzz9ia4i9n00r926j11ingj6629zqpcll")))

(define-public crate-floodgate-0.3.0 (c (n "floodgate") (v "0.3.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)))) (h "1k23xqhgad9m7nwm073hpn24g3z7hnyjhharm18q7pjl8s7yycn5")))

(define-public crate-floodgate-0.4.0 (c (n "floodgate") (v "0.4.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)))) (h "18pxsqn80njh7mqvsjnmw0gh8ihznj8qxwy4pgh782hx1ls2i8iz")))

(define-public crate-floodgate-0.5.0 (c (n "floodgate") (v "0.5.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)))) (h "0nan3rxva0j3w7savlanlsmbzbw89gj4wj0jpn2hz6g6c93rx6ia")))

(define-public crate-floodgate-0.5.1 (c (n "floodgate") (v "0.5.1") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)))) (h "1qpj891iy95l34nq09s6r80sygdybybgzgzn3s60xlandb0pf2mi")))

