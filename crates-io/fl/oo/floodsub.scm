(define-module (crates-io fl oo floodsub) #:use-module (crates-io))

(define-public crate-floodsub-0.27.0 (c (n "floodsub") (v "0.27.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tet-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tet-libp2p-swarm") (r "^0.27.0") (d #t) (k 0)))) (h "1wyxl43q8qwlqs2910vs3z8712q7ycbdyzzlzp8ycnizkymcwqj7")))

