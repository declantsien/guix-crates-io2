(define-module (crates-io fl oo flood-tide-gen) #:use-module (crates-io))

(define-public crate-flood-tide-gen-0.1.0 (c (n "flood-tide-gen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0hjgzsqwz9p1l4wdqz2gx2z21cpi3w1fs33sxqsc55j881kx5gak")))

(define-public crate-flood-tide-gen-0.1.1 (c (n "flood-tide-gen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1c6d49d3810k0gk17swn8269wpm4ndd099vfq8nrrg7lzs8sbwnb")))

(define-public crate-flood-tide-gen-0.1.2 (c (n "flood-tide-gen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "016jscm0d4frw6jg7hvyjb3sdkfjbl5fg3j7qcqf3sm271f3c420")))

(define-public crate-flood-tide-gen-0.1.3 (c (n "flood-tide-gen") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0ml9gnlgq9dgp89man8anaw8w5vvsqr927r86418ax7y5l2alczk")))

(define-public crate-flood-tide-gen-0.1.4 (c (n "flood-tide-gen") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0yai62wpq9kbm66al0arqxkcx64fx1yjqk6r0634slpwcy22zwyn")))

(define-public crate-flood-tide-gen-0.1.5 (c (n "flood-tide-gen") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0xd39knsdzg8siq2190mny1jzl6s14jhppbd5lj9m54f7msibi8q")))

(define-public crate-flood-tide-gen-0.1.6 (c (n "flood-tide-gen") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "05kmkm2lp4b4y467jappk503z3gpkyjpl2z44q5ac78cl6l4dbwh")))

(define-public crate-flood-tide-gen-0.1.7 (c (n "flood-tide-gen") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "19rl207lqr7ph3kmvlqc7w70sp2j0yxxc4glzj0ba0wv43jiyznw")))

(define-public crate-flood-tide-gen-0.1.8 (c (n "flood-tide-gen") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "125gfdw79a22dkhkh6851xwqx76gz3nb8ikmlwd9gbq5pnq4y7cg")))

(define-public crate-flood-tide-gen-0.1.9 (c (n "flood-tide-gen") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1mc6qjbmy4yz0fiq2spazl105cz2jyhaicfh71nv6cbjr43fzb1d")))

(define-public crate-flood-tide-gen-0.1.10 (c (n "flood-tide-gen") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0ygij5f8cqbg7dg9hm3syp14b97sga0vlx5fb9q184cyksk1i7j6")))

(define-public crate-flood-tide-gen-0.1.11 (c (n "flood-tide-gen") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "00qfa63dd8ma5844k9dsa39mbzm11xhx36gxb7id17gmpdx416kj")))

(define-public crate-flood-tide-gen-0.1.12 (c (n "flood-tide-gen") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0mba8x8xb9szisv1hjd712qgmbl90zwi35gykb7w512hpvd4vy9p")))

(define-public crate-flood-tide-gen-0.1.13 (c (n "flood-tide-gen") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "07ab5p3dvyxsd91mdz9k0ngx8wana2xb0p79jcm3aigs9i6d8rbv")))

(define-public crate-flood-tide-gen-0.1.14 (c (n "flood-tide-gen") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0w8fzx0kplgs30msvc9lw0wnjka3vdlbk82pf4z78lcr6pgm8rw6")))

(define-public crate-flood-tide-gen-0.1.15 (c (n "flood-tide-gen") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0vlv3r24rs4c0bg4hmxl37m5xi3xdkhlc11jfqr9p6ffh63z9vd2")))

(define-public crate-flood-tide-gen-0.1.16 (c (n "flood-tide-gen") (v "0.1.16") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1wz3c14hj2a9jwc82m5lsc60i9fn4bki5vxvvy22fhvxlcvg0fyi")))

(define-public crate-flood-tide-gen-0.1.17 (c (n "flood-tide-gen") (v "0.1.17") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1yxmcnxzby62aipy80lcixvvk349i6i065lp43lpq9irmmjfpm3r")))

(define-public crate-flood-tide-gen-0.1.18 (c (n "flood-tide-gen") (v "0.1.18") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0bhbpl75wmshdk92gbk40lkks7fz85vkf75b64qb3gfnrvhf8cip")))

(define-public crate-flood-tide-gen-0.1.19 (c (n "flood-tide-gen") (v "0.1.19") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "16hgy8pmk8n73r8l35aad959dx9yri8j90sc6f8f3k94v6ga95dg") (r "1.57.0")))

(define-public crate-flood-tide-gen-0.1.20 (c (n "flood-tide-gen") (v "0.1.20") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0a1bgmharaygd17ykh9gbwb820qk5y9m6dd2m93idbw02fbv2gy1") (r "1.57.0")))

(define-public crate-flood-tide-gen-0.1.21 (c (n "flood-tide-gen") (v "0.1.21") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0504bvv37gnkpjqbsif688qd1315wb9ns50is3q59xlxbim8j4dj") (r "1.56.0")))

