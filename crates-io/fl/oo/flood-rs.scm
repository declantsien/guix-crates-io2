(define-module (crates-io fl oo flood-rs) #:use-module (crates-io))

(define-public crate-flood-rs-0.0.1 (c (n "flood-rs") (v "0.0.1") (h "11ldwk43dh1qkn88yaa5dsxcg12lr76sc6gs9pn57ik9nxy72hpf")))

(define-public crate-flood-rs-0.0.2 (c (n "flood-rs") (v "0.0.2") (h "0q63pamfb3kknxxp283xdj971gclg4fzqsql5182s86x8x279d27")))

(define-public crate-flood-rs-0.0.3 (c (n "flood-rs") (v "0.0.3") (h "150kpkv715r5q1hbc0a4hmcq4rz44vf36fgbdr1la8kffmc8zqpi")))

(define-public crate-flood-rs-0.0.4 (c (n "flood-rs") (v "0.0.4") (h "0g1vsnr0frfw8izsgb804d3hj773lhrhvlixbbv5swfr5074i1xs")))

(define-public crate-flood-rs-0.0.5 (c (n "flood-rs") (v "0.0.5") (h "1g0vxxlzg18n133i323cc61dn7jmiqki716yy6v7p680dp59jffy")))

