(define-module (crates-io fl oo flood) #:use-module (crates-io))

(define-public crate-flood-0.1.0 (c (n "flood") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1y9nzpj6mybil1iz0ghwnfnisi2dalhraygz3xybsj005rx221gc")))

