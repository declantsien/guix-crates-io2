(define-module (crates-io fl oo floom) #:use-module (crates-io))

(define-public crate-floom-1.0.0 (c (n "floom") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jbcn33djgxqb63smf3bgcmlsjb7mkz2d2w8jk1rn2imvfa3j2rs")))

