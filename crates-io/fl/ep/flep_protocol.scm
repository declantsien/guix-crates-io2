(define-module (crates-io fl ep flep_protocol) #:use-module (crates-io))

(define-public crate-flep_protocol-0.1.0 (c (n "flep_protocol") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)))) (h "1bkg0qghlhpsw357633nriafvb142a3p5nk5v4mps76xxj0yz3id")))

(define-public crate-flep_protocol-0.1.1 (c (n "flep_protocol") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "rfc1700") (r "^0.1") (d #t) (k 0)))) (h "1rcl7xxhgc9n7xjd7xll1bfibyczzcqsf2sgspvd2z092gxlvdkb")))

(define-public crate-flep_protocol-0.1.2 (c (n "flep_protocol") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "rfc1700") (r "^0.1") (d #t) (k 0)))) (h "13y65zf74l8hqy73iqn71j71wifb4lcdr3h4333i39v9fvg1f3kp")))

(define-public crate-flep_protocol-0.2.0 (c (n "flep_protocol") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "rfc1700") (r "^0.1") (d #t) (k 0)))) (h "171fh2z7l2g51x90q3wrlid1z305ppgk7rb2yxgcnf6w5g0pv65f")))

