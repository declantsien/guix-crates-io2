(define-module (crates-io fl ep flep) #:use-module (crates-io))

(define-public crate-flep-0.1.0 (c (n "flep") (v "0.1.0") (d (list (d (n "flep_protocol") (r "^0.1.0") (d #t) (k 0)))) (h "188a9xy5xjacd82zvhnsp67xi2xswdgi66f3x5971bm7pxm6fn1j")))

(define-public crate-flep-0.1.1 (c (n "flep") (v "0.1.1") (d (list (d (n "flep_protocol") (r "^0.1.1") (d #t) (k 0)) (d (n "mio") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "00rp90iwg84lvacn2sg899wf68c6p1gwfhr2l79rbafand87wncv")))

(define-public crate-flep-0.1.2 (c (n "flep") (v "0.1.2") (d (list (d (n "flep_protocol") (r "^0.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "1p2wbfw9xacp6zks00hb88wf1vy1z3vbapl05d2shx0hxv8fpfzl")))

(define-public crate-flep-0.2.0 (c (n "flep") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "flep_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "1mkp4zxy5bqahfyxxgv1l5gcmazgk1nappfbwjb9c9331pjzvjdn")))

