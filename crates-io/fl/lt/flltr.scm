(define-module (crates-io fl lt flltr) #:use-module (crates-io))

(define-public crate-flltr-0.2.1 (c (n "flltr") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)))) (h "03nflvw68jy8icaf4w4w32zbwyrrxmvvkak0nxblqs4qcldnpk9n")))

