(define-module (crates-io fl ux fluxbeam-crate) #:use-module (crates-io))

(define-public crate-fluxbeam-crate-0.1.0 (c (n "fluxbeam-crate") (v "0.1.0") (d (list (d (n "ahash") (r "=0.8.6") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.28.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "solana-program") (r "=1.16.20") (d #t) (k 0)) (d (n "winnow") (r "=0.4.1") (d #t) (k 0)))) (h "1bx36df95q3rmf3pmwhq0l10h39qbni4dg0sm03s4nyy45ishbyw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

