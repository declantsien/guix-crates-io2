(define-module (crates-io fl ux fluxer) #:use-module (crates-io))

(define-public crate-fluxer-1.1.1 (c (n "fluxer") (v "1.1.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1rfiw05frascj347mw2n5j8hcn4kd6aw7p74cf076v39wzbiqjp2")))

(define-public crate-fluxer-1.2.0 (c (n "fluxer") (v "1.2.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0ym1lzz5908qhyjikg7j25izqcqihb0icknaxqzcvxkhnvg28vnf")))

