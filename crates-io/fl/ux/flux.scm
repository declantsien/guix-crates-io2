(define-module (crates-io fl ux flux) #:use-module (crates-io))

(define-public crate-flux-1.0.0 (c (n "flux") (v "1.0.0") (d (list (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7") (d #t) (k 0)))) (h "0vg3wrv7fm0px3y1adqm75bhks44r02f9yyfxn0a09ln1jgiifiv")))

(define-public crate-flux-1.0.1 (c (n "flux") (v "1.0.1") (d (list (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7") (d #t) (k 0)))) (h "1340kaahcr6cik6r6w9xyc5hzzbaliwn0lmy6wkh6mgy24lkyadl")))

(define-public crate-flux-1.0.2 (c (n "flux") (v "1.0.2") (d (list (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7") (d #t) (k 0)))) (h "0hc186rwif60qbwpwybkyyrrdgwfakcr9c3c66ry310j1zzi0zzg")))

(define-public crate-flux-1.0.3 (c (n "flux") (v "1.0.3") (d (list (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7") (d #t) (k 0)))) (h "1hr0iafvaw9c34nww2885wnlksdb5cxjrmpbqxzg21cxffxh1zi3")))

(define-public crate-flux-1.0.4 (c (n "flux") (v "1.0.4") (d (list (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7") (d #t) (k 0)))) (h "0zllyi0spxybfhykmw6kxfwh54wmprk6ay8wia5mg7h5s8m1z1xk")))

(define-public crate-flux-1.0.5 (c (n "flux") (v "1.0.5") (d (list (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7") (d #t) (k 0)))) (h "1f8gsr6hiw06l1y47bf0p8yhfy4ax1w74p4i0h9mhnkzmhiwz5x1")))

(define-public crate-flux-1.0.7 (c (n "flux") (v "1.0.7") (d (list (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7") (d #t) (k 0)))) (h "0cjbggpb082ks5d45m34k2i9v4sbh23v960m3hnvribxdq4dphkn")))

(define-public crate-flux-2.0.0 (c (n "flux") (v "2.0.0") (d (list (d (n "hyper") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7") (d #t) (k 0)))) (h "1f4w47ych9wyl5hqkr9nwqpc2y12qcycg8072nrl8d1c51lbn037")))

(define-public crate-flux-2.0.3 (c (n "flux") (v "2.0.3") (d (list (d (n "hyper") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "~0.7") (d #t) (k 1)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)))) (h "0dxdr07bghs60y2p6zgswy662pbnkzbj9vg58m9lbndb564pjw3c")))

(define-public crate-flux-3.0.0 (c (n "flux") (v "3.0.0") (d (list (d (n "hyper") (r "~0.9") (d #t) (k 0)) (d (n "json") (r "~0.7") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "1vg6b9ysybya7a5dmln43vjn7dx6yf5dczm25fzf8sbqy1jn2v90")))

(define-public crate-flux-3.0.1 (c (n "flux") (v "3.0.1") (d (list (d (n "hyper") (r "~0.9") (d #t) (k 0)) (d (n "json") (r "~0.7") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "080jicqljgxhphxfcdy2a4ifawm72dgxhmpn4vdsjc5ishfr7s67")))

