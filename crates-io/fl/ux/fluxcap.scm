(define-module (crates-io fl ux fluxcap) #:use-module (crates-io))

(define-public crate-fluxcap-0.0.1 (c (n "fluxcap") (v "0.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "earlgrey") (r "^0.0.5") (d #t) (k 0)) (d (n "kronos") (r "^0.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "lexers") (r "^0.0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "027arlj9ski677kzp53y0d6pbw6xpdq3l9jmp0w2ci4cx94vym73")))

(define-public crate-fluxcap-0.0.2 (c (n "fluxcap") (v "0.0.2") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "earlgrey") (r "^0.0.7") (d #t) (k 0)) (d (n "kronos") (r "^0.0.4") (d #t) (k 0)) (d (n "lexers") (r "^0.0.5") (d #t) (k 0)))) (h "0lsxbc34701hsmvds1c4gi6qngjx6q96fnmk4wy86lpm0v6zb3yc")))

(define-public crate-fluxcap-0.0.3 (c (n "fluxcap") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.0.7") (d #t) (k 0)) (d (n "kronos") (r "^0.0.5") (d #t) (k 0)) (d (n "lexers") (r "^0.0.5") (d #t) (k 0)))) (h "16knqd12smy6954d2a689i6giahzik1qg6k0y3si67h2sqcrhhag")))

(define-public crate-fluxcap-0.0.4 (c (n "fluxcap") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.0.7") (d #t) (k 0)) (d (n "kronos") (r "^0.0.5") (d #t) (k 0)) (d (n "lexers") (r "^0.0.5") (d #t) (k 0)))) (h "0p7vcjb2sg90smn37715xg82ys9018qlfcdqhpamd84f7217pmgj")))

(define-public crate-fluxcap-0.0.5 (c (n "fluxcap") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.1.0") (d #t) (k 0)) (d (n "kronos") (r "^0.0.5") (d #t) (k 0)) (d (n "lexers") (r "^0.0.5") (d #t) (k 0)))) (h "1xl3p5gvw42yybxvmb08gyk9jsxw0cp03z32pw8i8jnhdvhjvjfb")))

(define-public crate-fluxcap-0.0.6 (c (n "fluxcap") (v "0.0.6") (d (list (d (n "abackus") (r "^0.1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.2.1") (d #t) (k 0)) (d (n "kronos") (r "^0.1.1") (d #t) (k 0)) (d (n "lexers") (r "^0.0.7") (d #t) (k 0)))) (h "1vwl1kkhgzaj96q2sd07kl1j2idc1zw05b40jxglk8fk8wzcixf6")))

(define-public crate-fluxcap-0.0.7 (c (n "fluxcap") (v "0.0.7") (d (list (d (n "abackus") (r "^0.1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.2.1") (d #t) (k 0)) (d (n "kronos") (r "^0.1.1") (d #t) (k 0)) (d (n "lexers") (r "^0.0.7") (d #t) (k 0)))) (h "0f63npaadas8ggid594vd0bhc7yn4gm5kgaaj4y59fd2dlz75kbm")))

(define-public crate-fluxcap-0.0.8 (c (n "fluxcap") (v "0.0.8") (d (list (d (n "abackus") (r "^0.1.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.2.1") (d #t) (k 0)) (d (n "kronos") (r "^0.1.1") (d #t) (k 0)) (d (n "lexers") (r "^0.0.7") (d #t) (k 0)))) (h "1p6r6dy52rbdpsvp5zzx3kszr2x3akp6p1yys52wf7a8r7wqqplm")))

(define-public crate-fluxcap-0.0.9 (c (n "fluxcap") (v "0.0.9") (d (list (d (n "abackus") (r "^0.1.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.2.3") (d #t) (k 0)) (d (n "kronos") (r "^0.1.1") (d #t) (k 0)) (d (n "lexers") (r "^0.0.8") (d #t) (k 0)))) (h "0c794imr12v3i4n8x9pi2dcpnhpwlb4pn1101zww9zkxj9h61ibn")))

(define-public crate-fluxcap-0.0.10 (c (n "fluxcap") (v "0.0.10") (d (list (d (n "abackus") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.2.4") (d #t) (k 0)) (d (n "kronos") (r "^0.1.2") (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)))) (h "0333c7j2yyljspqshpx4jp70mljyxgbczcadqav1i934xja9kzd3")))

(define-public crate-fluxcap-0.0.11 (c (n "fluxcap") (v "0.0.11") (d (list (d (n "abackus") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.3") (d #t) (k 0)) (d (n "kronos") (r "^0.1.2") (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)))) (h "1gil3dhpmfr5jnp1h752agk7d5jwgz8iksvvjgnjfa5kjlcrrl40")))

(define-public crate-fluxcap-0.0.12 (c (n "fluxcap") (v "0.0.12") (d (list (d (n "abackus") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.3") (d #t) (k 0)) (d (n "kronos") (r "^0.1.3") (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)))) (h "0h68865a7kzxf00nw02qc8cqjgv9pz3qgvh0rria0qgzsh56sa5l")))

(define-public crate-fluxcap-0.0.13 (c (n "fluxcap") (v "0.0.13") (d (list (d (n "abackus") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.3") (d #t) (k 0)) (d (n "kronos") (r "^0.1.4") (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)))) (h "0g028ahilgl3rji6j2hyrsnd7zzbgp9ln260zckbxz2yr1zj8y4z")))

(define-public crate-fluxcap-0.0.14 (c (n "fluxcap") (v "0.0.14") (d (list (d (n "abackus") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.3") (d #t) (k 0)) (d (n "kronos") (r "^0.1.4") (d #t) (k 0)) (d (n "lexers") (r "^0.1.3") (d #t) (k 0)))) (h "0cjyhg7gr2k436mkbm1idb9rv3qkn6slisxpiwszg97y5ay32cc6")))

(define-public crate-fluxcap-0.0.15 (c (n "fluxcap") (v "0.0.15") (d (list (d (n "abackus") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.3") (d #t) (k 0)) (d (n "kronos") (r "^0.1.4") (d #t) (k 0)) (d (n "lexers") (r "^0.1.3") (d #t) (k 0)))) (h "12vbm6ws11431j597bd0ry7qds419y9qam514s58jbjkja277b8h")))

(define-public crate-fluxcap-0.0.16 (c (n "fluxcap") (v "0.0.16") (d (list (d (n "abackus") (r "^0.2.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.3.1") (d #t) (k 0)) (d (n "kronos") (r "^0.1.5") (d #t) (k 0)) (d (n "lexers") (r "^0.1.4") (d #t) (k 0)))) (h "0j0mhrsnv0svvqsqbg688y11wz1i86z93d61wxfdpq94y9iv6nv5")))

(define-public crate-fluxcap-0.1.0 (c (n "fluxcap") (v "0.1.0") (d (list (d (n "abackus") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.3") (d #t) (k 0)) (d (n "kronos") (r "^0.1") (d #t) (k 0)) (d (n "lexers") (r "^0.1") (d #t) (k 0)))) (h "0l8kab1iwbc7lrq3hbl7fl51fgjj00clhxy4k6wd2na0xrzjvwd4")))

(define-public crate-fluxcap-0.1.1 (c (n "fluxcap") (v "0.1.1") (d (list (d (n "abackus") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "earlgrey") (r "^0.3") (d #t) (k 0)) (d (n "kronos") (r "^0.1") (d #t) (k 0)) (d (n "lexers") (r "^0.1") (d #t) (k 0)))) (h "0gdjvq9pmn53ip336ll5k1hwggc2618i0iqyikz7455zzi1q78k2")))

