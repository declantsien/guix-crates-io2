(define-module (crates-io fl ux flux-crds) #:use-module (crates-io))

(define-public crate-flux-crds-0.1.0 (c (n "flux-crds") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.15.0") (f (quote ("v1_20"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1jr6yyjb4mwd0lkr9mfm92n65dz36k354p9cd4hx8qxbfjhr81nh")))

