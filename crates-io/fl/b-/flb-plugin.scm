(define-module (crates-io fl b- flb-plugin) #:use-module (crates-io))

(define-public crate-flb-plugin-0.1.0 (c (n "flb-plugin") (v "0.1.0") (d (list (d (n "flb-plugin-sys") (r "^0.1") (d #t) (k 0)))) (h "1xkvn5zrccc8d9jl9hpwpwkycvdsj17zs6i4g8698vbgq55lsfka")))

(define-public crate-flb-plugin-0.1.1 (c (n "flb-plugin") (v "0.1.1") (d (list (d (n "flb-plugin-sys") (r "^0.1") (d #t) (k 0)))) (h "1pwzz05s2iwn9ghmysk7bp3iqi8hv1y314225c38hyj7pamqxdq6")))

