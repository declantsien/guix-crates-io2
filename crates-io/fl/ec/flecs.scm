(define-module (crates-io fl ec flecs) #:use-module (crates-io))

(define-public crate-flecs-0.1.0 (c (n "flecs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1cflczxlqd97pa5qlry1bad31xljyjinnad5ssvk3gjqbks62vgl")))

(define-public crate-flecs-0.1.1 (c (n "flecs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ikmi7dwimaaw78cd0cyvhnyk1nn82zswjc7clhqs9nwkf6ys295")))

(define-public crate-flecs-0.1.2 (c (n "flecs") (v "0.1.2") (d (list (d (n "bevy_ecs") (r "^0.10.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "flax") (r "^0.4.0") (d #t) (k 2)) (d (n "hecs") (r "^0.10.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "16p483791rlgrksr6yynwd8jwzapb1zhjqmil4jn77p5kkjrg4pf")))

(define-public crate-flecs-0.1.3 (c (n "flecs") (v "0.1.3") (d (list (d (n "bevy_ecs") (r "^0.10.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "flax") (r "^0.4.0") (d #t) (k 2)) (d (n "hecs") (r "^0.10.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1xnh339xhfmx1d84ka4bcbcbqcdlx74mxbkaxpizmy45ma2jz0i5")))

(define-public crate-flecs-0.1.4 (c (n "flecs") (v "0.1.4") (d (list (d (n "bevy_ecs") (r "^0.10.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "flax") (r "^0.4.0") (d #t) (k 2)) (d (n "flecs-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "hecs") (r "^0.10.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "1fjpfbashkj7qhawbd84q0kqrkl3q5ajywv7rah2hbczfld8zaxp") (f (quote (("export_bindings" "flecs-sys/export_bindings")))) (r "1.68")))

(define-public crate-flecs-0.1.5 (c (n "flecs") (v "0.1.5") (d (list (d (n "bevy_ecs") (r "^0.11.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "flax") (r "^0.5.0") (d #t) (k 2)) (d (n "flecs-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "hecs") (r "^0.10.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "14b2z1922pk3hsszzws0q6y5958hz378gx7g9265iyx07bm8cc2c") (f (quote (("export_bindings" "flecs-sys/export_bindings")))) (r "1.68")))

