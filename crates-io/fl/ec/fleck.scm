(define-module (crates-io fl ec fleck) #:use-module (crates-io))

(define-public crate-fleck-0.1.0 (c (n "fleck") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0brk5ib1cai0chplrrvmym5kiza87g36b31i5sy5s6r1f1zvg1kd")))

(define-public crate-fleck-0.1.1 (c (n "fleck") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "148blszlwn2vz7p4jz9bprs38ykkk99ldv25v026b6pzha2naqfc")))

(define-public crate-fleck-0.2.0 (c (n "fleck") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "095vk37vx03bcn0lcym6qcr4klpbq4fwlqqvf1qrcry9gvrhc3pp") (f (quote (("no_std"))))))

