(define-module (crates-io fl ec flecs-sys) #:use-module (crates-io))

(define-public crate-flecs-sys-0.1.4 (c (n "flecs-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 1)))) (h "1y5qcr7h8vcx62aiv8d1kk3cpvdwchpsk18j2ghrkxxmklbv7xds") (f (quote (("export_bindings")))) (l "flecs") (r "1.68")))

(define-public crate-flecs-sys-0.1.5 (c (n "flecs-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 1)))) (h "0aqaidp8qczjc0qqmalcnqxvn2rkxd3hn252b1jriarqbsgmpcdm") (f (quote (("export_bindings")))) (l "flecs") (r "1.68")))

