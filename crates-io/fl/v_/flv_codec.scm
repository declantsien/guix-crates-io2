(define-module (crates-io fl v_ flv_codec) #:use-module (crates-io))

(define-public crate-flv_codec-0.0.1 (c (n "flv_codec") (v "0.0.1") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1pzss0fky0m40aqlp44z2psvz5zdbvm8587kxcl85iy6xxrk74hp")))

(define-public crate-flv_codec-0.0.2 (c (n "flv_codec") (v "0.0.2") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "11cgzyhh8lg6gvyss46w9mg1ym4zfhmfi9r8719ag60p8bwim63h")))

(define-public crate-flv_codec-0.1.0 (c (n "flv_codec") (v "0.1.0") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0f760jhgi4d7gdyjs4zz1hsnkkql7kbmh0wnrqgl4sz0n631ck6w")))

