(define-module (crates-io fl ol flololoat) #:use-module (crates-io))

(define-public crate-flololoat-0.1.0 (c (n "flololoat") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "13wb81knl7ir42n9zmnkb9jhb4jh9fc1nh9c6kifcgd3qdgfambd")))

