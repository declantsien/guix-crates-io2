(define-module (crates-io fl ys flyslice_minigrep) #:use-module (crates-io))

(define-public crate-flyslice_minigrep-0.1.0 (c (n "flyslice_minigrep") (v "0.1.0") (h "13anlhwl5v2013j8p46ryyhrwxr115jgi7xp16r8mchpz0daa5g1")))

(define-public crate-flyslice_minigrep-0.1.1 (c (n "flyslice_minigrep") (v "0.1.1") (h "1cd3zdlj35fcnkfhphswzs92gszh05x1vsvwxa4hlwmlvyjhnf2k")))

