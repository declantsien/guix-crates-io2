(define-module (crates-io fl yt flyteidl) #:use-module (crates-io))

(define-public crate-flyteidl-0.1.0 (c (n "flyteidl") (v "0.1.0") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)))) (h "0gxp6mz767rfzagk1jianykf09zl0cvsflqj4480rcy6cwlwhcc3")))

