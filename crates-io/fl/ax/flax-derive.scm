(define-module (crates-io fl ax flax-derive) #:use-module (crates-io))

(define-public crate-flax-derive-0.1.0 (c (n "flax-derive") (v "0.1.0") (d (list (d (n "glam") (r "^0.21") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ya8wkgzsiwh8h4vi7b73r2d8xprydmimmw986g57mg9n577a4ds")))

(define-public crate-flax-derive-0.3.0 (c (n "flax-derive") (v "0.3.0") (d (list (d (n "glam") (r "^0.21") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19xrwf2c67c21hc2ad48v89paa3sr25wxkydh2x0g04vpra1hw13")))

(define-public crate-flax-derive-0.4.0 (c (n "flax-derive") (v "0.4.0") (d (list (d (n "glam") (r "^0.21") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1af8vkr2kjcw48bmfskzz0z3szsx8n5r4cx6siv6abhgv03mb8c3")))

(define-public crate-flax-derive-0.5.0 (c (n "flax-derive") (v "0.5.0") (d (list (d (n "glam") (r "^0.24.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "15iivb43mfchazr3kw66839a78yf8qvqnw9jvqw9aqzfg3pby8j8")))

(define-public crate-flax-derive-0.6.0 (c (n "flax-derive") (v "0.6.0") (d (list (d (n "glam") (r "^0.24.2") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1ml6an80iyjlwk0xa62b97csdqp4f8xyj4y53ry83klknydin52x")))

(define-public crate-flax-derive-0.7.0 (c (n "flax-derive") (v "0.7.0") (d (list (d (n "glam") (r "^0.24.2") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1vpv8zx422bf2yzrasfr3rji0q5m60viifyvxmv6h91s6hapfliv")))

(define-public crate-flax-derive-0.7.1 (c (n "flax-derive") (v "0.7.1") (d (list (d (n "glam") (r "^0.24.2") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1chlzvsyis0qgga6qs886gkfpnw0w1q3qx65b90glb5rv94ksws3")))

