(define-module (crates-io fl am flameshot-rs) #:use-module (crates-io))

(define-public crate-flameshot-rs-0.1.0 (c (n "flameshot-rs") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12v5f894pcc2835g2nfrkq9lrg2svpf053cinzb6p6hk7cm3h1kp")))

(define-public crate-flameshot-rs-0.1.1 (c (n "flameshot-rs") (v "0.1.1") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kc8nyhz8qimz56rmckipvxqff7vcbpwf760pbnvrd2pzs2bb1m0")))

(define-public crate-flameshot-rs-0.1.2 (c (n "flameshot-rs") (v "0.1.2") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05417wgbq579d5gc1isdcj8sfk1gw947k04rxsqglddxfn7j9rri")))

(define-public crate-flameshot-rs-1.0.0 (c (n "flameshot-rs") (v "1.0.0") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yrxvw1c9hn73f4pjliw3fglm0lvmkgnc23g4fsrlippkl7i9aw3")))

(define-public crate-flameshot-rs-1.0.1 (c (n "flameshot-rs") (v "1.0.1") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "050c7cb98ilbs718rxzw9rmn1k2fdfwdl092s88bdgflr7jggkbg")))

