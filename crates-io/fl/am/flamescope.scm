(define-module (crates-io fl am flamescope) #:use-module (crates-io))

(define-public crate-flamescope-0.1.0 (c (n "flamescope") (v "0.1.0") (d (list (d (n "flame") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mr889fw0sbn11v0li0sfhsgn4vi408phwsjfcfxdh293s3rjzwi")))

(define-public crate-flamescope-0.1.1 (c (n "flamescope") (v "0.1.1") (d (list (d (n "flame") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dni1pi9qi8g7khjjgvbc3ndk0azxg1cr2rhwixvja2scbp6lgkf")))

(define-public crate-flamescope-0.1.2 (c (n "flamescope") (v "0.1.2") (d (list (d (n "flame") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "insta") (r "^1.7") (f (quote ("redactions"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02c9l4z2gpbwffbhh4w1jgbyw7k7lpfiw0l01qx6v8nzq2k2kk63")))

