(define-module (crates-io fl am flamegraph) #:use-module (crates-io))

(define-public crate-flamegraph-0.1.0 (c (n "flamegraph") (v "0.1.0") (d (list (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "14bkcf17xp3syk43m2886xjahbfyza220s0y985pagqdhyb53h49")))

(define-public crate-flamegraph-0.1.1 (c (n "flamegraph") (v "0.1.1") (d (list (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "13annnnf6isv0vl84x0l6vnvski3d17yqnw64qzgqvk9spsqw7kp")))

(define-public crate-flamegraph-0.1.2 (c (n "flamegraph") (v "0.1.2") (d (list (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0fdjq785s64pghh3m1rw4ad36a27mb4zl02qq318dp2iz99bsrn9")))

(define-public crate-flamegraph-0.1.4 (c (n "flamegraph") (v "0.1.4") (d (list (d (n "cargo_metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "02faii2kah8g5psxfx0s0la6awqwhxpsg6cms5yzbv4zc5cxf04h")))

(define-public crate-flamegraph-0.1.6 (c (n "flamegraph") (v "0.1.6") (d (list (d (n "cargo_metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1imglipvqzg41wnfcs1sz691z5myr8vv6z188mqg5sfl2a6m2yjl")))

(define-public crate-flamegraph-0.1.7 (c (n "flamegraph") (v "0.1.7") (d (list (d (n "cargo_metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0bkznbfrnmbzb07lxd57v8vlq3gffgkwgi04hs57pzyrf2cpzvwc")))

(define-public crate-flamegraph-0.1.8 (c (n "flamegraph") (v "0.1.8") (d (list (d (n "cargo_metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1bsh43i7zal92bzazqzrqcrqgnny85jzdsk53vi48hvz85lci55x")))

(define-public crate-flamegraph-0.1.9 (c (n "flamegraph") (v "0.1.9") (d (list (d (n "cargo_metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0m9b0qra0dmb2iza6193jciz8xx4wiwkzbi9zxmp6d8iy32l0rqa")))

(define-public crate-flamegraph-0.1.10 (c (n "flamegraph") (v "0.1.10") (d (list (d (n "cargo_metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1ch5yb9a235cf0j2nh6zlphigkmb0smhpk8wsdr8viz4ib4rz9vf")))

(define-public crate-flamegraph-0.1.11 (c (n "flamegraph") (v "0.1.11") (d (list (d (n "cargo_metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0afbdb2gxg7knzdyvd34zg78a2cgjjkcpvqc9wkdfa49whpy48vl")))

(define-public crate-flamegraph-0.1.12 (c (n "flamegraph") (v "0.1.12") (d (list (d (n "cargo_metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0257c8lj50vmkbvpkwzcghsa8mk6syl8jczq2nrg7aj4yv3zw2ks")))

(define-public crate-flamegraph-0.1.13 (c (n "flamegraph") (v "0.1.13") (d (list (d (n "cargo_metadata") (r "^0.7.1") (d #t) (k 0)) (d (n "inferno") (r "^0.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1j4fff064nijrb1bffa346bzlflp9mankhqll9xmsk68br4zl9lx")))

(define-public crate-flamegraph-0.2.0 (c (n "flamegraph") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "inferno") (r "^0.9.1") (d #t) (k 0)) (d (n "opener") (r "^0.4.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.12") (d #t) (t "cfg(unix)") (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "1cpz4hycnb1sfsm61rl5s1ddlff3k8846xk0s9dgdc56860dcpgx")))

(define-public crate-flamegraph-0.3.0 (c (n "flamegraph") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "inferno") (r "^0.9.6") (d #t) (k 0)) (d (n "opener") (r "^0.4.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (t "cfg(unix)") (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1nhk5lnb2ckpyr426ph2pgsp499z8cabkcfd7bf7bd3h6sn1rgbl")))

(define-public crate-flamegraph-0.3.1 (c (n "flamegraph") (v "0.3.1") (d (list (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "inferno") (r "^0.9.8") (d #t) (k 0)) (d (n "opener") (r "^0.4.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (t "cfg(unix)") (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1f4my366yrr9w2yb0lwb6kra3ypzpcbs5vvf84a57162d3jafqs2")))

(define-public crate-flamegraph-0.4.0 (c (n "flamegraph") (v "0.4.0") (d (list (d (n "cargo_metadata") (r "^0.11.3") (d #t) (k 0)) (d (n "inferno") (r "^0.10.0") (d #t) (k 0)) (d (n "opener") (r "^0.4.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (t "cfg(unix)") (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0f3c4iiqglq49ylrgiadcab7w2wyl9k1y0l3lvm4ppm001sfamv2")))

(define-public crate-flamegraph-0.5.0 (c (n "flamegraph") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "inferno") (r "^0.10.0") (d #t) (k 0)) (d (n "opener") (r "^0.5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (t "cfg(unix)") (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0y9hciwhv7l4fv50ni3yd6cgcwpndbhi4chjjdpdj0gfbqc37cc0")))

(define-public crate-flamegraph-0.5.1 (c (n "flamegraph") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "inferno") (r "^0.10.8") (d #t) (k 0)) (d (n "opener") (r "^0.5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (t "cfg(unix)") (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "07bzkifpg9q4lcnki9rwzk888xa6c3ll2ywfj5y1gg5v00rldqbn")))

(define-public crate-flamegraph-0.6.0 (c (n "flamegraph") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.0") (d #t) (k 0)) (d (n "inferno") (r "^0.11.0") (f (quote ("multithreaded" "nameattr"))) (k 0)) (d (n "opener") (r "^0.5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (t "cfg(unix)") (k 0)))) (h "1kmlp8f5478rhr4jdbd0l7wijs0jidhn1ac46vxinc3xsw6ih81j")))

(define-public crate-flamegraph-0.6.1 (c (n "flamegraph") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.0") (d #t) (k 0)) (d (n "inferno") (r "^0.11.0") (f (quote ("multithreaded" "nameattr"))) (k 0)) (d (n "opener") (r "^0.5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (t "cfg(unix)") (k 0)))) (h "1z5s7jssal35sng8vlb0yl9aywwza5ynp2j5wiji3cgdmf7dmazx")))

(define-public crate-flamegraph-0.6.2 (c (n "flamegraph") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "blondie") (r "^0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.0") (d #t) (k 0)) (d (n "inferno") (r "^0.11.0") (f (quote ("multithreaded" "nameattr"))) (k 0)) (d (n "opener") (r "^0.5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (t "cfg(unix)") (k 0)))) (h "0m07cnrgdbn31xhr170rjmxv63qdakggg1n7g1164vfzm87csagp")))

(define-public crate-flamegraph-0.6.3 (c (n "flamegraph") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "blondie") (r "^0.4.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 0)) (d (n "inferno") (r "^0.11.0") (f (quote ("multithreaded" "nameattr"))) (k 0)) (d (n "opener") (r "^0.6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (t "cfg(unix)") (k 0)))) (h "0kacc3daiha7pniiibjiz0dvcdvsqlin9pgk0c4vy38f1sdbywmy") (r "1.64")))

(define-public crate-flamegraph-0.6.4 (c (n "flamegraph") (v "0.6.4") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "blondie") (r "^0.4.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "cargo_metadata") (r "^0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 0)) (d (n "inferno") (r "^0.11.0") (f (quote ("multithreaded" "nameattr"))) (k 0)) (d (n "opener") (r "^0.6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (t "cfg(unix)") (k 0)))) (h "0fwc9kx796v5pal8jmax8r0x711hrkkxz19q1n75v5nc88j7d62v") (r "1.64")))

(define-public crate-flamegraph-0.6.5 (c (n "flamegraph") (v "0.6.5") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "blondie") (r "^0.4.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "cargo_metadata") (r "^0.18") (d #t) (k 0)) (d (n "clap") (r "^4.0.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 0)) (d (n "inferno") (r "^0.11.0") (f (quote ("multithreaded" "nameattr"))) (k 0)) (d (n "opener") (r "^0.6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (t "cfg(unix)") (k 0)))) (h "0ksv929pcks754ni75ngx8a3h2r3hlh3h83r1pqd1p8i9svgd89x") (r "1.64")))

