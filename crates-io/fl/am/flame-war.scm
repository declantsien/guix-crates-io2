(define-module (crates-io fl am flame-war) #:use-module (crates-io))

(define-public crate-flame-war-0.1.0 (c (n "flame-war") (v "0.1.0") (h "19vbcrfj0vb51haba68gs664h5dg6npipr2vj9gqzi03sv7fr036")))

(define-public crate-flame-war-0.1.1 (c (n "flame-war") (v "0.1.1") (h "1qyh6wlmzz1a8icwfb2jpd7r4gskgx5y21x1cx04xbp89zggkba4")))

