(define-module (crates-io fl am flame) #:use-module (crates-io))

(define-public crate-flame-0.1.0 (c (n "flame") (v "0.1.0") (h "0j2cdpbsmsa2zkh6qd8mchfwizi2p0g11vpnq44ikxkabym4dab7")))

(define-public crate-flame-0.1.1 (c (n "flame") (v "0.1.1") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)))) (h "008zadrkx4jnmr1x9db9cmsvfh2s23b5lgi2m3af5wbrg7arijrn")))

(define-public crate-flame-0.1.2 (c (n "flame") (v "0.1.2") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)))) (h "0bb7y27bm0agmxwvb5syr3z53si9nj7k540jhcqh2z9iak0q006x")))

(define-public crate-flame-0.1.3 (c (n "flame") (v "0.1.3") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)))) (h "1a6acdwlb3c3cdi78ynlbww8mi5326sndga8w5lb85xpvv7mpq81")))

(define-public crate-flame-0.1.4 (c (n "flame") (v "0.1.4") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)))) (h "1l8wdjgbhcdszrdrpblfsi4mgidwlhkl7d788kgmvz26q4mxrnp4")))

(define-public crate-flame-0.1.5 (c (n "flame") (v "0.1.5") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)))) (h "0lp0p5sm769mk8bj631i5js62ka827s98mw26xa2bj1kgcavn3my")))

(define-public crate-flame-0.1.6 (c (n "flame") (v "0.1.6") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)))) (h "06inpi50i0vv2ki3drxsjhxqyqrjhrxb88sdwd5s6zpnpa8d6p5m")))

(define-public crate-flame-0.1.7 (c (n "flame") (v "0.1.7") (h "1vnxn9v9qn30sxcbmnk76a1f0w7c4hmdbiazfk3xgxd2ril0y1a5")))

(define-public crate-flame-0.1.8 (c (n "flame") (v "0.1.8") (d (list (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0vncckv2bnvi6j5ija4yxbwr4nyrdj4yfppc1vclf7bafxp5v2gi") (f (quote (("json" "serde" "serde_macros" "serde_json") ("default"))))))

(define-public crate-flame-0.1.9 (c (n "flame") (v "0.1.9") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "^2.0.0") (d #t) (k 0)))) (h "1bl6gzcjximjwvfiwbbp6l20jd2gvzjcdxzxgafk0n08h3yvb555") (f (quote (("json" "serde" "serde_macros" "serde_json") ("default"))))))

(define-public crate-flame-0.1.10 (c (n "flame") (v "0.1.10") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "^2.0.0") (d #t) (k 0)))) (h "1gj2ma81p79xgdf9gy498v90bs3mvpf1c7jz345dcf6nzj62a0pj") (f (quote (("json" "serde" "serde_macros" "serde_json") ("default"))))))

(define-public crate-flame-0.1.11 (c (n "flame") (v "0.1.11") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "^2.0.0") (d #t) (k 0)))) (h "0xv1x7hqx4gpvibm16qi3cg6rx2a1gd30870c64j359c50mg42kd") (f (quote (("json" "serde" "serde_macros" "serde_json") ("default"))))))

(define-public crate-flame-0.1.12 (c (n "flame") (v "0.1.12") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "3.*.*") (d #t) (k 0)))) (h "1qbqgsgaylx85mdyrmvlj6ghnc21cw5q0inq37blqd90fdnh8avl") (f (quote (("json" "serde" "serde_derive" "serde_json") ("default" "json"))))))

(define-public crate-flame-0.2.0 (c (n "flame") (v "0.2.0") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "3.*.*") (d #t) (k 0)))) (h "1dr34d38vbjgg4xwr6ys3vcw30ndfypmp0vbsqqaxx55crj8bcff") (f (quote (("json" "serde" "serde_derive" "serde_json") ("default" "json"))))))

(define-public crate-flame-0.2.1 (c (n "flame") (v "0.2.1") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "3.*.*") (d #t) (k 0)))) (h "03jd3i4ll2awy34w6v2svv2j4ca02lrifh1c8m3jaq4z8gsgpazm") (f (quote (("json" "serde" "serde_derive" "serde_json") ("default" "json"))))))

(define-public crate-flame-0.2.2 (c (n "flame") (v "0.2.2") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "3.*.*") (d #t) (k 0)))) (h "0c5bmhyimzxch3pmh0w3z9n57saasgix4bmbbksr9vp1c5j71hhz") (f (quote (("json" "serde" "serde_derive" "serde_json") ("default" "json"))))))

(define-public crate-flame-0.2.1-pre2 (c (n "flame") (v "0.2.1-pre2") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "3.*.*") (d #t) (k 0)))) (h "09frixf2dxd4p1kfjy279bc792qg0v53p2kjxhv5r2ji6qqwndza") (f (quote (("json" "serde" "serde_derive" "serde_json") ("default" "json"))))))

