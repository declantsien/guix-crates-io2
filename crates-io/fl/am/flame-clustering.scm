(define-module (crates-io fl am flame-clustering) #:use-module (crates-io))

(define-public crate-flame-clustering-0.1.0 (c (n "flame-clustering") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1zr2jy0slvch325i90d5vmrnklcr8mbffafd78646fcv10j09rlk")))

(define-public crate-flame-clustering-0.2.0 (c (n "flame-clustering") (v "0.2.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1dhsrk7kczhi4b8pr5z5y05yyknc7mlb0qjall3g6iwz9avnmrj9")))

(define-public crate-flame-clustering-0.2.1 (c (n "flame-clustering") (v "0.2.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "077prisv4nsr60biszl0rrx1w6bnbybnz5i11mgzfvxainp7kzp9")))

(define-public crate-flame-clustering-0.2.2 (c (n "flame-clustering") (v "0.2.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1snjka4gh2paa9wb9vxms4b72jdl4xnzavn6bj1yh2p658cmqw0j")))

