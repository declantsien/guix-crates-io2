(define-module (crates-io fl am flamer) #:use-module (crates-io))

(define-public crate-flamer-0.1.0 (c (n "flamer") (v "0.1.0") (d (list (d (n "flame") (r "^0.1.2") (d #t) (k 0)))) (h "1bayz76sd4l4qdxnmbyl9yfqx8xaf3h2l20212dqg3mgfwy1f2bv")))

(define-public crate-flamer-0.1.1 (c (n "flamer") (v "0.1.1") (d (list (d (n "flame") (r "^0.1.2") (d #t) (k 0)))) (h "1s0d15rw9djigy3ffyjr8g01vlp1rxczj9kp0ygk1y5j7x02z0m0")))

(define-public crate-flamer-0.1.2 (c (n "flamer") (v "0.1.2") (d (list (d (n "flame") (r "^0.1.2") (d #t) (k 0)))) (h "1dgnp508802a08h82zhnd1n0bf67qr7xk1d62i7r3sm2vpn92xaf")))

(define-public crate-flamer-0.1.4 (c (n "flamer") (v "0.1.4") (d (list (d (n "flame") (r "^0.1.9") (d #t) (k 0)))) (h "0rhp1dbrpav81dr9dddvg5hjcnlw1lqv5j2xml4gp9jig3ra3jnn")))

(define-public crate-flamer-0.2.0 (c (n "flamer") (v "0.2.0") (d (list (d (n "flame") (r "^0.2.0") (d #t) (k 0)))) (h "0pkqyfmc6cwvypmadlqapkqgzq4ccnk4xg09sz667qiwlxcxadia")))

(define-public crate-flamer-0.2.1 (c (n "flamer") (v "0.2.1") (d (list (d (n "flame") (r "^0.2.2") (d #t) (k 0)))) (h "0gcns3h2aa9qra2rah5whd2bs4h08wb9l7dycfff5hv1s5sksr17")))

(define-public crate-flamer-0.2.2 (c (n "flamer") (v "0.2.2") (d (list (d (n "flame") (r "^0.2.2") (d #t) (k 0)))) (h "1b7gx1ingf9li3kjwlwls35sb434ya02ip18ibbq8qpy2cnqhhdx")))

(define-public crate-flamer-0.2.3 (c (n "flamer") (v "0.2.3") (d (list (d (n "flame") (r "^0.2.2") (d #t) (k 0)))) (h "1rxfwv7dmz6sp48vxixjhg72n36744v6k5y3kxlk0qcqz749a570")))

(define-public crate-flamer-0.2.4 (c (n "flamer") (v "0.2.4") (d (list (d (n "flame") (r "^0.2.2") (d #t) (k 0)))) (h "1y1md4b554khskdppil8qvwqhapcr6f3vbj8jc1nqqaig046ccvh") (y #t)))

(define-public crate-flamer-0.2.5 (c (n "flamer") (v "0.2.5") (d (list (d (n "flame") (r "^0.2.2") (d #t) (k 0)))) (h "1ildp75mlmyn0iy32m07xmm9h6z2lfb1hgmvgkm24jigswgrbgsm")))

(define-public crate-flamer-0.3.0 (c (n "flamer") (v "0.3.0") (d (list (d (n "flame") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("extra-traits" "full" "fold" "parsing"))) (d #t) (k 0)))) (h "1b2d7jx80f3p7hqpgdi7wksaiq18k9w23p0cs2sxf7jbx2jx3bgj")))

(define-public crate-flamer-0.4.0 (c (n "flamer") (v "0.4.0") (d (list (d (n "flame") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("extra-traits" "full" "fold" "parsing"))) (d #t) (k 0)))) (h "1avszq3fn4ix7p6wjfdkli6fjyxccks1qhzja92a6kpxakd35drn") (f (quote (("test-nightly"))))))

(define-public crate-flamer-0.5.0 (c (n "flamer") (v "0.5.0") (d (list (d (n "flame") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "fold" "parsing"))) (d #t) (k 0)))) (h "046vndifcjg9dparqrrvldyyqz3zc9djbzjx3594zif13vfxk4vn") (f (quote (("test-nightly"))))))

