(define-module (crates-io fl ak flaky_test_impl) #:use-module (crates-io))

(define-public crate-flaky_test_impl-0.2.2 (c (n "flaky_test_impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1glshs6wa5n229d4abqzijssy4nslyaiw3xlwpd2ymghq84mkqw5")))

