(define-module (crates-io fl ak flake-inputs) #:use-module (crates-io))

(define-public crate-flake-inputs-0.1.0 (c (n "flake-inputs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1w5ccdkd40bin092cg7p39ifpgskg1xxb3zavdq797nh7mmxzwls")))

