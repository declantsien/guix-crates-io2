(define-module (crates-io fl ak flakery-client) #:use-module (crates-io))

(define-public crate-flakery-client-0.1.0 (c (n "flakery-client") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "progenitor-client") (r "^0.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "stream"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)))) (h "0rd2ifhvqlv9h0xc5n9m51m1wmcq1dxszbg7yi20aln92h588ax7")))

