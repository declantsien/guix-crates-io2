(define-module (crates-io fl ak flak-ecs) #:use-module (crates-io))

(define-public crate-flak-ecs-0.0.0 (c (n "flak-ecs") (v "0.0.0") (d (list (d (n "flak-macros") (r "^0.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08sv8r68yknxbwkyj0r4fsnws2pjj1634a6msy6sjgk7dk5k9l15") (y #t)))

