(define-module (crates-io fl ak flaken) #:use-module (crates-io))

(define-public crate-flaken-0.2.0 (c (n "flaken") (v "0.2.0") (h "189cqp76nj46gn42596rn1sg0l5svxkpllnwz0v3b2h2z6cd8xx5")))

(define-public crate-flaken-0.2.1 (c (n "flaken") (v "0.2.1") (h "00kld8yxf3jl9fah13ni82r5h8vlp6kg874mw31v7byy325zf2hl")))

(define-public crate-flaken-0.2.2 (c (n "flaken") (v "0.2.2") (h "1jsrij9pm2rbgyl9ypqjjbn0c8k26nqrd171s8hr51h2gmwm15yr")))

