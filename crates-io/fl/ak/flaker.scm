(define-module (crates-io fl ak flaker) #:use-module (crates-io))

(define-public crate-flaker-0.1.2 (c (n "flaker") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "11yl823ic2avr9m9zp00n50akvjwjw7s25ql108ndjzdjkgv4y38")))

(define-public crate-flaker-0.1.3 (c (n "flaker") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "01041fgl9mmxjbi1p0243y1rg9h3wf3vv2q7g427ja4m1cg6rgbn")))

