(define-module (crates-io fl ak flaky_test) #:use-module (crates-io))

(define-public crate-flaky_test-0.1.0 (c (n "flaky_test") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14yvm0knhcx0xfwlykir2v198x5bpwf333yrdl2mmkv8n5gdx727")))

(define-public crate-flaky_test-0.2.2 (c (n "flaky_test") (v "0.2.2") (d (list (d (n "flaky_test_impl") (r "^0.2.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (k 2)))) (h "1nn8hddhl2vaxgfn0j87yrngr9bzlxlncgdd9vy53xyp4cgslv04")))

