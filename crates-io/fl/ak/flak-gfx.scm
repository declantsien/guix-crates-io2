(define-module (crates-io fl ak flak-gfx) #:use-module (crates-io))

(define-public crate-flak-gfx-0.0.0 (c (n "flak-gfx") (v "0.0.0") (d (list (d (n "vulkano") (r "^0.24.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.24.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.24.0") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "0xamv7csg0ljxm80am2x3pjrm5fgl5yx2s9z8lrbrjb8iffz0drv") (y #t)))

