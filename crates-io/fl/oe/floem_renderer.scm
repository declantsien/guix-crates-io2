(define-module (crates-io fl oe floem_renderer) #:use-module (crates-io))

(define-public crate-floem_renderer-0.1.0 (c (n "floem_renderer") (v "0.1.0") (d (list (d (n "floem-cosmic-text") (r "^0.7.0") (d #t) (k 0)) (d (n "floem-peniko") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("jpeg" "png"))) (d #t) (k 0)) (d (n "resvg") (r "^0.33.0") (d #t) (k 0)))) (h "13lyz9f52dnm95hd7c4ya1yg2z98nb2gbgm79qg48bj53ngpvcmy")))

