(define-module (crates-io fl oe floem-vger-rs) #:use-module (crates-io))

(define-public crate-floem-vger-rs-0.2.8 (c (n "floem-vger-rs") (v "0.2.8") (d (list (d (n "euclid") (r "^0.22.7") (d #t) (k 0)) (d (n "floem-cosmic-text") (r "^0.7.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.5") (d #t) (k 2)) (d (n "png") (r "^0.17.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rect_packer") (r "^0.2.1") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 2)) (d (n "wgpu") (r "^0.19.0") (d #t) (k 0)))) (h "1864w79j9d03zmsw53d574ngckjjr8wzm8jhbz6pzkfyfqnjvj7x")))

