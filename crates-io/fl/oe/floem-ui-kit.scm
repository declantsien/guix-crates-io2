(define-module (crates-io fl oe floem-ui-kit) #:use-module (crates-io))

(define-public crate-floem-ui-kit-0.0.1 (c (n "floem-ui-kit") (v "0.0.1") (h "1d2m5phfxbfjnwh09ckr1sk86yzilgmh6fl1fiianai6g001mx64")))

(define-public crate-floem-ui-kit-0.1.0 (c (n "floem-ui-kit") (v "0.1.0") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "floem") (r "^0.1.1") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18ww0hlbajb06l64bhhxym7nbzbl8fy8mb2mh1ybyjdl0mc53alr")))

