(define-module (crates-io fl oe floem_vger_renderer) #:use-module (crates-io))

(define-public crate-floem_vger_renderer-0.1.0 (c (n "floem_vger_renderer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "floem-peniko") (r "^0.1.0") (d #t) (k 0)) (d (n "floem-vger-rs") (r "^0.2.7") (d #t) (k 0) (p "floem-vger")) (d (n "floem_renderer") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("jpeg" "png"))) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.1") (d #t) (k 0)) (d (n "resvg") (r "^0.33.0") (d #t) (k 0)) (d (n "swash") (r "^0.1.8") (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "1dyqwb671w6al4gfbzll5pnrfhf4cjifxbhbydhxxhlimwbhx9hq")))

