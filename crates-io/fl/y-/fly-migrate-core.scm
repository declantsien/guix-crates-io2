(define-module (crates-io fl y- fly-migrate-core) #:use-module (crates-io))

(define-public crate-fly-migrate-core-0.2.0 (c (n "fly-migrate-core") (v "0.2.0") (d (list (d (n "postgres") (r "^0.19.4") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1iics01vyfnzg2yvkjwc7lykjd22q1x2crc6lsyiwkh407rcv3jm")))

(define-public crate-fly-migrate-core-0.2.1 (c (n "fly-migrate-core") (v "0.2.1") (d (list (d (n "postgres") (r "^0.19.4") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0mwh3rfw41ys2jascv3y69y2n7n8nbk6pj0g834hxrrb1159a4rb")))

