(define-module (crates-io fl y- fly-machine-gen) #:use-module (crates-io))

(define-public crate-fly-machine-gen-1.0.0 (c (n "fly-machine-gen") (v "1.0.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "stream"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)))) (h "19fjbffizyv1q8d4d5l2039qafag8abp9x8xbbl4422mqvick86i") (y #t)))

