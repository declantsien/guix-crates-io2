(define-module (crates-io fl y- fly-accept-encoding) #:use-module (crates-io))

(define-public crate-fly-accept-encoding-0.2.0-alpha.5 (c (n "fly-accept-encoding") (v "0.2.0-alpha.5") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15zsnxnisn3y7bysffymn8ysf5b56z7d3rwhrlic1sxwqd53w7bl")))

(define-public crate-fly-accept-encoding-0.2.0 (c (n "fly-accept-encoding") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a1bwbxizzni8bc647qa37nmqy2pr3zghf59jdg8xzfwdx8sgbx3")))

