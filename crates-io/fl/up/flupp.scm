(define-module (crates-io fl up flupp) #:use-module (crates-io))

(define-public crate-flupp-0.1.0 (c (n "flupp") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "insta") (r "=1.29.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (d #t) (k 0)))) (h "1y4axbc5yd31ixf1v4qlgzy224w0x1rjq1pqccz8migy4ds1313c")))

(define-public crate-flupp-0.1.1 (c (n "flupp") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "insta") (r "=1.29.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (d #t) (k 0)))) (h "0y3vrqsi0ivqfd6fmp8yzi4d88msq23vxqjjz1vjvzly12kp8576")))

(define-public crate-flupp-0.1.2 (c (n "flupp") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "insta") (r "=1.31.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (d #t) (k 0)))) (h "0m3linf8810j4nb4k68ld9jdl3df36765dz1ds2s34j3yi83yc1b")))

(define-public crate-flupp-0.1.3 (c (n "flupp") (v "0.1.3") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "insta") (r "=1.31.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (d #t) (k 0)))) (h "02smpd8hjqc8j7qb2yh6r9hbkqifi76l6726yhxr7633zr7a9i9h")))

