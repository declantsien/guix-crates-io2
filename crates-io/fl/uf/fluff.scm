(define-module (crates-io fl uf fluff) #:use-module (crates-io))

(define-public crate-fluff-0.0.0 (c (n "fluff") (v "0.0.0") (h "1llivqkyawzsa3m1k1fgjk0hnx0yp385k7nan6yqjv99sw82j0mp") (y #t)))

(define-public crate-fluff-0.1.0 (c (n "fluff") (v "0.1.0") (h "1619f5v72n7n93z9zvr613lak5lv885k1hs2c8dxcmdhnag0b7gg") (y #t)))

(define-public crate-fluff-0.1.1 (c (n "fluff") (v "0.1.1") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1kjmmyryf6r14ab0ayw6p9knlvz6v9iw4azn0arkpqki0f822q36") (y #t)))

