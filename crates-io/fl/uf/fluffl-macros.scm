(define-module (crates-io fl uf fluffl-macros) #:use-module (crates-io))

(define-public crate-fluffl-macros-0.0.3 (c (n "fluffl-macros") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "0bcq55fkv34105a5rvz4nn67wmiyxycsppm2z1qs8lxdp4w49x32")))

