(define-module (crates-io fl or floral_barrel) #:use-module (crates-io))

(define-public crate-floral_barrel-0.1.0 (c (n "floral_barrel") (v "0.1.0") (h "1mj7smd2kn9wn18b6588jr5b344hikl2zahdd3rxzaybxl5cqgwa")))

(define-public crate-floral_barrel-0.2.0 (c (n "floral_barrel") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "130kch3l9q8pw455mpqlfgcysfsf7z0kdpjc1akddfbz90ap3hxf")))

(define-public crate-floral_barrel-0.2.1 (c (n "floral_barrel") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1gpifyv6p1dm4xzi9xcf4bsb4v5hafpdffi1xk7l43ik3dc54hiw")))

