(define-module (crates-io fl p_ flp_abyss_viewer) #:use-module (crates-io))

(define-public crate-flp_abyss_viewer-0.1.0 (c (n "flp_abyss_viewer") (v "0.1.0") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rfd") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0qpmdb9dsnfrnrblzhfs0zrwlvg53kwl8k6x3zx6rxz5wi0xpasx") (y #t)))

(define-public crate-flp_abyss_viewer-0.1.1 (c (n "flp_abyss_viewer") (v "0.1.1") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rfd") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1k62j8z8g9yr9qinh825rxigw818gj3brhpjx72z9dj9pi4v94ql") (y #t)))

