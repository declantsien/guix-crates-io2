(define-module (crates-io fl dt fldtools) #:use-module (crates-io))

(define-public crate-fldtools-0.1.1 (c (n "fldtools") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "itertools") (r "^0.6.1") (d #t) (k 0)))) (h "1jmvdzwg6cgz8lg16ygahxcj5q4zix6fq07cyg6bdm985cvifh1s")))

