(define-module (crates-io fl us fluss) #:use-module (crates-io))

(define-public crate-fluss-0.1.0 (c (n "fluss") (v "0.1.0") (d (list (d (n "async-std") (r "^0.99") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0pvk0c0d1a6bn7lffi5vqb4zdabxx5r63hhxg725lhm79rkzj9i7")))

(define-public crate-fluss-0.1.1-alpha.1 (c (n "fluss") (v "0.1.1-alpha.1") (d (list (d (n "async-std") (r "^0.99") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "1r02p7g61g54wnihnwzc7z2n05iky100mj58695hx699j7j5b626")))

