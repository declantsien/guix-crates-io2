(define-module (crates-io fl us flussab-cnf) #:use-module (crates-io))

(define-public crate-flussab-cnf-0.1.0 (c (n "flussab-cnf") (v "0.1.0") (d (list (d (n "flussab") (r "^0.1.0") (d #t) (k 0)) (d (n "itoa") (r "^0.4.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0isqp74q05kx3gggr0slf7fm71micgi3ydbrhrb2m3d1jls661yl")))

(define-public crate-flussab-cnf-0.1.1 (c (n "flussab-cnf") (v "0.1.1") (d (list (d (n "flussab") (r "^0.1.0") (d #t) (k 0)) (d (n "itoa") (r "^0.4.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0qf42irk22xqximf4yhixlzlpija17ci7crl1bnch1i84lr3bdwx")))

(define-public crate-flussab-cnf-0.2.0 (c (n "flussab-cnf") (v "0.2.0") (d (list (d (n "flussab") (r "^0.1.0") (d #t) (k 0)) (d (n "itoa") (r "^0.4.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "146pvaa2ys7lpdncl60azb6kprf04g0a313fhp83mbm2l0vrbxcs")))

(define-public crate-flussab-cnf-0.3.0 (c (n "flussab-cnf") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "flussab") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0csjx0c93hxap0fz4l8fmimid8n2gaaq978wa6px0r61pmwhdm64")))

(define-public crate-flussab-cnf-0.3.1 (c (n "flussab-cnf") (v "0.3.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "flussab") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1w2jcghqij9gi1qqr4pk580vydffy1bg0jgi95i3h33lbn3hvpd1")))

