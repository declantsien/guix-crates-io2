(define-module (crates-io fl us flussab) #:use-module (crates-io))

(define-public crate-flussab-0.1.0 (c (n "flussab") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "060d0qmpgzlp3avh4j2rcg0cyzpsfds9cqkxmz8nlmivk0b4hlr0")))

(define-public crate-flussab-0.3.0 (c (n "flussab") (v "0.3.0") (d (list (d (n "itoap") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0hw810br6sl68gqbdhi341p10jvjla9ym9n4c5xkvqapzbidx49s")))

(define-public crate-flussab-0.3.1 (c (n "flussab") (v "0.3.1") (d (list (d (n "itoap") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0bxih9ydvk4dlibgqdmk19hrslwwkb9jsa11p9wls7ma867nvm7w")))

