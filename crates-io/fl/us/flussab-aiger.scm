(define-module (crates-io fl us flussab-aiger) #:use-module (crates-io))

(define-public crate-flussab-aiger-0.1.0 (c (n "flussab-aiger") (v "0.1.0") (d (list (d (n "flussab") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "zwohash") (r "^0.1.2") (d #t) (k 0)))) (h "1ll6g0aqn9q7nb2vlk32bplqpygfnbcs9jdkx1i325nhf2ckm2rp")))

