(define-module (crates-io fl us fluse) #:use-module (crates-io))

(define-public crate-fluse-0.1.0 (c (n "fluse") (v "0.1.0") (h "1g1fwmycvh06528d3m1wc9q8q9cq5bmvfjgxm1dx03mfqv9nifgl")))

(define-public crate-fluse-0.1.2 (c (n "fluse") (v "0.1.2") (h "02ghb5iijhl3pafihd3nvr4aas7b6alw4hgxr7ijdnpzniqnn6j3")))

(define-public crate-fluse-0.1.3 (c (n "fluse") (v "0.1.3") (h "0ljj8spad8pmhszh0zdsgqfs9spdk2zs5fpbbn6q4pcfiww1cwlh")))

(define-public crate-fluse-0.1.4 (c (n "fluse") (v "0.1.4") (h "15ydx83k1dqvhpdn9vw7gl246myz6da0bp9j9js3mml96iixlib8")))

