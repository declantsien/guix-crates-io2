(define-module (crates-io fl ek flek) #:use-module (crates-io))

(define-public crate-flek-1.4.0 (c (n "flek") (v "1.4.0") (d (list (d (n "cliply") (r "^0.1.0") (d #t) (k 0)) (d (n "coutils") (r "^1.4.0") (d #t) (k 0)))) (h "1mbd2ar383q7x6izd29q292gfq5srgvq19kbsrjvwga5knkgpy03")))

(define-public crate-flek-1.5.0 (c (n "flek") (v "1.5.0") (d (list (d (n "cliply") (r "^0.2.0") (d #t) (k 0)) (d (n "coutils") (r "^1.5.0") (d #t) (k 0)))) (h "09ayg9axdm8fdw9ch3z9l08zvh6c62xgm3h2vpp7kvrj7jdnnwj9")))

(define-public crate-flek-1.6.0 (c (n "flek") (v "1.6.0") (d (list (d (n "cliply") (r "^0.2.0") (d #t) (k 0)) (d (n "coutils") (r "^1.5.0") (d #t) (k 0)))) (h "0py10p817kjjn1kj9j23l5w8b4zk1p2qzg9l3x0k9qs09rcgd7h6")))

(define-public crate-flek-1.7.0 (c (n "flek") (v "1.7.0") (d (list (d (n "cliply") (r "^0.2.0") (d #t) (k 0)) (d (n "coutils") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha256") (r "^1.3.0") (d #t) (k 0)))) (h "19b2m93bg0wh7d2cg6zvlfvf7pwn47rz25fvnh2cjzx1hvr8j4x1")))

(define-public crate-flek-1.8.0 (c (n "flek") (v "1.8.0") (d (list (d (n "cliply") (r "^0.2.0") (d #t) (k 0)) (d (n "coutils") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha256") (r "^1.3.0") (d #t) (k 0)))) (h "0nzxs9xd00s7q13ivdba7pa99b1fngh57252lnkphnibpz7lswvf")))

(define-public crate-flek-1.9.0 (c (n "flek") (v "1.9.0") (d (list (d (n "cliply") (r "^0.2.0") (d #t) (k 0)) (d (n "coutils") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha256") (r "^1.3.0") (d #t) (k 0)))) (h "1x177lxma2pljkz4yq1kyz92y5mi7gds6n1wygqrz3qyhyxz5y4b")))

