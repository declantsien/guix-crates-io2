(define-module (crates-io fl ar flare) #:use-module (crates-io))

(define-public crate-flare-0.0.0 (c (n "flare") (v "0.0.0") (h "1jsavifvc1aandn82kgj5ska0rjwdl3dx0nl0dh135giki6hxlda")))

(define-public crate-flare-0.1.0 (c (n "flare") (v "0.1.0") (h "1ym9svg28685a7s30q9pgpr3l7df0ij9gjdd0ss3qyikgak18xy8") (y #t)))

(define-public crate-flare-0.0.1 (c (n "flare") (v "0.0.1") (h "079hfdh915h90149195nfnqz63iasaajn4dg9yk6rc8ws7mpa1cr")))

