(define-module (crates-io fl ar flarelog) #:use-module (crates-io))

(define-public crate-flarelog-0.1.0 (c (n "flarelog") (v "0.1.0") (h "06fhg5vyhirb0419iqhszxv5z5l9s4r7dj9w62a6ffabmnkkva1i")))

(define-public crate-flarelog-0.2.0 (c (n "flarelog") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "00shmzh9d8ifww6g7hdq7rhlp7vdi7kq4fgvvyx5p22hw2f6bda1")))

(define-public crate-flarelog-0.3.0 (c (n "flarelog") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "0c4yj7p3fc2g6yl0i3ri4fj7i5mvqwnj44k7p5xqvvchqc67qyw4")))

(define-public crate-flarelog-0.3.1 (c (n "flarelog") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "1kky3gqvyp3azzzjwjyp9lp6ky7yyf4djgs9ycr647cmhanndl1w")))

(define-public crate-flarelog-0.3.2 (c (n "flarelog") (v "0.3.2") (d (list (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "1d166f1iy46v9ff16v5bnhilw5ikbp096rdfpb4w3ywh2dnmi8jw")))

(define-public crate-flarelog-0.3.3 (c (n "flarelog") (v "0.3.3") (d (list (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "1gnzj8cfcdydf27171g9j3q9nkwkgzqghcrclyy3pnxvaiwm6isd")))

(define-public crate-flarelog-0.3.4 (c (n "flarelog") (v "0.3.4") (d (list (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "1rd43a592x0jq53qvxk62izni5l58gh4qr21sn08rllx1i8ykfkg")))

(define-public crate-flarelog-0.3.5 (c (n "flarelog") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "1nkfzd1b98jz2cdkfw1w0vbiaa7vr88cd1lqcbqz5yshb671m0vl")))

(define-public crate-flarelog-0.3.6 (c (n "flarelog") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "149rwzsc6przb7azgycxds50qn9sc37n75iqsm37svlx33lsxj9f")))

(define-public crate-flarelog-0.4.0 (c (n "flarelog") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "012xamnh14j1h818aw4xs9yaamsc08cr8xdss9gj8cg9bschn2p6")))

(define-public crate-flarelog-0.5.0 (c (n "flarelog") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.6") (d #t) (k 0)))) (h "19w8ggnkn4z6ak2gbcm8pbbz3bxby0fbbqwc3nsgac21yh852hd3")))

