(define-module (crates-io fl ar flarmnet) #:use-module (crates-io))

(define-public crate-flarmnet-0.1.0 (c (n "flarmnet") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "126327r4m4i4qxiyx567ffsmc14vgrizgyg4rg42vk9c0aswbc1n")))

(define-public crate-flarmnet-0.1.1 (c (n "flarmnet") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0hid9ccgxw5kjshs8h830dm0bppbqmkfz6wdz5pcl3c61wshk1p4")))

(define-public crate-flarmnet-0.2.0 (c (n "flarmnet") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "minidom") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "00gkrdh4pwcj30pay5qs4mzk239nij7b7p9i6zc8s56bv445w9ix") (f (quote (("lx" "minidom") ("default" "lx"))))))

(define-public crate-flarmnet-0.3.0 (c (n "flarmnet") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "minidom") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0qh57zs87jnqha02a79hf3i27rpinw3dy4dmp5ljrq9mz99g0084") (f (quote (("lx" "minidom") ("default" "lx"))))))

(define-public crate-flarmnet-0.4.0 (c (n "flarmnet") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.28") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "minidom") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0hws5ppqpybffz18bmwq8iy58asj0rm5sybns855m0q1abpwfc99") (f (quote (("xcsoar" "encoding_rs") ("lx" "minidom") ("default" "lx" "xcsoar"))))))

(define-public crate-flarmnet-0.5.0 (c (n "flarmnet") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.32") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "minidom") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1cgsq75vw6rxa861sggai86lz4c4ypwi4gmdbp1vh90i9fj053c0") (f (quote (("xcsoar" "encoding_rs") ("lx" "minidom" "quick-xml") ("default" "lx" "xcsoar"))))))

