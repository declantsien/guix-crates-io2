(define-module (crates-io fl ar flary) #:use-module (crates-io))

(define-public crate-flary-0.1.0 (c (n "flary") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cloudflare") (r "^0.5.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.1") (d #t) (k 0)))) (h "0d4g9mf034yavszh9a36v377786282a860cylcn4ln4hcf21vaic")))

(define-public crate-flary-0.1.1 (c (n "flary") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cloudflare-rs") (r "^0.6.6") (d #t) (k 0)) (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1ij6zwcdsxdnmaxrl78b1b2rzlsdkznksj5xwmam0rai5fc1nsgq")))

