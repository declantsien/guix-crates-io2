(define-module (crates-io fl if flif) #:use-module (crates-io))

(define-public crate-flif-0.0.0 (c (n "flif") (v "0.0.0") (h "0a5sh5c6w9y38sqab0681mcljjwg1d38q3smhwjbapv2r1d3plsl") (y #t)))

(define-public crate-flif-0.1.0 (c (n "flif") (v "0.1.0") (d (list (d (n "inflate") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "png") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "02d9w0qdqd1gmhw9qq7c2fxl64rcdz2y60crb4dsqmagrpg5rq7y") (f (quote (("binary" "structopt" "structopt-derive" "png"))))))

(define-public crate-flif-0.2.0 (c (n "flif") (v "0.2.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 2)))) (h "02dd7391z0nirnacmvv6f378rs4pfgcldjqs2wk6lklgkpfzlxwr")))

(define-public crate-flif-0.3.0 (c (n "flif") (v "0.3.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 2)))) (h "0g4rmh97yqbzkkbd5igsrfn3m551272riyq19n544vvkj9jrq289")))

(define-public crate-flif-0.4.0 (c (n "flif") (v "0.4.0") (d (list (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.13") (d #t) (k 2)))) (h "15jmwsxpzjc9a24p17211sji48ns8fdgbzsh87vapq6kvhi3zvnv") (y #t)))

(define-public crate-flif-0.4.1 (c (n "flif") (v "0.4.1") (d (list (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.13") (d #t) (k 2)))) (h "034lzfamlkkxllmz436l1vv41jl5k9ls1hh9ykfrmx13n3ng8c9v")))

