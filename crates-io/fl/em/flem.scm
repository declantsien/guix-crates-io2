(define-module (crates-io fl em flem) #:use-module (crates-io))

(define-public crate-flem-0.5.0 (c (n "flem") (v "0.5.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 2)))) (h "1mhc4g891sb1vq15mw3pqbyprcxbk2xyd7zji4w1hiwvxlknwqbp")))

(define-public crate-flem-0.5.1 (c (n "flem") (v "0.5.1") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 2)))) (h "11wxcz9dh56nv6vr8mrav7hxa3hdlf7am9ri52p019y9lphrbhj4")))

(define-public crate-flem-0.6.0 (c (n "flem") (v "0.6.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 2)))) (h "0prk9s0plw9lfs3hb5v03lgphppvpnmnxbv0yfyhc06krjhpjhmf")))

(define-public crate-flem-0.6.1 (c (n "flem") (v "0.6.1") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 2)))) (h "1nndcgzpxnk8jjssbv15yphbxal4w4p7vy2z8swzs36683n0x49n")))

(define-public crate-flem-0.6.2 (c (n "flem") (v "0.6.2") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 2)))) (h "03mvhyz9vw443m43whhz8ivpcbsl85z988hlsn5hklpkp2xb6nqa") (f (quote (("std") ("default"))))))

