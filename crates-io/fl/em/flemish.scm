(define-module (crates-io fl em flemish) #:use-module (crates-io))

(define-public crate-flemish-0.1.0 (c (n "flemish") (v "0.1.0") (d (list (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "fltk-flex") (r "^0.1") (d #t) (k 0)))) (h "04zw00gli3z76fnspy9d4kyjdgvmibriqdp1m70wj8zn09n8hcb8")))

(define-public crate-flemish-0.1.1 (c (n "flemish") (v "0.1.1") (d (list (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "fltk-flex") (r "^0.1") (d #t) (k 0)))) (h "1rlspnazvbg3kbj2520lm2mq57rcgmqd06m7smjca85d273s4kkm")))

(define-public crate-flemish-0.1.2 (c (n "flemish") (v "0.1.2") (d (list (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "fltk-flex") (r "^0.1") (d #t) (k 0)))) (h "1pibwa704z11wgzz06jjcw3pn2d5bk4i2zd2q2ql35bf0fissfqh")))

(define-public crate-flemish-0.1.3 (c (n "flemish") (v "0.1.3") (d (list (d (n "fltk") (r "^1") (f (quote ("enable-glwindow"))) (d #t) (k 0)) (d (n "fltk-flex") (r "^0.1.3") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.1.8") (d #t) (k 0)))) (h "0i25nfgch7b50i65i2gnv3h58nihl6n9kb8r6wkacwx3km041chz")))

(define-public crate-flemish-0.2.0 (c (n "flemish") (v "0.2.0") (d (list (d (n "fltk") (r "^1.2.7") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.4") (d #t) (k 0)))) (h "0brj9myhj6jwq8v628ykzp4k53x01k9y18abq4739gc5dwirdhy6")))

(define-public crate-flemish-0.2.1 (c (n "flemish") (v "0.2.1") (d (list (d (n "fltk") (r "^1.2.7") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.4") (d #t) (k 0)))) (h "053l5kmw7ldarqsf7x0nxbiw0w6809wln0jq0xj264wwfzrqw6vv")))

(define-public crate-flemish-0.3.0 (c (n "flemish") (v "0.3.0") (d (list (d (n "fltk") (r "^1.2.7") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.4") (d #t) (k 0)))) (h "0bsx1ydxf70srf9z6vvcnss5c24kb6rlm6my99pz33mryacwx9wa")))

(define-public crate-flemish-0.3.1 (c (n "flemish") (v "0.3.1") (d (list (d (n "fltk") (r "^1.3.14") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.4") (d #t) (k 0)))) (h "1sjj8lihwzk4n80yvdvazskh78qhwzaz0rxplb1is76gm6d3yhzs")))

(define-public crate-flemish-0.4.0 (c (n "flemish") (v "0.4.0") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.7") (d #t) (k 0)))) (h "1i39fpmn4ixgszh4fnfhc2i4bpa6yp232qk9i3vccbnfay2xnljn")))

(define-public crate-flemish-0.4.1 (c (n "flemish") (v "0.4.1") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.7") (d #t) (k 0)))) (h "10w0p2lf75iazy8z988zf2ssf3b3ifnl950i3nkjd51mq01nv5ki")))

(define-public crate-flemish-0.4.2 (c (n "flemish") (v "0.4.2") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.7") (d #t) (k 0)))) (h "0a63f8n76qibpqwy5bgpgvznis2l5xjf1m13an819ll4h7y5z3v0")))

(define-public crate-flemish-0.4.3 (c (n "flemish") (v "0.4.3") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.7") (d #t) (k 0)))) (h "1ainfyjwk0p0rv98brfg159wwl8x0rv4afv542w4dp6dbm9fwa3z")))

(define-public crate-flemish-0.5.0 (c (n "flemish") (v "0.5.0") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.7") (d #t) (k 0)))) (h "1w3bc0jcra6xqikwv0ykldvvkmy1raamkd215amwbmjwp19ic2lc")))

(define-public crate-flemish-0.5.1 (c (n "flemish") (v "0.5.1") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.7") (d #t) (k 0)))) (h "0csbv9d4nl340x4ajapg48kywfr76cf409pp29i6pr2kc0xc7d5v")))

