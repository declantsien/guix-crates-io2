(define-module (crates-io fl oy floyd-warshall-alg) #:use-module (crates-io))

(define-public crate-floyd-warshall-alg-0.1.2 (c (n "floyd-warshall-alg") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "safe-graph") (r "^0.1.4") (d #t) (k 0)))) (h "0h97g46iy2qfppx8wqjz6x44l4a7x6h5da8rg8m3y8j6p4pjszwm")))

(define-public crate-floyd-warshall-alg-0.1.3 (c (n "floyd-warshall-alg") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "safe-graph") (r "^0.1.4") (d #t) (k 0)))) (h "1a4zrqw7rksjdwnjbk8p57l2145sy8byczl17gzqm4841aq5xby7")))

