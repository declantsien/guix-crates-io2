(define-module (crates-io fl oy floyd-warshall) #:use-module (crates-io))

(define-public crate-floyd-warshall-0.0.0 (c (n "floyd-warshall") (v "0.0.0") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)))) (h "132939vmysav4mc523m8dd8r7bdi7747la727lbvwrk8hk6760xi")))

(define-public crate-floyd-warshall-0.0.1 (c (n "floyd-warshall") (v "0.0.1") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "text_io") (r "^0.1.6") (d #t) (k 2)))) (h "1plhay2i0ysq660zlws6hp4f1skq6jprpgz8s09rsghjyv9p996j")))

(define-public crate-floyd-warshall-0.0.2 (c (n "floyd-warshall") (v "0.0.2") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "text_io") (r "^0.1.6") (d #t) (k 2)))) (h "102yfmpm9q87n7jwgb1sg10mi621wi1jr8jr6jvn6igjim7i0xdq")))

(define-public crate-floyd-warshall-0.0.3 (c (n "floyd-warshall") (v "0.0.3") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "text_io") (r "^0.1.6") (d #t) (k 2)))) (h "1ad8gzvcl9i20k051nj9hbag5k4dmwc2wxdw0bqzf3h5zlc5yhx6")))

