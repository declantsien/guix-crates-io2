(define-module (crates-io fl oy floydrivest) #:use-module (crates-io))

(define-public crate-floydrivest-0.1.0 (c (n "floydrivest") (v "0.1.0") (h "0j495500acbp5fmmg1lydi6mi6qy3nbb90c9bc8gnzdxj51gp9h4")))

(define-public crate-floydrivest-0.1.1 (c (n "floydrivest") (v "0.1.1") (h "1q9crzx560jb2cxd2z8l1bj98jzw64kfgl6kp05r20ndjiw64pj1")))

(define-public crate-floydrivest-0.1.2 (c (n "floydrivest") (v "0.1.2") (h "0y7l8cyfi46jj2ybbmz9kapdjwdx08fc1cyskkacbfnjb50qc03q")))

(define-public crate-floydrivest-0.1.3 (c (n "floydrivest") (v "0.1.3") (h "0iy830vr2kaww23xs4qk47nsq9c50ydfbmjb0rf2i597pkhr2vnz") (y #t)))

(define-public crate-floydrivest-0.2.0 (c (n "floydrivest") (v "0.2.0") (h "0v9gsmnh6a9m90p04mrr10nncnjy45ixh5acga9znmf05py65ms3") (y #t)))

(define-public crate-floydrivest-0.2.1 (c (n "floydrivest") (v "0.2.1") (h "12sk5x5z9amx3hrrwghci5mcdda922ahqfk1zzgpr9m5110w0dp8")))

(define-public crate-floydrivest-0.2.2 (c (n "floydrivest") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kth") (r "^0.1.0") (d #t) (k 2)) (d (n "order-stat") (r "^0.1.3") (d #t) (k 2)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1gy8w1pd9mxrzgvkfq3br58b2vhfc75k1s5mr8kqr9dxdz9dwcwm")))

(define-public crate-floydrivest-0.2.3 (c (n "floydrivest") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kth") (r "^0.1.0") (d #t) (k 2)) (d (n "order-stat") (r "^0.1.3") (d #t) (k 2)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "11k77mcvysv29hcg2yhjljjwp7hr7bddhszqsj21vfz24yv7r0aj")))

(define-public crate-floydrivest-0.2.4 (c (n "floydrivest") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kth") (r "^0.1.0") (d #t) (k 2)) (d (n "order-stat") (r "^0.1.3") (d #t) (k 2)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "02hfs35s4as5z9zi00pzxlc82vmqjb2xlkgbnwqbg841dr1bcf3n")))

