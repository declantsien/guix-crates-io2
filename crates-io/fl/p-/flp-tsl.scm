(define-module (crates-io fl p- flp-tsl) #:use-module (crates-io))

(define-public crate-flp-tsl-0.1.0 (c (n "flp-tsl") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ddhb3ncj8cjyrj89g4lbzmsmcbshs8r5r8id5yvmksciw28ngli")))

(define-public crate-flp-tsl-0.1.1 (c (n "flp-tsl") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1p6p8f6iygjabwc8w42xf004rcdhaccpwj6hvjgvmz06rjgzvzvv")))

(define-public crate-flp-tsl-0.1.2 (c (n "flp-tsl") (v "0.1.2") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h7zplr5zczdh8cg7wvp9sdxw1hqh5qif249bvy5znlfsxh8w64b")))

(define-public crate-flp-tsl-0.1.4 (c (n "flp-tsl") (v "0.1.4") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vg62q3vjjx9vxsg30rjchhglxvxbp2jn662s9dwfd24na5ajyw8")))

