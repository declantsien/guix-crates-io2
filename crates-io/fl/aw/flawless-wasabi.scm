(define-module (crates-io fl aw flawless-wasabi) #:use-module (crates-io))

(define-public crate-flawless-wasabi-0.0.1 (c (n "flawless-wasabi") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jcvdxs1lv1df0g85bpscsqck543lkycwa88fvsvlp1pjhgv27sn") (y #t)))

(define-public crate-flawless-wasabi-1.0.0-alpha.1+brazilian-capybara (c (n "flawless-wasabi") (v "1.0.0-alpha.1+brazilian-capybara") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ncij5qvbi35b4l5x16s1i0549qxdq04fjwzx5la1gmrl4az2nbi")))

(define-public crate-flawless-wasabi-1.0.0-alpha.2+brazilian-capybara (c (n "flawless-wasabi") (v "1.0.0-alpha.2+brazilian-capybara") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00xq81pfqc62r76djczg6mn38il7m68whj803yg8v7pk7f90kccr")))

(define-public crate-flawless-wasabi-1.0.0-alpha.3+brazilian-capybara (c (n "flawless-wasabi") (v "1.0.0-alpha.3+brazilian-capybara") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lj1f99lzm8jbk9ibqy6rsscql90qs45xja6n1qrcyd29y4vcq96")))

(define-public crate-flawless-wasabi-1.0.0-alpha.4+brazilian-capybara (c (n "flawless-wasabi") (v "1.0.0-alpha.4+brazilian-capybara") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zfkk3y70a40q36v25smnlk0lzqn9l91phjvdl45j9vbkzy9lwjg")))

(define-public crate-flawless-wasabi-1.0.0-alpha.5+brazilian-capybara (c (n "flawless-wasabi") (v "1.0.0-alpha.5+brazilian-capybara") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00l2b3rqfjgspj8jry0i1wxzbkzbzjkac2p0rc1hp5sac4p205j4")))

(define-public crate-flawless-wasabi-1.0.0-alpha.6+brazilian-capybara (c (n "flawless-wasabi") (v "1.0.0-alpha.6+brazilian-capybara") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "003js4ks8ys0r4qxc45pmraxqgkcmkzvpnghfxva7nycp031qf9a")))

(define-public crate-flawless-wasabi-1.0.0-alpha.7+brazilian-capybara (c (n "flawless-wasabi") (v "1.0.0-alpha.7+brazilian-capybara") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h44mvs059nsmyxzhsxmvbixbp23zkaksrp0c0adbc6dlva7hb8q")))

(define-public crate-flawless-wasabi-1.0.0-alpha.8+brazilian-capybara (c (n "flawless-wasabi") (v "1.0.0-alpha.8+brazilian-capybara") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wqv3vzb5mallasmpzsayi2vi1nyna72qxmarrq6dpb4g9dmj0ci")))

(define-public crate-flawless-wasabi-1.0.0-alpha.9+brazilian-capybara (c (n "flawless-wasabi") (v "1.0.0-alpha.9+brazilian-capybara") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "087b1jza2cxdyr6i34xfakc24j48kiicvka9yi2fmrd1f7kas856")))

(define-public crate-flawless-wasabi-1.0.0-alpha.10+finnish-reindeer (c (n "flawless-wasabi") (v "1.0.0-alpha.10+finnish-reindeer") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "081f6ka4imrajhvi9h2696zzyp8pxcc1f06rd0rwcsrd6fbxb6yz")))

(define-public crate-flawless-wasabi-1.0.0-alpha.11+croatian-pine-marten (c (n "flawless-wasabi") (v "1.0.0-alpha.11+croatian-pine-marten") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (o #t) (d #t) (k 0)))) (h "0yb8rja2qfag678pw0dj54ba50alcczbcqna80rrf4h9qmyr00r5")))

(define-public crate-flawless-wasabi-1.0.0-alpha.13+croatian-pine-marten (c (n "flawless-wasabi") (v "1.0.0-alpha.13+croatian-pine-marten") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (o #t) (d #t) (k 0)))) (h "1l2nsgaxv1bvk4bpb35wgkicmzb89imijhv058dwj0cavgspfjyz") (f (quote (("default" "ureq")))) (s 2) (e (quote (("ureq" "dep:ureq"))))))

(define-public crate-flawless-wasabi-1.0.0-alpha.14+croatian-pine-marten (c (n "flawless-wasabi") (v "1.0.0-alpha.14+croatian-pine-marten") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (o #t) (d #t) (k 0)))) (h "1sycsz7nwlhmy7b092z8ka23rx62n5qld2rx855v9bhyah172j1h") (f (quote (("default" "ureq")))) (s 2) (e (quote (("ureq" "dep:ureq"))))))

(define-public crate-flawless-wasabi-1.0.0-alpha.15+croatian-pine-marten (c (n "flawless-wasabi") (v "1.0.0-alpha.15+croatian-pine-marten") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (o #t) (d #t) (k 0)))) (h "0iq6kk81x9x5y1yiy7ynzm7mvzjwbrc7k5fh54c7rww9d1sx654b") (s 2) (e (quote (("ureq" "dep:ureq"))))))

(define-public crate-flawless-wasabi-1.0.0-alpha.16+croatian-pine-marten (c (n "flawless-wasabi") (v "1.0.0-alpha.16+croatian-pine-marten") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (o #t) (d #t) (k 0)))) (h "18kgf1dnbxy5va1h09awdcg4wj4z5lgdqmhzas4yvz4h3j8iv8dd") (s 2) (e (quote (("ureq" "dep:ureq"))))))

