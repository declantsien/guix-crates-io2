(define-module (crates-io fl aw flaw) #:use-module (crates-io))

(define-public crate-flaw-0.1.0 (c (n "flaw") (v "0.1.0") (d (list (d (n "interpn") (r "^0.4.2") (k 0)) (d (n "num-traits") (r "^0.2.18") (f (quote ("libm"))) (k 0)))) (h "1cysfx7413p9zywkzvj4ffq4h1xxhqhxd05slddd7793ay1b1pgn") (f (quote (("std") ("default"))))))

