(define-module (crates-io fl et fletcher-simd) #:use-module (crates-io))

(define-public crate-fletcher-simd-0.1.0 (c (n "fletcher-simd") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "multiversion") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1bhkajh652w2qx3lk63m49k515dkckzba6vw4911dk019k3qz1qc")))

(define-public crate-fletcher-simd-0.1.1 (c (n "fletcher-simd") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "multiversion") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0vqjfayp9s72jkgw5vsfy2s41i9lgdqp7m14nyvch8w73pf9gqah")))

(define-public crate-fletcher-simd-0.2.0 (c (n "fletcher-simd") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "multiversion") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0cizg22kfi76ql3k97hkyczjf68rz2n2hdv0988c6gcqjk96irhp")))

(define-public crate-fletcher-simd-0.3.0 (c (n "fletcher-simd") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "multiversion") (r "^0.6") (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10xmzxkfnwgn9d5g9a8i9xzbfbbqisxjzih4nzcddqgx34icxcyp") (f (quote (("runtime_dispatch" "multiversion/std") ("default" "runtime_dispatch"))))))

(define-public crate-fletcher-simd-0.4.0 (c (n "fletcher-simd") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "multiversion") (r "^0.6") (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1q0zh4y5z1ggwdvjkzgqcnj47n070fj7ny72wnjzhgb6dr0adw02") (f (quote (("runtime_dispatch" "multiversion/std") ("default" "runtime_dispatch"))))))

