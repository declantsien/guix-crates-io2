(define-module (crates-io fl et fletcher) #:use-module (crates-io))

(define-public crate-fletcher-0.0.1 (c (n "fletcher") (v "0.0.1") (h "06762m1apnp27k0njfwjnymrsx7k3mg50kwd3pkkid46scp7pbvw")))

(define-public crate-fletcher-0.1.0 (c (n "fletcher") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 2)))) (h "04b6phl9asd102na3f74i3bac502w17wilpjlg2465ax0c55svy7")))

(define-public crate-fletcher-0.2.0 (c (n "fletcher") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 2)))) (h "1wq9zcnd3zmq038bdicadhfc8gbgdr9q3w1iqphzadgqv3c785lj")))

(define-public crate-fletcher-0.3.0 (c (n "fletcher") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 2)))) (h "0w0h31kqwz0n3ffizysmxzw3yysy7dnkdw28h2xd3r7j4i8svdkq")))

