(define-module (crates-io fl ic flic-rust-client) #:use-module (crates-io))

(define-public crate-flic-rust-client-0.2.0 (c (n "flic-rust-client") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ip130xg9j987ir6m4gqxxrsssjmadfg278ds43z1iskm5n5z0wr")))

(define-public crate-flic-rust-client-0.3.0 (c (n "flic-rust-client") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r6igi0xc8pdrs6cql0rza3qg0l9i86qzvg9b74sc2s5r3lfwbaq")))

(define-public crate-flic-rust-client-0.3.1 (c (n "flic-rust-client") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kynmcdz5pi0njrxwywhv2nzj123jj7cmjb9w0mcypm0wgnmbjlk")))

(define-public crate-flic-rust-client-0.3.2 (c (n "flic-rust-client") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10il7ims4j8alyn5w4xzicwr9m6y4zg1jv8gl1w937a18ix6znpn")))

(define-public crate-flic-rust-client-0.3.3 (c (n "flic-rust-client") (v "0.3.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f5804m6d51pccavpwglxgdkrwf4ivnhna3hlk444z4kj6f5xvqm")))

(define-public crate-flic-rust-client-0.4.0 (c (n "flic-rust-client") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14nnad49b1db4vm69lb6c7xfmxlzwimh0af3q88yp7fszn786wx2")))

