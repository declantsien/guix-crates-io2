(define-module (crates-io fl ic flick) #:use-module (crates-io))

(define-public crate-flick-0.0.0 (c (n "flick") (v "0.0.0") (h "0ghzzh66pd4x91p09310d7bs50p8ka40jw15ddx1h7vild5d5l93")))

(define-public crate-flick-0.0.1 (c (n "flick") (v "0.0.1") (h "0x4l4q0s7jc2il5j5c0in6yw4wj9rd0l9vnypdjism20884w52yz")))

