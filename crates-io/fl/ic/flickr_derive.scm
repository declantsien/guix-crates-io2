(define-module (crates-io fl ic flickr_derive) #:use-module (crates-io))

(define-public crate-flickr_derive-0.1.0 (c (n "flickr_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "0vgv5nkfi9dbd2fly655q26ygqwrxzl3y9cahcqa9chsvfvcqhm7")))

(define-public crate-flickr_derive-0.1.1 (c (n "flickr_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "0jamijphla43gm9fzf5rkfm7972r3n9kp0as9aqagxfy729cx6sz")))

