(define-module (crates-io fl ic flic) #:use-module (crates-io))

(define-public crate-flic-0.1.0 (c (n "flic") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.24") (d #t) (k 2)))) (h "0x48a53p8gibqgpa77x6dd9bnqxrhq9mgw967274yl8xiwg7bkpf")))

(define-public crate-flic-0.1.1 (c (n "flic") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.27") (d #t) (k 2)))) (h "0c9qclccyx9ys4i3x5mx705g4693sqwjzxrbwwhqcjs28wnmc21f")))

(define-public crate-flic-0.1.2 (c (n "flic") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.28") (d #t) (k 2)))) (h "1w32r80wp48chias68pfjvb5szs5fyix00hww93ngsm83wwsqr58")))

(define-public crate-flic-0.1.3 (c (n "flic") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.28") (d #t) (k 2)))) (h "0r4cs2wkm8x2v6v3j9h9svc3zb8iridlkgpi02lmq04sd6f7d4gq")))

(define-public crate-flic-0.1.4 (c (n "flic") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.28") (d #t) (k 2)))) (h "14h9ic4hcr6kngsk1razgfz4fvl2bap0x2yk62fr22nqgap8kgll")))

(define-public crate-flic-0.1.5 (c (n "flic") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.29") (d #t) (k 2)))) (h "0d1m45zaqvh17jq71vl4ybpllhjwr6palarnyw2dl3a01zqyz207")))

(define-public crate-flic-0.1.6 (c (n "flic") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 2)))) (h "1rd5yiyzvlpih4brnc3piiarfmlrhqlg88awwrdqlb6z1cb77i7i")))

