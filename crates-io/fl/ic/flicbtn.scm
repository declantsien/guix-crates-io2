(define-module (crates-io fl ic flicbtn) #:use-module (crates-io))

(define-public crate-flicbtn-0.1.0 (c (n "flicbtn") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mgw18bayr9hgsxx8ddqw68hmjp1h1sh421z22zamdz1d7x2i8w3")))

(define-public crate-flicbtn-0.1.1 (c (n "flicbtn") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "186rrxh3z70d664r4yxqbb8m0g400givh3i9f6n8v3d0rap0mi1k")))

