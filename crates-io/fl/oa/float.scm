(define-module (crates-io fl oa float) #:use-module (crates-io))

(define-public crate-float-0.1.0 (c (n "float") (v "0.1.0") (h "1437mnsy030f6jh5m96pgw3gkkadwyds87gkyw1fp2c9a31bn75y")))

(define-public crate-float-0.1.1 (c (n "float") (v "0.1.1") (h "1plrf3qp7wy2a8apm6yn9n11bbf9ys5l6lx44pggbf122x0g9rsj")))

(define-public crate-float-0.1.2 (c (n "float") (v "0.1.2") (h "0zxld6pm58clv04gxr4clq48vyvwqci7csqd7kfvkdv6h2d3qlq2")))

