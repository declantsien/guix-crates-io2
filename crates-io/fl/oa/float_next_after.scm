(define-module (crates-io fl oa float_next_after) #:use-module (crates-io))

(define-public crate-float_next_after-0.1.0 (c (n "float_next_after") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0byk7hl1sygdf4c3hxxi5a0wvyfbpzy23dl948kiyzpq1017sny9")))

(define-public crate-float_next_after-0.1.1 (c (n "float_next_after") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "15v6qfbl1w8d410sfi5gxrp8sfbr28p4vih3apry7bp5pmrmxz7y")))

(define-public crate-float_next_after-0.1.2 (c (n "float_next_after") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "15qawjmb4kcpcwwgglqyzvf5vsh62rc4765qlwzcfxqzdvrgi8cj")))

(define-public crate-float_next_after-0.1.3 (c (n "float_next_after") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1jx66gr7qw6x45v8qkvzdg7s884gz05v5ab2cwxqaxwnxm1gbllv")))

(define-public crate-float_next_after-0.1.4 (c (n "float_next_after") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0w575zivrsxk5fjcmnjajfchgjqng3lajnrswbjkpww26yszv5c8")))

(define-public crate-float_next_after-0.1.5 (c (n "float_next_after") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0cnn60pslh0gilpy2jr7qpqk22a6lmsdz847988bg1krhg2i5ijg")))

(define-public crate-float_next_after-1.0.0 (c (n "float_next_after") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)))) (h "1s7ikn69b394frihag05b0qcw9i9y04qanlhp5c8sjrw70bcrxwb")))

