(define-module (crates-io fl oa float-pretty-print) #:use-module (crates-io))

(define-public crate-float-pretty-print-0.1.0 (c (n "float-pretty-print") (v "0.1.0") (h "109y42xy2qi1ld8zhi6x9m7fnlm6fq55krgpwrbwzp216plyllaj")))

(define-public crate-float-pretty-print-0.1.1 (c (n "float-pretty-print") (v "0.1.1") (h "0cd2kiv01aprspl7jrzpan6y3r9pfn117lqri8048qkm1b2wvqfa")))

