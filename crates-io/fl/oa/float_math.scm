(define-module (crates-io fl oa float_math) #:use-module (crates-io))

(define-public crate-float_math-0.0.1 (c (n "float_math") (v "0.0.1") (h "0312vlda1ghcgi84rfq47z1w1ww6wnrzk9zvsqmvnpzlf4mifabs") (f (quote (("portable_simd") ("core_intrinsics"))))))

(define-public crate-float_math-0.0.2 (c (n "float_math") (v "0.0.2") (d (list (d (n "bytemuck") (r "^1.7.3") (f (quote ("nightly_portable_simd"))) (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 2)))) (h "10q7ib546q2zql2d14w4g54dkhg0ym07zjcbc5105nxdsgl1ys6g") (f (quote (("portable_simd") ("core_intrinsics"))))))

(define-public crate-float_math-0.0.3 (c (n "float_math") (v "0.0.3") (d (list (d (n "bytemuck") (r "^1.7.3") (f (quote ("nightly_portable_simd"))) (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 2)))) (h "0k1bf8k2p8xyb6mxvpxkxzbc6v295ssr9pi86a93jy8lzhxidv2l") (f (quote (("portable_simd") ("core_intrinsics"))))))

