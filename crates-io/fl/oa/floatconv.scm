(define-module (crates-io fl oa floatconv) #:use-module (crates-io))

(define-public crate-floatconv-0.1.0 (c (n "floatconv") (v "0.1.0") (h "17mx8afl91dqb8sp1273xpz0h0fsviijzxnmm929p7pf0gacw7br")))

(define-public crate-floatconv-0.2.0 (c (n "floatconv") (v "0.2.0") (h "16f1266z3ykp8hjrcslags49sixphph88skykw04y4hm62ll4y5h")))

(define-public crate-floatconv-0.2.1 (c (n "floatconv") (v "0.2.1") (h "1ahn5ddwb2f8mp5kdr5wvw5xzi79kbw5k3rzfxy670zmqds8a11r")))

(define-public crate-floatconv-0.2.2 (c (n "floatconv") (v "0.2.2") (h "1mmxymafsb1nap255k7rjf098bzci06nv32667wvyszq6r2byzz5") (y #t)))

(define-public crate-floatconv-0.2.3 (c (n "floatconv") (v "0.2.3") (h "0z4yrd6jh6f8ww85i2408h850pwhla38mirzsr1pnbxfywdgavng")))

(define-public crate-floatconv-0.2.4 (c (n "floatconv") (v "0.2.4") (h "1f47gzshpwwwayxwkqrf8ml6crbqqyjssl26xa8fwiz2ls2fmbgi")))

(define-public crate-floatconv-0.2.5 (c (n "floatconv") (v "0.2.5") (h "0p2gc7pbicfcs7cvsh31dwjh1pmvf3y04s4zpw1w48r2i191hham")))

(define-public crate-floatconv-0.2.6 (c (n "floatconv") (v "0.2.6") (h "02l0bi652ykmf5grndxpzw0b25gh1bcydr46k3ipqn7dy26vv0wf")))

(define-public crate-floatconv-0.2.7 (c (n "floatconv") (v "0.2.7") (h "0ph1bmmr0n15vd5yykxghgjywg5z75kn31bn2qr9bvjn3s7ylryj")))

(define-public crate-floatconv-0.2.8 (c (n "floatconv") (v "0.2.8") (h "0mgfpr8n5swi302a8byl9hyqywqkghhfvx63wi70r9y0a5qdp3lz")))

