(define-module (crates-io fl oa floatpack) #:use-module (crates-io))

(define-public crate-floatpack-0.1.0 (c (n "floatpack") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "bitpacking") (r "^0.8.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.17.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.17") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "034s0imlib4c0x9as4j3kc86s1qjvyk16ly9z1pd7r8vsi5mh3bl")))

