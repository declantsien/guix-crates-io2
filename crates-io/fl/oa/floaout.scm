(define-module (crates-io fl oa floaout) #:use-module (crates-io))

(define-public crate-floaout-0.1.0 (c (n "floaout") (v "0.1.0") (h "1g4dyqq5xbf6fcb2b08j4birkr2fi8z0il6x0m3hmq6mql6w4mv2")))

(define-public crate-floaout-0.2.0 (c (n "floaout") (v "0.2.0") (d (list (d (n "mpl") (r "^0.1") (d #t) (k 0)) (d (n "mycrc") (r "^0.3") (d #t) (k 0)))) (h "15clkcsl2dhh0b3xlvaq4vyfvib3rhl23sznspyybzzbyglnnnw6")))

(define-public crate-floaout-0.2.1 (c (n "floaout") (v "0.2.1") (d (list (d (n "mpl") (r "^0.2") (d #t) (k 0)) (d (n "mpl-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "mycrc") (r "^0.3") (d #t) (k 0)))) (h "0gdqg3j5c219wsfx8swd03ddh9wzk5y5mah02swni5lz2jc3v0ba")))

(define-public crate-floaout-0.2.2 (c (n "floaout") (v "0.2.2") (d (list (d (n "mpl") (r "^0.2") (d #t) (k 0)) (d (n "mpl-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "mycrc") (r "^0.3") (d #t) (k 0)))) (h "0cilr82z3x7ivsxfyckb67bdvvzsf8idzlfbq6kf3sqdbbmvgfwv")))

