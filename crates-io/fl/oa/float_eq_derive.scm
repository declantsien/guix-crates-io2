(define-module (crates-io fl oa float_eq_derive) #:use-module (crates-io))

(define-public crate-float_eq_derive-0.4.0 (c (n "float_eq_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05xnapw7lwv0pfcldcxprr8lsq5pcfyh64xnr389k0d5k9zl8anb")))

(define-public crate-float_eq_derive-0.4.1 (c (n "float_eq_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mvdpmp74fcf3jkrvbvcva135q70whzpprj5vhwx9jkpzkfb3015")))

(define-public crate-float_eq_derive-0.5.0 (c (n "float_eq_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yjz17ba3gbv6s92mwr2664hdd83ha7snq6zkfwmcv5v64yl5v72")))

(define-public crate-float_eq_derive-0.6.0 (c (n "float_eq_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g3cjq3l1rljxiijxb9ys32ghzsxfqfwlrn12qmxcddllj553bqn")))

(define-public crate-float_eq_derive-0.6.1 (c (n "float_eq_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03na88zbdmk7yic1s1xggrvfbz51vpmcp58wc7g5v1vxqwgrvc1i")))

(define-public crate-float_eq_derive-0.7.0 (c (n "float_eq_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1dn4db2zl65l4w46bkapxnzx383ka8h23656xjdipi8x3w8i4xjv")))

(define-public crate-float_eq_derive-1.0.0 (c (n "float_eq_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ffpf0vn6kmrr232yqggcgk1fxwwxr9zjm6bwvcgawhgpzidf4rb")))

(define-public crate-float_eq_derive-1.0.1 (c (n "float_eq_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mkg635lmr0nwjlxz6f5k8g60hrd800i061hrlmpvhdkl96d74j2")))

