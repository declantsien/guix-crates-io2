(define-module (crates-io fl oa float-traits) #:use-module (crates-io))

(define-public crate-float-traits-0.0.2 (c (n "float-traits") (v "0.0.2") (d (list (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "0mvp1vzlapcblchp2hxxrgq5xh4qf0ga6jz6wlnq5fkc9pia4p28")))

(define-public crate-float-traits-0.0.3 (c (n "float-traits") (v "0.0.3") (d (list (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "1wp7mq706n4q3kblld00ibc1rcpmn6xj7qj5bcq5y02k3r6r7lcl")))

