(define-module (crates-io fl oa floating-ui) #:use-module (crates-io))

(define-public crate-floating-ui-0.0.1 (c (n "floating-ui") (v "0.0.1") (d (list (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlDocument" "HtmlElement" "CssStyleDeclaration"))) (d #t) (k 0)))) (h "0xcj61mrkhqwplz23kv67mpab05ynnk3yw9rcx6kki87385rfpj4")))

