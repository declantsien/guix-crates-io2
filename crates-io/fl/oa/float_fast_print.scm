(define-module (crates-io fl oa float_fast_print) #:use-module (crates-io))

(define-public crate-float_fast_print-0.1.0 (c (n "float_fast_print") (v "0.1.0") (h "1hbfvq6d62dgaqa1y8f2pliya3n975r341dld7d24dilqqb7qbb4") (y #t)))

(define-public crate-float_fast_print-0.1.1 (c (n "float_fast_print") (v "0.1.1") (h "14x3ks2jfzf18pcic52gkr8d3fn3ypxma7ka3qgvf5k3lfggv600") (y #t)))

(define-public crate-float_fast_print-0.1.2 (c (n "float_fast_print") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 2)))) (h "00dd0cp7iq0bjnyqfrh2bba0g4w1drm10l1h8pr1qcy739g1vjd5")))

