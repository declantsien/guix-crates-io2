(define-module (crates-io fl oa floating-duration) #:use-module (crates-io))

(define-public crate-floating-duration-0.1.0 (c (n "floating-duration") (v "0.1.0") (h "10spp9ri23pzijir7fi16dcgc6zliai2z8ldg3ym1l8r3mgzk8f3")))

(define-public crate-floating-duration-0.1.1 (c (n "floating-duration") (v "0.1.1") (h "15r6ffq48ia00xf7hqqawil83np06nydksimj7igb1n39nb4dh9g")))

(define-public crate-floating-duration-0.1.2 (c (n "floating-duration") (v "0.1.2") (h "08nz3yk587zn8pi4c8ywa1ffqv5cmyddqyg87c583kfvv5qhpimj")))

