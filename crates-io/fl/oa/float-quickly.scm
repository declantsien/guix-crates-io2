(define-module (crates-io fl oa float-quickly) #:use-module (crates-io))

(define-public crate-float-quickly-0.1.0 (c (n "float-quickly") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)))) (h "0a9nk29sswi54nf147yyk90x2dg5bxqbfxq9kpz18rfaan8kf8wq") (f (quote (("external_doc"))))))

