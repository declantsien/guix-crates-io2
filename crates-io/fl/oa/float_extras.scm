(define-module (crates-io fl oa float_extras) #:use-module (crates-io))

(define-public crate-float_extras-0.1.0 (c (n "float_extras") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "15q6vy9wvjw26bypk01szrmaxd9slfk0zrf4mqf0q8654fan5v5p")))

(define-public crate-float_extras-0.1.1 (c (n "float_extras") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "14wdlk28ylx50wddq9asnjmz3qx8p5dx3yzsw56xvvfxyg4ncxqw")))

(define-public crate-float_extras-0.1.2 (c (n "float_extras") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p2gwgp7q7py3jnp30xvsqfc0sjkm696m66x5xza2gh60q66bcw5")))

(define-public crate-float_extras-0.1.3 (c (n "float_extras") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13y4i5nya3wh9f3pnbg7ymb18mxyjh4r72ljmwxqxmlgby0nxv1v")))

(define-public crate-float_extras-0.1.4 (c (n "float_extras") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pplzdnvwqr7x88k9pp2rh814b19m61hhmbwqy5bdsbiwxxh5la0")))

(define-public crate-float_extras-0.1.5 (c (n "float_extras") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n8bbmhpmr9r8zvdyl45frywn62khgz3anwcjl4rxbhw0bm7788a")))

(define-public crate-float_extras-0.1.6 (c (n "float_extras") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ymb731vf7qdvw20spxavy1a8k8f9fb6v8ziamck38lyckw70axj")))

