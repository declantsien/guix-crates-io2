(define-module (crates-io fl oa floating-distance) #:use-module (crates-io))

(define-public crate-floating-distance-0.1.0 (c (n "floating-distance") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0hzn4mk1scbs4mrdph1gg29bzlf2d4s8adyalflkskfb21cyk9dx") (r "1.60.0")))

(define-public crate-floating-distance-0.2.0 (c (n "floating-distance") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0nb3ckai248lv8aw4lnj7hplb8jpdxm18gy9jmhgmbrqmyxpyh2c") (f (quote (("simd")))) (r "1.60.0")))

(define-public crate-floating-distance-0.3.1 (c (n "floating-distance") (v "0.3.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0s17lxfvp7xz04bcd1qcpkf695jp7f0rzygpipg71wc4anpxbm82") (f (quote (("simd")))) (r "1.60.0")))

