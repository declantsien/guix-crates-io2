(define-module (crates-io fl oa float-format) #:use-module (crates-io))

(define-public crate-float-format-0.1.0 (c (n "float-format") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "040pivcc9ljcljd8kncpv8hnnqakcaw182f34h729cy31anr6ilm")))

(define-public crate-float-format-0.1.1 (c (n "float-format") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "fraction") (r "^0.10") (f (quote ("with-bigint"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xjmj14iv88ial5mp8kxhvragw44yk7ypichvxinr31xk1gfx1mx")))

(define-public crate-float-format-0.1.2 (c (n "float-format") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "fraction") (r "^0.10.0") (f (quote ("with-bigint"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0j602mz2rv78yf7d42xvyvdxn8bggm2rk0vrr1ibpmil1sl579fj")))

(define-public crate-float-format-0.1.3 (c (n "float-format") (v "0.1.3") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "fraction") (r "^0.10.0") (f (quote ("with-bigint"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16qsvf0hxicw3kn9l03851sdyfmkw1zdprnxj268lrshp5c0war3")))

