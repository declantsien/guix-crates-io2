(define-module (crates-io fl oa floader) #:use-module (crates-io))

(define-public crate-floader-0.1.0 (c (n "floader") (v "0.1.0") (d (list (d (n "bootloader") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ovmf-prebuilt") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "1f39j6isn9dqzz42c7h49sdg8vwzd0knccnhrn620zrghjf9qixa") (y #t)))

(define-public crate-floader-0.1.1 (c (n "floader") (v "0.1.1") (d (list (d (n "bootloader") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ovmf-prebuilt") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "0ihq2d22g5yj0hz2niyc1m1ja3bj3byc28mwshhq67cd5l4mcw6s") (y #t)))

