(define-module (crates-io fl oa floating_bar) #:use-module (crates-io))

(define-public crate-floating_bar-0.1.0 (c (n "floating_bar") (v "0.1.0") (d (list (d (n "gcd") (r "^1.2.0") (d #t) (k 0)))) (h "11vfb5ldiphzdrvn4gb3c92p45a7cmma8hkqknqm6wrr1m32d357")))

(define-public crate-floating_bar-0.1.1 (c (n "floating_bar") (v "0.1.1") (d (list (d (n "gcd") (r "^1.2.0") (d #t) (k 0)))) (h "14njjd2c2nmilg415qg3fz567qds18vjsdm0j0zd2sn0ivyx834q")))

(define-public crate-floating_bar-0.1.2 (c (n "floating_bar") (v "0.1.2") (d (list (d (n "gcd") (r "^1.2.0") (d #t) (k 0)))) (h "16h7bcgm0wj07anzmac50mwkc40fpdcbdnb3pk255c7dcjsz0vdm")))

(define-public crate-floating_bar-0.1.3 (c (n "floating_bar") (v "0.1.3") (d (list (d (n "gcd") (r "^1.2.0") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.2") (d #t) (k 0)))) (h "19csfzjsz3k194ys1g3ki827w9jdw9li0xqkglpfwjs5q6z6l1vk")))

(define-public crate-floating_bar-0.2.0 (c (n "floating_bar") (v "0.2.0") (d (list (d (n "gcd") (r "^1.2.0") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.2") (d #t) (k 0)))) (h "0vfisyfb670yi5972wqc1r8a3xxkhizc7q08sw5h4vyd1r45xpv1")))

(define-public crate-floating_bar-0.2.1 (c (n "floating_bar") (v "0.2.1") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.3") (d #t) (k 0)))) (h "14y7870wi3z6ga48ckm4whnil9ji5q79rj37g93fv34xxh48ipmm")))

(define-public crate-floating_bar-0.2.2 (c (n "floating_bar") (v "0.2.2") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.3") (d #t) (k 0)))) (h "0rbmflmphsda3sahsd6zdyi9blbdx5yma74b3c6iwavx7fbnnxn6") (f (quote (("bench")))) (y #t)))

(define-public crate-floating_bar-0.2.3 (c (n "floating_bar") (v "0.2.3") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.3") (d #t) (k 0)))) (h "0dcxmv7y8plx1k0d9018k3pndipv1c6n776rypbz5nkzhvb4xc7h") (f (quote (("bench"))))))

(define-public crate-floating_bar-0.2.4 (c (n "floating_bar") (v "0.2.4") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.3") (d #t) (k 0)))) (h "1fmzq0aaj11v1c53768w7mf1y37i7f2bk4lis7p1s6j10zmc6kbx") (f (quote (("denormals") ("bench"))))))

(define-public crate-floating_bar-0.3.0 (c (n "floating_bar") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.3") (d #t) (k 0)))) (h "09gq0jnzk5ww1fx571ir4v8s0kq5qshi22fiaayvkcab7m9gcr3b") (f (quote (("denormals") ("bench"))))))

(define-public crate-floating_bar-0.4.0 (c (n "floating_bar") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "gcd") (r "^2.0.1") (d #t) (k 0)))) (h "14c5vry83lr321x0rz0sf91mnf2wd4203dl71mj8nkb02z2agmf9") (f (quote (("std") ("default" "std") ("bench"))))))

