(define-module (crates-io fl oa float-derive-macros) #:use-module (crates-io))

(define-public crate-float-derive-macros-0.1.0 (c (n "float-derive-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r5sswvc7kzppgyac45028ilb0q2bx8829cqmvlkv9zwygxi81px")))

