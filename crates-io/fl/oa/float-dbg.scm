(define-module (crates-io fl oa float-dbg) #:use-module (crates-io))

(define-public crate-float-dbg-0.1.0 (c (n "float-dbg") (v "0.1.0") (h "0zvp8lxapghfwdcqx94lj59jlz08bgj6rii7x53z3adcf20x7s8x")))

(define-public crate-float-dbg-0.1.1 (c (n "float-dbg") (v "0.1.1") (h "1w2fr9yd42wgl6hzk0ijb96sdcpa96n8w02m8gsf6hc28cx5kljz")))

(define-public crate-float-dbg-0.2.0 (c (n "float-dbg") (v "0.2.0") (h "1vmingxnyjwlvy4sq704wmjj6zmz5qcif0djgvp3vlz1dqbn5rpy")))

(define-public crate-float-dbg-0.3.0 (c (n "float-dbg") (v "0.3.0") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)))) (h "0sqmdr2vgbdp75vjzpar8cchgk3vcfjmny8bba37qpfbphgw7p9l")))

(define-public crate-float-dbg-0.3.1 (c (n "float-dbg") (v "0.3.1") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)))) (h "0acgv654zx576abh3kf3b1x9vzvvnz06w0xkrlcyhxkfjsxlrv53")))

(define-public crate-float-dbg-0.3.2 (c (n "float-dbg") (v "0.3.2") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)))) (h "1w4abkvw9sl2rr9a9w0gv1yswzkm24dnmpfbg5x3i6309as7h2si")))

(define-public crate-float-dbg-0.4.0-alpha.1 (c (n "float-dbg") (v "0.4.0-alpha.1") (d (list (d (n "az") (r "^1.2.0") (d #t) (k 0)))) (h "0ph5abwsy5r4w1h427p20161cnskv61b38r63z61nv3wr0cipmwv")))

