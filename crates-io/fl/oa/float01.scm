(define-module (crates-io fl oa float01) #:use-module (crates-io))

(define-public crate-float01-0.0.0 (c (n "float01") (v "0.0.0") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (o #t) (d #t) (k 0)))) (h "0zzr7f67y9jsn6wkgsl2x7ld94jmj9m33cjsr0glvjzhl2ma873q")))

(define-public crate-float01-0.1.0 (c (n "float01") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (o #t) (d #t) (k 0)))) (h "1rc8sqxqv91vb9ydjb85m1skd289mkadbdz6j0j0srp01v5bwfm9")))

