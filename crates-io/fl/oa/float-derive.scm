(define-module (crates-io fl oa float-derive) #:use-module (crates-io))

(define-public crate-float-derive-0.1.0 (c (n "float-derive") (v "0.1.0") (d (list (d (n "float-derive-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "ordered-float") (r "^3.3.0") (d #t) (k 0)))) (h "1rflpznvqsgq0ij21zlidx3irrvx277vlfm95f0x37kdd1nx0vkx")))

