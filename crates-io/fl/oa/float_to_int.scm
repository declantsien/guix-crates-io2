(define-module (crates-io fl oa float_to_int) #:use-module (crates-io))

(define-public crate-float_to_int-0.1.0 (c (n "float_to_int") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1.0") (f (quote ("num-traits"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0bj8kcs5w68bffq0dmjp1r57jriihka98hpsj81zmvh8a1a60xk1") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "half?/std" "num-traits?/std") ("nightly" "half?/use-intrinsics") ("half" "dep:half" "dep:num-traits"))))))

