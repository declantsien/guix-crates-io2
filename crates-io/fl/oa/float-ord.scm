(define-module (crates-io fl oa float-ord) #:use-module (crates-io))

(define-public crate-float-ord-0.1.0 (c (n "float-ord") (v "0.1.0") (d (list (d (n "pdqsort") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1m6xycq0k73zps3b0a4f1z71ax092c17bxrdr1lw3jzkhh6b4062")))

(define-public crate-float-ord-0.1.1 (c (n "float-ord") (v "0.1.1") (d (list (d (n "pdqsort") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "142sh8hj7jdaq5mg69pl3v3p4wfn9mnh2da52aizz42ip0szkqs7")))

(define-public crate-float-ord-0.1.2 (c (n "float-ord") (v "0.1.2") (d (list (d (n "pdqsort") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1k1myq4npsadpvlq5za3lsjirs0p4z63ydc7v8ww5zd9cma6ri7q")))

(define-public crate-float-ord-0.2.0 (c (n "float-ord") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0kin50365sr3spnbscq43lksymybi99ai9rkqdw90m6vixhlibbv")))

(define-public crate-float-ord-0.3.0 (c (n "float-ord") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "05vm3vh0xp8kr5kzp1fihk5wjz60sirs89n1cajfcpr6vc0n5zy1")))

(define-public crate-float-ord-0.3.1 (c (n "float-ord") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ixmh86lh4vkn8d6q4b306n2hnbvljpnmngnb8f3zpdjs1q031k4")))

(define-public crate-float-ord-0.3.2 (c (n "float-ord") (v "0.3.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0gbvx6dzz8xvj38bj02cajpqd9p5syxjx9jyqpj8414amr4izs4c")))

