(define-module (crates-io fl oa float-cmp) #:use-module (crates-io))

(define-public crate-float-cmp-0.0.1 (c (n "float-cmp") (v "0.0.1") (h "11dl93l6gg458cwsddnnxkbgy479cy1xyw0h26096x2v4bpmfdck")))

(define-public crate-float-cmp-0.0.2 (c (n "float-cmp") (v "0.0.2") (h "04nvhbg94y70qh7npjx39dk029y2axq4i0si1f80ya7amk7xiqkk")))

(define-public crate-float-cmp-0.0.3 (c (n "float-cmp") (v "0.0.3") (h "1f8brqzljy71gdk5xfyvp999p11l6pjqahqb9w4kijph635fsx5q")))

(define-public crate-float-cmp-0.1.0 (c (n "float-cmp") (v "0.1.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0njxrnyz3y8645m062vfwqa8myfnaayl20a9zvj61wabwrsd6zjy")))

(define-public crate-float-cmp-0.1.1 (c (n "float-cmp") (v "0.1.1") (h "0k5z552xz3ii580f83kg1bdc4f07jfmjap1v00zg8vdin0blma51")))

(define-public crate-float-cmp-0.1.2 (c (n "float-cmp") (v "0.1.2") (h "09i9s21c0075jk2rw92apaynwhy1gy8q2y7bw82v7nqhqnrz6cfw")))

(define-public crate-float-cmp-0.1.3 (c (n "float-cmp") (v "0.1.3") (h "0d3wiwik2x5gmwg772yxx2nwkjbz3zikk36wi1s2p6kcfhsllnxh")))

(define-public crate-float-cmp-0.2.1 (c (n "float-cmp") (v "0.2.1") (h "0c0sjjmn59a7gw8623asg7051wk4409kd1gcxdwhnrb1vr3alpim")))

(define-public crate-float-cmp-0.2.2 (c (n "float-cmp") (v "0.2.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0wlxngrxnbgpm44nj46l4p8r013fgrq83jlnzmi91mn05p33pk44")))

(define-public crate-float-cmp-0.2.3 (c (n "float-cmp") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.1") (k 0)))) (h "04f4crd9fmi808cxzwpmisj46lk9bc9xjrhnn9p389q5r0x9sp5f")))

(define-public crate-float-cmp-0.2.4 (c (n "float-cmp") (v "0.2.4") (d (list (d (n "num") (r "^0.1") (k 0)))) (h "05pmdbw3xw56qs2zsw2z87rysq1qb2isj5nmhakg8p82mwj6pf2s")))

(define-public crate-float-cmp-0.2.5 (c (n "float-cmp") (v "0.2.5") (d (list (d (n "num") (r "^0.1") (k 0)))) (h "1w1il86960zsnk6cwbs2c3s2ncnjjswk7pfa717z963dxx1pl488")))

(define-public crate-float-cmp-0.3.0 (c (n "float-cmp") (v "0.3.0") (d (list (d (n "num") (r "^0.1") (k 0)))) (h "1c0hmj46xma5aysz0qb49padhc26aw875whx6q6rglsj5dqpds1b")))

(define-public crate-float-cmp-0.4.0 (c (n "float-cmp") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0036jb8ry4h83n319jb20b5yvyfyq8mx8dkxnyjm22nq8fl8yjhk")))

(define-public crate-float-cmp-0.4.1 (c (n "float-cmp") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0qmlz3hnal801gdjgf21xm4z65d6m004ccaq883klym2yq2ryiw1") (f (quote (("default" "num-traits")))) (y #t)))

(define-public crate-float-cmp-0.5.0 (c (n "float-cmp") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0370hkwwlnvd2kqz650yhgy3qyv2pnf43n9kbwi50d0qjiikmbhp") (f (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.5.1 (c (n "float-cmp") (v "0.5.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0kan284ij0nn6nc93524y1r5bsmpch95fiwz6p6hlfpqiqmrki7h") (f (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.5.2 (c (n "float-cmp") (v "0.5.2") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0pjvnhmph9ij96c45nfqhb2352bfdv6lz7yhva2810d297jfxx3y") (f (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.5.3 (c (n "float-cmp") (v "0.5.3") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "03hmx3n48hjm0x1ig84n1j87kzp75lzr6cj1sgi6a6pykgn4n8km") (f (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.6.0 (c (n "float-cmp") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0zb1lv3ga18vsnpjjdg87yazbzvmfwwllj3aiid8660rp3qw8qns") (f (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.7.0 (c (n "float-cmp") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0s4fmjzpdg7459i7xsx88jrzd31ssa6zrhg5wx3v0m6j0gx1p6j9") (f (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.8.0 (c (n "float-cmp") (v "0.8.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "1i56hnzjn5pmrcm47fwkmfxiihk7wz5vvcgpb0kpfhzkqi57y9p1") (f (quote (("std") ("ratio" "num-traits") ("default" "ratio"))))))

(define-public crate-float-cmp-0.9.0 (c (n "float-cmp") (v "0.9.0") (d (list (d (n "num-traits") (r "^0.2.1") (o #t) (k 0)))) (h "1i799ksbq7fj9rm9m82g1yqgm6xi3jnrmylddmqknmksajylpplq") (f (quote (("std") ("ratio" "num-traits") ("default" "ratio"))))))

