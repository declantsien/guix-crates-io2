(define-module (crates-io fl oa floating-ui-core) #:use-module (crates-io))

(define-public crate-floating-ui-core-0.0.1 (c (n "floating-ui-core") (v "0.0.1") (d (list (d (n "floating-ui-utils") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "13czsjwci62xhgg2za06sfzpbapjnmcf72b3pqy0fqgs340s0592")))

(define-public crate-floating-ui-core-0.0.2 (c (n "floating-ui-core") (v "0.0.2") (d (list (d (n "floating-ui-utils") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1ghgs7kvzgic9n96y6kxkvlfmpd16jf3fxnzml6qabwbpwbz3dlw")))

(define-public crate-floating-ui-core-0.0.3 (c (n "floating-ui-core") (v "0.0.3") (d (list (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "floating-ui-utils") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "047d0k39chiz4aqfkfsmvcl9n5g1zq9zc4xnfrzs3x0fvbl1n3f1")))

(define-public crate-floating-ui-core-0.0.4 (c (n "floating-ui-core") (v "0.0.4") (d (list (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "floating-ui-utils") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "00lkx28s03s9b2pasvzsaxpi05g29pql55md3kgbbyrv0w79fbqj")))

(define-public crate-floating-ui-core-0.0.5 (c (n "floating-ui-core") (v "0.0.5") (d (list (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "floating-ui-utils") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0vjc00c4n7b5nlbw0a7n6ygswq8j9z96x058sdb0yhfgbw67nc3h")))

(define-public crate-floating-ui-core-0.0.6 (c (n "floating-ui-core") (v "0.0.6") (d (list (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "floating-ui-utils") (r "^0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1k9786k4gxysi5zi19dg75gmp2fihqqbr6jl9svv5irx4b3wz6zz")))

(define-public crate-floating-ui-core-0.0.7 (c (n "floating-ui-core") (v "0.0.7") (d (list (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "floating-ui-utils") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1j42gi8v4isvs5lmgn6694qchd5b1hnf3xi61w21g3qgh02jd3gs")))

(define-public crate-floating-ui-core-0.0.8 (c (n "floating-ui-core") (v "0.0.8") (d (list (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "floating-ui-utils") (r "^0.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0wi6imls6bl9s3jdydr7mwa636ys3y3xd5rvmzki8ch39yd22688")))

