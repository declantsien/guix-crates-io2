(define-module (crates-io fl oa float_eq) #:use-module (crates-io))

(define-public crate-float_eq-0.1.0 (c (n "float_eq") (v "0.1.0") (h "01d3g0qaqlrls2vmiim0rqrbnwirg27z3d6a38ql6wxy86f0g3x3")))

(define-public crate-float_eq-0.1.1 (c (n "float_eq") (v "0.1.1") (h "00dkb3mijnvr37mbzxwz0p31m9dqkwfhyhwkfdl5ifmnnxy41s5f")))

(define-public crate-float_eq-0.1.2 (c (n "float_eq") (v "0.1.2") (h "09cn5s3gxng6hinxjrzw5a3m6bfmz8hqz961zix9lpq03n6nyp63") (f (quote (("std") ("default" "std"))))))

(define-public crate-float_eq-0.1.3 (c (n "float_eq") (v "0.1.3") (h "1pbgag1mkhkss9agbjvkhla15q6vmm8wyqk6i3amlq4z0kydkjas") (f (quote (("std") ("default" "std"))))))

(define-public crate-float_eq-0.2.0 (c (n "float_eq") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0rxxxkm1vcpk71xlrfbnglnvrf76yabpph7kqa8z4yhhq5rscx56") (f (quote (("std") ("num" "num-complex") ("default" "std"))))))

(define-public crate-float_eq-0.3.0 (c (n "float_eq") (v "0.3.0") (d (list (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1s8kqcncj2f6j1zr97s9163axkq3r374s6gb4ja069rmy3fb2zaw") (f (quote (("std") ("num" "num-complex") ("default" "std"))))))

(define-public crate-float_eq-0.3.1 (c (n "float_eq") (v "0.3.1") (d (list (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1pnj6s17nma6l1ikkij24syq1b4c0f03s3djjfbb5frmvk57irf7") (f (quote (("std") ("num" "num-complex") ("default" "std"))))))

(define-public crate-float_eq-0.4.0 (c (n "float_eq") (v "0.4.0") (d (list (d (n "float_eq_derive") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "16q4rr33kjc7ws01c32qlkzd1in7jafs91gvrl0y6l9bqv5faana") (f (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.4.1 (c (n "float_eq") (v "0.4.1") (d (list (d (n "float_eq_derive") (r "=0.4.1") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1icywlrkkw4c85f96699h1nz0z257k92n5lykfplvx27bki932k3") (f (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.5.0 (c (n "float_eq") (v "0.5.0") (d (list (d (n "float_eq_derive") (r "=0.5.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1wssi0wkaswbac65m7919scgzx265w4lqswiz520bp1w5y8bc8zv") (f (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.6.0 (c (n "float_eq") (v "0.6.0") (d (list (d (n "float_eq_derive") (r "=0.6.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1fab45xkzhb2pppbnyi20x90nh4fmpyig17a0arsnm7ghdi0b3sc") (f (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.6.1 (c (n "float_eq") (v "0.6.1") (d (list (d (n "float_eq_derive") (r "=0.6.1") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0biga4dqd8q2zrg4rc75qk3yz3y04xgxwpkkyli0kbzcixr4lcxa") (f (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.7.0 (c (n "float_eq") (v "0.7.0") (d (list (d (n "float_eq_derive") (r "=0.7.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0g82c7djwz0dlyg8ywy3b3rygbdwcbgllmrwwql7i6jgx6ck9mf1") (f (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-1.0.0 (c (n "float_eq") (v "1.0.0") (d (list (d (n "float_eq_derive") (r "=1.0.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0n6cabjig66jqjcycgrsqfbcis7vrjdqfa3x93b03djsqdln96xm") (f (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-1.0.1 (c (n "float_eq") (v "1.0.1") (d (list (d (n "float_eq_derive") (r "=1.0.1") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0lxqxkvdy5zh3qsksavpcazg57cbyjy9p54m16x13bfq8lqhxa18") (f (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

