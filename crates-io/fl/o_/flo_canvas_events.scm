(define-module (crates-io fl o_ flo_canvas_events) #:use-module (crates-io))

(define-public crate-flo_canvas_events-0.3.0 (c (n "flo_canvas_events") (v "0.3.0") (d (list (d (n "flo_canvas") (r "^0.3") (d #t) (k 0)))) (h "184n4nwbghrwliwc4v871lxghjgph74qqymrs875bmbakp6gdqa9")))

(define-public crate-flo_canvas_events-0.3.1 (c (n "flo_canvas_events") (v "0.3.1") (d (list (d (n "flo_canvas") (r "^0.3") (d #t) (k 0)))) (h "060whcc4v80wcb652nsn3hbxwpw0bxxh7l584c6pb3hz8s8qyv26")))

