(define-module (crates-io fl o_ flo_stream) #:use-module (crates-io))

(define-public crate-flo_stream-0.1.0 (c (n "flo_stream") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "04x40251v5kfcrc0kjb1y9a72nra9kdyxx829mbms8f2nj4kpbpb")))

(define-public crate-flo_stream-0.1.1 (c (n "flo_stream") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0w0iwlw4lcq101y4xjl8p61jmw4wrkaqyrc4pw50yvag71q9dphd")))

(define-public crate-flo_stream-0.1.2 (c (n "flo_stream") (v "0.1.2") (d (list (d (n "desync") (r "^0.2.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1481b6j1grhwqnygambr9gibfal3508flb0c82haiwb4pszq277g")))

(define-public crate-flo_stream-0.2.0 (c (n "flo_stream") (v "0.2.0") (d (list (d (n "desync") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1z1h3ybfk4a4zhyynhbazpa7hrvknjvsxq73491gb1jkjpj7wj53")))

(define-public crate-flo_stream-0.3.0 (c (n "flo_stream") (v "0.3.0") (d (list (d (n "desync") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "158pxp3s56qg1nmzhrfr2p8sbws2dy3cqdkb7mfgan54j36ansb1")))

(define-public crate-flo_stream-0.4.0 (c (n "flo_stream") (v "0.4.0") (d (list (d (n "desync") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "0jb0k7i5v48jvz2vgxms9ggydwq0sxrb7j8skha18xdjcwv0sbmh")))

(define-public crate-flo_stream-0.5.0 (c (n "flo_stream") (v "0.5.0") (d (list (d (n "desync") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "1i9xl75c15ykgzl0rgw0gjkwah8w5qr3diwwjp94cg444sk55wnh")))

(define-public crate-flo_stream-0.6.0 (c (n "flo_stream") (v "0.6.0") (d (list (d (n "desync") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "0w09h0z9xvsfvj2a4haa1857nz9a1n8f05ni0rlwmwx0f1c7w8qv")))

(define-public crate-flo_stream-0.7.0 (c (n "flo_stream") (v "0.7.0") (d (list (d (n "desync") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "0w54ylf5iycl576qfnn9lc5msvafqcrixp7y27xj9adn17dlcwha")))

