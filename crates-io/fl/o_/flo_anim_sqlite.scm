(define-module (crates-io fl o_ flo_anim_sqlite) #:use-module (crates-io))

(define-public crate-flo_anim_sqlite-0.1.0 (c (n "flo_anim_sqlite") (v "0.1.0") (d (list (d (n "desync") (r "^0.1.2") (d #t) (k 0)) (d (n "flo_animation") (r "^0.1.0") (d #t) (k 0)) (d (n "flo_canvas") (r "^0.1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.11.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0hs71ynncwfh37pxxfhh8cnkacjhspa41ha120bjpld851vxfi7f")))

