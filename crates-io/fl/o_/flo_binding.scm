(define-module (crates-io fl o_ flo_binding) #:use-module (crates-io))

(define-public crate-flo_binding-0.1.0 (c (n "flo_binding") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)))) (h "1clslizs5088mwyvfyb6kixb1dqvmwzb5i7l0pzjajyhr1k90jm6")))

(define-public crate-flo_binding-0.1.1 (c (n "flo_binding") (v "0.1.1") (d (list (d (n "desync") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)))) (h "17nb1s9xmql4liwxnk5qja33a4z81n2gpkqdbcwmv8ssaakpzxb3")))

(define-public crate-flo_binding-0.1.2 (c (n "flo_binding") (v "0.1.2") (d (list (d (n "desync") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)))) (h "1n1p78i9hj0awbyvgml41mysfw9045i15dd57j1pmihdzv4gk19w")))

(define-public crate-flo_binding-1.0.0 (c (n "flo_binding") (v "1.0.0") (d (list (d (n "desync") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)))) (h "09zx717ivsy9vjbr2zzf7kwp98s36wcpys0fzjn2s1hrjhydbl2s")))

(define-public crate-flo_binding-1.1.0 (c (n "flo_binding") (v "1.1.0") (d (list (d (n "desync") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0na0hpywpcc0z73a2r2wadc14hgh8p94z68ldjjyfxgzhdfva345")))

(define-public crate-flo_binding-2.0.0 (c (n "flo_binding") (v "2.0.0") (d (list (d (n "desync") (r "^0.7") (d #t) (k 0)) (d (n "flo_rope") (r "^0.1") (d #t) (k 0)) (d (n "flo_stream") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0rzag2fwanr7a1al4dfl2nzpr51gf23ywn1d1gxwip633vh1y156")))

(define-public crate-flo_binding-2.0.1 (c (n "flo_binding") (v "2.0.1") (d (list (d (n "desync") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "flo_rope") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)))) (h "181ggp47npxw4pg6ql1jhpiydzf36gaskxnbrshi84drc36na6ki") (f (quote (("stream" "desync" "futures" "flo_rope") ("default" "stream"))))))

(define-public crate-flo_binding-2.1.0 (c (n "flo_binding") (v "2.1.0") (d (list (d (n "desync") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "flo_rope") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "similar") (r "^2.1") (o #t) (d #t) (k 0)))) (h "12labdgwkzl8llqrkhn531km76cc7lyb78w8vjf0vwqmzvn5mn00") (f (quote (("stream" "desync" "futures") ("rope" "desync" "futures" "flo_rope") ("diff" "similar") ("default" "stream" "rope" "diff"))))))

(define-public crate-flo_binding-2.2.0 (c (n "flo_binding") (v "2.2.0") (d (list (d (n "desync") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "flo_rope") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "similar") (r "^2.1") (o #t) (d #t) (k 0)))) (h "1n7klpgbw40h7n4nzmkjq4fxra2fy7nhdwng33l99wdg57k9cy4p") (f (quote (("stream" "desync" "futures") ("rope" "desync" "futures" "flo_rope") ("diff" "similar") ("default" "stream" "rope" "diff"))))))

