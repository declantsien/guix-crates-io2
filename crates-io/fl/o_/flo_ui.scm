(define-module (crates-io fl o_ flo_ui) #:use-module (crates-io))

(define-public crate-flo_ui-0.1.0 (c (n "flo_ui") (v "0.1.0") (d (list (d (n "desync") (r "^0.1.2") (d #t) (k 0)) (d (n "flo_binding") (r "^0.1.0") (d #t) (k 0)) (d (n "flo_canvas") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "modifier") (r "^0.1.0") (d #t) (k 0)) (d (n "png") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "14l6n9lrqx983kywp5gk0p60yjk7clnmh2sh0hpxp4n9gkvrdilz")))

