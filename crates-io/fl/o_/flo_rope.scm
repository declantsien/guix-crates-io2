(define-module (crates-io fl o_ flo_rope) #:use-module (crates-io))

(define-public crate-flo_rope-0.1.0 (c (n "flo_rope") (v "0.1.0") (h "1gi7p16s6frxzd6gq9xxkqsa1lk9y54jzwc63sjavq6aizjjsklk")))

(define-public crate-flo_rope-0.2.0 (c (n "flo_rope") (v "0.2.0") (h "057zd1a18fzcjjn4ybnkmsxjmchcq6m5klwidan8la8q4hjzy345")))

