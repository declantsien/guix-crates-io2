(define-module (crates-io fl o_ flo_sparse_array) #:use-module (crates-io))

(define-public crate-flo_sparse_array-0.1.0 (c (n "flo_sparse_array") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "176cnirs3qr4lhsn5n1xy87d3lnp313jnpsljww5spgmb92sx869")))

