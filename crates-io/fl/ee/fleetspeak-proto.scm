(define-module (crates-io fl ee fleetspeak-proto) #:use-module (crates-io))

(define-public crate-fleetspeak-proto-0.1.0 (c (n "fleetspeak-proto") (v "0.1.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "1d3yjz8fcami601h9kqnj9b7hqww5zzpfr1c69xp1pvjn709w59l")))

(define-public crate-fleetspeak-proto-0.1.1 (c (n "fleetspeak-proto") (v "0.1.1") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "15g1n61f00mywdpx7wmqrrsj366ljfj3qiq6gxbqm1fsz5irqj3a")))

(define-public crate-fleetspeak-proto-0.1.2 (c (n "fleetspeak-proto") (v "0.1.2") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "1kxppzld5l3dwclc39gi6p3rk27gr3hjn21ysfrzjxwmfn5b8w1i")))

(define-public crate-fleetspeak-proto-0.1.3 (c (n "fleetspeak-proto") (v "0.1.3") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "05lhgaiw6766h4lm9j10a9if305braxmkqkd1d50cfabbmc70dh0")))

(define-public crate-fleetspeak-proto-0.2.0 (c (n "fleetspeak-proto") (v "0.2.0") (d (list (d (n "protobuf") (r "^2.8.2") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.8.2") (d #t) (k 1)))) (h "1dm2bp5b5d9pjys2knvb5bnzmm4ngkl69421b8i5p4hvdjdsas3v")))

(define-public crate-fleetspeak-proto-0.2.1 (c (n "fleetspeak-proto") (v "0.2.1") (d (list (d (n "protobuf") (r "=2.8.2") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "=2.8.2") (d #t) (k 1)))) (h "195kmza940nsnzqicjrzwqp6fmv1bnhgnpxxa69xjb2j1hw9l3q3")))

(define-public crate-fleetspeak-proto-0.3.0 (c (n "fleetspeak-proto") (v "0.3.0") (d (list (d (n "protobuf") (r "=2.8.2") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "=2.8.2") (d #t) (k 1)))) (h "16s73fll1m0gbcc48lpfr0dapb48bhnfc79b2sch0r1j11l95s77")))

(define-public crate-fleetspeak-proto-0.3.1 (c (n "fleetspeak-proto") (v "0.3.1") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.27.1") (d #t) (k 1)))) (h "13jc4jlcyq7glwb5y3v48yqjflz5mmmq3hmw88m9apxiiklg2zvz")))

(define-public crate-fleetspeak-proto-0.4.0 (c (n "fleetspeak-proto") (v "0.4.0") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.27.1") (d #t) (k 1)))) (h "08fni132nzccygpzbcv9rqlzq5wchfmax7gx7gxh56qnqkl0v7zy")))

(define-public crate-fleetspeak-proto-0.4.1 (c (n "fleetspeak-proto") (v "0.4.1") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.27.1") (d #t) (k 1)))) (h "10i7kp3993c9y4qgx43zhqk731ja16kndipdnnw71rqj0v00j2dz")))

