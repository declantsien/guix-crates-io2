(define-module (crates-io fl ee fleet) #:use-module (crates-io))

(define-public crate-fleet-0.1.0 (c (n "fleet") (v "0.1.0") (d (list (d (n "hyper") (r ">= 0.3.4") (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.7") (d #t) (k 0)))) (h "0rxzhw0qcps3qpr4cva1ib1dwf86429247cf79nbm60kijx335lp")))

(define-public crate-fleet-0.2.0 (c (n "fleet") (v "0.2.0") (d (list (d (n "hyper") (r ">= 0.3.4") (d #t) (k 0)) (d (n "retry") (r ">= 0.1.0") (d #t) (k 2)) (d (n "rustc-serialize") (r ">= 0.3.7") (d #t) (k 0)) (d (n "url") (r ">= 0.2.28") (d #t) (k 0)))) (h "1r3pflj4cd2fkxd77q6czr6s1ch6c0hhp98ylg2mndcq5mzi695q")))

