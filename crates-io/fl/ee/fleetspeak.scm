(define-module (crates-io fl ee fleetspeak) #:use-module (crates-io))

(define-public crate-fleetspeak-0.1.0 (c (n "fleetspeak") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "0wy4hd85955lfccaykl6vak4iqa2bbmmfxd7f1m8ba51sk2x4q0p")))

(define-public crate-fleetspeak-0.1.1 (c (n "fleetspeak") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "1b83ywjni09n9rw3ilqsslsmwr8pah87ca5k0shndfc9hmvv78vc")))

(define-public crate-fleetspeak-0.1.2 (c (n "fleetspeak") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "0nq8qlm3ifb53rb5mw9a9ri96x7i20rhyhc8dqba9yxj0f1qbznq")))

(define-public crate-fleetspeak-0.1.3 (c (n "fleetspeak") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "13zrn69l79lbwqh8cqnkb39rx521wpgm59j8i5b9p3vsynv6hcqf")))

(define-public crate-fleetspeak-0.2.0 (c (n "fleetspeak") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.2") (d #t) (k 0)))) (h "0idv89j7j4yl847wwfvif5bpm21y7d14swi3dqhy6zmn6pw3hz6m")))

(define-public crate-fleetspeak-0.2.1 (c (n "fleetspeak") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "protobuf") (r "=2.8.2") (d #t) (k 0)))) (h "0r5373ggnv021xw2n3p6yghcm0c83mnaqmkbd5925l2akfw6fqrg")))

(define-public crate-fleetspeak-0.3.0 (c (n "fleetspeak") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "protobuf") (r "=2.8.2") (d #t) (k 0)))) (h "06npiq1yx8gbiicavi5b7dbvygzvbfgcqv2xnhf6gnb6fz3sx8fk")))

(define-public crate-fleetspeak-0.3.1 (c (n "fleetspeak") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)))) (h "0v368gp9biv9802srbgiqibyi4khi5d3hr368cwliymmhxhf4924")))

(define-public crate-fleetspeak-0.4.0 (c (n "fleetspeak") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)))) (h "0hnxhj9p5va88sxjc2ibjhcsrr79x9ra3likzhgfbmjivkrc3acr")))

(define-public crate-fleetspeak-0.4.1 (c (n "fleetspeak") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "fleetspeak-proto") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "1m6yw1wd0a448lz284cf08vw3f1150m50nqb5y6hzin9ykjj29jw")))

