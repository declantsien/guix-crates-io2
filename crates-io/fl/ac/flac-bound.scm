(define-module (crates-io fl ac flac-bound) #:use-module (crates-io))

(define-public crate-flac-bound-0.1.0 (c (n "flac-bound") (v "0.1.0") (d (list (d (n "flac-sys") (r "^0.1") (d #t) (k 0)))) (h "1qybmlm3bc94cmwriy16sfzag2jl6j0h9wfbzrd00q8w5asapz8c")))

(define-public crate-flac-bound-0.2.0 (c (n "flac-bound") (v "0.2.0") (d (list (d (n "flac-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libflac-sys") (r "^0.1") (o #t) (d #t) (k 0)))) (h "004arcqglwn8xnn3rlsksmj0r8islnyvvksjgn5kjqvr87grj03m") (f (quote (("libflac" "libflac-sys") ("flac" "flac-sys") ("default" "flac"))))))

(define-public crate-flac-bound-0.3.0 (c (n "flac-bound") (v "0.3.0") (d (list (d (n "flac-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libflac-sys") (r "^0.2") (o #t) (k 0)))) (h "0v22ilyzqmndcchf0v0mvz6kdxdl6ka1ysnqs1i2x61fc5wxqf6l") (f (quote (("libflac-noogg" "libflac-nobuild" "libflac-sys/build-flac") ("libflac-nobuild" "libflac-sys") ("libflac" "libflac-nobuild" "libflac-sys/build-ogg") ("flac" "flac-sys") ("default" "flac"))))))

