(define-module (crates-io fl ac flack) #:use-module (crates-io))

(define-public crate-flack-0.1.0 (c (n "flack") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0r23jmrvkb32y9zb4ln88bdi34rprvfv2fkphb537wvw59ln033p")))

(define-public crate-flack-0.2.0 (c (n "flack") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1r98g9pvf0v4zb5g78ibnar9ik4v9snsf4bydi8snlbz651cqlhz")))

