(define-module (crates-io fl ac flac_music) #:use-module (crates-io))

(define-public crate-flac_music-0.2.0 (c (n "flac_music") (v "0.2.0") (d (list (d (n "druid") (r "^0.7.0") (f (quote ("im" "svg" "image" "png"))) (d #t) (k 0)) (d (n "ffmpeg-next") (r "^5.1.1") (d #t) (k 0)) (d (n "rodio") (r "^0.16.0") (f (quote ("symphonia-all" "flac" "vorbis" "wav" "mp3"))) (d #t) (k 0)))) (h "1zhwgvn48fs9arg42pwh4isanvcchb1swghmf8vm0q6pwn5kbwds")))

(define-public crate-flac_music-0.2.1 (c (n "flac_music") (v "0.2.1") (d (list (d (n "druid") (r "^0.7.0") (f (quote ("im" "svg" "image" "png"))) (d #t) (k 0)) (d (n "ffmpeg-next") (r "^5.1.1") (d #t) (k 0)) (d (n "rodio") (r "^0.16.0") (f (quote ("symphonia-all" "flac" "vorbis" "wav" "mp3"))) (d #t) (k 0)))) (h "0631q39qrdw01x8h72m4nkzbd13qm32d4iyq2x6lvqg9psi666zd")))

(define-public crate-flac_music-0.2.2 (c (n "flac_music") (v "0.2.2") (d (list (d (n "druid") (r "^0.7.0") (f (quote ("im" "svg" "image" "png"))) (d #t) (k 0)) (d (n "ffmpeg-next") (r "^5.1.1") (d #t) (k 0)) (d (n "rodio") (r "^0.16.0") (f (quote ("symphonia-all" "flac" "vorbis" "wav" "mp3"))) (d #t) (k 0)))) (h "141cqfvxlrqfjk4r4h2nhbj466saaq6w7b0p69sfgz1ybm8w4rj9")))

(define-public crate-flac_music-0.2.3 (c (n "flac_music") (v "0.2.3") (d (list (d (n "druid") (r "^0.7.0") (f (quote ("im" "svg" "image" "png"))) (d #t) (k 0)) (d (n "ffmpeg-next") (r "^5.1.1") (d #t) (k 0)) (d (n "rodio") (r "^0.16.0") (f (quote ("symphonia-all" "flac" "vorbis" "wav" "mp3"))) (d #t) (k 0)))) (h "190irj2w51fm5x2jmn384807chzbx7xbf6wcv7r47hd0lmhsbni0")))

