(define-module (crates-io fl ac flac) #:use-module (crates-io))

(define-public crate-flac-0.1.0 (c (n "flac") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "hound") (r "^1.1.0") (d #t) (k 0)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0j636rzhb45dy1w0ysvnv630m980sf9cm2k3j3yczb3vzrbmnk9w")))

(define-public crate-flac-0.2.0 (c (n "flac") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "hound") (r "^1.1.0") (d #t) (k 0)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "19n0f9kx9nkijj4iz3gdvkqxdkqcgn9yw93150c8606qrramjwsl")))

(define-public crate-flac-0.3.0 (c (n "flac") (v "0.3.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "hound") (r "^1.1.0") (d #t) (k 0)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1jr323x0zdh0xqzygbgzw6f5zchkws6a1qx4915hlp4k561zdv9m")))

(define-public crate-flac-0.4.0 (c (n "flac") (v "0.4.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "hound") (r "^1.1.0") (d #t) (k 0)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "03pi6kcc53ahk2jbhbp27lb9hrhwpdmc0d3j3ax9d1ncj99ryd0h")))

(define-public crate-flac-0.5.0 (c (n "flac") (v "0.5.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "hound") (r "^1.1.0") (d #t) (k 0)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0sc9644w0xrhpm3kdym3zqq5wr8kcc3hxds91kf3spw6l4h6c7dy")))

