(define-module (crates-io fl ai flair) #:use-module (crates-io))

(define-public crate-flair-0.0.0 (c (n "flair") (v "0.0.0") (h "07fg7rgxx1n6v6d91kcajpa2cm97aawwywmk8pcsl2zzrvgqd0y6")))

(define-public crate-flair-0.0.1 (c (n "flair") (v "0.0.1") (h "1pq4m6636i6k54rvypyrvihp9252gvhsrf6vxbx8bbgild9ij5m2")))

(define-public crate-flair-0.0.2 (c (n "flair") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "chrono") (r "^0.4.29") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "deadpool-sqlite") (r "^0.5.0") (f (quote ("rt_tokio_1" "serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("serde" "serde_json"))) (d #t) (k 0)) (d (n "ts-rs") (r "^7.0.0") (f (quote ("uuid" "url" "chrono" "chrono-impl"))) (d #t) (k 0)))) (h "0mqmrcigrwy11n865si88hlc3d8n64qv902jxpaq8nlnkmd2y7y1")))

