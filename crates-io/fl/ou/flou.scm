(define-module (crates-io fl ou flou) #:use-module (crates-io))

(define-public crate-flou-0.1.0 (c (n "flou") (v "0.1.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "0dk06crgvip51qn5w42j42gp0pp0in7vwa0ncmhanifsnqwzxf1c")))

