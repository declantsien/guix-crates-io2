(define-module (crates-io fl ou flou_cli) #:use-module (crates-io))

(define-public crate-flou_cli-0.1.0 (c (n "flou_cli") (v "0.1.0") (d (list (d (n "flou") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0h2id2p6qwdcj9qmwmbgk3g0kjywkkwnpp0c2qrhkmbm0h241n6q")))

