(define-module (crates-io fl ou floui) #:use-module (crates-io))

(define-public crate-floui-0.1.0 (c (n "floui") (v "0.1.0") (d (list (d (n "floui-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "02217jlbv5jqqv670jz8kjsh3clqi23bfh7y1mh0ck8h0jr9y5py")))

(define-public crate-floui-0.1.1 (c (n "floui") (v "0.1.1") (d (list (d (n "floui-sys") (r "=0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "16bjzxmgszi883xbcya0z1c6mm18ybv90wph4fsg1xj0s8wiph3v") (f (quote (("ios-webview" "floui-sys/ios-webview"))))))

(define-public crate-floui-0.1.2 (c (n "floui") (v "0.1.2") (d (list (d (n "floui-sys") (r "=0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "046rnyw6ql769af05v2a7adhgaci6s2qvsdixs5817k26qb4wgc6") (f (quote (("ios-webview" "floui-sys/ios-webview"))))))

(define-public crate-floui-0.1.3 (c (n "floui") (v "0.1.3") (d (list (d (n "floui-sys") (r "=0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "05jg0s3xcd0pmm2w14irwspn9hdlrfm4ynp52c3cshhfnl2c0apj") (f (quote (("ios-webview" "floui-sys/ios-webview"))))))

(define-public crate-floui-0.1.4 (c (n "floui") (v "0.1.4") (d (list (d (n "floui-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0ns5k5v5nzbn3gdrqwf7g53zdsh7wcwcd2xgza9c98aj5kik9jy9") (f (quote (("ios-webview" "floui-sys/ios-webview"))))))

(define-public crate-floui-0.1.5 (c (n "floui") (v "0.1.5") (d (list (d (n "floui-sys") (r "=0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1yi4mfhf40kb6ynbhvvg2wkzrzwbz2j8nqhdx0s1z81zzvdg9vpz") (f (quote (("ios-webview" "floui-sys/ios-webview"))))))

