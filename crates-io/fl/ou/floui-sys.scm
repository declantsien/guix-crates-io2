(define-module (crates-io fl ou floui-sys) #:use-module (crates-io))

(define-public crate-floui-sys-0.1.0 (c (n "floui-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(android))") (k 1)))) (h "16a4zzv89lrmk8003avcawfbs4444kf3ma84hq8rn9m7az4xzx6p")))

(define-public crate-floui-sys-0.1.1 (c (n "floui-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(android))") (k 1)))) (h "1i8shdywfkms8x5qhai415bg7z5jkhw5xz9zp8d9ww44g6hi9ql8")))

(define-public crate-floui-sys-0.1.2 (c (n "floui-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(android))") (k 1)))) (h "1mvqlh9inq7bvm9jcks5xbadkdi0q2bd869gr8k44y7hinrr1952") (f (quote (("ios-webview"))))))

(define-public crate-floui-sys-0.1.3 (c (n "floui-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(android))") (k 1)))) (h "0nnfvnj2dfp0gblzrh54ghxcrj90cw97lwbaaxnq5ws08940xfgy") (f (quote (("ios-webview"))))))

(define-public crate-floui-sys-0.1.4 (c (n "floui-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(android))") (k 1)))) (h "1yi82jzb8fxs209p20m2yr4rcjfm5k4y2ay0ild61jjzfaa6rv4x") (f (quote (("ios-webview"))))))

