(define-module (crates-io fl og flog) #:use-module (crates-io))

(define-public crate-flog-0.1.0 (c (n "flog") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 2)) (d (n "sled") (r "^0.34.3") (d #t) (k 2)))) (h "057wzr8jn3lr96qqq95bmhxj1pqv3lw43v83vymk514j2lp5w3aj")))

(define-public crate-flog-0.1.1 (c (n "flog") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pprof") (r "^0.3.18") (f (quote ("flamegraph"))) (d #t) (k 0)))) (h "1nvbvfqsfg3ll8qsla2kxkf44lhg7csq5skq43ikr5r3q0z8fcx6")))

(define-public crate-flog-0.1.2 (c (n "flog") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pprof") (r "^0.3.18") (f (quote ("flamegraph"))) (d #t) (k 0)))) (h "1bmgvxzhc082bim75fwmchq8kk307ixwww5nnrp0prrdbqs79nwj")))

