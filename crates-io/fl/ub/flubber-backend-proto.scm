(define-module (crates-io fl ub flubber-backend-proto) #:use-module (crates-io))

(define-public crate-flubber-backend-proto-0.1.0-alpha (c (n "flubber-backend-proto") (v "0.1.0-alpha") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "sval") (r "^0.4.7") (f (quote ("derive" "serde"))) (d #t) (k 0)))) (h "0wfs3cfin92yb2idfigdp5agmqrjk281yvp8diqqj57wca1jj7js")))

