(define-module (crates-io fl av flavio) #:use-module (crates-io))

(define-public crate-flavio-0.1.0 (c (n "flavio") (v "0.1.0") (h "0z80ydaflhqin298mlzsy822cir3rm113asdvx11d4f6wykgsymi")))

(define-public crate-flavio-0.1.1 (c (n "flavio") (v "0.1.1") (h "1p7pxvkj3wl086zdy5pp5jap1km397rirg4wymrvd3m35rvmbh9f")))

(define-public crate-flavio-0.1.2 (c (n "flavio") (v "0.1.2") (h "1vj5bmgvx1wkpl4lh1lq83z50rh1fvcp5g88rp5krdgvmxhkn569")))

(define-public crate-flavio-0.1.3 (c (n "flavio") (v "0.1.3") (h "1slqs06z1nximq04k9w1k4d1nx3ysxsfm4ypv11qcfmrn66ppf8w") (f (quote (("math") ("64") ("32"))))))

(define-public crate-flavio-0.1.4 (c (n "flavio") (v "0.1.4") (h "12yhdqyw1is9lj7mpg34w3x8m7iha8yc6bygkzvhdk14ya9nbq8d") (f (quote (("math") ("64") ("32"))))))

(define-public crate-flavio-0.1.5 (c (n "flavio") (v "0.1.5") (h "08mplhv2z0xl4fs7215588yjifki5aivq5q5sfp66njczkvi2qk3") (f (quote (("math") ("64") ("32"))))))

(define-public crate-flavio-0.1.6 (c (n "flavio") (v "0.1.6") (h "1nmbwdcwnj1hw9gsn28m7zl0cr5khhqqdhc1s0gfblax1fll6aij") (f (quote (("math") ("64") ("32"))))))

(define-public crate-flavio-0.1.7 (c (n "flavio") (v "0.1.7") (h "0v2m97r8b71zw2682dh3wwxdryzkff7bnyy05m0h6kk1ygr8c9v7") (f (quote (("math") ("64") ("32"))))))

(define-public crate-flavio-0.2.0 (c (n "flavio") (v "0.2.0") (h "1bdz40plnkzklcb1ahly0farry9gxkmgda079qpll4drvw6ib2rs") (f (quote (("mechanics" "math") ("math") ("64") ("32"))))))

(define-public crate-flavio-0.2.1 (c (n "flavio") (v "0.2.1") (h "0scimknf5lsaw85gflnyxjfa42vpncdas84v6h7yaashlq83wrc2") (f (quote (("mechanics" "math") ("math") ("constitutive" "mechanics") ("64") ("32"))))))

(define-public crate-flavio-0.2.2 (c (n "flavio") (v "0.2.2") (h "0hx2d2cmh0pzwpq3b83jh5nzffc1li87pk06cjqr98ii5xjzck8y") (f (quote (("mechanics" "math") ("math") ("constitutive" "mechanics") ("64") ("32"))))))

(define-public crate-flavio-0.2.3 (c (n "flavio") (v "0.2.3") (h "1m193gzc4n4bqjs89bgsivxq74cbhzic3szgdgyhrs6b0123ifc6") (f (quote (("mechanics" "math") ("math") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.2.4 (c (n "flavio") (v "0.2.4") (h "1fbc69kwrf92ff8bawmc7zpsfaviblf64d3480j4g4dk91dz94lf") (f (quote (("mechanics" "math") ("math") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.2.5 (c (n "flavio") (v "0.2.5") (h "1c0yy6dicl11v5z4ifjb242lb2ggiiiqihsrswb31hdi8wq074lq") (f (quote (("mechanics" "math") ("math") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.3.0 (c (n "flavio") (v "0.3.0") (h "1s3z6g12ark5lwwyl1ry7w9pyhnc065k7nqhqkxp78a847galhyq") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.3.1 (c (n "flavio") (v "0.3.1") (h "1946la5pw7ffp39lvck7hizl915hflf166ij3mvzcgl9mph75767") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.3.2 (c (n "flavio") (v "0.3.2") (h "0zf4nf8cpixspncg3kx5gs2inxc37430bc5bda4q78vwc07q31mf") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.3.3 (c (n "flavio") (v "0.3.3") (h "05wabd2ggggfgv3jdg4b5zmrscyz0cf9cpp142w1f9nr8h86c6xw") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.3.4 (c (n "flavio") (v "0.3.4") (h "0dipkgbi23dnqp1264qvp6wxrpacragr15xxzdipb94gag1d86rp") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.3.5 (c (n "flavio") (v "0.3.5") (h "1dip1lpjha7n6dm396gznlivxmbmikj0dsc16b8fx43vav14ybac") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.3.6 (c (n "flavio") (v "0.3.6") (h "1a17l8n6y05bzkq03xncapgajp7ddsl12717yk05ya3s5wkaia5l") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.3.7 (c (n "flavio") (v "0.3.7") (h "1892djsxrif5f6h28q8nsyx936kp30bsg1mkan1a9llv0x5p892v") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.4.0 (c (n "flavio") (v "0.4.0") (h "00r1j8xxflspsb9pciqcxad919lcyrcbga84mvh6mbxi9q586mmd") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.4.1 (c (n "flavio") (v "0.4.1") (h "0mvnfg4gxag23s7f0ya3j4m4ibx368mgr2y0gqdy0mpr77wv6j35") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.4.2 (c (n "flavio") (v "0.4.2") (h "0jr9gszagxy6m1bz9ibh1n0cdnvw886lmhhlyrq2inz6ia27brdk") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.4.3 (c (n "flavio") (v "0.4.3") (h "1qkmvhy8b24449bqwanh20g35vvawjj0lqx03h2v4pq1f0wiamf2") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.4.4 (c (n "flavio") (v "0.4.4") (h "09hnmrnwj8mk7k6gx3nw86aqvqwd9sjlb24d93f0q8v7hk0k01d9") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

(define-public crate-flavio-0.4.5 (c (n "flavio") (v "0.4.5") (h "1djc07j75xnw9p59p36yaiiv4jwdnwn82lx357fdp7xljmn8p0wr") (f (quote (("mechanics" "math") ("math") ("fem" "constitutive") ("constitutive" "mechanics"))))))

