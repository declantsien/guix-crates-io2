(define-module (crates-io fl av flav-md-engine) #:use-module (crates-io))

(define-public crate-flav-md-engine-0.1.0 (c (n "flav-md-engine") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (f (quote ("alloc"))) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0sx9hp1zl834k83wkkw88nr1yd90h7x0g9ls3w5vncq1wc3xy1a9")))

