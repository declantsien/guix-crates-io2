(define-module (crates-io fl ui fluidlite-lib) #:use-module (crates-io))

(define-public crate-fluidlite-lib-0.1.0 (c (n "fluidlite-lib") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (d #t) (k 1)))) (h "08bx6f0rzi0lj1qpg527083hdjdrkqf4p0y314602q2fprjxj477") (f (quote (("with-stb") ("with-sf3") ("shared") ("rustdoc"))))))

(define-public crate-fluidlite-lib-0.1.2 (c (n "fluidlite-lib") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (d #t) (k 1)))) (h "0jvw7p077k8kcc2pkn5avan5yhp50p10kw4cry71syaz3wy55pyg") (f (quote (("with-stb") ("with-sf3") ("shared") ("rustdoc"))))))

(define-public crate-fluidlite-lib-0.1.3 (c (n "fluidlite-lib") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (d #t) (k 1)))) (h "0sgap9az3pkh6jsk5vihi09d6y80qi3iji000d3ifnapvdan0cwg") (f (quote (("with-stb") ("with-sf3") ("shared") ("rustdoc"))))))

