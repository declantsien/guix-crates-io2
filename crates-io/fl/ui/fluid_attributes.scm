(define-module (crates-io fl ui fluid_attributes) #:use-module (crates-io))

(define-public crate-fluid_attributes-0.3.0 (c (n "fluid_attributes") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01v06af0qrakpiln58csy51i2wvfwwb0db7sxwcmhg04laxb34kk") (f (quote (("debug"))))))

(define-public crate-fluid_attributes-0.4.0 (c (n "fluid_attributes") (v "0.4.0") (d (list (d (n "pm") (r "^0.4") (d #t) (k 0) (p "proc-macro2")) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1i67vcas0gr64bc8spprlfp7m7msv1jyspghgq1q8f0qrnvy8px8") (f (quote (("debug"))))))

