(define-module (crates-io fl ui fluid) #:use-module (crates-io))

(define-public crate-fluid-0.0.0 (c (n "fluid") (v "0.0.0") (h "19s4ii7x2hxawywm8b26r7pzp4m08abahmnsb29kgikp3bnlm4mw")))

(define-public crate-fluid-0.1.0 (c (n "fluid") (v "0.1.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1c7mgxvlkffzl33srj8fah4k79k4kvz13wc1pg56cgd3n0ny16w9")))

(define-public crate-fluid-0.2.0 (c (n "fluid") (v "0.2.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1qdkdwlxa6ps53yf7pb3bblsgs49q3b1lpzwxndzyg1vrrfqs4z4")))

(define-public crate-fluid-0.2.1 (c (n "fluid") (v "0.2.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1a353kn3akb0m51fmsafyhn6qa7dy4y0xw86h5ccbpg1ljrkdyi5")))

(define-public crate-fluid-0.2.2 (c (n "fluid") (v "0.2.2") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1if85xcc221s9dbfs8zvgd9phxfhm7vkjzgwwas7g35sbc8z2iac")))

(define-public crate-fluid-0.2.3 (c (n "fluid") (v "0.2.3") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0dahdai0vs1w6an4rras32ws7lpw14mdncb2bb978jxb1c5lsgdk")))

(define-public crate-fluid-0.2.4 (c (n "fluid") (v "0.2.4") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1m5q3zf73j2fm0wzi4hklvgb9yvibcxzb4g9g193ym6i5pvpbyx6")))

(define-public crate-fluid-0.3.0 (c (n "fluid") (v "0.3.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "fluid_attributes") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1dyvddb8s1j1fknd67qzdsi1crdbmc85s142is63j01b3hgknxz1") (f (quote (("debug" "fluid_attributes/debug"))))))

(define-public crate-fluid-0.4.0 (c (n "fluid") (v "0.4.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "fluid_attributes") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0457xbwznxxg1plcf6s7h618haf7143kk96zls66j1xbarrbjyfm") (f (quote (("float" "num-traits") ("debug" "fluid_attributes/debug"))))))

(define-public crate-fluid-0.4.1 (c (n "fluid") (v "0.4.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "fluid_attributes") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "04qgdc4lx934158icx9rvs0v6lyvirmb0brllvz38hj9fsaqfbsp") (f (quote (("float" "num-traits") ("debug" "fluid_attributes/debug"))))))

