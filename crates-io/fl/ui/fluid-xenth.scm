(define-module (crates-io fl ui fluid-xenth) #:use-module (crates-io))

(define-public crate-fluid-xenth-0.1.0 (c (n "fluid-xenth") (v "0.1.0") (d (list (d (n "fluidlite") (r "^0.2.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "tune") (r "^0.24.0") (d #t) (k 0)))) (h "0v6jh7hs4nc63b0jywazxgy0z1s86cs4i4g841rswlkgb8spvmhd")))

(define-public crate-fluid-xenth-0.2.0 (c (n "fluid-xenth") (v "0.2.0") (d (list (d (n "fluidlite") (r "^0.2.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "tune") (r "^0.27.0") (d #t) (k 0)))) (h "0mqp7zc60v5588rx6jafh86c8i9m9q1q9y744xf1qd961b7l1xcd")))

(define-public crate-fluid-xenth-0.3.0 (c (n "fluid-xenth") (v "0.3.0") (d (list (d (n "fluidlite") (r "^0.2.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "tune") (r "^0.30.0") (d #t) (k 0)))) (h "1fmms4q448smgnqgdraxvkddm5vcr9brfblbhr122qd6r2rmcmcf") (f (quote (("stb" "fluidlite/with-stb") ("sf3" "fluidlite/with-sf3")))) (r "1.56")))

(define-public crate-fluid-xenth-0.4.0 (c (n "fluid-xenth") (v "0.4.0") (d (list (d (n "fluidlite") (r "^0.2.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "tune") (r "^0.31.0") (d #t) (k 0)))) (h "0nrs5swry6m34ay4ii2jhj35l50z1gmij1ymn7jpmskmdcwl6gj8") (f (quote (("stb" "fluidlite/with-stb") ("sf3" "fluidlite/with-sf3")))) (r "1.61")))

(define-public crate-fluid-xenth-0.5.0 (c (n "fluid-xenth") (v "0.5.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "oxisynth") (r "^0.0.1") (d #t) (k 0)) (d (n "tune") (r "^0.31.0") (d #t) (k 0)))) (h "1sidrh1a7xbj04sj00vjw2w8wfqv5izn4njc5p8fh8ggv27kxdnd") (f (quote (("sf3" "oxisynth/sf3")))) (r "1.61")))

(define-public crate-fluid-xenth-0.6.0 (c (n "fluid-xenth") (v "0.6.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "oxisynth") (r "^0.0.2") (d #t) (k 0)) (d (n "tune") (r "^0.32.0") (d #t) (k 0)))) (h "12i9lywq6lza5ff468na7ah6kk764j0y3gmfr924y78fvbkiwsh0") (f (quote (("sf3" "oxisynth/sf3")))) (r "1.61")))

(define-public crate-fluid-xenth-0.7.0 (c (n "fluid-xenth") (v "0.7.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "oxisynth") (r "^0.0.3") (d #t) (k 0)) (d (n "tune") (r "^0.33.0") (d #t) (k 0)))) (h "06r9v9qlajnc9f9nw1qaz81bdrh3c42jhj5ff51ri0b18ivdqxhv") (f (quote (("sf3" "oxisynth/sf3")))) (r "1.61")))

(define-public crate-fluid-xenth-0.8.0 (c (n "fluid-xenth") (v "0.8.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "oxisynth") (r "^0.0.3") (d #t) (k 0)) (d (n "tune") (r "^0.34.0") (d #t) (k 0)))) (h "0zyr4g0901gz9mz07qi9l37s69834cmc2wk5vjhir11lhyy4xd1g") (f (quote (("sf3" "oxisynth/sf3")))) (r "1.66")))

