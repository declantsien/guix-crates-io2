(define-module (crates-io fl ui fluid-macro) #:use-module (crates-io))

(define-public crate-fluid-macro-0.1.0 (c (n "fluid-macro") (v "0.1.0") (h "11akfyy306miisj4hzxjalilgs23aqil3shr622cjss5hmz71cna")))

(define-public crate-fluid-macro-0.2.0 (c (n "fluid-macro") (v "0.2.0") (h "17h5d81qzyx67yh8k5kzn863598khv7wzknllg13x9nbvil24x6s")))

(define-public crate-fluid-macro-0.3.0 (c (n "fluid-macro") (v "0.3.0") (h "0p00yjwnz7ncblm3z24s9vsd9732j0xi3ka3idwgf6hs2v3amklr")))

(define-public crate-fluid-macro-0.3.1 (c (n "fluid-macro") (v "0.3.1") (h "150zpv3gbj1pgc2s6812w5y7kl1xmwimr0qvil40nrz2xwknqw1j")))

