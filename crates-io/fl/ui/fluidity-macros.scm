(define-module (crates-io fl ui fluidity-macros) #:use-module (crates-io))

(define-public crate-fluidity-macros-0.1.1 (c (n "fluidity-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0dfvxhqi3ipvjxzss36q32nibjsr7ylwdn0w6yqwxznlwymgcn4i")))

(define-public crate-fluidity-macros-0.1.2 (c (n "fluidity-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0b3gn59w0p63a1s31ii77xa5a1kd0kq70bbd0bp05mlwcddki0qv")))

(define-public crate-fluidity-macros-0.1.3 (c (n "fluidity-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0wmb2zb47z57sg745qpikzm2gis8hai7syl9467hki8f4hym8f31")))

(define-public crate-fluidity-macros-0.1.4 (c (n "fluidity-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0k20mljgxhq8sy67f65sfp9rwp6lmv3bg6c4flb6gm5yvywjw2kk")))

