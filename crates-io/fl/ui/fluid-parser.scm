(define-module (crates-io fl ui fluid-parser) #:use-module (crates-io))

(define-public crate-fluid-parser-0.1.0 (c (n "fluid-parser") (v "0.1.0") (h "05g8dszcc5jx05nqbwn4zwrdygydrji4p6gh73sw6ayfhbc2rnx4")))

(define-public crate-fluid-parser-0.1.1 (c (n "fluid-parser") (v "0.1.1") (h "1h5365vdi7jfbg6ls5sf61ldswf2jz78p9jdlgwc0iwm7qjqiy45")))

(define-public crate-fluid-parser-0.1.2 (c (n "fluid-parser") (v "0.1.2") (h "0b12gs8p4a78qdzm089lphh9bxq65g5w8k7f6f3ypzf8xg1kyvp1")))

(define-public crate-fluid-parser-0.1.3 (c (n "fluid-parser") (v "0.1.3") (h "0vi79jmpyqkgd70xlpswwfdjamkx6cqry9asfwpzhjywaj6rr1qd")))

(define-public crate-fluid-parser-0.1.4 (c (n "fluid-parser") (v "0.1.4") (h "0rps7l0jhxan8wngv2vzr3bblm6amm426aqnygbav08sms70sd3d")))

(define-public crate-fluid-parser-0.1.5 (c (n "fluid-parser") (v "0.1.5") (h "1240drxr8q04x3nvbzv3g6yclb9ipcm70j3jnyb2zb40rnz3d22a")))

(define-public crate-fluid-parser-0.1.6 (c (n "fluid-parser") (v "0.1.6") (h "0nblnilinldjn36yjg065jzdxhz3njg79y52dglp3lvx5s66zz0f")))

(define-public crate-fluid-parser-0.1.7 (c (n "fluid-parser") (v "0.1.7") (h "0486ynrjwldp6n156zm8d8zkhxbzl4x5lff5rhh6fnh5b3q4hznm")))

(define-public crate-fluid-parser-0.1.8 (c (n "fluid-parser") (v "0.1.8") (h "10rnz26738p963rkajw1a678ydccc510rs3zyh862i6xp42clxzl")))

(define-public crate-fluid-parser-0.1.9 (c (n "fluid-parser") (v "0.1.9") (h "1pld4m40zjhicwhqgf0s9iwj3rlmazcr8a7ah9drmiwf5i8lyzxf")))

(define-public crate-fluid-parser-0.1.10 (c (n "fluid-parser") (v "0.1.10") (h "0snym90vvn296hqh7arnc72zc4da5xc3fn56wnlsfj3hjw79c9zb")))

(define-public crate-fluid-parser-0.1.11 (c (n "fluid-parser") (v "0.1.11") (h "1zsx85ihmp24l1k4mkc1k4d462vj0hjc7a2g3vcg649n965s2a0f")))

(define-public crate-fluid-parser-0.1.12 (c (n "fluid-parser") (v "0.1.12") (h "0cx3ip0dlcyr28cq378i1nz4bl1jf8z85ryn3vfjswjxskinf7r0")))

(define-public crate-fluid-parser-0.1.13 (c (n "fluid-parser") (v "0.1.13") (h "1agqr2b0bdqja5xjw20k550lmxhka75jv97h2fabfm9hwhqbxl0s")))

(define-public crate-fluid-parser-0.1.14 (c (n "fluid-parser") (v "0.1.14") (h "0gdinlfx7rd1nf0nkarasd2dy8ij2b1a14salwfsf3xik0bhmrrk")))

(define-public crate-fluid-parser-0.1.15 (c (n "fluid-parser") (v "0.1.15") (h "0a7pzpl2bz4wvbq094f1dhvdm31zpnq6mm1vih63ili0zid27hgq")))

