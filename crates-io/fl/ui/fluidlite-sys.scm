(define-module (crates-io fl ui fluidlite-sys) #:use-module (crates-io))

(define-public crate-fluidlite-sys-0.1.0 (c (n "fluidlite-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (o #t) (d #t) (k 1)))) (h "13aj8pk223n30qcf8sysbssqfhmi0nx46vk99qkvmj6z95kmgx5q") (f (quote (("rustdoc") ("generate-bindings" "fetch_unroll" "bindgen"))))))

(define-public crate-fluidlite-sys-0.1.2 (c (n "fluidlite-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (o #t) (d #t) (k 1)))) (h "0wsmmf23458ghwcl735d8gna5zr8fh8r18hfzyv1ba2b78j2xdfq") (f (quote (("rustdoc") ("generate-bindings" "fetch_unroll" "bindgen"))))))

(define-public crate-fluidlite-sys-0.1.3 (c (n "fluidlite-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (o #t) (d #t) (k 1)))) (h "0mf5bl0k4yrip12mah4f17fypn5zf74bzmcxbjvmwz7p5zsa1hpy") (f (quote (("rustdoc") ("generate-bindings" "fetch_unroll" "bindgen"))))))

(define-public crate-fluidlite-sys-0.2.0 (c (n "fluidlite-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0wf9v5c6xahvgpj41v624f691pkv500amn76wxvm6r8cszmxnj3x") (f (quote (("with-stb") ("with-sf3") ("update-bindings" "bindgen") ("static") ("shared") ("rustdoc") ("builtin"))))))

(define-public crate-fluidlite-sys-0.2.1 (c (n "fluidlite-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "029ql2rfrfw6mzv075m94gdana7iqb2q769zf7ch6g8zgpm791bg") (f (quote (("with-stb") ("with-sf3") ("update-bindings" "bindgen") ("static") ("shared") ("rustdoc") ("builtin")))) (l "fluidlite")))

