(define-module (crates-io fl ui fluidity-derive) #:use-module (crates-io))

(define-public crate-fluidity-derive-0.1.1 (c (n "fluidity-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "09nnfadmlwz91gsl1rlnl2sk9xpr0jg11nwp0mlan7isiak0h72k")))

(define-public crate-fluidity-derive-0.1.2 (c (n "fluidity-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "04aixir4qw7qdryqy5xf9x6cdyvvgfhzq1v80rh6pjishkaplhhf")))

(define-public crate-fluidity-derive-0.1.3 (c (n "fluidity-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "12wyr68y3nzm0v031pprh1xr9wqb58p345hml1k898mz1bqj3g5d")))

(define-public crate-fluidity-derive-0.1.4 (c (n "fluidity-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "115qhfs5k1v75jzgw6jlsd2nkmcxc2dkyj4bkdnywmw1160lwygs")))

