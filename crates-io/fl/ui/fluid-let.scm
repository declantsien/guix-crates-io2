(define-module (crates-io fl ui fluid-let) #:use-module (crates-io))

(define-public crate-fluid-let-0.1.0 (c (n "fluid-let") (v "0.1.0") (h "1mgc2rgawd3jzqcq6x681af9qk2wpxgljl9ysmhcmqdj2ziv08i6")))

(define-public crate-fluid-let-1.0.0 (c (n "fluid-let") (v "1.0.0") (h "0nj848aiglv2ggwg43pa05266lx74791v91i1f58gby1gn3zz73l") (f (quote (("static-init"))))))

