(define-module (crates-io fl ut flutter-download) #:use-module (crates-io))

(define-public crate-flutter-download-0.1.0 (c (n "flutter-download") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.19") (f (quote ("http2"))) (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "unzip") (r "^0.1.0") (d #t) (k 0)))) (h "11ifm4b9lsx66597lkqjy55fag7bz1a2jbxr2n88lzdx79d9m8wq")))

(define-public crate-flutter-download-0.3.0 (c (n "flutter-download") (v "0.3.0") (d (list (d (n "curl") (r "^0.4.19") (f (quote ("http2"))) (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "unzip") (r "^0.1.0") (d #t) (k 0)))) (h "1q31jgc6hapsv6f66zxsrbaj3h719ffg6p1n72p63dk3gd1p3rmk")))

(define-public crate-flutter-download-0.3.5 (c (n "flutter-download") (v "0.3.5") (d (list (d (n "curl") (r "^0.4.19") (f (quote ("http2"))) (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "unzip") (r "^0.1.0") (d #t) (k 0)))) (h "1vixfa05q5mdgb3izvx71nn0z9fd66cgmh8n4a21m8ijlfr575cx")))

