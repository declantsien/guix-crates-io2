(define-module (crates-io fl ut flutter_logger) #:use-module (crates-io))

(define-public crate-flutter_logger-0.1.0 (c (n "flutter_logger") (v "0.1.0") (d (list (d (n "flutter_rust_bridge") (r "^1.78.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1dgz05fkiwwdb58albpyvr9lqmfs5ms35x2c083ksmandmw07bkj") (f (quote (("panic"))))))

(define-public crate-flutter_logger-0.2.0 (c (n "flutter_logger") (v "0.2.0") (d (list (d (n "flutter_rust_bridge") (r "^1.78.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "03zcl9ckq9n0vqyyiykn2grifcbg0d79485yxsgpyksi2ydxk7d9") (f (quote (("panic"))))))

(define-public crate-flutter_logger-0.3.0 (c (n "flutter_logger") (v "0.3.0") (d (list (d (n "flutter_rust_bridge") (r "^1.79.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0qzi20pf39i7mhb96gqfa5i66y6c0mvvgbnimmbjbj4s9vffxd5m") (f (quote (("panic"))))))

(define-public crate-flutter_logger-0.4.0 (c (n "flutter_logger") (v "0.4.0") (d (list (d (n "flutter_rust_bridge") (r "^1.81.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1h6f3iw4ff58mn4wjxv996dv2w9pc02qgcl2as3rc6j8fccnc1wh") (f (quote (("panic"))))))

(define-public crate-flutter_logger-0.5.0 (c (n "flutter_logger") (v "0.5.0") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1y71hzqp6dfifhc0hzlji1c6anzmdjwcrqvyr7b50i3b4mrhng6y") (f (quote (("panic"))))))

(define-public crate-flutter_logger-0.6.0 (c (n "flutter_logger") (v "0.6.0") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0gd59lgm9fnncc8m9ag3qbv8v5kddw8d90xsryzmbxn9pmn3l8vs") (f (quote (("panic"))))))

(define-public crate-flutter_logger-0.6.1 (c (n "flutter_logger") (v "0.6.1") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1l6vgwvc9hxd1c01kgyw051js22hmxscbnvx7mg0cxpdsxzl0gba") (f (quote (("panic"))))))

