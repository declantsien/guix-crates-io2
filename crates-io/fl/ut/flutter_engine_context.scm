(define-module (crates-io fl ut flutter_engine_context) #:use-module (crates-io))

(define-public crate-flutter_engine_context-0.1.0 (c (n "flutter_engine_context") (v "0.1.0") (d (list (d (n "cocoa") (r "^0.24") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1bh4l0k1kn985i02gcjz9wsz21w9i07hzqal8ad0p49hmkk3jb5z")))

