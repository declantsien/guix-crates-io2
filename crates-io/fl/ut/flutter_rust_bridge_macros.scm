(define-module (crates-io fl ut flutter_rust_bridge_macros) #:use-module (crates-io))

(define-public crate-flutter_rust_bridge_macros-1.10.0 (c (n "flutter_rust_bridge_macros") (v "1.10.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0lwrvfqkr5924pissx6a7l152qxn9hjzf4x1msz9j31gsrmgqv2q")))

(define-public crate-flutter_rust_bridge_macros-1.11.0 (c (n "flutter_rust_bridge_macros") (v "1.11.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1j0zncvcyqzyawyfw0qfj1rgd0l6kb5xq02d7ay7msgby9gvi04x")))

(define-public crate-flutter_rust_bridge_macros-1.12.0 (c (n "flutter_rust_bridge_macros") (v "1.12.0") (h "19kh5aiqza940ya1c9bzacj679riigdsn918jm2h6d6808hs066j")))

(define-public crate-flutter_rust_bridge_macros-1.13.0 (c (n "flutter_rust_bridge_macros") (v "1.13.0") (h "0ladbnainl9zr4yvxdwbvi00prpbk5k45x3mqmpd50axld1wx6di")))

(define-public crate-flutter_rust_bridge_macros-1.14.0 (c (n "flutter_rust_bridge_macros") (v "1.14.0") (h "145d048qah35naibblivisc4j6vj232pqp58appjnf5dbc591g5i")))

(define-public crate-flutter_rust_bridge_macros-1.15.0 (c (n "flutter_rust_bridge_macros") (v "1.15.0") (h "1qbgn63yvx8l97pr055a5r97sck7jbbn2g2lh9cjrhf9ara5b8lr")))

(define-public crate-flutter_rust_bridge_macros-1.15.1 (c (n "flutter_rust_bridge_macros") (v "1.15.1") (h "0ckwx4g0j0jm1zxpp6gnd11q5jql1sgigj54kqkql2f5v1bj41nx")))

(define-public crate-flutter_rust_bridge_macros-1.16.0 (c (n "flutter_rust_bridge_macros") (v "1.16.0") (h "1hb81a8gyg8vdnmf5ycb0ypm9qv4qpv959ajpvd5gk1q1dx9nznr")))

(define-public crate-flutter_rust_bridge_macros-1.18.0 (c (n "flutter_rust_bridge_macros") (v "1.18.0") (h "03gnq9hiqaa9rd09j4k20lnjv1622jb4awzn1bdp43dr8w7nrpqm")))

(define-public crate-flutter_rust_bridge_macros-1.19.0 (c (n "flutter_rust_bridge_macros") (v "1.19.0") (h "1vfx0vcf5vs0qs81dbgyn1b809yr62zx1vdknkf1csqq21k3p3bc")))

(define-public crate-flutter_rust_bridge_macros-1.19.1 (c (n "flutter_rust_bridge_macros") (v "1.19.1") (h "0pi60llyhv1s60dciayh4grd32x1i4sal6ah8wxplkdmfzm0ig26")))

(define-public crate-flutter_rust_bridge_macros-1.19.2 (c (n "flutter_rust_bridge_macros") (v "1.19.2") (h "09yxbyb02879f7qi488z1f44mm40cvpcgbiy94m8bifv7r8ap0im")))

(define-public crate-flutter_rust_bridge_macros-1.20.0 (c (n "flutter_rust_bridge_macros") (v "1.20.0") (h "0hr6n0q820ya388wzp3sihp360zgi2aiazxvjp3rbhls1ix5sdsn")))

(define-public crate-flutter_rust_bridge_macros-1.20.1 (c (n "flutter_rust_bridge_macros") (v "1.20.1") (h "1iikdmf982vrk8wy88klfjnlclhalhwccgzaxlm1wcq5pdip0af7")))

(define-public crate-flutter_rust_bridge_macros-1.21.0 (c (n "flutter_rust_bridge_macros") (v "1.21.0") (h "1ggvb6nxwd6s736kd029p7y2riji4s93ydyzhm6c4qls26frcinw")))

(define-public crate-flutter_rust_bridge_macros-1.21.1 (c (n "flutter_rust_bridge_macros") (v "1.21.1") (h "1nq5890pcj6bzlqfry684qsbhzaiqhwg152d61x15673bn9n65b3")))

(define-public crate-flutter_rust_bridge_macros-1.22.0 (c (n "flutter_rust_bridge_macros") (v "1.22.0") (h "0hy9adh2s4pmhyqgivkqian4qw59xiv1fjhm0w2li0j0vlz2z6hz")))

(define-public crate-flutter_rust_bridge_macros-1.22.1 (c (n "flutter_rust_bridge_macros") (v "1.22.1") (h "0di9lmnjb8wi0xgqbva5c2nbfpn925i5h5lj69xch398rvy5mdq6")))

(define-public crate-flutter_rust_bridge_macros-1.22.2 (c (n "flutter_rust_bridge_macros") (v "1.22.2") (h "1nmnamxndkq5hfls4063by35xg4sssccljdvk1yydnb02ilv9yrp")))

(define-public crate-flutter_rust_bridge_macros-1.23.0 (c (n "flutter_rust_bridge_macros") (v "1.23.0") (h "0a2s8h7hgdjy4ww774n3nsnmnwllq3vs2x6wgi7m92w86yvhbcv7")))

(define-public crate-flutter_rust_bridge_macros-1.24.0 (c (n "flutter_rust_bridge_macros") (v "1.24.0") (h "0dn0ylhv4rkk8qpr05hx01dsv6z3vxbzpi0fibgj57fav2w1j240")))

(define-public crate-flutter_rust_bridge_macros-1.25.0 (c (n "flutter_rust_bridge_macros") (v "1.25.0") (h "0b6hasnrjq910021v38lbc6f4fjws9rajld33bvfnl1ljrlwr7gs")))

(define-public crate-flutter_rust_bridge_macros-1.26.0 (c (n "flutter_rust_bridge_macros") (v "1.26.0") (h "1ldg0phk40749q6m4p8gdw4cladqly92ixn748yfnnr7k5apdxyj")))

(define-public crate-flutter_rust_bridge_macros-1.27.0 (c (n "flutter_rust_bridge_macros") (v "1.27.0") (h "08cn7yq8lkqf5dk0g6bh7ml8g64jz2kzdplhb1cjxi3zrhrpfwsq")))

(define-public crate-flutter_rust_bridge_macros-1.27.1 (c (n "flutter_rust_bridge_macros") (v "1.27.1") (h "0bj7bi677dyxnsycfadc7v5wyxg264dqq1lf2n2cfi5q9s8kp2cz")))

(define-public crate-flutter_rust_bridge_macros-1.27.2 (c (n "flutter_rust_bridge_macros") (v "1.27.2") (h "0pv99w10kf8clr5d4r4y4phh44d2m4sjbpbkv1kac4yxs72kx9pp")))

(define-public crate-flutter_rust_bridge_macros-1.28.0 (c (n "flutter_rust_bridge_macros") (v "1.28.0") (h "1nrryw3fyfy7n133v5sh4l53w35r0q8bdjbmra51jwja3bfwkmbi")))

(define-public crate-flutter_rust_bridge_macros-1.28.1 (c (n "flutter_rust_bridge_macros") (v "1.28.1") (h "00mwwdf7p2bmg579qwy0fi29zzvfrwyxbwx0kpf1ii5x0cjdj1gd")))

(define-public crate-flutter_rust_bridge_macros-1.29.0 (c (n "flutter_rust_bridge_macros") (v "1.29.0") (h "1ydjwmpyh54329qv3smgqc2c2mccw5sgxc8bjx2g79sgkpi01slw")))

(define-public crate-flutter_rust_bridge_macros-1.30.0 (c (n "flutter_rust_bridge_macros") (v "1.30.0") (h "1g2zv0srh5c5z8csmlrfzmknmbq5vnbsj5gl8m755bgd2fbvvzv9")))

(define-public crate-flutter_rust_bridge_macros-1.31.0 (c (n "flutter_rust_bridge_macros") (v "1.31.0") (h "0lmclyyxkbpysz7rw2wzj4q18kj7kh6ncykdm228hqpr15s0csbf")))

(define-public crate-flutter_rust_bridge_macros-1.32.0 (c (n "flutter_rust_bridge_macros") (v "1.32.0") (h "1lf15m0fr6s4hrjpaa16bp8c0f6ijnk96a4rbq6j9qf9lhhxs008")))

(define-public crate-flutter_rust_bridge_macros-1.33.0 (c (n "flutter_rust_bridge_macros") (v "1.33.0") (h "100m5dbyclbk38qzrdhk54lm5qky9ryld9iqmk8q07w5lypabyp2")))

(define-public crate-flutter_rust_bridge_macros-1.34.0 (c (n "flutter_rust_bridge_macros") (v "1.34.0") (h "0iskcnagl9250bh08im3yyjikw0cmq607f9nabrvqrqxxgf1vlhz")))

(define-public crate-flutter_rust_bridge_macros-1.34.1 (c (n "flutter_rust_bridge_macros") (v "1.34.1") (h "1yfg069rmwri8a2ccfxjisdilsd4almk086h6g690wfak5xig4lp")))

(define-public crate-flutter_rust_bridge_macros-1.34.2 (c (n "flutter_rust_bridge_macros") (v "1.34.2") (h "0grn0p8nsl8i2drqyzr4r2n34psaz27v3y0j0apxa3iqjyml0i8x")))

(define-public crate-flutter_rust_bridge_macros-1.35.0 (c (n "flutter_rust_bridge_macros") (v "1.35.0") (h "0sr4j0fly49y1wwryypvnjv4b8yb8idln6kjnawz90l12bmkacys")))

(define-public crate-flutter_rust_bridge_macros-1.36.0 (c (n "flutter_rust_bridge_macros") (v "1.36.0") (h "17lmm8348gi9p18m9f4awcj6ldm7k3p92fq9yzjqmbbvc6h8pcaj")))

(define-public crate-flutter_rust_bridge_macros-1.37.1 (c (n "flutter_rust_bridge_macros") (v "1.37.1") (h "1wcx294cdjhrxyb04jvzn43bmsdabqlr416msvcbqi537bipkrjd")))

(define-public crate-flutter_rust_bridge_macros-1.37.2 (c (n "flutter_rust_bridge_macros") (v "1.37.2") (h "1vcz5sm53dhznzdym817i1676dmj73l9n7l0s18qkwpwkaka2y2b")))

(define-public crate-flutter_rust_bridge_macros-1.38.0 (c (n "flutter_rust_bridge_macros") (v "1.38.0") (h "0vbdax9c97rh96lzciaip87zwl13pk4ssp5v7gmg8fxwf6djnr8k")))

(define-public crate-flutter_rust_bridge_macros-1.38.1 (c (n "flutter_rust_bridge_macros") (v "1.38.1") (h "045mmj08hzs2gkilih3hh1y4xbv5qdk2n0bhqr37lzhsgmz5h0x4")))

(define-public crate-flutter_rust_bridge_macros-1.38.2 (c (n "flutter_rust_bridge_macros") (v "1.38.2") (h "1c6jjrapaix74c2c6fcpbvfi5fzja6wcp39b1qvx68mqsi8ra6sn")))

(define-public crate-flutter_rust_bridge_macros-1.39.0 (c (n "flutter_rust_bridge_macros") (v "1.39.0") (h "16cdc1mn7q4fqshjfkg0vapy9vcq63sy9435xpa96bj1g629hx2l")))

(define-public crate-flutter_rust_bridge_macros-1.40.0 (c (n "flutter_rust_bridge_macros") (v "1.40.0") (h "1cyqrd5xs4hq4licbf4iv0mcv1ssddad3flpsjwgqbv66527w1hm")))

(define-public crate-flutter_rust_bridge_macros-1.41.0 (c (n "flutter_rust_bridge_macros") (v "1.41.0") (h "1m7fnfd5swcdj9dzq7j5zz131cvv25j7whfnsg180gjw1s7z2aq0")))

(define-public crate-flutter_rust_bridge_macros-1.41.1 (c (n "flutter_rust_bridge_macros") (v "1.41.1") (h "07k1wlxr82sjg0aqdd5rnm87m4vv91lqvpabi6swi6dnh907qh1x")))

(define-public crate-flutter_rust_bridge_macros-1.41.2 (c (n "flutter_rust_bridge_macros") (v "1.41.2") (h "0vi8r49hvcay338lmvlfs3lk36f6rixympcfphcp9345wqxh0jc9")))

(define-public crate-flutter_rust_bridge_macros-1.41.3 (c (n "flutter_rust_bridge_macros") (v "1.41.3") (h "1996y808plmf42a31v3h0wz4bnlairs3b52jnn6x55sfmmgxanms")))

(define-public crate-flutter_rust_bridge_macros-1.42.0 (c (n "flutter_rust_bridge_macros") (v "1.42.0") (h "1jwvmmg0rghvr8sdarjrv8w9ay8xgc74kz1adiym1x0qk3glz9bc")))

(define-public crate-flutter_rust_bridge_macros-1.43.0 (c (n "flutter_rust_bridge_macros") (v "1.43.0") (h "0aln0fjhzijbkqdkiksd0nhmgyf80l7bzl8zanvs15aalpxbxc48")))

(define-public crate-flutter_rust_bridge_macros-1.44.0 (c (n "flutter_rust_bridge_macros") (v "1.44.0") (h "0qv2a2hdkz4vd97j1rkpvvck8hiabhdgppgc6b2ilsymn7hkixh5")))

(define-public crate-flutter_rust_bridge_macros-1.45.0 (c (n "flutter_rust_bridge_macros") (v "1.45.0") (h "0dh1826frzabr9afkh06kl1j9hj6ypl72czxb76snyhy5vg1714z")))

(define-public crate-flutter_rust_bridge_macros-1.46.0 (c (n "flutter_rust_bridge_macros") (v "1.46.0") (h "0p6laq47jqiw499ixrq1zlndd7clgwvq3g3kiw0q9bf7h89gf3ss")))

(define-public crate-flutter_rust_bridge_macros-1.47.0 (c (n "flutter_rust_bridge_macros") (v "1.47.0") (h "1g0n2cvbj1wbaqzz46laaylnbk49z1wwhkb77hk9nr5ma7ds54v0")))

(define-public crate-flutter_rust_bridge_macros-1.47.1 (c (n "flutter_rust_bridge_macros") (v "1.47.1") (h "1b46kza0iqb8p381jkrmyaag7ha4fcqm5yw3nnc9kljr3qwh3j4s")))

(define-public crate-flutter_rust_bridge_macros-1.48.0 (c (n "flutter_rust_bridge_macros") (v "1.48.0") (h "0jmny1vb3lgflrfcsa0j025aw3g301c24yvhr0szkndfck9jynzz")))

(define-public crate-flutter_rust_bridge_macros-1.48.1 (c (n "flutter_rust_bridge_macros") (v "1.48.1") (h "18ww9gcicz6v9r6npcgc5vfbv20nbgfy039xmq32znypp493bd7r")))

(define-public crate-flutter_rust_bridge_macros-1.49.0 (c (n "flutter_rust_bridge_macros") (v "1.49.0") (h "182fj2dc4a202wr4r25ymrm5awsjlghh8dai6rjgrg16y9phi96h")))

(define-public crate-flutter_rust_bridge_macros-1.49.1 (c (n "flutter_rust_bridge_macros") (v "1.49.1") (h "1s8qfplm99m0qyzmqvxhma9gbjxdyfrrkzagcl62c31zwj0swqb1")))

(define-public crate-flutter_rust_bridge_macros-1.49.2 (c (n "flutter_rust_bridge_macros") (v "1.49.2") (h "0jyysvhhwmjlfwfp7x140x548glvyh9yh62yg5pdli2vnhplc4nf")))

(define-public crate-flutter_rust_bridge_macros-1.50.0 (c (n "flutter_rust_bridge_macros") (v "1.50.0") (h "0ri9lyi21vq8d0ixs0bk5jg5h85d9l48lrhahlvwrn83hm3jx7lv")))

(define-public crate-flutter_rust_bridge_macros-1.51.0 (c (n "flutter_rust_bridge_macros") (v "1.51.0") (h "0sxfqz2zkmha036il26l9nrfzby0ylrb0gdmba7d93psg47g2p4x")))

(define-public crate-flutter_rust_bridge_macros-1.51.1 (c (n "flutter_rust_bridge_macros") (v "1.51.1") (h "008gx29wzisyxka5zxsvcdfzqj6sj322xgmqs9jh6i85lkxhkymi")))

(define-public crate-flutter_rust_bridge_macros-1.53.0 (c (n "flutter_rust_bridge_macros") (v "1.53.0") (h "1hil96hjaalkra5nlqx2n91bz0aj366lacffbc5vpmjb84pp0kc2")))

(define-public crate-flutter_rust_bridge_macros-1.54.0 (c (n "flutter_rust_bridge_macros") (v "1.54.0") (h "08q6zb5lq040lsy1kbavw588fcyf9j5dp13rvwsqhr86vbw3gqdf")))

(define-public crate-flutter_rust_bridge_macros-1.54.1 (c (n "flutter_rust_bridge_macros") (v "1.54.1") (h "0awd03y1ck3b70gnxx13wcdw9f1c3kzkh4p711jhfymfv7ldl3c9")))

(define-public crate-flutter_rust_bridge_macros-1.55.0 (c (n "flutter_rust_bridge_macros") (v "1.55.0") (h "0xvkbgd6yzg8zgjffrr9apzirgdlgwvahfzzj1ag8j3c6xxj4blg")))

(define-public crate-flutter_rust_bridge_macros-1.55.1 (c (n "flutter_rust_bridge_macros") (v "1.55.1") (h "08b3gch6n7ra9iyzx2b2m5ayi5z6zd86b391k58yd597610d2ci5")))

(define-public crate-flutter_rust_bridge_macros-1.56.0 (c (n "flutter_rust_bridge_macros") (v "1.56.0") (h "14f8vjqd9spccvgqd0vqv6l1qdffqpsa0g1k6m64w6qr57sa81sz")))

(define-public crate-flutter_rust_bridge_macros-1.57.0 (c (n "flutter_rust_bridge_macros") (v "1.57.0") (h "13738ic7ssfdgc10nj5hbp148j7rq6fdmw1cw4hkpvf6qr6j4jqj")))

(define-public crate-flutter_rust_bridge_macros-1.58.0 (c (n "flutter_rust_bridge_macros") (v "1.58.0") (h "0664j6v9chg2vg1gdp5wf9c6wngk40p5sy0jias911iyq7bf010d")))

(define-public crate-flutter_rust_bridge_macros-1.58.1 (c (n "flutter_rust_bridge_macros") (v "1.58.1") (h "1z7dyk7a862a4hsff89zaf2299ady9dx21cgxv0v74l9cgn33cl9")))

(define-public crate-flutter_rust_bridge_macros-1.58.2 (c (n "flutter_rust_bridge_macros") (v "1.58.2") (h "09mvsgxrad4xmx2ncykvpvfa09gdwvyalbs3f83vd3znkam92jq5")))

(define-public crate-flutter_rust_bridge_macros-1.59.0 (c (n "flutter_rust_bridge_macros") (v "1.59.0") (h "19vjafvy5n6xwcx8vg7mzl2apllk5bqqma24q9jjsaznp9x880pc")))

(define-public crate-flutter_rust_bridge_macros-1.60.0 (c (n "flutter_rust_bridge_macros") (v "1.60.0") (h "1px17wzjrvqz82xd2hd9yi0zb7fr6xnr5j3bicmfm18b9srq2vyq")))

(define-public crate-flutter_rust_bridge_macros-1.61.0 (c (n "flutter_rust_bridge_macros") (v "1.61.0") (h "0lfpxmyp58s46px16kzjdl24ysr3n199cq3dc61f5slil5vwqybw")))

(define-public crate-flutter_rust_bridge_macros-1.61.1 (c (n "flutter_rust_bridge_macros") (v "1.61.1") (h "0zfj9x2lyx3xmxr2hwbvs0l7rqc10l6qjyp5icvyz42nchkxhp4d")))

(define-public crate-flutter_rust_bridge_macros-1.62.0 (c (n "flutter_rust_bridge_macros") (v "1.62.0") (h "14iz6wlx0jsnvl8xb1jf8lbpmvmld8w1ipmapdi3472xwcpfs5kd")))

(define-public crate-flutter_rust_bridge_macros-1.62.1 (c (n "flutter_rust_bridge_macros") (v "1.62.1") (h "151hqb3l5vyx6nw0725wf35p1v1kwkldz8w1gywqc852c7ihpbqa")))

(define-public crate-flutter_rust_bridge_macros-1.63.0 (c (n "flutter_rust_bridge_macros") (v "1.63.0") (h "0vnanwn2h4w8pymsrvbz5848h4alb3yi805p107k6azs8badlp52")))

(define-public crate-flutter_rust_bridge_macros-1.63.1 (c (n "flutter_rust_bridge_macros") (v "1.63.1") (h "1w8rxrj740vnv3n6g4az1qalj9vyaqv37aw5f8ig0ha58jpxm8rg")))

(define-public crate-flutter_rust_bridge_macros-1.64.0 (c (n "flutter_rust_bridge_macros") (v "1.64.0") (h "01zb6idw7g0f5myl5nfw4687hszkh0b5h4yymck0avgzf0710cfs")))

(define-public crate-flutter_rust_bridge_macros-1.65.0 (c (n "flutter_rust_bridge_macros") (v "1.65.0") (h "0xlz8rv3f04svpr0j32ywfk05igimw4qi7z70l5bj83gismvvynn")))

(define-public crate-flutter_rust_bridge_macros-1.65.1 (c (n "flutter_rust_bridge_macros") (v "1.65.1") (h "00db498zjcvrbwiiydmpk1xljqmgci7120xixr3as2ssi5mxhbn5")))

(define-public crate-flutter_rust_bridge_macros-1.66.0 (c (n "flutter_rust_bridge_macros") (v "1.66.0") (h "10jzx76cjjv6x6xqj4y4dml6cqmygny5zfkh94pdkwnd66msd7id")))

(define-public crate-flutter_rust_bridge_macros-1.67.0 (c (n "flutter_rust_bridge_macros") (v "1.67.0") (h "1sxzipqh8yzq7pgy6h2k68qcgb0qfsmnwbz1w4wg7z7izpdr4a9n")))

(define-public crate-flutter_rust_bridge_macros-1.68.0 (c (n "flutter_rust_bridge_macros") (v "1.68.0") (h "0wns8pwmnwcmzsvr1imr3qwy6mywqf2rz5x94dyf9vdg6lb7s67n")))

(define-public crate-flutter_rust_bridge_macros-1.69.0 (c (n "flutter_rust_bridge_macros") (v "1.69.0") (h "0may8vji03igfwi96jbxanlp5jajwgvgyk2y0hklcxkyxsbyiac7")))

(define-public crate-flutter_rust_bridge_macros-1.70.0 (c (n "flutter_rust_bridge_macros") (v "1.70.0") (h "126l1a06w3d4l9vfy6fcpmabjn9kx6pqyg3zs5p92zsw9hflvy9d")))

(define-public crate-flutter_rust_bridge_macros-1.71.0 (c (n "flutter_rust_bridge_macros") (v "1.71.0") (h "0ddqrsq04s9izb0xff4p3sw09jlbqpfin8m6z0r00ixnb1qyn1mv")))

(define-public crate-flutter_rust_bridge_macros-1.71.1 (c (n "flutter_rust_bridge_macros") (v "1.71.1") (h "1ks86s9xz0hh91fdaw1m7akjmmfcadacczzc718lrl1g7lwigiga")))

(define-public crate-flutter_rust_bridge_macros-1.71.2 (c (n "flutter_rust_bridge_macros") (v "1.71.2") (h "1wzg9jm7xni08gcdz110nq655jjf3bzacnpxpkdiwvpbj1pg8m7i")))

(define-public crate-flutter_rust_bridge_macros-1.72.0 (c (n "flutter_rust_bridge_macros") (v "1.72.0") (h "0aaay0r6f5a85l9q4c34jvi2m2ijl0md9cjsabhg00hk8g05c624")))

(define-public crate-flutter_rust_bridge_macros-1.72.1 (c (n "flutter_rust_bridge_macros") (v "1.72.1") (h "08m2c2lsjjwqxxr7vyk6f6k90kjwsfnvxk6bz62k1j2w1p5v2rba")))

(define-public crate-flutter_rust_bridge_macros-1.72.2 (c (n "flutter_rust_bridge_macros") (v "1.72.2") (h "1mc637hcwc5irlzkrrr3zq4i4lxbq5v44j6kn5sgpc4nx2f3x9jz")))

(define-public crate-flutter_rust_bridge_macros-1.73.0 (c (n "flutter_rust_bridge_macros") (v "1.73.0") (h "0smm3c5xsjrds7hfwsmnk89bxgyny82zvb202qnlrvbj6fi78r0c")))

(define-public crate-flutter_rust_bridge_macros-1.74.0 (c (n "flutter_rust_bridge_macros") (v "1.74.0") (h "10a134nzgmgp0j6qb7p6snh56gzc09k42pmx3dllpkzcpmynkghv")))

(define-public crate-flutter_rust_bridge_macros-1.75.0 (c (n "flutter_rust_bridge_macros") (v "1.75.0") (h "0f8z7w8ka7rg2lvbql4skgnm6cn9k5c3f0q45wgrb6zkz8h7wnxp")))

(define-public crate-flutter_rust_bridge_macros-1.75.1 (c (n "flutter_rust_bridge_macros") (v "1.75.1") (h "1aqli62hnggikaljkl65d57lf94jma2hcjm4n8d8h8gkyd8sy6yf")))

(define-public crate-flutter_rust_bridge_macros-1.75.2 (c (n "flutter_rust_bridge_macros") (v "1.75.2") (h "0jic2hm1qv8prfydnz6qcy57xi0l389y5408rffb7z7wzh9lcmxf")))

(define-public crate-flutter_rust_bridge_macros-1.75.3 (c (n "flutter_rust_bridge_macros") (v "1.75.3") (h "0qi9l9f3z3x017lcsnwg51jy3r0b5j3s0hmhgbdabd3c4875crny")))

(define-public crate-flutter_rust_bridge_macros-1.76.0 (c (n "flutter_rust_bridge_macros") (v "1.76.0") (h "1khmv599vgsdqni4g1saa4hgrfn3vfd0iccyix3y6h6b769rvl96")))

(define-public crate-flutter_rust_bridge_macros-1.77.0 (c (n "flutter_rust_bridge_macros") (v "1.77.0") (h "1ivx0s285smymbnzp885hj4bibp83b0d06ijpr7s3qlhr1d2akhf")))

(define-public crate-flutter_rust_bridge_macros-1.77.1 (c (n "flutter_rust_bridge_macros") (v "1.77.1") (h "14mrp4b8fys0lqaq5jqv0nvysrkbsrjgawyh5llspf1c71nsp4x4")))

(define-public crate-flutter_rust_bridge_macros-1.78.0 (c (n "flutter_rust_bridge_macros") (v "1.78.0") (h "1sj599imjw44hz5dwlzh6zy11sjjvgd0zggabwx86fna4mz0pxbr")))

(define-public crate-flutter_rust_bridge_macros-1.79.0 (c (n "flutter_rust_bridge_macros") (v "1.79.0") (h "15j9q10rbnka00ngzx3xcmarvsax4rn90zz3krlyampa3h6xyzi3")))

(define-public crate-flutter_rust_bridge_macros-1.80.0 (c (n "flutter_rust_bridge_macros") (v "1.80.0") (h "1c2fn4c850is8y7968bkpb29fqzgd178yvkxpc7h373lmwwp326c")))

(define-public crate-flutter_rust_bridge_macros-1.80.1 (c (n "flutter_rust_bridge_macros") (v "1.80.1") (h "0afdm09j1n5frg8bbxpjcqrb513j5zqd2vix6dvzplr2rmryswnz")))

(define-public crate-flutter_rust_bridge_macros-1.81.0 (c (n "flutter_rust_bridge_macros") (v "1.81.0") (h "1zrsm5f7r1c4labj2zdp6phb032vizfxfzmwfgmqk5qr2md137wl")))

(define-public crate-flutter_rust_bridge_macros-1.82.0 (c (n "flutter_rust_bridge_macros") (v "1.82.0") (h "1ikbiaqzhns8c6dhjaqsbdlwcg3hsi0idj7bk46knpamq6v0gych")))

(define-public crate-flutter_rust_bridge_macros-1.82.1 (c (n "flutter_rust_bridge_macros") (v "1.82.1") (h "1ww2c8wny7839djscjdwi3540dsd9537lx67w85sa34jmim77x0f")))

(define-public crate-flutter_rust_bridge_macros-1.82.2 (c (n "flutter_rust_bridge_macros") (v "1.82.2") (h "13v6c75y2a7vmfdlrjl646jdllf3xmvab04sq1h2kymqvmfxiw5y")))

(define-public crate-flutter_rust_bridge_macros-1.82.3 (c (n "flutter_rust_bridge_macros") (v "1.82.3") (h "15ir4y4fwnicxjg9p22f5kvhmc30cixqvnaznnnw228abwbkvayi")))

(define-public crate-flutter_rust_bridge_macros-1.82.4 (c (n "flutter_rust_bridge_macros") (v "1.82.4") (h "1nvvyw8mdjzwl2lya37phs58idsckbhi84hak1xv38j9hg0y8bqw")))

(define-public crate-flutter_rust_bridge_macros-1.82.5 (c (n "flutter_rust_bridge_macros") (v "1.82.5") (h "02v01xcbka7j4l5l5fcn8g03kyigbjs5w9q7f6sj3lmwikyv0qnm")))

(define-public crate-flutter_rust_bridge_macros-1.82.6 (c (n "flutter_rust_bridge_macros") (v "1.82.6") (h "19abafnypfk1g7gsfp2nrl0klr590dnk8wllidbz9v8vj8yp9zm7")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.0 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.0") (h "1kn21zsfnxkj2sb02gbvlbgqkv1f6vy1fnk8g668sjgwmp8zsh0k")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.1 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.1") (h "1vkpgqdd0bn2ciz1kpq9zzk93x0mrf40sqsba68gm62crgdb6168")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.2 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.2") (h "1daij37a5881bnc2vp264i29spi7hsrw25mvlrhvs2br68dxdj2i")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.3 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.3") (h "0iq2qwjjilzbm2q5x70gc707kv1qxmqij5mf1rd90r85k7nbfhk9")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.4 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.4") (h "000ncscpwxfnyb80791mxaiswq36i1is344bkbx7sj5mnz6cdk18")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.5 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.5") (h "1hi3q1x3s18gakna83n7spsbkw1n77yf7s6yngp1rk417ikgrllv")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.6 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.6") (h "02b435yqb00ij9yzqhqagcdji2hic2dj11a3fj7mpm7683msh3bn")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.7 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.7") (h "19hxznnvd6r6477wp3dbmnynbh21iffixr83bd7aa54mr1xbazi4")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.8 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.8") (h "0vp22gkzz3cjfy501r8zl7grmb6i07hd28x5qdk3pldnkkw3mrj7")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.9 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.9") (h "1agrdknh74a32b0c4i28x3m3240prvx3l4334zgfv06a4pansqgl")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.10 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.10") (h "142912ij0d2kn4xi18qf79ilfnidqdignpp3sngkbfp7s23mkpmi")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.11 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.11") (h "0npyd4k1abc9gw1ngz947q7lzqdqglaa4q79h0nfg8zwf41fms47")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.12 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.12") (h "1wghwkhadw9c6rm21x5i7di0aw9n40vn48crp6yhddb7sn06gaan")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.13 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.13") (h "028946cbi3w0iij9br326si5jk7zdj3gcxcvsbfldj45xrpmdk3n")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.14 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.14") (h "0rlcgbjxg3r73k30by3sj86qfpmbjc7kpsh013zlclgp9ix6ccz3")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.15 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.15") (h "0d49l2l68g71fjqwkavm4p1h5snynjpawrdi45mfr3833jyq26zv")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.16 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.16") (h "13y72fv0kx5fknd0wsliqsrixyjd45s3v3avzhpl4yc7pgrkh50m")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.17 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.17") (h "0vhla6fsbmnlnznj4vysam0wdvqrnb8jsjwrljvk2696frg68r26")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.18 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.18") (h "1r89qyd8q23b0c77nnfyclv122nin5vihq14jdwvd6skc10di267")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.19 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.19") (h "1l508n8bw2b96wiasfx391y7fxamx1swlvzsnjyk7s3lx5gm1wr5")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.20 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.20") (h "0lyx4y6wwrm02ppikjx7gy93xf22fdzd1ay1aspqnhjgf6lcrvn2")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.21 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.21") (h "0clcb2ndfkr683lmbjx698ca420vr17839kgnzwqsqssp5k5adw1")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.22 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.22") (h "19ib19h6qapzg9ac2i1x9i2d8jli69gwjf4lcw829z5qxd0hljfx")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.23 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.23") (h "1jg8igljklii9xkb57q3k0d4hd90p4xx6kc755xs50zb4cqd7gfi")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.24 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.24") (h "09bq4ybi8n6y89qnv3sw50bi338ws5q7hvl5hb1amwbkk8ib547s")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.25 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.25") (h "13mxrf11a1llkdlc82i6yijlqkbk9drla3ggvpnv6l9dnzrfd9bs")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.26 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.26") (h "1i39hfw64fby74lrfc4irk4ff0vfzj3ixdg1p3ww5cmrp9d8xwk2")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.27 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.27") (h "0vicccg6m9z6n0ig84z2vv7kzn999dmrb2557ffzzj2rmydc4c75")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.28 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.28") (h "19pyr04rvhds4k52irn8mch1kx7fmq0nv3l370w05xm7dqfwfi2q")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.29 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.29") (h "0vni31f9rzhgmi8zz869n692rbr455dp881nc8lh07y9cdaxzkxy")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.30 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.30") (h "1wi4b3lgjck8zxqkiz7bm157n941ly675jr1mf9wbp377501rm6c")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.31 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.31") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jzpfc8gmgbx487ic55jadknmm10g3z7vca5656q1bq4dzjxybp0")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.32 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.32") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l9x2k9sfgdhdv65zpjjq6g2h8apdaa5jf3kk6559favjm3049gx")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.33 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.33") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p50v5a3nz6cy7w9ffkg8d6pc5zp28k6bqb0394w85k3q45lclnq")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.34 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.34") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11x2diblmqf8qbfbjp4g7axm5x3cyvmbmfly35w8jp7848fmxpgm")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.35 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.35") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07myknnrsp7csq4z9cazzpm0bammj82v3bdar33b4ga5wswza48a")))

(define-public crate-flutter_rust_bridge_macros-2.0.0-dev.36 (c (n "flutter_rust_bridge_macros") (v "2.0.0-dev.36") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jfmiqfgx0h04h7hf5lfw9xcqig1c0ldcmi3k03qzh9awf468dh4")))

