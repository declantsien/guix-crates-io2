(define-module (crates-io fl ut fluttershy) #:use-module (crates-io))

(define-public crate-fluttershy-0.1.0 (c (n "fluttershy") (v "0.1.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "063vlbb2g55pksz8050d6b56jzcam32vxhl8civy6aish4h3aisf") (y #t)))

