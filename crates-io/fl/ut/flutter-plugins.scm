(define-module (crates-io fl ut flutter-plugins) #:use-module (crates-io))

(define-public crate-flutter-plugins-0.3.4 (c (n "flutter-plugins") (v "0.3.4") (d (list (d (n "flutter-engine") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tinyfiledialogs") (r "^3.3.5") (d #t) (k 0)))) (h "0mdcqn7pjiipam84f9ic51vriali2w127wm6sbjq0h1w2jww4bkv")))

(define-public crate-flutter-plugins-0.3.5 (c (n "flutter-plugins") (v "0.3.5") (d (list (d (n "flutter-engine") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tinyfiledialogs") (r "^3.3.5") (d #t) (k 0)))) (h "1mzdh8av1s38i8kxfagr3lv2cadxw58lb75lxh68df5dkyl5wjb6")))

