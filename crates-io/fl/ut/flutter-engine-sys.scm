(define-module (crates-io fl ut flutter-engine-sys) #:use-module (crates-io))

(define-public crate-flutter-engine-sys-0.1.0 (c (n "flutter-engine-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)))) (h "0d3icaka0fgfi5mnw7j89l94cfcj4a4ficy3id8mhxhgr902qpzn")))

(define-public crate-flutter-engine-sys-0.3.0 (c (n "flutter-engine-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "flutter-download") (r "^0.3.0") (d #t) (k 1)))) (h "0lmi4ky6kqzsx4a2c8l2hzzyaabv76c2c3nq2xk0ibbm8fp1gc1h")))

(define-public crate-flutter-engine-sys-0.3.1 (c (n "flutter-engine-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "flutter-download") (r "^0.3.0") (d #t) (k 1)))) (h "0c1cmjb1va1lhdk5w7zd2wr5qphc4mwfkfw15sdpji3z0yn7412j")))

(define-public crate-flutter-engine-sys-0.3.2 (c (n "flutter-engine-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "flutter-download") (r "^0.3.0") (d #t) (k 1)))) (h "1gmnp8vyjblzizxkz4lgdllmj8ja800nwzxn9ci9ynhnab3n4zq1")))

(define-public crate-flutter-engine-sys-0.3.4 (c (n "flutter-engine-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "flutter-download") (r "^0.3.0") (d #t) (k 1)))) (h "0jlj4wz2a25vmaz882kgypsbpq9vmxfvqnmv59r6sbrxixm9f4s0")))

(define-public crate-flutter-engine-sys-0.3.5 (c (n "flutter-engine-sys") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "dirs") (r "^2.0.2") (d #t) (k 1)) (d (n "flutter-download") (r "^0.3.5") (d #t) (k 1)))) (h "1k5svmdaphm4l6y71789lb0sgx6m1bcalxq821lbhz56q8s9hy68")))

