(define-module (crates-io fl ut flutterbug) #:use-module (crates-io))

(define-public crate-flutterbug-0.1.0 (c (n "flutterbug") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "font-kit") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "x11") (r "^2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1qpr04hs8grg2002plaqfb3azjh5cq4l614x0dqv7pr6x6jaqpcz")))

(define-public crate-flutterbug-0.1.1 (c (n "flutterbug") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "font-kit") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "x11") (r "^2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0cq89qkwz8p6j3db5s79jbdagd3jy5kp3vassv1dpc43hdn7j3lg")))

