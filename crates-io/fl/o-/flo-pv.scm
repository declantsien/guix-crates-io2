(define-module (crates-io fl o- flo-pv) #:use-module (crates-io))

(define-public crate-flo-pv-0.1.0 (c (n "flo-pv") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)))) (h "0gn496kj8jf460d4fb932ir3zifmsshbpqnm1pwyx5rq0ra7kwdd")))

