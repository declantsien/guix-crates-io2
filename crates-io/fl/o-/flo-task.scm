(define-module (crates-io fl o- flo-task) #:use-module (crates-io))

(define-public crate-flo-task-0.1.0 (c (n "flo-task") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "macros" "time" "rt-core"))) (d #t) (k 0)))) (h "09ffagk387i8irf42wq5rcnw5myb12q2xzw0csbsy9qrjdwgh6ks")))

