(define-module (crates-io fl #{0r}# fl0rian_s_lesson_grrs) #:use-module (crates-io))

(define-public crate-fl0rian_s_lesson_grrs-0.1.0 (c (n "fl0rian_s_lesson_grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1kcmjsqq26b28r6klabbm4pz0n94ajxcgigyh1w5ym6hbnmann3a")))

