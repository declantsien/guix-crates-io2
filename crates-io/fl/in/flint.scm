(define-module (crates-io fl in flint) #:use-module (crates-io))

(define-public crate-flint-0.1.0 (c (n "flint") (v "0.1.0") (h "0bkikvhpb6iwbgbs9c09i5qbg3kz67ihn4rpghawfjcq2xjkqfr4")))

(define-public crate-flint-0.1.1 (c (n "flint") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "0rqn51qrq0vfdmc9vbchxhf6br98ddpzg8nvzzakqmll0w5jy6r6")))

(define-public crate-flint-2.0.0 (c (n "flint") (v "2.0.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sane") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07jwdw2iy5wj5j66apbrwzsqj0arf22928bis38qgjzbpd6vrgin")))

(define-public crate-flint-2.1.0 (c (n "flint") (v "2.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sane") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qf8qwcm2kpqbx3m3f6d9xs5kp9qpp92apiyyx88yyhaabf44s9n")))

(define-public crate-flint-2.2.0 (c (n "flint") (v "2.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sane") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i3nj84yb68rwx9ig6wa88f5pvmimx1s95a7624rh9kyvb8bg2kr")))

