(define-module (crates-io fl in flinn_engdahl) #:use-module (crates-io))

(define-public crate-flinn_engdahl-0.1.0 (c (n "flinn_engdahl") (v "0.1.0") (h "14lbsnj22rlv03fz4dlj55gxmmj8j59fbslxffp9x9fdvmq9dxdi")))

(define-public crate-flinn_engdahl-0.1.1 (c (n "flinn_engdahl") (v "0.1.1") (h "1hrwqsqqm41f4mw1q0aiy8jbi9frma9p9wfwayy4y6jajmq4bqjl")))

