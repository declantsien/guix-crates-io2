(define-module (crates-io fl ue fluence-fork-libp2p-core-derive) #:use-module (crates-io))

(define-public crate-fluence-fork-libp2p-core-derive-0.20.2 (c (n "fluence-fork-libp2p-core-derive") (v "0.20.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0hvm9nfdq5p6rcyjfdkrryi1b5s29l7plvwp496adr8li625kyvp")))

(define-public crate-fluence-fork-libp2p-core-derive-0.21.0 (c (n "fluence-fork-libp2p-core-derive") (v "0.21.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1h2alxzma4p2a2mrk66v1sb9sf0bjbld4pmily9wghshapl9flqx")))

(define-public crate-fluence-fork-libp2p-core-derive-0.20.3 (c (n "fluence-fork-libp2p-core-derive") (v "0.20.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1iwkb7400gvrx3k9ibkinkrhlj9l33wgc0lsxrh9wxc2ndznyyaj")))

(define-public crate-fluence-fork-libp2p-core-derive-0.21.1 (c (n "fluence-fork-libp2p-core-derive") (v "0.21.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0bc666iqh5hbl372vf8f4x4hy121iimwr5fbhq91zxw5kwpxym7b")))

