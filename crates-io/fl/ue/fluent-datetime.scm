(define-module (crates-io fl ue fluent-datetime) #:use-module (crates-io))

(define-public crate-fluent-datetime-0.1.0 (c (n "fluent-datetime") (v "0.1.0") (d (list (d (n "fluent") (r "^0.16") (d #t) (k 2)) (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "icu_calendar") (r "^1.3") (d #t) (k 0)) (d (n "icu_datetime") (r "^1.3") (d #t) (k 0)) (d (n "icu_locid") (r "^1.3") (d #t) (k 0)) (d (n "icu_provider") (r "^1.3") (d #t) (k 0)) (d (n "intl-memoizer") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "18nchx5gv92z4dhwym2zfvygqa9vdfslisihl2mgb8gmcx6imy0p")))

