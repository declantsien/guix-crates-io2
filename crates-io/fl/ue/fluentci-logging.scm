(define-module (crates-io fl ue fluentci-logging) #:use-module (crates-io))

(define-public crate-fluentci-logging-0.1.0 (c (n "fluentci-logging") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("rustls-tls" "blocking"))) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ynpgw5lflx9arip73b21vvajapyli8djysafjwvjm7h7ii60y9p")))

