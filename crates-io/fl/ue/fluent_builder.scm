(define-module (crates-io fl ue fluent_builder) #:use-module (crates-io))

(define-public crate-fluent_builder-0.1.0 (c (n "fluent_builder") (v "0.1.0") (h "0y0ddrkij2mi4hvy1d8n42bnqzlx8lzgfaaq9jmdggzs0ydy5cxl")))

(define-public crate-fluent_builder-0.2.0 (c (n "fluent_builder") (v "0.2.0") (h "0syxqh0ifqbfxsh2wbvrjk8vsp2rhf6w41n0zqdrz0nmfvwj23z8")))

(define-public crate-fluent_builder-0.2.1 (c (n "fluent_builder") (v "0.2.1") (h "1qpff51s8fn9ayjk3wi5wfbpqavi3q7v6bz1rsaixdfv19iw83xb")))

(define-public crate-fluent_builder-0.3.0 (c (n "fluent_builder") (v "0.3.0") (h "0lwhc99c8i2zmdxlxvlfjba878qrfv6k1680gqi6wq26ka1v6qnd")))

(define-public crate-fluent_builder-0.4.0 (c (n "fluent_builder") (v "0.4.0") (h "04wyiyb33xv4lwazimsszc0h6mk1qrwlcxpq0j2xk9qir77snckm")))

(define-public crate-fluent_builder-0.5.0 (c (n "fluent_builder") (v "0.5.0") (h "0mqbqbb8d02x3jh6qf7nb7fkm4s0m0vr9a2whj9n9s3mxkqbag7h")))

(define-public crate-fluent_builder-0.6.0 (c (n "fluent_builder") (v "0.6.0") (h "15xcpcrj3ry3awqam89gqm34kw99cd20nn6yvmnnbm393vdnzn7k")))

