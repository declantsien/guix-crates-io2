(define-module (crates-io fl ue fluence_sdk) #:use-module (crates-io))

(define-public crate-fluence_sdk-0.0.1 (c (n "fluence_sdk") (v "0.0.1") (h "0c649yi2c8vs3jrn53nak987da9lbxmzmk6n6f0w6mkbxsgb2bfq")))

(define-public crate-fluence_sdk-0.0.2 (c (n "fluence_sdk") (v "0.0.2") (h "0z0r7s0sc2z0vwgv056kmrswbi7218icqvm3kvh4ip9giwl717zc")))

(define-public crate-fluence_sdk-0.0.3 (c (n "fluence_sdk") (v "0.0.3") (h "1n7p47zd4syhzl617mr42jcpi4plzgyh34dxf9rpwz8qh5mdvk8m")))

(define-public crate-fluence_sdk-0.0.4 (c (n "fluence_sdk") (v "0.0.4") (h "0my9ba5gpn3ph0lplqywd863fwszs40mmbxyabinz1z889x3il1q")))

(define-public crate-fluence_sdk-0.0.5 (c (n "fluence_sdk") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "0c505n4b18mya1hi9d5618dgm3yg9jk6jswh21kg5dpfki0pl7r9") (f (quote (("wasm_logger"))))))

(define-public crate-fluence_sdk-0.0.6 (c (n "fluence_sdk") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "05w13znylr3w8qiknzk8wq5byghvq30ia2mifdsg36j05067mbz4") (f (quote (("wasm_logger"))))))

