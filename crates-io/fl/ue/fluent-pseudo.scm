(define-module (crates-io fl ue fluent-pseudo) #:use-module (crates-io))

(define-public crate-fluent-pseudo-0.0.1 (c (n "fluent-pseudo") (v "0.0.1") (d (list (d (n "regex") (r "^1.2") (d #t) (k 0)))) (h "03f4g4k6xhk731848fg9hgz9mbz36plh610yki4rj8151vm5hyc7")))

(define-public crate-fluent-pseudo-0.1.0 (c (n "fluent-pseudo") (v "0.1.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "09xycccbbm9rrh91brna7f8cngjqvrp4z73z66rymnhdmglh0hij")))

(define-public crate-fluent-pseudo-0.2.0 (c (n "fluent-pseudo") (v "0.2.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0x2k4ikkciqhr3arjbn5w7x5hf78d1hrrv279vx4bkmwxpq538xd")))

(define-public crate-fluent-pseudo-0.2.1 (c (n "fluent-pseudo") (v "0.2.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0qi41plllyx5ch61xjk61lb8mv8w4a4w07pv25fifbf4xw58ffna")))

(define-public crate-fluent-pseudo-0.2.2 (c (n "fluent-pseudo") (v "0.2.2") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1bawq7g1d75x24csxzxj2qs81p168aj24bqskkldclvmvxdx2bim")))

(define-public crate-fluent-pseudo-0.2.3 (c (n "fluent-pseudo") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fhnwmnfxm38zb60wq0y2qqbdf7598k3gk51iwykp42j5j0dca7h")))

(define-public crate-fluent-pseudo-0.3.0 (c (n "fluent-pseudo") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0izir100sqksai6szx9h9mx39aqccj6nbp7fyxz9ijw40vd5kaik")))

(define-public crate-fluent-pseudo-0.3.1 (c (n "fluent-pseudo") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0byldssmzjdmynbh1yvdrxcj0xmhqznlmmgwnh8a1fhla7wn5vgx")))

(define-public crate-fluent-pseudo-0.3.2 (c (n "fluent-pseudo") (v "0.3.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mp5rib3mzhiwbw9s3g0climzy4wxxp0angn5ycmspl0gdid6d81")))

