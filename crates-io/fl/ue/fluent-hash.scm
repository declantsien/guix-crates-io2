(define-module (crates-io fl ue fluent-hash) #:use-module (crates-io))

(define-public crate-fluent-hash-0.1.0 (c (n "fluent-hash") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "0j4nv580xjcnnv32b0km0bqlwkincji1rw7bkh3x53bcjrsilyh4") (y #t)))

(define-public crate-fluent-hash-0.1.1 (c (n "fluent-hash") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "0g0f2v6hzsf9fdpjk35s8mmy6fdz6c0qyi2kf53sbpghk0klldx0") (y #t)))

(define-public crate-fluent-hash-0.2.0 (c (n "fluent-hash") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "0p0lph519w9xzfl366gq0hdmbabki3ad5rsp9hkr2qaga073l1bl") (y #t)))

(define-public crate-fluent-hash-0.2.1 (c (n "fluent-hash") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "1jzvj89hbxiljpdwj0xc6vs8ik95k3x1087r1py0hjnb0i21xrdg")))

(define-public crate-fluent-hash-0.2.2 (c (n "fluent-hash") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "19c2xw6yd148jbf71h4mxzp34i40znsvjjwcyaxy4jq4ijk0x6zs")))

(define-public crate-fluent-hash-0.2.3 (c (n "fluent-hash") (v "0.2.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.17.6") (d #t) (k 0)))) (h "11fxwnhpmm9kfqdajc2qpb2nl4m0bkfxik7n0zhlp8bp0fsniddn")))

