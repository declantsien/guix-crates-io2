(define-module (crates-io fl ue fluent-testing) #:use-module (crates-io))

(define-public crate-fluent-testing-0.0.1 (c (n "fluent-testing") (v "0.0.1") (d (list (d (n "fluent-bundle") (r "^0.15.2") (d #t) (k 0)) (d (n "fluent-fallback") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros" "io-util"))) (o #t) (d #t) (k 0)))) (h "18gsnbvp0znplf813rsd5x1191c80j4s5b206zh79z5c6dcp032v") (f (quote (("sync") ("default" "sync") ("async" "tokio"))))))

(define-public crate-fluent-testing-0.0.2 (c (n "fluent-testing") (v "0.0.2") (d (list (d (n "fluent-bundle") (r "^0.15.2") (d #t) (k 0)) (d (n "fluent-fallback") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros" "io-util"))) (o #t) (d #t) (k 0)))) (h "1pqz0kj0akj471b487rzc65jhn9dqrds1jy936ixhlyq63w7sng4") (f (quote (("sync") ("default" "sync") ("async" "tokio"))))))

(define-public crate-fluent-testing-0.0.3 (c (n "fluent-testing") (v "0.0.3") (d (list (d (n "fluent-bundle") (r "^0.15.2") (d #t) (k 0)) (d (n "fluent-fallback") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros" "io-util"))) (o #t) (d #t) (k 0)))) (h "1y77f743azzi2q4d4rd2gyvjaf9n4zvyr3896k00dm9fqapfp5yd") (f (quote (("sync") ("default" "sync") ("async" "tokio"))))))

(define-public crate-fluent-testing-0.0.4 (c (n "fluent-testing") (v "0.0.4") (d (list (d (n "fluent-bundle") (r "^0.15.3") (d #t) (k 0)) (d (n "fluent-fallback") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros" "io-util"))) (o #t) (d #t) (k 0)))) (h "0csd1xqlwjqg8yxjpqmbq4sqla95jczpbnfmvpg6zhh1d823rbqd") (f (quote (("sync") ("default" "sync") ("async" "tokio"))))))

