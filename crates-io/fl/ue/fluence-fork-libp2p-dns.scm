(define-module (crates-io fl ue fluence-fork-libp2p-dns) #:use-module (crates-io))

(define-public crate-fluence-fork-libp2p-dns-0.20.0 (c (n "fluence-fork-libp2p-dns") (v "0.20.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.20.0") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "0gb0h2vms95g0iaipxv1hapyswmmgkjhqmqdhgc3nmmhqp5djbaj")))

(define-public crate-fluence-fork-libp2p-dns-0.26.0 (c (n "fluence-fork-libp2p-dns") (v "0.26.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.26.0") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "0m3j21v09fzg10iqr33l3drhg1q3k4dyms25g3ba3jhci84xh05k")))

(define-public crate-fluence-fork-libp2p-dns-0.26.1 (c (n "fluence-fork-libp2p-dns") (v "0.26.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.26.1") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "1k6a4dgw0q6c4rlmxlyny8n4mcabngzxwdwcp0wbbism1mc409zl")))

(define-public crate-fluence-fork-libp2p-dns-0.27.0 (c (n "fluence-fork-libp2p-dns") (v "0.27.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.27.1") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "1ppimrkb7g4dq9mbgkvfm2b2dh8wa8s9bj4qzyvxv04qif1wmal0")))

(define-public crate-fluence-fork-libp2p-dns-0.27.1 (c (n "fluence-fork-libp2p-dns") (v "0.27.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.27.2") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "03zgfvkw0x7q96ha3ajqpbxpvgr991qydqjswb43l6hac5f5sjbj")))

