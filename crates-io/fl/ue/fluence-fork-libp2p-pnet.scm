(define-module (crates-io fl ue fluence-fork-libp2p-pnet) #:use-module (crates-io))

(define-public crate-fluence-fork-libp2p-pnet-0.19.1 (c (n "fluence-fork-libp2p-pnet") (v "0.19.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.17") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "salsa20") (r "^0.3.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0vswng2d19kcj78k35vgb48m93rma4lll7lzfrg2d4qfn77zc82l")))

(define-public crate-fluence-fork-libp2p-pnet-0.20.0 (c (n "fluence-fork-libp2p-pnet") (v "0.20.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "salsa20") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "0i62gvpipfjcpkyy98f0q5slzbgdrqvwbrp008gbv9j7yfqdgkci")))

(define-public crate-fluence-fork-libp2p-pnet-0.20.1 (c (n "fluence-fork-libp2p-pnet") (v "0.20.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "salsa20") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "0z069v2xr0nn38bdh1kfkasdvj0cvpmny2z37cjhnli34wbafhx5")))

(define-public crate-fluence-fork-libp2p-pnet-0.20.2 (c (n "fluence-fork-libp2p-pnet") (v "0.20.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "salsa20") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "0sj05bfc48qyp1a88l914fsmbm4cpacl02m5sdi48c4p2ymmn3sk")))

