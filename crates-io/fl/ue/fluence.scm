(define-module (crates-io fl ue fluence) #:use-module (crates-io))

(define-public crate-fluence-0.0.6 (c (n "fluence") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "1vqp462jp7z3p7860yqsfzxp6aqnmvswwyya1v4anr3ljgfya3yg") (f (quote (("wasm_logger")))) (y #t)))

(define-public crate-fluence-0.0.7 (c (n "fluence") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "1si4anpag5ly1bvdzg28xi2b6x1r3ssd0m2lirm2ffl313kk2slx") (f (quote (("wasm_logger_warn") ("wasm_logger_info") ("wasm_logger_error") ("export_allocator")))) (y #t)))

(define-public crate-fluence-0.0.8 (c (n "fluence") (v "0.0.8") (d (list (d (n "fluence-sdk-macro") (r "= 0.0.8") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hmr7437xvrzbfgsi66gdnzm2vwkl0q2bspqf0wfn0j11b5s963i") (f (quote (("wasm_logger_warn" "fluence-sdk-main/wasm_logger_warn") ("wasm_logger_info" "fluence-sdk-main/wasm_logger_info") ("wasm_logger_error" "fluence-sdk-main/wasm_logger_error") ("export_allocator" "fluence-sdk-main/export_allocator")))) (y #t)))

(define-public crate-fluence-0.0.9 (c (n "fluence") (v "0.0.9") (d (list (d (n "fluence-sdk-macro") (r "= 0.0.9") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p7hz0qkgyf1b0s94hw2vdrv9k7j3pj31q98p6n4990ssbj9marl") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator")))) (y #t)))

(define-public crate-fluence-0.0.10 (c (n "fluence") (v "0.0.10") (d (list (d (n "fluence-sdk-macro") (r "= 0.0.10") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5vqyy4kyagh3dsa6vy3bpp7k1na4rb1kz9xf8v15b9w8smykr7") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator")))) (y #t)))

(define-public crate-fluence-0.0.11 (c (n "fluence") (v "0.0.11") (d (list (d (n "fluence-sdk-macro") (r "= 0.0.11") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.0.11") (d #t) (k 0)))) (h "1faycahqiyllwkqj0zmx1idp6kzx49z0xla8wjfmm4c37nscfnmw") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("no_export_allocator" "fluence-sdk-main/no_export_allocator")))) (y #t)))

(define-public crate-fluence-0.0.12 (c (n "fluence") (v "0.0.12") (d (list (d (n "fluence-sdk-macro") (r "= 0.0.12") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.0.12") (d #t) (k 0)))) (h "1b4pn8vj1ga8lpl8s7ysgs1lxs8y9g57f7fbjz3yb2z0b8mjav4h") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("no_export_allocator" "fluence-sdk-main/no_export_allocator")))) (y #t)))

(define-public crate-fluence-0.0.13 (c (n "fluence") (v "0.0.13") (d (list (d (n "fluence-sdk-macro") (r "= 0.0.13") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.0.13") (d #t) (k 0)))) (h "0ab0vlynsfn771xl9zzq1cbdqn9cyjvpvd6gqbx0h2mkqlsynw1p") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator")))) (y #t)))

(define-public crate-fluence-0.1.0 (c (n "fluence") (v "0.1.0") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.0") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.0") (d #t) (k 0)))) (h "03r4hjngjss67m5bijyw7lwhh9i38iv8gdz1s6xl6dw9d5d6d0r8") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator")))) (y #t)))

(define-public crate-fluence-0.1.2 (c (n "fluence") (v "0.1.2") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.2") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.2") (d #t) (k 0)))) (h "1w0lxl895vs95d169wiqinkblzagbkcfqsmcxkmgyrj0dlwjpy9f") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator")))) (y #t)))

(define-public crate-fluence-0.1.3 (c (n "fluence") (v "0.1.3") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.3") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.3") (d #t) (k 0)))) (h "1c4cicns1fk91x2y08n5l3w1if6vsgxbgng8hl19x7wxpwqmwxqi") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator"))))))

(define-public crate-fluence-0.1.4 (c (n "fluence") (v "0.1.4") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.4") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.4") (d #t) (k 0)))) (h "0l6prpqj3lxyafaqh24xv3lsswpgfn186h7d61p15691swmxsfvl") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator") ("default" "export_allocator"))))))

(define-public crate-fluence-0.1.5 (c (n "fluence") (v "0.1.5") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.5") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.5") (d #t) (k 0)))) (h "1pfa05gqvnqjnf4llzd28kchlkmd3l25mjzrn2xf6ijv6kw6ijv0") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator") ("default" "export_allocator"))))))

(define-public crate-fluence-0.1.6 (c (n "fluence") (v "0.1.6") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.6") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.6") (d #t) (k 0)))) (h "0qfb6dc08467pgsfa8x00fymm4vy1dxpc0mlzd4pg8502mipp9z5") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator") ("default" "export_allocator"))))))

(define-public crate-fluence-0.1.7 (c (n "fluence") (v "0.1.7") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.7") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.7") (d #t) (k 0)))) (h "0kcvl7qwb22k9jp57snccvvpay5hi8v9vvlwb26l6i4k2p7qxnjl") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator") ("default" "export_allocator"))))))

(define-public crate-fluence-0.1.8 (c (n "fluence") (v "0.1.8") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.8") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.8") (d #t) (k 0)))) (h "0fv74m2qjdnm5as9v85zvw5j0vivgg0lb2lgfprcc0i62vlvxhx9") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator") ("default" "export_allocator"))))))

(define-public crate-fluence-0.1.10 (c (n "fluence") (v "0.1.10") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.10") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.10") (d #t) (k 0)))) (h "1mqyz23mpjbbxrp7vxvd7pjixih7j0jdxw4d9yc4qp70x804q5vm") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("export_allocator" "fluence-sdk-main/export_allocator") ("default" "export_allocator"))))))

(define-public crate-fluence-0.1.11 (c (n "fluence") (v "0.1.11") (d (list (d (n "fluence-sdk-macro") (r "= 0.1.11") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "= 0.1.11") (d #t) (k 0)))) (h "0gd3j8hahfsb1x0xhxhacnyp067spdwxwk2a3drh5jyahp0bh3iq") (f (quote (("wasm_logger" "fluence-sdk-main/wasm_logger") ("side_module" "fluence-sdk-main/side_module") ("export_allocator" "fluence-sdk-main/export_allocator") ("default" "export_allocator"))))))

(define-public crate-fluence-0.2.0 (c (n "fluence") (v "0.2.0") (d (list (d (n "fluence-sdk-macro") (r "=0.2.0") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.0") (d #t) (k 0)))) (h "11rvg5xnkavq5qfc538j0000521iwvbpndvmqcsdmg5hrxinn6c9") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.1 (c (n "fluence") (v "0.2.1") (d (list (d (n "fluence-sdk-macro") (r "=0.2.1") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.1") (d #t) (k 0)))) (h "0fd2zj6wraxq3flz9z7ki7891jg6d2d0x7p06dzf00dl0129f1lk") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.2 (c (n "fluence") (v "0.2.2") (d (list (d (n "fluence-sdk-macro") (r "=0.2.2") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.2") (d #t) (k 0)))) (h "10q53vbhzzxhn35al16shr9zgkib5ph8kv93iswqzsfybrzhvv0l") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.3 (c (n "fluence") (v "0.2.3") (d (list (d (n "fluence-sdk-macro") (r "=0.2.3") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.3") (d #t) (k 0)))) (h "01v0r37n91hzscfnfigvvr6cpgvfqlimar4rsy27w9066px858s7") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.4 (c (n "fluence") (v "0.2.4") (d (list (d (n "fluence-sdk-macro") (r "=0.2.4") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.4") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)))) (h "17f93bailh4a6hlxjyngjvnids4djq4f1dfqyhqgp2sk6gbxvccc") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.7 (c (n "fluence") (v "0.2.7") (d (list (d (n "fluence-sdk-macro") (r "=0.2.7") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.7") (d #t) (k 0)))) (h "0zsrcx1i75nq9yrx01vb12rhvw1xvp5bpw7gjjn03sjkpxc5dkzr") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.8 (c (n "fluence") (v "0.2.8") (d (list (d (n "fluence-sdk-macro") (r "=0.2.8") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.8") (d #t) (k 0)))) (h "1vy2zgw9qaxndg0aq7qd50n7ln88z1wiya6x4jw44s7gh93y9vqj") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.9 (c (n "fluence") (v "0.2.9") (d (list (d (n "fluence-sdk-macro") (r "=0.2.9") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.9") (d #t) (k 0)))) (h "1ayqyiv7af763gnbsqwfilwsgr6i0gwxd3gvd64pb1j2nqdp768h") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.11 (c (n "fluence") (v "0.2.11") (d (list (d (n "fluence-sdk-macro") (r "=0.2.11") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.11") (d #t) (k 0)))) (h "1fy34x2ynax228z2phk2s3ml0na6as167wbwx2fadm1kp158qgjp") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.10 (c (n "fluence") (v "0.2.10") (d (list (d (n "fluence-sdk-macro") (r "=0.2.11") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.11") (d #t) (k 0)))) (h "0hj8kvm9y454y4bvfaldshp6w0nlscwqhffc38j5vyi89gmc90sh") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.12 (c (n "fluence") (v "0.2.12") (d (list (d (n "fluence-sdk-macro") (r "=0.2.12") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.12") (d #t) (k 0)))) (h "071zdid5mqbi04c5fi7d1gnqn8x6lmwgg1msa660gzzhikihcdba") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.13 (c (n "fluence") (v "0.2.13") (d (list (d (n "fluence-sdk-macro") (r "=0.2.13") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.13") (d #t) (k 0)))) (h "0pk1pjr86q9c0mnix25qvgvbs7ygxhjlk0hdy5s8pch0s2jl0gq8") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.13-release.1 (c (n "fluence") (v "0.2.13-release.1") (d (list (d (n "fluence-sdk-macro") (r "^0.2.13-release.1") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.2.13-release.1") (d #t) (k 0)))) (h "1syz9pkdsck4nrri2n61y6qxmmy11fpq7zm03yla5l148qv1jb2m") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.13-release.2 (c (n "fluence") (v "0.2.13-release.2") (d (list (d (n "fluence-sdk-macro") (r "^0.2.13-release.2") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.2.13-release.2") (d #t) (k 0)))) (h "049xsp48mpgy6hl1f5lq6xvak5cs0zdv2lzxyi8mszci9am1hmxr") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.14 (c (n "fluence") (v "0.2.14") (d (list (d (n "fluence-sdk-macro") (r "^0.2.14") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.2.14") (d #t) (k 0)))) (h "0jm5n1vhmhfh445wcbwpqcci7czzyg70746dgr0xqf5znhhvsrwg") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.15 (c (n "fluence") (v "0.2.15") (d (list (d (n "fluence-sdk-macro") (r "^0.2.15") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.2.15") (d #t) (k 0)))) (h "0fkvfxnn39mg42my1ws27hqljz82rkj6aq3918a3yxga3f7g6fql") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.16 (c (n "fluence") (v "0.2.16") (d (list (d (n "fluence-sdk-macro") (r "^0.2.16") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.2.16") (d #t) (k 0)))) (h "0hllz5a0sb1dkjm34l4v7c0rg89mha5ldckfz8l963hs53x9ypg6") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.17 (c (n "fluence") (v "0.2.17") (d (list (d (n "fluence-sdk-macro") (r "^0.2.17") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.2.17") (d #t) (k 0)))) (h "0bc61ljxpzsqnwfq8xf2dy1471c6xsj2s5jnxfiac1js46dp7m3h") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.2.18 (c (n "fluence") (v "0.2.18") (d (list (d (n "fluence-sdk-macro") (r "=0.2.18") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.2.18") (d #t) (k 0)))) (h "095b19pp478yxqk3xaxcqp5r3b5c78y8x5hfka0bsyrd57jabn97") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.3.1 (c (n "fluence") (v "0.3.1") (d (list (d (n "fluence-sdk-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.3.1") (d #t) (k 0)))) (h "0pfy2nkplnhl8cd82dm6rj47mf85r5blb8k49f93jr7fcn6fqwcp") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.3.2 (c (n "fluence") (v "0.3.2") (d (list (d (n "fluence-sdk-macro") (r "^0.3.2") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.3.2") (d #t) (k 0)))) (h "0as33s3zggwmffl746dn407nfl3nd4gqmjikaa7xx5gp3l8afk9k") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.3.3 (c (n "fluence") (v "0.3.3") (d (list (d (n "fluence-sdk-macro") (r "=0.3.3") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.3.3") (d #t) (k 0)))) (h "0k4wxl29sd3kd1iv9yqzirdxwmwy7fv1qmy4cqx8lqmsypn23il4") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.4.0 (c (n "fluence") (v "0.4.0") (d (list (d (n "fluence-sdk-macro") (r "=0.4.0") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.4.0") (d #t) (k 0)))) (h "131lb7kmv37f88x23hlwhalzg03gayz5lg4bama625kf15spww30") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.4.1 (c (n "fluence") (v "0.4.1") (d (list (d (n "fluence-sdk-macro") (r "=0.4.1") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.4.1") (d #t) (k 0)))) (h "0s03dypqiiamc56f2fmqdpg0djb15m0n4jh5ksf8vf9dqmd6nl2s") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.4.2 (c (n "fluence") (v "0.4.2") (d (list (d (n "fluence-sdk-macro") (r "=0.4.2") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.4.2") (d #t) (k 0)))) (h "03jpsb320fv9a5n6nvd32664sl1xyb30s8q1kz2zjy1lhzj8y1ii") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.5.0 (c (n "fluence") (v "0.5.0") (d (list (d (n "fluence-sdk-macro") (r "=0.5.0") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.5.0") (d #t) (k 0)))) (h "1104g92mxc40r3il4kbrsgw7l4mnwv3javyd1qi16zwp59rk2a1l") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.6.0 (c (n "fluence") (v "0.6.0") (d (list (d (n "fce-timestamp-macro") (r "=0.6.1") (d #t) (k 0)) (d (n "fluence-sdk-macro") (r "=0.6.1") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "02mlysy5y3zimwbn5c75w0bfkm8xl5nz4pw07kw01vm2qxm6cyxl") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.6.1 (c (n "fluence") (v "0.6.1") (d (list (d (n "fce-timestamp-macro") (r "=0.6.1") (d #t) (k 0)) (d (n "fluence-sdk-macro") (r "=0.6.1") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ry8wyii4nrdd75b98qhwvg76hq0mj5g1y8kf47d3p1j4j5ggcjl") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.6.2 (c (n "fluence") (v "0.6.2") (d (list (d (n "fce-timestamp-macro") (r "=0.6.1") (d #t) (k 0)) (d (n "fluence-sdk-macro") (r "=0.6.1") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "=0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1y2phyibdvg8y3hn1gfylb43k2chagvnzy9gdwc4afcslva1nwsv") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.6.3 (c (n "fluence") (v "0.6.3") (d (list (d (n "fluence-sdk-main") (r "=0.6.2") (d #t) (k 0)) (d (n "marine-macro") (r "=0.6.2") (d #t) (k 0)) (d (n "marine-timestamp-macro") (r "=0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1lb7mspabazcdw0h946nxh0y52hk3by5dl9ldxxa9hpzn29ggzra") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.6.4 (c (n "fluence") (v "0.6.4") (d (list (d (n "fluence-sdk-main") (r "=0.6.3") (d #t) (k 0)) (d (n "marine-macro") (r "=0.6.3") (d #t) (k 0)) (d (n "marine-timestamp-macro") (r "=0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "033jj6w842mk2fkl72zd9hr15ly819wnlyj4nlhqmpmhb3shavwi") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.6.5 (c (n "fluence") (v "0.6.5") (d (list (d (n "fluence-sdk-main") (r "=0.6.4") (d #t) (k 0)) (d (n "marine-macro") (r "=0.6.4") (d #t) (k 0)) (d (n "marine-timestamp-macro") (r "=0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0mizy0nsb095jik854nr0ffnc783m1hz02saisggibm69nd1a7xx") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.6.6 (c (n "fluence") (v "0.6.6") (d (list (d (n "fluence-sdk-main") (r "=0.6.6") (d #t) (k 0)) (d (n "marine-macro") (r "=0.6.6") (d #t) (k 0)) (d (n "marine-timestamp-macro") (r "=0.6.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0fnqmwdvzkpwirf49fhvdsfpa0znvq51q5a9x2sajjxj1167c7gr") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.6.8 (c (n "fluence") (v "0.6.8") (d (list (d (n "fluence-sdk-main") (r "=0.6.8") (d #t) (k 0)) (d (n "marine-macro") (r "=0.6.8") (d #t) (k 0)) (d (n "marine-timestamp-macro") (r "=0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0hq7cq44651kp6gza72sk3g5ndhskw0dcqidjppazqrvxnxic8d9") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

(define-public crate-fluence-0.6.9 (c (n "fluence") (v "0.6.9") (d (list (d (n "fluence-sdk-main") (r "=0.6.9") (d #t) (k 0)) (d (n "marine-macro") (r "=0.6.9") (d #t) (k 0)) (d (n "marine-timestamp-macro") (r "=0.6.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19nml4rkfd10yxflvvsff7ifqhgy1784gcwx2r6vll8ss4f9xc48") (f (quote (("logger" "fluence-sdk-main/logger") ("debug" "fluence-sdk-main/debug"))))))

