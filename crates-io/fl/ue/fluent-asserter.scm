(define-module (crates-io fl ue fluent-asserter) #:use-module (crates-io))

(define-public crate-fluent-asserter-0.1.0 (c (n "fluent-asserter") (v "0.1.0") (h "18l362f78zy5in1am0qhwf4jwxkv1qxld11hr4h85wyygrg837hx")))

(define-public crate-fluent-asserter-0.1.1 (c (n "fluent-asserter") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1ccmnk7x36capys2ir6zyqw8az23xy3d5r4sp9yph9rdf4mxqwmn")))

(define-public crate-fluent-asserter-0.1.2 (c (n "fluent-asserter") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "15c5y896psrvn6nqjpq6igmsqi68rjsbr1gi77hk0ksxbrvzdfa7")))

(define-public crate-fluent-asserter-0.1.3 (c (n "fluent-asserter") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "08346hind0h7hdj8zq00r7gvylxk71vbaqi3bqdc58pj316b4hqv")))

(define-public crate-fluent-asserter-0.1.4 (c (n "fluent-asserter") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0b39v4fx352vp6023pijhdvxka4rxbzwb12mwzvvs01q3cna3kca")))

(define-public crate-fluent-asserter-0.1.5 (c (n "fluent-asserter") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "02day5wa21ckxbkxzyjwanipmpbvrvga3zrgkk7nr14v1286nqj6")))

(define-public crate-fluent-asserter-0.1.6 (c (n "fluent-asserter") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0yakm77amdh3kndzhv3vkkh7qa8l1xph7nqa2h0lv5g2hqs8wfz4")))

(define-public crate-fluent-asserter-0.1.7 (c (n "fluent-asserter") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1333qgyizclfky6dsaawj4j9m15vn25f4nn8zpj02fia1nm22wfz")))

(define-public crate-fluent-asserter-0.1.8 (c (n "fluent-asserter") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1vixv4knv8f39pl0ab9bfdlmjnwypvd6mmx3gm6v6lpyw8z665xp")))

(define-public crate-fluent-asserter-0.1.9 (c (n "fluent-asserter") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0ha3s1vgd9h6hjpj9c8waqihir9pm15spj5c6yfqqp7i8c92mkb2")))

