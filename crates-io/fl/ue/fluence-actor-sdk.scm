(define-module (crates-io fl ue fluence-actor-sdk) #:use-module (crates-io))

(define-public crate-fluence-actor-sdk-0.1.0 (c (n "fluence-actor-sdk") (v "0.1.0") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "fvm_sdk") (r "^4.1") (d #t) (k 0)) (d (n "fvm_shared") (r "^4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1xfaxzsjb1w4pm1i06ci7jvkpw99d3szdcswfj5v1b8660rlmhbb")))

(define-public crate-fluence-actor-sdk-0.1.1 (c (n "fluence-actor-sdk") (v "0.1.1") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "fvm_sdk") (r "^4.0") (d #t) (k 0)) (d (n "fvm_shared") (r "^4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "11hzqmf2c4sxk9xvl7i555ghrji3crbihjn57k603iqiq5ymbhb1")))

(define-public crate-fluence-actor-sdk-0.1.2 (c (n "fluence-actor-sdk") (v "0.1.2") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "fvm_sdk") (r "^4.1") (d #t) (k 0)) (d (n "fvm_shared") (r "^4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0h13qywgfpyifkc5dpf0jkxlf7k7f1x7nylcc3s31gbp562gvynx")))

