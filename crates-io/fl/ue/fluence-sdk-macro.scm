(define-module (crates-io fl ue fluence-sdk-macro) #:use-module (crates-io))

(define-public crate-fluence-sdk-macro-0.0.8 (c (n "fluence-sdk-macro") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12agyrji9198p6abw0kvgn0rqc9lyqgcqa77mbcwdpldzh5q5mqi") (y #t)))

(define-public crate-fluence-sdk-macro-0.0.9 (c (n "fluence-sdk-macro") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pq3fnwgvhgsckpmifykardmfqal0hvw42a44a389zjax7vk067r") (y #t)))

(define-public crate-fluence-sdk-macro-0.0.10 (c (n "fluence-sdk-macro") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0plh4pvmsv5gwnkvl0j15qh38xiqncfp3xsrhc50gfsmnrczwm7x") (y #t)))

(define-public crate-fluence-sdk-macro-0.0.11 (c (n "fluence-sdk-macro") (v "0.0.11") (d (list (d (n "fluence-sdk-main") (r "= 0.0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "047irhsv7yabj0zvjxsn3fh32d6k2nds10qj0k6vrfanlb8mngbf") (y #t)))

(define-public crate-fluence-sdk-macro-0.0.12 (c (n "fluence-sdk-macro") (v "0.0.12") (d (list (d (n "fluence-sdk-main") (r "= 0.0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xlg5aqk2iymgr7i48n4z8cxm6k7dxsqx4ffxmsq0542l3i83b41") (y #t)))

(define-public crate-fluence-sdk-macro-0.0.13 (c (n "fluence-sdk-macro") (v "0.0.13") (d (list (d (n "fluence-sdk-main") (r "= 0.0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vl249ygapjpw4asjkj9i4hi55rqzrdkh0p5v3bh0rmn9m1h2mi5") (y #t)))

(define-public crate-fluence-sdk-macro-0.1.0 (c (n "fluence-sdk-macro") (v "0.1.0") (d (list (d (n "fluence-sdk-main") (r "= 0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wkslsj12kn15dndmcfd91qa9kwb3ga1jcb84h3lrv0wi38d492h") (y #t)))

(define-public crate-fluence-sdk-macro-0.1.2 (c (n "fluence-sdk-macro") (v "0.1.2") (d (list (d (n "fluence-sdk-main") (r "= 0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fq66pj18g7zgxm7qq4s6b68mn5ic45rm6d2yis60vrfvxlz6jjz") (y #t)))

(define-public crate-fluence-sdk-macro-0.1.3 (c (n "fluence-sdk-macro") (v "0.1.3") (d (list (d (n "fluence-sdk-main") (r "= 0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1brc46c32svsgk6zw8b129k40v0cyb8b4gjz7p7lmpxdn9sifqj3")))

(define-public crate-fluence-sdk-macro-0.1.4 (c (n "fluence-sdk-macro") (v "0.1.4") (d (list (d (n "fluence-sdk-main") (r "= 0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zs1xqj2p0yrmc33ibkj7c6cvg6xkbc3943hxslnpc3aq4xhjsqa")))

(define-public crate-fluence-sdk-macro-0.1.5 (c (n "fluence-sdk-macro") (v "0.1.5") (d (list (d (n "fluence-sdk-main") (r "= 0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v8l9faxy7w8fqbj2jajrwkrcyhwkv55h6n4xcjcl0g1mzwl2p6j")))

(define-public crate-fluence-sdk-macro-0.1.6 (c (n "fluence-sdk-macro") (v "0.1.6") (d (list (d (n "fluence-sdk-main") (r "= 0.1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1wrgpm571qg9h5pd8j52r9qjjjjlczhvmjflizwkglwyi161xqk1")))

(define-public crate-fluence-sdk-macro-0.1.7 (c (n "fluence-sdk-macro") (v "0.1.7") (d (list (d (n "fluence-sdk-main") (r "= 0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1gg6fbkw9mizsgavvlxwn2zmfwr4xkffczaa0anqycli7v8vq7m2")))

(define-public crate-fluence-sdk-macro-0.1.8 (c (n "fluence-sdk-macro") (v "0.1.8") (d (list (d (n "fluence-sdk-main") (r "= 0.1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1anykaixvn9lwzqbsfbim1h4g8vb9ir6452gqy026qn00sqg4vma")))

(define-public crate-fluence-sdk-macro-0.1.10 (c (n "fluence-sdk-macro") (v "0.1.10") (d (list (d (n "fluence-sdk-main") (r "= 0.1.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1iycn3lkk278icb8p8m47mgqr7pnn7520m0dd79qyx97db1j0by0")))

(define-public crate-fluence-sdk-macro-0.1.11 (c (n "fluence-sdk-macro") (v "0.1.11") (d (list (d (n "fluence-sdk-main") (r "= 0.1.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1aj3kvqgri12fnzmc4ngsay0n00siqanml05f50p0c6p8ydly06p")))

(define-public crate-fluence-sdk-macro-0.2.0 (c (n "fluence-sdk-macro") (v "0.2.0") (d (list (d (n "fluence-sdk-wit") (r "=0.2.0") (d #t) (k 0)))) (h "1hfxd8k9yiccw45hyfj95hxwqg8v9wzrn8q47awz7wh20nzzgdg1")))

(define-public crate-fluence-sdk-macro-0.2.1 (c (n "fluence-sdk-macro") (v "0.2.1") (d (list (d (n "fluence-sdk-wit") (r "=0.2.1") (d #t) (k 0)))) (h "11vr728jjk8rwddwzddd4lbisbv8sva38x321cyj307izy4hddqi")))

(define-public crate-fluence-sdk-macro-0.2.2 (c (n "fluence-sdk-macro") (v "0.2.2") (d (list (d (n "fluence-sdk-wit") (r "=0.2.2") (d #t) (k 0)))) (h "1rpbm9wk0fpclswrvzix39dma1vaqc9jpp4cg62bq5waqzf1zji1")))

(define-public crate-fluence-sdk-macro-0.2.3 (c (n "fluence-sdk-macro") (v "0.2.3") (d (list (d (n "fluence-sdk-wit") (r "=0.2.3") (d #t) (k 0)))) (h "06xc5nn9k3sbcdvcnk7p4g844h8qvwcpznzarkllikyw2ydgj9n3")))

(define-public crate-fluence-sdk-macro-0.2.4 (c (n "fluence-sdk-macro") (v "0.2.4") (d (list (d (n "fluence-sdk-wit") (r "=0.2.4") (d #t) (k 0)))) (h "1dlmlmjwndsy7ldi9z9lkcnwii5q15z6q9gj6kpd6b5wl4afp7js")))

(define-public crate-fluence-sdk-macro-0.2.6 (c (n "fluence-sdk-macro") (v "0.2.6") (d (list (d (n "fluence-sdk-wit") (r "=0.2.6") (d #t) (k 0)))) (h "0q9vs8h2cxxbmg0546fg2snxml59jph2v7vza0qs024pf99m97w0")))

(define-public crate-fluence-sdk-macro-0.2.7 (c (n "fluence-sdk-macro") (v "0.2.7") (d (list (d (n "fluence-sdk-wit") (r "=0.2.7") (d #t) (k 0)))) (h "1fw3jyz13a5wln9f1hm56gh666bwv9r6zxani4an8aa1jccnnfx8")))

(define-public crate-fluence-sdk-macro-0.2.8 (c (n "fluence-sdk-macro") (v "0.2.8") (d (list (d (n "fluence-sdk-wit") (r "=0.2.8") (d #t) (k 0)))) (h "1i55670bv34qgp538r1sl10m0izlxv73wgjj7sqnpb3n8c9205sb")))

(define-public crate-fluence-sdk-macro-0.2.9 (c (n "fluence-sdk-macro") (v "0.2.9") (d (list (d (n "fluence-sdk-wit") (r "=0.2.9") (d #t) (k 0)))) (h "1vlh7ssdijm8gbg9p9h6qxwlc0psx2knv3s3x3jb25zqrkqgspya")))

(define-public crate-fluence-sdk-macro-0.2.10 (c (n "fluence-sdk-macro") (v "0.2.10") (d (list (d (n "fluence-sdk-wit") (r "=0.2.10") (d #t) (k 0)))) (h "0qrahkq2kw75n2jsvj3ixsasqyxrcc8ajld1w1gqdipf5j0pyydm")))

(define-public crate-fluence-sdk-macro-0.2.11 (c (n "fluence-sdk-macro") (v "0.2.11") (d (list (d (n "fluence-sdk-wit") (r "=0.2.11") (d #t) (k 0)))) (h "0j5svakkssf8kkpidm7hhhk5805l2zcsswafap8vrq4qm2m9g0rl")))

(define-public crate-fluence-sdk-macro-0.2.12 (c (n "fluence-sdk-macro") (v "0.2.12") (d (list (d (n "fluence-sdk-wit") (r "=0.2.12") (d #t) (k 0)))) (h "0k6crds2x02lb4hdnc2g59sb3aa58277mpg6p8k51m7kxil1ckpl")))

(define-public crate-fluence-sdk-macro-0.2.13 (c (n "fluence-sdk-macro") (v "0.2.13") (d (list (d (n "fluence-sdk-wit") (r "=0.2.13") (d #t) (k 0)))) (h "1407nhf0cgiqa74bxq9ax39gj2vnkh6svk0akc4dgzfdsv7any9g")))

(define-public crate-fluence-sdk-macro-0.2.13-release.0 (c (n "fluence-sdk-macro") (v "0.2.13-release.0") (d (list (d (n "fluence-sdk-wit") (r "^0.2.13-release.0") (d #t) (k 0)))) (h "15a7k27bb3n6fx1pwiz7xyqniprqb3jz7vjifbgrhg38ks7bih8y")))

(define-public crate-fluence-sdk-macro-0.2.13-release.1 (c (n "fluence-sdk-macro") (v "0.2.13-release.1") (d (list (d (n "fluence-sdk-wit") (r "^0.2.13-release.1") (d #t) (k 0)))) (h "1z4096srndikdczq9d8acscpxn7r6w43j6gd3v1x60d2109s19v0")))

(define-public crate-fluence-sdk-macro-0.2.13-release.2 (c (n "fluence-sdk-macro") (v "0.2.13-release.2") (d (list (d (n "fluence-sdk-wit") (r "^0.2.13-release.2") (d #t) (k 0)))) (h "0gd36arc5cg5khdb0bfr37mv28r044c91nr9mg0hnifv4f7qn86j")))

(define-public crate-fluence-sdk-macro-0.2.14 (c (n "fluence-sdk-macro") (v "0.2.14") (d (list (d (n "fluence-sdk-wit") (r "^0.2.14") (d #t) (k 0)))) (h "17ds3wyq8giiyqcg4xicr75445gpwkds9w76p4yjldapspx02s2f")))

(define-public crate-fluence-sdk-macro-0.2.15 (c (n "fluence-sdk-macro") (v "0.2.15") (d (list (d (n "fluence-sdk-wit") (r "^0.2.15") (d #t) (k 0)))) (h "0k3vvrc4wrgc8dalrm7icfh3k60ly140yynldghb3dvwjxw84vja")))

(define-public crate-fluence-sdk-macro-0.2.16 (c (n "fluence-sdk-macro") (v "0.2.16") (d (list (d (n "fluence-sdk-wit") (r "^0.2.16") (d #t) (k 0)))) (h "0cldy9504bv1qfi37biliimbgxjqgkkv8lhzsidh56qrk77h4m2f")))

(define-public crate-fluence-sdk-macro-0.2.17 (c (n "fluence-sdk-macro") (v "0.2.17") (d (list (d (n "fluence-sdk-wit") (r "^0.2.17") (d #t) (k 0)))) (h "1sjf7n1ykww8ypalb15sxw2y5g21bhf2ld00mpmjwkgqdz3ji9j6")))

(define-public crate-fluence-sdk-macro-0.2.18 (c (n "fluence-sdk-macro") (v "0.2.18") (d (list (d (n "fluence-sdk-wit") (r "=0.2.18") (d #t) (k 0)))) (h "1fnly6lqj7y4hmd3zihmk4isnr8m9vcb85wspb8jgy0plrspq6pa")))

(define-public crate-fluence-sdk-macro-0.3.0 (c (n "fluence-sdk-macro") (v "0.3.0") (d (list (d (n "fluence-sdk-wit") (r "=0.3.0") (d #t) (k 0)))) (h "0qb34m1sigl3b1m56n77178cfrx3rwg8wlvk812jx7mxap9f3psc")))

(define-public crate-fluence-sdk-macro-0.3.1 (c (n "fluence-sdk-macro") (v "0.3.1") (d (list (d (n "fluence-sdk-wit") (r "^0.3.1") (d #t) (k 0)))) (h "0zlyn2xn1j98i1v09b0sb0n8i1sgrb4h171p9p7wzrqs1f8mcdck")))

(define-public crate-fluence-sdk-macro-0.3.2 (c (n "fluence-sdk-macro") (v "0.3.2") (d (list (d (n "fluence-sdk-wit") (r "^0.3.2") (d #t) (k 0)))) (h "1n7rzs9vmqv0rcsixlf2fwgcjrn1n26lr4afmc3ja0qsz288fr8q")))

(define-public crate-fluence-sdk-macro-0.3.3 (c (n "fluence-sdk-macro") (v "0.3.3") (d (list (d (n "fluence-sdk-wit") (r "=0.3.3") (d #t) (k 0)))) (h "1ywy4gqfppcs29lvn43dd641bk20bgcdksm5jia9rxd9w8nibvfk")))

(define-public crate-fluence-sdk-macro-0.4.0 (c (n "fluence-sdk-macro") (v "0.4.0") (d (list (d (n "fluence-sdk-wit") (r "=0.4.0") (d #t) (k 0)))) (h "0w2m7y8r6sdpggrdg00r5189zxkb8nyp18aa6drynmndyv6vwr4j")))

(define-public crate-fluence-sdk-macro-0.4.1 (c (n "fluence-sdk-macro") (v "0.4.1") (d (list (d (n "fluence-sdk-wit") (r "=0.4.1") (d #t) (k 0)))) (h "1gwbhmbi8px8r1ags03ddzicyw32n4wd2n8nfmkxmmkw2hgilr64")))

(define-public crate-fluence-sdk-macro-0.4.2 (c (n "fluence-sdk-macro") (v "0.4.2") (d (list (d (n "fluence-sdk-wit") (r "=0.4.2") (d #t) (k 0)))) (h "19lhp1wp7h7akc6jpyjxlalygzbjqdsmxf9izclbhgbw5pf81kkn")))

(define-public crate-fluence-sdk-macro-0.5.0 (c (n "fluence-sdk-macro") (v "0.5.0") (d (list (d (n "fluence-sdk-wit") (r "=0.5.0") (d #t) (k 0)))) (h "0lz6rdz7q5a23njjkxlxa6jf2j6mmd53wbi798nvcn2y5ral1j00")))

(define-public crate-fluence-sdk-macro-0.6.1 (c (n "fluence-sdk-macro") (v "0.6.1") (d (list (d (n "fluence-sdk-wit") (r "=0.6.1") (d #t) (k 0)))) (h "1pkxrmn7s6lqr9yww8ncjl7r15wmfnlqrvr5447bgv0v3zsx4hyb")))

