(define-module (crates-io fl ue fluent-serde) #:use-module (crates-io))

(define-public crate-fluent-serde-0.1.0 (c (n "fluent-serde") (v "0.1.0") (d (list (d (n "fluent") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08jp4aldxr92hxsihyvw4va7r6vzavllrw48mk73iy23p6d7alq0")))

