(define-module (crates-io fl ue fluence-fendermint-syscall) #:use-module (crates-io))

(define-public crate-fluence-fendermint-syscall-0.1.0 (c (n "fluence-fendermint-syscall") (v "0.1.0") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "fvm") (r "^4.1.0") (d #t) (k 0)) (d (n "fvm_shared") (r "^4.1.0") (d #t) (k 0)) (d (n "randomx-rs") (r "^1.3.0") (d #t) (k 0)))) (h "0bdwrqqvxpz1ngnvxikh2422hir69w6xmrwh2mbzgjbsi8js1vbk")))

(define-public crate-fluence-fendermint-syscall-0.1.1 (c (n "fluence-fendermint-syscall") (v "0.1.1") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "fvm") (r "^4.1.0") (d #t) (k 0)) (d (n "fvm_shared") (r "^4.1.0") (d #t) (k 0)) (d (n "randomx-rs") (r "^1.3.0") (d #t) (k 0)))) (h "048qhch6851fczvis7fwrxnzghcsgf9g6gz153mcc11cjxnn5772")))

(define-public crate-fluence-fendermint-syscall-0.1.2 (c (n "fluence-fendermint-syscall") (v "0.1.2") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "fvm") (r "^4.1.0") (k 0)) (d (n "fvm_shared") (r "^4.1.0") (d #t) (k 0)) (d (n "randomx-rs") (r "^1.3.0") (d #t) (k 0)))) (h "1m2wvinbdfgm8f3wd12nqiyi9k31j149n76z1gilh0qk9znjvxa3")))

(define-public crate-fluence-fendermint-syscall-0.1.3 (c (n "fluence-fendermint-syscall") (v "0.1.3") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "fvm") (r "^4.1.0") (k 0)) (d (n "fvm_shared") (r "^4.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "randomx-rust-wrapper") (r "^0.1.1") (d #t) (k 0)))) (h "19x71xniwp3n1wpr99wk1991crbx9sw2dnkwwgf4jrahbhfims8r")))

(define-public crate-fluence-fendermint-syscall-0.1.4 (c (n "fluence-fendermint-syscall") (v "0.1.4") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "fvm") (r "^4.1.0") (k 0)) (d (n "fvm_shared") (r "^4.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "randomx-rust-wrapper") (r "^0.1.1") (d #t) (k 0)))) (h "1hhjqx3hm24xqvx1gw3c6d9k4x537y5gmhrv8w72hx7jqlcjlnbp")))

(define-public crate-fluence-fendermint-syscall-0.1.5 (c (n "fluence-fendermint-syscall") (v "0.1.5") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "fvm") (r "^4.1.0") (k 0)) (d (n "fvm_shared") (r "^4.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "randomx-rust-wrapper") (r "^0.1.3") (d #t) (k 0)))) (h "0dlzl1sw90f2xn2rmqwgjjlymbh6z2vq5pgap3641xfsz5yzrn8g")))

(define-public crate-fluence-fendermint-syscall-0.1.6 (c (n "fluence-fendermint-syscall") (v "0.1.6") (d (list (d (n "ccp-randomx") (r "^0.2.0") (d #t) (k 0)) (d (n "ccp-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "fluence-fendermint-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "fvm") (r "^4.1.0") (k 0)) (d (n "fvm_shared") (r "^4.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "12cnvcaxkxsd0lf4067zxx7bsrli76w7ds9w6blmqgw8pkj1m338")))

