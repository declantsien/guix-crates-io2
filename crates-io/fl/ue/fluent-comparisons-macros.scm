(define-module (crates-io fl ue fluent-comparisons-macros) #:use-module (crates-io))

(define-public crate-fluent-comparisons-macros-0.1.0 (c (n "fluent-comparisons-macros") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0vlj6ic3a0qrz7yqlh6m46i524i5q4xaqcsvyq72il73abbrmjnl")))

(define-public crate-fluent-comparisons-macros-0.2.0 (c (n "fluent-comparisons-macros") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1dmqzhjv3v06smjvjcq8wqzmswmnp5s94nm1hvpdn34ynna8cbx1")))

(define-public crate-fluent-comparisons-macros-0.3.0 (c (n "fluent-comparisons-macros") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1a014nl9g1km15mmjlfyg3b38pg8w7nqlnnx7m4wnb4ggrl9vv6y")))

(define-public crate-fluent-comparisons-macros-1.0.0 (c (n "fluent-comparisons-macros") (v "1.0.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1x7bm3ykga71jxc4131hgwp3mdgpxahhfdvxz4smy4h18y3vf54c")))

