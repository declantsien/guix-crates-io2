(define-module (crates-io fl ue fluence-it-types) #:use-module (crates-io))

(define-public crate-fluence-it-types-0.1.0 (c (n "fluence-it-types") (v "0.1.0") (d (list (d (n "nom") (r "^5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "wast") (r "^8.0") (o #t) (d #t) (k 0)))) (h "04wfnw91mrz5h5nk1znsr98gry9cbsqr02y8k9mics0hn46bhyrs") (f (quote (("impls" "nom" "wast"))))))

(define-public crate-fluence-it-types-0.1.1 (c (n "fluence-it-types") (v "0.1.1") (d (list (d (n "it-to-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "wast") (r "^8.0") (o #t) (d #t) (k 0)))) (h "0g223gf79761v0qrf0nkmqz2r21v3g0gclc2dx0f4d9bwmqcg08v") (f (quote (("impls" "nom" "wast"))))))

(define-public crate-fluence-it-types-0.2.0 (c (n "fluence-it-types") (v "0.2.0") (d (list (d (n "it-to-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "wast") (r "^8.0") (o #t) (d #t) (k 0)))) (h "0kyfqkmrsfigbrzpz0z5gny0wjfpvrsbjaqzg38lh5d7vsqc1hsr") (f (quote (("impls" "nom" "wast"))))))

(define-public crate-fluence-it-types-0.3.0 (c (n "fluence-it-types") (v "0.3.0") (d (list (d (n "it-to-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "wast") (r "^8.0") (o #t) (d #t) (k 0)))) (h "17b1k4v818rr5a2fvhfpfcmdqd7w8p4lqcyjbnpj2m1lafax01jh") (f (quote (("impls" "nom" "wast"))))))

(define-public crate-fluence-it-types-0.3.1 (c (n "fluence-it-types") (v "0.3.1") (d (list (d (n "it-to-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)) (d (n "wast") (r "^8.0") (o #t) (d #t) (k 0)))) (h "0khsh7yp2nglx1n5gdrv7s5z1bsxa6qhg9jh4n3vijh7905nfzq4") (f (quote (("impls" "nom" "wast"))))))

(define-public crate-fluence-it-types-0.3.2 (c (n "fluence-it-types") (v "0.3.2") (d (list (d (n "it-to-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)) (d (n "wast") (r "^8.0") (o #t) (d #t) (k 0)))) (h "0w2by5p779jy05biflxrqjckfcgpzmrljkc4w1vd7asbr1a1pqgl") (f (quote (("impls" "nom" "wast"))))))

(define-public crate-fluence-it-types-0.4.0 (c (n "fluence-it-types") (v "0.4.0") (d (list (d (n "it-to-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)) (d (n "wast") (r "^53.0") (o #t) (d #t) (k 0)))) (h "10qhn6975kdf9si74ybv18lnxibj6y52yysa97l99kly7lvm2yfm") (f (quote (("impls" "nom" "wast"))))))

(define-public crate-fluence-it-types-0.4.1 (c (n "fluence-it-types") (v "0.4.1") (d (list (d (n "it-to-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)) (d (n "wast") (r "^8.0") (o #t) (d #t) (k 0)))) (h "19nak45ql3yxygjfrrqqv49s3vhggdm0zkazxqhms7sdxpaxis25") (f (quote (("impls" "nom" "wast"))))))

