(define-module (crates-io fl ue fluent-impl) #:use-module (crates-io))

(define-public crate-fluent-impl-0.1.1 (c (n "fluent-impl") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11fs6d2g3166yz7divp3c8lxrp797hknmbk2fxrfbqhzmhqvb7kv")))

(define-public crate-fluent-impl-0.1.2 (c (n "fluent-impl") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0babm3aaq3svxwhr3sjwhm94faclhic29i54dvks5xg00wllk8q3")))

(define-public crate-fluent-impl-0.1.3 (c (n "fluent-impl") (v "0.1.3") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nkgbnb05x1yq8jqyayz02hfka8ig2ys4c8i5knfgipbm384i3nh")))

(define-public crate-fluent-impl-0.1.4 (c (n "fluent-impl") (v "0.1.4") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cgkcg7qdkzqzfyp0gm2bmlh9x2fdv95kfxq6brb08pq3j7alnpw")))

(define-public crate-fluent-impl-0.2.0 (c (n "fluent-impl") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nyh5g7ar259g3iy53c2lxgz3ll8lm3gh7hn2sq8c2fhdwwl5z5v")))

