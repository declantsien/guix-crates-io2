(define-module (crates-io fl ue fluentci-types) #:use-module (crates-io))

(define-public crate-fluentci-types-0.1.0 (c (n "fluentci-types") (v "0.1.0") (h "03rwlf9jnr3lm0r2j5y58q9bdz1a40wf0zsh8i86qwwilw5rjk7j")))

(define-public crate-fluentci-types-0.1.1 (c (n "fluentci-types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "181hwx5a16v94j72zfbhzigbsyqninyjswqhp8s9v9hlwffbfrwi")))

(define-public crate-fluentci-types-0.1.2 (c (n "fluentci-types") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "0dr69q60llrhjnvzc94wxkpjqwsich1r0mfv773n22qhcgi518pr")))

(define-public crate-fluentci-types-0.1.3 (c (n "fluentci-types") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "0iz07zik62rn07z1c8n6crr2x56zr14az1d4dwb0d3y4s46vm4km")))

(define-public crate-fluentci-types-0.1.4 (c (n "fluentci-types") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "0jxbibks9895qafxvfqs5yjsqs3aqvkna7xbdnvavj2jzdpc2hz7")))

(define-public crate-fluentci-types-0.1.5 (c (n "fluentci-types") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "1nhbf5l5s4q7v94lff5l88cvl243v1hfjbhlymsh6fh59x6769hj")))

(define-public crate-fluentci-types-0.1.6 (c (n "fluentci-types") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)))) (h "0dfpgrp4f5m4swppr9rs2blni2dqx3fkdpk64cnzqixfy76cr13v")))

(define-public crate-fluentci-types-0.1.7 (c (n "fluentci-types") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)))) (h "1c8b9ca6px8wsi940p106q9ih6hfvmqjchjc4wx4l199zsvncw13")))

