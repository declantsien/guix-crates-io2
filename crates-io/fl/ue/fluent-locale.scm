(define-module (crates-io fl ue fluent-locale) #:use-module (crates-io))

(define-public crate-fluent-locale-0.1.0 (c (n "fluent-locale") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "061jh1mq9qskz7wp8aizk7a59bbib0jp005r8g4v6rl81cd7dj9s")))

(define-public crate-fluent-locale-0.2.0 (c (n "fluent-locale") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1hb463562cyfkxw2jgayshng0fya2hrcbmcbdqxa08lxjnamhla7")))

(define-public crate-fluent-locale-0.3.0 (c (n "fluent-locale") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "166kjv3zfq21ri2g9pxbdgffqjiiw0wk0ymac6y0cvj0zp3ccx38")))

(define-public crate-fluent-locale-0.3.1 (c (n "fluent-locale") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0k38kasghq2vwb8i1zfdall704fdxqdzzfinbkiqazlqpmi36x1n")))

(define-public crate-fluent-locale-0.3.2 (c (n "fluent-locale") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z9d56r6d7sk328ly302v7m3a0bca07azprm1z8g3im0xwn7r0gr")))

(define-public crate-fluent-locale-0.4.0 (c (n "fluent-locale") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zgqglqggcslv9rkr6q0cgl6f0h3pyppjpx5m84q5kzs8l6ghjai")))

(define-public crate-fluent-locale-0.4.1 (c (n "fluent-locale") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00by5i9v9myldqwbvvm4027l15c4bcahnj0n0lws0ls1il7shm2a")))

(define-public crate-fluent-locale-0.5.0 (c (n "fluent-locale") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-locale") (r "^0.1") (d #t) (k 0)))) (h "1zvv4h2qbvf6s2h8x4kgm1gnwr0lnrg7bviglngsailasjlljv68")))

(define-public crate-fluent-locale-0.6.0 (c (n "fluent-locale") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-langid") (r "^0.3.0") (d #t) (k 0)) (d (n "unic-locale") (r "^0.3.0") (d #t) (k 2)))) (h "04r4bc5v3ypjmv78vvzjbkw2a25pn97yjwssys34wryisfq7nr9s")))

(define-public crate-fluent-locale-0.7.0 (c (n "fluent-locale") (v "0.7.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-langid") (r "^0.4") (d #t) (k 0)) (d (n "unic-locale") (r "^0.4") (f (quote ("macros"))) (d #t) (k 2)))) (h "1glpwk9kb6m31q1qcy2j65xd9vijaxkaad0m1nszvwrg1czhicyv")))

(define-public crate-fluent-locale-0.8.0 (c (n "fluent-locale") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-langid") (r "^0.5") (d #t) (k 0)) (d (n "unic-locale") (r "^0.5") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ss6bdzas9bsfrafmad1bjyw2mmfhxvycissgs9c1fhl6lw9wz21")))

(define-public crate-fluent-locale-0.9.0 (c (n "fluent-locale") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-langid") (r "^0.5.2") (d #t) (k 0)) (d (n "unic-langid") (r "^0.5.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "unic-locale") (r "^0.5") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ps6w0z055kar8s2mxqdvia494gm5w3dn3z7846g16nn099p9zr6") (f (quote (("default") ("cldr" "unic-langid/likelysubtags"))))))

(define-public crate-fluent-locale-0.10.0 (c (n "fluent-locale") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-langid") (r "^0.6") (d #t) (k 0)) (d (n "unic-langid") (r "^0.6") (f (quote ("macros"))) (d #t) (k 2)) (d (n "unic-locale") (r "^0.6") (f (quote ("macros"))) (d #t) (k 2)))) (h "1haqynng87psg549yzhf4p56qywzfdz7bm2sf9zcwyb4c1n83hnw") (f (quote (("default") ("cldr" "unic-langid/likelysubtags"))))))

(define-public crate-fluent-locale-0.10.1 (c (n "fluent-locale") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unic-langid") (r "^0.6") (d #t) (k 0)) (d (n "unic-langid") (r "^0.6") (f (quote ("macros"))) (d #t) (k 2)) (d (n "unic-locale") (r "^0.6") (f (quote ("macros"))) (d #t) (k 2)))) (h "15mwvmgbnha7p7mbdld5v3gqh5k2jag9pzsfwrp0z68kj5rjdxjh") (f (quote (("default") ("cldr" "unic-langid/likelysubtags"))))))

