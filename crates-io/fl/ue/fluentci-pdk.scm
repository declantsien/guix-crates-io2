(define-module (crates-io fl ue fluentci-pdk) #:use-module (crates-io))

(define-public crate-fluentci-pdk-0.1.1 (c (n "fluentci-pdk") (v "0.1.1") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "0flsn8y4sqphmcjmxyn5w2cmnsg1ia2gwchpg6nq42fxyi1v812q")))

(define-public crate-fluentci-pdk-0.1.2 (c (n "fluentci-pdk") (v "0.1.2") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "0lahxk1kzzgmk2jcdawyhjm3z0ja2dzd13413r7fkz6nsz4cghm5")))

(define-public crate-fluentci-pdk-0.1.3 (c (n "fluentci-pdk") (v "0.1.3") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "1rprxdjq75wwp3f8q28sbdj4r4if5kar7pd5sla6whxcxvkjf4gc")))

(define-public crate-fluentci-pdk-0.1.4 (c (n "fluentci-pdk") (v "0.1.4") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "1y6q42ak47s74va68mqbdh486h5sh2p7k9vf9m9ab63fpsy8q1p6")))

(define-public crate-fluentci-pdk-0.1.5 (c (n "fluentci-pdk") (v "0.1.5") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "01nqr576423gl5aaiq88dfc92jc5z5cyzd3iiy0ws6c60xxr5pyq")))

(define-public crate-fluentci-pdk-0.1.6 (c (n "fluentci-pdk") (v "0.1.6") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "08zx7s0z7l64gq6qcs3n6h0j2gk02nd9lq4vmdwr0i7h8qsixrr9")))

(define-public crate-fluentci-pdk-0.1.7 (c (n "fluentci-pdk") (v "0.1.7") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "16rvabmj5704wzd9qfifw06r8qz90grr4ckym3q3mwhhv27zlafs")))

(define-public crate-fluentci-pdk-0.1.8 (c (n "fluentci-pdk") (v "0.1.8") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "1rz8imkyjl0fl9m18rbi7g67dj8yhpdjvg14by700rcly3c29m86")))

(define-public crate-fluentci-pdk-0.1.9 (c (n "fluentci-pdk") (v "0.1.9") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "1rxxqagiazjf5jfcb2gc9z2hy0f95xscnwhsh6b3n5qf7wkh902d")))

(define-public crate-fluentci-pdk-0.1.10 (c (n "fluentci-pdk") (v "0.1.10") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "0z771f7zski3h3lhckmvs0a04sr9hhxk29xqz6rlzgl1kkbwr6j2")))

(define-public crate-fluentci-pdk-0.1.11 (c (n "fluentci-pdk") (v "0.1.11") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "1dvsk7rrw0acnfclc9cid65gbn7cxcxf5sgdci63hjv2ca8grpby")))

(define-public crate-fluentci-pdk-0.1.12 (c (n "fluentci-pdk") (v "0.1.12") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "1b21amzr969p17l13azim0ava5a4i2myqfhvqjv9ks3b9qlibcba")))

(define-public crate-fluentci-pdk-0.1.13 (c (n "fluentci-pdk") (v "0.1.13") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "0ax1kcn0pgc2rxhribfidq2gx75jm7njba8lijlj9bjbwcjqgjji")))

(define-public crate-fluentci-pdk-0.2.0 (c (n "fluentci-pdk") (v "0.2.0") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "fluentci-types") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive" "derive"))) (d #t) (k 0)))) (h "131fahk5g57v113cx3f43nyfpp5ihxxrmd4189b75kgqhqn3mzw6")))

