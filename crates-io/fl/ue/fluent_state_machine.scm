(define-module (crates-io fl ue fluent_state_machine) #:use-module (crates-io))

(define-public crate-fluent_state_machine-0.1.0 (c (n "fluent_state_machine") (v "0.1.0") (h "0lj0q5773kdvclqr5gxvgxvksyrngbmg0hrnarh0zw8iijl4k4dn")))

(define-public crate-fluent_state_machine-0.2.0 (c (n "fluent_state_machine") (v "0.2.0") (h "0y9sj78yyqwj6cygsb9vf7if1ckh98dyp9qj1dyq8wl47v696bhs")))

(define-public crate-fluent_state_machine-0.3.0 (c (n "fluent_state_machine") (v "0.3.0") (h "1mql224j9f90nnni9ivcqxa3i8ra55il625wnw1g6im5jmp0ig04")))

(define-public crate-fluent_state_machine-0.4.0 (c (n "fluent_state_machine") (v "0.4.0") (h "0k7kc9lz4aq3rg4hrwxkx3818sb89j1rvka2w7ix689bhwkxri17")))

(define-public crate-fluent_state_machine-0.5.0 (c (n "fluent_state_machine") (v "0.5.0") (h "0h94njy34kqhi7q41g9s5wraiy4zrsbans1clr4d3awmfc3znjpy")))

