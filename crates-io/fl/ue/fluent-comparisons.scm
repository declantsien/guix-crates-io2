(define-module (crates-io fl ue fluent-comparisons) #:use-module (crates-io))

(define-public crate-fluent-comparisons-0.1.0 (c (n "fluent-comparisons") (v "0.1.0") (d (list (d (n "fluent-comparisons-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0das101bc9hkb2ffimqnipx27hp79ja9jf7f9mypidnypfx58pw3")))

(define-public crate-fluent-comparisons-0.1.1 (c (n "fluent-comparisons") (v "0.1.1") (d (list (d (n "fluent-comparisons-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0xbm1as9vvy5mlp7jymxprkjyd222m57q7f0ixlqxs2xq5ln0ma3")))

(define-public crate-fluent-comparisons-0.2.0 (c (n "fluent-comparisons") (v "0.2.0") (d (list (d (n "fluent-comparisons-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qyb0j5ifp03cg5b1l0qfxqq2hk60g2zcnmcg0kvmmsf0iclhm6y")))

(define-public crate-fluent-comparisons-0.2.1 (c (n "fluent-comparisons") (v "0.2.1") (d (list (d (n "fluent-comparisons-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0glsh2ywmna19jyh97m6sgjz3k370af67nv459bqhqsxn441plxl")))

(define-public crate-fluent-comparisons-0.3.0 (c (n "fluent-comparisons") (v "0.3.0") (d (list (d (n "fluent-comparisons-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "04b7jn0s25b2vxz5lwpf23yl13kg0jlpqgsyfa6930mr65mjmq3y")))

(define-public crate-fluent-comparisons-0.3.1 (c (n "fluent-comparisons") (v "0.3.1") (d (list (d (n "fluent-comparisons-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0bs5q5sffys0q1c5nia4h831cbv2ixgqhv8hv9z4vlyfk0ivqp5k")))

(define-public crate-fluent-comparisons-1.0.0 (c (n "fluent-comparisons") (v "1.0.0") (d (list (d (n "fluent-comparisons-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0awhq4g89hzl4ya7i914dafsbgchw5zhc2wpns5rdz98imj518b6")))

(define-public crate-fluent-comparisons-1.0.1 (c (n "fluent-comparisons") (v "1.0.1") (d (list (d (n "fluent-comparisons-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0h044a8gdmvmjnk69hj21h4simalkjb3zaazv45cj0dc0m7xrfr6")))

