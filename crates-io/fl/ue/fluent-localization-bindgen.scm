(define-module (crates-io fl ue fluent-localization-bindgen) #:use-module (crates-io))

(define-public crate-fluent-localization-bindgen-1.0.0 (c (n "fluent-localization-bindgen") (v "1.0.0") (d (list (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-localization-loader") (r "^1.0") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "075j03ivhbmw8qnkdg69dz0m7zj89ca0q0in41rgwxirbyyabqyh")))

(define-public crate-fluent-localization-bindgen-1.0.1 (c (n "fluent-localization-bindgen") (v "1.0.1") (d (list (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-localization-loader") (r "^1.0") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mqqqb71ihww690gmrql8lvvrq0vzpbqp95b6q30dl0333bcbqvb")))

(define-public crate-fluent-localization-bindgen-1.0.2 (c (n "fluent-localization-bindgen") (v "1.0.2") (d (list (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-localization-loader") (r "^1.0") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "11lgd7rmhfqpx5m2zws7xycknxlz702hvr19y5j5d9hvxbmm3h0f")))

(define-public crate-fluent-localization-bindgen-1.0.3 (c (n "fluent-localization-bindgen") (v "1.0.3") (d (list (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-localization-loader") (r "^1.0.3") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1qnklnznfv1qlyllyxdad8yd2kqq2wjprc454j9y6lzqr3nnzjgn")))

(define-public crate-fluent-localization-bindgen-1.0.4 (c (n "fluent-localization-bindgen") (v "1.0.4") (d (list (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-localization-loader") (r "^1.0") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "103jm2df2n6bmk5nwzszsic57wsm3h8zxmh7ms1yfjdm86mnjy17")))

