(define-module (crates-io fl ue fluent-localization-loader) #:use-module (crates-io))

(define-public crate-fluent-localization-loader-1.0.0 (c (n "fluent-localization-loader") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "intl-memoizer") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "1bqxp4z9gggcvkw5smxwhc7bw97150cibfhmm8qg5zhky4sympw6")))

(define-public crate-fluent-localization-loader-1.0.2 (c (n "fluent-localization-loader") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "intl-memoizer") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "1sviwlids68xwsxb01j7wkr1v2fmxzizwjv8wlr02558gh0j8pff")))

(define-public crate-fluent-localization-loader-1.0.3 (c (n "fluent-localization-loader") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)) (d (n "intl-memoizer") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "0y9zwf33a9hiv0cgfd499idqjybhqavs1m3in6gv244bra67lijk")))

