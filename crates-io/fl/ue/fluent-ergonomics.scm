(define-module (crates-io fl ue fluent-ergonomics) #:use-module (crates-io))

(define-public crate-fluent-ergonomics-0.1.0 (c (n "fluent-ergonomics") (v "0.1.0") (d (list (d (n "fluent") (r "^0.11") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "1swa3xzzsw8cbhzk92m53jq8yxw2s1i658qcrdai6s9pvqjjamwk")))

(define-public crate-fluent-ergonomics-0.1.1 (c (n "fluent-ergonomics") (v "0.1.1") (d (list (d (n "fluent") (r "^0.11") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "0d0s1zw6wsbfdgd5n8byrbcp3sxpr6akwxwba6fmmvsbr67ag5qr")))

(define-public crate-fluent-ergonomics-0.2.0 (c (n "fluent-ergonomics") (v "0.2.0") (d (list (d (n "fluent") (r "^0.11") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "1rbh2i0c9h2xnm9k0nng3kxqik5cfwk3a3cc300n36y9sh6qw3jl")))

