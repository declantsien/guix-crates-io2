(define-module (crates-io fl ue fluent-uri) #:use-module (crates-io))

(define-public crate-fluent-uri-0.1.0 (c (n "fluent-uri") (v "0.1.0") (d (list (d (n "beef") (r "^0.5.1") (d #t) (k 0)))) (h "0pnf0pdrd5llbqx5b0n4c441lqr1gijxyqcj0ya5vcrvqqsdyd29") (f (quote (("ipv_future")))) (y #t)))

(define-public crate-fluent-uri-0.1.1 (c (n "fluent-uri") (v "0.1.1") (d (list (d (n "beef") (r "^0.5.1") (d #t) (k 0)))) (h "0awdr1f9ml25lr312l43sp1wkz9aimdjcpdky3s58hkakq6p2g5v") (f (quote (("ipv_future")))) (y #t)))

(define-public crate-fluent-uri-0.1.2 (c (n "fluent-uri") (v "0.1.2") (d (list (d (n "beef") (r "^0.5.1") (d #t) (k 0)))) (h "0rm8m3sfzgkkxas497raaabk07rqiaswp5rvq0qi87yr3h2ivq1f") (f (quote (("ipv_future")))) (y #t)))

(define-public crate-fluent-uri-0.1.3 (c (n "fluent-uri") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0iiingw9ng21myrnb05fg51212v97ra42r63750qx7m3hl5wmjl0") (f (quote (("unstable") ("rfc6874bis") ("ipv_future"))))))

(define-public crate-fluent-uri-0.1.4 (c (n "fluent-uri") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "03ah2qajw5l1zbc81kh1n8g7n24mfxbg6vqyv9ixipg1vglh9iqp") (f (quote (("unstable") ("std") ("rfc6874bis") ("ipv_future") ("default" "std"))))))

(define-public crate-fluent-uri-0.2.0-alpha.0 (c (n "fluent-uri") (v "0.2.0-alpha.0") (d (list (d (n "borrow-or-share") (r "^0.1") (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "1g9a9m50n2cplwjv9rwm9hdd6gs14m1c56dn8x5l1mz45kpd9hxc") (f (quote (("std") ("net" "std") ("default" "net" "std"))))))

(define-public crate-fluent-uri-0.2.0-alpha.1 (c (n "fluent-uri") (v "0.2.0-alpha.1") (d (list (d (n "borrow-or-share") (r "^0.2") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0yxsda1qvqiz19vskrgjxkxhk99fkjgw8s011j27sydwykbx638w") (f (quote (("std") ("net") ("default" "net" "std")))) (r "1.77")))

(define-public crate-fluent-uri-0.2.0-alpha.2 (c (n "fluent-uri") (v "0.2.0-alpha.2") (d (list (d (n "borrow-or-share") (r "^0.2") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "1y7rihn60r2jvpjabb5pcl8i8hhcda4mx4f4z1rm8917gbvbnb09") (f (quote (("std") ("net") ("default" "net" "std")))) (r "1.77")))

(define-public crate-fluent-uri-0.2.0-alpha.3 (c (n "fluent-uri") (v "0.2.0-alpha.3") (d (list (d (n "borrow-or-share") (r "^0.2") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "1g105f44w7zlsbxp7jj0gvbg8c7s9fjgz5imjwqy1z3h8mrqinvg") (f (quote (("std") ("net") ("default" "net" "std")))) (r "1.65")))

(define-public crate-fluent-uri-0.2.0-alpha.4 (c (n "fluent-uri") (v "0.2.0-alpha.4") (d (list (d (n "borrow-or-share") (r "^0.2") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0p0cpqyc5bkv7ia4m7y2vkmrb5adci9k2ccnd76hc8agr0dqyz0y") (f (quote (("std") ("net") ("default" "net" "std")))) (r "1.65")))

(define-public crate-fluent-uri-0.2.0-alpha.5 (c (n "fluent-uri") (v "0.2.0-alpha.5") (d (list (d (n "borrow-or-share") (r "^0.2") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "17qk1jxlsrxy4scakgcz4fishq6q95h5501wrxxjqpl89dzl0bc5") (f (quote (("std") ("net") ("default" "net" "std")))) (r "1.65")))

