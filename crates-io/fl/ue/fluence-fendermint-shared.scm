(define-module (crates-io fl ue fluence-fendermint-shared) #:use-module (crates-io))

(define-public crate-fluence-fendermint-shared-0.1.0 (c (n "fluence-fendermint-shared") (v "0.1.0") (h "07sarbdf4v0zaynr4244qk3yf7sdaycv9a9bx66j67hrq0cz0yfp")))

(define-public crate-fluence-fendermint-shared-0.1.1 (c (n "fluence-fendermint-shared") (v "0.1.1") (h "1zwakkiccdixb4j4dr7fljjiwfksd005na2zmcjx4xk06n6bsylc")))

