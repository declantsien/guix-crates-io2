(define-module (crates-io fl ue fluent-assertions) #:use-module (crates-io))

(define-public crate-fluent-assertions-0.1.0 (c (n "fluent-assertions") (v "0.1.0") (h "01321df64ra2ac54waicd6f56d7xbdc3a88gvhfqqq396swvvic7")))

(define-public crate-fluent-assertions-0.2.0 (c (n "fluent-assertions") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13szw82d9gf5irg58v7kzdmcclrx78vwdaz6qiiq2xk81s931wif")))

(define-public crate-fluent-assertions-0.3.0 (c (n "fluent-assertions") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0y6bq8gk0h421l2r22cgh39bbdqb2d4n6pydzkp63hzjs68l6y03")))

