(define-module (crates-io fl ue fluence-spell-distro) #:use-module (crates-io))

(define-public crate-fluence-spell-distro-0.1.0 (c (n "fluence-spell-distro") (v "0.1.0") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)))) (h "08vnl4bwisbcj2fw8p1yg0xpa7l0l020ad0dlc8mjw19vqp1pdgp")))

(define-public crate-fluence-spell-distro-0.1.1 (c (n "fluence-spell-distro") (v "0.1.1") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0808ak9wzcakhsdnyznmm7vb04vfr7r0cnim00w1n23vgi9dx034")))

(define-public crate-fluence-spell-distro-0.2.0 (c (n "fluence-spell-distro") (v "0.2.0") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1wdcbww38f0bg2x8lizaxh0mkx4nr0li00zkhi3cn8xqnp6k00q7")))

(define-public crate-fluence-spell-distro-0.3.0 (c (n "fluence-spell-distro") (v "0.3.0") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1sbfb7rgxrs2arhzpqqj9gsfswdcrs8n6jsjpb78cd3ncvwrn276")))

(define-public crate-fluence-spell-distro-0.3.1 (c (n "fluence-spell-distro") (v "0.3.1") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1v0ghki4k932n2cwf1c7p3db4rkb3j06zlw41wjzyx9x0j0nsms2")))

(define-public crate-fluence-spell-distro-0.3.2 (c (n "fluence-spell-distro") (v "0.3.2") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1h1sfla157fsasq453h9da6xwlhyvd76nb3rbc0crlwwflsqjg36")))

(define-public crate-fluence-spell-distro-0.3.3 (c (n "fluence-spell-distro") (v "0.3.3") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "08s1iqm45ll0inmwnslabaxvf4gyc8xjva7ygmfmsxdszzk3lzsk")))

(define-public crate-fluence-spell-distro-0.3.4 (c (n "fluence-spell-distro") (v "0.3.4") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "070rc392784sx8csqgyclp5dj0yx8zv40834nmcw9z11iawnqqp3")))

(define-public crate-fluence-spell-distro-0.3.5 (c (n "fluence-spell-distro") (v "0.3.5") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1lywg1lw5wcxsvwp8kqwrdxzcay1fw65g2m4gymh65ysshj31szd")))

(define-public crate-fluence-spell-distro-0.3.6 (c (n "fluence-spell-distro") (v "0.3.6") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1qznbgp8057s0hl8yq0xq970xxdvbvvwbj3r1n6p67ci0k9g9w1w")))

(define-public crate-fluence-spell-distro-0.4.0 (c (n "fluence-spell-distro") (v "0.4.0") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1ygza65f3llqympy1dqdg3jb5y47p3baf73q7b6sg68jaxx23501")))

(define-public crate-fluence-spell-distro-0.5.0 (c (n "fluence-spell-distro") (v "0.5.0") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "157593mal4jnr72q2988v4hqbcil297nb1g4fys4lpw5xyqix4h9")))

(define-public crate-fluence-spell-distro-0.5.1 (c (n "fluence-spell-distro") (v "0.5.1") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0l4lnm6p2i6anhifp7jf50xy0qacc90xlw8h4gh6sdvsxalir47p")))

(define-public crate-fluence-spell-distro-0.5.2 (c (n "fluence-spell-distro") (v "0.5.2") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1skm7gdkg28nbdw7l6s5ssj22l2bx8xr8jdnlvmwzmfagwdjic43")))

(define-public crate-fluence-spell-distro-0.5.3 (c (n "fluence-spell-distro") (v "0.5.3") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "18kylm4cd1n9993bnbcfgq8bzci62920n268wpbsypq5hw0r4jy3")))

(define-public crate-fluence-spell-distro-0.5.4 (c (n "fluence-spell-distro") (v "0.5.4") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1fkri9q2ysl8fn6ym92k1ff1xrzhv07ldypfz2s76l9gzz99xc91")))

(define-public crate-fluence-spell-distro-0.5.6 (c (n "fluence-spell-distro") (v "0.5.6") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1bnsas3g0vz9r6yj798zzr528gdbdbda64m943if7wfxf9kfbpjq")))

(define-public crate-fluence-spell-distro-0.5.7 (c (n "fluence-spell-distro") (v "0.5.7") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "11v02cvc0a8v7aw1ba82yiwnhmsmihba320jvr6yi3yg2xnb3npr")))

(define-public crate-fluence-spell-distro-0.5.8 (c (n "fluence-spell-distro") (v "0.5.8") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1p652wg8sg9xvpygvnspjpqamhm30wygjq1k6phky7vz7gs15897")))

(define-public crate-fluence-spell-distro-0.5.9 (c (n "fluence-spell-distro") (v "0.5.9") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0456bdpgyahbynx6m849fxsj357pw9qpkjr7hmzfybjy194dmi00")))

(define-public crate-fluence-spell-distro-0.5.10 (c (n "fluence-spell-distro") (v "0.5.10") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0zk8andqill7c8bzbnbm5r37vql99bjxjm151cjm9il8xs1mcb3k")))

(define-public crate-fluence-spell-distro-0.5.11 (c (n "fluence-spell-distro") (v "0.5.11") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "15cywyvyzqn0v5666z5y71bvpl6haqa1nwc685i6gjy45z93j136")))

(define-public crate-fluence-spell-distro-0.5.12 (c (n "fluence-spell-distro") (v "0.5.12") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "05p4jy100n26ha89bv46x4civcvlq9i2bh9gf9qlb136v8n5ql68")))

(define-public crate-fluence-spell-distro-0.5.13 (c (n "fluence-spell-distro") (v "0.5.13") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "09sk3kcc5305ahl5d2xzqxdq3jj8ddccrj5lfj2xnia7jql1bx8i")))

(define-public crate-fluence-spell-distro-0.5.14 (c (n "fluence-spell-distro") (v "0.5.14") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "07x15ly13nxilj7dr3ld4hbm4yfkf6qbydp0cwcl1mzh40j1jhcs")))

(define-public crate-fluence-spell-distro-0.5.15 (c (n "fluence-spell-distro") (v "0.5.15") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1md847hn9ggh36f7clpxry2b944l86hsx3jx90d2d2k775kj0bpg")))

(define-public crate-fluence-spell-distro-0.5.16 (c (n "fluence-spell-distro") (v "0.5.16") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1sbrpvk82arpasi1vg21rpg9fwzgqv93blpzfz5j0r0y3k1fxnh8")))

(define-public crate-fluence-spell-distro-0.5.17 (c (n "fluence-spell-distro") (v "0.5.17") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0l396y59c0i1ii2lz5fs7bai9l3p9908jk5gg393fa82ms2yzxg6")))

(define-public crate-fluence-spell-distro-0.5.18 (c (n "fluence-spell-distro") (v "0.5.18") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0wipvar5z7q3cjgf4yaa6jqc0y3jr4bfwv5372h6kv5vd2jgqkhp")))

(define-public crate-fluence-spell-distro-0.5.20 (c (n "fluence-spell-distro") (v "0.5.20") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "123swavpp5cp3d5f1apnm7qbmmqb8bpy114yr9w6d9rm1yvzaplk")))

(define-public crate-fluence-spell-distro-0.5.21 (c (n "fluence-spell-distro") (v "0.5.21") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "02f712n61zsz2hl8fdvn5gy2pzwckms8a5dya272s5s4372dgm2m")))

(define-public crate-fluence-spell-distro-0.5.22 (c (n "fluence-spell-distro") (v "0.5.22") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1nyxzrvp2aqbhrh07yx1d5zkng3ijhh2cvqrg83h6d12qgwv6b44")))

(define-public crate-fluence-spell-distro-0.5.24 (c (n "fluence-spell-distro") (v "0.5.24") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "15f8py5hybdy9bbmclwqs5hjazjaip11ix70cqjh3bgllvll00cl")))

(define-public crate-fluence-spell-distro-0.5.25 (c (n "fluence-spell-distro") (v "0.5.25") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "151yq49mwc19a1w2m8g6ds02v2wqwnj9kg6658pv8dkrp8aqknx9")))

(define-public crate-fluence-spell-distro-0.5.26 (c (n "fluence-spell-distro") (v "0.5.26") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1jsl390az3s91nkawx8di4lyazswnf8abhx7z50q7wj8rhim9hsb")))

(define-public crate-fluence-spell-distro-0.5.27 (c (n "fluence-spell-distro") (v "0.5.27") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1lvlblp7fv6kd522z2dir1lyp6wy1p9gif4dcknan4nd2sj3ki4x")))

(define-public crate-fluence-spell-distro-0.5.28 (c (n "fluence-spell-distro") (v "0.5.28") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "14i7xsjkz813iv3cgy9gl8xnn62a7rvkswrm06nzj7lbjq7pha4i")))

(define-public crate-fluence-spell-distro-0.5.29 (c (n "fluence-spell-distro") (v "0.5.29") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "10nhk6qlg18a5vpwhy6ir37ilrp3n2l8xpzyqvjf317br2pl97m4")))

(define-public crate-fluence-spell-distro-0.5.30 (c (n "fluence-spell-distro") (v "0.5.30") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "166b0w6yx5zxh6n4c7mcvi7alm5r5gf100ir3cmgr3xfdvcmd3df")))

(define-public crate-fluence-spell-distro-0.5.31 (c (n "fluence-spell-distro") (v "0.5.31") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0bnnfn85j7d8xh23ypm6vbrk5yblv5fczwhdjqlva45xjfcp1m74")))

(define-public crate-fluence-spell-distro-0.5.32 (c (n "fluence-spell-distro") (v "0.5.32") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0bq5md7qvz6q18mhhsv5nzvapdndx02f7hc5fw29ifi4q33v72x3")))

(define-public crate-fluence-spell-distro-0.5.33 (c (n "fluence-spell-distro") (v "0.5.33") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1g7cj7ssg8dad62g8l8mvb492wlb83v4n0njl88jqvw6nzzz3dbd")))

(define-public crate-fluence-spell-distro-0.6.0 (c (n "fluence-spell-distro") (v "0.6.0") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0y1vcs4z1a9kxv0fz29ak3yv59bh76ni58g4jg7qjdj9rvhk4i1w")))

(define-public crate-fluence-spell-distro-0.6.1 (c (n "fluence-spell-distro") (v "0.6.1") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0wxsaq768qyrdmfa4iw2pgw4zlai12vw36rwfr6rjx15i70c79m8")))

(define-public crate-fluence-spell-distro-0.6.2 (c (n "fluence-spell-distro") (v "0.6.2") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0v4fhy8bg78lah1ch06iilv98ghxraxf5krzixc5wxxshapnv2ss")))

(define-public crate-fluence-spell-distro-0.6.3 (c (n "fluence-spell-distro") (v "0.6.3") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "17ddcfrkcnajy2hr09yi2sivi6f24yc8mhcbqv8bs3mqz8a4bwsp")))

(define-public crate-fluence-spell-distro-0.6.4 (c (n "fluence-spell-distro") (v "0.6.4") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1r6l1mdqpqh5d8wqa5nszwfg2nlisfv7bmclki2b5a1hbfa96r31")))

(define-public crate-fluence-spell-distro-0.6.5 (c (n "fluence-spell-distro") (v "0.6.5") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "09q91j7giqf7bp3z1hdw8qxvq2gx60czgladayih0fl6bawgc728")))

(define-public crate-fluence-spell-distro-0.6.6 (c (n "fluence-spell-distro") (v "0.6.6") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0kqdp5myqkzfhdbpnvy49mpwz70w4scwz3khr37j5sgfwm28v5l7")))

(define-public crate-fluence-spell-distro-0.6.7 (c (n "fluence-spell-distro") (v "0.6.7") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "01y69l8jk7wfgmrh7bxcd9jzdfv3cxw25vyhpr4cldp5ywq56yz4")))

(define-public crate-fluence-spell-distro-0.6.8 (c (n "fluence-spell-distro") (v "0.6.8") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "08a83qy9bamqk8wp78r6ygimzpkajj3fazr67lycsy5kbbg362b7")))

(define-public crate-fluence-spell-distro-0.6.9 (c (n "fluence-spell-distro") (v "0.6.9") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0684i8n42lnl8isvqsqrbb68jwljz17c84w705piz94m67c202cy")))

(define-public crate-fluence-spell-distro-0.6.10 (c (n "fluence-spell-distro") (v "0.6.10") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1gsjxxmp703z6bwms5zxfyrafkf4kqn43d7dvizw1bbxp0qyjrzh")))

(define-public crate-fluence-spell-distro-0.7.0 (c (n "fluence-spell-distro") (v "0.7.0") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1qdyfcf0l6lj1gyp0pqfv0cl1w7w00w2if6i0sgamybg8hcrk0ph")))

(define-public crate-fluence-spell-distro-0.7.1 (c (n "fluence-spell-distro") (v "0.7.1") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0xr5gybmhl5jjr9yk0k0wj545hl0km3y0gx61shwxfxaqixisz31")))

(define-public crate-fluence-spell-distro-0.7.2 (c (n "fluence-spell-distro") (v "0.7.2") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1fvkjsspygkp41j2rxl4y4dqy3f98dnb9jhnlr6xrc336xkhrazw")))

(define-public crate-fluence-spell-distro-0.7.3 (c (n "fluence-spell-distro") (v "0.7.3") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1a7ij4mlncp351xjjij1pxq92iv9bn5vwrcjyjjrickl4qcnyajd")))

(define-public crate-fluence-spell-distro-0.7.4 (c (n "fluence-spell-distro") (v "0.7.4") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "18a6maz54wz93yqb4i0yn3gljjqyqm7dw37wmxc2485f83msdxxl")))

(define-public crate-fluence-spell-distro-0.7.5 (c (n "fluence-spell-distro") (v "0.7.5") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "19w19qy0jqxnbj8b63nrgh38lxfbs8v25ksa2iyq8q6bb0vq6lhg")))

(define-public crate-fluence-spell-distro-0.7.6 (c (n "fluence-spell-distro") (v "0.7.6") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0cgzgdgcfx2z6fd1h78lj9i1xbbdmb4r7dpyg7qbw3ycqrx85ayn")))

