(define-module (crates-io fl ue fluere-config) #:use-module (crates-io))

(define-public crate-fluere-config-0.1.0 (c (n "fluere-config") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1p1wmkggarcnyg851z9s11lavw1k6k4kn71lk3x1kxgnqx128f8i")))

(define-public crate-fluere-config-0.1.2 (c (n "fluere-config") (v "0.1.2") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "15bxcxbg1c18jr9hkb64i2xvfmxvqz27imnsb51l0vbkdgi8c3pr")))

(define-public crate-fluere-config-0.1.3 (c (n "fluere-config") (v "0.1.3") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1lxyqcwij35gqhg01vi3w3dzvx0vf277lchs6v2i81922lqd2cci")))

(define-public crate-fluere-config-0.2.0 (c (n "fluere-config") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0lzr5vnsjvdfxfr2yhb7zzn0h98qxxj7xd4n7dc58qwa9yjpj0ia") (s 2) (e (quote (("log" "dep:log"))))))

