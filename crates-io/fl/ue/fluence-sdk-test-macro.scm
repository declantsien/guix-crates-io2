(define-module (crates-io fl ue fluence-sdk-test-macro) #:use-module (crates-io))

(define-public crate-fluence-sdk-test-macro-0.1.0 (c (n "fluence-sdk-test-macro") (v "0.1.0") (d (list (d (n "fluence-sdk-test-macro-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "1i146z3ylgb02l4iv3mpfvb5al9nvml9lxyp6f8cbg5ijxmlzgml")))

(define-public crate-fluence-sdk-test-macro-0.1.1 (c (n "fluence-sdk-test-macro") (v "0.1.1") (d (list (d (n "fluence-sdk-test-macro-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "1im4m50k27pj73pxl0bnvmd3kpq5ig39r5l1z0gws6m99bvb3pqn")))

(define-public crate-fluence-sdk-test-macro-0.1.2 (c (n "fluence-sdk-test-macro") (v "0.1.2") (d (list (d (n "fluence-sdk-test-macro-impl") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "17simrbdd21325nhxsa31iy42jdjf7ppi75mzkaz2m128iqfbym0")))

(define-public crate-fluence-sdk-test-macro-0.1.3 (c (n "fluence-sdk-test-macro") (v "0.1.3") (d (list (d (n "fluence-sdk-test-macro-impl") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "04fz393rx46zwgyqcsfk34wcn2mxyng6irf835w81xwm1ai2jiz6")))

(define-public crate-fluence-sdk-test-macro-0.1.4 (c (n "fluence-sdk-test-macro") (v "0.1.4") (d (list (d (n "fluence-sdk-test-macro-impl") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "0axnpbjxp5wz0vjql9izbklclzxz13lr6yhkkzd4jn2ccgpm7j8c")))

(define-public crate-fluence-sdk-test-macro-0.1.5 (c (n "fluence-sdk-test-macro") (v "0.1.5") (d (list (d (n "fluence-sdk-test-macro-impl") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "1adxdndyyx3ziwzd1495k1kpbfc885xhy98vvakvyr0f109d55g3")))

