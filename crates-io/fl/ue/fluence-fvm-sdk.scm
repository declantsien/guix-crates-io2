(define-module (crates-io fl ue fluence-fvm-sdk) #:use-module (crates-io))

(define-public crate-fluence-fvm-sdk-0.1.0 (c (n "fluence-fvm-sdk") (v "0.1.0") (d (list (d (n "fluence-fendermint-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "fvm_sdk") (r "^4.1") (d #t) (k 0)) (d (n "fvm_shared") (r "^4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "16j4dc4bn1xpir20j3px9fcml14490niifrvk0mdgqr8kaiv5y3x")))

