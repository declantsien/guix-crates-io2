(define-module (crates-io fl ue fluence-spell-dtos) #:use-module (crates-io))

(define-public crate-fluence-spell-dtos-0.1.0 (c (n "fluence-spell-dtos") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1q75kkzfi9086wav8zy4ki9r1x0a47szan6sbw1lq2vzvdfdsccb")))

(define-public crate-fluence-spell-dtos-0.1.3 (c (n "fluence-spell-dtos") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "007s0x87nj9nnx4pqjffiqjbdmd7i6afwnh35qccr9lrymqfi0fw")))

(define-public crate-fluence-spell-dtos-0.1.4 (c (n "fluence-spell-dtos") (v "0.1.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0y4lyvwf2wpnvziwi4v1lwlfmcq9rxbh63yw0hzdfxca724n1gry")))

(define-public crate-fluence-spell-dtos-0.3.1 (c (n "fluence-spell-dtos") (v "0.3.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1bx2575c3q98g1if1b6ip38pfw5v6nl2ylnvnhxrsv58dv61sqrl")))

(define-public crate-fluence-spell-dtos-0.3.2 (c (n "fluence-spell-dtos") (v "0.3.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1w1daraak7yk1i75a5643bv8v5wavvy12w1k2gc8ydyr4d15087l")))

(define-public crate-fluence-spell-dtos-0.3.3 (c (n "fluence-spell-dtos") (v "0.3.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0zcz947wmpnbbhr9dkk6as6cfdw6wp1bafj8qhpwjd9jp62ar7j5")))

(define-public crate-fluence-spell-dtos-0.3.4 (c (n "fluence-spell-dtos") (v "0.3.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0z2bdhjvf9b3dvm9sdayyx7aapjpsjlxr3yy3f521szybwi4xsvq")))

(define-public crate-fluence-spell-dtos-0.3.5 (c (n "fluence-spell-dtos") (v "0.3.5") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ljmrvfjazdqlrwpliz3k2knlx6p3dsh5qv7xnjs1dpb13lw6mm6")))

(define-public crate-fluence-spell-dtos-0.3.6 (c (n "fluence-spell-dtos") (v "0.3.6") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1rhqyw0kz4cjhlgys54c0a40icwnb375yd0l542fzr9wj421r35m")))

(define-public crate-fluence-spell-dtos-0.4.0 (c (n "fluence-spell-dtos") (v "0.4.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "084mpl3vbdgsygw1x7x3jilkanlkg38m8lsmjqnx0i7zy6d0avm9")))

(define-public crate-fluence-spell-dtos-0.5.0 (c (n "fluence-spell-dtos") (v "0.5.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1vxr60zq2hbkrx78z17rp7dklc1xdhrxx9zsk8mc1pgp5h7q0w2j")))

(define-public crate-fluence-spell-dtos-0.5.1 (c (n "fluence-spell-dtos") (v "0.5.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1z1m3fahybmwxn1sizwfbb71xl6p06d7b4xdyvwqbr88l84yis0p")))

(define-public crate-fluence-spell-dtos-0.5.2 (c (n "fluence-spell-dtos") (v "0.5.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1dpxybrp8m6aabn8hda2ada3wbb2nbfqan5qm2fiv1vl829bx1q7")))

(define-public crate-fluence-spell-dtos-0.5.3 (c (n "fluence-spell-dtos") (v "0.5.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1zbmvzyjincynrligv5a6m5xkzicqb2y3z1xjgwv7471mmk6x3h9")))

(define-public crate-fluence-spell-dtos-0.5.4 (c (n "fluence-spell-dtos") (v "0.5.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0n61q90lc96yxwngnyvziy4p3mljyhf1smlb97si9ac46nhmdfaf")))

(define-public crate-fluence-spell-dtos-0.5.6 (c (n "fluence-spell-dtos") (v "0.5.6") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1s5va9pq50k3m0xnlxwndg1nn1kc31f3ll358f2x180cpk14n7x9")))

(define-public crate-fluence-spell-dtos-0.5.7 (c (n "fluence-spell-dtos") (v "0.5.7") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "12gz9grplic29qsgzm8yxdi7cnqgdj7ilygcflpivasd5kzxhj5x")))

(define-public crate-fluence-spell-dtos-0.5.8 (c (n "fluence-spell-dtos") (v "0.5.8") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "16c43c8imiq5zvksqvy5lq3jw1ng17i1hzkjm8yj4qjrvdlg96dq")))

(define-public crate-fluence-spell-dtos-0.5.9 (c (n "fluence-spell-dtos") (v "0.5.9") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1l0d61y03q7dl49vvk9zksfx5bkbfgwhl5chp47qi5bnssc020aw")))

(define-public crate-fluence-spell-dtos-0.5.10 (c (n "fluence-spell-dtos") (v "0.5.10") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0nchdj53wa6j65glhwknhki17lx2h61nzq9m6wl8cwlbvvswdpid")))

(define-public crate-fluence-spell-dtos-0.5.11 (c (n "fluence-spell-dtos") (v "0.5.11") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1044rjq030s6r2qd7y490kw3qyv3n72q4dh5vkvpyzan1bhvzc39")))

(define-public crate-fluence-spell-dtos-0.5.12 (c (n "fluence-spell-dtos") (v "0.5.12") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "101s6fdpdlzdnx074khn0xl7j9x386ywyjyhpfrdr895lqhmlg6a")))

(define-public crate-fluence-spell-dtos-0.5.13 (c (n "fluence-spell-dtos") (v "0.5.13") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "17az9drrnsczjnq7iwch4zs1f399x17dhp2fsk68n5rbhwj2w9wg")))

(define-public crate-fluence-spell-dtos-0.5.14 (c (n "fluence-spell-dtos") (v "0.5.14") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0llqwc7daydmxnz4pidy9lvkc7dbbk3w5wg6rzc02fx7li18065v")))

(define-public crate-fluence-spell-dtos-0.5.15 (c (n "fluence-spell-dtos") (v "0.5.15") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0sns0z3jmwrxshflhy5vi6k8xwc66kk9wiaycis2h18l58di67c8")))

(define-public crate-fluence-spell-dtos-0.5.16 (c (n "fluence-spell-dtos") (v "0.5.16") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "03034h36kyw42qh6b2srxkq968ynwkbknpp9qg3y5dwyrq9vqz10")))

(define-public crate-fluence-spell-dtos-0.5.17 (c (n "fluence-spell-dtos") (v "0.5.17") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1i31417zd1n382cj4w3f0i7fhmf2zdzplmslbpgg4bh6alz082wp")))

(define-public crate-fluence-spell-dtos-0.5.18 (c (n "fluence-spell-dtos") (v "0.5.18") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.9.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0rnans660krpzd3mv25q7jna2d2fgl9jqqxmfbky8dg516lraj66")))

(define-public crate-fluence-spell-dtos-0.5.20 (c (n "fluence-spell-dtos") (v "0.5.20") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.9.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "08vglcqihzak4ak2j1dxjac8gjpmc4fjaxccjh79229q8bv12iic")))

(define-public crate-fluence-spell-dtos-0.5.21 (c (n "fluence-spell-dtos") (v "0.5.21") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.9.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "03kc4b41jivji5awqs9s6aii4hcdmrdbqgd6r8ca7zndkkkprpff")))

(define-public crate-fluence-spell-dtos-0.5.22 (c (n "fluence-spell-dtos") (v "0.5.22") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.9.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1cp45i1wcpc5ipx09f7nz94gdyjfs2n9820d0cvfnp0i6hi4wggd")))

(define-public crate-fluence-spell-dtos-0.5.24 (c (n "fluence-spell-dtos") (v "0.5.24") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.9.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "16jf9zhd1xycpwr5a83sc8sqkmqpar9idb139zi5mrhpqyhlpi7l")))

(define-public crate-fluence-spell-dtos-0.5.25 (c (n "fluence-spell-dtos") (v "0.5.25") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.9.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "087w21qmds13rw1rhnmk0i3qx434mbxk0652nfvb4qdg27wpsc5g")))

(define-public crate-fluence-spell-dtos-0.5.26 (c (n "fluence-spell-dtos") (v "0.5.26") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.9.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "05z3wl65ahnydmjyz4agfphk4dzjkv7g8dqshdxfdw9racxf2cf1")))

(define-public crate-fluence-spell-dtos-0.5.27 (c (n "fluence-spell-dtos") (v "0.5.27") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1irkm47vm8rp95na2ml7acn7615kdskgbjdigmiqc9f4smijc2l7")))

(define-public crate-fluence-spell-dtos-0.5.28 (c (n "fluence-spell-dtos") (v "0.5.28") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "07ilz93mgfk8safrnwi8xbdgfg8smb7q788xx68ps03s6njvd2vr")))

(define-public crate-fluence-spell-dtos-0.5.29 (c (n "fluence-spell-dtos") (v "0.5.29") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1xisawiqw34r20wq2mqx1xbfkjyxzqmldvkbqyzs5b8bcqybjhxc")))

(define-public crate-fluence-spell-dtos-0.5.30 (c (n "fluence-spell-dtos") (v "0.5.30") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "05g2vkw9krw1c0vyvsv98sryrsnrzzw41akbvx98hq1qxiiw1g29")))

(define-public crate-fluence-spell-dtos-0.5.31 (c (n "fluence-spell-dtos") (v "0.5.31") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1h14v5521dvs7104imsn7y2y7k3mhzsvk4qmn902my2y51mrb41k")))

(define-public crate-fluence-spell-dtos-0.5.32 (c (n "fluence-spell-dtos") (v "0.5.32") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "11bas5b7w4y01yqvpnj6dd9w3vqmx8lg5n3rzvs24i7maw5cwl5c")))

(define-public crate-fluence-spell-dtos-0.5.33 (c (n "fluence-spell-dtos") (v "0.5.33") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.1") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1f9hvrk18zan483vf8rq3359sikln095g08bkrssi4161ndws4zg")))

(define-public crate-fluence-spell-dtos-0.6.0 (c (n "fluence-spell-dtos") (v "0.6.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1h0yiakknl4njp6jbawy01hrp3km8h2albz3gfazz595w8gcdybb")))

(define-public crate-fluence-spell-dtos-0.6.1 (c (n "fluence-spell-dtos") (v "0.6.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "08ysfq93wxhkjdxwb1n3v7pi05a895fxzd3ddli42vxn82gw4iyp")))

(define-public crate-fluence-spell-dtos-0.6.2 (c (n "fluence-spell-dtos") (v "0.6.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0x87y6ccwrymrxz847al5ai40a3ll2rk79m8lgalj8f8x56z8v0a")))

(define-public crate-fluence-spell-dtos-0.6.3 (c (n "fluence-spell-dtos") (v "0.6.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0y8jj24k2kfm1pc54xzfji3042b0nyafqqflypm0vgk519km9ca2")))

(define-public crate-fluence-spell-dtos-0.6.4 (c (n "fluence-spell-dtos") (v "0.6.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1kxxsq6aw7n1p0ii3jr4581ijl6vrbrk3qn07q8biw40z5jiamw1")))

(define-public crate-fluence-spell-dtos-0.6.5 (c (n "fluence-spell-dtos") (v "0.6.5") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0bif9n66s61q5s82ja29d1xj49hl5k2j8ga03lip8s80y7rmpl80")))

(define-public crate-fluence-spell-dtos-0.6.6 (c (n "fluence-spell-dtos") (v "0.6.6") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "13yc2brkp7kq2796zkjmqcn56l37zdc95ld4kfkrwbdskpc292al")))

(define-public crate-fluence-spell-dtos-0.6.7 (c (n "fluence-spell-dtos") (v "0.6.7") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0kyyi2az3shp6lqk9g6ai4y2b3yi1pi76cklzjbjkny25b7bawmj")))

(define-public crate-fluence-spell-dtos-0.6.8 (c (n "fluence-spell-dtos") (v "0.6.8") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "052nz79qbznndxvxfgy28g53w08ydysvxl1syfn0mfzxlwv94h4d")))

(define-public crate-fluence-spell-dtos-0.6.9 (c (n "fluence-spell-dtos") (v "0.6.9") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0zi83kf3rgzpvmwlmr1i5yis1ndx4fcx4ndp44aw5k20c0kc9nai")))

(define-public crate-fluence-spell-dtos-0.6.10 (c (n "fluence-spell-dtos") (v "0.6.10") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.10.2") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ka1p24ndrfa10mmbkc4rx51f3saq665wmdj9az1r4b512hmslz2")))

(define-public crate-fluence-spell-dtos-0.7.0 (c (n "fluence-spell-dtos") (v "0.7.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.12.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "14r2ywrw2yjwxfk4ms2xx314hm4a3y88l09968701a8jv0vjfr0m")))

(define-public crate-fluence-spell-dtos-0.7.1 (c (n "fluence-spell-dtos") (v "0.7.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.12.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1mvdjw3bk13pb0hv5x535lz44plch4vrxciyh68b7vwdda35gdkl")))

(define-public crate-fluence-spell-dtos-0.7.2 (c (n "fluence-spell-dtos") (v "0.7.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.12.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0bpaqrd57jr55h42kwg5avs4l8vpxc5par4mgaxqsk1rfv6r2yvi")))

(define-public crate-fluence-spell-dtos-0.7.3 (c (n "fluence-spell-dtos") (v "0.7.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.12.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1c6sdvp7900mmqh4wqda0iyn90xscqk84m1c5n7zmzay6mzp48yv")))

(define-public crate-fluence-spell-dtos-0.7.4 (c (n "fluence-spell-dtos") (v "0.7.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.14.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0j41ybplfnmgkzw2j3yfswk61qr2f8bfv86vqcwcin8bmqimmihk")))

(define-public crate-fluence-spell-dtos-0.7.5 (c (n "fluence-spell-dtos") (v "0.7.5") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.14.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0dqhxpbd91px2rgmla3wqziqnz6mz12vylb6wpz52a3y1vzlsyg3")))

(define-public crate-fluence-spell-dtos-0.7.6 (c (n "fluence-spell-dtos") (v "0.7.6") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "marine-rs-sdk") (r "^0.14.0") (d #t) (k 0)) (d (n "marine-sqlite-connector") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "105gr6s9pasljrqxyybjrbwgmczzq30cgqk71454mp9n79mjzipl")))

