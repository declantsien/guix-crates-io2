(define-module (crates-io fl ue fluent-fmt) #:use-module (crates-io))

(define-public crate-fluent-fmt-0.1.0 (c (n "fluent-fmt") (v "0.1.0") (d (list (d (n "arc-rs") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)))) (h "0mlnkplw1jjmcz0gd6a54v8g3rxy389ibjyh5m1k0zkf42hs950d")))

(define-public crate-fluent-fmt-0.1.1 (c (n "fluent-fmt") (v "0.1.1") (d (list (d (n "arc-rs") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.11") (d #t) (k 0)))) (h "1m1cq1nzmq1flkfc7gdkznxhh0xh2cp8gs572r1vb0nys31i2xyl")))

