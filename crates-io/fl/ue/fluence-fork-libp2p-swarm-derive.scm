(define-module (crates-io fl ue fluence-fork-libp2p-swarm-derive) #:use-module (crates-io))

(define-public crate-fluence-fork-libp2p-swarm-derive-0.22.0 (c (n "fluence-fork-libp2p-swarm-derive") (v "0.22.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1xky6bql27104az81cdn3qjaasjxnvm01jpf69q23ac8bixw87py")))

(define-public crate-fluence-fork-libp2p-swarm-derive-0.22.1 (c (n "fluence-fork-libp2p-swarm-derive") (v "0.22.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0lk1m8fh95y16wq9g3kjkqxr341gkhfm02ijrbvbxg3dgz0fma5s")))

