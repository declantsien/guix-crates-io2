(define-module (crates-io fl ue fluent_field_assertions) #:use-module (crates-io))

(define-public crate-fluent_field_assertions-0.1.0 (c (n "fluent_field_assertions") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1hmd5ih316jm6977fkrrza7hh1k6797fn0lp1q6r4kz5z5krfy42")))

(define-public crate-fluent_field_assertions-0.1.1 (c (n "fluent_field_assertions") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1dzn44r0wnk74m2fb173nhmifnfd3lq6ybidlbkywdyqzswg235d")))

(define-public crate-fluent_field_assertions-0.1.2 (c (n "fluent_field_assertions") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1sjm7lx71wiqgpp3c1j4yisbv6l8h11a0a0cyns818q8bkd7a0jp")))

(define-public crate-fluent_field_assertions-0.1.3 (c (n "fluent_field_assertions") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "124wk8ms3vbsdifqamanbahaach1qh16kszfsrymlhg1zcifbv2z")))

(define-public crate-fluent_field_assertions-0.1.4 (c (n "fluent_field_assertions") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0dsdmz8072vpkrnkp80ny4dxgprr268v7bsm63ny7jzv76p309as")))

(define-public crate-fluent_field_assertions-0.2.0 (c (n "fluent_field_assertions") (v "0.2.0") (d (list (d (n "derive-getters") (r "^0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1hkyczsm4vy1hm8rq85l7zaxb8c6fql9w007xl2ds3a15i0b8gif")))

