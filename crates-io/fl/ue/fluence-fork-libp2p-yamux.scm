(define-module (crates-io fl ue fluence-fork-libp2p-yamux) #:use-module (crates-io))

(define-public crate-fluence-fork-libp2p-yamux-0.20.0 (c (n "fluence-fork-libp2p-yamux") (v "0.20.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.20.0") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4.5") (d #t) (k 0)))) (h "108pxgjbarp890hyxis8f998j7agljba25p594zvql6igapz0h22")))

(define-public crate-fluence-fork-libp2p-yamux-0.29.0 (c (n "fluence-fork-libp2p-yamux") (v "0.29.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.26.0") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.0") (d #t) (k 0)))) (h "1kq5l5ib8z51lfdkg4d1naf9835lc4nphipd39qazxpxfzj31ndg")))

(define-public crate-fluence-fork-libp2p-yamux-0.29.1 (c (n "fluence-fork-libp2p-yamux") (v "0.29.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.26.1") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.0") (d #t) (k 0)))) (h "0sjpndrlxf04skawplkarl7ix1zzi84i44c4rw5d8y77y34j5784")))

(define-public crate-fluence-fork-libp2p-yamux-0.30.1 (c (n "fluence-fork-libp2p-yamux") (v "0.30.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.27.1") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.1") (d #t) (k 0)))) (h "04bih5mw0jh2wyqs4l0bzz6yys64bp2m8a8msi607frf3s6qh78a")))

(define-public crate-fluence-fork-libp2p-yamux-0.30.2 (c (n "fluence-fork-libp2p-yamux") (v "0.30.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.27.2") (d #t) (k 0) (p "fluence-fork-libp2p-core")) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.1") (d #t) (k 0)))) (h "1mcjsvas4vn32vi2a75ag7r9cyplfzjibh8hkzxp4p8mwg6ldzc2")))

