(define-module (crates-io fl ue fluent_validator) #:use-module (crates-io))

(define-public crate-fluent_validator-0.1.0 (c (n "fluent_validator") (v "0.1.0") (h "0a8mn022xa9xa63rj0pfriixw4sp7rg6fmb9p83f59j9zlfm38z4") (y #t)))

(define-public crate-fluent_validator-0.1.2 (c (n "fluent_validator") (v "0.1.2") (h "0cm4wvrhm8zv6fc8gv2bdammr74q3pgjc526biiclhadv0cpnwnj") (y #t)))

