(define-module (crates-io fl ue fluent) #:use-module (crates-io))

(define-public crate-fluent-0.0.1 (c (n "fluent") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "term") (r "^0.4.4") (d #t) (k 0)))) (h "1z184z2s65whh0s7z6c8iagfwvdj2nfwz09bixpk18wma32r1d6r")))

(define-public crate-fluent-0.1.0 (c (n "fluent") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "1ki3ihnc63bz805xvn6n51fyi9yhj02w21spp6l3q82rkgf6p4pm")))

(define-public crate-fluent-0.1.1 (c (n "fluent") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "07pdb58kk1vfil28whjb6awc586c4xaic48xmm5967x5gsg06xng") (y #t)))

(define-public crate-fluent-0.1.2 (c (n "fluent") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "1ddkxpi925pga8hyqh0vimwm8pq31piw95y25zbw9jy5kmyg1jdi")))

(define-public crate-fluent-0.2.0 (c (n "fluent") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.3.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "0v73rysipmf8arf79mb5jskjr5d5560nqkg3v0a70psk8rj7m83w")))

(define-public crate-fluent-0.3.0 (c (n "fluent") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.3") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.1") (d #t) (k 0)) (d (n "intl_pluralrules") (r "^0.9") (d #t) (k 0)))) (h "1hqgqxivz3jh3fppkikyf0djf6gspvzfs2r6dhfsvp39qgasfaic")))

(define-public crate-fluent-0.3.1 (c (n "fluent") (v "0.3.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.1") (d #t) (k 0)) (d (n "intl_pluralrules") (r "^0.9") (d #t) (k 0)))) (h "0rjlfq1gfa3kv7yc4rws9xz123qb4b7hd19xmq4flxlj0bsy9skr")))

(define-public crate-fluent-0.4.0 (c (n "fluent") (v "0.4.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.1.1") (d #t) (k 0)) (d (n "intl_pluralrules") (r "^1.0") (d #t) (k 0)))) (h "0npcqsnc8c4wrsczy5qf2cav58alaqg2y69z0gj2gp96lcdpk4rj")))

(define-public crate-fluent-0.4.1 (c (n "fluent") (v "0.4.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.1.1") (d #t) (k 0)) (d (n "intl_pluralrules") (r "^1.0") (d #t) (k 0)))) (h "0vaz4m0zfphkiw4m8iyjgw2dx75p2rmzn3hg4zy347h1bajrdsh3")))

(define-public crate-fluent-0.4.2 (c (n "fluent") (v "0.4.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.1.1") (d #t) (k 0)) (d (n "intl_pluralrules") (r "^1.0") (d #t) (k 0)))) (h "0gznna0xnzy58i5b3cc304sn6cfx0m9kk6vc8349wpwrhac2lwxb")))

(define-public crate-fluent-0.4.3 (c (n "fluent") (v "0.4.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "fluent-syntax") (r "^0.1.1") (d #t) (k 0)) (d (n "intl_pluralrules") (r "^1.0") (d #t) (k 0)))) (h "1zkrw52mrrqafixka3nsylxjkckb95qgabmjpdd7bn50pv4iwqz1")))

(define-public crate-fluent-0.5.0 (c (n "fluent") (v "0.5.0") (d (list (d (n "fluent-bundle") (r "^0.5") (d #t) (k 0)))) (h "03xspahj8c38v3a6jdsmglzh9s5y6qzixym8c69kak9wn2z50lz5")))

(define-public crate-fluent-0.6.0 (c (n "fluent") (v "0.6.0") (d (list (d (n "fluent-bundle") (r "^0.6") (d #t) (k 0)))) (h "0vhkwz56pbplgn6h6zd0v59mgssax7l2s4kp8pbppibi95q77qp5")))

(define-public crate-fluent-0.7.0 (c (n "fluent") (v "0.7.0") (d (list (d (n "fluent-bundle") (r "^0.7.1") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.4.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "03w7msnapj6qaq8l0pazz4zz7f87v13il49cbhnha381xvr215m6")))

(define-public crate-fluent-0.7.1 (c (n "fluent") (v "0.7.1") (d (list (d (n "fluent-bundle") (r "^0.7.1") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.4.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "01x9kq4sd5i43rm8zwvslb4agmly918z076pnsqq8g7y4wy0c6q3")))

(define-public crate-fluent-0.7.2 (c (n "fluent") (v "0.7.2") (d (list (d (n "fluent-bundle") (r "^0.7.2") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.5") (f (quote ("macros"))) (d #t) (k 0)))) (h "0grdf6lw4c5mi4yp3zp5chixx12m0b1ak2w1k0rrn55iygvyvymb") (y #t)))

(define-public crate-fluent-0.8.0 (c (n "fluent") (v "0.8.0") (d (list (d (n "fluent-bundle") (r "^0.8.0") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.6") (f (quote ("macros"))) (d #t) (k 0)))) (h "1w49pzx3vq7bx1imm5sv2vr3mj2zdsa9jbsa0vx4sf5hn9v0lmld")))

(define-public crate-fluent-0.9.0 (c (n "fluent") (v "0.9.0") (d (list (d (n "fluent-bundle") (r "^0.9") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.7") (d #t) (k 0)))) (h "0492g70cikvf8bb3sjvajjqdacyjbwrs9x66m016xi6kzz2dnr8r")))

(define-public crate-fluent-0.9.1 (c (n "fluent") (v "0.9.1") (d (list (d (n "fluent-bundle") (r "^0.9") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.7") (d #t) (k 0)))) (h "0sl806srb00ppqr69c731545qm6yksv7ynmabi76k8v9qskndi9r")))

(define-public crate-fluent-0.10.0 (c (n "fluent") (v "0.10.0") (d (list (d (n "fluent-bundle") (r "^0.10") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "1wsjxjhy49dldpsrqykdxk7kwxa9nnfybm6ghfl2nipfryydk7dw")))

(define-public crate-fluent-0.10.1 (c (n "fluent") (v "0.10.1") (d (list (d (n "fluent-bundle") (r "^0.10.1") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "0jwc931y9p6id26ipi65lqrpirhx9d4xn60jyg7cc2pqrjb9gy3i")))

(define-public crate-fluent-0.10.2 (c (n "fluent") (v "0.10.2") (d (list (d (n "fluent-bundle") (r "^0.10.2") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "12r55jhxl7f0ypx9dlwwvsdxb6zylapg0ywjyfn8lnmkx8zhwwp7")))

(define-public crate-fluent-0.11.0 (c (n "fluent") (v "0.11.0") (d (list (d (n "fluent-bundle") (r "^0.11") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "1gzk7i7q89g5c52j7sryxrflpa1m33iikq79kn86l575w4r7bgly")))

(define-public crate-fluent-0.12.0 (c (n "fluent") (v "0.12.0") (d (list (d (n "fluent-bundle") (r "^0.12") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "07fyxzhc8m97rmpcjxsjx3jawhnka5l7qcws81v8fz9ps4r62frw")))

(define-public crate-fluent-0.13.0 (c (n "fluent") (v "0.13.0") (d (list (d (n "fluent-bundle") (r "^0.13") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "148irji6k12gy5hxfr7yfixp8wndi0f4lqiwrwwazxx8r0n4ybid") (y #t)))

(define-public crate-fluent-0.13.1 (c (n "fluent") (v "0.13.1") (d (list (d (n "fluent-bundle") (r "^0.13") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "1ymx16dqv5sj68byghiizda4vqhax68fg2llgm3y6jk7ggn597pg")))

(define-public crate-fluent-0.14.0 (c (n "fluent") (v "0.14.0") (d (list (d (n "fluent-bundle") (r "^0.14") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "1ci348vrpc6d7ymcbaic0wvpv45wzkrxgk31i4w0vsz460m37cdx")))

(define-public crate-fluent-0.14.1 (c (n "fluent") (v "0.14.1") (d (list (d (n "fluent-bundle") (r "^0.14.1") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "000l5r0lzi6gh3nfjdmfs44bsl6b4cb8amjcpgfy8cpr6ac3bwz4")))

(define-public crate-fluent-0.14.3 (c (n "fluent") (v "0.14.3") (d (list (d (n "fluent-bundle") (r "^0.14.3") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "01kd6bc246k02rc5gq6k470l6h05cgaaiy0jm0rkw5bsy635v2qp")))

(define-public crate-fluent-0.14.4 (c (n "fluent") (v "0.14.4") (d (list (d (n "fluent-bundle") (r "^0.14.4") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "10djhpzn3d6lhid5m1blw42q8f6vas5lwxwsgz3996w2gcqwc2ln")))

(define-public crate-fluent-0.15.0 (c (n "fluent") (v "0.15.0") (d (list (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "16k2grl0khdqax8chvp9ds1pkz4k3ckrzbschkj6c82y01172kdw")))

(define-public crate-fluent-0.16.0 (c (n "fluent") (v "0.16.0") (d (list (d (n "fluent-bundle") (r "^0.15") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "19s7z0gw95qdsp9hhc00xcy11nwhnx93kknjmdvdnna435w97xk1")))

(define-public crate-fluent-0.16.1 (c (n "fluent") (v "0.16.1") (d (list (d (n "fluent-bundle") (r "^0.15.3") (d #t) (k 0)) (d (n "fluent-pseudo") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "0njmdpwz52yjzyp55iik9k6vrixqiy7190d98pk0rgdy0x3n6x5v")))

