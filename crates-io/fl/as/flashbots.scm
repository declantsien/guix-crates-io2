(define-module (crates-io fl as flashbots) #:use-module (crates-io))

(define-public crate-flashbots-0.1.0 (c (n "flashbots") (v "0.1.0") (d (list (d (n "ethers-core") (r "^1.0.2") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "1n76q5m0wn0k5f3092wrjgva1fpv4cs3lh7bp85hp3cg6faxlfzy")))

