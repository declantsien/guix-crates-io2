(define-module (crates-io fl as flash_loan_receiver) #:use-module (crates-io))

(define-public crate-flash_loan_receiver-1.0.0 (c (n "flash_loan_receiver") (v "1.0.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "gemachain-program") (r "^1.8.2") (d #t) (k 0)) (d (n "gpl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1kvcdd3q8j0nzf5jvkjxbxjmb09zri3h3kgpxg9a0k0wg52h5a2y")))

