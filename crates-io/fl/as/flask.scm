(define-module (crates-io fl as flask) #:use-module (crates-io))

(define-public crate-flask-1.0.0 (c (n "flask") (v "1.0.0") (d (list (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "mockito") (r "^0.23.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0ij653p2c6zrlwc5irvygy8dykakia997r8f0il7jkj7pwmi9c2p")))

(define-public crate-flask-1.0.1 (c (n "flask") (v "1.0.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mockito") (r "^0.23.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0bgkx7z6algd14kbvbxd6gz4c653f5f06zf9380dzzs7v972xi2d")))

(define-public crate-flask-1.1.0 (c (n "flask") (v "1.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mockito") (r "^0.23.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "164610xqg81a73nwrzc4zrpbpk744lj3k8936k9jfp22kljm6j4p")))

(define-public crate-flask-2.0.0 (c (n "flask") (v "2.0.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mockito") (r "^0.23.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1gv78la144gc4xcx5jdmvpj6ccnvimnm95294kdkr9lkwahqa0jp")))

(define-public crate-flask-2.0.1 (c (n "flask") (v "2.0.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mockito") (r "^0.25.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "137g5pv181s7nsaq54l1sd6dfqys4danwgccbwswf73dg6jb8mnh")))

(define-public crate-flask-2.0.2 (c (n "flask") (v "2.0.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mockito") (r "^0.25.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1iaamrjrdqy6q3yj5si55mqf4xi3s6c3q4pifdyynark9vs049c7")))

(define-public crate-flask-2.0.3 (c (n "flask") (v "2.0.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mockito") (r "^0.28.0") (d #t) (k 2)) (d (n "nom") (r "^6.0.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0jry4afd69vhl3s425p31fqjd4pnyhwbrya2qm1h8ascnp80d7cf")))

(define-public crate-flask-2.0.4 (c (n "flask") (v "2.0.4") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mockito") (r "^0.31.0") (d #t) (k 2)) (d (n "nom") (r "^6.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1xvb1k308v3v7a1agngfvqvrlkk2km4pf4a6i1w65nb5q6k3a9iv")))

(define-public crate-flask-2.1.0 (c (n "flask") (v "2.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mockito") (r "^1.0.2") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0d5v0w2d4pi30i2537p9lcpdi9amcs3hmqvjzs3cf27gn7h6h6yp")))

(define-public crate-flask-2.1.1 (c (n "flask") (v "2.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mockito") (r "^1.0.2") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "108snr9skgnn7g6znrqnhhmq80dzhdrw44a96l1z1qm5gr2h6d3a")))

