(define-module (crates-io fl as flash-client) #:use-module (crates-io))

(define-public crate-flash-client-0.1.0 (c (n "flash-client") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0h7v5ahcab5nnq330hifzmnrss6wgpjr98ah0f9jarhm3qxfzgka")))

