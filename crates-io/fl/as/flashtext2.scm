(define-module (crates-io fl as flashtext2) #:use-module (crates-io))

(define-public crate-flashtext2-0.0.0 (c (n "flashtext2") (v "0.0.0") (h "0z1zc1ziwf77rk33vvw6pfqmxbyvac6zhwjj5m2xg5xcl1ppp3b6")))

(define-public crate-flashtext2-0.0.1 (c (n "flashtext2") (v "0.0.1") (h "1ba6igbh1hdz1shxckrpyabp97aszlk0hm5z8l5r0i3gwci87n3p")))

(define-public crate-flashtext2-0.0.2 (c (n "flashtext2") (v "0.0.2") (h "1yxw6b2gwxw7ss3076an81wrvapr9bdn65da3m0aj5zcrx5f37f4")))

(define-public crate-flashtext2-0.0.3 (c (n "flashtext2") (v "0.0.3") (h "0d7dbsfpzhdhhcxwfcb3fmznrka5bgf7wkw0r7wcpf00hdps8kbg")))

(define-public crate-flashtext2-0.1.0 (c (n "flashtext2") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "002gw37pavlr3lyysk4i8qyzbdir66qwjppb0ygg3vac8ln90a64")))

