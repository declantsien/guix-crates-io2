(define-module (crates-io fl as flashy64) #:use-module (crates-io))

(define-public crate-flashy64-0.2.0 (c (n "flashy64") (v "0.2.0") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "flashy64-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rppz13070cbb7pbn17csi0r3p364a8ajbhg6ng8ggizhy9c84l3")))

