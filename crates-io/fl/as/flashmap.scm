(define-module (crates-io fl as flashmap) #:use-module (crates-io))

(define-public crate-flashmap-0.0.0 (c (n "flashmap") (v "0.0.0") (d (list (d (n "hashbrown") (r "^0.12.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.6") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)))) (h "12dlr5z20xj71g4wb0s6d5cyl2v1kbs2f48aprfw4q069w65pqi5") (y #t)))

(define-public crate-flashmap-0.0.1-alpha (c (n "flashmap") (v "0.0.1-alpha") (d (list (d (n "hashbrown") (r "^0.12.1") (f (quote ("inline-more"))) (k 0)) (d (n "loom") (r "^0.5.6") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)))) (h "0dd9w76j5jc683khaa2drvwa6pywqrlgg2kfxkw2gg5h45656nqc")))

(define-public crate-flashmap-0.0.2-alpha (c (n "flashmap") (v "0.0.2-alpha") (d (list (d (n "hashbrown") (r "^0.12.1") (f (quote ("inline-more"))) (k 0)) (d (n "loom") (r "^0.5.6") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)))) (h "1cmfgv2hzq9bljpi6v236qx7kj0aq5skzrqw979wpb27x2ibnhx1")))

(define-public crate-flashmap-0.0.3-alpha (c (n "flashmap") (v "0.0.3-alpha") (d (list (d (n "hashbrown") (r "^0.12.1") (f (quote ("inline-more"))) (k 0)) (d (n "loom") (r "^0.5.6") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)))) (h "190b55m1mibxfsws3kvf5wq425s1bpnbmc0h285y4vznzh8vxlia")))

(define-public crate-flashmap-0.0.4-alpha (c (n "flashmap") (v "0.0.4-alpha") (d (list (d (n "hashbrown") (r "^0.12.3") (f (quote ("inline-more"))) (k 0)) (d (n "loom") (r "^0.5.6") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "slab") (r "^0.4.7") (d #t) (k 0)))) (h "1ccindyw5kbp37avflwz47iach37b98vrd877ck0z2ibr1dqz7bq") (f (quote (("nightly") ("default"))))))

(define-public crate-flashmap-0.1.0 (c (n "flashmap") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12.3") (f (quote ("inline-more"))) (k 0)) (d (n "loom") (r "^0.5.6") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "slab") (r "^0.4.7") (d #t) (k 0)))) (h "19qibhz7cas7bbfqpvgq13f0m971mrflh8jd8y963bykb9f2f27i") (f (quote (("nightly") ("default"))))))

