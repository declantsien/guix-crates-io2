(define-module (crates-io fl as flashbang) #:use-module (crates-io))

(define-public crate-flashbang-0.1.0-prealpha (c (n "flashbang") (v "0.1.0-prealpha") (d (list (d (n "argh") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (f (quote ("termination"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "stun") (r "^0.4") (d #t) (k 2)))) (h "0wz6a5p8shfnhvk9k8lbh0r0pjc7ah5m9fgv57yfpqksicgcgr6h") (f (quote (("server_binary" "argh" "ctrlc" "env_logger") ("default" "async_tokio") ("async_tokio" "tokio"))))))

