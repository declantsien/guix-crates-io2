(define-module (crates-io fl as flashkick) #:use-module (crates-io))

(define-public crate-flashkick-0.1.0 (c (n "flashkick") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fxv7ypxhk6gbfs34rzmy18a2c7pjgrfwzs2542n42llc8d9pa8c")))

(define-public crate-flashkick-0.1.1 (c (n "flashkick") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15grk5kn5ig3nx0psga502pp5gqc09l4357fa31pjlznpd93xgp3")))

