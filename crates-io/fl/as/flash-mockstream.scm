(define-module (crates-io fl as flash-mockstream) #:use-module (crates-io))

(define-public crate-flash-mockstream-0.1.0 (c (n "flash-mockstream") (v "0.1.0") (h "0assy1i6kwzw3l6kqdlfyc69gjf5d6706g2cz3hyl93vhbwms3q3")))

(define-public crate-flash-mockstream-0.1.1 (c (n "flash-mockstream") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.8") (d #t) (k 0)))) (h "1phwwgbflpziav9i0izr12qjr9dgsw985qhfa0dh6xp8j4s045d8")))

