(define-module (crates-io fl as flasky) #:use-module (crates-io))

(define-public crate-flasky-0.1.0 (c (n "flasky") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)))) (h "1yhgzly8qynp7d39jnfvjgy0x7vgvil381y4c74jpjkwdzr613p8") (y #t)))

(define-public crate-flasky-0.1.1 (c (n "flasky") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)))) (h "1n6lvay7msv54br2qjbq2qyz7irv1svspwqvw568ifkksm1g70sn") (y #t)))

(define-public crate-flasky-0.1.2 (c (n "flasky") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)))) (h "089z92hkvv0q8nzdsvxl1c09s7spwrpvby7hpq15px43b6ds1wqd")))

(define-public crate-flasky-0.1.3 (c (n "flasky") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)))) (h "0qvb4y74yvjm3accvxz8r7gf6x66kc7ygfwg9ylas0vy0p9izdal")))

(define-public crate-flasky-0.1.4 (c (n "flasky") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)))) (h "17mypn5f2ir3zg5s6l64lgnjj89zz26mc789axddd6j89h27bjhs")))

(define-public crate-flasky-0.1.5 (c (n "flasky") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)))) (h "1wnca3wv0swz43chj0kzfnvqahwafhsal1aw0jx4vdjcari3wij7")))

