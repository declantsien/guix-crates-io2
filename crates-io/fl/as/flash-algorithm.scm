(define-module (crates-io fl as flash-algorithm) #:use-module (crates-io))

(define-public crate-flash-algorithm-0.1.0 (c (n "flash-algorithm") (v "0.1.0") (h "02y42gnvzzi09p5r9i98f3r0ngvg7b41w4kqfnnsal0rxl1pcgir")))

(define-public crate-flash-algorithm-0.2.0 (c (n "flash-algorithm") (v "0.2.0") (h "1n98az3nsyh6rd19hn3irq8v5dwsivvzkyjj9f7j4xzzmnnypkvs")))

(define-public crate-flash-algorithm-0.3.0 (c (n "flash-algorithm") (v "0.3.0") (h "0k8q8r426ni90v4i5xwzia4w61kyrpbiy418czd5yjk81pjwrpvn")))

(define-public crate-flash-algorithm-0.4.0 (c (n "flash-algorithm") (v "0.4.0") (h "0f1idipappcrvvcp78alm1rvwxwniii45bh3h1y7hw8q234q2i3i") (f (quote (("verify") ("panic-handler") ("erase-chip") ("default" "erase-chip" "panic-handler"))))))

