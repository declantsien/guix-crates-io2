(define-module (crates-io fl yc flycatcher-diagnostic) #:use-module (crates-io))

(define-public crate-flycatcher-diagnostic-0.1.0 (c (n "flycatcher-diagnostic") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)))) (h "1z4ngkhl7bpaia9lm9qlkdaqg0y2qrf2yvipvvmdgw5vx0v6qswi")))

(define-public crate-flycatcher-diagnostic-0.1.1 (c (n "flycatcher-diagnostic") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)))) (h "0m7g6f9ri9yhy4izkn1czyi30kr1cjsrcjzkpv4dr5cc15gbm77v")))

