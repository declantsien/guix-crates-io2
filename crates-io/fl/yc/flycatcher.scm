(define-module (crates-io fl yc flycatcher) #:use-module (crates-io))

(define-public crate-flycatcher-0.1.0 (c (n "flycatcher") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "flycatcher-diagnostic") (r "^0.1.0") (d #t) (k 0)) (d (n "flycatcher-parser") (r "^0.1.0") (d #t) (k 0)))) (h "13hi2bbwymmwqz3b5i98qz2q22fngz6h25y5r2b5fzk52cdjvb7x")))

(define-public crate-flycatcher-0.1.1 (c (n "flycatcher") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "flycatcher-diagnostic") (r "^0.1.1") (d #t) (k 0)) (d (n "flycatcher-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "flycatcherc") (r "^0.1.1") (d #t) (k 0)) (d (n "flycatcherc-clif") (r "^0.1.1") (d #t) (k 0)) (d (n "flycatcherc-link") (r "^0.1.1") (d #t) (k 0)))) (h "1hwpxazs00hfc92awy3a6sjmm1x9skdawbjjrblkrfp78ym5r6wn")))

