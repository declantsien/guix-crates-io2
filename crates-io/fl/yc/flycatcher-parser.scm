(define-module (crates-io fl yc flycatcher-parser) #:use-module (crates-io))

(define-public crate-flycatcher-parser-0.1.0 (c (n "flycatcher-parser") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "flycatcher-lexer") (r "^0.1.0") (d #t) (k 0)))) (h "0izddh0w2fxrg6dg8iyi9b0digfikrd0541kl5awc74lq0f0aq92")))

(define-public crate-flycatcher-parser-0.1.1 (c (n "flycatcher-parser") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "flycatcher-lexer") (r "^0.1.1") (d #t) (k 0)))) (h "1c133y9bgkyyazr06vdgjx9l7jyjixaba70jjkadjl5zvdjm0f58")))

