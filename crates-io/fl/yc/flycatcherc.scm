(define-module (crates-io fl yc flycatcherc) #:use-module (crates-io))

(define-public crate-flycatcherc-0.1.1 (c (n "flycatcherc") (v "0.1.1") (d (list (d (n "flycatcher-diagnostic") (r "^0.1.1") (d #t) (k 0)) (d (n "flycatcher-parser") (r "^0.1.1") (d #t) (k 0)))) (h "1zqbq5hfk42d08gmn54k0bx1mbqprzkp2hzd675539qnaax4f9cs")))

