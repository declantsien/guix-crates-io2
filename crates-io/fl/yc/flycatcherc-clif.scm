(define-module (crates-io fl yc flycatcherc-clif) #:use-module (crates-io))

(define-public crate-flycatcherc-clif-0.1.1 (c (n "flycatcherc-clif") (v "0.1.1") (d (list (d (n "cranelift") (r "^0.76.0") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.76.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.76.0") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.76.0") (d #t) (k 0)) (d (n "cranelift-object") (r "^0.76.0") (d #t) (k 0)) (d (n "flycatcherc") (r "^0.1.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "151g9nd9v5wp1fx007h977axj8babi5fgk73jfdmah2vcmdyjx5j")))

