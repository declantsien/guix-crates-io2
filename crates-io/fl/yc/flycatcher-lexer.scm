(define-module (crates-io fl yc flycatcher-lexer) #:use-module (crates-io))

(define-public crate-flycatcher-lexer-0.1.0 (c (n "flycatcher-lexer") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1gbvx1vrrpqwkz2jy466x3qi4kfkmhwvfrygxvgqkjvp0zfq2asn")))

(define-public crate-flycatcher-lexer-0.1.1 (c (n "flycatcher-lexer") (v "0.1.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0wnp0iccwr3gyq305fzajws02sqbp5xfr6lkl37b5yvi1cz2a5bw")))

