(define-module (crates-io fl ap flappyspace) #:use-module (crates-io))

(define-public crate-flappyspace-1.0.0 (c (n "flappyspace") (v "1.0.0") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "bevy_embedded_assets") (r "^0.8.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "winit") (r "^0.28.7") (d #t) (k 0)))) (h "0infpq6l2frs2gbiaagm9rnv2qxqdgfw7xa53b23dbwwxfl4zgw5") (y #t)))

