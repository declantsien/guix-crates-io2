(define-module (crates-io fl ug flugrost) #:use-module (crates-io))

(define-public crate-flugrost-0.1.0 (c (n "flugrost") (v "0.1.0") (h "10zv2anf3rcl1qsjx65yd2ayisk3ikjw7133ngcddl6j6hdbcmr2") (y #t)))

(define-public crate-flugrost-0.0.1 (c (n "flugrost") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1kmz36v8j61jjbgyp9yw4mbpaf1ca9s8hs9cal33i2nx8wl6jdfg")))

