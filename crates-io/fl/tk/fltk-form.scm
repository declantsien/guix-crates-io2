(define-module (crates-io fl tk fltk-form) #:use-module (crates-io))

(define-public crate-fltk-form-0.1.0 (c (n "fltk-form") (v "0.1.0") (d (list (d (n "fltk") (r "^1.2.16") (d #t) (k 0)) (d (n "fltk-form-derive") (r "=0.1.0") (d #t) (k 0)))) (h "1y6qxyb70dpi3x87938zdjwc28v5cbzyvhx9gjyyg3d7zj0n85j6")))

(define-public crate-fltk-form-0.1.1 (c (n "fltk-form") (v "0.1.1") (d (list (d (n "fltk") (r "^1.2.16") (d #t) (k 0)) (d (n "fltk-form-derive") (r "=0.1.0") (d #t) (k 0)))) (h "0bzwdmch0292zhwj1m554hd4ndgn1qngbpzsw4f1xy92gzhy85hd")))

(define-public crate-fltk-form-0.1.2 (c (n "fltk-form") (v "0.1.2") (d (list (d (n "fltk") (r "^1.2.16") (d #t) (k 0)) (d (n "fltk-form-derive") (r "=0.1.1") (d #t) (k 0)))) (h "156n0s2gxp1yx9ajayycbiq4n44fzr9d4v645xffplvsbff13xl1")))

(define-public crate-fltk-form-0.1.3 (c (n "fltk-form") (v "0.1.3") (d (list (d (n "color-maps") (r "^0.1") (d #t) (k 0)) (d (n "fltk") (r "^1.2.16") (d #t) (k 0)) (d (n "fltk-form-derive") (r "=0.1.1") (d #t) (k 0)))) (h "1a5idsgwsn6ph9wqcdamk68l5whi2xkxfjn3r02m0m3w09i3j4v8")))

(define-public crate-fltk-form-0.1.5 (c (n "fltk-form") (v "0.1.5") (d (list (d (n "color-maps") (r "^0.1") (d #t) (k 0)) (d (n "fltk") (r "^1.3.25") (d #t) (k 0)) (d (n "fltk-form-derive") (r "=0.1.2") (d #t) (k 0)))) (h "102y179fpgrvw18y86lhf6k4zjjzf289kr47m3qv08g9p04xqsic")))

(define-public crate-fltk-form-0.2.0 (c (n "fltk-form") (v "0.2.0") (d (list (d (n "color-maps") (r "^0.1") (d #t) (k 0)) (d (n "fltk") (r "^1.4.0") (d #t) (k 0)) (d (n "fltk-form-derive") (r "=0.1.2") (d #t) (k 0)))) (h "0l7yj8va0hsx2538k2dj5l6m0l2wz6770205yi2vpwl9znvp24cz")))

