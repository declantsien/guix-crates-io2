(define-module (crates-io fl tk fltk-flex) #:use-module (crates-io))

(define-public crate-fltk-flex-0.1.0 (c (n "fltk-flex") (v "0.1.0") (d (list (d (n "fltk") (r "^1.1.2") (d #t) (k 0)) (d (n "fltk-derive") (r "^1.1.2") (d #t) (k 0)))) (h "03a9k9hrghwrdkgb5g1da9qafjb75qcznmf7nm01018a5492cs2n")))

(define-public crate-fltk-flex-0.1.1 (c (n "fltk-flex") (v "0.1.1") (d (list (d (n "fltk") (r "^1.1.2") (d #t) (k 0)) (d (n "fltk-derive") (r "^1.1.2") (d #t) (k 0)))) (h "12n7yj1c25ms1i7iwfasjjirk0kbmn0166xfqqkknihiadqbkaya")))

(define-public crate-fltk-flex-0.1.2 (c (n "fltk-flex") (v "0.1.2") (d (list (d (n "fltk") (r "^1.1.10") (d #t) (k 0)) (d (n "fltk-derive") (r "^1.1.10") (d #t) (k 0)))) (h "099jlzn39lw0chwbq3zhrxv5b9p7lwqkm9x3dvj4fibbaja6qsc1")))

(define-public crate-fltk-flex-0.1.3 (c (n "fltk-flex") (v "0.1.3") (d (list (d (n "fltk") (r "^1.1.10") (d #t) (k 0)) (d (n "fltk-derive") (r "^1.1.10") (d #t) (k 0)))) (h "115v8i7zan445h58h8g54rgjva7x5qwf9aail63jlifh41fh72lv")))

(define-public crate-fltk-flex-0.1.4 (c (n "fltk-flex") (v "0.1.4") (d (list (d (n "fltk") (r "^1.1.10") (d #t) (k 0)) (d (n "fltk-derive") (r "^1.1.10") (d #t) (k 0)))) (h "004apgc51nb6sqz4zjc6wbs2h5qz1rm5xmmrgv29p2pq2nrzhyd1")))

(define-public crate-fltk-flex-0.2.0 (c (n "fltk-flex") (v "0.2.0") (d (list (d (n "fltk") (r "^1.2.1") (d #t) (k 0)))) (h "13sraw73kp6wgx46v0m0c7l7wkca826fllazn8746idjsn1282ic")))

(define-public crate-fltk-flex-0.2.1 (c (n "fltk-flex") (v "0.2.1") (d (list (d (n "fltk") (r "^1.2.1") (d #t) (k 0)))) (h "1jnacqdfs1v1i1fjf2phq28x1zfv1v53jqzgps0kcfpmrfv3bjaq")))

