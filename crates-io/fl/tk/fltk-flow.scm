(define-module (crates-io fl tk fltk-flow) #:use-module (crates-io))

(define-public crate-fltk-flow-0.1.0 (c (n "fltk-flow") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.2.18") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.2.18") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.2.18") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0nvbwb6srizvnsszn7da5akpafdba8c6mr176lm3bwiqiqq6m2vf")))

(define-public crate-fltk-flow-0.1.1 (c (n "fltk-flow") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.2.19") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.2.19") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.2.19") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "18wz41400677afl59d0yg8bhrrbyqkyskamq1wi6vv1q7q53jhb0")))

(define-public crate-fltk-flow-0.1.2 (c (n "fltk-flow") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.2.19") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.2.19") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.2.19") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0ndkwxccd7m63n6fxz9nwr97hjy60rsigdyr814hx3k2b49psc3n")))

(define-public crate-fltk-flow-0.1.3 (c (n "fltk-flow") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.2.19") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.2.19") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.2.19") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1k963ad3gwnhhx1nq9rma2a4mmgrvy6y4inw7d9y70psszx7gy4k")))

(define-public crate-fltk-flow-0.1.4 (c (n "fltk-flow") (v "0.1.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.2.25") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.2.25") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.2.25") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0x2ca1fr2mrhnv84ch22l7fn5f6bkpqky3vvccl02mp415zqpv8f")))

(define-public crate-fltk-flow-0.1.5 (c (n "fltk-flow") (v "0.1.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.3.13") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.3.13") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.3.13") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1b9mzm3j7aj3xb1ja54i4n8w2k57qwhp3krj48nz2kmrf1llpfhx")))

(define-public crate-fltk-flow-0.1.6 (c (n "fltk-flow") (v "0.1.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.3.33") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.3.33") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.3.33") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0cxd8slynldk2wr5zw6c3mfzky8v9ys09iwrc38l3p27gi5cbyl4")))

(define-public crate-fltk-flow-0.2.0 (c (n "fltk-flow") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.4.0") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.4.0") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.4.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1drfibsjyw0wq189axlb13grz0j09byf6nf5asnb733zcc55z4d8")))

(define-public crate-fltk-flow-0.2.1 (c (n "fltk-flow") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.4.0") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.4.0") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.4.0") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1d5f8a9ycdjrbgdwyxp3hav6srcp0g29xpk7hhiccgjc1cyfh7x1")))

(define-public crate-fltk-flow-0.2.2 (c (n "fltk-flow") (v "0.2.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.4.11") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.4.11") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.4.11") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1dahs43278ysmkwai3xl7w161hwiaf986jnwin72j7pns9k277pk")))

(define-public crate-fltk-flow-0.2.3 (c (n "fltk-flow") (v "0.2.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.4.17") (d #t) (k 0)) (d (n "fltk-build") (r "^0.1") (d #t) (k 1)) (d (n "fltk-sys") (r "^1.4.17") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.4.17") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "075hzx356n9gw1zgd99azq9gnfy7xsg5plrlap35r2xkppzsiz4z")))

