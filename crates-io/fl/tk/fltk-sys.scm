(define-module (crates-io fl tk fltk-sys) #:use-module (crates-io))

(define-public crate-fltk-sys-0.1.0 (c (n "fltk-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0kiffpb87gsv61jzdnak76a3vpw27nlwgld998qjlr6mgmbycghn")))

(define-public crate-fltk-sys-0.1.2 (c (n "fltk-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1h157qja5kf9lk2qd5iqqbb4gznm7d2irhbiwq3cr3y33w0lc6v8")))

(define-public crate-fltk-sys-0.1.3 (c (n "fltk-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "07jv7y3m3dsz3kr02lk3593sqjl5f2cpb3xxya1cafx3yci3x14c")))

(define-public crate-fltk-sys-0.1.4 (c (n "fltk-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ba9yjl6xzwib478cyms1icz3qb1yy1kvni8rzny5iz24vplv0hd")))

(define-public crate-fltk-sys-0.1.5 (c (n "fltk-sys") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "13y4qkk65cgy20iks3s0q0ww8i2hiksvmq4592sb847hv57mdvz3")))

(define-public crate-fltk-sys-0.1.6 (c (n "fltk-sys") (v "0.1.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0amkqiaj4vn1i6ramfz84n2m1cmhaqnfanwxzk5bji4i5m9zm0yn")))

(define-public crate-fltk-sys-0.1.7 (c (n "fltk-sys") (v "0.1.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "18llc8f0w6r0l5nkw7d2m7vj875cbs8v047kbk14p8zr81647jpy")))

(define-public crate-fltk-sys-0.1.8 (c (n "fltk-sys") (v "0.1.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1v3p3w0jhyx555y5xqmvvdl8n8xihw5wzc57wxf09yy15c2cnlpw")))

(define-public crate-fltk-sys-0.1.9 (c (n "fltk-sys") (v "0.1.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1w7r7xni8mmninlscrzdrar5pl2pcyh228cfzsnadl01564i549k")))

(define-public crate-fltk-sys-0.1.10 (c (n "fltk-sys") (v "0.1.10") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "15pds26krhhwilpx2r58dqn8bzifqa9r417b3jv618v5hrva48sk")))

(define-public crate-fltk-sys-0.1.11 (c (n "fltk-sys") (v "0.1.11") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0phdyqz407hsngqg05hla0bzsya7h0zfbdl69bnxyl2hk0imf7xz")))

(define-public crate-fltk-sys-0.1.12 (c (n "fltk-sys") (v "0.1.12") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1xa1imk0xbzzf0bghi309sakv30fdkyni3zmvl9023wa0378n2cx")))

(define-public crate-fltk-sys-0.1.13 (c (n "fltk-sys") (v "0.1.13") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "048bkazgw8ja7n68wfm3bpwbvnvlr0c7w9fq0fbhad8ca76x7289")))

(define-public crate-fltk-sys-0.1.14 (c (n "fltk-sys") (v "0.1.14") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "01z5918685sjb1hf921d508p9izzgklprmas8nzlv640rp3vrdgx")))

(define-public crate-fltk-sys-0.1.15 (c (n "fltk-sys") (v "0.1.15") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "03dc1r5vk5j4zsyj0lsdckd9g33jzm24kh57pv0ppbskpg2i0b3g")))

(define-public crate-fltk-sys-0.1.16 (c (n "fltk-sys") (v "0.1.16") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1xlf6s7c2pvgc3ph4q818n5vanf3j17n8sldhv99yi1q8hyvakbv")))

(define-public crate-fltk-sys-0.1.17 (c (n "fltk-sys") (v "0.1.17") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "03qzl0x6ljsaawgdy8mwlzinsx5ssc9b1xd1ps8l140k28piv23j")))

(define-public crate-fltk-sys-0.1.18 (c (n "fltk-sys") (v "0.1.18") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0p7i3wza1zjcvijwhlyzkszifmisk6xcmjapz06d9wb8gpzl51rh")))

(define-public crate-fltk-sys-0.1.19 (c (n "fltk-sys") (v "0.1.19") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1hnl95d41hckvniygz6dd5jglr939gpwvpy91xlvf6hrh3bvg39s")))

(define-public crate-fltk-sys-0.1.20 (c (n "fltk-sys") (v "0.1.20") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1cjgnmxyi51v62kaxmk6lmb5n6jm874aylf7wv136prigmy5gn0r")))

(define-public crate-fltk-sys-0.1.21 (c (n "fltk-sys") (v "0.1.21") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0csw2yjb356x59f4iyx4z3rj2mfbhfl7rac7sw8rfg8ashgm9hg4")))

(define-public crate-fltk-sys-0.1.22 (c (n "fltk-sys") (v "0.1.22") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0fvqssxq5lkf6asjjb4s3i1micclifslgzsypxwal7pjgp35qszi")))

(define-public crate-fltk-sys-0.1.23 (c (n "fltk-sys") (v "0.1.23") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "15x0zg9gk2xaqg89r4g806h3750l3hk4shzxq0m9w0va4qhw4z7v")))

(define-public crate-fltk-sys-0.1.24 (c (n "fltk-sys") (v "0.1.24") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1cmhh4vr1hnmvs6j5ycb2cr2cgvy58bff8g4i5hm57fk326m5q0r")))

(define-public crate-fltk-sys-0.1.25 (c (n "fltk-sys") (v "0.1.25") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ch2il2iw70bjv16flvjc2hhjczbm6a6s0kynlx60g6cbzmyr5ny")))

(define-public crate-fltk-sys-0.1.26 (c (n "fltk-sys") (v "0.1.26") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0xi8cwrsg1yf4hm204rv61dykgq8mcbs36gqyp612s12xs4kxr50")))

(define-public crate-fltk-sys-0.2.0 (c (n "fltk-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0k1f1cpmmzwh89fn4r717kl30wsqvfws71app5yaib2yc0x7nm7w") (y #t)))

(define-public crate-fltk-sys-0.2.1 (c (n "fltk-sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ss2k4pymfxmnja9p1m814yms5090ins3bxyh24wj43v9yw67xfq")))

(define-public crate-fltk-sys-0.2.2 (c (n "fltk-sys") (v "0.2.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0cwbx1q0vvzm2ngns5azjw1x7s2n8i4s9vq7yrkgdhjryvchcp2b")))

(define-public crate-fltk-sys-0.2.3 (c (n "fltk-sys") (v "0.2.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "16sal3xv1hx3ywcmsxdahdrxp2kdiv1c4aclamj3sirwzj87bb8a")))

(define-public crate-fltk-sys-0.2.4 (c (n "fltk-sys") (v "0.2.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0q0w3zahbm2kp0qwf3gpk3ry55gfr563am7443a5m2j27rqdldsh")))

(define-public crate-fltk-sys-0.2.5 (c (n "fltk-sys") (v "0.2.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0xy9cs8za08m0izd73gshyf7jkd79g34hiv7lk3nqv9sjqxm0dzd") (y #t)))

(define-public crate-fltk-sys-0.2.6 (c (n "fltk-sys") (v "0.2.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "19z4j3kxmgpd1j65q81x3apw14rwdkfr6xcfrdrqsjwhksg63k8z") (y #t)))

(define-public crate-fltk-sys-0.2.7 (c (n "fltk-sys") (v "0.2.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1mlzcnc9j5q9zai5akj0a3vw79wii6p2gwiyqql2ah91xbj5ym50")))

(define-public crate-fltk-sys-0.2.8 (c (n "fltk-sys") (v "0.2.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1yrr40a3343bszkrdagm6l3p8l3vd2lak9h9q7124rwbyqmsj5zd")))

(define-public crate-fltk-sys-0.2.9 (c (n "fltk-sys") (v "0.2.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1rfbair82ak5gr5sdxkn5hwm0z2vqy7aigql3i9s0kkl597si1xr")))

(define-public crate-fltk-sys-0.2.10 (c (n "fltk-sys") (v "0.2.10") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0piylx42h6b9as40h0qjjhwdrh5cbj7g8bk6c8nvaia14gziz2c3")))

(define-public crate-fltk-sys-0.2.11 (c (n "fltk-sys") (v "0.2.11") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1vq3xm0x1phmkq6py168160xv5s9z9kkw6l4zyhsll4qsb7bqrwx")))

(define-public crate-fltk-sys-0.2.12 (c (n "fltk-sys") (v "0.2.12") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "06nbzp2vgrw3cxqd16hi9j58bllrhc3c688pabns0z2v0qnkazd0")))

(define-public crate-fltk-sys-0.2.13 (c (n "fltk-sys") (v "0.2.13") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1v3psp5cz768y10mjl5n70i4ck1f7hrlf7jhhwr5daj5b3x796xf")))

(define-public crate-fltk-sys-0.2.14 (c (n "fltk-sys") (v "0.2.14") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "12np3883qi2akcf62njhgx27y4h6ys1hslr7cjz63kk5lq9c9k1x")))

(define-public crate-fltk-sys-0.2.15 (c (n "fltk-sys") (v "0.2.15") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "184ghwv8zvzvpq974yzn45a6qyc1x7mrz0zqzc6qhgh611pjkqf3")))

(define-public crate-fltk-sys-0.2.16 (c (n "fltk-sys") (v "0.2.16") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0a61hkb25i5jkjn1a6iqvphwgz6y8kvs3pfi8ry39s6vfilzjwsd")))

(define-public crate-fltk-sys-0.2.17 (c (n "fltk-sys") (v "0.2.17") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1arbp4ljs4ll01hf8888717j7d57bz0nrqz276j6mpnjgr0nzgyq")))

(define-public crate-fltk-sys-0.2.18 (c (n "fltk-sys") (v "0.2.18") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "16akwz7kfglcfv6xb0pwrnwikzqfxvsklqr0hw7i8jk2zlvdk254")))

(define-public crate-fltk-sys-0.2.19 (c (n "fltk-sys") (v "0.2.19") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0axgk5xx8kj2kwlc3lgcvnglias5kj5c1f9bk31k7y48zjv7nypn") (f (quote (("fltk-shared") ("default"))))))

(define-public crate-fltk-sys-0.2.20 (c (n "fltk-sys") (v "0.2.20") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1cs0j2ac3lha2m24si0pvsn4ypa1vsbrxaylr3367sb19gx7lrs3") (f (quote (("use-ninja") ("fltk-shared") ("default"))))))

(define-public crate-fltk-sys-0.2.21 (c (n "fltk-sys") (v "0.2.21") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "140xhsv16zp8a8jfqvccci35k2mhchpfsprnvqbqyk1rwrjpd8k3") (f (quote (("use-ninja") ("fltk-shared") ("default"))))))

(define-public crate-fltk-sys-0.2.22 (c (n "fltk-sys") (v "0.2.22") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1s0cri3amqxkq4nc9njfp9j0cz94glzr5q5q1f0rfncj2zvlc90g") (f (quote (("use-ninja") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.3.0 (c (n "fltk-sys") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0vfhaj6rds97yp8d98sbnanisdgzdkb4pmpk7ppqny5j968qj07z") (f (quote (("use-ninja") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.3.1 (c (n "fltk-sys") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1kyc939viv3pjs59hf9wpv74y0cs6bn2baaymflckjnh5hqr4k6p") (f (quote (("use-ninja") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.3.2 (c (n "fltk-sys") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0y6h5z1vifrd7ypch9ws7gw4q98m6lccnph3w4c5ss0g04339sn6") (f (quote (("use-ninja") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.3.3 (c (n "fltk-sys") (v "0.3.3") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0ncf6j85zcqilgl17p7awx2136vf5qyc8hfsjqgm8fp9vsd76p03") (f (quote (("use-ninja") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.3.4 (c (n "fltk-sys") (v "0.3.4") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1zd0as0plqbisw1r3d7ndlwfsis8qvb9ppggwr63hbm8b3pbf4aq") (f (quote (("use-ninja") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.3.5 (c (n "fltk-sys") (v "0.3.5") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "17p6vm7y8w9dz08skjmkczp3m1yhwmyqillj7lnl231v7pkd5f80") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.3.6 (c (n "fltk-sys") (v "0.3.6") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0nh60a6kp6zkfcqrf6jwdknj6jgqf3gzwr4782pdpvw79d50z053") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.3.7 (c (n "fltk-sys") (v "0.3.7") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1gbsa8pkkkpr35w7619ry6dkb6i19v32hlyxq37frvv24kgjr4kb") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.3.8 (c (n "fltk-sys") (v "0.3.8") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0112b6qapfy86kpi8nb7hyvh81yi55iqsc9pw5bhw9pzmv7ps30y") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.3.9 (c (n "fltk-sys") (v "0.3.9") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1bkhis9422bmbl1frc0h7yd95xvm5csllhi4cq0idbdyn7dmgarn") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.3.10 (c (n "fltk-sys") (v "0.3.10") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "098gh99aqd271a23zqpk5gsr8hy8kkls0yp7mz49lbrncrmd82qz") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.3.11 (c (n "fltk-sys") (v "0.3.11") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "104a4293b2d8hzm62z8vr704j1nay33r314flmgrfmw43jpwjm2l") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.3.12 (c (n "fltk-sys") (v "0.3.12") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1rr0piz62wsbmha8sxana8dpm4fjnyhhjjh28d16aapckhgs7rl1") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.0 (c (n "fltk-sys") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "07fljnx7amy69mygdwvlbw4zf3hwvia2zsqz3ziww33nvnvdlals") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.1 (c (n "fltk-sys") (v "0.4.1") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1ypn61c24s7vhkhpzsqkqdk179sw49v8wc1japlgrwvwdh00j9fz") (f (quote (("use-ninja") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.2 (c (n "fltk-sys") (v "0.4.2") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1pil0hkq79hwwjghc2ly5kki6445ag01fr4f4ggr3f97kik0960f") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.3 (c (n "fltk-sys") (v "0.4.3") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1vhg1n9160pb0lq22f9a29ikrjsx0lp9mfd6h7li2azp9h8fcg33") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.4 (c (n "fltk-sys") (v "0.4.4") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0l74hllb71wz9q365mx5x43wsnsgk6pqbi4frv73bq3sr3vn8jk9") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.5 (c (n "fltk-sys") (v "0.4.5") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0pyh2a6prnd35pvpfv2z1dwyvgbsnbyizsznmxzj53b8piggdh8g") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.6 (c (n "fltk-sys") (v "0.4.6") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "17jsw3sn37xd5qn0shxifv5xgbdkl8jpm431cv6c2rhvypcp5vmr") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.7 (c (n "fltk-sys") (v "0.4.7") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1rgqwkk4dllfpncc9xbvzq4bdjq8pzzz71y2fvmrasgqhlhxz3gl") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("gl-window") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.8 (c (n "fltk-sys") (v "0.4.8") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0i70az4zwa978qw602f2v07sxvzaj8v4rj51q91dq01zg1a79b9m") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("gl-window") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.4.9 (c (n "fltk-sys") (v "0.4.9") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "195rfqlq5skh8l5517qcmgvkq0drmsmh8xbb28xfn0rfi8acyg4c") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.4.10 (c (n "fltk-sys") (v "0.4.10") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "18ld0sgi6fqg1kpcprxx86v16cb6k52201h7ngcz9rv3q9vivgbk") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.11 (c (n "fltk-sys") (v "0.4.11") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "19fjg46kr0xgz0j84k5zycv5v3dqp9krvfp8v9ndy3zrjbbsfqki") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.12 (c (n "fltk-sys") (v "0.4.12") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0nk7xl11zf5h6mcs8ranrsadi484qgjdnwjpi3ppsjb7l2637x59") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.4.13 (c (n "fltk-sys") (v "0.4.13") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1lk449ycnsqnx17jvcrfl8psz5562v82acy6yvr5ygxc22dmkna0") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.0 (c (n "fltk-sys") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1jn19nfnn92s1izj4v867g5vkbcyvy9p5idc5amz8ppcwzm1wmq1") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.1 (c (n "fltk-sys") (v "0.5.1") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1qcn4ibjmq13ywy48dxf28sb7k7c5i5mv0q2c84wlg55saz86mpn") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.2 (c (n "fltk-sys") (v "0.5.2") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1lhs2cqzprajljyp63q483vgb9lzsrm5mxyy9s66x8z2v21xgf6d") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.3 (c (n "fltk-sys") (v "0.5.3") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1mk2y6hqd8z1swh0z8r5m06fq5br0adm1gpmgnraa8ppfh41k402") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.4 (c (n "fltk-sys") (v "0.5.4") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1jy5q38a3sx9akyljkq19q8gkjq480dv4zgcm11iq5rsxs3bm4lm") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.5 (c (n "fltk-sys") (v "0.5.5") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "048jkz2mllbn8z5jsmimhs5dxrzwdwn35rsqlq38s9a8vmlqsvkp") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.6 (c (n "fltk-sys") (v "0.5.6") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1q9gc1l57ql2j7rj04cjls3mbcs7r4wy8fq6x7fvdbr5znqg9580") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.7 (c (n "fltk-sys") (v "0.5.7") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0a740nz7lrzj2q7lqkbd804ax721jkwa7zw8zr43xbai9ap1rq65") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.8 (c (n "fltk-sys") (v "0.5.8") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "16ickk19c9v30wmlhp29arqspld4gzsr0rx2i4r788b3mqjflg16") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.5.9 (c (n "fltk-sys") (v "0.5.9") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0j5g3lmivyz001n6adgd9cb7kg8yvhjp1pbll9sxblkxpg65j2wh") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.5.10 (c (n "fltk-sys") (v "0.5.10") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "16bv9md1rr65q9nxgzpaz0x48xwpmn2pfi4c9xp1k3bad80iwpr8") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.11 (c (n "fltk-sys") (v "0.5.11") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "147f73mfjsxk5dycqcn2ac1zvyvsag5r7lj8gfv2vmzxm3ghsgw1") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.5.12 (c (n "fltk-sys") (v "0.5.12") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1206sbrrw2hahh78ypy2ijhnj2qn7vnnl7jddrklknl10d0051h8") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.6.0 (c (n "fltk-sys") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0jap8f1z4ib2aq7zsvggyda5prppbac8gir913dfkkl8p6jic1kd") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.6.1 (c (n "fltk-sys") (v "0.6.1") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "08kwn8ghyj0f1gw228bgdf1hvxalv3qpkcliqhcn8fy2zgs3dy5h") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.6.2 (c (n "fltk-sys") (v "0.6.2") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0f4ii5cgccc1ypgryaqrf5q3vgnfxgpmkrzdiy3n7vf49g3x54qy") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.6.3 (c (n "fltk-sys") (v "0.6.3") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1x3dav3snj441a3cir0b03fxw4nzyd203wcj3706ciqd2l3j4g8h") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.6.4 (c (n "fltk-sys") (v "0.6.4") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1rc311dq3pkqhjzkmc2hi0l5lbhdrh4izx5mylfinjfdzryssyw2") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.6.5 (c (n "fltk-sys") (v "0.6.5") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "17y7c1j1j40mwi8zn8sp0fryr6r6f0dlvrrr4w7pcx40vi5yy52r") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.6.6 (c (n "fltk-sys") (v "0.6.6") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1x9fglyg848d6lxsl22iqp591qbykx4pallx006mbl56sh8mx6w2") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.6.7 (c (n "fltk-sys") (v "0.6.7") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1248dz2d9dzs47xkfl9yz7xv4yvp9r4qixbzl28mjxaix2y8rbj1") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.6.8 (c (n "fltk-sys") (v "0.6.8") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1bf033y50ngcgg544v5q6nagp5cblnc9yz1y5987dj823rwqg0c7") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.6.9 (c (n "fltk-sys") (v "0.6.9") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1yvix71iy8q6smj7bl9h4cyh2xp29yxss4791vqiyx44ls4083bq") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.0 (c (n "fltk-sys") (v "0.7.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1r0sajxsjzmlsq4ipdvdh0zcw3gk1di84acdda4m9rq2pdaxzc8k") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.1 (c (n "fltk-sys") (v "0.7.1") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1vqicsfyvb9wii1ikyy66p5vvd8qpmrppvrjd6krsxx7sd30mwj7") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.2 (c (n "fltk-sys") (v "0.7.2") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "10vb1w7sd3a63kn7yhzkfyxlg03z3ywky9dw7fwh38n8y0k3ds2k") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.3 (c (n "fltk-sys") (v "0.7.3") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0bfm6092ganf9figv59xwvxx38k37ccryaziadh1h0p240l5b9k0") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.4 (c (n "fltk-sys") (v "0.7.4") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "082h907wciln7nwrh7knxl415lcq3g7pzf64hb6r0sx0a5i7mkxm") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.5 (c (n "fltk-sys") (v "0.7.5") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "18zlzhzyia9g8jdvcl2z3bd2xdrmy8w5jaa52pinndi8mhkgl0xd") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.6 (c (n "fltk-sys") (v "0.7.6") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1xzmx32gs3dcks4l4chaalnvyjhf92vzqydg51fr3zsmk6p89mpy") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.7 (c (n "fltk-sys") (v "0.7.7") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0cl9fjyfxzhmz1vca19k47svl2cp4g8bnb47lzgfybgrk0vxwjvl") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.8 (c (n "fltk-sys") (v "0.7.8") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "13qiqmd9hilsgcqm51hhpjy79qxmahxvai6jcbb9rf66zpx4yj3m") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.9 (c (n "fltk-sys") (v "0.7.9") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "001b3da334p5dl5snhz2npcqkbmlfxxrz7jazpgp1qxivgpmavbj") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.10 (c (n "fltk-sys") (v "0.7.10") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0v1x1pddr8cbcwfwj8y1v4vpw3z1lly2ms0bmf4xzvdd7i2gfnjc") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.11 (c (n "fltk-sys") (v "0.7.11") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "14wllzl4a5ww3cj1wbj8d13147r35qlfc7f57d30i077ikkfxfx3") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.12 (c (n "fltk-sys") (v "0.7.12") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0p4irvcdgzqjx5fpm5z194p3qxfg4kf35nhcwzpqbx4n0i1k2fmy") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.13 (c (n "fltk-sys") (v "0.7.13") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1m2drbm1rw8izrcsx7px476x9qcwdil1q0411q6p98z51v8yfp3s") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.14 (c (n "fltk-sys") (v "0.7.14") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "1kcivx2fyxcjknk9rx9kvyk2x1j3zzw6v7a9dx2mvgvc72gnb5m0") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.15 (c (n "fltk-sys") (v "0.7.15") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "08layyamhls0sif3f2w90yyyh2sprn43hxi6hkzax2kqr49n8c4r") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.16 (c (n "fltk-sys") (v "0.7.16") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0nbx4h5qwkl1fwyfp184m7djw2d81skc0mvfxxvfpqzw2cqm6m57") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.17 (c (n "fltk-sys") (v "0.7.17") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0z3n4djns8p5x3dvzv49m8r7lna3lcl8fi6c68y3hjxi9x5964l0") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.18 (c (n "fltk-sys") (v "0.7.18") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0v9iz0qnxjass8lsk94gr7wk8hg3giw7ymp9myk2lqv6y9sfbvv8") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.19 (c (n "fltk-sys") (v "0.7.19") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "07dfkvmqdibsxv9ib2pf89czr928vls7lb4jqb28h1adyzy3yqmr") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.20 (c (n "fltk-sys") (v "0.7.20") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0yx28i626m7y2i40mj58n30822a2dvik0zvzcp1g40c13gy4q2i2") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.21 (c (n "fltk-sys") (v "0.7.21") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0n1g0likvz8qdi9f6d704bwiixm367cch04bhz2xdvzzm38d94hl") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.22 (c (n "fltk-sys") (v "0.7.22") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "12vlccxv92y8z47hmq6c6aaamgjzpkby8z5wazk5i3kk404kfs19") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.23 (c (n "fltk-sys") (v "0.7.23") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "187c6dz4pvhq7yyzvjc3fdcqi76mbkm308smlb3q69bvrdb3b884") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.24 (c (n "fltk-sys") (v "0.7.24") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "1dxsxvipk24q7bcknvhmcdi692vyqh301yvhqnxrj28zkrgmkkvp") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.25 (c (n "fltk-sys") (v "0.7.25") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "1g3w86z1wnv7jq3mdgzjm9p9g7m3l0cv2rxmixnxcfpmm8p5ldln") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.7.26 (c (n "fltk-sys") (v "0.7.26") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0ldkqbcwprvq3h5fnj1y29mp3m69rdzgrm29apapn3wqagkw0g87") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-opengl") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.8.0 (c (n "fltk-sys") (v "0.8.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "0yr86fqsgxxng0mrn1z4ask1icswhl7w714kwkc3wlp938r1hfvr") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.8.1 (c (n "fltk-sys") (v "0.8.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1b1892nz82ai9vd1nzcghrk8i4xla75xl8irxrm7jsvkikfh6zrm") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.8.2 (c (n "fltk-sys") (v "0.8.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "13xajl48yirsxzkbcfjnpf7zy8prhdf1gn183v5g6ciafw31h100") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.8.3 (c (n "fltk-sys") (v "0.8.3") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1lm2vxg1w9xm6zgs5fvwpj94g1pcmjzwhmgzxadp5zwlmyq4lj15") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.8.4 (c (n "fltk-sys") (v "0.8.4") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1knrwffyhy40i71qcxxkh2i6lrp643p3kc2v24ag0zg2q5d80qfw") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.8.5 (c (n "fltk-sys") (v "0.8.5") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "10hz9wjaqk6n5f40hrg9xjcz8xf7k1cgjxbfvzmnpa62gdszyb06") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.8.6 (c (n "fltk-sys") (v "0.8.6") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "1ab9hc2gdci2v75p6n0y272n0wy092rwqqlhn4rsrmw8dyjx3jxw") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.8.7 (c (n "fltk-sys") (v "0.8.7") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "0gjdpd5v54gs89vnf3k886dax569z8q5bcn6xbab6yh7pgp2w7v5") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.8.8 (c (n "fltk-sys") (v "0.8.8") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "19qs2j0y67xn6k1vqd51j4bgszry1l5fjq3ciavhr2vq12n3mv0j") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.9.0 (c (n "fltk-sys") (v "0.9.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1qmjcwd0akdaq62dl63cmy4msayy749qy4rv6jzma833cchzma3f") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.9.1 (c (n "fltk-sys") (v "0.9.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1l7r1zd8ficiz8r8smj64zpa0cxvk2maaykliv7ibnw60yl3q4d9") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.9.2 (c (n "fltk-sys") (v "0.9.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "0l0gbbzpvg02fzq1h8gik9yjcw5nb98wrixnlhnmc7vddhzrww3n") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.9.3 (c (n "fltk-sys") (v "0.9.3") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "0zqlc592ia7s5qxmipadw9wmvs8q0cklp06f1g4vhccmky77w9w4") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.9.4 (c (n "fltk-sys") (v "0.9.4") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)))) (h "026cgamyyysd3q31gxnaldxbvipy3lw43zk2cj7in57vkpasp70x") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.9.5 (c (n "fltk-sys") (v "0.9.5") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)))) (h "0a98wzkgn9w056q63gc0gigwbkca8rdbsbp0bn8zlwkgfiqcm7n8") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.9.6 (c (n "fltk-sys") (v "0.9.6") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)))) (h "1yylgjm7pvxlmjb5cxrkncij590iamrql3fcgn260p91pc6giilw") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.9.7 (c (n "fltk-sys") (v "0.9.7") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)))) (h "0fsb6jzsa6hgcjrizjpdfmbd7xk3z7af5clpzywjzs0yayw30ygx") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.10.0 (c (n "fltk-sys") (v "0.10.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1ikqf4dhb55zfgcjxmy3s5mhw3a5jyy40rfj8g3mbn3kkp7qjvz3") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing")))) (y #t)))

(define-public crate-fltk-sys-0.10.1 (c (n "fltk-sys") (v "0.10.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1bpgpxn11bd158qz8kkylm2sbc5yi9xbn963y5qd7mfwflg8fs5z") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.10.2 (c (n "fltk-sys") (v "0.10.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "18qns1cvmb9q7j9ssg1bijq06yrg8mhmncl2qw79ms6frg20czvw") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.10.3 (c (n "fltk-sys") (v "0.10.3") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0qh6yz9pa486mw1fmvg5qdm17lvsrg7cq78mdmr338dmm7yw99as") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.10.4 (c (n "fltk-sys") (v "0.10.4") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1paq19yd0smdq39p72g04bx3px3s0q3fvghd8hahfqf0bxxyrmnj") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.10.5 (c (n "fltk-sys") (v "0.10.5") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "084k9mk5pjnx9mnrs3vjz8y90qmxgywzwxw7hzifwgaxxjgj15av") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.10.6 (c (n "fltk-sys") (v "0.10.6") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "09f6bll1x53jsip6qcn9w3bkkjjlfrwqgk9ddlmw9yr41qn31wiy") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default") ("cpp-testing"))))))

(define-public crate-fltk-sys-0.10.7 (c (n "fltk-sys") (v "0.10.7") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1hf05gvwlg3jwbxm235z9f9n7ddlx5vf4xrx7l1mhvv5g68670qq") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.10.8 (c (n "fltk-sys") (v "0.10.8") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "12466r88mavhv2xbdy2yr7rl0zp9k6i545vhbzx05792by0vf9mm") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.10.9 (c (n "fltk-sys") (v "0.10.9") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0nyjndw6qkfwqyw0b7q16aw7ila2mf5mn1j2qkz4qm8zgjwa56ki") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.10.10 (c (n "fltk-sys") (v "0.10.10") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1j86564w5km37dr2lhbqd8yp0vyikqflww4v9apsxcd3135wvix8") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.10.11 (c (n "fltk-sys") (v "0.10.11") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0r2linga70fjk0g060bmg4xqxsmziqaclhvq8c2nab6qhw5ir5ch") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.10.12 (c (n "fltk-sys") (v "0.10.12") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1f7k8s0nlxqnsf9v4v1ndn86mpw3y8sdzj1qjvk5h7mbvqi65pka") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.10.13 (c (n "fltk-sys") (v "0.10.13") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "126wr3id9wrsikp8drpkwxswmcik7s5g9yz4vaf97sxmrbs6lzab") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.10.14 (c (n "fltk-sys") (v "0.10.14") (d (list (d (n "cmake") (r ">=0.1.45, <0.2.0") (d #t) (k 1)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)))) (h "18g6fh0w9nhnngnq53s7gs6pbmb9cn8awy5j8pq4948qvqjsmg7h") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.10.15 (c (n "fltk-sys") (v "0.10.15") (d (list (d (n "cmake") (r ">=0.1.45, <0.2.0") (d #t) (k 1)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)))) (h "1xpk55svflarbkzlv05v6dni0nqqvbinqwdsd1v6jq16j32r9sc3") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.11.0 (c (n "fltk-sys") (v "0.11.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1bhvianv16y89v2dykajr77yfyk5ci3khmvwr3cbwparp62cg640") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.11.1 (c (n "fltk-sys") (v "0.11.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1lwpxsn4svv2yj0h4xy89mq6nrx9nb6irkxag6n8244cg7srz7j8") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.11.2 (c (n "fltk-sys") (v "0.11.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0dpw1bmy02h5k1gbmdyrdfx23s6z60bp37ci9ady4xbbgxjkxsm4") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.11.3 (c (n "fltk-sys") (v "0.11.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "183v4fpqlcy7i50j6dp8r9ps4njx22b0bf3anmbivvjfk0fwc02w") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.11.4 (c (n "fltk-sys") (v "0.11.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0mnnmqs2kfgdvvijl633lsar163xn2gal7vmyy29svvkcgl13zzq") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.11.5 (c (n "fltk-sys") (v "0.11.5") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "16x3wsmqz2vh22hlgj7b53m9aslamdm9c14rjnngcc0n8cqj4g2d") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.12.0 (c (n "fltk-sys") (v "0.12.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "09w5ip1g8j2jh7nibxkly841whj8bcc86jcckb9rbpr89780aswq") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.12.1 (c (n "fltk-sys") (v "0.12.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "1lj61igv8x1g8979v0flhi90r7lr78xgw7ka5g96m1bsqnzrs9wv") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.12.2 (c (n "fltk-sys") (v "0.12.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "1445vdsizqm3b1bgg0igdzz4a7l85ysm8s7m6c80m7sbm6sj51jh") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.12.6 (c (n "fltk-sys") (v "0.12.6") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "1ynb3gsh7gmd7c3c9i2xr1shi2fac7s2dn9v0q2w3h7m2a7l2mx0") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.12.8 (c (n "fltk-sys") (v "0.12.8") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "0myvq0wsnafnswgf811zkafsavcnis9km700rhcj4f49rr9wdww4") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.12.9 (c (n "fltk-sys") (v "0.12.9") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "0lki1kj8bxmb5mqbbl9hpccry1mdxkvyqi9f9bng0hr5w5h1d0ym") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.12.10 (c (n "fltk-sys") (v "0.12.10") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "1y788d52wg29aygylvp64w80f5lrasmkz2p7p562i9ahmlfayfnm") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.13.0 (c (n "fltk-sys") (v "0.13.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "1zfxmmri2bl56wy8hkpz4whyb5wjf007s69h5zwsfsny95iahrcw") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.13.1 (c (n "fltk-sys") (v "0.13.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "1lnj8prw7dxhbplc21ad3brlx0z587zgb06razl74wbd42i7wsms") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.13.2 (c (n "fltk-sys") (v "0.13.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "0y4hlfsynvv3qqplksn1cwbw15ra9b8slf2j16wq3i4gw8cbx0f8") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.13.3 (c (n "fltk-sys") (v "0.13.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "0lgz4jbyj9a99igzklni7knk77jax1v49crssaj87v24v5k16id9") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.13.8 (c (n "fltk-sys") (v "0.13.8") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "0yrxx8lmsq91b67vlvx1x6l1hrpqys0i5axjadhg3xpwfqr230fk") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.13.9 (c (n "fltk-sys") (v "0.13.9") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "12h3pii003z00h9zhipi8pb0k3m9pfaqc1rzixjzyy2zar3cyg1y") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.13.11 (c (n "fltk-sys") (v "0.13.11") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "12kqy17ymjn6pblb9vkiq9my6d1qs7ck15hgmqzp15lqbmmwps89") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.13.14 (c (n "fltk-sys") (v "0.13.14") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "0hggdvzf9vq8w9q2fzz3x0dc4hd9jf6w9a6b6c04i7rijpn8fbzl") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.14.0 (c (n "fltk-sys") (v "0.14.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)))) (h "0i1snsy9paqbhbn2dxmkxhdri2lhg8k6g8i8wz9na72dyp5jcri3") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.14.1 (c (n "fltk-sys") (v "0.14.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)))) (h "0f1787zi31v4ns678f7h9f2qj2v0kla9600s9rj5134ibgrrhghj") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.14.2 (c (n "fltk-sys") (v "0.14.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)))) (h "0djnqn7bb8jm8kvx9slrmc13fcwbsyrfgxabg58fw0l1m4ylvs6w") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.14.3 (c (n "fltk-sys") (v "0.14.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)))) (h "0i3gs5av5s1rfhz8dvbpf0pi54qcfgglwd8f5sc7l4nc1s7cjab1") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.14.6 (c (n "fltk-sys") (v "0.14.6") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)))) (h "1vjci7axb6m0cx44xqd81jbf5l8vazcv4030w69napd8apk76dg5") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.14.8 (c (n "fltk-sys") (v "0.14.8") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.85") (d #t) (k 0)))) (h "1gzhvqg3gcjdlxsi6z1p0gzcbqk9k2gm2cn1bm3mvhgcshfjbj63") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.14.9 (c (n "fltk-sys") (v "0.14.9") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "16s5i16y43d1sr8gd70csv71b3jawp1zqws0zi8c702apgjfllra") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.14.11 (c (n "fltk-sys") (v "0.14.11") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "0pd016wzh58w01nxs18d1bfa3rky4bp6ka9rn38pvi5lnapmvjzw") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.14.12 (c (n "fltk-sys") (v "0.14.12") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "1v6kzbjjn9wfycqg41sjb56hh4g9qhdpm4i3qqn0pbxafwlb1rbc") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.15.0 (c (n "fltk-sys") (v "0.15.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "1f0vb4qzrp8jbffqfk5mb0v7hpfsyvlh8rapa4dlqvxgcwhy6jh0") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.15.3 (c (n "fltk-sys") (v "0.15.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "0gx4bf967m24gwqanv2iljz1akfz0qhd7hci8x3m5i8fn62lgzsa") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.15.4 (c (n "fltk-sys") (v "0.15.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "15dcvm9va9cg4ps4fq4zpmif3hprw46nlcqpxlz90nm5wfvr4bf3") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.15.6 (c (n "fltk-sys") (v "0.15.6") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "1qd7448a729pj8rqh97d4ayr6incini1gk907j2an1gkfsdnfj81") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.15.7 (c (n "fltk-sys") (v "0.15.7") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)))) (h "1w6c5wyg34qxz1qipxx177srl2lxmn21zv7jbjz65przrdkk9qd1") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.15.9 (c (n "fltk-sys") (v "0.15.9") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)))) (h "1a4xdvv6w9jncclxhg49wrai3q5l6dhlr2r0l1zxn4l75gwpq48k") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.15.11 (c (n "fltk-sys") (v "0.15.11") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)))) (h "0lw4cfanj3nxjvsrp952xnjknm50rqd97v8cxq3c8fcd72gg4g4h") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.15.13 (c (n "fltk-sys") (v "0.15.13") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.89") (d #t) (k 0)))) (h "0n9rmq3pipn646l5f4s5mrqr8ch4ag9fxi66hnjrxkzrrs2j32ca") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.15.14 (c (n "fltk-sys") (v "0.15.14") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.90") (d #t) (k 0)))) (h "0sh5n8d9vk02l9jmkgdg85arni5cgbk8f6bb6dr4nkkv7x34rhvq") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.16.0 (c (n "fltk-sys") (v "0.16.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.90") (d #t) (k 0)))) (h "0w484g7q51zn5saxvlfaadnxkrahj1y7i60d1n675gxc2xs6p6mj") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-0.16.1 (c (n "fltk-sys") (v "0.16.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.90") (d #t) (k 0)))) (h "1213aichsvxp9syvaxq1082j88m6m0mkha7a5vyv4akwaprrskk4") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.16.2 (c (n "fltk-sys") (v "0.16.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.91") (d #t) (k 0)))) (h "13qv7ngy7718xa682k398ipi2k3sl1pkgxp7y7wiwr6rsx90iqmw") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.16.3 (c (n "fltk-sys") (v "0.16.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.91") (d #t) (k 0)))) (h "095qjdqxg4ggw68mg4lzjhh2qphjacwdrz5m39v3jdmnlhjqd2hb") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.16.4 (c (n "fltk-sys") (v "0.16.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.92") (d #t) (k 0)))) (h "09xxp0bwwbq5h8gcv5baswrrph6g76p38kvd7i9mmxzqin01gz5f") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-0.16.5 (c (n "fltk-sys") (v "0.16.5") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)))) (h "0n75yb849alphq6afj4dc9c1d7hilaxwpf7ysmar8lpmsd6g3dvg") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.0 (c (n "fltk-sys") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)))) (h "1606zdca8s626nggiykzxcx6kil85fs1bvf8g3yif9yfg734l4dx") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.1 (c (n "fltk-sys") (v "1.0.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)))) (h "0fylhfanhr67mbkighp353h6hsswb63000rjm1h2f9zvs9hqyxdl") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.4 (c (n "fltk-sys") (v "1.0.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "0gxg8m1fv6nq75pp49ciagm789xch2xa8mj8xbwn69lmdzl4dh0p") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-1.0.5 (c (n "fltk-sys") (v "1.0.5") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "1715whsm25gyxkzl7jayf66bcwi8rgp52yha6742p77r2rh11gkf") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.6 (c (n "fltk-sys") (v "1.0.6") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "1x31kixramcm4gnl1flajbp21jibdkbsnx1wkzmdih6z74p9zldl") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.7 (c (n "fltk-sys") (v "1.0.7") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "099vlbjnnqcx7sbis6a91zx9mggl0pf7fdrbylgcrdih3g1ffcbc") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.8 (c (n "fltk-sys") (v "1.0.8") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "128n67ydfr0dkn83qkq09r127jqvrw4kl9kb1n24plhv5wrwls9m") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.9 (c (n "fltk-sys") (v "1.0.9") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "1qsl8wq0iw73yzh8s0jrxzq1f15rkr5b774bsdfjr9rqhrly74yx") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.10 (c (n "fltk-sys") (v "1.0.10") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "0z32ky36yvjvh1fs2bvaxrr2mdwbajfa02m38mnzfrcfl9pqkfs3") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-1.0.11 (c (n "fltk-sys") (v "1.0.11") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "1c8m0biyvy8jrcn3z371s5cg4zvi33xjfaki7960zfl0wk6ppq11") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.14 (c (n "fltk-sys") (v "1.0.14") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "0lqxgyp2p59rbpfybqznfpv9kdvfg1jrm45x4qa0n1dk5zqhbzi1") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.15 (c (n "fltk-sys") (v "1.0.15") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "0dyiahkrx8plbdaf48lqz110rqyb8f7rh7p6z65p5fsjn34k45ki") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-1.0.16 (c (n "fltk-sys") (v "1.0.16") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "1fdr767n4rm2sg7l5mplxz92nf8x5736cl1cc69mmagmh5llrk1s") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.17 (c (n "fltk-sys") (v "1.0.17") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "10b1fsrd7afjs36w495vin6h278rla0andh70kjnizxg97x8i7jl") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.19 (c (n "fltk-sys") (v "1.0.19") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "0c0q5vwr0pzs1ciiblh1cr7i7xkq8jf8rrwrwldg17qmzb042ivx") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.20 (c (n "fltk-sys") (v "1.0.20") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)))) (h "1fjvz27ykprv8yydmra40b82w7pya7y8x5vq4lbz5fymgly65jj6") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.21 (c (n "fltk-sys") (v "1.0.21") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)))) (h "0ywrdblwlsz1azp3g507cvzamziw807gcc8dd6x06ahhc272lh87") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.0.22 (c (n "fltk-sys") (v "1.0.22") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)))) (h "087dgdjk7y2bbh0ccjrmj7bbyl5zsqxyw3blwqlfckxd62bqpfbg") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.0 (c (n "fltk-sys") (v "1.1.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)))) (h "182c04wybfjhfh94fxlqikjrlm8bjmrrn7797zjkq37akizh7n5l") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.1 (c (n "fltk-sys") (v "1.1.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "1jvv84vc95m5w4j02fpswavgdrivw21kdkbmpc4dqp0x9vqbm2hm") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-1.1.2 (c (n "fltk-sys") (v "1.1.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "0bha5cr79s9w8h6bp9yl3k4p4xkspn29r20c22swanbl28jhd2ai") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.3 (c (n "fltk-sys") (v "1.1.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "196g0s25z8vh52fyaj40y5cgqbi0hbd1905wdc78xywwmhq667j7") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.4 (c (n "fltk-sys") (v "1.1.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "0qizclfjs0d49yhy2prjx1dzk0fl5ryw7ap11314k876gvk87d82") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.5 (c (n "fltk-sys") (v "1.1.5") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "154yxfi5af1476mirkl8lg1zgdxsydbf671ingpmk1gq6rs2ayq5") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.6 (c (n "fltk-sys") (v "1.1.6") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "06sqr7g8brq4vq09nyw9pdjwgkpk6cy9a5zvkfh46hsscv3pq7fj") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.7 (c (n "fltk-sys") (v "1.1.7") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "0zxck8jykqs0dj1gs0kic48g2p9cw03dkfmwmmj55978px9alw39") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.8 (c (n "fltk-sys") (v "1.1.8") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "0bnnl1199wk4payfmfd0r7gwi8qlvwlb4gqgi2fg7dp2kdjn8ykj") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.11 (c (n "fltk-sys") (v "1.1.11") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)))) (h "0pkp8pzbjnqm54l2szk0yqy78cv2b129q102s43gi7c3sjxfmiaw") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.12 (c (n "fltk-sys") (v "1.1.12") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.100") (d #t) (k 0)))) (h "1m16sn29hc2p2i45b3al23kcdkmqkcjmqaannyjg0vmb0z8j9r7b") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.13 (c (n "fltk-sys") (v "1.1.13") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1q6s27gwarh3gbpcxgq0b79lq49sgw74f58dsn3qak5fivvnp6rx") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.14 (c (n "fltk-sys") (v "1.1.14") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0yvq2zf101f5dwxrkdh6nrpdi5h94fwvxm4vbvjyaxmkrs2yny5b") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.16 (c (n "fltk-sys") (v "1.1.16") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1gpnjaj5zcibg9s2bl35whw8j6azzh3f2lm3skwa6ayxy9p2kz1a") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.17 (c (n "fltk-sys") (v "1.1.17") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "16xf1f403bf63dc74i6qmcmzc31i2xg8h1vzbhg88b453rr5mcvn") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.18 (c (n "fltk-sys") (v "1.1.18") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0q739ykgzbph4hhmqkfd3lc73wc5jqvaflqq9n07y60ifpdwgndg") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.1.19 (c (n "fltk-sys") (v "1.1.19") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "02p972mih5bp2c88p6d6gg84l5cq5clglndga06q5jja7sq7p898") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.0 (c (n "fltk-sys") (v "1.2.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1njbq546nvbyidayf4vr9xx9gkfh1qv7dws4blsrpf40zzwplvzi") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (y #t)))

(define-public crate-fltk-sys-1.2.1 (c (n "fltk-sys") (v "1.2.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1y2jmx9jz8xr5xnywgnhygxwb67b64a2y3c1p4klnjrxdvjn1by9") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.3 (c (n "fltk-sys") (v "1.2.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "00yvy0s7420a3bvnwlmssa1dmznskhwac41fr4lib3wl4k04ksgq") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.4 (c (n "fltk-sys") (v "1.2.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "12d4ahgrq5hv9fknlq3r6f280mlwqcayi24lp00rvghw6wl37k0c") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.5 (c (n "fltk-sys") (v "1.2.5") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1b6a2s3zvamdk0zwfswqqn2pyw2bl9p94m4j56q4hc21cklh30i2") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.7 (c (n "fltk-sys") (v "1.2.7") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0qya8zn5r79aankp60yqrlcdmk9xsgvvwkv4q4y1456080271pz2") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.8 (c (n "fltk-sys") (v "1.2.8") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0j2ajfp8jc6r4j28pldmg9m7dgxhk78dimcjqipx8jrvn8zjz2z3") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.9 (c (n "fltk-sys") (v "1.2.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "05skvf7lqv5lm2k2da22vvshrky9sb90s04md73x1k00d4kx0miy") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.11 (c (n "fltk-sys") (v "1.2.11") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0hshnb84d8q1k9507jimlxk9r2892cw3ln0iq4l1yji4s7fgrq3w") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.13 (c (n "fltk-sys") (v "1.2.13") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1sn8h61b1aggfjn7x8kck4n83glgylkv4xx4swrl2zwpdkg5nmm6") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.14 (c (n "fltk-sys") (v "1.2.14") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1jsck88f2041akbqzh3q6g35vcw5crwxkk4zk0vbx242b8s0xp5n") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.16 (c (n "fltk-sys") (v "1.2.16") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "089pqsyfx3y0i8142kwabzzqp62h2xlj424jl36pw70z9wzrgqvv") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.17 (c (n "fltk-sys") (v "1.2.17") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1qx5bglnz7xbgcpcq4pibbsxh0sk95csbpkkrpwy6ssgyy13aqbf") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.18 (c (n "fltk-sys") (v "1.2.18") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1b0qai0zkc4dw0m37sgw9dl5a9smj21kb3lhg4plf8dc1gcyf638") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.19 (c (n "fltk-sys") (v "1.2.19") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1cb234qr3rgqgyqf0mjvv6c1qa24x28j9pk18fnjsdlvgggl9xkn") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.20 (c (n "fltk-sys") (v "1.2.20") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1ml3hrx1mgz3l56ijjjg8yinpyd3a1600n5bbqyf95abag79mins") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.21 (c (n "fltk-sys") (v "1.2.21") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1vn14py9s3s7mv7qqcypx5795v1jzdl5f88dsggm13kz9c63rg0c") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.22 (c (n "fltk-sys") (v "1.2.22") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1wi0kzgz95p0px573y24frf5z2l1m3fzfkxql9n6p8n23d79msfr") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.23 (c (n "fltk-sys") (v "1.2.23") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "03q1xpg1bb3a01i8b4712l8532kw1q81qmk5gr24929fb2r9ic42") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.24 (c (n "fltk-sys") (v "1.2.24") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "05i104q94z20sfsb40qccq3ln4jh961y3wcn6dh1p0w3civgx13l") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.25 (c (n "fltk-sys") (v "1.2.25") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0xdxccdl0bj82kr9hbqcjrvym804nxibhy1bid96szirmazs0k2w") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.26 (c (n "fltk-sys") (v "1.2.26") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1jah166qgwlzg40ksx9aa1z891v8rhjqhdim0b91qfmffz355lsc") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.27 (c (n "fltk-sys") (v "1.2.27") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0gsqnac46gg4lppz9li6skhy3s3bzfk45jzzd05dpjnqsfsqyrlp") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.28 (c (n "fltk-sys") (v "1.2.28") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "07ccbc5l0d80l5c6nf39mc6mrh3s4xp0dvjqda9canb3rhzb7pcd") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.29 (c (n "fltk-sys") (v "1.2.29") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "04rp7h209x6znrzgsx5z17i6qkizgca59x5fb6h8qzi0awnis790") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.30 (c (n "fltk-sys") (v "1.2.30") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "11jgd9h781z32q26h172zing6ip4ijvgq9c6pm4gn21ggz3jnpyw") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default"))))))

(define-public crate-fltk-sys-1.2.31 (c (n "fltk-sys") (v "1.2.31") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "040mxi3972p3px4qrnqydmzyapcmgxcishlsh92fpf51lzl5cxfa") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.2.32 (c (n "fltk-sys") (v "1.2.32") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0yawhm9ndxx7x8i283dyzp1ysdy1s4g58arf3q9h9plh0k2l3wmn") (f (quote (("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.0 (c (n "fltk-sys") (v "1.3.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1nl3ckkadlqz7qdxpzg81d9as1aqn7l23wcslhbb7d7kdi7nl49h") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.1 (c (n "fltk-sys") (v "1.3.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "04fcz74dmiqvx3lapkhbfbdl17q2w01ij9lg2gymlqahs5n3djfb") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.2 (c (n "fltk-sys") (v "1.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1c1szj1nc4pd1v1r87x5yli8n9q0a3lxa0rmyp15hh1a2rsfd42m") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.3 (c (n "fltk-sys") (v "1.3.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1m32gryjkdfr1vf6c8lxpmp2gzxlx7cyzpbp9s1ibss0drgh0m9s") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.5 (c (n "fltk-sys") (v "1.3.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0pa63q214s3pajqv6g0p1pradmgad7h8p09hvk1fxwsj8y6n1vqz") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.7 (c (n "fltk-sys") (v "1.3.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0iwnrhmm487yx27avk5fi73xax6x4xa2m8ny2ihi4s45sxgywblc") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.8 (c (n "fltk-sys") (v "1.3.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0f1h3f7zlczn1rlmqg5hv09cczrb9cbqlnfnh425576miqlfb1fc") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.9 (c (n "fltk-sys") (v "1.3.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "12k4bmfz0hn5j1r6s51k0m7plqncprdgy7r6ysig3xz6f55h4wx5") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.10 (c (n "fltk-sys") (v "1.3.10") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1jl4mk1shvv83wndn5lxh6c6xgj6liv85c30qh02ypkysg0vnly0") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.11 (c (n "fltk-sys") (v "1.3.11") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1il1qzhm3s7r3i3h3cnilylyhxznp86vs11k2n7yxjj1qkddbl6a") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.12 (c (n "fltk-sys") (v "1.3.12") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0al0a8y84im00qwmr3878vh24x41rs82jaq8lmf1srchvyaar65w") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.13 (c (n "fltk-sys") (v "1.3.13") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0xsc36899k1ynhlxadz1fdnyyq6ggivxr89xxmmvv2mi7b4gqfld") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.14 (c (n "fltk-sys") (v "1.3.14") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1hljx3qy8simbdjph4dndf0fkv311wd8pw1c5wich9f7n2ag942q") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.16 (c (n "fltk-sys") (v "1.3.16") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1pprpiwbwyi8vcp67br2wg160173sr4xfh6mjcmc3n6ps3v12p6f") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.17 (c (n "fltk-sys") (v "1.3.17") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "17b04l5ikmaf0v79v2a3xcd4dhj13wgy30dgl8irjwv71mx9jba6") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.19 (c (n "fltk-sys") (v "1.3.19") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0zjvh794wnz271r93vzq5n6ac6g888shb4rnr4gfhfqsa9ki0nn8") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.20 (c (n "fltk-sys") (v "1.3.20") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "01qis8faqk3mpbb67drljsabalfgrw9gmp64mmksb4c20rmnra03") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.21 (c (n "fltk-sys") (v "1.3.21") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1yvvjxmvmfxrrgv65nm1908mk8y9g0l7f4hkplp23j43za014539") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.22 (c (n "fltk-sys") (v "1.3.22") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1qh0728vlidd4w5mmpckfv2nhlj5jymrj91mwvyh2lx13yimcmbm") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.24 (c (n "fltk-sys") (v "1.3.24") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1ryw7ml98vfrxrnzym7vhjmqlbgxh7yi9cgn333x1a628vw2amxf") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.25 (c (n "fltk-sys") (v "1.3.25") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "15b81kqll38lpm5l3hm46dkf72fa9ldd66xwhs2k41pn0jrxy0v1") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.26 (c (n "fltk-sys") (v "1.3.26") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1jcq7vdn6ij6pa94pg14307spga5vn45x01f99hwhxn362wyfxd1") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.27 (c (n "fltk-sys") (v "1.3.27") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0yw3pn4wpkkindhdcg22xsrjjfh5yy77dm4v811l60l127s02rz1") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.28 (c (n "fltk-sys") (v "1.3.28") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1yihsgrfqa99h2bhwgp73yg9zq2qhhx1r4fp4dqm64ilqnb03nf7") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.29 (c (n "fltk-sys") (v "1.3.29") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0kx6xy1mfj8ylw2433r4xbspnl8xgijn9c69n3ydbpr6raxgm7sv") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.30 (c (n "fltk-sys") (v "1.3.30") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0dbgng4ssmx7xpx8avsrz96iywvn3piriyhhd48h3fa2s5051vlj") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.31 (c (n "fltk-sys") (v "1.3.31") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0830vasn53aynr5pf1gzrir0yf84hbmzbl4sw50anv1ys2rmxk9l") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.32 (c (n "fltk-sys") (v "1.3.32") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "03schrqgyfxy3l7xfdq8lvy9ig5lc0lm9ya4zfy4mkg644pa15j8") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.33 (c (n "fltk-sys") (v "1.3.33") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "07k1m84yar9aymg9xsgg9kiymj15kz5h6pymi5v8nwafjwkypk1b") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.3.34 (c (n "fltk-sys") (v "1.3.34") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08vqjwh6a2vx550sgfid4qdd0pf723lsfxacr3i70a6q1wmr48ka") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk")))

(define-public crate-fltk-sys-1.4.0 (c (n "fltk-sys") (v "1.4.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "07rw2074w2xfday8pcvlyqfbjkcdxb3z5gx7fq0pv517l6vjxpz9") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.1 (c (n "fltk-sys") (v "1.4.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "06q0vg5s7n91yrpp0i3xm4x0aq94x507jxh6j4w6gwz0w4la54kj") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.2 (c (n "fltk-sys") (v "1.4.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1y00almp6v3j3xvcalmbfrhpv3v7nqzy11h7i05sr2xi0mm9gnla") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.3 (c (n "fltk-sys") (v "1.4.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1ny279shz6373mr03gl6c3qqph1f19rza0gw1g8fvhvc4lq61q7l") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.4 (c (n "fltk-sys") (v "1.4.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1mfxkjidf00yj2mgf7k165bnibbsxkn6i301h92rlv2hapliz54i") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.5 (c (n "fltk-sys") (v "1.4.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0l1a9x418jw0j53qh8p2j4gd1wxg8hnxgnp0xc0rqqiq0v1ans5w") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.6 (c (n "fltk-sys") (v "1.4.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "061xsgsn3mcylms5xxzhfxiw1m2pa3d2hd37przm0qpwdj9zwp98") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.7 (c (n "fltk-sys") (v "1.4.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1i97agww3j37p30kp6fly55xcm27s7axhal0g3563w3rw325ar92") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-bundled") ("enable-glwindow") ("default")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.8 (c (n "fltk-sys") (v "1.4.8") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1ahz792w4gs4g2pdx8s45r9n92mw6ly4z764rv648ablaivfhm6j") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.9 (c (n "fltk-sys") (v "1.4.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "03kz7250nljdb29k9q78hqvm9xmy3gkfl5s62l5dn1m0wdrqwx38") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.10 (c (n "fltk-sys") (v "1.4.10") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0a5g1glbf588fc8q0nv6awj5ixd0c6alwbnzx3rr3xwmy5aplyxa") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.11 (c (n "fltk-sys") (v "1.4.11") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0hs21xk17fmcx43i50yw76qibn2b74afx5hqkkklnppfvkn8h5cj") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.12 (c (n "fltk-sys") (v "1.4.12") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1nd11684x270v8abgkrhfjxrzzdl5xhmbq4p6n6hfz36bpngxdyc") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.13 (c (n "fltk-sys") (v "1.4.13") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1wss7iyp8y492776llg722iskdwbn9x1yyk5409nzj1ziik6hxin") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.14 (c (n "fltk-sys") (v "1.4.14") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1fcpnddkqnvw80awavaxzl8w66im9xf391yw6f4wb70f0dg4zlmj") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "fltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.15 (c (n "fltk-sys") (v "1.4.15") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0l11av5y3py806fy5nln908lj4zsdcrzjf844501af85w9p924dr") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.16 (c (n "fltk-sys") (v "1.4.16") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "18xmqh1y46xffxdn6msfiqasvhg3sia7ns0z3lvp7yry5m9h6yhj") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.17 (c (n "fltk-sys") (v "1.4.17") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0alvfz7d7wxpiwfnq6svj9kq8a72ddw2gi3hqqwpsg2bfjpr59hr") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.19 (c (n "fltk-sys") (v "1.4.19") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1prrp17wf33a4lgl8r5hc4hrdsc7abhgb6h3r0cbl0ch3rxl10a3") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.20 (c (n "fltk-sys") (v "1.4.20") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1310jlcissd6pkxj44hy1l0ymgsq3cnd2m7bkw782px26zb2rn8z") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.21 (c (n "fltk-sys") (v "1.4.21") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0f1y2nfjlmfqz6kbw09n40b0f2rk12f7239csaimmam6dvhbja2p") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.22 (c (n "fltk-sys") (v "1.4.22") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08fhdibdrf9x2xpkqb99g9kxvkm0lvhh7992s20l1kkdc4dljd7l") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.23 (c (n "fltk-sys") (v "1.4.23") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "07gxmv90rh11whcxyhaxzvnya2gv3jablwd7l3ngnj5pqzyc94k9") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.25 (c (n "fltk-sys") (v "1.4.25") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "12vh4py1ip1fnc1y93l1irkwxm8pp7shavs0vri16pciycl8xbz0") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.26 (c (n "fltk-sys") (v "1.4.26") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1znyzi73vk1ca7p67fqc8pwby25n6klpiz9w459bn3yi33ik4qys") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.27 (c (n "fltk-sys") (v "1.4.27") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0s3gpbn1hbgz31iyplw1l1q0bbgrdfbbyalv7pbxa4i1vwhwsksy") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.28 (c (n "fltk-sys") (v "1.4.28") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "116qc6kb15ksyyx3d7lni86m9ak1lpbcwnvwzrzbs1dcp6dbcj3n") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.29 (c (n "fltk-sys") (v "1.4.29") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0rax3896fr1z5syn5l1n7y2cpbj3am0zrzgzkn7q6fhwcnkfgn7l") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.56")))

(define-public crate-fltk-sys-1.4.30 (c (n "fltk-sys") (v "1.4.30") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1k9kj2vgyjv1zddz99yqi6fh80la9m0gjry73zhh45h95zjis3av") (f (quote (("use-wayland") ("use-ninja") ("system-zlib") ("system-libpng") ("system-libjpeg") ("system-fltk") ("static-msvcrt") ("single-threaded") ("pkg-config") ("no-pango") ("no-images") ("no-gdiplus") ("legacy-opengl") ("fltk-shared") ("fltk-config" "cc") ("fltk-bundled") ("enable-glwindow") ("default") ("cairoext")))) (l "cfltk") (r "1.64")))

