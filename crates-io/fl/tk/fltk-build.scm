(define-module (crates-io fl tk fltk-build) #:use-module (crates-io))

(define-public crate-fltk-build-0.1.0 (c (n "fltk-build") (v "0.1.0") (h "17ir11anwd4ddvnkjy6vdg944klir21dvh6qnrn8az4nlpqjdi63") (f (quote (("no-pango") ("no-gdiplus"))))))

(define-public crate-fltk-build-0.1.1 (c (n "fltk-build") (v "0.1.1") (h "0xdjkazcnf90a6wy0fi9d90lc7rmka4dp5af899la1ynl19nf9i4") (f (quote (("no-pango") ("no-gdiplus"))))))

