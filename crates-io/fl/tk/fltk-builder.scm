(define-module (crates-io fl tk fltk-builder) #:use-module (crates-io))

(define-public crate-fltk-builder-0.1.0 (c (n "fltk-builder") (v "0.1.0") (d (list (d (n "fltk") (r "^1") (k 0)) (d (n "fltk-anchor") (r "^0.1") (d #t) (k 2)) (d (n "fltk-theme") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (k 0)))) (h "18v3wh1pj6lc16z4jc7f4ld4g3l76crgrf625hpzhv6hm2rm4qvf") (f (quote (("id_map" "lazy_static") ("default" "id_map"))))))

