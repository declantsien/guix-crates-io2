(define-module (crates-io fl tk fltk-float) #:use-module (crates-io))

(define-public crate-fltk-float-0.1.0 (c (n "fltk-float") (v "0.1.0") (d (list (d (n "fltk") (r "^1.4.4") (d #t) (k 0)))) (h "1sp9ymmf9wn2c4r2cb4clr46qz5qcq7y9galnhglw20ld8cxqxhc") (f (quote (("static-msvcrt" "fltk/static-msvcrt") ("fltk-bundled" "fltk/fltk-bundled"))))))

(define-public crate-fltk-float-0.1.1 (c (n "fltk-float") (v "0.1.1") (d (list (d (n "fltk") (r "^1.4.4") (d #t) (k 0)))) (h "0ysfywhdc40q5bpg3087j7qkk6pn230x4wx8npk6nrb12a194wav") (f (quote (("static-msvcrt" "fltk/static-msvcrt") ("fltk-bundled" "fltk/fltk-bundled"))))))

(define-public crate-fltk-float-0.1.2 (c (n "fltk-float") (v "0.1.2") (d (list (d (n "fltk") (r "^1.4.4") (d #t) (k 0)))) (h "1rczyvm8a64wshv82hmaavisz0xfg5lfw8zilag7ifcvaxzrlh74") (f (quote (("static-msvcrt" "fltk/static-msvcrt") ("fltk-bundled" "fltk/fltk-bundled"))))))

(define-public crate-fltk-float-0.1.3 (c (n "fltk-float") (v "0.1.3") (d (list (d (n "fltk") (r "^1.4.4") (d #t) (k 0)))) (h "0j4fk7r7najwgvkzs2mdxavvf5w9vsks81s1g3ymhvm83g1jrgi3") (f (quote (("static-msvcrt" "fltk/static-msvcrt") ("fltk-bundled" "fltk/fltk-bundled"))))))

(define-public crate-fltk-float-0.1.4 (c (n "fltk-float") (v "0.1.4") (d (list (d (n "fltk") (r "^1.4.4") (d #t) (k 0)))) (h "0sa9glh265gp2cy4jwxnrn240ndgshmlc2m8c4p57pfn3wwfcpwk") (f (quote (("static-msvcrt" "fltk/static-msvcrt") ("fltk-bundled" "fltk/fltk-bundled"))))))

(define-public crate-fltk-float-0.1.5 (c (n "fltk-float") (v "0.1.5") (d (list (d (n "fltk") (r "^1.4.4") (d #t) (k 0)))) (h "0m2prfckg6gcskydyxlx4yx6n9fnajwqii6f849x8gji21kspwr4") (f (quote (("static-msvcrt" "fltk/static-msvcrt") ("fltk-bundled" "fltk/fltk-bundled"))))))

