(define-module (crates-io fl tk fltk-evented) #:use-module (crates-io))

(define-public crate-fltk-evented-0.1.0 (c (n "fltk-evented") (v "0.1.0") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "03ahxh4jdys4g1smfn4y51q2xjkkhpng5qj5lxsqq2q7lcidnsvv")))

(define-public crate-fltk-evented-0.1.1 (c (n "fltk-evented") (v "0.1.1") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1jjasbhlmd04jxz08l3kd7965kv7bx2isighfj1jnvx7d9mvzgwx")))

(define-public crate-fltk-evented-0.1.3 (c (n "fltk-evented") (v "0.1.3") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "0536fkmgvspzl59l8i42aghcxqn5qfv3ghhhw3frgb8ybijdr8k4")))

(define-public crate-fltk-evented-0.1.4 (c (n "fltk-evented") (v "0.1.4") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "114y6a8pji5l202wacnd8p8ndr23576q8ynzgl96rwrsglyyp94l")))

(define-public crate-fltk-evented-0.1.5 (c (n "fltk-evented") (v "0.1.5") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1snxljv6xichzy687mnnar85flhrb378jwgpry9d01fifrml6rzc")))

(define-public crate-fltk-evented-0.1.6 (c (n "fltk-evented") (v "0.1.6") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "0g5v7i4dh07jsw5vz4xzh6v2sh9j19fwwmkzhq9rqgm8dwwnbwvb") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.2.0 (c (n "fltk-evented") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "17lh3blvnlc03lcrzpb3spwymcpghb88izfqbh7hkhr7pwqk3kj8") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.3.0 (c (n "fltk-evented") (v "0.3.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "1iwwkddbig50qlp3yp2m05k6mkk5rbypllm15hmm2285bnmh8j88") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.3.1 (c (n "fltk-evented") (v "0.3.1") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "06h4s1ck507077s7hv8b38gkfcic4d96rm73i85bf2w0as552i9r") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.3.2 (c (n "fltk-evented") (v "0.3.2") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "15qsvkg8gqznwlq392fyq2jcgcfs69fs0n8hcp38mi4srikfa0v9") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.3.3 (c (n "fltk-evented") (v "0.3.3") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "surf") (r "^2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09kdgv1ql1gl1gx39xl1dbhqhgzqr6bkvhjahcp8a806sddvwhml") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.4.0 (c (n "fltk-evented") (v "0.4.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "surf") (r "^2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "06h2wkvwi0d4bf8jl53rf9jhxnkj6vznz8gxfvhrllwmwfzz8ldz") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.4.1 (c (n "fltk-evented") (v "0.4.1") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "surf") (r "^2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0j2bv7a4ra1w96y3xm8gf9kmp0q2ih8hiigdylxl5cik7fnqkq21") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.5.0 (c (n "fltk-evented") (v "0.5.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "1c0hhwkfmn7gmvc81himlicyq4wiq2gga0nh31b3iq0kv2ijpqjx") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.5.1 (c (n "fltk-evented") (v "0.5.1") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "0xqyy57zlrl7q2lw8wpxqqzy9sy0lndvysvv9vq7xfrg4nr6v5yx") (f (quote (("default"))))))

(define-public crate-fltk-evented-0.5.2 (c (n "fltk-evented") (v "0.5.2") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.4.24") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "0hdq9a5mbr7ivkj0yiqirbg3869iaclqa8jfhq0bgfabl7z29wm2") (f (quote (("default"))))))

