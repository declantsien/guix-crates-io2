(define-module (crates-io fl tk fltk-webview) #:use-module (crates-io))

(define-public crate-fltk-webview-0.1.0 (c (n "fltk-webview") (v "0.1.0") (d (list (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "gdk-sys") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gdkx11-sys") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gtk-sys") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "08ybhrwbjh0jfmikawv3ygsjn4x8n1ls3dbw8n1y6m7frxc1fayk") (y #t)))

(define-public crate-fltk-webview-0.1.1 (c (n "fltk-webview") (v "0.1.1") (d (list (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "gdk-sys") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gdkx11-sys") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gtk-sys") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0vkqrr7mzzah70n3z5xdkkblahj3l3dpivw0ajpkz9kkc2pjivai") (y #t)))

(define-public crate-fltk-webview-0.1.2 (c (n "fltk-webview") (v "0.1.2") (d (list (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "gdk-sys") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gdkx11-sys") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gtk-sys") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0kh09pw1pf9y05wvclm737r1mzwb1gwbl7sm2v4nh6i96w27fajw")))

(define-public crate-fltk-webview-0.1.3 (c (n "fltk-webview") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "10zz7rgjmismy2yhqpwhnw550yk2imyv618jgd74wh4fwka5ijss")))

(define-public crate-fltk-webview-0.1.4 (c (n "fltk-webview") (v "0.1.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "179xdkzkwbl0zqddh37q87ydvdwjym8lsw1al7gmmafdb7pc97jq")))

(define-public crate-fltk-webview-0.1.5 (c (n "fltk-webview") (v "0.1.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0469vrzbbrb6gj23arzrnvc2rwpmhkszdqmdzgrs749npanbc1bj")))

(define-public crate-fltk-webview-0.1.6 (c (n "fltk-webview") (v "0.1.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "08rqf0w8y4qrpkmsgnz1wqd23vd8f1wxhxwcp4zvj5v1fwyymvi7")))

(define-public crate-fltk-webview-0.1.7 (c (n "fltk-webview") (v "0.1.7") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0p3pqbnjvk8lryq5nxpzjsc0l8qkm2nrv56c5hwfi42sp2vcjjlb")))

(define-public crate-fltk-webview-0.1.8 (c (n "fltk-webview") (v "0.1.8") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "06l1qiv82pfzssjd8809r8avkdkyi6sn7wvl9iplh62vp96xlb4l")))

(define-public crate-fltk-webview-0.1.9 (c (n "fltk-webview") (v "0.1.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "19digdrrdi2b62r51firxqzvnrqqy2yq07a7qg9vb7y61b842fym")))

(define-public crate-fltk-webview-0.1.10 (c (n "fltk-webview") (v "0.1.10") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "07zfya86zlri2qsn9lkqi9kyrz0i4iyvb7ilfzzlkad1z7amy0hc")))

(define-public crate-fltk-webview-0.1.12 (c (n "fltk-webview") (v "0.1.12") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (t "cfg(not(any(target_os = \"macos\", target_os = \"windows\")))") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0hazf0b4j663w2wg70v5rri2kkrg9xdnsa4sf7mvv6r7y85b1p23")))

(define-public crate-fltk-webview-0.1.13 (c (n "fltk-webview") (v "0.1.13") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (t "cfg(not(any(target_os = \"macos\", target_os = \"windows\")))") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1jvnslv6jv4w2wgr1h72f6cqynqqllc0cvdyw8pnnyaqkis5qjgn")))

(define-public crate-fltk-webview-0.1.14 (c (n "fltk-webview") (v "0.1.14") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1.7") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (t "cfg(not(any(target_os = \"macos\", target_os = \"windows\")))") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1i4m9v5mw155lkx1rsm1h4ri4gawh6vlsw54smsrcqi8afkphs1x")))

(define-public crate-fltk-webview-0.1.15 (c (n "fltk-webview") (v "0.1.15") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1.7") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (t "cfg(not(any(target_os = \"macos\", target_os = \"windows\")))") (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0dp5j9jqfnqd35sv7kn1xzv7rxdm0y4bapwxbs5mm032j77binc1")))

(define-public crate-fltk-webview-0.1.16 (c (n "fltk-webview") (v "0.1.16") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1.7") (d #t) (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "08gg50r31ird0faynk4x03gxql5db8gnynmb331n1677bk9wr7xj") (y #t)))

(define-public crate-fltk-webview-0.1.17 (c (n "fltk-webview") (v "0.1.17") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1.7") (d #t) (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0qv713y7kdvj5z4rh36qlffpg6l67gcp5kvp93v5l1vz6vigihh0")))

(define-public crate-fltk-webview-0.1.18 (c (n "fltk-webview") (v "0.1.18") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1.7") (d #t) (k 0)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0irhj7nxh4m8hmnmhqfz1crsn6xlqj14h9wfcz25dp2w8njq2whk")))

(define-public crate-fltk-webview-0.2.0 (c (n "fltk-webview") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1s20534a1w8s1yml8kirq6wz4x0if8dsxxkvjqyf3h98qv9rbavg") (y #t)))

(define-public crate-fltk-webview-0.2.1 (c (n "fltk-webview") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "14bxd7xjdn5vszhx51cb3q3y3jzc0crq6g6bm171ix1sfp16acyx") (y #t)))

(define-public crate-fltk-webview-0.2.2 (c (n "fltk-webview") (v "0.2.2") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)) (d (n "webview-official-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0nsba9jd73g033gssyyxhjmiialq7srh3z05qwgcd97pvc5xqadd")))

(define-public crate-fltk-webview-0.2.3 (c (n "fltk-webview") (v "0.2.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)))) (h "1qh2kvld3y36pfvybapz4k5jx57g7hn5qfqm5imq0n0c84sldpl8")))

(define-public crate-fltk-webview-0.2.4 (c (n "fltk-webview") (v "0.2.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)))) (h "0aaajmx45xzy7lz7s88zqs2ns42qw8ylq22jxfcrfcsiab14d6dq")))

(define-public crate-fltk-webview-0.2.5 (c (n "fltk-webview") (v "0.2.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)))) (h "0x616v2ydzqiq6ksrr4f6sk20znkzh4hg79w8cs0r210ilizwd7a") (y #t)))

(define-public crate-fltk-webview-0.2.6 (c (n "fltk-webview") (v "0.2.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)))) (h "0qimx5gfbrikkcrxw1y35wrfwy63pnlb72rf3a1122zj4i7rw8gx") (y #t)))

(define-public crate-fltk-webview-0.2.7 (c (n "fltk-webview") (v "0.2.7") (d (list (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)))) (h "0ckm9bclsw739sp9qx22a4dz8lw1382gxxs357f4q8hxgsdbfwmf")))

(define-public crate-fltk-webview-0.2.8 (c (n "fltk-webview") (v "0.2.8") (d (list (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "1q19d2kl618lgynz6f756l4riwnw54b45q3dxfmlc5iaxji9lfzf")))

(define-public crate-fltk-webview-0.2.9 (c (n "fltk-webview") (v "0.2.9") (d (list (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.8") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "0xn0yxi2f98mkyr5y790gvxih8cjm8b3f8200yvddj2g0bb20qax")))

(define-public crate-fltk-webview-0.2.10 (c (n "fltk-webview") (v "0.2.10") (d (list (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.8") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "0kp8bbsr22qkm5xbrqalp8bwxhv0jsc107sq22a2xvscafkrxrgp")))

(define-public crate-fltk-webview-0.2.11 (c (n "fltk-webview") (v "0.2.11") (d (list (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.8") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "17n3bz3ssmz4zxiigrshnzmr4n1wvcs67bjf2xkpwwvvyiqjczbq")))

(define-public crate-fltk-webview-0.2.12 (c (n "fltk-webview") (v "0.2.12") (d (list (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.8") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "0fai3mgz935iiw3pf7m6x9w9nin5gfprpkik8b4wm6sfbkjjwwkf")))

(define-public crate-fltk-webview-0.2.13 (c (n "fltk-webview") (v "0.2.13") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "fltk") (r "^1.1") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.8") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "06djlsjd92payz8397jn45jrkbgpz607knfnj3bz5drd8nqv6kzk")))

(define-public crate-fltk-webview-0.2.14 (c (n "fltk-webview") (v "0.2.14") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.9") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "1zpvjpiys5gy424iyjnbzn8rfvn422fw6z69a8545ishvzsdyc1s")))

(define-public crate-fltk-webview-0.2.15 (c (n "fltk-webview") (v "0.2.15") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.10") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "1hlvaj43wi3zc67gm10kc9z9jlki3wix5pnz8wbd222m46r2f10w")))

(define-public crate-fltk-webview-0.2.16 (c (n "fltk-webview") (v "0.2.16") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.10") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "0znlxx9974gik5nrvvzl108d2bycq7kv64pkzmwcpy9gixhkq23c")))

(define-public crate-fltk-webview-0.2.17 (c (n "fltk-webview") (v "0.2.17") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.11") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "0d90ray7rm3zvlqcp4dsmmzv30sig4c5q3f3psshjxdgi3h84cdn")))

(define-public crate-fltk-webview-0.3.0 (c (n "fltk-webview") (v "0.3.0") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.2.11") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "1mwcijs78anxz5yhv75wxc7ikz7yzzibq9kmdhwgm86l3hdhz5br")))

(define-public crate-fltk-webview-0.3.1 (c (n "fltk-webview") (v "0.3.1") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "08dmg9qvqihdvl2254w6yg9gp59gayksc3rmk2zwmf4adkzgi7yp")))

(define-public crate-fltk-webview-0.4.0 (c (n "fltk-webview") (v "0.4.0") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "03k4i2whvr860wjnrvq3zx665433wga8l4f6jzjr3v8js56mlwf0")))

(define-public crate-fltk-webview-0.4.1 (c (n "fltk-webview") (v "0.4.1") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "1l44186wgf79jzmcqy05233l8kqqdixxw35azi954hlbrapbgyp6")))

(define-public crate-fltk-webview-0.4.2 (c (n "fltk-webview") (v "0.4.2") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)) (d (n "fltk-webview-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 2)) (d (n "tinyjson") (r "^2") (d #t) (k 2)))) (h "08b0p2qqhsfx05vhw6z5mqfrvaa108ifr8bd0hrfmq70qjd6d3cz")))

