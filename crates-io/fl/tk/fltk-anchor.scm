(define-module (crates-io fl tk fltk-anchor) #:use-module (crates-io))

(define-public crate-fltk-anchor-0.1.0 (c (n "fltk-anchor") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "fltk") (r "^1.1.10") (d #t) (k 0)))) (h "0yxggwpd5qa776yig8f5djd6dnn88asdnas7mmll9nx5sybm9pdy")))

(define-public crate-fltk-anchor-0.1.1 (c (n "fltk-anchor") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "fltk") (r "^1.1.10") (d #t) (k 0)))) (h "1473lkqrqhwyn8vcj164kmsmric7b2nv8x7ggfxamrswb05zspj3")))

(define-public crate-fltk-anchor-0.1.2 (c (n "fltk-anchor") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "fltk") (r "^1.1.10") (d #t) (k 0)))) (h "1c11ziq3mxx06nc5bljx9kl0v2ml536wrc666da21qr1ypjam96y")))

(define-public crate-fltk-anchor-0.1.3 (c (n "fltk-anchor") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "fltk") (r "^1.1") (d #t) (k 0)))) (h "1pbzk40y9n7wwcz37ad3bwqp7byh04dl51421k9mc6v6xilr8zsl")))

(define-public crate-fltk-anchor-0.1.4 (c (n "fltk-anchor") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "fltk") (r "^1.1") (d #t) (k 0)))) (h "1ycmhvzfp0iw1dy2sg3xfv8kfvxhfim8ay3wafg767xhv2jq9kla")))

(define-public crate-fltk-anchor-0.2.0 (c (n "fltk-anchor") (v "0.2.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "fltk") (r "^1.4") (d #t) (k 0)))) (h "15v2q6p377agr00447wwmp7ydqcz8c82rqyc79b15krk023dzi61")))

