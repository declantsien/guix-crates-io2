(define-module (crates-io fl tk fltk-term) #:use-module (crates-io))

(define-public crate-fltk-term-0.1.0 (c (n "fltk-term") (v "0.1.0") (d (list (d (n "fltk") (r "^1.4.15") (d #t) (k 0)) (d (n "portable-pty") (r "^0.8") (d #t) (k 0)) (d (n "vte") (r "^0.12.0") (d #t) (k 0)))) (h "05iscxmlfiw6chwr0api8kcgf0q60dbwx4am27sx8s7kvzxkq72g") (f (quote (("debug-term"))))))

(define-public crate-fltk-term-0.1.1 (c (n "fltk-term") (v "0.1.1") (d (list (d (n "fltk") (r "^1.4.19") (d #t) (k 0)) (d (n "portable-pty") (r "^0.8") (d #t) (k 0)))) (h "1wdfsw4pwsn9kqf5b7cqay509vzqwb142czwpqqqdqc8pm7my703") (f (quote (("debug-term"))))))

(define-public crate-fltk-term-0.1.2 (c (n "fltk-term") (v "0.1.2") (d (list (d (n "fltk") (r "^1.4.19") (d #t) (k 0)) (d (n "portable-pty") (r "^0.8") (d #t) (k 0)))) (h "1kssmgh7505vzxq2yg05x11x6dxv0c0rlmbp8cm24hjczr13ipzz") (f (quote (("debug-term"))))))

(define-public crate-fltk-term-0.1.3 (c (n "fltk-term") (v "0.1.3") (d (list (d (n "fltk") (r "^1.4.19") (d #t) (k 0)) (d (n "portable-pty") (r "^0.8") (d #t) (k 0)))) (h "00bknycbyc9fzgimfcr19jd1fmg3l17xslfcynbplydpblzy7170") (f (quote (("debug-term"))))))

