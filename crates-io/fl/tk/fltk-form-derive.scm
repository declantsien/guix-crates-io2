(define-module (crates-io fl tk fltk-form-derive) #:use-module (crates-io))

(define-public crate-fltk-form-derive-0.1.0 (c (n "fltk-form-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mg0b1yz3bx8r1fh899lx82qyc3y57a57p45dkhg6ziiigwpn0k8")))

(define-public crate-fltk-form-derive-0.1.1 (c (n "fltk-form-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "04q14wlikjf6vww335nhd4i1w9sci9kv8nfmhvxig5d9y777qjhs")))

(define-public crate-fltk-form-derive-0.1.2 (c (n "fltk-form-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0lq4nihk2bvwj0yza7f9ksz7vvd49nlsw6zd90kniyd1safx5n3z")))

