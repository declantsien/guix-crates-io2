(define-module (crates-io fl tk fltk-webview-sys) #:use-module (crates-io))

(define-public crate-fltk-webview-sys-0.2.7 (c (n "fltk-webview-sys") (v "0.2.7") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1blbz26rx87d91vgz5rpqldl9800p83zyqzcyh5kfbj5cxgzab8x")))

(define-public crate-fltk-webview-sys-0.2.8 (c (n "fltk-webview-sys") (v "0.2.8") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12ix4jmd4br2ghawlcqbkxc7al8vmd7dn35k7lbyl9lizgl2qfqy")))

(define-public crate-fltk-webview-sys-0.2.9 (c (n "fltk-webview-sys") (v "0.2.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04bfrpiininpan224lzcngiiviapcpr92h4ilszxfpgkf3w1rfs2")))

(define-public crate-fltk-webview-sys-0.2.10 (c (n "fltk-webview-sys") (v "0.2.10") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15jxbmafbl2rf8bxl2a5xpn7jba2vjyvw3mv22pmvd9kz20vpim6")))

(define-public crate-fltk-webview-sys-0.2.11 (c (n "fltk-webview-sys") (v "0.2.11") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1x8w2ypid0sxal42kdp35kbblldxx974i7qzb8xaxmla5ya5gv97")))

(define-public crate-fltk-webview-sys-0.3.0 (c (n "fltk-webview-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15sadjl44ci2wnlw1rc15npxa0px43jaap1p5fj5s92gprgxngw0")))

(define-public crate-fltk-webview-sys-0.3.1 (c (n "fltk-webview-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "wv-sys") (r "^0.1") (d #t) (k 0)))) (h "10x904hccz1ijgashlfksrs0h6wmb51flkaqcqyjq2727bd9rw9q")))

(define-public crate-fltk-webview-sys-0.3.2 (c (n "fltk-webview-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "wv-sys") (r "^0.1.2") (d #t) (k 0)))) (h "01lar0d96bwpz1x6dphfahm659xs4xihw21fppgdww2qjii6qqbi")))

(define-public crate-fltk-webview-sys-0.3.3 (c (n "fltk-webview-sys") (v "0.3.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "wv") (r "^0.1.0") (d #t) (k 0)) (d (n "wv-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1nvabfwds3w3rifshni6jznwmcy8appfpy602vwmjz1hyjayjhiw")))

