(define-module (crates-io fl tk fltk-check-env) #:use-module (crates-io))

(define-public crate-fltk-check-env-0.1.0 (c (n "fltk-check-env") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0wv3fsxq2d44js6k843sgw0rfn30jmc3pzv1hwvr91lkv2d3jl2g")))

(define-public crate-fltk-check-env-0.1.1 (c (n "fltk-check-env") (v "0.1.1") (d (list (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1d310lijkds5bps2jmd5accxh7kn38k8yaf7cx5fj0r5lffjdxwi")))

(define-public crate-fltk-check-env-0.1.2 (c (n "fltk-check-env") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0kvaclg5klr6ah0x7k639k2v76ad49p8fa69lzc309snc23gdgj7")))

(define-public crate-fltk-check-env-0.1.3 (c (n "fltk-check-env") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "135s9fmhjpsyhjsykckmdw4xc0d0nrn99yi5qawh7qdzx9b5hjjb")))

(define-public crate-fltk-check-env-0.1.4 (c (n "fltk-check-env") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "11m7np2wkyngfv27s3n4zs1ac7ws4z6s84j58ya6gvg31zi8ysh9")))

