(define-module (crates-io fl tk fltk-extras) #:use-module (crates-io))

(define-public crate-fltk-extras-0.1.0 (c (n "fltk-extras") (v "0.1.0") (d (list (d (n "fltk") (r "^1.3.17") (d #t) (k 0)))) (h "1fvbqryz63l2rk1kkrzw22baigwg01kcpch123yzdbsaxa43j0b0")))

(define-public crate-fltk-extras-0.1.1 (c (n "fltk-extras") (v "0.1.1") (d (list (d (n "fltk") (r "^1.3.17") (d #t) (k 0)))) (h "0lwsc7bvim5dpkbs5lc5d63rgkv4l5ys0pvvmg8bpkjxkzjigk8w")))

(define-public crate-fltk-extras-0.1.2 (c (n "fltk-extras") (v "0.1.2") (d (list (d (n "fltk") (r "^1.4.0") (d #t) (k 0)))) (h "11gv03wn4992rdm8k14kml46q1w1wdvxr4f95cv0rzpyx67kwqhy") (r "1.56")))

(define-public crate-fltk-extras-0.1.3 (c (n "fltk-extras") (v "0.1.3") (d (list (d (n "fltk") (r "^1.4.0") (d #t) (k 0)))) (h "1jvg8pasbrw59bwsivyshfbk1sz83n8kkaah6s1hgc4iab365kqf") (r "1.56")))

(define-public crate-fltk-extras-0.1.4 (c (n "fltk-extras") (v "0.1.4") (d (list (d (n "fltk") (r "^1.4.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (d #t) (k 0)))) (h "1f8z71fyzc1vir9s8kqb2wqys7c6695p13c3c8j6bnqifpyxhl21") (r "1.56")))

(define-public crate-fltk-extras-0.1.5 (c (n "fltk-extras") (v "0.1.5") (d (list (d (n "fltk") (r "^1.4.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (d #t) (k 0)))) (h "19a2s4x9zjvgy9hr2nnrxsvwb58zsccyy2wl71dkmf5il65zdamp") (r "1.56")))

(define-public crate-fltk-extras-0.1.6 (c (n "fltk-extras") (v "0.1.6") (d (list (d (n "fltk") (r "^1.4.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (d #t) (k 0)))) (h "15g7krgk464mdbj19pb2zlq709vp3pgkh9xl68v7qpr1yk8w73gn") (r "1.56")))

(define-public crate-fltk-extras-0.1.7 (c (n "fltk-extras") (v "0.1.7") (d (list (d (n "fltk") (r "^1.4.6") (d #t) (k 0)) (d (n "fltk-sys") (r "^1.4.6") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (d #t) (k 0)))) (h "1sfgvpidrifd4za6asrj623949chm32qa7z26xysh6g3m6s7i7id") (r "1.56")))

(define-public crate-fltk-extras-0.1.8 (c (n "fltk-extras") (v "0.1.8") (d (list (d (n "fltk") (r "^1.4.6") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (d #t) (k 0)))) (h "0fmjp3k29ik87mbjwryc0ak7iark1jv924gmm9l1mmrgccx16gad") (r "1.56")))

