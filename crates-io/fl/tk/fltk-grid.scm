(define-module (crates-io fl tk fltk-grid) #:use-module (crates-io))

(define-public crate-fltk-grid-0.1.0 (c (n "fltk-grid") (v "0.1.0") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)))) (h "0f1wc1ipznawlsqd3qny43cd2wm3by9c1i2fna7x5bk58rlqk6kv")))

(define-public crate-fltk-grid-0.1.1 (c (n "fltk-grid") (v "0.1.1") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)))) (h "16z775myxrvm9dyys51zc8zybbysr693af8g0r79rq95813hgclk")))

(define-public crate-fltk-grid-0.1.2 (c (n "fltk-grid") (v "0.1.2") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)))) (h "1rslpqqpm51mdr4pnigahy41b5fm43bzihn4rsxba7cv562nf7gl")))

(define-public crate-fltk-grid-0.1.3 (c (n "fltk-grid") (v "0.1.3") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)))) (h "1b1ixk1xvqc4xqispq9k33l0a2kxhfanp9g9x49c92fgwm1nk9zl")))

(define-public crate-fltk-grid-0.1.4 (c (n "fltk-grid") (v "0.1.4") (d (list (d (n "fltk") (r "^1.3.15") (d #t) (k 0)))) (h "1ixcs8s4a7pq66jxp8a9qf6qvjd1k3rkm4v5k10rqyxd65r6n1ys")))

(define-public crate-fltk-grid-0.2.0 (c (n "fltk-grid") (v "0.2.0") (d (list (d (n "fltk") (r "^1.3.33") (d #t) (k 0)))) (h "13n999ac3ibm3ksw87s1vpryj9nd9dyfyf9wv40hs12k9rwk8gac")))

(define-public crate-fltk-grid-0.3.0 (c (n "fltk-grid") (v "0.3.0") (d (list (d (n "fltk") (r "^1.4.0") (d #t) (k 0)))) (h "0093ml02ld18a5qsqv1xl3xjsps252kmrkf1aj6ci6fwgpcbxphi")))

(define-public crate-fltk-grid-0.3.1 (c (n "fltk-grid") (v "0.3.1") (d (list (d (n "fltk") (r "^1.4.0") (d #t) (k 0)))) (h "16g90waf9isl0qfw8iyahwsivssqshxwsld6mb05v1x18l0v2wrr")))

(define-public crate-fltk-grid-0.4.0 (c (n "fltk-grid") (v "0.4.0") (d (list (d (n "fltk") (r "^1.4.15") (d #t) (k 0)))) (h "1lb7bg7714jay34zx1qlm5f1djhzbhnrsc03li48z2rf46phqwak")))

