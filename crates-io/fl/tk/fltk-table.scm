(define-module (crates-io fl tk fltk-table) #:use-module (crates-io))

(define-public crate-fltk-table-0.1.0 (c (n "fltk-table") (v "0.1.0") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1vpqrqcf609ij6nm8v730hjvr6yzwx1xygchr7yad5h4d5i89hhg") (y #t)))

(define-public crate-fltk-table-0.1.1 (c (n "fltk-table") (v "0.1.1") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1g44c8i925fxhy9lrfnhzss7m1cdxp3bhln1d25x7vc2nb2pr20y") (y #t)))

(define-public crate-fltk-table-0.1.2 (c (n "fltk-table") (v "0.1.2") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1gg8maf5sv255xlgh62q1rva63fy5nsfiyhfqf512k1z1pl338vm")))

(define-public crate-fltk-table-0.1.3 (c (n "fltk-table") (v "0.1.3") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1b763yfg2fiqpb0acj5zif339k5x0gkj6pqciw4bzyk4ah77s741")))

(define-public crate-fltk-table-0.1.4 (c (n "fltk-table") (v "0.1.4") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "16id88449gvaj57h09f5pxvas7zivv3qpl47sj5wdj5bngz2rgcb")))

(define-public crate-fltk-table-0.1.5 (c (n "fltk-table") (v "0.1.5") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "13l4nd13vcjlwj7qghpv5l0cs8qhy1gi3c3gn7qd5wpkd2h2zz6i")))

(define-public crate-fltk-table-0.1.6 (c (n "fltk-table") (v "0.1.6") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1ficfc1zr48jz4fzfdf064kw15awrz8spzhbyx6nhfwbbx3x5paw")))

(define-public crate-fltk-table-0.1.7 (c (n "fltk-table") (v "0.1.7") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1rc9bfph3i07lrkmjpyyhy8svna5222kp422j6p2l3952fr8aiwl")))

(define-public crate-fltk-table-0.1.8 (c (n "fltk-table") (v "0.1.8") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1pk7r437ycic1y37b1gj5wqq0kqv7q0sjs9lpzxambhk1p0q83yq")))

(define-public crate-fltk-table-0.2.0 (c (n "fltk-table") (v "0.2.0") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "08f7ifzj8h4aff5lz5iih1n3ivrhx7xcwwfq7brjyf3q13m5xsld")))

(define-public crate-fltk-table-0.2.1 (c (n "fltk-table") (v "0.2.1") (d (list (d (n "fltk") (r "^1.2") (d #t) (k 0)))) (h "1zb3pb71x2shky9r9dqiszwpw29cvw6v5kqn7gqnvwwmdrsp3wxy")))

(define-public crate-fltk-table-0.2.2 (c (n "fltk-table") (v "0.2.2") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)))) (h "0i94h8cd0hwnjlkqg6l38vjspv23r0fbpyvcqwlfdk09mhpa9v7q")))

(define-public crate-fltk-table-0.3.0 (c (n "fltk-table") (v "0.3.0") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)))) (h "11mj57s0xl187dlhdd6x8m8r8v5mavgi451hxk41kwb6jcvcqvn8")))

(define-public crate-fltk-table-0.3.1 (c (n "fltk-table") (v "0.3.1") (d (list (d (n "fltk") (r "^1.4") (d #t) (k 0)))) (h "0jpicvky5ajmxnk9n3qm27kdxvgz21dd0j68hknwl8bc2dpbxy7r")))

