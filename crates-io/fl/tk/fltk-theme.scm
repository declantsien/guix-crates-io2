(define-module (crates-io fl tk fltk-theme) #:use-module (crates-io))

(define-public crate-fltk-theme-0.1.0 (c (n "fltk-theme") (v "0.1.0") (d (list (d (n "fltk") (r "^1.1.3") (d #t) (k 0)))) (h "0a7s280lbcfrfy8dgfdszc5w5a598pzwmk54jyqkbkiaxnrk2p45") (y #t)))

(define-public crate-fltk-theme-0.1.1 (c (n "fltk-theme") (v "0.1.1") (d (list (d (n "fltk") (r "^1.1.3") (d #t) (k 0)))) (h "166p1dc44h8la03s93gdnghwg409f3jxr2zg4p3fn614zafx8q10")))

(define-public crate-fltk-theme-0.1.2 (c (n "fltk-theme") (v "0.1.2") (d (list (d (n "fltk") (r "^1.1.3") (d #t) (k 0)))) (h "1g9r01gmsrpw7nfcbr4bwb6kfsbf1jz48y8085850k750s9lay2x")))

(define-public crate-fltk-theme-0.1.3 (c (n "fltk-theme") (v "0.1.3") (d (list (d (n "fltk") (r "^1.1.4") (d #t) (k 0)))) (h "0vqgvkbvcwa2jxvvmpwhcgvjd46z9q19n9bkbjnk2z4lzzi7iak6")))

(define-public crate-fltk-theme-0.1.4 (c (n "fltk-theme") (v "0.1.4") (d (list (d (n "fltk") (r "^1.1.4") (d #t) (k 0)))) (h "0hygqxg2kd1mapjcrgb6ahcisyy604dmaj3g0ixxv9d9dkgi5yws")))

(define-public crate-fltk-theme-0.1.5 (c (n "fltk-theme") (v "0.1.5") (d (list (d (n "fltk") (r "^1.1.4") (d #t) (k 0)))) (h "0i3kim7zhx32h7qjh69848b5kam7rcm6g0ivkjgkpgq9p6y1rpbk")))

(define-public crate-fltk-theme-0.1.6 (c (n "fltk-theme") (v "0.1.6") (d (list (d (n "fltk") (r "^1.1.6") (d #t) (k 0)))) (h "08mk2zz2d0xya7awqf9j68zh3raim5n7w2wnhb73gcks6f5pyss0")))

(define-public crate-fltk-theme-0.1.7 (c (n "fltk-theme") (v "0.1.7") (d (list (d (n "fltk") (r "^1.1.6") (d #t) (k 0)))) (h "1l44lvbsa1zqgp1ghjrix6nqqir59bypic295d662hi6kb94s294")))

(define-public crate-fltk-theme-0.1.8 (c (n "fltk-theme") (v "0.1.8") (d (list (d (n "fltk") (r "^1.1.6") (d #t) (k 0)))) (h "0kb9kqqfqfi5d0h8w600ailf0i010zw70x3a9151cgpq88qqc8b4")))

(define-public crate-fltk-theme-0.2.0 (c (n "fltk-theme") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "fltk") (r "^1.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0asl5ywaadyian02ksy1wrdg2852ddz768ynpyly4nzbhrj2l2q5") (y #t)))

(define-public crate-fltk-theme-0.2.1 (c (n "fltk-theme") (v "0.2.1") (d (list (d (n "cocoa-colors") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "fltk") (r "^1.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0amlcxpqz9ljafhs06hr88p4g8rbbm1xxn0k9yjflmxd1bslcycs") (y #t)))

(define-public crate-fltk-theme-0.2.2 (c (n "fltk-theme") (v "0.2.2") (d (list (d (n "cocoa-colors") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "fltk") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0pnyfcph5hkmwdic9hxkhjbw7bf92qaxdr03m30rs5wyarhfq95k")))

(define-public crate-fltk-theme-0.2.3 (c (n "fltk-theme") (v "0.2.3") (d (list (d (n "cocoa-colors") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "fltk") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1ac4ij1nqkrm37mi5kgz0xyipw1451jkalddq9nnfjqzw2ss6fy5")))

(define-public crate-fltk-theme-0.3.0 (c (n "fltk-theme") (v "0.3.0") (d (list (d (n "cocoa-colors") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "fltk") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0v372y9j9dn0m3ivmr5gpqqycpgxv5n389g4rjfsy1qni04h5vrq")))

(define-public crate-fltk-theme-0.3.1 (c (n "fltk-theme") (v "0.3.1") (d (list (d (n "cocoa-colors") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "fltk") (r "^1.2.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "19v2kwv6zgvncz5dyc1y7jy0inldmb9l2wi38ykadr113hv85j77")))

(define-public crate-fltk-theme-0.3.2 (c (n "fltk-theme") (v "0.3.2") (d (list (d (n "cocoa-colors") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "fltk") (r "^1.2.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1hplrlnqz23f4wqq4bkz6lglrflfpwclv3rsrgrdc43ql1p1fzw6")))

(define-public crate-fltk-theme-0.4.0 (c (n "fltk-theme") (v "0.4.0") (d (list (d (n "cocoa-colors") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "color-maps") (r "^0.1") (d #t) (k 0)) (d (n "fltk") (r "^1.2.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "13kbaiksdca19773d0bqkj02qqmkgjx49pkw45s8a44jb9pw4caq")))

(define-public crate-fltk-theme-0.4.1 (c (n "fltk-theme") (v "0.4.1") (d (list (d (n "cocoa-colors") (r "^0.1.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "color-maps") (r "^0.1") (d #t) (k 0)) (d (n "fltk") (r "^1.2.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0xc5sc08barj6kx8y9dyv7bl2gh150v1i38d6aisd8gzabw3cfmq")))

(define-public crate-fltk-theme-0.4.2 (c (n "fltk-theme") (v "0.4.2") (d (list (d (n "cocoa-colors") (r "^0.1.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "color-maps") (r "^0.1") (d #t) (k 0)) (d (n "fltk") (r "^1.2.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0rhjfyzgf1lrm5vq6bj84irc6wffazbpkhjc1v8lbgfq1sdkvvmb")))

(define-public crate-fltk-theme-0.6.0 (c (n "fltk-theme") (v "0.6.0") (d (list (d (n "cocoa-colors") (r "^0.1.2") (o #t) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "color-maps") (r "^0.1") (d #t) (k 2)) (d (n "fltk") (r "^1.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0hbh3ap08w38wlmk6dxjbj609xpry1rvrnr9van8cmh8ycaxlygb")))

(define-public crate-fltk-theme-0.7.0 (c (n "fltk-theme") (v "0.7.0") (d (list (d (n "cocoa-colors") (r "^0.1.2") (o #t) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "color-maps") (r "^0.1") (d #t) (k 2)) (d (n "fltk") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "07xnsrmsvixja1vybjdr2swv6n84nv6ji4r1hhk3cg7gw3s5m8ri")))

(define-public crate-fltk-theme-0.7.1 (c (n "fltk-theme") (v "0.7.1") (d (list (d (n "cocoa-colors") (r "^0.1.2") (o #t) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "color-maps") (r "^0.1") (d #t) (k 2)) (d (n "fltk") (r "^1.4.0") (d #t) (k 0)))) (h "0m3d2fq8jynghv90a49yh97v0rmmc6xcmnm4yhvxsnvb46qzjs73")))

(define-public crate-fltk-theme-0.7.2 (c (n "fltk-theme") (v "0.7.2") (d (list (d (n "cocoa-colors") (r "^0.1.2") (o #t) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "color-maps") (r "^0.1") (d #t) (k 2)) (d (n "fltk") (r "^1.4.12") (d #t) (k 0)))) (h "0s2818sadnk0b9ylhf9j2152gffnqvkvi9mgy9mba52mh5xd4f9m")))

