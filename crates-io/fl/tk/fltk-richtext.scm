(define-module (crates-io fl tk fltk-richtext) #:use-module (crates-io))

(define-public crate-fltk-richtext-0.1.0 (c (n "fltk-richtext") (v "0.1.0") (d (list (d (n "fltk") (r "^1.3.19") (d #t) (k 0)))) (h "0a8sxqkwf4z0ki3ans3qx0mgpkcwpr3hwhca5w8m5d3qab2y4j6j")))

(define-public crate-fltk-richtext-0.1.1 (c (n "fltk-richtext") (v "0.1.1") (d (list (d (n "fltk") (r "^1.3.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "18d2w5f5czbbjhvn413xx8dqasjdzxhcasn7nk4vp6l8pcqggaqc")))

