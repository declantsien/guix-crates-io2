(define-module (crates-io fl tk fltk-calendar) #:use-module (crates-io))

(define-public crate-fltk-calendar-0.1.0 (c (n "fltk-calendar") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^0.14") (d #t) (k 0)))) (h "1ax135n00spbh429jn735i0ciqdvqdg2ascjjbf84i4p2sch5n3g")))

(define-public crate-fltk-calendar-0.1.1 (c (n "fltk-calendar") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^0.14") (d #t) (k 0)))) (h "0vkb4nkhybw56qs5m2wr8vngkky0q963ycjysz1w0axncvagrcvl")))

(define-public crate-fltk-calendar-0.1.2 (c (n "fltk-calendar") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)))) (h "0z4vmnqa4wr0ldi93d5y0y9r5094wg9iyzqcf58crr74vgvi4j48")))

(define-public crate-fltk-calendar-0.2.0 (c (n "fltk-calendar") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^0.16") (d #t) (k 0)))) (h "0xq2niyk48a98d90csivr9srl2frzp1jzrzayq84lm26a65z1mh0")))

(define-public crate-fltk-calendar-0.3.0 (c (n "fltk-calendar") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^1") (d #t) (k 0)))) (h "11rc4j5vq9yjrdxfjgzn9s8is0rwxa8s9xyfj2bq6m49j026j380") (y #t)))

(define-public crate-fltk-calendar-0.3.1 (c (n "fltk-calendar") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^1") (d #t) (k 0)))) (h "019s4851ndi1c3902sqszfh0170yvqdlsgaq822kzwinp2ssw01v")))

(define-public crate-fltk-calendar-0.3.2 (c (n "fltk-calendar") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^1") (d #t) (k 0)))) (h "03ah19cfrp6crassblr8p29dq76mai4vg5hjxf87mfqprc0f7bm4")))

(define-public crate-fltk-calendar-0.3.3 (c (n "fltk-calendar") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^1") (d #t) (k 0)))) (h "0clfmrkzq01kj42w6nh2vvgjq1ck5vv3wd67in6fhx7552hap9yc")))

(define-public crate-fltk-calendar-0.3.4 (c (n "fltk-calendar") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^1") (d #t) (k 0)))) (h "0mjjk1i8impa0m8748ccfdqs60lpb7fblnx9j3nd83cj35kc3dym")))

(define-public crate-fltk-calendar-0.3.5 (c (n "fltk-calendar") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^1.2.22") (d #t) (k 0)))) (h "1hlgchlay89h7laj8hlcf799a3mqcmfjgqb0h9b4560l09ngknlw")))

(define-public crate-fltk-calendar-0.3.6 (c (n "fltk-calendar") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^1.2.22") (d #t) (k 0)))) (h "1rp1pv8rr7s3nb8r58877ppwli57x6kcqa4zq7i3vxnb7q3c4rxi")))

(define-public crate-fltk-calendar-0.4.0 (c (n "fltk-calendar") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fltk") (r "^1.4.0") (d #t) (k 0)))) (h "1jj5bcb4i2765vgp0sq58rhxd576qr1dd6prajjvsy1ggc850fyf")))

