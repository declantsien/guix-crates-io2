(define-module (crates-io fl ig flight-computer) #:use-module (crates-io))

(define-public crate-flight-computer-0.1.0 (c (n "flight-computer") (v "0.1.0") (d (list (d (n "bbqueue") (r "^0.5") (f (quote ("thumbv6"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-sdmmc") (r "^0.3") (d #t) (k 0)) (d (n "fugit") (r "^0.3") (d #t) (k 0)) (d (n "micromath") (r "^2") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "ublox") (r "^0.3") (k 0)))) (h "0w18g9lmif21qkzjb2rsq6n379bmnckrrfnj15r0s60dkcq68kgi") (y #t)))

