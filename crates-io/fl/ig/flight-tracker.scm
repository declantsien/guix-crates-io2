(define-module (crates-io fl ig flight-tracker) #:use-module (crates-io))

(define-public crate-flight-tracker-0.1.0 (c (n "flight-tracker") (v "0.1.0") (d (list (d (n "adsb") (r "^0.2") (d #t) (k 0)) (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0kbhvcr06xb3mlvia0gn33pjy429nfcg7g09r002m3zv23zavym7") (f (quote (("default" "cli") ("cli" "anyhow" "structopt"))))))

(define-public crate-flight-tracker-0.2.0 (c (n "flight-tracker") (v "0.2.0") (d (list (d (n "adsb") (r "^0.3") (d #t) (k 0)) (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "02fwvh9p8by65x3qknf14xkd3h394qwmp6jzk41q8nsqsfj7kdpv") (f (quote (("default" "cli") ("cli" "anyhow" "structopt"))))))

(define-public crate-flight-tracker-0.2.1 (c (n "flight-tracker") (v "0.2.1") (d (list (d (n "adsb") (r "^0.3") (d #t) (k 0)) (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08g4pw8z961yz2c0v8hmg842c46y84nwwpzdk4jarvkb6bqnbhbk") (f (quote (("default" "cli") ("cli" "anyhow" "clap"))))))

