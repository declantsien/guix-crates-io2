(define-module (crates-io fl op flopen) #:use-module (crates-io))

(define-public crate-flopen-0.1.0 (c (n "flopen") (v "0.1.0") (d (list (d (n "nix") (r "^0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1nhfmd0xpi9zf1d92is809ai3y7cgrahwk60c90w4a3v694bg40s")))

(define-public crate-flopen-0.1.1 (c (n "flopen") (v "0.1.1") (d (list (d (n "nix") (r "^0.27") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1q8vz6x6jq40rf89q7c520hxjf10xfxgbnl98x96yskpz4b8vg1b")))

