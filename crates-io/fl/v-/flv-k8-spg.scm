(define-module (crates-io fl v- flv-k8-spg) #:use-module (crates-io))

(define-public crate-flv-k8-spg-0.1.0 (c (n "flv-k8-spg") (v "0.1.0") (d (list (d (n "flv-k8-spu") (r "^0.1.0") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "types") (r "^0.1.0") (d #t) (k 0) (p "flv-types")))) (h "1rdybxhnigvn5dnhaqz3qh8npmd9fb96bcggsd2blya58rq17309") (y #t)))

(define-public crate-flv-k8-spg-1.0.0 (c (n "flv-k8-spg") (v "1.0.0") (d (list (d (n "flv-k8-spu") (r "^1.0.0") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "types") (r "^0.1.0") (d #t) (k 0) (p "flv-types")))) (h "1ph1wyr7wrqsbda0jdhnff0rqg1jjyygbr3kjx6n5v908r0418cw") (y #t)))

