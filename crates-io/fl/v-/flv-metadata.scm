(define-module (crates-io fl v- flv-metadata) #:use-module (crates-io))

(define-public crate-flv-metadata-0.1.0 (c (n "flv-metadata") (v "0.1.0") (d (list (d (n "k8-flv-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "kf-protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "types") (r "^0.1.0") (d #t) (k 0) (p "flv-types")))) (h "0lffmhqdvws24m7k8qcfp1n42dzym44p7r6fzh9yhz0p38f8f4am") (y #t)))

(define-public crate-flv-metadata-1.0.0 (c (n "flv-metadata") (v "1.0.0") (d (list (d (n "k8-flv-metadata") (r "^1.0.0") (d #t) (k 0)) (d (n "kf-protocol") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "types") (r "^0.1.0") (d #t) (k 0) (p "flv-types")))) (h "01hjlwx8h369f3xsar7m508fwkcrna5cwivsn1hv42x83lc5m1y2") (y #t)))

