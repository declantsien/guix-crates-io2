(define-module (crates-io fl v- flv-api-spu) #:use-module (crates-io))

(define-public crate-flv-api-spu-0.1.0 (c (n "flv-api-spu") (v "0.1.0") (d (list (d (n "flv-future-aio") (r "^0.1.0") (d #t) (k 0)) (d (n "kf-protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)))) (h "10sii9mi0b6bzx7gbmf2kbfkmlr8asymqyz2krjm41h8vlwfb8ix") (y #t)))

(define-public crate-flv-api-spu-1.0.0 (c (n "flv-api-spu") (v "1.0.0") (d (list (d (n "flv-future-aio") (r "^1.0.0") (d #t) (k 0)) (d (n "kf-protocol") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fp9j96v9qm1ji6n18hiph0rnx1ig4gwshnvxyqm9rcmks60wriy") (y #t)))

(define-public crate-flv-api-spu-1.1.0 (c (n "flv-api-spu") (v "1.1.0") (d (list (d (n "flv-future-aio") (r "^2.1.0") (d #t) (k 0)) (d (n "kf-protocol") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fg3id33bsi4rcrakxfk78zpzii2762yn0wgh19fyj49ph510z3p") (y #t)))

