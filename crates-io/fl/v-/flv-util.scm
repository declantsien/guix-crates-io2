(define-module (crates-io fl v- flv-util) #:use-module (crates-io))

(define-public crate-flv-util-0.1.0 (c (n "flv-util") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1f7yv3q8a2bzqmz75ii4qahzz1ckq5dnzw0074gf528zx78ff2cj") (f (quote (("fixture") ("default"))))))

(define-public crate-flv-util-0.1.1 (c (n "flv-util") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0gbhafkjrbicn7gj5f387hlwfdqfkh0x9axpcc0kvdz5k6kdkpv3") (f (quote (("fixture") ("default"))))))

(define-public crate-flv-util-0.2.0 (c (n "flv-util") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "11n21wwy7i90s8gb19c3w53iyd2d2pcyyn1msns51l8c770jhbrd") (f (quote (("fixture") ("default"))))))

(define-public crate-flv-util-0.2.1 (c (n "flv-util") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "01s7p0gbl6mvyvgzm268kwbv3dhrb3vgjmxj7x2hxafn1y7di1id") (f (quote (("fixture") ("default"))))))

(define-public crate-flv-util-0.3.0 (c (n "flv-util") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.17") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.11") (d #t) (k 0)))) (h "0yrlr8m504yp8lwcjb2kv2jaqgifl31bqj7gnkvv1953m7iir9ld") (f (quote (("fixture") ("default"))))))

(define-public crate-flv-util-0.3.1 (c (n "flv-util") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.17") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.11") (d #t) (k 0)))) (h "1svzdg2w84b77d6d9vmyxz11mlvavwzdhvh4958jlvnz6sjy3931") (f (quote (("fixture") ("default"))))))

(define-public crate-flv-util-0.3.2 (c (n "flv-util") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.17") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.11") (d #t) (k 0)))) (h "16y6x3gk7qb7v2gxk3iji1ba6rxdf1bm3m8xp9cyqf40k1f77kxm") (f (quote (("fixture") ("default"))))))

(define-public crate-flv-util-0.4.0 (c (n "flv-util") (v "0.4.0") (d (list (d (n "tracing") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.0") (d #t) (k 0)))) (h "1mwxx2x0csw64bn1ga8br32q04fmpp0iwr9qc9szcky7af8aa976") (f (quote (("fixture") ("default"))))))

(define-public crate-flv-util-0.5.0 (c (n "flv-util") (v "0.5.0") (d (list (d (n "tracing") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1ffh3dhmlraygpxhfjbsz9qz8r6nawbnrh8bj9rs7dy9ma1n112c") (f (quote (("subscriber" "tracing-subscriber") ("fixture"))))))

(define-public crate-flv-util-0.5.1 (c (n "flv-util") (v "0.5.1") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1n2jn4jm43b3hg3axdpw761p8xd1gpzgy83bjg2ar60qd1rrrjck") (f (quote (("fixture"))))))

(define-public crate-flv-util-0.5.2 (c (n "flv-util") (v "0.5.2") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.21") (d #t) (k 0)))) (h "0khjipr02sblzl9jgfsrdcka1d3a3jzagl8l0r6gmv2aidy492fy") (f (quote (("fixture"))))))

