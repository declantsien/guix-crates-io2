(define-module (crates-io fl v- flv-k8-partition) #:use-module (crates-io))

(define-public crate-flv-k8-partition-0.1.0 (c (n "flv-k8-partition") (v "0.1.0") (d (list (d (n "k8-obj-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "1bcqcp1m51qr8pm00l1pvw7mzj85800rnv2sjnrkriffwvy3fw3r") (y #t)))

(define-public crate-flv-k8-partition-1.0.0 (c (n "flv-k8-partition") (v "1.0.0") (d (list (d (n "k8-obj-metadata") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "1n200nx30xvnq8md4sfaapl4q3cqhj219k2ii0fxdqng28x5zw44") (y #t)))

