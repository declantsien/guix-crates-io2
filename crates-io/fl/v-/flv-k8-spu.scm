(define-module (crates-io fl v- flv-k8-spu) #:use-module (crates-io))

(define-public crate-flv-k8-spu-0.1.0 (c (n "flv-k8-spu") (v "0.1.0") (d (list (d (n "k8-obj-core") (r "^0.1.0") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1wkn5dghfdcsh12yfk66xfbzxlqh80hpxwjn223xsyn3pdx6b99m") (y #t)))

(define-public crate-flv-k8-spu-1.0.0 (c (n "flv-k8-spu") (v "1.0.0") (d (list (d (n "k8-obj-core") (r "^1.0.0") (d #t) (k 0)) (d (n "k8-obj-metadata") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1dc336af6pyd9j185wnfc12hglpi80vgzp5cm3gvw5g1563s7d3r") (y #t)))

