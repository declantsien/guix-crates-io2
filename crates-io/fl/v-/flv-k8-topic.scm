(define-module (crates-io fl v- flv-k8-topic) #:use-module (crates-io))

(define-public crate-flv-k8-topic-0.1.0 (c (n "flv-k8-topic") (v "0.1.0") (d (list (d (n "k8-obj-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0dw8zh7gvkkxj89gnckb3hg0gj7vg7a09xddzs1vllp5lm0w2p2s") (y #t)))

(define-public crate-flv-k8-topic-1.0.0 (c (n "flv-k8-topic") (v "1.0.0") (d (list (d (n "k8-obj-metadata") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1xj5pw8khh1sh6vl82xq6z8xkgwmy6gsfy59q6233kchg49fl1pz") (y #t)))

