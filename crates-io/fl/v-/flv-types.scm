(define-module (crates-io fl v- flv-types) #:use-module (crates-io))

(define-public crate-flv-types-0.1.0 (c (n "flv-types") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "181p1ldqs8skbqnkjkhyzplsjfw1k7r80hjlih3zxfp93xgqkrrw") (y #t)))

(define-public crate-flv-types-0.2.2 (c (n "flv-types") (v "0.2.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "08cqzb7g8817iy28nb3kwrg3rj3ipxm9smwkwdg2fr4ywmgjnjkh") (y #t)))

