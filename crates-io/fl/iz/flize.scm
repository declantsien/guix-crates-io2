(define-module (crates-io fl iz flize) #:use-module (crates-io))

(define-public crate-flize-0.0.0 (c (n "flize") (v "0.0.0") (d (list (d (n "loom") (r "^0.3.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1ynky2j2rakdjcjd0dvdxsry4fhqxp8v9xm51j0md8dpfqm09af4")))

(define-public crate-flize-1.0.0 (c (n "flize") (v "1.0.0") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0m45y2bh3ca1n4gkk60x99mfidd9dh9fki1q3zypqc8a2k4jw6kf") (f (quote (("ebr") ("default" "ebr")))) (y #t)))

(define-public crate-flize-1.0.1 (c (n "flize") (v "1.0.1") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1ay9v8pmbc2ald486qim479r59ffqdw2b06vk3llywy07dgsmlfi") (f (quote (("ebr") ("default" "ebr"))))))

(define-public crate-flize-1.0.2 (c (n "flize") (v "1.0.2") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0bzkb8mnn4h6qj9la3vx5kf573siq5fxx8xp5x1mpvhmkximdqpd") (f (quote (("ebr") ("default" "ebr"))))))

(define-public crate-flize-1.0.3 (c (n "flize") (v "1.0.3") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1333vrhbqkpvbpscir5vkwf237h5a80mk58g2gy6k9a9dbmvvixz") (f (quote (("ebr") ("default" "ebr"))))))

(define-public crate-flize-1.0.4 (c (n "flize") (v "1.0.4") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1kq4553iqnccdcwhm1bbv6iaj2in7lfi4w33qh63ih4ijaqmvm03") (f (quote (("ebr") ("default" "ebr"))))))

(define-public crate-flize-2.0.0 (c (n "flize") (v "2.0.0") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1bjsm16a3g7cnwln669h2yw1b34xw4qmsvq6wjdlviag6prj9j3q") (f (quote (("ebr") ("default" "ebr"))))))

(define-public crate-flize-2.0.1 (c (n "flize") (v "2.0.1") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1bx55p257f1i0h599wfrlsry67m7jkpxlcs7w8c0b0ax4r4n9zrd") (f (quote (("ebr") ("default" "ebr"))))))

(define-public crate-flize-3.0.0 (c (n "flize") (v "3.0.0") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1lwak3ivswdy3cq1m9hl5czz1xsgnxhj5jnmkxriap0madzzvp0x")))

(define-public crate-flize-3.1.0 (c (n "flize") (v "3.1.0") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "019v8mz2yic3am16ncqs9ww0dgk7ihr4dwaalnd1s452jbw10ffx")))

(define-public crate-flize-3.2.0 (c (n "flize") (v "3.2.0") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1qbhy5wyp0dyvnvys35f70812gr96flp90x2xmmnf9vgxhis25s2")))

(define-public crate-flize-3.3.0 (c (n "flize") (v "3.3.0") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0zidyywsqcaaiqd0ggrzxrid1sbkgxximfq0x73l3k3fv9qqnlvc")))

(define-public crate-flize-4.0.0 (c (n "flize") (v "4.0.0") (d (list (d (n "generic-array") (r "=0.14.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "once_cell") (r "=1.4.1") (d #t) (k 0)))) (h "11bg65df9m1kidm4zkwvwk4b0749skxbdxxi7vx9xflcp66hcl7j") (y #t)))

(define-public crate-flize-4.0.1 (c (n "flize") (v "4.0.1") (d (list (d (n "generic-array") (r "=0.14.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "once_cell") (r "=1.4.1") (d #t) (k 0)))) (h "14zbvafh5jf16irkwbpry93d6ynm3v16pc5gjg3a2lh4vmi9k65z")))

(define-public crate-flize-4.1.0 (c (n "flize") (v "4.1.0") (d (list (d (n "generic-array") (r "=0.14.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0mcm89mc1f64ijavvprdn8i41mqiwmja4dc7vikhsfgcdwbkmw03")))

(define-public crate-flize-4.1.1 (c (n "flize") (v "4.1.1") (d (list (d (n "generic-array") (r "=0.14.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "00jccjjhq512bwqpdkdcd6lbhnkvcjqp954yqcbxgyqq56f0h8wv")))

(define-public crate-flize-4.1.2 (c (n "flize") (v "4.1.2") (d (list (d (n "generic-array") (r "=0.14.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "17280wwhaw2i99kpxay91f3zh7v2dn3an5dgclm207hnjllsn77r")))

(define-public crate-flize-4.2.0 (c (n "flize") (v "4.2.0") (d (list (d (n "generic-array") (r "=0.14.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "03jchq5flx6qjr6cf96lsiriky00xq4c1p6538sv8wyfchhzm8rf") (f (quote (("fast-barrier" "libc" "winapi") ("default" "fast-barrier"))))))

(define-public crate-flize-4.2.1 (c (n "flize") (v "4.2.1") (d (list (d (n "generic-array") (r "=0.14.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0ak6sqvy98jqlklix3vg421pdd5jj25dyzm35dmx5s0xbjsjfrh3") (f (quote (("fast-barrier" "libc" "winapi") ("default" "fast-barrier"))))))

(define-public crate-flize-4.2.2 (c (n "flize") (v "4.2.2") (d (list (d (n "generic-array") (r "=0.14.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1amhag6vraw1n6zvd83rxpp4hg173hvhvwqbsk89cihfan5gwchd") (f (quote (("fast-barrier" "libc" "winapi") ("default" "fast-barrier"))))))

(define-public crate-flize-4.2.3 (c (n "flize") (v "4.2.3") (d (list (d (n "generic-array") (r "=0.14.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.77") (o #t) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "once_cell") (r "^1.5.2") (o #t) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tinyvec") (r "^1.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi"))) (o #t) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1harwps2gga6zwd09yabw2qhvhwvxswm3z8l9v3r24js76hhqic7") (f (quote (("std") ("fast-barrier" "std" "libc" "winapi" "once_cell") ("default" "std" "fast-barrier"))))))

