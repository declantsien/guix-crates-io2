(define-module (crates-io fl at flatty-base) #:use-module (crates-io))

(define-public crate-flatty-base-0.1.0-alpha.0 (c (n "flatty-base") (v "0.1.0-alpha.0") (h "0xl4ircyy1299b8n64cbp9490vdwmgnqin78kwliwf75bb9q8yp8")))

(define-public crate-flatty-base-0.1.0-alpha.1 (c (n "flatty-base") (v "0.1.0-alpha.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0x662daqxa461nhfiigvxzyj9cyc9wlrv1kbpm9qwv06k1pp2w6x") (f (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-flatty-base-0.1.0-alpha.2 (c (n "flatty-base") (v "0.1.0-alpha.2") (d (list (d (n "stavec") (r "^0.2.0-rc.0") (f (quote ("repr-c"))) (k 0)))) (h "1h8z2cfnvdkwm6dhzi6d7jaclz7fi2ax1sf0f1hhn976rrqwnnv9") (f (quote (("std" "stavec/std") ("default" "std"))))))

(define-public crate-flatty-base-0.1.0-alpha.3 (c (n "flatty-base") (v "0.1.0-alpha.3") (d (list (d (n "stavec") (r "^0.2.0") (f (quote ("repr-c"))) (k 0)))) (h "1kiw1kww8af0zkih9c8pv5z28p6fryap6qz5r218p6harxfnyfvr") (f (quote (("std" "stavec/std") ("default" "std"))))))

(define-public crate-flatty-base-0.1.0-alpha.5 (c (n "flatty-base") (v "0.1.0-alpha.5") (d (list (d (n "stavec") (r "^0.3.0") (f (quote ("repr-c"))) (k 0)))) (h "1p2jhp7bj86sfjap1vfy7307sb9wvn5yw07q4s7a0jpikaagdadx") (f (quote (("std" "stavec/std") ("default" "std"))))))

(define-public crate-flatty-base-0.1.0-alpha.6 (c (n "flatty-base") (v "0.1.0-alpha.6") (d (list (d (n "stavec") (r "^0.3.0") (f (quote ("repr-c"))) (k 0)))) (h "1ldvfxrkncbaha08kgs858xfr3c9qy09230h6ihpyz6jflyllwb2") (f (quote (("std" "stavec/std") ("default" "std"))))))

(define-public crate-flatty-base-0.1.0-alpha.7 (c (n "flatty-base") (v "0.1.0-alpha.7") (d (list (d (n "stavec") (r "^0.3.0") (f (quote ("repr-c"))) (k 0)))) (h "1rvhwkxz8n13amvaandlcjims9yaqw7f3nxvff8rhb8c653f12zr") (f (quote (("std" "stavec/std") ("default" "std"))))))

(define-public crate-flatty-base-0.1.0-rc.0 (c (n "flatty-base") (v "0.1.0-rc.0") (d (list (d (n "stavec") (r "^0.3.0") (f (quote ("repr-c"))) (k 0)))) (h "03iwlwn32wvz4y7wic4hvgs380nn3kssq5pa03lxzkj3j50d9q11") (f (quote (("std" "stavec/std") ("default" "std"))))))

(define-public crate-flatty-base-0.1.0-rc.1 (c (n "flatty-base") (v "0.1.0-rc.1") (d (list (d (n "stavec") (r "^0.3.0") (f (quote ("repr-c"))) (k 0)))) (h "1haaa4yf6diwd9s5g8i7fdpvmsy4c0k33rwcgkizjbqg8yrvx7hy") (f (quote (("std" "stavec/std") ("default" "std"))))))

(define-public crate-flatty-base-0.1.0-rc.2 (c (n "flatty-base") (v "0.1.0-rc.2") (d (list (d (n "stavec") (r "^0.3.0") (f (quote ("repr-c"))) (k 0)))) (h "173pkrxiy5jgv3hx45bxq7im1nsr46hwznq6hykn441lxmsh3lg1") (f (quote (("std" "stavec/std") ("default" "std"))))))

(define-public crate-flatty-base-0.1.0-rc.4 (c (n "flatty-base") (v "0.1.0-rc.4") (d (list (d (n "stavec") (r "^0.3.0") (f (quote ("repr-c"))) (k 0)))) (h "036m0mly7hj4mzdjnp8n4k6nb12ar0vji9csvz1b11w7qngvrh4m") (f (quote (("std" "alloc" "stavec/std") ("default" "std") ("alloc"))))))

