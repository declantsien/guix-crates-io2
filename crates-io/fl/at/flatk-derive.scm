(define-module (crates-io fl at flatk-derive) #:use-module (crates-io))

(define-public crate-flatk-derive-0.1.0 (c (n "flatk-derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wg45ya32x5n7bph99dmxdqvzhfwd1ra1iyanrpni94g4izwnwyn")))

(define-public crate-flatk-derive-0.2.0 (c (n "flatk-derive") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pf5ydph00y128c8wbyi3g2bljfb8k4x6zxr8yh5vxdnl3vxpmmk")))

(define-public crate-flatk-derive-0.2.1 (c (n "flatk-derive") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "123xj2x9d547k41f2p53mlm2bplmnpk6b4ib6q51pfbjndviil8q")))

(define-public crate-flatk-derive-0.2.2 (c (n "flatk-derive") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yvlk7f8hlzj30gf456j56yigb6ia7mvaw2qrv6f6pdgwxr4i9av")))

(define-public crate-flatk-derive-0.2.3 (c (n "flatk-derive") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r5zywcnjr1id7zyd67cp93d2fkxcd9qfsh36d11dpvdmx7aipxp")))

(define-public crate-flatk-derive-0.3.0 (c (n "flatk-derive") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01hng5yzvqh9a38w81k69dk5q6522pqhx5kb0mg9v2gl03g746dg")))

