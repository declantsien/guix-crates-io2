(define-module (crates-io fl at flatdata) #:use-module (crates-io))

(define-public crate-flatdata-0.1.0 (c (n "flatdata") (v "0.1.0") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "0p3djd3axdsxi8ag40l87sgxanvx1zv0hn1g1qlh51040an995sb")))

(define-public crate-flatdata-0.1.1 (c (n "flatdata") (v "0.1.1") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "136n4szr0fmprynjhkvmqb9l16if51v4904hgy72k236rk2cldzs")))

(define-public crate-flatdata-0.1.2 (c (n "flatdata") (v "0.1.2") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "016llhx42kfbra8nqb343id714fxi78b7yrqj0bnqh03cy6yml2f")))

(define-public crate-flatdata-0.1.3 (c (n "flatdata") (v "0.1.3") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "1yh3lk9ri91cai34rpaifz2rjbwxynsb5agcg4b7smbzlgsxq8am")))

(define-public crate-flatdata-0.1.4 (c (n "flatdata") (v "0.1.4") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "1m5z8ajpdg9mk57pg2dznn69xyknk1ycirds835rakf896159k6v")))

(define-public crate-flatdata-0.2.0 (c (n "flatdata") (v "0.2.0") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "1sj2xwzsvr42haknf5qlamdzbr5rr05sh11kwnq1awh4lnfg11ba")))

(define-public crate-flatdata-0.2.1 (c (n "flatdata") (v "0.2.1") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "1gykk8bvgc0flzyh3wa5283hxl041m5fqg6k4k8xyzyric131v2k")))

(define-public crate-flatdata-0.3.0 (c (n "flatdata") (v "0.3.0") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "045496rhfd2f6f5qfsnn9i2bdx4xx9aif35xbhjlazx4ipwiqrlz")))

(define-public crate-flatdata-0.3.1 (c (n "flatdata") (v "0.3.1") (d (list (d (n "diff") (r "^0.1.11") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "16b905kw01hlny2sfqx1jh1rldwny9argxdhzwva696xhpx773fz")))

(define-public crate-flatdata-0.5.0 (c (n "flatdata") (v "0.5.0") (d (list (d (n "diff") (r "^0.1.11") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1z648zmlmwlz7vda85rsszh4hsm1n2ljj7rvkzbmn91drhnqf9b5")))

(define-public crate-flatdata-0.5.1 (c (n "flatdata") (v "0.5.1") (d (list (d (n "diff") (r "^0.1.11") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "03wnqwzisxrqsg6qfpxmz3javwj8l09vyf3h7adwzjj846xacb0w")))

(define-public crate-flatdata-0.5.2 (c (n "flatdata") (v "0.5.2") (d (list (d (n "diff") (r "^0.1.11") (d #t) (k 0)) (d (n "memmap2") (r "^0.2.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "17dr02kcc4915mmiqz4f680qqayimh29bgzhw8m431zr52439j9b")))

(define-public crate-flatdata-0.5.3 (c (n "flatdata") (v "0.5.3") (d (list (d (n "diff") (r "^0.1.11") (d #t) (k 0)) (d (n "memmap2") (r "^0.2.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1s4hxa57v9f9iwsnwygszdviaz51s1zr5ycwi9hlrjil12xxac9c")))

(define-public crate-flatdata-0.5.5 (c (n "flatdata") (v "0.5.5") (d (list (d (n "diff") (r "^0.1.11") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1c5pnmdpswgih9fsya2l6sahinfihxidi09md6sg91w78965n4sk")))

