(define-module (crates-io fl at flat-tree) #:use-module (crates-io))

(define-public crate-flat-tree-1.0.0 (c (n "flat-tree") (v "1.0.0") (h "12m7ml9vw99q2sk2mhx5bs4mq64gsnpwz60fwzw52c1njlra85h9")))

(define-public crate-flat-tree-1.1.0 (c (n "flat-tree") (v "1.1.0") (h "0mj5qpcrslzsdx8kg77591kcxvswggmzi9g4kmwz6m461nymjmlb")))

(define-public crate-flat-tree-2.0.0 (c (n "flat-tree") (v "2.0.0") (h "0rbzw0mxgzxh8vsw1szw1l1cb2pandnb2d9c8ap6lxp1mbzq57m9")))

(define-public crate-flat-tree-2.0.1 (c (n "flat-tree") (v "2.0.1") (h "0ri64d0jbscvykv4ggc183f88j927csa1lb19fag8mckf3336sir")))

(define-public crate-flat-tree-2.1.0 (c (n "flat-tree") (v "2.1.0") (h "062b5hbwxklcg3dr8mvswhrgja94ihv3k1i9vhddlxpwr9vi03b9")))

(define-public crate-flat-tree-3.0.0 (c (n "flat-tree") (v "3.0.0") (h "0bffc8r9cllyzy25dlh0wwvip9wqbp41m1y397nw7rs0fvrs76ys")))

(define-public crate-flat-tree-3.1.0 (c (n "flat-tree") (v "3.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "05hpcwyb1ihdv7zj6cyl8bmf8siwxixxlyra6i13p5qs7rbghnv2")))

(define-public crate-flat-tree-3.2.0 (c (n "flat-tree") (v "3.2.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "17b7ny98dp5k98dfxs421vqmc6wa9ik81ghpfffnan361adzbirs")))

(define-public crate-flat-tree-3.3.0 (c (n "flat-tree") (v "3.3.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "1jmyb0wn0fcjmxdp2mqx48172qhdxd5z8sp4m0wf00v7hpfdniv1")))

(define-public crate-flat-tree-3.4.0 (c (n "flat-tree") (v "3.4.0") (h "0yp3i52kmrkdsf7i2yk27r07w7nx5v42qi8jjgz3jm7sibyz14nr")))

(define-public crate-flat-tree-3.5.0 (c (n "flat-tree") (v "3.5.0") (h "0b13112p0as66rbqhaq8kyc96hzm88hsxx4rxx30ni6l5fq5qnbn")))

(define-public crate-flat-tree-3.6.0 (c (n "flat-tree") (v "3.6.0") (h "13ag907n12c5zzl01cx8g2778vdan3a3c3a672y17vm09y0rgbg9")))

(define-public crate-flat-tree-3.7.0 (c (n "flat-tree") (v "3.7.0") (h "0v39xjqv8mdlzviys2b9qvy0b43rl06mrap1za2qja4pz0xifvph")))

(define-public crate-flat-tree-4.0.0 (c (n "flat-tree") (v "4.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "182f68ad3yvpy9iiam9rr5rpppazi1dyga5i18xi67d6ikc3hkfc")))

(define-public crate-flat-tree-4.0.1 (c (n "flat-tree") (v "4.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0k44jfwfcx35xdk6pwl8mfi5a73b0s0drbc8h6pdlbr4z91l1d6x")))

(define-public crate-flat-tree-4.1.0 (c (n "flat-tree") (v "4.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1psgbc0cccir5ngdyvi7sxd06xzrbhkwmzsvlvaj55nbqpm6377k")))

(define-public crate-flat-tree-5.0.0 (c (n "flat-tree") (v "5.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1r79llws4yjcbsb048bv4anl3x9hzf1b13j5s7rqb5bd9c6jhpgm")))

(define-public crate-flat-tree-6.0.0 (c (n "flat-tree") (v "6.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0a2m10k81kjxm6xi201jy917af1kak9ydir1ln7qdwi502phg4i1")))

