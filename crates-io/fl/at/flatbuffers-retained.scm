(define-module (crates-io fl at flatbuffers-retained) #:use-module (crates-io))

(define-public crate-flatbuffers-retained-0.1.0 (c (n "flatbuffers-retained") (v "0.1.0") (d (list (d (n "flatbuffers") (r "~22.10") (d #t) (k 0)))) (h "1wasb4clbn96n8fbkrgnz7j6sgls32bx8i4lcj9fachj4acnxrfc")))

(define-public crate-flatbuffers-retained-0.2.0 (c (n "flatbuffers-retained") (v "0.2.0") (d (list (d (n "flatbuffers") (r "~22.10") (d #t) (k 0)))) (h "1n7qsfwiqfv61snf266x9klxyckngg3f80c626iq2p5fnq4yypk4")))

(define-public crate-flatbuffers-retained-0.3.0 (c (n "flatbuffers-retained") (v "0.3.0") (d (list (d (n "flatbuffers") (r "~23.5") (d #t) (k 0)))) (h "0w4644p3rj9mhcq3i2g9km4d1lavhlj5yaki4yskq640s499k8ln")))

