(define-module (crates-io fl at flat-bytes) #:use-module (crates-io))

(define-public crate-flat-bytes-0.1.0 (c (n "flat-bytes") (v "0.1.0") (d (list (d (n "flat-bytes-derive") (r "^0.1") (d #t) (k 0)))) (h "1piqlfn5crdf9cjdqg1d5mnxm590mhcr7lh46266lhlsb0v9xbhg")))

(define-public crate-flat-bytes-0.1.1 (c (n "flat-bytes") (v "0.1.1") (d (list (d (n "flat-bytes-derive") (r "^0.1") (d #t) (k 0)))) (h "0mv5n9xn1yk9kqjl45cp0zadldl9imf6pgr5b0bv1i87cwa9j1m7")))

