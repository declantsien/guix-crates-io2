(define-module (crates-io fl at flat_device_tree) #:use-module (crates-io))

(define-public crate-flat_device_tree-2.1.0 (c (n "flat_device_tree") (v "2.1.0") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)))) (h "0kk09krh1q56v9c3g0ir2k4hph1s2zwn0g7q7v0iqgw8j57jl06s") (f (quote (("string-dedup"))))))

(define-public crate-flat_device_tree-2.1.1 (c (n "flat_device_tree") (v "2.1.1") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)))) (h "08q7qrl30ncsc1yz4lziid8np547pn92ff3yyjw025rd13ryaiwf") (f (quote (("string-dedup"))))))

(define-public crate-flat_device_tree-3.0.0 (c (n "flat_device_tree") (v "3.0.0") (h "09ylfiwb73is6m8wy3fa4czd81ip62vr8b3s5v59kl2r68i4zr31") (f (quote (("pretty-printing"))))))

(define-public crate-flat_device_tree-3.1.0 (c (n "flat_device_tree") (v "3.1.0") (h "1pc2q3gk917vqgid8fxj9vdc58ri381gdhmrzbz91g1wq7nxwc13") (f (quote (("pretty-printing"))))))

