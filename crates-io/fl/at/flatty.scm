(define-module (crates-io fl at flatty) #:use-module (crates-io))

(define-public crate-flatty-0.1.0-alpha.0 (c (n "flatty") (v "0.1.0-alpha.0") (d (list (d (n "base_") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "flatty-base")) (d (n "macros") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "flatty-macros")))) (h "1wzivalm14qb7jr30nyr0r8qw7gix3wvgn65k8w8mwffmin07dbl")))

(define-public crate-flatty-0.1.0-alpha.1 (c (n "flatty") (v "0.1.0-alpha.1") (d (list (d (n "base") (r "^0.1.0-alpha.1") (k 0) (p "flatty-base")) (d (n "macros") (r "^0.1.0-alpha.1") (d #t) (k 0) (p "flatty-macros")))) (h "0s217dd5jgdk3nvri89hjg6jx2kqx1qjli8asgaxnsrr2x9zg1sc") (f (quote (("std" "base/std") ("default" "std"))))))

(define-public crate-flatty-0.1.0-alpha.2 (c (n "flatty") (v "0.1.0-alpha.2") (d (list (d (n "base") (r "^0.1.0-alpha.2") (k 0) (p "flatty-base")) (d (n "macros") (r "^0.1.0-alpha.2") (d #t) (k 0) (p "flatty-macros")) (d (n "portable") (r "^0.1.0-alpha.2") (k 0) (p "flatty-portable")))) (h "0zx8r4b6875dcmnyhwyx34cr16ijsxrzh1hnim899s133lfbm099") (f (quote (("std" "base/std") ("default" "std"))))))

(define-public crate-flatty-0.1.0-alpha.3 (c (n "flatty") (v "0.1.0-alpha.3") (d (list (d (n "base") (r "^0.1.0-alpha.3") (k 0) (p "flatty-base")) (d (n "macros") (r "^0.1.0-alpha.3") (d #t) (k 0) (p "flatty-macros")) (d (n "portable") (r "^0.1.0-alpha.3") (k 0) (p "flatty-portable")))) (h "1rl96pwqjck0zx16bl3a00by2zzisf9wnir1d4npgs6awmispyia") (f (quote (("std" "base/std") ("default" "std")))) (r "1.64")))

(define-public crate-flatty-0.1.0-alpha.4 (c (n "flatty") (v "0.1.0-alpha.4") (d (list (d (n "flatty-base") (r "^0.1.0-alpha.3") (k 0)) (d (n "flatty-macros") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "flatty-portable") (r "^0.1.0-alpha.4") (k 0)))) (h "1i241rq29l74hh243i4izrs00ps3j7yv25i706fl0ss2yy9g8ng4") (f (quote (("std" "flatty-base/std") ("default" "std")))) (r "1.64")))

(define-public crate-flatty-0.1.0-alpha.5 (c (n "flatty") (v "0.1.0-alpha.5") (d (list (d (n "flatty-base") (r "^0.1.0-alpha.5") (k 0)) (d (n "flatty-macros") (r "^0.1.0-alpha.5") (d #t) (k 0)) (d (n "flatty-portable") (r "^0.1.0-alpha.5") (k 0)))) (h "19d346fxdrzy39k2968ziacw5h4cxbr5qwqmgvmkighmhwq1i8af") (f (quote (("std" "flatty-base/std" "flatty-portable/std") ("default" "std")))) (r "1.69")))

(define-public crate-flatty-0.1.0-alpha.7 (c (n "flatty") (v "0.1.0-alpha.7") (d (list (d (n "flatty-base") (r "^0.1.0-alpha.7") (k 0)) (d (n "flatty-macros") (r "^0.1.0-alpha.7") (d #t) (k 0)) (d (n "flatty-portable") (r "^0.1.0-alpha.7") (k 0)))) (h "1hgmm0v51yafgk4jy6kx40z2cw513svbnbds975ap6z1dlm9ww9j") (f (quote (("std" "flatty-base/std" "flatty-portable/std") ("default" "std")))) (r "1.69")))

(define-public crate-flatty-0.1.0-rc.0 (c (n "flatty") (v "0.1.0-rc.0") (d (list (d (n "flatty-base") (r "^0.1.0-rc.0") (k 0)) (d (n "flatty-macros") (r "^0.1.0-rc.0") (d #t) (k 0)) (d (n "flatty-portable") (r "^0.1.0-rc.0") (k 0)))) (h "0fyr4d9mrs1pzwkqwbpwh8dmxxsj4flm20id00ag9whq0wn06df4") (f (quote (("std" "flatty-base/std" "flatty-portable/std") ("default" "std")))) (r "1.69")))

(define-public crate-flatty-0.1.0-rc.1 (c (n "flatty") (v "0.1.0-rc.1") (d (list (d (n "flatty-base") (r "^0.1.0-rc.1") (k 0)) (d (n "flatty-macros") (r "^0.1.0-rc.1") (d #t) (k 0)) (d (n "flatty-portable") (r "^0.1.0-rc.1") (k 0)))) (h "1c91f4ch2an06vx980a3m3dmmclja62hmyh27y9isrv8m613k72x") (f (quote (("std" "flatty-base/std" "flatty-portable/std") ("default" "std")))) (r "1.69")))

(define-public crate-flatty-0.1.0-rc.2 (c (n "flatty") (v "0.1.0-rc.2") (d (list (d (n "flatty-base") (r "^0.1.0-rc.2") (k 0)) (d (n "flatty-macros") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "flatty-portable") (r "^0.1.0-rc.2") (k 0)))) (h "052c0w8qs3cqn8vv5chd14s9zwjrv4y3aq9hvf0lasfb1q5f8sri") (f (quote (("std" "flatty-base/std" "flatty-portable/std") ("default" "std")))) (r "1.69")))

(define-public crate-flatty-0.1.0-rc.4 (c (n "flatty") (v "0.1.0-rc.4") (d (list (d (n "flatty-base") (r "^0.1.0-rc.4") (k 0)) (d (n "flatty-macros") (r "^0.1.0-rc.4") (d #t) (k 0)) (d (n "flatty-portable") (r "^0.1.0-rc.4") (k 0)))) (h "0swmjv81jzwba0vlrg3i92xcyvbjp3547m2qxzj9scb7gh0fxak7") (f (quote (("std" "alloc" "flatty-base/std" "flatty-portable/std") ("default" "std") ("alloc" "flatty-base/alloc")))) (r "1.69")))

