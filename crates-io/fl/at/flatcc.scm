(define-module (crates-io fl at flatcc) #:use-module (crates-io))

(define-public crate-flatcc-22.10.26 (c (n "flatcc") (v "22.10.26") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flatc-rust") (r "^0.2") (d #t) (k 0)))) (h "1s2slvnbjlyp809c208370nzfk72z7cmryx6x5qk1xlz80qw8czw")))

(define-public crate-flatcc-23.1.21 (c (n "flatcc") (v "23.1.21") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flatc-rust") (r "^0.2") (d #t) (k 0)))) (h "1x0984xgyiwv960dwszzrm2gaxvs6hpx7slnh7h60sak1mc83xyw")))

(define-public crate-flatcc-24.3.25 (c (n "flatcc") (v "24.3.25") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "flatc-rust") (r "^0.2") (d #t) (k 0)))) (h "1m17gbv1vvn12fhrwwnzh5ab2h5qh0kawc17hd3vf5m8mb7gbzbs")))

