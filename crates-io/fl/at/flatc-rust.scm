(define-module (crates-io fl at flatc-rust) #:use-module (crates-io))

(define-public crate-flatc-rust-0.1.0 (c (n "flatc-rust") (v "0.1.0") (d (list (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "03qclfgvxn3fa80crk66kffy92jslq83nihs7mijkyliipvf6say")))

(define-public crate-flatc-rust-0.1.1 (c (n "flatc-rust") (v "0.1.1") (d (list (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1601rrjxqfpi8lydv34fwz6hrlwb2r1w5v78r3y9s92w3a3y67ch")))

(define-public crate-flatc-rust-0.1.2 (c (n "flatc-rust") (v "0.1.2") (d (list (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1vbabxssp8968596vf15pk7z1h2cpxjlfnrhsnh6lyzfbgc2wymk")))

(define-public crate-flatc-rust-0.1.3 (c (n "flatc-rust") (v "0.1.3") (d (list (d (n "log") (r ">=0.4.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "16mxjlwvnvwnnal8lsnkqzv0c744sxb9npi5w02w9k1viv32mkpx")))

(define-public crate-flatc-rust-0.2.0 (c (n "flatc-rust") (v "0.2.0") (d (list (d (n "log") (r ">=0.4.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "07c50zr15rlqg8aca45x3qi5ljncji1yggj8mwvv5xbfj8ki5rjp")))

(define-public crate-flatc-rust-0.2.1-alpha.0 (c (n "flatc-rust") (v "0.2.1-alpha.0") (d (list (d (n "log") (r ">=0.4.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1mw03m31xd7sw081bmpnggwqn6qglwwq6hly5sgyswgmi3ads4h5") (y #t)))

