(define-module (crates-io fl at flatc) #:use-module (crates-io))

(define-public crate-flatc-0.1.0+1.12.0 (c (n "flatc") (v "0.1.0+1.12.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0cdx4abqz7c5y4fxswq2hl31c07j3x5pnlqn77hjk225rdqg0ss7")))

(define-public crate-flatc-0.1.1+1.12.0 (c (n "flatc") (v "0.1.1+1.12.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0jxa7lff0wlwybnixx15j0w8fn5vpgpm624hbr38kpnw874wh989")))

(define-public crate-flatc-0.2.0+1.12.0 (c (n "flatc") (v "0.2.0+1.12.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1capq6frdiy19bg18ni87fb8s1cyxfqk531bjvqki24mxgv9h88b")))

(define-public crate-flatc-0.2.0+2.0.0 (c (n "flatc") (v "0.2.0+2.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1807kzxr70blhrdxrjdfz8avyzvravsizmzpzy95q40bk543j2rd")))

(define-public crate-flatc-0.2.1+2.0.0 (c (n "flatc") (v "0.2.1+2.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1m77m8ysl35yrj9lafrh524y1ryavd8mfg3imwpmzc7nad5qilcc")))

(define-public crate-flatc-0.2.1+23.5.8 (c (n "flatc") (v "0.2.1+23.5.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "00wn3x6gw4x0chff19bk9ka5lbjg107z1wvikckss65rqrl2gbn8")))

(define-public crate-flatc-0.2.2+23.5.26 (c (n "flatc") (v "0.2.2+23.5.26") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "07kjqgca401m95l1sfz902v0758k02c0ywkr7p73w8jq0c2s7fyl")))

