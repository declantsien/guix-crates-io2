(define-module (crates-io fl at flatfish) #:use-module (crates-io))

(define-public crate-flatfish-0.1.0 (c (n "flatfish") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "1s4y495hmzb2qmk6p66fahhrknjjfa6dzl65l783y800yf6gb64l")))

