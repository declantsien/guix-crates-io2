(define-module (crates-io fl at flatty-macros) #:use-module (crates-io))

(define-public crate-flatty-macros-0.1.0-alpha.0 (c (n "flatty-macros") (v "0.1.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04hmn77lpb2yz2gww62icmzdgfh0cdvylsp0nvbvg5hgzs7jayl4")))

(define-public crate-flatty-macros-0.1.0-alpha.1 (c (n "flatty-macros") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "009kca4n9kfkjypsmjmfwk5rlq069pbpg5ci1akbhfvpnn6wfbbg")))

(define-public crate-flatty-macros-0.1.0-alpha.2 (c (n "flatty-macros") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "193q0m5zqvrgqcbfvq5m3l39qzdc0m7i551p0r87fhmn4k1dba9g")))

(define-public crate-flatty-macros-0.1.0-alpha.3 (c (n "flatty-macros") (v "0.1.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "140xcpi5h90q6mfwvfjljgkdxx5i0qykf44z5hi1l302kqirn043")))

(define-public crate-flatty-macros-0.1.0-alpha.5 (c (n "flatty-macros") (v "0.1.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0migwvykwk7cj04f4qg1mdzij77gv3dzp91yc6jj986bqs9ixd3l")))

(define-public crate-flatty-macros-0.1.0-alpha.7 (c (n "flatty-macros") (v "0.1.0-alpha.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cbrr7ajadgqqiq7h2j1rpz5mhc2jz0qdypxb83v1gkmkis63p1d")))

(define-public crate-flatty-macros-0.1.0-rc.0 (c (n "flatty-macros") (v "0.1.0-rc.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kd2ldnxsd7ypw3ib0z20q63chrkwg6mpss92bsk05cj0wzfch9m")))

(define-public crate-flatty-macros-0.1.0-rc.1 (c (n "flatty-macros") (v "0.1.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1avif74wwfsm14yzrlcyr47fxygjfby4lalcxv2qqsb6rm5y0610")))

(define-public crate-flatty-macros-0.1.0-rc.2 (c (n "flatty-macros") (v "0.1.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "199lx220fzwkic3p6svima6fcdds2llayxz6rv7rv18rnxq85yy0")))

(define-public crate-flatty-macros-0.1.0-rc.4 (c (n "flatty-macros") (v "0.1.0-rc.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16iwswsijx31hb43h6gmp665ai7zlakd2hrxnbpxxgpzcg929nb3")))

