(define-module (crates-io fl at flat-token) #:use-module (crates-io))

(define-public crate-flat-token-0.0.0 (c (n "flat-token") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0c6kjg95z57wna2h7j83l70a3jn3sfn0w7saqdmdibrl20mqis3p")))

(define-public crate-flat-token-0.0.1 (c (n "flat-token") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)))) (h "0yq5xxwyak5n4hizcymf8akssnflxf563wxbpyg3y11gbvbcxs3f")))

