(define-module (crates-io fl at flat-multimap) #:use-module (crates-io))

(define-public crate-flat-multimap-0.1.0 (c (n "flat-multimap") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.13") (f (quote ("raw"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1n7p7xk2simvjia94qw3idp2v0sxhm66766xwmdjsmgdblghr694") (r "1.61")))

(define-public crate-flat-multimap-0.2.0 (c (n "flat-multimap") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.13") (f (quote ("raw"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1y5yxcvx8abych9kz64wxr96g370h0fk1b7ri9syh8jf57c1l5c6") (s 2) (e (quote (("rayon" "dep:rayon" "hashbrown/rayon")))) (r "1.61")))

