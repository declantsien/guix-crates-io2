(define-module (crates-io fl at flattree) #:use-module (crates-io))

(define-public crate-flattree-1.0.0 (c (n "flattree") (v "1.0.0") (h "06m97rand4z6khh2g0cni2zigqx9xv49jfmiwl9dsgvllc8vzbpk")))

(define-public crate-flattree-1.1.0 (c (n "flattree") (v "1.1.0") (h "16c5s0w55ar0jirf4fgjia1qxfzhlz998z6rq351w430hdf59l3b")))

(define-public crate-flattree-2.0.0 (c (n "flattree") (v "2.0.0") (h "1h0mlg28y0la0qx5yhpcw5jisj4rrsf99ida60xvsnbm0slm7dp3")))

