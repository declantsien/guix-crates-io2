(define-module (crates-io fl at flat-rs) #:use-module (crates-io))

(define-public crate-flat-rs-0.0.1 (c (n "flat-rs") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16d44rdkv0b6yhd31phwmx28jk5mzsfzxjcv2yi7s1jmxgw5zfgp")))

(define-public crate-flat-rs-0.0.2 (c (n "flat-rs") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1d2fzgwqay15c0n2fnxrxkysnadwfw6qm0cyz344j35hhipv84zf")))

(define-public crate-flat-rs-0.0.6 (c (n "flat-rs") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "14dzsgn1lgvlca69cicps22drmk43bcsh7926pq5f2y5nakm0ifx")))

(define-public crate-flat-rs-0.0.7 (c (n "flat-rs") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1bwpjzvn834wsrnnlfrk7r222m7rhf017rjk8v65cz19gx7xz45a")))

(define-public crate-flat-rs-0.0.10 (c (n "flat-rs") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0jbflx9gyba4q8vdsf9w0cqkzz9dq2fjwadpc44b8wb85mfh1q2m")))

(define-public crate-flat-rs-0.0.20 (c (n "flat-rs") (v "0.0.20") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0cw77f0lvrwfkmkarhkckwgpa0nb82kixiyaq62gks4mj45ccgcf")))

(define-public crate-flat-rs-0.0.21 (c (n "flat-rs") (v "0.0.21") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ivgqakg5yvnyfrchznkd72mzry2rk5js9mbrv7l113s3vv9s725")))

(define-public crate-flat-rs-0.0.27 (c (n "flat-rs") (v "0.0.27") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "15z0jdrzigs70kzv4v9yf5akr0jsi01rn4y5na0kb8r9an64bz3w")))

(define-public crate-flat-rs-1.0.0-alpha (c (n "flat-rs") (v "1.0.0-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0w617kmg171xzk8ym3fk6vyk8dz7pg3wp6z0lxifs8gqs8g8kn88") (r "1.66.1")))

(define-public crate-flat-rs-1.0.1-alpha (c (n "flat-rs") (v "1.0.1-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0ayixfqmbb95h13030w163s8vl0vqxb1k6h5mln8949gqpyskpx3") (r "1.66.1")))

(define-public crate-flat-rs-1.0.2-alpha (c (n "flat-rs") (v "1.0.2-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1hlxkp8xr04q2kq4a2agdnwmbs6c8rzzp4wwv0rghn98i99pirmx") (r "1.66.1")))

(define-public crate-flat-rs-1.0.3-alpha (c (n "flat-rs") (v "1.0.3-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0saakfvkvfmynlrjcfc9w6nfh7k3j8835rgxxlj33vlxil20c7xz") (r "1.66.1")))

(define-public crate-flat-rs-1.0.4-alpha (c (n "flat-rs") (v "1.0.4-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1i692s8kvjflmnix42dbz4ki1ifl8za29yc3kv0b8pmq949dwj1j") (r "1.66.1")))

(define-public crate-flat-rs-1.0.5-alpha (c (n "flat-rs") (v "1.0.5-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0q13vmibgpv6qiavb0arfahxs4nxlwas1v9v2fxhbhywcwgnyzym") (r "1.66.1")))

(define-public crate-flat-rs-1.0.6-alpha (c (n "flat-rs") (v "1.0.6-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0066jk801a4d2wjf2yw8d6k8bf7rinryxdb8v4w49ggm0mrw0fzb") (r "1.66.1")))

(define-public crate-flat-rs-1.0.7-alpha (c (n "flat-rs") (v "1.0.7-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0b20clzfz5r9hrrwlzllxgzp0jd9a884kxmgra915rz3q4nf4yq2") (r "1.66.1")))

(define-public crate-flat-rs-1.0.8-alpha (c (n "flat-rs") (v "1.0.8-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1xsvljp7g6ig6ilwm854r8h8kcfs6kmajzvkvnrmd0wx7kikxylc") (r "1.66.1")))

(define-public crate-flat-rs-1.0.9-alpha (c (n "flat-rs") (v "1.0.9-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "12z1cg1grf4lib4n5cc86g2cvghaafb537x3a2kfcg9v00d90mjr") (r "1.66.1")))

(define-public crate-flat-rs-1.0.10-alpha (c (n "flat-rs") (v "1.0.10-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0g5chs0l30pj83x2vh5w2ii7950fb6n6h6wj63n9w8n8isj9gx8d") (r "1.66.1")))

(define-public crate-flat-rs-1.0.11-alpha (c (n "flat-rs") (v "1.0.11-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0x8i68j5593a8fpg7va8h3ccad4iq0irs0wqamrfwkwk7215l24p") (r "1.66.1")))

(define-public crate-flat-rs-1.0.12-alpha (c (n "flat-rs") (v "1.0.12-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0y018vmmvdaaskh5151g19dl4q0rrylmpj7907aar6cmzf0bb65h") (r "1.66.1")))

(define-public crate-flat-rs-1.0.13-alpha (c (n "flat-rs") (v "1.0.13-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0gzq6f4v82zkhfrgkllvf5k80k0k6jphj09rh6ld5h6q9qp166k4") (r "1.66.1")))

(define-public crate-flat-rs-1.0.14-alpha (c (n "flat-rs") (v "1.0.14-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "09rvafdj3gyr1y92nb48d7gh7bbjfy505r52q3ynnahxyjpsr3yk") (r "1.66.1")))

(define-public crate-flat-rs-1.0.15-alpha (c (n "flat-rs") (v "1.0.15-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1cdy2g3kid8y2ag06ikzc23kl9c4ikx2finmgajql3w3yn0d2ww1") (r "1.66.1")))

(define-public crate-flat-rs-1.0.16-alpha (c (n "flat-rs") (v "1.0.16-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1yv3p1w2sghz60xnfpr43hznadyxxr9m91x9hhb36p9bbj63spdj") (r "1.66.1")))

(define-public crate-flat-rs-1.0.17-alpha (c (n "flat-rs") (v "1.0.17-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1r84n7glda36xnbqac8f1mvwk22p3bynfwgac00scfpq3438xrh8") (r "1.66.1")))

(define-public crate-flat-rs-1.0.18-alpha (c (n "flat-rs") (v "1.0.18-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1n7pwfn4ps0fagy0yc4q2hhlcdf08rncmaslmf0q5pw1zx2mz6i6") (r "1.66.1")))

(define-public crate-flat-rs-1.0.19-alpha (c (n "flat-rs") (v "1.0.19-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1461plypy56b3vkwf4h8s9d8d8mjzkghh3b726g77z9znwggiv6j") (r "1.66.1")))

(define-public crate-flat-rs-1.0.20-alpha (c (n "flat-rs") (v "1.0.20-alpha") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1q2yhp4yb71qb5qdhlij5a32f3nbsnjclxdsx7izm749mflwdppk") (r "1.66.1")))

(define-public crate-flat-rs-1.0.20 (c (n "flat-rs") (v "1.0.20") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0qjsnqiyqfa928rgz2y4wpli9wxvjwvfa0m3mhf9f8qw48fm6pk1") (y #t) (r "1.66.1")))

