(define-module (crates-io fl at flat-json) #:use-module (crates-io))

(define-public crate-flat-json-0.1.0 (c (n "flat-json") (v "0.1.0") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1xy7d3n1sg572pzgr5w5181advzijxfmiqwi9pr8zywiyglch412")))

