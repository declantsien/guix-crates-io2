(define-module (crates-io fl at flatten-json-object) #:use-module (crates-io))

(define-public crate-flatten-json-object-0.1.0 (c (n "flatten-json-object") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1lv63grhg3szdhhd6nfn8skq8gfv8iyhnw2n9z9npcqbn02gg3id")))

(define-public crate-flatten-json-object-0.1.1 (c (n "flatten-json-object") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0hbzc7kcdnbfa17xp9ql9r1rizpgc3ivdm0fqv3q3k61c4gvb8mm")))

(define-public crate-flatten-json-object-0.1.2 (c (n "flatten-json-object") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vpili9j96yli8wk2dvjmv3q923z14p5dyf03hl5sqnwwxpmnxky")))

(define-public crate-flatten-json-object-0.2.2 (c (n "flatten-json-object") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0kfq4fs59l25babi909ja17l9ara9dhssbz40f41g8q78k8icl6n")))

(define-public crate-flatten-json-object-0.3.0 (c (n "flatten-json-object") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0bh38mv28dlmdjyqpi62z7whyx8xzv343iwapfjcknnf8s5dr3gk")))

(define-public crate-flatten-json-object-0.4.0 (c (n "flatten-json-object") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0kx49ggik6hzwamqlcg9fnclgwdwhrmd094rnqvrj8kwaw104dcg")))

(define-public crate-flatten-json-object-0.5.0 (c (n "flatten-json-object") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0lwpkk3p82vf85h22z9z5k1chchhcfagfimm70yyx2m5bvj2bc6m")))

(define-public crate-flatten-json-object-0.6.0 (c (n "flatten-json-object") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0g2yhsv0i608w7fipln1vxyxxa2dqn9b9jyv7flj7lv5x5g45m7g")))

(define-public crate-flatten-json-object-0.6.1 (c (n "flatten-json-object") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04nmngmjzdi92sx9fbi1d5iy2vcjrv4d3d4z331zgjvsr3cdcfcm")))

