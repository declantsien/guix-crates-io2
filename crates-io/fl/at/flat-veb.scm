(define-module (crates-io fl at flat-veb) #:use-module (crates-io))

(define-public crate-flat-veb-0.1.0 (c (n "flat-veb") (v "0.1.0") (h "1kyiym17xlgz6lqqr4dl559ls7cfa0krkk5j8kkvv46g0v1k98yz") (y #t)))

(define-public crate-flat-veb-0.1.1 (c (n "flat-veb") (v "0.1.1") (h "13b23384g9imkgz0wb5qbr8ijhbaan6kzd3wv3p387p4aad44j7j") (y #t)))

(define-public crate-flat-veb-0.1.2 (c (n "flat-veb") (v "0.1.2") (h "0nvaz0kwmfyx103ljjni6pzrsw7mj29rj8iwwqw8jdgv3kiqzyv5")))

