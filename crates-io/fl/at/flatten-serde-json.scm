(define-module (crates-io fl at flatten-serde-json) #:use-module (crates-io))

(define-public crate-flatten-serde-json-0.1.0 (c (n "flatten-serde-json") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sg5cdc40icamhrd3v92xnlyiv1nml4parl9iqlsk8z1vdixgnkc")))

