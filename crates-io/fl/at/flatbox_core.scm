(define-module (crates-io fl at flatbox_core) #:use-module (crates-io))

(define-public crate-flatbox_core-0.1.0 (c (n "flatbox_core") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.18.0") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "12pvkh0fkl289v5cp6g32y591hhqc6lg4v7m9bb68cm2qm3vgpyz")))

