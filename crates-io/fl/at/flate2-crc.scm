(define-module (crates-io fl at flate2-crc) #:use-module (crates-io))

(define-public crate-flate2-crc-0.1.0 (c (n "flate2-crc") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 2)) (d (n "miniz-sys") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 2)))) (h "1gif22ps6yzrb2qiyf4alz003543l7jsrr85ajnc6jy3fk3d35ss")))

(define-public crate-flate2-crc-0.1.1 (c (n "flate2-crc") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 2)) (d (n "miniz-sys") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 2)))) (h "1ifb44glapxxc35xvkgsrilmv6074mc21kj78sc4fxzdx92j4yca")))

(define-public crate-flate2-crc-0.1.2 (c (n "flate2-crc") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 2)) (d (n "miniz-sys") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 2)))) (h "1sk46za1ch7ngrqx3d3hlyvna0xhy3czqsbwh4qjmh8crj7cn1zz")))

