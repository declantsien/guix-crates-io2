(define-module (crates-io fl at flatarchive) #:use-module (crates-io))

(define-public crate-flatarchive-0.0.1 (c (n "flatarchive") (v "0.0.1") (d (list (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 0)))) (h "1176mwkjyvg7lii0phy823ipji1lnjhdnx7z45nbfw4zalygbjnz")))

