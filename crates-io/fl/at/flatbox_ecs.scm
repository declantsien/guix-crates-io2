(define-module (crates-io fl at flatbox_ecs) #:use-module (crates-io))

(define-public crate-flatbox_ecs-0.1.0 (c (n "flatbox_ecs") (v "0.1.0") (d (list (d (n "as-any") (r "^0.3.1") (d #t) (k 0)) (d (n "flatbox_core") (r "^0.1.0") (d #t) (k 0)) (d (n "hecs") (r "^0.9.1-f") (f (quote ("macros" "column-serialize"))) (d #t) (k 0) (p "despero-hecs")) (d (n "hecs-schedule") (r "^0.6.2") (d #t) (k 0) (p "despero-hecs-schedule")) (d (n "parking_lot") (r "^0.12.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty-type-name") (r "^1.0.1") (d #t) (k 0)))) (h "0zn11jykhka0qpga9qm9j331z856xncvhmv49rrq6pm4yx9drc9l")))

