(define-module (crates-io fl at flatbuffers) #:use-module (crates-io))

(define-public crate-flatbuffers-0.1.0 (c (n "flatbuffers") (v "0.1.0") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)))) (h "18mzih0yhnr5crvjxkk1xw102jppqdvq9zpg4wimkkwr4pam6jrg") (f (quote (("test_idl_gen") ("default")))) (y #t)))

(define-public crate-flatbuffers-0.2.0 (c (n "flatbuffers") (v "0.2.0") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)))) (h "13a0xg7s3ajpa0dxpd7x7rh633kmlfyhznz7x33xpk7c7czdg4xj") (f (quote (("test_idl_gen") ("default")))) (y #t)))

(define-public crate-flatbuffers-0.2.1 (c (n "flatbuffers") (v "0.2.1") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)))) (h "0fp3r9fbgl5qxjn4ir2axf8v6hq5pyrnydwb0xzvq22ps83k65hh") (f (quote (("test_idl_gen") ("default")))) (y #t)))

(define-public crate-flatbuffers-0.3.0 (c (n "flatbuffers") (v "0.3.0") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)))) (h "16d0r6g1qmafilbrd33ad7aq2rhzmd227fa0dx9p5hprfa0zw55a") (f (quote (("test_idl_gen") ("default")))) (y #t)))

(define-public crate-flatbuffers-0.4.0 (c (n "flatbuffers") (v "0.4.0") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "03rk57mxav5slsxw0vz3xvb7qjxkzgfcszr7hwsvy1zf8ph4zssf")))

(define-public crate-flatbuffers-0.5.0 (c (n "flatbuffers") (v "0.5.0") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "07rkp3cf4sb8al9mla37xsp7iagxmnbfkbxcdy1136dyd7v3837a")))

(define-public crate-flatbuffers-0.6.0 (c (n "flatbuffers") (v "0.6.0") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "09i7wavzaisf6c958pqrbhqi3qlwhr563384xfdvaj42zmcszh9g")))

(define-public crate-flatbuffers-0.6.1 (c (n "flatbuffers") (v "0.6.1") (d (list (d (n "smallvec") (r "^1.0") (d #t) (k 0)))) (h "1fg40iycyjq727l14jqipmn1fhz91r4bbx2var07js0hvmlg1257")))

(define-public crate-flatbuffers-0.7.0 (c (n "flatbuffers") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)))) (h "0i0ny49yq3lp51y17jfkqacfbrmhd77qj9v5jhhdhjk7sslhdxnv")))

(define-public crate-flatbuffers-0.8.0 (c (n "flatbuffers") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ncw6r1nsw292jwp7ygxbm7kqpgppvr79zgfnrggch7azw5yil7l")))

(define-public crate-flatbuffers-0.8.1 (c (n "flatbuffers") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x47kzhb8v4is5ividjbbnv44smdhhn892ij7gi7vcdqggnd5hj0")))

(define-public crate-flatbuffers-0.8.2 (c (n "flatbuffers") (v "0.8.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "088wwlx6jh08yx0v7ijap1vj0s71iaqb5rap603v68898szc6qpb")))

(define-public crate-flatbuffers-0.8.3 (c (n "flatbuffers") (v "0.8.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0k79cvdkd5xyk0gcgm434qwswf5fi734l643gh3mrcmx97wv1db3")))

(define-public crate-flatbuffers-0.8.4 (c (n "flatbuffers") (v "0.8.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0sc0ngk9xim7xgqydx36xz4a1sqxq2fv7fmqn6z76vbx5cs05if3")))

(define-public crate-flatbuffers-0.8.5 (c (n "flatbuffers") (v "0.8.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1lckgkhp0by3gjxcgipirngq6hprrzl5vhagrdvqpy1gabk6knv7") (y #t)))

(define-public crate-flatbuffers-0.8.6 (c (n "flatbuffers") (v "0.8.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0a6wfg0hp1qkqcikrlvgc51afalr8zh7hnwfaz0ks8chdp1xpcgk") (y #t)))

(define-public crate-flatbuffers-0.9.0 (c (n "flatbuffers") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0zk41hb9hsqv06nkdsmzwikf4l1sklwmav3g1j2mpj0y26srkkjn")))

(define-public crate-flatbuffers-2.0.0 (c (n "flatbuffers") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1xp5ppif0hvgh9kfvy1199gdmjc3dw1517022l1x3ynpphw5fk7g")))

(define-public crate-flatbuffers-2.1.0 (c (n "flatbuffers") (v "2.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "core2") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (o #t) (d #t) (k 0)) (d (n "thiserror_core2") (r "^2.0.0") (o #t) (k 0)))) (h "1f9yvl626dmlhfsg607i8xy6d9n08ff5nff623sgmw88j83jl0g7") (f (quote (("serialize" "serde") ("no_std" "core2" "thiserror_core2") ("default" "thiserror"))))))

(define-public crate-flatbuffers-2.1.1 (c (n "flatbuffers") (v "2.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "core2") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (o #t) (d #t) (k 0)) (d (n "thiserror_core2") (r "^2.0.0") (o #t) (k 0)))) (h "1daykxiy3hm072zsz26brqlfxcxx3jicx6s4clkjykmqwi7ppabf") (f (quote (("serialize" "serde") ("no_std" "core2" "thiserror_core2") ("default" "thiserror"))))))

(define-public crate-flatbuffers-2.1.2 (c (n "flatbuffers") (v "2.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "core2") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (o #t) (d #t) (k 0)) (d (n "thiserror_core2") (r "^2.0.0") (o #t) (k 0)))) (h "0sibgyy7hlkkk3rvjq9n8a74zy60znsp6idqck1x3ggx2nvjid46") (f (quote (("serialize" "serde") ("no_std" "core2" "thiserror_core2") ("default" "thiserror"))))))

(define-public crate-flatbuffers-22.9.29 (c (n "flatbuffers") (v "22.9.29") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "core2") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (o #t) (d #t) (k 0)) (d (n "thiserror_core2") (r "^2.0.0") (o #t) (k 0)))) (h "0w4ypc4q4568mjw6x8cmrgvskjcgvyrgncbrc5wkbvqsj2widq4c") (f (quote (("serialize" "serde") ("no_std" "core2" "thiserror_core2") ("default" "thiserror"))))))

(define-public crate-flatbuffers-22.10.26 (c (n "flatbuffers") (v "22.10.26") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "16fqxxkirzrvq2qh3r7r6mmphlghrl9mpja29ncxilawr2gk3fnj") (f (quote (("std") ("serialize" "serde") ("default" "std"))))))

(define-public crate-flatbuffers-22.12.6 (c (n "flatbuffers") (v "22.12.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0px4vgpfvxvl19601qkiwblaj1n642q9d1vgxzkpakwh9p4bzqas") (f (quote (("std") ("serialize" "serde") ("default" "std"))))))

(define-public crate-flatbuffers-23.1.21 (c (n "flatbuffers") (v "23.1.21") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "06f6lyl5blvvh03c2siligj1xxk2scm888p532aawl4w5jf3kxbp") (f (quote (("std") ("serialize" "serde") ("default" "std"))))))

(define-public crate-flatbuffers-23.5.26 (c (n "flatbuffers") (v "23.5.26") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0h46mg8yb9igda4ff5dajkzc6k5mf4ix472asqb8rmv24ki57b2d") (f (quote (("std") ("serialize" "serde") ("default" "std"))))))

(define-public crate-flatbuffers-24.3.25 (c (n "flatbuffers") (v "24.3.25") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0py38wagfkj2h90mas07k3v8829pn6s712klpj1zlkrdzypkgpca") (f (quote (("std") ("serialize" "serde") ("default" "std"))))))

