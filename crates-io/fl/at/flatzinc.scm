(define-module (crates-io fl at flatzinc) #:use-module (crates-io))

(define-public crate-flatzinc-0.1.0 (c (n "flatzinc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1bls02xjzwa7azl5gn07xplynvl74bi2i9sxrwa6wnzz3dl3q1ad")))

(define-public crate-flatzinc-0.2.0 (c (n "flatzinc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0fj8hfm6p7fdcb060qc1iynv0xmiw53lsm0pl9rmp32s325b1pqb")))

(define-public crate-flatzinc-0.2.1 (c (n "flatzinc") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dpvlyvml152ic7psgw0wdkrfsprzsblvdwqskg9q4xk6hbf9m5x")))

(define-public crate-flatzinc-0.2.2 (c (n "flatzinc") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "03fp3bcsc8vgskdmv3gdf4ka7ngyj1av3yj5j0a4jpc7z6qmq3br")))

(define-public crate-flatzinc-0.3.0 (c (n "flatzinc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1a4a95y6q2kw2dyhsfzh1ypph24nrm9byzycw00dwnxc58qbslaz")))

(define-public crate-flatzinc-0.3.1 (c (n "flatzinc") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "01ag65d756l0cy9csr51p745j7nq6aaxic880496hnhzdi5bbvxl")))

(define-public crate-flatzinc-0.3.2 (c (n "flatzinc") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1sgifqjmczmggyqfwb111xdm8v4hjc90kl6qp40r4d172gr19xcj")))

(define-public crate-flatzinc-0.3.3 (c (n "flatzinc") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1bbpzv7cjixdgsh0izv31g45i5xbysijybyn2mw1f61wvmdfvaya")))

(define-public crate-flatzinc-0.3.4 (c (n "flatzinc") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dpbaa687n5db38kxd9z6bxy861f5hbkng5saxi39dvxcrkwyli4")))

(define-public crate-flatzinc-0.3.5 (c (n "flatzinc") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0s9mfljnazm1cw4lg7a490x1knr44yw04rdnwlq569wjzh6fh6rl")))

(define-public crate-flatzinc-0.3.6 (c (n "flatzinc") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0z0j75nj3y6zqf3jymggw51vzvv91dvh482gq0navgcnzzm3ka41")))

(define-public crate-flatzinc-0.3.7 (c (n "flatzinc") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1m7miyq4ypr7j816aqk6brhxhwky1zmh5vb73cqyhsb2m83b3yck")))

(define-public crate-flatzinc-0.3.8 (c (n "flatzinc") (v "0.3.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "157jm6y9r8kf3lpm9idlkjvmjkff4faj220501xl1v2k8dhz1p9j")))

(define-public crate-flatzinc-0.3.9 (c (n "flatzinc") (v "0.3.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1makpslly7169vjr0nalcyc3azwpzi2hwg6rg40nl13ysx9dr046")))

(define-public crate-flatzinc-0.3.10 (c (n "flatzinc") (v "0.3.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dgdid7w7vvn427mw9g9bhxlghkn0lr0fv2y8grwn33vd8jlz10z")))

(define-public crate-flatzinc-0.3.11 (c (n "flatzinc") (v "0.3.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0kd3dbcy01zyb2qn7zl4v2hpb8y1ym3p7pvczaxvyczmqqr3ybh2")))

(define-public crate-flatzinc-0.3.12 (c (n "flatzinc") (v "0.3.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1c6hlnwzsr955rz0c8aykhqdnwlx9xwgifj4d8qzpc6dh1m28n6i")))

(define-public crate-flatzinc-0.3.13 (c (n "flatzinc") (v "0.3.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1x06m8z4805ps84vn78wv17bs1bm0a3mf08zs0ns4acmhwd8qqwg")))

(define-public crate-flatzinc-0.3.14 (c (n "flatzinc") (v "0.3.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1nb8xsbqwxwxyvjf6i0zxgi5hpvqkq3qk1c2mn2jcjb95vkb17c6")))

(define-public crate-flatzinc-0.3.15 (c (n "flatzinc") (v "0.3.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0i2anmvbsrsdg4qbag75gmngdnf0sw00wciknnjpwxrh8cg9xm9p")))

(define-public crate-flatzinc-0.3.16 (c (n "flatzinc") (v "0.3.16") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "01whibdnbfm8zvv26bl6jpahgbqbdpfn2pi6gqga21av984pb90q")))

(define-public crate-flatzinc-0.3.17 (c (n "flatzinc") (v "0.3.17") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "064kf9x3fc6bliab8d58wx6ds2s4caxf8q8i1isdc9vmgizwfhm2")))

(define-public crate-flatzinc-0.3.18 (c (n "flatzinc") (v "0.3.18") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1bwqrdikzbva3pq9bwrja3b9jl35p4b0d8bmkziys6v8b022xlc3")))

(define-public crate-flatzinc-0.3.19 (c (n "flatzinc") (v "0.3.19") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0rfjg2fralm615fsiwajgvgb4sxrnwfg33ljr6n5nrx2ly44mnp8")))

(define-public crate-flatzinc-0.3.20 (c (n "flatzinc") (v "0.3.20") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "1i1pplv70p6nmpdypsaslyrnzp4asmml8h5adqlxz4iap9mkbnm8")))

