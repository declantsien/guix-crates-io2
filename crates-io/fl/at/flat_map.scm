(define-module (crates-io fl at flat_map) #:use-module (crates-io))

(define-public crate-flat_map-0.0.1 (c (n "flat_map") (v "0.0.1") (h "1cxghadhiqhd3wap0bl7cd26vhban2fank8znnpxrlx4k42yqdfy")))

(define-public crate-flat_map-0.0.2 (c (n "flat_map") (v "0.0.2") (h "002qx2qblvx22bqjyfiiwb2cl0s658nhqrv4gqf7pavx4qbc6ls9")))

(define-public crate-flat_map-0.0.3 (c (n "flat_map") (v "0.0.3") (h "0nj2j92p474z09v756jfgz38hr9rs6fjw0z4b7hh2l5k2m1hdq7q")))

(define-public crate-flat_map-0.0.4 (c (n "flat_map") (v "0.0.4") (h "0b0qxrxiqrjg04irbd9ph2liz05ly6g9l8ydcafysbgymy0w05pw")))

(define-public crate-flat_map-0.0.5 (c (n "flat_map") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0sdvli0rcf2ydypgfil1qk8m2wc0lzk7s57knq0cmgxp5jr8cgjx") (f (quote (("std") ("serde1" "serde/alloc" "serde_derive") ("default" "std"))))))

(define-public crate-flat_map-0.0.6 (c (n "flat_map") (v "0.0.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "06yplj48jxkmr562xi2vrdw7p5whny28qzyby6cb96pybvaw8cpf") (f (quote (("std") ("serde1" "serde/alloc" "serde_derive") ("default" "std"))))))

(define-public crate-flat_map-0.0.7 (c (n "flat_map") (v "0.0.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0ibsbplccmmi05ix2k85yp6rqzmji31rja5wdiwd69d63q69jljm") (f (quote (("std") ("serde1" "serde" "serde_derive") ("default" "std"))))))

(define-public crate-flat_map-0.0.8 (c (n "flat_map") (v "0.0.8") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0y48zn2710csqn1fs6zzcxny3881vcisidkmf5nnj1ing675sx8y") (f (quote (("std") ("serde1" "serde" "serde_derive") ("default" "std"))))))

(define-public crate-flat_map-0.0.9 (c (n "flat_map") (v "0.0.9") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1r94kgrw4xm85gxp2d7jg68bhf3l8na0g1mmsjk25hsvywp5dfwx") (f (quote (("std") ("serde1" "serde" "serde_derive") ("default" "std"))))))

(define-public crate-flat_map-0.0.10 (c (n "flat_map") (v "0.0.10") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0kr48vp1xsj30012lii1iw92pkwrqmv4dbfb3b43yj9wldb2myp4") (f (quote (("std") ("serde1" "serde" "serde_derive") ("default" "std"))))))

