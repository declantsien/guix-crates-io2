(define-module (crates-io fl at flatpage) #:use-module (crates-io))

(define-public crate-flatpage-0.1.0 (c (n "flatpage") (v "0.1.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14vsakvhflpwywkqxydj4g5jw79j2jirl0ipvasnzky7fr26rz9k")))

(define-public crate-flatpage-0.1.1 (c (n "flatpage") (v "0.1.1") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xaa0z8hgb544dpdq9vf1xn1if4c0dq1kbjdh5cmc77wajn3nhwn")))

(define-public crate-flatpage-0.2.0 (c (n "flatpage") (v "0.2.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00hyrylnn48vi8cqbkakl7vpyv3ay40bil5622q0glns29dv8758")))

