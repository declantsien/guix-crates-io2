(define-module (crates-io fl at flatty-portable) #:use-module (crates-io))

(define-public crate-flatty-portable-0.1.0-alpha.2 (c (n "flatty-portable") (v "0.1.0-alpha.2") (d (list (d (n "base") (r "^0.1.0-alpha.2") (k 0) (p "flatty-base")) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1kyb7sfi6a7p6fbj4mhvi5z1mkvslm29vdkazlahfja1sraaknx1") (f (quote (("std" "base/std" "num-traits/std") ("default" "std"))))))

(define-public crate-flatty-portable-0.1.0-alpha.3 (c (n "flatty-portable") (v "0.1.0-alpha.3") (d (list (d (n "base") (r "^0.1.0-alpha.3") (k 0) (p "flatty-base")) (d (n "num-traits") (r "^0.2") (k 0)))) (h "10gslhyc3qdym1k0wafk52abi28v11lkv35pzq18qjws7bq7jpp0") (f (quote (("std" "base/std" "num-traits/std") ("default" "std"))))))

(define-public crate-flatty-portable-0.1.0-alpha.4 (c (n "flatty-portable") (v "0.1.0-alpha.4") (d (list (d (n "flatty-base") (r "^0.1.0-alpha.3") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "13j0izfmdgas43f5ccqsnz1m11rzl7v1cp25h4pz6s4zcvf1k8jp") (f (quote (("std" "flatty-base/std" "num-traits/std") ("default" "std"))))))

(define-public crate-flatty-portable-0.1.0-alpha.5 (c (n "flatty-portable") (v "0.1.0-alpha.5") (d (list (d (n "flatty-base") (r "^0.1.0-alpha.5") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "08zi107y3qzdv1b066cb0jz7b5v284jx11njrq4m7h9ayx0mfiw0") (f (quote (("std" "flatty-base/std" "num-traits/std") ("default" "std"))))))

(define-public crate-flatty-portable-0.1.0-alpha.7 (c (n "flatty-portable") (v "0.1.0-alpha.7") (d (list (d (n "flatty-base") (r "^0.1.0-alpha.7") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1sbicjnf2gkb3rm53yfzcapc7pm30bqgcr64fj9cpnf67pxmfhj9") (f (quote (("std" "flatty-base/std" "num-traits/std") ("default" "std"))))))

(define-public crate-flatty-portable-0.1.0-rc.0 (c (n "flatty-portable") (v "0.1.0-rc.0") (d (list (d (n "flatty-base") (r "^0.1.0-rc.0") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1n07vnbf7c42wxw2b2sa81sgq6wjr13mvmlz9caizcczppxyaai3") (f (quote (("std" "flatty-base/std" "num-traits/std") ("default" "std"))))))

(define-public crate-flatty-portable-0.1.0-rc.1 (c (n "flatty-portable") (v "0.1.0-rc.1") (d (list (d (n "flatty-base") (r "^0.1.0-rc.1") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0wy2jd71cyf8lxzq6j8sry4nf4lfdg7xbp7sl7bsf2in8qd00pja") (f (quote (("std" "flatty-base/std" "num-traits/std") ("default" "std"))))))

(define-public crate-flatty-portable-0.1.0-rc.2 (c (n "flatty-portable") (v "0.1.0-rc.2") (d (list (d (n "flatty-base") (r "^0.1.0-rc.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0pkd9afwpylmnbilb26ngksajlia11rig5ygglf6na5zw2mrr2zs") (f (quote (("std" "flatty-base/std" "num-traits/std") ("default" "std"))))))

(define-public crate-flatty-portable-0.1.0-rc.4 (c (n "flatty-portable") (v "0.1.0-rc.4") (d (list (d (n "flatty-base") (r "^0.1.0-rc.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "01nnjd85zj50jyk19rhramv19ysbliq2f1paaj79h7cf50swffkq") (f (quote (("std" "flatty-base/std" "num-traits/std") ("default" "std"))))))

