(define-module (crates-io fl at flatvec) #:use-module (crates-io))

(define-public crate-flatvec-0.1.0 (c (n "flatvec") (v "0.1.0") (d (list (d (n "libflate") (r "^1.0") (d #t) (k 2)))) (h "0mh8fvj86zpr9scp44sddrcd8b29ddfwl6xqy8j7gn77kbwr3zx9")))

(define-public crate-flatvec-0.2.0 (c (n "flatvec") (v "0.2.0") (d (list (d (n "libflate") (r "^1.0") (d #t) (k 2)))) (h "19ybx7z83s2qq1q7a0kxi77dzlmfr47b1h2jkx0703swn414gk2q")))

(define-public crate-flatvec-0.3.0 (c (n "flatvec") (v "0.3.0") (d (list (d (n "libflate") (r "^1.0") (d #t) (k 2)) (d (n "tinyvec") (r "^1.5") (f (quote ("alloc" "rustc_1_55"))) (d #t) (k 0)))) (h "09mn4bw50pm3sbqzxmyy860iqh8iisa3ihbx4cn0hlbk00s5ir11")))

