(define-module (crates-io fl at flatbox_egui) #:use-module (crates-io))

(define-public crate-flatbox_egui-0.1.0 (c (n "flatbox_egui") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "egui") (r "^0.19") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.19.0") (f (quote ("clipboard" "links"))) (k 0)) (d (n "flatbox_assets") (r "^0.1.0") (d #t) (k 0)) (d (n "flatbox_core") (r "^0.1.0") (d #t) (k 0)) (d (n "flatbox_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "flatbox_render") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1swxhpjn152rx795c9fx6fcxjj067pglqbrmhz8gf844i66gi5kz")))

