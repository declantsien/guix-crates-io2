(define-module (crates-io fl at flatzinc-serde) #:use-module (crates-io))

(define-public crate-flatzinc-serde-0.1.0 (c (n "flatzinc-serde") (v "0.1.0") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1k24mi07jf40vawn9ypvfrzhca88ngs52819r98yyf3w2xsc5vkd")))

(define-public crate-flatzinc-serde-0.2.0 (c (n "flatzinc-serde") (v "0.2.0") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "ustr") (r "^1.0") (f (quote ("serde"))) (d #t) (k 2)))) (h "0svrd5z79pssvk1cqldjq2vg2vndrj49bkk9fwykdiqrc8mngbnb")))

(define-public crate-flatzinc-serde-0.3.0 (c (n "flatzinc-serde") (v "0.3.0") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "ustr") (r "^1.0") (f (quote ("serde"))) (d #t) (k 2)))) (h "1apg7rfgl2pwhddf74j73g1ghly4jf52xpbynscqs4m7w87bk3pg")))

