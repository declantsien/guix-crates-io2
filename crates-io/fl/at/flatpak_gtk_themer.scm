(define-module (crates-io fl at flatpak_gtk_themer) #:use-module (crates-io))

(define-public crate-flatpak_gtk_themer-0.1.0 (c (n "flatpak_gtk_themer") (v "0.1.0") (d (list (d (n "nix") (r "^0.21.0") (d #t) (k 0)))) (h "053bpn16j566a5jrslryya8mn6xnmfv00d70zws2ip9jf5jkzcfd")))

(define-public crate-flatpak_gtk_themer-0.1.1 (c (n "flatpak_gtk_themer") (v "0.1.1") (d (list (d (n "nix") (r "^0.21.0") (d #t) (k 0)))) (h "12vkdq3lj3nxk1xmll3x05vbd3nv25hj3wf5hy4j7z0kagdng0aa")))

