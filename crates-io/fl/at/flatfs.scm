(define-module (crates-io fl at flatfs) #:use-module (crates-io))

(define-public crate-flatfs-0.1.0 (c (n "flatfs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1fsw743navykmripg7jscnk1x24yl9l10qp986qbzl5z32n27h4v")))

(define-public crate-flatfs-0.1.1 (c (n "flatfs") (v "0.1.1") (h "1lvgd2lszs30cwwxx37qgac62xyx2yhbdlc7hlcmkvn4k3lagkc1")))

(define-public crate-flatfs-0.2.0 (c (n "flatfs") (v "0.2.0") (h "00p7wgi662pxsl5qicdsrgnrnwpqxp7sidhv7gdhvb05d1bg6v8i")))

(define-public crate-flatfs-0.2.1 (c (n "flatfs") (v "0.2.1") (h "0dcjrkdv6i49qza2gxyqfc77bvc85p4gnjppxf14sczdkddryydz")))

