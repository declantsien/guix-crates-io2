(define-module (crates-io fl at flate3) #:use-module (crates-io))

(define-public crate-flate3-0.1.0 (c (n "flate3") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1f6xl2iwss1506pzxp0591c3846j002jjdz8gi2ypwxgd6xpslcg")))

(define-public crate-flate3-0.1.1 (c (n "flate3") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "07v3rrjsh5c57m3jr7qa76rzncmc9k0r3ik36hvd4zg77dhc125j")))

(define-public crate-flate3-0.1.2 (c (n "flate3") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1400a818qx9ci2y7vidv2cxlfhn1s3x0d8b3w3h57sys0wm0h3ai")))

(define-public crate-flate3-0.1.3 (c (n "flate3") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "0dkj2y59x097q1h7jhkv9l1ggdrz7a2hrqyb38aqpz4mh5rqdqcf")))

(define-public crate-flate3-0.1.4 (c (n "flate3") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1jxhd0akx97jgcavdliw4qsjmlqsrhrq4w5kj66k4ggjgz74ppyp")))

(define-public crate-flate3-0.1.5 (c (n "flate3") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1dry43xa61ywxfrb3ipv3bp7fngizd1jq55fia6c0nf8g8cf245p")))

(define-public crate-flate3-0.1.6 (c (n "flate3") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1pjwwrnn8g6hja7icm3a1sd4z5d40f37rp6b2m8jswmngnl3md9q")))

(define-public crate-flate3-0.1.7 (c (n "flate3") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "086smdk1vlclxwvky8h9r3kd1yl54z0pwyrz1n2n1m30xds53gs6")))

(define-public crate-flate3-0.1.8 (c (n "flate3") (v "0.1.8") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "0fgsy3cq1vlhcpx8j5sk0xn3z5i3jd56bjqy0k1j1qi5s3f1yq67")))

(define-public crate-flate3-0.1.9 (c (n "flate3") (v "0.1.9") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1144cyvifjx1p2ilr2xqsqakcd167hnh59fgac5pmwswsljsjdbx")))

(define-public crate-flate3-0.1.10 (c (n "flate3") (v "0.1.10") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "0g72wrv8ppz75zha249f6kcibgky1x6dg3amw5c7fbn4n8cws3pd")))

(define-public crate-flate3-0.1.11 (c (n "flate3") (v "0.1.11") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1cipn1q2p36p039vqd6xgdw1k47nqyznjwg44pyznympficxxp23")))

(define-public crate-flate3-0.1.12 (c (n "flate3") (v "0.1.12") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "0s12mgr2jsv1vipsp3bhz1xxmin308py8xsxpkp17kymkz0wbwkn")))

(define-public crate-flate3-0.1.13 (c (n "flate3") (v "0.1.13") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1k5ayqy67d6yhscbxpqx0h0d8xam8yb00shichq29m092b9nmd6j")))

(define-public crate-flate3-0.1.14 (c (n "flate3") (v "0.1.14") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "106fkdyhil2h1lrpzwcvdxpzlrix46093ds8py0sxswqva7bvgq0")))

(define-public crate-flate3-0.1.15 (c (n "flate3") (v "0.1.15") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "16sildjff964pj7d1lwap5rnprscc5f0h358lw6cwmsfcvvzihv0")))

(define-public crate-flate3-0.1.16 (c (n "flate3") (v "0.1.16") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "0sz9vp24pmlp7f2lkia4chq4nm9cpk8yqhzhb3ib558547f6y20l")))

(define-public crate-flate3-0.1.17 (c (n "flate3") (v "0.1.17") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "00222jax0q0x3s9m1llfcb0lppqsn211cb51y57lzjzv2zk3p9av")))

(define-public crate-flate3-0.1.18 (c (n "flate3") (v "0.1.18") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1qba3d3nzbhx1yj467kqvj0ndwhlya79hmr44ka9hgsxn39hyf51")))

(define-public crate-flate3-0.1.19 (c (n "flate3") (v "0.1.19") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "06b3c4awh3l6ahrvsd3wbsc48wpkw53kkfclzqk33lwga9wqby3y")))

(define-public crate-flate3-0.1.20 (c (n "flate3") (v "0.1.20") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "0yahamyy8bv7sg5qmj6j4jkxy2g86nadvv237c7p7pafqwcrckx3")))

(define-public crate-flate3-0.1.21 (c (n "flate3") (v "0.1.21") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1dh6cvk04w8wlvc3vgf71dv0vkdrppg810rr3s2wm44476a35zvs")))

(define-public crate-flate3-0.1.22 (c (n "flate3") (v "0.1.22") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "09qnhc9x29ijd18v49lsrs2hg4f61nfg38ny3rs8amhmjxzirkhi")))

(define-public crate-flate3-0.1.23 (c (n "flate3") (v "0.1.23") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "0b1isr2ijn8sxjwsgyig0wv71cqrzg2pzwmlrplwrfarigp5y07r")))

(define-public crate-flate3-1.0.0 (c (n "flate3") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "0hipr3z1l7pzp5jvalrf3kisgplvgs6f6zwc0bi1ahvcmn0rlfda")))

(define-public crate-flate3-1.0.1 (c (n "flate3") (v "1.0.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "16pfvqs57rd7w12x62fk2p5yr87x074x52ibjfvcyabd2x5knyr9")))

(define-public crate-flate3-1.0.2 (c (n "flate3") (v "1.0.2") (h "1z707i2g9qj7jk6qdph50b89zzi953vjnn2545n4srbcxx6rhwdy")))

(define-public crate-flate3-1.0.3 (c (n "flate3") (v "1.0.3") (h "1286b1jj7ycq1klxzxn95jmdd47kf45c3nnlr8bz1ijfzna2xmza")))

(define-public crate-flate3-1.0.4 (c (n "flate3") (v "1.0.4") (h "04p411jk8x1ibqisnlh1p07p4jznypdsp97gl1k4lwsmc91bg9qy")))

(define-public crate-flate3-1.1.0 (c (n "flate3") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0hn0kk4za5m4xpcym7brjrb2x8571gs9rkq6hdc3hx8qrxr2khx9")))

