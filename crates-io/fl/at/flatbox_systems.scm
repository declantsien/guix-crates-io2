(define-module (crates-io fl at flatbox_systems) #:use-module (crates-io))

(define-public crate-flatbox_systems-0.1.0 (c (n "flatbox_systems") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flatbox_assets") (r "^0.1.0") (d #t) (k 0)) (d (n "flatbox_core") (r "^0.1.0") (d #t) (k 0)) (d (n "flatbox_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "flatbox_render") (r "^0.1.0") (d #t) (k 0)))) (h "0wzksfhrs7dha732knpy1q5xa7c1m0d3v0kwm9lm4m6f865ax7pv")))

