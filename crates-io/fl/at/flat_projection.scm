(define-module (crates-io fl at flat_projection) #:use-module (crates-io))

(define-public crate-flat_projection-0.1.0 (c (n "flat_projection") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.42") (d #t) (k 0)))) (h "0lxl2g3yxra6ryx2c645ri95qbvisc2apfcqxc84498rga0p72ch")))

(define-public crate-flat_projection-0.1.1 (c (n "flat_projection") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.42") (d #t) (k 0)))) (h "0g7h1f09a3ssailw2zz4n6ix58mmddd9sykdc7i3r2nmasr13ka8")))

(define-public crate-flat_projection-0.2.0 (c (n "flat_projection") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)))) (h "1ch2kfdgrbbx97643w4x0qmrncdcrh6nsg5a7yqwv2hdirx6jqyp")))

(define-public crate-flat_projection-0.3.0 (c (n "flat_projection") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)))) (h "101ym03sy547caksbyvnfcc596z9ddgrijq5nymxbw9jc62ic2l6")))

(define-public crate-flat_projection-0.4.0 (c (n "flat_projection") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)))) (h "0bg38pwjg7wn5jnksjh6yxnfx5wy4aqnnkvqslwqr29k4bjf0k4v")))

