(define-module (crates-io fl at flatten) #:use-module (crates-io))

(define-public crate-flatten-0.1.0 (c (n "flatten") (v "0.1.0") (h "1yhfkgahahlxmbcsnihmrlqsbbsws7z03hbnbgyyrj0j1npgar1b") (f (quote (("std") ("default-features" "std"))))))

(define-public crate-flatten-0.2.0 (c (n "flatten") (v "0.2.0") (d (list (d (n "either") (r "^1.4") (d #t) (k 0)))) (h "12qka0w5892xqh8clsrg17ar6d3zjinkdjavsi7p1s932lm19ydk") (f (quote (("std") ("default-features" "std"))))))

(define-public crate-flatten-0.2.1 (c (n "flatten") (v "0.2.1") (d (list (d (n "either") (r "^1.4") (d #t) (k 0)))) (h "1kxs9414b5q31k49wwk1r6yfc88a1pbzrirgvc6ngyi37n6lsfkx") (f (quote (("std") ("default-features" "std"))))))

(define-public crate-flatten-0.2.2 (c (n "flatten") (v "0.2.2") (d (list (d (n "either") (r "^1.4") (d #t) (k 0)))) (h "1a1ywm6rc0srbkwzzvswr6dnlrb3ibj84xkdqbxfrb9ma8fwgd8d") (f (quote (("std") ("default-features" "std"))))))

