(define-module (crates-io fl at flat_commands) #:use-module (crates-io))

(define-public crate-flat_commands-0.2.0 (c (n "flat_commands") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)))) (h "1d3zwx9plzccmk31fi44qhv6n4x19shvndx99ihikzra7anblhaa")))

(define-public crate-flat_commands-0.2.1 (c (n "flat_commands") (v "0.2.1") (d (list (d (n "bevy") (r "^0.6.0") (k 0)))) (h "1z0075c3agas8sbglfzj71nb204c3szx35r5s5bpg05bh2h4mhys")))

(define-public crate-flat_commands-0.2.2 (c (n "flat_commands") (v "0.2.2") (d (list (d (n "bevy") (r "^0.7.0") (k 0)))) (h "09cf7n84ymyb09s77ab8pvig4xiza1id5kisgv1ijnlnfnb9dilz")))

(define-public crate-flat_commands-0.4.0 (c (n "flat_commands") (v "0.4.0") (d (list (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 2)))) (h "0f1d28brphhyzz3r56w043x44ff1pys0vs6r9myr14w395zxzpnv")))

