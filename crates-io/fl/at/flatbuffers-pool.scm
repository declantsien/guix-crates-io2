(define-module (crates-io fl at flatbuffers-pool) #:use-module (crates-io))

(define-public crate-flatbuffers-pool-0.1.0 (c (n "flatbuffers-pool") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 2)) (d (n "flatbuffers") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0kv621apmfdbqcml2gqx2dfjbhksxdsji0fqh6ipiyh32dwxvzl4")))

(define-public crate-flatbuffers-pool-0.1.1 (c (n "flatbuffers-pool") (v "0.1.1") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 2)) (d (n "flatbuffers") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1cydcxn2wckqza4z9jspj809vlhb51yr6bjyp8gngc7lykkc9b6w")))

(define-public crate-flatbuffers-pool-0.1.2 (c (n "flatbuffers-pool") (v "0.1.2") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 2)) (d (n "flatbuffers") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0hcpkvj4jig2af6nv0yz9k93sf6dxvcp53w8fprjh5s2gwmn0y6g")))

(define-public crate-flatbuffers-pool-0.1.3 (c (n "flatbuffers-pool") (v "0.1.3") (d (list (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 2)) (d (n "flatbuffers") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1qrw21kski6k5dwkm9apjbmcl7zrxyggw5srjqfckclp572cph6r")))

