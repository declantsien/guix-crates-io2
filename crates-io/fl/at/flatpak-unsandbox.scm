(define-module (crates-io fl at flatpak-unsandbox) #:use-module (crates-io))

(define-public crate-flatpak-unsandbox-0.1.0 (c (n "flatpak-unsandbox") (v "0.1.0") (d (list (d (n "gio") (r "^0.19") (d #t) (k 0)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sl3csfb58m9f3rh6j8sp5yyrvisrhhmaj73fdx2m2pzp417ih43")))

