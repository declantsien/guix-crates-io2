(define-module (crates-io fl at flatter) #:use-module (crates-io))

(define-public crate-flatter-0.1.0 (c (n "flatter") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.16") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "resvg") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.5") (d #t) (k 0)) (d (n "usvg") (r "^0.15") (d #t) (k 0)))) (h "18rz0pcb1zk65g3h2sscyp2m7nl72i5106sfqxhi4zaa6154y929")))

