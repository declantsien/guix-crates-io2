(define-module (crates-io fl at flat-bytes-derive) #:use-module (crates-io))

(define-public crate-flat-bytes-derive-0.1.0 (c (n "flat-bytes-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0cl0pg1w57qjdg19piyxzi786b8r4kciajrk8fbvsc0bavs10diy")))

(define-public crate-flat-bytes-derive-0.1.1 (c (n "flat-bytes-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10v0gh8pm7mv7sj5np29gpd9iyypf7aw59h32pyv1qqqpkg2f531")))

