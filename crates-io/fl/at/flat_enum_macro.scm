(define-module (crates-io fl at flat_enum_macro) #:use-module (crates-io))

(define-public crate-flat_enum_macro-0.1.0 (c (n "flat_enum_macro") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "template-quote") (r "^0.3.1") (d #t) (k 0)))) (h "1kaqhjsyagg3dpzc39pv4hhyf3a8dgfgnwb23xfi67i9q8lydmz0")))

