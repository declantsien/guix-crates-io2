(define-module (crates-io fl at flatbuffers-rust) #:use-module (crates-io))

(define-public crate-flatbuffers-rust-0.1.0 (c (n "flatbuffers-rust") (v "0.1.0") (h "1v09yxs9zb5hdg76wf0swx0y1rjncp4gipcjan4sg9p9hv3j4z5l")))

(define-public crate-flatbuffers-rust-0.1.2 (c (n "flatbuffers-rust") (v "0.1.2") (h "0m6m2zyxgj33ql9pwf5n24n6lxc631vy1ddpwwjbdn9pwzyq3a2g")))

