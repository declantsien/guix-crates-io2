(define-module (crates-io fl at flatgeom) #:use-module (crates-io))

(define-public crate-flatgeom-0.0.1 (c (n "flatgeom") (v "0.0.1") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 2)) (d (n "geojson") (r "^0.24") (d #t) (k 2)) (d (n "indexmap") (r "^2.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1p65rhs4f0p5wl8xsqb18p4zrh09dx4ysa593vi9p83jkfn2k05g") (y #t)))

(define-public crate-flatgeom-0.0.2 (c (n "flatgeom") (v "0.0.2") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 2)) (d (n "geojson") (r "^0.24") (d #t) (k 2)) (d (n "indexmap") (r "^2.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0djvcn02f2kw81z4mdy5riq1h4gz1mq3y0pqff820b2micxjr1ic")))

