(define-module (crates-io fl at flatbuffers-owned) #:use-module (crates-io))

(define-public crate-flatbuffers-owned-0.1.0 (c (n "flatbuffers-owned") (v "0.1.0") (d (list (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "0b87c77g5kkxvr25w1v6m9dqrqqdrr4v2wxx6j42kw7vxm857xyq")))

(define-public crate-flatbuffers-owned-0.1.1 (c (n "flatbuffers-owned") (v "0.1.1") (d (list (d (n "flatbuffers") (r "^23.1.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "06g91kw9gcyi34x56jvmq998gd2fksx9ycw2vzfj6yhjgfjgk8a0")))

(define-public crate-flatbuffers-owned-0.2.0 (c (n "flatbuffers-owned") (v "0.2.0") (d (list (d (n "flatbuffers") (r "^23.5.26") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0cmj2xj4fjzh7xiasnpi94qfb194hbxnh80qj9r34q169b9q7x5r")))

