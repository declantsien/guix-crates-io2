(define-module (crates-io fl at flatr) #:use-module (crates-io))

(define-public crate-flatr-0.1.0 (c (n "flatr") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 0)))) (h "0mplz7fxyfim8szflf0fmjkf7safmcvh1aj8lq27gfbnj4y0wwz7")))

