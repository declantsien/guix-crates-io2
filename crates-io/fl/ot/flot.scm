(define-module (crates-io fl ot flot) #:use-module (crates-io))

(define-public crate-flot-0.1.2 (c (n "flot") (v "0.1.2") (d (list (d (n "json") (r "^0.11.8") (d #t) (k 0)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 0)))) (h "02l1lardwi0kg3flmh5zzsg13ihjrvw5w6c4azzli94hw9s2m138")))

(define-public crate-flot-0.1.3 (c (n "flot") (v "0.1.3") (d (list (d (n "json") (r "^0.11.8") (d #t) (k 0)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 0)))) (h "0c08apyfsmfaqn5wja946ssha9rn9c6qszsjjhr7scf6shg9mxx3")))

