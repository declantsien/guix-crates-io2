(define-module (crates-io fl an flanker) #:use-module (crates-io))

(define-public crate-flanker-0.1.0 (c (n "flanker") (v "0.1.0") (h "1c2p22avms53h0licjly2hdhp9bszg2blbsq0n14w46xc6nf92cr")))

(define-public crate-flanker-0.2.0 (c (n "flanker") (v "0.2.0") (h "1qc7ks3i18hbjsy4pccdn2jfnw03hlfszyk98dgb4m9780yhvgci")))

(define-public crate-flanker-0.2.1 (c (n "flanker") (v "0.2.1") (h "0wswkr2v97zsl7895ywpnl2sc509vzrpgfnw6bchrzrizhrcr9pl")))

(define-public crate-flanker-0.3.0 (c (n "flanker") (v "0.3.0") (h "0fprkpyfm471qbcy31wz24his39gmj8hz5daaxh6r77si4dc7wcd")))

(define-public crate-flanker-0.4.0 (c (n "flanker") (v "0.4.0") (h "0vc6qslprzyan7km532pk0akk587hgm7jy46j21zswz35i0m1cmc")))

(define-public crate-flanker-0.5.0 (c (n "flanker") (v "0.5.0") (h "0bgpaiikfib3ljwk6rvk5b64yi8x5rrrmkzlzijgpr71jk7365lk")))

