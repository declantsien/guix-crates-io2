(define-module (crates-io fl an flanker-assert-float) #:use-module (crates-io))

(define-public crate-flanker-assert-float-0.2.0 (c (n "flanker-assert-float") (v "0.2.0") (h "14ai1ckadz81gdrshycgc5xcf1z1vkgkjslnn95q8v4naqpwagh6")))

(define-public crate-flanker-assert-float-0.2.1 (c (n "flanker-assert-float") (v "0.2.1") (h "018i5j1xzbphgfc7d7i5py09syn8fnl5nmxl7j1q835k6fk13nvd")))

(define-public crate-flanker-assert-float-0.3.0 (c (n "flanker-assert-float") (v "0.3.0") (h "1ilbjs3wky4gg1fvhvl2qjmaxwqsz0sryvy7d5z52ba1dksagl1k")))

(define-public crate-flanker-assert-float-0.4.0 (c (n "flanker-assert-float") (v "0.4.0") (h "1d93ccakdkn943gzcr1fhbjl33ln0kvr9dvpdxk8ixc31qkp2c5b")))

(define-public crate-flanker-assert-float-0.5.0 (c (n "flanker-assert-float") (v "0.5.0") (h "1fi8la0ynqfjn7sjn0qvd4cj35lnwpr3m5krnjb4jch0xpx3h5ab")))

