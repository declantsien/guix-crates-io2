(define-module (crates-io fl an flange-flat-tree) #:use-module (crates-io))

(define-public crate-flange-flat-tree-0.1.0 (c (n "flange-flat-tree") (v "0.1.0") (h "1ggbak8lp2jvibki6asrdhx6r4dpxqp3fb5ls2fg86sxqgskmj19")))

(define-public crate-flange-flat-tree-0.1.1 (c (n "flange-flat-tree") (v "0.1.1") (h "1mn2mi57q55x4hc3bd49ips2wh9g49690m4mfg2r9jr3lvhpnnk0")))

(define-public crate-flange-flat-tree-0.1.2 (c (n "flange-flat-tree") (v "0.1.2") (h "0s49w38hhrpmam51llk1spaxm1ns6sdl5ymymj65xvjjjd2l88pj")))

(define-public crate-flange-flat-tree-0.1.3 (c (n "flange-flat-tree") (v "0.1.3") (h "1d619hsf7nxadj9xp0ic63vdkq5ghjn7jawldfz35pa8rz2icfia")))

(define-public crate-flange-flat-tree-0.2.0 (c (n "flange-flat-tree") (v "0.2.0") (h "1w661igd51q10ssw8n41w6bwrkv4ph1m36fch20yljrmd920srar")))

(define-public crate-flange-flat-tree-0.2.1 (c (n "flange-flat-tree") (v "0.2.1") (h "0pxmla291rpqhr1dcv6jl99aidapliwjpxpzvcdsvllf9ncr7i2v")))

(define-public crate-flange-flat-tree-0.2.2 (c (n "flange-flat-tree") (v "0.2.2") (d (list (d (n "cargo-readme") (r "^3") (d #t) (k 2)))) (h "0jq191rwz6cybqkvpyx74rj7wiln4jq4lzd8gvaiwpypr4q7n3hh")))

