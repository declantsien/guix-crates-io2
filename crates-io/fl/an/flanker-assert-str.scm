(define-module (crates-io fl an flanker-assert-str) #:use-module (crates-io))

(define-public crate-flanker-assert-str-0.3.0 (c (n "flanker-assert-str") (v "0.3.0") (h "0cjxrv71c5gmrfsfx2g91gc6z52w2w2vs5hd2mvykla5avr72rg3")))

(define-public crate-flanker-assert-str-0.4.0 (c (n "flanker-assert-str") (v "0.4.0") (h "1rh55q1ws1q6nsvqk7spc0bjpngw46cw7sk0x6ggnh42g083296h")))

(define-public crate-flanker-assert-str-0.5.0 (c (n "flanker-assert-str") (v "0.5.0") (h "0l750rf3xf110yablaryf5vj4hjgaid4lrvm6j6j9ikirbazsws3")))

