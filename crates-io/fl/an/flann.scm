(define-module (crates-io fl an flann) #:use-module (crates-io))

(define-public crate-flann-0.0.1 (c (n "flann") (v "0.0.1") (d (list (d (n "flann-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)))) (h "0x8smq8yq2bz94lhsrk1m2wsfxkfmckw5i3fk6yzx4p6zs4lzzzc")))

(define-public crate-flann-0.0.2 (c (n "flann") (v "0.0.2") (d (list (d (n "flann-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)))) (h "11pclib1mmavb482rr1jg2i04cp9spsq08wn56xyxj9mf7sl734p")))

(define-public crate-flann-0.0.4 (c (n "flann") (v "0.0.4") (d (list (d (n "flann-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)))) (h "1qvrb2ac9y6fbsinzfpb502181m0vcq8wxclx8aykvqw7kv7mvvr")))

(define-public crate-flann-0.0.5 (c (n "flann") (v "0.0.5") (d (list (d (n "flann-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "10141pm9if2vpglbk4a71h45mk4m4fpc4qsvrxnpdhnqbi461qkk")))

(define-public crate-flann-0.0.6 (c (n "flann") (v "0.0.6") (d (list (d (n "flann-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "15330kdm7q6a0whwzdgkshh4xmlva4g15rmdy94fr8d5d0px6z86")))

(define-public crate-flann-0.1.0 (c (n "flann") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "flann-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1wibb0fnfxliv323842lnm7rq57jfqihkr8v7fd08vd211cs3j2l")))

