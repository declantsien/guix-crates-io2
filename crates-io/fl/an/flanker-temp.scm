(define-module (crates-io fl an flanker-temp) #:use-module (crates-io))

(define-public crate-flanker-temp-0.1.0 (c (n "flanker-temp") (v "0.1.0") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5.0") (d #t) (k 0)))) (h "1azdxy2vzf4n726lpz6r6bw0nijx7xjj2lv4sicf45rsfb33s7yy")))

(define-public crate-flanker-temp-0.2.0 (c (n "flanker-temp") (v "0.2.0") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5.0") (d #t) (k 0)))) (h "03srj6avan11dqz7fw24fqsaipchfvqypi8xp1vwy2v067s3ivxx")))

(define-public crate-flanker-temp-0.2.1 (c (n "flanker-temp") (v "0.2.1") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5.0") (d #t) (k 0)))) (h "1560i19v68l9j3cfmj3ssih4zlw6jdiaa10wscccj8cls01m6cnh")))

(define-public crate-flanker-temp-0.3.0 (c (n "flanker-temp") (v "0.3.0") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5.0") (d #t) (k 0)))) (h "0z87xqc9f9wic8g8n8dd7nfv327nl7y6br74ahcswp3m5f6qkyhn")))

(define-public crate-flanker-temp-0.4.0 (c (n "flanker-temp") (v "0.4.0") (d (list (d (n "thread-id") (r "^4.0.0") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5.0") (d #t) (k 0)))) (h "1g0bgihx0dkx5ilsd17m3nbldg28mkr28a1hqv9ic3brxbanxg9y")))

(define-public crate-flanker-temp-0.5.0 (c (n "flanker-temp") (v "0.5.0") (d (list (d (n "thread-id") (r "^4.0.0") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5.0") (d #t) (k 0)))) (h "03rbp6klw4ay1g26vnmy8dadlqnmjvzxqsdl3fw12x7j467qha0l")))

