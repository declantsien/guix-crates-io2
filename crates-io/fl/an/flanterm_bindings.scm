(define-module (crates-io fl an flanterm_bindings) #:use-module (crates-io))

(define-public crate-flanterm_bindings-1.0.0 (c (n "flanterm_bindings") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)))) (h "05lci8gzp6vjcv0lgr2d2r60aw17a1s1bdcp2rs4nsfx07mxiq0d") (y #t)))

(define-public crate-flanterm_bindings-1.0.1 (c (n "flanterm_bindings") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1r9xyjzamha6qy1m6j8bkfgza30703qwz8l0b5q67i490hvd41kn") (y #t)))

(define-public crate-flanterm_bindings-0.1.0 (c (n "flanterm_bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)))) (h "1g5yyjhnrgrh44wy2ijqcxjnjjl0vxxyl8p49v8ky6sibjdrd3mc") (y #t)))

(define-public crate-flanterm_bindings-1.0.2 (c (n "flanterm_bindings") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)))) (h "131hg63kx64gzqkz73sg208xmnafzdnm9an83nwfrpa957w5ixsz")))

(define-public crate-flanterm_bindings-1.0.3 (c (n "flanterm_bindings") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)))) (h "1g7pskpchj0dcl1gk3dn5vjsn676ywkjcpzf8nwz08b5h4qqbnrg")))

(define-public crate-flanterm_bindings-1.0.4 (c (n "flanterm_bindings") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)))) (h "19wym323rfm3ik1cz4i0x6vbqj9whx91zdx34a4yq20mdy3ysisk")))

(define-public crate-flanterm_bindings-1.0.5 (c (n "flanterm_bindings") (v "1.0.5") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (d #t) (k 1)))) (h "0gxdcbb1pyp1vxzf3djwsplsdj76rqyd5q7xqw7560izfs4jxybw")))

