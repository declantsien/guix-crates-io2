(define-module (crates-io fl an flann-sys) #:use-module (crates-io))

(define-public crate-flann-sys-0.0.1 (c (n "flann-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "13lfnzm83jks2zgfpn0kl62sn84m5sjcjb9kh6ax2bnl5pvgsaw2")))

(define-public crate-flann-sys-0.0.3 (c (n "flann-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "07lj8765dl8n9klklfisr1h9lcrlanh9fvm401yfj927yimykf09")))

(define-public crate-flann-sys-0.0.4 (c (n "flann-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0b180bqkv7np2sx9870i8617l3kg1zkn8iicaw55sxxbzpmfw1w0")))

(define-public crate-flann-sys-0.1.0 (c (n "flann-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "0h851azkcj65p2g59rspkxg2vjj2rrclby3nbyzlya8c9i2gkv1x")))

