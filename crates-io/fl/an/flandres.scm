(define-module (crates-io fl an flandres) #:use-module (crates-io))

(define-public crate-flandres-0.1.0 (c (n "flandres") (v "0.1.0") (d (list (d (n "fltk") (r "^0.9") (d #t) (k 0)))) (h "0bar04920cravj6has8qmcakbck8091accqj1zprrbppv7b2pf6v")))

(define-public crate-flandres-0.2.0 (c (n "flandres") (v "0.2.0") (d (list (d (n "fltk") (r "^0.14") (d #t) (k 0)))) (h "193blfl7543azzrkl0wsgphdfq2b6v9am62in37lzz7gdsf7y6a0")))

(define-public crate-flandres-0.2.1 (c (n "flandres") (v "0.2.1") (d (list (d (n "fltk") (r "^0.15") (d #t) (k 0)))) (h "09svbc964ndq95p29zxpkiqvfr2rc27650zxmvri31x4k0z7vxaj")))

(define-public crate-flandres-0.3.0 (c (n "flandres") (v "0.3.0") (d (list (d (n "fltk") (r "^1") (d #t) (k 0)))) (h "0n61sn7n4g8vxa0by71342zjq9ymv7szz4fn8c6d701hmrhhddrd")))

(define-public crate-flandres-0.3.1 (c (n "flandres") (v "0.3.1") (d (list (d (n "fltk") (r "^1") (d #t) (k 0)))) (h "0kga45l6a60cxaklnbg2zw0pfbscplnrzn97p98bmlv4ykr0g9mq")))

