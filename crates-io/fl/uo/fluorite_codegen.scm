(define-module (crates-io fl uo fluorite_codegen) #:use-module (crates-io))

(define-public crate-fluorite_codegen-0.1.2 (c (n "fluorite_codegen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluorite") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "19466jm3vq5496iwm23ajgpg096nx2av9xn03yk0y4ri2pwcmh39")))

(define-public crate-fluorite_codegen-0.1.3 (c (n "fluorite_codegen") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluorite") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "1197mxkrgcgxl7vlm0fghyg78a4h1b5ba5yqjc3p4iqzprinar01")))

(define-public crate-fluorite_codegen-0.1.4 (c (n "fluorite_codegen") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluorite") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "1rn5hmlrjj0wchb3w2dpj81w20k76pivb1zz5rwcnz4a4m2f5ncz")))

(define-public crate-fluorite_codegen-0.1.5 (c (n "fluorite_codegen") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluorite") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "10abr78cx15jl587ki444pa3jj5zm8l5cg463n2r1lsgli5djcdl")))

(define-public crate-fluorite_codegen-0.1.6 (c (n "fluorite_codegen") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluorite") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "1ljbjcgvcd8fmf578swi1nnzaxjc80bp9sd4f58m0pl5qfqr5ai3")))

(define-public crate-fluorite_codegen-0.1.7 (c (n "fluorite_codegen") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluorite") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "10sikqm2sgrcc887fbbz0d8azgam1049694fmizjw553xa1a7px8")))

(define-public crate-fluorite_codegen-0.1.8 (c (n "fluorite_codegen") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive-new") (r "^0.6") (d #t) (k 0)) (d (n "fluorite") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "0rsmvrp1p820gf5h1rk5lshflk495zg20yb6q929a3107zhk0hka")))

(define-public crate-fluorite_codegen-0.1.9 (c (n "fluorite_codegen") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive-new") (r "^0.6") (d #t) (k 0)) (d (n "fluorite") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "12gs1l0ys8pj5sgzlyis7218vd52n0015j4smchv4873w62zihik")))

