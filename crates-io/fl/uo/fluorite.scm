(define-module (crates-io fl uo fluorite) #:use-module (crates-io))

(define-public crate-fluorite-0.1.0 (c (n "fluorite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "192r36sy7nw8f5l17i1x38a0klpk2qj1cyf895nwdd9zg6l1pgwi")))

(define-public crate-fluorite-0.1.1 (c (n "fluorite") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "07fv5z2vr2rpxka1flwb63y2lb8zhc4hlfzikcpvxpmmpd6mcxqn")))

(define-public crate-fluorite-0.1.2 (c (n "fluorite") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1p62vyzp2a1sfgimfq27vcpsfscm1pwxgzikmzpi3ymvj85rajg8")))

(define-public crate-fluorite-0.1.3 (c (n "fluorite") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0wm6m92b03q98s6kdj48a8sjq3bfrjr2k7fx295ii7h6p4mdfd3f")))

(define-public crate-fluorite-0.1.4 (c (n "fluorite") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "15d0v4w97if2y1f31aiicgah0bc8szyvc9rmhlyx3f29413hcwdl")))

(define-public crate-fluorite-0.1.5 (c (n "fluorite") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "09hsskdg7k9ixsx52r5h4kik7ig8yq7wrbiymlcwcpnvji1m6awp")))

(define-public crate-fluorite-0.1.6 (c (n "fluorite") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0ys8ahxz3v3ibxzaqd2iyvqkqnpgshpjcy726hmgr7v66k7xqhm5")))

(define-public crate-fluorite-0.1.7 (c (n "fluorite") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0fgz6r9q5311xf2dawxz664976a47wpvvw5qdy568gdcjf8ml9jj")))

(define-public crate-fluorite-0.1.8 (c (n "fluorite") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "14jkslahrza2x1ncrh1c59v1gwvrc4madj8jb5hkyrkcn87vi95f")))

(define-public crate-fluorite-0.1.9 (c (n "fluorite") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0hhrrgy9bzm8h1cnm5zmxb0l7f5f8asbivpwccgq6vx81il12k3q")))

