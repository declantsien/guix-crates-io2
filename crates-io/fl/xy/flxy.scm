(define-module (crates-io fl xy flxy) #:use-module (crates-io))

(define-public crate-flxy-0.1.0 (c (n "flxy") (v "0.1.0") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1s4mwvljjsyby8xn8lqgh2nifvfbld4bwj0k613ag77xlglqfzsq")))

(define-public crate-flxy-0.1.1 (c (n "flxy") (v "0.1.1") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1ljfil6fj707mlx2z165vnrzbhagnqnnvsbnwh2m5ndzqf8kkcma")))

(define-public crate-flxy-0.1.2 (c (n "flxy") (v "0.1.2") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "160lbcwaybs2lsfx9mp5qyd5y0znynpkfn63m31482kkqas3mgpp")))

(define-public crate-flxy-0.1.3 (c (n "flxy") (v "0.1.3") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0mx5mzscrynk4nrlmv1jw0pm0k7h4kahfi4acsc5c06nh2wdj0zh")))

(define-public crate-flxy-0.1.4 (c (n "flxy") (v "0.1.4") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1z36wkp4rgjl72ww104dk4cgxp6d7smclflzmj2knl1wppgvjyda")))

(define-public crate-flxy-0.1.5 (c (n "flxy") (v "0.1.5") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "14siz7hdn6z8xvmvdmdygadwi37l98pnrkvbj31k7a8c56kkm3nn")))

(define-public crate-flxy-0.1.6 (c (n "flxy") (v "0.1.6") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "02hk7s18q02z565x3cjgc41134476nl5572hqwv53dkrmbbqwvi7")))

(define-public crate-flxy-0.1.7 (c (n "flxy") (v "0.1.7") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0y9jx8xs9w11r7dc15mxsis9wngy27672qqba0dn1xcy53sjjk7w")))

(define-public crate-flxy-0.1.8 (c (n "flxy") (v "0.1.8") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0fn4hs5vr3hfqzbrabl2d1pbvhjjagyvb0c30wm069pf14fhc672")))

(define-public crate-flxy-0.1.9 (c (n "flxy") (v "0.1.9") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "115l92cslmvxv8qnbsza8cpwql3dkgj4d2gifilh8rbyd1i7v28r")))

(define-public crate-flxy-0.1.10 (c (n "flxy") (v "0.1.10") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "01p21lr9cixpp5max4yfvqw8wri8zpwskdazkr0hkyw0ghcmm92y")))

(define-public crate-flxy-0.1.11 (c (n "flxy") (v "0.1.11") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1v647lm7fkvl7mm7dsa1v4zd24crprad99a3v8slwds9493rjwwk")))

(define-public crate-flxy-0.1.12 (c (n "flxy") (v "0.1.12") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0phikfihskrmk4czhar0ibnc50sv7d1da6a7vnina38hdy8gv4v9")))

(define-public crate-flxy-0.1.13 (c (n "flxy") (v "0.1.13") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1w72xvgnr6m1qqw09lg1d2fy7xsrh3s0y6p5p8gq00zc9w5ha72k")))

(define-public crate-flxy-0.1.14 (c (n "flxy") (v "0.1.14") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0jxw80j3apj5493i9gcky2j0f3djzjdx7y4z30z6hrdfjdwh336d")))

(define-public crate-flxy-0.1.15 (c (n "flxy") (v "0.1.15") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "072lg6343d8szya1sa0y4d6rl6g55d2yrzwzdrdbhqsssk88fpp4")))

(define-public crate-flxy-0.1.16 (c (n "flxy") (v "0.1.16") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "13m527q1hqb2j7zxvx26vh62d23b9595g0w2i3k538bhmjavnmy0")))

(define-public crate-flxy-0.1.17 (c (n "flxy") (v "0.1.17") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "14cqsq9ysa4dhb6bxmqmagdxmwnvnw2n8dlx2d3fx7hlf9crmc7r")))

(define-public crate-flxy-0.1.18 (c (n "flxy") (v "0.1.18") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "08lcdmmr3l90bxak1ry0yagbq72cvla71lj4lgjixb727nzfhhqz")))

