(define-module (crates-io fl uv fluvio-protocol-api) #:use-module (crates-io))

(define-public crate-fluvio-protocol-api-0.1.0 (c (n "fluvio-protocol-api") (v "0.1.0") (d (list (d (n "fluvio-protocol") (r "^0.1.0") (d #t) (k 0) (p "fluvio-protocol-core")) (d (n "fluvio-protocol-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flv-util") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1r64yzib4ddq4six8fc2b045cla257ii8ffq5dz3bh9mqj9xprx9")))

(define-public crate-fluvio-protocol-api-0.1.1 (c (n "fluvio-protocol-api") (v "0.1.1") (d (list (d (n "fluvio-protocol") (r "^0.1.0") (d #t) (k 0) (p "fluvio-protocol-core")) (d (n "fluvio-protocol-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flv-util") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1prjr5c982ppfd4fhai1b2r0sqbmhdnp40q8v2xczm3pbwi4dfv6")))

(define-public crate-fluvio-protocol-api-0.2.0 (c (n "fluvio-protocol-api") (v "0.2.0") (d (list (d (n "fluvio-protocol") (r "^0.2.0") (d #t) (k 0) (p "fluvio-protocol-core")) (d (n "fluvio-protocol-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "flv-util") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "09bl7ajpnq8cqqsa2pbpy9nm70y4q455fvlq0a5naaggd28y15s9")))

(define-public crate-fluvio-protocol-api-0.3.0 (c (n "fluvio-protocol-api") (v "0.3.0") (d (list (d (n "fluvio-protocol") (r "^0.3.0") (d #t) (k 0) (p "fluvio-protocol-core")) (d (n "fluvio-protocol-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "flv-util") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0v0czx2m8m4qbpmhix47hmqs96ipw2psjf4l554zzzvzj0phfrmf")))

(define-public crate-fluvio-protocol-api-0.4.0 (c (n "fluvio-protocol-api") (v "0.4.0") (d (list (d (n "fluvio-protocol") (r "^0.3.0") (d #t) (k 0) (p "fluvio-protocol-core")) (d (n "fluvio-protocol-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "flv-util") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "12ph77c6gskq7m2mm62248nx0lz3yc6djqhr795g2sdy0plp8d87")))

(define-public crate-fluvio-protocol-api-0.4.1 (c (n "fluvio-protocol-api") (v "0.4.1") (d (list (d (n "fluvio-protocol") (r "^0.3.1") (d #t) (k 0) (p "fluvio-protocol-core")) (d (n "fluvio-protocol-derive") (r "^0.3") (d #t) (k 0)) (d (n "flv-util") (r "^0.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1gapw6kvn1bfxxc8m78wax79pzb1b1gy46i72nchipwri4g5d36n")))

