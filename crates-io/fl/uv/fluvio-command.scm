(define-module (crates-io fl uv fluvio-command) #:use-module (crates-io))

(define-public crate-fluvio-command-0.1.0 (c (n "fluvio-command") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.22") (d #t) (k 0)))) (h "15zhl6wa1hzcj74fzh97imkgvsl3rzjz1nkarx78b9kwyafz7p72")))

(define-public crate-fluvio-command-0.2.0 (c (n "fluvio-command") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.22") (d #t) (k 0)))) (h "0f3bcznspiyz4a5m395zm307djbzf97wccz1crkim460qin7sn1v")))

(define-public crate-fluvio-command-0.2.1 (c (n "fluvio-command") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.22") (d #t) (k 0)))) (h "11xsmp6qjnhwzqlds012j4fp9ad2f358ddgyfw0i6nlfp4vax8c2")))

