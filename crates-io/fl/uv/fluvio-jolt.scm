(define-module (crates-io fl uv fluvio-jolt) #:use-module (crates-io))

(define-public crate-fluvio-jolt-0.1.0 (c (n "fluvio-jolt") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vx35pbjv1s6w0igpdww1rwlwkjcx1rhkp3y66bmw0bgvjma3dwk")))

(define-public crate-fluvio-jolt-0.1.1 (c (n "fluvio-jolt") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1zhda0j6cj0l5a3jq84ynmcq88y5jiwwns624df5gr64i9n4w1l2")))

(define-public crate-fluvio-jolt-0.2.0 (c (n "fluvio-jolt") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indexmap") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "1c10bixg5yikrfhml43ch7rs997np9yd5y61if787cdgviy46fcx") (f (quote (("fuzz"))))))

(define-public crate-fluvio-jolt-0.3.0 (c (n "fluvio-jolt") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nwmq0qff7lyfbqhw21dfh6j6i3rrbqyvlwg78zpig1a19z7b21q") (f (quote (("fuzz"))))))

