(define-module (crates-io fl uv fluvio-helm) #:use-module (crates-io))

(define-public crate-fluvio-helm-0.1.0 (c (n "fluvio-helm") (v "0.1.0") (d (list (d (n "flv-util") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "007g7y34hnz0jz9z8v2hhi9gviqc71c2ica3jzk15bl66i70ppcy")))

(define-public crate-fluvio-helm-0.2.0 (c (n "fluvio-helm") (v "0.2.0") (d (list (d (n "flv-util") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "08siv5hcvdl8srba8akkjcc3h00hxjynr43hm2csbyqb7c75dkg7") (y #t)))

(define-public crate-fluvio-helm-0.2.1 (c (n "fluvio-helm") (v "0.2.1") (d (list (d (n "flv-util") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1cjiy6rxzaa9kq1nfif2djbl1kz7m3vq0qxjkk5f6p36l02hcrfx")))

(define-public crate-fluvio-helm-0.3.0 (c (n "fluvio-helm") (v "0.3.0") (d (list (d (n "flv-util") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "0nc9wbsm6gxzax9wd8rq3qi7d7q75pgjyvwdfjjaxwj86y9ig54k")))

(define-public crate-fluvio-helm-0.4.0 (c (n "fluvio-helm") (v "0.4.0") (d (list (d (n "flv-util") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "0fw99ib3wvcnwsxb2kaa83kysfc48drzmmv3rgps5v8zvg11ga0n")))

(define-public crate-fluvio-helm-0.4.1 (c (n "fluvio-helm") (v "0.4.1") (d (list (d (n "fluvio-command") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1pxaifgqmc79j44323dp3p6a9yfspkmkd9637jgv7v4v47kf7dhh")))

(define-public crate-fluvio-helm-0.4.2 (c (n "fluvio-helm") (v "0.4.2") (d (list (d (n "fluvio-command") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "01raa01ana0z4mkasy51vfvn6xb3kcmy8jwcsbv5jrxzxdp9218v")))

(define-public crate-fluvio-helm-0.4.3 (c (n "fluvio-helm") (v "0.4.3") (d (list (d (n "fluvio-command") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1vb1brkhqk6l4rii3fq7a95a1v077h2qg84pwqhv11nbcjdsc9g9")))

