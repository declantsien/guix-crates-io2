(define-module (crates-io fl uv fluvio-protocol-core) #:use-module (crates-io))

(define-public crate-fluvio-protocol-core-0.1.0 (c (n "fluvio-protocol-core") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1qpns8m679b9dv4yhwyv2g062zsvzihvnvb9x6hhl13848jyp1wv")))

(define-public crate-fluvio-protocol-core-0.2.0 (c (n "fluvio-protocol-core") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0rj6z5w9sc6080bgm6njhh6c9nac2zldg70d0jni7p38sia9i34m")))

(define-public crate-fluvio-protocol-core-0.2.1 (c (n "fluvio-protocol-core") (v "0.2.1") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0dm4v5px1gyn2b6shiaphp9g0ax4dfbrc7y8kqpmxgqq449dg87v")))

(define-public crate-fluvio-protocol-core-0.2.2 (c (n "fluvio-protocol-core") (v "0.2.2") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1ghycjh6r22pxwy0rx9kjw0368cyhvh9mai5dlar7pn46r35q9k0")))

(define-public crate-fluvio-protocol-core-0.3.0 (c (n "fluvio-protocol-core") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0jh2nax80bl9z97kg6qwl6yy73cwl1j1wc5vvq68gpvyla6n0l69")))

(define-public crate-fluvio-protocol-core-0.3.1 (c (n "fluvio-protocol-core") (v "0.3.1") (d (list (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0b1ccg1avpxcw8sgj9szgf63acnyjcr5v4hgpvgsp1gy4ajiayqr")))

(define-public crate-fluvio-protocol-core-0.3.2 (c (n "fluvio-protocol-core") (v "0.3.2") (d (list (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "14hqw83k3pb9xkqr5zfvbq98hk7jirpj4bw101280ibghassma8i")))

(define-public crate-fluvio-protocol-core-0.3.3 (c (n "fluvio-protocol-core") (v "0.3.3") (d (list (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0yb7vikza8sg88q1gldnd37sl70wya49gdcjsv81n041ysgcj3k9")))

