(define-module (crates-io fl uv fluvio-smartstream-wasm) #:use-module (crates-io))

(define-public crate-fluvio-smartstream-wasm-0.1.0 (c (n "fluvio-smartstream-wasm") (v "0.1.0") (d (list (d (n "fluvio-dataplane-protocol") (r "^0.4.1") (k 0)))) (h "135f12i8phzz0rvjalz9qdkihlqmf9a6bgkcif0mzi58hp6vk6jz")))

