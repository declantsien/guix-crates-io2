(define-module (crates-io fl uv fluvio-smartstream-derive) #:use-module (crates-io))

(define-public crate-fluvio-smartstream-derive-0.1.0 (c (n "fluvio-smartstream-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0svwq3vmc54xqhqhw953i8y91xfy5dnd66fzs6bd8d3kp5i5x0ll")))

(define-public crate-fluvio-smartstream-derive-0.1.1 (c (n "fluvio-smartstream-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07wd0j0ch35dmja07f3z1vqhp5j0kdppk805mrq0cnzkjisikxiw")))

(define-public crate-fluvio-smartstream-derive-0.1.2 (c (n "fluvio-smartstream-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1li2yavks6mig9dwm7vpa8xcv504c0cndxvy2dj1mmr2zq9h38dp")))

(define-public crate-fluvio-smartstream-derive-0.3.0 (c (n "fluvio-smartstream-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0insvhyw6snjp76d2y56y7vvsdi7hz2rm8qq3jdbqxkc4llsq96g")))

(define-public crate-fluvio-smartstream-derive-0.4.0 (c (n "fluvio-smartstream-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zz4a9vsw9ns5sw981b812kpjxhp18cda8v3w33cyc795irfwxgn")))

