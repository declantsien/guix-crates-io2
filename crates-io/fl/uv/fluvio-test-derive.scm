(define-module (crates-io fl uv fluvio-test-derive) #:use-module (crates-io))

(define-public crate-fluvio-test-derive-0.1.0 (c (n "fluvio-test-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "1q2wzrj2fknrys621psrk8dzs1ljm5dfz6c3j8wz0pxakjq4r7pw")))

(define-public crate-fluvio-test-derive-0.1.1 (c (n "fluvio-test-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "13j7vjm6kyk86d18xd24ymszjqfirx8zz5dwzks36dbdla0fz3y1")))

