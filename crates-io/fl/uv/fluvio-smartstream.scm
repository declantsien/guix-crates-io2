(define-module (crates-io fl uv fluvio-smartstream) #:use-module (crates-io))

(define-public crate-fluvio-smartstream-0.1.0 (c (n "fluvio-smartstream") (v "0.1.0") (d (list (d (n "fluvio-dataplane-protocol") (r "^0.5.1") (k 0)) (d (n "fluvio-smartstream-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "03bp9dy5aszbldfv1x3zvckh5rj7z6fjb95zqq3l914s52p4hw9j") (f (quote (("derive" "fluvio-smartstream-derive") ("default" "derive"))))))

(define-public crate-fluvio-smartstream-0.2.0 (c (n "fluvio-smartstream") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6") (k 0)) (d (n "fluvio-dataplane-protocol") (r "^0.6") (k 0)) (d (n "fluvio-smartstream-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1smm9m4m1ln18fc42yyp8ccm891g2rx0z4pf4hywdkvr87lka9nk") (f (quote (("derive" "fluvio-smartstream-derive") ("default" "derive"))))))

(define-public crate-fluvio-smartstream-0.2.1 (c (n "fluvio-smartstream") (v "0.2.1") (d (list (d (n "eyre") (r "^0.6") (k 0)) (d (n "fluvio-dataplane-protocol") (r "^0.6") (k 0)) (d (n "fluvio-smartstream-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0ghmdw8n65gih8199pjp8wg9vp6xwv6znpj0ccijanwkp1n01mxd") (f (quote (("derive" "fluvio-smartstream-derive") ("default" "derive"))))))

(define-public crate-fluvio-smartstream-0.2.2 (c (n "fluvio-smartstream") (v "0.2.2") (d (list (d (n "eyre") (r "^0.6") (k 0)) (d (n "fluvio-dataplane-protocol") (r "^0.6") (k 0)) (d (n "fluvio-smartstream-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1gaqfrs3avggk8vi7nrp6vd8905n3mycvkk3x1mjv2drw8fmzgd3") (f (quote (("derive" "fluvio-smartstream-derive") ("default" "derive"))))))

(define-public crate-fluvio-smartstream-0.2.3 (c (n "fluvio-smartstream") (v "0.2.3") (d (list (d (n "eyre") (r "^0.6") (k 0)) (d (n "fluvio-dataplane-protocol") (r "^0.7") (k 0)) (d (n "fluvio-smartstream-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0r3sig32sqs5j3r5rk94wc9hihlsrg428wkr2kk01d187qvnj4y3") (f (quote (("derive" "fluvio-smartstream-derive") ("default" "derive"))))))

(define-public crate-fluvio-smartstream-0.3.0 (c (n "fluvio-smartstream") (v "0.3.0") (d (list (d (n "eyre") (r "^0.6") (k 0)) (d (n "fluvio-dataplane-protocol") (r "^0.7.3") (k 0)) (d (n "fluvio-smartstream-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1nj8w8s5jv0r838y95xkhs50cxp15g1f7wzimsfvz5a1hvg2z490") (f (quote (("derive" "fluvio-smartstream-derive") ("default" "derive"))))))

(define-public crate-fluvio-smartstream-0.4.0 (c (n "fluvio-smartstream") (v "0.4.0") (d (list (d (n "eyre") (r "^0.6") (k 0)) (d (n "fluvio-dataplane-protocol") (r "^0.8.0") (k 0)) (d (n "fluvio-smartstream-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "fluvio-spu-schema") (r "^0.8") (d #t) (k 0)))) (h "0wacawyq9q2dmyfra1gq8sr8y1rzb0sv4zlvdyxv3j6w9vh98dd2") (f (quote (("derive" "fluvio-smartstream-derive") ("default" "derive"))))))

