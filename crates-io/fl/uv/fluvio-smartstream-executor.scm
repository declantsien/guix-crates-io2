(define-module (crates-io fl uv fluvio-smartstream-executor) #:use-module (crates-io))

(define-public crate-fluvio-smartstream-executor-0.1.0 (c (n "fluvio-smartstream-executor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "dataplane") (r "^0.7.0") (f (quote ("file"))) (d #t) (k 0) (p "fluvio-dataplane-protocol")) (d (n "fluvio-future") (r "^0.3.9") (f (quote ("subscriber" "openssl_tls" "zero_copy"))) (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.27") (d #t) (k 0)) (d (n "wasmtime") (r "^0.30") (d #t) (k 0)))) (h "1rgm2nn9crhzynwvnspkw5ff35jwivkndyhv18pvdbgz6rfjd6jb")))

