(define-module (crates-io fl uv fluvio-runner-local) #:use-module (crates-io))

(define-public crate-fluvio-runner-local-0.2.0 (c (n "fluvio-runner-local") (v "0.2.0") (d (list (d (n "fluvio-future") (r "^0.1.12") (f (quote ("subscriber"))) (d #t) (k 0)) (d (n "fluvio-sc") (r "^0.4.0") (d #t) (k 0)) (d (n "fluvio-spu") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1cg1xw9q0jpx259lbh0x0ni68gzh34a5kfsmnb2vrlcgiipdvnq9") (f (quote (("platform" "fluvio-sc/platform"))))))

(define-public crate-fluvio-runner-local-0.3.0 (c (n "fluvio-runner-local") (v "0.3.0") (d (list (d (n "fluvio-future") (r "^0.1.12") (f (quote ("subscriber"))) (d #t) (k 0)) (d (n "fluvio-sc") (r "^0.5.0") (d #t) (k 0)) (d (n "fluvio-spu") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "05f9p6mxcwfc5zjw8zlzlxrvsp7843ywziv2j7spaq11kl66ps6a") (f (quote (("platform" "fluvio-sc/platform"))))))

