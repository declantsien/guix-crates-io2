(define-module (crates-io fl uv fluvio-system-util) #:use-module (crates-io))

(define-public crate-fluvio-system-util-0.1.0 (c (n "fluvio-system-util") (v "0.1.0") (d (list (d (n "fluvio-types") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "0kcam09wrchdv6xdwcxyxzv8297kcqyshzyb5zwj50vpab7pr4wh") (f (quote (("fixture") ("default"))))))

(define-public crate-fluvio-system-util-0.1.1 (c (n "fluvio-system-util") (v "0.1.1") (d (list (d (n "fluvio-types") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "1xx18qacs7mxdxzg1ajzr87rk16ir1c406shpf3xg2m8h2i2i2b0") (f (quote (("fixture") ("default"))))))

