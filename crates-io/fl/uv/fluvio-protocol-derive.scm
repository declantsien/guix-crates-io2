(define-module (crates-io fl uv fluvio-protocol-derive) #:use-module (crates-io))

(define-public crate-fluvio-protocol-derive-0.1.0 (c (n "fluvio-protocol-derive") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w1x4gnxsfmndpr8avma48dpgbbdx784dyywj2bgrg6q7zw238c4")))

(define-public crate-fluvio-protocol-derive-0.1.1 (c (n "fluvio-protocol-derive") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zddzj3l3iiv506mqhlmwq8jz4pi57p6nb9543zzz1x6n0aaz81g")))

(define-public crate-fluvio-protocol-derive-0.2.0 (c (n "fluvio-protocol-derive") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03y5z4066pw1faapcp42vp36flz7q7gsgjjmc50gvzxwx9gbp8n1")))

(define-public crate-fluvio-protocol-derive-0.2.1 (c (n "fluvio-protocol-derive") (v "0.2.1") (d (list (d (n "fluvio-protocol") (r "^0.5.0") (f (quote ("derive" "api"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "04h29cssdws4nrsds8shyffkx164ydk9330fskz36xr7j8lxd9f7")))

(define-public crate-fluvio-protocol-derive-0.3.0 (c (n "fluvio-protocol-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0gc05yzgxr6bji01ai9607z6kfrxbgr5jlh1zb5vxham8yv5v8wd")))

(define-public crate-fluvio-protocol-derive-0.3.1 (c (n "fluvio-protocol-derive") (v "0.3.1") (d (list (d (n "fluvio-protocol") (r "^0.6") (f (quote ("derive" "api"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "096cjc3qzjdmwgj90cfw580p5vi4ny7zcqf0wf6w6jmi3rwi9fd3")))

(define-public crate-fluvio-protocol-derive-0.3.2 (c (n "fluvio-protocol-derive") (v "0.3.2") (d (list (d (n "fluvio-protocol") (r "^0.6") (f (quote ("derive" "api"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "19rxma0phbnll401ikmmha2ad1i879zwq79s6kyq3vmbr71l983j")))

(define-public crate-fluvio-protocol-derive-0.4.0 (c (n "fluvio-protocol-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1z4lbv0krkfwcq86slmlwpbkqxwajzfwbn2svhna8kr6dy5rszdi")))

(define-public crate-fluvio-protocol-derive-0.4.1 (c (n "fluvio-protocol-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0nb62nbsng3jb7y1p2jrhgqmqbppi5rihy5yxp2pc5viclk34azy")))

(define-public crate-fluvio-protocol-derive-0.4.2 (c (n "fluvio-protocol-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fvk9pp0x9bz6qkgxlv7lsdidfjqva7m0jpmm2k5in8ggr2v1q9p")))

(define-public crate-fluvio-protocol-derive-0.4.3 (c (n "fluvio-protocol-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "07sfmk5xm714bvywz3qsn2kawx232bjgbs4iq3mc0nb11qwmmpg1")))

(define-public crate-fluvio-protocol-derive-0.4.4 (c (n "fluvio-protocol-derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1v7fyf8mjqqlvzgvgwkqnzm0apqsdkgi83vvc0la6xz8zbxa7qav")))

(define-public crate-fluvio-protocol-derive-0.4.5 (c (n "fluvio-protocol-derive") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1g0bdcaasgwwvxzhgkpjkcswyy272rfzhkc5q4pf0i4hil8q56bf")))

(define-public crate-fluvio-protocol-derive-0.5.0 (c (n "fluvio-protocol-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1yylq4cb61pzmzqvva4gfz9pcad05l5khpnzbacwrnf9qc38lfgm")))

(define-public crate-fluvio-protocol-derive-0.5.1 (c (n "fluvio-protocol-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0rqd5yy9k3s0phmvygpkk88n8r0wy7khfbnckd7zilvz39vm9fm1")))

(define-public crate-fluvio-protocol-derive-0.5.2 (c (n "fluvio-protocol-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0zsb1iwv2allq74yvzf8z3ipgbzlm2p30zdr8v2p2c00ksng8axr")))

(define-public crate-fluvio-protocol-derive-0.5.3 (c (n "fluvio-protocol-derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0dyvnpl3xq75cp0l3r5r3lc1y21sihi2d78g2hb8q9jd95dwvq14")))

(define-public crate-fluvio-protocol-derive-0.5.4 (c (n "fluvio-protocol-derive") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "182cy5wwivpifh93606fy9a1myrvg17p42xavrifx5bb7c685a8k")))

