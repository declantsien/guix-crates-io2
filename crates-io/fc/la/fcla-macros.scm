(define-module (crates-io fc la fcla-macros) #:use-module (crates-io))

(define-public crate-fcla-macros-0.1.0 (c (n "fcla-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w1ky4hgbcpkzfc4sm3gvb1mh8qwsgklzl35cdfcjm7hf6cs1h3k")))

