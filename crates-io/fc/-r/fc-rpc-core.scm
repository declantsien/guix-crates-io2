(define-module (crates-io fc -r fc-rpc-core) #:use-module (crates-io))

(define-public crate-fc-rpc-core-0.1.0 (c (n "fc-rpc-core") (v "0.1.0") (h "034zcz9rmkvm6bc96nl5v7p2bqrg40a550cx9p7jld6lczdx5yyh")))

(define-public crate-fc-rpc-core-1.0.0 (c (n "fc-rpc-core") (v "1.0.0") (d (list (d (n "ethereum-types") (r "^0.11.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^15.0.0") (d #t) (k 0)) (d (n "jsonrpc-core-client") (r "^14.0.3") (d #t) (k 0)) (d (n "jsonrpc-derive") (r "^14.0.3") (d #t) (k 0)) (d (n "jsonrpc-pubsub") (r "^15.0.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qrh6k42glbwm1s2sl4w0f2k4h0i5l7bkmwk7jis8vh92g5rqd0r")))

