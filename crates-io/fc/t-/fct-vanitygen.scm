(define-module (crates-io fc t- fct-vanitygen) #:use-module (crates-io))

(define-public crate-fct-vanitygen-0.1.0 (c (n "fct-vanitygen") (v "0.1.0") (d (list (d (n "bs58") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.9.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0g5ndfyj3phq2zmir0fpb53qb2zbbgg1n648nhzxnw912r5zz4ym")))

