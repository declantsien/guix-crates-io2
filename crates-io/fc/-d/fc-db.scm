(define-module (crates-io fc -d fc-db) #:use-module (crates-io))

(define-public crate-fc-db-1.0.0 (c (n "fc-db") (v "1.0.0") (d (list (d (n "codec") (r "^2.0.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")) (d (n "kvdb") (r "^0.9.0") (d #t) (k 0)) (d (n "kvdb-rocksdb") (r "^0.11.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "sp-core") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-database") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^3.0.0") (d #t) (k 0)))) (h "1f13ijmvz553b9i6986djkjp52ll8y9l83h6czq9gpkwqfpbqisl")))

