(define-module (crates-io fc -m fc-mapping-sync) #:use-module (crates-io))

(define-public crate-fc-mapping-sync-1.0.0 (c (n "fc-mapping-sync") (v "1.0.0") (d (list (d (n "fc-consensus") (r "^1.0.0") (d #t) (k 0)) (d (n "fc-db") (r "^1.0.0") (d #t) (k 0)) (d (n "fp-consensus") (r "^1.0.0") (d #t) (k 0)) (d (n "fp-rpc") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("compat"))) (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "sc-client-api") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-api") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-blockchain") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^3.0.0") (d #t) (k 0)))) (h "0hbzyn4wsnsfpi2pk8ws5f8jaz469gdkl9lbkakz2apz43fz5qal")))

