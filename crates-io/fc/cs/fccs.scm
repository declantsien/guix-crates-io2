(define-module (crates-io fc cs fccs) #:use-module (crates-io))

(define-public crate-fccs-0.1.0 (c (n "fccs") (v "0.1.0") (h "1wiggym8n1yz9f8rvm3i7gyfbmhdjy5hipmp9cpkmfnvprcrbmsm")))

(define-public crate-fccs-0.1.1 (c (n "fccs") (v "0.1.1") (h "077324945s0paj4wa867znmipz0s6x9wqchaxmrwffbrwi1ax1s4")))

(define-public crate-fccs-0.1.2 (c (n "fccs") (v "0.1.2") (h "1dj6g38zjmvvd48xpz49xjiph0wbpaaaz6jf9amnk7k0w4j7infz")))

(define-public crate-fccs-0.1.3 (c (n "fccs") (v "0.1.3") (h "1jfkkjw6l1pkdnf4m9w4zv1zr8k5kwxqa12hlb5j1gmhkl1s4jcl")))

(define-public crate-fccs-0.1.4 (c (n "fccs") (v "0.1.4") (d (list (d (n "windows") (r "^0.18.0") (d #t) (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (k 1)))) (h "1dkrgyxmm7jwc8daw8pyycwblrd9dz7a0i3zlq4319ly02a6k55b")))

(define-public crate-fccs-0.1.5 (c (n "fccs") (v "0.1.5") (d (list (d (n "windows") (r "^0.18.0") (d #t) (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (k 1)))) (h "1wzj87lhkrsnyvf7kpsf3jwpyhwpvc3cvnk6pyj86aa7c6q0divb")))

