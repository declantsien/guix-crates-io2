(define-module (crates-io fc _a fc_add) #:use-module (crates-io))

(define-public crate-fc_add-0.1.0 (c (n "fc_add") (v "0.1.0") (h "1b8pdyxs4zzbdsqsxjdak52rhn1v453y7pd0qbw85pd6w02g5lp5") (y #t)))

(define-public crate-fc_add-0.2.0 (c (n "fc_add") (v "0.2.0") (h "0fj82397933jxlkiq1h7pimyppwc7vq0cg2cq03a172wnnhl14vp")))

