(define-module (crates-io fc gi fcgi) #:use-module (crates-io))

(define-public crate-fcgi-0.0.1 (c (n "fcgi") (v "0.0.1") (h "1s5521m8wvibpgivcdgddlsvsva3pazz183mshnzcj6l71dz0pc3") (y #t)))

(define-public crate-fcgi-0.0.2 (c (n "fcgi") (v "0.0.2") (h "1m0627x3jq3j9pvri1ril4712nxhapb0n6j94p37wfsbddc470v9") (y #t)))

