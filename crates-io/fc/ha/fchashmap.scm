(define-module (crates-io fc ha fchashmap) #:use-module (crates-io))

(define-public crate-fchashmap-0.1.1 (c (n "fchashmap") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)))) (h "0zarmiijcgj9qwkzvjw28p66sl1gh51y4g4mh2443fa1ld9gf9gp") (y #t)))

(define-public crate-fchashmap-0.1.2 (c (n "fchashmap") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)))) (h "1jil46sspapxirv14ldk15vwpdd1dpq825q44v92l106j3hqkrq9") (y #t)))

(define-public crate-fchashmap-0.1.3 (c (n "fchashmap") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)))) (h "1xzpqmmh5jj37y3xkzflyshrisyz5hv4dr7ny7dz94s31568k76w")))

