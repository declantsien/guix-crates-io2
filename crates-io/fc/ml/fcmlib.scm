(define-module (crates-io fc ml fcmlib) #:use-module (crates-io))

(define-public crate-fcmlib-0.1.0 (c (n "fcmlib") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "10rzi0yymbg6lhgk0i9bm5qmhwnrk12wsfi3rf78gsfyv2bp2yq0")))

