(define-module (crates-io fc nt fcnt) #:use-module (crates-io))

(define-public crate-fcnt-0.2.0 (c (n "fcnt") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)))) (h "1vz7j02d5gixr6rvkcmry7y1f56f8y7a4hm1kc81c3nq94irzb2r") (y #t)))

(define-public crate-fcnt-0.2.1 (c (n "fcnt") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)))) (h "1yilacr35dm7iplp1795fr2b6xrb841b7nlx50z8rr5kvhbcqjri") (y #t)))

(define-public crate-fcnt-0.2.2 (c (n "fcnt") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)))) (h "10b0009y83rivxlxc0h03758hrx09hi0z8h9fn455f9dmfx7bw6b") (y #t)))

(define-public crate-fcnt-0.2.3 (c (n "fcnt") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0rlzdyqs8m0r8n5sipx1ryaws71fgrvmnh4yaj7hrk3jj3w25br8")))

(define-public crate-fcnt-0.2.5 (c (n "fcnt") (v "0.2.5") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "12a56kib7i4gv6nyv3hxmjnf0ncm94c46rzpqi6y4jlnqxjilrp8")))

(define-public crate-fcnt-0.2.6 (c (n "fcnt") (v "0.2.6") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "05rn21ag2isrilandlby7913gamzn0qmiryi73q81w4zzv6wljzr")))

(define-public crate-fcnt-0.2.7 (c (n "fcnt") (v "0.2.7") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "09fs1blwkd11lkcgc30vd6yc3rx8bi712y9sdzsl50yb4j7az0fc")))

