(define-module (crates-io fc e_ fce_cli) #:use-module (crates-io))

(define-public crate-fce_cli-0.1.0 (c (n "fce_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fce-wit-generator") (r "^0.1.1") (d #t) (k 0)) (d (n "fce-wit-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "0prsw6dl987g14m4bqilwnh6ldxg9a4hlyfyk7wr15i0w2q2z0dd") (y #t)))

