(define-module (crates-io fc wt fcwt) #:use-module (crates-io))

(define-public crate-fcwt-0.1.0 (c (n "fcwt") (v "0.1.0") (d (list (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "0hmmw380bzvmffjf62xc6cn5wqv6wi0wlx2gh7d7wk2cc7wf6vbv")))

(define-public crate-fcwt-0.1.1 (c (n "fcwt") (v "0.1.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "eframe") (r "^0.26.2") (f (quote ("wgpu"))) (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (d #t) (k 2)) (d (n "egui_plot") (r "^0.26.2") (d #t) (k 2)) (d (n "fftw") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive" "alloc"))) (d #t) (k 2)))) (h "0kd47s3qsi4wrp15qa1k7rpcqgl17yl574h5y64hph10zdfzdbgz") (s 2) (e (quote (("fftw" "dep:fftw"))))))

(define-public crate-fcwt-0.1.3 (c (n "fcwt") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "eframe") (r "^0.26.2") (f (quote ("wgpu"))) (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (d #t) (k 2)) (d (n "egui_plot") (r "^0.26.2") (d #t) (k 2)) (d (n "fftw") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.39") (d #t) (k 2)) (d (n "puffin") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "puffin_egui") (r "^0.26") (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive" "alloc"))) (d #t) (k 2)))) (h "0pz6s3ayzk773fjb8dgpwmkg3nphl2y3zx1bml3h2hpl98341crj") (s 2) (e (quote (("profile" "dep:puffin") ("fftw" "dep:fftw"))))))

