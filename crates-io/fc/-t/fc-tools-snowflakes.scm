(define-module (crates-io fc -t fc-tools-snowflakes) #:use-module (crates-io))

(define-public crate-fc-tools-snowflakes-0.1.0 (c (n "fc-tools-snowflakes") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)))) (h "0skr6mdhcg1bi1ic2jjildfhw23yqfv14kq2pmkj3hhhinyc5p9l")))

(define-public crate-fc-tools-snowflakes-0.1.1 (c (n "fc-tools-snowflakes") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)))) (h "0plbsnm6wi4c7lv6ysz72f99qbxi2wp51s7i0275blwzdmny46nr")))

