(define-module (crates-io fc rc fcrc) #:use-module (crates-io))

(define-public crate-fcrc-0.1.0 (c (n "fcrc") (v "0.1.0") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 2)) (d (n "crc-catalog") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastrand") (r "^1.5.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (o #t) (d #t) (k 1)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "1nr2g01qjkwrq1s5fw7qlrmvpmdyk7as6a4yakjzclgk6swywg0r") (f (quote (("std") ("generate-models" "regex" "reqwest") ("default" "std") ("c_api" "std")))) (y #t)))

