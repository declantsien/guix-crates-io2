(define-module (crates-io fc p_ fcp_cryptoauth) #:use-module (crates-io))

(define-public crate-fcp_cryptoauth-0.1.0 (c (n "fcp_cryptoauth") (v "0.1.0") (d (list (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "hex") (r "~0.2.0") (d #t) (k 0)) (d (n "rust_sodium") (r "~0.1.2") (d #t) (k 0)))) (h "09yf20m9dbr14ayzqv28jmf2zs9f8pfns8fmjj4lldppf7awi4nc")))

(define-public crate-fcp_cryptoauth-0.2.0 (c (n "fcp_cryptoauth") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "~0.2.0") (d #t) (k 0)) (d (n "rust_sodium") (r "~0.2.0") (d #t) (k 0)))) (h "09qdiqkqy0mqhcdi3bg37ml2p3bhqi9ajfz6ziwlrkfwri4vb4da")))

(define-public crate-fcp_cryptoauth-0.3.0 (c (n "fcp_cryptoauth") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "rust_sodium") (r "^0.2.0") (d #t) (k 0)))) (h "08pfviw7nm6fncmnpbcifs7wjgg3g31a867pywl7bbmj31339bdd")))

(define-public crate-fcp_cryptoauth-0.4.0 (c (n "fcp_cryptoauth") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r ">=0.3.0, <0.5.0") (d #t) (k 0)) (d (n "rust_sodium") (r ">=0.3.0, <0.11.0") (d #t) (k 0)))) (h "00h7gpd7fyvwjv0m0wcgkji1cg7b5s37xnz5rc4xn1vlvpihh3n7")))

