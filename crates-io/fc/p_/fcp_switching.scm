(define-module (crates-io fc p_ fcp_switching) #:use-module (crates-io))

(define-public crate-fcp_switching-0.1.0 (c (n "fcp_switching") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "fcp_cryptoauth") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "simple_bencode") (r "^0.1.4") (d #t) (k 0)))) (h "0pgk09va09pl761kn0dvgwdlbqlvy3kbx8air7vwikwgqlabdawq")))

