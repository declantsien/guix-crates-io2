(define-module (crates-io fc pv fcpv2) #:use-module (crates-io))

(define-public crate-fcpv2-0.0.1 (c (n "fcpv2") (v "0.0.1") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0q0n262hd5dh2zgl14ia806y8wxz9lanpfnwr8x6r3w5yqgal3a9")))

(define-public crate-fcpv2-0.0.2 (c (n "fcpv2") (v "0.0.2") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1vzcz0i691dmymxmg7598xc8c1932sany4gq1cp29chywmay8yva")))

