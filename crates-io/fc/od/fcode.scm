(define-module (crates-io fc od fcode) #:use-module (crates-io))

(define-public crate-fcode-1.0.0 (c (n "fcode") (v "1.0.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06yhvxbx254vkj8h1yxi5hzn6x23w3y4v6dglw7s63h28zhqkbmx")))

