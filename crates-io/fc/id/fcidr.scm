(define-module (crates-io fc id fcidr) #:use-module (crates-io))

(define-public crate-fcidr-0.1.0 (c (n "fcidr") (v "0.1.0") (h "1rabv0pmxl8b79cyyys28q0cq2dm2qriqi6w37qmkgbbvnhn0d63") (y #t)))

(define-public crate-fcidr-0.2.0 (c (n "fcidr") (v "0.2.0") (h "1n4k6aqzx43jzhlb8a708421blwfy9q4scnv1d6f1l56cdl414yf") (y #t)))

(define-public crate-fcidr-0.3.0 (c (n "fcidr") (v "0.3.0") (h "0wkff25h4lkiy5p91rknqrb8i76558bfmmgp1gb5wd0ff3wkwjr9") (y #t)))

(define-public crate-fcidr-0.4.0 (c (n "fcidr") (v "0.4.0") (h "1jyrig8h3da94giawhasp2dxz11idjdv6dcajss8vnf3zpgbczwm") (y #t)))

(define-public crate-fcidr-0.5.0 (c (n "fcidr") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0yp7q0jynw9hrw8pgsyhkkkc45kmnsyi0gk1k0l6sf4r35a0jjg6") (y #t)))

(define-public crate-fcidr-0.5.1 (c (n "fcidr") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11jfrsv9jy1gi2wc31ypa787mj5hzksxs340aa1yk9y9c2ala51v") (y #t)))

(define-public crate-fcidr-0.6.0 (c (n "fcidr") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jidkq7mdszrdh62295dwqxlv0bvzarcbb4nk7h1ih7hxwg02kcx")))

(define-public crate-fcidr-0.7.0 (c (n "fcidr") (v "0.7.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g463sbry0wfv19v4cyj5shbfvgfl58gc4bmw8887fijgpysvr0m") (r "1.70.0")))

(define-public crate-fcidr-0.7.1 (c (n "fcidr") (v "0.7.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1gmv5vxjbj52hhmwsginrf3iqjazd0wi7qkn7rsym5p99bw5sdwj") (r "1.70.0")))

(define-public crate-fcidr-0.7.2 (c (n "fcidr") (v "0.7.2") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vc1wly8pc5qkxn4vpf6zpwdydyh893s5ckrr941jg6h7l0z3f0n") (r "1.70.0")))

(define-public crate-fcidr-0.8.0 (c (n "fcidr") (v "0.8.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1asw56na5yw1rahwi9rh3wx14h8cvpc044wjxdw4pcprpvhrcgqz") (r "1.70.0")))

(define-public crate-fcidr-0.8.1 (c (n "fcidr") (v "0.8.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02b5rrpd3nkfpl6vqlk0hxwx78qqwh577i08f2c5j9bygqwjvqdy") (r "1.70.0")))

