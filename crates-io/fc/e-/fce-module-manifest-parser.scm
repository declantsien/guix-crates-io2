(define-module (crates-io fc e- fce-module-manifest-parser) #:use-module (crates-io))

(define-public crate-fce-module-manifest-parser-0.1.0 (c (n "fce-module-manifest-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.5.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "0xgr4iqgimwp2vdxaimyd2q88xbc9vckkmrqb8w7y2nlp00v5yh0")))

