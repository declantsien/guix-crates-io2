(define-module (crates-io fc e- fce-sqlite-connector) #:use-module (crates-io))

(define-public crate-fce-sqlite-connector-0.1.0 (c (n "fce-sqlite-connector") (v "0.1.0") (d (list (d (n "fluence") (r "^0.2.3") (d #t) (k 0)) (d (n "fluence") (r "^0.2.3") (d #t) (k 2)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0yqv2mb4x5yq5fc5nw8f8vl1dsnkcr5s2diczx345s9ky6lsfc9r")))

(define-public crate-fce-sqlite-connector-0.1.1 (c (n "fce-sqlite-connector") (v "0.1.1") (d (list (d (n "fluence") (r "^0.2.7") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1z5q9w5jvvyypsbzmpkm0672p8kw7qalqnzpn5rsxspikbyy30cl")))

(define-public crate-fce-sqlite-connector-0.1.2 (c (n "fce-sqlite-connector") (v "0.1.2") (d (list (d (n "fluence") (r "^0.2.8") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "079wc8d14xw4dbk16v6pq69cc24y8x0x48diazjxmy248z1bzadl")))

(define-public crate-fce-sqlite-connector-0.1.3 (c (n "fce-sqlite-connector") (v "0.1.3") (d (list (d (n "fluence") (r "^0.2.9") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0099nm38n6f0d3amycd2mb260s2iqszi48nl9g0salr4wl70jrc1")))

(define-public crate-fce-sqlite-connector-0.1.4 (c (n "fce-sqlite-connector") (v "0.1.4") (d (list (d (n "fluence") (r "^0.4.2") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0yr8qamxfv4x3s357ajb7y0immvjcvj12xfp7adslqaxqjkbskz8")))

(define-public crate-fce-sqlite-connector-0.1.5 (c (n "fce-sqlite-connector") (v "0.1.5") (d (list (d (n "fluence") (r "^0.2.9") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "043p6m90dcja35d82balm8rk4vlbqgyjsxzs7cj3llnz9vayzf59")))

(define-public crate-fce-sqlite-connector-0.2.0 (c (n "fce-sqlite-connector") (v "0.2.0") (d (list (d (n "fluence") (r "^0.4.2") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0mhbqrx6hj7l4xs0xi3x8ijajqa770ir893b84c586bzzyfs2ay0")))

(define-public crate-fce-sqlite-connector-0.3.0 (c (n "fce-sqlite-connector") (v "0.3.0") (d (list (d (n "fluence") (r "^0.5.0") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1nfvqm3ma7w750fx285hxk5y0ysbxs0xcqidw44b31da9nggciix")))

(define-public crate-fce-sqlite-connector-0.4.0 (c (n "fce-sqlite-connector") (v "0.4.0") (d (list (d (n "fluence") (r "^0.6.1") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "09mlhnhpwlhp4dsjrn9gs7qijw7izx1cns9v5xqsv8faaj4ga5yd")))

