(define-module (crates-io fc e- fce-module-info-parser) #:use-module (crates-io))

(define-public crate-fce-module-info-parser-0.1.0 (c (n "fce-module-info-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fluence-sdk-main") (r "^0.6.1") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasmer-core") (r "^0.17.0") (d #t) (k 0) (p "wasmer-runtime-core-fl")))) (h "03d9l130yysa1c533mrhh2mkc776c2gnpzgygampwyz1y4xvc4lg")))

