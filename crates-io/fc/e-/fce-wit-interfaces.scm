(define-module (crates-io fc e- fce-wit-interfaces) #:use-module (crates-io))

(define-public crate-fce-wit-interfaces-0.1.0 (c (n "fce-wit-interfaces") (v "0.1.0") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "^0.17.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "10hi3xzi9pkzbfdy0rr4i8r57j3iyndi36ji7dnynrz8c6lcja0z")))

(define-public crate-fce-wit-interfaces-0.1.1 (c (n "fce-wit-interfaces") (v "0.1.1") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1bi54iqdn407pyazb4yjcv1bnib0cm0hz5gngpbbd5xc66b41yxk")))

(define-public crate-fce-wit-interfaces-0.1.2 (c (n "fce-wit-interfaces") (v "0.1.2") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.5") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0s40jyqr4w8zcfihxzlcj91pbf106nsqvqmfmz2vbyzfccinzmin")))

(define-public crate-fce-wit-interfaces-0.1.3 (c (n "fce-wit-interfaces") (v "0.1.3") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.6") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0gnrqhsignddw76kdwn5pn2q2qdrla7h8br8b7j644j9n50n2fmg")))

(define-public crate-fce-wit-interfaces-0.1.4 (c (n "fce-wit-interfaces") (v "0.1.4") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.9") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0d8afc2946j3ymr702vwcchvfqlgl2xdk7fhhi76a1nmpkrdl3n2")))

(define-public crate-fce-wit-interfaces-0.1.5 (c (n "fce-wit-interfaces") (v "0.1.5") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.9") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "06ggs91wk2l3a67m5cnn0j5srcnbbagk2lw3blijl0x2gggq0jqj")))

(define-public crate-fce-wit-interfaces-0.1.6 (c (n "fce-wit-interfaces") (v "0.1.6") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.10") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0bv6m1ff8r6iwj6ij7877l9ifsrp3jjwwbjglpxv2ap686gv87xg")))

(define-public crate-fce-wit-interfaces-0.1.7 (c (n "fce-wit-interfaces") (v "0.1.7") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "^0.17.16") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "06mwxsbg0743l28p49smijim7cck0ri25qsmxpsmnf1j69sblpdd")))

(define-public crate-fce-wit-interfaces-0.1.8 (c (n "fce-wit-interfaces") (v "0.1.8") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.17") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0pqhpnqsj2250l6qmnx8i5dpp9sa850b819zcijlnp7l9brj3idr")))

(define-public crate-fce-wit-interfaces-0.1.9 (c (n "fce-wit-interfaces") (v "0.1.9") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.20") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "08cg58w1g5xqnqb32440cdanqaslxnm8qagavf3kmy6c5bv8kkcz")))

(define-public crate-fce-wit-interfaces-0.1.10 (c (n "fce-wit-interfaces") (v "0.1.10") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.24") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0rzzhbr38yhsvvi320p7mih92pkw3pvw4k77glmw3p64ik2y9asm")))

(define-public crate-fce-wit-interfaces-0.1.16 (c (n "fce-wit-interfaces") (v "0.1.16") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.24") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1yss6nqllnixqxa1i06j1nc3jxgjd57c6n9fqpm1ynlp1lgk4kil")))

(define-public crate-fce-wit-interfaces-0.1.17 (c (n "fce-wit-interfaces") (v "0.1.17") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.24") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "16f8p7085a7hjy4n6j2jm2i3prmm9nj1d7vi3nymdsl1wjnv83sd")))

(define-public crate-fce-wit-interfaces-0.1.25 (c (n "fce-wit-interfaces") (v "0.1.25") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.24") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1d9w4bikcqw01cs84lwdfqbqcb68k3aiq815nhaxy5dhks4c0cnp")))

(define-public crate-fce-wit-interfaces-0.1.11 (c (n "fce-wit-interfaces") (v "0.1.11") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.24") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0iak73p3xaf7wgkpcikikmkva9is38qw7x1qmx2dbshhkx9lxqs5")))

(define-public crate-fce-wit-interfaces-0.1.28 (c (n "fce-wit-interfaces") (v "0.1.28") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.24") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "00npm5s2r2d3nvyxdp9q7h34rdcwjlqp2s0rhcxzcs8alka7bbg8")))

(define-public crate-fce-wit-interfaces-0.1.29 (c (n "fce-wit-interfaces") (v "0.1.29") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.17.24") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0n6bxy8pw12czimdy7z1wjpb5f7w9qr6avbzy9rcjpjjsssaziph")))

(define-public crate-fce-wit-interfaces-0.2.0 (c (n "fce-wit-interfaces") (v "0.2.0") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "=0.18.3") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "0xpbj7jdsvafl89shfqizg3jkhrvq691v6iq969skax1gzdbd1y8")))

(define-public crate-fce-wit-interfaces-0.3.0 (c (n "fce-wit-interfaces") (v "0.3.0") (d (list (d (n "multimap") (r "^0.8.1") (d #t) (k 0)) (d (n "wasmer-wit") (r "^0.20.0") (d #t) (k 0) (p "wasmer-interface-types-fl")))) (h "1hbrv3vjrd1p6lbc3q9llk4p6xa5awxh1b9igln17pgx39mawh5w")))

