(define-module (crates-io fc e- fce-timestamp-macro) #:use-module (crates-io))

(define-public crate-fce-timestamp-macro-0.6.0 (c (n "fce-timestamp-macro") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0a0bnv2mcgsswc5grhipjp387q80kzsfda9rmycw9hjn58nmnd27")))

(define-public crate-fce-timestamp-macro-0.6.1 (c (n "fce-timestamp-macro") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "1c0bc0hp1sa9m85az6rnd8ws97cl5gbyjzg8hhn3nn0254qacn9w")))

