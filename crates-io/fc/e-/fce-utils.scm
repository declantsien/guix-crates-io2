(define-module (crates-io fc e- fce-utils) #:use-module (crates-io))

(define-public crate-fce-utils-0.1.0 (c (n "fce-utils") (v "0.1.0") (h "1xvlyaw6l5hgf7gjcfn6y6xj2ld2j6xs735s2hsy11szz9irvpfw")))

(define-public crate-fce-utils-0.1.16 (c (n "fce-utils") (v "0.1.16") (h "0sip9r7g0hqyc4p34bh54dm25bxmc72z44x56h6155vws7lnnx8h")))

(define-public crate-fce-utils-0.1.17 (c (n "fce-utils") (v "0.1.17") (h "0rpc268gfi09a45a32mbx2l0ci06xpknxrrrjax5gwlmg6xsyqz2")))

(define-public crate-fce-utils-0.1.25 (c (n "fce-utils") (v "0.1.25") (h "1lsnzl52x6xh4ssllpry7zrzax70a076akhwmpvg1cdwivl3g55l")))

(define-public crate-fce-utils-0.1.1 (c (n "fce-utils") (v "0.1.1") (h "1afdk2zjsqhxn0kyhq6r110lfv0kfh1isj3ybj9bamz9qlprb7cz")))

(define-public crate-fce-utils-0.1.28 (c (n "fce-utils") (v "0.1.28") (h "0iljw1w5hcqd1mmna33x03hkf1jd76a3d868qk9cvg8nxbs6b4zz")))

(define-public crate-fce-utils-0.1.29 (c (n "fce-utils") (v "0.1.29") (h "0phqgq8wifpf9jccx9qi4zdfjv7hlld8qa2f0gq09fnc5n41d9dp")))

(define-public crate-fce-utils-0.2.0 (c (n "fce-utils") (v "0.2.0") (h "02biaa0g198z76cq444v9s3jvdw3i0yk5a0asf9g9i3h31qrh9wj")))

