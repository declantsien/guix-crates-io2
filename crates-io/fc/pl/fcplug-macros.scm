(define-module (crates-io fc pl fcplug-macros) #:use-module (crates-io))

(define-public crate-fcplug-macros-0.1.0 (c (n "fcplug-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c4i4c475nz9avmn60fxzx18h2dc426plxyw7kniz5zb5bc5bn2b")))

(define-public crate-fcplug-macros-0.2.0 (c (n "fcplug-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ds6qm46nvahkghnvxzkhswpal365gkcscinvnh5gacmfvvh9b36")))

(define-public crate-fcplug-macros-0.2.1 (c (n "fcplug-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bj7mrdgcgjbdf9rqa8z36by2qg7hxj6hdksqv5gf0p2xn770ip7")))

(define-public crate-fcplug-macros-0.2.2 (c (n "fcplug-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jg7iw60nz8apyagklcrl9jvq6fyarjc8gia6vkgz654xc5rhnp3")))

(define-public crate-fcplug-macros-0.2.3 (c (n "fcplug-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0swq3586f1nv2hc6r335578i4kvgcwa17s1rvf0swwsn7jh05p5r")))

(define-public crate-fcplug-macros-0.2.4 (c (n "fcplug-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pf418mrvbjb80jhnx51zgb5jcql4sg10rkqb3jskdxy5r4r5gd5")))

