(define-module (crates-io fc pl fcplug-rclib) #:use-module (crates-io))

(define-public crate-fcplug-rclib-0.1.0 (c (n "fcplug-rclib") (v "0.1.0") (d (list (d (n "fcplug-build") (r "^0.1.0") (d #t) (k 1)) (d (n "fcplug-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)))) (h "1g7ziyjmh8kj0b1cf8cp5wqp2kl1y03vv17mh4mqklhs2iiz9p00")))

(define-public crate-fcplug-rclib-0.2.0 (c (n "fcplug-rclib") (v "0.2.0") (d (list (d (n "defer-lite") (r "^1.0.0") (d #t) (k 0)) (d (n "fcplug-build") (r "^0.2.0") (d #t) (k 1)) (d (n "fcplug-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.5.26") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0b2c5paxqdgvrh90351pmhcbh2mcf5agp9p3jhv0ryk9bbwrjcdc")))

(define-public crate-fcplug-rclib-0.2.1 (c (n "fcplug-rclib") (v "0.2.1") (d (list (d (n "defer-lite") (r "^1.0.0") (d #t) (k 0)) (d (n "fcplug-build") (r "^0.2.1") (d #t) (k 1)) (d (n "fcplug-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.5.26") (d #t) (k 0)) (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0m10izwzspplnmvfpjx2fynpnz2665sblsxsw8mlnf82bmlxfy02")))

