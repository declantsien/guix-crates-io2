(define-module (crates-io fc s_ fcs_rs) #:use-module (crates-io))

(define-public crate-fcs_rs-0.1.0 (c (n "fcs_rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "polars") (r "^0.39.2") (f (quote ("lazy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0jkkcnlr0slxnc3bnncnms22hf0x2rjd9nml2dvj9nids1548z9w")))

