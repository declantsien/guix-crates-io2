(define-module (crates-io fc li fclicache) #:use-module (crates-io))

(define-public crate-fclicache-1.0.0 (c (n "fclicache") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pv5n94xk14gyxqwrsiy3mj8x3d4hbkb43py0b3mvmwq7ycb1s73")))

(define-public crate-fclicache-1.0.1 (c (n "fclicache") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mfydhnfl0fg0a7nm9h37dh4dy1fsqdb9nmqam3nqc07665m90zz")))

(define-public crate-fclicache-1.0.2 (c (n "fclicache") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pgbbfw1g9by4jd3iw0cbn7zsa5b3i3c73sgym507lakhccxxiza")))

