(define-module (crates-io fc sd fcsd) #:use-module (crates-io))

(define-public crate-fcsd-0.1.0 (c (n "fcsd") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1xssg2amdd71il0n6h13jyhg503ga2sr02njcj90gcwfzrih1xp9")))

(define-public crate-fcsd-0.1.1 (c (n "fcsd") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1gjg5zmbm66x14javsh8gzhw0xikhqm4crf2j78kinmqr0892v48")))

(define-public crate-fcsd-0.1.2 (c (n "fcsd") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1ic3l1nn31bbfxsnp5jihaxpv0xvw2v1mqkvahaqr83k0yk9ryz9")))

(define-public crate-fcsd-0.1.3 (c (n "fcsd") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "14j6kjzdli664sb5mbzdjsjsp1rk0l1zmg40yqvnpxmjb0fbv4h8")))

(define-public crate-fcsd-0.2.0 (c (n "fcsd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "07sv450zpdyj2afnazni6jr2w1fnxbmkj6zylfyn1zcv11cxg1wb")))

