(define-module (crates-io fc rl fcrlf) #:use-module (crates-io))

(define-public crate-fcrlf-0.1.0 (c (n "fcrlf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "03p4zbkmj4zdhzqj795xkrycq3sbnfghbwlhmhvi6ngafzpi41mv")))

