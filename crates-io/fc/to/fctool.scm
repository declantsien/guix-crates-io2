(define-module (crates-io fc to fctool) #:use-module (crates-io))

(define-public crate-fctool-0.1.0 (c (n "fctool") (v "0.1.0") (d (list (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "same-file") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "1hfm8lbczhdvz1zx8pgv4iy7js826wvmjbxbyh9x959zn9dzcrjx")))

(define-public crate-fctool-0.1.1 (c (n "fctool") (v "0.1.1") (d (list (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "same-file") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "08z48brn3zh7s7l8hksc1r8j366gfr076896k0a94w9b2b61k8v8")))

(define-public crate-fctool-0.1.2 (c (n "fctool") (v "0.1.2") (d (list (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "same-file") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "1bnbf9mchjskyx4hywg928c87aq2bw99563gp2yn0zz6dmjqnsj3")))

(define-public crate-fctool-0.2.0 (c (n "fctool") (v "0.2.0") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0aqcsr1dhb2mrbqxwb2vpmhnl5r9bs3gnazqsza35xf6lpglmymi")))

(define-public crate-fctool-0.2.1 (c (n "fctool") (v "0.2.1") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "1x36bbc3qqyxlbrfr29rkr2gh0llp5l2mcbd710f3k3ppb7l0m95")))

(define-public crate-fctool-0.2.2 (c (n "fctool") (v "0.2.2") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0yr2xqj4cfrbm766ghgjy3r9lv167dj9m2pahczr5bldpwczsyik")))

(define-public crate-fctool-0.2.3 (c (n "fctool") (v "0.2.3") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0zc6i8gb08kh16jhss2f6m8lc8jygwbwllv4mmln7d17r6cbnsay")))

(define-public crate-fctool-0.2.4 (c (n "fctool") (v "0.2.4") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "1zc1ph3wqd5hrdkaplay832bl8v41ssj4vjh7w0hnq27fgbcls20")))

(define-public crate-fctool-0.2.5 (c (n "fctool") (v "0.2.5") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "100p7s8ynsfz96b2pjczpzzzlmrcl355rnf9g2nc0cmwxjaaf2za")))

(define-public crate-fctool-0.3.0 (c (n "fctool") (v "0.3.0") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "10qz0caak5xrw6d2sj94badpwdynwyprix13ah17s5i39vksxw50")))

(define-public crate-fctool-0.3.1 (c (n "fctool") (v "0.3.1") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "1jqggaviillrqrc70fwymrpl7i2k7lgncs2kxb8j62h6ymymxwqx")))

(define-public crate-fctool-0.3.2 (c (n "fctool") (v "0.3.2") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "0a3b0kpb2yvnmmwc5nr7ya9nx6c6072fcbiqm8kja5472rrr6m3m")))

(define-public crate-fctool-0.3.3 (c (n "fctool") (v "0.3.3") (d (list (d (n "plex") (r "^0.2.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "1v1i56f7xx6szgbmnz70jwv6p4bjc78n914s04qfz7pgyd923fr9")))

(define-public crate-fctool-0.4.0 (c (n "fctool") (v "0.4.0") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "0wk99m9yfz9yyjj7hqi1xz8g288kajn51g5qccs7avb87sr65pi9")))

(define-public crate-fctool-0.4.1 (c (n "fctool") (v "0.4.1") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "1sjil0zzlbrva5av1k79xaya08ndfg1fpz3jajmss6lj0ma923gb")))

(define-public crate-fctool-0.4.9 (c (n "fctool") (v "0.4.9") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "042yy01pfi45cqfrh233fmfwympd7g6wli24kgh8igksgy5iqy0n")))

(define-public crate-fctool-0.5.0 (c (n "fctool") (v "0.5.0") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "0ra3c1sjs174piy19jrkgffyfacy6rfb21cass1k92xyr0aqqsir")))

(define-public crate-fctool-2.0.0 (c (n "fctool") (v "2.0.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 1)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "127zxcdz5fzynmkk3dcxq5jn11yh6jahj1plhrisy4g6hh5rid8c")))

(define-public crate-fctool-2.0.1 (c (n "fctool") (v "2.0.1") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 1)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1bhbcfm1ipk0b9mdn2liim28356j6lp62ncyq48qmjisin7apsna")))

