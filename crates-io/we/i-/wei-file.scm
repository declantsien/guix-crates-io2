(define-module (crates-io we i- wei-file) #:use-module (crates-io))

(define-public crate-wei-file-0.1.1 (c (n "wei-file") (v "0.1.1") (d (list (d (n "lzma-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0wxf7wrsxjyg9q2g517fy6cfh06aajc2xwv68lgnx9pzlw6c6q3d")))

(define-public crate-wei-file-0.1.2 (c (n "wei-file") (v "0.1.2") (d (list (d (n "lzma-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0rlmsgvakrj8smn27fpp69n6646hgi287bbf8mk6xy9rbxpmn3ck")))

