(define-module (crates-io we i- wei-hardware) #:use-module (crates-io))

(define-public crate-wei-hardware-0.1.0 (c (n "wei-hardware") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wei-env") (r "^0.1.0") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.0") (d #t) (k 0)))) (h "1xmfbb2695yn9qjhssvh0kzmh0ddw192ff3qp3sisxb2frsymwjh")))

