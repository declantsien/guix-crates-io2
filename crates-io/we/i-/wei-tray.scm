(define-module (crates-io we i- wei-tray) #:use-module (crates-io))

(define-public crate-wei-tray-0.1.0 (c (n "wei-tray") (v "0.1.0") (d (list (d (n "systray") (r "^0.4.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)) (d (n "wei-env") (r "^0.1.0") (d #t) (k 0)))) (h "02xq957pj4637140357jpq02q771kfmjxykz58hmqycjy9fi97bd")))

(define-public crate-wei-tray-0.2.0 (c (n "wei-tray") (v "0.2.0") (d (list (d (n "systray") (r "^0.4.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)) (d (n "wei-env") (r "^0.1.0") (d #t) (k 0)))) (h "1bf8j13cbqfcz7lha9hnf0iddghm4n4qsbhbfv76lfrgscmk3qp5")))

(define-public crate-wei-tray-0.2.2 (c (n "wei-tray") (v "0.2.2") (d (list (d (n "systray") (r "^0.4.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)))) (h "1mzc5351n61cp0a55qb4n22nd8jfqz7cxmq6l7y25v4iisyzi184")))

(define-public crate-wei-tray-0.2.3 (c (n "wei-tray") (v "0.2.3") (d (list (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "systray") (r "^0.4.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.18") (d #t) (k 0)))) (h "0f7w9rsc6sy74bvzqvhvwff5lrkg6d6yr0b36g8d32gs6xph2cbx")))

(define-public crate-wei-tray-0.2.4 (c (n "wei-tray") (v "0.2.4") (d (list (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "systray") (r "^0.4.0") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.2.5") (d #t) (k 0)))) (h "1bd39h679yhd6850z9zhqd0kgx9db5szcikrcivsyapw060yyajk")))

(define-public crate-wei-tray-0.2.5 (c (n "wei-tray") (v "0.2.5") (d (list (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "systray") (r "^0.4.0") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.2.5") (d #t) (k 0)))) (h "0p91ywsgfwxzqp86adg3mk1nqkq0q3sblhnaqm219vh4alvyy00c")))

(define-public crate-wei-tray-0.2.6 (c (n "wei-tray") (v "0.2.6") (d (list (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "systray") (r "^0.4.0") (d #t) (k 0)) (d (n "tauri") (r "^1.5.2") (f (quote ("system-tray" "icon-ico" "icon-png"))) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.2.5") (d #t) (k 0)))) (h "0hj5p22l4a58ppn04fxkcsz89glyhslr3gmmp8xmkh5m24wvzl8w")))

