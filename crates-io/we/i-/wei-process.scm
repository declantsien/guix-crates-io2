(define-module (crates-io we i- wei-process) #:use-module (crates-io))

(define-public crate-wei-process-0.1.1 (c (n "wei-process") (v "0.1.1") (d (list (d (n "procfs") (r "^0.15") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "procfs") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "tlhelp32" "handleapi" "wincon" "winuser" "psapi"))) (d #t) (k 0)))) (h "0vmdh0f72pmf6x7116i0d07jsp7v496x2pzz6xwsvbzlz3mgy9zr")))

