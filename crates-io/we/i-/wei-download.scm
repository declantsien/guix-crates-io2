(define-module (crates-io we i- wei-download) #:use-module (crates-io))

(define-public crate-wei-download-0.1.1 (c (n "wei-download") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.23") (d #t) (k 0)))) (h "0f4j31wsg5cwmbj5ynz4z2ab8x5l47j3mjip8pr5730rara1mg2w")))

