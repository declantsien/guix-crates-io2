(define-module (crates-io we i- wei-log) #:use-module (crates-io))

(define-public crate-wei-log-0.1.0 (c (n "wei-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0x9gdsvs8vcc04d6hfca6d1z0f2abnqzc3h5m2y1nhgll7kvsn1b")))

(define-public crate-wei-log-0.2.0 (c (n "wei-log") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "10xb8rm8p1d48x8c44ijcmpvwfmxkk4bgaa4pw888ss9gknpm8ag")))

(define-public crate-wei-log-0.2.1 (c (n "wei-log") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0slcvsyj1wlvg7r736768xgm6pk4hb4prf98028q8zs8qsi2y1j9")))

(define-public crate-wei-log-0.2.2 (c (n "wei-log") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "1d96xcy88rljbf641sbgzggsfaspwwahgrkn9az07abj3zbhg9h1")))

(define-public crate-wei-log-0.2.3 (c (n "wei-log") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "17prqiy9yzihmmbbkliic1axh3mfiy5slz38lfimb3xxjxx61ygz")))

(define-public crate-wei-log-0.2.4 (c (n "wei-log") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "12ffj0mq9jg7i4849i6mva5jzb4q598zbsa6azksyibqca428gnl")))

(define-public crate-wei-log-0.2.5 (c (n "wei-log") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0jh48k70v9b3i2lsid6x0q2avwng0h8jylyr666py8lgpnazk2hl")))

(define-public crate-wei-log-0.2.6 (c (n "wei-log") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0y70ryzflz982hbhr69mf7nix8bmn8milclqf8gqn34qsr4p9mak")))

(define-public crate-wei-log-0.2.7 (c (n "wei-log") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "078wnmsxnzxki2g5w3yvg3v43pvp5rk6x0gd0mp6vd2kxxfi44h7")))

(define-public crate-wei-log-0.2.8 (c (n "wei-log") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "1rqpc44ix5daqdkffw4kk9f8szbfz7aamdck4cf80v1kw1r2ws5d")))

(define-public crate-wei-log-0.2.9 (c (n "wei-log") (v "0.2.9") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0sg0vd5f9sbyvr8g1ajs6dnajxmzavxd1ia11grgzf74bh8wb3y1")))

(define-public crate-wei-log-0.2.10 (c (n "wei-log") (v "0.2.10") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "0q79mrs3dgvnapq9ch9abgs4ldw2kb1mfqr82fvz57l4kvv5z5fm")))

(define-public crate-wei-log-0.2.11 (c (n "wei-log") (v "0.2.11") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "0n758cn8jmzj6zsy2vc63i28j3sfc1ls5w3fa0k9011r4m5y7az9")))

(define-public crate-wei-log-0.2.12 (c (n "wei-log") (v "0.2.12") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "1dzncyf96s6dfbjl9x749k24qg1kwxnzlmixcn0ba9vb0p2w6bvd")))

