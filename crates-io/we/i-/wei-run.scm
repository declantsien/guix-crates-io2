(define-module (crates-io we i- wei-run) #:use-module (crates-io))

(define-public crate-wei-run-0.1.0 (c (n "wei-run") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)))) (h "18aykq8dpdggdzkxyhv7svw2vm3fhpn2q9xji7da8b1dwhwm1p5v")))

(define-public crate-wei-run-0.1.1 (c (n "wei-run") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)))) (h "1alxypiprxgd1qkij7h4g5xwx4xrxzb1xqxndl19ssb6vx9z9n2r")))

(define-public crate-wei-run-0.1.2 (c (n "wei-run") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)))) (h "096vrgdf03kia0jkl4yv4h7ibmv2h3rj4ylbknjid1f0p4m6jqs5")))

(define-public crate-wei-run-0.1.3 (c (n "wei-run") (v "0.1.3") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "093b9xw213fh3ddaq45wgvm6vjb95kyr2n4a9803pvysd4x9ax61")))

(define-public crate-wei-run-0.1.4 (c (n "wei-run") (v "0.1.4") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "0imrmr6b8y1ngwkk5g2b5bmkhfrk713n4v3mz8rr4q693pmzn6k4")))

(define-public crate-wei-run-0.1.5 (c (n "wei-run") (v "0.1.5") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1lb8z0rxa07gb56pmm00jx5fppar9hps9lh74fc70inrf1hdgxw3")))

(define-public crate-wei-run-0.1.6 (c (n "wei-run") (v "0.1.6") (d (list (d (n "wei-env") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1a353f9k84rlcqjfa9riv04zbpsq2lm8yr8jasxd8p1v3lr34fx9")))

(define-public crate-wei-run-0.1.7 (c (n "wei-run") (v "0.1.7") (d (list (d (n "wei-env") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1vjk05clqwjp1ryfxlyjacngi0szdkj400hc11l86pxqfpk4h0d1")))

(define-public crate-wei-run-0.1.8 (c (n "wei-run") (v "0.1.8") (d (list (d (n "wei-env") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1cx34fipwj5xhz0q76z94d22v419pwsp1g6bir11bfqcrmqpvl74")))

(define-public crate-wei-run-0.1.9 (c (n "wei-run") (v "0.1.9") (d (list (d (n "wei-env") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "0dqziyi9iai1ykg64fm5xxabma0b71ghzsfb186481q1fqpz4k30")))

(define-public crate-wei-run-0.1.10 (c (n "wei-run") (v "0.1.10") (d (list (d (n "wei-env") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1sq7wxb6iagw0pyyj7l4c9v7y7hi7w82wkxn99p6m7blkgf9sakw")))

(define-public crate-wei-run-0.1.11 (c (n "wei-run") (v "0.1.11") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1kvmavbgm24c5jn19m0h5ndlzjbh9izxv1d9mrwhdc6fmqxv7001")))

(define-public crate-wei-run-0.1.12 (c (n "wei-run") (v "0.1.12") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1gi2s22ws42mbg7jqiw11l4dz4jfm67fpxz5kfm93ypj48nh44k7")))

(define-public crate-wei-run-0.1.13 (c (n "wei-run") (v "0.1.13") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1xxbk6vzj2g5acn78y19717scb3kg94gns9mgwd80f1y7phmhjcv")))

(define-public crate-wei-run-0.1.14 (c (n "wei-run") (v "0.1.14") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1s4df204d5hr76afslsh1ggr0i9wmc6hwcb1dqpn96mvr9bkay98")))

(define-public crate-wei-run-0.1.15 (c (n "wei-run") (v "0.1.15") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "164gciibrq4l24fxa7r9pm67ja7fq5lx4yp7f0qcpw3di95yi5p9")))

(define-public crate-wei-run-0.1.16 (c (n "wei-run") (v "0.1.16") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "tlhelp32" "handleapi"))) (d #t) (k 0)))) (h "17af9izz9j2azkz0n0vmvamv8g5d8ni6a9nxgfrrzmnwavjl5b3v")))

(define-public crate-wei-run-0.1.17 (c (n "wei-run") (v "0.1.17") (d (list (d (n "procfs") (r "^0.15") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "procfs") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "tlhelp32" "handleapi"))) (d #t) (k 0)))) (h "0760yclpldmzgvmq26mgph6s2jmq2wylji50wbw2an9fbg7rlrmp")))

(define-public crate-wei-run-0.1.18 (c (n "wei-run") (v "0.1.18") (d (list (d (n "procfs") (r "^0.15") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "procfs") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "tlhelp32" "handleapi"))) (d #t) (k 0)))) (h "19ic2xlpqmghxhsfnq7a7h645f8cm6cx3954zsnpfdvqrwdh0822")))

(define-public crate-wei-run-0.1.20 (c (n "wei-run") (v "0.1.20") (d (list (d (n "procfs") (r "^0.15") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "procfs") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "tlhelp32" "handleapi" "wincon" "winuser"))) (d #t) (k 0)))) (h "135lkal13kvbm8xr84xz85w9ls8z8yxqb9r77ibra590hq5pznr4")))

(define-public crate-wei-run-0.1.21 (c (n "wei-run") (v "0.1.21") (d (list (d (n "procfs") (r "^0.15") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "procfs") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "tlhelp32" "handleapi" "wincon" "winuser"))) (d #t) (k 0)))) (h "0ilykgiy8xvpblb1yszma2vpbyxwnc2mi789kh660waa5vy4y8zc")))

(define-public crate-wei-run-0.1.22 (c (n "wei-run") (v "0.1.22") (d (list (d (n "procfs") (r "^0.15") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "procfs") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "tlhelp32" "handleapi" "wincon" "winuser"))) (d #t) (k 0)))) (h "1c6d7al2g784c820mnz0w2y83bj8yw3qjmy83bavcvilak1x7z1s")))

(define-public crate-wei-run-0.1.23 (c (n "wei-run") (v "0.1.23") (d (list (d (n "procfs") (r "^0.15") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "procfs") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "tlhelp32" "handleapi" "wincon" "winuser" "psapi"))) (d #t) (k 0)))) (h "0b35lsc9rr6gyr3fpr08r8a4hp0ligckfvml3v2awpi0kfln0k8k")))

(define-public crate-wei-run-0.2.1 (c (n "wei-run") (v "0.2.1") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "0f44zllm04g9ahrahdyzbc94vjw0466zxbnb6gi9zf75zddjsy89")))

(define-public crate-wei-run-0.2.2 (c (n "wei-run") (v "0.2.2") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "0ls66x3y3j4zd5npsf7q6y2ly64vx6v3cvby05yzp8c17g1n5jg1")))

(define-public crate-wei-run-0.2.3 (c (n "wei-run") (v "0.2.3") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "0dba9jwv67raqcidp0y3bfq4zrhy16jm5230vir8adb2xdm5iayx")))

(define-public crate-wei-run-0.2.4 (c (n "wei-run") (v "0.2.4") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "0nbma9i7c67ipv83c0gqzizm13zsi4c32kibinxvdvsfi3zdh82n")))

(define-public crate-wei-run-0.2.5 (c (n "wei-run") (v "0.2.5") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "0g16gnc19lf88kcgb1qvqmiwrs8kqkdizzhq1qy5lflz8jh3b4sw")))

(define-public crate-wei-run-0.2.6 (c (n "wei-run") (v "0.2.6") (d (list (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1ryaixm1h61savaaxism5nxsrx3nac4cb2zasdif6bzai8i0kvvd")))

(define-public crate-wei-run-0.3.1 (c (n "wei-run") (v "0.3.1") (d (list (d (n "wei-env") (r "^0.2.13") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.10") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "00swrwyj962gch6m36c3wipq69jc8a8ipiiwyzw6hbw6wg29hjps")))

(define-public crate-wei-run-0.3.2 (c (n "wei-run") (v "0.3.2") (d (list (d (n "wei-env") (r "^0.2.13") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.10") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "0x611ak8dx11sr86n32cg3qgnrizc498k7iw6xzjykzgkh7h208r")))

