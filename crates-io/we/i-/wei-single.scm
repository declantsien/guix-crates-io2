(define-module (crates-io we i- wei-single) #:use-module (crates-io))

(define-public crate-wei-single-0.3.5 (c (n "wei-single") (v "0.3.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("synchapi" "winnt" "errhandlingapi" "winerror" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17ikpv3cbf3db0rp7a4dgmq4nn2i1dpbwfp4ry49py7cmq9i9p2r")))

(define-public crate-wei-single-0.3.6 (c (n "wei-single") (v "0.3.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("synchapi" "winnt" "errhandlingapi" "winerror" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0x4vp40ibgawkpc59zk04n9lh48c00fsra264sx7n8v1r1vhyx85")))

