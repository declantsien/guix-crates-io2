(define-module (crates-io we i- wei-server) #:use-module (crates-io))

(define-public crate-wei-server-0.1.0 (c (n "wei-server") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.3") (d #t) (k 0)))) (h "1mmp7xn2vnb816r392ar4v5nfvg3qg0d2zyz5aa4dj1y70cr4md3")))

(define-public crate-wei-server-0.1.1 (c (n "wei-server") (v "0.1.1") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)))) (h "1j4a1iyvjszk5gmwdv2qdx9gf6zbq2xdz2ij7bgv62aa4fyj2z15")))

(define-public crate-wei-server-0.1.2 (c (n "wei-server") (v "0.1.2") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.4.4") (f (quote ("cors"))) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.23") (d #t) (k 0)))) (h "1vhh3ngj1gh8ihmpmz4z52r32v9a5j4whl7sn472c2s6fl2a6fn2")))

