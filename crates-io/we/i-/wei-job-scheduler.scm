(define-module (crates-io we i- wei-job-scheduler) #:use-module (crates-io))

(define-public crate-wei-job-scheduler-1.2.1 (c (n "wei-job-scheduler") (v "1.2.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vkmqfaw5igw2mvg4xnkzpdvm2052nxzn2whlfzw5cnhmh9n0xpd")))

(define-public crate-wei-job-scheduler-1.2.2 (c (n "wei-job-scheduler") (v "1.2.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "197sg20bir10p28hyw2q3cyxpjqm7yp09x8slvlljghz8g4730lz")))

