(define-module (crates-io we i- wei-update) #:use-module (crates-io))

(define-public crate-wei-update-0.1.0 (c (n "wei-update") (v "0.1.0") (d (list (d (n "self_update") (r "^0.36.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j5nk4nmv9117gmslbyphjp2arwq6irqhkwwabkgyvz01yij6y68")))

