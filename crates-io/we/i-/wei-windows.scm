(define-module (crates-io we i- wei-windows) #:use-module (crates-io))

(define-public crate-wei-windows-0.1.1 (c (n "wei-windows") (v "0.1.1") (h "10fmsim56ba0iq5w8bgmkx3wb85pw91zanhk9ndkik1g6ma6xq2l")))

(define-public crate-wei-windows-0.1.2 (c (n "wei-windows") (v "0.1.2") (h "00f9v1k120svr8ricfdij68nhp4yinvaddgjbffv3v2hg1mif79j")))

