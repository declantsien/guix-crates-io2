(define-module (crates-io we i- wei-updater) #:use-module (crates-io))

(define-public crate-wei-updater-0.1.1 (c (n "wei-updater") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.5") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.3") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.9") (d #t) (k 0)) (d (n "winrt-notification") (r "^0.5.1") (d #t) (k 0)))) (h "1g8bf0cbakvwwj7cvlvrh3l98zw1phdas2r6xc925yyykjc0spwf")))

