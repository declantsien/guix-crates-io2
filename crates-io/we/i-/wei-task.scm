(define-module (crates-io we i- wei-task) #:use-module (crates-io))

(define-public crate-wei-task-0.1.0 (c (n "wei-task") (v "0.1.0") (h "1ccsjdbxsr8sj52i16ykkqj1zw94mjyfghr68q14pv713z44l5wv")))

(define-public crate-wei-task-0.1.1 (c (n "wei-task") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-env") (r "^0.2.7") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.1") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.8") (d #t) (k 0)))) (h "0jngn7bbyr342b14f2i332qv7axg76h87h58w0ms1z9h3zx72igv")))

