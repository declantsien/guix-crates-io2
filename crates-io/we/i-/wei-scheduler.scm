(define-module (crates-io we i- wei-scheduler) #:use-module (crates-io))

(define-public crate-wei-scheduler-0.1.0 (c (n "wei-scheduler") (v "0.1.0") (d (list (d (n "job_scheduler") (r "^1.2.1") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.3.1") (d #t) (k 0)) (d (n "wei-single") (r "^0.3.5") (d #t) (k 0)))) (h "04ic5zwn9lmg9n4fhars4q43yda3n3f2li3shlc4fsp9dkmnxyfg")))

