(define-module (crates-io we i- wei-env) #:use-module (crates-io))

(define-public crate-wei-env-0.1.0 (c (n "wei-env") (v "0.1.0") (h "17356gv9a88fkk82ypw0grci0lr84s0s2bg7kz3fnzh4jb0445wx")))

(define-public crate-wei-env-0.1.1 (c (n "wei-env") (v "0.1.1") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "05wkd5yyifmy296rdlkmw79ssk0b97v303pws8mz8ri3bw8nx6lh")))

(define-public crate-wei-env-0.2.0 (c (n "wei-env") (v "0.2.0") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0l3zmll99k6cjvbm1nsxrad11jzwxwj3zx3jg9gaz3w43d1i9rcy")))

(define-public crate-wei-env-0.2.1 (c (n "wei-env") (v "0.2.1") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1za1hqcly677m7ris8kvrw3h2qlszaxj6dipxlppll12l2fqh513")))

(define-public crate-wei-env-0.2.2 (c (n "wei-env") (v "0.2.2") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "13sw6wpid2l8csj2iqah9ymc2jlxvml8kaiz1q3bqzvryx59qlwr")))

(define-public crate-wei-env-0.2.3 (c (n "wei-env") (v "0.2.3") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1ggvhiw84dvqhq9fs7alx2wdv769fnsi7n8hj0yrlgjbm79lh5zh")))

(define-public crate-wei-env-0.2.4 (c (n "wei-env") (v "0.2.4") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1vvzlgslss9wqijmia5vryqp61g2msyl6qxj9l5ms463l4hfxih2")))

(define-public crate-wei-env-0.2.5 (c (n "wei-env") (v "0.2.5") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "17kxic4gbzwfnrdq9w80vhnzljn3y2rgk42g4zlb0cgmbqchlzdz")))

(define-public crate-wei-env-0.2.6 (c (n "wei-env") (v "0.2.6") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0k2sm62pvqcyka2lyr8fjccsn285gskv743zm8q6fvn23ayaf953")))

(define-public crate-wei-env-0.2.7 (c (n "wei-env") (v "0.2.7") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "158c48dywjnxid7r8p8pllf3mp56gi9jv7dsvcrwnb86gzzwacqp")))

(define-public crate-wei-env-0.2.8 (c (n "wei-env") (v "0.2.8") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1bw3xvf2zyxwnxnybzfrf893dqi4xb909j9jk9j840hbx851fnwn")))

(define-public crate-wei-env-0.2.9 (c (n "wei-env") (v "0.2.9") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "16l108gk8mcf2ilgmnd8acjxhxxk5zr6nri533lpp5kjhpbnh90z")))

(define-public crate-wei-env-0.2.10 (c (n "wei-env") (v "0.2.10") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1451zdhhf84cnz48qyx7aj11cjf9ppf24djj9rhpi7fbq47mk1p0")))

(define-public crate-wei-env-0.2.11 (c (n "wei-env") (v "0.2.11") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "00q3gcindd86fyw5bl2ibkr84cfjwskr8yxp589lh83c0687z8mv")))

(define-public crate-wei-env-0.2.12 (c (n "wei-env") (v "0.2.12") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0zb5p42zwfsrd7lxldidl0haqalqvaahr121n0h71pmw11yrn1pr")))

(define-public crate-wei-env-0.2.13 (c (n "wei-env") (v "0.2.13") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0m9jka1jm98f5gv9nr4l5mknj4amyk3wqqzhha36m4mcz1vqn78s")))

