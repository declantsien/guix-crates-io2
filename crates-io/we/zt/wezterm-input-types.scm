(define-module (crates-io we zt wezterm-input-types) #:use-module (crates-io))

(define-public crate-wezterm-input-types-0.1.0 (c (n "wezterm-input-types") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc" "derive"))) (o #t) (d #t) (k 0)) (d (n "wezterm-dynamic") (r "^0.2") (d #t) (k 0)))) (h "0zp557014d458a69yqn9dxfy270b6kyfdiynr5p4algrb7aas4kh") (f (quote (("default" "serde"))))))

