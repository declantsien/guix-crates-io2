(define-module (crates-io we zt wezterm-bidi) #:use-module (crates-io))

(define-public crate-wezterm-bidi-0.1.0 (c (n "wezterm-bidi") (v "0.1.0") (d (list (d (n "k9") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wyn8ip8s7v7mhp8m0lwjqb1frkafa65m758q98d8vdjc3wb4xbx") (f (quote (("use_serde" "serde"))))))

(define-public crate-wezterm-bidi-0.2.0 (c (n "wezterm-bidi") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "k9") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wezterm-dynamic") (r "^0.1") (d #t) (k 0)))) (h "0hhb5wxgfp3b240hifid42h2j8bmf1c87z76m0bam25xh1sf1066")))

(define-public crate-wezterm-bidi-0.2.1 (c (n "wezterm-bidi") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "k9") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wezterm-dynamic") (r "^0.1") (d #t) (k 0)))) (h "063b543q312xq0jnpw2km1gpnw8jpwqq4m4x30lnyahvnfb5q2s9")))

(define-public crate-wezterm-bidi-0.2.2 (c (n "wezterm-bidi") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "k9") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wezterm-dynamic") (r "^0.1") (d #t) (k 0)))) (h "0dkcwscvlwnv6lnagxfb08rcd21gfyrxbr7afcjaj3wvycn3hq0m")))

(define-public crate-wezterm-bidi-0.2.3 (c (n "wezterm-bidi") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "k9") (r "^0.12.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wezterm-dynamic") (r "^0.2") (d #t) (k 0)))) (h "1v7kwmnxfplv9kgdmamn6csbn2ag5xjr0y6gs797slk0alsnw2hc")))

