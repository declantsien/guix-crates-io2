(define-module (crates-io we zt wezterm-color-types) #:use-module (crates-io))

(define-public crate-wezterm-color-types-0.1.0 (c (n "wezterm-color-types") (v "0.1.0") (d (list (d (n "csscolorparser") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05gqin7h9f99h4dwc67pn1507vrfd94zals5w0af1ys1hw46dzcd") (f (quote (("use_serde" "serde"))))))

(define-public crate-wezterm-color-types-0.2.0 (c (n "wezterm-color-types") (v "0.2.0") (d (list (d (n "csscolorparser") (r "^0.6") (f (quote ("lab"))) (d #t) (k 0)) (d (n "deltae") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wezterm-dynamic") (r "^0.1") (d #t) (k 0)))) (h "0xvphmrqgg69v9l879xj5lq010z13f5ixi854ykmny6j7m47lvjc") (f (quote (("use_serde" "serde"))))))

(define-public crate-wezterm-color-types-0.3.0 (c (n "wezterm-color-types") (v "0.3.0") (d (list (d (n "csscolorparser") (r "^0.6") (f (quote ("lab"))) (d #t) (k 0)) (d (n "deltae") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wezterm-dynamic") (r "^0.2") (d #t) (k 0)))) (h "15j29f60p1dc0msx50x940niyv9d5zpynavpcc6jf44hbkrixs3x") (f (quote (("use_serde" "serde"))))))

