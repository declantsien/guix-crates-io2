(define-module (crates-io we zt wezterm-dynamic) #:use-module (crates-io))

(define-public crate-wezterm-dynamic-0.1.0 (c (n "wezterm-dynamic") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wezterm-dynamic-derive") (r "^0.1") (d #t) (k 0)))) (h "1al8fmfr852m62mlcr0v2lg3a18icl2sv79zv7jnv9v0rk07hpm7")))

(define-public crate-wezterm-dynamic-0.2.0 (c (n "wezterm-dynamic") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wezterm-dynamic-derive") (r "^0.1") (d #t) (k 0)))) (h "1dbppcd5s509w3b25q2fx2c4c52cwdl61yw1fvh38rx8ryx2icfz")))

