(define-module (crates-io we nv wenv-cli) #:use-module (crates-io))

(define-public crate-wenv-cli-0.1.0 (c (n "wenv-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "tabwriter") (r "^1.3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "winreg") (r "^0.51.0") (d #t) (k 0)))) (h "1s7bi737y7rgb9ji5ckd37pgf5djlkvypfdcfyxa7s3r5flss0am")))

