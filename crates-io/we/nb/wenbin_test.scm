(define-module (crates-io we nb wenbin_test) #:use-module (crates-io))

(define-public crate-wenbin_test-0.1.0 (c (n "wenbin_test") (v "0.1.0") (h "02axsp2jblf6fn52d10430k7v2n69gvy1dmp88gm23rfd9lpp6xk")))

(define-public crate-wenbin_test-0.1.1 (c (n "wenbin_test") (v "0.1.1") (h "09rlkw85l9w5brkydfk2pcpkwrjk5dzngjhs8lzj9s3nxn2129hi")))

(define-public crate-wenbin_test-0.1.2 (c (n "wenbin_test") (v "0.1.2") (h "0lzg822irr1pah76id4fnivkcj2xl8cqak3k8xh61344mnb05ckf")))

