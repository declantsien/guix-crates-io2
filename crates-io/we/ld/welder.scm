(define-module (crates-io we ld welder) #:use-module (crates-io))

(define-public crate-welder-0.1.0 (c (n "welder") (v "0.1.0") (h "16idlxv9j64bz9myks7810mkprivkx7r4i8qm1gql1y4khxwig9c")))

(define-public crate-welder-0.2.0 (c (n "welder") (v "0.2.0") (h "091iwg00hc3vgz006yhmy89fn36f37zavi8p12qppcaljf5xf7cw")))

(define-public crate-welder-0.2.1 (c (n "welder") (v "0.2.1") (h "1l29sbpyknfd5ic3hhbi68q89b5d4i7l2djwz3mb535zasrg50jl")))

(define-public crate-welder-0.3.0 (c (n "welder") (v "0.3.0") (h "02gl8bqidxcdcyryy0blsx0z6d525asgwnc94kxpcxc8jndc5h8x")))

