(define-module (crates-io we ld weldr) #:use-module (crates-io))

(define-public crate-weldr-0.1.0 (c (n "weldr") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1i4y2w5igypiqrqlzimnmz6923d4qd1b4vvp87czjx7bnf8hhy5a")))

(define-public crate-weldr-0.2.0 (c (n "weldr") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0rfy5gvcgby8wq589lhv3wcy9whdzijn973rcnjmcahra2prasni") (f (quote (("default" "cgmath"))))))

(define-public crate-weldr-0.3.0 (c (n "weldr") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0aia40kv0q8wszcrx7x1jjxqkjpwp8iz6px7rssaxqcr1c02dg14")))

(define-public crate-weldr-0.3.1 (c (n "weldr") (v "0.3.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "115fsrbph1q917g2j1vkwjvldyj27w3lphhr5wrbxayv1qjmji5v")))

