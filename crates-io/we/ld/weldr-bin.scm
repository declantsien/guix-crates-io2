(define-module (crates-io we ld weldr-bin) #:use-module (crates-io))

(define-public crate-weldr-bin-0.1.0 (c (n "weldr-bin") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "weldr") (r "^0.3") (d #t) (k 0)))) (h "0izz9fy7zmmgxl4lwh14ypfirnw10pvpzcn49c8vnmz89k7l5b37")))

(define-public crate-weldr-bin-0.2.0 (c (n "weldr-bin") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "weldr") (r "^0.3.1") (d #t) (k 0)))) (h "0y0pzrrgx4mb841yjj1lvbz00aiy51adgbxx7n8v8q06swph39mr")))

