(define-module (crates-io we ll wellington) #:use-module (crates-io))

(define-public crate-wellington-0.0.1 (c (n "wellington") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "handlebars") (r "^1.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.7.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)) (d (n "url_serde") (r "^0.2.0") (d #t) (k 0)))) (h "0mfqkhd8cc6xf5x5a9jznmmnz5dry4zazvvavnlkb6vqkplhxa0a")))

