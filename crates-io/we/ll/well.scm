(define-module (crates-io we ll well) #:use-module (crates-io))

(define-public crate-well-0.0.0 (c (n "well") (v "0.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "git2") (r "^0.17") (f (quote ("vendored-libgit2" "vendored-openssl"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0dvmxrkbm2mxfc3y0wdk2j9kjqi4lx14h8idqdh115nln9xhb178")))

