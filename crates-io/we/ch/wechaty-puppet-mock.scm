(define-module (crates-io we ch wechaty-puppet-mock) #:use-module (crates-io))

(define-public crate-wechaty-puppet-mock-0.1.0-beta.0 (c (n "wechaty-puppet-mock") (v "0.1.0-beta.0") (d (list (d (n "actix") (r "^0.11.0-beta.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "wechaty_puppet") (r "^0.1.0-beta.0") (d #t) (k 0)))) (h "0mbx41dgf26ly2s2fpylnzqzi5z3dpl5cx9qsiml4za55cyl3x31")))

(define-public crate-wechaty-puppet-mock-0.1.0-beta.1 (c (n "wechaty-puppet-mock") (v "0.1.0-beta.1") (d (list (d (n "actix") (r "^0.11") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "wechaty_puppet") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "153y3c7r7saqf6si4w21gxy1mqpbjqi01bdfa4pil42p9jp1dznv")))

