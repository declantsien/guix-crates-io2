(define-module (crates-io we ch wechaty-grpc) #:use-module (crates-io))

(define-public crate-wechaty-grpc-0.1.0 (c (n "wechaty-grpc") (v "0.1.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 1)))) (h "0xfngxx8fw7a5f62bmm2k97d6c6k1lnv6cqpgl7zq2mp64lfb96f")))

(define-public crate-wechaty-grpc-0.2.0 (c (n "wechaty-grpc") (v "0.2.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 1)))) (h "19jb2vgl887m6rhqqcgx2j8nsckhd6hh2nw9ly6ma17xrqbxmmr2")))

(define-public crate-wechaty-grpc-0.3.0 (c (n "wechaty-grpc") (v "0.3.0") (d (list (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "tonic") (r "^0.5") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)))) (h "1fl8qjrzfiaxamxyfs05m4ks2s5k1psjh22cnh0bifq30pdzaqyh")))

