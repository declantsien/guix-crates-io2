(define-module (crates-io we ch wechatpay) #:use-module (crates-io))

(define-public crate-wechatpay-0.1.0 (c (n "wechatpay") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "md5") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "037kvmxi0wmd6lxhm1pcayp5w97fckpg967p6js18vmcj8fpw5vv")))

(define-public crate-wechatpay-0.1.1 (c (n "wechatpay") (v "0.1.1") (d (list (d (n "md5") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1hl4lii2v5nvzj06vx641yb16w1jl3cl3kp0vs912agxm0js4vpf")))

(define-public crate-wechatpay-0.1.2 (c (n "wechatpay") (v "0.1.2") (d (list (d (n "curl") (r "^0.3.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "md5") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "0mcs14wj37imahwrkr89954r7h6li24lrnnmjjjna1qim6br974v")))

(define-public crate-wechatpay-0.1.3 (c (n "wechatpay") (v "0.1.3") (d (list (d (n "curl") (r "^0.3.2") (d #t) (k 0)) (d (n "md5") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "0a2bv55h4rcvjy6d29fhczh4q7q2j4adw2ldlvz38kc2lri7jkwi")))

(define-public crate-wechatpay-0.1.4 (c (n "wechatpay") (v "0.1.4") (d (list (d (n "curl") (r "^0.3.2") (d #t) (k 0)) (d (n "md5") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1447dz977963bjw84qcag3d43il7w52nnp7v0n7h7azypv4a0v8m")))

