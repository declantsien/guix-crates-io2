(define-module (crates-io we ch wechsel) #:use-module (crates-io))

(define-public crate-wechsel-0.1.0 (c (n "wechsel") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0f2bj8m47kla6kap1kqr6q1z79r90gacscrsnml563x3a4a5pr1l")))

(define-public crate-wechsel-0.1.1 (c (n "wechsel") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "03nf84fjh1d9lspyi12rbzqc94rqfmxps75xb7zisfksqakb1as3")))

(define-public crate-wechsel-0.1.3 (c (n "wechsel") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.7.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0vgbc8lmpb2vrcg61ji2qgk4bsnaflhqv1jznpnv2k763rljll14")))

