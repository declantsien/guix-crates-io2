(define-module (crates-io we ch wechat) #:use-module (crates-io))

(define-public crate-wechat-0.1.0 (c (n "wechat") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sxd-document") (r "*") (d #t) (k 0)) (d (n "sxd-xpath") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "08l8iszgv1snp03n7vcr0vnvnjwv6a7rmnnljf0lpapi3mdf1nvr")))

