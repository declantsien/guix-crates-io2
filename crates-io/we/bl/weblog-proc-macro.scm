(define-module (crates-io we bl weblog-proc-macro) #:use-module (crates-io))

(define-public crate-weblog-proc-macro-0.3.0-alpha.1 (c (n "weblog-proc-macro") (v "0.3.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "07fr8l0wlsj8rxi9ip0binipfgp8csdyfhharhwbligq85v3lwds")))

(define-public crate-weblog-proc-macro-0.3.0-beta.1 (c (n "weblog-proc-macro") (v "0.3.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "0nlnq7vh73pjcr1p2l1m2kh5q6cyvmai88ir70qmmjx1b844rxnx")))

(define-public crate-weblog-proc-macro-0.3.0 (c (n "weblog-proc-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "013imwjpdgyi7v4g77sk7bsjfsi1zdr1swj6lc459pjfx27bp6lx")))

(define-public crate-weblog-proc-macro-0.4.0-beta.1 (c (n "weblog-proc-macro") (v "0.4.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "1hq4h7lyb2x84cyypssxgxwpnndvyhgw46agllggy33n4xx41llr")))

