(define-module (crates-io we bl weblab-macros) #:use-module (crates-io))

(define-public crate-weblab-macros-0.1.0 (c (n "weblab-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0143j9hfvkmc0h96c73mqmq2h3gyy8cfyv7y02rfnjdqjz5z4y84")))

(define-public crate-weblab-macros-0.1.1 (c (n "weblab-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0i2q4wqkyr5dk8wyax7b8qd4cwhslgzhhx12ja7khr5nmmi864x2")))

(define-public crate-weblab-macros-0.1.2 (c (n "weblab-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0nkd9ii8xygpvfdd8h08xw31ccal7y0xd0l299whirk9iv61n55q")))

(define-public crate-weblab-macros-0.1.3 (c (n "weblab-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1q3waxysbsx76w9awibzcs86d0n566z8fia8c4v30f8340mcjr67")))

(define-public crate-weblab-macros-0.1.4 (c (n "weblab-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1dkhacsa33rz8jqbgh2kg4m5k5avnsxwidryqm9l0qwsykffy12y")))

(define-public crate-weblab-macros-0.2.0 (c (n "weblab-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "137w0lhaas87m0m1ssqb3p3h5rjp8fyv7gy6bj7wd925izh40lsj")))

(define-public crate-weblab-macros-0.2.1 (c (n "weblab-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1pkviqd50777nws3q5ng14rypk209lkisdld6qzxnpzglypdrjf9")))

(define-public crate-weblab-macros-0.2.2 (c (n "weblab-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "08lblwh27yh29m7x1byffmf4vb83iqpprc7q99dl98klacjiis0v")))

(define-public crate-weblab-macros-0.2.5 (c (n "weblab-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0as90q92hclqzazknkmw0ryz2c8x0gmr310mf65l1hch8x7ncbs8")))

(define-public crate-weblab-macros-0.2.6 (c (n "weblab-macros") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0i5yb2cyq5v6j7q6f0zgmsvh91b70yfqpq148cyz1bxazhx27rwn")))

(define-public crate-weblab-macros-0.2.8 (c (n "weblab-macros") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0rpk1qijf4ki41179g4yk88zv7awrkxgxjix30fmfimzm4minsm4")))

(define-public crate-weblab-macros-0.2.9 (c (n "weblab-macros") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0ay5d6fv66yj7h2209drm2fkp4bvgr0rmaymcdsz97kl5n2g5cbr")))

(define-public crate-weblab-macros-0.2.10 (c (n "weblab-macros") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1g8gkn53vrwbb7vlna0i5ah47q9y80y7l6hmld0yqvbxgflpqsdp")))

(define-public crate-weblab-macros-0.2.12 (c (n "weblab-macros") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "044nzm9alh45pckbxga4yqk8rgxchrf3yr97al4vvcb01r66333b")))

(define-public crate-weblab-macros-0.2.14 (c (n "weblab-macros") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "11j1rhj96yscy0fyxmqgj4rnm53d83nwraa6sp1qdqn9mjqpbg8g")))

(define-public crate-weblab-macros-0.2.15 (c (n "weblab-macros") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0ll9bisihv431vs6izl1jd574lnmsjfpq54qdrhxm577ynrfqr9m")))

(define-public crate-weblab-macros-0.2.16 (c (n "weblab-macros") (v "0.2.16") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0p2xs0k5rfpak29ky9r2iqxdxsxdm60y2cb8bc4sdlbcdsgq454m")))

(define-public crate-weblab-macros-0.2.18 (c (n "weblab-macros") (v "0.2.18") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1i2wvjgv4rayf5qyqaavk2x0x3hblcvxhqnr827ls0gxkp9l4b73")))

(define-public crate-weblab-macros-0.2.20 (c (n "weblab-macros") (v "0.2.20") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0g4p4dvy53g2rfnqb0acszhyjy2snsinmfx7wxpigny93giq8nlr")))

(define-public crate-weblab-macros-0.2.22 (c (n "weblab-macros") (v "0.2.22") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0hmw501g2m8pyxpp8984gkcrhzck068sb2b78nz9ml543srfl1yp")))

(define-public crate-weblab-macros-0.2.24 (c (n "weblab-macros") (v "0.2.24") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "00hmv71cp1jrvg3i3jhsan2xn940wzm4291y9afndzbmp4rpwl0y")))

(define-public crate-weblab-macros-0.2.26 (c (n "weblab-macros") (v "0.2.26") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1ywsnp1n80a82p4ldbq1g680psn91slc64w6ga31r677hr9jh1hh")))

(define-public crate-weblab-macros-0.2.28 (c (n "weblab-macros") (v "0.2.28") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0zxs2h0jz6njhg1kp3ma06zkrkmmq6zs9jr9927z3846yaifk2w3")))

(define-public crate-weblab-macros-0.2.31 (c (n "weblab-macros") (v "0.2.31") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1l5an0x0wh7yrxpzwg9x2g13404w6s4f5m7hijcrnmdgnjxs3jgd")))

(define-public crate-weblab-macros-0.3.0 (c (n "weblab-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "03xah6b0pqzrcfg86qmfclh9fc5z8vwqs50304vpyxfhqh7hdw53")))

(define-public crate-weblab-macros-0.3.1 (c (n "weblab-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0fyicx4zf4imaryw4gj50pba48a97avi2m5pwnfrdxvg1zmc9jw8")))

(define-public crate-weblab-macros-0.3.2 (c (n "weblab-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1187wr9g07das29cbds9qj183n8g8psdg6n381w21xi6sxwwz3s6")))

(define-public crate-weblab-macros-0.3.3 (c (n "weblab-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "046y2nm24qpxfbnmqs5q8alb9lj1ywb1g7v8q1j9l1h9s9nskmp4")))

(define-public crate-weblab-macros-0.3.4 (c (n "weblab-macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "03569hyjkq6r6znckzqn6fgx8k20mf3k6r6zyrmghfpaj743ikm2")))

(define-public crate-weblab-macros-0.3.5 (c (n "weblab-macros") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "10bkjcm3l5p1xy58fa580bxxcjig4998cv1gc48akyc8aqhn6hfs")))

