(define-module (crates-io we bl weblab-runtime-tools) #:use-module (crates-io))

(define-public crate-weblab-runtime-tools-0.1.0 (c (n "weblab-runtime-tools") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml") (r "^0.0.1") (d #t) (k 0)))) (h "1k8hkfh4y5rpp9ir9r6a6cif0gjc14q566v8knby5fhcmazwqssj")))

(define-public crate-weblab-runtime-tools-0.2.1 (c (n "weblab-runtime-tools") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml") (r "^0.0.1") (d #t) (k 0)))) (h "0lpq0zvxyvyw6nwr5xqs9jspsi9y13fiw7bkvbfgzfk9ibzq96s7")))

