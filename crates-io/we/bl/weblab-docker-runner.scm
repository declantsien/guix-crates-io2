(define-module (crates-io we bl weblab-docker-runner) #:use-module (crates-io))

(define-public crate-weblab-docker-runner-0.1.0 (c (n "weblab-docker-runner") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14gzlai0dxxw1y9ndhsgx1kqga45l7gndckqwairn5yjk5l9bwad") (y #t)))

