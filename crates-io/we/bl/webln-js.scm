(define-module (crates-io we bl webln-js) #:use-module (crates-io))

(define-public crate-webln-js-0.1.0 (c (n "webln-js") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "webln") (r "^0.1") (k 0)))) (h "0a0vqzar5qhvll7nx3vgy525lqyziz2k8g7lrx58w5kln85xd3qn") (y #t) (r "1.64.0")))

(define-public crate-webln-js-0.1.1 (c (n "webln-js") (v "0.1.1") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "webln") (r "^0.1") (k 0)))) (h "1n3j2nb6ij7pz3w1pl1h74bqqncmb9s37dgslnprw5ziincj3ak9") (y #t) (r "1.64.0")))

(define-public crate-webln-js-0.1.2 (c (n "webln-js") (v "0.1.2") (d (list (d (n "console_error_panic_hook") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "webln") (r "^0.1") (k 0)))) (h "05sj1i7r61vp7ms6cz626l1389507xm1cmbavllhwip6kb4if06d") (f (quote (("default")))) (r "1.64.0")))

