(define-module (crates-io we bl weblib) #:use-module (crates-io))

(define-public crate-weblib-0.1.0 (c (n "weblib") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1z54a4d6gcpd10va6gi49ch9xpls02aqd2zzfamaqafx309xs5if")))

(define-public crate-weblib-0.1.1 (c (n "weblib") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)))) (h "03896985bcm1vfsf2pdsrr26dmh9a80ivw6b4h2h43lr42nxcaj4")))

(define-public crate-weblib-0.1.2 (c (n "weblib") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)))) (h "12gc3d8lqljsg2rqjslham4cvk6vphcs3l76fr6bv3sj6k1y521m")))

(define-public crate-weblib-0.1.3 (c (n "weblib") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0qw7hmqnqarafqx7vwn45ph96m63hk8qia6hhlcrhhhsr2khqpax")))

(define-public crate-weblib-0.1.4 (c (n "weblib") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1n2j4vicdkbrf5zxyl6ywxqc57r4wziyximp5bdvxs6763ldhnwf")))

