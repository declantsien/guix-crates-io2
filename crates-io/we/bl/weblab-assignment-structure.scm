(define-module (crates-io we bl weblab-assignment-structure) #:use-module (crates-io))

(define-public crate-weblab-assignment-structure-0.1.0 (c (n "weblab-assignment-structure") (v "0.1.0") (h "1hf4k0kmsv7n64k338dvm44hng6jpqsivs6gsf4336xmn5ijjcpy")))

(define-public crate-weblab-assignment-structure-0.2.0 (c (n "weblab-assignment-structure") (v "0.2.0") (h "198s6gjmjh3j4w0q0c56czzbq74q5n07p2girb2vanyxdmrmd1k4")))

(define-public crate-weblab-assignment-structure-0.2.1 (c (n "weblab-assignment-structure") (v "0.2.1") (h "1cc1c5rc8x5jhslmx5x5rz4h9cq0b3s3bykki4kc6ajj0088jh8r")))

(define-public crate-weblab-assignment-structure-0.2.5 (c (n "weblab-assignment-structure") (v "0.2.5") (h "1c8v5qf18p99nqslm3i4ml8kmb642yclmvyjk6xycyg61bq2g793")))

(define-public crate-weblab-assignment-structure-0.2.18 (c (n "weblab-assignment-structure") (v "0.2.18") (h "17s1fj4vnlbchfrk21dhl1cyk3f1npkzjqd3sgj5w7i7y8y8d5q3")))

(define-public crate-weblab-assignment-structure-0.2.22 (c (n "weblab-assignment-structure") (v "0.2.22") (h "1f9kl1nk1ffwh5n18p1apk0l6qmr8s15kkzbx146k6y5h80z6s2y")))

(define-public crate-weblab-assignment-structure-0.2.24 (c (n "weblab-assignment-structure") (v "0.2.24") (h "1x23ajzn287bfmy2mmr500ifdgamhwgpgmm6fg3svn08ng13dhmm")))

(define-public crate-weblab-assignment-structure-0.2.26 (c (n "weblab-assignment-structure") (v "0.2.26") (h "18kyjj55hmmma3bbxxvyvwhgrs6q4jsfj3mgc91klbbwwic1lcmz")))

(define-public crate-weblab-assignment-structure-0.2.31 (c (n "weblab-assignment-structure") (v "0.2.31") (h "16dyg22nxd4gir436wjhwcmg30l57kq0mvmphfgx5vlfbgq0fxc0")))

(define-public crate-weblab-assignment-structure-0.3.4 (c (n "weblab-assignment-structure") (v "0.3.4") (h "1bgvxp0qp0cz462h4rfnk60a1psghwicic7bl2cmjnxyv7dk3px5")))

