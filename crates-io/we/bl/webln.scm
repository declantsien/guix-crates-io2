(define-module (crates-io we bl webln) #:use-module (crates-io))

(define-public crate-webln-0.0.0 (c (n "webln") (v "0.0.0") (h "005kbf0cwf83pllsbs6p20117jmim6lqb061njmk72rnqnjcqgsw")))

(define-public crate-webln-0.1.0 (c (n "webln") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.27") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (k 0)))) (h "16f5z9vk5n68p5l1diripbhh57skah7w0xf8wplxz51y33gpcgx2") (f (quote (("std" "secp256k1/std" "wasm-bindgen/std") ("default" "std")))) (r "1.64.0")))

(define-public crate-webln-0.2.0 (c (n "webln") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3") (k 0)) (d (n "secp256k1") (r "^0.28") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (k 0)))) (h "1km8fhw44l1h3dlmb3ppg5knfx0y9m3g0fvnk4ry2g50fwqdl911") (f (quote (("std" "secp256k1/std" "wasm-bindgen/std") ("default" "std")))) (r "1.64.0")))

