(define-module (crates-io we dk wedkarz_guessing_game) #:use-module (crates-io))

(define-public crate-wedkarz_guessing_game-0.1.0 (c (n "wedkarz_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s5nb70rlsyq47bagx6xs1240kwxn1j1795jdzmrcsdrf0llhzwy") (y #t)))

(define-public crate-wedkarz_guessing_game-0.1.1 (c (n "wedkarz_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0li2w1qp19z5p1hsjxs4ffcnbwwx1jf11q8v9aanvb12j4fk6s3d")))

(define-public crate-wedkarz_guessing_game-0.1.2 (c (n "wedkarz_guessing_game") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v12bp98iffv8xw1g8c6k5pl53qdkqzfdpq51hcihg7ydjx6s738")))

