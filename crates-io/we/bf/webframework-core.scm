(define-module (crates-io we bf webframework-core) #:use-module (crates-io))

(define-public crate-webframework-core-0.0.2 (c (n "webframework-core") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "http") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.12.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.4") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "162qdzywmj13vzgxvl4a1ab38c6www916qirsyhnb8709zgf4lvf")))

