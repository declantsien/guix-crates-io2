(define-module (crates-io we bf webfoolkit) #:use-module (crates-io))

(define-public crate-WebFoolKit-0.1.0 (c (n "WebFoolKit") (v "0.1.0") (d (list (d (n "threadpool") (r "*") (d #t) (k 0)))) (h "0vqk60sq1j5y8pghzn95j8d6w9gkx9mcwdwz1hsqwn578marlsig")))

(define-public crate-WebFoolKit-0.1.1 (c (n "WebFoolKit") (v "0.1.1") (h "0ry2p7malz0g6931f7fzlq1zab7a2pavb32gx2krzl43fg4g1im0")))

