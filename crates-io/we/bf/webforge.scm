(define-module (crates-io we bf webforge) #:use-module (crates-io))

(define-public crate-webforge-0.1.0 (c (n "webforge") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1iiwr2l9rm15637bzpk4w54nvh3a30qzynfsgqvr53j2pzsgrfa0") (y #t)))

(define-public crate-webforge-0.1.1 (c (n "webforge") (v "0.1.1") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ipd0j7ylf1dlzkw35sz7x68k28fw3h1vnc96sni99im5z2n1x53")))

