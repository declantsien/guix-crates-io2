(define-module (crates-io we bf webforms) #:use-module (crates-io))

(define-public crate-webforms-0.2.1 (c (n "webforms") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 2)) (d (n "webforms_derive") (r "^0.2.1") (d #t) (k 0)))) (h "168sf5dndh97nllza7fff6mgab7z2h6nadbd6pgwr4qwy3627nyf") (f (quote (("validate") ("html") ("default" "validate" "html"))))))

(define-public crate-webforms-0.2.2 (c (n "webforms") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 2)) (d (n "webforms_derive") (r "^0.2.1") (d #t) (k 0)))) (h "052g8sd4w4spljbnl285ia02viilaywa62a6lk7y6qzpqfpf2k45") (f (quote (("validate") ("html") ("default" "validate" "html"))))))

