(define-module (crates-io we bf webforms_derive) #:use-module (crates-io))

(define-public crate-webforms_derive-0.2.1 (c (n "webforms_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c1mffj30y1k1b9cqczrhrravgza0gcgbpv3czagmnw1yrrqwb30")))

