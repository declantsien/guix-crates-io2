(define-module (crates-io we bf webframework-derive) #:use-module (crates-io))

(define-public crate-webframework-derive-0.0.1 (c (n "webframework-derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "18h5kr04vm9jbjdhblrpix34d0z2xs691qza2k4prxydl5azcsgj")))

(define-public crate-webframework-derive-0.0.2 (c (n "webframework-derive") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1sxps4fc32xavjlrzk4wqvi4qhdrw039x6vq563yw2i6lfys9z99")))

