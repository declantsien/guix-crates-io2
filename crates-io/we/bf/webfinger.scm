(define-module (crates-io we bf webfinger) #:use-module (crates-io))

(define-public crate-webfinger-0.1.0 (c (n "webfinger") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zx3p840k6dp90ia1m7n78gyqh2kxyy04v02xf5syl3hvv8yd917")))

(define-public crate-webfinger-0.2.0 (c (n "webfinger") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1blf1034dnkykry67fkcr3gynfd9d6zwjzzdpdc8b0czhmrhrqd3")))

(define-public crate-webfinger-0.3.0 (c (n "webfinger") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18mbl8i5nibvb6y43l4s0pl6c2ph7nbjp80q2yvk1pgy01gzlkz3")))

(define-public crate-webfinger-0.3.1 (c (n "webfinger") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0685jvqzpz1f8bna7l07b3abf5jgvr4d9mw5d9dvy14zyacg5j7d")))

(define-public crate-webfinger-0.4.0 (c (n "webfinger") (v "0.4.0") (d (list (d (n "mockito") (r "^0.16") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ig5yy2xxp7hlpg5v97192hcfxc20665i5g4myi7d9289yxf9w3z")))

(define-public crate-webfinger-0.4.1 (c (n "webfinger") (v "0.4.1") (d (list (d (n "mockito") (r "^0.16") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r5nhzbqxp0z0w7q41h2m3mfski7c87ysa0251nlcjqdf2qb297c")))

(define-public crate-webfinger-0.5.0 (c (n "webfinger") (v "0.5.0") (d (list (d (n "mockito") (r "^0.16") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ki06q5w255ga57vmvdqd103dxdn0hh649r9g7jgn6v7vvv22a2b")))

(define-public crate-webfinger-0.5.1 (c (n "webfinger") (v "0.5.1") (d (list (d (n "async-trait") (r "^0.1.56") (o #t) (d #t) (k 0)) (d (n "mockito") (r "^0.23") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1bxkxh498xgq9dbx0v08xxg74dd7wz7hr0wh0d4j5gpi89mk75gk") (f (quote (("default") ("async" "async-trait"))))))

