(define-module (crates-io we bi webio-rs) #:use-module (crates-io))

(define-public crate-webio-rs-0.0.1 (c (n "webio-rs") (v "0.0.1") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)))) (h "1lw1gcgs181clv5nfb1m7vhz1ppigxgd2zv73addxm2668j4jhb7") (y #t) (r "1.64.0")))

(define-public crate-webio-rs-0.0.1-aplha (c (n "webio-rs") (v "0.0.1-aplha") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)))) (h "1bza93w2n8gijkfzdpzjd45k51gggydd16m1r0xsm54hqmnlsfn6") (y #t) (r "1.64.0")))

