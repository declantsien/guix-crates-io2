(define-module (crates-io we bi webidl) #:use-module (crates-io))

(define-public crate-webidl-0.2.0 (c (n "webidl") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)))) (h "18gdjgihggmjj15yhs4622hmak9f8iac7zx74nnxf69a2kyfzl63") (f (quote (("default"))))))

(define-public crate-webidl-0.3.0 (c (n "webidl") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.3") (d #t) (k 2)))) (h "006qbyijw940n9d8cyay0gs13hnj3h229q65rzw3m9whpq4bh1rl") (f (quote (("default"))))))

(define-public crate-webidl-0.3.1 (c (n "webidl") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.3") (d #t) (k 2)))) (h "18zdw88fxl00dn8sqb27986fb2rry3daqrm8rinvbcgbjd2mq93x") (f (quote (("default"))))))

(define-public crate-webidl-0.3.2 (c (n "webidl") (v "0.3.2") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.3") (d #t) (k 2)))) (h "10ivc551g0n03hqyxgq84xj0j8i5bi7fzx6ykhjylbqkhn9nzcqk") (f (quote (("default"))))))

(define-public crate-webidl-0.3.3 (c (n "webidl") (v "0.3.3") (d (list (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 2)))) (h "1cyflk36m69rllm8v9j2m2rk3d3sh3cqsb5pq85lwgb2hx096881") (f (quote (("default"))))))

(define-public crate-webidl-0.4.0 (c (n "webidl") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 2)))) (h "0b0l3s8lwaryvazavin7yqvp0ndlgjiw9bjwkjz92pxwlrzv0nc0") (f (quote (("default"))))))

(define-public crate-webidl-0.4.1 (c (n "webidl") (v "0.4.1") (d (list (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 2)))) (h "0g7wm7np8vbapllkhnkc7s34ar7mwphs57n4ijmpbgs1cf8i3x79") (f (quote (("default"))))))

(define-public crate-webidl-0.5.0 (c (n "webidl") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.14") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.14") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 2)))) (h "1b579z7fb4l3sicdg84niw1yl6xb8mikkc9x5rk8s9j3a397qbwi") (f (quote (("default"))))))

(define-public crate-webidl-0.6.0 (c (n "webidl") (v "0.6.0") (d (list (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.15.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 2)))) (h "19ril4w73zq9mvn70256a1kyxsfjwhxknvk9dm6bpdcl3yvy856w") (f (quote (("default"))))))

(define-public crate-webidl-0.7.0 (c (n "webidl") (v "0.7.0") (d (list (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.15.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 2)))) (h "0hqbxr1iwz1fn9z4p8csh3qcvfz1d5k0qnz186spssdb77j55g6c") (f (quote (("default"))))))

(define-public crate-webidl-0.8.0 (c (n "webidl") (v "0.8.0") (d (list (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.16") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 2)))) (h "0agf7wpabw7dlaca0x8r8sla18vjfsfs12hqyy9hhs4d93vhgy6h") (f (quote (("default"))))))

(define-public crate-webidl-0.9.0 (c (n "webidl") (v "0.9.0") (d (list (d (n "lalrpop") (r "^0.17") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 2)))) (h "0p2pm6nyrv7pkwirhihbbsqwacqilaf8i33yfckadf7a3lhs2icb") (f (quote (("default"))))))

