(define-module (crates-io we bi webidl-parser) #:use-module (crates-io))

(define-public crate-webidl-parser-0.1.0 (c (n "webidl-parser") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)))) (h "1lqw1w6xzx8adp49p71ngp4x4lzx8pc62arj4s4bbs1nj5qhvjai") (f (quote (("default"))))))

(define-public crate-webidl-parser-0.2.0 (c (n "webidl-parser") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "lalrpop") (r "^0.13.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13.1") (d #t) (k 0)))) (h "1vhfziggi5pnry952g5jgavr1fvsxakw8l05ji9d75a9vm25fvvy") (f (quote (("default")))) (y #t)))

