(define-module (crates-io we bw webworker) #:use-module (crates-io))

(define-public crate-webworker-0.1.0 (c (n "webworker") (v "0.1.0") (d (list (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("ExtendableEvent" "Request" "Response" "ResponseInit" "Headers" "Url"))) (d #t) (k 0)))) (h "13inbp17lgv9pr63npf2q341nh8xsm0mfsi5n1ysds6w67j979w3")))

(define-public crate-webworker-0.1.1 (c (n "webworker") (v "0.1.1") (d (list (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("ExtendableEvent" "Request" "RequestInit" "Response" "ResponseInit" "Headers" "Url"))) (d #t) (k 0)))) (h "14xzxcpg5wrsq5bmp5q5rrzh9cvl6y94vsffpxxpj741lf17zdsx")))

(define-public crate-webworker-0.1.2 (c (n "webworker") (v "0.1.2") (d (list (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("ExtendableEvent" "Request" "RequestInit" "Response" "ResponseInit" "Headers" "Url"))) (d #t) (k 0)))) (h "12wxxyg4vzy1w24rrx0jf6jc6l648l8lqsqmiivpmkgq5zsybjhw")))

(define-public crate-webworker-0.1.3 (c (n "webworker") (v "0.1.3") (d (list (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("ExtendableEvent" "Request" "RequestInit" "Response" "ResponseInit" "Headers" "Url"))) (d #t) (k 0)))) (h "0a6m6pkg19c4j542pfdnk927r3y5i075a4g6ifbk571z60wwgwi6")))

(define-public crate-webworker-0.1.4 (c (n "webworker") (v "0.1.4") (d (list (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("ExtendableEvent" "Request" "RequestInit" "Response" "ResponseInit" "Headers" "Url"))) (d #t) (k 0)))) (h "05v91kbvbp5v9v32y6f0dly6rqw571ljxbhh9jxhk0fl7v40kd8c")))

