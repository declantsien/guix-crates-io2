(define-module (crates-io we bw webwire-cli) #:use-module (crates-io))

(define-public crate-webwire-cli-0.1.0 (c (n "webwire-cli") (v "0.1.0") (d (list (d (n "clap") (r ">=2.33.0, <3.0.0") (d #t) (k 0)) (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 0)) (d (n "nom_locate") (r ">=3.0.0, <4.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0yzcsn9ldbg2w18lv52x7hmj00gil5zgm2ipjm8jl7iazz3ymc95")))

(define-public crate-webwire-cli-0.1.1 (c (n "webwire-cli") (v "0.1.1") (d (list (d (n "clap") (r ">=2.33.0, <3.0.0") (d #t) (k 0)) (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 0)) (d (n "nom_locate") (r ">=3.0.0, <4.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1l8qrd63v42c7zh1hvzavjvkp0y03bxwdrmmndm6ihmjdnvvp529")))

(define-public crate-webwire-cli-0.1.2 (c (n "webwire-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0592p6s44sqmxjaj2m8w3c9hirlcd19vdm6637d2nplgricxwzrf")))

(define-public crate-webwire-cli-0.1.3 (c (n "webwire-cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0hwvpib06gv1046fv4h36hdkl9kx8z9xyvv6321zmcw61iwd8jb7")))

(define-public crate-webwire-cli-0.1.4 (c (n "webwire-cli") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1wnf661fpwl1ivxgg62388ca5azviflra8i91jnf4whclbd7qx14")))

(define-public crate-webwire-cli-0.1.5 (c (n "webwire-cli") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "11s2qc8w0kfh0kqxazfvwq8d8gfwf6byw6rz3f3yk8aaqav15h30")))

(define-public crate-webwire-cli-0.1.6 (c (n "webwire-cli") (v "0.1.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "07yymiy7kw3fkcngwwgn4b8n64ljvg80prpw9kcm58nhbkq2plky")))

