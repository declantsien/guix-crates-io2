(define-module (crates-io we ak weak-alloc) #:use-module (crates-io))

(define-public crate-weak-alloc-0.1.0 (c (n "weak-alloc") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "weak-list2") (r "^0.1") (d #t) (k 0)))) (h "0k8n50vdqr3i5dn06bja5z1j99bkhly93lijss84zrk4lqzqkf3v")))

