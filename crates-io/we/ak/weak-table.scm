(define-module (crates-io we ak weak-table) #:use-module (crates-io))

(define-public crate-weak-table-0.1.0 (c (n "weak-table") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0v32xba8982gsdjnjz7f884ksbvza6awqs2vyv7vhfbda9im2spy")))

(define-public crate-weak-table-0.1.1 (c (n "weak-table") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1yzng8vlzjlbnha99ln8c07ikzdvlg5w52y1wbb07cicsgpyq068")))

(define-public crate-weak-table-0.1.2 (c (n "weak-table") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "00312nwcr22g0zavr2z4jinms7nj09b5yk4rgwmzvryrq8hkwspm")))

(define-public crate-weak-table-0.1.3 (c (n "weak-table") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "05hk493yn2g2wv68kahnnvwc47mw5cl9qzmv48q49ln26yn8ai85")))

(define-public crate-weak-table-0.2.0 (c (n "weak-table") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "141s2hbxr68jqmkk9frhgddbcz0cyx9v2j6kb1wzzzk2zkgws2f1")))

(define-public crate-weak-table-0.2.1 (c (n "weak-table") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "01d59yxjxclcybf6kfs36jhhjdkfri2yifvb1l0kcz87zccapp1n")))

(define-public crate-weak-table-0.2.2 (c (n "weak-table") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1vypyrm3adk6q6abr8jg0qhljmqa586xp3c209sfkkg6vsb7asq0")))

(define-public crate-weak-table-0.2.3 (c (n "weak-table") (v "0.2.3") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "13c9v7nkwfh7s8qfy4s5ljyp24hqzxl9dhzkqrb2m1ac4jxn4n3a")))

(define-public crate-weak-table-0.3.0 (c (n "weak-table") (v "0.3.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "00fx45xiprhps15vjdlrx9jk8gllmjv930kax86m0hrd9zvkp3ws")))

(define-public crate-weak-table-0.2.4 (c (n "weak-table") (v "0.2.4") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "04w12y1px90b06lglhkckmvwn6gkpqjhp65nx4m7nm0hx0apgqvq")))

(define-public crate-weak-table-0.3.1 (c (n "weak-table") (v "0.3.1") (d (list (d (n "ahash") (r "^0.7.6") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1s4bgni6fmv5i1gxcw41qx9iblzm6dfyhg1zwygmmgjsn3vlxdx2") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-weak-table-0.3.2 (c (n "weak-table") (v "0.3.2") (d (list (d (n "ahash") (r "^0.7.6") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0ja5zqr1bp5z8wv928y670frnxlj71v6x75g3sg6d6iyaallsgrj") (f (quote (("std") ("default" "std") ("alloc"))))))

