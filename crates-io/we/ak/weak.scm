(define-module (crates-io we ak weak) #:use-module (crates-io))

(define-public crate-weak-0.0.0-0.0.0-0.0.0 (c (n "weak") (v "0.0.0-0.0.0-0.0.0") (h "1cj6fqi49p0iw6axldh30pzj12rdg6nv9v22h2shvx0rgc6d7kar") (y #t)))

(define-public crate-weak-0.0.0-dev.0 (c (n "weak") (v "0.0.0-dev.0") (d (list (d (n "num-derive") (r ">=0") (d #t) (k 0)) (d (n "num-traits") (r ">=0") (d #t) (k 0)) (d (n "weak-build-macros") (r "^0.0.0-dev.0") (d #t) (k 0)))) (h "17a0133m9mdvli5v0x1nfwfyrfn2nc7jipwqgfkhs42vv8qcaybq") (y #t)))

(define-public crate-weak-0.0.0-empty-test (c (n "weak") (v "0.0.0-empty-test") (h "1gyig7gn6a21vy42n3mgdbawzg3ryns0j71crys0r2p6i73c6s63") (y #t)))

(define-public crate-weak-0.0.0-empty-test2 (c (n "weak") (v "0.0.0-empty-test2") (h "1d5lwfv0jyrkhv7hca2dp46dcq1b1zbj0yqbkmiafas2950cnmgw") (y #t)))

(define-public crate-weak-0.0.0-empty-test3 (c (n "weak") (v "0.0.0-empty-test3") (h "0372g2p22z7dpbwvhf1y3bqb3hvy331x7qpj991m0q9n58z3p4bj") (y #t)))

(define-public crate-weak-0.0.0-- (c (n "weak") (v "0.0.0--") (h "1chw0psymb6zfnpipshgljmvmgxh5nwk2pzd7kxa60hjp51xcbz6") (y #t)))

