(define-module (crates-io we ak weakjson) #:use-module (crates-io))

(define-public crate-weakjson-0.0.1 (c (n "weakjson") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "06lf3r1xr4zywgnv3m71y5s71fs8ydsf5kmq27shfmyxl4nvrabc")))

(define-public crate-weakjson-0.0.2 (c (n "weakjson") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "156fvx4047hn13k59k5xbfh9w6qghx38vc7lbcpy6bp4fql7n4fr")))

(define-public crate-weakjson-0.0.3 (c (n "weakjson") (v "0.0.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0qrbimfsvw0kpg994ggsii5syy2bjng185hpqkpvvyx9b77s6k88")))

(define-public crate-weakjson-0.0.4 (c (n "weakjson") (v "0.0.4") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cr2l385qmyvz8s9biwvzzm73rz10x2irxd65aywpxl1z3xg0djk")))

(define-public crate-weakjson-0.0.5 (c (n "weakjson") (v "0.0.5") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0anbl82qd5ry0kg23dlimkmq0rdkdmpr1923aznxcrngqnccnhpy")))

(define-public crate-weakjson-0.0.6 (c (n "weakjson") (v "0.0.6") (d (list (d (n "rustc-serialize") (r "^0.3.7") (d #t) (k 0)))) (h "005cyyx4hz723957ls49vx3mj8cv4ijp4n5q92m8vg96wimzq456")))

(define-public crate-weakjson-0.0.7 (c (n "weakjson") (v "0.0.7") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0xq3sdhx6d0p11bh76q550cvpzz2lhifi1k2f4qhx3miiph2xwlg")))

