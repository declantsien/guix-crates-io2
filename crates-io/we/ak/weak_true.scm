(define-module (crates-io we ak weak_true) #:use-module (crates-io))

(define-public crate-weak_true-0.1.0 (c (n "weak_true") (v "0.1.0") (h "0sqrjaich2hj177cc6gh3li8h3zz2ipfmmdj8yfjihpq232hp60b")))

(define-public crate-weak_true-0.1.1 (c (n "weak_true") (v "0.1.1") (h "1nkqkklklzvdzdqdvkjv0b0dmy9cpzpn3qha9wcyhsrmvgx11fg1")))

(define-public crate-weak_true-0.1.2 (c (n "weak_true") (v "0.1.2") (h "001vfrvvkgfrwwy42lpk0f422c449xr8m4l6kv7xbm9gpprwc550")))

(define-public crate-weak_true-0.1.3 (c (n "weak_true") (v "0.1.3") (h "0pdr6g080nkhj5c3rdq7qkja04sza863ds5hvgzja8s2iag113aw")))

(define-public crate-weak_true-0.1.4 (c (n "weak_true") (v "0.1.4") (h "1kd18sgq2ffzsragzc4yg09v23jr9dp4gqlnv0mfkh2xrczj5bzz")))

