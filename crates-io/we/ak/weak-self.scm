(define-module (crates-io we ak weak-self) #:use-module (crates-io))

(define-public crate-weak-self-1.0.0 (c (n "weak-self") (v "1.0.0") (h "0zjxq3y9m9zgvixrfsx74iyqcc6rfrvjfhmsdjb4v49w1m9b7x3l")))

(define-public crate-weak-self-1.0.1 (c (n "weak-self") (v "1.0.1") (h "1qv3mbhwy3rqdi2y8sk2d8y6nn1i98carzkfy6qhkf57ssyzacfp")))

(define-public crate-weak-self-1.0.2 (c (n "weak-self") (v "1.0.2") (h "1q0dhdxam30l3jvrkk6lgwrrihkg6bh8xhpp19a7vdj172l4zpqs")))

