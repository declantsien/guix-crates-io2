(define-module (crates-io we ak weak-build-macros) #:use-module (crates-io))

(define-public crate-weak-build-macros-0.0.0-dev.0 (c (n "weak-build-macros") (v "0.0.0-dev.0") (h "1fndzdxwfig8za63c6sy9b1mqzq2vwkvjwddyr4kaxb9cqmaj5jl") (y #t)))

(define-public crate-weak-build-macros-0.0.0-- (c (n "weak-build-macros") (v "0.0.0--") (h "1g617z6rki0j7zx4f0ji9mk0q6n12nr49j8qkznblak1zz1lgqga") (y #t)))

