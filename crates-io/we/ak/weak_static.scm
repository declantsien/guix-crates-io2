(define-module (crates-io we ak weak_static) #:use-module (crates-io))

(define-public crate-weak_static-0.1.0 (c (n "weak_static") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "1abnwm1klgba7k924cckhzc9945i452qwiinjabl1h0i0qqwwp1s")))

(define-public crate-weak_static-0.1.1 (c (n "weak_static") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)))) (h "0ngjb58p0nwx9kp4ncf6fslwpz9qs7a2bl8423l78wjj9zdfq689")))

