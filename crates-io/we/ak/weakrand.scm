(define-module (crates-io we ak weakrand) #:use-module (crates-io))

(define-public crate-weakrand-1.0.0 (c (n "weakrand") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "03ppq2ngd21xsvlbwl3nszxdrpa3qhl4gqdrwk12v1kgi6zc04sq") (f (quote (("nightly"))))))

(define-public crate-weakrand-1.0.1 (c (n "weakrand") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1w6rhk7kc4fdx4d51adg79hcws5p923n2dl0n7qswnlsi9zzh9sn") (f (quote (("nightly"))))))

(define-public crate-weakrand-1.1.0 (c (n "weakrand") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0vnj883jqz7069wcjx63c36sax57dfxik63jsqpv28h8ai1dm67m") (f (quote (("nightly"))))))

(define-public crate-weakrand-1.2.0 (c (n "weakrand") (v "1.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0c7z0yxindjnz5jrs9lcwv08a2gw5zr95hz79s66m2bykx52ns0m") (f (quote (("nightly"))))))

