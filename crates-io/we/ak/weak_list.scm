(define-module (crates-io we ak weak_list) #:use-module (crates-io))

(define-public crate-weak_list-0.1.0 (c (n "weak_list") (v "0.1.0") (h "1rvi6s3wxzjcyksb663zfp6rqk4191ar3l9lm05mkpqms4fdn95v") (y #t)))

(define-public crate-weak_list-0.2.0 (c (n "weak_list") (v "0.2.0") (h "0m1i6f45hjvx4afd4km4h3sd6yjnvzxx1q1q7xy987c7s7j2sgdr") (y #t)))

