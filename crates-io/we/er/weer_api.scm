(define-module (crates-io we er weer_api) #:use-module (crates-io))

(define-public crate-weer_api-0.1.0 (c (n "weer_api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json" "charset"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1ih57x61sjcr75r6bjlc6diz0dva3l9qlcbff94j7aqphjwkr8n8")))

(define-public crate-weer_api-0.1.1 (c (n "weer_api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json" "charset"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "02271byz4s91hq6wznvmr53bkj84k4klj8k3vfkn2gmvxm7g5ncx")))

