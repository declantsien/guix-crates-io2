(define-module (crates-io we br webrtc-constraints) #:use-module (crates-io))

(define-public crate-webrtc-constraints-0.0.0 (c (n "webrtc-constraints") (v "0.0.0") (h "1yq3fmwmyqgl10pwzqsfrdrkvrfd8gxzl4wvgfjbhzh7a512jgci") (r "1.60.0")))

(define-public crate-webrtc-constraints-0.1.0 (c (n "webrtc-constraints") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (f (quote ("preserve_order"))) (d #t) (k 2)))) (h "025dhvy4851lmb4yw4ygzkqqrhdyy8wk9xwd2sdl836ijlgbgpxw") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.60.0")))

