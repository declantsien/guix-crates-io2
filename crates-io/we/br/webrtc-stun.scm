(define-module (crates-io we br webrtc-stun) #:use-module (crates-io))

(define-public crate-webrtc-stun-0.0.0 (c (n "webrtc-stun") (v "0.0.0") (h "0vsjrq8pvxdjl793k0ia9jk1zlbqsnjpp04llilbcjpbym0v2xaf")))

(define-public crate-webrtc-stun-0.1.13 (c (n "webrtc-stun") (v "0.1.13") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.19") (d #t) (k 0)) (d (n "subtle") (r "^2.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^2.2.0") (d #t) (k 0)) (d (n "util") (r "^0.1.6") (d #t) (k 0) (p "webrtc-util")))) (h "0fqvv0lgp91mvvfgj32ww6g7jywhdqzg8w5qvlx2yhksqd0a0x56")))

