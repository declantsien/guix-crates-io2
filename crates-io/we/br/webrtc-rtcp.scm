(define-module (crates-io we br webrtc-rtcp) #:use-module (crates-io))

(define-public crate-webrtc-rtcp-0.0.0 (c (n "webrtc-rtcp") (v "0.0.0") (h "0p85033qzf0hnirvy63fpcc45s4v8p4j9fvn8cgn8233469x4pjc") (y #t)))

(define-public crate-webrtc-rtcp-0.2.0 (c (n "webrtc-rtcp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1v5vyk5wki6zv3lg0gaz59y5pm5zhxg9dyg22mwd71ghjsi9806q") (y #t)))

