(define-module (crates-io we br webrender_build) #:use-module (crates-io))

(define-public crate-webrender_build-0.0.1 (c (n "webrender_build") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0fccv8nynmn3fik10azhpmgk9ffwk77m0lmrby8sfn3qlfjg67c9") (f (quote (("serialize_program" "serde"))))))

(define-public crate-webrender_build-0.1.0 (c (n "webrender_build") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0jz7fsj5njd1jm5im9yc1l3h0gnzdvzn1wbhgkbkk8jrwz2bnpl9") (f (quote (("serialize_program" "serde"))))))

