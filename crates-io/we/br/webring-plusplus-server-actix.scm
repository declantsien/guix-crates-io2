(define-module (crates-io we br webring-plusplus-server-actix) #:use-module (crates-io))

(define-public crate-webring-plusplus-server-actix-0.1.0 (c (n "webring-plusplus-server-actix") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0k9gnl2w16z2lfj5qydhszc8xlrh7m4i9nn0bd55qs0h1yi3v7wr")))

(define-public crate-webring-plusplus-server-actix-0.1.1 (c (n "webring-plusplus-server-actix") (v "0.1.1") (d (list (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0wgs09p20r4j8qx6w2kr170nprpqw8fwlpli5wpcw2mxx7x8qdy3")))

(define-public crate-webring-plusplus-server-actix-0.1.2 (c (n "webring-plusplus-server-actix") (v "0.1.2") (d (list (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1dsq0i9csvfay8lzvd7485zha0y68ak4f59gckz0bi9fndvw51r3")))

