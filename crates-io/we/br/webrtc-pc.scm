(define-module (crates-io we br webrtc-pc) #:use-module (crates-io))

(define-public crate-webrtc-pc-0.0.0 (c (n "webrtc-pc") (v "0.0.0") (h "0akhdq6cxy32h9sckydxq0lar510y6jsfgyvkasinrr172v0r2mj") (y #t)))

(define-public crate-webrtc-pc-0.0.1 (c (n "webrtc-pc") (v "0.0.1") (h "0wnmvplczxcfxlj6v4k8cg0cg91hn9l1airs3bvf692zs3b756j5") (y #t)))

(define-public crate-webrtc-pc-0.0.2 (c (n "webrtc-pc") (v "0.0.2") (h "0xc7p36kcygm1ij3c2hhhw2gd527x35d0sclbkn3g2wipwidv83g") (y #t)))

