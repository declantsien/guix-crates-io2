(define-module (crates-io we br webrtc-sys-build) #:use-module (crates-io))

(define-public crate-webrtc-sys-build-0.2.0 (c (n "webrtc-sys-build") (v "0.2.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots" "blocking"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1sm9fab5jf5ws2nhqprfvbjr30jwgs94rbmzqzzpfdvl6nj3yp1b")))

(define-public crate-webrtc-sys-build-0.3.0 (c (n "webrtc-sys-build") (v "0.3.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots" "blocking"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0f1hrwq7ibs9mprphrnv4b645h0ykbh71d8gg1zc8q5y7rw2wngw")))

(define-public crate-webrtc-sys-build-0.3.1 (c (n "webrtc-sys-build") (v "0.3.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots" "blocking"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1yf3knasvx38py53p6cd9k1s91wg40vivhvnhjfm0sa8h45zspf9")))

(define-public crate-webrtc-sys-build-0.3.2 (c (n "webrtc-sys-build") (v "0.3.2") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots" "blocking"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0qjvsj98pa0zfyb2zzlja1vhp3iiwzmqw7b6p4dn8w2nj2vgabwg")))

