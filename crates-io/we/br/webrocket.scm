(define-module (crates-io we br webrocket) #:use-module (crates-io))

(define-public crate-webrocket-0.1.0 (c (n "webrocket") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "sync" "time" "io-util" "rt-multi-thread"))) (d #t) (k 0)))) (h "0pf3r7f8c8vg7z1p3njv9b47znd5iqk8li6l9ydcjm19xby9bd8j")))

