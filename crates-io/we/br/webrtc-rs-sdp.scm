(define-module (crates-io we br webrtc-rs-sdp) #:use-module (crates-io))

(define-public crate-webrtc-rs-sdp-0.1.0 (c (n "webrtc-rs-sdp") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "util") (r "^0.1.0") (d #t) (k 0) (p "webrtc-rs-util")))) (h "1x4vnzg7j24hma3dk4fdfw17iv4v1wg4c5rnil7gn6471260x2ax") (y #t)))

