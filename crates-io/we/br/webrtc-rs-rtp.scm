(define-module (crates-io we br webrtc-rs-rtp) #:use-module (crates-io))

(define-public crate-webrtc-rs-rtp-0.1.0 (c (n "webrtc-rs-rtp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "util") (r "^0.1.0") (d #t) (k 0) (p "webrtc-rs-util")))) (h "0kf5rca975vlsnylxpyzmfpbi5qf48vr7008xs0g3465m2sc82h3") (y #t)))

