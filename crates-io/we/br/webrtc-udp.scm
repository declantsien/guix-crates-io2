(define-module (crates-io we br webrtc-udp) #:use-module (crates-io))

(define-public crate-webrtc-udp-0.0.0 (c (n "webrtc-udp") (v "0.0.0") (d (list (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "02fd65rqqnzdk1pxvwyrpjn755msccdmcg8bkxd8hm1mmnbg90id")))

