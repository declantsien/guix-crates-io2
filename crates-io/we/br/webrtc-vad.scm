(define-module (crates-io we br webrtc-vad) #:use-module (crates-io))

(define-public crate-webrtc-vad-0.1.0 (c (n "webrtc-vad") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "1fjdbn8isrns0nb2cx6swjcv8x21sjwmxs66i4zvz1jh7randshp")))

(define-public crate-webrtc-vad-0.2.0 (c (n "webrtc-vad") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "1yxdgcbf1bg5xmgqbmlrh45h4z1xhzz645p2mfp654x7pbdayxsp")))

(define-public crate-webrtc-vad-0.3.0 (c (n "webrtc-vad") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "1vfmhyvcqaky5sr6hv5qg1zgcyx7cqfbxqlwhwzr1cj1hg0fvn2b")))

(define-public crate-webrtc-vad-0.4.0 (c (n "webrtc-vad") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "021h9v4lz9yaxgvp6c5iw5p2i91bgx9s4lli8navx46asq7y989r")))

