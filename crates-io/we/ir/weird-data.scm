(define-module (crates-io we ir weird-data) #:use-module (crates-io))

(define-public crate-weird-data-0.0.0 (c (n "weird-data") (v "0.0.0") (h "15w12vkkjfg94chc3c6c7xgnw8imyfm6pw4kq7mn0n1ma0j6k54r")))

(define-public crate-weird-data-0.0.1 (c (n "weird-data") (v "0.0.1") (h "0ylww1n8h1s85zvjddgrmzvkjmcnrmr2ls0q03mrlll1jvgfd0as")))

(define-public crate-weird-data-0.1.0 (c (n "weird-data") (v "0.1.0") (d (list (d (n "fastrand") (r "^2.0.1") (k 0)))) (h "015cpvy4i0k2ck080d79w9phnnzr20w4npmn9l7g942x01nj3zlf") (f (quote (("std" "fastrand/std") ("default" "std")))) (r "1.75")))

(define-public crate-weird-data-0.2.0 (c (n "weird-data") (v "0.2.0") (d (list (d (n "fastrand") (r "^2.0.1") (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1wg59lfsq1016g9icnwdcqbr4q59wrjzsixnd7v2a8v56f51j9qj") (f (quote (("std" "fastrand/std") ("default" "std")))) (r "1.75")))

