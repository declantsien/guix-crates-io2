(define-module (crates-io we ir weirdgrep) #:use-module (crates-io))

(define-public crate-weirdgrep-1.0.0 (c (n "weirdgrep") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1q85x1j3wnk183qgsw5znjzg8j82x2acfgp8aqwzksg3nqg8ar28") (y #t)))

(define-public crate-weirdgrep-1.0.1 (c (n "weirdgrep") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nrbq5jnxxpbj04r8nd2lalyzfcjmpq598hybhr16sn844p84d2m")))

(define-public crate-weirdgrep-1.0.2 (c (n "weirdgrep") (v "1.0.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16nr2vs98j429wyrnw3x6mppgphw8gnxfqa0aa6d97xv8c7fnczd")))

(define-public crate-weirdgrep-1.0.3 (c (n "weirdgrep") (v "1.0.3") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "182rz8wc515lcjw2dw5l2xz6lpnc76nh03z3s6fic2pc6zrwjyj5") (y #t)))

(define-public crate-weirdgrep-1.0.4 (c (n "weirdgrep") (v "1.0.4") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yvn9j99c1vgffh9d44jwrb2svs8nliyg7g0zffzynz8gxmnd73h")))

(define-public crate-weirdgrep-1.0.5 (c (n "weirdgrep") (v "1.0.5") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1dbawsh9k5a6csbrmhzj2adir56rmgd2fws31f2cq340q8m74pzq")))

