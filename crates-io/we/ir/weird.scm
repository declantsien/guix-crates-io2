(define-module (crates-io we ir weird) #:use-module (crates-io))

(define-public crate-weird-0.1.0 (c (n "weird") (v "0.1.0") (h "0wfvm2mp5mmiv3qqa1zn5mzd29gmh9da4l7pyczlaqpqdyai69qk")))

(define-public crate-weird-0.1.1 (c (n "weird") (v "0.1.1") (h "0fg60mg8kx3f4ksq4anq1c64h4brwq2z269l2f5qr0d3rhr14m7i")))

(define-public crate-weird-0.1.2 (c (n "weird") (v "0.1.2") (h "13aj2spr7ksqmw7xm3yw0vdskc3c5kgkdqbfv8v1iw6xyhkqyvp2")))

(define-public crate-weird-0.2.0 (c (n "weird") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crockford") (r "^1.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "squirrel-rng") (r "^0.1.1") (d #t) (k 0)))) (h "0sr3wbcj6fjij07xffsd3x4vx37qvhs903zg06i3djxbj7l8ck0k")))

