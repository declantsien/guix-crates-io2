(define-module (crates-io we ir weiroll) #:use-module (crates-io))

(define-public crate-weiroll-0.1.0 (c (n "weiroll") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0.3") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0k2c9wbll8dm6igywn765j5vmdcgvly89mrmfjwx667624mwklq9")))

(define-public crate-weiroll-0.1.1 (c (n "weiroll") (v "0.1.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0.3") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "16gyf84w5vpcnyzbbq5gwpmbjj70qjnxsv4ygbwygb40q2v6h7rb")))

