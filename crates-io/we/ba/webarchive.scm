(define-module (crates-io we ba webarchive) #:use-module (crates-io))

(define-public crate-webarchive-0.1.0 (c (n "webarchive") (v "0.1.0") (d (list (d (n "plist") (r "^1.0") (d #t) (k 0)) (d (n "ruma-serde") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "16afya97p2405fkcg3n4f07jp1yjkapyaxg7a2a95m7jaa3bk2qg")))

(define-public crate-webarchive-0.2.0 (c (n "webarchive") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "plist") (r "^1.0") (d #t) (k 0)) (d (n "ruma-serde") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "00ib4zn85hbjp54q6qlwysa5ic2rrb3j2131mx1672jklr2qs2b8")))

(define-public crate-webarchive-0.2.1 (c (n "webarchive") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "plist") (r "^1.0") (d #t) (k 0)) (d (n "ruma-serde") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0q7c1lmjzwnn2clcnai29f55b56yfp3qqn6709plzljb2sb98f4f")))

