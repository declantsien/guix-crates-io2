(define-module (crates-io we ba webatui) #:use-module (crates-io))

(define-public crate-webatui-0.1.0 (c (n "webatui") (v "0.1.0") (d (list (d (n "base16-palettes") (r "^0.1.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Screen" "TouchEvent" "TouchList" "Touch" "CssStyleSheet" "StyleSheetList" "CssRuleList" "CssRule"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "0lipdnj35gln8rigmcqfj0i4gz4ns8cxanxvhz1c5qr4ybyqkwyn")))

(define-public crate-webatui-0.1.1 (c (n "webatui") (v "0.1.1") (d (list (d (n "base16-palettes") (r "^0.1.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Screen" "TouchEvent" "TouchList" "Touch" "CssStyleSheet" "StyleSheetList" "CssRuleList" "CssRule"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (d #t) (k 0)))) (h "1n6rmxax6vmfk7667v1z6kracqzhxaixljrz9wdvb710yvfk071r")))

