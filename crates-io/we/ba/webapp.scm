(define-module (crates-io we ba webapp) #:use-module (crates-io))

(define-public crate-webapp-0.1.0 (c (n "webapp") (v "0.1.0") (d (list (d (n "yew") (r "^0.4.0") (d #t) (k 0)))) (h "15n65b84vlphdjx2vfdsf3gkpyd1wz00v9srh5y7a7983d4c49v0")))

(define-public crate-webapp-0.2.0 (c (n "webapp") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5.10") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "mowl") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.20") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "tungstenite") (r "^0.5.3") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "191b9cx245s148f7lbdmqpnrwz3bxnwkhvqmvk5d8jqkd1kbbpwj") (f (quote (("frontend" "stdweb" "yew") ("backend" "env_logger" "log" "mowl" "tungstenite"))))))

(define-public crate-webapp-1.0.0 (c (n "webapp") (v "1.0.0") (d (list (d (n "diesel") (r "^1.4.2") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1mz6ysgghlsg6kmhc83r8592pcw35h0afm8rr4m93d43m8y7x8ik") (f (quote (("backend" "diesel"))))))

