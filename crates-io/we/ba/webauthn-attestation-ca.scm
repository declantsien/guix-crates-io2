(define-module (crates-io we ba webauthn-attestation-ca) #:use-module (crates-io))

(define-public crate-webauthn-attestation-ca-0.1.0 (c (n "webauthn-attestation-ca") (v "0.1.0") (d (list (d (n "base64urlsafedata") (r "^0.1.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "192pqpnl7yrjsfq1acgbxcpq3zl2rgdy3nna8rw6ci8w3gr2rgam") (r "1.70.0")))

(define-public crate-webauthn-attestation-ca-0.5.0 (c (n "webauthn-attestation-ca") (v "0.5.0") (d (list (d (n "base64urlsafedata") (r "^0.5.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.56") (d #t) (k 0)) (d (n "openssl") (r "^0.10.56") (d #t) (k 1)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1117p0iaq0hilwrgc72928izlxv4xlqiyxjsa5ds2335ynx2w3wv") (r "1.70.0")))

