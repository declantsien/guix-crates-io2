(define-module (crates-io we ba webauthn-rp-proxy) #:use-module (crates-io))

(define-public crate-webauthn-rp-proxy-0.5.0 (c (n "webauthn-rp-proxy") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webauthn-rs") (r "^0.5.0") (f (quote ("danger-allow-state-serialisation"))) (d #t) (k 0)))) (h "02y4b22db2044nql3lhwwwgxlq65nd4qiqpry37ha7m45rb15ldg") (r "1.70.0")))

