(define-module (crates-io we ba webapp-frontend) #:use-module (crates-io))

(define-public crate-webapp-frontend-1.0.0 (c (n "webapp-frontend") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "sass-rs") (r "^0.2.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stdweb") (r "^0.4.17") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 1)) (d (n "webapp") (r "^1.0.0") (d #t) (k 0)) (d (n "webapp") (r "^1.0.0") (d #t) (k 1)) (d (n "yew") (r "^0.6.0") (f (quote ("cbor"))) (d #t) (k 0)) (d (n "yew-router") (r "^0.2.0") (d #t) (k 0)))) (h "1mfa86mkj2kawb2i5pg9qv2rv06yly6ba96qjlja2jcw0anxqmvz")))

