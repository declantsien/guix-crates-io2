(define-module (crates-io we ba webauthn-rs-device-catalog) #:use-module (crates-io))

(define-public crate-webauthn-rs-device-catalog-0.5.0-20230418 (c (n "webauthn-rs-device-catalog") (v "0.5.0-20230418") (d (list (d (n "base64urlsafedata") (r "^0.1.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.56") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "webauthn-attestation-ca") (r "^0.1.0") (d #t) (k 0)))) (h "1s5as0dy88q3ngvs6b74m0cq7f7p08ms5cy4iw5757hxhxffc394")))

