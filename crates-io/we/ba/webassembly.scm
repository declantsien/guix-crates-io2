(define-module (crates-io we ba webassembly) #:use-module (crates-io))

(define-public crate-webassembly-0.0.0 (c (n "webassembly") (v "0.0.0") (h "0s2nm2bgm8bv276x07bvz6b2vrih9cz6a808rzavc82aa5ba0dfn")))

(define-public crate-webassembly-0.1.0 (c (n "webassembly") (v "0.1.0") (d (list (d (n "micromath") (r "^1.0.0") (d #t) (k 0)))) (h "0d2fk5wa5dk4ijinjsaa7ylkrwv889ancmxl3sc33xklrqy9hi8j")))

(define-public crate-webassembly-0.1.1 (c (n "webassembly") (v "0.1.1") (h "06f3m1yq6qdzgspl2zzb8psdr00abfjb1scni234g4vqmhhsdg9d")))

(define-public crate-webassembly-0.2.0 (c (n "webassembly") (v "0.2.0") (h "0si8632y59prxz236fw87x8la9mb0drvwyz8gkrz9xkb4hqhw13j")))

(define-public crate-webassembly-0.3.0 (c (n "webassembly") (v "0.3.0") (h "06jkp51dci6rnflkxacz5kjkh5rrc9pj331lwis9ls9m6hpn62jb")))

(define-public crate-webassembly-0.3.1 (c (n "webassembly") (v "0.3.1") (h "1276gri9qy8cr0nwahjgpfrd8cm2hiskbv3w6yinbfdnzks612q0")))

(define-public crate-webassembly-0.5.0 (c (n "webassembly") (v "0.5.0") (h "133sz94kcl9xyain192r89ixm1svqs4w4cyxrg7zkzyzvvhi06hx")))

(define-public crate-webassembly-0.6.0 (c (n "webassembly") (v "0.6.0") (h "1q5va9kbwh7qnhbs12zv3apyk9ldv8a6w8vrhnqj1nad7md50s90")))

(define-public crate-webassembly-0.7.0 (c (n "webassembly") (v "0.7.0") (h "0ljqvsspm2201xc13vhn61ls4ww4i1aj62ciaq2n0w8zyd09vhvd")))

(define-public crate-webassembly-0.8.0 (c (n "webassembly") (v "0.8.0") (h "1s0hh70dzvvsxcsg8l5b0v9kq13gb9d2r3wa41p1rq6as327yqn5")))

(define-public crate-webassembly-0.8.1 (c (n "webassembly") (v "0.8.1") (h "1jis5l6v5f3wvba9sbdyy36hr63gi5mk9cgw4c1yg5qr11b7zr6f")))

(define-public crate-webassembly-0.8.2 (c (n "webassembly") (v "0.8.2") (h "0pf4v8pbx3iq3j7iwxmvqrdy9qppvgs685vvrc9yrmz27610qx1x")))

