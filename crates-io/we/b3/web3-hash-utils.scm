(define-module (crates-io we b3 web3-hash-utils) #:use-module (crates-io))

(define-public crate-web3-hash-utils-1.0.0 (c (n "web3-hash-utils") (v "1.0.0") (d (list (d (n "ethereum-types") (r "^0.13") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1ris8a8pr7rx3cpwgvkq3lq43hnhiyzsh1nrfnbbvsigbyacsd6a")))

