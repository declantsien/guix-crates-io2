(define-module (crates-io we b3 web3-rpc) #:use-module (crates-io))

(define-public crate-web3-rpc-0.1.0 (c (n "web3-rpc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rcg64kwvqkcpjvlz60a685f6j3sgmg7hrdkr7gvc3bj3vsfgb4h")))

(define-public crate-web3-rpc-0.1.1 (c (n "web3-rpc") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mrhgy8qp41vm8hjd7pmcd3h21fcqyxb1y9f2ldh3qns9l4dwryr")))

(define-public crate-web3-rpc-0.1.2 (c (n "web3-rpc") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fmylx65aw2jy13zz3jcszw7cnshwhlddmphr5ymj1ac3ijg4gii")))

(define-public crate-web3-rpc-0.1.3 (c (n "web3-rpc") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qg3hbmw8jp94fj6cxafl0v2jjhnm6w71gybg5ymqkb6igxg21hj")))

(define-public crate-web3-rpc-0.1.4 (c (n "web3-rpc") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13hxkz971g25sw3f3z5xjvldxxni8zidszmlk9x3sc0n62wqhs7z")))

(define-public crate-web3-rpc-0.1.5 (c (n "web3-rpc") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12g4yk57i91fv3wif2d4a4qpzjjgl5nds46f2dnrjb0gvd7dfkxr")))

(define-public crate-web3-rpc-0.1.6 (c (n "web3-rpc") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03qk6j7bx77zns5awapankln77zybj47krh37v4b62yis3jz408g")))

(define-public crate-web3-rpc-0.1.7 (c (n "web3-rpc") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13knr6p5gmzrwrmky4ml0xmijwbmmn2mg6kb43xfndkdbydy5mjy")))

(define-public crate-web3-rpc-0.1.8 (c (n "web3-rpc") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1khlgpp7ryizdflisy4v40j88sfc4jbx95mfn4jmipcdviyg8643")))

(define-public crate-web3-rpc-0.1.9 (c (n "web3-rpc") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g36glif9cr657bds3nmms27i5b8rbig3ckzilkc5hzs25wg3lg6")))

(define-public crate-web3-rpc-0.1.10 (c (n "web3-rpc") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c81dlr6b4ziaj859lh358b48bmvv88sk41yncs5diwkb4a8k202")))

