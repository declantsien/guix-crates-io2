(define-module (crates-io we b3 web3scan) #:use-module (crates-io))

(define-public crate-web3scan-0.1.100 (c (n "web3scan") (v "0.1.100") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fxfiwv484alccfxbcf0fqr53fjz8wjbwc33s26h09phlbfs8kga")))

