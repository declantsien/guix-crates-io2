(define-module (crates-io we b3 web3-unit-converter) #:use-module (crates-io))

(define-public crate-web3-unit-converter-0.1.0 (c (n "web3-unit-converter") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.13.1") (f (quote ("std"))) (k 0)))) (h "0fcmycsfxv2dhwbmx5wq7zv2czm8mdijx0kid395gm1y3scxb4ff")))

(define-public crate-web3-unit-converter-0.1.1 (c (n "web3-unit-converter") (v "0.1.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.13.1") (f (quote ("std"))) (k 0)))) (h "1bg966cnqb5c6n41rfw6rmwjm9xi5a7mfj00z1wnch21a8f8kqpz")))

