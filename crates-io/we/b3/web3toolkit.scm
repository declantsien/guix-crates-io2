(define-module (crates-io we b3 web3toolkit) #:use-module (crates-io))

(define-public crate-web3toolkit-0.1.0 (c (n "web3toolkit") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ethers") (r "^1.0.2") (f (quote ("ws" "rustls" "abigen"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (d #t) (k 0)))) (h "0dkldlw4993xn1hmglycv95r4wglac12igvn6rmp2hamxd5d9imv") (y #t)))

