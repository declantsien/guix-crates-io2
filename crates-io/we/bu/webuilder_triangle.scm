(define-module (crates-io we bu webuilder_triangle) #:use-module (crates-io))

(define-public crate-webuilder_triangle-0.1.0 (c (n "webuilder_triangle") (v "0.1.0") (h "1jw93ckxs8ccwczs1gvb3svjj9snabibgyqiylmz4mnzk0514q0z") (y #t)))

(define-public crate-webuilder_triangle-0.1.1 (c (n "webuilder_triangle") (v "0.1.1") (h "0ln0scx1b7rjv174wck7cns5zpg7iq48y1dvq9insbf7mdq18clx")))

