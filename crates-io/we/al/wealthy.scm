(define-module (crates-io we al wealthy) #:use-module (crates-io))

(define-public crate-wealthy-0.1.0 (c (n "wealthy") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "separator") (r "^0.4.1") (d #t) (k 0)))) (h "0q8ki2v3cl5485lbhb9hivqm23l4h3msxh84gllrhzwih0isrg4q")))

(define-public crate-wealthy-0.2.0 (c (n "wealthy") (v "0.2.0") (d (list (d (n "cached") (r "^0.39.0") (d #t) (k 0)) (d (n "fraction") (r "^0.11.2") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lru") (r "^0.8.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "separator") (r "^0.4.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "0kb6cjrwd443ixvlqx2x1glfjrkxsyrvwps6ih7h7lcc3mqgb1jc")))

