(define-module (crates-io we b1 web1337) #:use-module (crates-io))

(define-public crate-web1337-0.1.0 (c (n "web1337") (v "0.1.0") (h "1gx9g6lxkddkc4xn272w7gpcikr0vn2p50h2s9ny5c0kdmccry8f")))

(define-public crate-web1337-0.1.1 (c (n "web1337") (v "0.1.1") (h "09s2v1m4379kzmvqaypsgdiy59r4m2hk16rxvhsy1l21rf5krlxj")))

