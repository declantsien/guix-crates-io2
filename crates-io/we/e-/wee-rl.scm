(define-module (crates-io we e- wee-rl) #:use-module (crates-io))

(define-public crate-wee-rl-1.0.0 (c (n "wee-rl") (v "1.0.0") (d (list (d (n "encode_unicode") (r "^0.1.3") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nix") (r "^0.8") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1679dgrw01f51vcf01c2cf7vbzdjsqwjvdi8dbi13ck4c988caqf") (y #t)))

