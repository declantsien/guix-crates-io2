(define-module (crates-io we e- wee-matrix) #:use-module (crates-io))

(define-public crate-wee-matrix-0.1.0 (c (n "wee-matrix") (v "0.1.0") (d (list (d (n "blas") (r "^0.15") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lapack") (r "^0.11") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "unittest") (r "^0.1.0") (d #t) (k 0)))) (h "0n8nsw1h9l8zcs2vis93fysgbwl7i85slqhmlgxxblq9h92mdhq4")))

