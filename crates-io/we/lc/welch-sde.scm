(define-module (crates-io we lc welch-sde) #:use-module (crates-io))

(define-public crate-welch-sde-0.1.0 (c (n "welch-sde") (v "0.1.0") (d (list (d (n "complot") (r "^0.3.2") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 2)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "0rv034ygy7jg2hsnwhd5bhcvrzwkxfd341gxzv2iy0sqvq7f1ycw")))

