(define-module (crates-io we lc welcomeback) #:use-module (crates-io))

(define-public crate-WelcomeBack-0.1.0 (c (n "WelcomeBack") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1ch3byhws7svr4g9hpcfbskj1gn5mixbpmd8mjn5q2xkck1yadqm") (y #t)))

(define-public crate-WelcomeBack-0.1.1 (c (n "WelcomeBack") (v "0.1.1") (h "19lisdil75805ldggbf49w26a4bwmswb02zddfkzvjj5mhggkn5f")))

