(define-module (crates-io we ih weihanglo-publish-test) #:use-module (crates-io))

(define-public crate-weihanglo-publish-test-0.0.1 (c (n "weihanglo-publish-test") (v "0.0.1") (h "1iam354azmf1vbacdnhlv0kl2yanc5rp7m0lrkv8x3slii4a7cpk")))

(define-public crate-weihanglo-publish-test-0.0.2 (c (n "weihanglo-publish-test") (v "0.0.2") (h "0w9hs3skv7yb4mv4n79kd38a01xdhm7cgbipvc53wy7fga6slj2k")))

(define-public crate-weihanglo-publish-test-0.0.3 (c (n "weihanglo-publish-test") (v "0.0.3") (h "19aphg2yikhiyiifin1h3ppvhqxg2y3r5v9vxmn43q2qaz2zfvmd")))

