(define-module (crates-io we re weresocool_ring_buffer) #:use-module (crates-io))

(define-public crate-weresocool_ring_buffer-1.0.0 (c (n "weresocool_ring_buffer") (v "1.0.0") (d (list (d (n "weresocool_shared") (r "^1.0.0") (d #t) (k 0)))) (h "10gm09f9q00hz1v0s30fcacrb4i9vpv92mm92b4v11gs148m8zfi")))

(define-public crate-weresocool_ring_buffer-1.0.1 (c (n "weresocool_ring_buffer") (v "1.0.1") (d (list (d (n "weresocool_shared") (r "^1.0.1") (d #t) (k 0)))) (h "1pkmmjwj6gdnl85pnbhi8fw3m4z5rxjnq4hjyhrh7z44dvk48kmn")))

(define-public crate-weresocool_ring_buffer-1.0.2 (c (n "weresocool_ring_buffer") (v "1.0.2") (d (list (d (n "weresocool_shared") (r "^1.0.1") (d #t) (k 0)))) (h "12i4yx5hx7dvbj779aczfwvcvci4s28y6yjckxb1c98r4142s84v")))

(define-public crate-weresocool_ring_buffer-1.0.3 (c (n "weresocool_ring_buffer") (v "1.0.3") (d (list (d (n "weresocool_shared") (r "^1.0.3") (d #t) (k 0)))) (h "0phg7507vwxcsjpl2q678v4r9a7g4xvhc2qbl5x49nvpfn7gm9d4")))

(define-public crate-weresocool_ring_buffer-1.0.31 (c (n "weresocool_ring_buffer") (v "1.0.31") (d (list (d (n "weresocool_shared") (r "^1.0.31") (d #t) (k 0)))) (h "0s2x88cybxmd2237vch8j3p4fdxgksglfv26d16yayhlgabkchdj")))

(define-public crate-weresocool_ring_buffer-1.0.32 (c (n "weresocool_ring_buffer") (v "1.0.32") (d (list (d (n "weresocool_shared") (r "^1.0.32") (d #t) (k 0)))) (h "19vj5s83ndbxiaq4k408dypvm8h57pim8nrpp798vl9bcr3zw6nw")))

(define-public crate-weresocool_ring_buffer-1.0.33 (c (n "weresocool_ring_buffer") (v "1.0.33") (d (list (d (n "weresocool_shared") (r "^1.0.33") (d #t) (k 0)))) (h "0x288xb8ryzmchzavxmnnw2gbgqdm46nxbpay6wi7dz25hzbz3b1")))

(define-public crate-weresocool_ring_buffer-1.0.34 (c (n "weresocool_ring_buffer") (v "1.0.34") (d (list (d (n "weresocool_shared") (r "^1.0.34") (d #t) (k 0)))) (h "1yg6k3w0kkvmkg1wrrb6v4lm4dyx19adfim9ny2ijnafpn11yfwm")))

(define-public crate-weresocool_ring_buffer-1.0.35 (c (n "weresocool_ring_buffer") (v "1.0.35") (d (list (d (n "weresocool_shared") (r "^1.0.35") (d #t) (k 0)))) (h "0x5y8v1ai6aamjhz4qhkp1x8hs5kij9sgvwrrpqhcmn2wqry8m8y")))

(define-public crate-weresocool_ring_buffer-1.0.36 (c (n "weresocool_ring_buffer") (v "1.0.36") (d (list (d (n "weresocool_shared") (r "^1.0.36") (d #t) (k 0)))) (h "182al1p55ppm5k71qnwsng2g36w3dpvj1k0jg1n0wf73wqprxh2n")))

(define-public crate-weresocool_ring_buffer-1.0.37 (c (n "weresocool_ring_buffer") (v "1.0.37") (d (list (d (n "weresocool_shared") (r "^1.0.37") (d #t) (k 0)))) (h "0vckg0v0p5yaqa5bx7b188yraph1n0kxlv31w6s7z3nlz3z7ymvk")))

(define-public crate-weresocool_ring_buffer-1.0.38 (c (n "weresocool_ring_buffer") (v "1.0.38") (d (list (d (n "weresocool_shared") (r "^1.0.38") (d #t) (k 0)))) (h "1nk03ngpw7pxb3pl86bclks7anp6mcmhidlgxjjjw5vnxqlq1f83")))

(define-public crate-weresocool_ring_buffer-1.0.39 (c (n "weresocool_ring_buffer") (v "1.0.39") (d (list (d (n "weresocool_shared") (r "^1.0.39") (d #t) (k 0)))) (h "1gxn0w4wrn41h1ww9rydydsg3p5wm9y96qz4ryqs06ddmbrlghgx")))

(define-public crate-weresocool_ring_buffer-1.0.40 (c (n "weresocool_ring_buffer") (v "1.0.40") (d (list (d (n "weresocool_shared") (r "^1.0.40") (d #t) (k 0)))) (h "0p34cg75wys1la3d8f2ng3w9cfn8fa30vj08dr0cprima4p7hr3l")))

(define-public crate-weresocool_ring_buffer-1.0.41 (c (n "weresocool_ring_buffer") (v "1.0.41") (d (list (d (n "weresocool_shared") (r "^1.0.41") (d #t) (k 0)))) (h "13fgxj7v1vbvan3lba426sfikf8lafggj29l4wrq2jk82n06r8ad")))

(define-public crate-weresocool_ring_buffer-1.0.42 (c (n "weresocool_ring_buffer") (v "1.0.42") (d (list (d (n "weresocool_shared") (r "^1.0.42") (d #t) (k 0)))) (h "1qa5008pzfzsvvx6xr5slzfi8ybpdlz86vh79mhbfwl7vycdjhmm")))

(define-public crate-weresocool_ring_buffer-1.0.43 (c (n "weresocool_ring_buffer") (v "1.0.43") (d (list (d (n "weresocool_shared") (r "^1.0.43") (d #t) (k 0)))) (h "0xnvydaml3lgvvxwbhpmmr80ajwzz6w59ym4wsl3ym4x33lvxn4d")))

(define-public crate-weresocool_ring_buffer-1.0.44 (c (n "weresocool_ring_buffer") (v "1.0.44") (d (list (d (n "weresocool_shared") (r "^1.0.44") (d #t) (k 0)))) (h "1ph2nw7j0d8kd491cwzvkx2d6p0mk1a0qdas5ifxykljczifhdpx")))

(define-public crate-weresocool_ring_buffer-1.0.45 (c (n "weresocool_ring_buffer") (v "1.0.45") (d (list (d (n "weresocool_shared") (r "^1.0.45") (d #t) (k 0)))) (h "02913kqcg2v372w4cm427ngggr6q3spp4lg9wnxc8cd9ny3pg6qn")))

(define-public crate-weresocool_ring_buffer-1.0.47 (c (n "weresocool_ring_buffer") (v "1.0.47") (d (list (d (n "weresocool_shared") (r "^1.0.47") (d #t) (k 0)))) (h "1gcn93ms2ngbkzc12i9y29d5m4gycdznv8904jdqq18xwhrsbmkx")))

