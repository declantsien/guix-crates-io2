(define-module (crates-io we re weresocool_filter) #:use-module (crates-io))

(define-public crate-weresocool_filter-1.0.45 (c (n "weresocool_filter") (v "1.0.45") (d (list (d (n "num-rational") (r "^0.3.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "weresocool_shared") (r "^1.0.47") (d #t) (k 0)))) (h "0jzw0nxxiqblcw3akv85pvzy7pcwwmnmhhb909879gg8vnkh2qqa")))

