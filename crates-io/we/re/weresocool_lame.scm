(define-module (crates-io we re weresocool_lame) #:use-module (crates-io))

(define-public crate-weresocool_lame-1.0.0 (c (n "weresocool_lame") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1npa1v8yjrr3pgn6b17syka073a1zy2nblfhriplypyjys6g5332")))

(define-public crate-weresocool_lame-1.0.1 (c (n "weresocool_lame") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0vlzhz400ghl75ijk5zh78znffa1f1v0lv2vpy2pkhl6cz9w0ss4")))

(define-public crate-weresocool_lame-1.0.2 (c (n "weresocool_lame") (v "1.0.2") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ps39pkpx7ycyq2bjyqb6phlbj8i59svipwcb6r4gqpcwmk2g26x")))

(define-public crate-weresocool_lame-1.0.3 (c (n "weresocool_lame") (v "1.0.3") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0jhjm4pffm8jpl6ch84n0kvc5vj7kkrx0qnqg6jspzl8jsmvgr5c")))

(define-public crate-weresocool_lame-1.0.31 (c (n "weresocool_lame") (v "1.0.31") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "18gd55jqmbzbi6ivvm5wwxn97nws39bqkfvy4zjvagvxhpd00dsb")))

(define-public crate-weresocool_lame-1.0.32 (c (n "weresocool_lame") (v "1.0.32") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "11n9x39x8610sp0f6dyph5kkg2zixqw1013vcjdyhd8khlz7d0gi")))

(define-public crate-weresocool_lame-1.0.33 (c (n "weresocool_lame") (v "1.0.33") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0x56xgfpmvvyji13ziyyfsbxj7xqwm618yxb8gllbm6iidwldq67")))

(define-public crate-weresocool_lame-1.0.34 (c (n "weresocool_lame") (v "1.0.34") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "111n35niyady3llpkiimhcjk30m530hxc5mn8j10d2fpj3vb3gv6")))

(define-public crate-weresocool_lame-1.0.35 (c (n "weresocool_lame") (v "1.0.35") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "14kpvjg7q8iv4ws0zy38ajrz0k9jl43xrw40h6pmv0irxw3k4lzc")))

(define-public crate-weresocool_lame-1.0.36 (c (n "weresocool_lame") (v "1.0.36") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "102nypwy0xm67i7cyjbrwsqhpkk1zy0wapzihzgraxhikm259ywp")))

(define-public crate-weresocool_lame-1.0.37 (c (n "weresocool_lame") (v "1.0.37") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1y734rp9hxssn17n1hjambp6858f6jal28jriwqs2njzpnw5fcy0")))

(define-public crate-weresocool_lame-1.0.38 (c (n "weresocool_lame") (v "1.0.38") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ib4w1zj1rr3g12vw1ka4x88nbqvjhx55w1xz228p5qd4avzf0wz")))

(define-public crate-weresocool_lame-1.0.39 (c (n "weresocool_lame") (v "1.0.39") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0xbrkiv4rm1r36a204lskjz87m5jqpm2hw47m2s81bhdqs01kr8z")))

(define-public crate-weresocool_lame-1.0.40 (c (n "weresocool_lame") (v "1.0.40") (d (list (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0c34va76vdwg3a6pym8ws2i5irha1gpymrhy09vw15nalg860xf9")))

(define-public crate-weresocool_lame-1.0.41 (c (n "weresocool_lame") (v "1.0.41") (d (list (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0b3ab2yq1gakzn91yqipfiqid722rlxbr8isfbks9vjfjlkghjmp")))

(define-public crate-weresocool_lame-1.0.42 (c (n "weresocool_lame") (v "1.0.42") (d (list (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "03052fi9wh3gv3dwfg7r70816qz5hxbzcbljlpyiij6bf28i7kww")))

(define-public crate-weresocool_lame-1.0.43 (c (n "weresocool_lame") (v "1.0.43") (d (list (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0yqrhcn4r9abbcxfk9d7cqk2phpddrkbs5j31d2fxyirxhsy919d")))

(define-public crate-weresocool_lame-1.0.44 (c (n "weresocool_lame") (v "1.0.44") (d (list (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1f9pyngi4smgmzh5w4lw6s3y2la8yh0z01axynnq2bxbr808c48c")))

(define-public crate-weresocool_lame-1.0.45 (c (n "weresocool_lame") (v "1.0.45") (d (list (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1dh9plk2w3r5qr7llrd9318ksc2hzrzv4544lydp4a50fi0lsmyp")))

(define-public crate-weresocool_lame-1.0.46 (c (n "weresocool_lame") (v "1.0.46") (d (list (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "00pcbxld344whqb3x4aydj4k4jviz5arbjkwh9xciz4605mz01nn")))

(define-public crate-weresocool_lame-1.0.47 (c (n "weresocool_lame") (v "1.0.47") (d (list (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0vp2cvqwcinhm8y704pck1z5vg4v0z9ajwwzq6np5xylsqw662vh")))

