(define-module (crates-io we re weresocool_portaudio) #:use-module (crates-io))

(define-public crate-weresocool_portaudio-0.7.0 (c (n "weresocool_portaudio") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "weresocool_portaudio_sys") (r "^0.1.0") (d #t) (k 0)))) (h "1rgp11zp0vywj40d3rqfw72yjklvwp75ikcs0fx0nd1gf2d4ilfq")))

(define-public crate-weresocool_portaudio-1.0.40 (c (n "weresocool_portaudio") (v "1.0.40") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "weresocool_portaudio_sys") (r "^1.0.40") (d #t) (k 0)))) (h "132pa2s4b55gxll2kbq78y27dpaakwflvgqahaks96r06yai6783")))

(define-public crate-weresocool_portaudio-1.0.41 (c (n "weresocool_portaudio") (v "1.0.41") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "weresocool_portaudio_sys") (r "^1.0.41") (d #t) (k 0)))) (h "1xk5bg9y62l9yi3wrqhh2zajmnsl45x4k9yn8ffz9hlii7mc46j4")))

(define-public crate-weresocool_portaudio-1.0.42 (c (n "weresocool_portaudio") (v "1.0.42") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "weresocool_portaudio_sys") (r "^1.0.42") (d #t) (k 0)))) (h "1v3d5x2wcrm068myb4lr8pk68ia7df2kbhyp83j7im2lsvnypvkp")))

(define-public crate-weresocool_portaudio-1.0.43 (c (n "weresocool_portaudio") (v "1.0.43") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "weresocool_portaudio_sys") (r "^1.0.43") (d #t) (k 0)))) (h "14dki0axdwd02likqag5fxjf09kh7ns9my3kc25wmlyk747h4rgx")))

(define-public crate-weresocool_portaudio-1.0.44 (c (n "weresocool_portaudio") (v "1.0.44") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "weresocool_portaudio_sys") (r "^1.0.44") (d #t) (k 0)))) (h "113jgv1xqxn18zry7yc4j84rqdwqs7cylhw0p4phj3gcg6i6jxyz")))

(define-public crate-weresocool_portaudio-1.0.45 (c (n "weresocool_portaudio") (v "1.0.45") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "weresocool_portaudio_sys") (r "^1.0.45") (d #t) (k 0)))) (h "0d4nhsp84xrz489m148i5wdczbcn0f8qsqajqzl6ijxj6jidkrp5")))

(define-public crate-weresocool_portaudio-1.0.46 (c (n "weresocool_portaudio") (v "1.0.46") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "weresocool_portaudio_sys") (r "^1.0.46") (d #t) (k 0)))) (h "1x8kqhhasg6aaibbbszsyp2ppawlc5llg4anp3102hhdybpf8g4a")))

(define-public crate-weresocool_portaudio-1.0.47 (c (n "weresocool_portaudio") (v "1.0.47") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "weresocool_portaudio_sys") (r "^1.0.47") (d #t) (k 0)))) (h "1k07h611842q2wpc7r2xr6ndjkdycgn8jddkdvks0v6gf1biz13z")))

