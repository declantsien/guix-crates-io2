(define-module (crates-io we re weresocool_analyze) #:use-module (crates-io))

(define-public crate-weresocool_analyze-1.0.0 (c (n "weresocool_analyze") (v "1.0.0") (d (list (d (n "weresocool_shared") (r "^1.0.0") (d #t) (k 0)))) (h "06w79gwp3nwy55ix2lkj0v8qjg4g11z2s6fn4shis3bx0dadwi6k")))

(define-public crate-weresocool_analyze-1.0.1 (c (n "weresocool_analyze") (v "1.0.1") (d (list (d (n "weresocool_shared") (r "^1.0.1") (d #t) (k 0)))) (h "00ix0bi6ml5jwivajvmvvs9dfbkbk6wl6rnhdq0z7df5cjnp0q91")))

(define-public crate-weresocool_analyze-1.0.2 (c (n "weresocool_analyze") (v "1.0.2") (d (list (d (n "weresocool_shared") (r "^1.0.1") (d #t) (k 0)))) (h "1psm637lq6mfk3ljxf27dj0594n1c2c6z494gnxg9xwg7gyk8a1s")))

(define-public crate-weresocool_analyze-1.0.3 (c (n "weresocool_analyze") (v "1.0.3") (d (list (d (n "weresocool_shared") (r "^1.0.3") (d #t) (k 0)))) (h "04zcqmjgxqgfff0sjwa2h7hhj2kqdcgjxhd65r3yij2sf0h4ibn1")))

(define-public crate-weresocool_analyze-1.0.31 (c (n "weresocool_analyze") (v "1.0.31") (d (list (d (n "weresocool_shared") (r "^1.0.31") (d #t) (k 0)))) (h "17y8w5hw2ym348gbk7ygjj3fhganjb40q7hfkvis7gm3gdp5pipl")))

(define-public crate-weresocool_analyze-1.0.32 (c (n "weresocool_analyze") (v "1.0.32") (d (list (d (n "weresocool_shared") (r "^1.0.32") (d #t) (k 0)))) (h "05161q2gdipqfcr5s58y5wzbhsj40xm8w6b5ny7cibfr7afzz2yb")))

(define-public crate-weresocool_analyze-1.0.33 (c (n "weresocool_analyze") (v "1.0.33") (d (list (d (n "weresocool_shared") (r "^1.0.33") (d #t) (k 0)))) (h "0vx0ks6hhz938y7z7sw2c9xwhxyjh3lprkm8dpda5ldr88ix8gd9")))

(define-public crate-weresocool_analyze-1.0.34 (c (n "weresocool_analyze") (v "1.0.34") (d (list (d (n "weresocool_shared") (r "^1.0.34") (d #t) (k 0)))) (h "1dj79d3v6mw68yb9wisfz057lswbv79pkhz689m8yakcyidgjv36")))

(define-public crate-weresocool_analyze-1.0.35 (c (n "weresocool_analyze") (v "1.0.35") (d (list (d (n "weresocool_shared") (r "^1.0.35") (d #t) (k 0)))) (h "1nj8qskdw4ks1a4gzb25psf8ambksg1j2n9adi57dmw9ll9ysa70")))

(define-public crate-weresocool_analyze-1.0.36 (c (n "weresocool_analyze") (v "1.0.36") (d (list (d (n "weresocool_shared") (r "^1.0.36") (d #t) (k 0)))) (h "0351fb3cirrx35msgcm75wv389ahp1sscflbhgd2hzs1dzbl1400")))

(define-public crate-weresocool_analyze-1.0.37 (c (n "weresocool_analyze") (v "1.0.37") (d (list (d (n "weresocool_shared") (r "^1.0.37") (d #t) (k 0)))) (h "07dm1qhazzdfbzmvny7980hc3q2vsnrynf32kpvqylv2yim6yyz7")))

(define-public crate-weresocool_analyze-1.0.38 (c (n "weresocool_analyze") (v "1.0.38") (d (list (d (n "weresocool_shared") (r "^1.0.38") (d #t) (k 0)))) (h "1snkxpj88gmfalpyfnv3n74bh66sy4dk0xqvpwpnm3kccc6md2sl")))

(define-public crate-weresocool_analyze-1.0.39 (c (n "weresocool_analyze") (v "1.0.39") (d (list (d (n "weresocool_shared") (r "^1.0.39") (d #t) (k 0)))) (h "1gh1ylvcl0h6xk67d0wlag6bbfxv6xnvknrql9v4210ym0jkdsf2")))

(define-public crate-weresocool_analyze-1.0.40 (c (n "weresocool_analyze") (v "1.0.40") (d (list (d (n "weresocool_shared") (r "^1.0.40") (d #t) (k 0)))) (h "1wch410ga5nfr1klj61n97a1wibrwcpv35nwrx935iyixa98af1a")))

(define-public crate-weresocool_analyze-1.0.41 (c (n "weresocool_analyze") (v "1.0.41") (d (list (d (n "weresocool_shared") (r "^1.0.41") (d #t) (k 0)))) (h "1ykvl0x78f7zafi7k7rlknmzx87z7rsr39gjklmgfzj0bvbzj0av")))

(define-public crate-weresocool_analyze-1.0.42 (c (n "weresocool_analyze") (v "1.0.42") (d (list (d (n "weresocool_shared") (r "^1.0.42") (d #t) (k 0)))) (h "0fnq4mjgig2cd64j9p3149yvacsqh9kidx48sqip12r7c6yf3sdy")))

(define-public crate-weresocool_analyze-1.0.43 (c (n "weresocool_analyze") (v "1.0.43") (d (list (d (n "weresocool_shared") (r "^1.0.43") (d #t) (k 0)))) (h "006f9xgl36lnjvay0v8xln0mkmfgplqwk8n195nprg6z1mwxqa8j")))

(define-public crate-weresocool_analyze-1.0.44 (c (n "weresocool_analyze") (v "1.0.44") (d (list (d (n "weresocool_shared") (r "^1.0.44") (d #t) (k 0)))) (h "0kadiw5rrpgz32s46vckp30dwhcd2xxiy09hd6sk9ri34ibxphcp")))

(define-public crate-weresocool_analyze-1.0.45 (c (n "weresocool_analyze") (v "1.0.45") (d (list (d (n "weresocool_shared") (r "^1.0.45") (d #t) (k 0)))) (h "0akc6xaf64x8w2vczi88yypshh0p8fh0apdkdpg01mpid9z90j5s")))

(define-public crate-weresocool_analyze-1.0.46 (c (n "weresocool_analyze") (v "1.0.46") (d (list (d (n "weresocool_shared") (r "^1.0.46") (d #t) (k 0)))) (h "1zgmddv3a03c0cxnhf9v95dcrshnl383250c1v63v2r1wr3yf3yq")))

(define-public crate-weresocool_analyze-1.0.47 (c (n "weresocool_analyze") (v "1.0.47") (d (list (d (n "weresocool_shared") (r "^1.0.47") (d #t) (k 0)))) (h "1az5nmm0r57j94vkadx4azzs8q05p3yly9kv7x1909x0fgqv4i6v")))

