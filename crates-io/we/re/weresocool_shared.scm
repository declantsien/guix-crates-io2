(define-module (crates-io we re weresocool_shared) #:use-module (crates-io))

(define-public crate-weresocool_shared-1.0.0 (c (n "weresocool_shared") (v "1.0.0") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "1y0s82awldcy14n6cq9byr5rgniq0fh5ignbibxgap3vn1dq1win")))

(define-public crate-weresocool_shared-1.0.1 (c (n "weresocool_shared") (v "1.0.1") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "12ks75avw2l9mb8fc3lgcci2xz0dpjam1lb0cdzdg6jk3zfmlmyd")))

(define-public crate-weresocool_shared-1.0.2 (c (n "weresocool_shared") (v "1.0.2") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "0zrc6klm4i1a88rsxj87n9iskvvh87xzalg6jgz0x24n621dg0k1")))

(define-public crate-weresocool_shared-1.0.3 (c (n "weresocool_shared") (v "1.0.3") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "1fv8nhlvbzw81s0pg3byah49y40ql6n8fx4z993ldfsbl4hppf1h")))

(define-public crate-weresocool_shared-1.0.31 (c (n "weresocool_shared") (v "1.0.31") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "05ij9cc6m6z4ah09df1a1qf76x404ysc7ylczc7wz702xj8mcvyf")))

(define-public crate-weresocool_shared-1.0.32 (c (n "weresocool_shared") (v "1.0.32") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "1mxjxv94kzzmsf27a6bnyjwz5radjg9a2nivhk0dfvd24ap8yyy8")))

(define-public crate-weresocool_shared-1.0.33 (c (n "weresocool_shared") (v "1.0.33") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "0i4vfvd58ksznwia98lg4z35gfd4bahh44fqqrnn1c60sqisdlk9")))

(define-public crate-weresocool_shared-1.0.34 (c (n "weresocool_shared") (v "1.0.34") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "00fv5m77q2n9z055vvrrsc5xm7d8v4c1ykfwqia8g28dakk062ap")))

(define-public crate-weresocool_shared-1.0.35 (c (n "weresocool_shared") (v "1.0.35") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "1s26bicpi6qx9pbkb67pkk1spjql1q96hwwjh920yhv9j1rqcsph")))

(define-public crate-weresocool_shared-1.0.36 (c (n "weresocool_shared") (v "1.0.36") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "1pci71kyg0xh9fjm85is8wynhvvkdsdckc9fw0nbhi96h9sdh5yn")))

(define-public crate-weresocool_shared-1.0.37 (c (n "weresocool_shared") (v "1.0.37") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "07nbr8w7kyx5rqs569msv2szgr35hx57cgd3ykmh2r2wgq72s0fp")))

(define-public crate-weresocool_shared-1.0.38 (c (n "weresocool_shared") (v "1.0.38") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "07j9b892g2n7a3qxwy7cv12xg1mmz3846mwags7wca7fmiv1g6yd")))

(define-public crate-weresocool_shared-1.0.39 (c (n "weresocool_shared") (v "1.0.39") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "05rdk736mn1bx296vnhpn95hllqh4iw29hs6p3279h36wbxc5p4q")))

(define-public crate-weresocool_shared-1.0.40 (c (n "weresocool_shared") (v "1.0.40") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "1dyn62wl9gl3wqm4jvhraf482dsgydm3ynlfwprmxzk9fh481h6w")))

(define-public crate-weresocool_shared-1.0.41 (c (n "weresocool_shared") (v "1.0.41") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "16rwax5m9kz85r71xb6z539dmbgpnrynfc5n6dlkp22r9p57ymvl")))

(define-public crate-weresocool_shared-1.0.42 (c (n "weresocool_shared") (v "1.0.42") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "0rllir0cmilxs8lmnc93bb2szcmh8vdwj7qj8r0ah8j69mhvi2bn")))

(define-public crate-weresocool_shared-1.0.43 (c (n "weresocool_shared") (v "1.0.43") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)))) (h "02b4l8c2qwb3x9sp5396xjszaarh3ilgpy6n7ffjga53qxzdp5ws")))

(define-public crate-weresocool_shared-1.0.44 (c (n "weresocool_shared") (v "1.0.44") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)))) (h "1pn6ssha617lf5ny0ajacwqia1l3w252cv96jcrzqx353blv1lmy")))

(define-public crate-weresocool_shared-1.0.45 (c (n "weresocool_shared") (v "1.0.45") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)))) (h "04xvydraf644b7vgrm6x9r5injg1sxyg3xgi8gjasall6kacgv5f")))

(define-public crate-weresocool_shared-1.0.46 (c (n "weresocool_shared") (v "1.0.46") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f15qgnz9hc8xhkm22gdqsf89n8a467rz5ah5m32jiza6ka2ycz9")))

(define-public crate-weresocool_shared-1.0.47 (c (n "weresocool_shared") (v "1.0.47") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qfxqx3ppd49xc6n6z0azygi6ci1f2hp79pa01b90w2m83bxrmlh")))

