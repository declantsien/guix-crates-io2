(define-module (crates-io we re weresocool_portaudio_sys) #:use-module (crates-io))

(define-public crate-weresocool_portaudio_sys-0.1.0 (c (n "weresocool_portaudio_sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "148z1kddgzrdwjjia85rxpa1wsbbqi644rcbdafji9n2bj72420r") (l "portaudio")))

(define-public crate-weresocool_portaudio_sys-1.0.40 (c (n "weresocool_portaudio_sys") (v "1.0.40") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "05y0yk09hmz2i15iy8v603ab633224z4bwb1acl3fs69d7pqk7gc") (l "portaudio")))

(define-public crate-weresocool_portaudio_sys-1.0.41 (c (n "weresocool_portaudio_sys") (v "1.0.41") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1wnhahv56yy7lgqbk2bdhavwkcbx3i7305m8rc5wpwnb8l3a5zaq") (l "portaudio")))

(define-public crate-weresocool_portaudio_sys-1.0.42 (c (n "weresocool_portaudio_sys") (v "1.0.42") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1s7h7gb19bi52wbykw3l6icwm09cx9544kydm5906g3z2mkz33w4") (l "portaudio")))

(define-public crate-weresocool_portaudio_sys-1.0.43 (c (n "weresocool_portaudio_sys") (v "1.0.43") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "030iyp4yq7sd0mmdlbvx1fj2lan6cp5nf99hdky8x4grlysvrsh0") (l "portaudio")))

(define-public crate-weresocool_portaudio_sys-1.0.44 (c (n "weresocool_portaudio_sys") (v "1.0.44") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0z2gbh32s5sjvp64madxmm26wdr6mnq7n0ihigqd9k62wlcbwqpc") (l "portaudio")))

(define-public crate-weresocool_portaudio_sys-1.0.45 (c (n "weresocool_portaudio_sys") (v "1.0.45") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1m13acijdii504j9jl79w61mr5l6pmg1mh9l9jgbvfxdrw3v3shm") (l "portaudio")))

(define-public crate-weresocool_portaudio_sys-1.0.46 (c (n "weresocool_portaudio_sys") (v "1.0.46") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1n8akdwz9jj4hnzhwi5p3jfijz7bsjw1ak55x0dlq0439bqkw3jh") (l "portaudio")))

(define-public crate-weresocool_portaudio_sys-1.0.47 (c (n "weresocool_portaudio_sys") (v "1.0.47") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "076mk0jr2d14kx126pq77lfxw2s0jhkpsw6qm876681039h8pdvq") (l "portaudio")))

