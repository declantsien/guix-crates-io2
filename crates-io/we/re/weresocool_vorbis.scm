(define-module (crates-io we re weresocool_vorbis) #:use-module (crates-io))

(define-public crate-weresocool_vorbis-1.0.0 (c (n "weresocool_vorbis") (v "1.0.0") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "0jzva4b1lbkpblqz6f5p8jhz72dbgbnsqrf38jq836vnbb6p013g")))

(define-public crate-weresocool_vorbis-1.0.1 (c (n "weresocool_vorbis") (v "1.0.1") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "1684gn21f1igi81kvig8ydgmf171lpkzkk9hb4frkd6ns3jf8h9g")))

(define-public crate-weresocool_vorbis-1.0.2 (c (n "weresocool_vorbis") (v "1.0.2") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "0dy8q72jhk3hi3rszx4aax8y3l21r2z40qwccmmkvjj4pw49mgdz")))

(define-public crate-weresocool_vorbis-1.0.3 (c (n "weresocool_vorbis") (v "1.0.3") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "00zi2bmzynjmpdqiphpdks2vq8qf2gc1l9z1jmnbpgfm0v7mzxgr")))

(define-public crate-weresocool_vorbis-1.0.31 (c (n "weresocool_vorbis") (v "1.0.31") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "14anryrbcdj7v7xxj6h82cn84kdb9ayckaw52qrsilv0lhmz0jn0")))

(define-public crate-weresocool_vorbis-1.0.32 (c (n "weresocool_vorbis") (v "1.0.32") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "051dhz39n69ns2gdbmp76fipa08q67y24nxzzw91bli0k4f6gb47")))

(define-public crate-weresocool_vorbis-1.0.33 (c (n "weresocool_vorbis") (v "1.0.33") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "1dz79h750mvc22ki2qihnk6fzly40wg8fjabpaf7dv2nm8gngjxb")))

(define-public crate-weresocool_vorbis-1.0.34 (c (n "weresocool_vorbis") (v "1.0.34") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "1s8pdkcjvra9zdbrz9wsgfcr6v665p4nybjcd4rjkv64wj7q3x56")))

(define-public crate-weresocool_vorbis-1.0.35 (c (n "weresocool_vorbis") (v "1.0.35") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "0qv4jccfh2da312rd2j1zbg9wpzxznc9jl41q8rh9saxk8zrcwp7")))

(define-public crate-weresocool_vorbis-1.0.36 (c (n "weresocool_vorbis") (v "1.0.36") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "0gq6lxqz7rzw71njwyp9sjijvl4bapbvh225rjzkid2m5kfgib8c")))

(define-public crate-weresocool_vorbis-1.0.37 (c (n "weresocool_vorbis") (v "1.0.37") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "0d9vyyqwzap0zanqncml02sk6qf83x8mhbb51ya3ndqx1gbvjdgb")))

(define-public crate-weresocool_vorbis-1.0.38 (c (n "weresocool_vorbis") (v "1.0.38") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "1k6pw61qmrk21arwz398h70gwj9h2v8si57jx4mrca499sc3fmc4")))

(define-public crate-weresocool_vorbis-1.0.39 (c (n "weresocool_vorbis") (v "1.0.39") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)))) (h "1dci0mfq2bxka85svgk2ckdlbvnlnnh69nsmy00xrbblgmna54s4")))

(define-public crate-weresocool_vorbis-1.0.40 (c (n "weresocool_vorbis") (v "1.0.40") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)) (d (n "weresocool_shared") (r "^1.0.40") (d #t) (k 0)))) (h "12dwxb7rxh67zypsqp6ldf8jcfbh7xaassj6kibzflz47axazwf2")))

(define-public crate-weresocool_vorbis-1.0.41 (c (n "weresocool_vorbis") (v "1.0.41") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)) (d (n "weresocool_shared") (r "^1.0.41") (d #t) (k 0)))) (h "0xg7z8adv7l0cq3y8whl6w2gs4c8x7i045c2v72ry0m4qcccggki")))

(define-public crate-weresocool_vorbis-1.0.43 (c (n "weresocool_vorbis") (v "1.0.43") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)) (d (n "weresocool_shared") (r "^1.0.43") (d #t) (k 0)))) (h "09yqgl5167i0r93n2faf4d9s6cv01y7q7gs6n6ffxn7020sgy248")))

(define-public crate-weresocool_vorbis-1.0.44 (c (n "weresocool_vorbis") (v "1.0.44") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)) (d (n "weresocool_shared") (r "^1.0.44") (d #t) (k 0)))) (h "0h839hkksp11c6xvgp5il5w5vs09c9nf6b3lr8nm0a3fja2cl04v")))

(define-public crate-weresocool_vorbis-1.0.45 (c (n "weresocool_vorbis") (v "1.0.45") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)) (d (n "weresocool_shared") (r "^1.0.45") (d #t) (k 0)))) (h "05d1bw7x5bcc1rfd4fj5cizc1jzqrad8f139fq2jrcblaz20ff7i")))

(define-public crate-weresocool_vorbis-1.0.47 (c (n "weresocool_vorbis") (v "1.0.47") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "vorbis-encoder") (r "^0.1.4") (d #t) (k 0)) (d (n "weresocool_shared") (r "^1.0.47") (d #t) (k 0)))) (h "12yzi9f0xj9j3360hi6n1dwl2083plgi4ak2za2n1x1gai64baz6")))

