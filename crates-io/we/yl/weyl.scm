(define-module (crates-io we yl weyl) #:use-module (crates-io))

(define-public crate-weyl-0.1.0 (c (n "weyl") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)))) (h "1gpk894sx3bmhzh7kyi0b4lc6x9wlp452lmfw8b11zi9r03ng8v5")))

(define-public crate-weyl-0.1.1 (c (n "weyl") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)))) (h "1rrnragggd0mq08pp5x507sc8smjq30ry6wxibif65z1qwk6np4p")))

(define-public crate-weyl-0.1.2 (c (n "weyl") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)))) (h "1cggchcm4g9aly6ybc893mrpyhzcx2ndn9dvbj4x87lqq36spsga")))

(define-public crate-weyl-0.1.3 (c (n "weyl") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)))) (h "0dsrbfapg3icagrmkl1jyvkczp27gid49scn7l0mni57858wfny5")))

