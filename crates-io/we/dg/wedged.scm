(define-module (crates-io we dg wedged) #:use-module (crates-io))

(define-public crate-wedged-0.1.0 (c (n "wedged") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "typenum") (r "^1.15") (d #t) (k 0)))) (h "1qivdmg9vw81fp8r29jrl81nckk3dl77slsfaqiajvmy4q7jqqaq") (f (quote (("fn_traits"))))))

