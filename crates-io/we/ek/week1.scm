(define-module (crates-io we ek week1) #:use-module (crates-io))

(define-public crate-week1-0.1.0 (c (n "week1") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18v6yypp7iifmlchm41s395a2pryb6d9mlwbygaabdw79m1nzs2j")))

(define-public crate-week1-0.1.1 (c (n "week1") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p3xmrb580121rpgfjfm6k0jdg59j1636qci19cmhsljqxv1q3g5")))

(define-public crate-week1-0.1.2 (c (n "week1") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qg6sp9ciwg81rprmk96n1izihqqkk1xczgz5jqg1812wwj3xhwk")))

(define-public crate-week1-0.1.5 (c (n "week1") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wncs6lkh0ylf62c81bvqcqyby7f9i03gq81zhm8478p6n4xfwq1")))

(define-public crate-week1-0.1.6 (c (n "week1") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zdia7ix5rjqazy7h24w38rq08sxj618ivhk3aisc6pzbinjl5m9")))

(define-public crate-week1-0.1.7 (c (n "week1") (v "0.1.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g02zcqnmxfl0kdz40qlw1164ws59j7a5560gl5xzmkr9l0swacz")))

(define-public crate-week1-0.1.8 (c (n "week1") (v "0.1.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gs8cjygfixs7pdyqdrlpghrrqw947qbdih52wkxich2g0r1hcb7")))

