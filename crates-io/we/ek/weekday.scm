(define-module (crates-io we ek weekday) #:use-module (crates-io))

(define-public crate-weekday-0.0.0 (c (n "weekday") (v "0.0.0") (h "1iz2hh1hrvzvy3bkqx43qyy7cm7chmc57x16znclvnibjp1dhchv")))

(define-public crate-weekday-0.1.0 (c (n "weekday") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n588sviv6clmcha5hkd5629w93dr5gc426qrc0gy6gfx5v2v0q4") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

(define-public crate-weekday-0.1.1 (c (n "weekday") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13x9y49sqjkwwqgk9sd0gkd07v8rvmdp84ia3w0zz5hwhaasmzd6") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

(define-public crate-weekday-0.1.2 (c (n "weekday") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0syal3n4ygl1m9aaw4rc5aqx23471q05dyh1yjc6bhnc7ymgzfh7") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

(define-public crate-weekday-0.2.0 (c (n "weekday") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10n9dl6gwr5wjf5zn9s610shc52d5k89lzf1qgflsj5hi9x5vprx") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

(define-public crate-weekday-0.2.1 (c (n "weekday") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1g6b52cnl11m2397f77la66cjhr909723mf48llwi7i2za26fl5b") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

(define-public crate-weekday-0.3.0 (c (n "weekday") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "00gyxxz8g10m6mxv4vaxx0yvj5cb50dbipqmifzmahx03380lcc2") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

