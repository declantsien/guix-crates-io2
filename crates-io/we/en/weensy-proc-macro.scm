(define-module (crates-io we en weensy-proc-macro) #:use-module (crates-io))

(define-public crate-weensy-proc-macro-0.1.0 (c (n "weensy-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "teensy4-bsp") (r "^0.4.4") (f (quote ("usb-logging"))) (d #t) (k 2)))) (h "1iijhnhjxif7a9vg4j9kwbx47frfsn69lvnl2dfrx2pxp2rhif5p")))

(define-public crate-weensy-proc-macro-0.1.1 (c (n "weensy-proc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "teensy4-bsp") (r "^0.4.4") (f (quote ("usb-logging"))) (d #t) (k 2)))) (h "1b1z1ny44qm0y7ymmrgn7m2gv9lbvc4ifgv7nm8c50jmw4s9lnq6")))

(define-public crate-weensy-proc-macro-0.1.2 (c (n "weensy-proc-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "teensy4-bsp") (r "^0.4.4") (f (quote ("usb-logging"))) (d #t) (k 2)))) (h "06k3jrba4976v7h1n05zzh7phw5pfcnllkj1iwj7hlwwaxc87drb")))

