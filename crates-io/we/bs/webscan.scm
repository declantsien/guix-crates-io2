(define-module (crates-io we bs webscan) #:use-module (crates-io))

(define-public crate-webscan-0.0.1 (c (n "webscan") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)))) (h "1x992rqkdmh053gjww9wgifxx58j3w0a846jrzi2mlc2kbp3vw2y")))

(define-public crate-webscan-0.1.0 (c (n "webscan") (v "0.1.0") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "18i76aj7xvjrp67ki4gfaq1c5hbw2a1yjyj5kyl4k66c5pyjp0f4")))

(define-public crate-webscan-0.2.0 (c (n "webscan") (v "0.2.0") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0cr0qg2vxbnipjzq1xg3j42lm5561mcnv71ivlrsbd1h180w7mby")))

(define-public crate-webscan-0.3.0 (c (n "webscan") (v "0.3.0") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1lsjmmsdgy0kq1c37fjps3r4a3dsbgr5srg5dz4nl7rijfdmgsyi") (f (quote (("vendored" "openssl-sys/vendored"))))))

(define-public crate-webscan-0.4.0 (c (n "webscan") (v "0.4.0") (d (list (d (n "dns-lookup") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (t "cfg(linux)") (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0bfdhh2rx5pxdk216nwjn9bx5snv7ffbqs9q7yag5ww4pgi23hy4") (f (quote (("vendored" "openssl-sys/vendored"))))))

