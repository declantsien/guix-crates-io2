(define-module (crates-io we bs website-screenshot-actix-governor) #:use-module (crates-io))

(define-public crate-website-screenshot-actix-governor-0.3.0 (c (n "website-screenshot-actix-governor") (v "0.3.0") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "actix-rt") (r "^2.5") (d #t) (k 2)) (d (n "actix-web") (r "^4") (k 0)) (d (n "actix-web") (r "^4") (f (quote ("macros"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "governor") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0aadal0ckbhd83z9380bj6dmmw9vl076f7iw5x59f6wjxr5x6d3q") (f (quote (("logger" "log"))))))

