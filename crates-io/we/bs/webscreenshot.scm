(define-module (crates-io we bs webscreenshot) #:use-module (crates-io))

(define-public crate-webscreenshot-0.2.1 (c (n "webscreenshot") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.1") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)))) (h "0zp90bzgkl1w8aih97cjihcq36bv4ixib0gay4lbiv6lz2k10kds") (f (quote (("cli-binary" "clap"))))))

(define-public crate-webscreenshot-0.2.2 (c (n "webscreenshot") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.1") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)))) (h "1qg9mg84crgby023lk9642b60jwj03aqg10n4bfjlyhr5ksdzxwj") (f (quote (("cli-binary" "clap"))))))

