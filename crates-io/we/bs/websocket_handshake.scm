(define-module (crates-io we bs websocket_handshake) #:use-module (crates-io))

(define-public crate-websocket_handshake-0.1.0 (c (n "websocket_handshake") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)))) (h "10wzzp9xj1dfy8jr516hb0m43di7xn1651a5m7i0px4gh8s910b0")))

