(define-module (crates-io we bs webster) #:use-module (crates-io))

(define-public crate-webster-0.1.0 (c (n "webster") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "090idii8r63p7yzz4m85rb42d31g9g41bqcfsxdq8c9jgc19lcay")))

(define-public crate-webster-0.1.1 (c (n "webster") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "06qwf3m982g888n9fmklpgl5x88pvyfxys7nq4y1zjg3j85x09j3")))

(define-public crate-webster-0.2.0 (c (n "webster") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1wb21abdnkllgk8s2a15hryxaizr7n61m9pm1v560b7yyhjdjfxg")))

(define-public crate-webster-0.2.1 (c (n "webster") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1xv5kh6p9yzw5wr6m2yw0bbmkckyz96s5rhyyikdkplr3y09yp29")))

(define-public crate-webster-0.3.0 (c (n "webster") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libflate") (r "^1") (d #t) (k 0)) (d (n "libflate") (r "^1.1.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 1)))) (h "0g6dfnn0w2g1z0wf8ghv8kzqx7dirn40fyja68sdrhc9l2867a7f")))

