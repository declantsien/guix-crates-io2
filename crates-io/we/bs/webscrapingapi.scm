(define-module (crates-io we bs webscrapingapi) #:use-module (crates-io))

(define-public crate-webscrapingapi-0.1.0 (c (n "webscrapingapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0djprfqzmz041w7zmdp5algvj80adkibysa1ljqxs5w8cs4yhphl")))

