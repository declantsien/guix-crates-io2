(define-module (crates-io we bs website-icon-extract) #:use-module (crates-io))

(define-public crate-website-icon-extract-0.1.0 (c (n "website-icon-extract") (v "0.1.0") (d (list (d (n "native-tls") (r "^0.1.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "0hr15dbs2y60k9ym316b7s01pgsci5ppzhsd8kpwvrk0178kia30")))

(define-public crate-website-icon-extract-0.2.0 (c (n "website-icon-extract") (v "0.2.0") (d (list (d (n "native-tls") (r "^0.1.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "1flyy1zwrzxpyx8zgi5yzbc9np3ks0pdmpsdjgcx6v2q2sv2333b")))

(define-public crate-website-icon-extract-0.3.0 (c (n "website-icon-extract") (v "0.3.0") (d (list (d (n "native-tls") (r "^0.1.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "08lc1zfdlcb26r56vy68fslj6mnxa0rxbqz5xkfyz4jb0dhgzh14")))

(define-public crate-website-icon-extract-0.4.0 (c (n "website-icon-extract") (v "0.4.0") (d (list (d (n "native-tls") (r "^0.2.3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0mdpd3gpfkvfyia59vfqy7x8nhxz3ad5qzxjgz3s53yzfhjrwpgw")))

(define-public crate-website-icon-extract-0.4.1 (c (n "website-icon-extract") (v "0.4.1") (d (list (d (n "native-tls") (r "^0.2.3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.0.0") (d #t) (k 0)))) (h "09k1isp6qbpijkgsmcsskcvqdc7w9bls09v38p5f06dwac40khfx")))

(define-public crate-website-icon-extract-0.4.2 (c (n "website-icon-extract") (v "0.4.2") (d (list (d (n "native-tls") (r "^0.2.7") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1dc69vr76yh6rk280rqh1q8fhz7jqz76qlk7pzirhzvsyflfy5pf")))

(define-public crate-website-icon-extract-0.5.0 (c (n "website-icon-extract") (v "0.5.0") (d (list (d (n "imagesize") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1hhkyjpzqi4p1h8sh19gf975ln2yb1z6hcqwsywya2pl71fvy9lc")))

(define-public crate-website-icon-extract-0.5.1 (c (n "website-icon-extract") (v "0.5.1") (d (list (d (n "imagesize") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 0)))) (h "13x5dp4gdbg6rg3jxncxyzwbp2biqj13783ik3c38g1p2cs8iyyl")))

(define-public crate-website-icon-extract-0.5.2 (c (n "website-icon-extract") (v "0.5.2") (d (list (d (n "imagesize") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quick-xml") (r "^0.25.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "deflate" "gzip"))) (d #t) (k 0)))) (h "0dnjmgmzm9bzyqs8afhsiaaccp52qk7wqqxbf6cb3iw3j4bj98al")))

(define-public crate-website-icon-extract-0.5.3 (c (n "website-icon-extract") (v "0.5.3") (d (list (d (n "imagesize") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "deflate" "gzip"))) (d #t) (k 0)))) (h "07dqy5kpypcm948yrb28w67phxvhhlwpw2gcrb31w3pqagyhkg3z")))

