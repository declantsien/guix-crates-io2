(define-module (crates-io we bs webscale) #:use-module (crates-io))

(define-public crate-webscale-0.9.4 (c (n "webscale") (v "0.9.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "irc") (r "^0.11.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "08j28j4dmm7iw8myqh31z3rk7mf7cv5mxkqpxh4b1rlbcvxy51dn")))

