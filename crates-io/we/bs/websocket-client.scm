(define-module (crates-io we bs websocket-client) #:use-module (crates-io))

(define-public crate-websocket-client-0.1.0 (c (n "websocket-client") (v "0.1.0") (d (list (d (n "simple-error") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "websocket") (r "^0.20") (f (quote ("sync" "sync-ssl"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0jqp6z8ga4wv550a51ipjjsi5j0mxhv96i54my29zp1awfgsrn0m")))

(define-public crate-websocket-client-0.1.1 (c (n "websocket-client") (v "0.1.1") (d (list (d (n "simple-error") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "websocket") (r "^0.20") (f (quote ("sync" "sync-ssl"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0lpdcn0kqd2mwk8ihswnnyrj39xgyalfnfwyjs9nsr1ddbbkhpgg")))

(define-public crate-websocket-client-0.2.0 (c (n "websocket-client") (v "0.2.0") (d (list (d (n "simple-error") (r "^0.1") (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "websocket") (r "^0.20") (f (quote ("sync" "sync-ssl"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0m8g1iq4p1n44rk1wq6gcycmirayjmcsksdb7pdlsscgcgshx7xl")))

(define-public crate-websocket-client-0.2.1 (c (n "websocket-client") (v "0.2.1") (d (list (d (n "simple-error") (r "^0.1") (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "websocket") (r "^0.20") (f (quote ("sync" "sync-ssl"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0wq19h6rvdlf91llz4prj7fhicah1rb8fmwsh18nz2rvcvnyh30k")))

