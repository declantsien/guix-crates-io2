(define-module (crates-io we bs webscrape) #:use-module (crates-io))

(define-public crate-webscrape-0.1.0 (c (n "webscrape") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "headless_chrome_fork") (r "^1") (f (quote ("fetch"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bzvbq5gg0y8abckcnhi5knk0k3v9l3sy7pxi42wd1y8znzz2zxq")))

