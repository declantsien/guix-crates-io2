(define-module (crates-io we bs websocket-stream) #:use-module (crates-io))

(define-public crate-websocket-stream-0.0.1 (c (n "websocket-stream") (v "0.0.1") (h "1a4m6yx0drh417phzyi77l40da9xkiycg0p8hgvv0nwmcypz7bdf")))

(define-public crate-websocket-stream-0.0.2 (c (n "websocket-stream") (v "0.0.2") (h "01dpxqdkm99mm18hxsnw4ycra6p6ql1k4z83ihp4h66q8vjw4wmi")))

(define-public crate-websocket-stream-0.0.3 (c (n "websocket-stream") (v "0.0.3") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1vk8f71rmyyk1b9fdb3sx61x7sf8izj1amzpbz0k8rbmnib88z5m")))

(define-public crate-websocket-stream-0.0.4 (c (n "websocket-stream") (v "0.0.4") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0dl5fk3bcyfwkczsidimg227sz0n17vh5fzbr7l7kjbzjm6zdppp")))

(define-public crate-websocket-stream-0.0.5 (c (n "websocket-stream") (v "0.0.5") (d (list (d (n "errno") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0hry07ddv73ww73j2bmmchdi0532nzaj2wqramsk7gqdghayjg15")))

