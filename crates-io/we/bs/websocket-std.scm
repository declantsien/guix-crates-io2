(define-module (crates-io we bs websocket-std) #:use-module (crates-io))

(define-public crate-websocket-std-0.0.1 (c (n "websocket-std") (v "0.0.1") (h "04348wv1gb3648l85h8hszkncfyl78p5623j1haxa932ldl9gisb") (y #t) (r "1.65.0")))

(define-public crate-websocket-std-0.0.2 (c (n "websocket-std") (v "0.0.2") (h "07h08d26c93banjpfqbvcbzmxmpdmhkkljqilapzz1bkpmyrjv7d") (y #t) (r "1.65.0")))

(define-public crate-websocket-std-0.0.3 (c (n "websocket-std") (v "0.0.3") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.11") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)))) (h "1scyydbgwfhnanpwbvgghrpk8j5b4a69y9pnc1pam0f0zz5gwhnz") (r "1.65.0")))

(define-public crate-websocket-std-0.0.4 (c (n "websocket-std") (v "0.0.4") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.11") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)))) (h "15sdrmspj165xvh00jsa03y5msfy07sz8iv0cfngfddqihzxcdym") (r "1.65.0")))

(define-public crate-websocket-std-0.0.5 (c (n "websocket-std") (v "0.0.5") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.11") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)))) (h "1dkgzg23msni62n75qjc64x7vl83bzyqylq130dl6kznfyks52zm") (r "1.65.0")))

(define-public crate-websocket-std-0.0.6 (c (n "websocket-std") (v "0.0.6") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.11") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)))) (h "09q74qhnvxxbbazvp1cfdclg694mr400xmpcjnvdsl9fr2hvq3y5") (r "1.65.0")))

