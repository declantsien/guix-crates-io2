(define-module (crates-io we bs websocket-transport) #:use-module (crates-io))

(define-public crate-websocket-transport-0.1.0 (c (n "websocket-transport") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "websocket") (r "^0.20") (f (quote ("async"))) (k 0)))) (h "0iarfw8vmysr6pv2ip5la3k98yrr5vm05v6p111xxv874dqw57f8") (f (quote (("strict" "clippy") ("default"))))))

