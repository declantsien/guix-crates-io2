(define-module (crates-io we bs webstool) #:use-module (crates-io))

(define-public crate-webstool-0.1.0 (c (n "webstool") (v "0.1.0") (h "0spr34r6kwr0wk6ca7ml7r1v4ggzcwpj22bl6d45kmqb68n6w9zj")))

(define-public crate-webstool-0.1.1 (c (n "webstool") (v "0.1.1") (h "004iln1gxrj49vmbvr2m24hjbh3inrkjfqqnrdxawwp6cihji2na")))

(define-public crate-webstool-0.1.2 (c (n "webstool") (v "0.1.2") (h "0vk3imvgmg6jdgyg3vqwl5jrszd3qic5bklfi5h1gjkp341wvkzh")))

(define-public crate-webstool-0.1.3 (c (n "webstool") (v "0.1.3") (h "19vzb85vkzh9f0m6slyalxyc4brq5x6qblfwc60mhx07aack57qb")))

