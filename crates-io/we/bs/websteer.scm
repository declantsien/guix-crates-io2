(define-module (crates-io we bs websteer) #:use-module (crates-io))

(define-public crate-websteer-0.1.0 (c (n "websteer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1c8s8ll155fkz2pj341fqznysr63assmhb3wcwxsiwqpz64p5d7q")))

