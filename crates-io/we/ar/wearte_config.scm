(define-module (crates-io we ar wearte_config) #:use-module (crates-io))

(define-public crate-wearte_config-0.0.1 (c (n "wearte_config") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1nz61szfgfa7wqz34kmiix2ld480wpgall4j6jxa9mi492dhfsws")))

(define-public crate-wearte_config-0.0.2 (c (n "wearte_config") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0ry6si6y5cyrj7skd6fd3mqsi227xqdkc1j1fzadd6qynzmci8ms")))

