(define-module (crates-io we ar wearte_helpers) #:use-module (crates-io))

(define-public crate-wearte_helpers-0.0.1 (c (n "wearte_helpers") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4.2") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1iljgmhlpi400y6kmy18yv7xiz3s8pr0zlji3vgid9v1lrjl5f61")))

(define-public crate-wearte_helpers-0.0.2 (c (n "wearte_helpers") (v "0.0.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_htmlescape") (r "^0.4.2") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0mschbwcxclnszxmkas2qjmxc94a2x6fblvk56v4ckrpvplv90mr")))

