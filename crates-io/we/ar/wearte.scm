(define-module (crates-io we ar wearte) #:use-module (crates-io))

(define-public crate-wearte-0.0.1 (c (n "wearte") (v "0.0.1") (d (list (d (n "actix-web") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "wearte_config") (r "^0.0") (d #t) (k 0)) (d (n "wearte_derive") (r "^0.0") (d #t) (k 0)) (d (n "wearte_helpers") (r "^0.0") (d #t) (k 0)))) (h "0hzjpgs0hxfxwz74la1ka7bh0gx56m4fc2y90vb76ibb76issks3") (f (quote (("with-actix-web" "actix-web" "wearte_derive/actix-web") ("default"))))))

(define-public crate-wearte-0.0.2 (c (n "wearte") (v "0.0.2") (d (list (d (n "actix-web") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 2)) (d (n "wearte_config") (r "^0.0") (d #t) (k 0)) (d (n "wearte_derive") (r "^0.0") (d #t) (k 0)) (d (n "wearte_helpers") (r "^0.0") (d #t) (k 0)))) (h "03r47nwdqp10iy1qd8i7kzwy4xdnsgv3pxdps8kgg1izrmlm5n2g") (f (quote (("with-actix-web" "actix-web" "wearte_derive/actix-web") ("default"))))))

