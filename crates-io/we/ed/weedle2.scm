(define-module (crates-io we ed weedle2) #:use-module (crates-io))

(define-public crate-weedle2-2.0.0-alpha0 (c (n "weedle2") (v "2.0.0-alpha0") (d (list (d (n "nom") (r "^5.0.0") (f (quote ("std"))) (k 0)))) (h "1z6qrybk9mnvgvxp9frj9p40ryh9k7awh9qg4bhlx1mh3w10w9w6")))

(define-public crate-weedle2-2.0.0 (c (n "weedle2") (v "2.0.0") (d (list (d (n "nom") (r "^5.0.0") (f (quote ("std"))) (k 0)))) (h "0lg49whn6sahsc4z0p5fwvm300p1j2p5swww35fncp4zgf0cnmc6")))

(define-public crate-weedle2-2.0.1 (c (n "weedle2") (v "2.0.1") (d (list (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (f (quote ("std"))) (k 0)))) (h "19kjw3s6rkx6c2ik13s10v47cra7x1z6f941l70pvpyhqdhb6sca")))

(define-public crate-weedle2-3.0.0 (c (n "weedle2") (v "3.0.0") (d (list (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (f (quote ("std"))) (k 0)))) (h "0msq2npqbg7xb9fc0r0yyl4j10lmlplg4k5680f16wgl3ja0swsx")))

(define-public crate-weedle2-4.0.0 (c (n "weedle2") (v "4.0.0") (d (list (d (n "fs-err") (r "^2.7.0") (d #t) (k 2)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0hbph2pwmwvkn6lcbrpjc24j5r2y0aaxnjyndwqa4hqzdqhcay9f")))

(define-public crate-weedle2-4.0.1 (c (n "weedle2") (v "4.0.1") (d (list (d (n "fs-err") (r "^2.7.0") (d #t) (k 2)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "1z845m4ywlv61bxwgijj78yh9lmrsv8fgmwgikmx5qi2f4k0xsv9") (y #t)))

(define-public crate-weedle2-5.0.0 (c (n "weedle2") (v "5.0.0") (d (list (d (n "fs-err") (r "^2.7.0") (d #t) (k 2)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "03mf9r8py0za38jh35ww3lgvd0lxky2hhy26z7d8g6h9xhj2r3cr")))

