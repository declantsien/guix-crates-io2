(define-module (crates-io we ed weedle) #:use-module (crates-io))

(define-public crate-weedle-0.1.0 (c (n "weedle") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (f (quote ("regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "05q8fv8i036c3w2l76qa3kwqh3fi3ybk3l84jifhnryysj72biky")))

(define-public crate-weedle-0.1.1 (c (n "weedle") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (f (quote ("regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "00gbqyj3gkyr372qx09ikfdfkx2rvg00399yi1afhgylf2nmg6y0")))

(define-public crate-weedle-0.2.0 (c (n "weedle") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (f (quote ("regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "069w1iq52mh4lbbavrfk5m3m0ii603dmal9bxh053x45z6mrm20x")))

(define-public crate-weedle-0.2.1 (c (n "weedle") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (f (quote ("regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "0z7r3s4fibhkc3dnj93il4afw8445wpxzhf9d61qixv01c8zkabd")))

(define-public crate-weedle-0.3.0 (c (n "weedle") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (f (quote ("regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "0bs6g9fyw57gajkrydwb7gwyvvbssh0b5qgadp74qjhjgmjgnx59")))

(define-public crate-weedle-0.3.1 (c (n "weedle") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (f (quote ("regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0l2k1hk0g3sxv235jzl0p8m6ibf2rz944mgrhpyg2pzzk3s4xi7n")))

(define-public crate-weedle-0.4.0 (c (n "weedle") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (f (quote ("regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1dn1v8d2gii6ja6fsfbjw9mlkiscdc9cw1vlyvpcw485vr6lnv4p")))

(define-public crate-weedle-0.5.0 (c (n "weedle") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (f (quote ("regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "13x5n9a2gpfrpipbx605z5yrhwsz9llwnykkwhiyic5f7c1zr4mg")))

(define-public crate-weedle-0.6.0 (c (n "weedle") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (f (quote ("regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1dgypcys7zqy69mkzakn0hzlackw6kpmgp36n3i0al1m03i5q4d2")))

(define-public crate-weedle-0.6.1 (c (n "weedle") (v "0.6.1") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "0vx71awgn295bniilzin741cb3g3f1ski5h9hhs296yr3hsg8na0")))

(define-public crate-weedle-0.7.0 (c (n "weedle") (v "0.7.0") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "0z2i8ryzmcnp3lhr006gcdlmxk50hpp0dindpi08z7vdp478qgl3")))

(define-public crate-weedle-0.8.0 (c (n "weedle") (v "0.8.0") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "0i00y61qpbr9jayp9k5m1v9vrb8hbm6p72hb75jxk1i32dzwd916")))

(define-public crate-weedle-0.9.0 (c (n "weedle") (v "0.9.0") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "07id7j5ay31vz466yj5ly539fdjlcpqyljq078gqpvns02i4mi5w")))

(define-public crate-weedle-0.10.0 (c (n "weedle") (v "0.10.0") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "0r0i2kllvkn9jil6cjzxdi1zsc6p1gjyk751w8lyclaii1q3zd1v")))

(define-public crate-weedle-0.11.0 (c (n "weedle") (v "0.11.0") (d (list (d (n "nom") (r "^5.0.0") (f (quote ("std"))) (k 0)))) (h "0dvz88iz8qj3f634iiif1g5kbzs4zb4ysx3viw6q0fkjxfglyzca")))

(define-public crate-weedle-0.12.0 (c (n "weedle") (v "0.12.0") (d (list (d (n "nom") (r "^5.0.0") (f (quote ("std"))) (k 0)))) (h "0d96ign8rfpd5692zwv2m067yhynsvq5gs42jsh8qx178y8502b1")))

