(define-module (crates-io we na wena) #:use-module (crates-io))

(define-public crate-wena-0.0.1 (c (n "wena") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1rf6fz0h41dkj6546acdgvsxdpk4s4jsaqxni4m69snyczgpw8vr")))

(define-public crate-wena-0.0.2 (c (n "wena") (v "0.0.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "12zi655zw5g6b42sgb4jihh9dmbbzarp29sblsadys2sgbk2nz7l")))

(define-public crate-wena-0.0.3 (c (n "wena") (v "0.0.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "104gag9jk2w9z8bcqw7mk9w6iwpfz89mq2r1xx8ivf9xs7k1kmhl")))

(define-public crate-wena-0.1.0 (c (n "wena") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "1psjhj08jgvplys8d70ncb4ikdaqrjn0sjarnrcnha1ns144lp90")))

(define-public crate-wena-0.1.1 (c (n "wena") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "16yi3h5dqjsypvs5n5dc7xv21v5ck0i5k34d7dfvi10jmy0swjs4")))

(define-public crate-wena-0.2.0 (c (n "wena") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "12waggndrb1fx3n9ippa216dzyxj9nyviy2mn2agyns71i9wglpj")))

