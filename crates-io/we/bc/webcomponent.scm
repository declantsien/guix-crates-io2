(define-module (crates-io we bc webcomponent) #:use-module (crates-io))

(define-public crate-webcomponent-0.0.1 (c (n "webcomponent") (v "0.0.1") (d (list (d (n "stdweb") (r "^0.2.2") (d #t) (k 0)))) (h "12adw0dk2isk7lyykp5c1s7q6lryyf4756r882g4zx1pxnxnv46j")))

(define-public crate-webcomponent-0.0.2 (c (n "webcomponent") (v "0.0.2") (d (list (d (n "stdweb") (r "^0.2.2") (d #t) (k 0)))) (h "18g3rj2pjwq1yk29jchqbkha36vqqqfiv38kgw9kg5g62ff58ir7")))

(define-public crate-webcomponent-0.0.3 (c (n "webcomponent") (v "0.0.3") (d (list (d (n "stdweb") (r "^0.2.2") (d #t) (k 0)))) (h "1wf0sf0k8nbrg90qsby5vn6r4wqnfgmxi215r6c1pmfrn2q36qn0")))

(define-public crate-webcomponent-0.0.4 (c (n "webcomponent") (v "0.0.4") (d (list (d (n "stdweb") (r "^0.2.2") (d #t) (k 0)))) (h "1v809bqxc4w05r38dpl6m05dhl27sd2b7ymxlkhcya9h08bsxycx")))

(define-public crate-webcomponent-0.0.5 (c (n "webcomponent") (v "0.0.5") (d (list (d (n "stdweb") (r "^0.2.2") (d #t) (k 0)))) (h "07zp7mjs38gsqnfw1bcqp1hlbmls842c976ax5cw3v7jgbh63nz9")))

(define-public crate-webcomponent-0.1.0 (c (n "webcomponent") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "callback") (r "^0.0.1") (d #t) (k 0)) (d (n "cstring") (r "^0.0.1") (d #t) (k 0)))) (h "100apwmqkxn01hfcz3a5phc1ffvgprlmgnpgzjq4df2kd5gghr2f")))

(define-public crate-webcomponent-0.1.2 (c (n "webcomponent") (v "0.1.2") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "callback") (r "^0.0.1") (d #t) (k 0)) (d (n "cstring") (r "^0.0.1") (d #t) (k 0)))) (h "12w08g6yhrsjdk7k92z0cjbqsz2d7jvhzgkdm9iiqy584y85db3v")))

(define-public crate-webcomponent-0.1.3 (c (n "webcomponent") (v "0.1.3") (d (list (d (n "callback") (r "^0.0.1") (d #t) (k 0)) (d (n "cstring") (r "^0.0.1") (d #t) (k 0)) (d (n "globals") (r "^0.0.1") (d #t) (k 0)))) (h "12xxx8i32dc5vk7jvx0fgjcy7b2cx4jlfvhx9a7c0vp7q2v53n12")))

(define-public crate-webcomponent-0.2.0 (c (n "webcomponent") (v "0.2.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "cstring") (r "^0.0.2") (d #t) (k 0)))) (h "1qjj796qpn2l489132xnwdicgbj758hhg68b0vpgcp2zamdk1fmy")))

(define-public crate-webcomponent-0.3.0 (c (n "webcomponent") (v "0.3.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "web-dom") (r "^0.0.4") (d #t) (k 0)))) (h "1zjpf0vn8b8pamnhh612pfx61drv1hryj0cgch3zkmxgpwqwplxf")))

(define-public crate-webcomponent-0.3.1 (c (n "webcomponent") (v "0.3.1") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "web-dom") (r "^0.0.5") (d #t) (k 0)))) (h "1531idpigb1jhr6mra981rxs83ya5bxcc1l24ipapg7qg83c2428")))

(define-public crate-webcomponent-0.3.2 (c (n "webcomponent") (v "0.3.2") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "web-dom") (r "^0.0.6") (d #t) (k 0)))) (h "0za6k60r4734wgfpwa4csq7m8g0c9fldh1nadfhv4zalcmp7kdsm")))

(define-public crate-webcomponent-0.3.3 (c (n "webcomponent") (v "0.3.3") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "web-dom") (r "^0.0.8") (d #t) (k 0)))) (h "0gb6xjannvn9akvc2xvp7hg0gzma8314hyh9qbqcckjgwbc71lkl")))

(define-public crate-webcomponent-0.4.0 (c (n "webcomponent") (v "0.4.0") (d (list (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "1p0rv2mx63w5h5zhwdgvls6hb2v64h5lg3aj99dy6zrw8bcc6k8m")))

(define-public crate-webcomponent-0.4.1 (c (n "webcomponent") (v "0.4.1") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "1b18qrg3a3nq64ddfish8hg6yx81mgfhznjp031x0nrssqjrjj0f")))

(define-public crate-webcomponent-0.4.2 (c (n "webcomponent") (v "0.4.2") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0iijc4xlgc4hdc7bkzklh9l6yr81zgnxpavlc7ziigz3gmfci8mg")))

(define-public crate-webcomponent-0.4.3 (c (n "webcomponent") (v "0.4.3") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "080alw864mkq6m3h5b8g33rrd46i9cdkswav5f3zvh1bjc03qlda")))

(define-public crate-webcomponent-0.4.4 (c (n "webcomponent") (v "0.4.4") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "highlight") (r "^0") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "01s7c8hazvnggjgdjnh9mclg0m7w8q6jwrlm2ghbklbsr556n2b7")))

(define-public crate-webcomponent-0.4.5 (c (n "webcomponent") (v "0.4.5") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "highlight") (r "^0") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0v3mw6vmsb6iz190pk54cyyhqq0if1l8hblqspd2sadlpiv2y7k8")))

(define-public crate-webcomponent-0.4.6 (c (n "webcomponent") (v "0.4.6") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "highlight") (r "^0") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1x11cag3wddqinrv8crq58pjp5hqmhz6r38492r0pmpwsh6c1yra")))

(define-public crate-webcomponent-0.5.0 (c (n "webcomponent") (v "0.5.0") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "highlight") (r "^0") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "08qrd605rn604qcq8v4fxa2fjrq7xxh82fhjiqckq6z756ij1af3")))

(define-public crate-webcomponent-0.5.1 (c (n "webcomponent") (v "0.5.1") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "highlight") (r "^0") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1hlmi8w65klc8pkjc2cj990f6ap4ix356ll07q20jv46mgw8cgjz")))

(define-public crate-webcomponent-0.5.2 (c (n "webcomponent") (v "0.5.2") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1yszq05z5vzi9v08vx64crwjal8fa19r5amalzn27cfhnmvvmp07")))

(define-public crate-webcomponent-0.6.0 (c (n "webcomponent") (v "0.6.0") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1nwp4xl6660mv5wjw01r3x04542jrhkw6r23b3b051cg8nw5z661")))

(define-public crate-webcomponent-0.6.1 (c (n "webcomponent") (v "0.6.1") (d (list (d (n "callback") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "1hbydldvknvngy6fgmhbxxljzpjix3xmb17y0b8zinihdp0g0m7q")))

(define-public crate-webcomponent-0.6.2 (c (n "webcomponent") (v "0.6.2") (d (list (d (n "callback") (r "^0.5.0") (d #t) (k 0)) (d (n "js") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "1vr28ymqs6sjhssngjap5sam461sypny8f6027iamsl9k8x6a551")))

