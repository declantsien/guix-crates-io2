(define-module (crates-io we bc webcryptobox) #:use-module (crates-io))

(define-public crate-webcryptobox-1.0.0 (c (n "webcryptobox") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)))) (h "1bcvqd849da22kwfpq5q4iiwn4vcpdy7azilknzxsq01n6x7vnlm")))

(define-public crate-webcryptobox-1.0.1 (c (n "webcryptobox") (v "1.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)))) (h "1kap47g2ff9lp0zmsh93mr62w806smfx7587wk4hcrylc9b2zxk2")))

(define-public crate-webcryptobox-1.0.2 (c (n "webcryptobox") (v "1.0.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)))) (h "18b15yq4cfh0gfsax821vj3fh2vqq6bkbb74vw6s3sgwbm17kv5z")))

(define-public crate-webcryptobox-1.0.3 (c (n "webcryptobox") (v "1.0.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)))) (h "1dq70qzxbc5jpvw6pxqvmp0mhlk551akjirj13f1fvjyypn5z89g")))

(define-public crate-webcryptobox-1.0.4 (c (n "webcryptobox") (v "1.0.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)))) (h "0i2fvghls26nbnrwfdajsy0881xw45hvpm8l8qyzpbxjn39147gr")))

(define-public crate-webcryptobox-1.0.5 (c (n "webcryptobox") (v "1.0.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)))) (h "1j6xh8c5403xvx1yahyz7kwqqwzsi00jvynwxnp548y0hy5qnyx8")))

(define-public crate-webcryptobox-2.0.0 (c (n "webcryptobox") (v "2.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)))) (h "1syg09j4p7zb6fmad54kjf3p4v8v5y88954dg87mclyh7xbsv2ch")))

(define-public crate-webcryptobox-2.1.0 (c (n "webcryptobox") (v "2.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)))) (h "1l11m0c4yzskazfs92j7jrk9jihlg9yyq1bywrywnxky5b0b97xg")))

(define-public crate-webcryptobox-2.1.1 (c (n "webcryptobox") (v "2.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.64") (d #t) (k 0)))) (h "0wnd3l9cmnrg6mr0lsycz12vac0nnkai9dl7by44mpf583lr5ad7")))

