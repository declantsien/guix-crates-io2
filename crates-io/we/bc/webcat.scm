(define-module (crates-io we bc webcat) #:use-module (crates-io))

(define-public crate-webcat-0.1.0 (c (n "webcat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "macros" "time" "rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1wvqjkxrcgff6c59k9dlkb0spgnsbrzr4q62byqcpzmwpclr89wd")))

(define-public crate-webcat-0.2.0 (c (n "webcat") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "macros" "time" "rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "00d72kklh3fkhrw8zhxk6qk7ykh5w9dyl2sx4m064z2jy4qbdjhq")))

