(define-module (crates-io we at weather_icons) #:use-module (crates-io))

(define-public crate-weather_icons-0.1.0 (c (n "weather_icons") (v "0.1.0") (h "0wk0idipr655g0jfh19pmn7l0yyb7zpf0272ikg9y5pv5ris61cz")))

(define-public crate-weather_icons-0.1.1 (c (n "weather_icons") (v "0.1.1") (h "0hrp3i64iax664svjkl2njg1xvkyz6aqk5bk7gab1p56wkgl1s43")))

(define-public crate-weather_icons-0.1.2 (c (n "weather_icons") (v "0.1.2") (h "0rcfn7r6j3689k242kv1lvrkk8l3xcn34cj537xrywxldqf2winj")))

(define-public crate-weather_icons-0.2.0 (c (n "weather_icons") (v "0.2.0") (h "1jzd2zqcxrfsgzwb69zanmv9vnnlxb1v0krrqi2rp73xk4l8mfpw")))

(define-public crate-weather_icons-0.3.0 (c (n "weather_icons") (v "0.3.0") (h "0rm523qcqhw1zl2axwxb343adliz4lm76x6gn85va8zl8zbi3dx9")))

(define-public crate-weather_icons-0.4.0 (c (n "weather_icons") (v "0.4.0") (h "1idxvv4sn2wxz639bxa747p9k5a3l08024glnl4fqvvfnbj8bk9j")))

