(define-module (crates-io we at weather-underground-cli) #:use-module (crates-io))

(define-public crate-weather-underground-cli-0.1.0 (c (n "weather-underground-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "weather-underground") (r "^0.1") (d #t) (k 0)))) (h "00f170gygkl2vn2q76l7pgm59182cdd9s9l1zd792wgdry4sj7qa")))

