(define-module (crates-io we at weather_station) #:use-module (crates-io))

(define-public crate-weather_station-0.1.0 (c (n "weather_station") (v "0.1.0") (d (list (d (n "ads1x1x") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "hyper") (r "^0.12.35") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "prometheus") (r "^0.7") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.3") (d #t) (k 0)) (d (n "tmp1x2") (r "^0.2.0") (d #t) (k 0)))) (h "1inndvzy4rsfr80w192v87bqbpv8z0nsmhv4y9slh05inicdkbyz") (y #t)))

(define-public crate-weather_station-0.1.1 (c (n "weather_station") (v "0.1.1") (d (list (d (n "ads1x1x") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "hyper") (r "^0.12.35") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "prometheus") (r "^0.7") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.3") (d #t) (k 0)) (d (n "tmp1x2") (r "^0.2.0") (d #t) (k 0)))) (h "1p61x6ldxp4ghq0wy5yflbwh1lhbfgzd7ifznsi96maqnbmp3861") (y #t)))

(define-public crate-weather_station-0.0.0 (c (n "weather_station") (v "0.0.0") (h "0h537cg4wsvwdnjrpqja42a8q0l5sb05y1d5zk75q858jsblm5ki")))

