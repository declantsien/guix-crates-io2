(define-module (crates-io we at weather_rust_api) #:use-module (crates-io))

(define-public crate-weather_RUST_API-0.1.0 (c (n "weather_RUST_API") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "12hlbzn79mscfhkb0cpjdvx94cqmjjb2al41nay5jkip7x2k7cx2")))

