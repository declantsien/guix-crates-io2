(define-module (crates-io we at weathernoaa) #:use-module (crates-io))

(define-public crate-weathernoaa-0.1.0 (c (n "weathernoaa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksis523sknkqa44p4js95x6762d27vv6lmyipv0dfnhjp68xjrg")))

(define-public crate-weathernoaa-0.2.0 (c (n "weathernoaa") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00waljx4wrrpa11iqjiqwd533s69i4fqx325ghkmpgzddr5g1zk3")))

