(define-module (crates-io we at weather-utils) #:use-module (crates-io))

(define-public crate-weather-utils-0.1.0 (c (n "weather-utils") (v "0.1.0") (d (list (d (n "micromath") (r "^2.1.0") (o #t) (d #t) (k 0)))) (h "10y6bnhgbjvyqr867575xfr8g1p0c043rccvpfpx9phhskh190sp") (f (quote (("no-std" "micromath") ("default"))))))

(define-public crate-weather-utils-0.2.0 (c (n "weather-utils") (v "0.2.0") (d (list (d (n "micromath") (r "^2.1.0") (o #t) (d #t) (k 0)))) (h "1rcxvpma74b3jm9clivy66g2ivyzmmwljahvgzdlzmpmsd5nh306") (f (quote (("no-std" "micromath") ("default"))))))

