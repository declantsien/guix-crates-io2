(define-module (crates-io we at weather-info) #:use-module (crates-io))

(define-public crate-weather-info-0.1.0 (c (n "weather-info") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("native-tls-vendored" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1agniw737qx7p8grj8ss8il2hp5bjaaibg4lc8b23l43nkkj1yvp")))

