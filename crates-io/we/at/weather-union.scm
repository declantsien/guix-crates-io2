(define-module (crates-io we at weather-union) #:use-module (crates-io))

(define-public crate-weather-union-0.1.0 (c (n "weather-union") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "114854fskszhqrv8d0jbrcj83dihxyn8s99xhv9aj2rsdw8rnsi5")))

(define-public crate-weather-union-0.1.1 (c (n "weather-union") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "065csypmhyxvjxjnmd686wl2vhq5sbxgk033202s6b781ipgwkrr")))

(define-public crate-weather-union-0.1.2 (c (n "weather-union") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0hfbqyxcqlnmq6b1lzmz90g86xy45jf3fki9lhyxbqkairiwi2v8")))

(define-public crate-weather-union-0.2.0 (c (n "weather-union") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1c7xx6hnb7mq71p99vqr032d12x0lcgdagk3q999r3p71sb2cyhk")))

