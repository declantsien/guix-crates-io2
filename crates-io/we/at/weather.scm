(define-module (crates-io we at weather) #:use-module (crates-io))

(define-public crate-weather-0.1.0 (c (n "weather") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "clap") (r "^2.5.2") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.6") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.0.11") (d #t) (k 0)) (d (n "serde") (r "^0.7.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.5") (d #t) (k 0)))) (h "0wng3bvybz3dwdfq9yiy0xbpfjalkk5ld0wgjgs471qhff0dbf2r")))

(define-public crate-weather-0.1.1 (c (n "weather") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "clap") (r "^2.5.2") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.4") (d #t) (k 0)))) (h "0yjh6a7kwm2vylr4l9hsysf9bx7y1y2qag5y4sasb1bqqmf3s6rl")))

(define-public crate-weather-0.1.2 (c (n "weather") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "clap") (r "^2.5.2") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.4") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.4") (d #t) (k 0)))) (h "1ama2rn9rgqhl55paal9pyqp23mbwasi96kw5prfimdz32n0g092")))

