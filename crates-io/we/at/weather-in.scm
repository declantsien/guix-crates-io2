(define-module (crates-io we at weather-in) #:use-module (crates-io))

(define-public crate-weather-in-0.1.0 (c (n "weather-in") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "073pv3zxw889xvgd3kb9bwz76hyi73w1knplrg2m7nr8ni37dkr7")))

