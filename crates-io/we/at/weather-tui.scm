(define-module (crates-io we at weather-tui) #:use-module (crates-io))

(define-public crate-weather-tui-0.1.0 (c (n "weather-tui") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (f (quote ("crossterm"))) (k 0)))) (h "1zm7klf8sv6qaxasggnilww467ib41w60cxc3wwa2ignr2hhmdlv")))

