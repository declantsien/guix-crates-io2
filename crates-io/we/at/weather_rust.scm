(define-module (crates-io we at weather_rust) #:use-module (crates-io))

(define-public crate-weather_rust-0.1.0 (c (n "weather_rust") (v "0.1.0") (h "06vmnzzq16s7dhpv3gyjsnzlz5pvg8asr0ld6lr9nqjwl67dq8wn")))

(define-public crate-weather_rust-0.1.1 (c (n "weather_rust") (v "0.1.1") (h "1k1yxqxvikwxhdmzg1hm7s0ak359gfkpqpmdw4m3605dzfx7rv1k")))

