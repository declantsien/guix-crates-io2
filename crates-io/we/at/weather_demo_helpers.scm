(define-module (crates-io we at weather_demo_helpers) #:use-module (crates-io))

(define-public crate-weather_demo_helpers-0.1.0 (c (n "weather_demo_helpers") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0l3xwsmgjh9f3p5yva7dqx2cvsay6ncp3s23i3jdzxl1cpxhk5bp") (y #t)))

(define-public crate-weather_demo_helpers-0.1.1 (c (n "weather_demo_helpers") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0apl5mqsy42n8npkpvkkvcr01whcxj8jpip43m5qxygwn3id4g5z")))

