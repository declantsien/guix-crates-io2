(define-module (crates-io we at weather_cli_t) #:use-module (crates-io))

(define-public crate-weather_cli_t-0.1.0 (c (n "weather_cli_t") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "httpie2") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nl9ll8h7f2rjrdl5ybmpikxjgifgh0a13p7xj86dfd9v4l4mg2p")))

