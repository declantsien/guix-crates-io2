(define-module (crates-io we at weathergov) #:use-module (crates-io))

(define-public crate-weathergov-0.2.0 (c (n "weathergov") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde-xml-any") (r "^0.0.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)))) (h "1w9piygfbh4snmpyb7njp10ydd37n2vly47grh8va0zg9qrpmdv1")))

(define-public crate-weathergov-0.2.1 (c (n "weathergov") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde-xml-any") (r "^0.0.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)))) (h "0sgihjj17s30qkz3z4ps2iiz875kadmj06bhcy6r94dy0qi0j64i")))

