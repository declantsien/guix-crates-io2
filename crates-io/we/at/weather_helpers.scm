(define-module (crates-io we at weather_helpers) #:use-module (crates-io))

(define-public crate-weather_helpers-0.1.0 (c (n "weather_helpers") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "fastly") (r "^0.6.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1lkl6dhk1djc7f53fs5hzs4m6ii5046qaplnzzy147n4lrks39a4")))

(define-public crate-weather_helpers-0.1.1 (c (n "weather_helpers") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "fastly") (r "^0.6.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0gghvdd5kmi2l5fj07j3xsz8m9zdp79vznkn3pi4dfm5n4kwdg99")))

(define-public crate-weather_helpers-0.1.2 (c (n "weather_helpers") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fastly") (r "^0.8") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1adr4804zszzdq997viaqipgrlnfgrlj7jx04yscrgqb74pmncpp")))

(define-public crate-weather_helpers-0.1.3 (c (n "weather_helpers") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fastly") (r "^0.9.5") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04qimgqfz6r8q0xgqpyyakdx2kcxd8sjgiy4z65k6pxn4czcigyv")))

