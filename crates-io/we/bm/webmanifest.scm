(define-module (crates-io we bm webmanifest) #:use-module (crates-io))

(define-public crate-webmanifest-0.1.0 (c (n "webmanifest") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0dk4q7xpjn4k29jr0wn3xfhraar91p9yzd2lrihydd7ybqcpci30")))

(define-public crate-webmanifest-0.1.2 (c (n "webmanifest") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1jz91y03bhlzg49s81p01aqqlzzcylkpdbsinh5vfyrxvfclw0jn")))

(define-public crate-webmanifest-0.2.0 (c (n "webmanifest") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "08f00i9wq3vb33169v616gm6g69ymvc0y7k7v3gsi7r4aj8syyp4")))

(define-public crate-webmanifest-0.2.1 (c (n "webmanifest") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1m9sk2yq80lp7vlwq455qngrfi42y5wgpiq879c3syr65ygxifn7")))

(define-public crate-webmanifest-0.2.2 (c (n "webmanifest") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "15b6a53jlz74f5z1yxjd33214i86yrz1vymqlqfgfyz999lqcipc")))

(define-public crate-webmanifest-0.2.3 (c (n "webmanifest") (v "0.2.3") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0pgcw8swng7jl45m5s39iv9gj1adxl67yzxsbfsivixxnqcwpl2j")))

(define-public crate-webmanifest-0.2.4 (c (n "webmanifest") (v "0.2.4") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "15sqn8fv591wsqsw6v0n3si801vm0pmndgvf4ijkq4318y75cag3")))

(define-public crate-webmanifest-0.2.5 (c (n "webmanifest") (v "0.2.5") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1ry7w6lbn7q2vx418hnqqn81c3p2ja5ipg7vf5jipzhwb6ahxxya")))

(define-public crate-webmanifest-1.0.0 (c (n "webmanifest") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1n09zqscmpm04zjj8m2hyiznz52hf7r6b5bazwpdyg5pgawmmd5j")))

(define-public crate-webmanifest-1.0.1 (c (n "webmanifest") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0xp9sa7p4s1g66az4ypxcd3kx6z09ij9siqqbrg42abm07mhqal2")))

(define-public crate-webmanifest-1.0.2 (c (n "webmanifest") (v "1.0.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1xd5rv8fc5ws99jdpgzsq6rjgnfgln6vyn40878wni8ww6qgjy3f")))

(define-public crate-webmanifest-1.0.3 (c (n "webmanifest") (v "1.0.3") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0civ138i6drbhs08y2mrwxnn94fg5kcf4mg1mg3fajb11janspa9")))

(define-public crate-webmanifest-1.1.0 (c (n "webmanifest") (v "1.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1i7wa2x9x6kzd0xfrrxyfijsm6l7945kpj390lm5m64v3kkaf6ip")))

(define-public crate-webmanifest-1.1.1 (c (n "webmanifest") (v "1.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "133bgks38mhlv5qp3zd7zmghin950fnqqv1bx49n2c7982s24blf")))

