(define-module (crates-io we bm webm-native) #:use-module (crates-io))

(define-public crate-webm-native-0.4.0 (c (n "webm-native") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "webm-sys-native") (r "^0.3.0") (d #t) (k 0)))) (h "0sqgbd0nxzy0mv568xiinl7yb84r2asa9cqbakd86fq00fvxks4c") (y #t)))

(define-public crate-webm-native-0.4.1 (c (n "webm-native") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "webm-sys-native") (r "^0.3.1") (d #t) (k 0)))) (h "0kvmrjlplf6xcdjz8jc304pw4dxrf50snx3mqmb85yy1n3c3clwv") (y #t)))

(define-public crate-webm-native-0.4.2 (c (n "webm-native") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "webm-sys-native") (r "^0.3.1") (d #t) (k 0)))) (h "1wg9x8dihxny49mfcip8a1ikslbrdbj12yv84i4mfbzgcfvr2il9") (y #t)))

(define-public crate-webm-native-0.4.3 (c (n "webm-native") (v "0.4.3") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "webm-sys-native") (r "^0.3.2") (d #t) (k 0)))) (h "1bmv1q5ibcp15rvc0m9hkmbal69wv4fyh8nhq98i546a0zdy4vzw") (y #t)))

(define-public crate-webm-native-0.4.4 (c (n "webm-native") (v "0.4.4") (d (list (d (n "webm-sys-native") (r "^0.3.3") (d #t) (k 0)))) (h "0r3c6zm2x3946pzkl3vfkhxzdw2dff5f5c2c5pl1ph7x1j2lqdcd") (y #t)))

(define-public crate-webm-native-0.4.5 (c (n "webm-native") (v "0.4.5") (d (list (d (n "webm-sys-native") (r "^0.3.5") (d #t) (k 0)))) (h "0v8hjafswgnvqm28hblx6zx9imq7d8ajn8h231alhxxwsi3d4yai") (y #t)))

(define-public crate-webm-native-0.4.6 (c (n "webm-native") (v "0.4.6") (d (list (d (n "webm") (r "^0.4.5") (d #t) (k 0)))) (h "0i9gk43jh51d1g3vmqpyf8wb7p6xzlx1jidb9lsnl6x1vyf2kcdb") (y #t)))

