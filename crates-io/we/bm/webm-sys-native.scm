(define-module (crates-io we bm webm-sys-native) #:use-module (crates-io))

(define-public crate-webm-sys-native-0.3.0 (c (n "webm-sys-native") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0pdqd4rhk7b7cb2n7vy3cw6q00n6iibjdg76zs70v4p10vqp4jxa") (y #t)))

(define-public crate-webm-sys-native-0.3.1 (c (n "webm-sys-native") (v "0.3.1") (d (list (d (n "gcc") (r "^0.3.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "17i6n343mh24b2iy9a17xkr533vpx1vlz1fvx64917skqvvshp1w") (y #t)))

(define-public crate-webm-sys-native-0.3.2 (c (n "webm-sys-native") (v "0.3.2") (d (list (d (n "gcc") (r "^0.3.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "00xlxhrjjlzw2y4ypas5g3w2bqcvdc0bl751i6dfvamiwfmiz85b") (y #t)))

(define-public crate-webm-sys-native-0.3.3 (c (n "webm-sys-native") (v "0.3.3") (d (list (d (n "gcc") (r "^0.3.42") (d #t) (k 1)))) (h "1cicgmjdy9q107yfj2aj9aacw3jjakfdp6071hx3bsbxvyz7fwy7") (y #t)))

(define-public crate-webm-sys-native-0.3.4 (c (n "webm-sys-native") (v "0.3.4") (d (list (d (n "gcc") (r "^0.3.42") (d #t) (k 1)))) (h "060hhgkwpzngj4fmlvv2cvhpiw8yvydf3wjzgjsd974z2hsj2acp") (y #t)))

(define-public crate-webm-sys-native-0.3.5 (c (n "webm-sys-native") (v "0.3.5") (d (list (d (n "gcc") (r "= 0.3.51") (d #t) (k 1)))) (h "16jq4r60n40cm62crkqx6zhcwmpaxnnrvp59lbm9h11ic8v5y4ri") (y #t)))

(define-public crate-webm-sys-native-0.3.6 (c (n "webm-sys-native") (v "0.3.6") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "05ia5p39q4wjpz9g873dzaik18v6ab8i4dkhbi74i2n4p57hxm2r") (y #t)))

