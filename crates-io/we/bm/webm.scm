(define-module (crates-io we bm webm) #:use-module (crates-io))

(define-public crate-webm-0.1.0 (c (n "webm") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "webm-sys") (r "^0.1") (d #t) (k 0)))) (h "1rm7ijcl3m4fp1l6l54g8kkdij8g4mgsq0hfzx47nd5n1hjmih44") (y #t)))

(define-public crate-webm-0.2.0 (c (n "webm") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "webm-sys") (r "^0.1") (d #t) (k 0)))) (h "1wgzwcwbf3wp82cc2xp4nr8xy9bkm2gkwm9xkkj5lfa6qb8wlq4v") (y #t)))

(define-public crate-webm-0.4.5 (c (n "webm") (v "0.4.5") (d (list (d (n "webm-sys-native") (r "^0.3.6") (d #t) (k 0)))) (h "1kdqr5yc41jb745gqn0qpc66w2ax9yvqamdlx64yjffwnwmjbmcy") (y #t)))

(define-public crate-webm-1.0.0 (c (n "webm") (v "1.0.0") (d (list (d (n "webm-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0dafjpp15cgvw5r3bbsxamh84rwqm1gdsqlvf2whv26vcpp61a22") (y #t)))

(define-public crate-webm-1.0.1 (c (n "webm") (v "1.0.1") (d (list (d (n "webm-sys") (r "^1.0.0") (d #t) (k 0)))) (h "1lqfb1wvn4lsb7ppznfwxjl7sqfl2y98w57rsp9bcsghzdmd9pdk")))

(define-public crate-webm-1.0.2 (c (n "webm") (v "1.0.2") (d (list (d (n "webm-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0dkiazn486yc2y7b8wrhbh06y0w2gv52nc16mgc1zvqji8a4gc7c")))

(define-public crate-webm-1.0.3 (c (n "webm") (v "1.0.3") (d (list (d (n "webm-sys") (r "^1.0.4") (d #t) (k 0)))) (h "1q1n9243skkxwk2ir3a4kc5mpj2wkhnvq8b1z69jb5jw1l0l0sxl")))

(define-public crate-webm-1.1.0 (c (n "webm") (v "1.1.0") (d (list (d (n "webm-sys") (r "^1.0.5") (d #t) (k 0)))) (h "0i1n9qrh90dh1axn8zr5bd2bxw4808xf8gifkfqpbcyzhs2w1l7z")))

(define-public crate-webm-1.1.1 (c (n "webm") (v "1.1.1") (d (list (d (n "webm-sys") (r "^1.0.6") (d #t) (k 0)))) (h "0dh6hvxn73bzbcl4i3n7f70q6wi2xmbryk4zxnl8l2bfzb8myp5p")))

