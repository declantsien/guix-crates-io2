(define-module (crates-io we bm webmark) #:use-module (crates-io))

(define-public crate-webmark-0.1.0 (c (n "webmark") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 0)))) (h "06f17sjzaaqjdyr2iq0qxcr62i9c7mdih03m3s87wldl7igvpax7")))

