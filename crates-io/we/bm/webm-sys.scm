(define-module (crates-io we bm webm-sys) #:use-module (crates-io))

(define-public crate-webm-sys-0.1.0 (c (n "webm-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pnacl-build-helper") (r "^1.4") (d #t) (k 1)))) (h "1gkzkji1kkf3fzbqasd0jmm3nhs0c6rwahngv57fc5jcsw4y7sj0") (y #t)))

(define-public crate-webm-sys-0.1.1 (c (n "webm-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pnacl-build-helper") (r "^1.4") (d #t) (k 1)))) (h "0bzcxf5hl20wyrimxihhmfmlsp3q0gam4jy7m6b1krqbsknh6wjz") (y #t)))

(define-public crate-webm-sys-0.3.6 (c (n "webm-sys") (v "0.3.6") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "11mynn73vl0yimv2xwqxhmfqm7dl4y8ghcq9y9q2wlp2xqx338kn") (y #t)))

(define-public crate-webm-sys-1.0.0 (c (n "webm-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)))) (h "1mrw9zv7j170ip7ws0dx01vdw3py6zikpy885g4xmwhc8vix2wza") (y #t)))

(define-public crate-webm-sys-1.0.1 (c (n "webm-sys") (v "1.0.1") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)))) (h "07crq4j4161m93kv0dcky23q3s2mbc4irm017mxaj7wq2kc4vmmi") (y #t)))

(define-public crate-webm-sys-1.0.2 (c (n "webm-sys") (v "1.0.2") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "0jn63a6wh6nqp90aif5fp07yjd99jvil40iiwa42hi10gfcpmvz2")))

(define-public crate-webm-sys-1.0.3 (c (n "webm-sys") (v "1.0.3") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "1wz7kn57z04fqnm7ph8hv575fff8r9wiaaqbbckgwlfg5k46xv8d") (l "webm")))

(define-public crate-webm-sys-1.0.4 (c (n "webm-sys") (v "1.0.4") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "1v2i6iihggdd4aawprj2485dwirc1hzflxb8jlavvbq47fxd6mms") (l "webm")))

(define-public crate-webm-sys-1.0.5 (c (n "webm-sys") (v "1.0.5") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "1m0m6qb6qa5p1482jqiyzzcv4cq9p0b0xf29ncirq45fs1wszfp2") (l "webm")))

(define-public crate-webm-sys-1.0.6 (c (n "webm-sys") (v "1.0.6") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "06wq386lx9agd6p0pxdw975fzkzrpipc9vj8zxcxz0hlrdzazy1z") (l "webm")))

