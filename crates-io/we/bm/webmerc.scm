(define-module (crates-io we bm webmerc) #:use-module (crates-io))

(define-public crate-webmerc-0.1.0 (c (n "webmerc") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)))) (h "04nx34xi7wngkp35ja0vww7dbm86ggz24nr6g3jfahjv4p4nx0f6")))

(define-public crate-webmerc-0.1.1 (c (n "webmerc") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)))) (h "1pmq9rq3y9znxp5lqjl1vkmqf6pfdrcmqrygwldl64ffgpq36wnj")))

