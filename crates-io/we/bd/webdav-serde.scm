(define-module (crates-io we bd webdav-serde) #:use-module (crates-io))

(define-public crate-webdav-serde-0.1.0 (c (n "webdav-serde") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0ln5v9ldn2m7jn9sxswab6iqcirmld3pbl3f7jbd9wvd2dv3qmac")))

(define-public crate-webdav-serde-0.1.1 (c (n "webdav-serde") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "001r2a9rjbp0g11yalbx4p3n1k5fbqskcwwcas4bij1240c1kfn2")))

