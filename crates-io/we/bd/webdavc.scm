(define-module (crates-io we bd webdavc) #:use-module (crates-io))

(define-public crate-webdavc-0.1.0 (c (n "webdavc") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "07xzk0fsjxxnl13zbwicplcawa0mh406vvrrh2w51rwzqmqq6qns")))

(define-public crate-webdavc-0.1.1 (c (n "webdavc") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0nxziakksrk15rmqxmbd0wfdj5w2w544d9187h4apsl7jh1klbkb")))

