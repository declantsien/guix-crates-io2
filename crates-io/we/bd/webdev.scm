(define-module (crates-io we bd webdev) #:use-module (crates-io))

(define-public crate-webdev-0.1.0 (c (n "webdev") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^5") (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0ny7bb2v55mddld2spk3pdppv704jj9i19qdp81lksnrm9bp7v6m")))

(define-public crate-webdev-0.1.1 (c (n "webdev") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^5") (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "00xs2w71bi9bgm2av0fdkd8a5jvz2hndanv1k5x0q528dcgg8gzg")))

