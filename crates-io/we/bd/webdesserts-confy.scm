(define-module (crates-io we bd webdesserts-confy) #:use-module (crates-io))

(define-public crate-webdesserts-confy-1.0.0 (c (n "webdesserts-confy") (v "1.0.0") (d (list (d (n "directories") (r "^0.10.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "06v1b5gsxf8bkrkf8q21maa0qvwz5bn0lgmi2vll6blnhq4l2p3j") (f (quote (("sd" "serde_derive"))))))

