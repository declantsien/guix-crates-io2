(define-module (crates-io we bd webdav-xml) #:use-module (crates-io))

(define-public crate-webdav-xml-0.1.0 (c (n "webdav-xml") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "bytestring") (r "^1.3.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 2)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "httpdate") (r "^1.0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)) (d (n "nonempty") (r "^0.9.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("parsing" "formatting"))) (d #t) (k 0)))) (h "1ld4a03macqy0h6wrf4aa1wmhjagfgvdyhjzvzpy7igj93pqncpi")))

