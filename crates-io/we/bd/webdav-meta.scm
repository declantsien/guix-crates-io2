(define-module (crates-io we bd webdav-meta) #:use-module (crates-io))

(define-public crate-webdav-meta-0.1.0 (c (n "webdav-meta") (v "0.1.0") (d (list (d (n "webdav-headers") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "webdav-methods") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "webdav-xml") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0janym6g3x4x8ca2qjx6shxr6dihclgw13h3d3jnvfqna5az73f5") (f (quote (("default" "headers" "methods" "xml")))) (s 2) (e (quote (("xml" "dep:webdav-xml") ("methods" "dep:webdav-methods") ("headers" "dep:webdav-headers"))))))

