(define-module (crates-io we bd webdav-headers) #:use-module (crates-io))

(define-public crate-webdav-headers-0.1.0 (c (n "webdav-headers") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.12") (d #t) (k 2)) (d (n "headers") (r "^0.4.0") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nonempty") (r "^0.9.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "uniresid") (r "^0.1.5") (k 0)))) (h "16a5gqyi2kzwf0nddcnbdi7pdmqbark66cbaxp42iagyy6w9bmws")))

