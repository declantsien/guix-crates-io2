(define-module (crates-io we av weaver_protos_rs) #:use-module (crates-io))

(define-public crate-weaver_protos_rs-0.0.1 (c (n "weaver_protos_rs") (v "0.0.1") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "fs"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "14x653zhdvm6377whr174ss6zncpjklyadf7ayr73grbqbnd0x45")))

(define-public crate-weaver_protos_rs-0.0.2 (c (n "weaver_protos_rs") (v "0.0.2") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "fs"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "18qhb6ga8hbq6fkz9vxj5lm8c2bmlawmkiy4vd7i527cb5vkpmgm")))

(define-public crate-weaver_protos_rs-1.5.4 (c (n "weaver_protos_rs") (v "1.5.4") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "fs"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "12xn11gs4ljgj5zl20hvsqmw4c4naj3k9syz2lvbpd5c4a0w8xhz")))

(define-public crate-weaver_protos_rs-1.5.7 (c (n "weaver_protos_rs") (v "1.5.7") (d (list (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (f (quote ("tls"))) (d #t) (k 0)))) (h "0b6f2bxihyszf0s9nr3s4kw7nfblixxqd3cxs373bfg0b7vprvrb")))

