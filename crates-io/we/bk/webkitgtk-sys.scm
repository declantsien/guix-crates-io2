(define-module (crates-io we bk webkitgtk-sys) #:use-module (crates-io))

(define-public crate-webkitgtk-sys-0.2.0 (c (n "webkitgtk-sys") (v "0.2.0") (d (list (d (n "atk-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.3.4") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "gdk-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "gio-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pango-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0h1bq4ygq41l5964hd8bqn8j9x1zsxx6008ivfn8jm8wh00h22vx") (y #t)))

