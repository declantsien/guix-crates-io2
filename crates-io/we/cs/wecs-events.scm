(define-module (crates-io we cs wecs-events) #:use-module (crates-io))

(define-public crate-wecs-events-0.1.0 (c (n "wecs-events") (v "0.1.0") (d (list (d (n "wecs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wecs-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0ng53d9wj6zsdp9kwyrnqx8b4kf2zq9x83xrykgvd950p8szqmvk") (y #t)))

(define-public crate-wecs-events-0.1.2 (c (n "wecs-events") (v "0.1.2") (d (list (d (n "wecs-core") (r "^0.1.2") (d #t) (k 0)) (d (n "wecs-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0v0jnbaq0qvdsh341lzdyrg8r57g7xrbjm5zsads5pz20vcn14ah")))

