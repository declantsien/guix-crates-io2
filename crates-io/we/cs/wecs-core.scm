(define-module (crates-io we cs wecs-core) #:use-module (crates-io))

(define-public crate-wecs-core-0.1.0 (c (n "wecs-core") (v "0.1.0") (d (list (d (n "wecs-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1k5z1akz0z0l5q52ml6gpdhbfj19k9ri4hcs15ghhbsg7zrpb3l4") (y #t)))

(define-public crate-wecs-core-0.1.1 (c (n "wecs-core") (v "0.1.1") (d (list (d (n "wecs-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1blds1vi84rsp4prsa68wn1bz6k6f5070hs8vgcycfca4f9kyfnz") (y #t)))

(define-public crate-wecs-core-0.1.2 (c (n "wecs-core") (v "0.1.2") (h "0dr8b92m4kmxpz6z9j4dwf37ajj9451ycliwcgig6qyqzlxj7hyk")))

