(define-module (crates-io we cs wecs-derive) #:use-module (crates-io))

(define-public crate-wecs-derive-0.1.0 (c (n "wecs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "17qmsga39vvsjwsvgcrmw6bkq364ix88afda5fsdmw5q7gpybk82") (y #t)))

(define-public crate-wecs-derive-0.1.1 (c (n "wecs-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1pzh34gx8ihfxd4d28hhymc0dfxcrs24ff6g9lv6rhhc2fbf9738") (y #t)))

(define-public crate-wecs-derive-0.1.2 (c (n "wecs-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "06kgf08n3mi60mhmp6bazyv1p39jnzbjfxmsgnndyia3pqqqhkgy")))

