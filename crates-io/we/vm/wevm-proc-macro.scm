(define-module (crates-io we vm wevm-proc-macro) #:use-module (crates-io))

(define-public crate-wevm-proc-macro-0.1.0 (c (n "wevm-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08v3cr7vyglsk5nfraqynh6m44gg5hhgdy4fyyx7q8390ih7j9qx")))

