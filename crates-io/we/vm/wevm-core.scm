(define-module (crates-io we vm wevm-core) #:use-module (crates-io))

(define-public crate-wevm-core-0.3.0 (c (n "wevm-core") (v "0.3.0") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "jni") (r "^0.21.0") (d #t) (k 0)) (d (n "jni") (r "^0.21.0") (f (quote ("invocation"))) (d #t) (k 2)) (d (n "wasmi") (r "^0.31.2") (d #t) (k 0)) (d (n "wat") (r "^1") (d #t) (k 2)) (d (n "wevm-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1aqipq55mysz6lslmq4b7wphpnim5q4g8w8gyhn5x3y6sndip6yx") (f (quote (("jvm") ("default" "jvm") ("bindings"))))))

(define-public crate-wevm-core-0.3.1 (c (n "wevm-core") (v "0.3.1") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "jni") (r "^0.21.0") (d #t) (k 0)) (d (n "jni") (r "^0.21.0") (f (quote ("invocation"))) (d #t) (k 2)) (d (n "wasmi") (r "^0.31.2") (d #t) (k 0)) (d (n "wat") (r "^1") (d #t) (k 2)) (d (n "wevm-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0g46122i070dw451a6d98b40g7ac29amss9rmz6v5zwmkgdmcbqz") (f (quote (("jvm") ("default" "jvm") ("bindings"))))))

(define-public crate-wevm-core-0.3.2 (c (n "wevm-core") (v "0.3.2") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "jni") (r "^0.21.0") (d #t) (k 0)) (d (n "jni") (r "^0.21.0") (f (quote ("invocation"))) (d #t) (k 2)) (d (n "wasmi") (r "^0.31.2") (d #t) (k 0)) (d (n "wat") (r "^1") (d #t) (k 2)) (d (n "wevm-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0axb1ldfzshfxqlllrkrk0pn5d29hb3nairkq0zf1nzb4dvcy7rs") (f (quote (("jvm") ("default" "jvm") ("bindings"))))))

