(define-module (crates-io we rd werds) #:use-module (crates-io))

(define-public crate-werds-1.0.0 (c (n "werds") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "0g9bs8jvkd2fpj0fp8d8ywjdsyqndfbmgmmq36185xhar61ilznc")))

