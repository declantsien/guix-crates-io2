(define-module (crates-io we bo webots-bindings) #:use-module (crates-io))

(define-public crate-webots-bindings-0.1.0 (c (n "webots-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0avgaby48ih98dy51fvcfbnscpiqh0sbsyhyxxkqw87hyy5x936a") (y #t)))

(define-public crate-webots-bindings-0.1.1 (c (n "webots-bindings") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 1)))) (h "058wmasy7plagnjvwz2q1dkcq3zqlazjqxy1222q4jjnr3nj99zp") (y #t)))

(define-public crate-webots-bindings-0.1.2 (c (n "webots-bindings") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0wlx72qnz3pjyhl2sxcz8qvxrafgifg5jcvr0vlyjmh8a2rgiiw6")))

(define-public crate-webots-bindings-0.2.0 (c (n "webots-bindings") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1smpy7cqlx264v8d308pv894y99rd0733x0pqsziggzxyvsdwiyr")))

(define-public crate-webots-bindings-0.3.0 (c (n "webots-bindings") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "172v10vcwg8zkxn0dwcrwjci1xq90wyxl96bsls44dwcw9zynyj1")))

(define-public crate-webots-bindings-0.5.0 (c (n "webots-bindings") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1ccajiawg1ff6savngylzlm2xdyzl7f3vr5ia19gc8p1v2dhaffi")))

(define-public crate-webots-bindings-0.6.0 (c (n "webots-bindings") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0plkz23ngsjiwgjhgrgba49nxyj2nzych9zn2g9h62yvgn0plhh9")))

(define-public crate-webots-bindings-0.7.0 (c (n "webots-bindings") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0czjp18r8z0y73b5z2brpjmg5zj3fzihgg8qzzss1fki21gwxfbv")))

(define-public crate-webots-bindings-0.8.0 (c (n "webots-bindings") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "12swgmcpdr2crkfnacik9iki8d3k5ckiscvh91c991g1la5lp8cd")))

