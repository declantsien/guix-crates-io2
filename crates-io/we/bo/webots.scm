(define-module (crates-io we bo webots) #:use-module (crates-io))

(define-public crate-webots-0.1.0 (c (n "webots") (v "0.1.0") (d (list (d (n "webots-bindings") (r "^0.1.0") (d #t) (k 0)))) (h "0z8xkiid5qx4s79m72jaax2mrwq3ah33w307ikqs1mbq1nxdgh9r") (y #t)))

(define-public crate-webots-0.1.1 (c (n "webots") (v "0.1.1") (d (list (d (n "webots-bindings") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "17mfrpvnkp7p1y5p60455rhl3ix93i2gp6kgfrpy7annps5q5d9v") (f (quote (("default" "webots-bindings")))) (y #t)))

(define-public crate-webots-0.1.2 (c (n "webots") (v "0.1.2") (d (list (d (n "webots-bindings") (r "^0.1.1") (d #t) (k 0)))) (h "117zj2wi135yjshm2yd00w41s8s25dvc2cx7l4kfcwdmfmvs2g2y") (y #t)))

(define-public crate-webots-0.1.3 (c (n "webots") (v "0.1.3") (d (list (d (n "webots-bindings") (r "^0.1.2") (d #t) (k 0)))) (h "0k2hz59qiraw75h8linvdnmdlgm231jm91a5jv3mh2qnk5f01485")))

(define-public crate-webots-0.2.0 (c (n "webots") (v "0.2.0") (d (list (d (n "webots-bindings") (r "^0.2.0") (d #t) (k 0)))) (h "1igrp3gcqhkxqmw6iy4dcw5rbhd6qjsafvmgx5n9ky2gpwhy23ay")))

(define-public crate-webots-0.3.0 (c (n "webots") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "webots-bindings") (r "^0.2.0") (d #t) (k 0)))) (h "1yj2dasvnhdkm33931dgxrpsb1113z7w5hj4z537vfj439glr2ri")))

(define-public crate-webots-0.4.0 (c (n "webots") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "webots-bindings") (r "^0.3.0") (d #t) (k 0)))) (h "06aidrv0yv7p2vh2r09rfrbkayfyv8n2bhlb22j7gxkra7rp23yj")))

(define-public crate-webots-0.5.0 (c (n "webots") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "webots-bindings") (r "^0.5.0") (d #t) (k 0)))) (h "0l6a2iszwhfghvw0mpjd2x5xlsf4z9svnsfrg2f2dmfrkmp90hs3")))

(define-public crate-webots-0.6.0 (c (n "webots") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "webots-bindings") (r "^0.6.0") (d #t) (k 0)))) (h "0860sny2d42ng3r65wc9dxxqsbjx3caa0xck7jmsb22mc9z7gz99")))

(define-public crate-webots-0.7.0 (c (n "webots") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "webots-bindings") (r "^0.7.0") (d #t) (k 0)))) (h "1a7c2r2gb6v6kdpd1npzk4qc4hmm9ansxcw74cmssf2jks6410rh")))

(define-public crate-webots-0.8.0 (c (n "webots") (v "0.8.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "webots-bindings") (r "^0.8.0") (d #t) (k 0)))) (h "1ix1p7km2v9c3ddyd72v2fiaxbzsln5lwdfnisxw3g83gf5a6yzn")))

