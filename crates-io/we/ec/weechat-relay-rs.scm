(define-module (crates-io we ec weechat-relay-rs) #:use-module (crates-io))

(define-public crate-weechat-relay-rs-0.1.0 (c (n "weechat-relay-rs") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1q4qvxm7ifg0mgh5hz5l7lnx43291gvv8lwlhj0319h6f24a5jcp") (f (quote (("cli" "atty" "clap"))))))

