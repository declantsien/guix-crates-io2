(define-module (crates-io we ec weechat-macro) #:use-module (crates-io))

(define-public crate-weechat-macro-0.2.0 (c (n "weechat-macro") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (d #t) (k 0)))) (h "1i5f5hjzzgnpnj4ybpyzd04yq3h8h48znk08xsiqkcvk09q8hqqc")))

(define-public crate-weechat-macro-0.3.0 (c (n "weechat-macro") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (d #t) (k 0)))) (h "05w4rsxav5qvq2jgi1kcx5klqajjgl8gbkyl3ma537xg28vqaw7p")))

(define-public crate-weechat-macro-0.3.1 (c (n "weechat-macro") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0ikz75hmsmvib2yl0p2zx5fwh349br9zjpfxzzgy1r87x16b8fd9")))

(define-public crate-weechat-macro-0.4.0 (c (n "weechat-macro") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.43") (d #t) (k 0)))) (h "1yq14aav2z6xis8dzzfcggwkyipqdjcal4kj4vxi4bm6a2w4b81m")))

