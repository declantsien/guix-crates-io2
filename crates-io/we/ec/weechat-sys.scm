(define-module (crates-io we ec weechat-sys) #:use-module (crates-io))

(define-public crate-weechat-sys-0.1.0 (c (n "weechat-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1fnbx3vh18qr3kiv1dfrca5fz4m0y7y034fr93v7gb0wzm4f0b3f")))

(define-public crate-weechat-sys-0.2.0 (c (n "weechat-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "0s0wp5i6qswi61jc8znjbwrz4hih930zzx08hv5p1p65mwx8vz1j")))

(define-public crate-weechat-sys-0.3.0 (c (n "weechat-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "15w707q8bb4khclr3c8w5kr1kjcihv3g63cd29br9h79agfzg6fq")))

(define-public crate-weechat-sys-0.3.1 (c (n "weechat-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)))) (h "0cxi9pxic49l1q4zqq2990dvyxfimdg7slnzm4prdjki7g39wba3")))

(define-public crate-weechat-sys-0.4.0 (c (n "weechat-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)))) (h "1935zd1x7i6ps2kigd3c8yp0gfdz0nbfdn0hjy3gasjrqni45vja")))

