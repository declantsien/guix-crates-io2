(define-module (crates-io we nd wendy-k8-metadata-client) #:use-module (crates-io))

(define-public crate-wendy-k8-metadata-client-3.3.1 (c (n "wendy-k8-metadata-client") (v "3.3.1") (d (list (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "k8-diff") (r "^0.1.0") (d #t) (k 0)) (d (n "k8-types") (r "^0.5.2") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)))) (h "02rk1kbim9jfcxb5c9zv7blq6cc4h95rrikjgi83bwras60hnfai")))

