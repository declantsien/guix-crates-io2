(define-module (crates-io we co wecom-agent) #:use-module (crates-io))

(define-public crate-wecom-agent-0.1.0 (c (n "wecom-agent") (v "0.1.0") (h "1jddlx6y94vgy0l6sqi6i70hmd0n38v8mrlf5whimyj131vr3bls")))

(define-public crate-wecom-agent-0.1.1 (c (n "wecom-agent") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "16n7rlzx2p3z3l5n4ms0k4fihqrp9361sgzy0ga2x66lr67rlviz")))

(define-public crate-wecom-agent-0.1.2 (c (n "wecom-agent") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j4dy9xmnlx2qclhq6mv0g1n16f63xv27km75p8pr2glihbq3jqs")))

(define-public crate-wecom-agent-0.1.3 (c (n "wecom-agent") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f2b3626vxhjf90q8iiqayy4rww8fbg4dys9swvzhqknwswns5sq")))

(define-public crate-wecom-agent-0.1.4 (c (n "wecom-agent") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mss6mqqd6ypcg9ybyk6049n1cfyhckjxs552nmdz5z1qkqg3z6b")))

(define-public crate-wecom-agent-0.1.5 (c (n "wecom-agent") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cnmrmp18lp1mw2q511w0l48m8wbajh87722ypmwm7d4qh6jgbml")))

(define-public crate-wecom-agent-0.1.6 (c (n "wecom-agent") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0swyf6d7m6kryx9ck2m3a2iqr1ly60nv7qqvwdscryrdg6gci2wm")))

(define-public crate-wecom-agent-0.1.7 (c (n "wecom-agent") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ghyjjpil4qh7xf3dzgf4qmd412gm1b26yil6dyn8cmihh2g432s")))

(define-public crate-wecom-agent-0.1.8 (c (n "wecom-agent") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0p902nl0wnq627khs72sj6i13aj1g1kp8mdkc8mrv1aaq1g255b1")))

(define-public crate-wecom-agent-0.1.9 (c (n "wecom-agent") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1n7a6vagx8m7j47ln6lin8qgyyhdiw048dz7p62yyf7znsifczl2")))

(define-public crate-wecom-agent-0.1.10 (c (n "wecom-agent") (v "0.1.10") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0k1qqis3x8ncyfbacwy85h5zk7yq9l3wwgr6h09zacr7cfn1j0yj")))

(define-public crate-wecom-agent-0.1.11 (c (n "wecom-agent") (v "0.1.11") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pvk74f2cfwlk2qd37jxj48m49wq7xkh0r6dajrdda89kxc30x52")))

(define-public crate-wecom-agent-0.1.12 (c (n "wecom-agent") (v "0.1.12") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "061g4k1kd42605jz4ypgc6m4jbqmbwlhdbhf4f3m20l691imlqzx")))

(define-public crate-wecom-agent-0.1.13 (c (n "wecom-agent") (v "0.1.13") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gkw4wmff4byycp0wv6vnxzam8d4fbciv5h5xv70g61561vlgmmr")))

(define-public crate-wecom-agent-0.1.14 (c (n "wecom-agent") (v "0.1.14") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0i14hqa2yl33467br2r0gwnsdaspild5x7v9sm3ijdbn7qcf7zdb")))

(define-public crate-wecom-agent-0.1.15 (c (n "wecom-agent") (v "0.1.15") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hw0q2rv9pwp24k554pydqlca43n7p52zp91725f00gf8fwy25px")))

(define-public crate-wecom-agent-0.1.16 (c (n "wecom-agent") (v "0.1.16") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ha5d34248bv3i9jv8dyiwrkq7yi9kkr8l9r2x4qyksz985y491p")))

