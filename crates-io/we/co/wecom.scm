(define-module (crates-io we co wecom) #:use-module (crates-io))

(define-public crate-wecom-0.1.0 (c (n "wecom") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12gl08900y2dxlmq4y1rp8czib3kqwqnlsd1p8w3c6kg4g0q0cnf")))

