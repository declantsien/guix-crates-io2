(define-module (crates-io we -c we-cdk) #:use-module (crates-io))

(define-public crate-we-cdk-0.3.0 (c (n "we-cdk") (v "0.3.0") (d (list (d (n "we-contract-proc-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "wevm-core") (r "^0.3.2") (f (quote ("bindings"))) (k 0)))) (h "167dykjg8sjv687lnz5g8m4chwy7si8153sa804h40w2049g9x3s")))

(define-public crate-we-cdk-0.3.1 (c (n "we-cdk") (v "0.3.1") (d (list (d (n "we-contract-proc-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "wevm-core") (r "^0.3.2") (f (quote ("bindings"))) (k 0)))) (h "0acbx62z0zc2i7rpncsg8vmg4fvyxl2qchj6qm7lls3831y27q5k")))

(define-public crate-we-cdk-0.3.2 (c (n "we-cdk") (v "0.3.2") (d (list (d (n "we-contract-proc-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "wevm-core") (r "^0.3.2") (f (quote ("bindings"))) (k 0)))) (h "07j2ngak5h9dpqjffaij8m87svza3v8lwjzcdmsamc5hwza3fzl3")))

