(define-module (crates-io we -c we-contract-proc-macro) #:use-module (crates-io))

(define-public crate-we-contract-proc-macro-0.3.0 (c (n "we-contract-proc-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06if0slwb4pqsh91fd3s2qfsl4fizxkn3iv6l1pl7y0g19621c2r")))

(define-public crate-we-contract-proc-macro-0.3.1 (c (n "we-contract-proc-macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "043jfk2f0ylnr7n2kgriijnlapdlznqkni4yb1chgifpzgf2wmfj")))

