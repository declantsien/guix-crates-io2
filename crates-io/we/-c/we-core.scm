(define-module (crates-io we -c we-core) #:use-module (crates-io))

(define-public crate-we-core-0.1.0 (c (n "we-core") (v "0.1.0") (h "133y4az0f314vg85gicvc5g92w5av1k6ra4iygrxs1blbpqpsvn5") (y #t)))

(define-public crate-we-core-0.1.1 (c (n "we-core") (v "0.1.1") (h "129ckrbs31fb48pxq6fi6n1j243b686d3lgy7i7pinqyfvrq51j3") (y #t)))

(define-public crate-we-core-0.1.2 (c (n "we-core") (v "0.1.2") (h "11h2n77pddbf2svs7h8m0whan2fxig6mrxfj3cms8djddqhnx5yp") (y #t)))

