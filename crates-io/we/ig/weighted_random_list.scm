(define-module (crates-io we ig weighted_random_list) #:use-module (crates-io))

(define-public crate-weighted_random_list-0.1.0 (c (n "weighted_random_list") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0l0dpkxgyf3xgki5raj25xni1y0npr01fymksdahz30pmf8grm89")))

(define-public crate-weighted_random_list-0.1.1 (c (n "weighted_random_list") (v "0.1.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1wzg2h1hp2y41bcnc1zx3kfh9hcwvz7v1nmwvpcgbmi8dg9gsprr")))

