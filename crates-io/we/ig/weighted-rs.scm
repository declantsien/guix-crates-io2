(define-module (crates-io we ig weighted-rs) #:use-module (crates-io))

(define-public crate-weighted-rs-0.1.0 (c (n "weighted-rs") (v "0.1.0") (h "0g056q37gnbagzjdqqxl5rbcml3hff0d6186fim9hwnysxvzhqyn")))

(define-public crate-weighted-rs-0.1.1 (c (n "weighted-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0f11xg3qm3vfrvvh6xg8nw19cf78vhs744hiap01h0jp7n2f61n6")))

(define-public crate-weighted-rs-0.1.2 (c (n "weighted-rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1920xpniyix8vnjm79m6rbq0zr614w1acah0h85yzqmbj9cfvvcr")))

(define-public crate-weighted-rs-0.1.3 (c (n "weighted-rs") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "08qcqclxpmz1h7x28vcnxc23k14yxhjwqpwxzppxwv6qp1r2hnrn")))

