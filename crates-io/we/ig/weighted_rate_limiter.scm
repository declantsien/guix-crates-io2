(define-module (crates-io we ig weighted_rate_limiter) #:use-module (crates-io))

(define-public crate-weighted_rate_limiter-0.1.0 (c (n "weighted_rate_limiter") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "queues") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full" "macros" "test-util"))) (d #t) (k 2)))) (h "0ln8blm8mbrpm3z9lcw2idlv69dylb1cw0r3l22a2s7vl6m6fijq")))

(define-public crate-weighted_rate_limiter-0.1.1 (c (n "weighted_rate_limiter") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "queues") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full" "macros" "test-util"))) (d #t) (k 2)))) (h "13qi9lgc0bxmbw9wpj46k455afvmy7snd0vmxqnsv9im6w9f28yn")))

