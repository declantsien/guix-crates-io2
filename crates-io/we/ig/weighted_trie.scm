(define-module (crates-io we ig weighted_trie) #:use-module (crates-io))

(define-public crate-weighted_trie-0.1.0 (c (n "weighted_trie") (v "0.1.0") (h "0y78m73sv2y4kyp5xgf9jppgglqkm13g7knr5hbd8920f35r02xb")))

(define-public crate-weighted_trie-0.1.1 (c (n "weighted_trie") (v "0.1.1") (h "0cfjz9421l9rclgpix21j2z2ixjz55dnd5yxrn43nznb3l2q6iqb")))

(define-public crate-weighted_trie-0.1.2 (c (n "weighted_trie") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0zn5pf0fy2sv6j4b9ifhvd9rl5fwgg544kkpw8li0175ls8hbv4k")))

(define-public crate-weighted_trie-0.1.3 (c (n "weighted_trie") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "09m75qxqmw7gdcqaqdp58caiahrb77f8csx6g4mj662rj2vlvs89")))

(define-public crate-weighted_trie-0.1.4 (c (n "weighted_trie") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "030w5fkpn3rj7l32knc0vdjj8fcngr95cpw7z3lbxqdm33c2brk6")))

