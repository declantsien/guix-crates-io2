(define-module (crates-io we ig weighted-median) #:use-module (crates-io))

(define-public crate-weighted-median-0.1.0 (c (n "weighted-median") (v "0.1.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)))) (h "19kbbrxmrm3svh5088gp9rm7cp3f88jhzb9vqybvk1fpgjkd3wgq")))

(define-public crate-weighted-median-0.2.0 (c (n "weighted-median") (v "0.2.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)))) (h "10kf72l0hpilmwlp2879qzjy7l1vr1fsi2w6b0kqa3ixf8800a5n")))

(define-public crate-weighted-median-0.3.0 (c (n "weighted-median") (v "0.3.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)))) (h "0k3yc40b7308x9w30yrckfwc6mb6zn4zvvsn9q3378vd3kh10mrd")))

(define-public crate-weighted-median-0.4.0 (c (n "weighted-median") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1gim7yw8q27v3yf100518qd86w463bp8wwap162b1nnm3jflbv5v")))

(define-public crate-weighted-median-0.5.0 (c (n "weighted-median") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0w63wv5ykyaapkgxkwikywxkag3ajvip80a6fybnh16c3l6zvqv3")))

(define-public crate-weighted-median-0.6.0 (c (n "weighted-median") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0rdfj2c337b3b1v42icv5jxssadbn5rwagv3vbf3y3v57gqyi93q")))

(define-public crate-weighted-median-0.7.0 (c (n "weighted-median") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0mgp0491423wcsibxjij2ya95mc0pwin5sgria5bmb7q0dyy410j")))

