(define-module (crates-io we ig weighted-select) #:use-module (crates-io))

(define-public crate-weighted-select-0.1.0 (c (n "weighted-select") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "0i2alhwkyqf1pby5qjh04rx7fms0dnmwns86jwhp7vhmic763jsk")))

(define-public crate-weighted-select-0.1.1 (c (n "weighted-select") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)))) (h "0rf2pf9g98xfjr0bnivr0lijwa8r77g5rcpk9ad2cj2mic1k6phl")))

