(define-module (crates-io we ig weighted-rs-wasm) #:use-module (crates-io))

(define-public crate-weighted-rs-wasm-0.1.3 (c (n "weighted-rs-wasm") (v "0.1.3") (h "16vc4j6k9in2chkcpdnq9rjclb7xca0mdgddsfw2czyzkqcy3p6q")))

(define-public crate-weighted-rs-wasm-0.1.4 (c (n "weighted-rs-wasm") (v "0.1.4") (h "1w6md142i1lin1g0zv5qsrirwz83xdib64kfmvsllv91zphsdvbz")))

