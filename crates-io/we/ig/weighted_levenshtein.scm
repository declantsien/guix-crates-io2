(define-module (crates-io we ig weighted_levenshtein) #:use-module (crates-io))

(define-public crate-weighted_levenshtein-0.1.0 (c (n "weighted_levenshtein") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 2)))) (h "1ldf4330yym2hpmx8c4zggllz53ppdw1k30giii29bg6z2chkc0d") (f (quote (("default-weight"))))))

(define-public crate-weighted_levenshtein-0.2.0 (c (n "weighted_levenshtein") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 2)))) (h "16zdrc2cczfyhfwik0b7px6nd66ia61rnxdp0mjpv7cm86y06c46") (f (quote (("default-weight"))))))

