(define-module (crates-io we ig weighty) #:use-module (crates-io))

(define-public crate-weighty-0.1.0 (c (n "weighty") (v "0.1.0") (d (list (d (n "hidapi") (r "^1.2.3") (d #t) (k 0)) (d (n "uom") (r "^0") (o #t) (d #t) (k 0)))) (h "0xx8bbx1f3hsx531ngax7krrbris0n5k6sxj3ibn7a10p4c40al2") (f (quote (("units" "uom") ("default"))))))

(define-public crate-weighty-0.1.1 (c (n "weighty") (v "0.1.1") (d (list (d (n "hidapi") (r "^1.2.3") (d #t) (k 0)) (d (n "uom") (r "^0") (o #t) (d #t) (k 0)))) (h "0pjkka9cb02lg53c78d30xm7z2y365fp8wb5wsmjlnblf77xnj1d") (f (quote (("units" "uom") ("default"))))))

