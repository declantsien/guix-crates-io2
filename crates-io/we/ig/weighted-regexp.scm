(define-module (crates-io we ig weighted-regexp) #:use-module (crates-io))

(define-public crate-weighted-regexp-0.1.0 (c (n "weighted-regexp") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)))) (h "1dacbswzmi47v06j7g3dl23pmzvgq5bd9pj03h1mg2aarahgcy2j")))

