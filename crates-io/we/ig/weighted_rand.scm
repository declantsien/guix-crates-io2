(define-module (crates-io we ig weighted_rand) #:use-module (crates-io))

(define-public crate-weighted_rand-0.1.0 (c (n "weighted_rand") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nbl51ydwbjyqhmb3qym37rwc6mfprgapqynfbi1j9j5x6nn2wk2")))

(define-public crate-weighted_rand-0.2.0 (c (n "weighted_rand") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04bvakpn3zz8cka29fc8qzfag8k1mdfcw56gy695xp9axw2xrkvv")))

(define-public crate-weighted_rand-0.3.0 (c (n "weighted_rand") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dcd1gnyc4y02kq07l66ykhf2h1pb3596z5f3yim7zy2yqpxg8qg")))

(define-public crate-weighted_rand-0.3.1 (c (n "weighted_rand") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1srq9n3fb8jna2sj93ycix08md227d8v81lwslx2gy68r1cb1wfb")))

(define-public crate-weighted_rand-0.3.2 (c (n "weighted_rand") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gpga6wqy1ra1s8y191lbm8wcjv3m85bql3l1q03f2wkyjxqz8qz")))

(define-public crate-weighted_rand-0.4.0 (c (n "weighted_rand") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1acpyz28ii65b20sf3vb3h0xr4slvr8zzy3f5i980bysrypdabhc") (y #t)))

(define-public crate-weighted_rand-0.4.1 (c (n "weighted_rand") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dz73vgdxpshsa67l7cr7dpwwpdy8xmcinf1pj6vfd4gpdn4w6j3")))

(define-public crate-weighted_rand-0.4.2 (c (n "weighted_rand") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vmiy0g5q2a1n834apr8xyrgp92ad8hn7i8yy19gf3j7m2rbdfca")))

