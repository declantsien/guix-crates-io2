(define-module (crates-io we ep weepingtown) #:use-module (crates-io))

(define-public crate-weepingtown-0.1.0 (c (n "weepingtown") (v "0.1.0") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "volmark") (r "^0.1.0") (d #t) (k 0)))) (h "1awkf9705723nxdhys8llymx75vl2wch86g4x7lxy4bhw8mb2shj")))

