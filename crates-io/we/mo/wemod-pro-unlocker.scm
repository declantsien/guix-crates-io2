(define-module (crates-io we mo wemod-pro-unlocker) #:use-module (crates-io))

(define-public crate-wemod-pro-unlocker-0.1.0 (c (n "wemod-pro-unlocker") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1vyax3d9yikmri63d8djd8f1crz3np1fhvnr9y7rp8kak9w2k3qx") (y #t)))

(define-public crate-wemod-pro-unlocker-0.1.1 (c (n "wemod-pro-unlocker") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "10w7gh7zzg1qzmvivbaxcw81gh496wsdbq71xbybx14ilf34ckmy") (y #t)))

(define-public crate-wemod-pro-unlocker-0.1.2 (c (n "wemod-pro-unlocker") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "0a03ffpv6jydjl6rpms9cai8gwzgz7kgwzsyamkiwzrjsjd95m2l") (y #t)))

(define-public crate-wemod-pro-unlocker-0.1.3 (c (n "wemod-pro-unlocker") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "0p8hw0j858lgdm9c6zwr8k70sx5rwryy15wh1b3n2qy4vyvf3fj1") (y #t)))

(define-public crate-wemod-pro-unlocker-0.2.0 (c (n "wemod-pro-unlocker") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "0lv0c4pj099kdyc3pdc9nbnpwcvqwra5g80vc376i8sp45b58n7l") (y #t)))

(define-public crate-wemod-pro-unlocker-0.2.1 (c (n "wemod-pro-unlocker") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "02fnqqxj7vbl1cgafrv6s2v6i31j91x85vkqwj0xipq91l3mwgvb") (y #t)))

(define-public crate-wemod-pro-unlocker-0.3.0 (c (n "wemod-pro-unlocker") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "02l4jcl1lgx4npllikidpc88ffyzsmayj8a2g6sa57cxhj9slfg2") (y #t)))

(define-public crate-wemod-pro-unlocker-0.3.1 (c (n "wemod-pro-unlocker") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "0k8fpbbpd7pijw25zxsjzg2d1lkp8lsll6685mn2af7jkg4xxavy") (y #t)))

(define-public crate-wemod-pro-unlocker-0.4.0 (c (n "wemod-pro-unlocker") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "0455i6bxhx1darqa993jwjz29y2c41hmlnicy9961qzm8fwxz3kh") (y #t)))

(define-public crate-wemod-pro-unlocker-0.5.1 (c (n "wemod-pro-unlocker") (v "0.5.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1pz46yb7vi41xdax7hjiwry4lg550lz5aq0xhbgifcfncvxcldi0") (y #t)))

(define-public crate-wemod-pro-unlocker-0.6.0 (c (n "wemod-pro-unlocker") (v "0.6.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1qmi88ln4x9jp7a12yb0lflw3yl3rl51ha5sd5fmi557g6fgky87") (y #t)))

(define-public crate-wemod-pro-unlocker-0.6.1 (c (n "wemod-pro-unlocker") (v "0.6.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1j8jxrlagjxr7k5r09jyl2548y0vhdsk21mlcd5ka7g5840slav3") (y #t)))

(define-public crate-wemod-pro-unlocker-0.6.2 (c (n "wemod-pro-unlocker") (v "0.6.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "0zxjyvc92wdfsyz32mzdlqjavayx2gk2qds27q0z3iksvkdzs2b0") (y #t)))

(define-public crate-wemod-pro-unlocker-0.6.4 (c (n "wemod-pro-unlocker") (v "0.6.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1m6gz67vlxv9da678bdkv2vxjw3ym6alap1pwga8swa9xn77fg49") (y #t)))

(define-public crate-wemod-pro-unlocker-0.7.0 (c (n "wemod-pro-unlocker") (v "0.7.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "07715ik32c1l6p7rmp6krhrdw76l7978nv2ajj8wbmlca710gjdi") (y #t)))

(define-public crate-wemod-pro-unlocker-0.8.0 (c (n "wemod-pro-unlocker") (v "0.8.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1dsk25098zs8wh12svlhbfnqdxzg32wy13agh1f8w59yrj4q5b52") (y #t)))

(define-public crate-wemod-pro-unlocker-0.8.1 (c (n "wemod-pro-unlocker") (v "0.8.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1aidg49vczpqrzz8s61ij7dgs7gp5c4h6biy3hyywrpkqc6dfb6i") (y #t)))

(define-public crate-wemod-pro-unlocker-0.9.0 (c (n "wemod-pro-unlocker") (v "0.9.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "05qrbkgqwbgnk6j595y9290i27d979464xhiga7qs0ff5mbyv7fl") (y #t)))

(define-public crate-wemod-pro-unlocker-0.10.0 (c (n "wemod-pro-unlocker") (v "0.10.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "0rkn4zczfrihwivnsslspvn6w9vn12rzlayb540ajykzd5iyba3r") (y #t)))

(define-public crate-wemod-pro-unlocker-0.10.1 (c (n "wemod-pro-unlocker") (v "0.10.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1mndcvbxjjbx0x2h3yxqxlw13v55wxkbkkxgkfydxzjk4b9c0rs4") (y #t)))

(define-public crate-wemod-pro-unlocker-0.10.2 (c (n "wemod-pro-unlocker") (v "0.10.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "0k6gl4hdbls7727j84sxbq8k2wzzcgg0lm7fsiaw5k8v908zwlvi") (y #t)))

(define-public crate-wemod-pro-unlocker-0.11.1 (c (n "wemod-pro-unlocker") (v "0.11.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "0584cgi5vbw5wlrv9bywfafn2ag433gdj7fdv0lxrajh6lg74zx7") (y #t)))

(define-public crate-wemod-pro-unlocker-0.12.0 (c (n "wemod-pro-unlocker") (v "0.12.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1cvidwhmmjnhg09rfia0gjm1dh2rjcdm3jhd06rxx7a5q34dixc8") (y #t)))

(define-public crate-wemod-pro-unlocker-0.12.1 (c (n "wemod-pro-unlocker") (v "0.12.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "19vv20b9hy8p9lfdwpriwhl2jy1msk9npb541dv8cw6cmi2qksqn") (y #t)))

(define-public crate-wemod-pro-unlocker-0.13.0 (c (n "wemod-pro-unlocker") (v "0.13.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "1biim6mrzh6shg3d4s4r1spggslvp9cfgmm5zy3hhbknhznymlqb") (y #t)))

(define-public crate-wemod-pro-unlocker-0.13.1 (c (n "wemod-pro-unlocker") (v "0.13.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "minreq") (r "^2.6.0") (f (quote ("https-native"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "simple--args") (r "^1.1.1") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "windirs") (r "^1.0.1") (d #t) (k 0)))) (h "00khd0088jd3gm00dflrpqf5i9j0znrhi9943n320hqqkmh4sdi1") (y #t)))

(define-public crate-wemod-pro-unlocker-1.0.0 (c (n "wemod-pro-unlocker") (v "1.0.0") (h "1hhi4x9q67nhw1qnp2aj52wp4sb7rnhs0mi1k785xhmxmgirj6vn") (y #t)))

