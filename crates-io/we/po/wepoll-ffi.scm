(define-module (crates-io we po wepoll-ffi) #:use-module (crates-io))

(define-public crate-wepoll-ffi-0.1.0 (c (n "wepoll-ffi") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vsx2p4j6w9a9hwifx83p7grw2sv813q5l5cbqns56a3r6ibxhvv")))

(define-public crate-wepoll-ffi-0.1.1 (c (n "wepoll-ffi") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ss49h1qm665l06pfnd4qm5qphpf49223d3f0a00zcp84qvd5v5h") (f (quote (("null-overlapped-wakeups-patch"))))))

(define-public crate-wepoll-ffi-0.1.2 (c (n "wepoll-ffi") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1yxpkva08d5f6ih3b5hdb8h45mkz3jq3dh1bzjspfhy6qpnzshyp") (f (quote (("null-overlapped-wakeups-patch"))))))

