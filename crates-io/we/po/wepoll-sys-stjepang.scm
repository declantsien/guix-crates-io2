(define-module (crates-io we po wepoll-sys-stjepang) #:use-module (crates-io))

(define-public crate-wepoll-sys-stjepang-1.0.0 (c (n "wepoll-sys-stjepang") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15pllir026h56l9m7fvqp6r10iz9xnz405gsi636d7gwvr6l3q3l") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

(define-public crate-wepoll-sys-stjepang-1.0.1 (c (n "wepoll-sys-stjepang") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0f2y1755q54hwcskqrbg11jyawvisavxv0smypmq4g1qf3fcbr5q") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

(define-public crate-wepoll-sys-stjepang-1.0.2 (c (n "wepoll-sys-stjepang") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0yfw1lar68067545rhmhbqp1c12n6dyaj1jjzpw433grzjqqlpq3") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

(define-public crate-wepoll-sys-stjepang-1.0.3 (c (n "wepoll-sys-stjepang") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1zqgj8vvr5lfasmkrpjvxc0ah7x67xg1g1fi5drwxcjsljggm1rr") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

(define-public crate-wepoll-sys-stjepang-1.0.4 (c (n "wepoll-sys-stjepang") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0cgm6n3xxn0fjigixjf82zqjlfrrx69zzc52lg6l8m2msprdawmj") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

(define-public crate-wepoll-sys-stjepang-1.0.5 (c (n "wepoll-sys-stjepang") (v "1.0.5") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0m46s27awqm2jkb6ifvbpdp6fmgrbdm094zcsz23xsx7hz2cp52x") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

(define-public crate-wepoll-sys-stjepang-1.0.6 (c (n "wepoll-sys-stjepang") (v "1.0.6") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "152651ajnd42n6g8ggk382swcrmd29l05c8p7ssnc0cqf7liklvg") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

(define-public crate-wepoll-sys-stjepang-1.0.7 (c (n "wepoll-sys-stjepang") (v "1.0.7") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "09xag8iynb2635q7iwlv3a86fxqb4npsw8yfl13h21nq2vpy51f8") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

(define-public crate-wepoll-sys-stjepang-1.0.8 (c (n "wepoll-sys-stjepang") (v "1.0.8") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "138pxc8k6wayyywnjcpk5nhywk3vk6h4i39fj8khpjlhy81vppqz") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

