(define-module (crates-io we po wepoll-binding) #:use-module (crates-io))

(define-public crate-wepoll-binding-1.0.0 (c (n "wepoll-binding") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^1.0") (d #t) (k 0)))) (h "0dhayan3313f2796cjzzsxlrqp3rgq4n8my1r7d0rx7iylirsb3p")))

(define-public crate-wepoll-binding-1.0.1 (c (n "wepoll-binding") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^1.0") (d #t) (k 0)))) (h "14086sji8s09cva4blkb6lh900p8w2xzy1scgd7380jkvdjdzbvq")))

(define-public crate-wepoll-binding-1.0.2 (c (n "wepoll-binding") (v "1.0.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^1.0") (d #t) (k 0)))) (h "0hk1pwkzm6skvhsci1sgzjyi3ipb4m8b9084yvw08kgv2xcxcmgr")))

(define-public crate-wepoll-binding-1.0.3 (c (n "wepoll-binding") (v "1.0.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^1.0.3") (d #t) (k 0)))) (h "0q15ch3zvhkhrrnps5r1yrd29vic3hhfm6xjgw7ahw9psslcxrpf")))

(define-public crate-wepoll-binding-1.0.4 (c (n "wepoll-binding") (v "1.0.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^1.0.3") (d #t) (k 0)))) (h "0a9x1jd37gk0ana7j5kas6kfri3glkk94m5l45shn6r9z7hps3c6")))

(define-public crate-wepoll-binding-1.0.5 (c (n "wepoll-binding") (v "1.0.5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^1.0.4") (d #t) (k 0)))) (h "0iy5ycqp2pr3fxqs87jj28833zmi2jn1xrd3ik0yvr7pdiadimrg")))

(define-public crate-wepoll-binding-1.1.0 (c (n "wepoll-binding") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^1.0.4") (d #t) (k 0)))) (h "16w8j28qxhxhl03zq8zzvhir4kfsqyh1jch1ql1c6g2jx9g20nbp")))

(define-public crate-wepoll-binding-2.0.0 (c (n "wepoll-binding") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^2.0") (d #t) (k 0)))) (h "1idgw46bg426xf20xisy9vkgdlz40rwmzhdvqybp03vlc8kca33j")))

(define-public crate-wepoll-binding-2.0.1 (c (n "wepoll-binding") (v "2.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^2.0") (d #t) (k 0)))) (h "1356lfs41cjb3a8izy8n7sybw5a9jxidrmkzc2x2rapdj96h24bm")))

(define-public crate-wepoll-binding-2.0.2 (c (n "wepoll-binding") (v "2.0.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^2.0") (d #t) (k 0)))) (h "19wri7a6fngzhrhi731dcdil1i2n676vl50dmnvgh7vhz57zykrp")))

(define-public crate-wepoll-binding-3.0.0 (c (n "wepoll-binding") (v "3.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "wepoll-sys") (r "^3.0") (d #t) (k 0)))) (h "0p3s55nqa0ydmhg7nmdfyyyhw9m25gh6h36lx6v7hh7751z7j4qd")))

