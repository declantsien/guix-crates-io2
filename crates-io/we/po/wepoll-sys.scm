(define-module (crates-io we po wepoll-sys) #:use-module (crates-io))

(define-public crate-wepoll-sys-1.0.0 (c (n "wepoll-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1vqga28nz83035m4dws51xm7hr5lbadj839bdn9w38qbgpz32i8b") (l "wepoll")))

(define-public crate-wepoll-sys-1.0.1 (c (n "wepoll-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "07g5lgckcnralbkzq6cpa1yq19w7n8ax5ikmpzm5sv0nan70zyvm") (f (quote (("default" "compile-wepoll") ("compile-wepoll")))) (l "wepoll")))

(define-public crate-wepoll-sys-1.0.2 (c (n "wepoll-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1fdxk1lc1753nhwv06lffmzzrfwrbdhdppj34ddcsqc0glhxxzy8") (f (quote (("default" "compile-wepoll") ("compile-wepoll")))) (l "wepoll")))

(define-public crate-wepoll-sys-1.0.3 (c (n "wepoll-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vsn6pp9bh5g06w01i8x5x51f9wx9qfbdia4s79prkz111a3ip8y") (l "wepoll")))

(define-public crate-wepoll-sys-1.0.4 (c (n "wepoll-sys") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "10gn648ylyzpcm6wzka6ac716l6ylbsja6s6wrkqpm2vdqq4qad1") (l "wepoll")))

(define-public crate-wepoll-sys-1.1.0 (c (n "wepoll-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0kjzccphsmxrps9081v30lhdqvvpp73cn5xka00if9hvgzn7v4h8") (l "wepoll")))

(define-public crate-wepoll-sys-2.0.0 (c (n "wepoll-sys") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "093azxx6rdh7kf10kw6s2pbc7w99rfh4lr9bkrvgd4frmrvsg0lh") (f (quote (("default") ("buildtime-bindgen" "bindgen")))) (l "wepoll")))

(define-public crate-wepoll-sys-3.0.0 (c (n "wepoll-sys") (v "3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1q619gq2cbz2j8kp4f3l5p7v1macz8kwfmfbiwdbx27ylg5w4aql") (f (quote (("default")))) (l "wepoll")))

(define-public crate-wepoll-sys-3.0.1 (c (n "wepoll-sys") (v "3.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1zvpkr4dz3ny0k20mg1wdlp8vawz5p4gnya7h8j24119m7g19jqg") (f (quote (("default")))) (l "wepoll")))

