(define-module (crates-io we bg webgl-matrix) #:use-module (crates-io))

(define-public crate-webgl-matrix-0.1.0 (c (n "webgl-matrix") (v "0.1.0") (h "0hslrf9ixpsxx5rs1mg2w3zq5x3q289lhpmbfzhr09hk3zpy3gqs") (f (quote (("default" "Matrix4" "Matrix3") ("Vector4" "SliceOps") ("Vector3" "SliceOps") ("SliceOps") ("Matrix4" "Vector4") ("Matrix3" "Vector3"))))))

