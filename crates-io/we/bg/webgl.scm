(define-module (crates-io we bg webgl) #:use-module (crates-io))

(define-public crate-webgl-0.1.0 (c (n "webgl") (v "0.1.0") (d (list (d (n "gl") (r "^0.9") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "glenum") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "00whiz2mkwg2yb968wfa0ywbzf1y65s9ywgw1akmd7idyxaq2x58")))

