(define-module (crates-io we bg webgpu_align) #:use-module (crates-io))

(define-public crate-webgpu_align-0.1.0 (c (n "webgpu_align") (v "0.1.0") (d (list (d (n "half") (r ">0") (o #t) (k 0)))) (h "1fb5mxxifdilq66afwmvw35p72hj4ykjd9nyz30gj2v01xppphf2") (f (quote (("default" "half")))) (y #t)))

