(define-module (crates-io we bg webgl-rendering-context) #:use-module (crates-io))

(define-public crate-webgl-rendering-context-0.1.0 (c (n "webgl-rendering-context") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.10") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)))) (h "00wvpni1q4p80xa9cg3jxsnp5crwxwk4rl5dhizhs6cka1bhjldw")))

