(define-module (crates-io we bg webgate) #:use-module (crates-io))

(define-public crate-webgate-0.1.0 (c (n "webgate") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "popol") (r "^0.5") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)))) (h "0hx6f1hbr3w2kpf25184w4578ylpsyjnmz0rv9rljz482iiqqfap")))

(define-public crate-webgate-0.1.1 (c (n "webgate") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "popol") (r "^0.5") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)))) (h "034ccy2xcc944cx29ipqsxx7jwi4am87qfkagynnyki350yf0pyk")))

(define-public crate-webgate-0.1.2 (c (n "webgate") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "popol") (r "^0.5") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)))) (h "1y4sdcm4vyy8k5043mc1qpn4msbz27xa17ji8cf44gk8576ccqlg")))

(define-public crate-webgate-0.1.3 (c (n "webgate") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "popol") (r "^0.5") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "12yy7y9w8acbjdssrmqfymm32q4yhwb7fwv3w77wg93ip6pwbwar")))

