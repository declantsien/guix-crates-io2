(define-module (crates-io we bg webgl_stdweb) #:use-module (crates-io))

(define-public crate-webgl_stdweb-0.1.0 (c (n "webgl_stdweb") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.0") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "webgl_generator") (r "^0.1.0") (d #t) (k 1)))) (h "11m128c2v4xjf46c7wd717c1fs9xrzwq2q1w094vgv9w1yrdka45")))

(define-public crate-webgl_stdweb-0.2.0 (c (n "webgl_stdweb") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.0") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "webgl_generator") (r "^0.1.0") (d #t) (k 1)))) (h "14j815p3w8iwaw30w6kwlwpci1pm933g4k573a7zn58p3h5i0q0z")))

(define-public crate-webgl_stdweb-0.3.0 (c (n "webgl_stdweb") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (k 0)) (d (n "stdweb-derive") (r "^0.5") (d #t) (k 0)) (d (n "webgl_generator") (r "^0.2.0") (d #t) (k 1)))) (h "0c1fpmgprydvprsldnq0am6pgwzsm1ccbhjhskc9yql6mq0p7kh4")))

