(define-module (crates-io we bg webgl_generator) #:use-module (crates-io))

(define-public crate-webgl_generator-0.1.0 (c (n "webgl_generator") (v "0.1.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "html2runes") (r "^1.0.0") (d #t) (k 0)) (d (n "khronos_api") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "webidl") (r "^0.4.1") (d #t) (k 0)))) (h "1xpiw459fn41nj3kxj4n8rfznldcdyfiir6pd63488pwhmh50gbp")))

(define-public crate-webgl_generator-0.2.0 (c (n "webgl_generator") (v "0.2.0") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "html2runes") (r "^1.0") (d #t) (k 0)) (d (n "khronos_api") (r "^3.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "webidl") (r "^0.8") (d #t) (k 0)))) (h "15aq5c1lh47k562j19iyvsr54x0y9wnxjzlzzrfy5xsa4xb9sx2d")))

