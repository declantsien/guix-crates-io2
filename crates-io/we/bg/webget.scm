(define-module (crates-io we bg webget) #:use-module (crates-io))

(define-public crate-webget-0.1.0 (c (n "webget") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "hyper") (r "^0.14.5") (f (quote ("client" "http1" "http2" "runtime" "stream"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22.1") (d #t) (k 0)) (d (n "mime2ext") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rk8kr0vv835yipy6nmza3i5i1j347icvl9nhbxc0ycbkijsizwg")))

