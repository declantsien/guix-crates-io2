(define-module (crates-io we bg webgestalt) #:use-module (crates-io))

(define-public crate-webgestalt-0.1.0 (c (n "webgestalt") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (f (quote ("supports-colors"))) (d #t) (k 0)) (d (n "webgestalt_lib") (r "^0.1.0") (d #t) (k 0)))) (h "0cg2wsa83x5b0rx11jis4m7wp7yx4zfj52z2jgcr8sgv5sd7caw3") (y #t) (r "1.63.0")))

(define-public crate-webgestalt-0.1.1 (c (n "webgestalt") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (f (quote ("supports-colors"))) (d #t) (k 0)) (d (n "webgestalt_lib") (r "^0.1.0") (d #t) (k 0)))) (h "0mfll8mapizgj9djmmi6gx5bbhlfpw9wx2jcr2aj09jpnnp7sgd4") (r "1.63.0")))

(define-public crate-webgestalt-0.2.0 (c (n "webgestalt") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (f (quote ("supports-colors"))) (d #t) (k 0)) (d (n "webgestalt_lib") (r "^0.2.0") (d #t) (k 0)))) (h "05np1wwmjnnby3bqg4qcq1y1fxd1ml45wyib45ywi1q5rqabbh0g") (r "1.63.0")))

(define-public crate-webgestalt-0.3.0 (c (n "webgestalt") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (f (quote ("supports-colors"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "webgestalt_lib") (r "^0.3.0") (d #t) (k 0)))) (h "0f88i7jbfk41az7kphxqmb818sq062radv7dxklv06gzz5cdrifc") (r "1.63.0")))

