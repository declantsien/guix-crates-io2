(define-module (crates-io we bg webgl-rc-macro) #:use-module (crates-io))

(define-public crate-webgl-rc-macro-0.1.0 (c (n "webgl-rc-macro") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0b20szrzd6fdy8r7dfyhnhdwkggcm793ia2q49rwbx117cj6fg5w")))

(define-public crate-webgl-rc-macro-0.1.1 (c (n "webgl-rc-macro") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "11mxinxmfjlil0vx5x22vyg4a728dcyhwdm00pb2zvlf7fvg86s5")))

(define-public crate-webgl-rc-macro-0.1.2 (c (n "webgl-rc-macro") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1fsv45pvlq1w3bjxdryh2xgavsdxzc2grxr1isvzpxrayck6awgj")))

(define-public crate-webgl-rc-macro-0.1.3 (c (n "webgl-rc-macro") (v "0.1.3") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "05831s4hwiyr38hl26gvf7sl2khjb9p3fs5y3apmicnkak56f4d5")))

