(define-module (crates-io we bg webgl-rs) #:use-module (crates-io))

(define-public crate-webgl-rs-0.1.0 (c (n "webgl-rs") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.13") (d #t) (k 0)))) (h "0qbk4yzhqn0hyr4mi6nplh10id1xny4ym6alg7wv6g7xamj3xpmm")))

(define-public crate-webgl-rs-0.1.1 (c (n "webgl-rs") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.13") (d #t) (k 0)))) (h "1yyq6v48cb3pxf42vx69x9dxdgxwm26jl0lq1y0macjnyl7x9h3j")))

(define-public crate-webgl-rs-0.2.0 (c (n "webgl-rs") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2.13") (d #t) (k 0)))) (h "0s8sjlc2085azwsx7v2qkfb8mq4d37ypp9smdijqdl5qn2b08wls")))

(define-public crate-webgl-rs-0.2.1 (c (n "webgl-rs") (v "0.2.1") (d (list (d (n "wasm-bindgen") (r "^0.2.25") (d #t) (k 0)))) (h "01yb6g0wfx9wmb1adc87l2xf0a6xavwrrdca42612bc42515ihcm")))

(define-public crate-webgl-rs-0.2.2 (c (n "webgl-rs") (v "0.2.2") (d (list (d (n "wasm-bindgen") (r "^0.2.25") (d #t) (k 0)))) (h "1c5d0f586h5p9nyfb6xyivn4fr3p53habgb0lw0rmz5084hqf5wx")))

