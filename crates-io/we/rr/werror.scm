(define-module (crates-io we rr werror) #:use-module (crates-io))

(define-public crate-werror-0.0.0 (c (n "werror") (v "0.0.0") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "19np81pg6zqdbqz8zmxvxhcy922rqyjzcxpc21c4kw3qs7yd2lrf")))

(define-public crate-werror-0.0.1 (c (n "werror") (v "0.0.1") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "0ph19k1a4mr9mx9kf5ixwdvmha6cjbj2wnhwi5m32yl5dqf0xfvn")))

(define-public crate-werror-0.0.2 (c (n "werror") (v "0.0.2") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "1aqz4qjyqkvv922lb4klx06p6ii274hj1vz7gqwizfyw9y67jj69")))

(define-public crate-werror-0.1.0 (c (n "werror") (v "0.1.0") (d (list (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "133bbk9r0591s9py6rzsn1clyzsg1636xhgc65i019zvrcb9cqkz")))

(define-public crate-werror-0.1.1 (c (n "werror") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1ralmpmfcrcmv05y4cyyw2zx7smkpxxzky21kmfabnnyw4lbm753")))

(define-public crate-werror-0.1.2 (c (n "werror") (v "0.1.2") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1h14aq7l95y69cs7y6dlk217jzl1ivsmzysg9s0pq1242wcf17ym")))

(define-public crate-werror-0.1.3 (c (n "werror") (v "0.1.3") (d (list (d (n "error_tools") (r "~0.1") (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0m6jqwzbdyz6v9j5zhi74q98sj3zmiydh0hb33ibjdi1s6kwk5dr") (f (quote (("use_std" "error_tools/use_std") ("use_alloc" "error_tools/use_alloc") ("full" "use_std" "error_handling_for_lib" "error_handling_for_app") ("error_handling_for_lib" "error_tools/error_handling_for_lib") ("error_handling_for_app" "error_tools/error_handling_for_app") ("default" "use_std" "error_handling_for_lib" "error_handling_for_app"))))))

(define-public crate-werror-0.3.0 (c (n "werror") (v "0.3.0") (d (list (d (n "error_tools") (r "~0.3.0") (k 0)) (d (n "test_tools") (r "~0.4.0") (d #t) (k 2)))) (h "1gdqnnd01dy5lg6jzdvpsyw2a8zdxa7bf649cgbqzpcrzsdl275g") (f (quote (("use_alloc" "error_tools/use_alloc") ("no_std" "error_tools/no_std") ("full" "enabled" "error_for_lib" "error_for_app") ("error_for_lib" "error_tools/error_for_lib") ("error_for_app" "error_tools/error_for_app") ("enabled" "error_tools/enabled") ("default" "enabled" "error_for_lib" "error_for_app"))))))

(define-public crate-werror-0.4.0 (c (n "werror") (v "0.4.0") (d (list (d (n "error_tools") (r "~0.4.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "0cxc2mv8h5l81za58cv9kzab85mz97v6db2y0dv1zg7blfc567bp") (f (quote (("use_alloc" "error_tools/use_alloc") ("no_std" "error_tools/no_std") ("full" "enabled" "error_for_lib" "error_for_app") ("error_for_lib" "error_tools/error_for_lib") ("error_for_app" "error_tools/error_for_app") ("enabled" "error_tools/enabled") ("default" "enabled" "error_for_lib" "error_for_app"))))))

