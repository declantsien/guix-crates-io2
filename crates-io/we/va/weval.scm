(define-module (crates-io we va weval) #:use-module (crates-io))

(define-public crate-weval-0.0.0 (c (n "weval") (v "0.0.0") (h "1s14502hgxqs7qvy9cgjzcglv1qmx0jdh5kygvlhixndqklhbqsl")))

(define-public crate-weval-0.1.0 (c (n "weval") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "waffle") (r "^0.0.22") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.20") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)) (d (n "wizer") (r "^2.0") (d #t) (k 0)))) (h "1fxghy8sr0mf246x49zva0ljlgszlix9ykkf1xjszdfqpgdz800q")))

