(define-module (crates-io we tr wetransfer) #:use-module (crates-io))

(define-public crate-wetransfer-0.1.0 (c (n "wetransfer") (v "0.1.0") (d (list (d (n "mockito") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (f (quote ("hyper-011"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jmx6pjsjr7z4fgz35dc5nq5pfijsrrrwigypajr7qrkf9wybd3k")))

(define-public crate-wetransfer-0.1.1 (c (n "wetransfer") (v "0.1.1") (d (list (d (n "mockito") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (f (quote ("hyper-011"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02xafzwvjyiiyms3spcix3v84h96xsgizn77iyzrbl7gyf30irgd")))

