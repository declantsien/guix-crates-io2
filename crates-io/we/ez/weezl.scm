(define-module (crates-io we ez weezl) #:use-module (crates-io))

(define-public crate-weezl-0.0.1 (c (n "weezl") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "1qsx1cxdbcbarfg0xdc6yqiq5ws1zjs4ans67mbym6v2z4z3axxv") (f (quote (("raii_no_panic") ("default" "raii_no_panic"))))))

(define-public crate-weezl-0.0.2 (c (n "weezl") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0jn3lajy8jlajz1n87g212395p9xwyszr6rxw67jkyv9y5nla5s4") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1.0-alpha (c (n "weezl") (v "0.1.0-alpha") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "043k8aijgd8vwx7gz44p2b86sd9y68w5cimbg0h66wzl0zwcpgci") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1.0-beta.0 (c (n "weezl") (v "0.1.0-beta.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "1x82qvf1vgqq73l8fr99b8skmglbnjyg53fcx093xjhykhh33ash") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1.0-beta.1 (c (n "weezl") (v "0.1.0-beta.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0z0sa940rl0r3dy1shdw632kl7k2zl0jniaapicgr19y5a138hq8") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1.0 (c (n "weezl") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "1fgyz7919kzqkjk9mw3k7aq6dghwpyz6smkr6arjza9sdi5z5lm3") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1.1 (c (n "weezl") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "1x424yarxj0jwqmailx01n0sqswinjw4956695wkv3lr9mx6xqp0") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1.2 (c (n "weezl") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "02jqx386s8dkw37nd7913g3bfn8dijxjdw8fq4y811blw7hdd5c7") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1.3 (c (n "weezl") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0xx299f7zck0q4g0klpnirzx9ha471kkc4v5rpbls209hgybjary") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1.4 (c (n "weezl") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.12") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (k 2)) (d (n "tokio-util") (r "^0.6.2") (f (quote ("compat"))) (k 2)))) (h "0v16mvdmsicinbhgsm1l7gq1jmcaqrvm22rgn9lrhkhg71wb6cja") (f (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

(define-public crate-weezl-0.1.5 (c (n "weezl") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.12") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (k 2)) (d (n "tokio-util") (r "^0.6.2") (f (quote ("compat"))) (k 2)))) (h "13hy1zwnqrf021syjhz79mmi9bwwqjizzr0lnx5bwlx2spgpzdyq") (f (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

(define-public crate-weezl-0.1.6 (c (n "weezl") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.12") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (k 2)) (d (n "tokio-util") (r "^0.6.2") (f (quote ("compat"))) (k 2)))) (h "1r6mbc322d93g5ayqafmhrs12sziiibdx4bh966q6dpqv24y95ww") (f (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

(define-public crate-weezl-0.1.7 (c (n "weezl") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.12") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (k 2)) (d (n "tokio-util") (r "^0.6.2") (f (quote ("compat"))) (k 2)))) (h "1frdbq6y5jn2j93i20hc80swpkj30p1wffwxj1nr4fp09m6id4wi") (f (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

(define-public crate-weezl-0.1.8 (c (n "weezl") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.12") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (k 2)) (d (n "tokio-util") (r "^0.6.2") (f (quote ("compat"))) (k 2)))) (h "10lhndjgs6y5djpg3b420xngcr6jkmv70q8rb1qcicbily35pa2k") (f (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

