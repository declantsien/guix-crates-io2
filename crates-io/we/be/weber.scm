(define-module (crates-io we be weber) #:use-module (crates-io))

(define-public crate-weber-0.1.0 (c (n "weber") (v "0.1.0") (d (list (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1d4fxxg6b7yqfwvlyqzlcqb27zyxkz3qmifrq8cc6iai4wmgc85z")))

(define-public crate-weber-0.1.1 (c (n "weber") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "086n827gn2nw4hlb2s526zi2k5qhrlls20dqsls67qlp7grqagqq")))

