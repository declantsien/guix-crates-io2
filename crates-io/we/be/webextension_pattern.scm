(define-module (crates-io we be webextension_pattern) #:use-module (crates-io))

(define-public crate-webextension_pattern-0.2.0 (c (n "webextension_pattern") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "01qh7ci360gjk6324f6fwp7ncypww20g2a8zyqqx82478yk277m5")))

(define-public crate-webextension_pattern-0.2.1 (c (n "webextension_pattern") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "05bjfhl2y17n04vvsk11nd0gqkfi6zp90gjwj53l0y7r6knd4nw8")))

(define-public crate-webextension_pattern-0.2.2 (c (n "webextension_pattern") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "07f0jfslp455lm6x7n1ik3jzh8dqimwzq1ynpczv452s39c5gfpb")))

(define-public crate-webextension_pattern-0.3.0 (c (n "webextension_pattern") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0hl8v6r8lw3nfr2dn6y6n5npi24cknqbh8cwqh2y50hggy3ng9j8")))

