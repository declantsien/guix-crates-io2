(define-module (crates-io we be webextension-native-messaging) #:use-module (crates-io))

(define-public crate-webextension-native-messaging-1.0.0 (c (n "webextension-native-messaging") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1s15s8m1wxyryip8nj1kk8skrawiwyr34h1hsxar74q74y1xm4i3")))

(define-public crate-webextension-native-messaging-1.0.1 (c (n "webextension-native-messaging") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "14vh5i090zxwgyg6l8ifkfx508kb06lp8c96ygp2hplr1p3m3dnj")))

