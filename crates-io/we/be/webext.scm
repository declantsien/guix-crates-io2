(define-module (crates-io we be webext) #:use-module (crates-io))

(define-public crate-webext-0.1.0 (c (n "webext") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "157z79mc9cd72v102f4flgykl4zz3jxr08l9j1ddgaz730x7l11m")))

