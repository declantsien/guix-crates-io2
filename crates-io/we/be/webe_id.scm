(define-module (crates-io we be webe_id) #:use-module (crates-io))

(define-public crate-webe_id-0.1.0 (c (n "webe_id") (v "0.1.0") (h "0j25sjks4ffn8lw6q6kmd8yvb7g49b9iz61q0sycdaznagfvd8ml")))

(define-public crate-webe_id-0.1.1 (c (n "webe_id") (v "0.1.1") (h "15n3igmrbav8z5vxkwyqj32171gghwi2fr7dbz7yifkdyqvf4bas")))

(define-public crate-webe_id-0.2.0 (c (n "webe_id") (v "0.2.0") (h "0isrwxfpiz495mxayy2bp3pl0i3s4rmm91dn1p6s20x89k1sr1wh")))

