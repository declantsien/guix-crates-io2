(define-module (crates-io we bb webb-proposal-derive) #:use-module (crates-io))

(define-public crate-webb-proposal-derive-0.1.0 (c (n "webb-proposal-derive") (v "0.1.0") (d (list (d (n "ethers-core") (r "^2.0.8") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vdhb5bq9a9q55v62kk97427hpi8kfcbvscsjyyhr0f1qc21j8zm") (f (quote (("std") ("default" "std"))))))

