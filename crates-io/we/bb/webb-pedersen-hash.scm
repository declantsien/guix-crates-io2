(define-module (crates-io we bb webb-pedersen-hash) #:use-module (crates-io))

(define-public crate-webb-pedersen-hash-0.1.0 (c (n "webb-pedersen-hash") (v "0.1.0") (d (list (d (n "ark-crypto-primitives") (r "^0.3.0") (f (quote ("r1cs"))) (d #t) (k 0)) (d (n "ark-ed-on-bn254") (r "^0.3.0") (f (quote ("r1cs"))) (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (k 2)) (d (n "rand_chacha") (r "^0.3") (k 0)))) (h "0gpghlszr1251vqaxf2dlnjivh3qjxx1k43mbzhmw7b7rz9va2z2")))

