(define-module (crates-io we bb webber) #:use-module (crates-io))

(define-public crate-webber-0.1.0 (c (n "webber") (v "0.1.0") (h "0fjjnvwp7yl0k3hwh7w252zw2lci32kcchaxkww1z9cy1jskynbl")))

(define-public crate-webber-1.0.0 (c (n "webber") (v "1.0.0") (h "1a38rgm3n0dspfqaip3asskaycylrx6p7p9j42zw19liqd62m47p")))

