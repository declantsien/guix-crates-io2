(define-module (crates-io we bb webbby) #:use-module (crates-io))

(define-public crate-webbby-0.1.0 (c (n "webbby") (v "0.1.0") (h "0d8d300c7n5rwh2d8fw9ggqz82fs0bzbvy9wz8xx92yfv8vfxv6x")))

(define-public crate-webbby-0.1.1 (c (n "webbby") (v "0.1.1") (h "07nr18npyy6vssm1521riry74alkk14hxyz5hxf133ki29ibicxf")))

