(define-module (crates-io we bb webbundle-bench) #:use-module (crates-io))

(define-public crate-webbundle-bench-0.1.0 (c (n "webbundle-bench") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "askama") (r "^0.11.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "webbundle") (r "^0.3.2") (d #t) (k 0)))) (h "1ma2fkzr738wrkyrhzpmnls5kwkx8pvbvf3c2jqp4c0x8np0184y")))

(define-public crate-webbundle-bench-0.2.0 (c (n "webbundle-bench") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "askama") (r "^0.11.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "webbundle") (r "^0.4.0") (d #t) (k 0)))) (h "1pdvmb0sgkdkviin4disaz8m0gji4i2yg7wqm3wadvaghm1qkw7h")))

(define-public crate-webbundle-bench-0.4.1 (c (n "webbundle-bench") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "askama") (r "^0.11.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "webbundle") (r "^0.4.1") (d #t) (k 0)))) (h "0p3cdyqgj88gg0vc2vkx8b1944bhg5lvpwkn4m9v43l2apglkf5i")))

(define-public crate-webbundle-bench-0.5.0 (c (n "webbundle-bench") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "askama") (r "^0.11.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "webbundle") (r "^0.5.0") (d #t) (k 0)))) (h "1lz8kmgk2a7p9nkfn90hj0kc2cylik1psw5mv9q0kz7qa5ql9ghd")))

(define-public crate-webbundle-bench-0.5.1 (c (n "webbundle-bench") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "askama") (r "^0.11.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "webbundle") (r "^0.5.1") (d #t) (k 0)))) (h "05007rzy9rycm79hcqv24860cjyndiwpwp2xw4nf16vxahdldc2y")))

