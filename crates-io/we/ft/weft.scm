(define-module (crates-io we ft weft) #:use-module (crates-io))

(define-public crate-weft-0.1.0 (c (n "weft") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "markup5ever") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 2)) (d (n "weft_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0hjq6yaayy2lcs84lia9pkr4vibw23wg9l685m388nr1z3kx8pcj")))

(define-public crate-weft-0.1.2 (c (n "weft") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "regex") (r "^1.0.5") (d #t) (k 2)) (d (n "v_htmlescape") (r "^0.15.7") (d #t) (k 0)) (d (n "weft_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0ld0wchllpggnkig8mfql70bwvyw6k2xa6jg20ysd99z7pw57fi9")))

