(define-module (crates-io we bp webpack-q-cli) #:use-module (crates-io))

(define-public crate-webpack-q-cli-0.2.0 (c (n "webpack-q-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "webpack-q") (r "^0.2.0") (d #t) (k 0)))) (h "1gsxgn4w8rg0sd8lpj6k7g8bb7f1fqs3cf0im79mkhz1pvykmhak")))

(define-public crate-webpack-q-cli-0.2.1 (c (n "webpack-q-cli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "webpack-q") (r "^0.2.0") (d #t) (k 0)))) (h "0r9cxx19zy2fk29b6f26zbd4z5025qsh2881012d43m2mqyzfx88")))

