(define-module (crates-io we bp webpacker) #:use-module (crates-io))

(define-public crate-webpacker-0.1.0 (c (n "webpacker") (v "0.1.0") (d (list (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "12jgs8rmfpijk99bkrb0cshjg0lvpbb7d749xj7mdlqxlchfp0wp")))

(define-public crate-webpacker-0.1.1 (c (n "webpacker") (v "0.1.1") (d (list (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "1s1hwzck5yji0y7xfxff934aic0iblr45qg28d3s7fy2m0xqn73r")))

(define-public crate-webpacker-0.1.2 (c (n "webpacker") (v "0.1.2") (d (list (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "0x5vg3mzll2nck5m0v1p9cj1322kl3n6f54yabiviv20jc82gn9j")))

(define-public crate-webpacker-0.1.3 (c (n "webpacker") (v "0.1.3") (d (list (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "1jaqfbhmpnx5lk9j8h3cvxxh8rfnrxmdrgmdkb4hwfz4cpyc3cfa")))

(define-public crate-webpacker-0.1.4 (c (n "webpacker") (v "0.1.4") (d (list (d (n "gotham") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "067dkxmd38r01f25c2g742pbw37kkgki28844cwwa96skh6zykyh") (f (quote (("gotham-helper" "gotham") ("default"))))))

(define-public crate-webpacker-0.2.0 (c (n "webpacker") (v "0.2.0") (d (list (d (n "gotham") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "0mg9361rwk9dwzmf88d3jl1j06y1zdqr48820vksmqwj7mi11zza") (f (quote (("gotham-helper" "gotham") ("default"))))))

(define-public crate-webpacker-0.3.0 (c (n "webpacker") (v "0.3.0") (d (list (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "1v0d1nvb8wbzywx6ay8jdmind3pxp8aiyhwkwavh1s6s9xql7xpn")))

(define-public crate-webpacker-0.3.1 (c (n "webpacker") (v "0.3.1") (d (list (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "0z5mc99w9lpqj63mry68ihlb2g7mif6qh776b3v66crgnxp0zyd4")))

(define-public crate-webpacker-0.3.2 (c (n "webpacker") (v "0.3.2") (d (list (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "1mdn1llgivsf6xbdb0h50hapx8qkc4agc41s1k86dx3k4rcal4r4")))

(define-public crate-webpacker-0.3.3 (c (n "webpacker") (v "0.3.3") (d (list (d (n "im") (r "~12.2.0") (d #t) (k 0)))) (h "18vpa94gwvd5f288nswb9szj52qlsb50jp63l5b51pfgw5g3rlaa")))

(define-public crate-webpacker-0.3.4 (c (n "webpacker") (v "0.3.4") (d (list (d (n "im") (r "~15.0.0") (d #t) (k 0)))) (h "188g6phr86v32xwnwjdd93vj7pj88m3rxvbgxr92rzign7lbdf0b")))

