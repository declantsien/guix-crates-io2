(define-module (crates-io we bp webparse) #:use-module (crates-io))

(define-public crate-webparse-0.1.0 (c (n "webparse") (v "0.1.0") (h "0nb215jjra43h71nam2mmy0pmm1d221szcgv3wranyifsfny4jak")))

(define-public crate-webparse-0.1.1 (c (n "webparse") (v "0.1.1") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "01b94xbs7k9yhnihxhb7d5mnlfhrf2kbkcsnhghhn15w25amd2wm")))

(define-public crate-webparse-0.1.2 (c (n "webparse") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0cvfkkdf5papp318flg42bv5avy4nc3lkd6jn0s4q5jl1smbpacx")))

(define-public crate-webparse-0.1.3 (c (n "webparse") (v "0.1.3") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1jyr5nl7jb57fm6d28z2c2vpf6daaa3kyhal666bv3ybbr3qizin")))

(define-public crate-webparse-0.1.4 (c (n "webparse") (v "0.1.4") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1f3jgdv7ghqnyzrh31xkbxsjrlj1w2dzzpi063xsqarfw282h087")))

(define-public crate-webparse-0.1.5 (c (n "webparse") (v "0.1.5") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1c38qjg0kx10ypn86p3kvxxsk37spgd0z8yv3jlmwgbgb69h76sx")))

(define-public crate-webparse-0.1.6 (c (n "webparse") (v "0.1.6") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0zq84x5pbg2cm5f5iknr1xawfyrc3l0p9664brrc47ximr6nq2g0")))

(define-public crate-webparse-0.1.7 (c (n "webparse") (v "0.1.7") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0ygjvj15wx3hp9zc8fwsyk3aqrixk813qfzbcvn11alp4nmkx0pf")))

(define-public crate-webparse-0.2.0 (c (n "webparse") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "15lfhlvl9kj8wrhjf0pfgv257mngzc6dq5dhw16sn9w2hcqjyklw")))

(define-public crate-webparse-0.2.2 (c (n "webparse") (v "0.2.2") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0h808l1c5nvdjhsp9i8wh1790p88lq80d6z0a4d8lrw2mi0kxzzd")))

(define-public crate-webparse-0.2.3 (c (n "webparse") (v "0.2.3") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1phv9jln59ixd1ib04rnnxh58ahaylagvs2wjz3q5g79pdiibaad")))

(define-public crate-webparse-0.2.4 (c (n "webparse") (v "0.2.4") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "16bid0kxa2l67sy49p2vn3zgk9vgzzb20mjbf41kgg9wnk4zdzhk")))

(define-public crate-webparse-0.2.6 (c (n "webparse") (v "0.2.6") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0jwlqqgc7289ma6h6cx8clknrxv411m08ipi4cfn0rkgclpzydxn")))

(define-public crate-webparse-0.2.7 (c (n "webparse") (v "0.2.7") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0hg4k12xd61hz3vi9qsv2071ijpsdz9irksj0a76mg8bskcsmpc1")))

