(define-module (crates-io we bp webp_killer) #:use-module (crates-io))

(define-public crate-webp_killer-1.2.1 (c (n "webp_killer") (v "1.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.15") (d #t) (k 0)))) (h "1y8swcmdnkhgm041qnkmvn50b6zpgi74bi4gksjdjanbjnis161z")))

(define-public crate-webp_killer-1.2.2 (c (n "webp_killer") (v "1.2.2") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.15") (d #t) (k 0)))) (h "0gclcs1asrbsrzz8kb0d3m5n44z15r39v2xrgiih60zidvdy4qdb")))

(define-public crate-webp_killer-1.2.3 (c (n "webp_killer") (v "1.2.3") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (d #t) (k 0)))) (h "1g1n2hjac6r4vna0s0qv0qx3iywp6c824p44698kv6bzfi26b87x")))

