(define-module (crates-io we bp webplatform) #:use-module (crates-io))

(define-public crate-webplatform-0.0.1 (c (n "webplatform") (v "0.0.1") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)))) (h "136a8am6kjahgs2dqdhkgp2f1ch00id9m96wcm6ny3n3ggaklhw1")))

(define-public crate-webplatform-0.1.0 (c (n "webplatform") (v "0.1.0") (d (list (d (n "concat_bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.3") (d #t) (k 0)))) (h "0j9zj2mfqwm3wskfihw152bxfz2fwg23jxah0h71s87x9wi2p8z3")))

(define-public crate-webplatform-0.1.1 (c (n "webplatform") (v "0.1.1") (d (list (d (n "concat_bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.3") (d #t) (k 0)))) (h "06fn5k8vd2g51nf9y5n68x6p944sps7r67x41hnadcarkd45bcm4")))

(define-public crate-webplatform-0.1.2 (c (n "webplatform") (v "0.1.2") (d (list (d (n "concat_bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.3") (d #t) (k 0)))) (h "1wwwq95gk5w6zp084i9c5030qrinn67qixf5k78qx3rprydncwkd")))

(define-public crate-webplatform-0.1.3 (c (n "webplatform") (v "0.1.3") (d (list (d (n "concat_bytes") (r "^0.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.3") (d #t) (k 0)))) (h "1a4kwjp4nr591zdp4w692cxg4bnq77dry658plai9aw3l7rhqh7a")))

(define-public crate-webplatform-0.1.4 (c (n "webplatform") (v "0.1.4") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)) (d (n "webplatform_concat_bytes") (r "^0.0.1") (d #t) (k 0)))) (h "11jnaypp4hxjr41anjs9g3f4nvh855fn1zfbififyx9m9jwysa6w")))

(define-public crate-webplatform-0.2.0 (c (n "webplatform") (v "0.2.0") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)) (d (n "webplatform_concat_bytes") (r "^0.0.1") (d #t) (k 0)))) (h "14gmlaad525gkvcrjm37zkv4fqwm5mrwnnrrkwkv1fnq89p33hj0")))

(define-public crate-webplatform-0.3.0 (c (n "webplatform") (v "0.3.0") (d (list (d (n "libc") (r "^0.1.3") (d #t) (k 0)) (d (n "webplatform_concat_bytes") (r "^0.0.1") (d #t) (k 0)))) (h "0fxwwmhg5k2nv83dz006rw8iy20ab51vj150glcaxwkb48m43zgk")))

(define-public crate-webplatform-0.4.0 (c (n "webplatform") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "0a625h93qs6yd6605mpjiksadm6c7mmdamw2wz6xmnir3b13zj5c")))

(define-public crate-webplatform-0.4.1 (c (n "webplatform") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "1wql0f0glva24fygjajfjv6lkggqx6isna96dbinnkg6g0a67hxq")))

(define-public crate-webplatform-0.4.2 (c (n "webplatform") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "1ds6j2kmmh0nmmgmdg4k6rh4zdx2cl9s4q8fv8b325g73nfdvvnw")))

