(define-module (crates-io we bp webport) #:use-module (crates-io))

(define-public crate-webport-0.1.0 (c (n "webport") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("native-tls" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "01qhc8gqd3s17p4kz2vpnqkgb7spj8s0syr0n1bm93cipgaxz114")))

(define-public crate-webport-0.1.1 (c (n "webport") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("native-tls" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1d5v8fk65l25xymljn27qkhbacw9rvk7a1xqbim2pvqj1sk85l84")))

(define-public crate-webport-0.1.2 (c (n "webport") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("native-tls" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "0m012908scf1v6ad9qsrik2k6lp69gn4vbv1f7n1hai4iv9qgm3g")))

