(define-module (crates-io we bp webp_sys) #:use-module (crates-io))

(define-public crate-webp_sys-0.1.0 (c (n "webp_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0r3xmcn857j1hmp1cfvkqxvxycbxvfm935mchz49hcfafwjf5na2")))

