(define-module (crates-io we bp webp_encoder) #:use-module (crates-io))

(define-public crate-webp_encoder-0.1.0 (c (n "webp_encoder") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xdw29m9sdnx48gnpj7zx1w0268dvdjby8cwyp507lxiayydllz3") (y #t)))

(define-public crate-webp_encoder-0.2.0 (c (n "webp_encoder") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1x0v7rqpds9yjjywq2iavxzx38697yqc9gf9j90hrhisq9n0szn1") (y #t)))

(define-public crate-webp_encoder-0.3.0 (c (n "webp_encoder") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k90bb6lml7f3qcd8yzq1di9mgq1qd0cjb9qy426rdjskwnwar26") (y #t)))

(define-public crate-webp_encoder-0.4.0 (c (n "webp_encoder") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17y27lfrq46ailfkk8wg7sjxv9zsjp0vq7yilh6j2ihiwrs0yyww") (y #t)))

(define-public crate-webp_encoder-0.5.0 (c (n "webp_encoder") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0m99s0wwqsv5vb4rrgxj3kj2z320zciylhyvqgraikbdxl3hpsar") (y #t)))

(define-public crate-webp_encoder-0.6.0 (c (n "webp_encoder") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1zwmkpmqh0mz88n363hqv9z777p566j5c6s2ljpdgzi2dg5wfpmq")))

