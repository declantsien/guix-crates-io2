(define-module (crates-io we bp webpage-cli) #:use-module (crates-io))

(define-public crate-webpage-cli-0.1.0 (c (n "webpage-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "webpage") (r "^1") (f (quote ("serde"))) (d #t) (k 0)))) (h "15cpwyc4dk1ckz2710syj21jf6qzf048cr9q9nax44gd25bs9jsb")))

(define-public crate-webpage-cli-0.1.1 (c (n "webpage-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "webpage") (r "^1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1rkjmilh3aqvk7ay90xafwial818f9i0m4rmgd0rzwmdc294akvg")))

