(define-module (crates-io we bp webpage) #:use-module (crates-io))

(define-public crate-webpage-0.1.0 (c (n "webpage") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.12") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)))) (h "1pfv07165icncg8r2fb9zr2j7ycw7990kmdamsnmirl9dhwzzmia")))

(define-public crate-webpage-0.1.1 (c (n "webpage") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.12") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)))) (h "1f846dppj03l5c9l1qfmljmhhzk9a99i6z29sd1ykwaqa3g3l1in")))

(define-public crate-webpage-0.1.2 (c (n "webpage") (v "0.1.2") (d (list (d (n "curl") (r "^0.4.12") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l83b5z5fn3y7vdf8jcwg0pf7vv5wap3yr3z1cvvnki8aq6wp9rw")))

(define-public crate-webpage-0.1.3 (c (n "webpage") (v "0.1.3") (d (list (d (n "curl") (r "^0.4.12") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pf68qm7sxndak95zpfy6s8br0ca3pwni6ailjzsvdnqnanbw0kg")))

(define-public crate-webpage-0.1.4 (c (n "webpage") (v "0.1.4") (d (list (d (n "curl") (r "^0.4.12") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nfzsiddpmwm5581q4mp0a5qbvp7rc2lw86jl43sfigf890wci9q")))

(define-public crate-webpage-1.0.0 (c (n "webpage") (v "1.0.0") (d (list (d (n "curl") (r "^0.4.12") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w8301qn809ivadlqlndvkqf8mav892cdijz8773jv91k0f411qc")))

(define-public crate-webpage-1.1.0 (c (n "webpage") (v "1.1.0") (d (list (d (n "curl") (r "^0.4.12") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "155g586337lxywsnmh76v9y5c9pfdfn3wq5dbsyzg2iqm2lih2aj")))

(define-public crate-webpage-1.2.0 (c (n "webpage") (v "1.2.0") (d (list (d (n "curl") (r "^0.4.12") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qdsb79b3r8f8l3c20r13gq0idc5fhphxjl0vgwq7fq88443ngn4")))

(define-public crate-webpage-1.3.0 (c (n "webpage") (v "1.3.0") (d (list (d (n "curl") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "160hy2v0pzm45ap6j6398qbn4dawl5xkpp40vklkydwa88r85pqs") (f (quote (("default" "curl"))))))

(define-public crate-webpage-1.4.0 (c (n "webpage") (v "1.4.0") (d (list (d (n "curl") (r "^0.4.41") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14581q74gyqfq2mxhj439185qrj2m60zdq2vvgzjyjg2iz5fn0sd") (f (quote (("default" "curl"))))))

(define-public crate-webpage-1.5.0 (c (n "webpage") (v "1.5.0") (d (list (d (n "curl") (r "^0.4.41") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cmjscnra3ppxv0xv3mfajy362g4lirqi7ncsfaqmd1c26pbnqfj") (f (quote (("default" "curl"))))))

(define-public crate-webpage-1.6.0 (c (n "webpage") (v "1.6.0") (d (list (d (n "curl") (r "^0.4.41") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s7klnwx4mjs3n0xd03h6j412zblgq6b4ysyx6mrbbxmxrdpi645") (f (quote (("default" "curl"))))))

(define-public crate-webpage-2.0.0-alpha.1 (c (n "webpage") (v "2.0.0-alpha.1") (d (list (d (n "curl") (r "^0.4.41") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z9da31bqqlwwvzxy1pyj0vm6vads340462y1r1w2q5zkcpkiajb") (f (quote (("default" "curl"))))))

(define-public crate-webpage-2.0.0-alpha.2 (c (n "webpage") (v "2.0.0-alpha.2") (d (list (d (n "curl") (r "^0.4.41") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0ymmdhps3hpr7j3p9ghv4g66s0wj2jyiwni7pnryc3f8m8y3j66v") (f (quote (("default" "curl")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-webpage-2.0.0 (c (n "webpage") (v "2.0.0") (d (list (d (n "curl") (r "^0.4.41") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "18jb8ba13havgy75l0adw9rppykg8vl1qmkzhschljcdwl96pf1z") (f (quote (("default" "curl")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-webpage-2.0.1 (c (n "webpage") (v "2.0.1") (d (list (d (n "curl") (r "^0.4.41") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.27") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1b1fh3k6xcwkksyi9gbcx28d15h5mqs9rfw2maxycihx0ky2x1kh") (f (quote (("default" "curl")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.63")))

