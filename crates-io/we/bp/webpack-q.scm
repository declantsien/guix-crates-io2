(define-module (crates-io we bp webpack-q) #:use-module (crates-io))

(define-public crate-webpack-q-0.1.0 (c (n "webpack-q") (v "0.1.0") (h "1znwpm5p9m7irkxbg5fq093hk1i1cr9m59zb3bp6hil93yl7kana")))

(define-public crate-webpack-q-0.0.0 (c (n "webpack-q") (v "0.0.0") (h "09vd2z7zzdjd92jf2dzrz6xsik9n3hb50q6qx8rh222fg0cgh489")))

(define-public crate-webpack-q-0.2.0 (c (n "webpack-q") (v "0.2.0") (d (list (d (n "askama") (r "^0.11") (f (quote ("serde-json"))) (d #t) (k 0)) (d (n "meshed") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "webpack-stats") (r "^0.2.0") (f (quote ("all"))) (d #t) (k 0)))) (h "1dwrz7q76d8sr57jp0x7ffw62yj9j0wmi6hrlqpwpq5lxkynxml1") (y #t)))

(define-public crate-webpack-q-0.2.1 (c (n "webpack-q") (v "0.2.1") (d (list (d (n "meshed") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "webpack-stats") (r "^0.2.0") (f (quote ("all"))) (d #t) (k 0)))) (h "1mihcnhycmldh1fl8ad7f2yc9infnwjsz9a7z95p22q7c49p5qfg")))

(define-public crate-webpack-q-0.2.2 (c (n "webpack-q") (v "0.2.2") (d (list (d (n "meshed") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "webpack-stats") (r "^0.2.0") (f (quote ("all"))) (d #t) (k 0)))) (h "0riclavl936sis9acaqamxcdiy0h107jcsj1b0gwcd0yd9y91sg9")))

