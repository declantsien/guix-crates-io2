(define-module (crates-io we bp webpki-roots) #:use-module (crates-io))

(define-public crate-webpki-roots-0.1.0 (c (n "webpki-roots") (v "0.1.0") (d (list (d (n "untrusted") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.2.2") (d #t) (k 0)))) (h "1z0iij8nlyvscz28jhr5qwm1lz8a3wm0pnyngdmbpp5bqpkwhcz8")))

(define-public crate-webpki-roots-0.2.0 (c (n "webpki-roots") (v "0.2.0") (d (list (d (n "untrusted") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.3.0") (d #t) (k 0)))) (h "1zfqg52324bsgw94m7qfc7ljsh20195fjym5alh8vvgf7ajdyl10")))

(define-public crate-webpki-roots-0.3.0 (c (n "webpki-roots") (v "0.3.0") (d (list (d (n "untrusted") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.3.0") (d #t) (k 0)))) (h "195hsrz3kqcbr5psz196xrw2l0h58qijvcl76jr8azv4vwmj2l3w")))

(define-public crate-webpki-roots-0.4.0 (c (n "webpki-roots") (v "0.4.0") (d (list (d (n "untrusted") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.3.0") (d #t) (k 0)))) (h "1pvv3fv13x76jqzjlk96vhi0721n1y7f4jxlnigr09i99yf017v2")))

(define-public crate-webpki-roots-0.5.0 (c (n "webpki-roots") (v "0.5.0") (d (list (d (n "untrusted") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.3.0") (d #t) (k 0)))) (h "1nk14ph27xd679b4vl67y03f892qfmblwarfa9qvx9fa708aghp9")))

(define-public crate-webpki-roots-0.6.0 (c (n "webpki-roots") (v "0.6.0") (d (list (d (n "untrusted") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.8.0") (d #t) (k 0)))) (h "09pqrlp49zzkhp7pxchbzfzl8g8cwl4zy8x97qxms3qjjn83y2bk")))

(define-public crate-webpki-roots-0.6.1 (c (n "webpki-roots") (v "0.6.1") (d (list (d (n "untrusted") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.9.2") (d #t) (k 0)))) (h "0fb2h2550bg7a2m2jf1ychywp69663pkar5dysvb62msnzbpa8vz")))

(define-public crate-webpki-roots-0.7.0 (c (n "webpki-roots") (v "0.7.0") (d (list (d (n "untrusted") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.10") (d #t) (k 0)))) (h "0q1hrdz0dip4qr5d7ndrh01nxys0p35xz02rjij7a2fsifbaq4gn")))

(define-public crate-webpki-roots-0.8.0 (c (n "webpki-roots") (v "0.8.0") (d (list (d (n "untrusted") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.10") (d #t) (k 0)))) (h "0ayzbp5q3rfrcmqs5k3ji4canj1i6jnpqal8kc79v3spnnm029ms")))

(define-public crate-webpki-roots-0.9.0 (c (n "webpki-roots") (v "0.9.0") (d (list (d (n "untrusted") (r "^0.5") (d #t) (k 0)) (d (n "webpki") (r "^0.11") (d #t) (k 0)))) (h "0vbswvc6bnsiy74rvvln9v0qqqjx5jaz5jgayjnr4fbzfad5garg")))

(define-public crate-webpki-roots-0.10.0 (c (n "webpki-roots") (v "0.10.0") (d (list (d (n "untrusted") (r "^0.5") (d #t) (k 0)) (d (n "webpki") (r "^0.12") (d #t) (k 0)))) (h "1dvcna33la8dahmcl95jkjpqq8djiq5qnz2bairxdh51aw3rnrdk")))

(define-public crate-webpki-roots-0.11.0 (c (n "webpki-roots") (v "0.11.0") (d (list (d (n "untrusted") (r "^0.5") (d #t) (k 0)) (d (n "webpki") (r "^0.14") (d #t) (k 0)))) (h "19w1q6w637n9hncgaa77y43v01aq7dg88hpl2wiss8cz9583zysv")))

(define-public crate-webpki-roots-0.12.0 (c (n "webpki-roots") (v "0.12.0") (d (list (d (n "untrusted") (r "^0.5") (d #t) (k 0)) (d (n "webpki") (r "^0.14") (d #t) (k 0)))) (h "08ap1ran9iv0adryxjl41j8wq00dqdcd226rr4iiri5kv6r3mf0j")))

(define-public crate-webpki-roots-0.13.0 (c (n "webpki-roots") (v "0.13.0") (d (list (d (n "untrusted") (r "^0.5.1") (d #t) (k 0)) (d (n "webpki") (r "^0.17") (d #t) (k 0)))) (h "0p21fn5ggimc452rwg3n1f8djwrla4i8rlkb0ykg7zdywmh40p8m")))

(define-public crate-webpki-roots-0.14.0 (c (n "webpki-roots") (v "0.14.0") (d (list (d (n "untrusted") (r "^0.6.1") (d #t) (k 0)) (d (n "webpki") (r "^0.18.0-alpha") (d #t) (k 0)))) (h "05zw919077i3jadbvdsvl69wv2siijg2pjbykl6fyi7hmgb7bggd")))

(define-public crate-webpki-roots-0.15.0 (c (n "webpki-roots") (v "0.15.0") (d (list (d (n "untrusted") (r "^0.6.2") (d #t) (k 0)) (d (n "webpki") (r "^0.18.1") (d #t) (k 0)))) (h "1gya8j75jnvf9lz36w0l4bf2xnw8qdx6plvhia891mcgj44g9lc5")))

(define-public crate-webpki-roots-0.16.0 (c (n "webpki-roots") (v "0.16.0") (d (list (d (n "untrusted") (r "^0.6.2") (d #t) (k 0)) (d (n "webpki") (r "^0.19") (d #t) (k 0)))) (h "03ny02mwqdgd2ff23k03kbwr2rrcaymxhp7jcjjikfh340hs83y1")))

(define-public crate-webpki-roots-0.17.0 (c (n "webpki-roots") (v "0.17.0") (d (list (d (n "webpki") (r "^0.21.0") (d #t) (k 0)))) (h "12vi8dh0yik0h4f0b9dnlw5i3gxyky7iblbksh6zcq4xvlvswqm2")))

(define-public crate-webpki-roots-0.18.0 (c (n "webpki-roots") (v "0.18.0") (d (list (d (n "webpki") (r "^0.21.0") (d #t) (k 0)))) (h "1d4ss607rgi9pj01zzqa13c1p3m35z314yh6lmjaj4kzvwv5gkci")))

(define-public crate-webpki-roots-0.19.0 (c (n "webpki-roots") (v "0.19.0") (d (list (d (n "webpki") (r "^0.21.0") (d #t) (k 0)))) (h "0fapdqwbfv0kncplpvbgnr0bjd5a9krlpij9jdzk0mvaa6vz9vzq")))

(define-public crate-webpki-roots-0.20.0 (c (n "webpki-roots") (v "0.20.0") (d (list (d (n "webpki") (r "^0.21.0") (d #t) (k 0)))) (h "17qpmyym1lsi967b4nc3112nb13ism8731bhjqd9hlajafkxw80g")))

(define-public crate-webpki-roots-0.21.0 (c (n "webpki-roots") (v "0.21.0") (d (list (d (n "webpki") (r ">=0.21.0, <0.22.0") (d #t) (k 0)))) (h "0xi3xjjx6brqh6hwa5jsrxvflsrhjcxa2x26k62q3bcb1dz5n0c2")))

(define-public crate-webpki-roots-0.21.1 (c (n "webpki-roots") (v "0.21.1") (d (list (d (n "webpki") (r "^0.21.0") (d #t) (k 0)))) (h "0h49lkr7hrxpyr0xg1nph4m3v1l6rhg8ax9n8msvfwz48hsibgma")))

(define-public crate-webpki-roots-0.22.0 (c (n "webpki-roots") (v "0.22.0") (d (list (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "1y2cx6pgjq04f6pmjlxiawxynyab01rs44iv14air8s6fmz3ccxm")))

(define-public crate-webpki-roots-0.22.1 (c (n "webpki-roots") (v "0.22.1") (d (list (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "0c56xd3sk82grjnm7272hp0lpjq4xhvkl13sf52r68a7dxn7hxf4")))

(define-public crate-webpki-roots-0.22.2 (c (n "webpki-roots") (v "0.22.2") (d (list (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "0jbll0ys9jakrvv3l1i216bbgj7jbxr7ad2dihw28xcm7s8fnb2m")))

(define-public crate-webpki-roots-0.22.3 (c (n "webpki-roots") (v "0.22.3") (d (list (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "1gy1pd7v534skxvr0j4nmlgpgb7fdwy4ibbhsamwh8y82n2dxn24")))

(define-public crate-webpki-roots-0.22.4 (c (n "webpki-roots") (v "0.22.4") (d (list (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "1bsdn08zs1k0hdgd350z5695s7v97vi1cy7d095c59k6sgq61izi")))

(define-public crate-webpki-roots-0.22.5 (c (n "webpki-roots") (v "0.22.5") (d (list (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "1gkmk4baan9knbf3dggwyvh8bqmdvi8x6mmpicih3yv9g5jzx2rn")))

(define-public crate-webpki-roots-0.22.6 (c (n "webpki-roots") (v "0.22.6") (d (list (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "11rd1aj73qzcvdj3x78crm1758sc4wrbc7rh0r8lmhyjsx01xixn")))

(define-public crate-webpki-roots-0.23.0 (c (n "webpki-roots") (v "0.23.0") (d (list (d (n "webpki") (r "^0.100.0") (d #t) (k 0) (p "rustls-webpki")))) (h "0981zxmdcmifjprzabv59rcabjjdxdmc8pfgf3hq8mdnjhv9cm5a")))

(define-public crate-webpki-roots-0.23.1 (c (n "webpki-roots") (v "0.23.1") (d (list (d (n "webpki") (r "^0.100.0") (d #t) (k 0) (p "rustls-webpki")))) (h "0f4k8nng542iilxbibh1nhrdf5wbyi9is4fr219zzrc6hgw5hc5h")))

(define-public crate-webpki-roots-0.24.0 (c (n "webpki-roots") (v "0.24.0") (d (list (d (n "webpki") (r "^0.101.0") (d #t) (k 0) (p "rustls-webpki")))) (h "120q85pvzpckvvrg085a5jhh91fby94pgiv9y1san7lxbmnm94dj")))

(define-public crate-webpki-roots-0.25.0 (c (n "webpki-roots") (v "0.25.0") (d (list (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots"))) (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "^0.101.0") (d #t) (k 2) (p "rustls-webpki")))) (h "0vcys6qyx5zk8ha1nrnfn4mif3plv454zmzngwmmr0wd0m9c8jhs")))

(define-public crate-webpki-roots-0.25.1 (c (n "webpki-roots") (v "0.25.1") (d (list (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots"))) (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "^0.101.2") (d #t) (k 2) (p "rustls-webpki")))) (h "15piy0vccppqb74li32gnn9l5a4ysxzwh8bp3qv6z8rhr2hyvin9")))

(define-public crate-webpki-roots-0.25.2 (c (n "webpki-roots") (v "0.25.2") (d (list (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots"))) (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "^0.101.2") (d #t) (k 2) (p "rustls-webpki")))) (h "1z13850xvsijjxxvzx1wq3m6pz78ih5q6wjcp7gpgwz4gfspn90l")))

(define-public crate-webpki-roots-0.26.0-alpha.0 (c (n "webpki-roots") (v "0.26.0-alpha.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "pki-types") (r "^0.1") (k 0) (p "rustls-pki-types")) (d (n "rcgen") (r "^0.11.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-manual-roots"))) (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "^0.102.0-alpha.1") (d #t) (k 2) (p "rustls-webpki")) (d (n "x509-parser") (r "^0.15.1") (d #t) (k 2)) (d (n "yasna") (r "^0.5.2") (d #t) (k 2)))) (h "1dkar0faq86d6zy654gvr1fsn4rjf94r5zdaln2gajclhxfvp5a0")))

(define-public crate-webpki-roots-0.26.0-alpha.1 (c (n "webpki-roots") (v "0.26.0-alpha.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "pki-types") (r "^0.2") (k 0) (p "rustls-pki-types")) (d (n "rcgen") (r "^0.11.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-manual-roots"))) (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "^0.102.0-alpha.2") (d #t) (k 2) (p "rustls-webpki")) (d (n "x509-parser") (r "^0.15.1") (d #t) (k 2)) (d (n "yasna") (r "^0.5.2") (d #t) (k 2)))) (h "1v959vypanfzfrhadv6j1xfx51pga8y7dld448r3b76aswlpj5a2")))

(define-public crate-webpki-roots-0.25.3 (c (n "webpki-roots") (v "0.25.3") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "rcgen") (r "^0.11.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-manual-roots"))) (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "^0.101.2") (d #t) (k 2) (p "rustls-webpki")) (d (n "x509-parser") (r "^0.15.1") (d #t) (k 2)) (d (n "yasna") (r "^0.5.2") (d #t) (k 2)))) (h "045g7az4mj1002m55iydln4jhyah4br2n0zms3wbz41vicpa8y0p")))

(define-public crate-webpki-roots-0.26.0-alpha.2 (c (n "webpki-roots") (v "0.26.0-alpha.2") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "pki-types") (r "^0.2.2") (k 0) (p "rustls-pki-types")) (d (n "rcgen") (r "^0.11.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-manual-roots"))) (d #t) (k 2)) (d (n "ring") (r "^0.17.0") (d #t) (k 2)) (d (n "rustls-pemfile") (r "=2.0.0-alpha.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "=0.102.0-alpha.7") (f (quote ("alloc"))) (d #t) (k 2) (p "rustls-webpki")) (d (n "x509-parser") (r "^0.15.1") (d #t) (k 2)) (d (n "yasna") (r "^0.5.2") (d #t) (k 2)))) (h "10bgixna84hiwn2p543m5jag7ds3z04yv1yqfb6an7r3h2fxkqw7")))

(define-public crate-webpki-roots-0.26.0 (c (n "webpki-roots") (v "0.26.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "pki-types") (r "^1") (k 0) (p "rustls-pki-types")) (d (n "rcgen") (r "^0.11.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-manual-roots"))) (d #t) (k 2)) (d (n "ring") (r "^0.17.0") (d #t) (k 2)) (d (n "rustls-pemfile") (r "^2.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "^0.102") (f (quote ("alloc"))) (d #t) (k 2) (p "rustls-webpki")) (d (n "x509-parser") (r "^0.15.1") (d #t) (k 2)) (d (n "yasna") (r "^0.5.2") (d #t) (k 2)))) (h "1221q07j5sv23bmwv8my49hdax70dwzdpsnjgrdbw88gk3dczqhd")))

(define-public crate-webpki-roots-0.26.1 (c (n "webpki-roots") (v "0.26.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "pki-types") (r "^1") (k 0) (p "rustls-pki-types")) (d (n "rcgen") (r "^0.12.0") (d #t) (k 2)) (d (n "ring") (r "^0.17.0") (d #t) (k 2)) (d (n "rustls") (r "^0.22") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "^0.102") (f (quote ("alloc"))) (d #t) (k 2) (p "rustls-webpki")) (d (n "x509-parser") (r "^0.15.1") (d #t) (k 2)) (d (n "yasna") (r "^0.5.2") (d #t) (k 2)))) (h "029006qfs61q75gl60aap25m0gdqmvd1pcpljid9b0q44yp39pmk")))

(define-public crate-webpki-roots-0.25.4 (c (n "webpki-roots") (v "0.25.4") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 2)) (d (n "rcgen") (r "^0.11.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-manual-roots"))) (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 2)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "webpki") (r "^0.101.2") (d #t) (k 2) (p "rustls-webpki")) (d (n "x509-parser") (r "^0.15.1") (d #t) (k 2)) (d (n "yasna") (r "^0.5.2") (d #t) (k 2)))) (h "1qgqa615gc1cgklls4bkjp9jv9pvv3jnl82lc6wd7dkximywa82z")))

