(define-module (crates-io we b- web-bench) #:use-module (crates-io))

(define-public crate-web-bench-0.1.0 (c (n "web-bench") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1sarjbdxx7yk5hqqksq8y451q302394sxnghkd34268mqm42rq2b")))

