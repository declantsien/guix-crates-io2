(define-module (crates-io we b- web-tools) #:use-module (crates-io))

(define-public crate-web-tools-0.1.0 (c (n "web-tools") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Node" "NodeList" "HtmlElement" "HtmlFormElement" "HtmlCollection"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1nfs1vx00i66qm73kbyc0hai9vckrbzshgnzkj27s2gjqy3ijlsi") (r "1.70")))

(define-public crate-web-tools-0.2.0 (c (n "web-tools") (v "0.2.0") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("Node" "NodeList" "HtmlElement" "HtmlFormElement" "HtmlCollection"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1wm8akm6x531qzxy30f3xj6p0ji9n9bf2q8l60icr7gcglxjzk4b") (r "1.70")))

(define-public crate-web-tools-0.2.1 (c (n "web-tools") (v "0.2.1") (d (list (d (n "web-sys") (r "^0.3.66") (f (quote ("Node" "NodeList" "HtmlElement" "HtmlFormElement" "HtmlCollection"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0ffvky3if3pp6jg8dcb289qv93qn5g8ysdjwi922s7wh4wif5009") (r "1.70")))

(define-public crate-web-tools-0.2.2 (c (n "web-tools") (v "0.2.2") (d (list (d (n "web-sys") (r "^0.3.66") (f (quote ("Node" "NodeList" "HtmlElement" "HtmlFormElement" "HtmlCollection"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0ia0z1dl3cr3pk2fah57jp3r83ivd3319mhz9czs0a639z9z6jjf") (r "1.70")))

(define-public crate-web-tools-0.2.3 (c (n "web-tools") (v "0.2.3") (d (list (d (n "web-sys") (r "^0.3.66") (f (quote ("Node" "NodeList" "HtmlElement" "HtmlFormElement" "HtmlCollection" "HtmlInputElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.21") (o #t) (d #t) (k 0)))) (h "015x8mgcnnlj77fsqr50m4a2s7cb8s6lza6is993h5l9khqj26wh") (r "1.70")))

