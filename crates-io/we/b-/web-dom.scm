(define-module (crates-io we b- web-dom) #:use-module (crates-io))

(define-public crate-web-dom-0.0.0 (c (n "web-dom") (v "0.0.0") (h "1iy5ydxz1ryg0hnhgrb33axl0ld83kfvdk4dyrp457sy8vw3q75c")))

(define-public crate-web-dom-0.0.1 (c (n "web-dom") (v "0.0.1") (h "04rsb8f46lqg7ls1kvsnfzzs9k1ih1l54py29gq0dfzb10xmk8wy")))

(define-public crate-web-dom-0.0.2 (c (n "web-dom") (v "0.0.2") (h "17c6dqlp29bfchd4na1v8mqlc0j4ypibsv3f97925gfgip04c0i2")))

(define-public crate-web-dom-0.0.3 (c (n "web-dom") (v "0.0.3") (h "0p761pn4wp7c4jy2hafldzhsmfl67zgjibbh823wy1wcbd3fnhcw")))

(define-public crate-web-dom-0.0.4 (c (n "web-dom") (v "0.0.4") (h "12h6pi8wp16zinlyh0bj2v820q9f4x6ydaf74dgr3abqbbp73nws")))

(define-public crate-web-dom-0.0.5 (c (n "web-dom") (v "0.0.5") (h "1g8749lp0bkhjkznq0vdsimb1wvc7jqiirmn7jgak4dsmhh6ni4c")))

(define-public crate-web-dom-0.0.6 (c (n "web-dom") (v "0.0.6") (h "0idmmkq2vgv6ij0hy9bmi8ymkm64xc04a2j079nyvf48z7k0n5iz")))

(define-public crate-web-dom-0.0.7 (c (n "web-dom") (v "0.0.7") (h "0izh929jpi3apd9b5l8c9hrcq8aa3fmadim8q1prhwdg4j671lnv")))

(define-public crate-web-dom-0.0.8 (c (n "web-dom") (v "0.0.8") (h "11vcanhbjsp6a6h28m7090b12z08pmbhlrgcxqd0navx8nygjr83")))

(define-public crate-web-dom-0.0.9 (c (n "web-dom") (v "0.0.9") (h "11iin4p4bwzrvy7gh4rv57jm18n6pig9cs7dkw4xqfbrsyvs8pcs")))

(define-public crate-web-dom-0.0.10 (c (n "web-dom") (v "0.0.10") (h "0hna0wbc9d9lc31y93van8dvx9ns5xwaj00nvx895ydrinwr8y45")))

(define-public crate-web-dom-0.0.12 (c (n "web-dom") (v "0.0.12") (h "1xvw0l40b8sak0lq8xf4dcavvkxfgj867vclnr5a1szsg1m4bdhm")))

(define-public crate-web-dom-0.0.14 (c (n "web-dom") (v "0.0.14") (h "1jmbv1xap1ifdx6jbjxihk84v3r96kyvr3vizxdvbrqzh0d8s5s8")))

(define-public crate-web-dom-0.0.15 (c (n "web-dom") (v "0.0.15") (h "068s7y6sl2xlcj4framvyrn59l7m0v0c3rfn63k7vjgd57c6vlpc")))

(define-public crate-web-dom-0.0.17 (c (n "web-dom") (v "0.0.17") (h "1lxqk0ifs8igxddawdh1gnxkl7c468j6s1rjjcqpb1cd1vx1zlxn")))

(define-public crate-web-dom-0.0.18 (c (n "web-dom") (v "0.0.18") (h "152g7l9ds3zbgz7gypcj301fkdy09nr6c5habvwnw8l9qf4cz626")))

(define-public crate-web-dom-0.0.19 (c (n "web-dom") (v "0.0.19") (h "08fn931yljfivanr9nzxjfkn9cbsawi76idysff8bwx9jsjglbzv")))

(define-public crate-web-dom-0.0.20 (c (n "web-dom") (v "0.0.20") (h "1qp2zm8wlh2wkhgk73mi04cdg4ylzasffhq4fdvj840nkac2qwyd")))

(define-public crate-web-dom-0.0.21 (c (n "web-dom") (v "0.0.21") (h "0h51p0rpybnl7s09aw98902v9h606x27p2jg0lga85q5n9pmxish")))

(define-public crate-web-dom-0.0.22 (c (n "web-dom") (v "0.0.22") (h "0fdp53ckdiqbiv2rkq50230bhj4g7cv5v74md3dachv0si6ckp1s")))

(define-public crate-web-dom-0.0.23 (c (n "web-dom") (v "0.0.23") (h "0rg5axaxfaf1p2kmaaad6cs8j31qjivqp0dvywwrnh3d86xqyvdj")))

(define-public crate-web-dom-0.0.24 (c (n "web-dom") (v "0.0.24") (h "1ji6bgpm9zbc0xqp7xxvsps4ds2wska35h31r2p6b7f957ab618k")))

(define-public crate-web-dom-0.0.26 (c (n "web-dom") (v "0.0.26") (h "11xxhvqihrdg8s72264anw43rmwribi1kzcjswy7ks7vspb0v06y")))

(define-public crate-web-dom-0.0.27 (c (n "web-dom") (v "0.0.27") (h "0mld0r2san0xzlscfh8wa1c0vic44mfs6g36f8j4api2bhffjgxa")))

(define-public crate-web-dom-0.0.28 (c (n "web-dom") (v "0.0.28") (h "1gldxfnx4iqlbrfahk87f1wa6ffywsjjvy7xggfvvhpn6b3j4z7m")))

(define-public crate-web-dom-0.0.29 (c (n "web-dom") (v "0.0.29") (h "0pzzbmnglp8nmdrmy6cj6kpahgwf1z8pbm0bc99z0c0d2afpaabm")))

(define-public crate-web-dom-0.1.0 (c (n "web-dom") (v "0.1.0") (h "13m89lpxikf2pfs74kn3cwjgwmjjknirpk84iqvv07sra80hz6fr")))

(define-public crate-web-dom-0.1.1 (c (n "web-dom") (v "0.1.1") (h "0p9p2fycx8qp4g0vdil2pwd2c9c4vv52xh2v5np1km0hk1gjjgll")))

(define-public crate-web-dom-0.1.2 (c (n "web-dom") (v "0.1.2") (h "1lpy30nkdpfyaf1p5bh7pngcrkpjvb5s0cz1kk05c4qsmbs094fx")))

(define-public crate-web-dom-0.1.3 (c (n "web-dom") (v "0.1.3") (h "1i5n62bgdylpnpv39vs9v47jfl0y9c760qpc484ibdda0kfq15w1")))

(define-public crate-web-dom-0.2.0 (c (n "web-dom") (v "0.2.0") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "1d87x3zkq273awphvnsaxhfh60dlw56l4jnwpwx5i4n6gd0w472s")))

(define-public crate-web-dom-0.2.1 (c (n "web-dom") (v "0.2.1") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "15zdbfsr7psv89q39apjk4ij92ydsmh61flvzfpx509cd9fzd0lr")))

(define-public crate-web-dom-0.2.2 (c (n "web-dom") (v "0.2.2") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "15a4pwx37ksbc7pcbf76zpqdb419z0jhn8xv7ihsmp272xp4vddi")))

(define-public crate-web-dom-0.2.3 (c (n "web-dom") (v "0.2.3") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "1ai1biwpvsxhdvvgdblr3c8pmzhvia0788g8j856lpynz6dhq4lc")))

(define-public crate-web-dom-0.2.4 (c (n "web-dom") (v "0.2.4") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "0xs51bmy2pjq4lv3lg5kl8v0paa5235hqb85x9r7gv113dz7vfpf")))

(define-public crate-web-dom-0.3.0 (c (n "web-dom") (v "0.3.0") (d (list (d (n "callback") (r "^0.5.0") (d #t) (k 0)) (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "0hl8cqamads95bsndx70g18ivg0pvcnc97ga23i5q2h08jqz4wad")))

(define-public crate-web-dom-0.3.1 (c (n "web-dom") (v "0.3.1") (d (list (d (n "callback") (r "^0.5.0") (d #t) (k 0)) (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "1mdmbp7gyvwk1vfbfnlih4ac37hqfhg6rg6iydlh5xx54z09ap8q")))

(define-public crate-web-dom-0.3.2 (c (n "web-dom") (v "0.3.2") (d (list (d (n "callback") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "10na9vavmslgafx32lj1fpxnbhyz472f6had8bf3cynb8czw1qkz")))

(define-public crate-web-dom-0.3.4 (c (n "web-dom") (v "0.3.4") (d (list (d (n "callback") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "1yr062k749vz93vyry394bbl4yvp2gp9794lwnqyy4i0kvf9mqgj")))

(define-public crate-web-dom-0.3.5 (c (n "web-dom") (v "0.3.5") (d (list (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "1y6sx05r5n33mvah2xy0d217zxpx0xp1rp86zaq67kiwrrsgmx26")))

(define-public crate-web-dom-0.3.6 (c (n "web-dom") (v "0.3.6") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "web_common") (r "^0") (d #t) (k 0)))) (h "0k840vjznfaia3pn0cjz5mskdmbx30g2c5qwi8ccch530j2cnk6n")))

(define-public crate-web-dom-0.3.7 (c (n "web-dom") (v "0.3.7") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "web_common") (r "^0") (d #t) (k 0)))) (h "09li9vbzpcb9hpybafbij0s8qj194ig3gaanp86wdvmb8vmlqr9w")))

(define-public crate-web-dom-0.3.8 (c (n "web-dom") (v "0.3.8") (d (list (d (n "js") (r "^0") (d #t) (k 0)) (d (n "web_common") (r "^0") (d #t) (k 0)))) (h "1g2519p5c5vxqm8wb5zd7p8p17668plks800h0zsxlx8zcdwrll6")))

(define-public crate-web-dom-0.3.9 (c (n "web-dom") (v "0.3.9") (d (list (d (n "js") (r "^0") (d #t) (k 0)) (d (n "web_common") (r "^0") (d #t) (k 0)))) (h "071i4na791xp20wdzda2gq98hqzgb1g6qr1lyyjfjk2rmgvy12k6")))

(define-public crate-web-dom-0.3.10 (c (n "web-dom") (v "0.3.10") (d (list (d (n "js") (r "^0") (d #t) (k 0)) (d (n "web_common") (r "^0") (d #t) (k 0)))) (h "05fcmy0p67ah7m2rlsb77yhpywpjjlzvmfr07ych9w5197d11cbm")))

