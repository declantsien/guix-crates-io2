(define-module (crates-io we b- web-sys-query-derive) #:use-module (crates-io))

(define-public crate-web-sys-query-derive-0.0.1-alpha.0 (c (n "web-sys-query-derive") (v "0.0.1-alpha.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "1n4mi301qzkaw57mlvfg4x39k7fjqj6xmx3akv287ilmd0vvyn6j")))

(define-public crate-web-sys-query-derive-0.0.1-alpha.1 (c (n "web-sys-query-derive") (v "0.0.1-alpha.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "0mnciplgln7ysxszkpaqqlkcrciwnw4lnzy82i29v5gjj8s8ynnp")))

(define-public crate-web-sys-query-derive-0.0.1-alpha.5 (c (n "web-sys-query-derive") (v "0.0.1-alpha.5") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)))) (h "0p2jav5wwnmhx0a2c7x5wmq5czivnx5kim1af9qq1c24vyif25rp")))

