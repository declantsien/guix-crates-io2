(define-module (crates-io we b- web-toolkit) #:use-module (crates-io))

(define-public crate-web-toolkit-0.1.0 (c (n "web-toolkit") (v "0.1.0") (d (list (d (n "argon2") (r "^0.5.0") (d #t) (k 0)) (d (n "jwt-simple") (r "^0.11.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0rkqf410s8cx3a19wkaij9wz0iknjlaak38n1krnp5356gvlh2xj")))

(define-public crate-web-toolkit-0.1.1 (c (n "web-toolkit") (v "0.1.1") (d (list (d (n "argon2") (r "^0.5.0") (d #t) (k 0)) (d (n "jwt-simple") (r "^0.11.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0yllzn8yvf5bdhp8inhb5nv0309sk5wh3n5njaqv9460n3fhwl7h")))

