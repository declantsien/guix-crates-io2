(define-module (crates-io we b- web-local-storage-api) #:use-module (crates-io))

(define-public crate-web-local-storage-api-0.0.0 (c (n "web-local-storage-api") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1k1p3ydilq19s16wacxj8rr912x9y8vhqgx64mb69hy3z08090jm")))

