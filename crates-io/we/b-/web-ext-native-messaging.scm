(define-module (crates-io we b- web-ext-native-messaging) #:use-module (crates-io))

(define-public crate-web-ext-native-messaging-0.1.0 (c (n "web-ext-native-messaging") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ckm2ygy4csjf3qznr1178r62y4q4cpdlppimmzgiw1dyf8chsww") (y #t)))

