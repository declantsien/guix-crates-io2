(define-module (crates-io we b- web-console-logger) #:use-module (crates-io))

(define-public crate-web-console-logger-0.1.0 (c (n "web-console-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "06pxdzsl75ak5anaf9xnc2agvw7is570i8ffhmimwa5vcmk9s3dm")))

(define-public crate-web-console-logger-0.1.1 (c (n "web-console-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "1gmn0n9gydk8bnw4s4v46259p3y830rvrh9aw392ni9jar3b83zk")))

(define-public crate-web-console-logger-0.1.2 (c (n "web-console-logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "175xags9fjwnpb5szra0652h8sy8kjxp0g99wvzz4xdvjj7yxwra")))

