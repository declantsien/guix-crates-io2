(define-module (crates-io we b- web-transport) #:use-module (crates-io))

(define-public crate-web-transport-0.0.1 (c (n "web-transport") (v "0.0.1") (h "0fbykp7vsn21h6zv3fzp1yily4r2ikwk67zhzh95zs6z7awrfkb5")))

(define-public crate-web-transport-0.10.0 (c (n "web-transport") (v "0.10.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "web-transport-quinn") (r "^0.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "web-transport-wasm") (r "^0.10") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0w1mvylk64rhknqidiqyllcqds5x6phy3k2scfzjwq3a3dfkl534") (y #t)))

(define-public crate-web-transport-0.1.0 (c (n "web-transport") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "web-transport-quinn") (r "^0.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "web-transport-wasm") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1va8f79644pb1wnihigyn2hhfxmlgi6gm27mg23fih12kyiw79fy")))

(define-public crate-web-transport-0.2.0 (c (n "web-transport") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "web-transport-quinn") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "web-transport-wasm") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "04mc1a4vhzz0i6gzm899mh4afh2hajqr6km2s5dyzfxrja2nypqc")))

(define-public crate-web-transport-0.2.1 (c (n "web-transport") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "web-transport-quinn") (r "^0.2.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "web-transport-wasm") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0hibsrxhhmyxixa3fpj4s087pfgsyx41icy9qdap47kbqjfmpbjy")))

(define-public crate-web-transport-0.3.0 (c (n "web-transport") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "web-transport-quinn") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "web-transport-wasm") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "14b57z74spy9gcfjicwy5g9rmpkpv1b1csrc0h9356fgnklsx4sp")))

