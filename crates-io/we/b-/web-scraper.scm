(define-module (crates-io we b- web-scraper) #:use-module (crates-io))

(define-public crate-web-scraper-0.1.0 (c (n "web-scraper") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jgpiq1dlj3fj4axfzcxnrc4iv7q9yhwhj373mb68s316i5cry2m")))

