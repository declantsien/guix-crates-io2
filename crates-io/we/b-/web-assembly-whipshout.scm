(define-module (crates-io we b- web-assembly-whipshout) #:use-module (crates-io))

(define-public crate-web-assembly-whipshout-0.9.0 (c (n "web-assembly-whipshout") (v "0.9.0") (d (list (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "1yrf33ldl0k558kffb42p5xvl4vb7licdpq34qfz0jhdlwv5lf7i")))

(define-public crate-web-assembly-whipshout-1.0.0 (c (n "web-assembly-whipshout") (v "1.0.0") (d (list (d (n "js-sys") (r "^0.3.54") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.54") (f (quote ("console"))) (d #t) (k 0)))) (h "06g10bkzy3y872d8wscqf800cfw0a27g4hshrn08nrwgw99jrqsz")))

