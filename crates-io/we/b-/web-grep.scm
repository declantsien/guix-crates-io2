(define-module (crates-io we b- web-grep) #:use-module (crates-io))

(define-public crate-web-grep-0.1.0 (c (n "web-grep") (v "0.1.0") (d (list (d (n "easy-scraper") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dc0z3sjmbp8wlmqvn09435haivl3m8cq9ihllh2j18hpa7nfapk")))

(define-public crate-web-grep-0.1.1 (c (n "web-grep") (v "0.1.1") (d (list (d (n "easy-scraper") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0css2w3wbyz9ihm2pxbxs60xc07vskrs6hxn9vdsdka4s0vb61yx")))

(define-public crate-web-grep-0.1.2 (c (n "web-grep") (v "0.1.2") (d (list (d (n "easy-scraper") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1031vv89a0jpyf2hjc7z4f2rlrabnj13k8n6lbvsillf4gvbd7mf")))

(define-public crate-web-grep-0.1.3 (c (n "web-grep") (v "0.1.3") (d (list (d (n "easy-scraper") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "19qzmafppgdsm614sj6j3gs5llwcgnd3xbfnmqpff45f4rhq7rl9")))

(define-public crate-web-grep-0.1.4 (c (n "web-grep") (v "0.1.4") (d (list (d (n "easy-scraper") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1m8jyc9hlibdr3m09a2v6cqhdvyklrdf6ql0hjw16drj3m22hbsp")))

