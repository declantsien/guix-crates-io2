(define-module (crates-io we b- web-log) #:use-module (crates-io))

(define-public crate-web-log-1.0.0 (c (n "web-log") (v "1.0.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1q8k3vj3iwcl22ra2bkk22mslicd6rq9s2dn36ckm4g9v27w738y") (f (quote (("std")))) (y #t)))

(define-public crate-web-log-1.0.1 (c (n "web-log") (v "1.0.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1lrdbizvcamnzq9w5k2p4bzfknz5nraxx43p4vrkmmpawsnkwn2p") (f (quote (("std"))))))

