(define-module (crates-io we b- web-bundler) #:use-module (crates-io))

(define-public crate-web-bundler-0.1.0 (c (n "web-bundler") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sass-rs") (r "^0.2") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0bs8jqa24qnifxbw91vmxg3g9j776ir3wwpdswhg8svcq3z7xylv")))

(define-public crate-web-bundler-0.1.1 (c (n "web-bundler") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sass-rs") (r "^0.2") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "006d087c81i2yss2xw26br8kar3h7b6mvgv1afqim7yypz0mkpn2")))

(define-public crate-web-bundler-0.1.2 (c (n "web-bundler") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sass-rs") (r "^0.2") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1ldcw70d5szmw02gf8rdhhj7k176vdaxsc7g6inwgnx69dhg4nbw")))

(define-public crate-web-bundler-0.1.3 (c (n "web-bundler") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sass-rs") (r "^0.2") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasm-pack") (r "^0.9.1") (d #t) (k 0)))) (h "1xscxz550m7g3xbg5z01ynyvxl973jrgb1haprrglzmzk45593yf")))

(define-public crate-web-bundler-0.1.4 (c (n "web-bundler") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sass-rs") (r "^0.2") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasm-pack") (r "^0.9.1") (d #t) (k 0)))) (h "097ka9zgngrdsmsz21q4n15ynxcmmdgr4623271b6jqm8y8qd5p3")))

(define-public crate-web-bundler-0.2.0 (c (n "web-bundler") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sass-rs") (r "^0.2") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "wasm-pack") (r "^0.12") (d #t) (k 0)))) (h "0lxizm21v181hmdzlwgmc6xjkk1d43cvr5qzg5zh07gpkx36x9yk")))

