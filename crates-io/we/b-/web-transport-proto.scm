(define-module (crates-io we b- web-transport-proto) #:use-module (crates-io))

(define-public crate-web-transport-proto-0.6.1 (c (n "web-transport-proto") (v "0.6.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "08b9v7wgw0vb51yp6k01djr8z0r379kxmzvxxgizjkq249qz30sq") (y #t)))

(define-public crate-web-transport-proto-0.1.0 (c (n "web-transport-proto") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1rladqzka1pcxqa6pnh8qfl8f9fwfk937dd42znba1sjahinrdx8")))

(define-public crate-web-transport-proto-0.1.1 (c (n "web-transport-proto") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "154bs4bsy96gqsv337l8lqnnw2xrwbzj9lbls18ninymy6dgscym")))

(define-public crate-web-transport-proto-0.2.0 (c (n "web-transport-proto") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1jp0mqys06ddl0s2jxz85yss9aihqzss000wfjwwx468akvj42fz")))

