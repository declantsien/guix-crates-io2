(define-module (crates-io we b- web-assembler) #:use-module (crates-io))

(define-public crate-web-assembler-0.1.0 (c (n "web-assembler") (v "0.1.0") (h "0yhdhcv5b28vr5vmd77y9v8nn4lxq5fh4crqabc1r1k7whq46w2q")))

(define-public crate-web-assembler-0.1.1 (c (n "web-assembler") (v "0.1.1") (h "0jjc9xsz46wmivqmsv60461hkclva32pp1xryivsym2q1jixq8an")))

(define-public crate-web-assembler-0.1.2 (c (n "web-assembler") (v "0.1.2") (h "0qnh4aqg0lysfvdwbwg0spgmkgpam1mqx4p8wwr5l3jd543d0d14")))

