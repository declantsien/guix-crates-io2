(define-module (crates-io we b- web-arena-indigo) #:use-module (crates-io))

(define-public crate-web-arena-indigo-0.1.0 (c (n "web-arena-indigo") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1pskl9fh5n38cpi2pxirs0mqs6gilahv9xwinhsw5qxrq6wjpz4s")))

