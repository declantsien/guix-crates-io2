(define-module (crates-io we b- web-sys-main-loop) #:use-module (crates-io))

(define-public crate-web-sys-main-loop-0.1.0 (c (n "web-sys-main-loop") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "HtmlCanvasElement" "WebGl2RenderingContext" "KeyboardEvent" "MouseEvent"))) (d #t) (k 0)))) (h "0ljy2rxdihgaq0c9g94ayrw85m67vdyg5fp22l5dnr1q7j1g4j33")))

(define-public crate-web-sys-main-loop-0.1.1 (c (n "web-sys-main-loop") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "HtmlCanvasElement" "WebGl2RenderingContext" "KeyboardEvent" "MouseEvent"))) (d #t) (k 0)))) (h "1nwmmghpkb1z7aacyqq95j96krgnd9jqf9zccbjlbrldygqqdjxz")))

(define-public crate-web-sys-main-loop-0.1.2 (c (n "web-sys-main-loop") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "HtmlCanvasElement" "WebGl2RenderingContext" "KeyboardEvent" "MouseEvent"))) (d #t) (k 0)))) (h "0nj9bivjn4vpvnwnh28k0ki9wy22pkvch9k4ms9h8509hfh1mmly")))

(define-public crate-web-sys-main-loop-0.1.3 (c (n "web-sys-main-loop") (v "0.1.3") (d (list (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "HtmlCanvasElement" "WebGl2RenderingContext" "KeyboardEvent" "MouseEvent"))) (d #t) (k 0)))) (h "1mybacalhwzvs4h31jy4vz7yxyiw2nrxs1fk5aca3chk9ybx47r2")))

(define-public crate-web-sys-main-loop-0.1.4 (c (n "web-sys-main-loop") (v "0.1.4") (d (list (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "HtmlCanvasElement" "WebGl2RenderingContext" "KeyboardEvent" "MouseEvent"))) (d #t) (k 0)))) (h "1w1g3as44afaaq8wz2z7xyrbjb4lkfhbm76zpybmkjf8hk9dwyg3") (y #t)))

(define-public crate-web-sys-main-loop-0.1.5 (c (n "web-sys-main-loop") (v "0.1.5") (d (list (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "HtmlCanvasElement" "WebGl2RenderingContext" "KeyboardEvent" "MouseEvent"))) (d #t) (k 0)))) (h "0c7sjky30x74w879qz61zkq5ncifvg5gaf5npsi8z4x4yw8ddxfs")))

(define-public crate-web-sys-main-loop-0.1.6 (c (n "web-sys-main-loop") (v "0.1.6") (d (list (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "HtmlCanvasElement" "WebGl2RenderingContext" "KeyboardEvent" "MouseEvent"))) (d #t) (k 0)))) (h "1gnbmv4vd0fhrh7mxqkh72b5f88ah2ksy5qg6qif811y32m79ng8")))

(define-public crate-web-sys-main-loop-0.1.7 (c (n "web-sys-main-loop") (v "0.1.7") (d (list (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "HtmlCanvasElement" "WebGl2RenderingContext" "KeyboardEvent" "MouseEvent"))) (d #t) (k 0)))) (h "1c2nw6q95rbzn77jlc79kxwjhm1fg16nanwf7ix5jcir34ws9r27")))

(define-public crate-web-sys-main-loop-0.1.8 (c (n "web-sys-main-loop") (v "0.1.8") (d (list (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Document" "Window" "HtmlCanvasElement" "WebGl2RenderingContext" "KeyboardEvent" "MouseEvent"))) (d #t) (k 0)))) (h "1yhqqax96c6w869645d49cdkby3wrgxakdh1acnbj9aycr4skplw")))

