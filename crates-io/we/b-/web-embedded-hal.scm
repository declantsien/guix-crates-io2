(define-module (crates-io we b- web-embedded-hal) #:use-module (crates-io))

(define-public crate-web-embedded-hal-0.1.0 (c (n "web-embedded-hal") (v "0.1.0") (d (list (d (n "critical-section") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-1") (r "^1.0.0-alpha.8") (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-async") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "1s6qwfh1issl8jln2mlhh53i5rw8prg3rj05wkcffad21ink6zcg")))

