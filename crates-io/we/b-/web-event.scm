(define-module (crates-io we b- web-event) #:use-module (crates-io))

(define-public crate-web-event-0.1.0 (c (n "web-event") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.55") (f (quote ("InputEvent" "KeyboardEvent" "Location" "Storage" "MouseEvent" "PointerEvent" "EventTarget"))) (d #t) (k 0)))) (h "1hih6l83lvmhyxlgy73yrbl89sz2lvc6q63936m4fwv24db4cnyp")))

(define-public crate-web-event-0.1.1 (c (n "web-event") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.55") (f (quote ("InputEvent" "KeyboardEvent" "Location" "Storage" "MouseEvent" "PointerEvent" "EventTarget"))) (d #t) (k 0)))) (h "0l43w0w9s04mx32lkd0q1hak2a1hrnzwnynykgbjkibw41y6gsis")))

(define-public crate-web-event-0.1.2 (c (n "web-event") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.55") (f (quote ("InputEvent" "KeyboardEvent" "Location" "Storage" "MouseEvent" "PointerEvent" "EventTarget" "DataTransfer"))) (d #t) (k 0)))) (h "1kc49mb6ky7bvmmhfggfxkn1mkglcf3jkd77bnz5j1qnrbdm716d")))

(define-public crate-web-event-0.1.3 (c (n "web-event") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.55") (f (quote ("Node" "Window" "InputEvent" "KeyboardEvent" "Location" "Storage" "MouseEvent" "PointerEvent" "EventTarget" "DataTransfer"))) (d #t) (k 0)))) (h "0lch2ggd63c2gmfv56w56jy1wi0d46x6kzp9bwsfn9gm5l8vlg5n")))

