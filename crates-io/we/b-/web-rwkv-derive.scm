(define-module (crates-io we b- web-rwkv-derive) #:use-module (crates-io))

(define-public crate-web-rwkv-derive-0.2.0 (c (n "web-rwkv-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0prj6pqz2ggv1wkif01y4xyigi8i5zxr2cdpcq6gqyjp0n7yclvq")))

(define-public crate-web-rwkv-derive-0.2.1 (c (n "web-rwkv-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "08hzwx8sqqw4scw5yhmrw72aih23z8yfxkvxynna2lfrgyxs8iin")))

(define-public crate-web-rwkv-derive-0.2.2 (c (n "web-rwkv-derive") (v "0.2.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1b24xk2a2yr52j6dmhb2q1aczvx99wzd1v2kpiqzx402xi7x7wp9")))

(define-public crate-web-rwkv-derive-0.2.3 (c (n "web-rwkv-derive") (v "0.2.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0l9mcmfgsh2h0lfk61i2gi7954kdsqnyzrkcz6iwdiiqdnfvbiw8")))

(define-public crate-web-rwkv-derive-0.2.4 (c (n "web-rwkv-derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1gcg69lflvrd1pg00j77dad786pqm18bidyzrilz9zx9kfc3gr76")))

