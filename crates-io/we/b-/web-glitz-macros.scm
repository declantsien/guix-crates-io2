(define-module (crates-io we b- web-glitz-macros) #:use-module (crates-io))

(define-public crate-web-glitz-macros-0.1.0 (c (n "web-glitz-macros") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1l2d1f04g7w12rc8l7d843vzh7dvrpxnj7sdkvyhmg72imcr6qqn")))

(define-public crate-web-glitz-macros-0.2.0 (c (n "web-glitz-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1xw9nqxixbd91v1nirsvms0w8bfrfwd25pv96fw708b625iw1zqn")))

