(define-module (crates-io we b- web-archive) #:use-module (crates-io))

(define-public crate-web-archive-0.1.0 (c (n "web-archive") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1zvkjdl6k33qdk9qf4hmjr2k0gys1nrwqpacz1clqrd6b2ichnv3") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

(define-public crate-web-archive-0.2.0 (c (n "web-archive") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1v3jaf0967ryqjz9gc424892k97b9fb6y1g2chf6b9ghwbmwbma0") (f (quote (("default" "blocking") ("blocking" "reqwest/blocking"))))))

