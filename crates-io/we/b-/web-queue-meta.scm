(define-module (crates-io we b- web-queue-meta) #:use-module (crates-io))

(define-public crate-web-queue-meta-0.1.0 (c (n "web-queue-meta") (v "0.1.0") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "191zj1nfaj0w2vjjlnw4glm0jj3ayihfi2x9ralj5f55p0ibwils")))

(define-public crate-web-queue-meta-0.1.1 (c (n "web-queue-meta") (v "0.1.1") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "104qixapylp28x6xsmqkdjbzvz00vmydzzr8nq84lkkd1fgx8bsg")))

