(define-module (crates-io we b- web-scraper-flows) #:use-module (crates-io))

(define-public crate-web-scraper-flows-0.1.0 (c (n "web-scraper-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0nchmrbw45kbg5y5nz5rwysf56n30q1am71mwh8ps36cc2660afl")))

