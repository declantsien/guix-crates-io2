(define-module (crates-io we b- web-crawler) #:use-module (crates-io))

(define-public crate-web-crawler-0.1.2 (c (n "web-crawler") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0dnp3nazigwidnpx7qh9rlj9497c5pq52bmv61ycipmd5cqmpdwy")))

