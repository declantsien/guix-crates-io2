(define-module (crates-io we b2 web2app) #:use-module (crates-io))

(define-public crate-web2app-0.2.5 (c (n "web2app") (v "0.2.5") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1lqxjih3id0np9n3ss24gybdig78vw4hnkd307g6wp5s58iizc66")))

(define-public crate-web2app-0.2.6 (c (n "web2app") (v "0.2.6") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0qrrsxj76hcgi78gb1f769xz61n8xbwdaz9pj9w3wlhpjp4kppyn")))

(define-public crate-web2app-0.2.7 (c (n "web2app") (v "0.2.7") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1m0zbpilyj5sf8c7bbba86gm303jf6xpmpsbyzn6qq2484faiwmc")))

(define-public crate-web2app-0.2.8 (c (n "web2app") (v "0.2.8") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "03qcmz9k5dxisc9qdb5wvsmyfc6z03plch84q6mwj89f3c4rxcy3")))

(define-public crate-web2app-0.2.9 (c (n "web2app") (v "0.2.9") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1ab8s8fbbqsddwagmgvg0hy8zf9rzgi27491slm9p0lkcilfpmd4")))

(define-public crate-web2app-0.3.0 (c (n "web2app") (v "0.3.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1zyfx0mlh6mzfs60s0z3vj2qmqqqj0p48alw4b7b0pd92zmddgbi") (y #t)))

(define-public crate-web2app-0.2.10 (c (n "web2app") (v "0.2.10") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "10sdbh1wpj12ky3710n6dsa4xbq5f6v9i5w6lm61r4b7429i4m1w")))

(define-public crate-web2app-0.2.11 (c (n "web2app") (v "0.2.11") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0ww69gbawy0inzy7si9m0b6gx9msyd6r9v4860lf4dwh5sbbg8vb")))

(define-public crate-web2app-0.2.12 (c (n "web2app") (v "0.2.12") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0hpif2fpvzy1yggsp13xnhjiqdxk95393djxznihk257by8d5jxr")))

