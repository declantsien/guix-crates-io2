(define-module (crates-io we ng wenger) #:use-module (crates-io))

(define-public crate-wenger-0.0.0 (c (n "wenger") (v "0.0.0") (h "1hykdg2gx2wj9qqlwgmc2fd8xxrq1kiw3yf8ap0lvm0ncj3j3j12")))

(define-public crate-wenger-0.0.1 (c (n "wenger") (v "0.0.1") (h "0rnwvfhkgbk24j6zf3ivb51xcy2f8m89chz4gmxn0178hrx2bnyi")))

(define-public crate-wenger-0.0.2 (c (n "wenger") (v "0.0.2") (h "1njd56x4hnm6w04gddxc5am5wz5rczpr5xdz6gpm0q4077vpnk8n")))

(define-public crate-wenger-0.0.3 (c (n "wenger") (v "0.0.3") (h "00xj9xalfrxn8ymlrli6nhsf92ggkz4b1bdpcjwwc7p0dvw2mk2k")))

