(define-module (crates-io we ix weixin_rust) #:use-module (crates-io))

(define-public crate-weixin_rust-0.1.0 (c (n "weixin_rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cc9rm4ykgsqym7vpysvsx9grmchjpny3ssp2c0869a9mhjmhx93") (y #t)))

(define-public crate-weixin_rust-0.1.1 (c (n "weixin_rust") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zq6816rwkd3b1hrf7j264dk8y9ashm88p6h2xzbqrj57gbmr49w") (y #t)))

(define-public crate-weixin_rust-0.1.2 (c (n "weixin_rust") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xlg875m0r4g9nbmlwgk569pdbfhkh4djfq0c8wdw2rv1blavbz6")))

(define-public crate-weixin_rust-0.1.3 (c (n "weixin_rust") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yhqgdl9gjzqikd4b9ajblddxqslzr12s5xdqmdg9xg8cavhbac5")))

(define-public crate-weixin_rust-0.1.4 (c (n "weixin_rust") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a7gfq4gakrhknw4wwgaj4817j7ybg5c4giqy47j6lik3244wzmi")))

(define-public crate-weixin_rust-0.1.5 (c (n "weixin_rust") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0aa8zjmcy6an91g6sm0yjyp6vi6c2bsmdq26q0c7kqiasjnbdl")))

(define-public crate-weixin_rust-0.1.6 (c (n "weixin_rust") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12cjm0ls9kcz5a2wr9cy50qrd34ld21dbwc1dmf2p4a5d0gx1ncx")))

