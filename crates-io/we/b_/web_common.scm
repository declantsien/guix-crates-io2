(define-module (crates-io we b_ web_common) #:use-module (crates-io))

(define-public crate-web_common-0.0.1 (c (n "web_common") (v "0.0.1") (h "0asf41dfw7aysxj2h7jqqnngvisicq4bh22z45yi2jrm080n0c0f")))

(define-public crate-web_common-0.0.2 (c (n "web_common") (v "0.0.2") (h "0985f9in02a8l6vjak0x9fs9pyq5ram1d3h10g3gdbdr6lvzsyci")))

(define-public crate-web_common-0.0.3 (c (n "web_common") (v "0.0.3") (h "002rw46l41gwmh8kprcjxk3plabkkdhnymkvcryr1625fmhi4ycn")))

(define-public crate-web_common-0.0.4 (c (n "web_common") (v "0.0.4") (h "1rbz8dl97jq67g66rgmy2ccrln96b9a73lxgmycbav3w8a0sk02k")))

(define-public crate-web_common-0.0.5 (c (n "web_common") (v "0.0.5") (h "0j4n0v02h5ddryks8c4ga2an1grbwrdq9kpnih4dspwffvi86wp3")))

(define-public crate-web_common-0.0.6 (c (n "web_common") (v "0.0.6") (h "0dlhvdvghndiayqv43sr16fzrx4iqch30hsil5jzy7r61vmv51kb")))

(define-public crate-web_common-0.0.7 (c (n "web_common") (v "0.0.7") (h "1n47fw3m2grkrvrzkg8fif3amzg5wpvg89dxfd652azmvl0csjmy")))

(define-public crate-web_common-0.1.0 (c (n "web_common") (v "0.1.0") (h "1rm02qns6ymw8bbc5mfcqwnqylnh6pks19imi5qf83g9mg5im50y")))

(define-public crate-web_common-0.2.0 (c (n "web_common") (v "0.2.0") (d (list (d (n "web_console") (r "^0") (d #t) (k 0)) (d (n "web_random") (r "^0") (d #t) (k 0)) (d (n "web_timer") (r "^0") (d #t) (k 0)))) (h "0kx6ywy1vlgwc81b1b5p7zlqcs1kfp669szgp81sf1cx1mvhmxnq")))

(define-public crate-web_common-0.3.0 (c (n "web_common") (v "0.3.0") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "1fkhppv57nvnildcz0n7q925j8iys2rldf2n4rpvbina919w3gld")))

(define-public crate-web_common-0.3.1 (c (n "web_common") (v "0.3.1") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "1wm22f99ixranb4c0hw5wm1y46w21vpq0i3q0sk4sjc4iahjrl75")))

(define-public crate-web_common-0.3.2 (c (n "web_common") (v "0.3.2") (d (list (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "192dddandnr1fv7x0ylj4f8ck3xxw620m0h17agw2wgmi3iirj2s")))

(define-public crate-web_common-0.3.3 (c (n "web_common") (v "0.3.3") (d (list (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "1866b9jc6x2vanydpgx6a536vfyr0paf73sfpz7fpfyrl0h875kk")))

(define-public crate-web_common-0.4.0 (c (n "web_common") (v "0.4.0") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)))) (h "0s850vpaiynvfmd9kfv1nd7s15944raicxda1r060bjj2wlpiskj")))

(define-public crate-web_common-0.4.1 (c (n "web_common") (v "0.4.1") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)))) (h "0s5crpn9zb7af0qy66yndaa0jb4yibsyn9wkmphm5y7fz93pbvch")))

(define-public crate-web_common-0.4.2 (c (n "web_common") (v "0.4.2") (d (list (d (n "js") (r "^0") (d #t) (k 0)))) (h "09nsbj5r1bpn4h09aqkfi7m9x4gbbqdax263n2dy13zpynq988nn")))

(define-public crate-web_common-0.4.3 (c (n "web_common") (v "0.4.3") (d (list (d (n "js") (r "^0") (d #t) (k 0)))) (h "0saw3612czy0089am0wyrmkj13jxjnqwygb39kpnii4cqpssbb5r")))

