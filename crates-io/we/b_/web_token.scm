(define-module (crates-io we b_ web_token) #:use-module (crates-io))

(define-public crate-web_token-0.1.0 (c (n "web_token") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "1bxmpi73nj440x5hx34fhr3m3hi4s6njnkhma1nv4bp6mazjqkhk")))

(define-public crate-web_token-0.1.1 (c (n "web_token") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "0mkdq4c0jq17ddsy5qd7qrrc0ij57qc7w66s0y98bbn1xrxbmc6v")))

(define-public crate-web_token-0.2.0 (c (n "web_token") (v "0.2.0") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 2)))) (h "1kj1xs8qj9618kb29x30h4r3g9qaix2kcvg4h2w3g5n4aqmxn7ga") (f (quote (("serde_support" "serde" "serde_derive"))))))

(define-public crate-web_token-0.3.0 (c (n "web_token") (v "0.3.0") (d (list (d (n "diesel") (r "^1.3.2") (o #t) (d #t) (k 0)) (d (n "diesel-derive-newtype") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 2)))) (h "12h7hwm30m374h41rfpgsydqf43bipqiyqdmpiycxmx6lvk70c54") (f (quote (("serde_support" "serde" "serde_derive") ("diesel_support" "diesel" "diesel-derive-newtype"))))))

(define-public crate-web_token-1.0.0 (c (n "web_token") (v "1.0.0") (d (list (d (n "diesel") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "diesel-derive-newtype") (r "^2.0.0-rc.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 2)))) (h "02af7wm63y7gk53sd63i9hjlrclbnm1y2y3g8s6gj661f2ns5pan") (f (quote (("serde_support" "serde" "serde_derive") ("diesel_support" "diesel" "diesel-derive-newtype"))))))

