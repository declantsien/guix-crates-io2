(define-module (crates-io we b_ web_instant) #:use-module (crates-io))

(define-public crate-web_instant-0.1.0 (c (n "web_instant") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0pwbmkvjd9gy68sdpg63g8566q5yrm1v56xnzy73b328n28w1jzm")))

(define-public crate-web_instant-0.2.0 (c (n "web_instant") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "091srdrfg2qbbsad92l6231y9y9xck3rbq8c70is9h1hsz52h64w")))

(define-public crate-web_instant-0.2.1 (c (n "web_instant") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1plh6bqn00zk9h92993awkqcx33xa74jpih73402n18bmyayp7zv")))

(define-public crate-web_instant-0.3.0 (c (n "web_instant") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0pys816q4rsf5ir00mzj0h3mggg1sng4xy99g176qhpli66ip5sf")))

