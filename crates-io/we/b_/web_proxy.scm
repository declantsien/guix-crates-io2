(define-module (crates-io we b_ web_proxy) #:use-module (crates-io))

(define-public crate-web_proxy-0.1.0 (c (n "web_proxy") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "hyper") (r "^1.0.0-rc.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0m7yjf6a45wy62xmd7cwr9zkl3100hss9hy1axw5jqhw6k9c1753") (r "1.68.0")))

