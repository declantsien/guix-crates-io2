(define-module (crates-io we b_ web_server) #:use-module (crates-io))

(define-public crate-web_server-0.1.0 (c (n "web_server") (v "0.1.0") (h "0c0rj7l9dzfqczyw42b71n99v2phfvx2hwic683vj86rb4zhqx9z")))

(define-public crate-web_server-0.1.1 (c (n "web_server") (v "0.1.1") (h "1h4k604d3l6ka4g7jw0aab0d3lww5k53jxp3kyx14iyzix3107qp")))

(define-public crate-web_server-0.1.11 (c (n "web_server") (v "0.1.11") (h "0cbg75qfnhqibfwlsc0y07h4k7a1za5cxqpvp7zaaqqlr0p57cwn")))

(define-public crate-web_server-0.1.2 (c (n "web_server") (v "0.1.2") (h "0v7jajxqzd7bxi0mi6dspfkbkwm9m8wg5ad1vp99pnhbqhjrwhsz")))

(define-public crate-web_server-0.1.20 (c (n "web_server") (v "0.1.20") (h "1k6bcxzlxaiblfz4dak38nmmx3bak3adcrhmlgjws2ypd760p9m5")))

(define-public crate-web_server-0.1.30 (c (n "web_server") (v "0.1.30") (h "128x3jh7v7dl52dwys1wjj0qcgski5g15xi51iq1j5v0m8fnr9vq")))

(define-public crate-web_server-0.2.1 (c (n "web_server") (v "0.2.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "04aynpiv3451jn131aw5mcypkzgb9i21qdql1iq5rqj2gwf9q34s")))

(define-public crate-web_server-0.2.2 (c (n "web_server") (v "0.2.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "10fifkz31c94qj70dhjn84rrnp0wn6grivdl15m4i8laiyhvryz1")))

(define-public crate-web_server-0.3.0 (c (n "web_server") (v "0.3.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "0adkay3azmcr9grp682sky7n45f3piavgmnzdcv11jmljawx6wbg")))

(define-public crate-web_server-0.3.1 (c (n "web_server") (v "0.3.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "08jw6ybk9in3b5zb71a9pmzpar07djymh6ahf4w9sp1m3hd33bi5")))

(define-public crate-web_server-0.3.2 (c (n "web_server") (v "0.3.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "0w8v1xs1v926877j7dz2mfvs05717w9kg89vdj9ml71h8rdnw89s")))

(define-public crate-web_server-0.3.3 (c (n "web_server") (v "0.3.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "16a14hzmh8mmkkc9bp6rhw9wy3awjndj55x1pswj2llpkrcq34nb")))

(define-public crate-web_server-0.3.4 (c (n "web_server") (v "0.3.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "17fzwrwznc6yx0yyqlwq6y59867f3zacwh9kzznpqg904ygzrz06")))

(define-public crate-web_server-0.3.41 (c (n "web_server") (v "0.3.41") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "1acy2f86821253fccwmnld1wfw4iy4yg8p6j4gn17khdb5rk6j55")))

(define-public crate-web_server-0.4.0 (c (n "web_server") (v "0.4.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "1gk7w8l3ci9gxm7df85il4pfbskfqnmna6d25m5y74mibv1md4m4")))

(define-public crate-web_server-0.4.1 (c (n "web_server") (v "0.4.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "10x6i35mg3fjsv2v8nr3ymvwffj713ihm1h63pdqz7dafq546kb8")))

(define-public crate-web_server-0.4.2 (c (n "web_server") (v "0.4.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy"))) (k 2)))) (h "10k6nqmawh508il44kd58y93k15hlkcfjp9761fy636a52mjc0vj")))

