(define-module (crates-io we b_ web_canvas) #:use-module (crates-io))

(define-public crate-web_canvas-0.0.0 (c (n "web_canvas") (v "0.0.0") (h "0hpagv3yab2ns5pb9rgix9zsjvzb4bglcz4nh6qmckciid817g43")))

(define-public crate-web_canvas-0.0.1 (c (n "web_canvas") (v "0.0.1") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js") (r "^0.2") (d #t) (k 0)))) (h "1xzv4f9nz3i9psmvg2mcykrcxlclzscp5df2768mcv8xr6x63k4d")))

(define-public crate-web_canvas-0.0.2 (c (n "web_canvas") (v "0.0.2") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "0zv5w9czv711f8jz1i7hhby199qbparqa2b7cf8ada70i32kpwjb")))

(define-public crate-web_canvas-0.0.3 (c (n "web_canvas") (v "0.0.3") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "03hy0va46yj0303avaxx6j9zwq49vpi796wz806la0yz68yb3myp")))

(define-public crate-web_canvas-0.0.4 (c (n "web_canvas") (v "0.0.4") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "1xp1ic78hxgypm5k5708xd124m4cdwzbbvd7fh9151rj64dx3b8r")))

(define-public crate-web_canvas-0.0.5 (c (n "web_canvas") (v "0.0.5") (d (list (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "1j0gipy2qyfzbqar1i0dni76av9z58y42fprsmrnxwkyhkfn0r6v")))

(define-public crate-web_canvas-0.0.6 (c (n "web_canvas") (v "0.0.6") (d (list (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "06092dxvg3mlvbbqcdy5g661ad7baz9j1qgysyqq5h789jk5j81j")))

(define-public crate-web_canvas-0.0.7 (c (n "web_canvas") (v "0.0.7") (d (list (d (n "js") (r "^0") (d #t) (k 0)))) (h "02c880d8jri0n1hf2rlhfmpq4b8l3cmzmyaackzb23fdvajlv1fp")))

