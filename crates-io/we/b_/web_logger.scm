(define-module (crates-io we b_ web_logger) #:use-module (crates-io))

(define-public crate-web_logger-0.1.0 (c (n "web_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (k 0)))) (h "19b1s61lqc0x681qjmfzjw825s3bi18ihv294nl3p0vb2k1bk3d7")))

(define-public crate-web_logger-0.2.0 (c (n "web_logger") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"wasm32\", not(cargo_web)))") (k 0)))) (h "10s6qxdnsn7n6vm0z7p5sbqmk1cw0knx2xzv7yiqqwif1k1gw2pm")))

