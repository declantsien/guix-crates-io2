(define-module (crates-io we b_ web_random) #:use-module (crates-io))

(define-public crate-web_random-0.0.0 (c (n "web_random") (v "0.0.0") (d (list (d (n "js_ffi") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "017av33g61cqdwik9bc6j5fw11fvrisgzzxwlcvmgbbdrcppsnq7")))

(define-public crate-web_random-0.0.1 (c (n "web_random") (v "0.0.1") (d (list (d (n "js_ffi") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1wflzb60kv8hjy3h17w9kwj42iag39nnym37c7xa6ns5p2sayyz2")))

(define-public crate-web_random-0.0.2 (c (n "web_random") (v "0.0.2") (d (list (d (n "js_ffi") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "02jrqdjjxmmyqzsrn1wfcjphg0xkwcsmrfljlc8kp1f0qskzsgf1")))

(define-public crate-web_random-0.1.0 (c (n "web_random") (v "0.1.0") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0wbrxya9kjdl8ypm0y5yb30v3aqigzinmlgh0m897rhsjcj1q273")))

(define-public crate-web_random-0.1.1 (c (n "web_random") (v "0.1.1") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "1mcc8s2sf69qf31zqpspzy9daqdb11740h5r4vrdq1sg8a8lz89s")))

(define-public crate-web_random-0.1.2 (c (n "web_random") (v "0.1.2") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "04qb6q9al0h81b5v2dx2vd48qgm73r8mca9z6vvrjc6wvh9i0400")))

(define-public crate-web_random-0.1.4 (c (n "web_random") (v "0.1.4") (d (list (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "1jdhm6m1ajm62y5py5ly7qgd68x31fj0py9xnqjrh3f8hdr5vw8d")))

(define-public crate-web_random-0.1.5 (c (n "web_random") (v "0.1.5") (d (list (d (n "js") (r "^0") (d #t) (k 0)))) (h "1b56nwkc8d2c7g8x3lqjzcxbdshi1h94gqdvhbx8sykznj27ds8z")))

