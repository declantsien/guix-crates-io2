(define-module (crates-io we b_ web_console) #:use-module (crates-io))

(define-public crate-web_console-0.0.0 (c (n "web_console") (v "0.0.0") (d (list (d (n "js_ffi") (r "^0.0.13") (d #t) (k 0)))) (h "11jixj5ygs2lqdx4kxdm4p321xa24g3n43376c9fw9j4zqwjwisl")))

(define-public crate-web_console-0.0.2 (c (n "web_console") (v "0.0.2") (d (list (d (n "js_ffi") (r "^0.1") (d #t) (k 0)))) (h "1qk115paj04ijnsghgfalip634a60yniad38dcl2z2nkwxw17yrj")))

(define-public crate-web_console-0.0.3 (c (n "web_console") (v "0.0.3") (d (list (d (n "js_ffi") (r "^0.5") (d #t) (k 0)))) (h "0bzspyrnmq1qj0fzzcjb2xrl16bfnpy6bag2a2cy5d6zwhb56flk")))

(define-public crate-web_console-0.0.4 (c (n "web_console") (v "0.0.4") (d (list (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "0kqbfxhyg98sx39m5in53jra8h30bs3wv1cj3z9wf9b9c2chh66w")))

(define-public crate-web_console-0.1.0 (c (n "web_console") (v "0.1.0") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "01k2640dbpjg0sbxsm6hy6dd67nm28597ax176r2vrvd8hji74hp")))

(define-public crate-web_console-0.2.0 (c (n "web_console") (v "0.2.0") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "1gak1i8g57gpvnkgramss67lhclcmk95rb4s29cqzralwa0b5pa5")))

(define-public crate-web_console-0.2.1 (c (n "web_console") (v "0.2.1") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "18r84529qn7pynmc50adhzc1yas50wmcwk8sq8aim42wz8y5kp7y")))

(define-public crate-web_console-0.3.1 (c (n "web_console") (v "0.3.1") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js") (r "^0.2") (d #t) (k 0)))) (h "0cgg4b94rvjhzy3qy1hr2xhb2xlxiw2h3kkj3ry896jf82wi7i7w")))

(define-public crate-web_console-0.3.2 (c (n "web_console") (v "0.3.2") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "06qn3fsn7ljdp5ghxd6xmqixfq0yn1yn8x3g88r6kh3gmcdj8dc6")))

(define-public crate-web_console-0.3.3 (c (n "web_console") (v "0.3.3") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "0fjd3rdsc3l7lng9vk0qm6i6ic3pi6ngb1lmlr5kzn4x1ibl357g")))

(define-public crate-web_console-0.3.4 (c (n "web_console") (v "0.3.4") (d (list (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "1hbm9hsll4n52rac1gz3wcir6mb4dka94395g47qlcivw823fb23")))

(define-public crate-web_console-0.3.5 (c (n "web_console") (v "0.3.5") (d (list (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "1116fazvsfg4x5dp5xzaapza3yjdz7gj57rlwkiy79kz7w2nlz8m")))

(define-public crate-web_console-0.3.6 (c (n "web_console") (v "0.3.6") (d (list (d (n "js") (r "^0.2") (d #t) (k 0)))) (h "1p27aqgr2qk979z7bk94wvaj2vblqk2zdgl7cy5r907n9jsd5f5r")))

(define-public crate-web_console-0.3.7 (c (n "web_console") (v "0.3.7") (d (list (d (n "js") (r "^0") (d #t) (k 0)))) (h "0barnlv08bk9i8sxkmhn1081zx6n1mvmn251zmh38lgxd81wr4p1")))

