(define-module (crates-io we b_ web_panic_hook) #:use-module (crates-io))

(define-public crate-web_panic_hook-0.1.0 (c (n "web_panic_hook") (v "0.1.0") (d (list (d (n "html") (r "^0.6") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "DomException" "Element" "HtmlElement" "Window"))) (d #t) (k 0)))) (h "06parm2c9pwdgccsxib0xs1v2g77aw8cixl10n8v0di7kpvxqfmz")))

