(define-module (crates-io we b_ web_panic_report) #:use-module (crates-io))

(define-public crate-web_panic_report-0.1.0 (c (n "web_panic_report") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.69") (f (quote ("Window" "Document" "CssStyleDeclaration" "HtmlElement" "HtmlTextAreaElement" "HtmlButtonElement"))) (d #t) (k 0)))) (h "0rcd7hl1s1a2y0cgnywlblirvy682393vrg9xvmllmpr7xsq3irf")))

(define-public crate-web_panic_report-0.1.1 (c (n "web_panic_report") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "CssStyleDeclaration" "HtmlElement" "HtmlTextAreaElement" "HtmlButtonElement"))) (d #t) (k 0)))) (h "06ag8igw1vhbc455sfpijqm5addmr6vs9f96zqycbzsv2ng6l69s")))

(define-public crate-web_panic_report-0.2.0 (c (n "web_panic_report") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "CssStyleDeclaration" "HtmlElement" "HtmlTextAreaElement" "HtmlButtonElement"))) (d #t) (k 0)))) (h "1kl17rxxxsn2gm92w5kca14hqn9811xx2jn46p1h34n0f9h5i28c") (f (quote (("default-form") ("default" "default-form"))))))

