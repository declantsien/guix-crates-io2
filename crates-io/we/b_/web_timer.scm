(define-module (crates-io we b_ web_timer) #:use-module (crates-io))

(define-public crate-web_timer-0.0.0 (c (n "web_timer") (v "0.0.0") (d (list (d (n "js_ffi") (r "^0.1.0") (d #t) (k 0)))) (h "0n4fys07qsnrcpxbh38335pgxdga1gzpqd61x2ycv4d5lp6kjnkc")))

(define-public crate-web_timer-0.0.1 (c (n "web_timer") (v "0.0.1") (d (list (d (n "js_ffi") (r "^0.1.0") (d #t) (k 0)))) (h "1fpaq9sldyjdzxpm141f5n5nhqwjr0k4jsqnr65p9nmljwglw129")))

(define-public crate-web_timer-0.0.2 (c (n "web_timer") (v "0.0.2") (d (list (d (n "js_ffi") (r "^0.1.0") (d #t) (k 0)))) (h "1d8y6j7k9ri6phn1p3jbfzixas7rx58i5w26f7w9imcxzhzyq91s")))

(define-public crate-web_timer-0.1.0 (c (n "web_timer") (v "0.1.0") (d (list (d (n "js_ffi") (r "^0.1") (d #t) (k 0)))) (h "0s8vyp28qfizdfd9qjcz4q4h03dqvfb30v7kq9ap2pr6x0gil73a")))

(define-public crate-web_timer-0.1.1 (c (n "web_timer") (v "0.1.1") (d (list (d (n "js_ffi") (r "^0.1") (d #t) (k 0)))) (h "0hzbp03adj1vqiyhqcd9f51q639xzxbflykzabkaa5viglcpcsqb")))

(define-public crate-web_timer-0.1.2 (c (n "web_timer") (v "0.1.2") (d (list (d (n "js_ffi") (r "^0.5") (d #t) (k 0)))) (h "07fric5a683v3znj2qw8qz0v5jxvhkp0p1wch772pkkb534vf738")))

(define-public crate-web_timer-0.1.3 (c (n "web_timer") (v "0.1.3") (d (list (d (n "js_ffi") (r "^0.5") (d #t) (k 0)))) (h "1mj2fkmc4lrzjdxz40g6mc8djsazksb7c6i62hsf4id78wc5nkjm")))

(define-public crate-web_timer-0.1.4 (c (n "web_timer") (v "0.1.4") (d (list (d (n "js_ffi") (r "^0.6") (d #t) (k 0)))) (h "0vjy6frvyqd6qjs9v3fb7wbz25j2c9rnfki78187ijgfsh1whin0")))

(define-public crate-web_timer-0.1.5 (c (n "web_timer") (v "0.1.5") (d (list (d (n "js_ffi") (r "^0.8") (d #t) (k 0)))) (h "0q53r0jv3s9ixrhczqyi2s1rp482bsgzabdsz505dbf2z4n8xqyq")))

(define-public crate-web_timer-0.2.0 (c (n "web_timer") (v "0.2.0") (d (list (d (n "callback") (r "^0.5") (d #t) (k 0)) (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js") (r "^0.2") (d #t) (k 0)))) (h "010cl1qydg671i3wy6j64l8yjsv6mkr09q8pam4zkik9j02zbr5r")))

(define-public crate-web_timer-0.2.1 (c (n "web_timer") (v "0.2.1") (d (list (d (n "callback") (r "^0.5.0") (d #t) (k 0)) (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "0r717lpm7wiqv7vlbqvhq9xvska4d0cjh228ijafplpipiay0qgs")))

(define-public crate-web_timer-0.2.2 (c (n "web_timer") (v "0.2.2") (d (list (d (n "callback") (r "^0.5.0") (d #t) (k 0)) (d (n "js") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)))) (h "1wr2nj9jgvnnlm6majrsmwlhdxkjqk5ci2la2xl5zh0m1akndjmq")))

(define-public crate-web_timer-0.2.3 (c (n "web_timer") (v "0.2.3") (d (list (d (n "callback") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (f (quote ("spin_no_std"))) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "170pl3rs14pb06x5h15r30q4bqasqcy9aj83k8y637dcli7h31ls")))

(define-public crate-web_timer-0.2.4 (c (n "web_timer") (v "0.2.4") (d (list (d (n "js") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "1a7xr1vkg4qn1vak8j6sin3l5rhf3f9pxa5r281m3mmfpwhasynl")))

(define-public crate-web_timer-0.2.5 (c (n "web_timer") (v "0.2.5") (d (list (d (n "js") (r "^0") (d #t) (k 0)))) (h "13srbndvxg0x7bcgskw8dkldycmk32la8q1968j8x9v4q5yhkp8j")))

