(define-module (crates-io we b_ web_macro) #:use-module (crates-io))

(define-public crate-web_macro-0.0.0 (c (n "web_macro") (v "0.0.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0syy92b2yh7kmlwk36hx5hl28c746n887d4p7aarydiqsqrwvh5b")))

(define-public crate-web_macro-0.0.1 (c (n "web_macro") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1gw9p7y5fbr9fvli2k0ycrzdn842ahsvnicd3j034a8nk4za12m2")))

(define-public crate-web_macro-0.0.2 (c (n "web_macro") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1rnllz1jny6b3ndnvw8dmnzzyj29xad5kyl4v3z2qrj0zk9bidyb")))

(define-public crate-web_macro-0.0.3 (c (n "web_macro") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1q3a09a94wlskndmrzva9065wrndcar49gjlf2pqajg5cx99z492")))

