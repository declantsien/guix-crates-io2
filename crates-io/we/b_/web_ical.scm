(define-module (crates-io we b_ web_ical) #:use-module (crates-io))

(define-public crate-web_ical-0.1.0 (c (n "web_ical") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "08ldvyrd92g0gym43njwkrxm5axdjjwx0i8iililh5h107p9wdh4") (y #t)))

(define-public crate-web_ical-0.1.1 (c (n "web_ical") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "02091kik2755v0inv7piqipd1b49mm9fqvdhyciiva4iknigdj77") (y #t)))

(define-public crate-web_ical-0.1.2 (c (n "web_ical") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "08mrq5i7a8fzl5j4s8j8zwh8ib31rdbxkhp67v42sjrq749hq93p")))

