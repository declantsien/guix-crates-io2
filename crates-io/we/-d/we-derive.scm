(define-module (crates-io we -d we-derive) #:use-module (crates-io))

(define-public crate-we-derive-0.1.0 (c (n "we-derive") (v "0.1.0") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09rp0ga5kq7dblr95njpydp9rk887ibi104qyi34s2g3xv83h31c")))

(define-public crate-we-derive-0.1.1 (c (n "we-derive") (v "0.1.1") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zm34lxkl31r8j6cprxdrzak9h417kjy286k9yhb9nx2brsx7z1p")))

(define-public crate-we-derive-0.1.2 (c (n "we-derive") (v "0.1.2") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16annrghv5235ik63x21sp1v76k5xnm2y2jhl9sxmc4pbgl619fa")))

