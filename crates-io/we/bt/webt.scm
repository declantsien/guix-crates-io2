(define-module (crates-io we bt webt) #:use-module (crates-io))

(define-public crate-webt-0.0.1 (c (n "webt") (v "0.0.1") (d (list (d (n "axum") (r "^0.6.20") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0ijyfb1xdynf093621iwmx9s7rlnzpk8hpgnlrv1kqvishzskzxf") (y #t)))

(define-public crate-webt-0.0.2 (c (n "webt") (v "0.0.2") (d (list (d (n "axum") (r "^0.6.20") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0ndl8kbdvmyibr22v8al110zk4hzz4hjrfqwsw7r73sjnn9ghwzh") (y #t)))

(define-public crate-webt-0.0.3 (c (n "webt") (v "0.0.3") (d (list (d (n "axum") (r "^0.6.20") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1ibkyx6jgxciiazmkw6pia8ni2x0zl96lbs3lvgi77albhdcqhvw")))

