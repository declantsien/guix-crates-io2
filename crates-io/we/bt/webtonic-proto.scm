(define-module (crates-io we bt webtonic-proto) #:use-module (crates-io))

(define-public crate-webtonic-proto-0.1.0 (c (n "webtonic-proto") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.3") (k 0)) (d (n "http") (r "^0.2.1") (k 0)) (d (n "http-body") (r "^0.3.1") (k 0)) (d (n "prost") (r "^0.6.1") (f (quote ("prost-derive"))) (k 0)) (d (n "tonic") (r "^0.3.1") (k 0)))) (h "0k372n6m5lwbnh1kl8dh4jiqj4n1cinzqhfk5fhbh9wm9iq626ry")))

(define-public crate-webtonic-proto-0.1.1 (c (n "webtonic-proto") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0.1") (k 0)) (d (n "http") (r "^0.2.3") (k 0)) (d (n "http-body") (r "^0.4.1") (k 0)) (d (n "prost") (r "^0.7.0") (f (quote ("prost-derive"))) (k 0)) (d (n "tonic") (r "^0.4.1") (k 0)))) (h "1mpq7ip61kj8hvjxagk2j1597sqz4fyq6ahin68mb393mqv91kg3")))

