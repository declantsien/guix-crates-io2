(define-module (crates-io we bt webtransport-generic) #:use-module (crates-io))

(define-public crate-webtransport-generic-0.2.0 (c (n "webtransport-generic") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1z88x27ywpql0fbcspqgc8lf3n9fhj9yk8r56k6j93fa6xv2d06v")))

(define-public crate-webtransport-generic-0.3.0 (c (n "webtransport-generic") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "04139a34cx72irv7792nxpn120pr506vys7q8a0z03mvjqz5i90b")))

(define-public crate-webtransport-generic-0.5.0 (c (n "webtransport-generic") (v "0.5.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0iafcx38ml6zxzcld4gxqq1fnf1qxgmrswslysb2jcb1swbj6wfz")))

(define-public crate-webtransport-generic-0.8.0 (c (n "webtransport-generic") (v "0.8.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)))) (h "1f27j76hwm0l9g9ikjjf6pa08hkainhznh40lmd4hkp2qzax07xw")))

(define-public crate-webtransport-generic-0.9.0 (c (n "webtransport-generic") (v "0.9.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "055qr6n589sn4n9f0grf78dfl61nv041nwqs9kyvi2gqhdywr5ip")))

