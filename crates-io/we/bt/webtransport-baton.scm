(define-module (crates-io we bt webtransport-baton) #:use-module (crates-io))

(define-public crate-webtransport-baton-0.1.0 (c (n "webtransport-baton") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "webtransport-generic") (r "^0.5") (d #t) (k 0)))) (h "1jcgkvxny7j2y6fk4pzzh8fxyidmip17f3r5l7w05bjnjzq312fb")))

