(define-module (crates-io we bt webtype) #:use-module (crates-io))

(define-public crate-webtype-0.0.1 (c (n "webtype") (v "0.0.1") (h "0k8gdl23zmw81hr1zmsgxbsf7p1cn7bxs21dv6zz61wzq15im0pr")))

(define-public crate-webtype-0.1.0 (c (n "webtype") (v "0.1.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.24.12") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1wjnivzdh6x35hg1d8jbwfq1jikmkbnryi9laak8adkfhdf02rfc")))

(define-public crate-webtype-0.1.1 (c (n "webtype") (v "0.1.1") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.24.14") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "01l1cz32xif6m4357amab9gxv7rh9x1bdjk5pxp9sczxhw9w0r4s")))

(define-public crate-webtype-0.2.0 (c (n "webtype") (v "0.2.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.24.14") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "09j39hhj6qkv4xdvc9xs2byl04ifdxa10bqxjlqjxx8js29yawa8")))

(define-public crate-webtype-0.2.1 (c (n "webtype") (v "0.2.1") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.24.14") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0wpwpg1h7y3dyb71php9fcjbhf5bjlxdvwglwkmbg22vvcyblr0k")))

(define-public crate-webtype-0.3.0 (c (n "webtype") (v "0.3.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.25.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "11fl5zxkscgzh22hgv161cwz0ch4facv6ckf3r1i7iay0h3jhpd6")))

(define-public crate-webtype-0.4.0 (c (n "webtype") (v "0.4.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.26.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0qz7805mml57nqwrxcbwxpfpwhqb4h1zsrlz3jzs5cy9z8mqyg6p")))

(define-public crate-webtype-0.5.0 (c (n "webtype") (v "0.5.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.27.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0fx3nrw8bx2abnf74sjlfdi6phl88bcd2qj4yiq6znmxg5i6yd56")))

(define-public crate-webtype-0.6.0 (c (n "webtype") (v "0.6.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.28.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1glr7xng3xj5gdzispi9rf6z5s1rhaxp4zl95b157pg0mqwd5lh5")))

(define-public crate-webtype-0.7.0 (c (n "webtype") (v "0.7.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.29.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "0samy6hbfgmqnjgzzpjg46bpcnq3vszsvhg6c0n3w2vd61l84wbs")))

(define-public crate-webtype-0.8.0 (c (n "webtype") (v "0.8.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.30.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "079hzw14gm9s1nq0ncmm3p676ahccsacw2dada902abmk4qa76q4")))

(define-public crate-webtype-0.9.0 (c (n "webtype") (v "0.9.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.31.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "12qjcr5a6rxj80zii8yxpkinf80fi3z415pi35rvsilnnhxqc8yb")))

(define-public crate-webtype-0.10.0 (c (n "webtype") (v "0.10.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.32.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0") (d #t) (k 0)))) (h "1r4lnb74f102pj9mjxxc8v5p9f0z4r6y4z1h0c4m184b4xznwp6i")))

(define-public crate-webtype-0.10.1 (c (n "webtype") (v "0.10.1") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.32.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0.4.0") (d #t) (k 0)))) (h "1i57yf42hsahi9ign40nak6a6l49s0cr6600frw84nbxcs9ihnrx")))

(define-public crate-webtype-0.11.0 (c (n "webtype") (v "0.11.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.33.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "0sfbvg4s45ggvwfg3f7bs8lipvzwcxjfrjmqv3j3hilp919x0rj1")))

(define-public crate-webtype-0.12.0 (c (n "webtype") (v "0.12.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.34.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "03s34rwbsinm3bl515h74afiywqg9rpli14a2ia763ynsdsv01lm")))

(define-public crate-webtype-0.13.0 (c (n "webtype") (v "0.13.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.35.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0.4.1") (d #t) (k 0)))) (h "1pysgcv3yc4y26hkdahb233f7yjsgszf1gn81hgqklmi88p82brp")))

(define-public crate-webtype-0.13.1 (c (n "webtype") (v "0.13.1") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.35.2") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "1r8mqicd5ax6x4w81d1p2azlrfi8lgc37magfkdf8cx8viyy3pn3")))

(define-public crate-webtype-0.14.0 (c (n "webtype") (v "0.14.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.36.0") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "1h3klrp5rnlaf3jqgd66i1yj813wfpz3pk224796qqyb9jv3ywf8")))

(define-public crate-webtype-0.15.0 (c (n "webtype") (v "0.15.0") (d (list (d (n "brotli-decompressor") (r "^2") (d #t) (k 0)) (d (n "opentype") (r "^0.37") (f (quote ("ignore-invalid-checksums"))) (d #t) (k 0)) (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "0wpdcnpcbrv7q8wsbrmkl6vyfq5jvd88b6cxpgv065vrr0bh06jn")))

