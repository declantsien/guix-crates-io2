(define-module (crates-io we bt webthings-gateway-ipc-types) #:use-module (crates-io))

(define-public crate-webthings-gateway-ipc-types-1.0.0-alpha.2 (c (n "webthings-gateway-ipc-types") (v "1.0.0-alpha.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 1)) (d (n "jsonschema_code_generator") (r "^2.0.0") (d #t) (k 1)) (d (n "os_pipe") (r "^0.9.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 1)))) (h "0qqh3rj7wbmwwj3x5b850hz3ifppzi7w2wcx0324v7780lqi6fp4")))

