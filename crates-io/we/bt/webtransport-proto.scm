(define-module (crates-io we bt webtransport-proto) #:use-module (crates-io))

(define-public crate-webtransport-proto-0.2.1 (c (n "webtransport-proto") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jw9q4z97xdcg21xism0xaqwqc5ahr04mgfamsmp2alq8jx75ls7") (y #t)))

(define-public crate-webtransport-proto-0.3.0 (c (n "webtransport-proto") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1miwjw8d90k8vc4ji3h8i88p9hb7dbfacgdw70imn1rql7x93a14")))

(define-public crate-webtransport-proto-0.3.2 (c (n "webtransport-proto") (v "0.3.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1l1jw8v2w7iz72mjxfmk7wh90yz85z7bf7mg07wylllmlxbck09g")))

(define-public crate-webtransport-proto-0.4.0 (c (n "webtransport-proto") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1iwj10jq1q1ds1407zd8kqbicqc98wxqana68ixm07b551bzpzi1")))

(define-public crate-webtransport-proto-0.5.4 (c (n "webtransport-proto") (v "0.5.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0kzas0dqafn0vp65cbf89zgksz0r3pvjddhl448k8kczlwki3m2l")))

(define-public crate-webtransport-proto-0.6.0 (c (n "webtransport-proto") (v "0.6.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0xm265v2ppkjf43nqb31gilw2ag383cbh2ifms02jc6n6x8dmspb")))

(define-public crate-webtransport-proto-0.6.1 (c (n "webtransport-proto") (v "0.6.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1gli82q5czsikh2lsrwyd43fa32azic2f14gyz2r48hgp8slks3x")))

