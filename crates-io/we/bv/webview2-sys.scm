(define-module (crates-io we bv webview2-sys) #:use-module (crates-io))

(define-public crate-webview2-sys-0.1.0-alpha.4 (c (n "webview2-sys") (v "0.1.0-alpha.4") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi"))) (d #t) (k 0)))) (h "1rwm29x1br7ajw7fyj6p4b8k21mnkb1a5sny3dy2vz40638v5vvj")))

(define-public crate-webview2-sys-0.1.0-alpha.5 (c (n "webview2-sys") (v "0.1.0-alpha.5") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi"))) (d #t) (k 0)))) (h "1xiz1i517cdwzdzk24gaay0fvlik12hnni5x14kb1xplnl050p2z")))

(define-public crate-webview2-sys-0.1.0-alpha.6 (c (n "webview2-sys") (v "0.1.0-alpha.6") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi"))) (d #t) (k 0)))) (h "06k41b2p3l1brqg4b004x23ab9q56r90v2a68hzni8b5iqbcjwxp")))

(define-public crate-webview2-sys-0.1.0-alpha.7 (c (n "webview2-sys") (v "0.1.0-alpha.7") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi"))) (d #t) (k 0)))) (h "1i4g4rkkfji60s80cqwswidpz9vfizy8nnc0v8l8ny9ik39lg8m2")))

(define-public crate-webview2-sys-0.1.0-alpha.8 (c (n "webview2-sys") (v "0.1.0-alpha.8") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi"))) (d #t) (k 0)))) (h "0gxr5irmv6zdsai72w3vqyg37w87s2p732vn9qzgszcb55lnz99g")))

(define-public crate-webview2-sys-0.1.0-beta.1 (c (n "webview2-sys") (v "0.1.0-beta.1") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi" "winver" "shellapi"))) (d #t) (k 0)))) (h "0mnlbbdgdp4p5d948k07h8y889s3s7hc69fmh5jgzp9k1699qgxm")))

(define-public crate-webview2-sys-0.1.0 (c (n "webview2-sys") (v "0.1.0") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi" "winver" "shellapi"))) (d #t) (k 0)))) (h "1zs6afdrhkjhg08f3cl0kjcbi29mwdqn47ln1dxczjz0y778hlnc")))

(define-public crate-webview2-sys-0.1.1 (c (n "webview2-sys") (v "0.1.1") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi" "winver" "shellapi"))) (d #t) (k 0)))) (h "0k1a06w5jrajw3yvhf1ca5b5xa0krvinnda6fc6wbi1si6g8idr4")))

