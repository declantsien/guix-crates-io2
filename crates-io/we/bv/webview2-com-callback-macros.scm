(define-module (crates-io we bv webview2-com-callback-macros) #:use-module (crates-io))

(define-public crate-webview2-com-callback-macros-0.1.0 (c (n "webview2-com-callback-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "0mk2ydwwkdwajiw9z1vgqkrhbmyclkg627iqbxfy2s7ay38rgwn6")))

(define-public crate-webview2-com-callback-macros-0.1.1 (c (n "webview2-com-callback-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "0zr191c7d4yan6mssiiw7p7dg28c05sq6c81c44gpgdg5i4r73h5")))

