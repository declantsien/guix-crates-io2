(define-module (crates-io we bv webview-sys_suppress) #:use-module (crates-io))

(define-public crate-webview-sys_suppress-0.1.1 (c (n "webview-sys_suppress") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i72k0i7jh6z34cdw58aarkwahqxc7hgc5q028s636jxf0l1lp5h") (y #t) (l "webview")))

(define-public crate-webview-sys_suppress-0.1.2 (c (n "webview-sys_suppress") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0py13v3403vj9jyx7cvz1zaxyj5k9nkidpxzav305r56yzwny97h") (y #t) (l "webview")))

