(define-module (crates-io we bv webview2-com-macros) #:use-module (crates-io))

(define-public crate-webview2-com-macros-0.2.0 (c (n "webview2-com-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "068y7pj05xs8v33yaf90hhwfgqj140ypgw3x9n3bi6hcjlgj414c")))

(define-public crate-webview2-com-macros-0.3.0 (c (n "webview2-com-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "12fan75rc223zbxwksd6v1a2ggyvasik55a7wzkqvg4gpgykbfif")))

(define-public crate-webview2-com-macros-0.4.0 (c (n "webview2-com-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "0gg1b55mf9z3brkwvrr6h475pk3lz9mqvjm49rv7alh3ajrs9g07")))

(define-public crate-webview2-com-macros-0.5.0 (c (n "webview2-com-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "169y9906rhyz04cwx7anrbrlzgix4f72rixcv9p3zsff5z4cc58m")))

(define-public crate-webview2-com-macros-0.6.0 (c (n "webview2-com-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "1b05znlbb6d5mpixl9xfvhvxhkqyxz2m5jp4x5idp48nq2bf3sza")))

(define-public crate-webview2-com-macros-0.7.0 (c (n "webview2-com-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1g69b4ghd387zbmb1r650qi6vp75jldxzg20i13250fdirwla4xc")))

