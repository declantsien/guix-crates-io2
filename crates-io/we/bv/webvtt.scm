(define-module (crates-io we bv webvtt) #:use-module (crates-io))

(define-public crate-webvtt-0.1.0 (c (n "webvtt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1xpvzq969n9xkpp6i2bq4nfl9fzkl9qrxwxcdgq8r2llw12pm1mw") (y #t)))

(define-public crate-webvtt-0.2.0 (c (n "webvtt") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yck229gs0rnj9wlgrml5gvxsxcyh0ng49ck1a74mspkn9vw95gx")))

