(define-module (crates-io we bv webview-official-sys) #:use-module (crates-io))

(define-public crate-webview-official-sys-0.0.0 (c (n "webview-official-sys") (v "0.0.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0blwhwmyhc9ww7fqby7390kjkycx489njv0i9a9vl79fjrr11zb4")))

(define-public crate-webview-official-sys-0.1.0 (c (n "webview-official-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ig71zkqfgrc6pj1j0s5wnxksa7hxdymi0q74nyvaf84mdasa53g")))

(define-public crate-webview-official-sys-0.1.1 (c (n "webview-official-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0qca2zn7mckkhhqkgmdqs6gq1kxlnixv33riz4dv9qfjl53c39by")))

(define-public crate-webview-official-sys-0.1.2 (c (n "webview-official-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lp1m6lq9jgfnbq1c7m9jpisxgnn0a73plj7zsjqp4xwypywbbn4")))

