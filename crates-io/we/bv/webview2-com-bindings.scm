(define-module (crates-io we bv webview2-com-bindings) #:use-module (crates-io))

(define-public crate-webview2-com-bindings-0.1.0 (c (n "webview2-com-bindings") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 1)) (d (n "windows") (r "^0.19.0") (d #t) (k 0)) (d (n "windows") (r "^0.19.0") (d #t) (k 1)))) (h "0b0079zxsf8650k7rkz5m1hfy0yksqy0n77axam2qmw19m8vh5c0")))

(define-public crate-webview2-com-bindings-0.1.1 (c (n "webview2-com-bindings") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 1)) (d (n "windows") (r "^0.19.0") (d #t) (k 0)) (d (n "windows") (r "^0.19.0") (d #t) (k 1)))) (h "08h1g7vs80np9s0qyl1qlwki4l72n1ayz9mir2y333l9yplqhhin")))

