(define-module (crates-io we bv webview) #:use-module (crates-io))

(define-public crate-webview-0.1.0 (c (n "webview") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "0zlx9y8p2a7snfr8vqixnmiq2z8jsar8gq7agqkrwwy7qi8pq5hy")))

(define-public crate-webview-0.1.1 (c (n "webview") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "1m93y5np0pwmi10j96yqpad1p07w54qyazmacljc2nhywi8rviw3")))

