(define-module (crates-io we bh webhooks) #:use-module (crates-io))

(define-public crate-webhooks-0.0.1 (c (n "webhooks") (v "0.0.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r ">1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0lbni851c092l0wgrprxhafqq4qv05lf2yvdnjawxxlj0zmhb3x0")))

