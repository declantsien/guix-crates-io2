(define-module (crates-io we bh webhook-flows-macros) #:use-module (crates-io))

(define-public crate-webhook-flows-macros-0.1.0 (c (n "webhook-flows-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0npmb76l67xnljb5w9rqfahapin3pwka5q5zk0jkljhxy5sx6zf5")))

(define-public crate-webhook-flows-macros-0.2.0 (c (n "webhook-flows-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nw83g1l3zbqzxy7548gb2pdh4a0zrif0slkgd1dkvjy6c4i7s37")))

(define-public crate-webhook-flows-macros-0.2.1 (c (n "webhook-flows-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l76bmpd70gxmnjqpcmkdqs6js7809jl9niayf7qh3dwmykp1ihs")))

(define-public crate-webhook-flows-macros-0.2.2 (c (n "webhook-flows-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bz09pblz9j5mvy8xympz3cj78sk72jrb12mhpbkk5wl8n2pb8mc")))

