(define-module (crates-io we bh webhook_listener) #:use-module (crates-io))

(define-public crate-webhook_listener-0.2.0 (c (n "webhook_listener") (v "0.2.0") (d (list (d (n "actix-web") (r "^1.0.3") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1zy0impa6698sd1wz3c6kvylymmkzzsy8ph2plz0rq9dhy4xhli9")))

(define-public crate-webhook_listener-0.2.1 (c (n "webhook_listener") (v "0.2.1") (d (list (d (n "actix-web") (r "^1.0.3") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0fbx8m04w2aa2ynxhas5aqhdqbxba2wzrnw1f09k0l1wxaani9yv")))

