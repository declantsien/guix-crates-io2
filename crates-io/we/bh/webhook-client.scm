(define-module (crates-io we bh webhook-client) #:use-module (crates-io))

(define-public crate-webhook-client-0.2.0 (c (n "webhook-client") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yp1xgr6mp9j2d80y71lmm0504pi7chz88ndhdcsx7sk2ifdgq90")))

(define-public crate-webhook-client-0.2.1 (c (n "webhook-client") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "08h7qscxi9brqnld2nzsdbmism8cb947wvczvqg9kha3jwflvycy")))

