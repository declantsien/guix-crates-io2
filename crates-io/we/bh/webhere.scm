(define-module (crates-io we bh webhere) #:use-module (crates-io))

(define-public crate-webhere-0.1.0 (c (n "webhere") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1p0dlwj2xyyd9i2i5nvgzq75ls2d8vf6bwllkq221s3rzasckrj2") (y #t)))

(define-public crate-webhere-0.1.1 (c (n "webhere") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0lqsax8jp6b51br3kgg1gsbh0cmfwc4516kbrzqq18gi2q7vl04p") (y #t)))

(define-public crate-webhere-0.1.2 (c (n "webhere") (v "0.1.2") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "033l57c67fa0r4qxibgmhr58swlnnpndd72q99rl6bvhffwgsf6r") (y #t)))

(define-public crate-webhere-0.1.3 (c (n "webhere") (v "0.1.3") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0p5i280rf4almxnq2xdb7cj6p2zspmrq8hljvhagrdi3lb59p3h8") (y #t)))

(define-public crate-webhere-0.1.4 (c (n "webhere") (v "0.1.4") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1wf824a9zh5qgs7mfjamqifjryvy1i9jlwp6dnzji8n77gsr8hjr")))

