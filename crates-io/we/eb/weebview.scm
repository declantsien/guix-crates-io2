(define-module (crates-io we eb weebview) #:use-module (crates-io))

(define-public crate-weebview-0.1.0 (c (n "weebview") (v "0.1.0") (d (list (d (n "gdk-sys") (r "^0.9") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.9") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.9") (d #t) (k 0)) (d (n "javascriptcore-rs-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "webkit2gtk-sys") (r "^0.10") (f (quote ("v2_8"))) (d #t) (k 0)))) (h "1388a8m5whbyd1k8fy3xww1cqa3499p03byjvvq3c1nf5vi24gd2")))

