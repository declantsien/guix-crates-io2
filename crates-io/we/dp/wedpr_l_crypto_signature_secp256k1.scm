(define-module (crates-io we dp wedpr_l_crypto_signature_secp256k1) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_signature_secp256k1-0.0.1 (c (n "wedpr_l_crypto_signature_secp256k1") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^0.0.1") (d #t) (k 0)))) (h "06b0fkaykc062drwpcq7jrgcky7pr4xyq7wz22sjbqfrvqkmspy0")))

(define-public crate-wedpr_l_crypto_signature_secp256k1-1.0.0 (c (n "wedpr_l_crypto_signature_secp256k1") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.0.0") (d #t) (k 0)))) (h "0xn5wwn5x7hpznydpasfz0df4lcv6p2hsml48wjlvr99wnlrhz8i")))

(define-public crate-wedpr_l_crypto_signature_secp256k1-1.0.1 (c (n "wedpr_l_crypto_signature_secp256k1") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.0.0") (d #t) (k 0)))) (h "0s22dbvrh1zy3fkaqqzg7hy9bgv300hwhxrkk7zhpaiz6xn1ih45")))

(define-public crate-wedpr_l_crypto_signature_secp256k1-1.1.0 (c (n "wedpr_l_crypto_signature_secp256k1") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "0c0ja0k1085v4z59afhm5vj9hdaa2r77y49168lz380kqbcn36l4")))

