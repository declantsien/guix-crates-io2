(define-module (crates-io we dp wedpr_l_common_coder_hex) #:use-module (crates-io))

(define-public crate-wedpr_l_common_coder_hex-0.0.1 (c (n "wedpr_l_common_coder_hex") (v "0.0.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^0.0.1") (d #t) (k 0)))) (h "1yrq3ymsiql0gkgswr0v58ahmmrdia5jm79d44h0gcpsk3x716vw")))

(define-public crate-wedpr_l_common_coder_hex-1.0.0 (c (n "wedpr_l_common_coder_hex") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.0.0") (d #t) (k 0)))) (h "10jr829yx91m7lz9przx26n9iz6lw4x7w496aq920wfc6q41f3xl")))

(define-public crate-wedpr_l_common_coder_hex-1.1.0 (c (n "wedpr_l_common_coder_hex") (v "1.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "06836vs3x6486977xppdb1bqfnasn4ly12ny24r3kbwdp9c63p8f")))

