(define-module (crates-io we dp wedpr_l_common_coder_base64) #:use-module (crates-io))

(define-public crate-wedpr_l_common_coder_base64-0.0.1 (c (n "wedpr_l_common_coder_base64") (v "0.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^0.0.1") (d #t) (k 0)))) (h "02x2dvfvjhvivl6yh4j2prvckw5054n94bnaam4iq5zad3h4lixb")))

(define-public crate-wedpr_l_common_coder_base64-1.0.0 (c (n "wedpr_l_common_coder_base64") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.0.0") (d #t) (k 0)))) (h "137ycqjhwlw8650ij82awgyhrxz14ys04hcps3bc8xkm3fmnskmf")))

(define-public crate-wedpr_l_common_coder_base64-1.1.0 (c (n "wedpr_l_common_coder_base64") (v "1.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "1r5hi1bjqwjf7kmj3s800wf3jylm8fc70sf3xc1whr8d11z5b21v")))

