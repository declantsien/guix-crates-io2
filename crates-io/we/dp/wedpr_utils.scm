(define-module (crates-io we dp wedpr_utils) #:use-module (crates-io))

(define-public crate-wedpr_utils-1.1.0 (c (n "wedpr_utils") (v "1.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)) (d (n "wedpr_macros") (r "^1.1.0") (d #t) (k 0)))) (h "03scl3847rs6n9vkvm5y8a56ssnl04d1y72d4zxkfk5f397hrkx8")))

