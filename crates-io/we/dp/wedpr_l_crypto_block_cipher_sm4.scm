(define-module (crates-io we dp wedpr_l_crypto_block_cipher_sm4) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_block_cipher_sm4-1.0.0 (c (n "wedpr_l_crypto_block_cipher_sm4") (v "1.0.0") (d (list (d (n "block-modes") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sm4") (r "^0.3.0") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "1gdx992bwkika1mvhck7b7yyv0h342mkzlv0s5jlyz48m8si5jxj")))

