(define-module (crates-io we dp wedpr_crypto) #:use-module (crates-io))

(define-public crate-wedpr_crypto-1.2.0 (c (n "wedpr_crypto") (v "1.2.0") (d (list (d (n "bulletproofs") (r "^1.0.4") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ecies") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "merlin") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)) (d (n "wedpr_macros") (r "^1.1.0") (d #t) (k 0)) (d (n "wedpr_protos") (r "^1.1.0") (d #t) (k 0)) (d (n "wedpr_utils") (r "^1.1.0") (d #t) (k 0)))) (h "1673z8xa28mkmrbs5j88mh8cda4p5drjdzgkp9mz1iykb3lh226g")))

