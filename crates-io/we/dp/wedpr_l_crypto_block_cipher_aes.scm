(define-module (crates-io we dp wedpr_l_crypto_block_cipher_aes) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_block_cipher_aes-1.0.0 (c (n "wedpr_l_crypto_block_cipher_aes") (v "1.0.0") (d (list (d (n "aes") (r "^0.6.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "1whm24n7xl37vy8icxy3k3g7c1kj71vl6lnh7lqdi9l1yk0n0nm9")))

