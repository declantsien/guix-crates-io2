(define-module (crates-io we dp wedpr_l_crypto_hash_sm3) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_hash_sm3-0.0.1 (c (n "wedpr_l_crypto_hash_sm3") (v "0.0.1") (d (list (d (n "wedpr_l_libsm") (r "^0.3.0") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^0.0.1") (d #t) (k 0)))) (h "15y15fyjp9499fh93i9kbslry95hc8xnsba1wqpq7yrqvv0xfqzd")))

(define-public crate-wedpr_l_crypto_hash_sm3-1.0.0 (c (n "wedpr_l_crypto_hash_sm3") (v "1.0.0") (d (list (d (n "wedpr_l_libsm") (r "^0.3.0") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.0.0") (d #t) (k 0)))) (h "1cfp6rrw5s8ch4wd3alm5krrv12gradnrxx1qdwr6jysi0bk28wm")))

(define-public crate-wedpr_l_crypto_hash_sm3-1.1.0 (c (n "wedpr_l_crypto_hash_sm3") (v "1.1.0") (d (list (d (n "wedpr_l_libsm") (r "^0.3.1") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "0jgwx6fl1rcyinc5rr33g3fml91y11jxjngz55hsg0q8q0d4l4za")))

