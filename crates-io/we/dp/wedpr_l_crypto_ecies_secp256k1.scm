(define-module (crates-io we dp wedpr_l_crypto_ecies_secp256k1) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_ecies_secp256k1-0.0.1 (c (n "wedpr_l_crypto_ecies_secp256k1") (v "0.0.1") (d (list (d (n "ecies") (r "^0.1.4") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^0.0.1") (d #t) (k 0)))) (h "04aarzz7fygfjpjaq5qx7w7wax8z9dl80dmmjbjllcg56qrdy8sp")))

(define-public crate-wedpr_l_crypto_ecies_secp256k1-1.0.0 (c (n "wedpr_l_crypto_ecies_secp256k1") (v "1.0.0") (d (list (d (n "ecies") (r "^0.1.4") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.0.0") (d #t) (k 0)))) (h "1256ngkgzxm73wajvz3lhwyc514ilz2i21y9r0pr79f8psdd9042")))

(define-public crate-wedpr_l_crypto_ecies_secp256k1-1.1.0 (c (n "wedpr_l_crypto_ecies_secp256k1") (v "1.1.0") (d (list (d (n "ecies") (r "^0.1.4") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "18d7dxbazpljc0s4pjapj7ylsbjnvjxkk4bi8v9m0kdpllggs3kg")))

