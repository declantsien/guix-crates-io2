(define-module (crates-io we dp wedpr_ffi_c_common) #:use-module (crates-io))

(define-public crate-wedpr_ffi_c_common-1.0.0 (c (n "wedpr_ffi_c_common") (v "1.0.0") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)))) (h "130psyjz95majxha23y2775069alraawsyw8i9ysl573rambv1z9")))

