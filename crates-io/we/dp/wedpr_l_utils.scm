(define-module (crates-io we dp wedpr_l_utils) #:use-module (crates-io))

(define-public crate-wedpr_l_utils-0.0.1 (c (n "wedpr_l_utils") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0wfj9zr3jf25n3agi93pis18cl7r2mid4ifm0gf0s147ck3gc23a")))

(define-public crate-wedpr_l_utils-1.0.0 (c (n "wedpr_l_utils") (v "1.0.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "050d39dfx4hag41ygc9kvpjrn5smh8lx3zcq8anndx13x4if9wan")))

(define-public crate-wedpr_l_utils-1.1.0 (c (n "wedpr_l_utils") (v "1.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1sr4fm6qg7mpj7gvb8k7rnzpi7hrlvl6siziv8ra5nd0p981pg8g")))

