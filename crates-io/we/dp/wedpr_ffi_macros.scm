(define-module (crates-io we dp wedpr_ffi_macros) #:use-module (crates-io))

(define-public crate-wedpr_ffi_macros-0.0.1 (c (n "wedpr_ffi_macros") (v "0.0.1") (h "0cziy591mzyjbiic5ia48v1chsx5p4zsi89flh85rkckh2abnwwh")))

(define-public crate-wedpr_ffi_macros-1.0.0 (c (n "wedpr_ffi_macros") (v "1.0.0") (h "0m0wh5jddbi5wimi7pq6620sz8mmmdl4dzp45yc07hjlcfiwvkcp")))

(define-public crate-wedpr_ffi_macros-1.1.0 (c (n "wedpr_ffi_macros") (v "1.1.0") (h "0fw67ibiv6dv37zayp80m85sdqfpqi9jbsds86qvysnz5k453hi5")))

