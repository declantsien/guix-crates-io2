(define-module (crates-io we dp wedpr_l_crypto_signature_ed25519) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_signature_ed25519-1.0.1 (c (n "wedpr_l_crypto_signature_ed25519") (v "1.0.1") (d (list (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "0kb1590r2yv1n7gmbyja7k0nl24zr96migmy807srw2rzgfg5r82")))

(define-public crate-wedpr_l_crypto_signature_ed25519-1.1.0 (c (n "wedpr_l_crypto_signature_ed25519") (v "1.1.0") (d (list (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "0fl240zabh30npz8g57vkdg70zyd20in2k4q36n1apdg66fb6r7x")))

