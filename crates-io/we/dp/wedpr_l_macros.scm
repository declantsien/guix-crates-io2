(define-module (crates-io we dp wedpr_l_macros) #:use-module (crates-io))

(define-public crate-wedpr_l_macros-0.0.1 (c (n "wedpr_l_macros") (v "0.0.1") (h "07763zw35qs5qpqsvdgmq3z0karprgj57kjs2rr20r65n75iwhr4")))

(define-public crate-wedpr_l_macros-1.0.0 (c (n "wedpr_l_macros") (v "1.0.0") (h "046xd66xwgkdw0wrjpyclxdbrmn1q8pqbiwj7ibg9wy9v02x556c")))

