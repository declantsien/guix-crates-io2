(define-module (crates-io we dp wedpr_protos) #:use-module (crates-io))

(define-public crate-wedpr_protos-1.2.0 (c (n "wedpr_protos") (v "1.2.0") (d (list (d (n "protobuf") (r "^2.10.1") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.10.1") (d #t) (k 0)))) (h "0kwqdx8qi4g2r0afcia5z8x62id2580mwzyrr7vv9850k4zpn7xc")))

