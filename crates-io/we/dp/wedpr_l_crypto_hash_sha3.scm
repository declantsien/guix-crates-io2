(define-module (crates-io we dp wedpr_l_crypto_hash_sha3) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_hash_sha3-1.0.0 (c (n "wedpr_l_crypto_hash_sha3") (v "1.0.0") (d (list (d (n "sha3") (r "^0.8") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "01r18w74ccccrd0q041m7hmy83rprdx4ixsfsqiywyzn5nf93vkz")))

