(define-module (crates-io we dp wedpr_l_crypto_hash_ripemd160) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_hash_ripemd160-1.0.0 (c (n "wedpr_l_crypto_hash_ripemd160") (v "1.0.0") (d (list (d (n "ripemd160") (r "^0.9.1") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "1w9qcscdcxgzyn3kacmmrayyvpxn4arjb20m16kbvzp78mij4w39")))

