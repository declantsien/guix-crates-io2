(define-module (crates-io we dp wedpr_l_crypto_hash_blake2b) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_hash_blake2b-1.0.0 (c (n "wedpr_l_crypto_hash_blake2b") (v "1.0.0") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "07bs6h5h4j7y9ggxrg0qgy4ibsw9mc78ahcxmq6izr25y6bvipvr")))

