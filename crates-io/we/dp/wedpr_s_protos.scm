(define-module (crates-io we dp wedpr_s_protos) #:use-module (crates-io))

(define-public crate-wedpr_s_protos-1.3.0 (c (n "wedpr_s_protos") (v "1.3.0") (d (list (d (n "protobuf") (r "^2.20.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.20.0") (d #t) (k 0)))) (h "1358mvhkjrlii22giw4i85cnsyjgig66jh34s9lig8kxnw9s6j35")))

(define-public crate-wedpr_s_protos-1.5.0 (c (n "wedpr_s_protos") (v "1.5.0") (d (list (d (n "protobuf") (r "^2.22.1") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.22.1") (d #t) (k 0)))) (h "1vjj0fjsnpmgmdinyy5yh0xr3d06jsa6k0lp2g5lkf5w3c6x6sl6")))

