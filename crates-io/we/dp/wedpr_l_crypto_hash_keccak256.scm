(define-module (crates-io we dp wedpr_l_crypto_hash_keccak256) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_hash_keccak256-0.0.1 (c (n "wedpr_l_crypto_hash_keccak256") (v "0.0.1") (d (list (d (n "sha3") (r "^0.8") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^0.0.1") (d #t) (k 0)))) (h "1rbfhgvc7brzsk22cmkk1rxlrxpc27l3596p8vc6z4sfrh4akmz9")))

(define-public crate-wedpr_l_crypto_hash_keccak256-1.0.0 (c (n "wedpr_l_crypto_hash_keccak256") (v "1.0.0") (d (list (d (n "sha3") (r "^0.8") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.0.0") (d #t) (k 0)))) (h "0a8srhjarnppml6y71ylcn2hn34rrmarkdvlv1sbppajdy2zf5ap")))

(define-public crate-wedpr_l_crypto_hash_keccak256-1.1.0 (c (n "wedpr_l_crypto_hash_keccak256") (v "1.1.0") (d (list (d (n "sha3") (r "^0.8") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "0l14f1j7ni477fpzc9qch8v42m15qy1qfldnralqnri3vpm92dl2")))

