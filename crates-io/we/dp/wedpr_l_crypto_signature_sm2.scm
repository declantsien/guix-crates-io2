(define-module (crates-io we dp wedpr_l_crypto_signature_sm2) #:use-module (crates-io))

(define-public crate-wedpr_l_crypto_signature_sm2-0.0.1 (c (n "wedpr_l_crypto_signature_sm2") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "wedpr_l_libsm") (r "^0.3.1") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^0.0.1") (d #t) (k 0)))) (h "0g906nrzys1pg2j64z049hkiymi9855nyl2yc7yv2mj2rhb5nahg")))

(define-public crate-wedpr_l_crypto_signature_sm2-1.0.0 (c (n "wedpr_l_crypto_signature_sm2") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "wedpr_l_libsm") (r "^0.3.1") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.0.0") (d #t) (k 0)))) (h "1r1i8iqf895yixxkwipam3q230y8wrl03c26al60k209lqy3nmpv")))

(define-public crate-wedpr_l_crypto_signature_sm2-1.0.1 (c (n "wedpr_l_crypto_signature_sm2") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "wedpr_l_libsm") (r "^0.3.1") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.0.0") (d #t) (k 0)))) (h "06f43cqyg1rl533zcz3jqi376kvjdq94rxr7n8d00b3kpsm5a5vc")))

(define-public crate-wedpr_l_crypto_signature_sm2-1.1.0 (c (n "wedpr_l_crypto_signature_sm2") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "wedpr_l_libsm") (r "^0.3.2") (d #t) (k 0)) (d (n "wedpr_l_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "06alpficrs6kjyd40j4bknpcm60hp4hvm4xm9xfvicfp4bp8s9ng")))

