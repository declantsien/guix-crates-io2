(define-module (crates-io we dp wedpr_l_protos) #:use-module (crates-io))

(define-public crate-wedpr_l_protos-0.0.1 (c (n "wedpr_l_protos") (v "0.0.1") (d (list (d (n "protobuf") (r "^2.20.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.20.0") (d #t) (k 0)))) (h "07g4v26m5081w04d4bcwmkqmjfix9v2k6vwzq7sk9wjnfbcf6lch")))

(define-public crate-wedpr_l_protos-1.0.0 (c (n "wedpr_l_protos") (v "1.0.0") (d (list (d (n "protobuf") (r "^2.20.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.20.0") (d #t) (k 0)))) (h "19yab8dch3lkqv58rih8bkwqd4ax1jrbrc5az7kavnl4nrby3wx8")))

(define-public crate-wedpr_l_protos-1.1.0 (c (n "wedpr_l_protos") (v "1.1.0") (d (list (d (n "protobuf") (r "^2.22.1") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.22.1") (d #t) (k 0)))) (h "1f2vs7mnav4cqfyf56rmm5bxqqkilbfsprj1spjw45alskdd9nlh")))

(define-public crate-wedpr_l_protos-1.1.1 (c (n "wedpr_l_protos") (v "1.1.1") (d (list (d (n "protobuf") (r "^2.22.1") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.22.1") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "18k1b1i9r9hpxhgd0a7rnqdbwd473b7a1n7faa2bcggry1n6vx31")))

(define-public crate-wedpr_l_protos-1.2.0 (c (n "wedpr_l_protos") (v "1.2.0") (d (list (d (n "protobuf") (r "^2.22.1") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.22.1") (d #t) (k 0)) (d (n "wedpr_l_utils") (r "^1.1.0") (d #t) (k 0)))) (h "1kak2mfjz3nwc4mg27a39x33kyarja853q6qx68x6sv0mbknl6n3")))

