(define-module (crates-io os o- oso-cloud) #:use-module (crates-io))

(define-public crate-oso-cloud-0.1.0 (c (n "oso-cloud") (v "0.1.0") (h "0g33z87b91sk9scrkd8h8cwbdh24g1kznld9z2cbwygyacghmm2m")))

(define-public crate-oso-cloud-0.2.0 (c (n "oso-cloud") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0bp0i6as4x0lx8p60qa6kr9gh5vck34g3xcsaq9zhrl3s8yz92x3")))

(define-public crate-oso-cloud-0.2.1 (c (n "oso-cloud") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "09x1gqjdnyrl8kr7y68iywphpv7y68r03jvp742a25bnhw5z3mcp")))

(define-public crate-oso-cloud-0.4.0 (c (n "oso-cloud") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0p1r8369w03az3xsmrnh5n092qfq5ydg1hmfk0778mvamxg3fdn5")))

