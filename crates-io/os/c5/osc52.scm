(define-module (crates-io os c5 osc52) #:use-module (crates-io))

(define-public crate-osc52-0.1.0 (c (n "osc52") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "06swpqr40b7cxix41p785014m2asj9hn9572570yc0zfdjw6y5dv")))

