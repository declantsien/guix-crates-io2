(define-module (crates-io os -v os-version) #:use-module (crates-io))

(define-public crate-os-version-0.1.0 (c (n "os-version") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 0)) (d (n "plist") (r "^0.5.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (t "cfg(target_os = \"openbsd\")") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("libloaderapi" "ntstatus" "sysinfoapi" "winnt" "winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "17s7mgvmzgjz6bic4df7b0dq2zljl4ii4szislvi0m0qcqxba65x")))

(define-public crate-os-version-0.1.1 (c (n "os-version") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 0)) (d (n "plist") (r "^0.5.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (t "cfg(target_os = \"openbsd\")") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("libloaderapi" "ntstatus" "sysinfoapi" "winnt" "winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0i3jyqxd39pdjn53483v02xgc1xajip8zhaajxpchv2ibp5y0amb")))

(define-public crate-os-version-0.2.0 (c (n "os-version") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (t "cfg(target_os = \"openbsd\")") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("libloaderapi" "ntstatus" "sysinfoapi" "winnt" "winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0860w48vxqicc15vv8cj0akcb4ws4av0d8cc0lwmwxmcfvniz2js")))

