(define-module (crates-io os _s os_str_bytes) #:use-module (crates-io))

(define-public crate-os_str_bytes-0.1.0 (c (n "os_str_bytes") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.1.13") (d #t) (k 2)))) (h "1iw0x2rahf8xwllqvi2sg3d1561caxnvlwnm1y1044ccpabqycdn") (y #t)))

(define-public crate-os_str_bytes-0.1.1 (c (n "os_str_bytes") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.1.13") (d #t) (k 2)))) (h "16sg2mcbdhzqlka6s8cc6pn1w5zpzq2ps7hc58gxll5xizynv33q") (y #t)))

(define-public crate-os_str_bytes-0.1.2 (c (n "os_str_bytes") (v "0.1.2") (d (list (d (n "getrandom") (r "^0.1.13") (d #t) (k 2)))) (h "1xbikxs2sqsphjkv7nik4ywivirlizgp6h8h00qgplr1iqfbix8n") (y #t)))

(define-public crate-os_str_bytes-0.1.3 (c (n "os_str_bytes") (v "0.1.3") (d (list (d (n "getrandom") (r "^0.1.13") (d #t) (k 2)))) (h "13z61l5i18sbzfi3ar1j7iz0kiy2viajais2cn5wg7j8vp5vid0g") (y #t)))

(define-public crate-os_str_bytes-0.2.0 (c (n "os_str_bytes") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.1.13") (d #t) (k 2)))) (h "05n3a6q8r691nqy66rh4zk0fq9fii4wfq7gv7gyap9h5c25jhhy1") (y #t)))

(define-public crate-os_str_bytes-0.2.1 (c (n "os_str_bytes") (v "0.2.1") (d (list (d (n "getrandom") (r "^0.1.13") (d #t) (k 2)))) (h "0l3qvj8gw254s7s8qgi0xsh8wb36da87ma45ljnllv5wl3idv6g6") (y #t)))

(define-public crate-os_str_bytes-0.3.0 (c (n "os_str_bytes") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.1.13") (d #t) (k 2)))) (h "1rqb591jbmljila1jrlan6zrk2d2hjbvwpn54hz0zwb7mwai30h1") (y #t)))

(define-public crate-os_str_bytes-1.0.0 (c (n "os_str_bytes") (v "1.0.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "1ydpjczbc7blniva0j2nbg90v8xgjm77zxy6cgl6wlfpblhrxixz") (y #t)))

(define-public crate-os_str_bytes-1.0.1 (c (n "os_str_bytes") (v "1.0.1") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "0lbad4gdxh0w7ynsq6brss5px18qqbpnb4l64pzhq6nwkvsagl2i") (y #t)))

(define-public crate-os_str_bytes-1.0.2 (c (n "os_str_bytes") (v "1.0.2") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "06z9652lrbkmkjsf1xgvaq7y7d0rbwwkri8ybphcdqfgr83535l8") (y #t)))

(define-public crate-os_str_bytes-1.0.3 (c (n "os_str_bytes") (v "1.0.3") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "0wcjwvn804pw7mh844pcr2r5afli5k86mrv58yif3fsijji5w3ix") (y #t)))

(define-public crate-os_str_bytes-1.1.0 (c (n "os_str_bytes") (v "1.1.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "1554vj5ia6p7h6ggxjmd3df743qg2fc8lx9bswxjks9a7ah0jgpi") (y #t)))

(define-public crate-os_str_bytes-2.0.0 (c (n "os_str_bytes") (v "2.0.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "139vdl5g3j9jd2wmg0qyzadrk6sr4jp0sy7b9cj9hql5qr0c88kb")))

(define-public crate-os_str_bytes-2.1.0 (c (n "os_str_bytes") (v "2.1.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "00cmbpc3lwz177ffk8byczsm2jc0wh4z86474vpm0892rk9pkj94")))

(define-public crate-os_str_bytes-2.2.0 (c (n "os_str_bytes") (v "2.2.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "186vrahmmij55jfy9anjhj6y3lm73xfwwmjzvwfx6jwffrz97bas")))

(define-public crate-os_str_bytes-2.2.1 (c (n "os_str_bytes") (v "2.2.1") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "1r5hxdbcx27iix27aqkmjx0x5rpmsx0h5qq95vldjmkknlx242h3")))

(define-public crate-os_str_bytes-2.3.0 (c (n "os_str_bytes") (v "2.3.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "1m043n2l8g0fkk89baygm6j2dwkk6g1qff4l5cbshqwv8ghi5nff") (f (quote (("raw"))))))

(define-public crate-os_str_bytes-2.3.1 (c (n "os_str_bytes") (v "2.3.1") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "041m0bp3cbmfwrkpw1jbf0qh5faysgndi6i1ji68qz9l92w4gph6") (f (quote (("raw"))))))

(define-public crate-os_str_bytes-2.3.2 (c (n "os_str_bytes") (v "2.3.2") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "1b5fdm9an92vcdj6jgwapaxalhcrygjbngisjlwy60gp70szxiia") (f (quote (("raw"))))))

(define-public crate-os_str_bytes-2.4.0 (c (n "os_str_bytes") (v "2.4.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)))) (h "11agh8n3x2l4sr3sxvx6byc1j3ryb1g6flb1ywn0qhq7xv1y3cmg") (f (quote (("raw"))))))

(define-public crate-os_str_bytes-3.0.0 (c (n "os_str_bytes") (v "3.0.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)))) (h "04phgadwzxqjc4fxa5f4fnickqrhf3gignqhy2yn38mfcn4md4z2") (f (quote (("raw"))))))

(define-public crate-os_str_bytes-3.1.0 (c (n "os_str_bytes") (v "3.1.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)))) (h "0bfgm53jgdacylwd6ynjhciczmnlrp45p98h0nsrmrhglrcfzjva") (f (quote (("raw"))))))

(define-public crate-os_str_bytes-4.0.0 (c (n "os_str_bytes") (v "4.0.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "print_bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^2.0") (o #t) (d #t) (k 0)))) (h "1yq1khxhz0c13plj421gqs5m9b27m9cj4rhy9p1m0vhjdg8jd7bl") (f (quote (("raw_os_str") ("default" "raw_os_str"))))))

(define-public crate-os_str_bytes-4.1.0 (c (n "os_str_bytes") (v "4.1.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "print_bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^2.0") (o #t) (d #t) (k 0)))) (h "12cxcnccc324ahkcjrhpbw6vjlx41waphqvx1a2cdk6f6ip4aabi") (f (quote (("raw_os_str") ("default" "raw_os_str"))))))

(define-public crate-os_str_bytes-4.1.1 (c (n "os_str_bytes") (v "4.1.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "print_bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0ns7pa7nvhi9x4z46vrf0jhjs9763fnqzxmalryrp83fr4jbnjij") (f (quote (("raw_os_str") ("default" "raw_os_str"))))))

(define-public crate-os_str_bytes-4.2.0 (c (n "os_str_bytes") (v "4.2.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "16d70qzd2g18i28i6znjcpck0r9hjd5gz5qcr1cl2l9s6d1sknmd") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str"))))))

(define-public crate-os_str_bytes-5.0.0 (c (n "os_str_bytes") (v "5.0.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "157lzhx2yyb793pgah486l3qbpdwm8r0zpir54wj0vl9xjlliigf") (f (quote (("raw_os_str") ("deprecated-byte-patterns") ("default" "memchr" "raw_os_str")))) (r "1.51.0")))

(define-public crate-os_str_bytes-6.0.0 (c (n "os_str_bytes") (v "6.0.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "0r5z5xds2wzzqlqjaw96dpjsz5nqyzc1rflm4mh09aa32qyl88lf") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str")))) (r "1.52.0")))

(define-public crate-os_str_bytes-6.0.1 (c (n "os_str_bytes") (v "6.0.1") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "0ddl4664vycbxd1fbzgkxm9gk3vky9v9d9yw57g2k0hr5w5qv782") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str")))) (r "1.52.0")))

(define-public crate-os_str_bytes-6.1.0 (c (n "os_str_bytes") (v "6.1.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1ykav0kv9152z4cmv8smwmd9pac9q42sihi4wphnrzlwx4c6hci1") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str")))) (r "1.52.0")))

(define-public crate-os_str_bytes-6.2.0 (c (n "os_str_bytes") (v "6.2.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1m1w958pj5cgj3m0n7x9ifpr0pd8ils8wxpaihni1h6mwpph3034") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str")))) (r "1.57.0")))

(define-public crate-os_str_bytes-6.3.0 (c (n "os_str_bytes") (v "6.3.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1zqfx1ajwmv3g9kp95v18zwpjm2fkq6rxpsib0ig3zz3k9g43xwz") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (r "1.57.0")))

(define-public crate-os_str_bytes-6.3.1 (c (n "os_str_bytes") (v "6.3.1") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1afyx7wwn34y3ck0s5k8m9w9c7drc8nb9k6n1pmx4nakkkirdbrv") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (r "1.57.0")))

(define-public crate-os_str_bytes-6.4.0 (c (n "os_str_bytes") (v "6.4.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "0zpdkd14921yh96kzsfxxx0w65lfl3vnn62izzirw7j18xsg4nvv") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (y #t) (r "1.57.0")))

(define-public crate-os_str_bytes-6.4.1 (c (n "os_str_bytes") (v "6.4.1") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1vi3wx4zs8wgfhhzbk1n279gn8yp0n4l8s8wyb4mfm7avawj0y4v") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (r "1.57.0")))

(define-public crate-os_str_bytes-6.5.0 (c (n "os_str_bytes") (v "6.5.0") (d (list (d (n "fastrand") (r "^1.8") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "0rz2711gl575ng6vm9a97q42wqnf4wk1165wn221jb8gn17z9vff") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (r "1.57.0")))

(define-public crate-os_str_bytes-6.5.1 (c (n "os_str_bytes") (v "6.5.1") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1b1yrz32030z2xz3r1ybwyvd98ip8sww4vgr5smfjkhp9fqrwpad") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (r "1.57.0")))

(define-public crate-os_str_bytes-6.6.0 (c (n "os_str_bytes") (v "6.6.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1j2srbxn992qv1xjf2rrxvaxr2s23sr75v5lssd0jajj8k1vlzjv") (f (quote (("raw_os_str") ("nightly") ("default" "memchr" "raw_os_str") ("conversions") ("checked_conversions" "conversions")))) (r "1.61.0")))

(define-public crate-os_str_bytes-6.6.1 (c (n "os_str_bytes") (v "6.6.1") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "print_bytes") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "uniquote") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1885z1x4sm86v5p41ggrl49m58rbzzhd1kj72x46yy53p62msdg2") (f (quote (("raw_os_str") ("nightly") ("default" "memchr" "raw_os_str") ("conversions") ("checked_conversions" "conversions")))) (r "1.61.0")))

(define-public crate-os_str_bytes-7.0.0-beta.0 (c (n "os_str_bytes") (v "7.0.0-beta.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0k3qjiggjynlhn6chc133bmaad3aqf126adfrp3lhyk7c5hsx769") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("conversions") ("checked_conversions" "conversions")))) (r "1.74.0")))

(define-public crate-os_str_bytes-7.0.0 (c (n "os_str_bytes") (v "7.0.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3.5") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 2)))) (h "1fg04rslyvzrrpb99n1shx4y60a747f81gdln6cwfxzm9aclri3s") (f (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("conversions") ("checked_conversions" "conversions")))) (r "1.74.0")))

