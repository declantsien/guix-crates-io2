(define-module (crates-io os _s os_socketaddr) #:use-module (crates-io))

(define-public crate-os_socketaddr-0.1.0 (c (n "os_socketaddr") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15s382qxd4dp5qpna2rknj3b1k0vv77npxpq0nqv51lw9i2bfx4a") (y #t)))

(define-public crate-os_socketaddr-0.1.1 (c (n "os_socketaddr") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mr063ziyz4w5mvx1b04mzr2sq6xfh3wd9rpc0wns6xnhj7jhs5h") (y #t)))

(define-public crate-os_socketaddr-0.2.0 (c (n "os_socketaddr") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "1llv37bpwkj7ia222h6kgkksqqa7jcmp229zrkzzj0vrwvg8dzw7") (y #t)))

(define-public crate-os_socketaddr-0.2.1 (c (n "os_socketaddr") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "1ys5l3hrnkrpf4alp1ca2z0r8b86sak7w833v0aaj5dax5zqg410") (y #t)))

(define-public crate-os_socketaddr-0.2.2 (c (n "os_socketaddr") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "160nz7psyv32mgyy4xl02v5bsgwjnwz5dx90n6jkx99wpqi63dis")))

(define-public crate-os_socketaddr-0.2.3 (c (n "os_socketaddr") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "1gqxh9q7imh7gjz5s7rw85jii1af7l66k8r0xnwjpdylk48g0p26")))

(define-public crate-os_socketaddr-0.2.4 (c (n "os_socketaddr") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "0c9wqkgqrz93gd5042hcdgkj4c3n2hbkhx5i47kc9xy8nhgnl5g9")))

(define-public crate-os_socketaddr-0.2.5 (c (n "os_socketaddr") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "0i5jsj298k8g0zmx9lbklflki83f3vbzr443jr0ypbcwq0z9y57s")))

