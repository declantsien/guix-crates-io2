(define-module (crates-io os -d os-detect) #:use-module (crates-io))

(define-public crate-os-detect-0.1.0 (c (n "os-detect") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.1.2") (d #t) (k 0)) (d (n "sys-mount") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0iai5akkbw6iw44zfav6vaw2njasqfjx3l45wmp0cwvqnr2phzl6") (y #t)))

(define-public crate-os-detect-0.2.0 (c (n "os-detect") (v "0.2.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.1.3") (d #t) (k 0)) (d (n "sys-mount") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "10nkpggzggwk9rbzlzq6v3dhcgmp009r0gcwc5flzigpmvd75yda") (y #t)))

(define-public crate-os-detect-0.2.1 (c (n "os-detect") (v "0.2.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.1.6") (d #t) (k 0)) (d (n "sys-mount") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0kwsygckizvjfpmx5p0ds7kx5frc4jwar8p8six38qzb0nzlnbf4") (y #t)))

(define-public crate-os-detect-0.2.2 (c (n "os-detect") (v "0.2.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.2.0") (d #t) (k 0)) (d (n "sys-mount") (r "^1.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0w8i1kcvw34v4z63ngr5r8f1nrr7hppmw023r373kxh47cv17sha")))

