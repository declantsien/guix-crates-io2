(define-module (crates-io os -q os-query-builder-rs) #:use-module (crates-io))

(define-public crate-os-query-builder-rs-0.1.0 (c (n "os-query-builder-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1c9wv2s7kvxmjx825j18sfhj990dvs8d105bxr6ps371wdlv6rb4")))

(define-public crate-os-query-builder-rs-0.1.1 (c (n "os-query-builder-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0f1xiwj7cpwgqz12w6xvkbk3gf2q2j41cj5bhi41mra02yw9l2ix")))

(define-public crate-os-query-builder-rs-0.1.2 (c (n "os-query-builder-rs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0lfhyqqsbc7gzwl41cnvgvp0qanjfzvb8zb1z8m04p74rapyznbg")))

(define-public crate-os-query-builder-rs-0.1.3 (c (n "os-query-builder-rs") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0qpfbllgyb53r9r8gdhwkmp043w295x7f7lb4qf81dq0mqb5ir6q")))

(define-public crate-os-query-builder-rs-0.1.4 (c (n "os-query-builder-rs") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1pbjb5zq68mmr4a5x8p2l95rf8csh0l67qv76imxyfyh1l75k4p2")))

(define-public crate-os-query-builder-rs-0.1.5 (c (n "os-query-builder-rs") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0dc5j17llvpi6c52jmmcm81pix2y43d7ygg8q17nbmj3gafcxhlz")))

