(define-module (crates-io os _t os_type) #:use-module (crates-io))

(define-public crate-os_type-0.1.0 (c (n "os_type") (v "0.1.0") (h "0lh0sfg8bgks5b6ph4a8j1if6br7wzwyn9sz953a330i0z00j0ck")))

(define-public crate-os_type-0.1.1 (c (n "os_type") (v "0.1.1") (h "164087z8vmhll5zz06gk1bmjpd1080bvx8bnzggn72j7s6slcmj5")))

(define-public crate-os_type-0.2.0 (c (n "os_type") (v "0.2.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "1lc6svzzp6lqp0p62g2d2g1v27h08jnbz6vamkm48ysxjwfcpm1b")))

(define-public crate-os_type-0.2.1 (c (n "os_type") (v "0.2.1") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "1fn25d46ji8jnypkyjma5xbwls7fcnlhnffdn0mjq8lism1a5z7h")))

(define-public crate-os_type-0.2.2 (c (n "os_type") (v "0.2.2") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "0281pl6kk95mv3mgw72disx48gp6wkbvij6g31v9ghqzrhv8zrvp")))

(define-public crate-os_type-0.4.0 (c (n "os_type") (v "0.4.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "08fbs9afcbnri6j9j1ssmk7s1k05mvxj1v9fpxy1cpwkrmp0a16q")))

(define-public crate-os_type-0.5.0 (c (n "os_type") (v "0.5.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "0h0r5idfhwsi026c0gm7jhk777mz7qnysvmiciyy80x7jzd1d999")))

(define-public crate-os_type-0.6.0 (c (n "os_type") (v "0.6.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "0yp7smyn95v1qbj9l3z58nv5w7kx7sh51xh1pfzfla0l78w8y3ln")))

(define-public crate-os_type-1.0.0 (c (n "os_type") (v "1.0.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "0qc4ch3fkr15dmw8gra8hpflsm4hgqsywzshbxbanl9ypbv9hvmq")))

(define-public crate-os_type-2.0.0 (c (n "os_type") (v "2.0.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "028n8bbxzlab4mmvw5x1kplqbdnhibljzmz0xs7n64bnylwha7q8")))

(define-public crate-os_type-2.1.1 (c (n "os_type") (v "2.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qdw36nk0gy9c9rljrp3wjr7zfhxnclmxyxjwsziy1gl9jj2dxzd")))

(define-public crate-os_type-2.2.0 (c (n "os_type") (v "2.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sz90xi9br82zmp3s0fbp397ajm5f0xsir7pikwbg65fy0d03p3y")))

(define-public crate-os_type-2.3.0 (c (n "os_type") (v "2.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "146zx5mh84n5p2xhlwb8b75wbkqw87fvb87n3adl44lz5pifpsln")))

(define-public crate-os_type-2.4.0 (c (n "os_type") (v "2.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1293gl2x38q7bhk3f7155k1aq0nvhq6vdksgz1cq6abhchgpdpy3")))

(define-public crate-os_type-2.5.0 (c (n "os_type") (v "2.5.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p3fs2qqwq2dl1lprr42qpp0aah7rb84qmk85fjv11cpqpbjvi6c")))

(define-public crate-os_type-2.6.0 (c (n "os_type") (v "2.6.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19bv5jq9z04bw3kf9qdxw76yngjy9g5dmxnqdr8nf0d3xv048kg2")))

