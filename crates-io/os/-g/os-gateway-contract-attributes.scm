(define-module (crates-io os -g os-gateway-contract-attributes) #:use-module (crates-io))

(define-public crate-os-gateway-contract-attributes-1.0.0 (c (n "os-gateway-contract-attributes") (v "1.0.0") (d (list (d (n "cosmwasm-std") (r "=1.0.0") (d #t) (k 2)))) (h "0zmc6wsz67kaimx0jpiiq1l4f32qx3d449ll1r40mrh6bl5nd1li") (f (quote (("library"))))))

(define-public crate-os-gateway-contract-attributes-1.0.1 (c (n "os-gateway-contract-attributes") (v "1.0.1") (d (list (d (n "cosmwasm-std") (r "=1.0.0") (d #t) (k 2)))) (h "0fyrbx4mj1b0bhqqs37gx1v4fh8rnkwq3017fba3kj9wbmwr6jq7") (f (quote (("library"))))))

