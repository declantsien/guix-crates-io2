(define-module (crates-io os ma osmanthus) #:use-module (crates-io))

(define-public crate-osmanthus-0.0.1 (c (n "osmanthus") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0p53ah5c9vcjxjhpap3r8j09l6ps23h8ldknl44hlvqdclsf004r")))

(define-public crate-osmanthus-0.0.2 (c (n "osmanthus") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0djbjbf2hf806r3mm3b3dz4rk1mlgiwwglqrgnml3gywdabgsnpc")))

(define-public crate-osmanthus-0.0.3 (c (n "osmanthus") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "142whb24cmwsv743cdz6navzp64mqvh77ca3s7s033ilqzbh4i5l")))

(define-public crate-osmanthus-1.0.0 (c (n "osmanthus") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0s5ypjp0sym67r16igzsg7h2fv41pvsd3pl1hglkrkqbv16282hq")))

