(define-module (crates-io os m4 osm4routing) #:use-module (crates-io))

(define-public crate-osm4routing-0.1.0 (c (n "osm4routing") (v "0.1.0") (d (list (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.3") (d #t) (k 0)))) (h "1pfq6766m80qlndf0xcqir0lpiv9fw6fxj2g6bpaxxffpjkb8rsr")))

(define-public crate-osm4routing-0.2.0 (c (n "osm4routing") (v "0.2.0") (d (list (d (n "csv") (r "^0.14.7") (d #t) (k 0)) (d (n "docopt") (r "^0.6.86") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.3") (d #t) (k 0)))) (h "0zvp22npabl75v3cdg4q4maqk66gmzc0hmk75fv3j1vj2mkjjvrs")))

(define-public crate-osm4routing-0.3.0 (c (n "osm4routing") (v "0.3.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.14.0") (d #t) (k 0)))) (h "19phv9qsm61qsnxkcmfxkswla0ym8ys8wpcfpkn8vc1f7hns972b")))

(define-public crate-osm4routing-0.4.0 (c (n "osm4routing") (v "0.4.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.15.2") (d #t) (k 0)))) (h "13v0p4ysirlzgm82j75s6qkfy7wcfnm7j1jl1lqlxqjl5b34jxbr")))

(define-public crate-osm4routing-0.4.1 (c (n "osm4routing") (v "0.4.1") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)))) (h "1364470n74nqr8yy6lbn0src36kj2rp24fkz6aipw9kgyapm1j5z")))

(define-public crate-osm4routing-0.5.0 (c (n "osm4routing") (v "0.5.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0m0k1wsh1mbpsykxy2v7ax90sfnz7b1fla4gai2xpgvbk4ir53kv")))

(define-public crate-osm4routing-0.5.1 (c (n "osm4routing") (v "0.5.1") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1rnar0xakx6zrrbs530ngr3fznxa5xadzwmslfwbp5610qbdr9s7")))

(define-public crate-osm4routing-0.5.2 (c (n "osm4routing") (v "0.5.2") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02l6w81265ck5hhrngz2wh8lig9z0kgv6ar18as94pl12gmh6560")))

(define-public crate-osm4routing-0.5.3 (c (n "osm4routing") (v "0.5.3") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0hacx22m0xkqm5zala0gas11accxaz0akyiqklnw0wsyynl55jjn")))

(define-public crate-osm4routing-0.5.4 (c (n "osm4routing") (v "0.5.4") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0g8wf4i3hzjb0grsb34gir02a5pqyv7wf27hsqqc2r28ns45spl4")))

(define-public crate-osm4routing-0.5.5 (c (n "osm4routing") (v "0.5.5") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1146zfhd38vzdk5fmy8yawqlxamg2waw1vbvz5gdzpv5plrf7xc3")))

(define-public crate-osm4routing-0.5.6 (c (n "osm4routing") (v "0.5.6") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0nmqb95ym657rgcwhraic83bj0xfrxwc7vm3iv75mimhca87z8b7")))

(define-public crate-osm4routing-0.5.7 (c (n "osm4routing") (v "0.5.7") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1lhrjk7xsk0wprfa6jk1yr2ldldcddl5vcc42f4bz25852zqjmrc")))

(define-public crate-osm4routing-0.5.8 (c (n "osm4routing") (v "0.5.8") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "docopt") (r "^1.1.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ay4hgsy1zficrhr1hsrwxxd53mli87gnsgsyy6mqy2paadi5p63")))

(define-public crate-osm4routing-0.5.9 (c (n "osm4routing") (v "0.5.9") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "docopt") (r "^1.1.1") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0fwg13kj34af3nqdyydwydc4y2004kdvcfabswj9498wy4jpx7iq")))

(define-public crate-osm4routing-0.6.0 (c (n "osm4routing") (v "0.6.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0lp2ig0b0cbr0hiwys3x80vpwpz6fzznijiy5j6v0k6jg3jch0fi")))

(define-public crate-osm4routing-0.6.1 (c (n "osm4routing") (v "0.6.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0f0na6lhmvzvdkfiypy7hmxgn9rx9r2kz8clvxj06gbp8mbqy2ph")))

(define-public crate-osm4routing-0.6.2 (c (n "osm4routing") (v "0.6.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "045r0c5yisjlqighwhdizlazjhnqshblhr4qs5hrd2lgqnrjsnrs")))

