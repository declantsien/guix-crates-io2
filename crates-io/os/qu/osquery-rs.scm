(define-module (crates-io os qu osquery-rs) #:use-module (crates-io))

(define-public crate-osquery-rs-0.1.2 (c (n "osquery-rs") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"macos\"))") (k 2)) (d (n "named_pipe") (r "^0.4.1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0xz2zgzzm4ayvav8qvlwyp9q6k7bzlffms3aqm8pcqd40x6lyw3y")))

(define-public crate-osquery-rs-0.1.3 (c (n "osquery-rs") (v "0.1.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"macos\"))") (k 2)) (d (n "named_pipe") (r "^0.4.1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0nq8ik08l0v2zcd6v0lms9v00ysq73ivdh3i58wm6nl3y6i8n0r5")))

