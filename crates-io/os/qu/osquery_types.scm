(define-module (crates-io os qu osquery_types) #:use-module (crates-io))

(define-public crate-osquery_types-0.1.0 (c (n "osquery_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "00qvmwf50a5g18xlh8li36zpdqamcksn95wghlnaqniqdcc8yh28")))

(define-public crate-osquery_types-0.1.1 (c (n "osquery_types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "130brqs0l0ij0lyi1ryr2n84fhybs6i73bqf7ywd6l53jn8rlhsm")))

