(define-module (crates-io os qu osquery-rust-codegen) #:use-module (crates-io))

(define-public crate-osquery-rust-codegen-0.1.0 (c (n "osquery-rust-codegen") (v "0.1.0") (h "1ckr2wxbvzkcw45ac8ynlza1yr1w2422bf9qhvhyxvi611ndz3py")))

(define-public crate-osquery-rust-codegen-0.1.1 (c (n "osquery-rust-codegen") (v "0.1.1") (h "16w25kigvaa91cr7q6rg56ssg5nxfakliysn3iymlm1q1sl4dqwg")))

