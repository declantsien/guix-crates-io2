(define-module (crates-io os dp osdpctl) #:use-module (crates-io))

(define-public crate-osdpctl-0.0.1 (c (n "osdpctl") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libosdp") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("signal"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "14cin2d47q30ls6y6kfg52w0m01iic6iykqxdnda6v0p63vm059g")))

