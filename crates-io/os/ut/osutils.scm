(define-module (crates-io os ut osutils) #:use-module (crates-io))

(define-public crate-osutils-0.1.0 (c (n "osutils") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "nix") (r ">=0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "path-clean") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08534a64vgzk89qd982vn9fya3j3l5mani2wdm7wsrp856makx71")))

