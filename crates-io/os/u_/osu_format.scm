(define-module (crates-io os u_ osu_format) #:use-module (crates-io))

(define-public crate-osu_format-0.1.0 (c (n "osu_format") (v "0.1.0") (h "1cv1grpv1c7i6j0xi1pjw5spp9qvlb1pchml5736ynzr3iwha44g")))

(define-public crate-osu_format-0.1.1 (c (n "osu_format") (v "0.1.1") (h "0mgll1as2gksdhs3p43rs52gqm0ymqflkdprg87vmccjzawcl3g5")))

