(define-module (crates-io os me osmesa-sys) #:use-module (crates-io))

(define-public crate-osmesa-sys-0.0.0 (c (n "osmesa-sys") (v "0.0.0") (d (list (d (n "gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "09622h4rbqa3m4rz7qb2l4vxwg0samzmq59d3q6z3ab6f2bfpmyg")))

(define-public crate-osmesa-sys-0.0.1 (c (n "osmesa-sys") (v "0.0.1") (d (list (d (n "gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0vvny6mq0gmqkwmj48ayxzfxp06jhgdsiw03pp2mz2rh688jj6xl")))

(define-public crate-osmesa-sys-0.0.2 (c (n "osmesa-sys") (v "0.0.2") (d (list (d (n "gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0hgvdi9q20fzhn13im1pzk37rbpfmwrffsfi4zrm0zmp19jgg76h") (f (quote (("force-link"))))))

(define-public crate-osmesa-sys-0.0.3 (c (n "osmesa-sys") (v "0.0.3") (d (list (d (n "gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1pcjhza5nrcsw21jj69gjfwn2k4y5mvngjlccxf7727bfm7a0ds9")))

(define-public crate-osmesa-sys-0.0.4 (c (n "osmesa-sys") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "shared_library") (r "^0.1") (d #t) (k 0)))) (h "10161qbiji7zlp9x1j38dkavjdb9yzw3ia6k3xb4qcqdw58qq9w2")))

(define-public crate-osmesa-sys-0.0.5 (c (n "osmesa-sys") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "shared_library") (r "^0.1") (d #t) (k 0)))) (h "179nqpaxaz1x48gs365v5cmwm76849n2rpw0q92ms9gsf26jsp72")))

(define-public crate-osmesa-sys-0.0.6 (c (n "osmesa-sys") (v "0.0.6") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "shared_library") (r "^0.1") (d #t) (k 0)))) (h "0fgvw9pshzr0i174qc4lj112fnys6xi442n78palxgjsr9b5hna6")))

(define-public crate-osmesa-sys-0.1.0 (c (n "osmesa-sys") (v "0.1.0") (d (list (d (n "shared_library") (r "^0.1") (d #t) (k 0)))) (h "0hmvi2f95qh51g9bf380pjqx03478b9g0jkxxa4fd5wc9262552n")))

(define-public crate-osmesa-sys-0.1.1 (c (n "osmesa-sys") (v "0.1.1") (d (list (d (n "shared_library") (r "^0.1") (d #t) (k 0)))) (h "13c2iwphkvzn7b1vwqs21g4mkfgvq96hsbxmdfmwsd8ljy90615h")))

(define-public crate-osmesa-sys-0.1.2 (c (n "osmesa-sys") (v "0.1.2") (d (list (d (n "shared_library") (r "^0.1") (d #t) (k 0)))) (h "0fq1q1zcgfb0qydrg9r2738jlwc4hqxgb9vj11z72bjxx7kfrkw8")))

