(define-module (crates-io os as osascript) #:use-module (crates-io))

(define-public crate-osascript-0.1.0 (c (n "osascript") (v "0.1.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0wqqy0gbc29pp7v02v7m2wlxh52y4k23xgpz8c95h0gwa0g60n0j")))

(define-public crate-osascript-0.2.0 (c (n "osascript") (v "0.2.0") (d (list (d (n "serde") (r "^0") (d #t) (k 0)) (d (n "serde_derive") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^0") (d #t) (k 0)))) (h "0pj4iyhvwk9vr0cwsvlhswfyl5bfhz6ypng6z7fvizi42j9ncqcp")))

(define-public crate-osascript-0.3.0 (c (n "osascript") (v "0.3.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p47zqg463wrymv7yilqy91b89jr2ri9bjk6xhd9yrzgb6l1ywrq")))

