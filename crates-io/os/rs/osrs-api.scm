(define-module (crates-io os rs osrs-api) #:use-module (crates-io))

(define-public crate-osrs-api-0.1.0 (c (n "osrs-api") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.2") (d #t) (k 0)))) (h "062hlfxr7ppr5h8170cpv25fs12a3fm96fhak007j8vxf4cz5rsv")))

