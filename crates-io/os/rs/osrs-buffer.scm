(define-module (crates-io os rs osrs-buffer) #:use-module (crates-io))

(define-public crate-osrs-buffer-0.0.0 (c (n "osrs-buffer") (v "0.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1ggcdgvg9g3kz92slinp8rd6fx4y16hllpb4smrdlbi1fghwsgwx")))

(define-public crate-osrs-buffer-0.1.0 (c (n "osrs-buffer") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "181z3l1lg5lb59bnsmky95zncya51qi4bfs8a5whvycasc4l7i29")))

(define-public crate-osrs-buffer-0.2.0 (c (n "osrs-buffer") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0v17qzrgjhjsj6zvsf5885fq5n5n33bl2c2kia1x5z1r2nbjssyw")))

(define-public crate-osrs-buffer-0.3.0 (c (n "osrs-buffer") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1y7kq0wg5h714jy2psap36qic1gnxf0f9c54gad84nc00qpfs1f4")))

(define-public crate-osrs-buffer-0.4.0 (c (n "osrs-buffer") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0ssy8xafgjxkyxaq1yw9f5nh6kx34g7palsp27dcln3ih2w3mngw")))

(define-public crate-osrs-buffer-0.5.0 (c (n "osrs-buffer") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0wpjzfi5d14shw30x38rc07wc3yhp45yzsb30vyl5mhbf6lqgfk3")))

(define-public crate-osrs-buffer-0.6.0 (c (n "osrs-buffer") (v "0.6.0") (h "0crbzk4h89ampq1gawxhi5hrfbdc8pkf3x5icmiq00271nb816ix")))

(define-public crate-osrs-buffer-0.7.0 (c (n "osrs-buffer") (v "0.7.0") (h "0f1h0zg5vzql3xnhxpwcihbgl15zqh8c21703q0m342qnmqkz5ck")))

