(define-module (crates-io os rs osrs-bytes) #:use-module (crates-io))

(define-public crate-osrs-bytes-0.0.0 (c (n "osrs-bytes") (v "0.0.0") (h "0405jrnky5xaqygg2h7a95rl723pxcqwqabb4d4kawl9chhlwxzy")))

(define-public crate-osrs-bytes-0.1.0 (c (n "osrs-bytes") (v "0.1.0") (h "0j3gkfk267y0f8lq76c0avf8g6vdiq39z3p4swlli0n5aiqyl0ac")))

(define-public crate-osrs-bytes-0.2.0 (c (n "osrs-bytes") (v "0.2.0") (h "0ianj88s6j81bf47qifd8ij4gb5chlj3zh6m4fxrfh9gnmfa3p5c")))

(define-public crate-osrs-bytes-0.3.0 (c (n "osrs-bytes") (v "0.3.0") (h "0d8wbr11slmn793qm2r8zi6k1aivsy8h1f8b77fhf13bcljx4ab9")))

(define-public crate-osrs-bytes-0.4.0 (c (n "osrs-bytes") (v "0.4.0") (h "053j6fa1wl9h008kr66ap4j78xp4g49p09wwrps12cfhhvi1a724")))

(define-public crate-osrs-bytes-0.5.0 (c (n "osrs-bytes") (v "0.5.0") (h "1qv3x5bfkl6i7x54d25ljys0jqmpr7r610njs6bfynmz8l0l9442")))

