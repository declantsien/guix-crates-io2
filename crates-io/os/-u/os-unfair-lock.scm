(define-module (crates-io os -u os-unfair-lock) #:use-module (crates-io))

(define-public crate-os-unfair-lock-0.1.0 (c (n "os-unfair-lock") (v "0.1.0") (h "18zakij26sqxzx0rj9l3gi5sk7rg7h0w33l24igcbk3i4bh72glp") (f (quote (("nightly")))) (y #t)))

(define-public crate-os-unfair-lock-0.2.0 (c (n "os-unfair-lock") (v "0.2.0") (h "0dr7kq942hp6k9ikla4i0a1fgiq3b6qly48frcbnlj3gyka6rfv2") (f (quote (("nightly")))) (y #t)))

(define-public crate-os-unfair-lock-0.3.0 (c (n "os-unfair-lock") (v "0.3.0") (h "13crvvf4jg16cvgwkzwhr07kbzlkbgpzch2cr0mxgk5djpa2kclr") (f (quote (("nightly"))))))

