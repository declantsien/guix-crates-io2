(define-module (crates-io os ir osiris-typed) #:use-module (crates-io))

(define-public crate-osiris-typed-0.1.0 (c (n "osiris-typed") (v "0.1.0") (d (list (d (n "osiris-data") (r "0.*") (f (quote ("data"))) (k 0)) (d (n "osiris-process") (r "0.*") (f (quote ("base-floating"))) (k 0)))) (h "1bnvcdwf5xi4g4jjkx8iqczlskjm7hmsiw00a0730x53szvmml0w")))

(define-public crate-osiris-typed-0.1.1 (c (n "osiris-typed") (v "0.1.1") (d (list (d (n "osiris-data") (r "0.*") (f (quote ("data"))) (k 0)) (d (n "osiris-process") (r "0.*") (f (quote ("base-floating"))) (k 0)))) (h "0q7gkk0cvhfdsd8fk6y2q6avzf2d2jq9spbsq1bw3z31lvppymv7")))

