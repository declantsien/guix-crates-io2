(define-module (crates-io os ir osiris-process) #:use-module (crates-io))

(define-public crate-osiris-process-0.1.0 (c (n "osiris-process") (v "0.1.0") (d (list (d (n "osiris-data") (r "^0.1.2") (d #t) (k 0)))) (h "1y3qirs3j09v20y13iwkalqqry7lzd8r54hni7kxn8p2v2fy351x") (y #t)))

(define-public crate-osiris-process-0.1.1 (c (n "osiris-process") (v "0.1.1") (d (list (d (n "osiris-data") (r "^0.1.2") (d #t) (k 0)))) (h "1rds028fw3mq8pl82ay7xgxcz160zdhjy63a5k5nb3bmk2qgcbpl") (y #t)))

(define-public crate-osiris-process-0.1.2 (c (n "osiris-process") (v "0.1.2") (d (list (d (n "osiris-data") (r "^0.1.2") (d #t) (k 0)))) (h "014hg46xl08n55a8zci192kgay4kjphpijf76y3ygqav04gkif37") (y #t)))

(define-public crate-osiris-process-0.1.3 (c (n "osiris-process") (v "0.1.3") (d (list (d (n "osiris-data") (r "^0.1") (d #t) (k 0)))) (h "06scp8x40y7070ybm5dxvs6g5qyvhs9lld0w8jwhpx3yd8v8m18y") (y #t)))

(define-public crate-osiris-process-0.1.4 (c (n "osiris-process") (v "0.1.4") (d (list (d (n "osiris-data") (r "^0.1") (d #t) (k 0)))) (h "129r84nlngdlhnnlsf4wigva61yxb2ny8kq2xy691xkb2q9d0syz")))

(define-public crate-osiris-process-0.2.0 (c (n "osiris-process") (v "0.2.0") (d (list (d (n "osiris-data") (r "^0.1") (d #t) (k 0)))) (h "0pdz2q08yq09fiphn01is6g9j591nc0397kza4slxncib2kzr7r3")))

(define-public crate-osiris-process-0.2.1 (c (n "osiris-process") (v "0.2.1") (d (list (d (n "osiris-data") (r "^0.1") (d #t) (k 0)))) (h "1iyf96dl4c9g06qbbrmsh0639mcjhqizy44jzgzr3l9482308wfl")))

(define-public crate-osiris-process-0.2.2 (c (n "osiris-process") (v "0.2.2") (d (list (d (n "osiris-data") (r "0.*") (d #t) (k 0)))) (h "1m28131w7wdrj27v3qzwc7ydwk5izpwgm2r2dxymf37jn1ah4hcz")))

(define-public crate-osiris-process-0.2.3 (c (n "osiris-process") (v "0.2.3") (d (list (d (n "osiris-data") (r "0.*") (d #t) (k 0)))) (h "1bmrwkizhnz8sspijk918f9lnmy0rfpv8wn8lkbwkpbrd4nmiy26")))

(define-public crate-osiris-process-0.2.4 (c (n "osiris-process") (v "0.2.4") (d (list (d (n "osiris-data") (r "0.*") (d #t) (k 0)))) (h "11cwhgw3j8dr7m8mc2p4dz0d8is1jhs6azn1b3nffa7vsh1v8fr0")))

(define-public crate-osiris-process-0.3.0 (c (n "osiris-process") (v "0.3.0") (d (list (d (n "osiris-data") (r "0.*") (d #t) (k 0)))) (h "1gw88k467qadzm1hm6774k9dsa5f347370villr9pq7ijnwd1kpc") (f (quote (("state" "base-integral") ("processor" "communication" "base-floating" "state") ("default" "processor") ("communication") ("base-integral") ("base-floating"))))))

(define-public crate-osiris-process-0.3.1 (c (n "osiris-process") (v "0.3.1") (d (list (d (n "osiris-data") (r "0.*") (d #t) (k 0)))) (h "03hlh5jndbkh4qdmas8gzxdhpwnm1c47j0x0sk1lps45v9f07wgm") (f (quote (("state" "base-integral") ("processor" "communication" "base-floating" "state") ("default" "processor") ("communication") ("base-integral") ("base-floating"))))))

