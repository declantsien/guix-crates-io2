(define-module (crates-io os ir osiris-data) #:use-module (crates-io))

(define-public crate-osiris-data-0.1.0 (c (n "osiris-data") (v "0.1.0") (h "0ccjz2mb74cnfgy6rk4sc9phzxzf5wwkgz1qv96b6351aj8pb0rf")))

(define-public crate-osiris-data-0.1.1 (c (n "osiris-data") (v "0.1.1") (h "0xcz9xfc0hphrlsjqhdfs82fwgl9kdp66lml5vmwf11jllr17pmr")))

(define-public crate-osiris-data-0.1.2 (c (n "osiris-data") (v "0.1.2") (h "0ql673pqb5qx6ihm87ajgcq6rkvq9a4q96gfkwia23hwhzvp111p")))

(define-public crate-osiris-data-0.1.3 (c (n "osiris-data") (v "0.1.3") (h "1lm44xf8dvzn66bpylmp2ldmsisrbbqf9qdr2xbik213p8w7vrhv")))

(define-public crate-osiris-data-0.1.4 (c (n "osiris-data") (v "0.1.4") (h "0zhl90zmz8k4980zrc4gzjv269w4z9yrcax9khmxid29hf6y5ya3")))

(define-public crate-osiris-data-0.1.4-withmoredoc (c (n "osiris-data") (v "0.1.4-withmoredoc") (h "0dn2b9i29bddrbjr5yp0nadsq4j1z6gmv33y51jjamgv4nis8x0c") (y #t)))

(define-public crate-osiris-data-0.1.5 (c (n "osiris-data") (v "0.1.5") (h "1l73cqmrn8fbvslhgmrdd3d69q8lz3inn1b15z2hs2wj3s3qyaf2") (f (quote (("memory" "data") ("default" "memory") ("data"))))))

(define-public crate-osiris-data-0.1.6 (c (n "osiris-data") (v "0.1.6") (h "0h0bfkhngwfbd5h2c3kaq0jrn6kp1gkh2b3z4z3hv7idfn6c7hjx") (f (quote (("memory" "data") ("default" "memory") ("data"))))))

(define-public crate-osiris-data-0.2.0 (c (n "osiris-data") (v "0.2.0") (h "13vy4iwsd46ny3nln672sww7dbfl0a1fx9p2vb5ws5qznrrns7br") (f (quote (("memory" "data") ("default" "memory") ("data") ("converters" "data"))))))

(define-public crate-osiris-data-0.2.1 (c (n "osiris-data") (v "0.2.1") (h "15cs4d4jzzkrbv9pcxajij9mynab3sbgix9cqvxndgx7dy8g60lw") (f (quote (("memory" "data") ("default" "memory") ("data") ("converters" "data"))))))

