(define-module (crates-io os ir osiris-display) #:use-module (crates-io))

(define-public crate-osiris-display-0.1.0 (c (n "osiris-display") (v "0.1.0") (d (list (d (n "osiris-data") (r "0.*") (d #t) (k 0)) (d (n "osiris-typed") (r "0.*") (d #t) (k 0)))) (h "0s93ifdwb0jpxv8rv9hbczmzr9k23402915m1w5rp4id2czv2zfa")))

(define-public crate-osiris-display-0.1.1 (c (n "osiris-display") (v "0.1.1") (d (list (d (n "osiris-data") (r "0.*") (d #t) (k 0)) (d (n "osiris-typed") (r "0.*") (d #t) (k 0)))) (h "0fic48fqs2l79igbqc9p2ai8hmfmz5ld8yxzkx719z6q1jijxbsb")))

