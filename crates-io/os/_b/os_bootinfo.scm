(define-module (crates-io os _b os_bootinfo) #:use-module (crates-io))

(define-public crate-os_bootinfo-0.1.0 (c (n "os_bootinfo") (v "0.1.0") (d (list (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "1i8qckk07mxc0d3hsp90axmnnmfsh43mji80mqc2yi5rbpj6dn5p")))

(define-public crate-os_bootinfo-0.2.0-alpha-001 (c (n "os_bootinfo") (v "0.2.0-alpha-001") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "17l3psvyvl8nmhsbnn69v72xmziwqqldg637k4skcn13iiqyf3vb")))

(define-public crate-os_bootinfo-0.2.0-alpha-002 (c (n "os_bootinfo") (v "0.2.0-alpha-002") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0ag1akhldsfsjhmgjp22c12r4nazr3aj5jixgki1hz5f6bswmg51")))

(define-public crate-os_bootinfo-0.2.0-alpha-003 (c (n "os_bootinfo") (v "0.2.0-alpha-003") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "1fbbz5azxspx975yxigk2mhgbz958w42kp4vwcja568iixw7nz2k")))

(define-public crate-os_bootinfo-0.2.0-alpha-004 (c (n "os_bootinfo") (v "0.2.0-alpha-004") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0xg9mgp5ahzr3r641j637k17kyzy4lp2fq615maqzr6i5dkgx63n")))

(define-public crate-os_bootinfo-0.2.0-alpha-005 (c (n "os_bootinfo") (v "0.2.0-alpha-005") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0r7hxshdaw92zb9i7dsrgjx9m3zsgqfpajzz2i929m3mm8pdjlh4")))

(define-public crate-os_bootinfo-0.2.0-alpha-006 (c (n "os_bootinfo") (v "0.2.0-alpha-006") (d (list (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "04rylbyn2dmznh9c5fr0sd14mp25i5x4ry442vi6v7yp1svd5k9c")))

(define-public crate-os_bootinfo-0.2.0-alpha-007 (c (n "os_bootinfo") (v "0.2.0-alpha-007") (d (list (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "07a1zm41c5g3w8j4hhyszi05pb26429aakybxxq208p3ga4y372l")))

(define-public crate-os_bootinfo-0.2.0-alpha-008 (c (n "os_bootinfo") (v "0.2.0-alpha-008") (d (list (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0fpcsf5glg7kvw5zjva54cg75dhgbwfpz0j0g4n5qf4cag554hii")))

(define-public crate-os_bootinfo-0.2.0-alpha-009 (c (n "os_bootinfo") (v "0.2.0-alpha-009") (d (list (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0r81w9pkj4ba10s4iap2h2rbiwjjzby71m22zslpkmm4xys5ql9x")))

(define-public crate-os_bootinfo-0.2.0-alpha-010 (c (n "os_bootinfo") (v "0.2.0-alpha-010") (d (list (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0hqk5wgvnhsqyaigir82pziq2byyynd5iz2nlcxbr9pgq7r7zkfh")))

(define-public crate-os_bootinfo-0.2.0-alpha-011 (c (n "os_bootinfo") (v "0.2.0-alpha-011") (d (list (d (n "x86_64") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "1b0bg9z239ndbl4ipx6827la07l38fp8k8wk1l6s8l30n4qwnya8")))

(define-public crate-os_bootinfo-0.2.0 (c (n "os_bootinfo") (v "0.2.0") (h "0mj3wyil2w2i0n18l889wihf677pxhzmnyyjf0w5q4zvw17a8lv9")))

(define-public crate-os_bootinfo-0.2.1 (c (n "os_bootinfo") (v "0.2.1") (h "02invdrjcx7kqig8qa1q8f1nyy1h5h2dcg5nhnyyfwz7nnz1sj36")))

(define-public crate-os_bootinfo-0.3.0 (c (n "os_bootinfo") (v "0.3.0") (h "0bjgygfjayx2rablv6jrwik4yvr3dpai2dii1p0k1l30d1mskzyk") (y #t)))

