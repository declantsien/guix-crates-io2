(define-module (crates-io os ra osrand) #:use-module (crates-io))

(define-public crate-osrand-0.0.1 (c (n "osrand") (v "0.0.1") (h "038kg7qzqnk39gz0c9rkgpp6mgp2ha0l528rz40zdzb1l3gmiz93") (y #t)))

(define-public crate-osrand-0.2.0 (c (n "osrand") (v "0.2.0") (h "0fxi1r1zz156pm2ppddqfwm61ppzqfvp544x9wvqz985rj6wdnw8")))

(define-public crate-osrand-0.2.1 (c (n "osrand") (v "0.2.1") (h "0m3bidhig96ignfynqyxcwpjq8mwr9zgyc64mhx5njqkbaspvx5p") (f (quote (("urandom") ("default" "urandom"))))))

