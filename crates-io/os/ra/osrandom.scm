(define-module (crates-io os ra osrandom) #:use-module (crates-io))

(define-public crate-osrandom-0.1.0 (c (n "osrandom") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1ps0n4c7hr5h2x1fj88l2r40ngd6r7p1n47q813r4pyvv57pnd53") (f (quote (("default"))))))

