(define-module (crates-io os pf ospf-parser) #:use-module (crates-io))

(define-public crate-ospf-parser-0.1.0 (c (n "ospf-parser") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "nom-derive") (r "^0.5") (d #t) (k 0)) (d (n "rusticata-macros") (r "^2.0.3") (d #t) (k 0)))) (h "1lv2l5jk8fwccvy6q7gj6cbm9sc7yzs2iwqzwz9ja15c7ljh6lnd")))

(define-public crate-ospf-parser-0.2.0 (c (n "ospf-parser") (v "0.2.0") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "nom-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^2.0.3") (d #t) (k 0)))) (h "1nislcn4ir67j9hb7w2skf5gs69ga4x50pfzgnrnmp4354g3ldbi")))

(define-public crate-ospf-parser-0.3.0 (c (n "ospf-parser") (v "0.3.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom-derive") (r "^0.7") (d #t) (k 0)) (d (n "rusticata-macros") (r "^3.0") (d #t) (k 0)))) (h "0lx5hmzfcjawhm0an4vy9aj64db5a2871qk36m9qbqkys5qs67vz")))

(define-public crate-ospf-parser-0.4.0 (c (n "ospf-parser") (v "0.4.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "nom-derive") (r "^0.9") (d #t) (k 0)) (d (n "rusticata-macros") (r "^3.0") (d #t) (k 0)))) (h "0nda9jf2idx7kb9gc4ffj692x7d682pbhffmrya8q29yi4kf0h8j")))

(define-public crate-ospf-parser-0.5.0 (c (n "ospf-parser") (v "0.5.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom-derive") (r "^0.10") (d #t) (k 0)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)))) (h "1vxfs0igvs1aczfic41ac5m4fv1ips6njjachkaja0afl6fwfqih")))

