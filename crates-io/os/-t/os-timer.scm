(define-module (crates-io os -t os-timer) #:use-module (crates-io))

(define-public crate-os-timer-1.0.0 (c (n "os-timer") (v "1.0.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"wasm32\"))") (k 0)))) (h "1fax6p65qy8jbahcmdb4x24dsp3dbaaycbv626jhpxif2n4rh80w") (y #t)))

(define-public crate-os-timer-1.0.1 (c (n "os-timer") (v "1.0.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "07dy81z1lwxjw1mrfbprcaza0cnbdxssh5l4s6j1ky8m11r61zb8") (y #t)))

(define-public crate-os-timer-1.0.2 (c (n "os-timer") (v "1.0.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "0i8hjv4lw6b64vc31i1zxwa7z74a2hjj9iz5ym5jbac06w66dlq9") (y #t)))

(define-public crate-os-timer-1.0.3 (c (n "os-timer") (v "1.0.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "0gbzacb6kfyby2qg5g6n2947kpdbzwijkj49y3nw8x9mmjj80446") (y #t)))

(define-public crate-os-timer-1.0.4 (c (n "os-timer") (v "1.0.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "0k9gc2hkb6v77cphan1i2l34s636znd75cz97pxah89c5jjy1pcc") (y #t)))

(define-public crate-os-timer-1.0.5 (c (n "os-timer") (v "1.0.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "1ssipmkcvgdw0cifkmcdqgv3a3s2d7c30828vqs5qr6x87rwhi41")))

(define-public crate-os-timer-1.0.6 (c (n "os-timer") (v "1.0.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "1b637ygivcgfsd0cljiykcg9gjj0x8sc6irwwrp5bi4jqs442y2y")))

(define-public crate-os-timer-1.0.7 (c (n "os-timer") (v "1.0.7") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "0shv8bf468g8x34r0x7287i4dni3xadazd4yzas5kzpz96wd55y2")))

(define-public crate-os-timer-1.0.8 (c (n "os-timer") (v "1.0.8") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "1n9gqsq94a7ilwnhka0fypjis9r8zxcxsaypsdbvqzapwb978cky")))

(define-public crate-os-timer-1.0.9 (c (n "os-timer") (v "1.0.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "11zvngmnyzz7ccj5l7c1sni71g2076ff8vg6ndfdj2bjyzajkpsn") (y #t)))

(define-public crate-os-timer-1.0.10 (c (n "os-timer") (v "1.0.10") (d (list (d (n "cc") (r "^1") (d #t) (k 1) (p "cc")) (d (n "libc") (r "^0.2") (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)))) (h "1y6fxcxyxrmpi4ab9zr012vpjzfpvwysys1nzyb3mvdm0wd6cxnl")))

