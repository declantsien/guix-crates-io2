(define-module (crates-io os -t os-thread-local) #:use-module (crates-io))

(define-public crate-os-thread-local-0.1.0 (c (n "os-thread-local") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (t "cfg(not(windows))") (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fibersapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hvg11rbk0928axf07h59xgjkm0zimh3h6qh11y2x3x8n2yyi2by")))

(define-public crate-os-thread-local-0.1.1 (c (n "os-thread-local") (v "0.1.1") (d (list (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (t "cfg(not(windows))") (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fibersapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1i09gmd48l77pr3g6sdflw1zdh6ayxy3sg3akvi5rmysab6nmgcs")))

(define-public crate-os-thread-local-0.1.2 (c (n "os-thread-local") (v "0.1.2") (d (list (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (t "cfg(not(windows))") (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fibersapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13z2gjyah6h5wnyiz19x8j766252l3kz6hbnyvh9x72lb7m3fp57")))

(define-public crate-os-thread-local-0.1.3 (c (n "os-thread-local") (v "0.1.3") (d (list (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (t "cfg(not(windows))") (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fibersapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z1bmz8r3b1h4lwbnnkzds91207d0q0p63miz43qkp57kvxcfzyx")))

