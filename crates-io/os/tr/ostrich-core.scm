(define-module (crates-io os tr ostrich-core) #:use-module (crates-io))

(define-public crate-ostrich-core-0.1.0 (c (n "ostrich-core") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1r6y477asa29fh7n7c5fp6vnj21dpfg3h9djd0cqy8c7ncqlzlxx")))

