(define-module (crates-io os ha oshash) #:use-module (crates-io))

(define-public crate-oshash-0.1.0 (c (n "oshash") (v "0.1.0") (d (list (d (n "util") (r "^0.1.3") (d #t) (k 0)))) (h "1qxnwdswyay8f1gsq2i4hws0wch8yihv2qggzi50rz6gfnbl4lq0")))

(define-public crate-oshash-0.1.1 (c (n "oshash") (v "0.1.1") (d (list (d (n "util") (r "^0.1.3") (d #t) (k 0)))) (h "1p01ylpwbk41yxsiwhi7d0k0cxk4by3bq4rshvvncaiy9410jvp5")))

