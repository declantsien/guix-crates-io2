(define-module (crates-io os ex osexave) #:use-module (crates-io))

(define-public crate-OSEXave-0.0.1 (c (n "OSEXave") (v "0.0.1") (h "04g8yyd25wbs1v16wh8iyv8pny76ggaqmswpycfl4293917v9z6x") (f (quote (("default")))) (y #t)))

(define-public crate-OSEXave-0.0.2 (c (n "OSEXave") (v "0.0.2") (h "18dx1xgyirxnpyzw094d13x7sh45b7869a3qg3sqjhfr6yr5aycl") (f (quote (("default")))) (y #t)))

(define-public crate-OSEXave-0.0.3 (c (n "OSEXave") (v "0.0.3") (h "0j8bxw7xfj7rxk39girv4n38lj2xyrx3wajjrvg2m6m0cixp08ms") (f (quote (("default")))) (y #t)))

(define-public crate-OSEXave-0.0.4 (c (n "OSEXave") (v "0.0.4") (h "1x52ilnxrl8x4qgfhiyvdq9gqqr5acayh2fi2shyq2ln393a3j9r") (f (quote (("default")))) (y #t)))

(define-public crate-OSEXave-0.0.5 (c (n "OSEXave") (v "0.0.5") (h "0jxqa58f14anhy7am19r7bk5kwdvfriqjdmgkhip52d074kq9894") (f (quote (("default")))) (y #t)))

(define-public crate-OSEXave-0.0.6 (c (n "OSEXave") (v "0.0.6") (h "0fh7ld57h0d5x2rajnnd55w689n019jqsm6dwak3n79vni5dx3nh") (f (quote (("default")))) (y #t)))

(define-public crate-OSEXave-0.0.7 (c (n "OSEXave") (v "0.0.7") (h "0kw4xxrfw4468sgzzflhmfh02z2knkg64h5zkwxgh13jwzwg9l0w") (f (quote (("default"))))))

