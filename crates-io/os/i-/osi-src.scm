(define-module (crates-io os i- osi-src) #:use-module (crates-io))

(define-public crate-osi-src-0.2.0+0.108.7 (c (n "osi-src") (v "0.2.0+0.108.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)))) (h "1ss1341dsc8m1v2vplgzvpkmb01ifzirjlcabjssz0hksp9srjn3") (f (quote (("osixpr") ("osispx") ("osimsk") ("osigrb") ("osiglpk") ("osicpx") ("default")))) (l "Osi")))

(define-public crate-osi-src-0.2.2+0.108.7 (c (n "osi-src") (v "0.2.2+0.108.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)))) (h "0jnykmcpspwir61668qppg92zyymbpy5mj7gh2fp4zwmzl8jyz4z") (f (quote (("osixpr") ("osispx") ("osimsk") ("osigrb") ("osiglpk") ("osicpx") ("default")))) (l "Osi")))

(define-public crate-osi-src-0.2.4+0.108.8 (c (n "osi-src") (v "0.2.4+0.108.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)))) (h "084b3vqr9hwgj7phr13gf2j0sx07syd9hb5x61ihyfwm11kxj3kx") (f (quote (("osixpr") ("osispx") ("osimsk") ("osigrb") ("osiglpk") ("osicpx") ("default")))) (l "Osi")))

(define-public crate-osi-src-0.2.5+0.108.9 (c (n "osi-src") (v "0.2.5+0.108.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)))) (h "1v15hn32hvkzcr75wsjbp40ns7xi4iwb8sxdk2qlj66nqzji70b3") (f (quote (("osixpr") ("osispx") ("osimsk") ("osigrb") ("osiglpk") ("osicpx") ("default")))) (l "Osi")))

(define-public crate-osi-src-0.2.6+0.108.9 (c (n "osi-src") (v "0.2.6+0.108.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)))) (h "1c4za4qcq8n8qmrxjgz8qb01avxm9fgxh5c83www0dynhrnvg6aq") (f (quote (("osixpr") ("osispx") ("osimsk") ("osigrb") ("osiglpk") ("osicpx") ("default")))) (l "Osi")))

(define-public crate-osi-src-0.2.7+0.108.9 (c (n "osi-src") (v "0.2.7+0.108.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)))) (h "0lwcpjkb7j737nhx1649vynkyj7qvd0mkj8n9g3b6ifz13zixkbh") (f (quote (("osixpr") ("osispx") ("osimsk") ("osigrb") ("osiglpk") ("osicpx") ("default")))) (l "Osi")))

(define-public crate-osi-src-0.2.8+0.108.10 (c (n "osi-src") (v "0.2.8+0.108.10") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "coinutils-src") (r "^0.2") (d #t) (k 0)))) (h "0i1i76q3hk9nr1bhhrgw4gfydn90k65ysnbgggqd9y6ls5rf3wcx") (f (quote (("osixpr") ("osispx") ("osimsk") ("osigrb") ("osiglpk") ("osicpx") ("default")))) (l "Osi")))

