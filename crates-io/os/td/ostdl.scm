(define-module (crates-io os td ostdl) #:use-module (crates-io))

(define-public crate-ostdl-0.9.0 (c (n "ostdl") (v "0.9.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "libflate") (r "^0.1.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.9.0") (d #t) (k 0)))) (h "1mg5mh6ydspgq80q7vikzp2mx2vjp1khm6a39rvwdxnpww9wicmi")))

(define-public crate-ostdl-0.9.1 (c (n "ostdl") (v "0.9.1") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "libflate") (r "^0.1.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.9.0") (d #t) (k 0)))) (h "1q7nw94awdv8x39jjjcb71pvx5h2rgv8lddmjzkb30snbwv7sdcr")))

