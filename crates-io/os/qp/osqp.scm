(define-module (crates-io os qp osqp) #:use-module (crates-io))

(define-public crate-osqp-0.0.1-pre.1 (c (n "osqp") (v "0.0.1-pre.1") (d (list (d (n "osqp-sys") (r "^0.2.1-pre.1") (d #t) (k 0)) (d (n "static_assertions") (r "^0.2.3") (d #t) (k 0)))) (h "17d57kydpvv4fs6jdq57r523rs6dngakflzar961zcjjryhl3mzl")))

(define-public crate-osqp-0.0.1-pre.2 (c (n "osqp") (v "0.0.1-pre.2") (d (list (d (n "osqp-sys") (r "^0.2.1-pre.2") (d #t) (k 0)) (d (n "static_assertions") (r "^0.2.3") (d #t) (k 0)))) (h "0wc6xfk8zklpsa5nnl10zb9j5pkw0ll8ngf7fh4z9p4gnakkyw7k")))

(define-public crate-osqp-0.0.1-pre.3 (c (n "osqp") (v "0.0.1-pre.3") (d (list (d (n "osqp-sys") (r "^0.2.1-pre.3") (d #t) (k 0)) (d (n "static_assertions") (r "^0.2.3") (d #t) (k 0)))) (h "1hgajzb6cabf97xb374vnm7nhg383ici8cyr53x5qwzg9v80v221")))

(define-public crate-osqp-0.2.1-pre.4 (c (n "osqp") (v "0.2.1-pre.4") (d (list (d (n "osqp-sys") (r "^0.2.1-pre.3") (d #t) (k 0)))) (h "19fm0c6wcg0r1yzh89k28mwlqyvn4swqkcwsrg6rdpyzxb6b0mjy")))

(define-public crate-osqp-0.3.0-pre.1 (c (n "osqp") (v "0.3.0-pre.1") (d (list (d (n "osqp-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1n2ppqfa4qxaxj3p4szmn9d637r3izyfkwrifn8d3vd4dracfbb2")))

(define-public crate-osqp-0.3.0-pre.2 (c (n "osqp") (v "0.3.0-pre.2") (d (list (d (n "osqp-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1rg8hb41z0l25nln6n21jzdb4vp210d75wr1adgb0qz76mfnp2yi")))

(define-public crate-osqp-0.3.1 (c (n "osqp") (v "0.3.1") (d (list (d (n "osqp-sys") (r "^0.3.1") (d #t) (k 0)))) (h "13dj1vagmbvw2sn8z12yw17rxrsbxiprzi4iis6yqw8pjwvrg8ja")))

(define-public crate-osqp-0.4.0 (c (n "osqp") (v "0.4.0") (d (list (d (n "osqp-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0475bng8qgijvi7lcnsln0z80va4p3r7yh9n6jybs5wfr5cb5png")))

(define-public crate-osqp-0.4.1 (c (n "osqp") (v "0.4.1") (d (list (d (n "osqp-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1ychyz9g3lgxwzinw1n16kmz5w3wc612pgiwz0rxdlibzjfyfy8z")))

(define-public crate-osqp-0.5.0 (c (n "osqp") (v "0.5.0") (d (list (d (n "osqp-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0c4135slpzmwjn4scc9gs8jn86nan9qw8mbi58kxa435wa56h27b")))

(define-public crate-osqp-0.6.0 (c (n "osqp") (v "0.6.0") (d (list (d (n "osqp-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0swlyqd05790cpca8799l0yfj1772pvfhz6044i0hvvkar5v4775")))

(define-public crate-osqp-0.6.1 (c (n "osqp") (v "0.6.1") (d (list (d (n "osqp-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0v94vgwjggklzgf0zs1l35zynhmpx5n2zdrbkqsiwa4srgfchg1v")))

(define-public crate-osqp-0.6.2-pre (c (n "osqp") (v "0.6.2-pre") (d (list (d (n "osqp-sys") (r "^0.6.2-pre") (d #t) (k 0)))) (h "1ar0adzpgr50iqp3fzw8kizxknmmynzga7kh22fmjiw2xqq5wa4k")))

(define-public crate-osqp-0.6.2 (c (n "osqp") (v "0.6.2") (d (list (d (n "osqp-sys") (r "^0.6.2") (d #t) (k 0)))) (h "07i7yizg5m550js01qvyrqlj8nf19npq5kjpj78f280jf0j5c1ry")))

