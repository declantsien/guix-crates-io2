(define-module (crates-io os qp osqp-sys) #:use-module (crates-io))

(define-public crate-osqp-sys-0.2.1-pre.1 (c (n "osqp-sys") (v "0.2.1-pre.1") (d (list (d (n "cmake") (r "^0.1.28") (d #t) (k 1)))) (h "1giz8n73jzdaim38zc38r6b1lgpgksjwxfsa57vln6bxli5cfd2c")))

(define-public crate-osqp-sys-0.2.1-pre.2 (c (n "osqp-sys") (v "0.2.1-pre.2") (d (list (d (n "cmake") (r "^0.1.28") (d #t) (k 1)))) (h "09qj248l0s8g535qxszrw8cvp8barhfj1ckc5fv8p0hb1v9b6n44")))

(define-public crate-osqp-sys-0.2.1-pre.3 (c (n "osqp-sys") (v "0.2.1-pre.3") (d (list (d (n "cmake") (r "^0.1.28") (d #t) (k 1)))) (h "13iqnjxya4i0cv1xmcnxasp3a8pwhj1f86gv1384x59yvczkawd3")))

(define-public crate-osqp-sys-0.3.0 (c (n "osqp-sys") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.28") (d #t) (k 1)))) (h "1vxnwxrdmy1zhbb9v5vkmrnhfj6wghf701rbbff00mspgd0h05gz")))

(define-public crate-osqp-sys-0.3.1 (c (n "osqp-sys") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.28") (d #t) (k 1)))) (h "1g4z5yksx0r185mj2s5iak5y2k6r8j9l6dnc8xc02547zszyk9hv") (l "osqp")))

(define-public crate-osqp-sys-0.4.0 (c (n "osqp-sys") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1.28") (d #t) (k 1)))) (h "0xr7yi1avw68xqyia7viamcifd6in9izjg4p0zjqqab1wb3f3cad") (l "osqp")))

(define-public crate-osqp-sys-0.4.1 (c (n "osqp-sys") (v "0.4.1") (d (list (d (n "cmake") (r "^0.1.28") (d #t) (k 1)))) (h "0jfc8r8vq6zc3fshaarl9950xmxb66hcx61iz6i22396z1c7aqnj") (l "osqp")))

(define-public crate-osqp-sys-0.5.0 (c (n "osqp-sys") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1.28") (d #t) (k 1)))) (h "07k3vn61haw25gv3993p2c1p51ci3yrxgrfvfpa16imbnl1sf4z6") (l "osqp")))

(define-public crate-osqp-sys-0.6.0 (c (n "osqp-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.36") (d #t) (k 1)) (d (n "cmake") (r "^0.1.28") (d #t) (k 1)))) (h "1ysmqjqg46fbhm6b626wsm6157wkzm5xdrs2nvn9xc476723m33v") (l "osqp")))

(define-public crate-osqp-sys-0.6.2-pre (c (n "osqp-sys") (v "0.6.2-pre") (d (list (d (n "cc") (r "^1.0.36") (d #t) (k 1)) (d (n "cmake") (r "^0.1.28") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0q39xv3rpx2m1ap7wvn096kbllr0krkhm34sabsmmshh8lmwgn4w") (l "osqp")))

(define-public crate-osqp-sys-0.6.2 (c (n "osqp-sys") (v "0.6.2") (d (list (d (n "cc") (r "^1.0.36") (d #t) (k 1)) (d (n "cmake") (r "^0.1.28") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "1akzs6g087crq8bglk77ql7xn4177cn6dagrzgs4c2x7vf1slrvp") (l "osqp")))

