(define-module (crates-io os qp osqp-rust) #:use-module (crates-io))

(define-public crate-osqp-rust-0.6.2 (c (n "osqp-rust") (v "0.6.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "osqp-rust-sys") (r "^0.6.2") (d #t) (k 0)))) (h "0nnn12csb10hjvxfy5y1sg6icfhfp1mzv68ar65nrgnwxdgbbjmw")))

