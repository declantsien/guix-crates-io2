(define-module (crates-io os -r os-rs) #:use-module (crates-io))

(define-public crate-os-rs-0.1.0 (c (n "os-rs") (v "0.1.0") (h "19nz775j5czazrp5s91lprjqfhd0sqmymxq922qwmgv2n2z4c5xj") (y #t)))

(define-public crate-os-rs-0.1.1 (c (n "os-rs") (v "0.1.1") (h "12h58ida0bhcfg20zh6r1r09h7py074aaa3kd8nmnfycr6zsbhx6") (y #t)))

(define-public crate-os-rs-0.1.2 (c (n "os-rs") (v "0.1.2") (h "09g1i4jj29d1nyms02rjkkhz2z8sl2dkfmy60a87bx179c795b7d") (y #t)))

(define-public crate-os-rs-0.1.3 (c (n "os-rs") (v "0.1.3") (h "02z5hlrqw0f5kq2vqia6yj98sq7z2p4gbxq2zx0i2ywnp5mbiawn") (y #t)))

