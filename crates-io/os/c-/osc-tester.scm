(define-module (crates-io os c- osc-tester) #:use-module (crates-io))

(define-public crate-osc-tester-0.1.0 (c (n "osc-tester") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "01ik0z9l7iwcr0609l371230xp1m9ryphspxvpm69wqrp838cfp5")))

(define-public crate-osc-tester-0.2.0 (c (n "osc-tester") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "0nznniqn6s6zhlm9s2y1f68vmfxxy5v836gnfzhpqdaf1ldrnpxj")))

(define-public crate-osc-tester-0.2.1 (c (n "osc-tester") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "15aang0s4wm1az64qkaa0l0vk7ac8dyc5hckvqxi823my7lcx74f")))

(define-public crate-osc-tester-0.2.2 (c (n "osc-tester") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "0rk7mp284p02hib055vxkbq8s15j3z4jsgiggghrbncmafhdsa27")))

(define-public crate-osc-tester-0.2.3 (c (n "osc-tester") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "0yvm80c9f7ixcbgwasmn1p49pykjybk3kxcqqn26zmhj81qpm6gk")))

(define-public crate-osc-tester-0.2.4 (c (n "osc-tester") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "0g78pk07s7s9xrrqwkwcwbg9pnabw97vd3gkrmhzxf3zdvyvkphi")))

