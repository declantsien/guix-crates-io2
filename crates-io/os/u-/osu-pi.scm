(define-module (crates-io os u- osu-pi) #:use-module (crates-io))

(define-public crate-osu-pi-0.1.0 (c (n "osu-pi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "094rbjcqw3lw4rw06v25s56y5bjsmr2792k9b9855pwv5vyhhwdh")))

