(define-module (crates-io os u- osu-replay-parser) #:use-module (crates-io))

(define-public crate-osu-replay-parser-0.1.0 (c (n "osu-replay-parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (d #t) (k 0)))) (h "1wsgjr359485ic6ppb0s3pai28mi2wq9z908467vvjkqyb2ksrdv")))

(define-public crate-osu-replay-parser-0.2.0 (c (n "osu-replay-parser") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (d #t) (k 0)))) (h "19v7fpd6j2rgwkc5781fnx6zpzj80vil8c873zpk00yn89ig3v1p")))

