(define-module (crates-io os u- osu-db) #:use-module (crates-io))

(define-public crate-osu-db-0.1.0 (c (n "osu-db") (v "0.1.0") (d (list (d (n "bit") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "xz2") (r "^0.1") (o #t) (d #t) (k 0)))) (h "08h7g9frhjnkd7d46m2ibs3pqpfg8a8jw5qbz3vj3qb7fvmkib3n") (f (quote (("ser-de" "serde" "serde_derive" "chrono/serde") ("default" "compression") ("compression" "xz2"))))))

(define-public crate-osu-db-0.2.0 (c (n "osu-db") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "xz2") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0g496jykn6hi4wi945r25r5q46sf7wlqqfc3inhvaymcg1vll0n9") (f (quote (("ser-de" "serde" "serde_derive" "chrono/serde") ("default" "compression") ("compression" "xz2"))))))

(define-public crate-osu-db-0.3.0 (c (n "osu-db") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "xz2") (r "^0.1") (o #t) (d #t) (k 0)))) (h "00vgickpz9cyrj5zfpiy1di1fnsgi4cyyvbpwvkd9wlam2i1smc1") (f (quote (("ser-de" "serde" "serde_derive" "chrono/serde") ("default" "compression") ("compression" "xz2"))))))

