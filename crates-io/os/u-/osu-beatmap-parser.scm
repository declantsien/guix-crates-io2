(define-module (crates-io os u- osu-beatmap-parser) #:use-module (crates-io))

(define-public crate-osu-beatmap-parser-0.14.0 (c (n "osu-beatmap-parser") (v "0.14.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "19p79mk309jfxb3bx7scw6s5f910nby0a3qzzlhcizv1m7kgnppv")))

