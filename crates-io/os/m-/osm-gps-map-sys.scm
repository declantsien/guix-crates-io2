(define-module (crates-io os m- osm-gps-map-sys) #:use-module (crates-io))

(define-public crate-osm-gps-map-sys-0.2.5 (c (n "osm-gps-map-sys") (v "0.2.5") (d (list (d (n "cairo-sys-rs") (r "^0.9.0") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "gdk-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0xsxsl7mhdsbd1c1fxar1dj59whh1qk9mpq2mx8kvdwnka4c05zf") (f (quote (("dox")))) (l "osm_gps_map")))

