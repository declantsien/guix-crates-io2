(define-module (crates-io os m- osm-geo-mapper-macros) #:use-module (crates-io))

(define-public crate-osm-geo-mapper-macros-0.1.0 (c (n "osm-geo-mapper-macros") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1l2zh4hlab9gbcmcp94lxpj2bhr2rh44fyzjlvwjxyz18h481gaa")))

(define-public crate-osm-geo-mapper-macros-0.2.0 (c (n "osm-geo-mapper-macros") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0g42wjvai4hw3laqyd5y2nr17ky1q87932a48rpb86mckxp8vf05")))

(define-public crate-osm-geo-mapper-macros-0.2.1 (c (n "osm-geo-mapper-macros") (v "0.2.1") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0w5wc45xm7nqly4pkrhbnprxg4kjlv6c48q7878dsp255nfxr1n2")))

(define-public crate-osm-geo-mapper-macros-0.3.0 (c (n "osm-geo-mapper-macros") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1d1nriid07xrc7sprvj5ds04i4g1ikpjirhk2kksmpdn7fjxkfs1")))

