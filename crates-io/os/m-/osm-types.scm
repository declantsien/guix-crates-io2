(define-module (crates-io os m- osm-types) #:use-module (crates-io))

(define-public crate-osm-types-0.1.0 (c (n "osm-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "16f4l5vn4c8v6865z4hnzl0s06yyrvmf32c9w0d7i5m0ffkgznc8") (y #t)))

(define-public crate-osm-types-0.1.1 (c (n "osm-types") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1ac30n9wbs3jnsi3k6bkirzv6c4lrgwn3qgvv2rj5ixkvppcgvsd")))

(define-public crate-osm-types-0.1.2 (c (n "osm-types") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0vi3zkg80psjvpfmk971rr212n2j69y62479bbkxvw3g2kp7rwza")))

(define-public crate-osm-types-0.1.3 (c (n "osm-types") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "0yj017hhmdmmnfhibcqbxc292dwb2ngvz1yn41ik0i3rr4r59x7l")))

(define-public crate-osm-types-0.1.4 (c (n "osm-types") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "16ay2hfbd3lhbf50nz0l9zvlp0jxpk9lri3c3x5hpimpprwsyg7y")))

(define-public crate-osm-types-0.1.5 (c (n "osm-types") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rhvvv3952vab49bnhxpm50q8ax079jzfhpxkdq1zncgc67rcfxf") (s 2) (e (quote (("serde" "dep:serde" "kstring/serde" "chrono/serde"))))))

