(define-module (crates-io os m- osm-xml) #:use-module (crates-io))

(define-public crate-osm-xml-0.4.0 (c (n "osm-xml") (v "0.4.0") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0vgb4hws42ibyhd567r5hpgl09ca3wn3qdz1pj6p0d44ff4g5n9l")))

(define-public crate-osm-xml-0.5.0 (c (n "osm-xml") (v "0.5.0") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1sabwdxzzjazwzvggysg72j6986nyaidphlbjhpbj52nm9ankdf6")))

(define-public crate-osm-xml-0.5.1 (c (n "osm-xml") (v "0.5.1") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "01mpj31prn2v4a7jpj8zkh2gfw7ryn7nh000yaqwalldjaf01snf")))

(define-public crate-osm-xml-0.6.1 (c (n "osm-xml") (v "0.6.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1vfx5zv4rh4kmhyg0kpxsrzr092dfpnh06a58g67fgs4avx7mjdn")))

(define-public crate-osm-xml-0.6.2 (c (n "osm-xml") (v "0.6.2") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1n5fa558x6m912ryi8dj7y3qbm9akr78f5gi3rh4rxz1n2rnplf2")))

