(define-module (crates-io os m- osm-to-geojson) #:use-module (crates-io))

(define-public crate-osm-to-geojson-0.1.0 (c (n "osm-to-geojson") (v "0.1.0") (d (list (d (n "geojson") (r "^0.13.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0f9l9zxqd2yifwcsdj8f6q16q39yz8jq0x3i18bqa4wi3xvplwan")))

