(define-module (crates-io os m- osm-primitives) #:use-module (crates-io))

(define-public crate-osm-primitives-0.1.0 (c (n "osm-primitives") (v "0.1.0") (h "0q7hsgcj2lgsgy9q3fq9b9sqcwcpyjxd6rzxwk5m9n0kkw1a0sra")))

(define-public crate-osm-primitives-0.2.0 (c (n "osm-primitives") (v "0.2.0") (h "1hjmcx2sixzwdk52j60pp82wgk4dwvs5p15zj9zgxbf4bggxs37h")))

(define-public crate-osm-primitives-1.0.0 (c (n "osm-primitives") (v "1.0.0") (h "0x1c02jqpp3391hkxj04bw85dxx0aji791hmsldf3l7mcgzn0kj1")))

(define-public crate-osm-primitives-2.0.0 (c (n "osm-primitives") (v "2.0.0") (h "0gicgnkhp7vwy98108fmv9c9mg60ajwkrz4wyprl5fa9mx02hc93")))

(define-public crate-osm-primitives-2.1.0 (c (n "osm-primitives") (v "2.1.0") (h "13a8yrrl7775687ya5l8vc3im57061h5pw8fx50dhzgla783218m")))

