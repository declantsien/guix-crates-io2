(define-module (crates-io os m- osm-rs) #:use-module (crates-io))

(define-public crate-osm-rs-0.1.0 (c (n "osm-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^4.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "17c90c68h7nsg477kax7c3iwwk3ymfmdqri93jviy5wv63w3lpxk")))

