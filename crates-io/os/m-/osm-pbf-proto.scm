(define-module (crates-io os m- osm-pbf-proto) #:use-module (crates-io))

(define-public crate-osm-pbf-proto-0.1.0-alpha.1 (c (n "osm-pbf-proto") (v "0.1.0-alpha.1") (d (list (d (n "protobuf") (r "^3.0.0-alpha.8") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.0.0-alpha.8") (d #t) (k 1)))) (h "0csymgmlxi5n06kis21swabaayk9a9ij2j96vgfambdr2z59zbvi")))

(define-public crate-osm-pbf-proto-0.1.0-alpha.2 (c (n "osm-pbf-proto") (v "0.1.0-alpha.2") (d (list (d (n "protobuf") (r "^3.0.0-alpha.8") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.0.0-alpha.8") (d #t) (k 1)))) (h "1m377p3xpax6vh07wy5v97d5my75idi2mg4v5r98s7ndg7ky2xpv")))

(define-public crate-osm-pbf-proto-0.1.0-alpha.3 (c (n "osm-pbf-proto") (v "0.1.0-alpha.3") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2.0") (d #t) (k 1)))) (h "0q84lk97xsan5cz33sqpy0py2f2dq1zpvsv1l2s029wzzfiaizfg")))

(define-public crate-osm-pbf-proto-0.1.0-alpha.4 (c (n "osm-pbf-proto") (v "0.1.0-alpha.4") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "protobuf") (r "^3.3.0") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.3.0") (d #t) (k 1)))) (h "1yq0sjx0qkv0cmsackxzg4ak8g1kj7pw5xp7mgj0yqk0afs06q87")))

(define-public crate-osm-pbf-proto-0.1.0 (c (n "osm-pbf-proto") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "protobuf") (r "^3.3.0") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.3.0") (d #t) (k 1)))) (h "16l4nhdavmwl4g9rgw78qz680wh13d15ywqfrp89jxdml8m7n6zi")))

(define-public crate-osm-pbf-proto-0.1.1 (c (n "osm-pbf-proto") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "protobuf") (r "^3.3.0") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.3.0") (d #t) (k 1)))) (h "17w3a9vx6mlma75ix0zw4822kr4xvhgaq8fwcv9i3ds99hpwn38k")))

