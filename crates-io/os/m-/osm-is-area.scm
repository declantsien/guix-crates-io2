(define-module (crates-io os m- osm-is-area) #:use-module (crates-io))

(define-public crate-osm-is-area-0.1.0 (c (n "osm-is-area") (v "0.1.0") (h "1zd27jwh3czrq28vs87y95z6cr92q916kyh5j95ai2msr29px6v3")))

(define-public crate-osm-is-area-0.1.1 (c (n "osm-is-area") (v "0.1.1") (h "1m7vgjmflm47q3fsxa8myh9n29ha7fgvbrc6z14qwhhzjz5spxsp")))

(define-public crate-osm-is-area-0.1.2 (c (n "osm-is-area") (v "0.1.2") (h "1rhz43fz5vn1ppy0j0lswh4yd7qh1ai1v99ac4k0h36gxariqgv0")))

(define-public crate-osm-is-area-0.1.3 (c (n "osm-is-area") (v "0.1.3") (h "1i96fykn3ndm7z23cm6cxcmbcjchxxpprj5xv5lg95xnjwna3qfv")))

(define-public crate-osm-is-area-0.1.4 (c (n "osm-is-area") (v "0.1.4") (h "01j2mg26iv45b0l9j61khdmap00q1b2bxbdiw5yy00znsijb4hys")))

(define-public crate-osm-is-area-1.0.0 (c (n "osm-is-area") (v "1.0.0") (h "1rxfw1pcz6kn5nm6jnhhsw9rdjdwfhw4914cl1c8anm307820fn8")))

