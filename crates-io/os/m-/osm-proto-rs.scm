(define-module (crates-io os m- osm-proto-rs) #:use-module (crates-io))

(define-public crate-osm-proto-rs-0.1.0 (c (n "osm-proto-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "prost") (r "^0.1.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.1.0") (d #t) (k 1)) (d (n "prost-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0qbv7yvlgi4khk5daxfl43vp5sdj2jn1q5w3xrb4fjc4rhl98q8w")))

(define-public crate-osm-proto-rs-0.2.0 (c (n "osm-proto-rs") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "prost") (r "^0.1.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.1.0") (d #t) (k 1)) (d (n "prost-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0khqfk8ixlkybc34is7vl026ih2x67jpa9wwn8r7n9n8c4v35q1d")))

