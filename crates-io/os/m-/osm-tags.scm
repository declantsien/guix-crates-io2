(define-module (crates-io os m- osm-tags) #:use-module (crates-io))

(define-public crate-osm-tags-0.1.0 (c (n "osm-tags") (v "0.1.0") (d (list (d (n "kstring") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1qf10dkaw4h7r3qk9z5s0kygmc4l0p3vwaybrskv611a0hzvp721") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-osm-tags-0.1.1 (c (n "osm-tags") (v "0.1.1") (d (list (d (n "kstring") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0shx1nngfqx3ny8j8ly576s1hg1w8v1an3gibc64n7mh3b8423lf") (s 2) (e (quote (("serde" "dep:serde"))))))

