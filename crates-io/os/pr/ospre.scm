(define-module (crates-io os pr ospre) #:use-module (crates-io))

(define-public crate-ospre-0.1.0 (c (n "ospre") (v "0.1.0") (h "1h5zk3x5lpaqa3kwrhqvd3z8qax3zz2gc0006h20bd1xdqv5d1jp")))

(define-public crate-ospre-0.1.1 (c (n "ospre") (v "0.1.1") (h "1dwldpa277fl7a9iiss1czgc4g3vxzjl3jl1wb71miwga9prcb5y")))

(define-public crate-ospre-0.1.2 (c (n "ospre") (v "0.1.2") (h "005vkh4i9krnd6l509cjjk9pv1xwnvvs3v4kxrd593b6wm7r3hxa")))

(define-public crate-ospre-0.1.4 (c (n "ospre") (v "0.1.4") (h "0w1gfwcvfz6pz2jkmr9xhvapp8xwnaq4z4999hrxpk3zn6ki3vb9")))

(define-public crate-ospre-0.1.5 (c (n "ospre") (v "0.1.5") (h "1fnkd0dp3vkjsjp4lbcs4pppbazyd2j17c0h7h6nnmbh9k6s9646")))

(define-public crate-ospre-0.1.6 (c (n "ospre") (v "0.1.6") (h "1yk0gdni8kcm3nxp8m7qdh0hd5jn2z2mdi7g7bij447bhvwpqw7m")))

