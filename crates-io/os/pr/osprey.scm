(define-module (crates-io os pr osprey) #:use-module (crates-io))

(define-public crate-osprey-0.2.1 (c (n "osprey") (v "0.2.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "falcon") (r "^0.2.1") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "gluon") (r "^0.6.2") (d #t) (k 0)) (d (n "gluon_vm") (r "^0.6.2") (d #t) (k 0)))) (h "1w2zdv9wjmkgcrwndavrvfkdhkz3fj151n6b4c4gr5m7b5r3kn00")))

(define-public crate-osprey-0.2.4 (c (n "osprey") (v "0.2.4") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "falcon") (r "^0.2.4") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "gluon") (r "^0.7.0") (d #t) (k 0)) (d (n "gluon_vm") (r "^0.7.0") (d #t) (k 0)))) (h "0a2dxirhkhkw0dgkgam3s384q95l14l154cxmmk9044lig8b1bch")))

(define-public crate-osprey-0.4.0 (c (n "osprey") (v "0.4.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "falcon") (r "^0.4.0") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "gluon") (r "^0.8") (d #t) (k 0)) (d (n "gluon_vm") (r "^0.8") (d #t) (k 0)))) (h "0kv8j6ad5y7m46wyqv94kcwq4likqqnwb5i6kyvmnwc15h86b3rj")))

(define-public crate-osprey-0.4.4 (c (n "osprey") (v "0.4.4") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "falcon") (r "^0.4.4") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "gluon") (r "^0.9.4") (d #t) (k 0)) (d (n "gluon_vm") (r "^0.9.4") (d #t) (k 0)))) (h "1bw36r50j069ha9pv7sj1z6cwc0yw4wpz4amxj9mxj8s87qyxz67")))

