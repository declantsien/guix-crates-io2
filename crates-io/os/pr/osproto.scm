(define-module (crates-io os pr osproto) #:use-module (crates-io))

(define-public crate-osproto-0.0.1 (c (n "osproto") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^1.0") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "0gd7kmznvj5aiba8xnzmqz36mql38p01xpdnalh2zia6jkna1a9g")))

(define-public crate-osproto-0.0.2 (c (n "osproto") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^1.0") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "0irf7441rv4405r4dd2420qc36j45pg9jr8hhaz6jxb35ca4iss5")))

(define-public crate-osproto-0.1.0 (c (n "osproto") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^1.0") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "1vqffklz0bh88cjm92x4bk1ginpvfpr74ndw6cf6f834v6c46v6g")))

(define-public crate-osproto-0.1.1 (c (n "osproto") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "0qmnavk8772hn5px7a6k8sv2x7ciyaw9wp8mz3r0jsg6x3njgjsx")))

(define-public crate-osproto-0.1.2 (c (n "osproto") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "1w65i52h6g5r4hlbsms6alg60wlpia8vy2p7738798437nxj96wf")))

(define-public crate-osproto-0.2.0 (c (n "osproto") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ggx53phgbdv80jgs5zc7d4lvms36r3gcw6mrk6h5kivlnpff3gx")))

(define-public crate-osproto-0.2.1 (c (n "osproto") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "13g4snil37b0r1fdm34145y6d9pqdcfb4i2rqlb76xiydm83inls")))

(define-public crate-osproto-0.2.2 (c (n "osproto") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1bkvckyrwbax9316mnbsr6lssjm90s58jbkrfi3rmg1ggnrzinn8")))

