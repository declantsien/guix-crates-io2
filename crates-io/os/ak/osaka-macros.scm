(define-module (crates-io os ak osaka-macros) #:use-module (crates-io))

(define-public crate-osaka-macros-0.1.1 (c (n "osaka-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "111hbycgz9rffjzkq2kz1nfaljsy90hbh09k4vplb5bw020n79bq")))

(define-public crate-osaka-macros-0.1.3 (c (n "osaka-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pqnbwayvn1myskngl183j18v6cvnmyf6vcq4risgc6rg5n85138")))

(define-public crate-osaka-macros-0.2.0 (c (n "osaka-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14qd5hbfg97xx2lxhjc7sxr4pcxlyzyl0vy8mhxdgpqbyz0m17zf")))

(define-public crate-osaka-macros-0.2.2 (c (n "osaka-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gi24cyzlcvz3gjysh1pg4q29cw9x3kmi133161y8q011jws7lni")))

(define-public crate-osaka-macros-0.2.3 (c (n "osaka-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l9cwishh2bkn1qbgvzvcz2llsd40h1s7mgyj470ybgwc6sb5qhz")))

(define-public crate-osaka-macros-0.2.8 (c (n "osaka-macros") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d7brahmkcs445bbkw2qig6x7ly36p7kfs4mjq451qq2n5cp7ahl")))

(define-public crate-osaka-macros-0.2.10 (c (n "osaka-macros") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bq6n24cccjjcllnih2hsmbvk8aa19q6r30c786s2yyqnqc7q2gk")))

