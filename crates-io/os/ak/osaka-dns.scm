(define-module (crates-io os ak osaka-dns) #:use-module (crates-io))

(define-public crate-osaka-dns-0.1.1 (c (n "osaka-dns") (v "0.1.1") (d (list (d (n "osaka") (r "^0.1.1") (d #t) (k 0)))) (h "1rdkmmi4mr40qjlb4dwa44z76jhsaxwwvz8qz4nw65g1a9s18rk3")))

(define-public crate-osaka-dns-0.1.2 (c (n "osaka-dns") (v "0.1.2") (d (list (d (n "osaka") (r "^0.1.1") (d #t) (k 0)))) (h "1h2rl6jkdzpy0vy6zncx6g3d2mk4g6909swgiagc9alpbryshccs")))

(define-public crate-osaka-dns-0.2.0 (c (n "osaka-dns") (v "0.2.0") (d (list (d (n "osaka") (r "^0.2") (d #t) (k 0)) (d (n "tinylogger") (r "^0.1") (d #t) (k 2)))) (h "1bf73rhgyvv47rc7in72r20ivig7akgjix8lylwnf3sis0qz5l92")))

(define-public crate-osaka-dns-0.2.5 (c (n "osaka-dns") (v "0.2.5") (d (list (d (n "osaka") (r "^0.2.5") (d #t) (k 0)) (d (n "tinylogger") (r "^0.1") (d #t) (k 2)))) (h "1ix4rsj5sfl092sp36nyk7a241n67lz8lsns6xshsiqahs0jvj5a")))

(define-public crate-osaka-dns-0.2.10 (c (n "osaka-dns") (v "0.2.10") (d (list (d (n "osaka") (r "^0.2.10") (d #t) (k 0)) (d (n "tinylogger") (r "^0.1") (d #t) (k 2)))) (h "1pkgvrp0vvm6kf90jggn4dz34i5f2qjr6zwczq3kbzsd5hgg7gm3")))

(define-public crate-osaka-dns-0.2.11 (c (n "osaka-dns") (v "0.2.11") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "osaka") (r "^0.2.10") (d #t) (k 0)) (d (n "tinylogger") (r "^0.1") (d #t) (k 2)))) (h "1cbd0wjr7bvak97aq7qf86sxkwcmwi2xf95v1v1g9dk5sl2zgr32")))

(define-public crate-osaka-dns-0.3.0 (c (n "osaka-dns") (v "0.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "osaka") (r "^0.3.0") (d #t) (k 0)) (d (n "tinylogger") (r "^0.1") (d #t) (k 2)))) (h "18adx71c37scbpsridg2a52kjfxgska8nqpmsl0071dx89iimy93")))

