(define-module (crates-io os ak osaka) #:use-module (crates-io))

(define-public crate-osaka-0.1.0 (c (n "osaka") (v "0.1.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "054qyy61virif2i3dfskbbn89yaa9al2icc8cwlx2dqq474i52gg")))

(define-public crate-osaka-0.1.1 (c (n "osaka") (v "0.1.1") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1abpvz0g223wgyghyxm4n1q3lk4fjf6ww1kaa46wcbb8yxyzf78w")))

(define-public crate-osaka-0.1.3 (c (n "osaka") (v "0.1.3") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.1.3") (d #t) (k 0)))) (h "15h7lp0ddqn3nksyjavqlymikza2l1ix7lyiiv0xwbnh658545zd")))

(define-public crate-osaka-0.2.0 (c (n "osaka") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.2") (d #t) (k 0)))) (h "1yaazykzfq8ds5243xjpr9q0ic2y647jawg7q4fpdxs2w1zipjmi")))

(define-public crate-osaka-0.2.1 (c (n "osaka") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.2") (d #t) (k 0)))) (h "02aqr14v9fwrmqppzb8aa9xpi29xdc93a1jx5f6cw37ivpg5m4wx")))

(define-public crate-osaka-0.2.2 (c (n "osaka") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.2") (d #t) (k 0)))) (h "1fq1y2173wn92sim0yyiy8z2nhwpkgvyngylz2yzyjsnqd3nihy5")))

(define-public crate-osaka-0.2.3 (c (n "osaka") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.2") (d #t) (k 0)))) (h "112d98apy4m0bvwv2b3d7ilqqzfv3jvh6cqvpd9c7x889rak18yl")))

(define-public crate-osaka-0.2.4 (c (n "osaka") (v "0.2.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.2") (d #t) (k 0)))) (h "16sk11sb5dg9qx1657bybjch1ihn0n23k97n1hak89mhd924fgmf")))

(define-public crate-osaka-0.2.5 (c (n "osaka") (v "0.2.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.2") (d #t) (k 0)))) (h "1v006hf3cfaq0qgh8y8wmldyllrmk75xz603m9j7ylc6sk1bamvm")))

(define-public crate-osaka-0.2.6 (c (n "osaka") (v "0.2.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.2") (d #t) (k 0)))) (h "0fi5pcq4y2ljqfxcyzff1yfp2gqqsb7zar8mfalap21r8qwl7izl") (y #t)))

(define-public crate-osaka-0.2.7 (c (n "osaka") (v "0.2.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.2") (d #t) (k 0)))) (h "1r21q6g4vlp7dk5civinsgxcnhl46jjq4acxyyhmzmzm29j24mmk")))

(define-public crate-osaka-0.2.8 (c (n "osaka") (v "0.2.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "^0.2.8") (d #t) (k 0)))) (h "1hnsrm6j88aq2d8iqs70jzjs2mfrn70l68d5cjjxf80nm1y893rv")))

(define-public crate-osaka-0.2.10 (c (n "osaka") (v "0.2.10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "= 0.2.10") (d #t) (k 0)))) (h "1ppwqlm7nsqrj8aa0bxvfh72949jqhbbmisbyh735n5jsqlsi6vg")))

(define-public crate-osaka-0.3.0 (c (n "osaka") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "osaka-macros") (r "= 0.2.10") (d #t) (k 0)))) (h "1ic5833x2w111sz963w3g07dvy6jgw362ah3qmzd0in2my9nyv4v")))

