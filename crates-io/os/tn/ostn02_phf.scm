(define-module (crates-io os tn ostn02_phf) #:use-module (crates-io))

(define-public crate-ostn02_phf-0.1.0 (c (n "ostn02_phf") (v "0.1.0") (d (list (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 1)))) (h "0blpwcanimvlsvn3izv2rlyvasdcjc0lcykxb7sm94dzfzcqwxx2")))

(define-public crate-ostn02_phf-0.1.1 (c (n "ostn02_phf") (v "0.1.1") (d (list (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 1)))) (h "09ldpdr9s99nnayb7whjzz7vqlm6kx2q7n9zbr71yshqq9jjz539")))

(define-public crate-ostn02_phf-0.1.2 (c (n "ostn02_phf") (v "0.1.2") (d (list (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 1)))) (h "1189s1w2vv55ldzrcxxx6ww4zi7fbcr2j7fcfcfr7rz2ywdwfc5a")))

(define-public crate-ostn02_phf-0.1.3 (c (n "ostn02_phf") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "0md3g4gqmbvnawnx6663y4alld9f6yns2myw67vv0p6pw9vgwa14")))

(define-public crate-ostn02_phf-0.1.4 (c (n "ostn02_phf") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "1p13d82amcr6sns5dmxhlb96cb8zaknc6n3ncpwvy5xr3z4y4hwi")))

(define-public crate-ostn02_phf-0.1.5 (c (n "ostn02_phf") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "1mfb8m30px7j1mbrnyxpqdfv16hj13y11ipb0qj21y61v88gzm6i")))

(define-public crate-ostn02_phf-0.1.6 (c (n "ostn02_phf") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "10vgnrmcg2ygrckp3v8kxzrapxkyv02vx91wqrbmvakpwd3w1gh4")))

(define-public crate-ostn02_phf-0.1.7 (c (n "ostn02_phf") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "1jd7fng02k63kp1ddq822a7lklczg72r4mb11rf4zbfqdaisqblx")))

(define-public crate-ostn02_phf-0.1.8 (c (n "ostn02_phf") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "0mgkz1j642r7qinmxjl1iyg3zgavg50s7wds3skcs1lxvj3xixhx")))

(define-public crate-ostn02_phf-0.1.9 (c (n "ostn02_phf") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.13") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.13") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "1scwxcr11hry93q25y6cmp2xv7k48lplfsdagdqcn3622wwsx3cx")))

(define-public crate-ostn02_phf-0.1.10 (c (n "ostn02_phf") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.13") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.13") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "0r13bnlnpavmpmgnidmldayg2kpz4p1jd9x57mxc7g1ry4smfqpz")))

(define-public crate-ostn02_phf-0.1.11 (c (n "ostn02_phf") (v "0.1.11") (d (list (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "phf") (r "^0.7.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.14") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "0m6z3jj315wcvhz4h9524kq4mppyv57yd71qwq1fc5na9y6d7qs4")))

(define-public crate-ostn02_phf-0.1.12 (c (n "ostn02_phf") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "phf") (r "^0.7.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.14") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "1sm0qz8qgr03b4l2rk4jgsmsjbj6zyhqk5rhn4cwvwc7q9w14i7s")))

(define-public crate-ostn02_phf-0.1.13 (c (n "ostn02_phf") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "phf") (r "^0.7.16") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.16") (d #t) (k 1)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 1)))) (h "00sg1c0d4h9mp2dhksxcnsvd2sq0gair2l6ngrc7cz7blpsgn3qf")))

