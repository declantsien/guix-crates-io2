(define-module (crates-io os _i os_info_cli) #:use-module (crates-io))

(define-public crate-os_info_cli-1.0.0 (c (n "os_info_cli") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "os_info") (r "^2.0.3") (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1hijms9d0drv5mglcg6xad673jb8439kf20p9kqqhz3s5ijaxyn1")))

(define-public crate-os_info_cli-1.0.1 (c (n "os_info_cli") (v "1.0.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "os_info") (r "^2.0.4") (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1jpwgql0wznvqyzhkj8mhqsi6rjab4kprm260f8a824gkvmy8725")))

(define-public crate-os_info_cli-1.0.2 (c (n "os_info_cli") (v "1.0.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "os_info") (r "^2.0.6") (k 0)) (d (n "predicates") (r "^1.0.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "06xvw7148i0x05n825c0dniml64086r05jxpw85fd9jyqakxdgvs")))

(define-public crate-os_info_cli-2.0.0 (c (n "os_info_cli") (v "2.0.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "os_info") (r "^3.2.0") (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0kqj31qswn3m7nvjl12hpj1zqxvhlvfql7lzvc7lji901ld0ki8s")))

