(define-module (crates-io os lo oslog) #:use-module (crates-io))

(define-public crate-oslog-0.0.1 (c (n "oslog") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0li2q5mp227qpn6psy9h8jdrm0bbixrjaxggzm9g29w4binx6l4h") (f (quote (("logger" "log") ("default" "logger"))))))

(define-public crate-oslog-0.0.2 (c (n "oslog") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "12bjgxg4cnh41injb4yzvys5lk0k199kw2ppn3ywsz5y8awfvbiv") (f (quote (("logger" "log") ("default" "logger"))))))

(define-public crate-oslog-0.0.3 (c (n "oslog") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dashmap") (r "^3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0zxwml9h803yr3f968jx8r42drlwap2w7my5gc2xkyskwshjq2g9") (f (quote (("logger" "dashmap" "log") ("default" "logger"))))))

(define-public crate-oslog-0.0.4 (c (n "oslog") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dashmap") (r "^4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "07021ak3gzf15hx9436zww4j18l77jfzja9mmw0lqsrp6rmkkic1") (f (quote (("logger" "dashmap" "log") ("default" "logger"))))))

(define-public crate-oslog-0.1.0 (c (n "oslog") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dashmap") (r "^4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "1cr8jxb1qrgasamsp5akd0sk0igcfskhxp870a6fdrqqbyawwhw3") (f (quote (("logger" "dashmap" "log") ("default" "logger"))))))

(define-public crate-oslog-0.2.0 (c (n "oslog") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "dashmap") (r "^5.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (o #t) (k 0)))) (h "0s9rgbwsa70ay65ir7cfbxzm0h2z56rbgxxiyjr7rmv13wyh9ll0") (f (quote (("logger" "dashmap" "log") ("default" "logger"))))))

