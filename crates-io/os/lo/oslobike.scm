(define-module (crates-io os lo oslobike) #:use-module (crates-io))

(define-public crate-oslobike-0.1.0 (c (n "oslobike") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "13gha246rgn2id5ncn7s3avrigavvqn4iij1wzwg17zhbn00xvh0")))

