(define-module (crates-io os cl osclip) #:use-module (crates-io))

(define-public crate-osclip-0.0.0 (c (n "osclip") (v "0.0.0") (d (list (d (n "base64-stream") (r "^3.0.1") (d #t) (k 0)))) (h "1fgmwkmxjgwgkfl43iyvqy254lvdp90xz0n428grn2jphymwpi2q")))

(define-public crate-osclip-0.1.0 (c (n "osclip") (v "0.1.0") (d (list (d (n "base64-stream") (r "^3.0.1") (d #t) (k 0)))) (h "13sk246h710grpnma3mlrd60likl6n6clf8c3628smf67p0v3xwd")))

(define-public crate-osclip-0.1.1 (c (n "osclip") (v "0.1.1") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)))) (h "0p5l59za9miiqrfgfbqqip32qfyzip28lznmfp0il6w1ij8z3kik")))

