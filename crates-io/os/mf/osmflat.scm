(define-module (crates-io os mf osmflat) #:use-module (crates-io))

(define-public crate-osmflat-0.1.0 (c (n "osmflat") (v "0.1.0") (d (list (d (n "flatdata") (r "^0.5.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "png") (r "^0.16.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "smallvec") (r "^1.4.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3.20") (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 2)))) (h "1f2lmh0h4zg84422fyjhniysrqkqd0gn5is14yp45fl1c840w3v5")))

