(define-module (crates-io os -c os-core) #:use-module (crates-io))

(define-public crate-os-core-0.1.0 (c (n "os-core") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "1a2k9gxcvixs5z9g62i0bz41g0rk5l2h2qfqdpkscnch38cwpnv5")))

(define-public crate-os-core-0.2.0 (c (n "os-core") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "1vl2qf40bhzrc93jsz3m14d38f5zpxnirhnncqs78m5f62230957")))

