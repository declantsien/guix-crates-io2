(define-module (crates-io os fe osfetch-rs) #:use-module (crates-io))

(define-public crate-osfetch-rs-0.1.0 (c (n "osfetch-rs") (v "0.1.0") (d (list (d (n "nixinfo") (r "^0.2.8") (d #t) (k 0)) (d (n "whoami") (r "^1.1.2") (d #t) (k 0)))) (h "0cgiyv8mgj2ii1g9dfx7qq6x9bikxnvyrxx5y78r8yl57s3l5fa0")))

