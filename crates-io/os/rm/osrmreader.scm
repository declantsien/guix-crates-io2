(define-module (crates-io os rm osrmreader) #:use-module (crates-io))

(define-public crate-osrmreader-0.1.0 (c (n "osrmreader") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "199g41bamh4czdjdh557c2wmvpn8w86nz5rmd4xd5imiz816782y")))

(define-public crate-osrmreader-0.1.1 (c (n "osrmreader") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "05wrqlvhz8vym0l1adhhyyz8mcmd661kx5f6cb1si54n9c1q8jng")))

