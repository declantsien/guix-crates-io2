(define-module (crates-io os mi osmium) #:use-module (crates-io))

(define-public crate-osmium-0.0.1 (c (n "osmium") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)))) (h "1dhhhbvdh7f7zy195ndqz046i8zik1d0rib5kscwyx7ballram7b")))

(define-public crate-osmium-0.0.2 (c (n "osmium") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)))) (h "1996n6xg7dkgpg3xi12ch9y2kajsr98fbwkb0s4ykpjwmf9qc6xv")))

