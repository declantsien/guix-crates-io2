(define-module (crates-io os mi osmium-libs-lsp-server-wrapper) #:use-module (crates-io))

(define-public crate-osmium-libs-lsp-server-wrapper-0.1.0 (c (n "osmium-libs-lsp-server-wrapper") (v "0.1.0") (d (list (d (n "lsp-server") (r "^0.7.4") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0s96qinn5x6xfyci6z55d6pwhnqlriyfl105cr3cvkfwfipv26cv")))

