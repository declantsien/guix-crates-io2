(define-module (crates-io os mi osmium-libs-solidity-ast-extractor) #:use-module (crates-io))

(define-public crate-osmium-libs-solidity-ast-extractor-0.1.0 (c (n "osmium-libs-solidity-ast-extractor") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn-solidity") (r "^0.4.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1l0ln7yr4vimgr9z7ylla34ky1dqszrllhxvmlsv5rbjf457ws63")))

(define-public crate-osmium-libs-solidity-ast-extractor-0.1.1 (c (n "osmium-libs-solidity-ast-extractor") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn-solidity") (r "^0.4.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1859kh558rhp06bgc7kjq846vlhwgka0197yqz6nh79dx84lrp2s")))

(define-public crate-osmium-libs-solidity-ast-extractor-0.1.2 (c (n "osmium-libs-solidity-ast-extractor") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn-solidity") (r "^0.4.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19632g5jqv0lj2zmvks480mv0ywb2y8mn973kv0qzjqqv3vzxxyn")))

