(define-module (crates-io os mo osmosis-authenticators) #:use-module (crates-io))

(define-public crate-osmosis-authenticators-0.22.0-alpha.1 (c (n "osmosis-authenticators") (v "0.22.0-alpha.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1lbcrb4jnzqx983fl1jxyprx8j8vhbzpgjqpkbacryzrxqza7l8a")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.2 (c (n "osmosis-authenticators") (v "0.22.0-alpha.2") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1nsv31wz4zl5qipjyksmns13gwwm2f7s2426l7mgn6y6bm9d37zl")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.3 (c (n "osmosis-authenticators") (v "0.22.0-alpha.3") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0bx9kssm9zjhk8xfknlkhdqwrcjpdi58jf9m4am1kg5dcfdszhby")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.4 (c (n "osmosis-authenticators") (v "0.22.0-alpha.4") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "02pqpqd8nl8gay9k3ikl3612ysn1iblq9i9bblw5rwf7jbqjhild")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.5 (c (n "osmosis-authenticators") (v "0.22.0-alpha.5") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "18r4rx45c4wdqv3waclqnraq9s96b3vxwylpz8k7d1v0fv5hv5qb")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.6 (c (n "osmosis-authenticators") (v "0.22.0-alpha.6") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "18g4m0dqv508m0a8sr9wg0vq2ak8kxrzbr3040sad9fjjxwr8wkh")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.7 (c (n "osmosis-authenticators") (v "0.22.0-alpha.7") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0ahxlmmmwvjqpn1dnw1d6gdfwrhms7xyz6kzhchcj2ib1h1xl1r1")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.8 (c (n "osmosis-authenticators") (v "0.22.0-alpha.8") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0i2aplwjbnfpvapgplaqiirgdbklmy4syrzrn634l1ksdhk14pbi")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.9 (c (n "osmosis-authenticators") (v "0.22.0-alpha.9") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0m8ap6701pm3n80567crm7adz69i15ncryag5l9syldz178bmhnh")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.10 (c (n "osmosis-authenticators") (v "0.22.0-alpha.10") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1sd6zzm1cq64jv7yimsxg1as6ylhmnln47xv8ph411fy8p1wj17v")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.11 (c (n "osmosis-authenticators") (v "0.22.0-alpha.11") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0gyj3kzw7anac3cbh3ib047vsy86bbgamfk9vqknf5n0rxrk3jd8")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.12 (c (n "osmosis-authenticators") (v "0.22.0-alpha.12") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1kw3mm3kdav738gvp317l9c3ajfr57s5dzhmq8blfnmjz1m5rwyi")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.13 (c (n "osmosis-authenticators") (v "0.22.0-alpha.13") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0p937q4m30ln86pqd6r6rf8slx04rq1w57qqsmrfjnsis270hbpn")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.14 (c (n "osmosis-authenticators") (v "0.22.0-alpha.14") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "11f7ricyiih3021pskb6jmgf6lk3r4wkhlsljqwgfxjprp3nlsjf")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.15 (c (n "osmosis-authenticators") (v "0.22.0-alpha.15") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde-json-wasm") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1rrpl2myy9fxk0znqz74fdjc8xdwk0y9dh8pqs1a0k4cq4117df9")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.16 (c (n "osmosis-authenticators") (v "0.22.0-alpha.16") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0cs5y7ha9n1p5nydwyj702cag05fh8vky11h5z9hmcnsvyk8fv7h")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.17 (c (n "osmosis-authenticators") (v "0.22.0-alpha.17") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0ndlk7sszjp8pwy41qwjfsbw4jfgsb025bw2h0rmbxl1ihnw09mz")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.18 (c (n "osmosis-authenticators") (v "0.22.0-alpha.18") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "051yrjjn247aidvksavzlpg31932xg301y30f03rwlnbgmi0j305")))

(define-public crate-osmosis-authenticators-0.22.0-alpha.19 (c (n "osmosis-authenticators") (v "0.22.0-alpha.19") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1xcp3w7cs1qiah8bhqli09jhqscpc9n6vy7yzw7r2asdyv0x6bjr")))

