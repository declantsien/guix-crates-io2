(define-module (crates-io os mo osmojit) #:use-module (crates-io))

(define-public crate-osmojit-0.3.0 (c (n "osmojit") (v "0.3.0") (d (list (d (n "osmojit-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "osmojit-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0rcpicygwryzmxdyj15q3clx0jc7ab3i3ayikpvdvqyb9wqdq0sy")))

(define-public crate-osmojit-0.4.0 (c (n "osmojit") (v "0.4.0") (d (list (d (n "osmojit-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "osmojit-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1rbll7hwdvc0mizgax0cs098mk18s92ardrlb6maxg8dknl1lhsw")))

(define-public crate-osmojit-0.4.1 (c (n "osmojit") (v "0.4.1") (d (list (d (n "osmojit-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "osmojit-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0f5yxll8lv136synsrp2ciyin2z7c9vrrsasi8x71fzmh58j4417")))

(define-public crate-osmojit-0.4.2 (c (n "osmojit") (v "0.4.2") (d (list (d (n "osmojit-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "osmojit-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1iyckxp8d6bki9rj8dpj6cyzg60z7ggynjwcihcnc933qysir3v2")))

