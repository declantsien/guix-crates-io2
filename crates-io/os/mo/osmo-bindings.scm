(define-module (crates-io os mo osmo-bindings) #:use-module (crates-io))

(define-public crate-osmo-bindings-0.1.0 (c (n "osmo-bindings") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "07ajx9qj5v28ndvpa4ksj017nyfcr1s8sy48sd913g2nvs5am90c")))

(define-public crate-osmo-bindings-0.2.0 (c (n "osmo-bindings") (v "0.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "16va5jsxi15j2kbphfjx7zmgj17ipz4y3ckiy4qb4g9yf48y80qs")))

(define-public crate-osmo-bindings-0.3.0 (c (n "osmo-bindings") (v "0.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "18xxv3lx336y67z01vw1jkvw430pq4pxb3wdfjq59d8as995x6r0")))

(define-public crate-osmo-bindings-0.4.0 (c (n "osmo-bindings") (v "0.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1fvb2h3nhcgh9z85l3b341zhkpcg1vdc3p05gawpmnxp0sh7lpys")))

(define-public crate-osmo-bindings-0.4.1 (c (n "osmo-bindings") (v "0.4.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "13q8aiq3mys8ckqaganbyp5hrka3fx0dm3dgav779kgq37qlyx9f")))

(define-public crate-osmo-bindings-0.5.0 (c (n "osmo-bindings") (v "0.5.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0rl7cywsgn2swgx5jw2gky70pybmm2gsjs4mivj4l205bshnhxhn")))

(define-public crate-osmo-bindings-0.5.1 (c (n "osmo-bindings") (v "0.5.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "123z3h6gb75mdd3m2d2f1ii06ybvdinkp8g3cd772h7ibnzlk6nj")))

(define-public crate-osmo-bindings-0.6.0 (c (n "osmo-bindings") (v "0.6.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta8") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1s12l7lsp4wsv02aban8w2gs2qjj4imakjlnngpp8k3lrcxk4zjp")))

