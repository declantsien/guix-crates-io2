(define-module (crates-io os mo osmojit-derive) #:use-module (crates-io))

(define-public crate-osmojit-derive-0.3.0 (c (n "osmojit-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "1hfsbgk24ighjlgl6ay95m9f8im6zy6pyqjp1wah0d6i033a3m9r")))

(define-public crate-osmojit-derive-0.4.0 (c (n "osmojit-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "11884gg3avlkvz2q9n9mkbqlidazyf6qdl9v48rb25z5kbn5dlil")))

(define-public crate-osmojit-derive-0.4.1 (c (n "osmojit-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0hsshpd2qiy64s4w2gjdxcwcsdxxn4fi038va8l3vcl80diswxwn")))

(define-public crate-osmojit-derive-0.4.2 (c (n "osmojit-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0cjl683gsniyclvh2my7bjlnk0ia2b1a7vv0vija1j41f3fx4kyf")))

