(define-module (crates-io os mo osmojit-sys) #:use-module (crates-io))

(define-public crate-osmojit-sys-0.3.0 (c (n "osmojit-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "1sbk9awq6nkniik4gy7hk59l5c6823y3329g2bk0f4cr5pq7gnj9")))

(define-public crate-osmojit-sys-0.4.0 (c (n "osmojit-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "18y32cmdjlh79caqmja1cq9625khfvfz1rg1jdp33pp38sgs3q6w")))

(define-public crate-osmojit-sys-0.4.1 (c (n "osmojit-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1n3dgdij7hx8lz80id718vzh4nxqzsj0xpllb57jnbx420sa7pqp")))

(define-public crate-osmojit-sys-0.4.2 (c (n "osmojit-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1b7yr14nci7dpk887bj86fx5gwbbm3sjymvciw0hg4x024zy64g4")))

