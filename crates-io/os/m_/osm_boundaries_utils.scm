(define-module (crates-io os m_ osm_boundaries_utils) #:use-module (crates-io))

(define-public crate-osm_boundaries_utils-0.2.0 (c (n "osm_boundaries_utils") (v "0.2.0") (d (list (d (n "geo") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.11") (d #t) (k 0)))) (h "1n7h7y5dm4j8xv0981skaiikli3hc8d057nzi5j1jakfnz0j64zn")))

(define-public crate-osm_boundaries_utils-0.2.1 (c (n "osm_boundaries_utils") (v "0.2.1") (d (list (d (n "geo") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.11") (d #t) (k 0)))) (h "1z86gx3c11hskhckknjidj57qnp4y60zrpk1dhljkm2rl7sp82hw")))

(define-public crate-osm_boundaries_utils-0.2.2 (c (n "osm_boundaries_utils") (v "0.2.2") (d (list (d (n "geo") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.11") (d #t) (k 0)))) (h "0akv9bkb2qrvikaxi2rdzfqmd691rh3la7m8nxrwj8y4hv16672q")))

(define-public crate-osm_boundaries_utils-0.3.0 (c (n "osm_boundaries_utils") (v "0.3.0") (d (list (d (n "geo") (r "^0.10.2") (d #t) (k 0)) (d (n "geo-types") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.11") (d #t) (k 0)))) (h "1jzbndr6qzjmmpxypmkc8j69lkifr101snbgswdjziird78xz701")))

(define-public crate-osm_boundaries_utils-0.4.0 (c (n "osm_boundaries_utils") (v "0.4.0") (d (list (d (n "geo") (r "^0.12") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.13") (d #t) (k 0)))) (h "1sw9fb1m2ay04sfds9g970jykm84awpq9qqj2s5x1gx21cy8ayhd")))

(define-public crate-osm_boundaries_utils-0.5.0 (c (n "osm_boundaries_utils") (v "0.5.0") (d (list (d (n "geo") (r "^0.12") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.13") (d #t) (k 0)))) (h "0ww9rm72jylzdvfhbwcxz6a4843qvlgiycy04v0rnkparc8rx2yk")))

(define-public crate-osm_boundaries_utils-0.6.0 (c (n "osm_boundaries_utils") (v "0.6.0") (d (list (d (n "geo") (r "^0.14") (d #t) (k 0)) (d (n "geo-types") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.13") (d #t) (k 0)))) (h "0glcyh2nhsxjly3qhil99kr9wni0ls01w2biqvryjngh42hdkp07")))

(define-public crate-osm_boundaries_utils-0.7.0 (c (n "osm_boundaries_utils") (v "0.7.0") (d (list (d (n "geo") (r "^0.14") (d #t) (k 0)) (d (n "geo-types") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.14") (d #t) (k 0)))) (h "11vp69m5h7znjszadpk76v3diyjj5yvs5fl446bsd0ygcjvjl658")))

(define-public crate-osm_boundaries_utils-0.8.0 (c (n "osm_boundaries_utils") (v "0.8.0") (d (list (d (n "geo") (r "^0.15") (d #t) (k 0)) (d (n "geo-types") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.14") (d #t) (k 0)))) (h "1knh6majf8rb7xikycs9r6x2lzhvjdjvl6jy2mzj2jk34p04693z")))

(define-public crate-osm_boundaries_utils-0.8.1 (c (n "osm_boundaries_utils") (v "0.8.1") (d (list (d (n "geo") (r "^0.15") (d #t) (k 0)) (d (n "geo-types") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.14") (d #t) (k 0)))) (h "1qlj85jzl97lkxnmv8ag26j8d9nmcmzvk2fbq1h1va73bh86xvan")))

(define-public crate-osm_boundaries_utils-0.8.2 (c (n "osm_boundaries_utils") (v "0.8.2") (d (list (d (n "geo") (r ">=0.16.0, <0.17.0") (d #t) (k 0)) (d (n "geo-types") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "osmpbfreader") (r ">=0.14.0, <0.15.0") (d #t) (k 0)))) (h "1yph8pzq4z9z6bilsj8vv2qkvwsykjm3rpk8b0j3ym3f5vznmzf2")))

(define-public crate-osm_boundaries_utils-0.9.0 (c (n "osm_boundaries_utils") (v "0.9.0") (d (list (d (n "geo") (r "^0.18") (d #t) (k 0)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.14") (d #t) (k 0)))) (h "1c7dzgzxm81wmza9z5b98iysavqbh3v6nvg51ffn1w2f9mm92fdf")))

(define-public crate-osm_boundaries_utils-0.10.0 (c (n "osm_boundaries_utils") (v "0.10.0") (d (list (d (n "geo") (r "^0.18") (d #t) (k 0)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.15") (d #t) (k 0)))) (h "1zvrxlbk2ydd69r5is99cbj08zgqqn1qwd8mhv3qy6laidi5cv6x")))

(define-public crate-osm_boundaries_utils-0.11.0 (c (n "osm_boundaries_utils") (v "0.11.0") (d (list (d (n "geo") (r "^0.18") (d #t) (k 0)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16") (d #t) (k 0)))) (h "0v33rbarj33rak8kwf7jdb33mdvmfvsg4ad38bv0hlflp64if20l")))

(define-public crate-osm_boundaries_utils-0.12.0 (c (n "osm_boundaries_utils") (v "0.12.0") (d (list (d (n "geo") (r "^0.26.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.16") (d #t) (k 0)))) (h "0h117i0qqdmqjkdv4490pbpq2s7vmamq8334yv9n9b7lgfplp5y7")))

