(define-module (crates-io os m_ osm_pbf_iter) #:use-module (crates-io))

(define-public crate-osm_pbf_iter-0.1.0 (c (n "osm_pbf_iter") (v "0.1.0") (d (list (d (n "byteorder") (r "~0.5.3") (d #t) (k 0)) (d (n "flate2") (r "~0.2.14") (d #t) (k 0)) (d (n "num_cpus") (r "~1.2.1") (d #t) (k 0)) (d (n "protobuf_iter") (r "^0.1.0") (d #t) (k 0)))) (h "035iv7pwsf7sp0cd3vghbig00jqnajy7b43ilf4d063qllxna2x0")))

(define-public crate-osm_pbf_iter-0.2.0 (c (n "osm_pbf_iter") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (f (quote ("rust_backend"))) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "protobuf_iter") (r ">= 0.1.2") (d #t) (k 0)))) (h "02igiv2dyfhl7y7q0cpj1qrgr9ab9s4h55v6kmc7cnahak2jvqmi")))

(define-public crate-osm_pbf_iter-0.2.1 (c (n "osm_pbf_iter") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libdeflater") (r "^1") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "protobuf_iter") (r ">=0.1.2") (d #t) (k 0)))) (h "0fcqff4kprzdwyn27x5s7h98lv5626q95r8ybvml1qmibrn12286")))

