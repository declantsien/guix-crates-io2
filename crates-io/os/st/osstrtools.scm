(define-module (crates-io os st osstrtools) #:use-module (crates-io))

(define-public crate-osstrtools-0.1.0 (c (n "osstrtools") (v "0.1.0") (h "1mkgc8yfn8ihg52n6fab80phh23bdhnqcb8jnfsncr5jaga9vj3g")))

(define-public crate-osstrtools-0.1.1 (c (n "osstrtools") (v "0.1.1") (h "0rlvmd4hkyfvgarcrklmcbqjw846hbzl9h66cmbh5bpq3q4872ks")))

(define-public crate-osstrtools-0.1.2 (c (n "osstrtools") (v "0.1.2") (h "12c0x4hnpnwhdkvdv0iayz91w9ycs2qbrfxwgakj9ywk8zy9zbd5")))

(define-public crate-osstrtools-0.1.3 (c (n "osstrtools") (v "0.1.3") (h "00qplhbw6haxzcivbm61575jgnpmjg3rxqnnw2zqb546wikvxk93")))

(define-public crate-osstrtools-0.1.4 (c (n "osstrtools") (v "0.1.4") (h "0nqgfdyhz2bz4mv1rq3ypgpcy593z7aj9ilwvby6zdk5y88psmb3")))

(define-public crate-osstrtools-0.1.5 (c (n "osstrtools") (v "0.1.5") (h "1xx19ar08sdmlwixyx5pmwgpq4xqg086azs973fqnkaq5vj352y6")))

(define-public crate-osstrtools-0.1.8 (c (n "osstrtools") (v "0.1.8") (h "1g7c60h57w0wdhqmj4qjsbks7fba423ypb0nqjasjf0ax9cjp80g")))

(define-public crate-osstrtools-0.1.9 (c (n "osstrtools") (v "0.1.9") (h "1w5san8n5m8j326q4zi959q2gi1mvggcc4gizr08rffnns11vb6d")))

(define-public crate-osstrtools-0.2.0 (c (n "osstrtools") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "os_str_bytes") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mqniqpfr8yvl78lzriy0k9gac4jqyn49178fiy2k8z5sinnk5a0") (f (quote (("windows" "os_str_bytes") ("default"))))))

(define-public crate-osstrtools-0.2.1 (c (n "osstrtools") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "os_str_bytes") (r "^1.0") (o #t) (d #t) (k 0)))) (h "150dpp0c6cpp38h4j62hx1ikdpz0p9wlbjj6vh3l3dgq18dizjvs") (f (quote (("windows" "os_str_bytes") ("default"))))))

(define-public crate-osstrtools-0.2.2 (c (n "osstrtools") (v "0.2.2") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "os_str_bytes") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fna5n3b6237dsp6cm8wi07v44iw6azxhryks9wbhfpfkvddla7k") (f (quote (("windows" "os_str_bytes") ("default"))))))

