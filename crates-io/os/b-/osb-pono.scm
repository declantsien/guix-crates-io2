(define-module (crates-io os b- osb-pono) #:use-module (crates-io))

(define-public crate-osb-pono-0.1.0 (c (n "osb-pono") (v "0.1.0") (h "0b541f8b8ywkka6mn9qxarwdapi83vbnxzrj8g8j4g8n30vfldvh")))

(define-public crate-osb-pono-0.1.1 (c (n "osb-pono") (v "0.1.1") (d (list (d (n "osb") (r "^0.1.0") (d #t) (k 0)))) (h "178fh1mma7bj7bkpan8ncakfv273wr88p0x94n419qfm8ygx1ld9")))

(define-public crate-osb-pono-0.1.2 (c (n "osb-pono") (v "0.1.2") (d (list (d (n "osb") (r "^0.1.0") (d #t) (k 0)))) (h "19k8jsmczz07qakfymp6qid412kmrg25ngcyfcfmdyrxcbm2y7bn")))

