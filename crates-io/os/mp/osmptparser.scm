(define-module (crates-io os mp osmptparser) #:use-module (crates-io))

(define-public crate-osmptparser-1.0.0 (c (n "osmptparser") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 2)) (d (n "osm_pbf_iter") (r "^0.2.0") (d #t) (k 0)))) (h "1mbrg4f0b2ps7vn4x3s46k4a5qgi260l221dqnw4g33vjd3pz50v")))

(define-public crate-osmptparser-1.1.0 (c (n "osmptparser") (v "1.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 2)) (d (n "osm_pbf_iter") (r "^0.2.0") (d #t) (k 0)))) (h "15kkdc5113vmzb0416az0vgd44ckyv24dnaqa2vm00m1ifgjj2rs")))

(define-public crate-osmptparser-1.2.0 (c (n "osmptparser") (v "1.2.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 2)) (d (n "osm_pbf_iter") (r "^0.2.0") (d #t) (k 0)))) (h "0llb4fql7dqzw08wgyd4cwfvmrj87h8kj2rks56xncm312n24cql")))

(define-public crate-osmptparser-1.2.1 (c (n "osmptparser") (v "1.2.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 2)) (d (n "osm_pbf_iter") (r "^0.2.0") (d #t) (k 0)))) (h "0a3z3za6rl76a5nc0klnv3zxdcw7bmf8v3inkmcq6c34x2pb8za6")))

(define-public crate-osmptparser-1.2.2 (c (n "osmptparser") (v "1.2.2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 2)) (d (n "osm_pbf_iter") (r "^0.2.0") (d #t) (k 0)))) (h "0i15q4ncdh8kaxsz7mm9bc5lkpikld8zxyc8ad5hrk8kkjs4jha8")))

(define-public crate-osmptparser-2.0.0 (c (n "osmptparser") (v "2.0.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "osm_pbf_iter") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1cm57pjw93cbmkqv37l8zya5rjssqlwg749bpbjzj36ib2z9vilj")))

(define-public crate-osmptparser-2.1.0 (c (n "osmptparser") (v "2.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "osm_pbf_iter") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "144c41q8g2g8kd69xrg8im2xgdbjpqkjzx6fh4jipiigi9ljrdjr")))

