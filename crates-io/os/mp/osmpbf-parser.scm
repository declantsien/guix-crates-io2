(define-module (crates-io os mp osmpbf-parser) #:use-module (crates-io))

(define-public crate-osmpbf-parser-1.0.0 (c (n "osmpbf-parser") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)) (d (n "unbounded-interval-tree") (r "^0.2.3") (d #t) (k 0)))) (h "1sdpchk849c5835dchqrg13h3kgjhcr9jb9b4igxd8i10nnh3va2")))

(define-public crate-osmpbf-parser-1.1.0 (c (n "osmpbf-parser") (v "1.1.0") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)) (d (n "unbounded-interval-tree") (r "^0.2.3") (d #t) (k 0)))) (h "06gxg92qlpvd3amajvldd1sf484n9ksrnh9xnb0x1vzb222xj23k")))

(define-public crate-osmpbf-parser-1.2.0 (c (n "osmpbf-parser") (v "1.2.0") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)) (d (n "unbounded-interval-tree") (r "^0.2.3") (d #t) (k 0)))) (h "0glk0xwzvydkkmlc108hpglbfjg4a645142dgnih8w2n3vf0marj")))

(define-public crate-osmpbf-parser-1.3.0 (c (n "osmpbf-parser") (v "1.3.0") (d (list (d (n "desert") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)) (d (n "unbounded-interval-tree") (r "^0.2.3") (d #t) (k 0)))) (h "0d1356b74g22hz3apvwa87xdlvkids16yfbryl0ymyr1wy5iqwzv")))

(define-public crate-osmpbf-parser-1.3.1 (c (n "osmpbf-parser") (v "1.3.1") (d (list (d (n "desert") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.0") (d #t) (k 0)) (d (n "unbounded-interval-tree") (r "^0.2.3") (d #t) (k 0)))) (h "125dsbgxlsa3ml74a38658bjha756s43jimf0pv24bwyq7rppk00")))

