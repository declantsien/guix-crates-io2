(define-module (crates-io os up osuparse) #:use-module (crates-io))

(define-public crate-osuparse-0.1.0 (c (n "osuparse") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1b9hzi3mjngdzbwybq837v4rfl7wr29a0rfks159rlybq5lpnbix")))

(define-public crate-osuparse-0.1.1 (c (n "osuparse") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mfdllwk9zckklmh2lcnzallz9a20cldgh4fxxwbf5hmf1dhrk6a")))

(define-public crate-osuparse-1.0.0 (c (n "osuparse") (v "1.0.0") (d (list (d (n "unicase") (r "^2.2.0") (d #t) (k 0)))) (h "04209cy9s1g5m7ajzcbjmy7dg2cc80sradp3dpdyfyr45vw37p0c")))

(define-public crate-osuparse-2.0.0 (c (n "osuparse") (v "2.0.0") (d (list (d (n "unicase") (r "^2.2.0") (d #t) (k 0)))) (h "1dv3xbr86n31iwjizw4cnj30fr8s397zd7ya47slb58c1yzfv89q")))

(define-public crate-osuparse-2.0.1 (c (n "osuparse") (v "2.0.1") (d (list (d (n "unicase") (r "^2.2.0") (d #t) (k 0)))) (h "0mpbv732j3ghx512zd7mh83z1qyi3mpilncpkwvp13cx9s51bkin")))

