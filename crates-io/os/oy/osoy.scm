(define-module (crates-io os oy osoy) #:use-module (crates-io))

(define-public crate-osoy-0.2.3 (c (n "osoy") (v "0.2.3") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1hmqbh608m2wv97hab50wr2vj9nz5jc9m7iw31zdhd1ndrad1r1i")))

(define-public crate-osoy-0.2.5-1 (c (n "osoy") (v "0.2.5-1") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fgba2zw1zzm49d6hnqykjwq1dak6sg2b57brskn6v80wk1w3z8g")))

(define-public crate-osoy-0.2.5-2 (c (n "osoy") (v "0.2.5-2") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "133zh0z21ipd5cjgs9qrjkzald1abgc9hxa650hbr7mly931ipcq")))

(define-public crate-osoy-0.2.5 (c (n "osoy") (v "0.2.5") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1drlxrii8hkhdbwxvj28ais4cf5p045367p9x0al7a3mv0v4md30")))

(define-public crate-osoy-0.3.0 (c (n "osoy") (v "0.3.0") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0nin5wv0h3mscg01dyl4sbhdlqn39vlhkjblh8bqjpfs5z7mmkyq")))

(define-public crate-osoy-0.3.1 (c (n "osoy") (v "0.3.1") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0mq0nglg72afkcz4zz7ykp6vi6ag9mx4s2408xsnms891yqsgj5f")))

(define-public crate-osoy-0.4.0 (c (n "osoy") (v "0.4.0") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "12zngsnk6xm2bjr5vzk6vnq6ff06p8lhnhi954l4g0xkb05r75m7")))

(define-public crate-osoy-0.4.1 (c (n "osoy") (v "0.4.1") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0aik5vp0jx8rcayij4rcaqjlgmjm1g1hlaaxngjm83qrach3djzh")))

(define-public crate-osoy-0.5.0 (c (n "osoy") (v "0.5.0") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "16cf5ng34kxmiij5jwcha7sy3f2adr85ad9ba9vwlcca5kqdmzfb")))

(define-public crate-osoy-0.5.1 (c (n "osoy") (v "0.5.1") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1y1cbs161hkn7km4wc65lj0ymsd6wh8nxbmrqh8h30v2bn8c42ii")))

