(define-module (crates-io os #{-i}# os-id) #:use-module (crates-io))

(define-public crate-os-id-1.0.0 (c (n "os-id") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "163n52sjdmg09ksxsv61106mxmijrmgmczslr201z3sl5lssxfjg")))

(define-public crate-os-id-2.0.0 (c (n "os-id") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "19dlqzhbhxgnqdc49b0v0vlz8d6f578y3sxhbdygqisslzzg8b78")))

(define-public crate-os-id-2.0.1 (c (n "os-id") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "0zm34k07kgr3xbhi15rh9g0lpk7204kkjszx6f3z8j5cz1i1dyhd")))

(define-public crate-os-id-2.0.2 (c (n "os-id") (v "2.0.2") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "str-buf") (r "^2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("stringapiset" "processthreadsapi" "winbase" "winerror" "errhandlingapi"))) (o #t) (t "cfg(windows)") (k 0)))) (h "1wqil70k16l69p2cqw4lqdsili1cga6qcy1vlhj00kmw66yg44yn") (f (quote (("thread-name" "str-buf" "winapi"))))))

(define-public crate-os-id-3.0.0 (c (n "os-id") (v "3.0.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "10axflay1ijq879bzgrk514ckzn26d1dzy50c9qp9zjnfp0qnddk")))

(define-public crate-os-id-3.0.1 (c (n "os-id") (v "3.0.1") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "1xm5l0wjqd52xg9lgp0wzs6g51rbqfaxyxfnn1nxhln5apn5c22i")))

