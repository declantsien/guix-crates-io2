(define-module (crates-io os r- osr-parser) #:use-module (crates-io))

(define-public crate-osr-parser-0.1.0 (c (n "osr-parser") (v "0.1.0") (h "0d96qd7gc2na24cr3ndjb7szw1gh9b551hw0c85r1xd7hx5smrp4") (y #t)))

(define-public crate-osr-parser-0.2.0 (c (n "osr-parser") (v "0.2.0") (h "1763qv205nsvhg8pfzkcnil5mplwb5ww8pndqw366h5i9pf94c1a") (y #t)))

(define-public crate-osr-parser-0.3.0 (c (n "osr-parser") (v "0.3.0") (h "16939apjai7mznzxar8bmz753bwi3cssdr8il52rjyiqhrha3j5h") (y #t)))

(define-public crate-osr-parser-0.4.0 (c (n "osr-parser") (v "0.4.0") (h "0cgxa5ig5rv470ddmp1zqx2jq9s56lchr1vw834frivap678x7mr") (y #t)))

(define-public crate-osr-parser-0.5.0 (c (n "osr-parser") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pbirgnck7fda5ii1kj0j1nq6rbgbk6y4szmiwz84fl5js11fvxa") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-osr-parser-0.6.0 (c (n "osr-parser") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kpq83pi9wal5jpxd2vwm9dz650zmhs73ksq21azmvdgg4g9wc8x") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-osr-parser-0.7.0 (c (n "osr-parser") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pnhnzaydiydkmjja88axgvn590wkn43smkyny4k7608c0d3arc6") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-osr-parser-0.7.1 (c (n "osr-parser") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "166c5n3cwd0dgz74plcinlyqh6hpgs6wh5j034izm3q5cd89i1mz") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-osr-parser-0.7.2 (c (n "osr-parser") (v "0.7.2") (d (list (d (n "lzma") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "065mvsh6z8wxan3yb9vvbd8brx5xv2zn5ysmhr2nqlgrc6qzspb7") (s 2) (e (quote (("serde" "dep:serde") ("lzma" "dep:lzma"))))))

(define-public crate-osr-parser-0.7.3 (c (n "osr-parser") (v "0.7.3") (d (list (d (n "lzma") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bzpps0sa67vph27dm5l1ni4zzijzw25ll5ryb4cjydm9127nlw5") (s 2) (e (quote (("serde" "dep:serde") ("lzma" "dep:lzma"))))))

(define-public crate-osr-parser-0.7.4 (c (n "osr-parser") (v "0.7.4") (d (list (d (n "lzma") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ymadyxbnh20yy7zzjb8fzjznvaphszndci471sdf26ap22gr5b2") (s 2) (e (quote (("serde" "dep:serde") ("lzma" "dep:lzma"))))))

