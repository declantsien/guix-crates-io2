(define-module (crates-io os ci oscillatorsetups) #:use-module (crates-io))

(define-public crate-oscillatorsetups-0.0.1 (c (n "oscillatorsetups") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l0m4sq4hmf4bdsianlgsf2bxjh7px9cb5z14d93jsgq1fsz2gxa")))

(define-public crate-oscillatorsetups-0.1.0 (c (n "oscillatorsetups") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1f0sdg3n4nshg0hy1f8d6b5dk4h40if4vqbx0yr96pscp28wspqw")))

