(define-module (crates-io os ci oscirs_plot) #:use-module (crates-io))

(define-public crate-oscirs_plot-0.1.0 (c (n "oscirs_plot") (v "0.1.0") (d (list (d (n "open") (r "^4.1.0") (d #t) (k 0)))) (h "000al5l724ny3qdcnnvmfflk9dixb3hscw70q6gzy95d08f4jbyw")))

(define-public crate-oscirs_plot-0.1.1 (c (n "oscirs_plot") (v "0.1.1") (d (list (d (n "open") (r "^4.1.0") (d #t) (k 0)))) (h "0v4yq2fpxfs7qr8rhfkkidwn7c145ymdzd9lm9d80a89yb0i9ffq")))

(define-public crate-oscirs_plot-0.2.0-alpha (c (n "oscirs_plot") (v "0.2.0-alpha") (d (list (d (n "open") (r "^4.1.0") (d #t) (k 0)))) (h "1jr5rbr82xjxzwwm7jfc92qysq0gbkxxqc26xjy4ki3z8lvi2pzz")))

(define-public crate-oscirs_plot-0.2.0 (c (n "oscirs_plot") (v "0.2.0") (d (list (d (n "open") (r "^4.1.0") (d #t) (k 0)))) (h "0925cy3d5pc6nba4psgzlnh2dmgfdzyyxdvklvgg76ffkfgpbyy2")))

(define-public crate-oscirs_plot-0.3.0 (c (n "oscirs_plot") (v "0.3.0") (d (list (d (n "open") (r "^4.1.0") (d #t) (k 0)))) (h "1xx96bx9ay41alqzhqbbi09cg19l77lyaxbpsly195k7kh4mr9dh")))

