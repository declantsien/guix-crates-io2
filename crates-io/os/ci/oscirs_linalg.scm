(define-module (crates-io os ci oscirs_linalg) #:use-module (crates-io))

(define-public crate-oscirs_linalg-0.1.0 (c (n "oscirs_linalg") (v "0.1.0") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "1rsvqkpvbn6k2b623w0l2g7xs0dazwmr05xyjhfa20jlyzaf4psc")))

(define-public crate-oscirs_linalg-0.1.1 (c (n "oscirs_linalg") (v "0.1.1") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "0w091aas9p9my591iwrydn1wjsj4gf978l37rrv4z14yl7xj2wgm")))

(define-public crate-oscirs_linalg-0.2.0-alpha (c (n "oscirs_linalg") (v "0.2.0-alpha") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "1n1ans2k9mnvmplrzs4l8l9cpk0pg3c4q2a5y2fmh0jhd9ngxl5s")))

(define-public crate-oscirs_linalg-0.2.0-alpha.1 (c (n "oscirs_linalg") (v "0.2.0-alpha.1") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "1knvn206xn638jxnszsqzwvnn7znnq04ixc7mlr0iik6zazmwasa")))

(define-public crate-oscirs_linalg-0.2.0 (c (n "oscirs_linalg") (v "0.2.0") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "1g3hgvnbi3hvlccjjhmgsxyrzf30j8iy7kjvqyfidiicwl9kzisd")))

(define-public crate-oscirs_linalg-0.3.0 (c (n "oscirs_linalg") (v "0.3.0") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "00x31zlkqxg856w7n0jq8r63lp6zji40zmppa3xh36bkll33zji7")))

(define-public crate-oscirs_linalg-0.4.0-alpha (c (n "oscirs_linalg") (v "0.4.0-alpha") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "1fhrpnm8krqg97i9jgxzxqd8m9b0i5vhh4kdqdp8wssi8yj9jdq8")))

