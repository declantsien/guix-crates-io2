(define-module (crates-io os ci oscirs) #:use-module (crates-io))

(define-public crate-oscirs-0.1.0 (c (n "oscirs") (v "0.1.0") (d (list (d (n "oscirs_linalg") (r "^0.1.0") (d #t) (k 0)) (d (n "oscirs_plot") (r "^0.1.0") (d #t) (k 0)) (d (n "oscirs_stats") (r "^0.1.0") (d #t) (k 0)))) (h "06j4sfbsp2dkbnmp7c787j1j50vgwn52nh8iaf8a189pblaizk64")))

(define-public crate-oscirs-0.1.1 (c (n "oscirs") (v "0.1.1") (d (list (d (n "oscirs_linalg") (r "^0.1.1") (d #t) (k 0)) (d (n "oscirs_plot") (r "^0.1.1") (d #t) (k 0)) (d (n "oscirs_stats") (r "^0.1.1") (d #t) (k 0)))) (h "15qjf1d8lz4hzyfmb0jd1pn0qv9v5rrrg79qg0fv95wnsjpywicc")))

(define-public crate-oscirs-0.2.0 (c (n "oscirs") (v "0.2.0") (d (list (d (n "oscirs_linalg") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "oscirs_plot") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "oscirs_stats") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1h7rp2pq3xi0g0jmw5siy4vky4f7q0bdw8a9dbw8ni3z44xpd6jk") (f (quote (("all" "linalg" "plot" "stats")))) (s 2) (e (quote (("stats" "dep:oscirs_stats") ("plot" "dep:oscirs_plot") ("linalg" "dep:oscirs_linalg"))))))

(define-public crate-oscirs-0.3.0 (c (n "oscirs") (v "0.3.0") (d (list (d (n "oscirs_linalg") (r "^0.3.0") (d #t) (k 0)) (d (n "oscirs_plot") (r "^0.3.0") (d #t) (k 0)) (d (n "oscirs_stats") (r "^0.3.0") (d #t) (k 0)))) (h "1nvrw5ybd77jnzjnrxmcnjh8jrz8xll6f1fjrzll1xi4y0w9raah")))

