(define-module (crates-io os ci oscillator) #:use-module (crates-io))

(define-public crate-oscillator-0.1.0 (c (n "oscillator") (v "0.1.0") (d (list (d (n "stopwatch") (r "^0.0.6") (d #t) (k 0)))) (h "1wvyr1yl1djsh1yviqbyjhq01ylxb5f25d50w9m5r6rwnhwr15g0")))

(define-public crate-oscillator-0.1.1 (c (n "oscillator") (v "0.1.1") (h "0scl6c9msyp6191k0h2245z2bkv93vspdxfiam4yrsah3f3ayi6h")))

