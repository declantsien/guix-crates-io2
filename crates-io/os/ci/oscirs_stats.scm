(define-module (crates-io os ci oscirs_stats) #:use-module (crates-io))

(define-public crate-oscirs_stats-0.1.0 (c (n "oscirs_stats") (v "0.1.0") (h "044j0f4yck2h6pl2m02bjlj3g08kprwij28kxnlpjdb5i32iqfdn")))

(define-public crate-oscirs_stats-0.1.1 (c (n "oscirs_stats") (v "0.1.1") (h "1dlwaklhl9cq5q7pdrfz5wdlqf6jlz9cvs8y4wqbqa6cbzp9jg93")))

(define-public crate-oscirs_stats-0.2.0 (c (n "oscirs_stats") (v "0.2.0") (h "1ywhzayb79s9i7nn77c1nby17s64g9d5p0vlpbz27sx6j8r7iy21")))

(define-public crate-oscirs_stats-0.3.0 (c (n "oscirs_stats") (v "0.3.0") (h "0x0cf9phdxv0iacqhih8rpw07izar6dwv827vcma1rk98g2j5mbd")))

