(define-module (crates-io os vr osvr-sys) #:use-module (crates-io))

(define-public crate-osvr-sys-0.1.0 (c (n "osvr-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.29.0") (d #t) (k 1)))) (h "0ppgcnw5qsjd8ac8kpk5y8j7x55vhf66srr76z4dnq89qzagjmwm")))

(define-public crate-osvr-sys-0.1.1 (c (n "osvr-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.29.0") (d #t) (k 1)) (d (n "sdl2") (r "^0.27") (d #t) (k 0)))) (h "0l429s825xjz82xks7a7wvjihlr4z797f8nmlahi38ww7h94jcx6")))

