(define-module (crates-io os vr osvr) #:use-module (crates-io))

(define-public crate-osvr-0.1.0 (c (n "osvr") (v "0.1.0") (d (list (d (n "gl") (r "^0.6.3") (d #t) (k 0)) (d (n "osvr-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.27") (d #t) (k 0)))) (h "08fw0j8bi7jms9wl17zy0593fpxpdfyiw1zv8s5hpya501ryxcr9")))

(define-public crate-osvr-0.1.1 (c (n "osvr") (v "0.1.1") (d (list (d (n "gl") (r "^0.6.3") (d #t) (k 0)) (d (n "osvr-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.27") (d #t) (k 0)))) (h "1n69hb1vp317l1rcrrwvvbm8nj2n8za414cz7p7imldb333zynsj")))

