(define-module (crates-io os au osauth-derive) #:use-module (crates-io))

(define-public crate-osauth-derive-0.1.0 (c (n "osauth-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02mfgc47qyyr30nq8p1rbm83h8kqfr434qgr08s53i1vkyxxpiiz")))

(define-public crate-osauth-derive-0.1.1 (c (n "osauth-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "osauth") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00xk17kpvhxjg6ab80j2p9zq07hq0w5m0x6g4wzdbwiy2vgny9i8")))

(define-public crate-osauth-derive-0.1.2 (c (n "osauth-derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "osauth") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q8w7232j0qc2sqxr68pgangpqcfnqn7b6zbra4f7v7w1z2an0qk")))

