(define-module (crates-io os _u os_utils) #:use-module (crates-io))

(define-public crate-os_utils-0.0.1 (c (n "os_utils") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cocoa") (r "^0.18") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "sysinfoapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "126s79p2qjjjzrr2hc2av59bgx1bz2vjz27csbxmbq5iqy9ydx6i")))

