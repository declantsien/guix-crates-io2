(define-module (crates-io os _d os_display) #:use-module (crates-io))

(define-public crate-os_display-0.1.0 (c (n "os_display") (v "0.1.0") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1p72p0jyiy6p6kv6mxlfxivdxzww61wbzgxyd47qamdz48p06in3") (f (quote (("windows") ("unix") ("std" "alloc") ("native") ("default" "native" "alloc" "std") ("alloc"))))))

(define-public crate-os_display-0.1.1 (c (n "os_display") (v "0.1.1") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1kyqfx7lz4lvhk8y15gwdr0w4skbhacxd9dzf4bq59r451rxfx4f") (f (quote (("windows") ("unix") ("std" "alloc") ("native") ("default" "native" "alloc" "std") ("alloc"))))))

(define-public crate-os_display-0.1.2 (c (n "os_display") (v "0.1.2") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "19f76bbhwz6f71mif0905q68m327qp48vpdyllb7692mvk8c333l") (f (quote (("windows") ("unix") ("std" "alloc") ("native") ("default" "native" "alloc" "std") ("alloc"))))))

(define-public crate-os_display-0.1.3 (c (n "os_display") (v "0.1.3") (d (list (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0xfgfqvfg5nyidv5p85fb87l0mif1nniisxarw6npd4jv2x2jqks") (f (quote (("windows") ("unix") ("std" "alloc") ("native") ("default" "native" "alloc" "std") ("alloc"))))))

