(define-module (crates-io os c_ osc_address_derive) #:use-module (crates-io))

(define-public crate-osc_address_derive-0.2.0 (c (n "osc_address_derive") (v "0.2.0") (d (list (d (n "osc_address") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_osc") (r "^0.4.1") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1v1mdcxhlq6hil2ly03hgxdi0hwy25zrbs1qnfchfx7i5kmyhnfg")))

(define-public crate-osc_address_derive-0.2.1 (c (n "osc_address_derive") (v "0.2.1") (d (list (d (n "osc_address") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_osc") (r "^0.4.1") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0lac0qy5qkpzjb0zqrmd2qd975mw9kfj2zhxbyqs5xnvhrbhn6a3")))

(define-public crate-osc_address_derive-0.2.2 (c (n "osc_address_derive") (v "0.2.2") (d (list (d (n "osc_address") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_osc") (r "^0.4.1") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1c60drx6b0mjzy8hi4n0aady3nnwxhynpjlybrlkwf49aq427gmf")))

