(define-module (crates-io os c_ osc_address) #:use-module (crates-io))

(define-public crate-osc_address-0.2.0 (c (n "osc_address") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08l6a4l4f3wq4djggqq8j0qvd4k8jff3sn9a7kkb96iwwirgdwhx")))

(define-public crate-osc_address-0.2.1 (c (n "osc_address") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1v7axwnz0r2kx077gaq8h62s5cpafvqh1sv72pfmh8i1r0117kmy")))

(define-public crate-osc_address-0.2.2 (c (n "osc_address") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16m9cw90yxv0qykd8793bsrbysd1yznly7jl42bkzb9snrgalwwz")))

