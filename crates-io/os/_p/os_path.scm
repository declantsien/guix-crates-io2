(define-module (crates-io os _p os_path) #:use-module (crates-io))

(define-public crate-os_path-0.1.0 (c (n "os_path") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "07l9b72cn50invqqsm4r11yiyaaqnyckkmw3r2bspm5yir1bh9gs")))

(define-public crate-os_path-0.2.0 (c (n "os_path") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fa3k437gpcfi51sq8l669fdfjp5ad2rk5090an4afg97bfgk22q")))

(define-public crate-os_path-0.2.1 (c (n "os_path") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c3775s039w1q1xcfxbmd0kn2lhjgn9m7m138a8hd9x2vb6ap0w6")))

(define-public crate-os_path-0.2.2 (c (n "os_path") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qva3lh5n9dr4qvghv0pgrclh7zm4bryxw44fj0667zvxra4sf2a")))

(define-public crate-os_path-0.2.3 (c (n "os_path") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "12wrk4ry0679x49kk608jvgv2gd2i9w6lfmbd5j868v7njpjl00f")))

(define-public crate-os_path-0.3.0 (c (n "os_path") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0adkb542sqlw9wb6f6gxjdgzdbni7dam1gc6kvg7rzbmqnkkr72x")))

(define-public crate-os_path-0.4.0 (c (n "os_path") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pzdjc9mz4ddxqwnsfdsj52s6ipnrinh4j0l0qjylgbra8m3nfg6")))

(define-public crate-os_path-0.5.0 (c (n "os_path") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bdsp91wiqhqnk3510k882q1xzx0qnc67flg7f1z4j1zmqbvbllm")))

(define-public crate-os_path-0.6.0 (c (n "os_path") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d54ywpbyxk6nk8xjzipzqxdjdmqxiwl4380rlrxlnpdw0r0wdik")))

(define-public crate-os_path-0.6.1 (c (n "os_path") (v "0.6.1") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "03ngiaxqa7wzzjcsljykr83q0mni23mpg4pjzjbkdrgpxl6fbpaf")))

(define-public crate-os_path-0.6.3 (c (n "os_path") (v "0.6.3") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hr8nz8wf3cqxwzdwfcmaljwhg35gxhh6ayhxd7z35rs1sj80g3a")))

(define-public crate-os_path-0.6.4 (c (n "os_path") (v "0.6.4") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0inlp7zd5fsz6066jz28kg753ivxdap4sm99nl01nahgvcdjk59z")))

(define-public crate-os_path-0.7.0 (c (n "os_path") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16s54x3yaqszp5pl3yvscm1d76xh0jpjk9vdhpjilxfl2yy9vbn0")))

(define-public crate-os_path-0.8.0 (c (n "os_path") (v "0.8.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sqzfisflr4lzvpfjcw1kjbwzwwwfyl6qxqqmsjlnm4z2b5nw2in")))

