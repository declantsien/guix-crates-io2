(define-module (crates-io os _p os_pipe) #:use-module (crates-io))

(define-public crate-os_pipe-0.1.0 (c (n "os_pipe") (v "0.1.0") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0y0i4i5c6d6d8w7ys01psx34k4z90fmkjmvf925n1al8w6r3gi4g")))

(define-public crate-os_pipe-0.1.1 (c (n "os_pipe") (v "0.1.1") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1dklwi1nri7vi6ckpiakcnd8k6v15g52ix8nynjifwfrh04yvadq")))

(define-public crate-os_pipe-0.2.0 (c (n "os_pipe") (v "0.2.0") (d (list (d (n "kernel32-sys") (r ">= 0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r ">= 0.7.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r ">= 0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1laj2cv4px1y1iakkrzm57zpld93g1fhb0aj66lai6m7qwdvj914")))

(define-public crate-os_pipe-0.2.1 (c (n "os_pipe") (v "0.2.1") (d (list (d (n "kernel32-sys") (r ">= 0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r ">= 0.7.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r ">= 0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1j09zf9xs4gv5bhi26pz73nmy54kk993g883sar0gzps5llv3kqy")))

(define-public crate-os_pipe-0.3.0 (c (n "os_pipe") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0ax0nx89wfapa26yw0dqdqc7b2rhlz4hfrqvhk3zrykl7icwi7jn")))

(define-public crate-os_pipe-0.4.0 (c (n "os_pipe") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1jggdl64np9znjynjj2ng9kfbwnpi3kdjrsz8v646ifq6sh3ad52")))

(define-public crate-os_pipe-0.4.1 (c (n "os_pipe") (v "0.4.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "04aqy9c1c5rkmimn4j2dgfw5r06sn6cca6l4x58ak3mjq8xf5bih")))

(define-public crate-os_pipe-0.5.0 (c (n "os_pipe") (v "0.5.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.8.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1g2ghx0nqryc1fyf1lki4phvxclwbj62brskmf9d3y9b7qs56qfh")))

(define-public crate-os_pipe-0.5.1 (c (n "os_pipe") (v "0.5.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.8.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1ccwc7caq3hhgxyrglkl2fw8qzkx0kxanh9azs852w9f0jrzp2wr")))

(define-public crate-os_pipe-0.6.0 (c (n "os_pipe") (v "0.6.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1h536ikx337smr11rpv0j4mfbqx8ahw5fg9kj2n1zwd1ms4pcm3z")))

(define-public crate-os_pipe-0.6.1 (c (n "os_pipe") (v "0.6.1") (d (list (d (n "nix") (r "^0.10.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mzipf61gqcw1s8529hr8g4a0jdxqpmfv21xysga7mvfz31nhj4k")))

(define-public crate-os_pipe-0.6.2 (c (n "os_pipe") (v "0.6.2") (d (list (d (n "nix") (r "^0.11.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0gr59gkbmq69cjh52wg3wx4crvqsxb9d3zr2xcz2q133sljk40zy")))

(define-public crate-os_pipe-0.7.0 (c (n "os_pipe") (v "0.7.0") (d (list (d (n "nix") (r "^0.11.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1iv4r9r3i9n8w7mqh0az8skqjwicmbgmy5pgakp3kvydcy9377fd")))

(define-public crate-os_pipe-0.8.0 (c (n "os_pipe") (v "0.8.0") (d (list (d (n "nix") (r "^0.11.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0i2aarfzg981ygy3i8d58c3c8m30fr56bzz6kfkkg508qpv65wkl")))

(define-public crate-os_pipe-0.8.1 (c (n "os_pipe") (v "0.8.1") (d (list (d (n "nix") (r "^0.13.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0yqmjndfhas4i82g76rmy2s9iz7ddwah5bhfaghal5a576fq27ff")))

(define-public crate-os_pipe-0.8.2 (c (n "os_pipe") (v "0.8.2") (d (list (d (n "nix") (r "^0.15.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "049ax8shxik7mm68r2nf7xnrcq3z3p7hz54sbrcxwywxqsjdzs41")))

(define-public crate-os_pipe-0.9.0 (c (n "os_pipe") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("namedpipeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10dn8c1k6az20mml2sq29iybkfjm6mq4nva4ilahr2w67b489brb")))

(define-public crate-os_pipe-0.9.1 (c (n "os_pipe") (v "0.9.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "08ma8w5qsi11sz33h9j671a32v22267d1ck5562wx43hb8shckfv")))

(define-public crate-os_pipe-0.9.2 (c (n "os_pipe") (v "0.9.2") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04yjs1hf88jjm17g8a2lr7ibxyyg460rzbgcw9f1yzihq833y8zv")))

(define-public crate-os_pipe-1.0.0 (c (n "os_pipe") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09xbp0wdpc2043qn8nf400p9ng81wbf2gm1yw9gqj6rkrbmr4d0f")))

(define-public crate-os_pipe-1.0.1 (c (n "os_pipe") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mczqmqrkzmln4xg5ki1gwgykf4dsii0h4p7fxf667889ysz54ic")))

(define-public crate-os_pipe-1.1.0 (c (n "os_pipe") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dfrn2b8vgqm2rrzy3i8r60vaksfqx2awqrbr71hzasyi14al4ni") (f (quote (("io_safety"))))))

(define-public crate-os_pipe-1.1.1 (c (n "os_pipe") (v "1.1.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02lkqimqzp0kviyg1mp6djxs79a5573v4ic0akhmxhsr7zjbgkhd") (f (quote (("io_safety"))))))

(define-public crate-os_pipe-1.1.2 (c (n "os_pipe") (v "1.1.2") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Pipes" "Win32_Security" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fa640v9bi1qcq3jgq1p76lphi4fwj4a9msrmfrq87n1z3qm58n6") (f (quote (("io_safety"))))))

(define-public crate-os_pipe-1.1.3 (c (n "os_pipe") (v "1.1.3") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_System_Pipes" "Win32_Security" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04ls83i290scb8sfzdzj9b3kr9yplb5k864kg841cjzkz8hbngd5") (f (quote (("io_safety"))))))

(define-public crate-os_pipe-1.1.4 (c (n "os_pipe") (v "1.1.4") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Pipes" "Win32_Security" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xy1igr1jfd9ijhr4sccvl8mzp0jic7njdmr56lsk3220ym5ks0a") (f (quote (("io_safety"))))))

(define-public crate-os_pipe-1.1.5 (c (n "os_pipe") (v "1.1.5") (d (list (d (n "libc") (r "^0.2.62") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Pipes" "Win32_Security" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fcgfg3ddnsh6vfhkk579p7z786kh1khb1dar4g4k1iri4xrq4ap") (f (quote (("io_safety"))))))

