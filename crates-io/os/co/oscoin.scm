(define-module (crates-io os co oscoin) #:use-module (crates-io))

(define-public crate-oscoin-0.2.0 (c (n "oscoin") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1npmyj6y43y7d3sxdvhjvzcr8l0phwvj1wzlvaxsc33yxx2d92d0") (f (quote (("schedule") ("default"))))))

