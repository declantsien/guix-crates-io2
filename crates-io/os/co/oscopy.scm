(define-module (crates-io os co oscopy) #:use-module (crates-io))

(define-public crate-oscopy-0.1.0 (c (n "oscopy") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)))) (h "0rimhlky4syrpg8zr4dipmk6cas9q1w9jmxc125pkqqrw222fr6m")))

(define-public crate-oscopy-0.2.0 (c (n "oscopy") (v "0.2.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)))) (h "1c361f5wb5azkzs1mk6apxlxq0ijajjgvf8sq1gc1rswm21a4c4j")))

