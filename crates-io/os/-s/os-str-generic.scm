(define-module (crates-io os -s os-str-generic) #:use-module (crates-io))

(define-public crate-os-str-generic-0.1.0 (c (n "os-str-generic") (v "0.1.0") (h "01ivdcbjjxyck4c8sf3mpg8b709yjm61pm0dg9gnhi5nvbzgxnn7")))

(define-public crate-os-str-generic-0.1.1 (c (n "os-str-generic") (v "0.1.1") (h "0gm7hij62h3zyz5qkc8zikw4551yvc1mchz0ypnzfwrxm760ilpd")))

(define-public crate-os-str-generic-0.2.0 (c (n "os-str-generic") (v "0.2.0") (h "0zhmlpfc02rv9mf1mrhsxfkhqkmx54qdmwdpahzpd2bvh8hddwvq")))

