(define-module (crates-io os -s os-str-manip) #:use-module (crates-io))

(define-public crate-os-str-manip-0.0.1 (c (n "os-str-manip") (v "0.0.1") (h "1wywqb551zllklhm2446czcvdil9s300a2p8spqfv9rhv5nzpj45") (f (quote (("unchecked_index")))) (y #t) (r "1.56.1")))

(define-public crate-os-str-manip-0.0.2 (c (n "os-str-manip") (v "0.0.2") (h "0dngz2rjp01iar0iijpjmqpw28q99dpfphiwzyn73270l73frlfj") (f (quote (("unchecked_index")))) (y #t) (r "1.56.1")))

(define-public crate-os-str-manip-0.0.3 (c (n "os-str-manip") (v "0.0.3") (h "0y4s68wq2lzlr0jj5dcvl45xpwimk2sqr1v7s9hsvrk972p6g8jl") (f (quote (("unchecked_index")))) (y #t) (r "1.56.1")))

(define-public crate-os-str-manip-0.0.4 (c (n "os-str-manip") (v "0.0.4") (d (list (d (n "proptest") (r "^1.2.0") (d #t) (k 2)))) (h "1jvk7llwdv01d33mn935vh79aqh9hw68h39nrbkmk8b57g42hm4z") (f (quote (("unchecked_index")))) (y #t) (r "1.56.1")))

