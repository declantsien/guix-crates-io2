(define-module (crates-io os -s os-sync) #:use-module (crates-io))

(define-public crate-os-sync-0.1.0 (c (n "os-sync") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "1za5m18q6f3km6akmj05wa3qm5x98mgg273kwm7vzk0r1ssyzkm1")))

(define-public crate-os-sync-0.2.0 (c (n "os-sync") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "1inh3rxmw5vi84pgypkfzjcjk9b1q1jww0xci96gvk96mqi03cl6")))

(define-public crate-os-sync-0.3.0 (c (n "os-sync") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "13n12vq92mshkdk9jnlgsqpamhgq9n34jygjv6xdyvl7f0mrkaki")))

(define-public crate-os-sync-0.3.1 (c (n "os-sync") (v "0.3.1") (d (list (d (n "error-code") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "07iwvanny782fpm0dyq1wp2kmxkyf6xbqv45vlp595fwhfwvw3cq")))

(define-public crate-os-sync-0.3.2 (c (n "os-sync") (v "0.3.2") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "13prbsjxrhy7v173qahp59d0j43gwx683kl2wck4jjy5vi3h09mm")))

(define-public crate-os-sync-0.3.3 (c (n "os-sync") (v "0.3.3") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "07246vph01yvwx3xhsal0kqijna8w21wzrjkhzyz14k4h2k0vx0p")))

