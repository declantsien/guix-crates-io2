(define-module (crates-io os su ossuary) #:use-module (crates-io))

(define-public crate-ossuary-0.5.0 (c (n "ossuary") (v "0.5.0") (d (list (d (n "chacha20-poly1305-aead") (r "= 0.1.2") (f (quote ("simd" "simd_opt"))) (d #t) (k 0)) (d (n "ed25519-dalek") (r "= 1.0.0-pre.1") (d #t) (k 0)) (d (n "rand") (r "= 0.6.5") (d #t) (k 0)) (d (n "x25519-dalek") (r "= 0.5.2") (d #t) (k 0)))) (h "0glfv60dcwm9sgjskgczsiiis70wpf5ffsah3fscfpvd1yf65kah")))

(define-public crate-ossuary-0.5.1 (c (n "ossuary") (v "0.5.1") (d (list (d (n "chacha20-poly1305-aead") (r "= 0.1.2") (f (quote ("simd" "simd_opt"))) (d #t) (k 0)) (d (n "ed25519-dalek") (r "= 1.0.0-pre.1") (d #t) (k 0)) (d (n "rand") (r "= 0.6.5") (d #t) (k 0)) (d (n "x25519-dalek") (r "= 0.5.2") (d #t) (k 0)))) (h "028a99nqvcx062qxjy02msclqksq3h2w9vv4sfq61466293x0a5h")))

