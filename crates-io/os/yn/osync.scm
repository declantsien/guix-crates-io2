(define-module (crates-io os yn osync) #:use-module (crates-io))

(define-public crate-osync-1.2.1 (c (n "osync") (v "1.2.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ftp") (r "^3.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "12gqb2clixgm0jrpkafhw0lzb64yqwa38fid4h5rakjwd8plqh82")))

(define-public crate-osync-1.3.0 (c (n "osync") (v "1.3.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ftp") (r "^3.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0yhmaqk4ylh457ja2fzg2cmfvzfjhmhj59r2c9mdcjfp2w6bbg49")))

(define-public crate-osync-1.3.1 (c (n "osync") (v "1.3.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ftp") (r "^3.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1wmf8xi1mgzc5x48209fzibnkzws26i9rrc342nmszmgj5mwf3ya")))

(define-public crate-osync-1.3.2 (c (n "osync") (v "1.3.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ftp") (r "^3.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1x6jayaiddyjf9h6dggz372rk09zrf24kbby38p3pb36gcl504j4")))

(define-public crate-osync-1.4.0 (c (n "osync") (v "1.4.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ftp") (r "^3.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "09kii5b7xcfq1cxcv3y9hpb3fx9j5x98mb5g2a9hgn2jn9rxxyj5")))

(define-public crate-osync-1.5.0 (c (n "osync") (v "1.5.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ftp") (r "^3.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "16nf7m8czqjrqfciacs2yxbmn57av6s57sm3g4c05gadd21sr22f")))

