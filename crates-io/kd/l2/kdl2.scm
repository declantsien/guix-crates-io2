(define-module (crates-io kd l2 kdl2) #:use-module (crates-io))

(define-public crate-kdl2-0.0.0 (c (n "kdl2") (v "0.0.0") (d (list (d (n "kii") (r "^0.0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "0xp31w96rsvl2m8ad2x16v4dr6jgbpqvqzrpy34xm36x5vsjgadg")))

