(define-module (crates-io kd l2 kdl2xml) #:use-module (crates-io))

(define-public crate-kdl2xml-0.1.0 (c (n "kdl2xml") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "kdl") (r "^4.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0l16aynva34h9jvpa7x2n0ca68nz9r4a6m03l8qnv0lsdxx41rg7") (r "1.62")))

