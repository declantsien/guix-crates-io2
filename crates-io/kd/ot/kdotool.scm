(define-module (crates-io kd ot kdotool) #:use-module (crates-io))

(define-public crate-kdotool-0.2.1 (c (n "kdotool") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "handlebars") (r "^4.5.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "01zpdbml27fni2cmpfx108sxaifxkqr2lgwf1cmdq55cxwmq70z1")))

