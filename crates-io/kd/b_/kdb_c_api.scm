(define-module (crates-io kd b_ kdb_c_api) #:use-module (crates-io))

(define-public crate-kdb_c_api-0.1.0 (c (n "kdb_c_api") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0x407b1hhw76j839sgsbwyksfp9hqm3xsbw39f1rmldnhspvrgjg")))

(define-public crate-kdb_c_api-0.1.1 (c (n "kdb_c_api") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0y1v448fgk00mnm217la2p3ciql4sdalzdrn628h3a97skws22fa") (y #t)))

(define-public crate-kdb_c_api-0.1.2 (c (n "kdb_c_api") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "083pdjck90vi70j5gb6aciy6lz52wf7lhdsk91xrx8c6crqp7fzv") (y #t)))

(define-public crate-kdb_c_api-0.1.3 (c (n "kdb_c_api") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1wplzabmkimisvj5awx98mbnblprg2gza0xd6y6wkbskwxi7djan") (y #t)))

(define-public crate-kdb_c_api-0.1.4 (c (n "kdb_c_api") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "18a5mql2vv98972ifdyr84a6apx3chcpabnr85icfc35l9bn4jyb")))

(define-public crate-kdb_c_api-0.1.5 (c (n "kdb_c_api") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1km15sm67nvsnnmfdvv90bqh5hk2z96ldm3vdl785gba9w7knjnd")))

(define-public crate-kdb_c_api-0.1.6 (c (n "kdb_c_api") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0901lhzngy4hfd7p2rg8aw64yyr0pczp15cvlnf3j029419ry69j")))

