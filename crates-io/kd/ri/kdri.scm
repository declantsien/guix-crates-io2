(define-module (crates-io kd ri kdri) #:use-module (crates-io))

(define-public crate-kdri-0.4.0 (c (n "kdri") (v "0.4.0") (d (list (d (n "bluetooth-serial-port") (r "^0.2") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "mio") (r "^0.5") (d #t) (k 0)))) (h "0ix9j74ayg3pda6xpq54nxwf6gidpcarj39x47l1bc0q4z3pbjls")))

(define-public crate-kdri-0.4.1 (c (n "kdri") (v "0.4.1") (d (list (d (n "bluetooth-serial-port") (r "^0.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "mio") (r "^0.5") (d #t) (k 0)))) (h "1h5f1dx26p8i9mfsbxsip46gn2i3j5hpabcikin0wi60x8jhwwbs")))

(define-public crate-kdri-0.4.2 (c (n "kdri") (v "0.4.2") (d (list (d (n "bluetooth-serial-port") (r "^0.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)))) (h "0ksidivyjk9vgk8ik632rr1slrckmjfclm537x5pnmbzs9wanb6s")))

(define-public crate-kdri-0.4.3 (c (n "kdri") (v "0.4.3") (d (list (d (n "bluetooth-serial-port") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "120sci5kwh2jfcrgq7lfqg2wya6fb4df6aqp96pfyffgqj2ssl1g")))

