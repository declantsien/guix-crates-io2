(define-module (crates-io kd e_ kde_frameworks) #:use-module (crates-io))

(define-public crate-kde_frameworks-0.1.0 (c (n "kde_frameworks") (v "0.1.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "0xqgkxqsina17c19i5y1dil5dqhx541qzxwl7ah64j69g8kskyps")))

(define-public crate-kde_frameworks-0.1.1 (c (n "kde_frameworks") (v "0.1.1") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "0jkdpzgcz6livcqjfqgy6lmmzvxaxnn8337vcasy5gkmycggfl15")))

(define-public crate-kde_frameworks-0.1.2 (c (n "kde_frameworks") (v "0.1.2") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "19i0jc0fl4l820rxj1rf4rkkbf6i8c50iqsvqqc9bvxkirix7zdg")))

(define-public crate-kde_frameworks-0.1.3 (c (n "kde_frameworks") (v "0.1.3") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "13b9l04y4vg9zgzlrn714rs30bbffl1knzv9l6rbzc0vm861c3qk")))

(define-public crate-kde_frameworks-0.1.4 (c (n "kde_frameworks") (v "0.1.4") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "00gs3zxadq8cy96h1syqjcfnvrkrcxva8biifn3mp2zysznyfkvi")))

(define-public crate-kde_frameworks-0.2.0 (c (n "kde_frameworks") (v "0.2.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0wbdvj9sankhpdmhqkr1h2d15lgqqkql6yl66ahmx6gmw759h0zi")))

