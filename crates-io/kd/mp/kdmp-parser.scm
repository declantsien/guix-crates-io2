(define-module (crates-io kd mp kdmp-parser) #:use-module (crates-io))

(define-public crate-kdmp-parser-0.1.0 (c (n "kdmp-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1a32sj7ah6ivrzicnl3cr82vzdv93azba8k8ah1j3n3qxbcmk4kd") (y #t) (r "1.70")))

(define-public crate-kdmp-parser-0.1.1 (c (n "kdmp-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "018jvjjssaha9sww3zg46212jb3rvx1gzyjsm2nn6q0kzy409pc6") (r "1.70")))

(define-public crate-kdmp-parser-0.2.0 (c (n "kdmp-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04c3lnr3x8c282ql5wgf7z4hjb2zka8bbnccmgsyk0jyrr73mlqg") (r "1.70")))

