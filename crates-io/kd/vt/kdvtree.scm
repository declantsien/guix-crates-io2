(define-module (crates-io kd vt kdvtree) #:use-module (crates-io))

(define-public crate-kdvtree-0.5.0 (c (n "kdvtree") (v "0.5.0") (h "0lq75ixkaf2m32i8gxwrxa40h43faq0g45kcb36aq0pg7hmwdqss")))

(define-public crate-kdvtree-0.6.0 (c (n "kdvtree") (v "0.6.0") (h "1dyra4wjdjh4k9x6acrn9wjc63izcfid79a6xajynxs7v7z08n84")))

(define-public crate-kdvtree-0.7.5 (c (n "kdvtree") (v "0.7.5") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1jq6lhy36vv0ykmmdlg480g51vj28pxd2qk011ynd6c8mmbkxyic")))

(define-public crate-kdvtree-0.8.0 (c (n "kdvtree") (v "0.8.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0y3wnsg1zfj0hjf4ly9n6a0sz8mlgz8jrm0qi5cjaz1drsnydqb0")))

