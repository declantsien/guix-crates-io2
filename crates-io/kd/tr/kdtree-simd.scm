(define-module (crates-io kd tr kdtree-simd) #:use-module (crates-io))

(define-public crate-kdtree-simd-0.6.1-alpha.0 (c (n "kdtree-simd") (v "0.6.1-alpha.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0kvxs38i2gy4bng43w1wd0q17rmhdb8qzg3p1adj61kljx1g0rlc") (f (quote (("serialize" "serde" "serde_derive"))))))

