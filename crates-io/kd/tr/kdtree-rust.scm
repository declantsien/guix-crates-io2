(define-module (crates-io kd tr kdtree-rust) #:use-module (crates-io))

(define-public crate-kdtree-rust-0.1.0 (c (n "kdtree-rust") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0mlrk7wd2py956z2j9yz0nl1ghvil70nsyj1wvljx763g63k2nmy")))

(define-public crate-kdtree-rust-0.1.1 (c (n "kdtree-rust") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1mm3zz1l71fmi312jbx5rczgzrh2laypvpzkhavsnqqls1myhp54")))

(define-public crate-kdtree-rust-0.1.2 (c (n "kdtree-rust") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0fz5rll4wi1s77adkrdlwgrabrznlq616a5slkflph09qh2k6k22")))

