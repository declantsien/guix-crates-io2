(define-module (crates-io kd tr kdtree-ray) #:use-module (crates-io))

(define-public crate-kdtree-ray-0.1.0 (c (n "kdtree-ray") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "0qi7n32xcxclwd1f6phkskpfvss113yzgmi1bg7zwwwhmabq1gn4")))

(define-public crate-kdtree-ray-0.1.1 (c (n "kdtree-ray") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "03zbyblq1lghc647aj8dlcc2dw66a6xn0fxcny719cn98xgl2jki")))

(define-public crate-kdtree-ray-0.1.2 (c (n "kdtree-ray") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)))) (h "0dbsy9gh5crzzrdq2xqwmaysxg8z51wxj9k8mbkjl73h8ivfnsw9")))

(define-public crate-kdtree-ray-1.0.0 (c (n "kdtree-ray") (v "1.0.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 0)))) (h "1g13zps1abngnqwi644k14i5ycq4vxy5ag88gh7z0sppf449c6aq")))

(define-public crate-kdtree-ray-1.1.0 (c (n "kdtree-ray") (v "1.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 0)))) (h "0dq07kvflg0bhwfdga6zg7cijgrb8hf60h7ryc6vac8wj87a4kn2")))

(define-public crate-kdtree-ray-1.2.0 (c (n "kdtree-ray") (v "1.2.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "1a1yiq5s0rsa2bi6qdrrjjpldkmgs7ji2wk8ajlh7s1yh4p91ya5")))

(define-public crate-kdtree-ray-1.2.1 (c (n "kdtree-ray") (v "1.2.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "07zsxirnpd3d3i457fycrpfd66dbghz0pzaq81fxi9hfk067v7nx")))

