(define-module (crates-io kd tr kdtree) #:use-module (crates-io))

(define-public crate-kdtree-0.0.0 (c (n "kdtree") (v "0.0.0") (h "1c6dlilg4d12hq848pg2a35h68qxxlsp5awm4c4ma01cprlwjm68")))

(define-public crate-kdtree-0.1.0 (c (n "kdtree") (v "0.1.0") (d (list (d (n "rand") (r "~0.3.9") (d #t) (k 2)))) (h "0iqyz1yrqb8av0fscnyqmfw8dcyyb93931pjqz2vb8zanjxgx54y")))

(define-public crate-kdtree-0.2.0 (c (n "kdtree") (v "0.2.0") (d (list (d (n "rand") (r "~0.3.9") (d #t) (k 2)))) (h "1b15k9z23y26fh8j8zdd4azqiginhn3kn76974y6g784312dpwsx")))

(define-public crate-kdtree-0.2.1 (c (n "kdtree") (v "0.2.1") (d (list (d (n "rand") (r "~0.3.9") (d #t) (k 2)))) (h "1x8zzs5rlkk87794kbpiynwgylnibalcmbpd7pjp39h825hanpjx")))

(define-public crate-kdtree-0.3.0 (c (n "kdtree") (v "0.3.0") (d (list (d (n "rand") (r "~0.3.9") (d #t) (k 2)))) (h "16zr47y3n9k7f2dxsirbzivv405q096py1a8706rk4ibv667rr2i")))

(define-public crate-kdtree-0.3.1 (c (n "kdtree") (v "0.3.1") (d (list (d (n "rand") (r "~0.3.9") (d #t) (k 2)))) (h "1xnlmy370h01jh1m6s7q5if81var4dl5avlhi8ssgihg11bb3ml5")))

(define-public crate-kdtree-0.3.2 (c (n "kdtree") (v "0.3.2") (d (list (d (n "rand") (r "~0.3.9") (d #t) (k 2)))) (h "0s9yc1f927lq9ldx289mgkvkim2v6bx9mbzdxv2csblaqrpjs7y7")))

(define-public crate-kdtree-0.3.3 (c (n "kdtree") (v "0.3.3") (d (list (d (n "rand") (r "~0.3.9") (d #t) (k 2)))) (h "05ac8xv0hx1h3qxpj3wgxrh36c74frx3sxhi2gw8b4cbwvca9c6j")))

(define-public crate-kdtree-0.4.0 (c (n "kdtree") (v "0.4.0") (d (list (d (n "rand") (r "^0.3.9") (d #t) (k 2)))) (h "13mgcl61j4g1ax4g876cfkacn3lwfcq4qxr8x9c8njig32vmbid6")))

(define-public crate-kdtree-0.5.0 (c (n "kdtree") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0nhay5p4s23276qhsmfw3q75p7bcllgl9lid1ngrzbzgr3mwxpwg") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-kdtree-0.5.1 (c (n "kdtree") (v "0.5.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "011v3fixz84qhk9fhmsf358695c3j1alk5dgzclf8zvih686agdn") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-kdtree-0.6.0 (c (n "kdtree") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18mip12jillxil7agmi7l5lyq9yxqikla2pwwglqg47w529kbvl0") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-kdtree-0.7.0 (c (n "kdtree") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fj7bq8w58sx73dgc1zdzyg33idmgak9f3yhmb4vlr8bfyghw2hg") (f (quote (("serialize" "serde" "serde_derive"))))))

