(define-module (crates-io kd um kdump) #:use-module (crates-io))

(define-public crate-kdump-0.1.0 (c (n "kdump") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "05i0ck42y8y0xhncmsw900yg87wf0hgzxf3dvsmysnvvric07qb0")))

(define-public crate-kdump-1.1.0 (c (n "kdump") (v "1.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1lp067hmfbs336qvb01wp6m1x2sprnimdlpkvqxijl4xilvfyibx")))

(define-public crate-kdump-1.1.1 (c (n "kdump") (v "1.1.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1s7y7gs0fbjgrj00fjnlg8ifnxvlf1c6234rlxr0n6rjwvpa8sfk")))

(define-public crate-kdump-1.1.2 (c (n "kdump") (v "1.1.2") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "03jh53h8zax9vphjxwy4yfcfbv20qa2pwdwwidvg8brysvy1nbss")))

(define-public crate-kdump-1.1.4 (c (n "kdump") (v "1.1.4") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "14r1qjm105rlwsvl549h2ckzc4zm7yjqbqvf5xzv01i7y00vamhm")))

(define-public crate-kdump-1.2.0 (c (n "kdump") (v "1.2.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^2.0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0zi8maq161zcr0xy1yahx49svli714ncwi10mhbxmg7kp3xdi6r9")))

(define-public crate-kdump-1.2.1 (c (n "kdump") (v "1.2.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^2.0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1rnplbjvixlippzz689yhj0zv3r9ian28xh8201fdapgqk2mlvrs")))

(define-public crate-kdump-1.2.5 (c (n "kdump") (v "1.2.5") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^2.0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1wakfgqnk2hld790mdzp6frwqgwz8gwr8gi0y4lwwl2n3ckq4fw5")))

(define-public crate-kdump-1.2.6 (c (n "kdump") (v "1.2.6") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^2.0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "08777p2v1hmvpz62dq0byvkcsx18avyrvmxzgnyk3vskc8hg1lqr")))

(define-public crate-kdump-1.5.4 (c (n "kdump") (v "1.5.4") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.0.7") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1cmkxdj67qm0jw9261h90z110pbv93mdk64vf3vgf9h1lbqhdr5j")))

(define-public crate-kdump-1.6.3 (c (n "kdump") (v "1.6.3") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "18ny0a3lfhc0qfjs0dppmgv3b3fz294p479s66270i1dg0sznza3")))

(define-public crate-kdump-1.6.4 (c (n "kdump") (v "1.6.4") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "19fsrsl2pr5cffaa31s88fqnwbhs7v5p1yk0d3mmd4ywkd7v82mc")))

(define-public crate-kdump-1.6.5 (c (n "kdump") (v "1.6.5") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1jk4m4dkz6y9d286m8j6dfn8dyfs7xfq2h7m9qn0frs2hhm8y5fq")))

(define-public crate-kdump-1.6.6 (c (n "kdump") (v "1.6.6") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.11") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1q1wy9fzgfzjmyy8hylk4nqn5a6c03qn69s9rkg0a846kmg95p6s")))

(define-public crate-kdump-1.6.7 (c (n "kdump") (v "1.6.7") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "15mcbjwfb91ka3dxc7q996yam1ylasn77qcf12rqz03sarvp63c4")))

(define-public crate-kdump-1.6.8 (c (n "kdump") (v "1.6.8") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0ix69339rnx9y0alzpycpgcx7v9lg4r7kb0vvcsph9nz8nj6km2p")))

(define-public crate-kdump-1.6.9 (c (n "kdump") (v "1.6.9") (d (list (d (n "clap") (r "~2.34.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0vl3c1jyvkcqgjp4qih2nslwp1b667ad0wqxpp7y9dgxcs7y81a6")))

(define-public crate-kdump-1.6.10 (c (n "kdump") (v "1.6.10") (d (list (d (n "clap") (r "~2.34.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "19hnfi2s8i80h1drmcwhvwrx3x6rackfj5ly3cbc4na1p3ib0jym")))

(define-public crate-kdump-1.6.11 (c (n "kdump") (v "1.6.11") (d (list (d (n "clap") (r "~2.34.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.13") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0jfzw5dikfd1x2gcrqbjwrf42sp493m11lwiyh5dp908i64dzwvi")))

(define-public crate-kdump-2.0.0 (c (n "kdump") (v "2.0.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^4.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0h9hsqfis38lx3rnyjqk68l0x0s9lmdaz8zg5knli81qb5sw75k2")))

(define-public crate-kdump-2.0.1 (c (n "kdump") (v "2.0.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^4.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0xkwykaq8wlhcqbimfiyfz0ayi30y237xshncwplcz398r4mr0mb")))

