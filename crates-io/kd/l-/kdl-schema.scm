(define-module (crates-io kd l- kdl-schema) #:use-module (crates-io))

(define-public crate-kdl-schema-0.1.0 (c (n "kdl-schema") (v "0.1.0") (d (list (d (n "knuffel") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "miette") (r "^3.3.0") (f (quote ("fancy"))) (d #t) (k 2)))) (h "1brvnkf1py0ycykpdxh06h8rxzk369jkwcqwg1n5m8b4w3chiil3") (f (quote (("parse-knuffel" "knuffel") ("default" "parse-knuffel"))))))

