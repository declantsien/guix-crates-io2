(define-module (crates-io kd l- kdl-schema-check) #:use-module (crates-io))

(define-public crate-kdl-schema-check-0.1.0 (c (n "kdl-schema-check") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kdl-schema") (r "^0.1.0") (d #t) (k 0)) (d (n "knuffel") (r "^1.1.0") (f (quote ("base64" "line-numbers"))) (k 0)) (d (n "miette") (r "^3.3.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "09d5dyw616pc98is1q0mxrf35bfclpb2vkjn3fj7zwfciyma6ci2")))

