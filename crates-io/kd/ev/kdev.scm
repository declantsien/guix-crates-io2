(define-module (crates-io kd ev kdev) #:use-module (crates-io))

(define-public crate-kdev-0.0.0 (c (n "kdev") (v "0.0.0") (h "0kxjxb8p63dfzdc69j4cacws3pkynjia3z809xs4z4b97iw7zrrb") (y #t)))

(define-public crate-kdev-0.0.1 (c (n "kdev") (v "0.0.1") (h "0q1fxmy0wrgwbfvcn09bdplgrrlrkx1cd7rw4naclrzq8z4rnhl4") (y #t)))

(define-public crate-kdev-0.0.2 (c (n "kdev") (v "0.0.2") (h "12dn3ic9rwfznyn8m60vma7c9hrhpv93yvl73d9wsm6w7gqa6xiw") (y #t)))

