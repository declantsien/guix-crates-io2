(define-module (crates-io kd bu kdbush) #:use-module (crates-io))

(define-public crate-kdbush-0.1.0 (c (n "kdbush") (v "0.1.0") (h "11yn02h9jwgfijs14kiwqc2f534svyqcnbk9i12w3n0l53cjpqri")))

(define-public crate-kdbush-0.2.0 (c (n "kdbush") (v "0.2.0") (h "1cv47psv1ii66rgadm1mqsp43f1h0whs0bd976nbrnrch2hbfif2")))

