(define-module (crates-io kd _i kd_interval_tree) #:use-module (crates-io))

(define-public crate-kd_interval_tree-0.1.0 (c (n "kd_interval_tree") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18kip9cbp1wlckvcfr0dpsl5xw6wf5w4svp5khjlnwj5m4f2gj2z")))

(define-public crate-kd_interval_tree-0.1.1 (c (n "kd_interval_tree") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1dwjs5dnd9mr4nilalqn39q3fxmkby777vbq8mk4wgyb59sc8a5z")))

