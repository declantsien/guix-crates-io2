(define-module (crates-io kd a- kda-tools) #:use-module (crates-io))

(define-public crate-kda-tools-0.1.1 (c (n "kda-tools") (v "0.1.1") (d (list (d (n "kvc") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "17x2n8ljxhmabyi1xh0an4ya66q1322mgx7kpwi8ffz4jrc9cy4d")))

(define-public crate-kda-tools-0.9.0 (c (n "kda-tools") (v "0.9.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cli-table") (r "^0.4.5") (d #t) (k 0)) (d (n "kvc") (r "^1.0.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.4") (d #t) (k 0)) (d (n "poisson-rate-test") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "08vkamrgmmrpaph783rlkz1biwf9cp330nk9s6rd9i5fdmzxambv")))

(define-public crate-kda-tools-1.3.1 (c (n "kda-tools") (v "1.3.1") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "combinations") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "kvc") (r "^1.0.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.4") (d #t) (k 0)) (d (n "poisson-rate-test") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "0i7gb6isg2767mrcf8q9nyd4dbzrdm9lfg09kn6ycn692f2wp49p")))

