(define-module (crates-io ph an phanes) #:use-module (crates-io))

(define-public crate-phanes-0.1.0 (c (n "phanes") (v "0.1.0") (d (list (d (n "cursive") (r "^0.17.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "languages-rs") (r "^0.2.0") (f (quote ("with-toml"))) (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.0") (d #t) (k 0)))) (h "0xpxk4kz8mwxi60d5mkqif7wrrrgl6h0frrzh69mnzq30zn50q44")))

