(define-module (crates-io ph an phantom-editor) #:use-module (crates-io))

(define-public crate-phantom-editor-0.1.0 (c (n "phantom-editor") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "ropey") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03jvicc8b58gynwxmz3m97akqnxlmf08qaa1rjncrwfzvkk6kbpm")))

