(define-module (crates-io ph an phantom-type) #:use-module (crates-io))

(define-public crate-phantom-type-0.1.0 (c (n "phantom-type") (v "0.1.0") (d (list (d (n "educe") (r "^0.4.13") (f (quote ("Debug" "Default" "Hash" "Clone" "Copy" "Eq" "Ord" "PartialEq" "PartialOrd"))) (k 0)))) (h "13rp02y87lr25jx0pj98cqsqg5jylhlqlm2dyigwxb2zzsdcinj6") (f (quote (("std") ("default" "std"))))))

(define-public crate-phantom-type-0.2.0 (c (n "phantom-type") (v "0.2.0") (d (list (d (n "educe") (r "^0.4.13") (d #t) (k 0)))) (h "1gqsv4r520vf5ilc3xkqxlnbym36lzqwd96pf5zy4lq2qiq7dx8m") (f (quote (("std") ("default" "std"))))))

(define-public crate-phantom-type-0.3.0 (c (n "phantom-type") (v "0.3.0") (d (list (d (n "educe") (r "^0.4.13") (d #t) (k 0)))) (h "1idr439yhsb0ra1adxfgvwf84087j5ngqf813i4809j8772px01v") (f (quote (("std") ("default" "std"))))))

(define-public crate-phantom-type-0.3.1 (c (n "phantom-type") (v "0.3.1") (d (list (d (n "educe") (r "^0.4.13") (d #t) (k 0)))) (h "10j912829s9xpi5g9yghzmi4b86m8ybvnqdbjx7v04cp3k8sy47p") (f (quote (("std") ("default" "std"))))))

(define-public crate-phantom-type-0.4.0 (c (n "phantom-type") (v "0.4.0") (d (list (d (n "educe") (r "^0.4.13") (d #t) (k 0)))) (h "06qy3lwm9938apqsd0d1x107a8j6pfmbnjn9v8584ywyxlrqhb8b") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-phantom-type-0.4.1 (c (n "phantom-type") (v "0.4.1") (d (list (d (n "educe") (r "^0.4.13") (d #t) (k 0)))) (h "11jjh59gz152lv74cc5ynxdikqv4gnjsnn20bpchc2gzqrslh0sm") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-phantom-type-0.4.2 (c (n "phantom-type") (v "0.4.2") (d (list (d (n "educe") (r "^0.4.13") (d #t) (k 0)))) (h "1anygbsdg6jn941aj9l883qgm6258wak5ig14kh479y2jz3mv3z6") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-phantom-type-0.5.0 (c (n "phantom-type") (v "0.5.0") (d (list (d (n "educe") (r "^0.5.11") (d #t) (k 0)))) (h "09qrfk1kwjcxq2yavv4bb41yxbg5l0hyx9x5lh5bikdv8a2nvaaa") (f (quote (("std") ("default" "std")))) (r "1.70")))

