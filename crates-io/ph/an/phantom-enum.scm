(define-module (crates-io ph an phantom-enum) #:use-module (crates-io))

(define-public crate-phantom-enum-0.9.0 (c (n "phantom-enum") (v "0.9.0") (h "0pxvbbca022rd06f3vlxjwwmii1ymzn5bd04lrjwm2ybpmpmmwgz")))

(define-public crate-phantom-enum-0.9.1 (c (n "phantom-enum") (v "0.9.1") (h "1qn7kgf7zygsh5721bmwh22v384bpi9xjd2s5iflb9f7am7khn35")))

(define-public crate-phantom-enum-0.9.2 (c (n "phantom-enum") (v "0.9.2") (h "0i9apgf2lpdx5ci18l7k7qyi7z045wj2rp94ddv3w22fz3w5zggk")))

(define-public crate-phantom-enum-0.9.3 (c (n "phantom-enum") (v "0.9.3") (h "0x4kk6x17sfxrzjlggd0nfrllfqlspylv7bhngshmwzimx0h7r1d")))

(define-public crate-phantom-enum-0.9.4 (c (n "phantom-enum") (v "0.9.4") (h "18y2bdp17i90bplbrzadyhh7d5lakg1mjymanx1asrivmqagj662")))

(define-public crate-phantom-enum-0.9.5 (c (n "phantom-enum") (v "0.9.5") (h "15ywpdjf3169rla38wa5884xx3b7cdkl018fb9y4v9gx58y9rn1w")))

(define-public crate-phantom-enum-1.0.0 (c (n "phantom-enum") (v "1.0.0") (h "0nkbljw787q18xphyxln1lrrhjql0pww9z4csy2vff629xjkaiwq")))

