(define-module (crates-io ph an phantom-secret) #:use-module (crates-io))

(define-public crate-phantom-secret-0.1.0 (c (n "phantom-secret") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10p4774610hvpdd2w826a3q2jdk6i4162xcqbhgjz0bsarkymzfd")))

(define-public crate-phantom-secret-0.1.1 (c (n "phantom-secret") (v "0.1.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qxxmlwqdraml9psc3iqjsm667j9x5sq0plz8a3x5gvvwrc76kjd")))

(define-public crate-phantom-secret-0.1.2 (c (n "phantom-secret") (v "0.1.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hah752kqh38b6safkv6lxpq00cp5qpqy7hnkv1nkfjrrw38bk9b")))

