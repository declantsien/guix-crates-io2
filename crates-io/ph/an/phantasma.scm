(define-module (crates-io ph an phantasma) #:use-module (crates-io))

(define-public crate-phantasma-0.1.0 (c (n "phantasma") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (o #t) (k 0)) (d (n "fastrand") (r "^2.1") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1qsak12abrvmxfn1k575wik8hyhap5w5qkhh8jgshh5qazyxfb3l") (f (quote (("logtime" "chrono") ("default" "logtime"))))))

