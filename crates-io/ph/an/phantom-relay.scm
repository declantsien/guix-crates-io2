(define-module (crates-io ph an phantom-relay) #:use-module (crates-io))

(define-public crate-phantom-relay-0.1.0 (c (n "phantom-relay") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "tcp"))) (d #t) (k 0)))) (h "0w0dfxnbc1xniyx18q3xd2q4v564r8mv4fp6wn5iy763gb4grfki")))

(define-public crate-phantom-relay-0.2.0 (c (n "phantom-relay") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "tcp"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19") (d #t) (k 0)))) (h "1j2nj0p8ls04yvqhzgkxlh06cmg9l1mmpfizvqgx10ijs2j68ilj")))

