(define-module (crates-io ph an phantom_newtype) #:use-module (crates-io))

(define-public crate-phantom_newtype-0.1.0 (c (n "phantom_newtype") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "18lrrwiv13rvapbbxh62p2nybx2zgnfdyg760lwwv93677vvah4v")))

(define-public crate-phantom_newtype-0.2.0 (c (n "phantom_newtype") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "01j58xp5jfan211hmckc6r8jb71pzqkc7v7gkq7mgy2xw4a6dqx4")))

