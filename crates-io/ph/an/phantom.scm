(define-module (crates-io ph an phantom) #:use-module (crates-io))

(define-public crate-phantom-0.0.0 (c (n "phantom") (v "0.0.0") (h "18l8cwrz5m3dl5xzv83k4f3ch0nz7arb5lb2c9dmgsh3hw4qs7f3")))

(define-public crate-phantom-0.0.1 (c (n "phantom") (v "0.0.1") (h "058bx5r66i24s167m23l5ii99i40v61iayfyfnw74zyaqkvly5gp")))

(define-public crate-phantom-0.0.2 (c (n "phantom") (v "0.0.2") (h "0kac9963c91wrk22wbna2w5nddx5b2x4biw420vys8g8g2gzk7f7")))

(define-public crate-phantom-0.0.3 (c (n "phantom") (v "0.0.3") (h "0yjcsvzlqwyxa54a8yjk9hkbdyqhfv4nfms1w2rigmqwhxa89i6v")))

(define-public crate-phantom-0.0.4 (c (n "phantom") (v "0.0.4") (h "12i5d7njjl3ghyffgvw7xs9cb044h8wf0qw55pfxp7nqb14pix2w")))

