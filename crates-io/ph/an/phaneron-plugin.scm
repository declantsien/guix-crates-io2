(define-module (crates-io ph an phaneron-plugin) #:use-module (crates-io))

(define-public crate-phaneron-plugin-0.1.1 (c (n "phaneron-plugin") (v "0.1.1") (d (list (d (n "abi_stable") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ymrppns2l413yib0wch5zv8q8wgy8md1myvrrw0pzcvandlzy1i") (y #t)))

(define-public crate-phaneron-plugin-0.1.2 (c (n "phaneron-plugin") (v "0.1.2") (d (list (d (n "abi_stable") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "035s1bvb5gn3v89xkslmnrbvn09s83lds6n6fb92nr66vyp9qpad")))

