(define-module (crates-io ph an phantomdrop) #:use-module (crates-io))

(define-public crate-phantomdrop-1.0.0 (c (n "phantomdrop") (v "1.0.0") (h "0hvph6yzsqc9r9jb1lmy68gjyj7239whh9p158iymk04xw2i6s4i")))

(define-public crate-phantomdrop-1.0.1 (c (n "phantomdrop") (v "1.0.1") (h "025ci41vx2a4cyp6gljd49ags3hy3rv6i15dn4dd38kwx4q4iam9")))

