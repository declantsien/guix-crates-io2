(define-module (crates-io ph an phant) #:use-module (crates-io))

(define-public crate-phant-0.0.2 (c (n "phant") (v "0.0.2") (d (list (d (n "url") (r "~0.2.1") (d #t) (k 0)))) (h "1a1dl7nb9s1z0fhwvkyamr704vwxv9x7x1kw2nqbvgf7a7c9j417")))

(define-public crate-phant-0.0.3 (c (n "phant") (v "0.0.3") (d (list (d (n "url") (r "^0.2.14") (d #t) (k 0)))) (h "0m3jk5p281f9idm2g41dipkmkrl8cv8i0q9grxhfvcf6csb2bxds")))

(define-public crate-phant-0.0.4 (c (n "phant") (v "0.0.4") (d (list (d (n "url") (r "*") (d #t) (k 0)))) (h "15ly1rv99pgfz2pqivf4p1329gm117m97p058z66v8iif9xn7kw5")))

(define-public crate-phant-0.0.6 (c (n "phant") (v "0.0.6") (d (list (d (n "url") (r "*") (d #t) (k 0)))) (h "1rsj9jfbshnzvk5kymbxxkjxpa8fnly5yqlvhapphs8nrj26qpcv")))

(define-public crate-phant-0.1.0 (c (n "phant") (v "0.1.0") (d (list (d (n "url") (r "*") (d #t) (k 0)))) (h "05lx7x7c8mqx9q8wba1mbic1na2icl6xcd1knfkzkixwkfgmi8l3")))

(define-public crate-phant-0.0.7 (c (n "phant") (v "0.0.7") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "132bvcpw96ss9y6kvxfc6i3jxa1cl4i327jk28vvmchyaiir2985")))

(define-public crate-phant-0.1.1 (c (n "phant") (v "0.1.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "015myk92jgjczi44f7826nz0wpz00bw84sjk7s78w8f8j61i1gyp")))

(define-public crate-phant-0.1.2 (c (n "phant") (v "0.1.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_json") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0nba3spryc6a4ankb6a2whg07r1qzvf1km0z4nknf24zny129nm6")))

(define-public crate-phant-0.1.3 (c (n "phant") (v "0.1.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_json") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1y4yxgwjb0mi03a7s7mq7z7w7xzbj7slpi0a2p2iz0bx16cwywx4")))

(define-public crate-phant-0.1.4 (c (n "phant") (v "0.1.4") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "0savpbj1rv1fjch33qs39vxchi83fg71rskc0rh93mqmxkbi70gk")))

(define-public crate-phant-0.1.5 (c (n "phant") (v "0.1.5") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "0f25a3i1r6p0afz1yfbpgnr6yk65hw5p2263yyk619rmmdjkp2ss")))

