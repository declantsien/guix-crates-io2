(define-module (crates-io ph an phaneron-plugin-ffmpeg) #:use-module (crates-io))

(define-public crate-phaneron-plugin-ffmpeg-0.1.2 (c (n "phaneron-plugin-ffmpeg") (v "0.1.2") (d (list (d (n "abi_stable") (r "^0.11.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "ffmpeg-the-third") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "phaneron-plugin") (r "^0.1.2") (d #t) (k 0)) (d (n "phaneron-plugin-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0x31zk4fx7z3b9yrxwdzrpm214kcb4wvzzj0v6c2vi0qygz6drbm")))

