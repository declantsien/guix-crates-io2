(define-module (crates-io ph ol phollaits) #:use-module (crates-io))

(define-public crate-phollaits-0.1.0 (c (n "phollaits") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1d6qz4q39qdyrsrsjykzqkiaf4ara17w7x4c7s4qxvbq16r00i09")))

(define-public crate-phollaits-0.1.2 (c (n "phollaits") (v "0.1.2") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "18386p0wb2a440f8pidkpn2dwwfxj4fds7gf932b0cgi0236d8v1")))

(define-public crate-phollaits-0.1.3 (c (n "phollaits") (v "0.1.3") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1gkxklkylfsicxbpipihsgg718lsavkacff0x6y00mr1lxvd4ggd")))

(define-public crate-phollaits-0.1.4 (c (n "phollaits") (v "0.1.4") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1q2y9i413qc38vfmghhvr239rnkihd32whb3sgl072anaq4v0nsk")))

(define-public crate-phollaits-0.1.5 (c (n "phollaits") (v "0.1.5") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0vyk5zmih44xpz18910zad91kmjqz2g4cqlq4afdhwbsd524fmcv")))

(define-public crate-phollaits-0.1.6 (c (n "phollaits") (v "0.1.6") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0jv7ckc0i1nf66nf1hgz5z166cpib3ijj5hi11x2qi1dfg48jppx")))

(define-public crate-phollaits-0.1.7 (c (n "phollaits") (v "0.1.7") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "071h9rl0nybmygryjdzl600lpf3gk09p19abhphrz3qqa3mdw6rr")))

(define-public crate-phollaits-0.1.8 (c (n "phollaits") (v "0.1.8") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1sbq50aj8csk5297birl1kdy3nkwc196qrxl87chyv0xcp8gpvh1")))

(define-public crate-phollaits-0.1.9 (c (n "phollaits") (v "0.1.9") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1d0ww5h5mkhgiaaksqsgs4ylf2ppal7knrbzbfp7fzm4c7dvh9zh")))

(define-public crate-phollaits-0.2.0 (c (n "phollaits") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1pjwvjra0yyal6rkicashqnnsg5chkbm05636m9wgvd6l24qwq99")))

(define-public crate-phollaits-0.2.1 (c (n "phollaits") (v "0.2.1") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1mlradsjqwcjiqd9f3nm4lgs0rgxci17fzk56211z719n49f3ari")))

(define-public crate-phollaits-0.2.2 (c (n "phollaits") (v "0.2.2") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0v0lqbnkhailygja96vqb40rfya1z5lczpnkq641hjswl3z6ss78")))

(define-public crate-phollaits-0.2.3 (c (n "phollaits") (v "0.2.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "041p1rr5k7dp8s3j182riksr56v917g09xwbbsm9ik58d8jnx7ll")))

(define-public crate-phollaits-0.2.4 (c (n "phollaits") (v "0.2.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1rcwj56xpzwff1rqbx1mxxxmdwa5b217sdhf3f9l6axjjgcylmdb")))

(define-public crate-phollaits-0.2.5 (c (n "phollaits") (v "0.2.5") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1b8x3kqrsqsf2f8fhhzql1x2y9b0bbqd9wk5m1zb9zkzp77w4462")))

(define-public crate-phollaits-0.2.6 (c (n "phollaits") (v "0.2.6") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0bf9a8h3l5lnqgkhqa0k2vrizz1l8sqd2fhparlwg4pjcx58h2qz")))

(define-public crate-phollaits-0.2.7 (c (n "phollaits") (v "0.2.7") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "18cm8kksm9ga3n27h1dq0bi0mj6ckg11lhg4zlxy976z2la86id8")))

(define-public crate-phollaits-0.2.8 (c (n "phollaits") (v "0.2.8") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "17z44pwlfv997v87i16sv3rl43yi9mbck0cfybqzv5vvhnfck10f")))

(define-public crate-phollaits-0.3.1 (c (n "phollaits") (v "0.3.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0xs5by9lfwyyqwsarkb5l13a8rffss4x25pga6vfsdj9fspc67q4")))

(define-public crate-phollaits-0.3.3 (c (n "phollaits") (v "0.3.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0f4fwai56c7m489s9himf6ix6wwiyygarw5lcg818q9jffvrsnb1")))

