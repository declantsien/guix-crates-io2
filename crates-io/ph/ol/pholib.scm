(define-module (crates-io ph ol pholib) #:use-module (crates-io))

(define-public crate-pholib-0.1.0 (c (n "pholib") (v "0.1.0") (h "0ll4fbvcc6j44nbrr3jzin39r5vnnck6yy974gfxp018n563dr1w") (y #t)))

(define-public crate-pholib-1.0.0 (c (n "pholib") (v "1.0.0") (h "0bgaqak1vwxmc1plv1f1kfrkk7mhmrrcwmjh7nl0wxwgp69v394k") (y #t)))

(define-public crate-pholib-1.0.1 (c (n "pholib") (v "1.0.1") (h "1gj6ynrvmd2yfi32aidh3w89d8aw3hyk1sf2adcn0gnrqmn25ssz") (y #t)))

(define-public crate-pholib-1.1.0 (c (n "pholib") (v "1.1.0") (h "0pxkv6ixnaxgx4l5k5jrba2dcch9x8p9yndnqc0hii68qwzn0hjb") (y #t)))

