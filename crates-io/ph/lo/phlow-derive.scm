(define-module (crates-io ph lo phlow-derive) #:use-module (crates-io))

(define-public crate-phlow-derive-1.0.0 (c (n "phlow-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hbwbs4c65a197zjwkb5gp0s8anrs1zpr4fr561rny8f0rsf60pn")))

(define-public crate-phlow-derive-1.1.0 (c (n "phlow-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zrq6d9cbds631a47zhjczkcpbjps6gqihh1ir4fqdglsmgaic5d")))

(define-public crate-phlow-derive-1.2.0 (c (n "phlow-derive") (v "1.2.0") (d (list (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h201jm9g9mxnb428hly0xqnaxrm0nv8zab7arb8w1pb9nd9rydw")))

(define-public crate-phlow-derive-1.2.1 (c (n "phlow-derive") (v "1.2.1") (d (list (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mckjljw0zjx8g7b46clnciar0r2wb4sk7gl98fdnr1ql46av0wd")))

(define-public crate-phlow-derive-1.2.2 (c (n "phlow-derive") (v "1.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3") (f (quote ("token_stream"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nvjh017gwknx17wj5afvp09ddbrnf1f1ajc3d9dvrghjv0xwl2b")))

(define-public crate-phlow-derive-1.3.0 (c (n "phlow-derive") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3") (f (quote ("token_stream"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s1g33y1m6xm99vwi6kj3f7jyx4kn0rh1cz0d9hkryfzn23jick9")))

(define-public crate-phlow-derive-1.4.0 (c (n "phlow-derive") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3") (f (quote ("token_stream"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03vq90qgbmcr7i5jjsy6k306p076vpgc4yjcnmrh665l720s8bqd")))

(define-public crate-phlow-derive-2.0.0 (c (n "phlow-derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3") (f (quote ("token_stream"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10dsh8vj5hbfs2zfbwi6yx29ybwndkf9ihnajr9xz7v0gwphdwyn")))

