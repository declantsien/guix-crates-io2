(define-module (crates-io ph lo phlow-ffi) #:use-module (crates-io))

(define-public crate-phlow-ffi-1.0.0 (c (n "phlow-ffi") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "phlow") (r "^1") (d #t) (k 0)) (d (n "string-box") (r "^1") (d #t) (k 0)) (d (n "value-box") (r "^2") (f (quote ("phlow"))) (d #t) (k 0)))) (h "0jwwp00c6vghk5gyfkmm5p7w1amqz8jg617ka5p7zsbhlxk3lxmn")))

(define-public crate-phlow-ffi-1.0.1 (c (n "phlow-ffi") (v "1.0.1") (d (list (d (n "phlow") (r "^1") (d #t) (k 0)) (d (n "string-box") (r "^1") (d #t) (k 0)) (d (n "value-box") (r "^2") (f (quote ("phlow"))) (d #t) (k 0)))) (h "0pr2p9qllz9z3wj2nc0jggwrpsba6a7mp85hi7mhp9n31vgqxvxx")))

(define-public crate-phlow-ffi-1.1.0 (c (n "phlow-ffi") (v "1.1.0") (d (list (d (n "phlow") (r "^1") (d #t) (k 0)) (d (n "string-box") (r "^1") (d #t) (k 0)) (d (n "value-box") (r "^2") (f (quote ("phlow"))) (d #t) (k 0)))) (h "0mh05p9swz9cgxfsk6la30w6pzfpli6iiv8y3wiql7yn0zyl5qgf")))

(define-public crate-phlow-ffi-1.6.0 (c (n "phlow-ffi") (v "1.6.0") (d (list (d (n "phlow") (r "^1") (d #t) (k 0)) (d (n "string-box") (r "^1") (d #t) (k 0)) (d (n "value-box") (r "^2") (f (quote ("phlow"))) (d #t) (k 0)))) (h "1j8ldbqd2ghrxngsfmk8pvvk8s55xdg9m5dr4pfm6frkvbdzgz3y")))

(define-public crate-phlow-ffi-2.0.0 (c (n "phlow-ffi") (v "2.0.0") (d (list (d (n "phlow") (r "^2.0") (d #t) (k 0)) (d (n "string-box") (r "^1") (d #t) (k 0)) (d (n "value-box") (r "^2") (f (quote ("phlow"))) (d #t) (k 0)))) (h "1rwrksp0n177ih8djdcbw9y1vx1x4s88qpcajiyjzjrkpk3jjl33")))

