(define-module (crates-io ph lo phlow-extensions) #:use-module (crates-io))

(define-public crate-phlow-extensions-1.0.0 (c (n "phlow-extensions") (v "1.0.0") (d (list (d (n "phlow") (r "=1.0.0") (f (quote ("phlow-derive"))) (d #t) (k 0)) (d (n "phlow") (r "^1.0") (f (quote ("phlow-derive"))) (d #t) (k 2)))) (h "0hm01b7r39bfh39bwbbak3jvcrxka6zdp71vdig3a7xiq5p59vip")))

(define-public crate-phlow-extensions-1.1.0 (c (n "phlow-extensions") (v "1.1.0") (d (list (d (n "phlow") (r "=1.1.0") (f (quote ("phlow-derive"))) (d #t) (k 0)) (d (n "phlow") (r "^1") (f (quote ("phlow-derive"))) (d #t) (k 2)))) (h "1kp6xqqigrkby0w5pamm04ng98am0jzd7nwywqwka5px7z0rf6jx")))

(define-public crate-phlow-extensions-1.2.0 (c (n "phlow-extensions") (v "1.2.0") (d (list (d (n "phlow") (r "=1.2.0") (f (quote ("phlow-derive"))) (d #t) (k 0)) (d (n "phlow") (r "^1") (f (quote ("phlow-derive"))) (d #t) (k 2)))) (h "071bzdvdm3m9mr62gyhc12klnxfr5qq9dlsg28d4jp91d517sad9")))

(define-public crate-phlow-extensions-1.2.1 (c (n "phlow-extensions") (v "1.2.1") (d (list (d (n "phlow") (r "=1.2.1") (f (quote ("phlow-derive"))) (d #t) (k 0)) (d (n "phlow") (r "^1") (f (quote ("phlow-derive"))) (d #t) (k 2)))) (h "0s0lrxw6g4zmf7fd71x7m09wvmsp8dhhxmbns9j51b7vcwds1axh")))

(define-public crate-phlow-extensions-1.2.2 (c (n "phlow-extensions") (v "1.2.2") (d (list (d (n "phlow") (r "^1.2") (f (quote ("phlow-derive"))) (d #t) (k 0)) (d (n "phlow") (r "^1") (f (quote ("phlow-derive"))) (d #t) (k 2)))) (h "083cl5j06330ln347bf60jawmwfsm5n9jwszpqavlz09n4irn8y4")))

(define-public crate-phlow-extensions-1.3.0 (c (n "phlow-extensions") (v "1.3.0") (d (list (d (n "phlow") (r "^1.3") (f (quote ("phlow-derive"))) (d #t) (k 0)) (d (n "phlow") (r "^1") (f (quote ("phlow-derive"))) (d #t) (k 2)))) (h "1j463gsn7f8ppzq24b0v7ww2y9paw3vsg9fcjs2y8kyfk5h75v4r")))

(define-public crate-phlow-extensions-1.4.0 (c (n "phlow-extensions") (v "1.4.0") (d (list (d (n "phlow") (r "^1.4") (f (quote ("phlow-derive"))) (d #t) (k 0)) (d (n "phlow") (r "^1") (f (quote ("phlow-derive"))) (d #t) (k 2)))) (h "1vjjivky414a4gl9zlfrr835jq96hcm6iqssw53y3whl2cg857vj")))

(define-public crate-phlow-extensions-1.5.0 (c (n "phlow-extensions") (v "1.5.0") (d (list (d (n "phlow") (r "^1.5") (d #t) (k 0)) (d (n "phlow") (r "^1") (d #t) (k 2)))) (h "1cgaq6x5r2n4anps8y33abg8v60w58sf5mkm77i69dnd6pnnsaar")))

(define-public crate-phlow-extensions-1.6.0 (c (n "phlow-extensions") (v "1.6.0") (d (list (d (n "phlow") (r "^1.5") (d #t) (k 0)) (d (n "phlow") (r "^1") (d #t) (k 2)))) (h "0hzy2v05v9n144q970b0pv8qdfqsixp4dj5pizy1bp0vlsp3rzq6")))

(define-public crate-phlow-extensions-2.0.0 (c (n "phlow-extensions") (v "2.0.0") (d (list (d (n "phlow") (r "^2") (d #t) (k 0)))) (h "1rbs7ya21ly4mdb6dwd8644r49gc58kdbnpjgcisrw6ibfdykm8g")))

