(define-module (crates-io ph lo phloem) #:use-module (crates-io))

(define-public crate-phloem-0.1.0 (c (n "phloem") (v "0.1.0") (d (list (d (n "clippy") (r "*") (d #t) (k 0)))) (h "19ij06lh14x75a36wxawg5l14sj5v9bffpxwwndwckak7ik268hz") (f (quote (("dev") ("default"))))))

(define-public crate-phloem-0.2.0 (c (n "phloem") (v "0.2.0") (d (list (d (n "clippy") (r "*") (d #t) (k 0)))) (h "064vli4cr4rn044nbz2jv46qiq2vqw541y73jjprzdl788j98j28") (f (quote (("dev") ("default"))))))

(define-public crate-phloem-0.2.1 (c (n "phloem") (v "0.2.1") (d (list (d (n "clippy") (r "*") (d #t) (k 0)))) (h "1v6ahhrmc8msgf2zvamizly6f4y19dx717fxiyz52xqmm1w1ni22") (f (quote (("dev") ("default"))))))

(define-public crate-phloem-0.2.2 (c (n "phloem") (v "0.2.2") (d (list (d (n "clippy") (r "*") (d #t) (k 0)))) (h "0q8zbscxabhvib4ljrznshab817gc1z1faqhvqdnidxz76likws0") (f (quote (("dev") ("default"))))))

(define-public crate-phloem-0.2.3 (c (n "phloem") (v "0.2.3") (d (list (d (n "clippy") (r "^0.0.22") (d #t) (k 0)))) (h "0njfiil7w1wwgz3mypy1nvhvi35wv10jpwdpfyryksb32pmrwpci") (f (quote (("dev") ("default"))))))

(define-public crate-phloem-0.2.4 (c (n "phloem") (v "0.2.4") (d (list (d (n "clippy") (r "^0.0.23") (d #t) (k 0)))) (h "1zk7wdx03w98bbwlajdj7hnd9c0s39n9rznv0nmshgn8whd9rxs9") (f (quote (("dev") ("default"))))))

(define-public crate-phloem-0.3.0 (c (n "phloem") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.23") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "~0.0.3") (d #t) (k 0)))) (h "0hybljs3d4zbh0zg6998jd2l1gq9b01rnkzkhbcgbx6bj8y3iinp") (f (quote (("lint" "clippy") ("dev") ("default"))))))

(define-public crate-phloem-0.3.1 (c (n "phloem") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0.23") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "= 0.0.3") (d #t) (k 0)))) (h "1z1glbc9vwy13aabf960fwsmbl0dwyba9s0cimkrqxg4pvncvf32") (f (quote (("lint" "clippy") ("dev") ("default"))))))

