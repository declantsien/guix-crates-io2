(define-module (crates-io ph pi phpify) #:use-module (crates-io))

(define-public crate-phpify-0.1.0-beta1 (c (n "phpify") (v "0.1.0-beta1") (h "0bksrlwvics20hknvsvsmww84frnl7cwc0534x0gqjd4pww1j0s5")))

(define-public crate-phpify-0.1.0-beta2 (c (n "phpify") (v "0.1.0-beta2") (h "0vy5jrsajm8dqapbsangp9wrnazcpz2zx1f13gkv3zqpl22pll8w")))

(define-public crate-phpify-0.1.0-beta3 (c (n "phpify") (v "0.1.0-beta3") (h "1b5l39f6r4zcm6sgmr1yrrgby0fd5g769nnjbc27lcv6yy6mz07b")))

(define-public crate-phpify-0.1.0-beta4 (c (n "phpify") (v "0.1.0-beta4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1z9rwxfvp26wmr2lq3vklwz236sdfhsv7bx0i15kb1syzaw4yhh1")))

