(define-module (crates-io ph os phosphor-leptos) #:use-module (crates-io))

(define-public crate-phosphor-leptos-0.1.0 (c (n "phosphor-leptos") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 1)) (d (n "leptos") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 1)))) (h "0pa5dm2zk2wmq2fgci8bchbv3iq8dx2ij557xrz7xnlzi1ynq8i3")))

(define-public crate-phosphor-leptos-0.1.1 (c (n "phosphor-leptos") (v "0.1.1") (d (list (d (n "leptos") (r "^0.5.1") (d #t) (k 0)))) (h "0rn7mf9ji1kib8h948zr1nhvggvaavqiqrgwpz17vhi05dc8vf6l")))

(define-public crate-phosphor-leptos-0.2.0 (c (n "phosphor-leptos") (v "0.2.0") (d (list (d (n "leptos") (r "^0.5") (d #t) (k 0)))) (h "0v54yx6ancl830axsmnhj5ib5vffkfnvi61rfdf57s0kw5kj8jbm")))

(define-public crate-phosphor-leptos-0.2.1 (c (n "phosphor-leptos") (v "0.2.1") (d (list (d (n "leptos") (r "^0.5") (d #t) (k 0)))) (h "0fawqfcm7r51m2dipx6ca3n1hl2ibzjd8f38xh5621csayl9n5xa")))

(define-public crate-phosphor-leptos-0.3.0 (c (n "phosphor-leptos") (v "0.3.0") (d (list (d (n "leptos") (r "^0.6") (d #t) (k 0)))) (h "0a1nvgkm9la6ja5iizi2bvivzni99m11748p14974jldldc3h3n3")))

(define-public crate-phosphor-leptos-0.3.1 (c (n "phosphor-leptos") (v "0.3.1") (d (list (d (n "leptos") (r "^0.6") (d #t) (k 0)))) (h "03frcr4ic1fj95s8wxdahiva60xh9i4pczwn525l81bbar5rmvx3") (f (quote (("weather") ("uncategorized") ("system") ("people") ("office") ("objects") ("nature") ("media") ("map") ("health") ("games") ("finance") ("editor") ("development") ("design") ("default" "all") ("communication") ("commerce") ("brand") ("arrows") ("all" "arrows" "brand" "commerce" "communication" "design" "development" "editor" "finance" "games" "health" "map" "media" "nature" "objects" "office" "people" "system" "uncategorized" "weather"))))))

(define-public crate-phosphor-leptos-0.4.0 (c (n "phosphor-leptos") (v "0.4.0") (d (list (d (n "leptos") (r "^0.6") (d #t) (k 0)))) (h "1pny8nhn8bzy7c5vsa996zqwqldjma7m0s5arp24cvhg3gy74sx7") (f (quote (("weather") ("uncategorized") ("system") ("people") ("office") ("objects") ("nature") ("media") ("map") ("health") ("games") ("finance") ("editor") ("development") ("design") ("default" "all") ("communication") ("commerce") ("brand") ("arrows") ("all" "arrows" "brand" "commerce" "communication" "design" "development" "editor" "finance" "games" "health" "map" "media" "nature" "objects" "office" "people" "system" "uncategorized" "weather"))))))

(define-public crate-phosphor-leptos-0.5.0 (c (n "phosphor-leptos") (v "0.5.0") (d (list (d (n "leptos") (r "^0.6") (d #t) (k 0)))) (h "15ywx0zsixf0g1llwhrwb4j1cmj25ms4fb8a46ffvyq7zh4kq290") (f (quote (("weather") ("uncategorized") ("system") ("people") ("office") ("objects") ("nature") ("media") ("map") ("health") ("games") ("finance") ("editor") ("development") ("design") ("default" "all") ("communication") ("commerce") ("brand") ("arrows") ("all" "arrows" "brand" "commerce" "communication" "design" "development" "editor" "finance" "games" "health" "map" "media" "nature" "objects" "office" "people" "system" "uncategorized" "weather"))))))

