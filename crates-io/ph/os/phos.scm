(define-module (crates-io ph os phos) #:use-module (crates-io))

(define-public crate-phos-0.0.1 (c (n "phos") (v "0.0.1") (h "0dlfb66fszjjvwzp0ivlh1q5yr0k25f7hjvwbdl13j6lmnbdi5jb")))

(define-public crate-phos-0.0.2 (c (n "phos") (v "0.0.2") (h "0w3yb8sss4ja1f7bfhqymz24cz9dwx6fl1zzzcgzgl1zic8a2bav")))

