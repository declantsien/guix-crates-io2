(define-module (crates-io ph os phosphor-svgs) #:use-module (crates-io))

(define-public crate-phosphor-svgs-0.1.0 (c (n "phosphor-svgs") (v "0.1.0") (h "1phn27m68y83f71dlp0ckch3d8hc6gh2miqxk0ac9k5gv0vsdhv9")))

(define-public crate-phosphor-svgs-0.2.0 (c (n "phosphor-svgs") (v "0.2.0") (h "02jh8r102w2rbi612w8i2a8b9fv8qh2kb7cmi3v1i9a8gy52njny")))

