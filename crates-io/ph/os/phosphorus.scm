(define-module (crates-io ph os phosphorus) #:use-module (crates-io))

(define-public crate-phosphorus-0.0.0 (c (n "phosphorus") (v "0.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "khronos_api") (r "^3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0ldkbszs2lbmlx0qzprw1jilg2k069qkmh7gy7j70v8gai5y5wc2") (y #t)))

(define-public crate-phosphorus-0.0.1 (c (n "phosphorus") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "khronos_api") (r "^3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1kz74h8wm62al67454hajs0nv2d60rnl2yw6l74gq1fyz5rgjjrg")))

(define-public crate-phosphorus-0.0.2 (c (n "phosphorus") (v "0.0.2") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "khronos_api") (r "^3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1n0alh7yzvhbdvb9szl9mig8rhx30fzzdcrma77bs84acr9v5dfm")))

(define-public crate-phosphorus-0.0.3 (c (n "phosphorus") (v "0.0.3") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "khronos_api") (r "^3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0kpf9ciwql2z0hqbm5711sq64y80d3aq1nj7kmb4dxd8lsm17pq0")))

(define-public crate-phosphorus-0.0.4 (c (n "phosphorus") (v "0.0.4") (d (list (d (n "chlorine") (r "^1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)))) (h "0pzmxqw51a43r5265df6rsyx7l1xs31k1ww079d68pd8wc95p2s8") (f (quote (("trace_calls") ("struct_loader") ("global_loader") ("error_checks") ("default" "global_loader" "struct_loader" "trace_calls" "error_checks"))))))

(define-public crate-phosphorus-0.0.5 (c (n "phosphorus") (v "0.0.5") (d (list (d (n "magnesium") (r "^0.0.4") (d #t) (k 0)))) (h "151fmh3b91dlbhzl9q7zjj7qmsi5vb8sa8jq0lni7wnm8szfpyqj")))

(define-public crate-phosphorus-0.0.6 (c (n "phosphorus") (v "0.0.6") (d (list (d (n "magnesium") (r "^0.0.4") (d #t) (k 0)))) (h "059l0xdz7k6vgks75vymiwlzzjndafiqxcmisk4mb53382a80gbr")))

(define-public crate-phosphorus-0.0.7 (c (n "phosphorus") (v "0.0.7") (d (list (d (n "magnesium") (r "^0.0.4") (d #t) (k 0)))) (h "0s9cbnrr0v18wi56dd968dspf8xilj0gppqhmyqk8pihvaxdqcbk")))

(define-public crate-phosphorus-0.0.8 (c (n "phosphorus") (v "0.0.8") (d (list (d (n "magnesium") (r "^0.0.4") (d #t) (k 0)))) (h "0z392j2xx808sknsai22z2yk93kapp2fvacs7d7ll52xi670qw9p")))

(define-public crate-phosphorus-0.0.9 (c (n "phosphorus") (v "0.0.9") (d (list (d (n "magnesium") (r "^0.0.4") (d #t) (k 0)))) (h "01z59z4m58v1yw0rpx33j3f026dqxljlmz1lkcgb2wz0kjdahzah")))

(define-public crate-phosphorus-0.0.10 (c (n "phosphorus") (v "0.0.10") (d (list (d (n "magnesium") (r "^0.0.4") (d #t) (k 0)))) (h "11qzmmqyd7cnl5dsyd2rx2frk8vd60747d5xn5drx34zy54chmwb")))

(define-public crate-phosphorus-0.0.11 (c (n "phosphorus") (v "0.0.11") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "0kgs66wkc3amnmbsil7x354clgq1xiv8267bg8s2lgvjvqhs70qs")))

(define-public crate-phosphorus-0.0.12 (c (n "phosphorus") (v "0.0.12") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "0241g5vzfkcbrpahlpsrwj008zlh722m98hs92rzxal13xzdj420")))

(define-public crate-phosphorus-0.0.13 (c (n "phosphorus") (v "0.0.13") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "1aw8rzip18ba9qcqy2fr1jhv5n4f6c44by8nqz4mcnz3cjm62cd9")))

(define-public crate-phosphorus-0.0.14 (c (n "phosphorus") (v "0.0.14") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "0hb8rrzwpvrz2zzxrnypg153iwjiiavcqynq2mnqzhysxnpga5wh")))

(define-public crate-phosphorus-0.0.15 (c (n "phosphorus") (v "0.0.15") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "1lgy49dpk4qmzhws6kr03p4rj2zw3phvjbzwq9sap5mmay5kzl3b")))

(define-public crate-phosphorus-0.0.16 (c (n "phosphorus") (v "0.0.16") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "1n0j0sc49h0mjp0jdmiz0cck2b3z04f9gjhzm7mgkv58nrrc0r4g")))

(define-public crate-phosphorus-0.0.17 (c (n "phosphorus") (v "0.0.17") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "05w9m1flla3kvdd9yk7wwjp7h1izq3fj797fblf3haaryf4azmcy")))

(define-public crate-phosphorus-0.0.18 (c (n "phosphorus") (v "0.0.18") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "1wzjb69zl1jprrlls52bgg98zmjaw4f4ivrkpwa0lgdw706a84nb")))

(define-public crate-phosphorus-0.0.19 (c (n "phosphorus") (v "0.0.19") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "0ymva54pfh828lskpi6bbpxz5w8pvbdfmz8v4mk0dhvmwy907801")))

(define-public crate-phosphorus-0.0.20 (c (n "phosphorus") (v "0.0.20") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "1iz96issswxf8lpkd4br0c65fbv6lj05s1hpnvcr1gnzplvzf32n")))

(define-public crate-phosphorus-0.0.21 (c (n "phosphorus") (v "0.0.21") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "1n62m1x9vgxdbrsf7zmjgm3h1pyw72j649w2q60i2k638i2gflh4")))

(define-public crate-phosphorus-0.0.22 (c (n "phosphorus") (v "0.0.22") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "0qcfyi8k7p7pv1nmi9vgzfbiwjp2bm17qxkmhdr0sj8h2vkgnj8j")))

(define-public crate-phosphorus-0.0.23 (c (n "phosphorus") (v "0.0.23") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "06mnwrbirsvfys1plc5baf0svjgjxz84dw77ijknrwh6k18j9s69")))

(define-public crate-phosphorus-0.0.24 (c (n "phosphorus") (v "0.0.24") (d (list (d (n "magnesium") (r "^1") (d #t) (k 0)))) (h "0lc2bak6k0jw8x5zn4dskcndhvfmcaxc2l8igf1wzbj39pjb0j7a")))

(define-public crate-phosphorus-1.0.0 (c (n "phosphorus") (v "1.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magnesium") (r "^1.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1xxll4v9qb0nmiz1f9pjb8c8z86apq78s3gs2iniw85zxwh9q68s")))

(define-public crate-phosphorus-2.0.0 (c (n "phosphorus") (v "2.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magnesium") (r "^1.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0iszbgmg7g5gz05hqbf21hpan0099xw2fqjizapgkiz4wv98xqyi")))

(define-public crate-phosphorus-2.0.1 (c (n "phosphorus") (v "2.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magnesium") (r "^1.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1mdfm223i8jz5cm1kkkmknl6fwx33qq3m891y9zsr5dccz0mac0d")))

(define-public crate-phosphorus-2.0.2 (c (n "phosphorus") (v "2.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magnesium") (r "^1.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0finxkmfmi5cngrxwfq5x7d6kp6fx0sbgla38i3036nvifwd0h2c")))

(define-public crate-phosphorus-2.0.3 (c (n "phosphorus") (v "2.0.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magnesium") (r "^1.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "01b5f9fqpbl14r67qnzdfp4310lrr04d05jy374f518p0bkiv9md")))

