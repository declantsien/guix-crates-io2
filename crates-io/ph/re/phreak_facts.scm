(define-module (crates-io ph re phreak_facts) #:use-module (crates-io))

(define-public crate-phreak_facts-0.1.0 (c (n "phreak_facts") (v "0.1.0") (d (list (d (n "compact") (r "^0.2") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "055fw6zmyw54jb508b1b7kpqi4a7qc2zwnazpqv1mxkbcfr6009n")))

(define-public crate-phreak_facts-0.1.1 (c (n "phreak_facts") (v "0.1.1") (d (list (d (n "compact") (r "^0.2") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "115msjm2w0kxia9an41zfvqljzbf6c0y99vqlflvbd35rgc6cb9s")))

(define-public crate-phreak_facts-0.1.2 (c (n "phreak_facts") (v "0.1.2") (d (list (d (n "compact") (r "^0.2") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "1clawi2j9jj9h67fk87lra6sb1plb10slx54jibq20vrpqbgskqg")))

(define-public crate-phreak_facts-0.1.3 (c (n "phreak_facts") (v "0.1.3") (d (list (d (n "compact") (r "^0.2") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "1mbg63zx45c573nykkqg8m4ay3fw2b13spb21i82mqczmkhdbf2c")))

(define-public crate-phreak_facts-0.1.4 (c (n "phreak_facts") (v "0.1.4") (d (list (d (n "compact") (r "^0.2") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "0f54arq0jk238wdbj3xx6rp50yf5p8a1zbp7mj5rhb225r8s9jpd")))

(define-public crate-phreak_facts-0.1.5 (c (n "phreak_facts") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc" "derive"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1fcs7jkr45z82pgh0153bvsgrnvkk030nk996rnlq9fa6pqfjdj9")))

