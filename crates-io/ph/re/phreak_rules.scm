(define-module (crates-io ph re phreak_rules) #:use-module (crates-io))

(define-public crate-phreak_rules-0.1.0 (c (n "phreak_rules") (v "0.1.0") (h "0d568kgvv3asracr168345vysny2a7dvpyid28ma3hd64hhmv6xh")))

(define-public crate-phreak_rules-0.1.1 (c (n "phreak_rules") (v "0.1.1") (h "1y3ydvci36rh3kwzbxdqsq8giq9srqi3l6lqi29n147qyl4b65ij")))

(define-public crate-phreak_rules-0.1.2 (c (n "phreak_rules") (v "0.1.2") (h "15synh7aksr1d40zjrygpawgs5vzvq5r93h98fbfbydgsqjjk8kk")))

(define-public crate-phreak_rules-0.1.3 (c (n "phreak_rules") (v "0.1.3") (h "0kig6q0frm21ki3z0h01rpwg2vkggvylgqsf6xrl36y74pwfk3vz")))

(define-public crate-phreak_rules-0.1.4 (c (n "phreak_rules") (v "0.1.4") (h "0mzmy72xqamcd0iam6yxjiapia6ryzjqgxn9ylxr273s8f4vzb68")))

(define-public crate-phreak_rules-0.1.5 (c (n "phreak_rules") (v "0.1.5") (h "1m89vmpwl5nfwqhplcp3lam230lcarfl359hb5s67ygyddyaiwim")))

