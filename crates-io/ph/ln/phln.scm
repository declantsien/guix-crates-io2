(define-module (crates-io ph ln phln) #:use-module (crates-io))

(define-public crate-phln-0.1.0 (c (n "phln") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bxcan") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (k 0)))) (h "1ikxl0ddb0bk91w1c8149wvw9xvkk60n11415a8vqwcbpzb8qlj2")))

