(define-module (crates-io ph y_ phy_numerical) #:use-module (crates-io))

(define-public crate-phy_numerical-0.1.0 (c (n "phy_numerical") (v "0.1.0") (h "0f7xa6v8zhmfq5dgj3sp62ip9s9ml3j6mffxn3rziv5brdxn9a53")))

(define-public crate-phy_numerical-0.1.1 (c (n "phy_numerical") (v "0.1.1") (h "0kl9663072wvhpwy2k16wxlmaav464i9l1hy3p7rcdr6xphvsa3c")))

(define-public crate-phy_numerical-0.1.2 (c (n "phy_numerical") (v "0.1.2") (h "0kfzk9bkbvlspqqfvsapknivw656bdrga88n1lr9lyr5dw4qv8cg")))

(define-public crate-phy_numerical-0.1.3 (c (n "phy_numerical") (v "0.1.3") (h "15fs4jcclqig2649rnpvclvxc2g096fdsaw9h3186hv19ahyli3v")))

(define-public crate-phy_numerical-0.1.4 (c (n "phy_numerical") (v "0.1.4") (h "1a3yq04yqzfji6vlpr0a3s1zbkhxr6hpg1bcgarbhw4kfk85bd9z")))

