(define-module (crates-io ph ar pharos) #:use-module (crates-io))

(define-public crate-pharos-0.1.0 (c (n "pharos") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)))) (h "1d6bkms5mfwkm92brgh8i1va5217zpi0nhn5js8n9dza1s8adig8")))

(define-public crate-pharos-0.1.1 (c (n "pharos") (v "0.1.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)))) (h "1l1wgkcc4ycmgwr4y1ppqikbsqyvajx3m249v0pg0ga7qh97p482")))

(define-public crate-pharos-0.2.0 (c (n "pharos") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "11bza58qxv6rmlz241i07bpilasjsdvmvzvmvz5yrja6rp5x5a2a")))

(define-public crate-pharos-0.2.1 (c (n "pharos") (v "0.2.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "1a5ivc0lxnmvv8x8a953br941ba3481x98bcniwzi2v1xqqxz664")))

(define-public crate-pharos-0.2.2 (c (n "pharos") (v "0.2.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha") (d #t) (k 0)))) (h "0g5mkarkv5jzpclg67gavhbdbdbm76xiqa60vyrvd0m0g5p5m9nx") (f (quote (("external_doc"))))))

(define-public crate-pharos-0.3.0 (c (n "pharos") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0-beta") (d #t) (k 0)))) (h "04hxj7sxcajm7qcalfkra2pdqx858mxhj5fiwbg51ri3jyal0q58") (f (quote (("external_doc")))) (y #t)))

(define-public crate-pharos-0.3.1 (c (n "pharos") (v "0.3.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0-beta") (d #t) (k 0)))) (h "16k7kaafw1km5lblwaxk8nn78xblzib3iv32k58yypjavmpv82yd") (f (quote (("external_doc"))))))

(define-public crate-pharos-0.3.2 (c (n "pharos") (v "0.3.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0-beta") (d #t) (k 0)))) (h "1cf4fi197c8j73467qakzbjd1krwn4x2km8hva3jiz8z52d8aswp") (f (quote (("external_doc"))))))

(define-public crate-pharos-0.4.0 (c (n "pharos") (v "0.4.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha") (f (quote ("async-await"))) (d #t) (k 0)))) (h "0iqa43y4xcr0vdskcnn91yfnjnqs0dhz8kqhgycx4502pbmd7gvb") (f (quote (("external_doc"))))))

(define-public crate-pharos-0.4.1 (c (n "pharos") (v "0.4.1") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xajjar5q0lmfp676ljja4wyq8r8s848l7a344765v5fmyhmhkws") (f (quote (("external_doc"))))))

(define-public crate-pharos-0.4.2 (c (n "pharos") (v "0.4.2") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)))) (h "1gmid60pc327z3m8r3sja6jyk0450xy2a89z72wvnzgc80syg25r") (f (quote (("external_doc"))))))

(define-public crate-pharos-0.5.0 (c (n "pharos") (v "0.5.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async_executors") (r "^0.4") (f (quote ("async_std"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "098ppf6p1jjyc8k4yd0nm8z3jzdrmn700r7mzc1gzb11vx4wyi10")))

(define-public crate-pharos-0.5.1 (c (n "pharos") (v "0.5.1") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async_executors") (r "^0.4") (f (quote ("async_std"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "05r947rlrfihm8yx0cladk6fqin2p1zzwza86rsj5jykqffg1r9j")))

(define-public crate-pharos-0.5.2 (c (n "pharos") (v "0.5.2") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async_executors") (r "^0.4") (f (quote ("async_std"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "149mkjxhc66nlxs5p3l7h14j4by1nsn85jafm7mzallmphp4np13")))

(define-public crate-pharos-0.5.3 (c (n "pharos") (v "0.5.3") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async_executors") (r "^0.4") (f (quote ("async_std"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "055lg1dzrxnryfy34a9cyrg21b7cl6l2frfx2p7fdvkz864p6mp9")))

