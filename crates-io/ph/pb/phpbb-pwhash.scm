(define-module (crates-io ph pb phpbb-pwhash) #:use-module (crates-io))

(define-public crate-phpbb-pwhash-0.1.0 (c (n "phpbb-pwhash") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)))) (h "02zd2zk28c3x4cm4d9yy7xjfzkxaiqb7kg2jc00s2i021q3s6nvm")))

(define-public crate-phpbb-pwhash-0.1.1 (c (n "phpbb-pwhash") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)))) (h "09zivc3h0g4k4qyj1vrgks1gab41vwchls2fa39fmqikhq7rfsh5")))

