(define-module (crates-io ph tm phtm) #:use-module (crates-io))

(define-public crate-phtm-1.0.0 (c (n "phtm") (v "1.0.0") (h "1w05wxfx6aq2j5k81x60ngdz8ikkhz5amj3wlx1s536mf2qv0r08") (y #t)))

(define-public crate-phtm-1.0.1 (c (n "phtm") (v "1.0.1") (h "159i99yp0d69j3xwr7ppx9xbmmjz2mpnvdc6j1d7096arkh58yjr") (y #t)))

(define-public crate-phtm-1.0.2 (c (n "phtm") (v "1.0.2") (h "0plv23miv792cla9hsb9ng4kx5xfc54x639p76hh7fvf38yjx1cx") (y #t)))

(define-public crate-phtm-1.0.3 (c (n "phtm") (v "1.0.3") (h "0vdmdywnckpmbaxhc9friypxh3rld2l2jz9mvyrdfx0g46ngwwmj") (y #t)))

(define-public crate-phtm-2.0.0 (c (n "phtm") (v "2.0.0") (h "0hmpxnx5ys8gk9xzsjlwihnrs6l08l07dsc2my6f18nzr3pgz023")))

(define-public crate-phtm-2.0.1 (c (n "phtm") (v "2.0.1") (h "1rp02hnkljl64rdscsjikysgmzzfw9qvcky8l32f1v1crrngnvyw")))

