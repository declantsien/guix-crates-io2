(define-module (crates-io ph as phastft) #:use-module (crates-io))

(define-public crate-phastft-0.1.0 (c (n "phastft") (v "0.1.0") (h "09wc77hvrp85nbx7rvcv2rda1sgifgajha387a7rca0cgl0ishg1")))

(define-public crate-phastft-0.1.1 (c (n "phastft") (v "0.1.1") (h "1hwwn55swc5hapz748hxj9crsivjwcgiwc2y7qw6ndpziwyhlz18")))

(define-public crate-phastft-0.2.0 (c (n "phastft") (v "0.2.0") (d (list (d (n "fftw") (r "^0.8.0") (d #t) (k 2)) (d (n "multiversion") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1rwy3cp7qyw46wm75395nshxa8baf7yf2sm53x25f0himh8g75fj")))

(define-public crate-phastft-0.2.1 (c (n "phastft") (v "0.2.1") (d (list (d (n "fftw") (r "^0.8.0") (d #t) (k 2)) (d (n "multiversion") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0fndag9jkyalawsfcbv2hyw6q03smgg7lqrs3p1q780af2aq3pfi")))

