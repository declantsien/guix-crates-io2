(define-module (crates-io ph as phase-protocol-sdk) #:use-module (crates-io))

(define-public crate-phase-protocol-sdk-0.1.0 (c (n "phase-protocol-sdk") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0vwa85fq4kzh86k03i5gl0skcg0ww9l5f6nkr3h5bsd38rmag9sw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-phase-protocol-sdk-0.2.0 (c (n "phase-protocol-sdk") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0rb0cqvy2m1h9lwyzyl368zqjmhwxkhpwwnhz8r4hh0yv4075w9j") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-phase-protocol-sdk-0.3.0 (c (n "phase-protocol-sdk") (v "0.3.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0anyh5n1k5w3ip234l6pdfpn1lgxx0c4d17nhdzbla2rlp8hfns7") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

