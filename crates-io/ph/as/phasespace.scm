(define-module (crates-io ph as phasespace) #:use-module (crates-io))

(define-public crate-phasespace-0.0.1 (c (n "phasespace") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1782rrh1i9s3zfkfhi3ks8fsafiwj8f6dc3d6x1izp4lv4khc608")))

