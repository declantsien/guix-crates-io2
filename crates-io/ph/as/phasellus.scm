(define-module (crates-io ph as phasellus) #:use-module (crates-io))

(define-public crate-phasellus-0.1.0 (c (n "phasellus") (v "0.1.0") (d (list (d (n "calamine") (r "^0.18.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "08m0wa2bp9fpr8n3nhvynifyif9j3l7xc114kfz1gndlc9ilcfns")))

(define-public crate-phasellus-0.2.0 (c (n "phasellus") (v "0.2.0") (d (list (d (n "rustyline") (r "^10.0.0") (f (quote ("signal-hook"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "18jickckaf92zxy2s1vbv8svzrh2yy0r4kxglis0rzjml5n16b70") (y #t)))

(define-public crate-phasellus-0.3.0 (c (n "phasellus") (v "0.3.0") (d (list (d (n "rustyline") (r "^11.0") (f (quote ("signal-hook"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1igk6qv2dcgxd81ix86jsnyfv7dfn39v6jb31iy4rv1slac52cwf")))

(define-public crate-phasellus-0.4.0 (c (n "phasellus") (v "0.4.0") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)))) (h "0wxb0284zqqzdr1z60x185v21r61dyqpr1iirni6j06kqf8v6zyf") (y #t)))

(define-public crate-phasellus-0.4.1 (c (n "phasellus") (v "0.4.1") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0a1s001cayviar7m4c67kisyd4pgif1v0v671197dbd556gx3wlm") (y #t)))

(define-public crate-phasellus-0.4.2 (c (n "phasellus") (v "0.4.2") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0lk5rry1prlsy313ba3mb07zhb8slshfl8r5fxjwqiaf2in5lg78")))

(define-public crate-phasellus-0.4.4 (c (n "phasellus") (v "0.4.4") (d (list (d (n "cursive") (r "^0.20") (f (quote ("crossterm-backend"))) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "11kglgapx4zhyq6bdxg1sbpccgabx2ahh23iy13ss29gnfn8nn95")))

