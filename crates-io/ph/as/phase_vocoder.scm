(define-module (crates-io ph as phase_vocoder) #:use-module (crates-io))

(define-public crate-phase_vocoder-0.1.0 (c (n "phase_vocoder") (v "0.1.0") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "0xp43g773bhbp64g2bm0x17y8jl903kqqfpaalg5qv7gj9fbsj1n")))

