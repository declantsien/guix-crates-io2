(define-module (crates-io ph as phase) #:use-module (crates-io))

(define-public crate-phase-0.0.0 (c (n "phase") (v "0.0.0") (d (list (d (n "arctk") (r "2.0.*") (d #t) (k 0)) (d (n "nalgebra") (r "0.32.*") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0p14i47b6mdm39imx6rqw1vxdcnijx1hvayhbzkkf9x4r6zrw644")))

