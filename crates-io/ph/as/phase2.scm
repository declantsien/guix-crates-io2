(define-module (crates-io ph as phase2) #:use-module (crates-io))

(define-public crate-phase2-0.0.1 (c (n "phase2") (v "0.0.1") (d (list (d (n "bellman") (r "^0.1") (d #t) (k 0)) (d (n "pairing") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0fjjf75cz9qi4pg28pk8bsv2nm97x3wgr7c2yq6vgk3zlrb4fxh3")))

(define-public crate-phase2-0.1.0 (c (n "phase2") (v "0.1.0") (d (list (d (n "bellman") (r "^0.1") (d #t) (k 0)) (d (n "blake2") (r "^0.6.1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.8.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "pairing") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "0zwyhyvdybzcfz7r6d9h9aawflp6qswqbgz0yaphdas5n39pwxgw")))

(define-public crate-phase2-0.2.0 (c (n "phase2") (v "0.2.0") (d (list (d (n "bellman") (r "^0.1") (d #t) (k 0)) (d (n "blake2") (r "^0.6.1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.8.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "pairing") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "1jk8s7wxrpi4dwbmdalfm4xq8bd39cb58kjpzk24cs9fdv81hyq2")))

(define-public crate-phase2-0.2.1 (c (n "phase2") (v "0.2.1") (d (list (d (n "bellman") (r "^0.1") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "pairing") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "10yxmqixzrqb2l85d3gkf2kcw94ry781ricrl0nx6hqq3b3sgxbc")))

(define-public crate-phase2-0.2.2 (c (n "phase2") (v "0.2.2") (d (list (d (n "bellman") (r "^0.1") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "pairing") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0xqff3alscn3fjw6rw01f38qmrf3k3s1vkf1pggz0qbfbm8fnv30")))

