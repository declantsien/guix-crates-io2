(define-module (crates-io ph ab phab-grpc) #:use-module (crates-io))

(define-public crate-phab-grpc-0.0.1 (c (n "phab-grpc") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "built") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "06jz2aijnqwfs9skjivhbcfwkgj2d688y901fixabzf4l7nsx6ig")))

