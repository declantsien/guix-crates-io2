(define-module (crates-io ph ab phabricator-oauth) #:use-module (crates-io))

(define-public crate-phabricator-oauth-0.1.0 (c (n "phabricator-oauth") (v "0.1.0") (d (list (d (n "oauth2") (r "^4.0.0-alpha.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16s68csy5szw7wlnrwg49cqyacfq1vdzxhm7hfmi5jyw5z7ifjpl")))

(define-public crate-phabricator-oauth-0.1.1 (c (n "phabricator-oauth") (v "0.1.1") (d (list (d (n "oauth2") (r "^4.0.0-alpha.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z04b5lvcbhrhdkhplpmb8v9hkwd17n50sv5wvs6d77al29qkwpj")))

(define-public crate-phabricator-oauth-0.1.2 (c (n "phabricator-oauth") (v "0.1.2") (d (list (d (n "oauth2") (r "^4.0.0-alpha.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03n0r2hr1r3dd4sj4pcxcc3idyyrxr4zzr45xc5gvmkid9hlddlp")))

(define-public crate-phabricator-oauth-0.1.3 (c (n "phabricator-oauth") (v "0.1.3") (d (list (d (n "oauth2") (r "=4.0.0-alpha.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bqr11yj2yibcg0nsva8ha28g4nw8ifsmpm28bjadmg17z68ay7d")))

(define-public crate-phabricator-oauth-0.1.4 (c (n "phabricator-oauth") (v "0.1.4") (d (list (d (n "oauth2") (r "=4.0.0-alpha.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a8xh7zvvr7p3cj38ips0c1992canwblz3kii4ylscjx3s0fa4l5")))

(define-public crate-phabricator-oauth-0.1.5 (c (n "phabricator-oauth") (v "0.1.5") (d (list (d (n "oauth2") (r "^4") (f (quote ("reqwest"))) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05i9gnd9c7gyzpsl4m4wmxnlmz7wf7zh06hibv6v3p790mj6lwbd")))

(define-public crate-phabricator-oauth-0.1.6 (c (n "phabricator-oauth") (v "0.1.6") (d (list (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00l6a3b0fqx0l7s4wdrazw103jgww1fwd8y6vwmgvqii98kvajfn")))

(define-public crate-phabricator-oauth-0.1.7 (c (n "phabricator-oauth") (v "0.1.7") (d (list (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y28874nkk9nyhmslfm2388vhg0ycbh4v0xladwy3mcdxid2rfkn")))

(define-public crate-phabricator-oauth-0.1.8 (c (n "phabricator-oauth") (v "0.1.8") (d (list (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13j7p1wqfzsdnjrj7xnq93gyjqxpv6ybsq7haybl1rkvdjlg4b90")))

