(define-module (crates-io ph ot photon-geocoding) #:use-module (crates-io))

(define-public crate-photon-geocoding-1.0.0 (c (n "photon-geocoding") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sn54gbk052rk9dy5wm29288fy2s00y44614y5q1cvwv2ddn3205") (r "1.64")))

(define-public crate-photon-geocoding-1.0.1 (c (n "photon-geocoding") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f1fbkzrc9qvscv9rji83wrl3mc0g8nviinq4c50hm1356igszl8") (r "1.64")))

(define-public crate-photon-geocoding-1.1.0 (c (n "photon-geocoding") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (f (quote ("json"))) (d #t) (k 0)))) (h "0kzyqk6bshcd34bdka6fd2q45llppifrihpxr8scyr39s2373xaw") (r "1.64")))

(define-public crate-photon-geocoding-1.1.1 (c (n "photon-geocoding") (v "1.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0viwv5salax1d4z88rrhldljn82r2ix7bjxnq49ah9r5d4pcmvpy") (r "1.64")))

