(define-module (crates-io ph ot photo) #:use-module (crates-io))

(define-public crate-photo-0.1.0 (c (n "photo") (v "0.1.0") (h "0as7mbjnp1h7apjlvjrynl0xdrzcsn9limpn6abynk965gw5nvf5")))

(define-public crate-photo-0.1.1 (c (n "photo") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (k 0)) (d (n "wgpu") (r "^0.18") (d #t) (k 0)) (d (n "winit") (r "^0.29") (f (quote ("rwh_05"))) (d #t) (k 0)))) (h "1iqgy1icjzi2anxzb0kk26nkwifqz2c70mqs3y1hg5wrfc6h6ikd")))

(define-public crate-photo-1.0.0 (c (n "photo") (v "1.0.0") (d (list (d (n "enterpolation") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "0d87l12vz4ca7qpdblnhzwi6q88nqdn91qfnxvmrsqp3hf9znqkf")))

