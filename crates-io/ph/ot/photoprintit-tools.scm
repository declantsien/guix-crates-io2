(define-module (crates-io ph ot photoprintit-tools) #:use-module (crates-io))

(define-public crate-photoprintit-tools-0.1.0 (c (n "photoprintit-tools") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "13zhpidwnsxj7csvaixmfhp16v7mcd0qa712imcr8dz2asl1g6hh")))

