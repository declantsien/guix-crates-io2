(define-module (crates-io ph ot photonio-base) #:use-module (crates-io))

(define-public crate-photonio-base-0.0.2 (c (n "photonio-base") (v "0.0.2") (h "1b74hi5ym9pca1lr0hk7n6lbblabss9pj4my67s4zbs2ann9d32g")))

(define-public crate-photonio-base-0.0.4 (c (n "photonio-base") (v "0.0.4") (h "0yxm2rxaqj6jqkygvfvzflwkx80064lz5ycc7384wqwlazjhy669")))

(define-public crate-photonio-base-0.0.5 (c (n "photonio-base") (v "0.0.5") (h "175drdlwhiyj7nr7j3s7bcsdw8sg8asj31awwlizx3ikzk9csx2v")))

