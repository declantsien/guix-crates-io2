(define-module (crates-io ph ot photo_organizer) #:use-module (crates-io))

(define-public crate-photo_organizer-0.2.0 (c (n "photo_organizer") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color" "vec_map"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.4") (d #t) (k 0)) (d (n "mkdirp") (r "^1.0.0") (d #t) (k 0)))) (h "09f0ynrdxjads9j3kldw2hzqf2jdq22vgksy89387gdiqh33mx3d")))

(define-public crate-photo_organizer-0.2.1 (c (n "photo_organizer") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color" "vec_map"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.4") (d #t) (k 0)) (d (n "mkdirp") (r "^1.0.0") (d #t) (k 0)))) (h "1adxjg2vj7jjj5v3g0xikn8m9aapd65lwmp9b61c90y09w3pjqc6")))

(define-public crate-photo_organizer-0.2.2 (c (n "photo_organizer") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color" "vec_map"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.4") (d #t) (k 0)) (d (n "mkdirp") (r "^1.0.0") (d #t) (k 0)))) (h "07hz2j1sgbd78sjv9rj3mzxkqp165d1i94afnz1klf71d9r4nbgw")))

(define-public crate-photo_organizer-0.3.0 (c (n "photo_organizer") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color" "vec_map"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.4") (d #t) (k 0)) (d (n "mkdirp") (r "^1.0.0") (d #t) (k 0)))) (h "0r4mvv8s9a0jkqda8qlhxb78107qlaabr97ayjv7s0g0rsqi8dzg")))

