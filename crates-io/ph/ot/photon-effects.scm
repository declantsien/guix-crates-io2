(define-module (crates-io ph ot photon-effects) #:use-module (crates-io))

(define-public crate-photon-effects-0.1.0 (c (n "photon-effects") (v "0.1.0") (d (list (d (n "image") (r "^0.23.12") (f (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (k 0)) (d (n "photon-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.2.23") (d #t) (k 0)))) (h "10ayx33x8wd67x53f3j3wd7n1q0sj9j89d1jdqf6qlk3zwkzwcgv")))

(define-public crate-photon-effects-1.0.0 (c (n "photon-effects") (v "1.0.0") (d (list (d (n "image") (r "^0.23.12") (f (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (k 0)) (d (n "photon-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.2.23") (d #t) (k 0)))) (h "0rxpq781jgmh3qpkapggfddfcwws0r2l14llfwj24xfkww6lylhp")))

