(define-module (crates-io ph ot photon_garden_obsidian) #:use-module (crates-io))

(define-public crate-photon_garden_obsidian-0.1.0 (c (n "photon_garden_obsidian") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0i9sh9d6lxy4039345v4a0yklgip2bw6kd1gm391mswz72djh4y7")))

