(define-module (crates-io ph ot photonio-macros) #:use-module (crates-io))

(define-public crate-photonio-macros-0.0.3 (c (n "photonio-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "151ism0spg528xv6zkagnjs9rnmijhvir3c0748zax2ph0w9skr3")))

(define-public crate-photonio-macros-0.0.4 (c (n "photonio-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02hhvypm2maf57j7cq2hsws71zpkq73xq7pxlrrlykfdj0mbb6z2")))

(define-public crate-photonio-macros-0.0.5 (c (n "photonio-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vlc287r120zac3dcc22592q78k6v9jgf7g88d96am82frn8hhhs")))

