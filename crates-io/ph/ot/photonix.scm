(define-module (crates-io ph ot photonix) #:use-module (crates-io))

(define-public crate-photonix-0.1.0 (c (n "photonix") (v "0.1.0") (d (list (d (n "photonix_derive") (r "^0.1.0") (d #t) (k 0)))) (h "19vpw3gfr76fl7ip40rv95v85w1bin06kd4ik4vhzskra6xhq824") (y #t)))

(define-public crate-photonix-0.1.1 (c (n "photonix") (v "0.1.1") (d (list (d (n "photonix_derive") (r "^0.1.1") (d #t) (k 0)))) (h "10m7kdvjiii1rmk7f4ancjhr87gbapbmlch02lsgncarn4r3pnd6")))

