(define-module (crates-io ph ot photo-captioner) #:use-module (crates-io))

(define-public crate-photo-captioner-0.1.0 (c (n "photo-captioner") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "cursive") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "18xw4v5nfv51jvng90j85s3nnys8yji8qg3f0zkbnn5fch0hfkbi")))

(define-public crate-photo-captioner-0.1.1 (c (n "photo-captioner") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "cursive") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1ipc8rs391c9g20vsjh0p78s83xkm2ddhnz1w6nkz13y597q7zjh")))

(define-public crate-photo-captioner-0.1.3 (c (n "photo-captioner") (v "0.1.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "cursive") (r "^0.16") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0jr2jmcn54ww6q5myyarq45kxaphvl9p800djcqdh4ja4rw59ylw") (f (quote (("termion-backend" "cursive/termion-backend") ("pancurses-backend" "cursive/pancurses-backend") ("ncurses-backend" "cursive/ncurses-backend") ("default" "ncurses-backend") ("crossterm-backend" "cursive/crossterm-backend") ("blt-backend" "cursive/blt-backend"))))))

