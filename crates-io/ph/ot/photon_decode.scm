(define-module (crates-io ph ot photon_decode) #:use-module (crates-io))

(define-public crate-photon_decode-0.1.0 (c (n "photon_decode") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1bdb9hc0riq1q4i03pdxalxp1avhvcps3avkk43rzscfsjrms9zv")))

(define-public crate-photon_decode-0.1.1 (c (n "photon_decode") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "0grlf3x7wa5m70n83yf3hznnqlynn7kjvx5kazwwsn22l5n6xk68")))

(define-public crate-photon_decode-0.2.0 (c (n "photon_decode") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "0711pcv9352pwj05066imx3iglrm1llrhyxl7ijiz1lzfm6pzwvj")))

(define-public crate-photon_decode-0.2.1 (c (n "photon_decode") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1wckf3v2f683wgmdyyfllg77564w3ydnbwd1d0bi03wkdvdcmbm2")))

