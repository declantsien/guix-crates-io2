(define-module (crates-io ph ot photonio-tokio) #:use-module (crates-io))

(define-public crate-photonio-tokio-0.0.2 (c (n "photonio-tokio") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "photonio-base") (r "^0.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1ivnahn2a2q4kh1zr5ar73wzj3qn7irwb609q3yij5isfc3ljms8")))

(define-public crate-photonio-tokio-0.0.4 (c (n "photonio-tokio") (v "0.0.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "photonio-base") (r "^0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "16pbj9nq2ny61ssnr342a5v7wc8lbdj8gq3n5ya25ffkp4zym22r")))

(define-public crate-photonio-tokio-0.0.5 (c (n "photonio-tokio") (v "0.0.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "photonio-base") (r "^0.0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1nfqj4scx8inbv8q5l1s287aw18w4lpyxazsif6i23amdaf8dvwg")))

