(define-module (crates-io ph ot photondb-engine) #:use-module (crates-io))

(define-public crate-photondb-engine-0.0.1 (c (n "photondb-engine") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bumpalo") (r "^3.10") (f (quote ("collections"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06ajdwf81h1dsgqshkb70kl8h5bicndlb0prhkay5zxdv40xc8a1")))

