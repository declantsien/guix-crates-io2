(define-module (crates-io ph ot photonix_derive) #:use-module (crates-io))

(define-public crate-photonix_derive-0.1.0 (c (n "photonix_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0i750f97p8d6if7lj5vchrdr9av4amfvx9w81izmzr85clisf6ky")))

(define-public crate-photonix_derive-0.1.1 (c (n "photonix_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1p8sh30d7rl713sf4v83a73w9h39xq58jv4d1slppja1fmnl7ckj")))

