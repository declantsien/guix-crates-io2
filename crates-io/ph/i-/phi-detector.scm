(define-module (crates-io ph i- phi-detector) #:use-module (crates-io))

(define-public crate-phi-detector-0.1.0 (c (n "phi-detector") (v "0.1.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("macros" "time" "rt-threaded"))) (d #t) (k 2)))) (h "1igbnsnn4l2dzpa4d7h4jd3ny7sf5cb2265pba7pkjyvyj2hysrw")))

(define-public crate-phi-detector-0.2.0 (c (n "phi-detector") (v "0.2.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("macros" "time" "rt-threaded"))) (d #t) (k 2)))) (h "0ifh8lbbvd6xx3myazs16n2sb1iyby15727dc5x6gjj8r78amx9w")))

(define-public crate-phi-detector-0.3.0 (c (n "phi-detector") (v "0.3.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("macros" "time" "rt-threaded"))) (d #t) (k 2)))) (h "0wnzrkw7xn6adq744qlx7xb1n2gq0v5hmhfh4nj2aalj0dxh5z1b")))

(define-public crate-phi-detector-0.4.0 (c (n "phi-detector") (v "0.4.0") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "17sa1cv5vmxj71njz7nkwvcksyhs4rr0lb8agbfzwg3xxzkpn8cz")))

