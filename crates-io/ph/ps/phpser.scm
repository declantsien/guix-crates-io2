(define-module (crates-io ph ps phpser) #:use-module (crates-io))

(define-public crate-phpser-0.0.0 (c (n "phpser") (v "0.0.0") (d (list (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "getset") (r "^0.1.0") (d #t) (k 0)))) (h "1zbwalgin9fcwb6yi3wy6n0b6s13n7f9dlxdllir9v1n05sl07a9")))

