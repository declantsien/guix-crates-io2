(define-module (crates-io ph az phazer) #:use-module (crates-io))

(define-public crate-phazer-0.1.0 (c (n "phazer") (v "0.1.0") (h "0qpn0rb6d5acalp55bvaxgc54mlyx3izpi0sw61hn7308fyihchy")))

(define-public crate-phazer-0.1.1 (c (n "phazer") (v "0.1.1") (h "09cv28135dri6vdsk5b4c8ilrq50wyxvq5kwndkqfyw0hc85frsg") (f (quote (("simple"))))))

(define-public crate-phazer-0.1.2 (c (n "phazer") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.22") (f (quote ("stream"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 2)))) (h "0n31j0l3cdxqb8366ldfyf3w8z2mqa55cvi36mn01pgc3rrc1g0y") (f (quote (("simple"))))))

