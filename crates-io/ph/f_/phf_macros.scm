(define-module (crates-io ph f_ phf_macros) #:use-module (crates-io))

(define-public crate-phf_macros-0.6.0 (c (n "phf_macros") (v "0.6.0") (d (list (d (n "phf_shared") (r "= 0.6.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1cz6rnpxjb34dvqrvkwwb8dppx1pbw6mn1wzcvybkb1m2l6y6ikk") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.1 (c (n "phf_macros") (v "0.6.1") (d (list (d (n "phf_shared") (r "= 0.6.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1vriyval2imwkd04p0abf5vrrvvrlkra5h1g9v79bh7p3iyqyh6i") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.2 (c (n "phf_macros") (v "0.6.2") (d (list (d (n "phf_shared") (r "= 0.6.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0g6npjj6hz9a0galpxkz6kfh6i7b2pnw95nh7p5sh4j6rdgfpdk8") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.3 (c (n "phf_macros") (v "0.6.3") (d (list (d (n "phf_shared") (r "= 0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1kdkcnzc9qwrm33yvmgnf1yvrxaaxipg4hzjk69mgsmz6mcp2k6w") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.4 (c (n "phf_macros") (v "0.6.4") (d (list (d (n "phf_shared") (r "= 0.6.4") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0z35k5l76y442wh5225a4sfmw1h7v4jcwivhkfkf00gi2csrwcpf") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.5 (c (n "phf_macros") (v "0.6.5") (d (list (d (n "phf_shared") (r "= 0.6.5") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1c2fyrqn4sv90a4qpbryvx5yqs0w1bfxk81css1qyzfmnadfpdrn") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.6 (c (n "phf_macros") (v "0.6.6") (d (list (d (n "phf_shared") (r "= 0.6.6") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0pln4ajjz4h4qgfc3fm706547fwqvga0ilp42ylfdychafy1p2kg") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.7 (c (n "phf_macros") (v "0.6.7") (d (list (d (n "phf_shared") (r "= 0.6.7") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "01ryx6bxyqygipy971lb0vyzydz0iih77x3851b59g7hljxrr20y") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.8 (c (n "phf_macros") (v "0.6.8") (d (list (d (n "phf_shared") (r "= 0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1rigpbx34arlh8z64ac47ywwjrldfpdw0g5mqi7asb8a2giknpx5") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.9 (c (n "phf_macros") (v "0.6.9") (d (list (d (n "phf_shared") (r "= 0.6.9") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1zm5lcwyjcwh6cxfqcjmk8q2sdmx6p9v4i37dvbi2ah7y0x5pknx") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.10 (c (n "phf_macros") (v "0.6.10") (d (list (d (n "phf") (r "= 0.6.10") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.10") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.10") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "02nzchrw9vpsqvjz780vg1sv5cxslcpkbd6hbs09bc3nnl16i6d1") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.11 (c (n "phf_macros") (v "0.6.11") (d (list (d (n "phf") (r "= 0.6.11") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.11") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.11") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1lqjidhmyl29yz6ra4ic266718ffhpkrradqfyydqahw9rfc1ng0") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.12 (c (n "phf_macros") (v "0.6.12") (d (list (d (n "phf") (r "= 0.6.12") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.12") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.12") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "02pybnbqvzhbqxik71rljqqxqj7znak1sjqq776p22gcsh7963na") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.13 (c (n "phf_macros") (v "0.6.13") (d (list (d (n "phf") (r "= 0.6.13") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.13") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.13") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0dxchs0mik7jg30bpyd9dz78i4hzwxa23khkhwq1zlpqn8dc0gzr") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.14 (c (n "phf_macros") (v "0.6.14") (d (list (d (n "phf") (r "= 0.6.14") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.14") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1jhddwfmfrmmdqsnrw1091h2c0p4c89frxnq803lzvn90345sfjb") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.15 (c (n "phf_macros") (v "0.6.15") (d (list (d (n "phf") (r "= 0.6.15") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.15") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.15") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "11k6hf6amzr5ffax3v86x3nk62jbhivv1l1kcrmyagvx7l6w7rrk") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.17 (c (n "phf_macros") (v "0.6.17") (d (list (d (n "phf") (r "= 0.6.17") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.17") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.17") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0vpn7zlm292pj9a7cly21q5b0nqlx54vlcalbbvagkzl8bxxbw67") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.18 (c (n "phf_macros") (v "0.6.18") (d (list (d (n "phf") (r "= 0.6.18") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.18") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.18") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1bg55v6zdxqqp5v9r25xyn7236mn990phk8j69q3khc0pjg5q8s7") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6.19 (c (n "phf_macros") (v "0.6.19") (d (list (d (n "phf") (r "= 0.6.19") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.19") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.19") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ch2djc3ld6zlgj16dzbm9w0bdl3pvp7bvf695nfz2j6fy6fr4n1") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.0 (c (n "phf_macros") (v "0.7.0") (d (list (d (n "phf") (r "= 0.7.0") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.0") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "13rkwdldnq41vqm2slascika7kj53za01fxv0hi79lgiwzzm34k8") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.1 (c (n "phf_macros") (v "0.7.1") (d (list (d (n "phf") (r "= 0.7.1") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.1") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1r2rpnbg4sh151rflgxp360h7zxcikjmw26m4p8gp4xsv7r6bkmh") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.2 (c (n "phf_macros") (v "0.7.2") (d (list (d (n "phf") (r "= 0.7.2") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.2") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1b8q8flac2b7z5fhqcqvlg12ldzipsp5jp0r9prh78ha4v3bzr8q") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.3 (c (n "phf_macros") (v "0.7.3") (d (list (d (n "phf") (r "= 0.7.3") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.3") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0v6mrg3fmfby1x4vbih6ivb80q96z0ilywz7i2cpvx87f5dggazc") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.4 (c (n "phf_macros") (v "0.7.4") (d (list (d (n "phf") (r "= 0.7.4") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.4") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "10d1kvr31sk98vj9l9xxzzbryhhfdci7i75b3dxj59xaw4j0ndh1") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.5 (c (n "phf_macros") (v "0.7.5") (d (list (d (n "phf") (r "= 0.7.5") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.5") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0dy3ix0fmsvny58hnpi4cs371p57yc25p6fk3inwrb6vjyqyp55d") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.6 (c (n "phf_macros") (v "0.7.6") (d (list (d (n "phf") (r "= 0.7.6") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.6") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1pp4gac2jpziqlsdn2cd21b1rl3yzcpjrvvb9nza3p5vi9fz55i6") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.7 (c (n "phf_macros") (v "0.7.7") (d (list (d (n "phf") (r "= 0.7.7") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.7") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.7") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1n0vr9sfiqzg091r6rlgycib4p3952vjhqsgqqfid2qgkbyw6v94") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.8 (c (n "phf_macros") (v "0.7.8") (d (list (d (n "phf") (r "= 0.7.8") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.8") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0cvr3jm14f80lnq6lvbqwgb6f6k7j84k57ad8paxwg2fr24kflfl") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.9 (c (n "phf_macros") (v "0.7.9") (d (list (d (n "phf") (r "= 0.7.9") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.9") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1n8087767kbv6sdcjw51qpxq5mgnnmrsa01xiih5a3grylgdy9cs") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.10 (c (n "phf_macros") (v "0.7.10") (d (list (d (n "phf") (r "= 0.7.10") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.10") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.10") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0p14sxz7z8jsjh5qsdqk0hznbp35q74i1llvgqxgwmvpi8r51hnb") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.11 (c (n "phf_macros") (v "0.7.11") (d (list (d (n "phf") (r "= 0.7.11") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.11") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.11") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hkh624a73d2sfcl07rxx5vbjxrw16g5cr86pm3n21bafgj1fv4a") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.12 (c (n "phf_macros") (v "0.7.12") (d (list (d (n "phf") (r "= 0.7.12") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.12") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.12") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ripj0cshjad7f9y3yxvgc3nw7yfzacd3d0n0mgwmaq8y1v7byvg") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.13 (c (n "phf_macros") (v "0.7.13") (d (list (d (n "phf") (r "= 0.7.13") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.13") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.13") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "194i4p1jbr0kbhnfrdpgqrml152kidlamcxba2ngy32bal9ma9pg") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.14 (c (n "phf_macros") (v "0.7.14") (d (list (d (n "phf") (r "= 0.7.14") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.14") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.14") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0c5hfrgv164kndr3d43dpfzy85jd68k6wg5i5wfj1426qhpwrx1y") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.15 (c (n "phf_macros") (v "0.7.15") (d (list (d (n "phf") (r "= 0.7.15") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.15") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.15") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0g8grvnw7wn1c3d9jlw5k50n757indi5bb8lnv2z4xp7jn9i73qy") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.16 (c (n "phf_macros") (v "0.7.16") (d (list (d (n "phf") (r "= 0.7.16") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.16") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.16") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0zp3zkx841wdkj5xjw862mgvv51a55if4m0fipcj0ib5xl7afbhk") (f (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7.17 (c (n "phf_macros") (v "0.7.17") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "= 0.7.17") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.17") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.17") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0csrwrvx91g3b5k3w8al64hlv1xivvclf7k1cxd0lk0jz2hjxqqw") (f (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats" "time"))))))

(define-public crate-phf_macros-0.7.18 (c (n "phf_macros") (v "0.7.18") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "= 0.7.18") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.18") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.18") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1apc1ryasg3qin0b6rymv940xk0lv1br9r60y53pdckpgnv1dzca") (f (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats" "time"))))))

(define-public crate-phf_macros-0.7.19 (c (n "phf_macros") (v "0.7.19") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "= 0.7.19") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.19") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.19") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0n3klbp267yciv88nv59y1c1fw0k5h9rmvaqivgjhkalvh95kg2b") (f (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats" "time"))))))

(define-public crate-phf_macros-0.7.20 (c (n "phf_macros") (v "0.7.20") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "= 0.7.20") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.20") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.20") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "15z4ca3wz5pmav0ipxc7fhjw0kwj1dh7f0vnvrv37ih3hw92r6v1") (f (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats" "time"))))))

(define-public crate-phf_macros-0.7.21 (c (n "phf_macros") (v "0.7.21") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "^0.7.21") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.21") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.21") (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0125fcyhczq27gvqfif05jnxhi58ijg9q294zgwcp29x5qvdmv80") (f (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats"))))))

(define-public crate-phf_macros-0.7.22 (c (n "phf_macros") (v "0.7.22") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "phf") (r "^0.7.22") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.22") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.22") (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1d833kl45w2rr02sxja5481srmavzdmmck1jw598br85cs2gwv6f") (f (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats"))))))

(define-public crate-phf_macros-0.7.23 (c (n "phf_macros") (v "0.7.23") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "phf") (r "^0.7.23") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.7.23") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.23") (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1mm6glranym3m0617f7j01igxcsvmp3nrqdra3c7kxjhd4nzja8i") (f (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats"))))))

(define-public crate-phf_macros-0.7.24 (c (n "phf_macros") (v "0.7.24") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "phf_generator") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7.24") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0dzylcy14ksy60h265l433j9ra8xhg8xlq3pd5qk658m6f1mxd5x")))

(define-public crate-phf_macros-0.8.0 (c (n "phf_macros") (v "0.8.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros" "unicase"))) (d #t) (k 2)) (d (n "phf_generator") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "unicase") (r "^2.4.0") (d #t) (k 2)))) (h "170qm6yqn6b9mjlwb2xmm3iad9d5nzwgfawfwy7zr7s2zwcdwvvz")))

(define-public crate-phf_macros-0.9.0 (c (n "phf_macros") (v "0.9.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros" "unicase"))) (d #t) (k 2)) (d (n "phf_generator") (r "^0.9.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.9.0") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "unicase_") (r "^2.4.0") (o #t) (d #t) (k 0) (p "unicase")) (d (n "unicase_") (r "^2.4.0") (d #t) (k 2) (p "unicase")))) (h "11mbi4x03gz7jnf4bg9fzxdxn6gd8ddkj29hms0dh3mmds9za1mp") (f (quote (("unicase" "unicase_" "phf_shared/unicase"))))))

(define-public crate-phf_macros-0.9.1 (c (n "phf_macros") (v "0.9.1") (d (list (d (n "phf") (r "^0.9") (f (quote ("macros" "unicase"))) (d #t) (k 2)) (d (n "phf_generator") (r "^0.9.1") (d #t) (k 0)) (d (n "phf_shared") (r "^0.9.0") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "unicase_") (r "^2.4.0") (o #t) (d #t) (k 0) (p "unicase")) (d (n "unicase_") (r "^2.4.0") (d #t) (k 2) (p "unicase")))) (h "1rpc0jy5sfrk3ir87k2q0kk44a45nsrbwc131jmsi6f8hi3fqi7g") (f (quote (("unicase" "unicase_" "phf_shared/unicase")))) (y #t)))

(define-public crate-phf_macros-0.10.0 (c (n "phf_macros") (v "0.10.0") (d (list (d (n "phf") (r "^0.9") (f (quote ("macros" "unicase"))) (d #t) (k 2)) (d (n "phf_generator") (r "^0.10.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.10.0") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "unicase_") (r "^2.4.0") (o #t) (d #t) (k 0) (p "unicase")) (d (n "unicase_") (r "^2.4.0") (d #t) (k 2) (p "unicase")))) (h "1q5ljwvb10dx188i6jxzckqfimjw5pm2p4kkvmhg2q6m9lcg7zaq") (f (quote (("unicase" "unicase_" "phf_shared/unicase"))))))

(define-public crate-phf_macros-0.11.0 (c (n "phf_macros") (v "0.11.0") (d (list (d (n "phf_generator") (r "^0.11.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.0") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicase_") (r "^2.4.0") (o #t) (d #t) (k 0) (p "unicase")))) (h "1ai6wlha906m32f0y0m3m01gjdaik811agl8b5myaw2fqhd3b56d") (f (quote (("unicase" "unicase_" "phf_shared/unicase")))) (r "1.60")))

(define-public crate-phf_macros-0.11.1 (c (n "phf_macros") (v "0.11.1") (d (list (d (n "phf_generator") (r "^0.11.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicase_") (r "^2.4.0") (o #t) (d #t) (k 0) (p "unicase")))) (h "0rncvjimjri2vancig85icbk8h03a5s3z4cyasd70s37y72wvalj") (f (quote (("unicase" "unicase_" "phf_shared/unicase")))) (r "1.60")))

(define-public crate-phf_macros-0.11.2 (c (n "phf_macros") (v "0.11.2") (d (list (d (n "phf_generator") (r "^0.11.1") (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicase_") (r "^2.4.0") (o #t) (d #t) (k 0) (p "unicase")))) (h "0js61lc0bhzzrbd9vhpcqp11vvwckdkz3g7k95z5h1k651p68i1l") (f (quote (("unicase" "unicase_" "phf_shared/unicase")))) (r "1.60")))

