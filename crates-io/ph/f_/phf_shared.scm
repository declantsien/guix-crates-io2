(define-module (crates-io ph f_ phf_shared) #:use-module (crates-io))

(define-public crate-phf_shared-0.0.1 (c (n "phf_shared") (v "0.0.1") (d (list (d (n "xxhash") (r "*") (d #t) (k 0)))) (h "027d9davy409phb4s604rd38bpxj8nvq2dp9lj2z4vr527xahq6v")))

(define-public crate-phf_shared-0.1.0 (c (n "phf_shared") (v "0.1.0") (d (list (d (n "xxhash") (r "*") (d #t) (k 0)))) (h "0pl2bcd1v9rypqny3fnv3dsrssff6gdzmz68f5n05ac4whlg7m6w")))

(define-public crate-phf_shared-0.2.0 (c (n "phf_shared") (v "0.2.0") (d (list (d (n "xxhash") (r "*") (d #t) (k 0)))) (h "0fn7251k0lddxb4rk25rc1h9xrmg5yvancwxvq9jzamckakiiqkr")))

(define-public crate-phf_shared-0.3.0 (c (n "phf_shared") (v "0.3.0") (d (list (d (n "xxhash") (r "*") (d #t) (k 0)))) (h "1hnanp338ivqp6w81lcs9cflyg5nqjp28di8jicz9mz6a7wbx3ii")))

(define-public crate-phf_shared-0.4.0 (c (n "phf_shared") (v "0.4.0") (h "04i8vfz9hvhamjl0x11d0znsnq2x42x40690mw70irbapcl4f6mz")))

(define-public crate-phf_shared-0.4.1 (c (n "phf_shared") (v "0.4.1") (h "12hvyxz8mh27f48wshkfrlmws3cfxjryf6fa8yhn2v5hxry1p2kr")))

(define-public crate-phf_shared-0.4.2 (c (n "phf_shared") (v "0.4.2") (h "05v51nhp475ljll8wcxmlplx1alyw4q8bvry8amdhlf3h8icrkr6")))

(define-public crate-phf_shared-0.4.3 (c (n "phf_shared") (v "0.4.3") (h "0pklfrgjl16hhh249gy2cgbr94m025llm77w70qsjv2kb9phxvj7")))

(define-public crate-phf_shared-0.4.4 (c (n "phf_shared") (v "0.4.4") (h "0x7a00iiqhi7awqls0yd06c6lsm6lix8sdsbcgmghkzy3rvwrp6p")))

(define-public crate-phf_shared-0.4.5 (c (n "phf_shared") (v "0.4.5") (h "1dz9nbg2iwambjrvzglfx8g40zdyz0avj9xqwkb91scvgjjzj1z5")))

(define-public crate-phf_shared-0.4.6 (c (n "phf_shared") (v "0.4.6") (h "085p8cbdziv864sqisrvml3d8v3bprllj2a4lm8wj7gs3mx5b13p")))

(define-public crate-phf_shared-0.4.7 (c (n "phf_shared") (v "0.4.7") (h "0cwxjzmrhwn5890h2044kdyl3sinddamad98h845jq9hpc3xgkf5") (y #t)))

(define-public crate-phf_shared-0.4.8 (c (n "phf_shared") (v "0.4.8") (h "1v5h452810srv5sml4lwnvx964hh8vfidsk35dn0xzbqslfz84hn")))

(define-public crate-phf_shared-0.4.9 (c (n "phf_shared") (v "0.4.9") (h "0h2x3ikw1rzzpsqzn9fpnpmswx6nf0a5j39ag64scw36i3fsa28h")))

(define-public crate-phf_shared-0.5.0 (c (n "phf_shared") (v "0.5.0") (h "1qvwqi7hfi5mk5sbr65x1wnjmypxccv0dzmpcz924j3df942vl92")))

(define-public crate-phf_shared-0.6.0 (c (n "phf_shared") (v "0.6.0") (h "0wdz9a96p0b75mp4mf09l5f2g9r3lbbzdzkpi9kkblzymr18bpg1")))

(define-public crate-phf_shared-0.6.1 (c (n "phf_shared") (v "0.6.1") (h "0qxcrwd9zn5azwjr0h5mcqpq3ii5wgqhjb0dwvld0s2s2hy1x0jc")))

(define-public crate-phf_shared-0.6.2 (c (n "phf_shared") (v "0.6.2") (h "1navq41ysw6mmbdrkl42fk0as7z84710z2rynxz0wzs6qb5gc0d0") (f (quote (("core"))))))

(define-public crate-phf_shared-0.6.3 (c (n "phf_shared") (v "0.6.3") (h "1d63xwjwjpwf0avbq49bkk2zqz3dzmd96ddr3lsv2psx5jl71nrh") (f (quote (("core"))))))

(define-public crate-phf_shared-0.6.4 (c (n "phf_shared") (v "0.6.4") (h "0yghsdxvrcqbl7v6z6xn90m9xr8vn3fn7r5gbv261fp42iaq72yf") (f (quote (("core"))))))

(define-public crate-phf_shared-0.6.5 (c (n "phf_shared") (v "0.6.5") (h "07xwm2fmp5vgi8g607nwv01mmp7whpm10s3qfd7k4gngrw1aichv") (f (quote (("core"))))))

(define-public crate-phf_shared-0.6.6 (c (n "phf_shared") (v "0.6.6") (h "1aynmisj395m7w69rlxbvghlzp00z7q27rvg97jz185w2nm2ja6c") (f (quote (("core"))))))

(define-public crate-phf_shared-0.6.7 (c (n "phf_shared") (v "0.6.7") (h "1cf2cdf4ll7rbqhw08bg2qy3jab77fh0l8gc8dszmc3mckab3bcc") (f (quote (("core"))))))

(define-public crate-phf_shared-0.6.8 (c (n "phf_shared") (v "0.6.8") (h "1z816hfz5yc45l6rabxqbp2mjb5yar48xgf69q8qfljzz8cj2ii8") (f (quote (("core"))))))

(define-public crate-phf_shared-0.6.9 (c (n "phf_shared") (v "0.6.9") (h "0ibm6z075b8hmr6idlmk95k0258sdq73xlddl6472kjnx72qf3d6") (f (quote (("core"))))))

(define-public crate-phf_shared-0.6.10 (c (n "phf_shared") (v "0.6.10") (h "1248dd1chxvwmv5583w1r8bq8y31f9ggbyg7c09ain45phgny456")))

(define-public crate-phf_shared-0.6.11 (c (n "phf_shared") (v "0.6.11") (h "07g2frk4pafnv7xhrj0r9w5ryy4fss61maqms06gg7jv3m42mss3")))

(define-public crate-phf_shared-0.6.12 (c (n "phf_shared") (v "0.6.12") (h "1y8lvjyphbffyf93qrx1k87xdgs0v4ycy1wgws2ayyn8iglz7pnf")))

(define-public crate-phf_shared-0.6.13 (c (n "phf_shared") (v "0.6.13") (h "0xxkdmi678rvsahziv0iidq2pw3a6aqjv4yybhfvfspria4i2785")))

(define-public crate-phf_shared-0.6.14 (c (n "phf_shared") (v "0.6.14") (h "0i6wzj198957rl26d1xzi3vfvrg62g1s7icya0r9dnwgn403jiff")))

(define-public crate-phf_shared-0.6.15 (c (n "phf_shared") (v "0.6.15") (h "0r83y0bbcg03wjnf2bik30f4chnxgzjlwpnklbrk6kfcrkayd45z")))

(define-public crate-phf_shared-0.6.16 (c (n "phf_shared") (v "0.6.16") (h "14bicmjdksbggy3izyyaikyglpvfqccri4i2w322dkcgp2m3s5yd") (y #t)))

(define-public crate-phf_shared-0.6.17 (c (n "phf_shared") (v "0.6.17") (h "0w9gw0zrhwx3fakplgdxlpqlbmik4sjyllw3r46l27rpn5c2q5g2")))

(define-public crate-phf_shared-0.6.18 (c (n "phf_shared") (v "0.6.18") (h "04zhm5252m4k8pz8i7yizzknllks6pifjx1a28gd215q76g0xbcm")))

(define-public crate-phf_shared-0.6.19 (c (n "phf_shared") (v "0.6.19") (h "1z22f8mavnh7fg16m4yhff52y9ccv9dy7s6c9i2h454w19ii09ps")))

(define-public crate-phf_shared-0.7.0 (c (n "phf_shared") (v "0.7.0") (h "1a7bwgch6sh8i2frmmvx435p99mgaamdf8fx9xld1l6rk8lngca1")))

(define-public crate-phf_shared-0.7.1 (c (n "phf_shared") (v "0.7.1") (h "19amrznlf17xcrzyzg2ckd8vf4dayga0v80qp46wbqn4ya1rvnik")))

(define-public crate-phf_shared-0.7.2 (c (n "phf_shared") (v "0.7.2") (h "09q9hsfcdk0bf628nqffqjfwbsxpzzhc4722rmlximmrg1ycmapw")))

(define-public crate-phf_shared-0.7.3 (c (n "phf_shared") (v "0.7.3") (h "06584y29ncb2r2pya3kkwpb4am5q97vw8xjjsni57n35pwgy0hj5")))

(define-public crate-phf_shared-0.7.4 (c (n "phf_shared") (v "0.7.4") (h "0ry9xxf66ag0mlvllk5i3jslr997m46nf1drmrj32k7bhki6la8v")))

(define-public crate-phf_shared-0.7.5 (c (n "phf_shared") (v "0.7.5") (h "1xdnd1rgp483miq9rk2n0i5571glx31cpd7qnnr3gyy7jbrdd7rd")))

(define-public crate-phf_shared-0.7.6 (c (n "phf_shared") (v "0.7.6") (h "1x149424na80x6rp11p0fyn7681c6n04zywfid5ckyamkynsm5rp") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.7 (c (n "phf_shared") (v "0.7.7") (h "1yk5khwm16jljajfqrx8wjqjd4hq2yn8z1525hk1j5qv8xs4vlw8") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.8 (c (n "phf_shared") (v "0.7.8") (h "0lcz78y4sldglp7ysyajislfz8im2pachib2cif2k39c5fwnb7ml") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.9 (c (n "phf_shared") (v "0.7.9") (h "0jq5vjb53l6g708znrqskhc9ib2pxpd8vwsp6q36sv4h94i40rfd") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.10 (c (n "phf_shared") (v "0.7.10") (h "1z2n6mjb72xq8n9ida1dxyblkfgj3zxk3wghd03ijxf008hxgpmb") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.11 (c (n "phf_shared") (v "0.7.11") (h "0fsxnby0p4w4j167lll7lbrqh4445mlhaif1pp2h2j0f627xbj7b") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.12 (c (n "phf_shared") (v "0.7.12") (h "1lx0r3ipyibfnwp9m0ir3qczslgxxg3nvrqqs5hi81svhq36k3zl") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.13 (c (n "phf_shared") (v "0.7.13") (h "0i9qfidsm448mg0vw52zm5a5l4gvq9b394yajdk2h52jcc4m2a36") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.14 (c (n "phf_shared") (v "0.7.14") (d (list (d (n "unicase") (r "^1.1") (o #t) (d #t) (k 0)))) (h "19ia1yac83pa8afc0ga8nr3rhj9sz5n9f5cv7h94akqfjcwx1r7y") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.15 (c (n "phf_shared") (v "0.7.15") (d (list (d (n "unicase") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1fpzzpnmgiiai5m3hmixi5yb25ll4iphjxqlnh32n30lq6m18v5v") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.16 (c (n "phf_shared") (v "0.7.16") (d (list (d (n "unicase") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1mhd2dz26p2vj452nhhm7zajzrvhdnq0pia5xpsg2cadx7dvahrc") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.17 (c (n "phf_shared") (v "0.7.17") (d (list (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1g98i064dqc55xncqnix1a4vxr6ixhbm67nkk33k6a0pj68z0raa") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.18 (c (n "phf_shared") (v "0.7.18") (d (list (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0awfgbr65x61y6qkylihaaagj0521gmarr04xi4f6qpg8gdqqrk9") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.19 (c (n "phf_shared") (v "0.7.19") (d (list (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0f64samykz5938rk073bm4pdjinwl98vav32kr5ynh20ii2q8gcz") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.20 (c (n "phf_shared") (v "0.7.20") (d (list (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1dygq9jgcywai50giak1w2q9bhw4rzq1jb2vw6y4fh8dwnh8aqr8") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.21 (c (n "phf_shared") (v "0.7.21") (d (list (d (n "siphasher") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1wn2gcxb44qizhm9j2qf0fqbp28vdgdb7wij0v6xwfv4m464pqh7") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.22 (c (n "phf_shared") (v "0.7.22") (d (list (d (n "siphasher") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0c2rggczc7jgbmpqrwiffr7wdixrf7ihnaq220xsmdib9ia1s9n2") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.23 (c (n "phf_shared") (v "0.7.23") (d (list (d (n "siphasher") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "14qb2lzqxiky173qgjwjbl4nkp29frrhajpn0znks9yl4a6qjfdm") (f (quote (("core"))))))

(define-public crate-phf_shared-0.7.24 (c (n "phf_shared") (v "0.7.24") (d (list (d (n "siphasher") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^1.4") (o #t) (d #t) (k 0)))) (h "18371fla0vsj7d6d5rlfb747xbr2in11ar9vgv5qna72bnhp2kr3") (f (quote (("core"))))))

(define-public crate-phf_shared-0.8.0 (c (n "phf_shared") (v "0.8.0") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "1xssnqrrcn0nr9ayqrnm8xm37ac4xvwcx8pax7jxss7yxawzh360") (f (quote (("std") ("default" "std"))))))

(define-public crate-phf_shared-0.9.0 (c (n "phf_shared") (v "0.9.0") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uncased") (r "^0.9.6") (o #t) (k 0)) (d (n "unicase") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "1ng0hi2byifqah6bcdy3zcpbwq8jxgl4laz65gq40dp3dm11i0x6") (f (quote (("std") ("default" "std"))))))

(define-public crate-phf_shared-0.10.0 (c (n "phf_shared") (v "0.10.0") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uncased") (r "^0.9.6") (o #t) (k 0)) (d (n "unicase") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "15n02nc8yqpd8hbxngblar2g53p3nllc93d8s8ih3p5cf7bnlydn") (f (quote (("std") ("default" "std"))))))

(define-public crate-phf_shared-0.11.0 (c (n "phf_shared") (v "0.11.0") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uncased") (r "^0.9.6") (o #t) (k 0)) (d (n "unicase") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "1r28c1shni0zq5mzfpgkdk0hkqvc2hdjx8q8z5kp3y1d9ffn1mcx") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-phf_shared-0.11.1 (c (n "phf_shared") (v "0.11.1") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uncased") (r "^0.9.6") (o #t) (k 0)) (d (n "unicase") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0xp6krf3cd411rz9rbk7p6xprlz786a215039j6jlxvbh9pmzyz1") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-phf_shared-0.11.2 (c (n "phf_shared") (v "0.11.2") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uncased") (r "^0.9.6") (o #t) (k 0)) (d (n "unicase") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0azphb0a330ypqx3qvyffal5saqnks0xvl8rj73jlk3qxxgbkz4h") (f (quote (("std") ("default" "std")))) (r "1.60")))

