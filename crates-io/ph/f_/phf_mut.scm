(define-module (crates-io ph f_ phf_mut) #:use-module (crates-io))

(define-public crate-phf_mut-0.3.0 (c (n "phf_mut") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "0cf9cdrqyxf1ai889axlyhn2ck0nq6wpjvl0grcf2ggmmf07745g")))

(define-public crate-phf_mut-0.3.1 (c (n "phf_mut") (v "0.3.1") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1f6c9m3iyam50k4q20cpp9sjlzmc30h695cljx57q8c15x500h97")))

(define-public crate-phf_mut-0.3.2 (c (n "phf_mut") (v "0.3.2") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "0g6f1yvf7pqqxy87jnggcya97bqzw47rpqw48wrsc8v7hswwhrni")))

(define-public crate-phf_mut-0.3.3 (c (n "phf_mut") (v "0.3.3") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "11z0z1143y0q2s9p2wras695jsqwr4dfgvdxa4604fchbhny5li1")))

(define-public crate-phf_mut-0.4.1 (c (n "phf_mut") (v "0.4.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)))) (h "00z0953brzncnz4a3j20mghylp6ssvdw8kdq0axhl65wwkbajgaf")))

