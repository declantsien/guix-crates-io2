(define-module (crates-io ph f_ phf_mac) #:use-module (crates-io))

(define-public crate-phf_mac-0.0.1 (c (n "phf_mac") (v "0.0.1") (d (list (d (n "phf_shared") (r "^0.0.1") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1r6jvcqs2f5rkz7wm1c799h8q06vk8qc11z31q1srqyasd6p8fxx")))

(define-public crate-phf_mac-0.1.0 (c (n "phf_mac") (v "0.1.0") (d (list (d (n "phf_shared") (r "= 0.1.0") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0ds67dk0bibwj60k57hqbbfd413qd7mx5l6w2x2fpyap2cknbffg")))

(define-public crate-phf_mac-0.2.0 (c (n "phf_mac") (v "0.2.0") (d (list (d (n "phf_shared") (r "= 0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0lg3q9n51lv80qac65p4240jiph45vn3jcjjpk2rsi5ljjndv17y")))

(define-public crate-phf_mac-0.3.0 (c (n "phf_mac") (v "0.3.0") (d (list (d (n "phf_shared") (r "= 0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1hawawk9fs7d8cdsk8nbafsrw9wjblw578jvrj0bf1vyglm7abc9")))

(define-public crate-phf_mac-0.4.0 (c (n "phf_mac") (v "0.4.0") (d (list (d (n "phf_shared") (r "= 0.4.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "03iyw9fg92fyb9n4wf9jw33cfiwj0x3h4ibrcal080gbp94grszx")))

(define-public crate-phf_mac-0.4.1 (c (n "phf_mac") (v "0.4.1") (d (list (d (n "phf_shared") (r "= 0.4.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1i99l9bsqvzswk7bki75kyx7lnxfxb82vly9fsf3k2da69274n3w")))

(define-public crate-phf_mac-0.4.2 (c (n "phf_mac") (v "0.4.2") (d (list (d (n "phf_shared") (r "= 0.4.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "00i7rjh7grixcacjk6wm0vay9a8r9a7n6phglqrvbf78nzvnmzjx")))

(define-public crate-phf_mac-0.4.3 (c (n "phf_mac") (v "0.4.3") (d (list (d (n "phf_shared") (r "= 0.4.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1y7dpmdlak94l78klvi2zz68xahc1czmfwa13c1rnaxx8g5z8g6x")))

(define-public crate-phf_mac-0.4.4 (c (n "phf_mac") (v "0.4.4") (d (list (d (n "phf_shared") (r "= 0.4.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ga48zkpk7day0f4b6ygils6mlfn5v0lx6ykzpn07r716njn8dw9")))

(define-public crate-phf_mac-0.4.5 (c (n "phf_mac") (v "0.4.5") (d (list (d (n "phf_shared") (r "= 0.4.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "01ma7li31rbn6a8ym8hzs50667f2r9356kxfiz7pxalp6n6k0s2a")))

(define-public crate-phf_mac-0.4.6 (c (n "phf_mac") (v "0.4.6") (d (list (d (n "phf_shared") (r "= 0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1yjfgnzklcfwf6s615pw2c90jx8aaklhdczbkwk8y7cfaf182fyp")))

(define-public crate-phf_mac-0.4.8 (c (n "phf_mac") (v "0.4.8") (d (list (d (n "phf_shared") (r "= 0.4.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ylsxni2i6wayxwka212im5afl834sswmxia286jm5l8ip8wfkc9")))

(define-public crate-phf_mac-0.4.9 (c (n "phf_mac") (v "0.4.9") (d (list (d (n "phf_shared") (r "= 0.4.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0aqsa0d7cgr2wgwx7xkbs5rfrknyhjb51ir3r79c2m7bwzpsscnl") (f (quote (("stats" "time"))))))

(define-public crate-phf_mac-0.5.0 (c (n "phf_mac") (v "0.5.0") (d (list (d (n "phf_shared") (r "= 0.5.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "035j8q5l340d5rpqk0pspwbz5003fs86mf90ghh57r4f3zj5203w") (f (quote (("stats" "time"))))))

