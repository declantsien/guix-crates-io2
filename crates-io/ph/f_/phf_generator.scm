(define-module (crates-io ph f_ phf_generator) #:use-module (crates-io))

(define-public crate-phf_generator-0.6.10 (c (n "phf_generator") (v "0.6.10") (d (list (d (n "phf_shared") (r "= 0.6.10") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "1b9695wdbpq3jb8na6vj3z4pd0v07m2jmzc5dz8ydmqkzk0cq6zx")))

(define-public crate-phf_generator-0.6.11 (c (n "phf_generator") (v "0.6.11") (d (list (d (n "phf_shared") (r "= 0.6.11") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "08y5qs4wjkij5hxmnd0yfmlpspwil9bi58s3qw2ssvy9k5999h8g")))

(define-public crate-phf_generator-0.6.12 (c (n "phf_generator") (v "0.6.12") (d (list (d (n "phf_shared") (r "= 0.6.12") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "1q2m7mahbjh1rzjq0vp6gnd60lpgpvcdh46s2xigz12xvb63sl1y")))

(define-public crate-phf_generator-0.6.13 (c (n "phf_generator") (v "0.6.13") (d (list (d (n "phf_shared") (r "= 0.6.13") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "0sql7bpw50xcyzmqmrrqbn5hlfqzksr0mn40z3szjq8j5d3wswxv")))

(define-public crate-phf_generator-0.6.14 (c (n "phf_generator") (v "0.6.14") (d (list (d (n "phf_shared") (r "= 0.6.14") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "1givzyhdf7b53h7cw6vhv315rn3z7synwkh71ylxy8vg024q6070")))

(define-public crate-phf_generator-0.6.15 (c (n "phf_generator") (v "0.6.15") (d (list (d (n "phf_shared") (r "= 0.6.15") (d #t) (k 0)) (d (n "rand") (r "^0.2") (d #t) (k 0)))) (h "1qdf0vapnz11yg5ps5p84x1sik61m7gc9bb2galiw8q88gp2mhv7")))

(define-public crate-phf_generator-0.6.16 (c (n "phf_generator") (v "0.6.16") (d (list (d (n "phf_shared") (r "= 0.6.15") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ngc1pk4sw2s1am41rqyn13jyrpxm0ifcw7x7l0lwam1mc0j0h7h") (y #t)))

(define-public crate-phf_generator-0.6.17 (c (n "phf_generator") (v "0.6.17") (d (list (d (n "phf_shared") (r "= 0.6.17") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0dvcnwb2li21hzxi9s7gzx7z6z93z3ifzj3w93whx64l0m491qkp")))

(define-public crate-phf_generator-0.6.18 (c (n "phf_generator") (v "0.6.18") (d (list (d (n "phf_shared") (r "= 0.6.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "08mhchrag737yw1janm1hwm43jgn3yzwqi4hw49a1bkfd7xqaf47")))

(define-public crate-phf_generator-0.6.19 (c (n "phf_generator") (v "0.6.19") (d (list (d (n "phf_shared") (r "= 0.6.19") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "178ij3gd8ifawqvhr2b8m2576isz6mwgd7gwihjd3v0inr4aqxkx")))

(define-public crate-phf_generator-0.7.0 (c (n "phf_generator") (v "0.7.0") (d (list (d (n "phf_shared") (r "= 0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1vglmkfyp1xw5x29s4nblqyqammqj7zgqx67x315wlw8rbq565jw")))

(define-public crate-phf_generator-0.7.1 (c (n "phf_generator") (v "0.7.1") (d (list (d (n "phf_shared") (r "= 0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "17p693zxypdck2zp3m5m69bi2hnpjzrbb049gz6la4nr916m4gzv")))

(define-public crate-phf_generator-0.7.2 (c (n "phf_generator") (v "0.7.2") (d (list (d (n "phf_shared") (r "= 0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0gsgm7w981wvdzbxll0a4f3knyjwj1wx72b79kr0n4dxqifpr0kk")))

(define-public crate-phf_generator-0.7.3 (c (n "phf_generator") (v "0.7.3") (d (list (d (n "phf_shared") (r "= 0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1x8xk6vfmlzahyzhaq0mfz2d2k1ikdvnlyxk24k77lwmbwy3ir28")))

(define-public crate-phf_generator-0.7.4 (c (n "phf_generator") (v "0.7.4") (d (list (d (n "phf_shared") (r "= 0.7.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1yz4nnx88ajipsh599v8qxgyy6nr8p8bp51lgx5aj09gqvkslp9n")))

(define-public crate-phf_generator-0.7.5 (c (n "phf_generator") (v "0.7.5") (d (list (d (n "phf_shared") (r "= 0.7.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1i1q56cqrhsplkps5vw7l8hkmys5z1hsy8kssz546jv892q5rhy5")))

(define-public crate-phf_generator-0.7.6 (c (n "phf_generator") (v "0.7.6") (d (list (d (n "phf_shared") (r "= 0.7.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0v42yqppxykh0gyaaqpafkpbch9vpsjkka975084r15jy9acmdbd")))

(define-public crate-phf_generator-0.7.7 (c (n "phf_generator") (v "0.7.7") (d (list (d (n "phf_shared") (r "= 0.7.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1pidg0srlc131i3iziwkgl80smjgra75zcmfcq2a863iry77cl7c")))

(define-public crate-phf_generator-0.7.8 (c (n "phf_generator") (v "0.7.8") (d (list (d (n "phf_shared") (r "= 0.7.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "036hghhjq8i48rwj3gyrnp6znk81d16wrv85d32a5ns8bzgljpaa")))

(define-public crate-phf_generator-0.7.9 (c (n "phf_generator") (v "0.7.9") (d (list (d (n "phf_shared") (r "= 0.7.9") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0hcmp7s0m6nvm1f4zpxwznh34fq5vri934w19njl442zm7gpcd0j")))

(define-public crate-phf_generator-0.7.10 (c (n "phf_generator") (v "0.7.10") (d (list (d (n "phf_shared") (r "= 0.7.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "14m1lk2vqkwphl6x7vqph84yv22kdg8nsxrr1k0alzfh9m54a1j9")))

(define-public crate-phf_generator-0.7.11 (c (n "phf_generator") (v "0.7.11") (d (list (d (n "phf_shared") (r "= 0.7.11") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0g9nv1wj7z58z77w4d46n3w2g6c2akfpxh32i5d9qg434x9rars5")))

(define-public crate-phf_generator-0.7.12 (c (n "phf_generator") (v "0.7.12") (d (list (d (n "phf_shared") (r "= 0.7.12") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1gwf0r7hsv6xhmg9l04rzj26rxj06x5divkq0i70pvyyx0210faz")))

(define-public crate-phf_generator-0.7.13 (c (n "phf_generator") (v "0.7.13") (d (list (d (n "phf_shared") (r "= 0.7.13") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1hq9miimf5lrbzfjwk8vgygvyqly3kz4sbchfx808041mj9mi9by")))

(define-public crate-phf_generator-0.7.14 (c (n "phf_generator") (v "0.7.14") (d (list (d (n "phf_shared") (r "= 0.7.14") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1hqwvjvxrvrc8rwlm8csd1dhb0pm9j4pqshhfj60r04rzl45c06v")))

(define-public crate-phf_generator-0.7.15 (c (n "phf_generator") (v "0.7.15") (d (list (d (n "phf_shared") (r "= 0.7.15") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0jhjlppz9pcwm2r6migwhfxkpv7cmpxrxrnm645c9mqprjw2rg5i")))

(define-public crate-phf_generator-0.7.16 (c (n "phf_generator") (v "0.7.16") (d (list (d (n "phf_shared") (r "= 0.7.16") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1i5pxh05n4lva0js1v8s49db80r0kc93n4yri7ynxjr8bs1fmd84")))

(define-public crate-phf_generator-0.7.17 (c (n "phf_generator") (v "0.7.17") (d (list (d (n "phf_shared") (r "= 0.7.17") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0hlh0a9faazdmsch95wh9x74rxhqbqdx83wn7d22cdf8lqq6hh91")))

(define-public crate-phf_generator-0.7.18 (c (n "phf_generator") (v "0.7.18") (d (list (d (n "phf_shared") (r "= 0.7.18") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "13ggnmxxh5vzcyk8k72b60shdpblkg47m1yljn96yvmbj3fpnjg3")))

(define-public crate-phf_generator-0.7.19 (c (n "phf_generator") (v "0.7.19") (d (list (d (n "phf_shared") (r "= 0.7.19") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "123wsp1sqwaxszbq0a172icawl5qp5p9wf7a4nq0kinz4jrd9qfm")))

(define-public crate-phf_generator-0.7.20 (c (n "phf_generator") (v "0.7.20") (d (list (d (n "phf_shared") (r "= 0.7.20") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1c3y1infxwjf2gc94ylway0jpdwp1j1vdmsx7h4glnppf1wvvzsh")))

(define-public crate-phf_generator-0.7.21 (c (n "phf_generator") (v "0.7.21") (d (list (d (n "phf_shared") (r "= 0.7.21") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "00zcfp9c507y7hp65ygwbgpy9ngmkd36an64mziqbk1cag6gy1vb")))

(define-public crate-phf_generator-0.7.22 (c (n "phf_generator") (v "0.7.22") (d (list (d (n "phf_shared") (r "= 0.7.22") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "166rxc3sbwg2j9vdn9r8rni5dzayq2vcncfb456nfyrf0pfpk805")))

(define-public crate-phf_generator-0.7.23 (c (n "phf_generator") (v "0.7.23") (d (list (d (n "phf_shared") (r "= 0.7.23") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0jqcxv5d9j5f8m56pzlyq6p837avg6dm9mih2gfb024vxcgikp03")))

(define-public crate-phf_generator-0.7.24 (c (n "phf_generator") (v "0.7.24") (d (list (d (n "phf_shared") (r "^0.7.24") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0qi62gxk3x3whrmw5c4i71406icqk11qmpgln438p6qm7k4lqdh9")))

(define-public crate-phf_generator-0.8.0 (c (n "phf_generator") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "phf_shared") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "09i5338d1kixq6a60fcayz6awgxjlxcfw9ic5f02abbgr067ydhp")))

(define-public crate-phf_generator-0.9.0 (c (n "phf_generator") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "phf_shared") (r "^0.9.0") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "03igl7ly34wqr1sap302d4wwalxz6y0n1fzh73arffhgv9x47h8g")))

(define-public crate-phf_generator-0.9.1 (c (n "phf_generator") (v "0.9.1") (d (list (d (n "criterion") (r "=0.3.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "phf_shared") (r "^0.9.0") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "10ih96kaxnkn1yxk3ghpzgm09nc0rn69fd52kv68003fv4h34gyl")))

(define-public crate-phf_generator-0.10.0 (c (n "phf_generator") (v "0.10.0") (d (list (d (n "criterion") (r "=0.3.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "phf_shared") (r "^0.10.0") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1mlq6hlajsvlsx6rhw49g9ricsm017lrxmgmmbk85sxm7f4qaljx")))

(define-public crate-phf_generator-0.11.0 (c (n "phf_generator") (v "0.11.0") (d (list (d (n "criterion") (r "=0.3.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "phf_shared") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "01b75z0wm5vjagw8ccd8z6j3gwv56yyi8n0rpkxgnp7pnqh0fiav") (r "1.60")))

(define-public crate-phf_generator-0.11.1 (c (n "phf_generator") (v "0.11.1") (d (list (d (n "criterion") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "phf_shared") (r "^0.11.1") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 0)))) (h "1gsgy5k45y937qnwp58dc05d63lwlfm3imqr1zslb8qgb2a1q65i") (r "1.60")))

(define-public crate-phf_generator-0.11.2 (c (n "phf_generator") (v "0.11.2") (d (list (d (n "criterion") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "phf_shared") (r "^0.11.2") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 0)))) (h "1c14pjyxbcpwkdgw109f7581cc5fa3fnkzdq1ikvx7mdq9jcrr28") (r "1.60")))

