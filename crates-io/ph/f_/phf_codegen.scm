(define-module (crates-io ph f_ phf_codegen) #:use-module (crates-io))

(define-public crate-phf_codegen-0.6.10 (c (n "phf_codegen") (v "0.6.10") (d (list (d (n "phf") (r "= 0.6.10") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.10") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.10") (d #t) (k 0)))) (h "18yn4r79vgm0ys4z99s4zhyq9d56358dipa83jvadw82k1g5x2mx")))

(define-public crate-phf_codegen-0.6.11 (c (n "phf_codegen") (v "0.6.11") (d (list (d (n "phf") (r "= 0.6.11") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.11") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.11") (d #t) (k 0)))) (h "1pmd5h3x4hmmwlkwbb0rs2l03f9ip2mp784f691p87m3cyi9qzkl")))

(define-public crate-phf_codegen-0.6.12 (c (n "phf_codegen") (v "0.6.12") (d (list (d (n "phf") (r "= 0.6.12") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.12") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.12") (d #t) (k 0)))) (h "06s6cnh39pxdrjrd0ghy2axknw25c05gmsmj9llfzwp0vp5sm2bk")))

(define-public crate-phf_codegen-0.6.13 (c (n "phf_codegen") (v "0.6.13") (d (list (d (n "phf") (r "= 0.6.13") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.13") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.13") (d #t) (k 0)))) (h "0l817apx19vj3dr02limzbzic0527gkdz9vvkg3jw508fy99n14j")))

(define-public crate-phf_codegen-0.6.14 (c (n "phf_codegen") (v "0.6.14") (d (list (d (n "phf") (r "= 0.6.14") (d #t) (k 2)) (d (n "phf_generator") (r "= 0.6.14") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.14") (d #t) (k 0)))) (h "0g9n38vgjq2xc03qyjms9f2562qxcic3vmwvgyg9f4p11d9pzwww")))

(define-public crate-phf_codegen-0.6.15 (c (n "phf_codegen") (v "0.6.15") (d (list (d (n "phf_generator") (r "= 0.6.15") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.15") (d #t) (k 0)))) (h "034w55azd7bc3kmp1f91b1dz63ii2ncv0jw81v5gvksm7l0aadwf")))

(define-public crate-phf_codegen-0.6.17 (c (n "phf_codegen") (v "0.6.17") (d (list (d (n "phf_generator") (r "= 0.6.17") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.17") (d #t) (k 0)))) (h "0mck0bm31pkmw2zm0drnf1zwck0b7l9cqzrcf6b7lk7sv8az6jiq")))

(define-public crate-phf_codegen-0.6.18 (c (n "phf_codegen") (v "0.6.18") (d (list (d (n "phf_generator") (r "= 0.6.18") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.18") (d #t) (k 0)))) (h "15274cjh69xiyv2x4536jh8lg083fcbh1yhy05yyb1d8s2939zs7")))

(define-public crate-phf_codegen-0.6.19 (c (n "phf_codegen") (v "0.6.19") (d (list (d (n "phf_generator") (r "= 0.6.19") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.6.19") (d #t) (k 0)))) (h "0a88nqrp6qxa0qrq4ji1bskhf1aqn48jag0aqwinlw3dd18wmcky")))

(define-public crate-phf_codegen-0.7.0 (c (n "phf_codegen") (v "0.7.0") (d (list (d (n "phf_generator") (r "= 0.7.0") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.0") (d #t) (k 0)))) (h "19b5k844vlhfc2cw3x0b9lm8p8xmnfhj6pnzydhacpnlqdkigici")))

(define-public crate-phf_codegen-0.7.1 (c (n "phf_codegen") (v "0.7.1") (d (list (d (n "phf_generator") (r "= 0.7.1") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.1") (d #t) (k 0)))) (h "1zgdaw1f98l3vh2f69ysi4a2vh6dqwbz8wd09lndydlyhdzq8vxb")))

(define-public crate-phf_codegen-0.7.2 (c (n "phf_codegen") (v "0.7.2") (d (list (d (n "phf_generator") (r "= 0.7.2") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.2") (d #t) (k 0)))) (h "1x9b5l336b2y5mkj0k1rhg10wm33rn600mv1b10265vx1l23y60k")))

(define-public crate-phf_codegen-0.7.3 (c (n "phf_codegen") (v "0.7.3") (d (list (d (n "phf_generator") (r "= 0.7.3") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.3") (d #t) (k 0)))) (h "0srwpfv6p67c4pjdzp7a6132r21gq3n8gk6fr67qh1v5wxi5crpn")))

(define-public crate-phf_codegen-0.7.4 (c (n "phf_codegen") (v "0.7.4") (d (list (d (n "phf_generator") (r "= 0.7.4") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.4") (d #t) (k 0)))) (h "0f89771ifn5km6waa6ysap8wj8v1cgwgc268zln7vlgk2hdbcvix")))

(define-public crate-phf_codegen-0.7.5 (c (n "phf_codegen") (v "0.7.5") (d (list (d (n "phf_generator") (r "= 0.7.5") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.5") (d #t) (k 0)))) (h "014pvs43mnglhx5cqq54xxnacx5hspljrq0l93ps2nyjspm4jk0n")))

(define-public crate-phf_codegen-0.7.6 (c (n "phf_codegen") (v "0.7.6") (d (list (d (n "phf_generator") (r "= 0.7.6") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.6") (d #t) (k 0)))) (h "10ywi1fd4vigz2i6bvsakz42r2wnnlgr27fk4vfd4ks3f92fv656")))

(define-public crate-phf_codegen-0.7.7 (c (n "phf_codegen") (v "0.7.7") (d (list (d (n "phf_generator") (r "= 0.7.7") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.7") (d #t) (k 0)))) (h "190fsl7mm1lv461clllpzr07ak3yrgacgl4nnid6bx6z22klb4jv")))

(define-public crate-phf_codegen-0.7.8 (c (n "phf_codegen") (v "0.7.8") (d (list (d (n "phf_generator") (r "= 0.7.8") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.8") (d #t) (k 0)))) (h "03chv6qjj7zrzc5dlf80hby8wjrh4jmid5b86mbx52lv184nhqyr")))

(define-public crate-phf_codegen-0.7.9 (c (n "phf_codegen") (v "0.7.9") (d (list (d (n "phf_generator") (r "= 0.7.9") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.9") (d #t) (k 0)))) (h "1ip88vyfzafi7chqzwbaw4ad0m0fpzxgvv830y0djj6ca398x61j")))

(define-public crate-phf_codegen-0.7.10 (c (n "phf_codegen") (v "0.7.10") (d (list (d (n "phf_generator") (r "= 0.7.10") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.10") (d #t) (k 0)))) (h "1hj22n4fijz5karr9zc383b1wb19c4fn3928pz1y8gip9fngpgsp")))

(define-public crate-phf_codegen-0.7.11 (c (n "phf_codegen") (v "0.7.11") (d (list (d (n "phf_generator") (r "= 0.7.11") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.11") (d #t) (k 0)))) (h "0j5lrgg2mfvpg12lss6jv7gz9s0vgfs9s6518gs5i4ffwggswmwf")))

(define-public crate-phf_codegen-0.7.12 (c (n "phf_codegen") (v "0.7.12") (d (list (d (n "phf_generator") (r "= 0.7.12") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.12") (d #t) (k 0)))) (h "08653qq0mkm5fkm366c7g8wkl308p4ccp6m1g5i7nsj9rr976ngs")))

(define-public crate-phf_codegen-0.7.13 (c (n "phf_codegen") (v "0.7.13") (d (list (d (n "phf_generator") (r "= 0.7.13") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.13") (d #t) (k 0)))) (h "1bv12fc315ldiyiciv17b63v81iwgfkyhwfc2cpjmnhl4dx72rv9")))

(define-public crate-phf_codegen-0.7.14 (c (n "phf_codegen") (v "0.7.14") (d (list (d (n "phf_generator") (r "= 0.7.14") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.14") (d #t) (k 0)))) (h "07ghhaxi5dx3w9bnxgjafh3k97pnl705q7j95clh59bm7xyaxxwa")))

(define-public crate-phf_codegen-0.7.15 (c (n "phf_codegen") (v "0.7.15") (d (list (d (n "phf_generator") (r "= 0.7.15") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.15") (d #t) (k 0)))) (h "0dsladnfvxiw5vi5rp55izkfgj0m560690p7xkpvri68y6307im5")))

(define-public crate-phf_codegen-0.7.16 (c (n "phf_codegen") (v "0.7.16") (d (list (d (n "phf_generator") (r "= 0.7.16") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.16") (d #t) (k 0)))) (h "02ymivdk8aww0lgwd71wgfrplpj4kapw13iqf2pfi8hfrk11528a")))

(define-public crate-phf_codegen-0.7.17 (c (n "phf_codegen") (v "0.7.17") (d (list (d (n "phf_generator") (r "= 0.7.17") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.17") (d #t) (k 0)))) (h "0hm2avcxh8lq61n5ridlkxl9n3akhybvprs2y6zljbbr2446ffsn")))

(define-public crate-phf_codegen-0.7.18 (c (n "phf_codegen") (v "0.7.18") (d (list (d (n "phf_generator") (r "= 0.7.18") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.18") (d #t) (k 0)))) (h "0wbzxrx04mxx7ck2jvszdry7cf5dh4i0asbxq4y4kpdi36hpzyi9")))

(define-public crate-phf_codegen-0.7.19 (c (n "phf_codegen") (v "0.7.19") (d (list (d (n "phf_generator") (r "= 0.7.19") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.19") (d #t) (k 0)))) (h "0850l8gs6rqa75nikdr9gig3jbda9nb1wp41xzdnlpzal1p50x4b")))

(define-public crate-phf_codegen-0.7.20 (c (n "phf_codegen") (v "0.7.20") (d (list (d (n "phf_generator") (r "= 0.7.20") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.20") (d #t) (k 0)))) (h "0z1gnskdvrl2gm5lpssfhxirvrz0m09q6pd6f8hqy4lspwhz2qvb")))

(define-public crate-phf_codegen-0.7.21 (c (n "phf_codegen") (v "0.7.21") (d (list (d (n "phf_generator") (r "= 0.7.21") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.21") (d #t) (k 0)))) (h "13z87h15zva2prc02bmgklq90zhp70h50p8p6gv69i2lpg0989fn")))

(define-public crate-phf_codegen-0.7.22 (c (n "phf_codegen") (v "0.7.22") (d (list (d (n "phf_generator") (r "= 0.7.22") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.22") (d #t) (k 0)))) (h "0asgfczsd8vdhiwqhmf76fdnw4j9660d7mpc4y0np86pgpz4hh2f")))

(define-public crate-phf_codegen-0.7.23 (c (n "phf_codegen") (v "0.7.23") (d (list (d (n "phf_generator") (r "= 0.7.23") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.23") (d #t) (k 0)))) (h "1b85wq4r117ksbf2kcxfca8l8fklh7qnr3w9v2yazmcqrl07y63x")))

(define-public crate-phf_codegen-0.7.24 (c (n "phf_codegen") (v "0.7.24") (d (list (d (n "phf_generator") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7.24") (d #t) (k 0)))) (h "0zjiblicfm0nrmr2xxrs6pnf6zz2394wgch6dcbd8jijkq98agmh")))

(define-public crate-phf_codegen-0.8.0 (c (n "phf_codegen") (v "0.8.0") (d (list (d (n "phf_generator") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.8.0") (d #t) (k 0)))) (h "05d8w7aqqjb6039pfm6404gk5dlwrrf97kiy1n21212vb1hyxzyb")))

(define-public crate-phf_codegen-0.9.0 (c (n "phf_codegen") (v "0.9.0") (d (list (d (n "phf_generator") (r "^0.9.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.9.0") (d #t) (k 0)))) (h "00q5chz1v9idglrzk5qbijjyr80sgibpbkq1skgnbvi2rw8xnfln")))

(define-public crate-phf_codegen-0.9.1 (c (n "phf_codegen") (v "0.9.1") (d (list (d (n "phf_generator") (r "^0.9.1") (d #t) (k 0)) (d (n "phf_shared") (r "^0.9.0") (d #t) (k 0)))) (h "1q4r7mqisvzjz5fzfnr16nb5bxrj6xv32qnq1ds75xql783md31z") (y #t)))

(define-public crate-phf_codegen-0.10.0 (c (n "phf_codegen") (v "0.10.0") (d (list (d (n "phf_generator") (r "^0.10.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.10.0") (d #t) (k 0)))) (h "1k8kdad9wk2d5972k6jmjki2xpdy2ky4zd19rv7ybm2dpjlc7cag")))

(define-public crate-phf_codegen-0.11.0 (c (n "phf_codegen") (v "0.11.0") (d (list (d (n "phf_generator") (r "^0.11.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.0") (d #t) (k 0)))) (h "0n7f584spriq7dsb0dzhia18fffqry8259i454mlkdm1sx1hrfij") (r "1.60")))

(define-public crate-phf_codegen-0.11.1 (c (n "phf_codegen") (v "0.11.1") (d (list (d (n "phf_generator") (r "^0.11.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.0") (d #t) (k 0)))) (h "0w274bcqbz499vpvd7isb252bc5mxmj9kagapn5mkjp3qn8chsm5") (r "1.60")))

(define-public crate-phf_codegen-0.11.2 (c (n "phf_codegen") (v "0.11.2") (d (list (d (n "phf_generator") (r "^0.11.0") (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.0") (d #t) (k 0)))) (h "0nia6h4qfwaypvfch3pnq1nd2qj64dif4a6kai3b7rjrsf49dlz8") (r "1.60")))

