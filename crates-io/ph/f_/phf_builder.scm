(define-module (crates-io ph f_ phf_builder) #:use-module (crates-io))

(define-public crate-phf_builder-0.7.14 (c (n "phf_builder") (v "0.7.14") (d (list (d (n "phf") (r "= 0.7.14") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.14") (d #t) (k 0)))) (h "15hfrf8bchh1q49z147mssaj78nngwgyk5yxkynf7v5z402062bs")))

(define-public crate-phf_builder-0.7.15 (c (n "phf_builder") (v "0.7.15") (d (list (d (n "phf") (r "= 0.7.15") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.15") (d #t) (k 0)))) (h "1xch7ffvs6ygnamywfb8yyq5z7bqhmsz29g5qbp6ni2yf07n9x2w")))

(define-public crate-phf_builder-0.7.16 (c (n "phf_builder") (v "0.7.16") (d (list (d (n "phf") (r "= 0.7.16") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.16") (d #t) (k 0)))) (h "0wq7ssm4bzlvbn0ph1qsicd6zl5q8dkqc3v8kvhx6q6syr9k90ka")))

(define-public crate-phf_builder-0.7.17 (c (n "phf_builder") (v "0.7.17") (d (list (d (n "phf") (r "= 0.7.17") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.17") (d #t) (k 0)))) (h "0r93zb9vf1vrsdry5jn8n509sbhxzpl27im0gq6yxd1izkg1aajg")))

(define-public crate-phf_builder-0.7.18 (c (n "phf_builder") (v "0.7.18") (d (list (d (n "phf") (r "= 0.7.18") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.18") (d #t) (k 0)))) (h "0jlmwq4x8nbvmgsjf5aig1y7z6yj9s0fshrwmgiv2gwvwvjf5xrp")))

(define-public crate-phf_builder-0.7.19 (c (n "phf_builder") (v "0.7.19") (d (list (d (n "phf") (r "= 0.7.19") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.19") (d #t) (k 0)))) (h "0826xxzzg0zl4p8jimckf5xxss7i982an9y3p80pn8jh7r27xcg5")))

(define-public crate-phf_builder-0.7.20 (c (n "phf_builder") (v "0.7.20") (d (list (d (n "phf") (r "= 0.7.20") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.20") (d #t) (k 0)))) (h "03wax6wimgxajj8ria6wb6hh8dylslahkyddgc0z440knrm88l5n")))

(define-public crate-phf_builder-0.7.21 (c (n "phf_builder") (v "0.7.21") (d (list (d (n "phf") (r "= 0.7.21") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.21") (d #t) (k 0)))) (h "06sykzalcfs8ya7vnx5hxfx71dahj08bbz891pmcxs7j9knwgsmc")))

(define-public crate-phf_builder-0.7.22 (c (n "phf_builder") (v "0.7.22") (d (list (d (n "phf") (r "= 0.7.22") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.22") (d #t) (k 0)))) (h "0m775x7h74gd9pw2ibr7d5m2r0f9cl3qgviiljc258idwn2xds8s")))

(define-public crate-phf_builder-0.7.23 (c (n "phf_builder") (v "0.7.23") (d (list (d (n "phf") (r "= 0.7.23") (d #t) (k 0)) (d (n "phf_generator") (r "= 0.7.23") (d #t) (k 0)))) (h "07zz8pkxx8mvivrdsjrcm8vkfq7ygmrlsiygg3ccry2dcbp6dmn9")))

(define-public crate-phf_builder-0.7.24 (c (n "phf_builder") (v "0.7.24") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_generator") (r "^0.7.24") (d #t) (k 0)))) (h "09891gv9wvsp6d0xn22y5mkpng0yrb8d7r4bfq0whh545nq7zvrk")))

(define-public crate-phf_builder-0.7.25 (c (n "phf_builder") (v "0.7.25") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_generator") (r "^0.7.24") (d #t) (k 0)))) (h "1awihp5kxddzda62pawr8cpd49yx35wzdp6nij58944fmiyj0yj4")))

