(define-module (crates-io ph on phonon) #:use-module (crates-io))

(define-public crate-phonon-0.1.0 (c (n "phonon") (v "0.1.0") (d (list (d (n "biquad") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "derive_deref") (r "^1") (d #t) (k 0)) (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "13jgcvvm20zq6cw43wzfawhjvs4907fvdng2is9bld8cpaflj1bv")))

