(define-module (crates-io ph on phoner) #:use-module (crates-io))

(define-public crate-phoner-0.5.3 (c (n "phoner") (v "0.5.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "0cf8p3csiqwnn8nag8sn1knhc88gnlzpp02a68r9ng4xc0r5plkp")))

(define-public crate-phoner-0.5.4 (c (n "phoner") (v "0.5.4") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "17yp2wdadgggna3ln54zqx9g4hlm9xahv6a5dwb07ib1l713iwg5")))

(define-public crate-phoner-0.5.5 (c (n "phoner") (v "0.5.5") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "1vs6f6628ggg2clw682igyiw4iwpw097r6jinmjjpgyx6p9cd9q1")))

(define-public crate-phoner-0.6.0 (c (n "phoner") (v "0.6.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "0bz9anm2j2gfvy137g530i03c3nfhj3wm601gfsr43xrqmvfgb01")))

(define-public crate-phoner-0.6.1 (c (n "phoner") (v "0.6.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "1jg1zn96y323n1ijjhclf2xr7x5akcnah49q77b68h93dj7wzsw9")))

(define-public crate-phoner-0.6.2 (c (n "phoner") (v "0.6.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "1nbnk29dbzwkzspbcb1gndlsh8jahs590kprk6qiqdsrybsha74i")))

(define-public crate-phoner-0.6.3 (c (n "phoner") (v "0.6.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "01jcjrvk3ypi2aqhyph66ifxwq900dw6117156c0wasj486wwb2v")))

(define-public crate-phoner-0.6.4 (c (n "phoner") (v "0.6.4") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "03rhakg0bdygcj45y5zblwx8jz973qsidnjg86871iz89gyvcddn")))

(define-public crate-phoner-0.7.0 (c (n "phoner") (v "0.7.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "1jap3qj97z44f03pl53rwznw3mw3adn7c87drcaihxcfidvydpcc")))

(define-public crate-phoner-0.8.0 (c (n "phoner") (v "0.8.0") (h "05f8z24s0ks1lpnycsn4hyzykk9m3p4cfcgn4xs71ibdmb5b00k6")))

