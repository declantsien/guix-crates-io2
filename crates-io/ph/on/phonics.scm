(define-module (crates-io ph on phonics) #:use-module (crates-io))

(define-public crate-phonics-0.0.1 (c (n "phonics") (v "0.0.1") (h "0ddpc7c25sx6xmix9vg283gwzvscgyh2a4yaqs1z34dc43qzvak3")))

(define-public crate-phonics-0.1.0 (c (n "phonics") (v "0.1.0") (d (list (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13wrzylqm6ifp0bdf50dkkiv7cckq64nj3lhr53k3y18hid3z6k2")))

