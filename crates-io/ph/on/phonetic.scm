(define-module (crates-io ph on phonetic) #:use-module (crates-io))

(define-public crate-phonetic-0.1.1 (c (n "phonetic") (v "0.1.1") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tabular") (r "^0.2.0") (d #t) (k 0)))) (h "0f05fjcyyicakzz55v4jf7q4l8wnmkq8v8n3k5lkjxf9fzz3840s") (y #t)))

(define-public crate-phonetic-0.1.2 (c (n "phonetic") (v "0.1.2") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tabular") (r "^0.2.0") (d #t) (k 0)))) (h "1l2y4gs5zc8jlpckkqn86najjdaiyrcidwpgbmqkmi30mfqnhi2k") (y #t)))

(define-public crate-phonetic-0.2.0 (c (n "phonetic") (v "0.2.0") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3.0") (d #t) (k 1)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tabular") (r "^0.2.0") (d #t) (k 0)))) (h "0wjyz7rhdmz46f5xqiyjbb4n7q636qy79m1spm397mhmbmhvix5p") (y #t)))

