(define-module (crates-io ph on phonet) #:use-module (crates-io))

(define-public crate-phonet-0.8.0 (c (n "phonet") (v "0.8.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "09a99gzjs336jvchzpllcs55z97z9dyya3dhnzg8cx13fmbp0bgj")))

(define-public crate-phonet-0.8.1 (c (n "phonet") (v "0.8.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "0hnwj5m8nba9wc1mn7mbl8lwqz46ggyklm3mcfiynzznq6vzhbqc")))

(define-public crate-phonet-0.9.0 (c (n "phonet") (v "0.9.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "108dgm5vvi7jii3hh4a10zsyalsgvm79g87iyybw5k3qqg11lsc8")))

(define-public crate-phonet-0.9.1 (c (n "phonet") (v "0.9.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0ikvww9da8wrvcaifj71l6zs2km4d39yvwgpn3hqbdpfwixsr7p4")))

(define-public crate-phonet-0.9.2 (c (n "phonet") (v "0.9.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1br8qwqc8srqlv8fskya59dfdx8xawwsgvv9ss4wrm4iqwjrc31f")))

(define-public crate-phonet-0.9.3 (c (n "phonet") (v "0.9.3") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stilo") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1mm8jjymk1dmga3nfwczgdx9vfr2s8ci2hd6ik5fryysga0y5bhw")))

(define-public crate-phonet-0.9.4 (c (n "phonet") (v "0.9.4") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stilo") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1ykz840zc0vv5czy3524wghrz0v8nz70r6y0xs03cm5wn7mqzp5r")))

(define-public crate-phonet-0.9.5 (c (n "phonet") (v "0.9.5") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stilo") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1lis050sxwlppgrrbb1raiyp1alf9n2gpqf0cnqjfiqdp9ir411l")))

(define-public crate-phonet-0.9.6 (c (n "phonet") (v "0.9.6") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stilo") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0qwd7f0r0cck44iiimdzrrd302g0m1r1xqlklv2shl72nx4i05xk")))

(define-public crate-phonet-1.0.0 (c (n "phonet") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stilo") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0nd91y6gc6s9qma420k7pn9wwzzcy34bv0533sx5isbpkwj795k6")))

(define-public crate-phonet-1.0.1 (c (n "phonet") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stilo") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1kfp621g5iyjkgimm3xwdkxrsjrhnc77hqhcn0mxaja235xgbzgi")))

(define-public crate-phonet-1.0.2 (c (n "phonet") (v "1.0.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "fancy-regex-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stilo") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "16kcqc95bc97z35ppsd3bpm6z8jj4idbq9y8nh2swj5j5x723wv5")))

