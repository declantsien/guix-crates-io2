(define-module (crates-io ph on phone-number-verifier) #:use-module (crates-io))

(define-public crate-phone-number-verifier-1.0.0 (c (n "phone-number-verifier") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0i6272zgwq9p91y0bzfns30dwzkhm67jwaf2jcrk15sj1mivcpwx")))

(define-public crate-phone-number-verifier-1.0.1 (c (n "phone-number-verifier") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0014kldw56dnaay02g7apiw64nisskxa1ldqc4zazr36jy3sdknd")))

