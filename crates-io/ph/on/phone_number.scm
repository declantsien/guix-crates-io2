(define-module (crates-io ph on phone_number) #:use-module (crates-io))

(define-public crate-phone_number-0.1.0 (c (n "phone_number") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0f4p2i91p1qy9hw96l7zys4iwbahmqy0gr4a7k0s9y22zg5np7xw")))

(define-public crate-phone_number-0.1.1 (c (n "phone_number") (v "0.1.1") (d (list (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1fqrpnfa5rlyw7ka50a4xdvdc7y6wxs161l0krrpiqkhcxjxkp0j")))

