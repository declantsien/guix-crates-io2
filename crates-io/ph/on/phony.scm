(define-module (crates-io ph on phony) #:use-module (crates-io))

(define-public crate-phony-0.1.0 (c (n "phony") (v "0.1.0") (h "0z3pmsjbp6y0960j99x74bq0n00phmskppnivk2sck970354vvi9")))

(define-public crate-phony-0.1.1 (c (n "phony") (v "0.1.1") (h "17vs8lkxz0p15ffsmn0y9nm5c76jyc95bfpxl1c30z56vi2xi51y")))

(define-public crate-phony-0.1.2 (c (n "phony") (v "0.1.2") (h "0m61svlqqz9xv8kbr5c7hcy35djm5q9ivdcvmma37xsmpb328ryf")))

