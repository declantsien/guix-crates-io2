(define-module (crates-io ph on phone) #:use-module (crates-io))

(define-public crate-phone-0.1.0 (c (n "phone") (v "0.1.0") (h "1f3wrfrlljrr7xlqaja3cz1dgclvmbcdf7ibagqv7ig2wl50qgm7")))

(define-public crate-phone-0.1.1 (c (n "phone") (v "0.1.1") (h "1f2amam936j9b847nk0s4gvv0b4wx40xr1gbgs2mr0iidmvi2v7l")))

(define-public crate-phone-0.1.2 (c (n "phone") (v "0.1.2") (h "1hmz3mz5fwgng2s0lr2lysrk4hhz7v1v3wlbgam5irq74igrl23v")))

