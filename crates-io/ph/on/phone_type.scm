(define-module (crates-io ph on phone_type) #:use-module (crates-io))

(define-public crate-phone_type-0.1.0 (c (n "phone_type") (v "0.1.0") (d (list (d (n "phone-number-verifier") (r "^1.0.1") (d #t) (k 0)))) (h "06lbisdhgr8xjpb0d5imdw7vd2infpyicb4gcyviwmpjpva9y88n")))

(define-public crate-phone_type-0.1.1 (c (n "phone_type") (v "0.1.1") (d (list (d (n "phone-number-verifier") (r "^1.0.1") (d #t) (k 0)))) (h "19glvckdhq4fca7f5g46clb2vp5wxk2yqi663ijf3giyk22sgbc0")))

(define-public crate-phone_type-0.2.0 (c (n "phone_type") (v "0.2.0") (d (list (d (n "phone-number-verifier") (r "^1.0.1") (d #t) (k 0)))) (h "17fkgfv4n3wi9kwf0kbchskr603byizmd2jh5s6sn810xsg2cv0z")))

(define-public crate-phone_type-0.2.1 (c (n "phone_type") (v "0.2.1") (d (list (d (n "phone-number-verifier") (r "^1.0.1") (d #t) (k 0)))) (h "1cswgj49bbyz0qk170jhmyh8pivggqpm8zh4v5h94i7408g6vhp5")))

(define-public crate-phone_type-0.3.0 (c (n "phone_type") (v "0.3.0") (d (list (d (n "phone-number-verifier") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0c2065z1wiayp1r28a5mjaxrv6i5zk3zkx48p7mcg97f3sva99d9") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

