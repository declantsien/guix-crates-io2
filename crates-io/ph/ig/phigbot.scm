(define-module (crates-io ph ig phigbot) #:use-module (crates-io))

(define-public crate-phigbot-0.1.0 (c (n "phigbot") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "teloxide") (r "^0.9") (f (quote ("macros" "auto-send"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1r2f29lw49p0vlp154khx07khh5zp5ispkx32hv76iq3alvfkxx3")))

(define-public crate-phigbot-0.2.0 (c (n "phigbot") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "teloxide") (r "^0.10") (f (quote ("ctrlc_handler" "macros" "auto-send"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0f7ys72hb2ybmpxln1cxyw071s7ff8s9jy1p48p7rg3b0fh3s5qw")))

(define-public crate-phigbot-0.2.1 (c (n "phigbot") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "teloxide") (r "^0.10") (f (quote ("ctrlc_handler" "macros" "auto-send"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1qyzf15mw9af51gr9y9pr8g79xyn6r7fdn9x5cdqn5mk0amdzpbj")))

