(define-module (crates-io ph il philipshue-edj) #:use-module (crates-io))

(define-public crate-philipshue-edj-0.3.3 (c (n "philipshue-edj") (v "0.3.3") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ssdp") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1w7p44kl6rny88jw69ryms6ziwbg7hk23h9rmkgp97wnffnj2nr4") (f (quote (("upnp" "ssdp") ("unstable" "upnp") ("nupnp" "hyper-openssl") ("default" "nupnp"))))))

