(define-module (crates-io ph il philec) #:use-module (crates-io))

(define-public crate-philec-0.1.3 (c (n "philec") (v "0.1.3") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "phile") (r "^0.1.3") (d #t) (k 0)))) (h "11f5bvq26kllvm00l938060ga79hq8vc6f0ihq8j31cbv8safrci")))

(define-public crate-philec-0.1.4 (c (n "philec") (v "0.1.4") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "phile") (r "^0.1.4") (d #t) (k 0)))) (h "0sag19nrbiw24vc0yqmmm17da9m75mrgwak2fnl81czqnkpxq9mr")))

