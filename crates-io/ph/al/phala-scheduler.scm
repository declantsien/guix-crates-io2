(define-module (crates-io ph al phala-scheduler) #:use-module (crates-io))

(define-public crate-phala-scheduler-0.1.0 (c (n "phala-scheduler") (v "0.1.0") (d (list (d (n "rbtree") (r "^0.1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0474ila5v1m97dxcjw16zy4vzddadxwxn0w6v8b5ql4xramdssc1")))

