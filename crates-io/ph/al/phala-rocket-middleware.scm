(define-module (crates-io ph al phala-rocket-middleware) #:use-module (crates-io))

(define-public crate-phala-rocket-middleware-0.1.0 (c (n "phala-rocket-middleware") (v "0.1.0") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "15n4zi3h27wx73wpjzsmk1faf904d9h2nsdqkxyg62nsiy922jn5")))

(define-public crate-phala-rocket-middleware-0.1.1 (c (n "phala-rocket-middleware") (v "0.1.1") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "11w7b2x7l5c7b207dvr2nkwll8hfnk60dcx00yx2mqky3r659k9b")))

