(define-module (crates-io ph al phalanx) #:use-module (crates-io))

(define-public crate-phalanx-0.0.1 (c (n "phalanx") (v "0.0.1") (h "1hn0lkjr3mzyn310jdcszvxpdb87fzzzlj7bjydxv4bvrsdfp3rk")))

(define-public crate-phalanx-0.0.2 (c (n "phalanx") (v "0.0.2") (h "1g5558gw9laqnja1kixm0avijzi1l258fy45n8v4br6i1qc9lfp8")))

