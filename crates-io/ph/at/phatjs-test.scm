(define-module (crates-io ph at phatjs-test) #:use-module (crates-io))

(define-public crate-phatjs-test-0.1.0 (c (n "phatjs-test") (v "0.1.0") (d (list (d (n "drink") (r "^0.8.5") (d #t) (k 0)) (d (n "drink-pink-runtime") (r "^1.2.13") (d #t) (k 0)) (d (n "hex_fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "ink") (r "^4.3.0") (d #t) (k 0)) (d (n "phat_js") (r "^0.2.8") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tracing") (r "^0.1.27") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "05amkyldmx52z6ckp8i5r9d6jrzah1fzhsi9ri8vka1dy4zm9bn4")))

