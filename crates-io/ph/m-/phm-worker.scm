(define-module (crates-io ph m- phm-worker) #:use-module (crates-io))

(define-public crate-phm-worker-0.0.2 (c (n "phm-worker") (v "0.0.2") (d (list (d (n "defmt") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "phm-icd") (r "^0.0.2") (f (quote ("use-defmt"))) (d #t) (k 0)))) (h "11q93l2i1xzrwcvj5fqwyf4034v4699888mz7j05z7m60d9jvbd6")))

