(define-module (crates-io ph m- phm-cli) #:use-module (crates-io))

(define-public crate-phm-cli-0.0.2 (c (n "phm-cli") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "phm") (r "^0.0.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "07pkhp4b4914yn2njls5d9418mda4jpw8w5d94l4rhwfykai03v2")))

