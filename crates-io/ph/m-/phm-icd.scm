(define-module (crates-io ph m- phm-icd) #:use-module (crates-io))

(define-public crate-phm-icd-0.0.1 (c (n "phm-icd") (v "0.0.1") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "018fsm9pn36qnclm9n561k8xiwkwka418zh50j9vr3inxypvx2ii") (f (quote (("use-defmt" "defmt" "heapless/defmt-impl"))))))

(define-public crate-phm-icd-0.0.2 (c (n "phm-icd") (v "0.0.2") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "0vwnh18jqpx02578pam2awjj1v1n1h3mk4pfnjixff5vv2sp3qd0") (f (quote (("use-defmt" "defmt" "heapless/defmt-impl"))))))

