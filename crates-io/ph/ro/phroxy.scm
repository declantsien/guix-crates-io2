(define-module (crates-io ph ro phroxy) #:use-module (crates-io))

(define-public crate-phroxy-0.1.0 (c (n "phroxy") (v "0.1.0") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "phetch") (r "^0.1.10") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0cs8zdq9gx6wmwhwvyczy39lsly1hs0pl0xrsqg2zwjj8862df2s")))

(define-public crate-phroxy-0.1.1 (c (n "phroxy") (v "0.1.1") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "phetch") (r "^0.1.10") (d #t) (k 0)) (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1kihxxvfdzdgq676kcxrh1fqnxigrn0zc3785rh9dgs5sx354ss0")))

(define-public crate-phroxy-0.1.2 (c (n "phroxy") (v "0.1.2") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "phetch") (r "^0.1.10") (d #t) (k 0)) (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "15w03fwnvnbg1lirdx43jv29i7y7khla8dxf5pq65d0gj5ilw8lx")))

(define-public crate-phroxy-0.1.3 (c (n "phroxy") (v "0.1.3") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)) (d (n "phetch") (r "^0.1.10") (d #t) (k 0)) (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0k70kdj6kh28x8sh26lm6p5nxz2vf0vl9qkqlpawkwqjr8xlcw0c")))

(define-public crate-phroxy-0.1.4 (c (n "phroxy") (v "0.1.4") (d (list (d (n "autolink") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)) (d (n "phetch") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "vte") (r "^0.3.2") (d #t) (k 0)))) (h "1i3si2yaripdg4qxlnvlm91rd793gysm2m42pgc06721b9b28nri")))

(define-public crate-phroxy-0.1.5 (c (n "phroxy") (v "0.1.5") (d (list (d (n "autolink") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)) (d (n "phetch") (r "^1.0.2") (f (quote ("tls"))) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "vte") (r "^0.3.2") (d #t) (k 0)))) (h "0cax9jyr4nz4dn7p25xhqlsvfsz2qcfdd0652cg8jgb0c56qddqx")))

(define-public crate-phroxy-0.1.6 (c (n "phroxy") (v "0.1.6") (d (list (d (n "autolink") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)) (d (n "phetch") (r "^1.0.3") (f (quote ("tls"))) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "vte") (r "^0.3.2") (d #t) (k 0)))) (h "1f7i7h2hdivwpr7s6wpgq82if64dxrr872ma9vgdqfpiwcjdj93h")))

(define-public crate-phroxy-0.1.7 (c (n "phroxy") (v "0.1.7") (d (list (d (n "autolink") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "phetch") (r "^1.0.3") (f (quote ("tls"))) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "rust-embed") (r "^5.2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "vte") (r "^0.3.2") (d #t) (k 0)))) (h "1ib595qi5w66j4y887lz64pkrckk7p1572sh4vcxnx0xv0jb3v2k")))

