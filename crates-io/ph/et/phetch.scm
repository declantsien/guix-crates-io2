(define-module (crates-io ph et phetch) #:use-module (crates-io))

(define-public crate-phetch-0.1.1 (c (n "phetch") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "09jcpd1dkxi2b61avbzmqpp702dn8qjzz5p5djk4xlx0kz3qjrmz")))

(define-public crate-phetch-0.1.2 (c (n "phetch") (v "0.1.2") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0w4mi7ywjrk7iby0wbhrib1jsi0kyhp395j11237wnsykl810jaq")))

(define-public crate-phetch-0.1.3 (c (n "phetch") (v "0.1.3") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1hqwqxyhw1jr7wc39ipv839ad5gk1db979jsgj5ixdvb43mv965i")))

(define-public crate-phetch-0.1.4 (c (n "phetch") (v "0.1.4") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "15j1dx6a85ksfw6x0p4ma3zrh69s2smyz7baf98c5q8y5hax8jz3")))

(define-public crate-phetch-0.1.6-dev (c (n "phetch") (v "0.1.6-dev") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1bxj9m5l0mh7z4z7smbxgqd0v3xm91fl214jrxci2wz4jyx5r8ym")))

(define-public crate-phetch-0.1.5 (c (n "phetch") (v "0.1.5") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1phb4iiygyyf0jl0a4mbdqcighkcdq18pxsja5k03jbvm2w6s8va")))

(define-public crate-phetch-0.1.6 (c (n "phetch") (v "0.1.6") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0vmwcgvqmdr8vqwcyfa8i72pik77lvfklzxlhlvj0vagay2h97sr")))

(define-public crate-phetch-0.1.7 (c (n "phetch") (v "0.1.7") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1p6q5sr2x4ikhpphzwx1xhrg5x0nj39i357qp6qldg8zycxnpwi9")))

(define-public crate-phetch-0.1.8 (c (n "phetch") (v "0.1.8") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "16kn456y4g4x8s961k1f63y0y5ip6jps1b7iza39522inrp90p6b")))

(define-public crate-phetch-0.1.9 (c (n "phetch") (v "0.1.9") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0mnzfl63ccqzfbddc241xilpm6sqi67lk2pgaxpjhisb7fqr305z")))

(define-public crate-phetch-0.1.10 (c (n "phetch") (v "0.1.10") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0vk1xpyvhnrdvbjwq7fbn8q7mw9yirzbghdsh8i3aspmrgxdjz14")))

(define-public crate-phetch-0.1.11 (c (n "phetch") (v "0.1.11") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "02ya2fxrpil8l4y8qrclrb322xw7jzgyay7iljhwk64kk40hwqnr") (f (quote (("tls") ("default"))))))

(define-public crate-phetch-0.1.12 (c (n "phetch") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0zhlm42n0x80mqmzi7d5fjbd0kiyxw5k2v85k9j8jb2y2pxp342h") (f (quote (("disable-tls") ("default"))))))

(define-public crate-phetch-0.1.13 (c (n "phetch") (v "0.1.13") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (d #t) (k 0)))) (h "11x321ks8x6rsyhg6njcyfygnnbaw459q0s4jaqhqfdkr9dkssgg") (f (quote (("disable-tls") ("default"))))))

(define-public crate-phetch-0.9.0 (c (n "phetch") (v "0.9.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1nzp32w528n2gv3pvil51gib5c763lbxkvxa92z03gvb8i0y75xf") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-0.9.1 (c (n "phetch") (v "0.9.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1nvh4xaav78jd939zcs1s5q70crxjwiqsz4wlxvqf0hkl324y71a") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-0.9.2 (c (n "phetch") (v "0.9.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "05aarkk4r128g26l88mcj5vm20ikb9nz9cpqjfhpx3ihwg2s2h3z") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-0.9.3 (c (n "phetch") (v "0.9.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0hd6p66nyjy673jns5milljq54d0imc1a833s6kikp9mjsqr3s0x") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-0.9.4 (c (n "phetch") (v "0.9.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0fah158m0425ib8lgh3q15i22ycb6lcri51212jckm0r4w1vcdlr") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.0.0 (c (n "phetch") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1fvc8zlwzfk855wwmnbpv274bjj3a7sqw0r50c5z0aq2ks64dr0g") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.0.1 (c (n "phetch") (v "1.0.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0x6snj768x5w2rlh1cnk2vxc2x1jl9sdwzkjkh4g0rcagnjq2a0s") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.0.2 (c (n "phetch") (v "1.0.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0zcz1anq1zcvfkww8iwvvwvaxdxy44vf80mx2879383q92wqlv02") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.0.3 (c (n "phetch") (v "1.0.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0qbvkq46kb8dm4mlaymr8lnlgzy9r6ifrq15pajs4j1rki9rr3rv") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.0.4 (c (n "phetch") (v "1.0.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0f3wq9bspdg75nqrx0q72d42z4rbqih5cxv5s1z7i0zf3kmjvail") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.0.5 (c (n "phetch") (v "1.0.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0x4gvs591pjm1r98swhvisrx80b2yws7p6h92kcnkqvgh3sm421y") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.0.6 (c (n "phetch") (v "1.0.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1byfjazy4l0z7kmask82lfhz852vbmdfkwc77hxfri7nji0xnpwl") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.0.7 (c (n "phetch") (v "1.0.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "153cjxh0dr7sp5wsdw8h1qwlnwvrsb6vz8h3wj4pa7bmldpicyhy") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.1.0 (c (n "phetch") (v "1.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "cp437") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "10yni5d03hl33zkz4w05ypik1j4df2kbssp0glbyckpgaxafjwis") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

(define-public crate-phetch-1.2.0 (c (n "phetch") (v "1.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "cp437") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tor-stream") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "04fyy82f1yy8a6992cz3z211kiwm15kqq3cs7y0dsw0slhp6v050") (f (quote (("tor" "tor-stream") ("tls" "native-tls") ("default" "tls" "tor"))))))

