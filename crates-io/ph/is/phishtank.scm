(define-module (crates-io ph is phishtank) #:use-module (crates-io))

(define-public crate-phishtank-0.0.2 (c (n "phishtank") (v "0.0.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0pdw6jxqqqlw8k0s1bvwq1vc4yx4f9vfv1gs7ky6sxzvzxg96hqx")))

(define-public crate-phishtank-0.1.0 (c (n "phishtank") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0s74cd5rhjxpjagkzsb4m85cz9209w8gyijy0yziljazqlmgr7wq")))

