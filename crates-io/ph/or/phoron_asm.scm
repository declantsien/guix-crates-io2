(define-module (crates-io ph or phoron_asm) #:use-module (crates-io))

(define-public crate-phoron_asm-0.1.0 (c (n "phoron_asm") (v "0.1.0") (h "0k6929nzy7w8dx5blafjpj3is99ji8sdfcwcbjrnzlrm1k9wl4np")))

(define-public crate-phoron_asm-1.0.0 (c (n "phoron_asm") (v "1.0.0") (d (list (d (n "phoron_core") (r "^0.5.3") (d #t) (k 0)))) (h "0f01y0a21rj0hmh6za92cp9l1fnnp85lknfs8hamqlds9n3xlknq")))

(define-public crate-phoron_asm-1.0.1 (c (n "phoron_asm") (v "1.0.1") (d (list (d (n "phoron_core") (r "^0.5.4") (d #t) (k 0)))) (h "0nz7y6jv0awxmvnnpijfkkl6q68v753i3z8qk3c4p9v1f853nm5q")))

(define-public crate-phoron_asm-1.0.2 (c (n "phoron_asm") (v "1.0.2") (d (list (d (n "phoron_core") (r "^0.5.4") (d #t) (k 0)))) (h "1mczfnascl54c7kvx1kab12n9gdqrrq6lwmnk25frjykqwyqhfwv")))

