(define-module (crates-io ph or phoron_core) #:use-module (crates-io))

(define-public crate-phoron_core-0.1.0 (c (n "phoron_core") (v "0.1.0") (h "1qg4r1k6z47g6lwdvfkwp7bxx5gg1jxlwjy7clc6bdwrb7j21yi1")))

(define-public crate-phoron_core-0.2.0 (c (n "phoron_core") (v "0.2.0") (h "11mnlby5jxvskpdv6sajvpf3x6pw16ay341v7jx9zzhs3mam6hn2")))

(define-public crate-phoron_core-0.3.0 (c (n "phoron_core") (v "0.3.0") (h "00ynkj01h0zd1n2dp53wr70msm9jpa5jm7km6rbf8abn9f80pcx4")))

(define-public crate-phoron_core-0.4.0 (c (n "phoron_core") (v "0.4.0") (h "051xb5cnq8j73nab9g7wjs0gjn5nbjrrb2qjwykdb78y3raiqnlj")))

(define-public crate-phoron_core-0.4.1 (c (n "phoron_core") (v "0.4.1") (h "1dg52vnk6s3yvpmy4v2l59b7h5gxc1kmmmisn2gfj97spxq4slnj")))

(define-public crate-phoron_core-0.5.1 (c (n "phoron_core") (v "0.5.1") (h "0bgwcg4splxq3r9v76qvizascvfzanng008gn0xrhr89y64x0cak")))

(define-public crate-phoron_core-0.5.2 (c (n "phoron_core") (v "0.5.2") (h "0f4prmdy8mi8m4cnnvawyy9gpf94zzqrnmw3xawns1y7k8macgzm")))

(define-public crate-phoron_core-0.5.3 (c (n "phoron_core") (v "0.5.3") (h "052wkg8vxdlmgmgnbww4s0p9kz00kf3xwv7if3g5xfxcximfz4mp")))

(define-public crate-phoron_core-0.5.4 (c (n "phoron_core") (v "0.5.4") (h "1ldxnp04ibm81nnaibpqyixavvrawxhkswg926hn6i93l7y1xm95")))

