(define-module (crates-io ph or phoronix-reader) #:use-module (crates-io))

(define-public crate-phoronix-reader-0.2.3 (c (n "phoronix-reader") (v "0.2.3") (d (list (d (n "gdk") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.0.7") (f (quote ("v3_16"))) (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9.2") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "04ykj4f4v2s9knx3jg8ihav27bmb1n491ksgx58algqhp97bffv3") (f (quote (("enable_gtk" "gtk" "gdk") ("enable_colors" "term"))))))

