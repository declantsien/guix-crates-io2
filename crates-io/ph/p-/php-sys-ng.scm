(define-module (crates-io ph p- php-sys-ng) #:use-module (crates-io))

(define-public crate-php-sys-ng-8.2.9 (c (n "php-sys-ng") (v "8.2.9") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "1chvihany64lmv5mp49x7w72qwwf0hcmbi5ncs0ppnqwnixld81a") (y #t) (l "php8")))

(define-public crate-php-sys-ng-9.0.0 (c (n "php-sys-ng") (v "9.0.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "1i3l2gb8yr89f20frfbw0qd1dr70gbkj5kahrxzdlpbfzdwfsvas") (y #t) (l "php8")))

