(define-module (crates-io ph p- php-indexer) #:use-module (crates-io))

(define-public crate-php-indexer-0.0.0-b1 (c (n "php-indexer") (v "0.0.0-b1") (d (list (d (n "cmder") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "php-indexer-lib") (r "^0.0.0-b1") (d #t) (k 0)))) (h "076ys8pyhh80pb151n2g8fz7705997a6kxx4j0dlndy4804bqbrn")))

