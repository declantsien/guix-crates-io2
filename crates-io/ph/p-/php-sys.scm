(define-module (crates-io ph p- php-sys) #:use-module (crates-io))

(define-public crate-php-sys-7.2.12 (c (n "php-sys") (v "7.2.12") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "0zg8snbvl5fwnk4cppmxhzwpckja4iqdim503lc50lyr04cq3fv8") (l "php7")))

