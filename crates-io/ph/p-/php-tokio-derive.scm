(define-module (crates-io ph p- php-tokio-derive) #:use-module (crates-io))

(define-public crate-php-tokio-derive-0.1.0 (c (n "php-tokio-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "1486y9wrjwvbp38rdf10s8ixjgqxasj7hr65kspzx04l9zmzkw1p")))

(define-public crate-php-tokio-derive-0.2.0 (c (n "php-tokio-derive") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0q3hwz2ziqg8gnk1q7gbz0i3pas3i7w2gjf7pmykidwx70vfihks")))

