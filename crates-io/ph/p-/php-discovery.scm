(define-module (crates-io ph p- php-discovery) #:use-module (crates-io))

(define-public crate-php-discovery-0.1.0 (c (n "php-discovery") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "01d7v21dmjqzjv4pbywgx0mng33xb5989ws1np0j2dzdp51prkl9")))

(define-public crate-php-discovery-0.1.1 (c (n "php-discovery") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "007vlsc1j88ss7qq9bg2nizb1b5c7pc8binq8kraxh5c77g8c8am")))

(define-public crate-php-discovery-0.1.2 (c (n "php-discovery") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0p40r0v07z86n3f1max2aschhnw800l8gnw94kssarhcki2wdk3d")))

