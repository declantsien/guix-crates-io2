(define-module (crates-io ph p- php-parser-rs) #:use-module (crates-io))

(define-public crate-php-parser-rs-0.0.0 (c (n "php-parser-rs") (v "0.0.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0cicyl239ilyra6sx2lcmakn92qvk360yzqpxqswpihbq0h5q6sn") (y #t)))

(define-public crate-php-parser-rs-0.0.0-b1 (c (n "php-parser-rs") (v "0.0.0-b1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "05k4d3h5i9ccm3yphdmakahnwrhjza34f8vikng8vfzmgg0vs3cj")))

(define-public crate-php-parser-rs-0.0.0-b2 (c (n "php-parser-rs") (v "0.0.0-b2") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0nk3k3djvpdwixp9mhq08cvsdvg9wcr5wjwzk1q5qpgzjpx4d4c8")))

(define-public crate-php-parser-rs-0.0.0-b3 (c (n "php-parser-rs") (v "0.0.0-b3") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1gncjnhf470h1incsryr53qz2alv1ck6hxd3fkq4n46snvqphj6x")))

(define-public crate-php-parser-rs-0.0.0-b4 (c (n "php-parser-rs") (v "0.0.0-b4") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "19vcgcm4x8crsiywsdax7j9m0a79k79ldp4q25mbaq7qijn4ddl3")))

(define-public crate-php-parser-rs-0.1.0 (c (n "php-parser-rs") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "17gnvgjja6sfkrhg774ljnp6xdi9bb139yhmqmc3rd6wzkx3zcy9")))

(define-public crate-php-parser-rs-0.1.1 (c (n "php-parser-rs") (v "0.1.1") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1fkv1yvi6vwyvz3cba1mkza1qap7qvjs5bhg484kcpr4s16vnmsh")))

(define-public crate-php-parser-rs-0.1.2 (c (n "php-parser-rs") (v "0.1.2") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1fjn5q55w877668z2p309z56cbviahrzpr8agq4v29wllpwl66v0")))

(define-public crate-php-parser-rs-0.1.3 (c (n "php-parser-rs") (v "0.1.3") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0mc53f5sshykivjsbc3qifzi67fsx1g112krwsnc2zswy9b3i7jx")))

