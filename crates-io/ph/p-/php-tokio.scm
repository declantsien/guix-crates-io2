(define-module (crates-io ph p- php-tokio) #:use-module (crates-io))

(define-public crate-php-tokio-0.1.0 (c (n "php-tokio") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nicelocal-ext-php-rs") (r "^0.10.3") (d #t) (k 0)) (d (n "php-tokio-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-pipe") (r "^0.2") (d #t) (k 0)))) (h "1apac9p1n97s6x1mf73r8vcpv7y0lypyk7fcycxfr2c8f3kssafa")))

(define-public crate-php-tokio-0.1.1 (c (n "php-tokio") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nicelocal-ext-php-rs") (r "^0.10.3") (d #t) (k 0)) (d (n "php-tokio-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-pipe") (r "^0.2") (d #t) (k 0)))) (h "1zznm6l8vj8w7c2xpsslplhqp114vvr4ynryikwcyyzfqv8mvz8v")))

(define-public crate-php-tokio-0.1.2 (c (n "php-tokio") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nicelocal-ext-php-rs") (r "^0.10.4") (d #t) (k 0)) (d (n "php-tokio-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-pipe") (r "^0.2") (d #t) (k 0)))) (h "0jwfaykl44di5l6fp67gnxq372ygy6izxcpdhc77jc96kswyyp2h")))

(define-public crate-php-tokio-0.1.3 (c (n "php-tokio") (v "0.1.3") (d (list (d (n "ext-php-rs") (r "^0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "php-tokio-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-pipe") (r "^0.2") (d #t) (k 0)))) (h "11mx928jslvlsk6wvlhn929sb1j5j0p4lxhbk8fzml3dp5cabih0") (y #t)))

(define-public crate-php-tokio-0.1.4 (c (n "php-tokio") (v "0.1.4") (d (list (d (n "ext-php-rs") (r "^0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "php-tokio-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-pipe") (r "^0.2") (d #t) (k 0)))) (h "0nygs07sdf8ibcii8w515dnpci3hhpx409wqsvvkmxj40l8b944p")))

(define-public crate-php-tokio-0.1.5 (c (n "php-tokio") (v "0.1.5") (d (list (d (n "ext-php-rs") (r "^0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "php-tokio-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-pipe") (r "^0.2") (d #t) (k 0)))) (h "1mqx1d7qmq53j418s6b0ilgq7j9x81551kpl848fbzj76fqpd76w")))

(define-public crate-php-tokio-0.1.6 (c (n "php-tokio") (v "0.1.6") (d (list (d (n "ext-php-rs") (r "^0.10.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "php-tokio-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-pipe") (r "^0.2") (d #t) (k 0)))) (h "1la4vpjqj09r7np4fybs0i48rw5xi02jy7wy2c67gz4q1ldka3bj")))

(define-public crate-php-tokio-0.1.7 (c (n "php-tokio") (v "0.1.7") (d (list (d (n "ext-php-rs") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "php-tokio-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-pipe") (r "^0.2") (d #t) (k 0)))) (h "1azkp00nd0dhgz71r0xp58y6c1wn3giz5jl0v7vyq3p8if14b9w9")))

