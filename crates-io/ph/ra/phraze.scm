(define-module (crates-io ph ra phraze) #:use-module (crates-io))

(define-public crate-phraze-0.3.3 (c (n "phraze") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "include-lines") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "0r6pdf3vi1jbkxp7kv2v4xc9d16sz9z0f9cnlvkfgvjrrzpff3aa")))

(define-public crate-phraze-0.3.4 (c (n "phraze") (v "0.3.4") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "include-lines") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "0lp9nhd3hcyzgpail9dl0pbp18gi4kinp7j7rywm1xc88dcr1pw3")))

(define-public crate-phraze-0.3.5 (c (n "phraze") (v "0.3.5") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "include-lines") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "1magbwqmpa2qlvj9aqa0rvp3g2kycvgg808rq7v7q1d43f2ik22q")))

(define-public crate-phraze-0.3.6 (c (n "phraze") (v "0.3.6") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "include-lines") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "06jiymm8rp2kmggi676xls18imy2az7m4mn66g5piaqlh3gq0p35")))

