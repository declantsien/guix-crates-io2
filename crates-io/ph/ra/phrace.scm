(define-module (crates-io ph ra phrace) #:use-module (crates-io))

(define-public crate-phrace-0.1.0 (c (n "phrace") (v "0.1.0") (d (list (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "0hmddmminlmarwfd8qxrnsb8sylgiqrmzadc3zv0yglrkgfa6r9n")))

(define-public crate-phrace-0.2.0 (c (n "phrace") (v "0.2.0") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "0dd62b21aib3ylb7hlhfsakzfbfl92g43q7hkl2gpb0jajgph0nx")))

(define-public crate-phrace-0.2.1 (c (n "phrace") (v "0.2.1") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1qhxavipvb7y8rvxfc4aindnbsd784n5p71s7q42y0vf50j6rsv1")))

(define-public crate-phrace-0.2.2 (c (n "phrace") (v "0.2.2") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1wgw1pxi36wjm50hpba7rw8d5c3gj6k032xzrgqa40yw1m0hxics")))

(define-public crate-phrace-0.2.3 (c (n "phrace") (v "0.2.3") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1grjybvbacm2632awa0zsqkvdhxh6xb31hz9m0p7z5xk4bps2fn2")))

(define-public crate-phrace-0.2.4 (c (n "phrace") (v "0.2.4") (d (list (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "15ik4mvna13mydgajwc87994fnjdsz30gc4k9r6ni01zqs50yjx6")))

