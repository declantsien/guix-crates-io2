(define-module (crates-io ph ra phrases) #:use-module (crates-io))

(define-public crate-phrases-0.1.0 (c (n "phrases") (v "0.1.0") (h "04pqybhcxzqjvq1l24hnkpbfm6kbs4gqc59vv3x4pwrzvc8qpsvk")))

(define-public crate-phrases-0.0.1 (c (n "phrases") (v "0.0.1") (h "0lnf8j221lfh8bcrygrcmwjrfs3638fyy6i130rw6azg27j6ifk6")))

