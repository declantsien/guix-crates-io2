(define-module (crates-io ph oe phoenix-tls) #:use-module (crates-io))

(define-public crate-phoenix-tls-0.1.0 (c (n "phoenix-tls") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "nudge") (r "^0.2") (d #t) (k 0)))) (h "1xlq712d8qdn6693inb9w4wwf12mlkhmd07m854xjk21cjh2zazw") (f (quote (("std" "nudge/std") ("nightly" "nudge/nightly") ("default" "std"))))))

