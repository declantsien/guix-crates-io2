(define-module (crates-io ph oe phoenix) #:use-module (crates-io))

(define-public crate-phoenix-0.1.1 (c (n "phoenix") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "websocket") (r "^0.20") (d #t) (k 0)))) (h "0pqlw4l8f4xnr9jzl44w7z6962nssyfbxbwgiaxlcbbssxqnhs56")))

(define-public crate-phoenix-0.1.2 (c (n "phoenix") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "websocket") (r "^0.20") (d #t) (k 0)))) (h "0zb7v6407anafg589li4fc6shlypfpfmf695cgas0yi3jzsmc81a")))

