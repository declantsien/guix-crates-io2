(define-module (crates-io ph oe phoenix-rs) #:use-module (crates-io))

(define-public crate-phoenix-rs-0.1.0 (c (n "phoenix-rs") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.12") (d #t) (k 0)) (d (n "websocket") (r "^0.20.2") (d #t) (k 0)))) (h "0gg3x9g58wmwq6qp0549g832k564azp906ibpn040bfzi9c3y9r7")))

(define-public crate-phoenix-rs-0.1.1 (c (n "phoenix-rs") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "websocket") (r "^0.20.2") (d #t) (k 0)))) (h "0nl629y0lzc14zxcqxjkx3r327y4pmv61d445cgkz66ki96iamw6")))

