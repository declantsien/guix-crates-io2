(define-module (crates-io ph oe phoenix-rec) #:use-module (crates-io))

(define-public crate-phoenix-rec-0.1.0 (c (n "phoenix-rec") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "0grdylp4l4bs1v5pp8ki3vzs7bcs7dn30khdh4qmzi4w9cry1chc")))

(define-public crate-phoenix-rec-0.2.0 (c (n "phoenix-rec") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "0n9kvil9mzg29ckm3rklw88fi2f3dn7fywq2xc2841w0051r43jy")))

(define-public crate-phoenix-rec-0.2.1 (c (n "phoenix-rec") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "0fci3gi22nq30gymblqmqsg6ar36biwh1vr1zip9adrfz27hay3k")))

(define-public crate-phoenix-rec-0.2.2 (c (n "phoenix-rec") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "1nwnnmapi6a7i2ykw5rfj0gzq77v9krpf6avzwaihrmkmyqpzz6a")))

(define-public crate-phoenix-rec-0.2.3 (c (n "phoenix-rec") (v "0.2.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "0rw2s13vzbjc4v7gmyid3nwmhb6cyvnk28yb0bjp29lr25bs8d06")))

(define-public crate-phoenix-rec-0.2.4 (c (n "phoenix-rec") (v "0.2.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "0gk0x79ss35k2hajxjyw59s95isf2hzhks268q1hcdai41z1qs3k")))

(define-public crate-phoenix-rec-0.2.5 (c (n "phoenix-rec") (v "0.2.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "1dq3myhghyqwksapps6yw8ki14fabbjn0845vhs72bxh58s7pxb3")))

(define-public crate-phoenix-rec-0.2.6 (c (n "phoenix-rec") (v "0.2.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "0aby3v1f78dib9yrpi9pmyydvf91mgpzcrjfnk9q8c972yyh9vq9")))

(define-public crate-phoenix-rec-0.2.7 (c (n "phoenix-rec") (v "0.2.7") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "17sbhw6cbfy01y40z9a86j75cxabvazl5pp3b69wjcx8jm40kb3q")))

(define-public crate-phoenix-rec-0.2.8 (c (n "phoenix-rec") (v "0.2.8") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "05zyk0bcljygn5k6kjxdnyinlj7jdx7b5wd74bgn2703n0pkf2q7")))

(define-public crate-phoenix-rec-0.2.9 (c (n "phoenix-rec") (v "0.2.9") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "06qxb8h8kwp7p63mdj4gygrw5b2qzhjsdhsaiaa3l80yrkpwiqpg")))

(define-public crate-phoenix-rec-0.2.10 (c (n "phoenix-rec") (v "0.2.10") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "0wran6y06sypf2khx1nhwdbjv3ka8g9hw7p8fs0lj2drfnzi3wm9")))

(define-public crate-phoenix-rec-0.2.11 (c (n "phoenix-rec") (v "0.2.11") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "1xm6hzmlh13h4cgjw0zq8mig94k4cipph7vfh67pm59wkvxkakx9")))

(define-public crate-phoenix-rec-0.2.12 (c (n "phoenix-rec") (v "0.2.12") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)))) (h "07lsa2idmc6p2z2kyqq14wgf2nb84s9id95mfk3r1b95ixj6fxgx")))

(define-public crate-phoenix-rec-0.2.13 (c (n "phoenix-rec") (v "0.2.13") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "lz4-compression") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)))) (h "0vid7kkxq8yvm4mh8lwpwgiggwqfrfh2kb6pwlbyvxa4xsvjv4jz")))

