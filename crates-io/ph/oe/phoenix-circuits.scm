(define-module (crates-io ph oe phoenix-circuits) #:use-module (crates-io))

(define-public crate-phoenix-circuits-0.1.0 (c (n "phoenix-circuits") (v "0.1.0") (d (list (d (n "dusk-jubjub") (r "^0.14") (k 0)) (d (n "dusk-plonk") (r "^0.19") (k 0)) (d (n "dusk-poseidon") (r "^0.39") (f (quote ("zk"))) (d #t) (k 0)) (d (n "ff") (r "^0.13") (k 2)) (d (n "jubjub-schnorr") (r "^0.4") (f (quote ("zk"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "phoenix-core") (r "^0.28.0") (d #t) (k 0)) (d (n "poseidon-merkle") (r "^0.6") (f (quote ("rkyv-impl" "zk" "size_32"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "1h3lzghvkzair3mzhlrlr36vf20z9c4pa8wbhg2hq0cgbr9dwca6")))

