(define-module (crates-io ph oe phoenix_editor) #:use-module (crates-io))

(define-public crate-phoenix_editor-0.1.0 (c (n "phoenix_editor") (v "0.1.0") (d (list (d (n "eframe") (r "^0.25.0") (f (quote ("glow" "wayland" "default_fonts"))) (k 0)))) (h "1rnmv3c1hhvldrqzsq2ghp77r83vd0bqm5m2dz06w79w0ijfmg35")))

