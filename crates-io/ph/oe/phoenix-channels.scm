(define-module (crates-io ph oe phoenix-channels) #:use-module (crates-io))

(define-public crate-phoenix-channels-0.1.0 (c (n "phoenix-channels") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "websocket") (r "^0.20.2") (d #t) (k 0)))) (h "1i792q68lbg76czd6dhvvgiwjgnpc6m6n2hvnf2g4zwkxbgbbhfd")))

(define-public crate-phoenix-channels-0.1.1 (c (n "phoenix-channels") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "websocket") (r "^0.20.2") (d #t) (k 0)))) (h "1hacy8za62i7ccz448nhbgnrgrl6fqyqcck95wf8hphgcjr6v96q")))

(define-public crate-phoenix-channels-0.1.2 (c (n "phoenix-channels") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2.2.1") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3.0.2") (d #t) (k 0)) (d (n "websocket") (r "^0.20.2") (d #t) (k 0)))) (h "1smgyfz81gxlx2alwdy3akmlbbqa9yb09f7phslkzjmbj8y3b5i0")))

(define-public crate-phoenix-channels-0.1.3 (c (n "phoenix-channels") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2.2.1") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3.0.2") (d #t) (k 0)) (d (n "websocket") (r "^0.20.2") (d #t) (k 0)))) (h "08gpfymppd7d2pxsjj580g88m7xwfj0szvxz61bq0syk7n2vqf9a")))

(define-public crate-phoenix-channels-0.1.4 (c (n "phoenix-channels") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^4.0.0") (d #t) (k 0)) (d (n "websocket") (r "^0.23.0") (d #t) (k 0)))) (h "0qs6d22sf6qfgmf5v6qb8k8i2aiw51pjw733x6lq6mxafcq7xs2j")))

