(define-module (crates-io ph ys physics3) #:use-module (crates-io))

(define-public crate-physics3-0.1.0 (c (n "physics3") (v "0.1.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cyqdqf13261ib3vkar4abqkglz9a561dl0fpg3g24az4026qnkg")))

(define-public crate-physics3-0.2.0 (c (n "physics3") (v "0.2.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1dqvy2aw0b71b907dx70p3z2g991pwkqml17b5qmsapqjpx28jsh")))

