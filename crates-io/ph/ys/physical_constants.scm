(define-module (crates-io ph ys physical_constants) #:use-module (crates-io))

(define-public crate-physical_constants-0.0.0 (c (n "physical_constants") (v "0.0.0") (h "1cag4x52l0blq5nyrhb7b5ay483zfac2yvxv820fi9cb6m8vgy2f")))

(define-public crate-physical_constants-0.1.0 (c (n "physical_constants") (v "0.1.0") (h "1w4n3srylg8jj1b0f18v08rdlnxbpfsxlli5vfz10ap7q5pgrzsv")))

(define-public crate-physical_constants-0.1.1 (c (n "physical_constants") (v "0.1.1") (h "1sgplpcdickhdn9k55sxk1g87q3khj69wvkm2b6diy2xwczxw29p")))

(define-public crate-physical_constants-0.2.0 (c (n "physical_constants") (v "0.2.0") (h "04c5prr5vwwy43czs0m432cs37b8qw3j65cnwmjvnx25qfipnwvl")))

(define-public crate-physical_constants-0.3.0 (c (n "physical_constants") (v "0.3.0") (h "12a4bha8rs1z9n2hvbcbsfphjpqh4hsbmg01s9lh78i2dyfimlxq")))

(define-public crate-physical_constants-0.4.0 (c (n "physical_constants") (v "0.4.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "08aiz60r129ymrg8w4ryvxam1li8c327gl3xvxvjl7s9v2ddsb17")))

(define-public crate-physical_constants-0.4.1 (c (n "physical_constants") (v "0.4.1") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "1vl4l9fq2xrilmz6vwg5pypzkswprx2h2k1mb6xgdkmkn71mjg1x")))

(define-public crate-physical_constants-0.5.0 (c (n "physical_constants") (v "0.5.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 1)))) (h "0wsc1snyx6fh4hrbzg4mcmim5ld8a7r9513x1d1z5c8rdxax9apk")))

