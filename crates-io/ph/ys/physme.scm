(define-module (crates-io ph ys physme) #:use-module (crates-io))

(define-public crate-physme-0.1.0 (c (n "physme") (v "0.1.0") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "rstar") (r "^0.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "0y48vrzcfmdqbfpn6cddnx2r20kmp8s52rjzqs73k3gidgby15w7")))

(define-public crate-physme-0.2.0 (c (n "physme") (v "0.2.0") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "rstar") (r "^0.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "0md96424vq0axf0y9bcvc8sjs62yq1gx4gbgqn895wcyyidpps9i")))

(define-public crate-physme-0.2.1 (c (n "physme") (v "0.2.1") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "rstar") (r "^0.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "1qrlcd5lj2cryckh0ssfvkyfra77qbz5nl2x08rih4g8g7c8y7s9")))

(define-public crate-physme-0.2.2 (c (n "physme") (v "0.2.2") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "rstar") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "1v8460l2022x9m9l98pagspfsnvaq2l8s4wik3cqm585l5bhnqrv")))

(define-public crate-physme-0.2.3 (c (n "physme") (v "0.2.3") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "rstar") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "0pa660h1y5s59fg950w8x2qfdip5xvf3si4jnh2zq4wl5p5sw8c3")))

(define-public crate-physme-0.2.4 (c (n "physme") (v "0.2.4") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "rstar") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "18631i30l950bdab7svjn6y1wp7y4yg40ynfhilcb14krhbclyhf")))

