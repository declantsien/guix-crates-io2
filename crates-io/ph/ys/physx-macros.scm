(define-module (crates-io ph ys physx-macros) #:use-module (crates-io))

(define-public crate-physx-macros-0.1.0 (c (n "physx-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "0gk1k6bvjwjrkmdinfa2zzfyz29liv61r18nf7kvhi0rardqd4ig")))

(define-public crate-physx-macros-0.1.1 (c (n "physx-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rhrlkchys6imc2iahwnabgi4zk3xdwgdvf82yw16ha2i6dd2ki7")))

