(define-module (crates-io ph ys physfs-sys) #:use-module (crates-io))

(define-public crate-physfs-sys-0.0.0 (c (n "physfs-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "026hi8waz43m7h1l3r1ksrim896avf44yy9zbs27z9hwwriw8kw9") (f (quote (("default" "PHYSFS_ARCHIVE_ZIP") ("PHYSFS_ARCHIVE_ZIP") ("PHYSFS_ARCHIVE_WAD") ("PHYSFS_ARCHIVE_VDF") ("PHYSFS_ARCHIVE_SLB") ("PHYSFS_ARCHIVE_QPAK") ("PHYSFS_ARCHIVE_MVL") ("PHYSFS_ARCHIVE_ISO9660") ("PHYSFS_ARCHIVE_HOG") ("PHYSFS_ARCHIVE_GRP") ("PHYSFS_ARCHIVE_7Z")))) (l "physfs")))

