(define-module (crates-io ph ys physics) #:use-module (crates-io))

(define-public crate-physics-0.1.0 (c (n "physics") (v "0.1.0") (d (list (d (n "collisions") (r "~0.1.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)))) (h "1qj6fxx879fvwkmmpf90ivg0mqby5lbkzpgarg88afvnirbvjhmg")))

(define-public crate-physics-0.1.1 (c (n "physics") (v "0.1.1") (d (list (d (n "collisions") (r "~0.1.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)))) (h "1ajzd04a0alh0v7254l28wh7a96zkcqwzhcnv4mkx7cfanhq84n5")))

(define-public crate-physics-0.1.2 (c (n "physics") (v "0.1.2") (d (list (d (n "collisions") (r "~0.1.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)))) (h "1as9gcvnhx3yyadafmgzbl6sgvfgr7qim6ahg4bxjmv1ljzdr1l7")))

