(define-module (crates-io ph ys physdes-rs) #:use-module (crates-io))

(define-public crate-physdes-rs-0.1.0 (c (n "physdes-rs") (v "0.1.0") (d (list (d (n "intervallum") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0xzar6qs6gbp208k8iyyl1apbnbik8fvqhq6irz6czhzdki2p11z")))

(define-public crate-physdes-rs-0.1.1 (c (n "physdes-rs") (v "0.1.1") (d (list (d (n "intervallum") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0b6mmni8fjvhg94ww6vckh7ar56knrqlarfs0zm17lx4s8g1jgb8")))

