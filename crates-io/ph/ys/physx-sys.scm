(define-module (crates-io ph ys physx-sys) #:use-module (crates-io))

(define-public crate-physx-sys-0.1.0+4.1 (c (n "physx-sys") (v "0.1.0+4.1") (d (list (d (n "build-helper") (r "^0.1.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "18nabrq5nn32g7w1s2r57x2div3495irmfnvhh3cp7d9grl32pvz")))

(define-public crate-physx-sys-0.1.1+4.1 (c (n "physx-sys") (v "0.1.1+4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "1n2n24ni32vqp10qmg3bgsj1kdmszdp3pkwp99sg8zk6a544636j") (y #t)))

(define-public crate-physx-sys-0.1.2+4.1 (c (n "physx-sys") (v "0.1.2+4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "0rbj916jlf8j347jpvc7z5c31gkp5sdbazp0krna3f285lrmlgdq")))

(define-public crate-physx-sys-0.2.0+4.1.1 (c (n "physx-sys") (v "0.2.0+4.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "0llir2ikwzwk6q9q6dbr55c4vnyz6xzilck9k4bqp61xagxg3049")))

(define-public crate-physx-sys-0.2.1+4.1.1 (c (n "physx-sys") (v "0.2.1+4.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "0qa9faiydmchfs57ndqgz47lq6p6316hidzr8rgprww07vza1gm4")))

(define-public crate-physx-sys-0.2.2+4.1.1 (c (n "physx-sys") (v "0.2.2+4.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "0rp9h5l05nysi3jj70xj19hq8xwpaw19rlh2f1qkq4qg64idags3") (y #t)))

(define-public crate-physx-sys-0.2.3+4.1.1 (c (n "physx-sys") (v "0.2.3+4.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "1waz2k293zi0pd2ldbbmjpd8qx6dg5jzar76s2fq5mdv5bp0m1w1")))

(define-public crate-physx-sys-0.2.4+4.1.1 (c (n "physx-sys") (v "0.2.4+4.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "0lppbqqdgcd6j1j7d4px229sz0lzp7fgr7dmfdvyv95pc3kqxjc3")))

(define-public crate-physx-sys-0.3.0 (c (n "physx-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "1zigsyv8y9dfgglqcxymsqn6540sjf1lja6awa2hi1qr0jv7541f")))

(define-public crate-physx-sys-0.4.0 (c (n "physx-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (o #t) (d #t) (k 1)))) (h "0imq71mlwjxcgcjgh1xx2rys237bjia4l32wj0hmihq49apjnrwg") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.1 (c (n "physx-sys") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (o #t) (d #t) (k 1)))) (h "021v6hx15j1x2kx4zp8fw8gi3ag2883jlmi2k1r9c7k9zp6mgq5y") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.2 (c (n "physx-sys") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (o #t) (d #t) (k 1)))) (h "12p77cnljcx1cbrc0q8mv1rxkkwmibsqrmcjk6cnh5rw7qjrpc4n") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.3 (c (n "physx-sys") (v "0.4.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (o #t) (d #t) (k 1)))) (h "13kaph2ilbk19z0qh4i3lwvhj7ddnj5innk17z8plb1j53c60svh") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.4 (c (n "physx-sys") (v "0.4.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (o #t) (d #t) (k 1)))) (h "0bqb67dkpqkbjfs5999v4s4ckrf5k9k7bnp056q8ryv98jxn75vm") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.5 (c (n "physx-sys") (v "0.4.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (o #t) (d #t) (k 1)))) (h "1d32ppxgkg7ha25w8lg6zc932wp9iv52w97cgx306qpacbl8shhk") (f (quote (("use-cmake" "cmake") ("structgen")))) (y #t)))

(define-public crate-physx-sys-0.4.6 (c (n "physx-sys") (v "0.4.6") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (o #t) (d #t) (k 1)))) (h "0y7h93x4wz7nl0wmb7mczl8ac9vs6781dc6bpflqab611g537vz0") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.7 (c (n "physx-sys") (v "0.4.7") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (o #t) (d #t) (k 1)))) (h "05ndcampjh7n2gh1f7if13a7g5jsqqwwzyn1snnk0bsigzy1xv1i") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.8 (c (n "physx-sys") (v "0.4.8") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (o #t) (d #t) (k 1)))) (h "1v47ibrawdax84pg07y0mpb472vh4jw6384zrcnyw6jxl4j2z5jw") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.9 (c (n "physx-sys") (v "0.4.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)))) (h "0lrvd7c2svkw4fiwxgf10jcb7ab9pxxxlfl6sfamx7fpgiz6h6lx") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.10 (c (n "physx-sys") (v "0.4.10") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)))) (h "1vmvzj2incpkxliajvcd1g9xll9crcg9vm2q0p81snqfgdlzv33j") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.11 (c (n "physx-sys") (v "0.4.11") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)))) (h "0pccl54x3giw04cm3djrm4w7has0g3x5sk7bg2hwq099ypmfpp78") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.12 (c (n "physx-sys") (v "0.4.12") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)))) (h "1m4ql4383f4nr011can1f049cj6679c62alcijm63crccpamm1c3") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.14 (c (n "physx-sys") (v "0.4.14") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)))) (h "0zm6px3syqvqqylnz2ikpd1n5ddhlg7clakyirk249k5bp91xbfc") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.15 (c (n "physx-sys") (v "0.4.15") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)))) (h "1p2inn941bn532d8izjz9jgxsvr5vm8icv6fr12q47ar77bqs58s") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.4.16 (c (n "physx-sys") (v "0.4.16") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)))) (h "09xchiz9whx08nch8zs2ycc8wpp31mv1pzwlp65acdlmailkghar") (f (quote (("use-cmake" "cmake") ("structgen"))))))

(define-public crate-physx-sys-0.5.0 (c (n "physx-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0hvrkb45k3w606jpzr1x6f25v43jk734i36aanm3rm7jy8l316fi") (f (quote (("structgen"))))))

(define-public crate-physx-sys-0.6.0 (c (n "physx-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "15xl5ip4y97zzk507bx78yavjxp3an7b842hm6cfkfxb727gkawz") (f (quote (("structgen") ("profile"))))))

(define-public crate-physx-sys-0.7.0 (c (n "physx-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "10sapv5l4d1s6bby50aqbbh1dpsdh5w8pqc9jxjipw3qjb1i9cfh") (f (quote (("structgen") ("profile"))))))

(define-public crate-physx-sys-0.8.0 (c (n "physx-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0c7km47qg4x7bh1djj7gcy6bksxh9lj5vccp4d811bcaiga51flr") (f (quote (("structgen") ("profile"))))))

(define-public crate-physx-sys-0.8.1 (c (n "physx-sys") (v "0.8.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "06rs4yld548pz32qgqnx4ay265b5x9r7gqlp3wn25jncsdqn0iqr") (f (quote (("structgen") ("profile"))))))

(define-public crate-physx-sys-0.8.2 (c (n "physx-sys") (v "0.8.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "06alb3v302zk69am64330hd9zb2dfcin7vkl4mfal6x7pzqlpfrm") (f (quote (("structgen") ("profile"))))))

(define-public crate-physx-sys-0.10.0 (c (n "physx-sys") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1ihwz9n77jcac5f4dkmvkzm3m1vx46s7idwwc1hf0g3q06lvs5qn") (f (quote (("structgen") ("profile") ("debug-structs") ("cpp-warnings"))))))

(define-public crate-physx-sys-0.11.0 (c (n "physx-sys") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0zaph4pxbi7f6hhwsns6jy7ffyb4yw39bqph1wpp8yxa59rjqrj5") (f (quote (("structgen") ("profile") ("debug-structs") ("cpp-warnings"))))))

(define-public crate-physx-sys-0.11.1 (c (n "physx-sys") (v "0.11.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0g5ywf6gxdjwgndx81ajdsv1hmv0dazd9pmr6jdnaq4rsswqj353") (f (quote (("structgen") ("profile") ("debug-structs") ("cpp-warnings"))))))

(define-public crate-physx-sys-0.11.2 (c (n "physx-sys") (v "0.11.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "033byl8jxp4fddw05k9xds6brhnhmf4cmjqvlxb6y6zzld7pn4ya") (f (quote (("structgen") ("profile") ("debug-structs") ("cpp-warnings")))) (y #t)))

(define-public crate-physx-sys-0.11.3 (c (n "physx-sys") (v "0.11.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1lwsf4c90g3kb3rqbjfyyyf1b5csz9fcdac4mg2ckg3qi9bln86a") (f (quote (("structgen") ("profile") ("debug-structs") ("cpp-warnings"))))))

(define-public crate-physx-sys-0.11.4 (c (n "physx-sys") (v "0.11.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0y7mfjcj1i3qm5yr8ncxjj16azvdsf01l9k1xz3z5zk68fs7ajbz") (f (quote (("structgen") ("profile") ("debug-structs") ("cpp-warnings"))))))

(define-public crate-physx-sys-0.11.5 (c (n "physx-sys") (v "0.11.5") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "12kmf0149arb5bxffs4df046g8aa4hpxmxbv1xvsi1k7dkbh2lkf") (f (quote (("structgen") ("profile") ("debug-structs") ("cpp-warnings"))))))

