(define-module (crates-io ph ys physpatch) #:use-module (crates-io))

(define-public crate-physpatch-0.1.0 (c (n "physpatch") (v "0.1.0") (d (list (d (n "aobscan") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta10") (d #t) (k 0)) (d (n "memflow-qemu") (r "^0.2.0-beta10") (d #t) (k 0)) (d (n "memflow-win32") (r "^0.2.0-beta10") (d #t) (k 0)) (d (n "x86") (r "^0.52.0") (d #t) (k 0)))) (h "0hdailq3pdkcywrikjkn9ciyj9njhsrz99bbwx5fg23m3myg3l8p")))

(define-public crate-physpatch-0.2.0 (c (n "physpatch") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "aobscan") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta10") (d #t) (k 0)) (d (n "memflow-qemu") (r "^0.2.0-beta10") (d #t) (k 0)) (d (n "memflow-win32") (r "^0.2.0-beta10") (d #t) (k 0)) (d (n "x86") (r "^0.52.0") (d #t) (k 0)))) (h "1r3d95d18jmrv6qjdinaps9ajw9pqfafaf1hvkp36dbrzv22w78q")))

