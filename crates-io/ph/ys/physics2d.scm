(define-module (crates-io ph ys physics2d) #:use-module (crates-io))

(define-public crate-physics2d-0.1.0 (c (n "physics2d") (v "0.1.0") (d (list (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "16641b446wq8f2d42b5z1n7hbzbaxh13d1cmv5m9dfdjzngxj5vk")))

(define-public crate-physics2d-0.1.1 (c (n "physics2d") (v "0.1.1") (d (list (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "0gi4rc61x0z9cq9aambk5chgyvj2baqa23kgykvi9dzi14kyayfy")))

(define-public crate-physics2d-0.1.2 (c (n "physics2d") (v "0.1.2") (d (list (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "0qkaflzvsvh8mdwm0j5y05w0zax51iwrq7ng65575nz33vh1xp7j")))

(define-public crate-physics2d-0.2.0 (c (n "physics2d") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "0f4k8v7fyqy9zq0kl0hd6j9nm3nykg1vx6dmqa52wyg46jjrqyr0")))

(define-public crate-physics2d-0.3.0 (c (n "physics2d") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "1yykbam9q5pslcyv1wxbw3v2q6is0ixw1q4mslsg6lwfgnbxgi35")))

(define-public crate-physics2d-0.3.1 (c (n "physics2d") (v "0.3.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "1smc25z0f6ixzzqzlmhjxhxnwr3vzx973l19kfzaqdjf8m40zf8z")))

(define-public crate-physics2d-0.4.0 (c (n "physics2d") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "0p9jlbrk83g4gvpbxlz6pyrz4hjf3nr5qzsnz4nnj3q4ihv695ly")))

(define-public crate-physics2d-0.4.1 (c (n "physics2d") (v "0.4.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "1jg134zi5sbp957xa6nif40imwj68f384s94n1991zdx0v78z0v6") (y #t)))

(define-public crate-physics2d-0.5.0 (c (n "physics2d") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "09g6k99d4ng9n0y71bhhm8pq5cmsqa302vjd9idmxxwx5jc84590")))

(define-public crate-physics2d-0.6.0 (c (n "physics2d") (v "0.6.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "sfml") (r "^0.14.0") (d #t) (k 2)))) (h "0d7349m62jvg7an30fxx61dahnwyga7wbgmxpabb4rr4mg6kvcw5")))

