(define-module (crates-io ph p_ php_serde) #:use-module (crates-io))

(define-public crate-php_serde-0.6.0 (c (n "php_serde") (v "0.6.0") (d (list (d (n "bson") (r "^0.14.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0nrj8x6qv7vr6adg4pyfc6zdny032mjnp9j9vgbp8lx24ysw8a9d")))

