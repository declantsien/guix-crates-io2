(define-module (crates-io ph pf phpfmt-lib) #:use-module (crates-io))

(define-public crate-phpfmt-lib-0.0.0-b1 (c (n "phpfmt-lib") (v "0.0.0-b1") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.151") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "1qhihwjiy4ccsqzamiq10qszwgi58ypx4ps2k0kx6mrh4xp782c1")))

