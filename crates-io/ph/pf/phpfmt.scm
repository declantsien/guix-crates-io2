(define-module (crates-io ph pf phpfmt) #:use-module (crates-io))

(define-public crate-phpfmt-0.0.0-b1 (c (n "phpfmt") (v "0.0.0-b1") (d (list (d (n "cmder") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "phpfmt-lib") (r "^0.0.0-b1") (d #t) (k 0)))) (h "0x9m3waw3q8wp64s3cw4pxhbrgsgbx60cd0dj9982b2fymxlry6j")))

