(define-module (crates-io ph yl phyloalifold) #:use-module (crates-io))

(define-public crate-phyloalifold-0.1.0 (c (n "phyloalifold") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "phylofold") (r "^0.1") (d #t) (k 0)) (d (n "phyloprob") (r "^0.1") (d #t) (k 0)) (d (n "rna-algos") (r "^0.1") (d #t) (k 0)))) (h "0pn90rqcn37gm6b02jw89hp2mrp7r63djhf658ac1br3rph0alxp")))

(define-public crate-phyloalifold-0.1.1 (c (n "phyloalifold") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "phylofold") (r "^0.1") (d #t) (k 0)) (d (n "phyloprob") (r "^0.1") (d #t) (k 0)) (d (n "rna-algos") (r "^0.1") (d #t) (k 0)))) (h "0r729ylgpwvyxzw755xcknzhshij9wz9icdr214q8kggk6ycphvp")))

(define-public crate-phyloalifold-0.1.2 (c (n "phyloalifold") (v "0.1.2") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "phylofold") (r "^0.1") (d #t) (k 0)) (d (n "phyloprob") (r "^0.1") (d #t) (k 0)))) (h "10mdj14gki7la919nsp68in31izz93s6dc0b2fsnwx0pdnsymxac")))

(define-public crate-phyloalifold-0.1.3 (c (n "phyloalifold") (v "0.1.3") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "phylofold") (r "^0.1") (d #t) (k 0)) (d (n "phyloprob") (r "^0.1") (d #t) (k 0)))) (h "1d9nf0p78b1fgf1qy9ia3fg3ddm48d7jgkyicrz95a0xzadqqi6i")))

