(define-module (crates-io ph yl phylogeny) #:use-module (crates-io))

(define-public crate-phylogeny-0.0.1 (c (n "phylogeny") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml" "color" "suggestions"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23.0") (d #t) (k 0)))) (h "1vrc3w8kbj9bplh48jd54s3ii7bjwa55mpkflqfnp1q1jy3inr8w")))

