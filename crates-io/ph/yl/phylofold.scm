(define-module (crates-io ph yl phylofold) #:use-module (crates-io))

(define-public crate-phylofold-0.1.0 (c (n "phylofold") (v "0.1.0") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "phyloprob") (r "^0.1") (d #t) (k 0)) (d (n "rna-algos") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1fygxj383cnhw6hlc24dw84fxgzw0hz2fm6dgwwswgh87px53bpx")))

(define-public crate-phylofold-0.1.1 (c (n "phylofold") (v "0.1.1") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "phyloprob") (r "^0.1") (d #t) (k 0)))) (h "1pwa5a81s5whbnvrfs24dvvfh06gds9q6q8669mlnhhxh2k8nfdf")))

(define-public crate-phylofold-0.1.2 (c (n "phylofold") (v "0.1.2") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "phyloprob") (r "^0.1") (d #t) (k 0)))) (h "0z1cc3fmp5min6dys2z70byk4km4lbnic8dcb8748wq9g9vwrwyl")))

