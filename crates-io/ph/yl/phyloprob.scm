(define-module (crates-io ph yl phyloprob) #:use-module (crates-io))

(define-public crate-phyloprob-0.1.0 (c (n "phyloprob") (v "0.1.0") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rna-algos") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0jmvmnqaqixsharxk9dfl98xckavm7i6whldr4cbrsy4hxcjrh9l")))

(define-public crate-phyloprob-0.1.1 (c (n "phyloprob") (v "0.1.1") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rna-algos") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "14asy8m7j4p68mg8v3q55jagr5az1ysmp396wdgpcv61ssqlv04y")))

(define-public crate-phyloprob-0.1.2 (c (n "phyloprob") (v "0.1.2") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rna-algos") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "129z2p5khxv4c7rlhqzry2jv1241yljd13zwdjm3wdbq6zds69w6")))

