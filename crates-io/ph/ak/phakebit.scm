(define-module (crates-io ph ak phakebit) #:use-module (crates-io))

(define-public crate-phakebit-0.1.0 (c (n "phakebit") (v "0.1.0") (d (list (d (n "cargo-readme") (r "^3.3.0") (d #t) (k 2)) (d (n "circular-buffer") (r "^0.1.3") (d #t) (k 2)))) (h "0y3ak6anh705f6lpi9dnis6819wdka59i3c9vc4nzhxkxcz0ybx1")))

(define-public crate-phakebit-0.1.1 (c (n "phakebit") (v "0.1.1") (d (list (d (n "cargo-readme") (r "^3.3.0") (d #t) (k 2)) (d (n "circular-buffer") (r "^0.1.3") (d #t) (k 2)))) (h "01k3y1zyzq4vvcf171zj3ms4b21g63a6rb3ah0jc0vpjz9zvqqs6")))

(define-public crate-phakebit-0.1.2 (c (n "phakebit") (v "0.1.2") (d (list (d (n "cargo-readme") (r "^3.3.0") (d #t) (k 2)) (d (n "circular-buffer") (r "^0.1.3") (d #t) (k 2)))) (h "0fkvwqvgr3rn7qbjs4yjrl2znfi6x7prh9ywjr76qis7xxvlgp5s")))

(define-public crate-phakebit-0.1.3 (c (n "phakebit") (v "0.1.3") (d (list (d (n "cargo-readme") (r "^3.3.0") (d #t) (k 2)) (d (n "circular-buffer") (r "^0.1.3") (d #t) (k 2)))) (h "0j9vk4g4g4q1kldfpijw752smi89nsadpm91qi5rksfgyvv9yjna")))

(define-public crate-phakebit-0.1.4 (c (n "phakebit") (v "0.1.4") (d (list (d (n "cargo-readme") (r "^3.3.0") (d #t) (k 2)) (d (n "circular-buffer") (r "^0.1.3") (d #t) (k 2)))) (h "07i8rdww8w880s77bc1wz2yjgjj48pj389yp20hi8vx022f2ymn4")))

