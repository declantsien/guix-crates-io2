(define-module (crates-io ph ak phaktionz) #:use-module (crates-io))

(define-public crate-phaktionz-0.4.0 (c (n "phaktionz") (v "0.4.0") (d (list (d (n "mkproj-lib") (r "^0.1.5") (d #t) (k 0)) (d (n "run_script") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0p0lywi3flpm6h0bw36sq5rs5nc2qxcijc0l7pfqa21ncnwb3fd0") (y #t)))

(define-public crate-phaktionz-0.4.1 (c (n "phaktionz") (v "0.4.1") (d (list (d (n "mkproj-lib") (r "^0.1.5") (d #t) (k 0)) (d (n "run_script") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1pyrf43r3yww310wbwjivdzwn2imfwk0m9r2hs3hz2lc4w0ar2di") (y #t)))

(define-public crate-phaktionz-0.4.2 (c (n "phaktionz") (v "0.4.2") (d (list (d (n "mkproj-lib") (r "^0.1.5") (d #t) (k 0)) (d (n "run_script") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0r0gh8qlics6a9ijkj949422bd9kj4mmvhj2b6w1yail98rxz82i") (y #t)))

(define-public crate-phaktionz-0.5.3 (c (n "phaktionz") (v "0.5.3") (d (list (d (n "mkproj-lib") (r "^0.1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "00p5d6i8zrxcc3gv97h29nxa72ccsg2mf5fgmsi0pim2s6ff2viz") (y #t)))

(define-public crate-phaktionz-1.6.0 (c (n "phaktionz") (v "1.6.0") (d (list (d (n "mkproj-lib") (r "^0.2.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0niqjhh12zd2c9k8i2dh3gp0gw8qvp10c9a125kklryrh0rh303i") (y #t)))

