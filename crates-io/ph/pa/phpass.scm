(define-module (crates-io ph pa phpass) #:use-module (crates-io))

(define-public crate-phpass-0.1.0 (c (n "phpass") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)))) (h "1nkpzbp9vbx4lsdqwinb3awrkpkzsnqvw7gkvmrdv7nichll8wba")))

(define-public crate-phpass-0.1.1 (c (n "phpass") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)))) (h "1amnywx5s3pd84d2j0nmpzxdbbfldfpcw8hznrqqpyvpgq7ayzvp")))

(define-public crate-phpass-0.1.2 (c (n "phpass") (v "0.1.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "11hjrqrd4lwip6dmxz2gkk59j7skw5a08m359l06rvz7zkm0g7zl")))

(define-public crate-phpass-0.1.3 (c (n "phpass") (v "0.1.3") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0bh939c95mz3r6kpi91id2mjka7l7km2zs148dsi5qiswkzvghv8")))

