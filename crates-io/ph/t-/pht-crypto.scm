(define-module (crates-io ph t- pht-crypto) #:use-module (crates-io))

(define-public crate-pht-crypto-0.1.0-alpha (c (n "pht-crypto") (v "0.1.0-alpha") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "glass_pumpkin") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rug") (r "^1.13.0") (f (quote ("integer" "rand" "serde"))) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fzhvph4h3n20qvbx5gm082mlzk0dm0ygqdh662l0w7208wmxypx")))

