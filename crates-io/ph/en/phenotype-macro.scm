(define-module (crates-io ph en phenotype-macro) #:use-module (crates-io))

(define-public crate-phenotype-macro-0.1.0 (c (n "phenotype-macro") (v "0.1.0") (d (list (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing" "visit"))) (d #t) (k 0)))) (h "1h7z5hv12lvfgk5rj3ib6y254ffj8mspgsfmv0a8kaswdbnq973v")))

(define-public crate-phenotype-macro-0.1.1 (c (n "phenotype-macro") (v "0.1.1") (d (list (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing" "visit"))) (d #t) (k 0)))) (h "1pnphnsm3yry1r4pv6pab5148b76lm7j1gbd1h0as9fi9vaplm1g")))

(define-public crate-phenotype-macro-0.1.2 (c (n "phenotype-macro") (v "0.1.2") (d (list (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing" "visit"))) (d #t) (k 0)))) (h "1rdldff5i36gcbs3b94fz5w27hzc2fixrxlvdlgpjq6dm3qhk05x")))

(define-public crate-phenotype-macro-0.1.3 (c (n "phenotype-macro") (v "0.1.3") (d (list (d (n "phenotype-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing" "visit"))) (d #t) (k 0)))) (h "1gh1wbh9j4awvd6cgjfv4p050l4ikcrza63avlnfv0iqf7dnf0vx")))

(define-public crate-phenotype-macro-0.1.4 (c (n "phenotype-macro") (v "0.1.4") (d (list (d (n "phenotype-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "printing" "visit"))) (d #t) (k 0)))) (h "0cqgzn0iw6cw2dknp7cz7nwm9r5am6dchw5r7lpzpq7p9akvm42i")))

