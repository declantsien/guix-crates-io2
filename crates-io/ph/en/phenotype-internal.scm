(define-module (crates-io ph en phenotype-internal) #:use-module (crates-io))

(define-public crate-phenotype-internal-0.1.0 (c (n "phenotype-internal") (v "0.1.0") (h "1sxq5dnz0cqx50c4pzcsn4qlh3ay8ai7yph8xdm49i4vh5h4p1ia")))

(define-public crate-phenotype-internal-0.2.0 (c (n "phenotype-internal") (v "0.2.0") (h "1bbirx7nnywhhrs8h11sll3khlshgi2vb1ijlnlcady7dbyfr60y")))

