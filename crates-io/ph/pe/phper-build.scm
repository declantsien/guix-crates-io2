(define-module (crates-io ph pe phper-build) #:use-module (crates-io))

(define-public crate-phper-build-0.0.0 (c (n "phper-build") (v "0.0.0") (h "1mynz92s5fizyg8lxdc0myiga0a6qycf5fzb73smfa7qggmvhlb6")))

(define-public crate-phper-build-0.2.0-alpha.1 (c (n "phper-build") (v "0.2.0-alpha.1") (d (list (d (n "phper-sys") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "11pz1p1qfp8d75hiv1qi1j5k0kix91wa0g45xrw892434ppb6d36")))

(define-public crate-phper-build-0.2.0-alpha.2 (c (n "phper-build") (v "0.2.0-alpha.2") (d (list (d (n "phper-sys") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "0fw5bsn1xbfrwfqkcn4d65xac56i3v5vswnjl17kxf8w396n41z1")))

(define-public crate-phper-build-0.2.0-alpha.3 (c (n "phper-build") (v "0.2.0-alpha.3") (d (list (d (n "phper-sys") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "10s99wa7f9g1jcqgcx3kxwzcfv3pz0bjldiq2f0qlvxmydbmhnrv")))

(define-public crate-phper-build-0.2.0 (c (n "phper-build") (v "0.2.0") (d (list (d (n "phper-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1y8q30m9lwb677nl3j2725xqhnnss6swai388f8zibrar8jxn3h2")))

(define-public crate-phper-build-0.3.0 (c (n "phper-build") (v "0.3.0") (d (list (d (n "phper-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1l701c9hayfa61zbzl3y2f3g145szx2d2jk7x0vbnw7li91qipym") (r "1.56")))

(define-public crate-phper-build-0.4.0-alpha.1 (c (n "phper-build") (v "0.4.0-alpha.1") (d (list (d (n "phper-sys") (r "^0.4.0-alpha.1") (d #t) (k 0)))) (h "1q1861jii0ia19q2f046gkz1ph8q7z9vsdfzq7zadw0shbkfgspp") (r "1.56")))

(define-public crate-phper-build-0.4.0-alpha.2 (c (n "phper-build") (v "0.4.0-alpha.2") (d (list (d (n "phper-sys") (r "^0.4.0-alpha.2") (d #t) (k 0)))) (h "1dii26gz6b1x3dx5cm4ir3z5is6yp378m9pzk654w5nzfpwbhp73") (r "1.56")))

(define-public crate-phper-build-0.4.0-alpha.3 (c (n "phper-build") (v "0.4.0-alpha.3") (d (list (d (n "phper-sys") (r "^0.4.0-alpha.3") (d #t) (k 0)))) (h "1bycfdvx8yxy1irr5xima9a51i9hybnv27zgsrnvm4x70fvp8ycz") (r "1.56")))

(define-public crate-phper-build-0.4.0-alpha.4 (c (n "phper-build") (v "0.4.0-alpha.4") (d (list (d (n "phper-sys") (r "^0.4.0-alpha.4") (d #t) (k 0)))) (h "0rycazlannx3jzrv85xcm8w6j046al3axkjv2hj1d99rpaq30qra") (r "1.56")))

(define-public crate-phper-build-0.4.0 (c (n "phper-build") (v "0.4.0") (d (list (d (n "phper-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0wav3n37cddh94fqp90q3lq869h68bjm99225jyfrbc7szprj65h") (r "1.56")))

(define-public crate-phper-build-0.5.0 (c (n "phper-build") (v "0.5.0") (d (list (d (n "phper-sys") (r "^0.5.0") (d #t) (k 0)))) (h "039f8221mc5a9wpxzl68zxa1wbiqz2z3xnjyjmqwgn70hm8632dn") (r "1.56")))

(define-public crate-phper-build-0.5.1 (c (n "phper-build") (v "0.5.1") (d (list (d (n "phper-sys") (r "^0.5.1") (d #t) (k 0)))) (h "1pa2d5w239ly11hrl8l15n5zy0ccn0rqgb2kf5aisl69q2rbvich") (r "1.56")))

(define-public crate-phper-build-0.6.0 (c (n "phper-build") (v "0.6.0") (d (list (d (n "phper-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1h0cx3liwdrn6jllvky3ybhby4ydi0cb26vci3c4w43b5iivjix6") (r "1.65")))

(define-public crate-phper-build-0.7.0 (c (n "phper-build") (v "0.7.0") (d (list (d (n "phper-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0k3wcfdvqx80fl8l7vi4q7hrk5kgn58zky272kxcy168r44crmxp") (r "1.65")))

(define-public crate-phper-build-0.8.0 (c (n "phper-build") (v "0.8.0") (d (list (d (n "phper-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0xfh15b4zp06xlr5asjf358dbqwnrmm4cx15izmazm1d1nn3l583") (r "1.65")))

(define-public crate-phper-build-0.9.0 (c (n "phper-build") (v "0.9.0") (d (list (d (n "phper-sys") (r "^0.9.0") (d #t) (k 0)))) (h "118wmmzqngc6c0cv821ipkjf1h431fraka2y34rv200ngkwga8fm") (r "1.65")))

(define-public crate-phper-build-0.10.0 (c (n "phper-build") (v "0.10.0") (d (list (d (n "phper-sys") (r "^0.10.0") (d #t) (k 0)))) (h "1q7rnjadbgfy9z78b63fhcfgqy08fi4ndyymavib6vh68ijd63bc") (r "1.65")))

(define-public crate-phper-build-0.10.1 (c (n "phper-build") (v "0.10.1") (d (list (d (n "phper-sys") (r "^0.10.1") (d #t) (k 0)))) (h "1s9zhqs3v0rs47sq783xs4wmia265sgdq0bjw32z49c2l4pc6s7y") (r "1.65")))

(define-public crate-phper-build-0.10.2 (c (n "phper-build") (v "0.10.2") (d (list (d (n "phper-sys") (r "^0.10.2") (d #t) (k 0)))) (h "0dlcq108ikkv567g5jlks55ic6s0b2rwax4p5ar70wjpmc223hlh") (r "1.65")))

(define-public crate-phper-build-0.11.0 (c (n "phper-build") (v "0.11.0") (d (list (d (n "phper-sys") (r "^0.11.0") (d #t) (k 0)))) (h "1kqwabba90669v9mlj7k6ml5b44x306wwakrm7sj4gcpa4494pm1") (r "1.65")))

(define-public crate-phper-build-0.11.1 (c (n "phper-build") (v "0.11.1") (d (list (d (n "phper-sys") (r "^0.11.0") (d #t) (k 0)))) (h "1l3af375rmqx4z1v53iv545pvgmqahg6v4h9n27mws85n2mmqa48") (r "1.65")))

(define-public crate-phper-build-0.12.0 (c (n "phper-build") (v "0.12.0") (d (list (d (n "phper-sys") (r "^0.12.0") (d #t) (k 0)))) (h "0k83yw53bal0dqfvk53drpwxcl1kdmcafsb0ca5a7njkjjdj9ij6") (r "1.65")))

(define-public crate-phper-build-0.13.0 (c (n "phper-build") (v "0.13.0") (d (list (d (n "phper-sys") (r "^0.13.0") (d #t) (k 0)))) (h "10w1jk9i3gx9xch3nrsqbw7r88jf1iraq17h3vfvhkxgkfmja2by") (r "1.67")))

(define-public crate-phper-build-0.13.1 (c (n "phper-build") (v "0.13.1") (d (list (d (n "phper-sys") (r "^0.13.1") (d #t) (k 0)))) (h "1kcr3cyfyg4ikby42bix49zr5dq5q27niryzdy3s9yy7gx9yg32r") (r "1.65")))

