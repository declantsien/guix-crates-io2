(define-module (crates-io ph pe phper-alloc) #:use-module (crates-io))

(define-public crate-phper-alloc-0.0.0 (c (n "phper-alloc") (v "0.0.0") (h "0ibnk4kpxmsiszjrcdcyjkkn2xmgdifp07lqrqvr2gjnrihr98wh")))

(define-public crate-phper-alloc-0.2.0-alpha.1 (c (n "phper-alloc") (v "0.2.0-alpha.1") (d (list (d (n "phper-build") (r "^0.2.0-alpha.1") (d #t) (k 1)) (d (n "phper-sys") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "1s3pbf98wvakazfmdmw6kq32prl9w2cf1v6x5fygzf98risp0la1")))

(define-public crate-phper-alloc-0.2.0-alpha.2 (c (n "phper-alloc") (v "0.2.0-alpha.2") (d (list (d (n "phper-build") (r "^0.2.0-alpha.2") (d #t) (k 1)) (d (n "phper-sys") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "0nyhp51jxqsa8wp0f1iic7myqafbfs2bcql9ycdyi5340iwqncwl")))

(define-public crate-phper-alloc-0.2.0-alpha.3 (c (n "phper-alloc") (v "0.2.0-alpha.3") (d (list (d (n "phper-build") (r "^0.2.0-alpha.3") (d #t) (k 1)) (d (n "phper-sys") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "08yzkjxixby1589c2bhc7sjxpijkla35mqkxzp8v2m31whbas815")))

(define-public crate-phper-alloc-0.2.0 (c (n "phper-alloc") (v "0.2.0") (d (list (d (n "phper-build") (r "^0.2.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0pr5vj40y7p7y3hncb63cnqncv4ilyrc6li49ls9jmp34fk43shy")))

(define-public crate-phper-alloc-0.3.0 (c (n "phper-alloc") (v "0.3.0") (d (list (d (n "phper-build") (r "^0.3.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1r7laanq8m696kxjjima2k4k9r2y3n7l5jmj4kbmf4fvcx4chnj8") (r "1.56")))

(define-public crate-phper-alloc-0.4.0-alpha.1 (c (n "phper-alloc") (v "0.4.0-alpha.1") (d (list (d (n "phper-build") (r "^0.4.0-alpha.1") (d #t) (k 1)) (d (n "phper-sys") (r "^0.4.0-alpha.1") (d #t) (k 0)))) (h "03m509lld8sm0yqi18l47wnmanfsj3m10zbmfxprp886b9219wh5") (r "1.56")))

(define-public crate-phper-alloc-0.4.0-alpha.2 (c (n "phper-alloc") (v "0.4.0-alpha.2") (d (list (d (n "phper-build") (r "^0.4.0-alpha.2") (d #t) (k 1)) (d (n "phper-sys") (r "^0.4.0-alpha.2") (d #t) (k 0)))) (h "06wy38vk0j18n2bk2v2qd16df782sydxawsidd2sl94qsmaj4c74") (r "1.56")))

(define-public crate-phper-alloc-0.4.0-alpha.3 (c (n "phper-alloc") (v "0.4.0-alpha.3") (d (list (d (n "phper-build") (r "^0.4.0-alpha.3") (d #t) (k 1)) (d (n "phper-sys") (r "^0.4.0-alpha.3") (d #t) (k 0)))) (h "00qghiqn0179gsia8z4m5l4x04gsl0p0lhvlgf7ak059li48c0hj") (r "1.56")))

(define-public crate-phper-alloc-0.4.0-alpha.4 (c (n "phper-alloc") (v "0.4.0-alpha.4") (d (list (d (n "phper-build") (r "^0.4.0-alpha.4") (d #t) (k 1)) (d (n "phper-sys") (r "^0.4.0-alpha.4") (d #t) (k 0)))) (h "05c78qwzgf4xw5pxkxlks10ddx5g5ks211ck92y3km8qh7p120hj") (r "1.56")))

(define-public crate-phper-alloc-0.4.0 (c (n "phper-alloc") (v "0.4.0") (d (list (d (n "phper-build") (r "^0.4.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0na5fxn1f7v2w2xd7r8ricxrhmjir9rcas3nqsv74ans2nw2lnd5") (r "1.56")))

(define-public crate-phper-alloc-0.5.0 (c (n "phper-alloc") (v "0.5.0") (d (list (d (n "phper-build") (r "^0.5.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0cn8bj5pdkpi0133zys47i465v4iy9j3dahfa403bys0bmnplq4k") (r "1.56")))

(define-public crate-phper-alloc-0.5.1 (c (n "phper-alloc") (v "0.5.1") (d (list (d (n "phper-build") (r "^0.5.1") (d #t) (k 1)) (d (n "phper-sys") (r "^0.5.1") (d #t) (k 0)))) (h "1ph8s4wjwk8fcn8nqpyvdcpnzq9q33fp0n7bg6p7gk953vzchsxm") (r "1.56")))

(define-public crate-phper-alloc-0.6.0 (c (n "phper-alloc") (v "0.6.0") (d (list (d (n "phper-build") (r "^0.6.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1krp1l4qajrma3xnyj1xjv835sx90xr9mqv2m1cr52gi1x6cszqp") (r "1.65")))

(define-public crate-phper-alloc-0.7.0 (c (n "phper-alloc") (v "0.7.0") (d (list (d (n "phper-build") (r "^0.7.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.7.0") (d #t) (k 0)))) (h "15id0ffl9ns6v2pglsbwzs20xlnzzp1ch99ljsknd8rsq03xikhj") (r "1.65")))

(define-public crate-phper-alloc-0.8.0 (c (n "phper-alloc") (v "0.8.0") (d (list (d (n "phper-build") (r "^0.8.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0jgxwm5g6ylyy37v8mmfaq64dsbvqd7rxjgmizgrv7ap64myp7rz") (r "1.65")))

(define-public crate-phper-alloc-0.9.0 (c (n "phper-alloc") (v "0.9.0") (d (list (d (n "phper-build") (r "^0.9.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.9.0") (d #t) (k 0)))) (h "0h2bji3zbf12lzhhf9c7nhmmi9v2h74a7qj20x4pz41fkvyqcy5w") (r "1.65")))

(define-public crate-phper-alloc-0.10.0 (c (n "phper-alloc") (v "0.10.0") (d (list (d (n "phper-build") (r "^0.10.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.10.0") (d #t) (k 0)))) (h "0756mcpzqkylmvfr8lwsgrmhlpj3lkjmsndv25vjnr6rp5ik16yx") (r "1.65")))

(define-public crate-phper-alloc-0.10.1 (c (n "phper-alloc") (v "0.10.1") (d (list (d (n "phper-build") (r "^0.10.1") (d #t) (k 1)) (d (n "phper-sys") (r "^0.10.1") (d #t) (k 0)))) (h "1i5k79x0l9ljp6ahqikwi253cd7k9zd9rz1jf7n9vj3bkv9dipx3") (r "1.65")))

(define-public crate-phper-alloc-0.10.2 (c (n "phper-alloc") (v "0.10.2") (d (list (d (n "phper-build") (r "^0.10.2") (d #t) (k 1)) (d (n "phper-sys") (r "^0.10.2") (d #t) (k 0)))) (h "0j4kmd4jsfr1jk5c8r65sqq53pjbdfj1b3afkxbk5cvd6h37rfsw") (r "1.65")))

(define-public crate-phper-alloc-0.11.0 (c (n "phper-alloc") (v "0.11.0") (d (list (d (n "phper-build") (r "^0.11.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.11.0") (d #t) (k 0)))) (h "1xy5whpk79c0cmi6jxkcxww57aq9wj6gpvb3mn8cbbb0c99ns0gd") (r "1.65")))

(define-public crate-phper-alloc-0.11.1 (c (n "phper-alloc") (v "0.11.1") (d (list (d (n "phper-build") (r "^0.11.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.11.0") (d #t) (k 0)))) (h "1zixjqdvw1w124hr0azz9z1bbzmccn8y90slx6d5ybnp0hfb4ng4") (r "1.65")))

(define-public crate-phper-alloc-0.12.0 (c (n "phper-alloc") (v "0.12.0") (d (list (d (n "phper-build") (r "^0.12.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.12.0") (d #t) (k 0)))) (h "1rl2r8zbrir4zg503kiddqfv2ymjmgwb8in5mndq4ahm0n3bdnpx") (r "1.65")))

(define-public crate-phper-alloc-0.13.0 (c (n "phper-alloc") (v "0.13.0") (d (list (d (n "phper-build") (r "^0.13.0") (d #t) (k 1)) (d (n "phper-sys") (r "^0.13.0") (d #t) (k 0)))) (h "05dhrpn2mihkk22rbvs5v9llqfagyrwy8b512iw6r6giza0j79kp") (r "1.67")))

(define-public crate-phper-alloc-0.13.1 (c (n "phper-alloc") (v "0.13.1") (d (list (d (n "phper-build") (r "^0.13.1") (d #t) (k 1)) (d (n "phper-sys") (r "^0.13.1") (d #t) (k 0)))) (h "1ip0v1n4fljbxzch91acszbn822c9d09ni0jad5cgs3igz1r05id") (r "1.65")))

