(define-module (crates-io ph pe phper-doc) #:use-module (crates-io))

(define-public crate-phper-doc-0.6.0 (c (n "phper-doc") (v "0.6.0") (d (list (d (n "phper") (r "^0.6.0") (d #t) (k 2)))) (h "1pa6c0y83db0f1bxq847s5khahlswa0fc4jwf94hvcf7a9hz2jvb") (r "1.65")))

(define-public crate-phper-doc-0.7.0 (c (n "phper-doc") (v "0.7.0") (d (list (d (n "phper") (r "^0.7.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0jf0vaa7fnjm2f8zbpzmqvw6i149kd6s1nzp9jxs0i8ralf7wjnr") (r "1.65")))

(define-public crate-phper-doc-0.8.0 (c (n "phper-doc") (v "0.8.0") (d (list (d (n "phper") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0z9zs2nr316rn1anqar0a4bnk99dlxxwsj5nfwhs1zqd2jfs094p") (r "1.65")))

(define-public crate-phper-doc-0.9.0 (c (n "phper-doc") (v "0.9.0") (d (list (d (n "phper") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1x46yyaf79cdbqkdq42fwdpfshgdm08jn12zx940nbnmddk0q1aq") (r "1.65")))

(define-public crate-phper-doc-0.10.0 (c (n "phper-doc") (v "0.10.0") (d (list (d (n "phper") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1x2ngdi1jjpw6kvrimy2pxqb2lihn28isvqjfzfdc64pxh4mx015") (r "1.65")))

(define-public crate-phper-doc-0.10.1 (c (n "phper-doc") (v "0.10.1") (d (list (d (n "phper") (r "^0.10.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1zspw3mnha5g7hp219yrp4jmrknv310yr76ff8kdbnl1vfz35d3j") (r "1.65")))

(define-public crate-phper-doc-0.10.2 (c (n "phper-doc") (v "0.10.2") (d (list (d (n "phper") (r "^0.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1m7sf3a3x85h5nxphqa1p76khblf114f111fpj89dhzcidv4vsnl") (r "1.65")))

(define-public crate-phper-doc-0.11.0 (c (n "phper-doc") (v "0.11.0") (d (list (d (n "phper") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0k72l790n9c58f7ghzap55qc73f6k6vlx0fdzpv3pdm3570h7hxn") (r "1.65")))

(define-public crate-phper-doc-0.11.1 (c (n "phper-doc") (v "0.11.1") (d (list (d (n "phper") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1ig1671cl0rw7gbd2xaq8yyp298684khq49ak1ihw9nm13zq92s7") (r "1.65")))

(define-public crate-phper-doc-0.12.0 (c (n "phper-doc") (v "0.12.0") (d (list (d (n "phper") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1hrbdnp4dkgf6y83czdri03jryyl5fgn2p90db3i1hxib4zp8dp0") (r "1.65")))

(define-public crate-phper-doc-0.12.1 (c (n "phper-doc") (v "0.12.1") (d (list (d (n "phper") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0qlsir5rwnfv5ah70k6k4hys8vd1sn6rn1jdn3zy5jpyrzjrc434") (r "1.65")))

(define-public crate-phper-doc-0.13.0 (c (n "phper-doc") (v "0.13.0") (d (list (d (n "phper") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 2)))) (h "0bakx0ljbmqc7sl2x0nszavib5k7f1wm0yp4j3d1gsbgjxz1x7hn") (r "1.67")))

(define-public crate-phper-doc-0.13.1 (c (n "phper-doc") (v "0.13.1") (d (list (d (n "phper") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "cookies"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 2)))) (h "0mnhijaimrg480vgbjw9inrzj780h0q3laj8js300w1fqw04x8yj") (r "1.65")))

