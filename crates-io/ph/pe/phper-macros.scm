(define-module (crates-io ph pe phper-macros) #:use-module (crates-io))

(define-public crate-phper-macros-0.0.0 (c (n "phper-macros") (v "0.0.0") (h "0cwsj663yqwjw5xgfm3mz07fzm5wgsfa5ir0px93qvpj6y15rvjn")))

(define-public crate-phper-macros-0.1.0 (c (n "phper-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "18a2k2jb6j3h8n21qk6v71p8w1hqjj9f4w3yz2w36ibw59d5rb7p")))

(define-public crate-phper-macros-0.2.0-alpha.1 (c (n "phper-macros") (v "0.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0lh7r6r18p8apcvhj4fx2sa29axhw64wj3z5n9hx5rdzmx4s0lza")))

(define-public crate-phper-macros-0.2.0-alpha.2 (c (n "phper-macros") (v "0.2.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1q5ipbjh0kq841cc55zn97fs2fr9vxyhlv3qdxfwhy9rmyq0yipy")))

(define-public crate-phper-macros-0.2.0-alpha.3 (c (n "phper-macros") (v "0.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1dq20321q8j43xa7z7lnanvk581rz2czwhkvygli534p52ssphqx")))

(define-public crate-phper-macros-0.2.0 (c (n "phper-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "006dm35pkg4hq7bkr3qrp4c5pgm76mhlyq7qaagfiaz3a3w40s26")))

(define-public crate-phper-macros-0.3.0 (c (n "phper-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1aqj7jfdlgm6956crd6ly3msjn37hw1b32qxry36x01c8l11frq9") (r "1.56")))

(define-public crate-phper-macros-0.4.0-alpha.1 (c (n "phper-macros") (v "0.4.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0psv6jrk9q1f35i0ddxzgxkjjl76j0vd4brq89zz74niinzgs27l") (r "1.56")))

(define-public crate-phper-macros-0.4.0-alpha.2 (c (n "phper-macros") (v "0.4.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1xvygrq90lks6lp40hhyfhy831rlr2ajv5d2mnwwalmd8182djdg") (r "1.56")))

(define-public crate-phper-macros-0.4.0-alpha.3 (c (n "phper-macros") (v "0.4.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0mgdklqwh4586skpqw0dcb3sljg0lsslmppvrz5jcj6rsg92jnrm") (r "1.56")))

(define-public crate-phper-macros-0.4.0-alpha.4 (c (n "phper-macros") (v "0.4.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0w3kz2mjhb7iyqb93wwhk2pq16f7xra8r63viv3lrjpxzz4vgkb9") (r "1.56")))

(define-public crate-phper-macros-0.4.0 (c (n "phper-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1xm8ps6blgalsiq6505vyyqaw6cv28q60mscp8i8pwyx0896mxm8") (r "1.56")))

(define-public crate-phper-macros-0.5.0 (c (n "phper-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "17kr6nl82ihcq7rk18ll2f2d3gidfv0g0aknks3ik9kwir2gd8fq") (r "1.56")))

(define-public crate-phper-macros-0.5.1 (c (n "phper-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1kj5c5c7ik6yx4ndjx21fy3bcwrkjjm0q7pbjsaiv6ig14brkc0g") (r "1.56")))

(define-public crate-phper-macros-0.6.0 (c (n "phper-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1kgw974v83wzm5g4mgr7jyvc02ky5iw12i5h4g887q7713mzibcp") (r "1.65")))

(define-public crate-phper-macros-0.7.0 (c (n "phper-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1wnqz1h2rzm2qwnh35rbfca5jcvz3672n6za9w2y7wjzpq3sqslq") (r "1.65")))

(define-public crate-phper-macros-0.8.0 (c (n "phper-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1jixq9vpmiplz1w1wngjcp6667h6f9pb6942wdbgdy6aimlbx5w3") (r "1.65")))

(define-public crate-phper-macros-0.9.0 (c (n "phper-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "05i4z7vqzh03csgzrb4rv16mrycmc410xka49xcrwyn8wsiabhna") (r "1.65")))

(define-public crate-phper-macros-0.10.0 (c (n "phper-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1miqp9z0vvwqc1w2vn8crz64zjqpfp6kig0mq331sc6yjjp8ynym") (r "1.65")))

(define-public crate-phper-macros-0.10.1 (c (n "phper-macros") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "02fn5j08lwbq7q5svbgaz1gcw7mnpj2vxia6bjcskk0lw09c1d6g") (r "1.65")))

(define-public crate-phper-macros-0.10.2 (c (n "phper-macros") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "06nn1ql9vgr3ix0sw5w1mx4z8lymj6pky9m7vmlkpd1z7wigqabv") (r "1.65")))

(define-public crate-phper-macros-0.11.0 (c (n "phper-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0qddf2xc0hx23w1ix9jskp6hn46clph6wsak0spilfbd393pi4mq") (r "1.65")))

(define-public crate-phper-macros-0.11.1 (c (n "phper-macros") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0s21b4k79xir8ipw07wvxqnhck16bxf9i33d0y7c9fm73nlzfn3h") (r "1.65")))

(define-public crate-phper-macros-0.12.0 (c (n "phper-macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0yzxx7yyppxz5ik7a9dknij1lk7g2zypjsv4xsfvmp780hpmq712") (r "1.65")))

(define-public crate-phper-macros-0.13.0 (c (n "phper-macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0x27m15anv5hs5y80561vzrparfzi7z7jdjl3xvgjbkz0ib06mzn") (r "1.67")))

(define-public crate-phper-macros-0.13.1 (c (n "phper-macros") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1k8hy894b86yqw98j0isd8fgyxjj5vcgjl7qgf7pz1isz8ms6wip") (r "1.65")))

