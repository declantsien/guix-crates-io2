(define-module (crates-io ph pe phper-sys) #:use-module (crates-io))

(define-public crate-phper-sys-0.0.0 (c (n "phper-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1qpram47snfkbhrala6qcyx5y3i67v5xbxdv3ag9lpllnh4hk463")))

(define-public crate-phper-sys-0.0.1 (c (n "phper-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "01db0bpp05fdw1b8qaxc8x3nl4hlp5f7a0hr8x35xyys5qf18vsp")))

(define-public crate-phper-sys-0.1.0 (c (n "phper-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1rf3pipl5cz6xf415v52mkpq29a1l43palzg6aahrji4y6iykby6")))

(define-public crate-phper-sys-0.2.0-alpha.1 (c (n "phper-sys") (v "0.2.0-alpha.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)))) (h "0hq62z7mf2808z7sk0kj3k72lm2y309hakjzqzaxiis9yhmid256")))

(define-public crate-phper-sys-0.2.0-alpha.2 (c (n "phper-sys") (v "0.2.0-alpha.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)))) (h "1rv1dpjgxk7fy244iix1w2ly3f2pcqshxajvj1r0f565pf1847mb")))

(define-public crate-phper-sys-0.2.0-alpha.3 (c (n "phper-sys") (v "0.2.0-alpha.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)))) (h "1cy28rhps8di1z5r9kb7dl1n9skmw6jhnlbrjbkas42xhz1k16wm")))

(define-public crate-phper-sys-0.2.0 (c (n "phper-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)))) (h "08q78bn2lhmryxs42kz5a0f1lnzwn0azfwkqxw7jgl4gsgrfl3fw")))

(define-public crate-phper-sys-0.3.0 (c (n "phper-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)))) (h "0f9y9kjxjyrwhwlb0d8m211660k75v7j4593s64k1xylch2gqpy0") (r "1.56")))

(define-public crate-phper-sys-0.3.1 (c (n "phper-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)))) (h "0xv3sav5j0sddx14xmyxd094lk1zw25g773rn34k0s8mrdr5rqij") (r "1.56")))

(define-public crate-phper-sys-0.4.0-alpha.1 (c (n "phper-sys") (v "0.4.0-alpha.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)))) (h "0a9pjjbpz4kwmcjn6cbaqmagwq0zfrxpfpjcjqyzchpmqwi11zii") (r "1.56")))

(define-public crate-phper-sys-0.4.0-alpha.2 (c (n "phper-sys") (v "0.4.0-alpha.2") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)))) (h "1xgn69mffgf1jjxyzmy1lf5aiwh12q71r776g42hxlwyr2lckk3j") (r "1.56")))

(define-public crate-phper-sys-0.4.0-alpha.3 (c (n "phper-sys") (v "0.4.0-alpha.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0c9ldgxdar6jqfxy1a7h5l6d55l1yz5clah1f57j5cp3ph9iza2s") (r "1.56")))

(define-public crate-phper-sys-0.4.0-alpha.4 (c (n "phper-sys") (v "0.4.0-alpha.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "14r0pd7mhapkwvh0gx5avsa8ndqr1j6sgfmwjh4p6iwwmb7vgy6l") (r "1.56")))

(define-public crate-phper-sys-0.4.0 (c (n "phper-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "14bibb2psy3pnzpm9cj3p1n0sb85p4n1av9g6iqnyjga25734jpl") (r "1.56")))

(define-public crate-phper-sys-0.5.0 (c (n "phper-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1afv8aydqxl3bmdwr8v2npmqvj8nmc70ibxr68zwajm2rbvg9c83") (r "1.56")))

(define-public crate-phper-sys-0.5.1 (c (n "phper-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1q5hd43ihj7hn3h58ficc2rmhpvms7d4pwdh6p7bb74b8fpdibdz") (r "1.56")))

(define-public crate-phper-sys-0.6.0 (c (n "phper-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "0iag6qjayvxfa47ssrh6m7q67m5l1l13axj9lkc8vh9dhvafyhlg") (r "1.65")))

(define-public crate-phper-sys-0.7.0 (c (n "phper-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "0vlmz7zc0vhsr3zkximl8l0ii5lq3x4n14i6z97p1j4vhcndb9av") (r "1.65")))

(define-public crate-phper-sys-0.8.0 (c (n "phper-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "1gmcdqnhza1rpl11q5aixvj2q9415n7bdjwx13z1vr6777cxhakv") (r "1.65")))

(define-public crate-phper-sys-0.9.0 (c (n "phper-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "1nclnkyvmkw8yc9rk5xd3md6hm71bzmramib2n2q7f0vxxqvgnnb") (r "1.65")))

(define-public crate-phper-sys-0.10.0 (c (n "phper-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "14l2qxrr7dp16mlxcar93z5yyxjxqfs96nfgg5bvrbsys7lglzfp") (r "1.65")))

(define-public crate-phper-sys-0.10.1 (c (n "phper-sys") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "1z4808x8lrqhgzs7bhn397rkb6j289yibhhynyys9c23v7pr96p4") (r "1.65")))

(define-public crate-phper-sys-0.10.2 (c (n "phper-sys") (v "0.10.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "0phd154pscjdb7dzahkf1a62arvk84jy981kh02gggz0cjm00704") (r "1.65")))

(define-public crate-phper-sys-0.11.0 (c (n "phper-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "1v6l8kgllx4nwciai6hlhq5sg2nmhpjq1vmvl6bcmgi9w67yrg74") (r "1.65")))

(define-public crate-phper-sys-0.11.1 (c (n "phper-sys") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "1sgxd1zwk1j1zx3p06c5i8nzbdghcmimkgk5zf0pzjpdxdan6cpv") (r "1.65")))

(define-public crate-phper-sys-0.12.0 (c (n "phper-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "01wmmzf52n30vslh2fidygbbi02rgkcrlak1q0wpzy8q8s2qnlxj") (r "1.65")))

(define-public crate-phper-sys-0.12.1 (c (n "phper-sys") (v "0.12.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "19ynbjiwljirq9fgw35kna5zypk33frpsy889xb3w6490d5pm9px") (r "1.65")))

(define-public crate-phper-sys-0.13.0 (c (n "phper-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0njnzv5907jpnmmc1kydsiz6b4p0wcc759bqldwh5aq42kifvdj5") (r "1.67")))

(define-public crate-phper-sys-0.13.1 (c (n "phper-sys") (v "0.13.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "regex") (r "^1.5.6") (d #t) (k 1)))) (h "0ihzav475820c7ym5a7k374miwfmdyakkpcb2njyzgqgy4gqci65") (r "1.65")))

