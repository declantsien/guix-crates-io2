(define-module (crates-io bo b2 bob2) #:use-module (crates-io))

(define-public crate-bob2-0.0.1 (c (n "bob2") (v "0.0.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0hpr9k0430bx5gfs5sn0w2s895hmncp1qslgnw45lzb9fj1y1fj1")))

