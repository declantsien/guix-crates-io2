(define-module (crates-io bo tn botnet-macros) #:use-module (crates-io))

(define-public crate-botnet-macros-0.2.0 (c (n "botnet-macros") (v "0.2.0") (d (list (d (n "botnet-utils") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kp8xpvrq95mll112jh2x56q72s7s8zg01xsifi266qbra98kdpq")))

