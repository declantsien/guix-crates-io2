(define-module (crates-io bo dy bodyparser) #:use-module (crates-io))

(define-public crate-bodyparser-0.0.1 (c (n "bodyparser") (v "0.0.1") (d (list (d (n "iron") (r "*") (d #t) (k 0)))) (h "0cpvlxl48zcabwlm0dbwc0giyk1rzwp73ncjvafzar0zgi77igbn")))

(define-public crate-bodyparser-0.0.2 (c (n "bodyparser") (v "0.0.2") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "persistent") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1y9nzskh95x8gr9ir3gvk1kg7xdrcw2ymizhv16q0qjmz9m76mrg")))

(define-public crate-bodyparser-0.0.3 (c (n "bodyparser") (v "0.0.3") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "persistent") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0dj9dhiv87hxszf5m67hwp05fn6h01mc91i3n3zy70an9kvc2pas")))

(define-public crate-bodyparser-0.0.4 (c (n "bodyparser") (v "0.0.4") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "persistent") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1wzfs36q1izycidpip3s7y79aba2d01v71b2hgz50akzc57ivvgk")))

(define-public crate-bodyparser-0.0.5 (c (n "bodyparser") (v "0.0.5") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "persistent") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0z62805995l10diwlca7sf14msnch7jzpdz3xbzslvc0bmbnp2x6")))

(define-public crate-bodyparser-0.0.6 (c (n "bodyparser") (v "0.0.6") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "persistent") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0isc6nhi5gsw5872k6ry812xrabziawbwjsg5zxp4r1rblkq1hkq")))

(define-public crate-bodyparser-0.1.0 (c (n "bodyparser") (v "0.1.0") (d (list (d (n "iron") (r "^0.2") (k 0)) (d (n "persistent") (r "^0") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.6.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)))) (h "1mm2qwrdyffdszrcv2br2iqkzxhbxxb1hklqc81v3fpmp9apcl00")))

(define-public crate-bodyparser-0.2.0 (c (n "bodyparser") (v "0.2.0") (d (list (d (n "iron") (r "^0.2") (k 0)) (d (n "persistent") (r "^0") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "0hlx8kdljjzp9hyvflc0fgq163i7ijcqb90qwhd806ll0yc1sdsw")))

(define-public crate-bodyparser-0.3.0 (c (n "bodyparser") (v "0.3.0") (d (list (d (n "iron") (r "^0.3") (d #t) (k 0)) (d (n "persistent") (r "^0.1") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "1lznhjk23md6mqd2i29hdx7shwcphvcvxxs98isydb2hk1ydxf31")))

(define-public crate-bodyparser-0.4.0 (c (n "bodyparser") (v "0.4.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "persistent") (r "^0.2") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "0yklb5d1g6k6pyxy9i3mx1fzlim6f3qmwd6i9z0jn9s83k0mhyr0")))

(define-public crate-bodyparser-0.4.1 (c (n "bodyparser") (v "0.4.1") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "persistent") (r "^0.2") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1mhzxfd704ih2nwg7v5cc2n82njp40zp26h1067xr0z50ys73c87")))

(define-public crate-bodyparser-0.5.0 (c (n "bodyparser") (v "0.5.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "persistent") (r "^0.3") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1xbbka2hyj09s17v2svvb4m66p53l91aksfn3lysfx4bacbyha39")))

(define-public crate-bodyparser-0.6.0 (c (n "bodyparser") (v "0.6.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "persistent") (r "^0.3") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1agn6i5302kxzvcvial6l3sn3gnilbicdywbisv674xp1cka1a5g")))

(define-public crate-bodyparser-0.7.0 (c (n "bodyparser") (v "0.7.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "persistent") (r "^0.3") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0306225k1ilcmxvsvyccm5sqgv10sfvdjcwq77z1pydik6212b7q")))

(define-public crate-bodyparser-0.8.0 (c (n "bodyparser") (v "0.8.0") (d (list (d (n "iron") (r ">= 0.5, < 0.7") (d #t) (k 0)) (d (n "persistent") (r "^0.4") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c1gynj9l7wv3mjrzr5jifmy0pjdwachfqz09aygdmmab3xan8zh")))

