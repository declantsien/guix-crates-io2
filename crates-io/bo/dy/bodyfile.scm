(define-module (crates-io bo dy bodyfile) #:use-module (crates-io))

(define-public crate-bodyfile-0.1.0 (c (n "bodyfile") (v "0.1.0") (d (list (d (n "duplicate") (r "^0.3") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)))) (h "1q4ly1gr6vfn6rd1m30y9vijj45r1sykhpsdll576mpgmhig1rkw")))

(define-public crate-bodyfile-0.1.1 (c (n "bodyfile") (v "0.1.1") (d (list (d (n "duplicate") (r "^0.3") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)))) (h "0db6xqki0gq84g73rd09sfwlfapgfp8i7c5q0dlbvsw876xqjvwg")))

(define-public crate-bodyfile-0.1.2 (c (n "bodyfile") (v "0.1.2") (d (list (d (n "duplicate") (r "^0.3") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)))) (h "1yv58x2iirrilr41schx9ma5qga1xrxvfc806b00p2cpalfs7ivc")))

(define-public crate-bodyfile-0.1.3 (c (n "bodyfile") (v "0.1.3") (d (list (d (n "duplicate") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1sk0kkiwavqifrv2n1xmphaan0pgnfni260j3r88nra3gba0mxsy")))

(define-public crate-bodyfile-0.1.4 (c (n "bodyfile") (v "0.1.4") (d (list (d (n "duplicate") (r "^0.3") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)))) (h "1df2nvgjrii321p9kjy223wlnv47dp1y9v8py6x2ll5x8fpwv0mf")))

