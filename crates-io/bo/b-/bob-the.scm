(define-module (crates-io bo b- bob-the) #:use-module (crates-io))

(define-public crate-bob-the-0.1.0 (c (n "bob-the") (v "0.1.0") (h "0m57mp0nsrghlyvw2iqzrsvh932arlzxj18pv1098m7lq5w8x106")))

(define-public crate-bob-the-0.1.1 (c (n "bob-the") (v "0.1.1") (h "1kzq75bjrdq37ibrygab41bp9d9w453nb11qb8yp009vnp9xgajw")))

(define-public crate-bob-the-0.1.3 (c (n "bob-the") (v "0.1.3") (h "0y1n97ncdvf633cmgw6g3fqq6z67lrg504mpc1jyj0j3aadh5xfk")))

(define-public crate-bob-the-0.1.4 (c (n "bob-the") (v "0.1.4") (h "03jnkh926m56kvvpj7yqpqavcbc7a4b53540yxw04fk63d1569a4")))

(define-public crate-bob-the-0.1.5 (c (n "bob-the") (v "0.1.5") (h "0l6ckx7xnkdiwj04sb9n7gigd0zjqdbw8j2l6ri7k5dr146firp0")))

(define-public crate-bob-the-0.1.6 (c (n "bob-the") (v "0.1.6") (h "0k1icmklxcx2cs5ywvhjvb9a89ggp3m4aj8p5pxf3ckybnqp7aq5")))

