(define-module (crates-io bo nf bonfida-cli) #:use-module (crates-io))

(define-public crate-bonfida-cli-0.1.0 (c (n "bonfida-cli") (v "0.1.0") (d (list (d (n "bonfida-autobindings") (r "^0.1") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.1") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.1") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "0gfbixdpxfa1b4h459j1yh3n81kcp9i2i24xvq810r0xs8gfr51k")))

(define-public crate-bonfida-cli-0.1.1 (c (n "bonfida-cli") (v "0.1.1") (d (list (d (n "bonfida-autobindings") (r "^0.1") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.1") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.1") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "0xkgj2751az7jg32kzk606qgbj1nak97f298k7q4xaxc62x0jx6s")))

(define-public crate-bonfida-cli-0.3.5 (c (n "bonfida-cli") (v "0.3.5") (d (list (d (n "bonfida-autobindings") (r "^0.3.5") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.3.5") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.3.5") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.3.5") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "1dqylljn1lnlhdsdpij5fsj8pqcla7alf58y8i74x5shdgf164r5")))

(define-public crate-bonfida-cli-0.3.6 (c (n "bonfida-cli") (v "0.3.6") (d (list (d (n "bonfida-autobindings") (r "^0.3.6") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.3.6") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.3.6") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.3.6") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "1ldpqv2l1ri5caghxdidlqamr5x8j3m54yl16gn2qjx109314iqb")))

(define-public crate-bonfida-cli-0.3.7 (c (n "bonfida-cli") (v "0.3.7") (d (list (d (n "bonfida-autobindings") (r "^0.3.7") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.3.7") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.3.7") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.3.7") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "1afq8x42h1br82z3invdxgyikmkn4p5cnn780rl7zcwfg8gl38wg")))

(define-public crate-bonfida-cli-0.3.8 (c (n "bonfida-cli") (v "0.3.8") (d (list (d (n "bonfida-autobindings") (r "^0.3.8") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.3.8") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.3.8") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.3.8") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "1x10p8iqdszv10c0krjj9rsnjhglgd97im67hziclf9b0xniar1a")))

(define-public crate-bonfida-cli-0.3.9 (c (n "bonfida-cli") (v "0.3.9") (d (list (d (n "bonfida-autobindings") (r "^0.3.9") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.3.9") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.3.9") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.3.9") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "0kgw76zxg6si5hc1wzavwwwsick590fq78mbz5a3g1xk7lp3qm6p")))

(define-public crate-bonfida-cli-0.3.10 (c (n "bonfida-cli") (v "0.3.10") (d (list (d (n "bonfida-autobindings") (r "^0.3.10") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.3.10") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.3.10") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.3.10") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "1kyzsz564njdr44bq0blwkipzsp10jy3kz74izycsa4ms47sv3bd")))

(define-public crate-bonfida-cli-0.3.11 (c (n "bonfida-cli") (v "0.3.11") (d (list (d (n "bonfida-autobindings") (r "^0.3.11") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.3.11") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.3.11") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.3.11") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "1dj8xa4shhjw8hgshfqchb2b2c7pf7ds4yfczd35qvkxm6p2zzp2")))

(define-public crate-bonfida-cli-0.3.12 (c (n "bonfida-cli") (v "0.3.12") (d (list (d (n "bonfida-autobindings") (r "^0.3.12") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.3.12") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.3.12") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.3.12") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "0jpml7gkssb1s5x6v4w73ryl2rsxdhc26s28v31jfpi90i9ph9hd")))

(define-public crate-bonfida-cli-0.3.13 (c (n "bonfida-cli") (v "0.3.13") (d (list (d (n "bonfida-autobindings") (r "^0.3.13") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.3.13") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.3.13") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.3.13") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "0b8cip0wm1aqm2n55y4yf9xqgdnimjds0pfchbarh6rbpjg2wwwy")))

(define-public crate-bonfida-cli-0.4.0 (c (n "bonfida-cli") (v "0.4.0") (d (list (d (n "bonfida-autobindings") (r "^0.4.0") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.4.0") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.4.0") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "09mwlmxx93q6kigdhf1hjbp9kbbwi6wad0zz08hwm0wcss5r5230")))

(define-public crate-bonfida-cli-0.4.1 (c (n "bonfida-cli") (v "0.4.1") (d (list (d (n "bonfida-autobindings") (r "^0.4.1") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.4.1") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.4.1") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "1hgxk1g0xms21wjcl5i39haah7a4zcdil9il8ydp76mfibxqmw7s")))

(define-public crate-bonfida-cli-0.4.2 (c (n "bonfida-cli") (v "0.4.2") (d (list (d (n "bonfida-autobindings") (r "^0.4.2") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.4.2") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.4.2") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.4.2") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "14ms1bszp6gl63qdgrp509ak1s73fag4pmi4900cli1wncr8fyfm")))

(define-public crate-bonfida-cli-0.4.3 (c (n "bonfida-cli") (v "0.4.3") (d (list (d (n "bonfida-autobindings") (r "^0.4.3") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.4.3") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.4.3") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "19pbgdm1al9wryr7xsgdnw71570lgc3z3934cr9z41a56cj5k651")))

(define-public crate-bonfida-cli-0.4.4 (c (n "bonfida-cli") (v "0.4.4") (d (list (d (n "bonfida-autobindings") (r "^0.4.4") (d #t) (k 0)) (d (n "bonfida-autodoc") (r "^0.4.4") (d #t) (k 0)) (d (n "bonfida-autoproject") (r "^0.4.4") (d #t) (k 0)) (d (n "bonfida-benchviz") (r "^0.4.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (d #t) (k 0)))) (h "06q81hw1nd2vcj8izmyqm00ad71lw7h2b9qsy8ri7s1kdv7wh17k")))

