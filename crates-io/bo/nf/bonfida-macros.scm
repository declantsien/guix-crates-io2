(define-module (crates-io bo nf bonfida-macros) #:use-module (crates-io))

(define-public crate-bonfida-macros-0.1.0 (c (n "bonfida-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "188gqqjhbhzh5zfbwb0ii3q4174rz5cy9wlqkh45ldy5scq5p9if")))

(define-public crate-bonfida-macros-0.1.1 (c (n "bonfida-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00dlyr5jwrl3cpx4l2cpaf6c2cnyz9ijpq5l8gr757hyfr52hq0x")))

(define-public crate-bonfida-macros-0.1.2 (c (n "bonfida-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m8q351znhq1il2wa922yhp4r2y6qlw7llq7vv1mpbdcymdcax1x") (f (quote (("instruction_params_casting"))))))

(define-public crate-bonfida-macros-0.2.0 (c (n "bonfida-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hh8wxh23xsdps82v7dkmi15dqqhqgp668rldmf0igvl0fqvf7vc") (f (quote (("instruction_params_casting"))))))

(define-public crate-bonfida-macros-0.2.3 (c (n "bonfida-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zv03kigf2s0qm3wr799k2l57cqcqdam35yk9bmw1jdwckmw8q2m") (f (quote (("instruction_params_casting"))))))

(define-public crate-bonfida-macros-0.2.4 (c (n "bonfida-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06q4gj15dj0mb9spvrh089aczl4kskxcjv6wan4ahm61ihx04vix")))

(define-public crate-bonfida-macros-0.2.5 (c (n "bonfida-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m2ah6kh38kgs01fbxwqgl2gd6li5sy43pm0svb6rd8fsigcwnhg")))

(define-public crate-bonfida-macros-0.2.6 (c (n "bonfida-macros") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0364hl0a9d0v552r98m1bnfh2b2avc8a6n884na42k52fb9s8f77")))

(define-public crate-bonfida-macros-0.2.7 (c (n "bonfida-macros") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1651mnkwbkq7ksxrywlhskw2q9y77pb6pqb0nsm8jmh1fgsjjznl")))

(define-public crate-bonfida-macros-0.2.8 (c (n "bonfida-macros") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11hjgrg5sm7nxs8h8chyp0dn16b5r5cxcajmcmsv6jffyml10pnd")))

(define-public crate-bonfida-macros-0.2.9 (c (n "bonfida-macros") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03i9a70675jvyl9099ch84a99zgv3p1irlxmcs5kc8ljcx56mi8h")))

(define-public crate-bonfida-macros-0.3.5 (c (n "bonfida-macros") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hyar52904z7ycj0ywgx5dxq0dh8bqzwxrlhbvfnnlp4v5mh32df")))

(define-public crate-bonfida-macros-0.3.6 (c (n "bonfida-macros") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1asmf131dv7m49pbhqqij7pawvlhr6jz6cmpv0gaidlmflq5cysh")))

(define-public crate-bonfida-macros-0.3.7 (c (n "bonfida-macros") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12kdpblq5h3cgwn4fwzwbqsy7y2g02g86h66mjhxb785zmr4a3pr")))

(define-public crate-bonfida-macros-0.3.8 (c (n "bonfida-macros") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13p74wkv5k503ha02874p3r283hkg7l6mkksafpr2j7azgljb1a4")))

(define-public crate-bonfida-macros-0.3.9 (c (n "bonfida-macros") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04vwn7smcq946mfpi08555wgqfcg5gnvcqprrfa97mvwh71fl05q")))

(define-public crate-bonfida-macros-0.3.10 (c (n "bonfida-macros") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n2ymasls3gj5sfa4k49mql1jrk6wb0i5qm8syc2x9w0a0xmcaiy")))

(define-public crate-bonfida-macros-0.3.11 (c (n "bonfida-macros") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j4qb9ibz2jklvmk3q9ld8c6qlsp4c75dia77aji4p3ingfyywb9")))

(define-public crate-bonfida-macros-0.3.12 (c (n "bonfida-macros") (v "0.3.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fqkjssl1wzfy5m5b4s2vq5sdr5spbwj9av2qwswlyng9zcymbfq")))

(define-public crate-bonfida-macros-0.3.13 (c (n "bonfida-macros") (v "0.3.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "<1.16.0") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n9130hx9gi3j18ar9z2w71gv7vnlaydc7kpk2d2zbp8b29cb0gc")))

(define-public crate-bonfida-macros-0.4.0 (c (n "bonfida-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.16") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vsa0bhi7m6p92fnps9rb3s750xxswp234g8p0wlvli9m9bd9pxd")))

(define-public crate-bonfida-macros-0.4.1 (c (n "bonfida-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.16") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "102ljrb2aqy25kxmcjgwys1i4j0dxps86hghmsd8wf4qcc1wzpds")))

(define-public crate-bonfida-macros-0.4.2 (c (n "bonfida-macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.16") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zn86m5hm0f1hv1ca2ah7dw6bwsa7sfcnbmzyjpib27mrhq6zlis")))

(define-public crate-bonfida-macros-0.4.3 (c (n "bonfida-macros") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.16") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c57nz8nv5jsnvg5ivi5r23dmn6k9mvvyzh0j3krpmb0niczbgng")))

(define-public crate-bonfida-macros-0.4.4 (c (n "bonfida-macros") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.16") (d #t) (k 0)) (d (n "spl-name-service") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ihhfcf2wjd7k6s8a6v279629p13753nmd33vksaj5kbid7pqmga")))

