(define-module (crates-io bo nd bondrewd-derive) #:use-module (crates-io))

(define-public crate-bondrewd-derive-0.2.9 (c (n "bondrewd-derive") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m27rai34vp20bdlam4x12i8kjyybrqwsd3r3nli9h3svlgpfr2q") (f (quote (("slice_fns"))))))

(define-public crate-bondrewd-derive-0.2.12 (c (n "bondrewd-derive") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0b2s2g9rpnwm2xxrfpcmdv08ky17w55d8ki6jnc3sgyxkjyhlrmz") (f (quote (("slice_fns") ("hex_fns") ("default"))))))

(define-public crate-bondrewd-derive-0.2.13 (c (n "bondrewd-derive") (v "0.2.13") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09azvp6hn54lyxy0r36gmm5dwmr6r7anf31xpzx4zwslyxrhwsz7") (f (quote (("slice_fns") ("hex_fns") ("default"))))))

(define-public crate-bondrewd-derive-0.3.4 (c (n "bondrewd-derive") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nipcvypbf5sjzi3v391iyjcppdxmnck7fchkx9r1jq5is31hpqn") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.6 (c (n "bondrewd-derive") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i1hdz872m21la46x8jnrka7fcsix60jvdc4z2qv7j9h20bip5nj") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.7 (c (n "bondrewd-derive") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g2plyg3g9rg6228jaq2g0f1rrig9cnd4jjppc7az807fb886i2n") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.8 (c (n "bondrewd-derive") (v "0.3.8") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04vi2kyymsp97252j82snzbaxq4g8yl36h0pc09z590qwcisf7pi") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.9 (c (n "bondrewd-derive") (v "0.3.9") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h9x0j8rbzd7njvqylazl706n50zgj4hcc00024v2pfw2573haxq") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.10 (c (n "bondrewd-derive") (v "0.3.10") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1843zgazagff27hnqjddf1rmk5wf7jyqsxi1ny8sd3hrxalhqjm3") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.11 (c (n "bondrewd-derive") (v "0.3.11") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k38ms36hzpj7zli90hs9jfxslhaj46s9s9nq7878nnsy63pax1d") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.12 (c (n "bondrewd-derive") (v "0.3.12") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "13s3gj4ca40n2xyx3a00vb9yy27l6d9m06znvj9h7yh7araydsk8") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.13 (c (n "bondrewd-derive") (v "0.3.13") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0kihq6276mamki77qgfjgxprb3df2ps8y3nv1ysvdmdxyrsl0v03") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.14 (c (n "bondrewd-derive") (v "0.3.14") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0hl8csm3m14b6fp5rr4cr3xspmiyrpw8rqf587lgqy6bv26qwhrr") (f (quote (("slice_fns") ("setters") ("hex_fns")))) (y #t)))

(define-public crate-bondrewd-derive-0.3.17 (c (n "bondrewd-derive") (v "0.3.17") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0iiyal2icnaw9ynlyzgzls09xw734hdqdcr51s6wfw31mr8qdjkc") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

(define-public crate-bondrewd-derive-0.3.18 (c (n "bondrewd-derive") (v "0.3.18") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0xv91876zqggl8khfnva1i829h5xjbw961qv0bf8xwzfw7ga0gbj") (f (quote (("slice_fns") ("setters") ("hex_fns"))))))

