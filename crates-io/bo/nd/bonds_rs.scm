(define-module (crates-io bo nd bonds_rs) #:use-module (crates-io))

(define-public crate-bonds_rs-0.0.0 (c (n "bonds_rs") (v "0.0.0") (h "05dfz7frdk3ia81fbirk9ax8yjcgg5rrlg4j6g23n9801mc7n730") (y #t)))

(define-public crate-bonds_rs-0.1.0 (c (n "bonds_rs") (v "0.1.0") (h "0hc65hzyasjgh2mqb5hgpgcnhsdh82iks376dmrwjyx9cqhsi2ac")))

(define-public crate-bonds_rs-0.1.1 (c (n "bonds_rs") (v "0.1.1") (h "0bz2r6ppy3924bkmp4s09gzpbijxjrclv9xyng5j1vdg9l8k1vzk")))

