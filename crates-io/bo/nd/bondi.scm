(define-module (crates-io bo nd bondi) #:use-module (crates-io))

(define-public crate-bondi-0.1.0 (c (n "bondi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j0xf4iq3i59ak8iszwp3ghdg40p6xi7178sp8a4vz7kz00kbq96")))

(define-public crate-bondi-0.1.1 (c (n "bondi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l5s0i15y9546d3bqv3sd2qix81svwkxjk11bm478ppy4h99k03m")))

(define-public crate-bondi-0.1.2 (c (n "bondi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17bkv3akr64vdkpqqxp3fgcbqzna90gys3qb56fmydywg9iwmn0a")))

