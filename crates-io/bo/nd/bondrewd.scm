(define-module (crates-io bo nd bondrewd) #:use-module (crates-io))

(define-public crate-bondrewd-0.1.3 (c (n "bondrewd") (v "0.1.3") (d (list (d (n "bondrewd-derive") (r "^0.2.9") (o #t) (d #t) (k 0)))) (h "0vg4xgxsyckh8rp25gjavxhcyis6aqkypqw7l9h8zyxkkspm3mja") (f (quote (("slice_fns" "bondrewd-derive/slice_fns") ("derive" "bondrewd-derive") ("default"))))))

(define-public crate-bondrewd-0.1.4 (c (n "bondrewd") (v "0.1.4") (d (list (d (n "bondrewd-derive") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "1aarvld3pampl6ijw6fasbl47ckhxhaxg42jp3q3jz8rc7cvi95f") (f (quote (("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive") ("default" "hex_fns")))) (y #t)))

(define-public crate-bondrewd-0.1.5 (c (n "bondrewd") (v "0.1.5") (d (list (d (n "bondrewd-derive") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "03p3mw35ar70dp1nylmm1zgwv0q75z0n7y4rfw2xjs5389p0wkwz") (f (quote (("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive") ("default"))))))

(define-public crate-bondrewd-0.1.6 (c (n "bondrewd") (v "0.1.6") (d (list (d (n "bondrewd-derive") (r "^0.2.13") (o #t) (d #t) (k 0)))) (h "107p8ri4qj0nx7388nidj9qcrc51x98lcjzlbn4s96h09j5q7siq") (f (quote (("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive") ("default"))))))

(define-public crate-bondrewd-0.1.7 (c (n "bondrewd") (v "0.1.7") (d (list (d (n "bondrewd-derive") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0kl7104w0l8l6ay1yk35akqh8iz8gpx71477rwi0xd0a0awmb45q") (f (quote (("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive"))))))

(define-public crate-bondrewd-0.1.8 (c (n "bondrewd") (v "0.1.8") (d (list (d (n "bondrewd-derive") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "090fkz8mcd8c6p3h3z3l4dv5ijzd68h08dshsj2fydkhrb4k79vq") (f (quote (("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive"))))))

(define-public crate-bondrewd-0.1.9 (c (n "bondrewd") (v "0.1.9") (d (list (d (n "bondrewd-derive") (r "^0.3.6") (o #t) (d #t) (k 0)))) (h "013ncvcwbcap930cxyri8favskpgk3fkhfmnaj61jqri4rqqgdqp") (f (quote (("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive"))))))

(define-public crate-bondrewd-0.1.10 (c (n "bondrewd") (v "0.1.10") (d (list (d (n "bondrewd-derive") (r "^0.3.7") (o #t) (d #t) (k 0)))) (h "150wbijnpicrbd8y1i4cvs0727wyqsdgpwv0csgni7c53zp722yg") (f (quote (("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive"))))))

(define-public crate-bondrewd-0.1.11 (c (n "bondrewd") (v "0.1.11") (d (list (d (n "bondrewd-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0k7dpgdclhhwfd3263qxrbcrgkfnp8s48d1zl5fi1z1r55nj8jb6") (f (quote (("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive"))))))

(define-public crate-bondrewd-0.1.12 (c (n "bondrewd") (v "0.1.12") (d (list (d (n "bondrewd-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1a7r6giyajnplbv2shhc9waw66v284yv8vgxxlij5fgh7jhqvjha") (f (quote (("std") ("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive") ("default" "std"))))))

(define-public crate-bondrewd-0.1.13 (c (n "bondrewd") (v "0.1.13") (d (list (d (n "bondrewd-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1vvq9b58zs52syp87k0rbnapxsdg853nsb5fr8r1daq8cm079r7v") (f (quote (("std") ("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive") ("default" "std"))))))

(define-public crate-bondrewd-0.1.14 (c (n "bondrewd") (v "0.1.14") (d (list (d (n "bondrewd-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1k6i8apf8qvyf8n96sqrvsramxgdmwzlar5c9pacxb6kr3x605kd") (f (quote (("std") ("slice_fns" "bondrewd-derive/slice_fns") ("hex_fns" "bondrewd-derive/hex_fns") ("derive" "bondrewd-derive") ("default" "std"))))))

