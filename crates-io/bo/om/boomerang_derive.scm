(define-module (crates-io bo om boomerang_derive) #:use-module (crates-io))

(define-public crate-boomerang_derive-0.1.0 (c (n "boomerang_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "19xwjfdr2lic3vlm2hyzhg0aw6lgwmzml0qs0gkxv6dvjnjn8mvw")))

(define-public crate-boomerang_derive-0.2.0 (c (n "boomerang_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "06f5dc1gbqkqrdbm6vvqh223vgdn2mb6ckr387z1l3wa0npskq8h")))

