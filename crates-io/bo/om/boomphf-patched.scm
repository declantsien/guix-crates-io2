(define-module (crates-io bo om boomphf-patched) #:use-module (crates-io))

(define-public crate-boomphf-patched-0.5.9 (c (n "boomphf-patched") (v "0.5.9") (d (list (d (n "crossbeam-utils") (r ">=0.7.2, <0.9") (d #t) (k 0)) (d (n "dyn_size_of") (r ">=0.3.0") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rayon") (r ">=1.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wyhash") (r ">=0.3, <=0.5") (d #t) (k 0)))) (h "1wgnfbb98nxcpgar60iwj0054w715hf9nq240r8wxbhh9867k848") (y #t)))

(define-public crate-boomphf-patched-0.5.9+0 (c (n "boomphf-patched") (v "0.5.9+0") (d (list (d (n "crossbeam-utils") (r ">=0.7.2, <0.9") (d #t) (k 0)) (d (n "dyn_size_of") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rayon") (r ">=1.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wyhash") (r ">=0.3, <=0.5") (d #t) (k 0)))) (h "0qq4g60vhs14pkpmxir88kab0k934n6ddch127fxaqki9v21zg3b") (y #t)))

(define-public crate-boomphf-patched-0.5.9+1 (c (n "boomphf-patched") (v "0.5.9+1") (d (list (d (n "crossbeam-utils") (r ">=0.7.2, <0.9") (d #t) (k 0)) (d (n "dyn_size_of") (r ">=0") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rayon") (r ">=1.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wyhash") (r ">=0.3, <=0.5") (d #t) (k 0)))) (h "0vay1gav1snw47gaa9rgm1pw4vzbq10v75g0ndisqxx0ph3yvrv8") (y #t)))

(define-public crate-boomphf-patched-0.5.9-0 (c (n "boomphf-patched") (v "0.5.9-0") (d (list (d (n "crossbeam-utils") (r ">=0.7.2, <0.9") (d #t) (k 0)) (d (n "dyn_size_of") (r ">=0") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rayon") (r ">=1.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wyhash") (r ">=0.3, <=0.5") (d #t) (k 0)))) (h "1dbwr8ng6ki1wiwnk3398jildri7y2afx9i5dl1xgpak6lr9166h")))

