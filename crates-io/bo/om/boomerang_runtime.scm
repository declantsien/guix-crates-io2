(define-module (crates-io bo om boomerang_runtime) #:use-module (crates-io))

(define-public crate-boomerang_runtime-0.2.0 (c (n "boomerang_runtime") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tinymap") (r "^0.1.0") (d #t) (k 0) (p "boomerang_tinymap")) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1a0f0n630sajjayp116gg94dpcrbmdpi20askg02ih1xddiia75y") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

