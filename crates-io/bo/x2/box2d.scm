(define-module (crates-io bo x2 box2d) #:use-module (crates-io))

(define-public crate-box2d-0.0.1 (c (n "box2d") (v "0.0.1") (h "0h3vy35fpj82hbdfn3rfgq9z6vcr9gllk5v6gdaznmgi1v12d9zv")))

(define-public crate-box2d-0.0.2 (c (n "box2d") (v "0.0.2") (h "0yqzanglgnwsjjyd1jb2vjjfapfkbism791qgbpjbmya8v25cdw5")))

