(define-module (crates-io bo ri boringtun-cli) #:use-module (crates-io))

(define-public crate-boringtun-cli-0.5.0 (c (n "boringtun-cli") (v "0.5.0") (d (list (d (n "boringtun") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("env"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "10rbvf4nkdc7rmk64mj3x59xfyzds6kyzms5xkpxjp8bfxwig47x") (y #t)))

(define-public crate-boringtun-cli-0.5.1 (c (n "boringtun-cli") (v "0.5.1") (d (list (d (n "boringtun") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("env"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "1a1q33r48nkqy8wmxs5fxrplaq5iprlsqlkbh0x2ihrpcwpzc8v9")))

(define-public crate-boringtun-cli-0.5.2 (c (n "boringtun-cli") (v "0.5.2") (d (list (d (n "boringtun") (r "^0.5.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("env"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "1xi300pgwbqjld1dcpr47p51gy6dwqwmfqgl6ddafym9avjc7yig")))

(define-public crate-boringtun-cli-0.6.0 (c (n "boringtun-cli") (v "0.6.0") (d (list (d (n "boringtun") (r "^0.6.0") (f (quote ("device"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("env"))) (d #t) (k 0)) (d (n "daemonize") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "0rb796nwr9l0qvwycm60l2wn62hfb3rfbzbxg5az6nzsb0lmwnb8")))

