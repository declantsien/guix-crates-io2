(define-module (crates-io bo ri boringauth) #:use-module (crates-io))

(define-public crate-boringauth-0.6.0 (c (n "boringauth") (v "0.6.0") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "nom") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "ring") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "179h64whj9h3h6wsl1mk30xx3dmjqz1r9nlhc5fx69249z4ifzki") (f (quote (("cbindings" "libc"))))))

(define-public crate-boringauth-0.6.1 (c (n "boringauth") (v "0.6.1") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "nom") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "ring") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "00xv44ld5sdvn7islx6icqf5j2187jg23pj7x667ibf9cgfspgyi") (f (quote (("cbindings" "libc"))))))

(define-public crate-boringauth-0.6.2 (c (n "boringauth") (v "0.6.2") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "nom") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "0vq1dpfz0xb5ymdr3xs4qsp0zzn7wq6fl04xny96jnhsq8whynf9") (f (quote (("cbindings" "libc"))))))

(define-public crate-boringauth-0.6.3 (c (n "boringauth") (v "0.6.3") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "nom") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "ring") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "0z76g039m8brqf08r31l1hb54npjq4zvwj0q781hszgwm1n0fd8z") (f (quote (("cbindings" "libc"))))))

(define-public crate-boringauth-0.6.4 (c (n "boringauth") (v "0.6.4") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "nom") (r "^2.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "ring") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "17my3f4fqk6fphkg9wq0j537j8h4pn9x61ac5xv7bwksrrnf23bn") (f (quote (("cbindings" "libc"))))))

(define-public crate-boringauth-0.7.0 (c (n "boringauth") (v "0.7.0") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "1br89i2jzy8sri7k7d5q0a7nzwwhvgj222897qypdl3mywzkn9a4") (f (quote (("cbindings" "libc"))))))

(define-public crate-boringauth-0.8.0 (c (n "boringauth") (v "0.8.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "ring") (r "^0.14") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1fhhshcd4kpnjpx2xxwv99cz6ksgx8ck12l2cw2m5b2qvavv51h1") (f (quote (("cbindings" "libc"))))))

(define-public crate-boringauth-0.9.0 (c (n "boringauth") (v "0.9.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "ring") (r ">= 0.15, < 0.17") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "173p3423jplbc8kkma2l27p6p13cvjv3c35s4z4jh12p0mwxnz7v") (f (quote (("cbindings" "libc"))))))

