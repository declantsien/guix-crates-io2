(define-module (crates-io bo ri boringascii) #:use-module (crates-io))

(define-public crate-boringascii-1.0.0 (c (n "boringascii") (v "1.0.0") (d (list (d (n "borsh") (r "^1.0.0-alpha.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zeroize") (r "^1") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "1cqiyd1bjnn2v7y9cs7wcf83iwfd8c5sm0cgf5z6xawhg751139p")))

