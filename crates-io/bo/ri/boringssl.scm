(define-module (crates-io bo ri boringssl) #:use-module (crates-io))

(define-public crate-boringssl-0.0.1 (c (n "boringssl") (v "0.0.1") (h "107p7mvznky0rnqzbzx5l7x6sshvxzvcm6pkksj63i89l6zqbrqj")))

(define-public crate-boringssl-0.0.2 (c (n "boringssl") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1bwz1pjq7dxhimq5y4zvw2lmkb416lblr9qrwzm46nxb89lcvshf")))

(define-public crate-boringssl-0.0.3 (c (n "boringssl") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1bxr0mrqqdbgrqgqsddldgfr4cq530x3v8zd9g4dkpyv5pjxlb7m")))

(define-public crate-boringssl-0.0.4 (c (n "boringssl") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1llxvabbgzq1v19aq9lc86hvijdgb6ghb4dinh4ias23sl60b6sk")))

(define-public crate-boringssl-0.0.5 (c (n "boringssl") (v "0.0.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0xvvd2kg82xqlxaygyj1zknmks83bpp0wpd0xziw3y7m4prwmchm")))

