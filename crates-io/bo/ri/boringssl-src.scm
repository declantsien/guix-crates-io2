(define-module (crates-io bo ri boringssl-src) #:use-module (crates-io))

(define-public crate-boringssl-src-0.1.0 (c (n "boringssl-src") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0dkp18wldnsyx7q0yhm3q69cnqnb144yskcdwi6nw2azfb6y64n2")))

(define-public crate-boringssl-src-0.2.0 (c (n "boringssl-src") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "15kwlbza6804y2q5q06g74la8chrzifwx7i7nw2nww1rnzqbj485")))

(define-public crate-boringssl-src-0.3.0+688fc5c (c (n "boringssl-src") (v "0.3.0+688fc5c") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0awjlrwdahhsvfwpidcslfqxhp0jw6jj7y979qpymlihz36sq0gr")))

(define-public crate-boringssl-src-0.4.0+fc44652 (c (n "boringssl-src") (v "0.4.0+fc44652") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "1pdz6fdh1xf1k34lw8hmn2vy8dkz1cjp7rd80iylcxhkns1vs63d")))

(define-public crate-boringssl-src-0.5.0+b9232f9 (c (n "boringssl-src") (v "0.5.0+b9232f9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0y9ip7ycm37y8m50mwwzpq4b4gsaf0g6qm0nmmfa6f200pifayk9")))

(define-public crate-boringssl-src-0.5.1+b9232f9 (c (n "boringssl-src") (v "0.5.1+b9232f9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0r0n0wsvd023avw8j54g4cxyvcvb07qy4fpmqx5045v5dwj0sm8k")))

(define-public crate-boringssl-src-0.5.2+6195bf8 (c (n "boringssl-src") (v "0.5.2+6195bf8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "1mf6mr59qhgf1zrx9wfs0rmqd6f9ya5x0g81la1flxp2qp66bdbs")))

(define-public crate-boringssl-src-0.6.0+e46383f (c (n "boringssl-src") (v "0.6.0+e46383f") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0mq4p5vs0xc4bqml86gkcdzjbchlvbq8jr9rl97dh560jwhw9pjy")))

