(define-module (crates-io bo ri boring-sys-imp) #:use-module (crates-io))

(define-public crate-boring-sys-imp-2.0.0 (c (n "boring-sys-imp") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "06jlkw4hw2rkvpid702h7zd9fkwqwgc9jp1cmmk2avmk5hx221zk") (f (quote (("fips")))) (y #t) (l "boringssl")))

(define-public crate-boring-sys-imp-2.0.1 (c (n "boring-sys-imp") (v "2.0.1") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0mkf35kmw05284k8703zzxxpr4frmx5d797ghzdyyaznb3m3xzvn") (f (quote (("fips")))) (l "boringssl")))

(define-public crate-boring-sys-imp-2.0.2 (c (n "boring-sys-imp") (v "2.0.2") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1mj85wk0vmfnrgvq244palk9ql4c0sj209550fr0yipmyyp91yv9") (f (quote (("fips")))) (l "boringssl")))

(define-public crate-boring-sys-imp-2.0.3 (c (n "boring-sys-imp") (v "2.0.3") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1i2c6hpsfc2naahlpj8jd3m77vws1fg37w98s2qc186gphs202cj") (f (quote (("fips")))) (l "boringssl")))

(define-public crate-boring-sys-imp-2.0.4 (c (n "boring-sys-imp") (v "2.0.4") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "old_bindgen") (r "^0.60.1") (f (quote ("runtime"))) (o #t) (k 1) (p "bindgen")))) (h "0pckcdb9pjlzssc0mdbn632pciwxrvsa7yws8d68n981k746i39x") (f (quote (("fips")))) (y #t) (l "boringssl") (s 2) (e (quote (("old_bindgen" "dep:old_bindgen") ("default" "dep:bindgen"))))))

(define-public crate-boring-sys-imp-2.0.5 (c (n "boring-sys-imp") (v "2.0.5") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "old_bindgen") (r "^0.60.1") (f (quote ("runtime"))) (o #t) (k 1) (p "bindgen")))) (h "1krj407iv5l8c6yv1c7jh870f5l4gxji3jpzr5nxlcal8rr99867") (f (quote (("fips")))) (l "boringssl") (s 2) (e (quote (("old_bindgen" "dep:old_bindgen") ("default" "dep:bindgen"))))))

(define-public crate-boring-sys-imp-2.0.6 (c (n "boring-sys-imp") (v "2.0.6") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "old_bindgen") (r "^0.60.1") (f (quote ("runtime"))) (o #t) (k 1) (p "bindgen")))) (h "0zbbcj6r5ks8r4vxd2j1xwqysl6fxsy9r5zgddzcgv1qklijmzgb") (f (quote (("fips")))) (l "boringssl") (s 2) (e (quote (("old_bindgen" "dep:old_bindgen") ("default" "dep:bindgen"))))))

(define-public crate-boring-sys-imp-2.1.0 (c (n "boring-sys-imp") (v "2.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0jpd79b3x19g2amvrv82hg5bzfb5n90fs3xaljyr246a0ydd4c84") (f (quote (("fips")))) (l "boringssl") (s 2) (e (quote (("default" "dep:bindgen"))))))

