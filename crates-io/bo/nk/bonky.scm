(define-module (crates-io bo nk bonky) #:use-module (crates-io))

(define-public crate-bonky-0.2.0 (c (n "bonky") (v "0.2.0") (h "12f36aib82mh4hrbv6qn0wfckvxjqfn7p2mb5q0dwxjd0zzjkxhl") (r "1.63.0")))

(define-public crate-bonky-0.3.0 (c (n "bonky") (v "0.3.0") (h "1jpd0m0683nbfrpr6mkjg8cniszvv64djl4ggiycgdkasr0m0sr8") (r "1.63.0")))

(define-public crate-bonky-0.3.1 (c (n "bonky") (v "0.3.1") (h "165453m49ijl8wvlmx2bg86v1m6113zfaxb0k4yiv9hfdvbyqqcq") (r "1.63.0")))

(define-public crate-bonky-0.3.2 (c (n "bonky") (v "0.3.2") (h "05nblnkk7v7zcla7w0wf1p01cdgzk79v2x0qs9l3dqzy391zs8lp") (r "1.63.0")))

(define-public crate-bonky-0.4.0 (c (n "bonky") (v "0.4.0") (h "06yycmav4rc3ng8n33zvww2kpqcwh7rkffv6hwcn20g219zn7c4m")))

