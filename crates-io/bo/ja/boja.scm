(define-module (crates-io bo ja boja) #:use-module (crates-io))

(define-public crate-boja-0.1.0 (c (n "boja") (v "0.1.0") (h "03g309kpw6i9p7jyfgb9z6q58d8zxwlaa7jm8xfxylraidahxfdq")))

(define-public crate-boja-0.2.0 (c (n "boja") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "ariadne") (r "^0.2.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1aw077pj5acs2lchmi504fkl4mcbw1msrnbpmqlbg55agkyc01d3")))

