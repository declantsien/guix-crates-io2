(define-module (crates-io bo x- box-collections) #:use-module (crates-io))

(define-public crate-box-collections-0.1.0 (c (n "box-collections") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1hc94nh8si2x6cicmqdxqmqz0xfs9kmakgkkicajw40kw3llq40y") (f (quote (("std") ("priority-queue") ("default" "std") ("binary-heap")))) (r "1.47")))

