(define-module (crates-io bo x- box-counter) #:use-module (crates-io))

(define-public crate-box-counter-0.1.0 (c (n "box-counter") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s3cdyvgrf4jcbxjrgnqmh4r574ahw75c32728x1gwwxr1mqpfss") (f (quote (("rate") ("default") ("count")))) (r "1.47")))

