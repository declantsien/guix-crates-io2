(define-module (crates-io bo x- box-self) #:use-module (crates-io))

(define-public crate-box-self-0.1.0 (c (n "box-self") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0pgh93k4vyj1qjygs8lrm37wzpggm76q6b8xhk5d8gy1ag96ly8s")))

(define-public crate-box-self-0.1.1 (c (n "box-self") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1wxnlgxynq6sxdimi2361dxi83j0phi2v4hk42vg21jiv3gi442m")))

(define-public crate-box-self-0.1.2 (c (n "box-self") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "05n08wrkwkspvzyq9yqrvrgrpf6nc2ra9h06k0fanss091sm8z4p")))

(define-public crate-box-self-0.1.3 (c (n "box-self") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "13k32vjyln082igf0dcq4awnapgbss5mfrbf7gj4ylpiivh30k2c")))

(define-public crate-box-self-0.1.4 (c (n "box-self") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "07ap2wk9r3apmfgzd6475gww0snzlldv6ggy4ncp04zg007b5als")))

(define-public crate-box-self-0.1.5 (c (n "box-self") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07kgzj9avgk5y9awb2ilc9vga96kxaccqsd23x43l7fp250q7qix")))

