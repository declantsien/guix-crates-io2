(define-module (crates-io bo x- box-dyn) #:use-module (crates-io))

(define-public crate-box-dyn-0.0.5 (c (n "box-dyn") (v "0.0.5") (d (list (d (n "prettyplease") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fpbkjahbmfsgvaa8sv36in11wdqjka6ad917v8app6mdhjkpia4")))

(define-public crate-box-dyn-0.0.6 (c (n "box-dyn") (v "0.0.6") (d (list (d (n "prettyplease") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0sq6iq31cqhf1irx7ly0j66icgpk172jpqsc9z0jmqrvdfrhm9xi")))

(define-public crate-box-dyn-0.0.7 (c (n "box-dyn") (v "0.0.7") (d (list (d (n "prettyplease") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "02f0id9dy7d2nm2axv5gpb01djc6d3rzqz76sc3mx3439r8z6pfr")))

(define-public crate-box-dyn-0.0.8 (c (n "box-dyn") (v "0.0.8") (d (list (d (n "prettyplease") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w8x0mabnllkjmcppm5wjs17ajixhlblg65xn682w8n5kss20y27")))

