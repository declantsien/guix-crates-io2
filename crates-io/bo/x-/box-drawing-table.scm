(define-module (crates-io bo x- box-drawing-table) #:use-module (crates-io))

(define-public crate-box-drawing-table-0.1.0 (c (n "box-drawing-table") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.13.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "119ip67527a7yhb1wa6pqw6qqfd9bqkjs5wkl7np8c8c9r1mk9xf")))

