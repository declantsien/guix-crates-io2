(define-module (crates-io bo x- box-macro) #:use-module (crates-io))

(define-public crate-box-macro-0.1.0 (c (n "box-macro") (v "0.1.0") (h "0b4qiiwh121bgagbjx35hblj1fqqy80wvmdhzdn5v75pwvhq07n6")))

(define-public crate-box-macro-0.2.0 (c (n "box-macro") (v "0.2.0") (h "1cxlpxh3dx13jw47ylgpg6kxlrk86jjnfpzapz457yp6qhbxm101")))

