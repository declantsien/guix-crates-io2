(define-module (crates-io bo ro boron-lang) #:use-module (crates-io))

(define-public crate-boron-lang-0.1.0 (c (n "boron-lang") (v "0.1.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)))) (h "1s8j5h2q0mc1zrkv48ai6wf1ahv2p9844fpjlswlv9plrnhjmlv8")))

(define-public crate-boron-lang-0.2.0 (c (n "boron-lang") (v "0.2.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)))) (h "1vlm14z9c27aqg4zyq4qrsvzpm00c1q8a2rfgihbz9rssn34bz9k")))

(define-public crate-boron-lang-0.3.0 (c (n "boron-lang") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)))) (h "141an132hhnj3q4qpilr4xf1cgjih3xg9j8apylcdpf3hdrkbv7n")))

(define-public crate-boron-lang-0.4.0 (c (n "boron-lang") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)))) (h "0s8ky57p8fddcfzlg7p6yb686z68n6lmqkd62qc56c4kavrkbxn0")))

(define-public crate-boron-lang-0.5.0 (c (n "boron-lang") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)))) (h "1nzcw7376r0bm6dksnvv7h1d229lqcygn29pinzhsacz197aq225")))

(define-public crate-boron-lang-0.5.1 (c (n "boron-lang") (v "0.5.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)))) (h "0zk1q2l2q3hbd319lygzw84f0b2b83j36a2ar7n395ry0hc35p2v")))

(define-public crate-boron-lang-0.5.2 (c (n "boron-lang") (v "0.5.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)))) (h "01awy4a9jss2i0l0x4qw3a4dcbwdx1wyrhivvj3wvqw4z3qlvf2w")))

(define-public crate-boron-lang-0.5.3 (c (n "boron-lang") (v "0.5.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)))) (h "0k6r03r93py8c87b778ki2ds4v36lbcr7z020h9g6x3sgickpmyv")))

(define-public crate-boron-lang-0.6.0 (c (n "boron-lang") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0i7grdcmkqk5zixpcqv40fh3y6xwp4ykk2xaqv5gd269k774bfnj")))

(define-public crate-boron-lang-0.7.0 (c (n "boron-lang") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0myydscaiajfwvq09cqqb2fhx9m7hn6vx8jgpzyxd1qg1jpp4nl0")))

(define-public crate-boron-lang-0.8.0 (c (n "boron-lang") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0h73dam9x2l5409ii34lw7czzvrm9q104b2iqjgwzr0jqarbds2l")))

(define-public crate-boron-lang-0.9.0 (c (n "boron-lang") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0d47151913s1yp3s9fazy9d6gy8f6adckf4bmbj52i62lbbri11k")))

(define-public crate-boron-lang-0.10.0 (c (n "boron-lang") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "08wfz612bynrgg474cgvciak0khj3n2wmfgrmj15drx01xhjv0p3")))

(define-public crate-boron-lang-0.11.0 (c (n "boron-lang") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "00skrfgxv8k0jnin75ld5bn8cv8y8m7aqw55r79z6rvsqmzx74np")))

(define-public crate-boron-lang-0.12.0 (c (n "boron-lang") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0p4l8iwp32nq1hv0jzyr623jnpk0diwyprrnqzsp23x7rz0gka6q")))

(define-public crate-boron-lang-0.13.0 (c (n "boron-lang") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0jy9vs696hrlr23fpvphvah69r1sfr2kzkv6b09gbnxd6r7yb63b")))

(define-public crate-boron-lang-0.13.1 (c (n "boron-lang") (v "0.13.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0kcz5kspliinpfwaplg50nc4rhi57j6r8ff685p9jx838znlidn1")))

(define-public crate-boron-lang-0.13.2 (c (n "boron-lang") (v "0.13.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0kcqqzy3ghg8vzjar48r5yzy6rz8hcmvjmrwz77nd63bjhwwmhis")))

(define-public crate-boron-lang-0.14.0 (c (n "boron-lang") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1xyhzqaw5x3kijqxp0p43yd001m8sc3wlh6q58mvapj4wh43r7gh")))

(define-public crate-boron-lang-0.14.1 (c (n "boron-lang") (v "0.14.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "15q2br5six09im7kwail5q9jb8mdxj6s0q3g2xilglcjj0r7nhld")))

(define-public crate-boron-lang-0.15.0 (c (n "boron-lang") (v "0.15.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0s4s31464vjvi8fjvjvj1s2rcphfm0y04x844a6x2s2ldzcr2nz3")))

(define-public crate-boron-lang-0.15.1 (c (n "boron-lang") (v "0.15.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "039a6qnlf26l9z6vbqwp4n61yf0bs4la9p64vpqg4wgzizgsnsyd")))

(define-public crate-boron-lang-0.16.0 (c (n "boron-lang") (v "0.16.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1xj3519pj40ar6lzy3bakxzm79h87a14zvc16z4pna0dxqymg1dm")))

(define-public crate-boron-lang-0.17.0 (c (n "boron-lang") (v "0.17.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1r770k68cy1lg3fzp7s4wd2j44p12wfq0z321vxf0q4m5502ifvf")))

(define-public crate-boron-lang-0.17.1 (c (n "boron-lang") (v "0.17.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0kx3w3dz15mlbw8m6ly7d15mmry9rj1dyncxscmg0k88qz85rqgi")))

(define-public crate-boron-lang-0.18.0 (c (n "boron-lang") (v "0.18.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "03iqxg14iqzv4rz3bxrx06362yvj5ni3sf8q3mnm54q4ljhqgf4w")))

(define-public crate-boron-lang-0.19.0 (c (n "boron-lang") (v "0.19.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1dbvlcb3m8qh22g47n550yzg6r7z7fdp9lli81n3zhgkm5hzb8vw")))

(define-public crate-boron-lang-0.19.1 (c (n "boron-lang") (v "0.19.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "15q3dq2ydg43cfac9dv6122wbbfnf0c5i9dh2wz965hycww01q2n")))

(define-public crate-boron-lang-0.20.0 (c (n "boron-lang") (v "0.20.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1baid9745h4i1hs5i9w53k8k56plcls7cdf2r1hpy49241cb7308")))

(define-public crate-boron-lang-0.20.1 (c (n "boron-lang") (v "0.20.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1gnl3ai6vci8kwshizdbkfakf2bik7b58iid5wcrk7dswnxlhmbg")))

(define-public crate-boron-lang-0.20.2 (c (n "boron-lang") (v "0.20.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0jv1bng929x6zmr7mv28fnipwf673wcayw59h6mzqc3fpavhlnxc")))

(define-public crate-boron-lang-0.21.0 (c (n "boron-lang") (v "0.21.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0b29aq6sf2qxlazzjws25q2bj211swpzp9lgflb93a9lwg6g8ir1")))

(define-public crate-boron-lang-0.22.0 (c (n "boron-lang") (v "0.22.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1zwg44mh8a08c5brq6isx08rx9s3sxb14x902w6i89yh0sqaqwrv")))

(define-public crate-boron-lang-0.23.0 (c (n "boron-lang") (v "0.23.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "17ws70mp0ha0a93ps0vi3d6bmlzjsm3ayxmqak3q6n47ljxvcdav")))

(define-public crate-boron-lang-0.23.1 (c (n "boron-lang") (v "0.23.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0wz1kjvgpi1gv4h66fj5dwcis2yrv4nkc9xw0pm9jpzppy8lmg0c")))

(define-public crate-boron-lang-0.23.2 (c (n "boron-lang") (v "0.23.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0qsc9lyc227098s4bj45qyr78xas1dh01c4vy0zngg0qz75acbc8")))

(define-public crate-boron-lang-0.24.0 (c (n "boron-lang") (v "0.24.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.0") (d #t) (k 0)))) (h "056xjhhygvijm7nxxrjs0b1r3q15m952gqr8yz5a8c7dilcbli3k")))

(define-public crate-boron-lang-0.25.0 (c (n "boron-lang") (v "0.25.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "06ipkb782ibiz0a16drjs243zzp67djn2a57vfhhxg7jnkc15wgd")))

(define-public crate-boron-lang-0.25.1 (c (n "boron-lang") (v "0.25.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1x8gszymg8ksnd15bx48jssx19ar0n9iyfyyy3pa1vm6cjb0avav")))

(define-public crate-boron-lang-0.26.0 (c (n "boron-lang") (v "0.26.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "02mbphlia9cj6k37ilccminh47g6av8j5lsy3hbvzm8fbj4927sf")))

