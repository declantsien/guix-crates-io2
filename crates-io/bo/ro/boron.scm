(define-module (crates-io bo ro boron) #:use-module (crates-io))

(define-public crate-boron-0.0.1 (c (n "boron") (v "0.0.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "1xzcc5zyqq4jpxdkq07maynhcfj7q3apwdq28w2kw5pq568fa4x9")))

(define-public crate-boron-0.0.2 (c (n "boron") (v "0.0.2") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "14lj4gkbjnrkjzpkz8jh7z4m7z2xqlzpd429b87k7bj1aky9s6f1")))

