(define-module (crates-io bo ot boot-loader-spec) #:use-module (crates-io))

(define-public crate-boot-loader-spec-0.1.0 (c (n "boot-loader-spec") (v "0.1.0") (h "0mnm6bmxhiipm19sga3wbak4d5pnsff7rm00kmqcx9x7ra1g3274")))

(define-public crate-boot-loader-spec-0.1.1 (c (n "boot-loader-spec") (v "0.1.1") (h "0b4vbbdjx42vaw00c48fwf12xclv0c7c95xs8fdv226zphnx7f26")))

(define-public crate-boot-loader-spec-0.1.2 (c (n "boot-loader-spec") (v "0.1.2") (h "0s7qsb1fnz0i5lgc3iwc0hrlfpr75w3acvx88g7nspa4bnl2x2cr")))

(define-public crate-boot-loader-spec-0.1.3 (c (n "boot-loader-spec") (v "0.1.3") (h "12d017813jlwb9yx9367dnr2152m965ls8d4jdm181vrzgkjds8k")))

(define-public crate-boot-loader-spec-0.1.4 (c (n "boot-loader-spec") (v "0.1.4") (h "0js0hhbgiwx2km7db5plicrkjpb0zffl3ldy957dysipwxmm7cdc")))

