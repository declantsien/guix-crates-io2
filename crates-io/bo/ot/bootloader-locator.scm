(define-module (crates-io bo ot bootloader-locator) #:use-module (crates-io))

(define-public crate-bootloader-locator-0.0.1 (c (n "bootloader-locator") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.11.1") (d #t) (k 0)) (d (n "locate-cargo-manifest") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "19dv5k8wnf5xdd5mndnwssry467wjqjfyyvwlbs01j0xfr09q582")))

(define-public crate-bootloader-locator-0.0.2 (c (n "bootloader-locator") (v "0.0.2") (d (list (d (n "cargo_metadata") (r "^0.11.1") (d #t) (k 0)) (d (n "locate-cargo-manifest") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0ig6v81y9jkzyszfvnlfij3ihi37cfwzl49dgg5k5j76xrp4rb98")))

(define-public crate-bootloader-locator-0.0.3 (c (n "bootloader-locator") (v "0.0.3") (d (list (d (n "cargo_metadata") (r "^0.11.1") (d #t) (k 0)) (d (n "locate-cargo-manifest") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1nzib5s677viwckkx1g564c7a777xvvjjsynga07qb3kn98ijap2")))

(define-public crate-bootloader-locator-0.0.4 (c (n "bootloader-locator") (v "0.0.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0lfm5gz48rs2zvack5wp8g9ni942xcqhfp9f5xic4clx6frrvama")))

