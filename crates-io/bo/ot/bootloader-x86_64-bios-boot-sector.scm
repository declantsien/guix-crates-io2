(define-module (crates-io bo ot bootloader-x86_64-bios-boot-sector) #:use-module (crates-io))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.1.0-beta (c (n "bootloader-x86_64-bios-boot-sector") (v "0.1.0-beta") (h "05lpbrssjzrak6nl4knfhb8fzkb3cwwgi42hyzhxp8bn8blrz85i")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.1.0-beta.1 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.1.0-beta.1") (h "1sbd84bmazjq80kkv32mk98ax3b56yk5gzxjr6hp4r177cqqikkz")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.0-beta.2 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.0-beta.2") (h "027b56634ymrmh20zl29m255kysq6x1w87q8k0p3f46jmb4rvfv7")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.0-beta.4 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.0-beta.4") (h "0223fgz8skkk5swhvlc8j4yyi43mjj4y9xvwk7zc7msa9l9cyp0d")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.0-beta.5 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.0-beta.5") (h "02iqlryirxflb5a28gv6glyccjg8h6xajwl549s1i74jmpl7i3x4")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.0-beta.8 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.0-beta.8") (h "163gnqfar4dil7f3nir44xwzyl1mvmmw85hdvxbjmq64gfsr9ssa")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.0 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.0") (h "1c1c5z8271xqzg28n4n6r51jb3qbgkvgl8m8rn9rna61d0hx6lgr")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.1 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.1") (h "1w9v4ng556rwvldwf31561crdm8f45mc716b693svn0nb52kndzh")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.2 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.2") (h "0sbqls2zk4mdcp1q3zv65q9zrnxvsdmz3526lm3ifylhh1prkh5v")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.3 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.3") (h "0v8wxnnmym9mpka8sa27bg5a4xr5v26wigcqr2vv577pzd9hf8vg")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.4 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.4") (h "15rzck7l8ng40fxy4cmkvvpsklpnbjqbxxsfxb80pn8118m4rc0d")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.5 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.5") (h "04hdbf8xbihc5pqxagzi3sqajfb9fcgw12dxjwcn7gxi0rpgqkpi")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.6 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.6") (h "1b2a07hk7qvawwlycqxh0l4970ggh3rica5vkpf8prvhs6r9zfr2")))

(define-public crate-bootloader-x86_64-bios-boot-sector-0.11.7 (c (n "bootloader-x86_64-bios-boot-sector") (v "0.11.7") (h "1lc28yiqv6zgdp4sag7x1yl1xsb0fnn3dibgbq6haq91h6w1r94s")))

