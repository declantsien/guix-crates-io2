(define-module (crates-io bo ot bootloader_precompiled) #:use-module (crates-io))

(define-public crate-bootloader_precompiled-0.2.0-alpha-001 (c (n "bootloader_precompiled") (v "0.2.0-alpha-001") (h "09cc0zlbf3pglj1v5k77a5gjc2adx8c08mp1sblq28v2lyk25wiy")))

(define-public crate-bootloader_precompiled-0.2.0-alpha-002 (c (n "bootloader_precompiled") (v "0.2.0-alpha-002") (h "0yrqynw3129cdb4ilb6qxp9kmwz9brc7zk8mabxdzs8f4cizssin")))

(define-public crate-bootloader_precompiled-0.2.0-alpha-003 (c (n "bootloader_precompiled") (v "0.2.0-alpha-003") (h "154z0sqhdafmxc3l2d2bybj62z2l4hj3n1kgim9y73vdmi6gqf4a")))

(define-public crate-bootloader_precompiled-0.2.0-alpha-004 (c (n "bootloader_precompiled") (v "0.2.0-alpha-004") (h "0wm6041fl1y7crk8dfns860zg5mp9vxk5fzn57ryn5sj1mv0rr8j")))

(define-public crate-bootloader_precompiled-0.2.0-alpha-005 (c (n "bootloader_precompiled") (v "0.2.0-alpha-005") (h "1sqxzi023gkj9irspfw8h2gmcqwcj7j33z249wx28x4h03zca7js")))

(define-public crate-bootloader_precompiled-0.2.0-beta (c (n "bootloader_precompiled") (v "0.2.0-beta") (d (list (d (n "os_bootinfo") (r "^0.2.0") (d #t) (k 0)))) (h "130jhyj5q6sid6c02i7a9gbih7w54k8hdnaip4wa6435khj9n2gs")))

(define-public crate-bootloader_precompiled-0.2.0 (c (n "bootloader_precompiled") (v "0.2.0") (d (list (d (n "os_bootinfo") (r "^0.2.0") (d #t) (k 0)))) (h "1rhb63v9mng3g0r4fiya6g09ba0z5iknlr3i3s05qziy9w4n4lr4")))

(define-public crate-bootloader_precompiled-0.3.0 (c (n "bootloader_precompiled") (v "0.3.0") (d (list (d (n "x86_64") (r "= 0.2.11") (d #t) (k 0)))) (h "0x6k9d43h5yci3nfkic3ircsbl815ni7hdhwldx8r8cfaf655p8f")))

