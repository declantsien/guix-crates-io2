(define-module (crates-io bo ot boot-contract-derive) #:use-module (crates-io))

(define-public crate-boot-contract-derive-0.1.3 (c (n "boot-contract-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rgx6zlga1rwn0jzh3lbgjij08y74h8d9mmh501h0lxapbpx61cb")))

(define-public crate-boot-contract-derive-0.2.0-rc.1 (c (n "boot-contract-derive") (v "0.2.0-rc.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12xi1bgd0pxzbhcagwmnxwg16jjd8r0558pg6f6gywj80cg34y49")))

(define-public crate-boot-contract-derive-0.2.0 (c (n "boot-contract-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kvr4yh1pjx3f17lgwf9qvpaywm4calwl5hd1v7gpdr8zp48hcnh")))

(define-public crate-boot-contract-derive-0.3.0 (c (n "boot-contract-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qwj0wrgkqmh2czw2280mgbbphnsygg22jdf6wrxlqnxmkq29rwp")))

(define-public crate-boot-contract-derive-0.4.0 (c (n "boot-contract-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1498ay5r84i6jxaam0r6my29rqglhkq7fm9b7v29f20cqknwvq2k")))

(define-public crate-boot-contract-derive-0.5.0 (c (n "boot-contract-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05lispj2grx4k14dhhrsqsyqr8r2fc50dg8gk38047q1nyp3v15d")))

(define-public crate-boot-contract-derive-0.5.1 (c (n "boot-contract-derive") (v "0.5.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12qvvgd27p7byqqm11mg9agb4kawkmncwxp3xsvvcljva6k6fcwk")))

(define-public crate-boot-contract-derive-0.6.0 (c (n "boot-contract-derive") (v "0.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yvw5bw7vhf9jp8f2yggq6g7qxjhilll22jwppv1ahw36kqxcbbw")))

(define-public crate-boot-contract-derive-0.6.1 (c (n "boot-contract-derive") (v "0.6.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gvjprssrih57ds3y08gisc9awwv7kq8iw5s1kb7nyd59f9v2s63")))

(define-public crate-boot-contract-derive-0.8.0-beta.1 (c (n "boot-contract-derive") (v "0.8.0-beta.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fy73wq3zrw927685g5jvigx4nc8va6wvklksgkx733fxhjahaby")))

(define-public crate-boot-contract-derive-0.8.0 (c (n "boot-contract-derive") (v "0.8.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11jj9qrwgb2lb4b3xxgc5lqwcp52xcxv165dv5zd5jw4smkd4437")))

(define-public crate-boot-contract-derive-0.9.0 (c (n "boot-contract-derive") (v "0.9.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jnrg1c8h89r8927r63qva4aad9p03qvr5qp7fm7gyzzgcblzn1q")))

(define-public crate-boot-contract-derive-0.10.0 (c (n "boot-contract-derive") (v "0.10.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zbimypfhr043aqzd6gwrn5y9z6kmj9am86whlci4bsxzc703mdn")))

