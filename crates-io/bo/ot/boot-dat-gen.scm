(define-module (crates-io bo ot boot-dat-gen) #:use-module (crates-io))

(define-public crate-boot-dat-gen-0.1.0 (c (n "boot-dat-gen") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.15") (d #t) (k 0)) (d (n "tx-custom-boot") (r "^0.1.0") (d #t) (k 0)))) (h "0xmxpfpml6hrc0fxqz1k5cwcsgxv92qmgmp57fv5x52n15y6ami9")))

(define-public crate-boot-dat-gen-0.2.0 (c (n "boot-dat-gen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.15") (d #t) (k 0)) (d (n "tx-custom-boot") (r "^0.2.0") (d #t) (k 0)))) (h "058wrx7qz00ipdc59j1c7azzirkmxw2kg3nghly8y3fm7b38wlwr")))

(define-public crate-boot-dat-gen-0.2.1 (c (n "boot-dat-gen") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.15") (d #t) (k 0)) (d (n "tx-custom-boot") (r "^0.3.0") (d #t) (k 0)))) (h "02h9hsq8g0yrra2fi5dwp7wckcgib4sfzgiqs0v6bhapsld8y80y")))

(define-public crate-boot-dat-gen-0.2.2 (c (n "boot-dat-gen") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (d #t) (k 0)) (d (n "tx-custom-boot") (r "^0.3.0") (d #t) (k 0)))) (h "0igxgxr4kpa63w6r32ixgvfskp9xiny8hj8wm5z6f2xh7n96rmbh")))

