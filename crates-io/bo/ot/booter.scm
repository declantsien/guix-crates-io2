(define-module (crates-io bo ot booter) #:use-module (crates-io))

(define-public crate-booter-0.1.0 (c (n "booter") (v "0.1.0") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "01clh2hy5g68fnrybcdhn5aksgj9j2dpi3b5a9qnhffxnw85fn9d") (y #t)))

(define-public crate-booter-1.0.0 (c (n "booter") (v "1.0.0") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "07a5mc5c578y0hrfygb0x92ly5bi4srm81xmsxn3w6fkbq0f68m8") (y #t)))

(define-public crate-booter-1.0.1 (c (n "booter") (v "1.0.1") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "1qnnkwkz4akam3aq4klhxq2vn8vvagscr6rilypjzfiky8ixxq4n") (y #t)))

(define-public crate-booter-1.0.2 (c (n "booter") (v "1.0.2") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "0khiqjx5hbvbydij66iz94s51rsk0gfhx9ik45903w44ihnj4f6w") (y #t)))

(define-public crate-booter-1.0.3 (c (n "booter") (v "1.0.3") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "0w533laiylc4av5bf9ck9gf822snrjvym3zq4s98bhd7wrwdxwv5") (y #t)))

(define-public crate-booter-1.0.4 (c (n "booter") (v "1.0.4") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "05ynn4797ikm2jig591z88y491fa7dzjkhiiq23cs3cdxjyl48r8") (y #t)))

(define-public crate-booter-1.0.5 (c (n "booter") (v "1.0.5") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "0j507f040wx4cll5yjd1r8f0dhamz0miwqn0bgv81p137f3gxhwr") (y #t)))

(define-public crate-booter-1.0.6-beta.1 (c (n "booter") (v "1.0.6-beta.1") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "0m9c0bil6yllmxfhy7agmgj2m6c2x7dsli5fwkx6bdlaihn43nid") (y #t)))

(define-public crate-booter-1.1.0 (c (n "booter") (v "1.1.0") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "1nvwss54pb6ds0yfq7idr1dnbycd7psj663qrn4zlizsxbpsjrnz") (y #t)))

(define-public crate-booter-1.1.1-beta.1 (c (n "booter") (v "1.1.1-beta.1") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "179x1mkjc8appmgbnambv3r1pwyyldki24m01n4xf1wj9qbrg5hc") (y #t)))

(define-public crate-booter-1.1.1 (c (n "booter") (v "1.1.1") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "18yiga2jpds78hamrd6csxgkhzms9kk6a5lxri6z0g5fj53p43d3")))

(define-public crate-booter-1.1.2 (c (n "booter") (v "1.1.2") (d (list (d (n "atomic-take") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.1.10") (d #t) (k 0)))) (h "1377cn0k9cnh77pln0j3bj350kf0f776ffn6mi0n33w2q3kyjkhb")))

