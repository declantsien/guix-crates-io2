(define-module (crates-io bo ot bootstub-updater) #:use-module (crates-io))

(define-public crate-bootstub-updater-1.0.0 (c (n "bootstub-updater") (v "1.0.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1qd3x575yz5g842ka914dkflc4yi38ch955k3v0xsaxrr81rn2g0")))

