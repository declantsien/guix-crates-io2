(define-module (crates-io bo ot bootstraps) #:use-module (crates-io))

(define-public crate-bootstraps-0.1.0 (c (n "bootstraps") (v "0.1.0") (h "0z3g0candxs7zryrvfvalz4v4n19k39070gmaip41rrwgprl3j3r")))

(define-public crate-bootstraps-0.1.1 (c (n "bootstraps") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bgsi4rhcq96iq4j8p3yisyscaybaqh2fiz14759p7srqg949yqm")))

