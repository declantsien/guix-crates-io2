(define-module (crates-io bo ot booth) #:use-module (crates-io))

(define-public crate-booth-0.0.0 (c (n "booth") (v "0.0.0") (h "093026xdfiqg8dnzhm33y25pfzqlg0la6v4yqfdvgc8dyr4i3zvg") (r "1.78.0")))

(define-public crate-booth-0.0.1 (c (n "booth") (v "0.0.1") (h "0lsndchcn1vngy94ga06x99ww8id6ak7q098nmddpl42c54816zq") (r "1.78.0")))

