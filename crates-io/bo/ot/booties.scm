(define-module (crates-io bo ot booties) #:use-module (crates-io))

(define-public crate-booties-0.1.0 (c (n "booties") (v "0.1.0") (h "0njj5db7lk4k0n4qblhn9m35w1fc8afqkb0yrrp1h1fqrn0s5xjy") (y #t)))

(define-public crate-booties-0.1.1 (c (n "booties") (v "0.1.1") (h "1lls2khm9xxswvsgfyqzlr25vximcal4zx957g8a0sh0hfn8pckf") (y #t)))

