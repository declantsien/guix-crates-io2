(define-module (crates-io bo ot bootsector) #:use-module (crates-io))

(define-public crate-bootsector-0.1.0 (c (n "bootsector") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)))) (h "07q39k5p5502gs308izkbxrgsdayh51mr2fknpvp5w6b1sxndzm1")))

(define-public crate-bootsector-0.1.1 (c (n "bootsector") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)))) (h "0jld812p62zspww1v713hnsz257gb0dgkrfb8y74hsa6407z1piw")))

(define-public crate-bootsector-0.1.2 (c (n "bootsector") (v "0.1.2") (d (list (d (n "crc") (r "^1") (d #t) (k 0)))) (h "0vqgf632whgyylp7v5b8gfrn9wj67pwd1b5v5cngh2djwy2wnfx6")))

(define-public crate-bootsector-0.1.3 (c (n "bootsector") (v "0.1.3") (d (list (d (n "crc") (r "^1") (d #t) (k 0)))) (h "1g51hcjvv3r7fh0i9n8xk2wq6gj5irxc73fhq3m9vzhr68fy28iy")))

(define-public crate-bootsector-0.1.4 (c (n "bootsector") (v "0.1.4") (d (list (d (n "crc") (r "^1") (d #t) (k 0)))) (h "0wvg5bvc6jjd897k00z72vrkcwas1j7yccx7jf6gc3nfy14dc5hh")))

(define-public crate-bootsector-0.1.5 (c (n "bootsector") (v "0.1.5") (d (list (d (n "crc") (r "^1") (d #t) (k 0)))) (h "01q0qf4yf2iqycyw0723pcy39ha5mc3rd39i14dddkyvi8ppyy74")))

(define-public crate-bootsector-0.1.6 (c (n "bootsector") (v "0.1.6") (d (list (d (n "crc") (r "^2") (d #t) (k 0)))) (h "0wyvz9akqzpsg0yi5kzgp0mqvmdpglizrcc3i0h2mx1w2zk94c9z")))

(define-public crate-bootsector-0.2.0 (c (n "bootsector") (v "0.2.0") (d (list (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "positioned-io2") (r "^0.3") (d #t) (k 0)))) (h "08r0jv97qdn487c4wsa1if51n8p62jpj933vs25a3c26ihlby3hc")))

