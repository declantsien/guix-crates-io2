(define-module (crates-io bo ot bootloader-boot-config) #:use-module (crates-io))

(define-public crate-bootloader-boot-config-0.11.1 (c (n "bootloader-boot-config") (v "0.11.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "14v93rnf997gr0m08ls7gnyilny0506r4gvmcv963vvrr6hzl249")))

(define-public crate-bootloader-boot-config-0.11.2 (c (n "bootloader-boot-config") (v "0.11.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "01ypwnzin7x2n2nw1yv21l30h2nmz4cbv0l81rv69i8clfdsi7qr")))

(define-public crate-bootloader-boot-config-0.11.3 (c (n "bootloader-boot-config") (v "0.11.3") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "1yjm97v4wnf4adsxzf4z1jplg0550561b37n09a1wl1r1gjz3rca")))

(define-public crate-bootloader-boot-config-0.11.4 (c (n "bootloader-boot-config") (v "0.11.4") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "0gkfnbz2kvlazrxaj1zh6z69a5m05bbrxsgc5142ai3bnbvhsv53")))

(define-public crate-bootloader-boot-config-0.11.5 (c (n "bootloader-boot-config") (v "0.11.5") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "0sfj3s67dwpy91xgjpni5k89ibrpf83fm4nkccp6yg4mga4zw85y")))

(define-public crate-bootloader-boot-config-0.11.6 (c (n "bootloader-boot-config") (v "0.11.6") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "1lblqn0ypgs1c3v6c9jxvy4ppp09kca4g84zrn69alh683xgmp33")))

(define-public crate-bootloader-boot-config-0.11.7 (c (n "bootloader-boot-config") (v "0.11.7") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "05gavvvmsaqmjkv17ny5wz3dil4yf6lja42kchcdk6ry7g4bcij9")))

