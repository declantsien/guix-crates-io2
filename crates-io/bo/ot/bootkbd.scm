(define-module (crates-io bo ot bootkbd) #:use-module (crates-io))

(define-public crate-bootkbd-0.1.0 (c (n "bootkbd") (v "0.1.0") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "usb-host") (r "~0.1") (d #t) (k 0)))) (h "0na5gk7zk101293gxk59smg4vdyw7i4dgrwg9y7qbynrr0zy499c")))

(define-public crate-bootkbd-0.2.0 (c (n "bootkbd") (v "0.2.0") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "usb-host") (r "~0.1") (d #t) (k 0)))) (h "10d7yhnw5xxkh2wh7xkvpgjv3s95kh9ph70y8wjl84gq8dlzhr3v")))

(define-public crate-bootkbd-0.2.1 (c (n "bootkbd") (v "0.2.1") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "usb-host") (r "~0.1") (d #t) (k 0)))) (h "0s5ikkalzc92km6m6k0qxj1gk08wljvc8j1afmpgahgvxl3zy531")))

(define-public crate-bootkbd-0.2.2 (c (n "bootkbd") (v "0.2.2") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "usb-host") (r "~0.1") (d #t) (k 0)))) (h "1zsp2a2r51i7j5qsyksd0704cqj53ypz6lkjykjv51z0la4vnhqj")))

