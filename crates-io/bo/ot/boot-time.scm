(define-module (crates-io bo ot boot-time) #:use-module (crates-io))

(define-public crate-boot-time-0.1.0 (c (n "boot-time") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0sw7yk4786l2304p3143p023fwls9p007sd7kxr2gxqmn8vaq94g") (y #t)))

(define-public crate-boot-time-0.1.1 (c (n "boot-time") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1kcy1jvk9gr001a9zgh7hhi5sx8bpgbmccqg45lc7a61mijl0xrm")))

(define-public crate-boot-time-0.1.2 (c (n "boot-time") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1xjpf4b35sljgbl92yna92lsb0qvmyw6p04kjfph5lwg7d60jayf")))

