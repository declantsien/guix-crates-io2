(define-module (crates-io bo ot bootspec) #:use-module (crates-io))

(define-public crate-bootspec-0.1.0 (c (n "bootspec") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0i0xipl505pdmhx3m6xmsby2ki8xyi0nj1jcs6zqyalwh63i5ylj")))

(define-public crate-bootspec-0.2.0 (c (n "bootspec") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "132lq57097q9jiqrmkir6svmkswyaw2axcm0ajpd0jw36662vrq0")))

(define-public crate-bootspec-1.0.0 (c (n "bootspec") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1bjgx76l3h4s1ngdpilfrnykhhkjdx2f8yj2hrphpyxpzkjrndma")))

