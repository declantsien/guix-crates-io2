(define-module (crates-io bo ot bootuefi) #:use-module (crates-io))

(define-public crate-bootuefi-0.1.0 (c (n "bootuefi") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.8") (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "locate-cargo-manifest") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "0sqbabbzccjrvmszhf2x58pwc5s3l3p4jqnqhgkgskf1hi1bk7ai")))

(define-public crate-bootuefi-0.1.1 (c (n "bootuefi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (k 0)) (d (n "locate-cargo-manifest") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "0s8lplcl2gd30a7cvg63dgk9x7nn32shr3l9fgjrbw511w4cd1li")))

