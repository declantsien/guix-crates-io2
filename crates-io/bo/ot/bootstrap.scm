(define-module (crates-io bo ot bootstrap) #:use-module (crates-io))

(define-public crate-bootstrap-1.0.0 (c (n "bootstrap") (v "1.0.0") (h "0kjrrdwc6g94n4k6w8mhi1z18xrw6b17zkb5d091qx967rcxsdjf")))

(define-public crate-bootstrap-1.0.1 (c (n "bootstrap") (v "1.0.1") (h "0xfmvskj8b4m5bcp9ms6bz1m0nscna6gm9ykb32bzl3b6pzhal01")))

(define-public crate-bootstrap-1.0.2 (c (n "bootstrap") (v "1.0.2") (h "120m5lrrxqxh1m88mbz71gqcga0is1w6a07b7acbddcp40zyy8c5")))

