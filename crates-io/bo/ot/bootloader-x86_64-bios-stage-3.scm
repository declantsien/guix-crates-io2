(define-module (crates-io bo ot bootloader-x86_64-bios-stage-3) #:use-module (crates-io))

(define-public crate-bootloader-x86_64-bios-stage-3-0.1.0-beta (c (n "bootloader-x86_64-bios-stage-3") (v "0.1.0-beta") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.1.0-beta") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "0hixqw41vb9wp9ap600x33ji7bw8q0snbm7yqlcs05ldm1irfpv7")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.0-beta.2 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.0-beta.2") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0-beta.2") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "1baav14d47yzhb1sq0zi9z1j33swrjib1fmd6hm99wdh7s88qbpw")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.0-beta.4 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.0-beta.4") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0-beta.4") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "1cq0brykx0bzmd5fhw4q8ylvjjwp69x7y9phvgs5qlaiksbmd6ps")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.0-beta.5 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.0-beta.5") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0-beta.5") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "04hgg3hw768d4m3lndarrgbj6lhrcivbldkjzxha3llrnb6fwi0s")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.0-beta.8 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.0-beta.8") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0-beta.8") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "1c361waravjc299i9c6z1lqkywcnqkr2q045k6cgis05jqvcnbzb")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.0 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.0") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "103wijx6p9c5k95hiqv5f2sb8217a092bdrs747h0z3cw44xnvx0")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.1 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.1") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.1") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "12d1zbfmm20lkgay9qya8advs9a34701zqs2q423abgq4svmpr30")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.2 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.2") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.2") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "1a88bp5lk9vwvh5pdghvw9zkvsswydi1d4k4nlsvjw0iyphcz3zb")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.3 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.3") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.3") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "1mbfrwbcchq7mi97ypdzi1wy7crnzwgl2zai8knxlk1ak8bvgb6r")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.4 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.4") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.4") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "0fsjz3wxz4pmqa7sg61x2wgajvncz7q15lj0aziwlk7nbbra25mf")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.5 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.5") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.5") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "14vkl14gxrlcm2xjainlvs1xqnkznyq3bw4q0545l19pdmrvrzls")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.6 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.6") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.6") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "0b8l5rvkijqjwd78jim1w2359nh4yhpvr3ilxy3pqhmgq37lm66z")))

(define-public crate-bootloader-x86_64-bios-stage-3-0.11.7 (c (n "bootloader-x86_64-bios-stage-3") (v "0.11.7") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.7") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)))) (h "0d7h6yk8cd6y4kc7fg5lfy4z5xvyhsk3fqvpsp6xwy5qsy7m8s4z")))

