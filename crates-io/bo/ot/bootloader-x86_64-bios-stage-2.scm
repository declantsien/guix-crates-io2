(define-module (crates-io bo ot bootloader-x86_64-bios-stage-2) #:use-module (crates-io))

(define-public crate-bootloader-x86_64-bios-stage-2-0.1.0-beta (c (n "bootloader-x86_64-bios-stage-2") (v "0.1.0-beta") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.1.0-beta") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "0g9vb7969k4kvvxhzly05jn4a2a2l18bnxyrjc9qzvf1zy1lsdgs")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.0-beta.2 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.0-beta.2") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0-beta.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "0sdpl7kjg1zi1n6rr3l55px6504232fass3k5pb6rvac0p9jjxcp")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.0-beta.4 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.0-beta.4") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0-beta.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "16cmxnr6i2xwqbz1zpqgqk18ydi9xh9bxc8d5spi7rhidryi51yk")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.0-beta.5 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.0-beta.5") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0-beta.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "10fl9x31iqbg9hmzwr8826prmg3pqz830m0gyizwgnkick5lsakh")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.0-beta.8 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.0-beta.8") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0-beta.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "1iwsdka8h1kdxw94zva1p5v8fdl77nsd5nxrc0h4pw10sgvwf2kk")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.0 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.0") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "1149xbxkj5s8k9g8byzwmlxx5x5sps0v2pyjy3g56l1aqd4vbiac")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.1 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.1") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "017vlvvmhrrhbygr3s7kigpgy4czqi0sj3h1pg7bf1i0fd6ml1l6")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.2 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.2") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "00cky3pmy8c41wbcbdba360f8f8bx6sh6vpmwcdhpwc57d3zajpy")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.3 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.3") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "1h0xsnl3vchbshjb5id4yxfl7125486y2x9ha5nv2rg1wsan90a5")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.4 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.4") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "1k8kfwgiw3mvhxzj6r0kjn281vd7jskn1rxr0i6ryl9pdkyc95fr")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.5 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.5") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "1b7gmc87521m0f6wgd0zcp4v63fjz2836r1gi0234z2ap1bjjlmx")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.6 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.6") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "1vmaw0h6j5k0f9n8l33wkq2jczyi23saz8pq7wbxb00zz64is27d")))

(define-public crate-bootloader-x86_64-bios-stage-2-0.11.7 (c (n "bootloader-x86_64-bios-stage-2") (v "0.11.7") (d (list (d (n "bootloader-x86_64-bios-common") (r "^0.11.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "mbr-nostd") (r "^0.1.0") (d #t) (k 0)))) (h "05r08a0mcf2s1rcf46x22xsp1l7kdrkvld8pn6wqc2wzl0al08nf")))

