(define-module (crates-io bo ot bootinfo) #:use-module (crates-io))

(define-public crate-bootinfo-0.1.0 (c (n "bootinfo") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.6") (d #t) (k 0)) (d (n "derive-error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)))) (h "13cmya8xwbhmimayqy7nqkv6i5wbsx289b8pndvc7gn14aqwpwsn")))

(define-public crate-bootinfo-0.2.0 (c (n "bootinfo") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "derive-error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1yhldiraajm2l8shl31pfcsp1dz4vkj0p921djlzb66xri0k9j2f")))

