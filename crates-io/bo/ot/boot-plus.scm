(define-module (crates-io bo ot boot-plus) #:use-module (crates-io))

(define-public crate-boot-plus-0.1.1 (c (n "boot-plus") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "boot-core") (r "^0.1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.15.0") (d #t) (k 0)) (d (n "cw1-whitelist") (r "^0.15.1") (d #t) (k 0)) (d (n "cw20") (r "^0.15.0") (d #t) (k 0)) (d (n "cw20-base") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.1") (d #t) (k 2)))) (h "0jj1lp926n96gny3cfqza189wpqj7n4n9msg69wcy8l3xjd5czxi")))

