(define-module (crates-io bo rk borked) #:use-module (crates-io))

(define-public crate-borked-0.1.0 (c (n "borked") (v "0.1.0") (h "00f0j0xgcndmhjfkpgalv8s2c7m9adva76102jk48fxs6kk7lf9b")))

(define-public crate-borked-0.1.1 (c (n "borked") (v "0.1.1") (h "1y90css9wqqsdard9ra8lnwsqvhi7p81y9s19lk0lgfrky07snzj") (y #t)))

(define-public crate-borked-0.1.2 (c (n "borked") (v "0.1.2") (h "1al9c7dn7hh8f6qqgdl60hfwaix74ingx96k09jldq6ikb2p4qjq")))

