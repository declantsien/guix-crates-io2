(define-module (crates-io bo lu boluo-macros) #:use-module (crates-io))

(define-public crate-boluo-macros-0.1.0 (c (n "boluo-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11nzlysidfy2rpq26np3qzrf9v1dhh8k94khb5dk01djdisp8kcd") (r "1.75")))

(define-public crate-boluo-macros-0.1.1 (c (n "boluo-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zb4b9fsm669c64kxp263hmlnahkrshgpw72rm616rjlzpgn5yfk") (r "1.75")))

(define-public crate-boluo-macros-0.1.2 (c (n "boluo-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jnfl0hj03850r178sg5y8nxbzk8m3ax1l8cyjy8mbp9bjyxnn1z") (r "1.75")))

