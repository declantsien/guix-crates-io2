(define-module (crates-io bo bs bobsled) #:use-module (crates-io))

(define-public crate-bobsled-0.0.0 (c (n "bobsled") (v "0.0.0") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "promptly") (r "^0.3.0") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "sled") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tonic") (r "^0.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1.0") (d #t) (k 1)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "v5"))) (d #t) (k 0)))) (h "06lhj20wckssd349gsk5igh43y3vk9drp8d9w4whvwjw8hm1nbyc")))

