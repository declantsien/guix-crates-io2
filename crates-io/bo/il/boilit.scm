(define-module (crates-io bo il boilit) #:use-module (crates-io))

(define-public crate-boilit-0.1.0 (c (n "boilit") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "0lnvr9rn3l70rwyzc87pdrml3r2v8g3k57279h8is4is6mys1hiz")))

(define-public crate-boilit-0.1.3 (c (n "boilit") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "0shi946g68xagwjdsv8y4nj4plprwvbwbp7l1r2ydl688wbj5km2")))

