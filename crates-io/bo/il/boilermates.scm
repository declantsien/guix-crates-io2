(define-module (crates-io bo il boilermates) #:use-module (crates-io))

(define-public crate-boilermates-0.1.0 (c (n "boilermates") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1av336rm4a4afnz46wygb4xm9rxflki7yzhi62y3df2g0bjdyix0") (y #t)))

(define-public crate-boilermates-0.1.1 (c (n "boilermates") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "107ng94r8989hyf92k1218jlx0k8070zsjn55w6m035mgy3yys2b")))

(define-public crate-boilermates-0.2.0 (c (n "boilermates") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k8rwy8rzw5i1c62rfw7mc6a6i8zn991hjrxwraiqv32r66ai572")))

(define-public crate-boilermates-0.3.0 (c (n "boilermates") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nfchr6bla2ijjxvv6sqcj6v7bwqlanw0pgmqa6vk538ldzk6ndi")))

