(define-module (crates-io bo il boiler) #:use-module (crates-io))

(define-public crate-boiler-0.1.0 (c (n "boiler") (v "0.1.0") (d (list (d (n "boiler-generated") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1f548d6mcy4yhwxig8zw0v9j1l2dqgrrs6psysi2al2xm71dqj9z")))

(define-public crate-boiler-0.1.1 (c (n "boiler") (v "0.1.1") (d (list (d (n "boiler-generated") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1jr505nv00mw2vp78i41ril0s6l7xjka06afp5b9v1mc46q40q6q")))

