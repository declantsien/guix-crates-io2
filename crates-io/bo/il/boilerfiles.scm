(define-module (crates-io bo il boilerfiles) #:use-module (crates-io))

(define-public crate-boilerfiles-0.1.0 (c (n "boilerfiles") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wi7pywgzfd27qx505yqh2jikdbhf6mb2xjvl9pa104hpfhs01qf")))

(define-public crate-boilerfiles-0.2.0 (c (n "boilerfiles") (v "0.2.0") (d (list (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ghwc6p8mxi57sfr8lxvhvh85y356l5dic3s1bfxhwphrjz1yhxl")))

(define-public crate-boilerfiles-0.3.0 (c (n "boilerfiles") (v "0.3.0") (d (list (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "0fyji3inxi5s9dq97y8bjsks8ihkac0if276jnqs35i45f3zxjxr")))

(define-public crate-boilerfiles-0.3.1 (c (n "boilerfiles") (v "0.3.1") (d (list (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "0hkwszlzc9cqjrl61cld6lgqlrw5hj395bdbbih8bi2cws8zygq1")))

