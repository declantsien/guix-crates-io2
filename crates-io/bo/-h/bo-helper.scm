(define-module (crates-io bo -h bo-helper) #:use-module (crates-io))

(define-public crate-bo-helper-0.1.0 (c (n "bo-helper") (v "0.1.0") (h "04lrd1z5y3wbnwhvdbrf9dmlk047lg57l6kmhbfyz7j4mfmbmbiw")))

(define-public crate-bo-helper-0.1.3 (c (n "bo-helper") (v "0.1.3") (h "1dl62d8qqy1kls777pwg5x70nbhcqzya7sii9s2kd0mvnpqpzbs6")))

(define-public crate-bo-helper-0.1.24 (c (n "bo-helper") (v "0.1.24") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 0)))) (h "1py5p3fb8ihdhvr2y1m3f24bn1wj4g0w5cg9145qk8lfb9l3zcy2")))

