(define-module (crates-io bo oh boohashing) #:use-module (crates-io))

(define-public crate-boohashing-0.1.0 (c (n "boohashing") (v "0.1.0") (d (list (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "01spap2fmhi9f4lr9rg9k92m10qahwyjcrmiqkwmlvijink9kcsl")))

