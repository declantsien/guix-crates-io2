(define-module (crates-io bo rd border-tensorboard) #:use-module (crates-io))

(define-public crate-border-tensorboard-0.0.6 (c (n "border-tensorboard") (v "0.0.6") (d (list (d (n "border-core") (r "^0.0.6") (d #t) (k 0)) (d (n "tensorboard-rs") (r "^0.2.4") (d #t) (k 0)))) (h "0hcn1cqm4f878jf19dffhxqb2db9n48x0f5gkhw7f8zf487ljrlg") (r "1.68.2")))

