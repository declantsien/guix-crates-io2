(define-module (crates-io bo rd borderrs) #:use-module (crates-io))

(define-public crate-borderrs-0.1.0 (c (n "borderrs") (v "0.1.0") (h "1ibnfh1y640l4x5hp9sk0b1g02nwfwx90hm0ga6bbwnj06jyryfw")))

(define-public crate-borderrs-0.1.1 (c (n "borderrs") (v "0.1.1") (h "08j16a38b080cih7nj9pv3hmv5q5mf8b7dhw411sjh6gf1bgqjk0")))

