(define-module (crates-io bo rc borc) #:use-module (crates-io))

(define-public crate-borc-0.1.0 (c (n "borc") (v "0.1.0") (d (list (d (n "half") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "040havbybrpm5n0r3gxv9b6bjqqr9asjn9kkr3wi3vgqd5zcfqsd")))

(define-public crate-borc-0.2.0 (c (n "borc") (v "0.2.0") (d (list (d (n "half") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0gr59pfsgzig6pq4jbn22xxkxzkr9rakwr7prxmi7g300s7ffrvd")))

(define-public crate-borc-0.3.0 (c (n "borc") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.22") (o #t) (d #t) (k 0)) (d (n "half") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1fa4v9q9pd3pb16033a1dlvg10axip6d171c6xbmm5hq2k7rgr4g")))

(define-public crate-borc-0.4.0 (c (n "borc") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.22") (o #t) (d #t) (k 0)) (d (n "half") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1sa6k5a43pnfr5zj82ka7rr622lzicgdljv6vk9wa2z22w4bna72")))

(define-public crate-borc-0.5.0 (c (n "borc") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.22") (o #t) (d #t) (k 0)) (d (n "half") (r "^2.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1r93v7q3pb85g55hnl80awaksyfl77f7zwxil4km5vb5vw9rlvc5")))

(define-public crate-borc-0.6.0 (c (n "borc") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.22") (o #t) (d #t) (k 0)) (d (n "half") (r "^2.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0p28x4fm60ns0glwc6v9phy6zqkkaljkrp8h7c595bkan2vsz2f2")))

