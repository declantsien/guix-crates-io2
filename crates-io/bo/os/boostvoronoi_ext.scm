(define-module (crates-io bo os boostvoronoi_ext) #:use-module (crates-io))

(define-public crate-boostvoronoi_ext-0.10.2 (c (n "boostvoronoi_ext") (v "0.10.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "19b8h65mv8gh5c7gjrgjshkpbnahyci7rdrv509gf73p1b0ffd68")))

(define-public crate-boostvoronoi_ext-0.10.3 (c (n "boostvoronoi_ext") (v "0.10.3") (d (list (d (n "approx") (r "~0.5") (d #t) (k 0)) (d (n "libm") (r "~0.2") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "ordered-float") (r "~2.8") (d #t) (k 0)) (d (n "smallvec") (r "~1.7") (d #t) (k 0)))) (h "094z71ds11z6snkjs0y6lma4sgnxj71s7spjkdcxxaiv0qhglf5g")))

(define-public crate-boostvoronoi_ext-0.11.0 (c (n "boostvoronoi_ext") (v "0.11.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.1") (d #t) (k 0)))) (h "0148j5vnrxzy61qwya8ds3a42nyxyhzc0zipwplabg8j1xs35jj5")))

(define-public crate-boostvoronoi_ext-0.11.1 (c (n "boostvoronoi_ext") (v "0.11.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)))) (h "18ysh17wwyhgli1rl23vl0k646lsirjjvklfjgqs580f1zwdnq9k")))

