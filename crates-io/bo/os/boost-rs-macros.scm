(define-module (crates-io bo os boost-rs-macros) #:use-module (crates-io))

(define-public crate-boost-rs-macros-0.0.1 (c (n "boost-rs-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1mka4bahd4slsbs88j73vhkymx4287d6r97wi4viwahhzsr1wjfl")))

(define-public crate-boost-rs-macros-0.0.2 (c (n "boost-rs-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "15jcjwvmqq37zzr7bzlk9z4gaw818hpzfwp65qbn9d0c5cf1bm2p")))

