(define-module (crates-io bo eh boehm-rs) #:use-module (crates-io))

(define-public crate-boehm-rs-0.1.0 (c (n "boehm-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04n8l0kl592i0ljrmcw9l93qjfd7hhmhhlq4q9icdkqm9g0jqjf6")))

(define-public crate-boehm-rs-0.1.2 (c (n "boehm-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wrkhls7bvprs1ln4z74vlgnh4c5bp637hixxbwv3szacr1v5ac6")))

