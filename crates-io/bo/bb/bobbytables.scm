(define-module (crates-io bo bb bobbytables) #:use-module (crates-io))

(define-public crate-bobbytables-0.1.0 (c (n "bobbytables") (v "0.1.0") (h "0jzw6dg3mzl0ydhid9wr4pasghj4b0rjwli4wyjgd0qsbsqkshss")))

(define-public crate-bobbytables-0.2.0 (c (n "bobbytables") (v "0.2.0") (h "1jc785xgk7k3rdwd1gfgrxb141ksmxf6hzvqhv5rgf4hz3xghzrx")))

