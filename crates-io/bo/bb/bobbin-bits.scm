(define-module (crates-io bo bb bobbin-bits) #:use-module (crates-io))

(define-public crate-bobbin-bits-0.1.0 (c (n "bobbin-bits") (v "0.1.0") (h "0cy3mlmx549a33ysbbmsqhc08yypsksd31c1hq4rydspv1blm0gi")))

(define-public crate-bobbin-bits-0.1.1 (c (n "bobbin-bits") (v "0.1.1") (h "0wp5qn3wxy0rf9sy89ijk395z7i6dzkjsfg3xamlw540im85da44")))

