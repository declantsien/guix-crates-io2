(define-module (crates-io bo o- boo-hoo) #:use-module (crates-io))

(define-public crate-boo-hoo-0.1.0 (c (n "boo-hoo") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)))) (h "16z2ygbjwb0y6mn5a25n1mbayrah0f76ngdiamqfpb1rp7hhc3bq")))

(define-public crate-boo-hoo-0.2.0 (c (n "boo-hoo") (v "0.2.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)))) (h "119yibbcrjzfxyaxahzxsz66yrmfd9057bsginfvh5alkp86f1iz")))

