(define-module (crates-io bo xi boxing-arena) #:use-module (crates-io))

(define-public crate-boxing-arena-0.9.0 (c (n "boxing-arena") (v "0.9.0") (h "033bgn8fv93sjrmr0q969zq7h7bhpa6qw9f9c8ln27cdpl17cvz2") (y #t)))

(define-public crate-boxing-arena-0.9.1 (c (n "boxing-arena") (v "0.9.1") (h "17rpml18ygzyddw75c9zgidmd3dgvi5qrpv057zcsif4ksn7ar4l") (y #t)))

(define-public crate-boxing-arena-0.9.2 (c (n "boxing-arena") (v "0.9.2") (h "17vn61jy5yfyfg2npw55s39w3qj3h4qpvf2jv5yr5n1j766iqhb1")))

(define-public crate-boxing-arena-0.9.3 (c (n "boxing-arena") (v "0.9.3") (h "091aqxzhnbqxmxhp0hm7rgifvhwnmb45xmzjkj765xyf3p15ixn5")))

