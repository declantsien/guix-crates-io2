(define-module (crates-io bo xi boxify-macro) #:use-module (crates-io))

(define-public crate-boxify-macro-0.1.0 (c (n "boxify-macro") (v "0.1.0") (d (list (d (n "litrs") (r "^0.4.1") (f (quote ("proc-macro2"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "143nf51ishgr1b8wh53309ya1cmm5851wikbzcz0ky48xrzbbyid")))

