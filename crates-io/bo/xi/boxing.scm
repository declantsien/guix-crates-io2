(define-module (crates-io bo xi boxing) #:use-module (crates-io))

(define-public crate-boxing-0.1.0 (c (n "boxing") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pprof") (r "^0.11") (f (quote ("criterion" "flamegraph"))) (d #t) (t "cfg(unix)") (k 2)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "0yf4d6p9f0kkmqvb6cbiwa7izysca9zqwnfx1zdjmgab37im2m93")))

(define-public crate-boxing-0.1.1 (c (n "boxing") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pprof") (r "^0.11") (f (quote ("criterion" "flamegraph"))) (d #t) (t "cfg(unix)") (k 2)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "07yfmsglmr0j9z8wfgfi2lkp50m3p568paxkzm7h9w6060dj7rcq")))

(define-public crate-boxing-0.1.2 (c (n "boxing") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pprof") (r "^0.11") (f (quote ("criterion" "flamegraph"))) (d #t) (t "cfg(unix)") (k 2)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "126sv9ycfmhlqh2hn816pw48ykyq603a1gk52pz38nw0xw97z0bs")))

