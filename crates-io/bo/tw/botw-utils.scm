(define-module (crates-io bo tw botw-utils) #:use-module (crates-io))

(define-public crate-botw-utils-0.1.0 (c (n "botw-utils") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (d #t) (k 0)))) (h "1483a21sg1lc20n9csdn9qmbadji8lxqlpm63bps7d0fdykckpjl")))

(define-public crate-botw-utils-0.1.1 (c (n "botw-utils") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (d #t) (k 0)))) (h "0k44bpv1fqwmqzaf8isgbp60w3lsikv4sgldbfp8i5270rr5bkq9")))

(define-public crate-botw-utils-0.2.0 (c (n "botw-utils") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (d #t) (k 0)))) (h "18gxkiyva76lkk612dhjp2yas2j5vqiflx7jg9ahlhkfb251aadl")))

(define-public crate-botw-utils-0.3.0 (c (n "botw-utils") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (d #t) (k 0)))) (h "01l3239yzk9zbifwg3xr3isknzjnqr3qg9jqf38a4sm98vzhrc5i")))

(define-public crate-botw-utils-0.3.1 (c (n "botw-utils") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (d #t) (k 0)))) (h "1735iflvr84kpiis5sricadl58k273mqhki6hv99ckibqfdmwglv")))

(define-public crate-botw-utils-0.4.0 (c (n "botw-utils") (v "0.4.0") (d (list (d (n "include-flate") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "roead") (r "^0.20.0") (f (quote ("yaz0" "binrw" "smartstring"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)))) (h "0drbzlnlvf6hkz08jd2s8h86hd2p4cplxvjazk8z14c8s58p9cqg")))

(define-public crate-botw-utils-0.4.1 (c (n "botw-utils") (v "0.4.1") (d (list (d (n "include-flate") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "roead") (r "^0.20.2") (f (quote ("yaz0" "binrw"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)))) (h "0kr6bmpylkhbn0q3pvcrvpglv6srqh09py4h3h9pfgnal06s0a6m")))

(define-public crate-botw-utils-0.5.0 (c (n "botw-utils") (v "0.5.0") (d (list (d (n "include-flate") (r "^0.2.0") (f (quote ("stable"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "roead") (r "^0.23.0") (f (quote ("yaz0" "binrw"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)))) (h "0d7na1jjllm5kf90xy84ayiqkakmsni14gzncaxqq6rall2vpz4j")))

