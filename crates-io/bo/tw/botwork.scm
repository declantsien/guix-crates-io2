(define-module (crates-io bo tw botwork) #:use-module (crates-io))

(define-public crate-botwork-0.1.0 (c (n "botwork") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "192jl8bfzqpa90g7pnggn6cw9hpw6cjp500yywpl9bsqaf23j1nx")))

(define-public crate-botwork-0.2.0 (c (n "botwork") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0dl4v1q1bl3rsd7lgwjfrscz11y7snyn14zqsyy6a9nvl2v2y4zb")))

