(define-module (crates-io bo tw botway-rs) #:use-module (crates-io))

(define-public crate-botway-rs-0.1.0 (c (n "botway-rs") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1w7sdyqv1iy783r2ryralz0fjw5g1cqqzh8c1mnsn1ya2qvslsh5")))

(define-public crate-botway-rs-0.2.0 (c (n "botway-rs") (v "0.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1gjrp546vbgcpx4wxp3l1lpl385gm5lg6j2414kc9zzj8504np3v")))

(define-public crate-botway-rs-0.2.1 (c (n "botway-rs") (v "0.2.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "12jh9ilzglgl279fvi0v19zwwqli7f8j0ia8aghzq9y7qdh0lmbh")))

(define-public crate-botway-rs-0.2.11 (c (n "botway-rs") (v "0.2.11") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0qi48xxkaqalwydnk99fx6fia6p1i1aqnx71gw56c6xii2879xlr")))

(define-public crate-botway-rs-0.2.2 (c (n "botway-rs") (v "0.2.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1a3qpcqssknf3l8j5vqls9znrxkz8pmcn3wi10m3rfc4z6nk2qhb")))

