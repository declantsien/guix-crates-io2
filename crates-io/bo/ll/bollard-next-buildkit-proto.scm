(define-module (crates-io bo ll bollard-next-buildkit-proto) #:use-module (crates-io))

(define-public crate-bollard-next-buildkit-proto-0.4.0 (c (n "bollard-next-buildkit-proto") (v "0.4.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)) (d (n "tonic-build") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (o #t) (d #t) (k 0)))) (h "1522cmnw35w6jiykc7zg51zmv9bzpznzhg1mrns9iwzb6rz5j8bc") (f (quote (("fetch" "ureq") ("default" "fetch") ("build" "tonic-build"))))))

