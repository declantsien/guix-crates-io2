(define-module (crates-io bo ll bollard-buildkit-proto) #:use-module (crates-io))

(define-public crate-bollard-buildkit-proto-0.1.0 (c (n "bollard-buildkit-proto") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.5.2") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0c093zsyss6pqsca2wv2kr2kfalxjdz28rfkx261j909l4kfy1i2") (f (quote (("build" "tonic-build" "indexmap"))))))

(define-public crate-bollard-buildkit-proto-0.2.0 (c (n "bollard-buildkit-proto") (v "0.2.0") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1li1bcy2j2j69krv0imr0w90ncg5wrrfwf7f1zvciklblw9zi0c6") (f (quote (("build" "tonic-build" "indexmap"))))))

(define-public crate-bollard-buildkit-proto-0.2.1 (c (n "bollard-buildkit-proto") (v "0.2.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1gs0xagdk6kx9mfyn6p1lfv6vqbw530cg1329i2dfzd93l39qdrf") (f (quote (("build" "tonic-build"))))))

(define-public crate-bollard-buildkit-proto-0.3.0 (c (n "bollard-buildkit-proto") (v "0.3.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)) (d (n "tonic-build") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0gxp8w03qy9gr5rjxk094kvnmnkmrfb9p5j4r6qqx016yqvhlpd0") (f (quote (("build" "tonic-build"))))))

