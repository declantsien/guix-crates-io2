(define-module (crates-io bo mb bomboni_request_derive) #:use-module (crates-io))

(define-public crate-bomboni_request_derive-0.1.12 (c (n "bomboni_request_derive") (v "0.1.12") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "03cx20hw4hdmaxgs2sf2z547wslxj2k5796vrbbc1lg6yx3i5ji1")))

(define-public crate-bomboni_request_derive-0.1.13 (c (n "bomboni_request_derive") (v "0.1.13") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0l0dmcvvhyszlsqjp4q3i3qsalmkdakfq5d75d0bfa061xxkllp7")))

(define-public crate-bomboni_request_derive-0.1.14 (c (n "bomboni_request_derive") (v "0.1.14") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1sbwz6k0qpdvn8f9amyjyl6jghlg9pl6404zl30ing1imc5qmbg9")))

(define-public crate-bomboni_request_derive-0.1.15 (c (n "bomboni_request_derive") (v "0.1.15") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "120fdwqaihfbrisfa4mjbj30b4g722yvmaksd84b6bm5f65dczgk")))

(define-public crate-bomboni_request_derive-0.1.16 (c (n "bomboni_request_derive") (v "0.1.16") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "12v6hmn2r41px1m9j42gkpzldm651z8696qhxc097dx4n4pds28m")))

(define-public crate-bomboni_request_derive-0.1.17 (c (n "bomboni_request_derive") (v "0.1.17") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0cm9kaky6bwflz41rbzgr3vg6l3p0q11mc9lvpddn8j0g21qgnxr")))

(define-public crate-bomboni_request_derive-0.1.18 (c (n "bomboni_request_derive") (v "0.1.18") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0vxzq6wqh55wf7j387jd2rg6rlq961sikjr8xp31jsffhdlgibrq")))

(define-public crate-bomboni_request_derive-0.1.19 (c (n "bomboni_request_derive") (v "0.1.19") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0pqcg01szq7xxxp13wq53ca71qp8hhflc28x04n75yibwlypmg4m")))

(define-public crate-bomboni_request_derive-0.1.20 (c (n "bomboni_request_derive") (v "0.1.20") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0z59qxbb4gnf4virqm0ihxi50127r76xn9ijbhlvxmr1i1kvn7x4")))

(define-public crate-bomboni_request_derive-0.1.21 (c (n "bomboni_request_derive") (v "0.1.21") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1b5ys1ykg55g9mqi6hk6rb44lxwg6mmsj1ld19m749rmdzhw72af")))

(define-public crate-bomboni_request_derive-0.1.22 (c (n "bomboni_request_derive") (v "0.1.22") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0fmhpji6wjnz2m72ajria2s3c6s246dd561hpjcz8kh407h9zr0i")))

(define-public crate-bomboni_request_derive-0.1.23 (c (n "bomboni_request_derive") (v "0.1.23") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1jj792gr9dqv27nadgvc9f6jjmq5wfhm6wy8x15rkvl5gdr0bk2y")))

(define-public crate-bomboni_request_derive-0.1.24 (c (n "bomboni_request_derive") (v "0.1.24") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1h9s21gd8rs9g020lar5ylqwbl9mbszp30clkd0dsfbnwd13b45w")))

(define-public crate-bomboni_request_derive-0.1.25 (c (n "bomboni_request_derive") (v "0.1.25") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1kh6g6ifbxv5gwk5pbzhkgm65znyia5spxlbily793i8ax9k17br")))

(define-public crate-bomboni_request_derive-0.1.26 (c (n "bomboni_request_derive") (v "0.1.26") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "06sn1rh5j2j0j8n5fbmanwknsx73xwa2wqbylhlf1swlgkhjf4ny")))

(define-public crate-bomboni_request_derive-0.1.27 (c (n "bomboni_request_derive") (v "0.1.27") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0ss3rz39ri77jlmq5dbxhsjpbcp638j2g2jyh6w6q323cbl6xjzw")))

(define-public crate-bomboni_request_derive-0.1.28 (c (n "bomboni_request_derive") (v "0.1.28") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0mjk68iczn76xqxsmb0j3kn08b6ykmy6h5hcq51xvi15mkjpjrpd")))

(define-public crate-bomboni_request_derive-0.1.29 (c (n "bomboni_request_derive") (v "0.1.29") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1cvcqjwjz0dj3mkgvc9ijagfzgwsr8z0i5mz9ii62618xig4vjhz")))

(define-public crate-bomboni_request_derive-0.1.30 (c (n "bomboni_request_derive") (v "0.1.30") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "11fsmg5djx5c8cg3i9zcw403ri3xys9mfvy2xc0hgsigw279xxrg")))

(define-public crate-bomboni_request_derive-0.1.31 (c (n "bomboni_request_derive") (v "0.1.31") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0gypkyf9vzji4rw58fzxwdql0lhwarvlhrmbnmg9rd787xg14yr0")))

(define-public crate-bomboni_request_derive-0.1.32 (c (n "bomboni_request_derive") (v "0.1.32") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0gaiqm1izb2d8hnrn7iijkaaf4lg0c2mczsmhb8xhhizc258dgfg")))

(define-public crate-bomboni_request_derive-0.1.33 (c (n "bomboni_request_derive") (v "0.1.33") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1ilkn6sf3fpbwxm8g0h3q1kg5xjlb5fzxsd3mz02kglhwy71nhg3")))

(define-public crate-bomboni_request_derive-0.1.34 (c (n "bomboni_request_derive") (v "0.1.34") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1hlfcm02g1wpg01l08ck3y44v5dhilfr10shj27g1ns8vx6yv8db")))

(define-public crate-bomboni_request_derive-0.1.35 (c (n "bomboni_request_derive") (v "0.1.35") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1dphqf5h03bp30db8yg6hrwyxm92bfm291i98n6cijb9hjird2hb")))

(define-public crate-bomboni_request_derive-0.1.36 (c (n "bomboni_request_derive") (v "0.1.36") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "15bz2fckmg1r5sv3msq266vwk8qlzryb9zvx0y5lc6pwb4w0kmic")))

(define-public crate-bomboni_request_derive-0.1.37 (c (n "bomboni_request_derive") (v "0.1.37") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "179ixn973fpx6b26z44lcr0flfm59ih824nq2h67hfyfziw4v47c")))

(define-public crate-bomboni_request_derive-0.1.38 (c (n "bomboni_request_derive") (v "0.1.38") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "07jzjwngg971x8rf5w71zkw3x1gm9risfanjkz38xna3m88qkdik")))

(define-public crate-bomboni_request_derive-0.1.39 (c (n "bomboni_request_derive") (v "0.1.39") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1fadw2vhfss8qid6sy7dp42949m021616z016g1igpv33vgksn6n")))

(define-public crate-bomboni_request_derive-0.1.40 (c (n "bomboni_request_derive") (v "0.1.40") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1d93cslx507psjvmvyiwpnddnwysdcpjcgl9zfnv6qwp3k15g1fc")))

(define-public crate-bomboni_request_derive-0.1.41 (c (n "bomboni_request_derive") (v "0.1.41") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "1y61njzl4xzdh8v6760cyf02zhr9vzks04wf6vjipdx02pmq387w")))

(define-public crate-bomboni_request_derive-0.1.42 (c (n "bomboni_request_derive") (v "0.1.42") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "11ashhraf95ad08xkv19hykgb8s7bk3hc9v44q9lizjfafisx7kc")))

(define-public crate-bomboni_request_derive-0.1.43 (c (n "bomboni_request_derive") (v "0.1.43") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "0jmpfrbq2vhjpgi3vzbyii994f8i1d94wlx900c5cl35g7c2rk1i")))

(define-public crate-bomboni_request_derive-0.1.44 (c (n "bomboni_request_derive") (v "0.1.44") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "0c8gmnq9jipsha4f5m50swdladppgs2429548hhb0qs32qd1jn51")))

(define-public crate-bomboni_request_derive-0.1.45 (c (n "bomboni_request_derive") (v "0.1.45") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "10l1rw3ypz7x1gl6zdd0jq15lkfiy5idkmqcd558nvbjdq08f393")))

(define-public crate-bomboni_request_derive-0.1.46 (c (n "bomboni_request_derive") (v "0.1.46") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "049x70nxcg5k2k79ii9lnzi3jcsp5hpns8bghhaj72zafmbwsank")))

(define-public crate-bomboni_request_derive-0.1.47 (c (n "bomboni_request_derive") (v "0.1.47") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "1pc1ss9310mdp00zd7jcwwd4l5x983awgsis6df1jh90ha8nyrl9")))

(define-public crate-bomboni_request_derive-0.1.48 (c (n "bomboni_request_derive") (v "0.1.48") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "1bk61qwzyhbdqcgdfgzzmrlq92yfpl632i9inc9ifcjd953g0633")))

(define-public crate-bomboni_request_derive-0.1.49 (c (n "bomboni_request_derive") (v "0.1.49") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "1snlckc7qs99999kka4yljw7cn2y67crvw8fra8cdl0sgfp8v567")))

(define-public crate-bomboni_request_derive-0.1.50 (c (n "bomboni_request_derive") (v "0.1.50") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "0sacgj5dj0n4c544irannjbny8ayldyq5nrylnb0vhvfbnx5fhnm")))

(define-public crate-bomboni_request_derive-0.1.51 (c (n "bomboni_request_derive") (v "0.1.51") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "0fzm4vzqahx4lrvyilb6swgp2hk3vb8307050j21pwaxfnhcl3x0")))

(define-public crate-bomboni_request_derive-0.1.52 (c (n "bomboni_request_derive") (v "0.1.52") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "0qsdbd80dcj36msh2sicqgl8fipnw858yiimj6qzawyf8m3yfmz9")))

(define-public crate-bomboni_request_derive-0.1.53 (c (n "bomboni_request_derive") (v "0.1.53") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "0i6sjzi2jhilrs1k1x794rx341r9vdh8b1wwasrl3jn7vcsmzxwh")))

(define-public crate-bomboni_request_derive-0.1.54 (c (n "bomboni_request_derive") (v "0.1.54") (d (list (d (n "bomboni_core") (r "^0.1.54") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0733wczklfs530cb9v4j42xvqx93lrnw89d9b7py769pmdkpsnr7")))

(define-public crate-bomboni_request_derive-0.1.55 (c (n "bomboni_request_derive") (v "0.1.55") (d (list (d (n "bomboni_core") (r "^0.1.55") (d #t) (k 0)) (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0d4sr81nvq5y2qs06x5x0z5qdlkbvbjy9wc66l202ijxfp8qj1df")))

(define-public crate-bomboni_request_derive-0.1.56 (c (n "bomboni_request_derive") (v "0.1.56") (d (list (d (n "bomboni_core") (r "^0.1.56") (d #t) (k 0)) (d (n "darling") (r "^0.20.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)))) (h "1cp641xs8xr76a2cmj529aaghg0df79rnn33ymxl761kxv4vm44x")))

