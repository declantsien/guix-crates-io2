(define-module (crates-io bo mb bomb) #:use-module (crates-io))

(define-public crate-bomb-0.1.0 (c (n "bomb") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "fake") (r "^2.9.2") (f (quote ("derive" "chrono"))) (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)))) (h "0xkp71dnb86g5mxd19aqhlwb599jnaxy7hpi96xic3yjkp48j5hn")))

