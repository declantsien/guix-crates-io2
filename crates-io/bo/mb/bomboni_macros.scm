(define-module (crates-io bo mb bomboni_macros) #:use-module (crates-io))

(define-public crate-bomboni_macros-0.1.51 (c (n "bomboni_macros") (v "0.1.51") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "0yibqx87vp2gyk31gxcfglfy4s8hrncnavhzz1mnasgi7a2c0pbj")))

(define-public crate-bomboni_macros-0.1.52 (c (n "bomboni_macros") (v "0.1.52") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (d #t) (k 0)))) (h "035agjglgzsb9mzysj9zb4rbzqy2my575ilk593fkgkpzcpqy189")))

