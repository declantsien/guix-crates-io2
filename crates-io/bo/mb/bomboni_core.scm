(define-module (crates-io bo mb bomboni_core) #:use-module (crates-io))

(define-public crate-bomboni_core-0.1.54 (c (n "bomboni_core") (v "0.1.54") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0i084jrnxs9d0s9dlsrzy1ix8q7bkcaqd1dz7w23mg8bbgfk6729")))

(define-public crate-bomboni_core-0.1.55 (c (n "bomboni_core") (v "0.1.55") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1yj2fsjx1051ivc31h1qrwjhzfr6s4zpbsdh8wrmqzynxssaz6lw")))

(define-public crate-bomboni_core-0.1.56 (c (n "bomboni_core") (v "0.1.56") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full"))) (d #t) (k 0)))) (h "0cpk8j0js99dj0a9agngf36l8614lsr47kicc386r6gzv724hi0x")))

