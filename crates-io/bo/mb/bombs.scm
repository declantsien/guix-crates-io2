(define-module (crates-io bo mb bombs) #:use-module (crates-io))

(define-public crate-bombs-0.1.0 (c (n "bombs") (v "0.1.0") (h "13jqv4dsrvzzyyn72sxcjm8jxblirlpnivjhgrvyhl631p4awmk3") (y #t)))

(define-public crate-bombs-0.1.1 (c (n "bombs") (v "0.1.1") (h "1m6ls9v0xznaspdwz8p3kknd2isaybvdh2n0mza9cvxs9x02bh7v") (y #t)))

(define-public crate-bombs-0.1.2 (c (n "bombs") (v "0.1.2") (h "0amw02ic1i8nrxgxsqqwm5558fh5i97z3r6lvv2hk2fxzsyrkycx") (y #t)))

(define-public crate-bombs-0.1.3 (c (n "bombs") (v "0.1.3") (h "0dfd4a6sdrk3zi0a56yb38wrr5d1zqjz5wavfvan7qmgjp69rhh8") (y #t)))

(define-public crate-bombs-0.1.4 (c (n "bombs") (v "0.1.4") (h "1ksp5wsnk7ygmfv5s2bfw44qacmy80lmq11jngsr4pq4n14wl6k7") (y #t)))

(define-public crate-bombs-0.1.5 (c (n "bombs") (v "0.1.5") (h "1ays8ny5fwj0yhgslnalj5nrnfrkq5fwngkkblgnzjwj5l7kxd2w")))

(define-public crate-bombs-0.2.0 (c (n "bombs") (v "0.2.0") (d (list (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 0)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "0y5zch94sbdbnd130siirjkx2imx6pqh08fnbh8zcnsnw6cc1kww") (y #t)))

(define-public crate-bombs-0.2.1 (c (n "bombs") (v "0.2.1") (d (list (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 0)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "12abs5s7flh2siki6p7rp2xvdf3xkfk87mvyi74ybbjg7fihbvnq")))

