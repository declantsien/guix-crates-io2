(define-module (crates-io bo mb bombhopper) #:use-module (crates-io))

(define-public crate-bombhopper-0.1.0 (c (n "bombhopper") (v "0.1.0") (d (list (d (n "either") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "1rgypv9pvd86ck7xk77xprd13bnx1nhiag5d20n23irgxhgmpn7d")))

(define-public crate-bombhopper-0.1.1 (c (n "bombhopper") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "098am1nx9bz4816swbx1b55g6bfljmyyfav8hkgr5ks5jcr9blsd")))

(define-public crate-bombhopper-0.1.2 (c (n "bombhopper") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0zryyhsbj8wyl692wpdrj9gf8klq9nh0fy4pvc9xmpnyxh97nnvr")))

