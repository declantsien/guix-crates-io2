(define-module (crates-io bo mb bomboni_derive) #:use-module (crates-io))

(define-public crate-bomboni_derive-0.1.3 (c (n "bomboni_derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1szx6ssw6c6sj1jhwhgrcg1ic0z9hddsgi74cifkg8rrdwdh85bk")))

(define-public crate-bomboni_derive-0.1.4 (c (n "bomboni_derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0dp6r1ja9dgvd6kisv5xdz7b994bf11l4n4qlimbyp659qc0hn1m")))

(define-public crate-bomboni_derive-0.1.5 (c (n "bomboni_derive") (v "0.1.5") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "104zc766sh1p6mk4apzq557hs2nn0wy5jnz6lpnjwh2nhm9dx09l")))

(define-public crate-bomboni_derive-0.1.6 (c (n "bomboni_derive") (v "0.1.6") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0jld9xywqi4rd7hfngzkrlzfvf0wlprp48s5rxq1d0w4gmb17fhl")))

(define-public crate-bomboni_derive-0.1.7 (c (n "bomboni_derive") (v "0.1.7") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1a5fwm1x1lsph8nl1y3l9z23ri40vh97nwz5kyjskaimq0cv7gi9")))

(define-public crate-bomboni_derive-0.1.8 (c (n "bomboni_derive") (v "0.1.8") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "00cm3x6bmkpy2pz9p85fb09bi8ig5mcah57p6kqgd2c8xw0563n7")))

(define-public crate-bomboni_derive-0.1.9 (c (n "bomboni_derive") (v "0.1.9") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "15jxpncvinfi88rsbd97cfcpqq0la6w68zpwdsb0b58izcbb5fvk")))

(define-public crate-bomboni_derive-0.1.10 (c (n "bomboni_derive") (v "0.1.10") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1z7xgw81xkglrh20hb7sz1yqw44m9j9h0zqhqzvcmrdff7hm54db")))

(define-public crate-bomboni_derive-0.1.11 (c (n "bomboni_derive") (v "0.1.11") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1vwlb9z72laz1r67qmqndmqzjww5ijp19dn6apckf5xjgz5lzpha")))

