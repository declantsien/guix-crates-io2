(define-module (crates-io bo mb bomboni_fs) #:use-module (crates-io))

(define-public crate-bomboni_fs-0.1.55 (c (n "bomboni_fs") (v "0.1.55") (h "0g6gfwfdkd9rx0iw86p94xmyycyh69dph6k1j99l4mn4glqmx0n1")))

(define-public crate-bomboni_fs-0.1.56 (c (n "bomboni_fs") (v "0.1.56") (h "126hhnng0vjf13fqiy13xs458a0amq6m58ayfmmn6rny8a6m5zsq")))

