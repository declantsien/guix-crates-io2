(define-module (crates-io bo rs borsh) #:use-module (crates-io))

(define-public crate-borsh-0.1.0 (c (n "borsh") (v "0.1.0") (d (list (d (n "borsh-derive") (r "^0.1.0") (d #t) (k 0)))) (h "10bl1npzfwapy3qs90da9xlffnq03y0r3bsmdqp30fma1jw29skd") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.0 (c (n "borsh") (v "0.2.0") (d (list (d (n "borsh-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0l0351l871qxxvl94kk0js1y7lnmqjg4fsf66mryk2i44japwj21") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.1 (c (n "borsh") (v "0.2.1") (d (list (d (n "borsh-derive") (r "^0.2.1") (d #t) (k 0)))) (h "1l1x86vxxrxzvazbikwi1rark477png4ncnkka48mhnjh24qz5ix") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.2 (c (n "borsh") (v "0.2.2") (d (list (d (n "borsh-derive") (r "^0.2.2") (d #t) (k 0)))) (h "1v1fa5bkh2fg9v0rkygwq7nakfhac7cnw5xxd5kps5kc586qcbld") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.3 (c (n "borsh") (v "0.2.3") (d (list (d (n "borsh-derive") (r "^0.2.3") (d #t) (k 0)))) (h "12bgy320w898l5d802dkxbf9xaa4v4vzv0smr567ccvg4ricirfj") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.4 (c (n "borsh") (v "0.2.4") (d (list (d (n "borsh-derive") (r "^0.2.4") (d #t) (k 0)))) (h "1c25v4pimfch9bpinsb6msw00hl9j6fa2blb70bmbkn7vcikxx2a") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.5 (c (n "borsh") (v "0.2.5") (d (list (d (n "borsh-derive") (r "^0.2.5") (d #t) (k 0)))) (h "1rw7ig06ga9wcqi9mh11x8yc1d668cl46v4xkgghiv4q4zjn27mm") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.7 (c (n "borsh") (v "0.2.7") (d (list (d (n "borsh-derive") (r "^0.2.7") (d #t) (k 0)))) (h "0m5my1l9z1ikxnx1j52yaq33ppsdvfgrndsqsxpan3r19k87ly8c") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.8 (c (n "borsh") (v "0.2.8") (d (list (d (n "borsh-derive") (r "^0.2.8") (d #t) (k 0)))) (h "0492lv2rmjsmpwq28xck3anw4fzsxs2p1j3gn9sc2skw969j62nj") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.9 (c (n "borsh") (v "0.2.9") (d (list (d (n "borsh-derive") (r "^0.2.9") (d #t) (k 0)))) (h "0vnxi0w83q9k2f9kgyd274jz2cl7w586k9gaalvbs51dn9hrkhfw") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.2.10 (c (n "borsh") (v "0.2.10") (d (list (d (n "borsh-derive") (r "^0.2.10") (d #t) (k 0)))) (h "1yxypxnx8fvjszvxd03k195rlfgf8ic4dlvim49vz185xphvqmkc") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.3.0 (c (n "borsh") (v "0.3.0") (d (list (d (n "borsh-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1q26qiyvl8ymfsfvk0cn1qrpmpigymfvydxlgrv7a7zsfxxi292l") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.4.0 (c (n "borsh") (v "0.4.0") (d (list (d (n "borsh-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1rvzwpwhcgylxx7i8zs5ka2axdnrqnwqsqimw7xhcrn9s8j7czvi") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.5.0 (c (n "borsh") (v "0.5.0") (d (list (d (n "borsh-derive") (r "^0.5.0") (d #t) (k 0)))) (h "1clf2w2iy9flfyl7zk610jhyxnqpls0k1z34q4m2m35s7nxax6g9") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.6.0 (c (n "borsh") (v "0.6.0") (d (list (d (n "borsh-derive") (r "^0.6.0") (d #t) (k 0)))) (h "0c2x8f6yw6vyahp3lv5ymcwdf7gcvc8znrpq69l2z01m3a49zf9z") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.6.1 (c (n "borsh") (v "0.6.1") (d (list (d (n "borsh-derive") (r "^0.6.1") (d #t) (k 0)))) (h "1cr176fi8yqzfj1fzyzp0ya73cg7h62h80sm36y2d9vzq2jav79g") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.6.2 (c (n "borsh") (v "0.6.2") (d (list (d (n "borsh-derive") (r "^0.6.2") (d #t) (k 0)))) (h "03fpsz43pz2amgr1rllrkm2wsqy2kppwg2xwgdhwfsnwdy7ryxn7") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.7.0 (c (n "borsh") (v "0.7.0") (d (list (d (n "borsh-derive") (r "^0.7.0") (d #t) (k 0)))) (h "09acgip5is40csh8g16m7l5sh1c30rpng3fbhzchglpqmn921m41") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.7.1 (c (n "borsh") (v "0.7.1") (d (list (d (n "borsh-derive") (r "^0.7.1") (d #t) (k 0)))) (h "1121zcsza1w0qj554smrq2h7x5qvp8mrpg4iwz3afn93j9jzvwrh") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.8.0 (c (n "borsh") (v "0.8.0") (d (list (d (n "borsh-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)))) (h "0n8dx815zggvz7gjldfx8qc9lmfzzapsyaa2dl720lgbzlzy1lnq") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.8.1 (c (n "borsh") (v "0.8.1") (d (list (d (n "borsh-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)))) (h "0r6ka1pn8bd0z85nbfc1klmbhgdsnw6v4absivqq20pnvm9nr8m5") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.7.2 (c (n "borsh") (v "0.7.2") (d (list (d (n "borsh-derive") (r "^0.7.2") (d #t) (k 0)))) (h "1rl2wzjdac0d572a0d7y4ap309gjmwk4bvp5093y6aznkgx16axl") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.8.2 (c (n "borsh") (v "0.8.2") (d (list (d (n "borsh-derive") (r "=0.8.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)))) (h "0x4a2sx7wf97vi6il2vmgzjswdhn0zxj78w5g1023ivwg4gi39q9") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.9.0 (c (n "borsh") (v "0.9.0") (d (list (d (n "borsh-derive") (r "=0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)))) (h "0dldqsd500ywcsi7alzmd4yvdk4i8j1agjbdhs8dznkgh41bpjjg") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-0.9.1 (c (n "borsh") (v "0.9.1") (d (list (d (n "borsh-derive") (r "=0.9.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)))) (h "090rx8hs9nr57ai4dv32y8fdzhvdj850al8sdbcc14wif3fagp8q") (f (quote (("std") ("rc") ("default" "std") ("const-generics"))))))

(define-public crate-borsh-0.9.2 (c (n "borsh") (v "0.9.2") (d (list (d (n "borsh-derive") (r "=0.9.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)))) (h "1f4cx55v726lr1z80cgpbks2wqpzrdwshbk94qb1wc5qxxc0k7ac") (f (quote (("std") ("rc") ("default" "std") ("const-generics"))))))

(define-public crate-borsh-0.9.3 (c (n "borsh") (v "0.9.3") (d (list (d (n "borsh-derive") (r "=0.9.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)))) (h "1ylvjdlyfyfscyq5phmvgbh7vlgvy485wn8mj2lzz2qd4183dgqm") (f (quote (("std") ("rc") ("default" "std") ("const-generics"))))))

(define-public crate-borsh-0.10.0 (c (n "borsh") (v "0.10.0") (d (list (d (n "borsh-derive") (r "=0.10.0") (d #t) (k 0)) (d (n "hashbrown") (r ">=0.11, <0.14") (d #t) (k 0)))) (h "1hsifx3krymhnz9vf34yx4c6isns6r5qg9lq2yb32yxiwz0n4942") (f (quote (("std") ("rc") ("default" "std") ("const-generics"))))))

(define-public crate-borsh-0.10.1 (c (n "borsh") (v "0.10.1") (d (list (d (n "borsh-derive") (r "=0.10.1") (d #t) (k 0)) (d (n "hashbrown") (r ">=0.11, <0.14") (d #t) (k 0)))) (h "1y4jsmqrf98xfyb5ff3izy6756rsabp71hdy3p2pljz36z8hbvx3") (f (quote (("std") ("rc") ("default" "std") ("const-generics"))))))

(define-public crate-borsh-0.10.2 (c (n "borsh") (v "0.10.2") (d (list (d (n "borsh-derive") (r "=0.10.2") (d #t) (k 0)) (d (n "hashbrown") (r ">=0.11, <0.14") (d #t) (k 0)))) (h "0qncnadk0l0b8r083bi1g90wl7amqnmv07ap2mycpr5jk0vcmya0") (f (quote (("std") ("rc") ("default" "std") ("const-generics"))))))

(define-public crate-borsh-0.10.3 (c (n "borsh") (v "0.10.3") (d (list (d (n "borsh-derive") (r "=0.10.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "hashbrown") (r ">=0.11, <0.14") (d #t) (k 0)))) (h "0sq4l9jfik5dmpy1islcj40bing1jkji2q1qbrkvq1d02n92f521") (f (quote (("std") ("rc") ("default" "std") ("const-generics"))))))

(define-public crate-borsh-0.11.0 (c (n "borsh") (v "0.11.0") (d (list (d (n "borsh-derive") (r "^0.11.0") (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "hashbrown") (r ">=0.11, <0.14") (d #t) (k 0)))) (h "0i2zbqhj47yr2znl4ml888l9p31hbmml61sbbaaqlz3dnjas431h") (f (quote (("std") ("rc") ("default" "std") ("const-generics")))) (y #t)))

(define-public crate-borsh-1.0.0-alpha.1 (c (n "borsh") (v "1.0.0-alpha.1") (d (list (d (n "borsh-derive") (r "^1.0.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.14") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "0gwzlhh42a850lyk1v2sfn0dylrsky4wjx6mlziw3n931f58wn1z") (f (quote (("std") ("schema" "borsh-derive/schema") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.0.0-alpha.2 (c (n "borsh") (v "1.0.0-alpha.2") (d (list (d (n "borsh-derive") (r "^1.0.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.14") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "0ijg21sj69pmw9qlz9g5zzmvy2gyv2x4ll4hi8hij4rjbnkpi9zz") (f (quote (("std") ("schema" "borsh-derive/schema") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.0.0-alpha.3 (c (n "borsh") (v "1.0.0-alpha.3") (d (list (d (n "borsh-derive") (r "^1.0.0-alpha.3") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.14") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1xx4ii5xwy4n9kr352zi31nd5fx9iy1slzcma10kpnyhkxacn6ja") (f (quote (("std") ("schema" "borsh-derive/schema") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.0.0-alpha.4 (c (n "borsh") (v "1.0.0-alpha.4") (d (list (d (n "borsh-derive") (r "^1.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1d8ka1fisb5h5vfmp99rmjkfp5vkjw97jsckfl13y3bv21vq52a1") (f (quote (("std") ("schema" "derive" "borsh-derive/schema") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.0.0-alpha.5 (c (n "borsh") (v "1.0.0-alpha.5") (d (list (d (n "borsh-derive") (r "^1.0.0-alpha.5") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1s9rwadi4rky9njw6l87cr1gbfc7ni0qnpm5pm77yg4afi9rx0c6") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.0.0-alpha.6 (c (n "borsh") (v "1.0.0-alpha.6") (d (list (d (n "borsh-derive") (r "^1.0.0-alpha.6") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "03m7nl91z43fjmxapghc5xw8245cd6dfx2blxyk3csrldr42niql") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.0.0 (c (n "borsh") (v "1.0.0") (d (list (d (n "borsh-derive") (r "~1.0.0") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "01yj1mkza6kbib8m53vhx2qj67byljq2in7n4bl16qlrg4svcv2f") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.1.0 (c (n "borsh") (v "1.1.0") (d (list (d (n "borsh-derive") (r "~1.1.0") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1f3i9gh1fswijnm85rx49hvr1qp4hh0k7f1jq3w328hsv0fdgvdc") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.1.1 (c (n "borsh") (v "1.1.1") (d (list (d (n "borsh-derive") (r "~1.1.1") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1hidzw9v0757rp9fxgqb0sqqif4qknw5xcmv7164b3gdxrcp6gsy") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.1.2 (c (n "borsh") (v "1.1.2") (d (list (d (n "borsh-derive") (r "~1.1.2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1snb5py6lw3dgzcav8wd38wrgngd2bnjcachy16p6cx4fsn499qq") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.2.0 (c (n "borsh") (v "1.2.0") (d (list (d (n "ascii") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "borsh-derive") (r "~1.2.0") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "0vh5sjz4igvsg6013m2n02w2iwkhv1i51zjbfwpwkgfdynmpyqdz") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.2.1 (c (n "borsh") (v "1.2.1") (d (list (d (n "ascii") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "borsh-derive") (r "~1.2.1") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1l89yf34vqsbbyvls7kxbl4c2z93l9p46zkdvrlj2dnj3c7yz5wq") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.3.0 (c (n "borsh") (v "1.3.0") (d (list (d (n "ascii") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "borsh-derive") (r "~1.3.0") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "0a1hic4kj1rcy86qkmappi4ckch7iwap52akhx3bafqszkdddm16") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.66.0")))

(define-public crate-borsh-1.3.1 (c (n "borsh") (v "1.3.1") (d (list (d (n "ascii") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "borsh-derive") (r "~1.3.1") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "0rznh8z6yfccz5zp795g9ichc9cav4675ddds0pnx324ssgmb2zm") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.67.0")))

(define-public crate-borsh-1.4.0 (c (n "borsh") (v "1.4.0") (d (list (d (n "ascii") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "borsh-derive") (r "~1.4.0") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1mmdb08kigv48klg099czj6s11kxn4nnyv8hw0xwi95cn27gq089") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.67.0")))

(define-public crate-borsh-1.5.0 (c (n "borsh") (v "1.5.0") (d (list (d (n "ascii") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "borsh-derive") (r "~1.5.0") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 1)) (d (n "hashbrown") (r ">=0.11, <0.15.0") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "0xqdla3s5kn7plrj10vckvlqyq52l4hbv7ywn5zifm29447b3rfv") (f (quote (("unstable__schema" "derive" "borsh-derive/schema") ("std") ("rc") ("derive" "borsh-derive") ("default" "std") ("de_strict_order")))) (r "1.67.0")))

