(define-module (crates-io bo rs borsh-schema-derive-internal) #:use-module (crates-io))

(define-public crate-borsh-schema-derive-internal-0.3.0 (c (n "borsh-schema-derive-internal") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0q05rqxw9s8zf6cp6pjjzqzccpvkmc1ldw8l7bqjp59sn5a68iic")))

(define-public crate-borsh-schema-derive-internal-0.4.0 (c (n "borsh-schema-derive-internal") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ym5pj312i6gs0bkq1bss2hdxr1q5i2g4qjzk8fk4fgqk7m5gcf4")))

(define-public crate-borsh-schema-derive-internal-0.5.0 (c (n "borsh-schema-derive-internal") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0cc94pjiahx74323wmpw2smdpw4djwx48jyh3i56ichcxxwpw5xy")))

(define-public crate-borsh-schema-derive-internal-0.6.0 (c (n "borsh-schema-derive-internal") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "05x2vy2rfmzjm4rm3qf6kzvr72mh0yzrnggbw4prvdll56rdsg7z")))

(define-public crate-borsh-schema-derive-internal-0.6.1 (c (n "borsh-schema-derive-internal") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0cxa9347ivg8rp4id27yasyfz7925bp7z2jsqydak7knscq35fdk")))

(define-public crate-borsh-schema-derive-internal-0.6.2 (c (n "borsh-schema-derive-internal") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0adsfc3kcdgjc3if6g9a5n16d1f6nnzxp5ymncl4bbi3wawvvzmy")))

(define-public crate-borsh-schema-derive-internal-0.7.0 (c (n "borsh-schema-derive-internal") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1bn5nmnh230wskm5zgcb13q509hpn3zc7rx421jxqsp7bwdjzrhx")))

(define-public crate-borsh-schema-derive-internal-0.7.1 (c (n "borsh-schema-derive-internal") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1f394v80pwzg31is3cgpggclifrcvvmj9l3h7r4v9hpbaqxm9wjx")))

(define-public crate-borsh-schema-derive-internal-0.8.0 (c (n "borsh-schema-derive-internal") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1m17mv750050b36frfw8l21dx9gdik129hicy15j0jqc6c3j3s9x")))

(define-public crate-borsh-schema-derive-internal-0.8.1 (c (n "borsh-schema-derive-internal") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1dykfb2azjxmcwg6kkhrqxrpcggal7id7hxsl7mvk34cm7j8xxyw")))

(define-public crate-borsh-schema-derive-internal-0.7.2 (c (n "borsh-schema-derive-internal") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1xrvdygwcs7nnan3xhk1g1h1azj166ypwb4wjjq3f22pvazqmcw5")))

(define-public crate-borsh-schema-derive-internal-0.8.2 (c (n "borsh-schema-derive-internal") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0a67jzg6ffdi3vcz5likklbk2zc50sna54c67xr6zm7w322fnadf")))

(define-public crate-borsh-schema-derive-internal-0.9.0 (c (n "borsh-schema-derive-internal") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1mqgzwr91p0pfqyvfw9zk4krlinw2r1zwq2igb61kcv4a541l58m")))

(define-public crate-borsh-schema-derive-internal-0.9.1 (c (n "borsh-schema-derive-internal") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "10q4fp5pqcwszw9s2x3h94rqlnmzj0329wvf8hni82wv9j69fv0r")))

(define-public crate-borsh-schema-derive-internal-0.9.2 (c (n "borsh-schema-derive-internal") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1djqmm8hzyk2qi9y5j4hg2dcg1q778chs80dhq584ylhf5vsgclr")))

(define-public crate-borsh-schema-derive-internal-0.9.3 (c (n "borsh-schema-derive-internal") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1h2i37xrbhxvdl32v94j2k8vlf45l4aaffgyv59iv8mzv2b5dgfd")))

(define-public crate-borsh-schema-derive-internal-0.10.0 (c (n "borsh-schema-derive-internal") (v "0.10.0") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "13ib2963ib322a2xh8a2lx967w3icwy7pbpki8ghvwny2gxdnv67")))

(define-public crate-borsh-schema-derive-internal-0.10.1 (c (n "borsh-schema-derive-internal") (v "0.10.1") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0s9zidmdczwfsa7ggw4b32v39c2lakadwhy864rzviciyy2x0wgn")))

(define-public crate-borsh-schema-derive-internal-0.10.2 (c (n "borsh-schema-derive-internal") (v "0.10.2") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0jfccr9s22gkjimn25200ga574hjvq59c6wrhps2crii108gzdwr")))

(define-public crate-borsh-schema-derive-internal-0.10.3 (c (n "borsh-schema-derive-internal") (v "0.10.3") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1kaw1xdprb8chqj50c8gxjb5dadx1rac91zg8s81njpp8g60ahk3")))

(define-public crate-borsh-schema-derive-internal-0.11.0 (c (n "borsh-schema-derive-internal") (v "0.11.0") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1z0vh0gh7c2h0ajgzridd54zcw710fqjwzpa58n8dzvmsrkw96m8") (y #t)))

