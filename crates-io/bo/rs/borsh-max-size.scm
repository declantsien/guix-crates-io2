(define-module (crates-io bo rs borsh-max-size) #:use-module (crates-io))

(define-public crate-borsh-max-size-0.1.0 (c (n "borsh-max-size") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "borsh-max-size-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1") (o #t) (k 0)))) (h "15ck5kal7anzp5n1ssy8xv9ac7l7dsrqkfg8w0wh2k66s5piks2b")))

(define-public crate-borsh-max-size-0.2.0 (c (n "borsh-max-size") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "borsh-max-size-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1") (o #t) (k 0)))) (h "1zxclqy130xzrgmfpwsjxfp0pv7ciq5rlmh6czyfz03swd5d2zvz")))

