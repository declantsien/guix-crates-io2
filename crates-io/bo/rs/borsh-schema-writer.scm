(define-module (crates-io bo rs borsh-schema-writer) #:use-module (crates-io))

(define-public crate-borsh-schema-writer-0.1.0 (c (n "borsh-schema-writer") (v "0.1.0") (d (list (d (n "borsh") (r ">=0.9.3, <=0.10.3") (d #t) (k 0)) (d (n "borsh-derive") (r ">=0.9.3, <=0.10.3") (d #t) (k 0)))) (h "0f9gvx4j0p64wjpyjspsrnrwvrbn6q008q52cycxxqihbins8yxw")))

(define-public crate-borsh-schema-writer-1.0.0 (c (n "borsh-schema-writer") (v "1.0.0") (d (list (d (n "borsh") (r "^1.1.1") (f (quote ("std" "unstable__schema"))) (d #t) (k 0)) (d (n "borsh-derive") (r "^1.1.1") (d #t) (k 0)))) (h "167rdywyzx3pr7g5safp0y1dmh6zjzpslw52352541g49md78q9j")))

(define-public crate-borsh-schema-writer-1.0.1 (c (n "borsh-schema-writer") (v "1.0.1") (d (list (d (n "borsh") (r "^1.4.0") (f (quote ("std" "unstable__schema"))) (d #t) (k 0)) (d (n "borsh-derive") (r "^1.4.0") (d #t) (k 0)))) (h "1x7k4glrhihsqdhazl9f48bqa4km0rfhaxmhxqc0s987yp5b5pfn")))

