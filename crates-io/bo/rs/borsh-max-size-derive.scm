(define-module (crates-io bo rs borsh-max-size-derive) #:use-module (crates-io))

(define-public crate-borsh-max-size-derive-0.1.0 (c (n "borsh-max-size-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "07m3r17m5y27gqbb108z73di0b33gf1r9x9ffdlfagb9z7ymb8aa")))

