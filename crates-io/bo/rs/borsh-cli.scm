(define-module (crates-io bo rs borsh-cli) #:use-module (crates-io))

(define-public crate-borsh-cli-0.1.0 (c (n "borsh-cli") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0x4n50v359qc4mjx4z7l9mh79nsrk7czsx91nkbp3nbxhchzdbg8")))

(define-public crate-borsh-cli-0.1.1 (c (n "borsh-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1dalq4wan87aqpmblmgy0j1bdsdl8nyyc2gs7c21h4iic00mkliq")))

(define-public crate-borsh-cli-0.1.2 (c (n "borsh-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0iw1ki38s523h3vahk106zgh1bddjp481kakqp4ksjr954h59aiy")))

(define-public crate-borsh-cli-0.1.3 (c (n "borsh-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1qf939c5i3401qs7jjg8kfs4ancjl512ah1jka08p71dsrvas09r")))

