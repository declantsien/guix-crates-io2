(define-module (crates-io bo rs borsh-derive-internal) #:use-module (crates-io))

(define-public crate-borsh-derive-internal-0.1.0 (c (n "borsh-derive-internal") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0vh1n8k3gp0q5mkli317ylpdfzy3r8iy1hfqwvwncdys0kfdg673")))

(define-public crate-borsh-derive-internal-0.2.0 (c (n "borsh-derive-internal") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.4.5") (f (quote ("cargo-fmt"))) (d #t) (k 2)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1f7ghi14kgfpwai6apdrrk7bnl4gcxhs2w14yya0kaxlynqks5mc")))

(define-public crate-borsh-derive-internal-0.2.1 (c (n "borsh-derive-internal") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.4.5") (f (quote ("cargo-fmt"))) (d #t) (k 2)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1llc8hqlz7c5pmf3sml38bv7y7xmic2zwpjmnp05xfk1805ii8d6")))

(define-public crate-borsh-derive-internal-0.2.2 (c (n "borsh-derive-internal") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.4.5") (f (quote ("cargo-fmt"))) (d #t) (k 2)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0s15xynnivm5vpggzysff9pj1p3zhkai9aq6hax9lx55wvggk1kf")))

(define-public crate-borsh-derive-internal-0.2.3 (c (n "borsh-derive-internal") (v "0.2.3") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.4.5") (f (quote ("cargo-fmt"))) (d #t) (k 2)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "14ldll2nymk79achf7dk3l90xqq1x7nlc8wcrknsknaxkh18vc6a")))

(define-public crate-borsh-derive-internal-0.2.4 (c (n "borsh-derive-internal") (v "0.2.4") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.4.5") (f (quote ("cargo-fmt"))) (d #t) (k 2)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1di11q54i5frh0bkby63jv7vlbjav7cspaa3afq5djcr837xb87a")))

(define-public crate-borsh-derive-internal-0.2.5 (c (n "borsh-derive-internal") (v "0.2.5") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.4.5") (f (quote ("cargo-fmt"))) (d #t) (k 2)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "04r9r2hcjcf2cvclkzlrmkbxwpf2l9dsbqn2f6idrh9zp2797q76")))

(define-public crate-borsh-derive-internal-0.2.7 (c (n "borsh-derive-internal") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1vcd7w5m851p60yrjkwkvr1azqmw76h7b5014j5zibyk3y4k5cal")))

(define-public crate-borsh-derive-internal-0.2.8 (c (n "borsh-derive-internal") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "04dgvj8spmha175g49w91lb093hqd31lixdh60g153cmqriz0jkr")))

(define-public crate-borsh-derive-internal-0.2.9 (c (n "borsh-derive-internal") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1iwkgnfvlgpzxlnfwdk0r689lqqh5w1h7gx7f1s01g6qysr63r60")))

(define-public crate-borsh-derive-internal-0.2.10 (c (n "borsh-derive-internal") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1m4y6dy0gmlnz7a2fw5q001s9a0zdkdzr3z9v0scdj008nqhkjk3")))

(define-public crate-borsh-derive-internal-0.3.0 (c (n "borsh-derive-internal") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0bg4glf0hnl92h4wd3d40hzwa759r8vrw5qlzqcf2n15q4iwala0")))

(define-public crate-borsh-derive-internal-0.4.0 (c (n "borsh-derive-internal") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1y0yg5292d9phpljrdr18gavhgv1i4ld9az1kkmialx95rwq3hnz")))

(define-public crate-borsh-derive-internal-0.5.0 (c (n "borsh-derive-internal") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "06zckcp5dm9a23943mxyqfjx959byz56w61hcwa8772bdh6a0crz")))

(define-public crate-borsh-derive-internal-0.6.0 (c (n "borsh-derive-internal") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0lx89x23hlqm7znyh3zn1bkfkw10ffkfr0f7bazbmfhwr1j8vbwx")))

(define-public crate-borsh-derive-internal-0.6.1 (c (n "borsh-derive-internal") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "106bqq6p6q8d8yzm6gyxxj9xv1sb09yjfsv7mqj6bk9l0jbq0kyk")))

(define-public crate-borsh-derive-internal-0.6.2 (c (n "borsh-derive-internal") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "155jz4f0d0mjkx781qnzxp02f3i670igld4hcx7s74cqkvqj3dir")))

(define-public crate-borsh-derive-internal-0.7.0 (c (n "borsh-derive-internal") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0qk22w7pdy240wvk9h2jihmva3047c2hp4bfan30pzhklqdyfnp7")))

(define-public crate-borsh-derive-internal-0.7.1 (c (n "borsh-derive-internal") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "07p4hfj9882wgmrs912zzs4cipniq4dlivs66pc8wb54nxn9d7sc")))

(define-public crate-borsh-derive-internal-0.8.0 (c (n "borsh-derive-internal") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1179l15w6fl43fjy94z7wi790skw9xy6d434819gbxzynii898a0")))

(define-public crate-borsh-derive-internal-0.8.1 (c (n "borsh-derive-internal") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "09fpnzmxhnq9m5djagc3vvb61irqbx0z9kwjck1piv8bxcjzl4yq")))

(define-public crate-borsh-derive-internal-0.7.2 (c (n "borsh-derive-internal") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "18mxarswazqdpihvl11g1pk9mm8jz57bhgaqw9acqrfa7jfinqk1")))

(define-public crate-borsh-derive-internal-0.8.2 (c (n "borsh-derive-internal") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1g2703rhb9yb3ds7jgn5bfhcffzjyac6j0cfr4f46nck2xrlq46j")))

(define-public crate-borsh-derive-internal-0.9.0 (c (n "borsh-derive-internal") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "11dpl7i61407ybh7fyrr39rckvhivq5nfvcsnn7bii1s189ilchy")))

(define-public crate-borsh-derive-internal-0.9.1 (c (n "borsh-derive-internal") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1hy5kch8yicfbjq4s2b8061w2k5nhli7hc0qhymxwgkdicpzc0i1")))

(define-public crate-borsh-derive-internal-0.9.2 (c (n "borsh-derive-internal") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0p6i911wjs4jvdnp0lfgb9r5jkkfyzz6fjffqkw4l7r37gs5jhgp")))

(define-public crate-borsh-derive-internal-0.9.3 (c (n "borsh-derive-internal") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ra0qkc3a2ah08y4z8b40zximiwv2fzji2iab4g2sbrmgf5c4jal")))

(define-public crate-borsh-derive-internal-0.10.0 (c (n "borsh-derive-internal") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0yqyv2x35vmx8z18a9gpfm5dn8y94da4hrqyifczpslkar60p0k1")))

(define-public crate-borsh-derive-internal-0.10.1 (c (n "borsh-derive-internal") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1z5gwv7w50pv1s39km45kb68vf0fkpq4srj1jj34gsz4ld3zgz5h")))

(define-public crate-borsh-derive-internal-0.10.2 (c (n "borsh-derive-internal") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "12hg6plx3kyr37x4yvd9s5h67azsr4rp4pf9j0z79xn9l57p6sqq")))

(define-public crate-borsh-derive-internal-0.10.3 (c (n "borsh-derive-internal") (v "0.10.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1yx27ic6aal83bdi1h6v80wfs9ixvw51qzmdgcn8sn8rd4akid5g")))

(define-public crate-borsh-derive-internal-0.11.0 (c (n "borsh-derive-internal") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "05r4w4fq3xmr4ypz7cwns7pb0d1llja70rd1pp36jrajq9icj6a0") (y #t)))

