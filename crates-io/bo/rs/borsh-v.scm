(define-module (crates-io bo rs borsh-v) #:use-module (crates-io))

(define-public crate-borsh-v-0.7.2 (c (n "borsh-v") (v "0.7.2") (d (list (d (n "borsh-derive") (r "^0.7.1") (d #t) (k 0)))) (h "0x7nrkaysavj97c7vpx0vcl5hbbxyq2d5gk5fw4cq9zzar9c5wyf") (f (quote (("std") ("default" "std"))))))

(define-public crate-borsh-v-0.7.3 (c (n "borsh-v") (v "0.7.3") (d (list (d (n "borsh-derive") (r "^0.7.1") (d #t) (k 0)))) (h "0zwza4yx4wy7c2q804yfwfmb1lw6430vhbghd2sm1an6ks4ymqd4") (f (quote (("std") ("default" "std"))))))

