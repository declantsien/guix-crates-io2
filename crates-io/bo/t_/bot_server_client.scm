(define-module (crates-io bo t_ bot_server_client) #:use-module (crates-io))

(define-public crate-bot_server_client-0.1.0 (c (n "bot_server_client") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "0hx7d187xy9215bwahfjymkjanz0nj2rgkr1cirgkw8gzbx088p1")))

(define-public crate-bot_server_client-0.2.0 (c (n "bot_server_client") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "0i10iqa013n6ywl26dfkkp6mhhi0c595vlzv58w248r7id5yd5b3")))

(define-public crate-bot_server_client-0.3.0 (c (n "bot_server_client") (v "0.3.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "0x1dh9hqcsd1h1vd47xspc0b451swn24dx356jlc4za6by2086cp")))

(define-public crate-bot_server_client-0.4.0 (c (n "bot_server_client") (v "0.4.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "0r39i50lclym8ny1p2ys103sv3m5ysypc5bgn4rdsbabprkmz4hz")))

(define-public crate-bot_server_client-0.5.0 (c (n "bot_server_client") (v "0.5.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "0480mwazzpp34nrj587yh02rj5i6zarz6243fzx3b8ivbvn9yaqb")))

(define-public crate-bot_server_client-0.6.0 (c (n "bot_server_client") (v "0.6.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "1rnqp98xp90kjyvnpa7747ackxryjqkgq934ydf3v0rv69qyv7dj")))

(define-public crate-bot_server_client-0.7.0 (c (n "bot_server_client") (v "0.7.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "07zlzjmlw9asai72i8vkz7mm8j1hll98i2gnhivaiz69xr792p4h")))

(define-public crate-bot_server_client-0.8.0 (c (n "bot_server_client") (v "0.8.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "1074xyha5hiag4fz9jzka353wjyrcjffxmj2zwk9lia1b6z8s161")))

(define-public crate-bot_server_client-0.9.0 (c (n "bot_server_client") (v "0.9.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "1bx2kcn70p8m28mlmpkhx23kv2lcylh9yp9jqrvri5zr8kfgih90")))

(define-public crate-bot_server_client-0.10.0 (c (n "bot_server_client") (v "0.10.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "1465jasxq9z9cdka8w9lji1agzlj5kswr4n5ki3w9zwmnx049xpc")))

(define-public crate-bot_server_client-1.0.0 (c (n "bot_server_client") (v "1.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "1lvds4j141gd3nwfrw6dlj7is6xgmrjz2nb67s7lw596a4i30lw8")))

(define-public crate-bot_server_client-1.0.1 (c (n "bot_server_client") (v "1.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "1crbfay95yjxyppk37wv3habqz5xrakmw93afz8hslv4cbl5iilx")))

(define-public crate-bot_server_client-1.1.0 (c (n "bot_server_client") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "0sdb5yxaq5vmgx10ymd3cf6vg8283ndp3ihpxl91wjgbdsiy6lw2")))

(define-public crate-bot_server_client-1.1.1 (c (n "bot_server_client") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "0d5c1s4hl5dpyma5y8a566kr0livlgqg6nwypn50ph80nimhh44d")))

(define-public crate-bot_server_client-1.1.2 (c (n "bot_server_client") (v "1.1.2") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "websocket") (r "^0.26.5") (d #t) (k 0)))) (h "12zhrf94knalgji9zp6pi7ap4kf5fc5my1pq5rrsgx92dcf03j6w")))

(define-public crate-bot_server_client-1.1.3 (c (n "bot_server_client") (v "1.1.3") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "1l45bzn7bazx5jd4sjgshihs18c79iaigf8i919b2s8ismidv1kv")))

(define-public crate-bot_server_client-1.1.4 (c (n "bot_server_client") (v "1.1.4") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "websocket") (r "^0.26.5") (d #t) (k 0)))) (h "1ly5m07hcl32rpdj8s9x398ca9mdg3kl411vkivm6j8ah6cppxrf")))

(define-public crate-bot_server_client-1.1.5 (c (n "bot_server_client") (v "1.1.5") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "websocket") (r "^0.26.5") (d #t) (k 0)))) (h "1grf1883hzfnr0vc0adf1rcv7b0j9rk3pd5x1if1y118d33v4j2z")))

(define-public crate-bot_server_client-1.1.6 (c (n "bot_server_client") (v "1.1.6") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "websocket") (r "^0.26.5") (d #t) (k 0)))) (h "11k3bz5bvc2x5958fz3cfwk8637fnqmcfmfja2dnrfi6vb7ic21a")))

(define-public crate-bot_server_client-2.0.0 (c (n "bot_server_client") (v "2.0.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.17.3") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1xjkgghfzx2i3sphqbgdas8iwhvm7zwp67h1hz76sargxib7nnzj") (y #t)))

(define-public crate-bot_server_client-2.0.1 (c (n "bot_server_client") (v "2.0.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.17.3") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1srjlkn1k6avbc9a76ys82vh8i6zikmsklpwf7397fg1zivjdkxc") (y #t)))

(define-public crate-bot_server_client-2.0.2 (c (n "bot_server_client") (v "2.0.2") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.17.3") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "08dcvifcw4kkr4p0df92641ni8s85nfw1jmyznhrmjpymgwlkngx") (y #t)))

