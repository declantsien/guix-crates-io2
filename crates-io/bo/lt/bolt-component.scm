(define-module (crates-io bo lt bolt-component) #:use-module (crates-io))

(define-public crate-bolt-component-0.0.1 (c (n "bolt-component") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-system") (r "^0.0.1") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0s74jbjnby2x31vjkhixich5byyc7y55vj5pks1qi9wmnhxqzdiq") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-component-0.0.2 (c (n "bolt-component") (v "0.0.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-system") (r "=0.0.2") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1lgys7k7wabfmcxdyxv47476bcprjlyab01hfa6z3vgayl1m05hv") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-component-0.1.0 (c (n "bolt-component") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-system") (r "=0.1.0") (f (quote ("cpi"))) (d #t) (k 0)))) (h "19gksdqpj0m87q47vnxiglyzsi58zg6zal2bys7bxx82gj8ixifs") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-component-0.1.1 (c (n "bolt-component") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-system") (r "=0.1.1") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0vh555y80nw0hq72vlnhxxckwd36nfi18704wf2hyf2d94b8irlw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-component-0.1.2 (c (n "bolt-component") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-system") (r "=0.1.2") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0nvfp9d6ynbrmvkgdi8j75z5zj46jqzhgdq2ygkwrgpd148z6ylf") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-component-0.1.3 (c (n "bolt-component") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-system") (r "=0.1.3") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0nn93j3zi7jq54zxjqz8agkgbm7j144pyid47zbw5phrpdg22ay7") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-component-0.1.4 (c (n "bolt-component") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-system") (r "=0.1.4") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0mhyl033gc517i0c426r2c6chvs63vnvky8llj2s5bb6312prman") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-component-0.1.5 (c (n "bolt-component") (v "0.1.5") (d (list (d (n "anchor-lang") (r "=0.30.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-system") (r "=0.1.5") (f (quote ("cpi"))) (d #t) (k 0)))) (h "10aj7fn2rs8rm2fqbrncvmfdy02mhb19kvbrj66im5q5k5dv2a74") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

