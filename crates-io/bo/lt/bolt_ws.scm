(define-module (crates-io bo lt bolt_ws) #:use-module (crates-io))

(define-public crate-bolt_ws-0.11.11 (c (n "bolt_ws") (v "0.11.11") (h "1pkfsv9hcjcqpyzkypggj2gdi7c0c573lmfx15j2ia0swd4wa531")))

(define-public crate-bolt_ws-0.12.4 (c (n "bolt_ws") (v "0.12.4") (d (list (d (n "bolt_common") (r "^0.12.4") (d #t) (k 0)))) (h "0qgy13li3mvnszn6dpis37n2x3ziq06jakxwf18c5yzb35jz91ib")))

(define-public crate-bolt_ws-0.12.5 (c (n "bolt_ws") (v "0.12.5") (d (list (d (n "bolt_common") (r "^0.12.5") (d #t) (k 0)))) (h "1dyw2adggz3hkxv56hy9j82mqijka9bmnq1dgr7kdzfahxh8ll3z")))

