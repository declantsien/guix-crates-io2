(define-module (crates-io bo lt bolt-proto-derive) #:use-module (crates-io))

(define-public crate-bolt-proto-derive-0.1.0 (c (n "bolt-proto-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1vsmr5n5wqdhm79yapab6hvzfmq7vn7cgll9ysw2p9mg0zvqhlvi")))

(define-public crate-bolt-proto-derive-0.2.0 (c (n "bolt-proto-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1xzxdjc82hk3702rs0bim9605wzp5ki6ffiidyn4l9ix89l33hyl")))

(define-public crate-bolt-proto-derive-0.2.1 (c (n "bolt-proto-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1843p0i9b8mwrs491jxd8mc5chac0njx2vdi2cv0iwl3rgbypz2z") (y #t)))

(define-public crate-bolt-proto-derive-0.3.0 (c (n "bolt-proto-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "13lq04nf88njaza0h3yybcv7wix96xldg4z5cvmlzxgqhvv2kd4i")))

(define-public crate-bolt-proto-derive-0.4.0 (c (n "bolt-proto-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1z95nngs6ka6zirzdp0j2490im1hw9qnfgrvvzk8j2466sqx0a7c")))

(define-public crate-bolt-proto-derive-0.5.0 (c (n "bolt-proto-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (d #t) (k 0)))) (h "04c8sia5x3xpd8r0hm3x0g3z7gsgr8pjbdwc3wwl7qkzfzwkhv9h")))

(define-public crate-bolt-proto-derive-0.5.1 (c (n "bolt-proto-derive") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "11s8790rp2sps1l0rx3zc8qykq98lxn3h1x9cx004cy3bxbx7gr8")))

(define-public crate-bolt-proto-derive-0.5.2 (c (n "bolt-proto-derive") (v "0.5.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (d #t) (k 0)))) (h "0rckfc4fc83m9qixv011vp8b1z7ra04bwkvgj2driv5d326wgzvw")))

(define-public crate-bolt-proto-derive-0.6.0 (c (n "bolt-proto-derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1c6lfdlbmwa59klp1ngz7sxy38i8ca8rn6fr2jrnmnabmkzpb3vn")))

