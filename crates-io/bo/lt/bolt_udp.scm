(define-module (crates-io bo lt bolt_udp) #:use-module (crates-io))

(define-public crate-bolt_udp-0.11.11 (c (n "bolt_udp") (v "0.11.11") (h "0knhj9ikmkcxs774fppcvis51717aw4sfww1bx8lmwcpsvgljixq")))

(define-public crate-bolt_udp-0.12.4 (c (n "bolt_udp") (v "0.12.4") (d (list (d (n "bolt_common") (r "^0.12.4") (d #t) (k 0)))) (h "14vipcawf0l7b0fjc4aal19vn8zx9g3q08h3pinpvcrcjrzbc97i")))

(define-public crate-bolt_udp-0.12.5 (c (n "bolt_udp") (v "0.12.5") (d (list (d (n "bolt_common") (r "^0.12.5") (d #t) (k 0)))) (h "08157r60indp973r2mvyv880l2h1baszad44qqrmj0x2nckpgh5g")))

