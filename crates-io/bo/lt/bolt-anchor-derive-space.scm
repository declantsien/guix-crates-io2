(define-module (crates-io bo lt bolt-anchor-derive-space) #:use-module (crates-io))

(define-public crate-bolt-anchor-derive-space-0.29.1 (c (n "bolt-anchor-derive-space") (v "0.29.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ns4wzhyysk35hdf7ihcr7h72nngx7msrlfbb0ly6gqcwnilw5s3") (r "1.60")))

