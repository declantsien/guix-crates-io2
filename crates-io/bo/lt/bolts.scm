(define-module (crates-io bo lt bolts) #:use-module (crates-io))

(define-public crate-bolts-0.1.0 (c (n "bolts") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1z3s4jfdhaprwlg0dyci3k9n0ff7ki0dzfdhzi7k8i9qjjqbfz8k")))

