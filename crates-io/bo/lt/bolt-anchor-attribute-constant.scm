(define-module (crates-io bo lt bolt-anchor-attribute-constant) #:use-module (crates-io))

(define-public crate-bolt-anchor-attribute-constant-0.29.1 (c (n "bolt-anchor-attribute-constant") (v "0.29.1") (d (list (d (n "anchor-syn") (r "^0.29.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m5c86zb9rq0ngzad6684d9xvlyvn6rrblhdidjq4jnvh2ghdwf5") (f (quote (("idl-build" "anchor-syn/idl-build") ("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.60")))

