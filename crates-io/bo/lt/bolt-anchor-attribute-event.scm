(define-module (crates-io bo lt bolt-anchor-attribute-event) #:use-module (crates-io))

(define-public crate-bolt-anchor-attribute-event-0.29.1 (c (n "bolt-anchor-attribute-event") (v "0.29.1") (d (list (d (n "anchor-syn") (r "^0.29.0") (f (quote ("hash"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0748n6742cibz3hmnh0anbhhn5fc6canqa9sx96jyz2iif087p9d") (f (quote (("idl-build" "anchor-syn/idl-build") ("event-cpi" "anchor-syn/event-cpi") ("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.60")))

