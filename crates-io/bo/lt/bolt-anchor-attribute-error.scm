(define-module (crates-io bo lt bolt-anchor-attribute-error) #:use-module (crates-io))

(define-public crate-bolt-anchor-attribute-error-0.29.1 (c (n "bolt-anchor-attribute-error") (v "0.29.1") (d (list (d (n "anchor-syn") (r "^0.29.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pcqqg10ih9bcpd6l4y32kkw9xcaw00sz8i93dl73a3h6myf9mp0") (f (quote (("idl-build" "anchor-syn/idl-build") ("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.60")))

