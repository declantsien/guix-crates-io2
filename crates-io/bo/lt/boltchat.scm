(define-module (crates-io bo lt boltchat) #:use-module (crates-io))

(define-public crate-boltchat-0.1.0 (c (n "boltchat") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gz3fwc4qkq40718lgncm18yi33n6gkby64gdcm26wgbf7v702rw")))

(define-public crate-boltchat-0.2.0 (c (n "boltchat") (v "0.2.0") (d (list (d (n "pgp") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c56qykplckasncwqq5lb6xb1ci9n6ppdacqjcvld35xhz1hcxg3")))

(define-public crate-boltchat-0.2.1 (c (n "boltchat") (v "0.2.1") (d (list (d (n "pgp") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "086pypphzc1ybxarnrmlv54wrsz2dvxp151vpzh04h3vl21cwswz")))

(define-public crate-boltchat-0.2.2 (c (n "boltchat") (v "0.2.2") (d (list (d (n "pgp") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1by4qb2srgmn1gkili89dzr58ynxc7n0cid1dpl1avc6w57f03sk")))

