(define-module (crates-io bo lt bolt-utils) #:use-module (crates-io))

(define-public crate-bolt-utils-0.0.2 (c (n "bolt-utils") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "full"))) (d #t) (k 0)))) (h "0cgfx6w7s98ymmi9ksbiwd0flzqxv9c7nvav5xrv13x4dzs06wci")))

(define-public crate-bolt-utils-0.1.0 (c (n "bolt-utils") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "full"))) (d #t) (k 0)))) (h "08pgncz5fx9wdj93ai1my0kbva8dg4bslpv80khz05d9a0m48mrk")))

(define-public crate-bolt-utils-0.1.1 (c (n "bolt-utils") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "full"))) (d #t) (k 0)))) (h "1c9i8mmg8pjc0jbr6db74z2z8anj6zc7791lhkb1ff0wqwrn4a82")))

(define-public crate-bolt-utils-0.1.2 (c (n "bolt-utils") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "full"))) (d #t) (k 0)))) (h "0f3p4bnnlsfgwyqb9a5xjwp4wyi0344igjfdigmgx89j8n08sk0z")))

(define-public crate-bolt-utils-0.1.3 (c (n "bolt-utils") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "full"))) (d #t) (k 0)))) (h "1vag4gdsifwr51gqk2y4aa477cd6144giskqc3fg56ky46ya2sfd")))

(define-public crate-bolt-utils-0.1.4 (c (n "bolt-utils") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "full"))) (d #t) (k 0)))) (h "1ncv9yhjb0zpf29l3q0r9d88sy2sqhmz0ji9l2x69z8xr6296g45")))

(define-public crate-bolt-utils-0.1.5 (c (n "bolt-utils") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "full"))) (d #t) (k 0)))) (h "0vjf117m940cl48s8pjbkmghl5hcjd7rwz4gfsg3c1yxs9gd7i46")))

