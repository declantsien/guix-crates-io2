(define-module (crates-io bo lt bolt-attribute-bolt-system-input) #:use-module (crates-io))

(define-public crate-bolt-attribute-bolt-system-input-0.0.2 (c (n "bolt-attribute-bolt-system-input") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1p95lnq9adrd9fsfwml152g22y3cm1dp8fw6v793blcbvg4qsphw")))

(define-public crate-bolt-attribute-bolt-system-input-0.1.0 (c (n "bolt-attribute-bolt-system-input") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0gvz2bhb9xfvbrpqy90m4c5wsb5imhc5x8jb52inxns1i651rjfp")))

(define-public crate-bolt-attribute-bolt-system-input-0.1.1 (c (n "bolt-attribute-bolt-system-input") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1y6zfbbih3ynci2ikn1jpkfi961bjscyj6lndqgr2178ahhq93nc")))

(define-public crate-bolt-attribute-bolt-system-input-0.1.2 (c (n "bolt-attribute-bolt-system-input") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1jd712924jrq0z6fpv4qq5vk65f32cp735dq5s8wl1xl97pl3l6d")))

(define-public crate-bolt-attribute-bolt-system-input-0.1.3 (c (n "bolt-attribute-bolt-system-input") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "14z7i5lvkk5bxmsblj26zb0c9zsi3p6vn061p8qds3pxa1arvzv6")))

(define-public crate-bolt-attribute-bolt-system-input-0.1.4 (c (n "bolt-attribute-bolt-system-input") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1yqwmazkprixx5h6bpnqcsz1db572782cj56l5l1nkxddp2hiz61")))

(define-public crate-bolt-attribute-bolt-system-input-0.1.5 (c (n "bolt-attribute-bolt-system-input") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "146sp6kridsyi7iw1pi13mnmlc78fv3pkcv40wymmkq0binrsnmf")))

