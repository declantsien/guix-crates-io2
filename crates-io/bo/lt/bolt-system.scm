(define-module (crates-io bo lt bolt-system) #:use-module (crates-io))

(define-public crate-bolt-system-0.0.1 (c (n "bolt-system") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)))) (h "00qnybm52lzssjkjp99mdz61vskf4xy0avr01rj90c6wy0fabadd") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-system-0.0.2 (c (n "bolt-system") (v "0.0.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "bolt-helpers-system-template") (r "=0.0.2") (d #t) (k 0)))) (h "0dpnwl911pf7zg1akzxk6l6ha039rcds3rkl9il2qgvr4f48dass") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-system-0.1.0 (c (n "bolt-system") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "bolt-helpers-system-template") (r "=0.1.0") (d #t) (k 0)))) (h "0bqgs86pvzym45ggrk5ws0ljzgyfcrjqavmgb7kqvd5z04v0c1sl") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-system-0.1.1 (c (n "bolt-system") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "bolt-helpers-system-template") (r "=0.1.1") (d #t) (k 0)))) (h "07rvdnggdqnys77s33713gsxziyff6n7vdm1w20n2jbkjkfzahg0") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-system-0.1.2 (c (n "bolt-system") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-helpers-system-template") (r "=0.1.2") (d #t) (k 0)))) (h "1n8avbc2cx77blkx3fn8cnqlgcxg88ldpdm093yrlmbv6vkrlyfr") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-system-0.1.3 (c (n "bolt-system") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-helpers-system-template") (r "=0.1.3") (d #t) (k 0)))) (h "0r0gcrxpjq8714v3rnwrkkw6pri03fl2n3z2ikxpfnvyk5b8s99x") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-system-0.1.4 (c (n "bolt-system") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-helpers-system-template") (r "=0.1.4") (d #t) (k 0)))) (h "145pcqc0vr27r44lsgcb01k1gvzvvcib9wjl0zif21fjw4jcqg8b") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bolt-system-0.1.5 (c (n "bolt-system") (v "0.1.5") (d (list (d (n "anchor-lang") (r "=0.30.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "bolt-helpers-system-template") (r "=0.1.5") (d #t) (k 0)))) (h "0q6ybcga6wcr9m8h4l1vykrnzzqinicp72hgvbxr9fhaaqqrarj3") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("idl-build" "anchor-lang/idl-build") ("default") ("cpi" "no-entrypoint"))))))

