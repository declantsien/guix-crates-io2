(define-module (crates-io bo lt bolt-attribute-bolt-component-deserialize) #:use-module (crates-io))

(define-public crate-bolt-attribute-bolt-component-deserialize-0.0.1 (c (n "bolt-attribute-bolt-component-deserialize") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n7ipd0cdp48b52m3sy42w5pkwb52c1k64ifvcgiygk1sl4pczly")))

(define-public crate-bolt-attribute-bolt-component-deserialize-0.0.2 (c (n "bolt-attribute-bolt-component-deserialize") (v "0.0.2") (d (list (d (n "bolt-utils") (r "=0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0001liagqwy95rfarcn72cd1i70q6z1l9q370vk2h7mljqjd1g6l")))

(define-public crate-bolt-attribute-bolt-component-deserialize-0.1.0 (c (n "bolt-attribute-bolt-component-deserialize") (v "0.1.0") (d (list (d (n "bolt-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0mslvk66f5wrg1by9jcgg5czkxk2arv10pdpcvcny598j6a53xzd")))

(define-public crate-bolt-attribute-bolt-component-deserialize-0.1.1 (c (n "bolt-attribute-bolt-component-deserialize") (v "0.1.1") (d (list (d (n "bolt-utils") (r "=0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "17jzkf97fynv34vn0xn3r9p7niw225v8yz3ak351b37xnhqb0c44")))

(define-public crate-bolt-attribute-bolt-component-deserialize-0.1.2 (c (n "bolt-attribute-bolt-component-deserialize") (v "0.1.2") (d (list (d (n "bolt-utils") (r "=0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0072676lmb25l98r2ab4khrkzg1vh7bqrxrv6jg1rr8bvfv2scvp")))

(define-public crate-bolt-attribute-bolt-component-deserialize-0.1.3 (c (n "bolt-attribute-bolt-component-deserialize") (v "0.1.3") (d (list (d (n "bolt-utils") (r "=0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1cszyvsxd0iyrhih80pmqa1spdbri8mnmd7as1nlg4a339j61098")))

(define-public crate-bolt-attribute-bolt-component-deserialize-0.1.4 (c (n "bolt-attribute-bolt-component-deserialize") (v "0.1.4") (d (list (d (n "bolt-utils") (r "=0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0xxqq646ll4qal3garqjhrmgfc6k6q30z9ii31p663pgbsr6lhkl")))

(define-public crate-bolt-attribute-bolt-component-deserialize-0.1.5 (c (n "bolt-attribute-bolt-component-deserialize") (v "0.1.5") (d (list (d (n "bolt-utils") (r "=0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1s1ppizg85m273qd2s9yymzvx4i45z3fwxbyms9ap5jjbl26awi7")))

