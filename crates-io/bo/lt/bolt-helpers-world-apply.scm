(define-module (crates-io bo lt bolt-helpers-world-apply) #:use-module (crates-io))

(define-public crate-bolt-helpers-world-apply-0.0.2 (c (n "bolt-helpers-world-apply") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1mslrn9l5vzmlllglff0ybccdfap9l3ybgnrbqxlr609f1b03zs6")))

(define-public crate-bolt-helpers-world-apply-0.1.0 (c (n "bolt-helpers-world-apply") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0r8l5vznd5psam8bqq6g4i39n6bc63wqvpr29nrm15gc32qwfb3h")))

(define-public crate-bolt-helpers-world-apply-0.1.1 (c (n "bolt-helpers-world-apply") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0gim6q6n0lvk5rxwhj74dmsvcz08dy7iynm6ibp8m9k48ij95j9w")))

(define-public crate-bolt-helpers-world-apply-0.1.2 (c (n "bolt-helpers-world-apply") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1jh2y70zwj78xgrqxz2hn5vz7bcr47npcys6kby917c16h5pmm32")))

(define-public crate-bolt-helpers-world-apply-0.1.3 (c (n "bolt-helpers-world-apply") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "07djxw7j37r0880k32gwsn92mcg8r13kndfkf79lr7f9fr2xrsa9")))

(define-public crate-bolt-helpers-world-apply-0.1.4 (c (n "bolt-helpers-world-apply") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "14bgm08za1b430pfgk7aj7cnlrzsxjymj0xmddpzjhw4ijcpfjx2")))

(define-public crate-bolt-helpers-world-apply-0.1.5 (c (n "bolt-helpers-world-apply") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "14bb0a03cjpivzh8xma20vic9pnnn3jj96b1yf9ry7p84kh2zlh4")))

