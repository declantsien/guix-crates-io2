(define-module (crates-io bo lt bolt-helpers-system-template) #:use-module (crates-io))

(define-public crate-bolt-helpers-system-template-0.0.2 (c (n "bolt-helpers-system-template") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0hszqw3bn0jkxb2chwsyp1x606xxbl3fb1gq0xy1m42539kk3lhc")))

(define-public crate-bolt-helpers-system-template-0.1.0 (c (n "bolt-helpers-system-template") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0d6p3z3p1dc4v7spr1inj5pfjn1vfrmis4zxaa5i6fz5z2jzg3lc")))

(define-public crate-bolt-helpers-system-template-0.1.1 (c (n "bolt-helpers-system-template") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0j8nymkwzl44m25vdxm4diagxrn7cha8g4vyij5xfa1izam4gf7k")))

(define-public crate-bolt-helpers-system-template-0.1.2 (c (n "bolt-helpers-system-template") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "16gqgzmr0vcbifgi636n5sfrgwzjp2h64x86mi5jmb1p8gafnq8v")))

(define-public crate-bolt-helpers-system-template-0.1.3 (c (n "bolt-helpers-system-template") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "15yyvi99ddn2agfrn0axmqq40njmwq5w77vgcfnkbhy30b5wpa3j")))

(define-public crate-bolt-helpers-system-template-0.1.4 (c (n "bolt-helpers-system-template") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0c2q7x7m0lf14m0g8pziid8il93jl74vl5lh25r1i2bfwlcihlk1")))

(define-public crate-bolt-helpers-system-template-0.1.5 (c (n "bolt-helpers-system-template") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0xyd0ly893x862kc4c0sk7q55gixyyydlq2n63m63kzvk04a5kfm")))

