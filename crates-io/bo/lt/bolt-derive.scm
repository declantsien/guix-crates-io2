(define-module (crates-io bo lt bolt-derive) #:use-module (crates-io))

(define-public crate-bolt-derive-0.1.0 (c (n "bolt-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w1ycx58xp385ycfmihm3431vvvswjpm81nrsdd1gm1j87bbqv1m")))

(define-public crate-bolt-derive-0.2.0 (c (n "bolt-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sg0vc52nz0309jwxvfqhd20xgr6a6mgrp4fm91934yxsb2j0jkv")))

