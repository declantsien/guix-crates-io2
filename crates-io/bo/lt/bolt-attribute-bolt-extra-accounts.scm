(define-module (crates-io bo lt bolt-attribute-bolt-extra-accounts) #:use-module (crates-io))

(define-public crate-bolt-attribute-bolt-extra-accounts-0.1.2 (c (n "bolt-attribute-bolt-extra-accounts") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0a09hs7i6cn959mdqpy3i6ziiy5h8qynd84g39r9xzjh7rp229qp")))

(define-public crate-bolt-attribute-bolt-extra-accounts-0.1.3 (c (n "bolt-attribute-bolt-extra-accounts") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "00cgkma7rs61m90i1rjb58qiliv5n44mif4i7v6d4zzdxs4lp23g")))

(define-public crate-bolt-attribute-bolt-extra-accounts-0.1.4 (c (n "bolt-attribute-bolt-extra-accounts") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1aafk1l5azsgq2bv2ml3jviqbn3azqzigdqcivkjdcz7vzhl22kb")))

(define-public crate-bolt-attribute-bolt-extra-accounts-0.1.5 (c (n "bolt-attribute-bolt-extra-accounts") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "16q0pkxw43f1y8q3zkbb81fa4lndiq28w00js1cbz7h2z2cxhipd")))

