(define-module (crates-io bo lt bolt-attribute-bolt-program) #:use-module (crates-io))

(define-public crate-bolt-attribute-bolt-program-0.0.1 (c (n "bolt-attribute-bolt-program") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qbkrp5hf9m47zzbin5xivhn9xgrvfxbayfcprzz8nj5if42srfc")))

(define-public crate-bolt-attribute-bolt-program-0.0.2 (c (n "bolt-attribute-bolt-program") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0df5g6zpxkhslhfcvllns9w9pn95bx9slyf95rhmwr0lliabirnf")))

(define-public crate-bolt-attribute-bolt-program-0.1.0 (c (n "bolt-attribute-bolt-program") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0yw31adk9qpsa7fv0h4hq4yj6vi7arydydj7x1wl7vngvzdyx058")))

(define-public crate-bolt-attribute-bolt-program-0.1.1 (c (n "bolt-attribute-bolt-program") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0rskcssb48ndm3zvnjd9n616c4c3adn9kka78p8fzbl0ckan6s03")))

(define-public crate-bolt-attribute-bolt-program-0.1.2 (c (n "bolt-attribute-bolt-program") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1kfkvz576zl9dn16bavpw7jbg5l9g4hnjfvdipjz7g6qfcgdxcwm")))

(define-public crate-bolt-attribute-bolt-program-0.1.3 (c (n "bolt-attribute-bolt-program") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "116pv509f7sxhzw9sx8hn37z7zmnmn8mx0l10939f6xgyb9y1jz2")))

(define-public crate-bolt-attribute-bolt-program-0.1.4 (c (n "bolt-attribute-bolt-program") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0igmbjdrwxkv9bahhdda7sbvsi62rwfini0vy17pxarpa5cp1jl3")))

(define-public crate-bolt-attribute-bolt-program-0.1.5 (c (n "bolt-attribute-bolt-program") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1lrf11skfswgphzy0czj1dr4rgd1rsqn93994c1vj4z8yy8d95x8")))

