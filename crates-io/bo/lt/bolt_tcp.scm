(define-module (crates-io bo lt bolt_tcp) #:use-module (crates-io))

(define-public crate-bolt_tcp-0.11.11 (c (n "bolt_tcp") (v "0.11.11") (h "055pfzhdalihmglgc38ngllfdbv98qnv48a3l7k5lv18qiy811vm")))

(define-public crate-bolt_tcp-0.12.4 (c (n "bolt_tcp") (v "0.12.4") (d (list (d (n "bolt_common") (r "^0.12.4") (d #t) (k 0)))) (h "0pvx1x3sdjqsbhwljr174wwkpg5pwhpjgc1v8fxvw7sp7pfs9362")))

(define-public crate-bolt_tcp-0.12.5 (c (n "bolt_tcp") (v "0.12.5") (d (list (d (n "bolt_common") (r "^0.12.5") (d #t) (k 0)))) (h "0h3yqippw51ysywvbfbn46z2rivkq5if00wklm8n77llf78h1n85")))

