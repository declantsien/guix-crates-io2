(define-module (crates-io bo lt bolt-api) #:use-module (crates-io))

(define-public crate-bolt-api-0.1.0 (c (n "bolt-api") (v "0.1.0") (h "149aahx73jdb3pyx24fcizl4hvg7gl02f2wpgp5vvwk61pvbyv9f")))

(define-public crate-bolt-api-0.1.1 (c (n "bolt-api") (v "0.1.1") (d (list (d (n "iced") (r "^0.8.0") (f (quote ("glow"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)))) (h "16hnygifa9p8lp0hw1qs21scqpigsgkp98zqm2qjp5d9gqd9649w")))

(define-public crate-bolt-api-0.2.1 (c (n "bolt-api") (v "0.2.1") (d (list (d (n "iced") (r "^0.8.0") (f (quote ("glow"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)))) (h "0vgdnl9w7ybaqp4a3jbk330lwksnpr0lw6xvjrh09dqz0ynrzy37")))

