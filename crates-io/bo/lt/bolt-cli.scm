(define-module (crates-io bo lt bolt-cli) #:use-module (crates-io))

(define-public crate-bolt-cli-0.1.0 (c (n "bolt-cli") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1skj6il9zkykvnj6w5v9xwdsrn1k3255sr445bmw0j935kk6yh1q")))

