(define-module (crates-io bo lt bolt-attribute-bolt-component-id) #:use-module (crates-io))

(define-public crate-bolt-attribute-bolt-component-id-0.0.1 (c (n "bolt-attribute-bolt-component-id") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mq6qdbk5gair4m160gi85rnc00ib0wgxaqj4212f6c6rwia1y8q")))

(define-public crate-bolt-attribute-bolt-component-id-0.0.2 (c (n "bolt-attribute-bolt-component-id") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1waszi4zmi4jgcmkc15izgfcib5n848drcrvi098gqf324jrh1aa")))

(define-public crate-bolt-attribute-bolt-component-id-0.1.0 (c (n "bolt-attribute-bolt-component-id") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1zwrwngjar3pz66ax47h68ay9zvsl1zjl53p25sdfhd207x8vmam")))

(define-public crate-bolt-attribute-bolt-component-id-0.1.1 (c (n "bolt-attribute-bolt-component-id") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1bzsr0zqa1jvl7723qxslr1fmdc1nnwrpcj24m0w0izz1x3nv5j5")))

(define-public crate-bolt-attribute-bolt-component-id-0.1.2 (c (n "bolt-attribute-bolt-component-id") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1w1nwdfj02z1as7bzh1mc44088wh9f1qikrynf22hzx5rvvkgawp")))

(define-public crate-bolt-attribute-bolt-component-id-0.1.3 (c (n "bolt-attribute-bolt-component-id") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1730f48r1bnpgv23hg6fmrf8r332yi2vg3fvlf556rgjlg8bl90q")))

(define-public crate-bolt-attribute-bolt-component-id-0.1.4 (c (n "bolt-attribute-bolt-component-id") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "169qybqf2fl82w3bmgwjy9jzqc20vgdws59sipg6inryjy8z1w20")))

(define-public crate-bolt-attribute-bolt-component-id-0.1.5 (c (n "bolt-attribute-bolt-component-id") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "18qn75jnjfjsixvq2vw6q048g464p19mdry3zf34pvpw6v691d36")))

