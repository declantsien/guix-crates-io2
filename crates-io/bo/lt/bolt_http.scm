(define-module (crates-io bo lt bolt_http) #:use-module (crates-io))

(define-public crate-bolt_http-0.11.11 (c (n "bolt_http") (v "0.11.11") (d (list (d (n "bolt_common") (r "^0.11.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)))) (h "1h4zvn69qsb5jg0amdw517n4yz09f1rb70sziyws48v7aarr5n2v")))

(define-public crate-bolt_http-0.12.4 (c (n "bolt_http") (v "0.12.4") (d (list (d (n "bolt_common") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)))) (h "028d64nk54v785vhysr02wq71gqabr3h30lanpadmzn3y90xv6va")))

(define-public crate-bolt_http-0.12.5 (c (n "bolt_http") (v "0.12.5") (d (list (d (n "bolt_common") (r "^0.12.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)))) (h "1i0k68b4dhhzdglllrya13h315v28413q5bsf977k0caqmpx2lj7")))

