(define-module (crates-io bo lt boltserver) #:use-module (crates-io))

(define-public crate-boltserver-0.11.11 (c (n "boltserver") (v "0.11.11") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.3") (d #t) (k 0)))) (h "03jl2x61l84fglxzh8l7hmjm72ch69l2ybzyq2blk11lmnvb9l7i")))

