(define-module (crates-io bo lt bolt-client-macros) #:use-module (crates-io))

(define-public crate-bolt-client-macros-0.1.0 (c (n "bolt-client-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i4wh2qpblsh2x81my6q50i8adm1g85w4mjqjq82ls76gvk5p1mk")))

(define-public crate-bolt-client-macros-0.1.1 (c (n "bolt-client-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qw5s1rry28vxzs451g04l6a81x3fil7i8hsrgibab7k9qk7hqm9")))

(define-public crate-bolt-client-macros-0.2.0 (c (n "bolt-client-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "04ybgidinx01w92y65q1sddlb32zg8yfbmfq0n1073b25j9acnx9")))

(define-public crate-bolt-client-macros-0.2.1 (c (n "bolt-client-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0cr1kgw0w4i1wkz2ld938vxpr888nihdzvfy2265md0bj9f66c7y")))

(define-public crate-bolt-client-macros-0.2.2 (c (n "bolt-client-macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "000dw7rd15vv4hmbmvgvi97z8czqn088qdp70j70fscga1zrlc1x") (y #t)))

(define-public crate-bolt-client-macros-0.3.0 (c (n "bolt-client-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0lrb1am47j8smx998yfcsxyv95xszzs5ky5y6gc0fcg8h5f90b1c")))

