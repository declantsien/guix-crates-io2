(define-module (crates-io bo lt bolt-anchor-attribute-account) #:use-module (crates-io))

(define-public crate-bolt-anchor-attribute-account-0.29.1 (c (n "bolt-anchor-attribute-account") (v "0.29.1") (d (list (d (n "anchor-syn") (r "^0.29.0") (f (quote ("hash"))) (d #t) (k 0)) (d (n "bs58") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rnczww85fc747jc7m2hnyihyf6mgxmwpd73653da57hrgwywz0v") (f (quote (("idl-build" "anchor-syn/idl-build") ("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.60")))

