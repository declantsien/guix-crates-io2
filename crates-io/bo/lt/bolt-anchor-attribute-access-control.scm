(define-module (crates-io bo lt bolt-anchor-attribute-access-control) #:use-module (crates-io))

(define-public crate-bolt-anchor-attribute-access-control-0.29.1 (c (n "bolt-anchor-attribute-access-control") (v "0.29.1") (d (list (d (n "anchor-syn") (r "^0.29.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cc8q29sqp6cwzmv5fa8z3labgl9nb6zrz9mj9saq2xx0akvfjz5") (f (quote (("anchor-debug" "anchor-syn/anchor-debug")))) (r "1.60")))

