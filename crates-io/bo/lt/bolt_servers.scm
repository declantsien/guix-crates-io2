(define-module (crates-io bo lt bolt_servers) #:use-module (crates-io))

(define-public crate-bolt_servers-0.11.11 (c (n "bolt_servers") (v "0.11.11") (h "0rbmbd24nsdh6c17400w0n0wsxmyx681f0arc3651gkvqk1n8p0n")))

(define-public crate-bolt_servers-0.12.4 (c (n "bolt_servers") (v "0.12.4") (d (list (d (n "bolt_common") (r "^0.12.4") (d #t) (k 0)))) (h "1c03pxjy7nsihnqg79kd84kr9vjg264c0mdgwc0a8ifbmf8jipl1")))

(define-public crate-bolt_servers-0.12.5 (c (n "bolt_servers") (v "0.12.5") (d (list (d (n "bolt_common") (r "^0.12.5") (d #t) (k 0)))) (h "0dqxrmy3rri9p5cq3hx5z8k5hvqa2n4cgm1mligwkbc6hyn4nplm")))

