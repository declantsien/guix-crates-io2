(define-module (crates-io bo lt bolt-attribute-bolt-arguments) #:use-module (crates-io))

(define-public crate-bolt-attribute-bolt-arguments-0.1.2 (c (n "bolt-attribute-bolt-arguments") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1y3j4vsmzx4j1c71dzfp0pifmdawdnpalkjpv166yy8znx4jpg8w")))

(define-public crate-bolt-attribute-bolt-arguments-0.1.3 (c (n "bolt-attribute-bolt-arguments") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1ds7i67gdf985w7m55z4n57ykmb3gyxlgwb5f2bvf1fz6jsahdx7")))

(define-public crate-bolt-attribute-bolt-arguments-0.1.4 (c (n "bolt-attribute-bolt-arguments") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0z4hl0d0yz56g2r1pkhzsb427awf3c3cs6mxvibfjq22073gq240")))

(define-public crate-bolt-attribute-bolt-arguments-0.1.5 (c (n "bolt-attribute-bolt-arguments") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0sbqb1cia4zn8py756yyv2c7a181f07c4qf2ndcmx03rqg04k8cv")))

