(define-module (crates-io bo lt bolt11) #:use-module (crates-io))

(define-public crate-bolt11-0.1.0 (c (n "bolt11") (v "0.1.0") (d (list (d (n "base58check") (r "^0.0.1") (d #t) (k 0)) (d (n "bitcoin-bech32") (r "^0.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.1.13") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "098hi13i1sfkszjrbh3bnlfxbhp7nd09a7k6qq5xz170h7n6h4yz")))

