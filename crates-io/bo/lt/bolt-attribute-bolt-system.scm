(define-module (crates-io bo lt bolt-attribute-bolt-system) #:use-module (crates-io))

(define-public crate-bolt-attribute-bolt-system-0.0.1 (c (n "bolt-attribute-bolt-system") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0i7y5zd5vxcqljm1405xgjbj7hlzw1qjlnrhmib8qac48ib2i7hg")))

(define-public crate-bolt-attribute-bolt-system-0.0.2 (c (n "bolt-attribute-bolt-system") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0c4mxh7cm4jqjdyxqv2s8686kfaa30sq9giq2xl8qr3g6gf409a4")))

(define-public crate-bolt-attribute-bolt-system-0.1.0 (c (n "bolt-attribute-bolt-system") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "13rly51mw1q5mv5spnwf2zk8l1pqznxrmwjqnpc2nj6s3y5wxv1d")))

(define-public crate-bolt-attribute-bolt-system-0.1.1 (c (n "bolt-attribute-bolt-system") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0kiprpqbkhjipwcshj9276555j6f52avd6kkid3k0px13x5ll21b")))

(define-public crate-bolt-attribute-bolt-system-0.1.2 (c (n "bolt-attribute-bolt-system") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0an47qa4g7ymyzc08vn5l7w36ncrxpi37b1amk2xrih4wyvxvb0k")))

(define-public crate-bolt-attribute-bolt-system-0.1.3 (c (n "bolt-attribute-bolt-system") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1yjn2667h6z0lsb53ifzyrhcx1lwa2d2rjqhkfpr32rigi1rpgnm")))

(define-public crate-bolt-attribute-bolt-system-0.1.4 (c (n "bolt-attribute-bolt-system") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1xwkxfdghfhyw79xgv0jhysgv2wm3ax4fc6w78mkyjf8r6d7psml")))

(define-public crate-bolt-attribute-bolt-system-0.1.5 (c (n "bolt-attribute-bolt-system") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1h4fh3kjxsgavj6618a8is1ldb774i61c7rw6d4jyic1glylplgm")))

