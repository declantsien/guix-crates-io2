(define-module (crates-io bo lt bolt-server) #:use-module (crates-io))

(define-public crate-bolt-server-0.0.1 (c (n "bolt-server") (v "0.0.1") (d (list (d (n "cookie") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0r5h48y8wsipksv93xscydpvzkp4ljfqid5g8j5bqwx5qqsq371b") (y #t)))

