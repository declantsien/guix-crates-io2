(define-module (crates-io bo lt bolt_common) #:use-module (crates-io))

(define-public crate-bolt_common-0.11.11 (c (n "bolt_common") (v "0.11.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gn3dsk5jg8fvi7rxpw1mpb6iw3n926iqvr2skc0hp6a8s3nx9al")))

(define-public crate-bolt_common-0.12.3 (c (n "bolt_common") (v "0.12.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("js" "v4"))) (d #t) (k 0)))) (h "1265cc8xy8gzy277f7brqbd0yfwn2jkddrw59gws75vkdmndwbmb")))

(define-public crate-bolt_common-0.12.4 (c (n "bolt_common") (v "0.12.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("js" "v4"))) (d #t) (k 0)))) (h "13f7vi65pvlxkmgr6lbh8dq2kxxcav3kq1vp8h8cm0sn8665vryh")))

(define-public crate-bolt_common-0.12.5 (c (n "bolt_common") (v "0.12.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("js" "v4"))) (d #t) (k 0)))) (h "13p5hhqhmrn21c02zz3nz3184wadbp5h3isl7m9bf8zqdi3z4s3d")))

