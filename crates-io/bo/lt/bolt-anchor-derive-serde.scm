(define-module (crates-io bo lt bolt-anchor-derive-serde) #:use-module (crates-io))

(define-public crate-bolt-anchor-derive-serde-0.29.1 (c (n "bolt-anchor-derive-serde") (v "0.29.1") (d (list (d (n "bolt-anchor-syn") (r "^0.29.1") (d #t) (k 0)) (d (n "borsh-derive-internal") (r ">=0.9, <0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qbgkxqdkl4gmz0w5mq2v9pmfy6hvlj9ssnpxxcnfxppx4zcy3za") (f (quote (("idl-build" "bolt-anchor-syn/idl-build")))) (r "1.60")))

