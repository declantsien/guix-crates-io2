(define-module (crates-io bo ss bossac) #:use-module (crates-io))

(define-public crate-bossac-2.0.0 (c (n "bossac") (v "2.0.0") (d (list (d (n "bossa") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kpkkd6l66yyjmfr8hgxnpcs55djf60y3l1wi8i4xzn6m8vi0qry")))

(define-public crate-bossac-2.1.0 (c (n "bossac") (v "2.1.0") (d (list (d (n "bossa") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kv161b740jyhwj4b091b2p51a7ycadsj4hdnzswk1c66ml5x56h")))

(define-public crate-bossac-2.1.1 (c (n "bossac") (v "2.1.1") (d (list (d (n "bossa") (r "^2.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11xqs6v5pplisgp8225nwjk3km1a253qqnxbx8b323j0zkgvmzf9")))

