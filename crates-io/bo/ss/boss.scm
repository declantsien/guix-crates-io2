(define-module (crates-io bo ss boss) #:use-module (crates-io))

(define-public crate-boss-0.0.1 (c (n "boss") (v "0.0.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "isahc") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08xnjlq91k2v5vj2z6fpdc92qfwwq5y4byr05n4qw2s7hmi4g9j4")))

(define-public crate-boss-0.0.2 (c (n "boss") (v "0.0.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "isahc") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "05v18a5cnnh0sdh3m6bfr8397yb525dvarcxh8y92vw2xvj6fqx6")))

(define-public crate-boss-0.0.3 (c (n "boss") (v "0.0.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "isahc") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mrwfy2vjy30r929xmafafqkr12l6plpngg61dyajc4aq0nb5d1x")))

