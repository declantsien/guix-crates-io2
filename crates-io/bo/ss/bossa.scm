(define-module (crates-io bo ss bossa) #:use-module (crates-io))

(define-public crate-bossa-2.0.0 (c (n "bossa") (v "2.0.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1q9v6fh3m13ri1k3r1kahhqirva0s8i0yx6sqnzkmf7q9vih97v8")))

(define-public crate-bossa-2.1.0 (c (n "bossa") (v "2.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "11p03gh3m24pkcjw306dy9h67y9885lx7fmr62g98z00wbgd4gr7")))

(define-public crate-bossa-2.2.0 (c (n "bossa") (v "2.2.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1igw3maqsgsg7nhrws2amcnkpkzczf2ndk351sy4j5l6k6zfx86q")))

(define-public crate-bossa-2.3.0 (c (n "bossa") (v "2.3.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0zpri7b5dghh2sra8v1wj4mfryccpvyxj9rv8ddla81q75f4mbj1")))

