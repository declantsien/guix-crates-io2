(define-module (crates-io bo ss bossy) #:use-module (crates-io))

(define-public crate-bossy-0.1.0 (c (n "bossy") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0fhs0kkbz6f521jg1py1vjax0fvkp8bvnhfvfc003jf9fbqxw50w") (y #t)))

(define-public crate-bossy-0.1.1 (c (n "bossy") (v "0.1.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 2)))) (h "01pvas0n9gkz9babwj5r02yqz6ikd8b3xsfv9vxajalc4lrmw4iw")))

(define-public crate-bossy-0.1.2 (c (n "bossy") (v "0.1.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 2)))) (h "124hiydwgs6sgidimfp9wa00izjinqvhkc9r1yjqkd088y8wfksb")))

(define-public crate-bossy-0.1.3 (c (n "bossy") (v "0.1.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 2)))) (h "0g7ja01irwk2nznrjm7w2bp2d541pi8bkzwhk57039k19wmz8fm7")))

(define-public crate-bossy-0.1.4 (c (n "bossy") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "137sn1jimax5psw7qjakd7wn9cjrpvwxq310r0iv0hzd50sp33hg")))

(define-public crate-bossy-0.2.0 (c (n "bossy") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0g24ngdgl9c4kqv5954yks2gyy95p0bhbghh0ck3isgvjyilm9xy")))

(define-public crate-bossy-0.2.1 (c (n "bossy") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v6vxvg33p0x4a1bsi2gkvn8xny49h6dpqyqsiz2zaw8sr5xf160")))

