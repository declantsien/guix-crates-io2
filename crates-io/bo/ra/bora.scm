(define-module (crates-io bo ra bora) #:use-module (crates-io))

(define-public crate-bora-0.0.0 (c (n "bora") (v "0.0.0") (h "02rh09d304i373kj4i11331baklw5xhf2lfw5wkyg4j8mppvn0za")))

(define-public crate-bora-0.0.1 (c (n "bora") (v "0.0.1") (h "1068lgm8mv8qf2fbpqsflcjqvr96xvjd7vqmmlgvgzgnh3050qib")))

