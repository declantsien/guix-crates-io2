(define-module (crates-io bo x_ box_intersect_ze) #:use-module (crates-io))

(define-public crate-box_intersect_ze-0.1.0 (c (n "box_intersect_ze") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 2)))) (h "1jb7cph372si89pzj9drkx1b83s28jzj8v9xi45qapl1fw5gfiri") (f (quote (("rand-crate" "rand") ("default" "rand-crate"))))))

(define-public crate-box_intersect_ze-0.1.1 (c (n "box_intersect_ze") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 2)))) (h "10y7bv0771sfwcgvaw8k5z7943jqyb9fg499saild004zp5wrfdm") (f (quote (("rand-crate" "rand") ("default" "rand-crate"))))))

