(define-module (crates-io bo x_ box_raw_ptr) #:use-module (crates-io))

(define-public crate-box_raw_ptr-0.1.0 (c (n "box_raw_ptr") (v "0.1.0") (h "1x5d7q9qrz7xd2p22hmdywdqqz9gwhvrkdahzsv1vssyfp0knwji")))

(define-public crate-box_raw_ptr-0.1.2 (c (n "box_raw_ptr") (v "0.1.2") (h "1pij646lak03510lv828w3k10a1xgr6v43jka12ww5x6h0hya85g")))

(define-public crate-box_raw_ptr-0.1.4 (c (n "box_raw_ptr") (v "0.1.4") (h "1nqhwgy1c69x5yhcy7h2afjpr8ssm3kmp6r16hfcpn039chgqis0")))

(define-public crate-box_raw_ptr-0.1.6 (c (n "box_raw_ptr") (v "0.1.6") (h "1c0v4z71lr2hiwsm0i8dz1j1gwp3pywy17124w5s9bi7b318yyai")))

(define-public crate-box_raw_ptr-0.1.8 (c (n "box_raw_ptr") (v "0.1.8") (h "1wk87c76ldxydqcvc65m7vf7vxagxwwb2vavbbpaib8avpmi7x7f")))

(define-public crate-box_raw_ptr-0.2.0 (c (n "box_raw_ptr") (v "0.2.0") (h "005zw4fkm1bnmdaxx4w9cfxmgm1ka9hd9ai38ar5l63z536bwc2c")))

(define-public crate-box_raw_ptr-0.2.2 (c (n "box_raw_ptr") (v "0.2.2") (h "1r1p54zyikgm8slxbw3l8kakkqq41lzciifx10i6gwb5nx7gg6vy")))

(define-public crate-box_raw_ptr-0.2.4 (c (n "box_raw_ptr") (v "0.2.4") (h "1p7isdxr8h7rl9xpv9ks0mkym57k1v0hxlyljjzcw9wnfxk2b47j")))

(define-public crate-box_raw_ptr-0.3.0 (c (n "box_raw_ptr") (v "0.3.0") (h "0mp65d974vy95yqh8bz55iz95zdsb9gk7kcbl9p8fz9bkdvlfz45")))

(define-public crate-box_raw_ptr-0.3.1 (c (n "box_raw_ptr") (v "0.3.1") (h "1z2xc7y67nbc6msnpka7lf7fd02k7jnwkrnyk4md7sgzgsv1ish0")))

(define-public crate-box_raw_ptr-0.3.2 (c (n "box_raw_ptr") (v "0.3.2") (h "1q72bpm5d2104fypkjfrc1nbhfxvlsdjv8b7ghr5fj939vmaxgpq")))

(define-public crate-box_raw_ptr-0.3.4 (c (n "box_raw_ptr") (v "0.3.4") (h "1pqbb9p5kaqlx7bvmaqcbcjcfwnjdcv86rvib0vjjs0pnvp8qvaz")))

(define-public crate-box_raw_ptr-0.3.5 (c (n "box_raw_ptr") (v "0.3.5") (h "1mdnk8nbksg1m6swmwwqmcl2l11qh2jy2c2xdmhywh6z9z9kxwir")))

(define-public crate-box_raw_ptr-0.3.6 (c (n "box_raw_ptr") (v "0.3.6") (h "00kvcp2mhhr9qhp760fcf1s581zf500v59lmmg3a9zf6by1a69ys")))

