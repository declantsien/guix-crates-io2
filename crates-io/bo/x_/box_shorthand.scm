(define-module (crates-io bo x_ box_shorthand) #:use-module (crates-io))

(define-public crate-box_shorthand-0.1.0 (c (n "box_shorthand") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "112l72jxpgqyn3dpap8r780f26rnda4j96nxz9jfbgnbcsmdp4jd")))

