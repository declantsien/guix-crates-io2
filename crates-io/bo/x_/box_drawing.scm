(define-module (crates-io bo x_ box_drawing) #:use-module (crates-io))

(define-public crate-box_drawing-0.1.0 (c (n "box_drawing") (v "0.1.0") (h "0rwp6zwvjjn8aa6calzkfs4pm0vghis909284jaiwa1q8wpniqgm")))

(define-public crate-box_drawing-0.1.1 (c (n "box_drawing") (v "0.1.1") (h "0n5sy84mys78p6brvq02yvvc3zzl49vvkl43mz7x47v99zqnzgyq")))

(define-public crate-box_drawing-0.1.2 (c (n "box_drawing") (v "0.1.2") (h "0jx4rrxy4xmgmplmgl398vrng67sfl8qny7n7d91fyw6zpaxh9za")))

