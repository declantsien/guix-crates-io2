(define-module (crates-io bo x_ box_any) #:use-module (crates-io))

(define-public crate-box_any-0.1.0 (c (n "box_any") (v "0.1.0") (h "0q437r6rzkx0mfl0b20nb98drjyjfs58rkz75bnh9fnhj1zzdv22")))

(define-public crate-box_any-0.2.0 (c (n "box_any") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0dg4i3aqvmhw4wfmr94sk08a4dx1z0a3byqqc598sg6lzw1kgmrm")))

