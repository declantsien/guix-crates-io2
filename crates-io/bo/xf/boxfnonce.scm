(define-module (crates-io bo xf boxfnonce) #:use-module (crates-io))

(define-public crate-boxfnonce-0.0.1 (c (n "boxfnonce") (v "0.0.1") (h "128567dccb5k3jrirxxnir6m8ijkj642g3w8lchw80v0r9x6p8yi")))

(define-public crate-boxfnonce-0.0.2 (c (n "boxfnonce") (v "0.0.2") (h "0crjra3k4w29h2z8hcjkvjbgvw3jps0p3154lv69kijhn9wssv4w")))

(define-public crate-boxfnonce-0.0.3 (c (n "boxfnonce") (v "0.0.3") (h "0xfx8rrk320k2yjshspz4wjcjny0f901c1ljyvk9j479xxdi1043")))

(define-public crate-boxfnonce-0.1.0 (c (n "boxfnonce") (v "0.1.0") (h "13nr940xkffi9cargfjkzxgrdzccj3c92gs0rpixh8pkc32n1v6b")))

(define-public crate-boxfnonce-0.1.1 (c (n "boxfnonce") (v "0.1.1") (h "09ilf4zyx92hyhkxlsxksfyprzr9iwq5gqqb22aaqr32c8fwp22r")))

