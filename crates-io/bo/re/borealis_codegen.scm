(define-module (crates-io bo re borealis_codegen) #:use-module (crates-io))

(define-public crate-borealis_codegen-0.1.0 (c (n "borealis_codegen") (v "0.1.0") (d (list (d (n "aster") (r "^0.15") (d #t) (k 0)) (d (n "borealis") (r "^0.1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quasi") (r "^0.9") (d #t) (k 0)) (d (n "quasi_macros") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1") (d #t) (k 0)) (d (n "string_cache") (r "^0.2") (d #t) (k 0)))) (h "13ii2gvp0y222si900nphpsbzzq2kbv1w03p2dyc708vkjhy3c4r")))

