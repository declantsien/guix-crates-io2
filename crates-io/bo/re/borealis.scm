(define-module (crates-io bo re borealis) #:use-module (crates-io))

(define-public crate-borealis-0.1.0 (c (n "borealis") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.5") (d #t) (k 0)) (d (n "string_cache") (r "^0.2") (d #t) (k 0)))) (h "0n7vilivs5f29fzzy8yprcmcx7ijkhald93673rcwxqclhlyam0v") (f (quote (("nightly"))))))

