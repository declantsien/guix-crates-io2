(define-module (crates-io bo re boreal-parser) #:use-module (crates-io))

(define-public crate-boreal-parser-0.1.0 (c (n "boreal-parser") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "0sjl40wh18fq54ypshnnzrcbsgf6gz3xq8jfz82wv0nml3a8c1nj")))

(define-public crate-boreal-parser-0.2.0 (c (n "boreal-parser") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "0z0g1mn2vbw88dbv5jdidqvblkh6kmbpyryannp49c1q3nksih6m") (r "1.62")))

(define-public crate-boreal-parser-0.3.0 (c (n "boreal-parser") (v "0.3.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1hnpvsczzn0fkdcwlfymrbzpmdzx65vyf522lxd0xfz6knkn9dl9") (r "1.62")))

(define-public crate-boreal-parser-0.4.0 (c (n "boreal-parser") (v "0.4.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1gkf5srpajhx62s8v94iia89x2m0413sb860j8n37f1p5lswhlnf") (r "1.65")))

(define-public crate-boreal-parser-0.5.0 (c (n "boreal-parser") (v "0.5.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1zx8b1d1wxmpcjl1v9hivcxnplwz1fnn91fbrpqc3yqb7krmdcvm") (r "1.65")))

