(define-module (crates-io bo rr borrowned) #:use-module (crates-io))

(define-public crate-borrowned-0.1.0 (c (n "borrowned") (v "0.1.0") (h "0im6mjr8s1s46jkpkgp0wjxhqk20ig6pdlgz5i4h5ixmjjlrm9wx")))

(define-public crate-borrowned-0.1.1 (c (n "borrowned") (v "0.1.1") (h "1qilj0xy5cjgaky5x7x3hx5zd2bgkan3bl7328126f1xj7bm0nzp")))

(define-public crate-borrowned-0.2.0 (c (n "borrowned") (v "0.2.0") (h "1p8s24h8igc09pnn8420lhp2kvb1rhg5b9xnn9pc5ip6kh8agmnv")))

(define-public crate-borrowned-0.3.0 (c (n "borrowned") (v "0.3.0") (h "1iahr5jwscjkdgvr4lfsy3wq2aly20r24kfw09qx4y0ghgzfk5h7")))

(define-public crate-borrowned-0.4.0 (c (n "borrowned") (v "0.4.0") (h "17f813zikl58winii6jd0bamidgm2fhwh6w6b3kkv0cmdhrw3r9x")))

(define-public crate-borrowned-0.5.0 (c (n "borrowned") (v "0.5.0") (h "1145igp4w6lk0cm8nkr13rlcd2dhh1rcq4wais4dwsizgc3q8n2k")))

(define-public crate-borrowned-0.6.0 (c (n "borrowned") (v "0.6.0") (h "1iclkm2jhn7i3367w6q9pmy2dmz8dr0nhv9d8dxw3mmqgm2d2fxj")))

