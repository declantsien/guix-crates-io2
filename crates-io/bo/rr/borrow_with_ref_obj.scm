(define-module (crates-io bo rr borrow_with_ref_obj) #:use-module (crates-io))

(define-public crate-borrow_with_ref_obj-0.1.0 (c (n "borrow_with_ref_obj") (v "0.1.0") (h "0f678688hd7w8ijx7r2h5zqs61wpqn5yn8d66lfylgfmky4c0a73")))

(define-public crate-borrow_with_ref_obj-0.1.1 (c (n "borrow_with_ref_obj") (v "0.1.1") (h "0l5vs9sbd2ani02xm4576srxmi088h5cz85x0xhsrlia5hj00wkr")))

(define-public crate-borrow_with_ref_obj-0.1.2 (c (n "borrow_with_ref_obj") (v "0.1.2") (h "0130d7p603gbfy8iqwddc710qdk5bcpcgrplniixi8c3zn4hj3hq")))

(define-public crate-borrow_with_ref_obj-0.1.3 (c (n "borrow_with_ref_obj") (v "0.1.3") (h "0f2qaxnav8l15lpcfay5hh0wwp8bvddkmq6d1balmlmzxv2yb9mg")))

