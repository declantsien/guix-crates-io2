(define-module (crates-io bo rr borrow_trait) #:use-module (crates-io))

(define-public crate-borrow_trait-0.1.0 (c (n "borrow_trait") (v "0.1.0") (d (list (d (n "atomic_refcell") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cell") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0svh3qd65b56nv3kd0p2jlqmhldnh44d0645d8id1f6d5whnlibk") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-borrow_trait-0.1.1 (c (n "borrow_trait") (v "0.1.1") (d (list (d (n "atomic_refcell") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cell") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0dkq255g2lhsk86xkacb4839xv3wsbgsbjndayqqjm9ibzyda4cd") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

