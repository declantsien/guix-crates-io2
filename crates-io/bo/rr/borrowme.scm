(define-module (crates-io bo rr borrowme) #:use-module (crates-io))

(define-public crate-borrowme-0.0.1 (c (n "borrowme") (v "0.0.1") (d (list (d (n "borrowme-macros") (r "=0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0qfj4c6f1hn95k32npn0vy66q5hzc5nlnlgv3l1hfzzfb71g4gff") (r "1.65")))

(define-public crate-borrowme-0.0.2 (c (n "borrowme") (v "0.0.2") (d (list (d (n "borrowme-macros") (r "=0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1i5cr7znlp0kykv1mql2c3v73s5mnsl340827pi4fa18hg52v14v") (r "1.65")))

(define-public crate-borrowme-0.0.3 (c (n "borrowme") (v "0.0.3") (d (list (d (n "borrowme-macros") (r "=0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0qj18kx11z9gz78glgg4sg9aiznx52nvlzpr33zw74idsj0bplir") (r "1.65")))

(define-public crate-borrowme-0.0.4 (c (n "borrowme") (v "0.0.4") (d (list (d (n "borrowme-macros") (r "=0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "121a4an59v9any18zk8dirhvydzd4747mfh5m78zc20q1dcqlm59") (r "1.65")))

(define-public crate-borrowme-0.0.5 (c (n "borrowme") (v "0.0.5") (d (list (d (n "borrowme-macros") (r "=0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1kh9bjyf75nd6lhr1s55halhqh017gvx124s3nrazpgyqj9r6bil") (r "1.65")))

(define-public crate-borrowme-0.0.6 (c (n "borrowme") (v "0.0.6") (d (list (d (n "borrowme-macros") (r "=0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0aarnlvcvyggrwqg2kbyplg62aay40gbi8yjgmp1aarphnwg895a") (r "1.65")))

(define-public crate-borrowme-0.0.7 (c (n "borrowme") (v "0.0.7") (d (list (d (n "borrowme-macros") (r "=0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0jdir9zqc8g3rr0alnvwz1sy75mhfzv30b75658xbpl8axfnxiyk") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-borrowme-0.0.8 (c (n "borrowme") (v "0.0.8") (d (list (d (n "borrowme-macros") (r "=0.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "17avlbqnxggvcmqjj818ibdzyrjy9ccpnwy17pampdggxqvhlxzk") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-borrowme-0.0.9 (c (n "borrowme") (v "0.0.9") (d (list (d (n "borrowme-macros") (r "=0.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1z8zgn716fs1jzvlbh73iy7g1n1bhzjd7ly0yyfp1gnfmkmz8yng") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-borrowme-0.0.10 (c (n "borrowme") (v "0.0.10") (d (list (d (n "borrowme-macros") (r "=0.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1967c5hrjr2mrg4wwpikpsrm0gwxwyjfzhd6gxsgbv1yravzbnla") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-borrowme-0.0.11 (c (n "borrowme") (v "0.0.11") (d (list (d (n "borrowme-macros") (r "=0.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "04ykhnynxm7c6byn78s9y30ldfvzbqzigg58ziv4fkqs1w8fki86") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-borrowme-0.0.12 (c (n "borrowme") (v "0.0.12") (d (list (d (n "borrowme-macros") (r "=0.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0jbfphkxcqv963dc9477x77vr0q8k4p5h26g5zpvww1cchlvkgap") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-borrowme-0.0.13 (c (n "borrowme") (v "0.0.13") (d (list (d (n "borrowme-macros") (r "=0.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0h65dbcsdd1kiyc6zgdjpy64ziaq6av9wdw4chxn0wgw114zbjz2") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-borrowme-0.0.14 (c (n "borrowme") (v "0.0.14") (d (list (d (n "borrowme-macros") (r "=0.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1803s92pz8gfyyspmp27fnk98683x94y5ng3x5pglgzy2z4a7ldy") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-borrowme-0.0.15 (c (n "borrowme") (v "0.0.15") (d (list (d (n "borrowme-macros") (r "=0.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "06f5hjpzfpx1sb5q3lgbmhpd288ihx51irzgyaqwl1595y4dwxfc") (f (quote (("std") ("default" "std")))) (r "1.65")))

