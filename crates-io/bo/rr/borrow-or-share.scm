(define-module (crates-io bo rr borrow-or-share) #:use-module (crates-io))

(define-public crate-borrow-or-share-0.0.0 (c (n "borrow-or-share") (v "0.0.0") (h "1i1nv89fp1i2igw01kq9h9xqbvpgmwc1xvq5nfkk4cjdj58lx5fw") (f (quote (("std") ("default" "std"))))))

(define-public crate-borrow-or-share-0.0.1 (c (n "borrow-or-share") (v "0.0.1") (h "11rpgnq9kr5jxr97v6z4wr4a2598hs2wyb67p383l17j2c4xr9vv") (f (quote (("std") ("default" "std"))))))

(define-public crate-borrow-or-share-0.1.0 (c (n "borrow-or-share") (v "0.1.0") (h "0wfmyigdyiijkhv0s5cwnb3g0f34ri1wry9yx5ji8cfgallwnq2f") (f (quote (("std") ("default" "std"))))))

(define-public crate-borrow-or-share-0.2.0 (c (n "borrow-or-share") (v "0.2.0") (h "0y1m20c5ycdhi7943ybgf3qihrfsvb8784inwvyb24593kw5yrxp") (f (quote (("std") ("default"))))))

(define-public crate-borrow-or-share-0.2.1 (c (n "borrow-or-share") (v "0.2.1") (h "0bgh8rwl0113nllfigclzm4f3ddcfhsq1cf8m2f7x2dqdv7i0by9") (f (quote (("std") ("default"))))))

(define-public crate-borrow-or-share-0.2.2 (c (n "borrow-or-share") (v "0.2.2") (h "0ciski5i69a8mx6f0fh901hn73fii3g39lpl8k3xgi88651b9siy") (f (quote (("std") ("default"))))))

