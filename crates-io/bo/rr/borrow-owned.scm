(define-module (crates-io bo rr borrow-owned) #:use-module (crates-io))

(define-public crate-borrow-owned-0.1.0 (c (n "borrow-owned") (v "0.1.0") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "11s7jvzjbb3jyv3ss8p26bnv2lcaa7a06r8vnwa406pn70hqlkr1") (f (quote (("default") ("async" "futures-channel"))))))

