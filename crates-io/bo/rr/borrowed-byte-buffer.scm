(define-module (crates-io bo rr borrowed-byte-buffer) #:use-module (crates-io))

(define-public crate-borrowed-byte-buffer-0.0.1 (c (n "borrowed-byte-buffer") (v "0.0.1") (h "0swqgiag9nqb5jkmi3h8p7cngfs2sh7k7zdy94af3j5c31zxcy4m")))

(define-public crate-borrowed-byte-buffer-0.1.0 (c (n "borrowed-byte-buffer") (v "0.1.0") (h "13xdams3d7x7bmynsyb0ll6y9lls306hg9w4sb6l5jckyb22sj57")))

(define-public crate-borrowed-byte-buffer-0.1.1 (c (n "borrowed-byte-buffer") (v "0.1.1") (h "1vi003sxsjsvwsmws55zvjss7nj6nf73l3f5pdc3glixa29njqgg")))

