(define-module (crates-io bo rr borrow-bag) #:use-module (crates-io))

(define-public crate-borrow-bag-0.1.0 (c (n "borrow-bag") (v "0.1.0") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "1vcvixljrf4a5r822576531p3y8spjagaylh0vm03dbvbrk3xyzb")))

(define-public crate-borrow-bag-0.2.0 (c (n "borrow-bag") (v "0.2.0") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "0a8hyqhmlpmvk7w00zsqhnkd0myffx2ir8sysq1fsz9372s719p9")))

(define-public crate-borrow-bag-0.3.0 (c (n "borrow-bag") (v "0.3.0") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "1psi3dk0zy8bbybn4gz1b46yy8cjjgp66x44rll3k0xf6jypp5df")))

(define-public crate-borrow-bag-0.3.1 (c (n "borrow-bag") (v "0.3.1") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "1p840i131azp7s0h8bzrawwvwqdr0amhvy1dbiza3dkh6mz938y4")))

(define-public crate-borrow-bag-0.3.2 (c (n "borrow-bag") (v "0.3.2") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "0mjmkxlic7fgpk99qzm584a014ksa2s9acf77bdjglrz8mzijbxj")))

(define-public crate-borrow-bag-0.4.0 (c (n "borrow-bag") (v "0.4.0") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "0pm1p7r3sx7zi94b9irp8n11ss75b8j7wr9bl4r3mgs0n3ph1y2w")))

(define-public crate-borrow-bag-1.0.0 (c (n "borrow-bag") (v "1.0.0") (d (list (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0vx5bgc7i39xwsky2ncyma6f5y50nyyazqhw5vsicqz252lmzkx8")))

(define-public crate-borrow-bag-1.1.0 (c (n "borrow-bag") (v "1.1.0") (h "1mmgjzm2ivb6y7bj0j6kslqp8qri6g3n4jzq3hjjgxcf3fc1za12")))

(define-public crate-borrow-bag-1.1.1 (c (n "borrow-bag") (v "1.1.1") (h "1kzwqyakbdl8cndskfhz8vvr091l5p571abbm5gz6jd19pizwm5n")))

