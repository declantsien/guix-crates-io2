(define-module (crates-io bo rr borrowed-thread) #:use-module (crates-io))

(define-public crate-borrowed-thread-0.1.0 (c (n "borrowed-thread") (v "0.1.0") (h "04a6rl3cfirshhdawq2yf07bqyamg1cx8krvkci3lxizn5rl7p2n")))

(define-public crate-borrowed-thread-0.1.1 (c (n "borrowed-thread") (v "0.1.1") (h "158p2qdxgkcwsjd5p4546q9zj2kxm5rm4d424qabhbl12nc7knms")))

(define-public crate-borrowed-thread-0.1.2 (c (n "borrowed-thread") (v "0.1.2") (h "0dww33jpskj0pzl309hhi7890m89gb67p1drggjazmpxk1m3c33g")))

(define-public crate-borrowed-thread-0.1.3 (c (n "borrowed-thread") (v "0.1.3") (h "04q9kincimk437z81g0wmp7yr8ihnh97fbqri73dchh1d47ijvw1")))

