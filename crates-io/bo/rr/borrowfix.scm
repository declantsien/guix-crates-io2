(define-module (crates-io bo rr borrowfix) #:use-module (crates-io))

(define-public crate-borrowfix-0.1.0 (c (n "borrowfix") (v "0.1.0") (h "0inf76n5ls86wbb6y2spd0nd5jy2xjygrmgi4z8waa0wvqnj8ghp")))

(define-public crate-borrowfix-0.1.1 (c (n "borrowfix") (v "0.1.1") (h "1xm80shq5d49qydgrg0ak42pi4f31cd7p6p791f80kqjmhj3pizh")))

(define-public crate-borrowfix-0.1.2 (c (n "borrowfix") (v "0.1.2") (h "1rzbz02i0br4dw4w5zjwhwcjk6b9q2q8g86d5klg233cncj5bq40")))

(define-public crate-borrowfix-0.1.3 (c (n "borrowfix") (v "0.1.3") (h "0mrp1vg9zz9mkpqvwv8pn437x08sdv726kblwhrmpz1r0x4nq1w5")))

(define-public crate-borrowfix-0.1.4 (c (n "borrowfix") (v "0.1.4") (h "0zv3rzmhz5605rgs2ng4y9nm92b2pzfz48dpm7vp8khjmk9p528h")))

