(define-module (crates-io bo rr borrowing_exerci) #:use-module (crates-io))

(define-public crate-borrowing_exerci-0.1.0 (c (n "borrowing_exerci") (v "0.1.0") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "0lk416203df3jqdfgvh1bf7rld4c6kh7lbvgcca4sifj13lkhwx3") (f (quote (("ok") ("error"))))))

(define-public crate-borrowing_exerci-0.2.0 (c (n "borrowing_exerci") (v "0.2.0") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "12anacp0hphr29c7wrqp58k2xqw38p8lsrzsdnh29ams4gwy0hrp") (f (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2.1 (c (n "borrowing_exerci") (v "0.2.1") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "05zxrvfpvrbm3ylf4gaqz6gvbx7nglpq3sygwxyi3lc0pjxakgvj") (f (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2.2 (c (n "borrowing_exerci") (v "0.2.2") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "1g1p9k0bpip35l1k9jjwp6g2qj8i8k0m7mffdrj7a499akg358l5") (f (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2.50 (c (n "borrowing_exerci") (v "0.2.50") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "0lkba52jcl1a4ig43d18b2kj2v11wgbwm3qkw96497nvhjm2fxqp") (f (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2.51 (c (n "borrowing_exerci") (v "0.2.51") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "0qyc8spjq9w8crpzh4knmadl932wkifsig4c9mcmmwqaccxy1ff0") (f (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2.52 (c (n "borrowing_exerci") (v "0.2.52") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "0r6zwlar486l1mipxyfdlgg2z92f0w6xgmkwvh2ai9zmrdadspsp") (f (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.3.50 (c (n "borrowing_exerci") (v "0.3.50") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "0jqvk66b6j506d860qiix6vsiyv1x6rqvkni7czxppz7awjh42zg") (f (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.3.51 (c (n "borrowing_exerci") (v "0.3.51") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "068kqhpyxzbdfai9msalgcfbiy00v34vbzqj2p8gggc1n2r5g7jf") (f (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.3.52 (c (n "borrowing_exerci") (v "0.3.52") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "18051r431q2kjvkvmvbd53x0c30wwdj40pg9z177py1cfhli80va") (f (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.3.53 (c (n "borrowing_exerci") (v "0.3.53") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "1snf6fa56sksbp3vxg0gbd8sais0hjvql9grva077iy8qr0x6idd") (f (quote (("ok") ("err") ("cp"))))))

(define-public crate-borrowing_exerci-0.4.50 (c (n "borrowing_exerci") (v "0.4.50") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "15dv06rszahfd6fvx673z7iriy8n2shf9ld7krx2j7r17qcn6362") (f (quote (("okey") ("okay") ("ok") ("err_11") ("err_10") ("err_09") ("err_08") ("err_07") ("err_06") ("err_05") ("err_04") ("err_03") ("err_02") ("err_01") ("err") ("cp"))))))

(define-public crate-borrowing_exerci-0.4.51 (c (n "borrowing_exerci") (v "0.4.51") (d (list (d (n "bat") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "run_script") (r "^0.3.2") (d #t) (k 0)))) (h "0fqbg459qn9p070biw76arvjfzy5xbb7f8i2gsv46ypg0vjz324s") (f (quote (("okey") ("okay") ("ok") ("err_11") ("err_10") ("err_09") ("err_08") ("err_07") ("err_06") ("err_05") ("err_04") ("err_03") ("err_02") ("err_01") ("err") ("cp"))))))

