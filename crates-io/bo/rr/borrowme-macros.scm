(define-module (crates-io bo rr borrowme-macros) #:use-module (crates-io))

(define-public crate-borrowme-macros-0.0.1 (c (n "borrowme-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1schdr1fc15yv3pnsyrmdj3fn6cx6jqinwbb010r4vzxq9b6vy9j") (r "1.65")))

(define-public crate-borrowme-macros-0.0.2 (c (n "borrowme-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1f2jvc7c7zdl87zw6x23vgcyyyxv06ix8frf0bi4alrmwl3rrn0c") (r "1.65")))

(define-public crate-borrowme-macros-0.0.3 (c (n "borrowme-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1k4w258cg23xvr0rq2i6prqx4am8wrj0f18z5frjzy39v97h9li0") (r "1.65")))

(define-public crate-borrowme-macros-0.0.4 (c (n "borrowme-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0bhrpdxrya1mfxig1ra3gls3f8fw26a0lprpjsqs9vxpd4zykm5z") (r "1.65")))

(define-public crate-borrowme-macros-0.0.5 (c (n "borrowme-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0hqjzmfdmj9fimxnj02nb0p6619agr2fwsy6sqw1429910v6xcjj") (r "1.65")))

(define-public crate-borrowme-macros-0.0.6 (c (n "borrowme-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1830b8ys55svmqs1rcwfh20rczh7yi5zqizwzcg1bvqy2z8n13vz") (r "1.65")))

(define-public crate-borrowme-macros-0.0.7 (c (n "borrowme-macros") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1xm4df8yj0xn9js2xl4vmd2yf6910kd8l5zhna9fwab73p5pbyvc") (r "1.65")))

(define-public crate-borrowme-macros-0.0.8 (c (n "borrowme-macros") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0mfjqxvzf0gh726s1hyidqgjg9jj5x6n0dhgg5inmfbraq1cbax1") (r "1.65")))

(define-public crate-borrowme-macros-0.0.9 (c (n "borrowme-macros") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1c547fzsv19j920gz4b50qab8lnhxhkz96s3j8d3rxag48qazf81") (r "1.65")))

(define-public crate-borrowme-macros-0.0.10 (c (n "borrowme-macros") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1plvhgbz0jpa8i3msdj8abmlgxrm1adibinj1ss8g64d3qsby4v0") (r "1.65")))

(define-public crate-borrowme-macros-0.0.11 (c (n "borrowme-macros") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1f9bnb6aqd97hxs4ch771b05j44c9kfcb27qyia1vr53pzfawdiz") (r "1.65")))

(define-public crate-borrowme-macros-0.0.12 (c (n "borrowme-macros") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0nc0dbp6dsvsa894zj0sz3krlkrn076g66z9bnbhpi7ckxrldas7") (r "1.65")))

(define-public crate-borrowme-macros-0.0.13 (c (n "borrowme-macros") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0l11r3j9q6lha6h2wwlzaqlv133z3cf3v5v4yv72jy47qzid50vh") (r "1.65")))

(define-public crate-borrowme-macros-0.0.14 (c (n "borrowme-macros") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1y53cvrxkirf08w9sp2q3q1ij4jnb15w3i5w4310zgwd37mzv6p1") (r "1.65")))

(define-public crate-borrowme-macros-0.0.15 (c (n "borrowme-macros") (v "0.0.15") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1l20q2yzzahmswbidfnry4idaizhk9xar7a230n7idad6jdls3xh") (r "1.65")))

