(define-module (crates-io bo ye boyer-moore-magiclen) #:use-module (crates-io))

(define-public crate-boyer-moore-magiclen-0.1.0 (c (n "boyer-moore-magiclen") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "1jmig1h3qy3imrr6b5gz7p1fbwcrv6ymykq88758lkfrn0ibzpyi") (f (quote (("nightly"))))))

(define-public crate-boyer-moore-magiclen-0.1.1 (c (n "boyer-moore-magiclen") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "0i5sch5fxj4dkbxx1vnj0cp1fclvkjmg4rf248g1079dzwdpz57n") (f (quote (("nightly"))))))

(define-public crate-boyer-moore-magiclen-0.1.2 (c (n "boyer-moore-magiclen") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "1psiqrkxcdy36sgzkflzxpjpnb0g15vcn6vzs8l6vsmb048xjqgh") (f (quote (("nightly"))))))

(define-public crate-boyer-moore-magiclen-0.1.3 (c (n "boyer-moore-magiclen") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "0gn7cpfkii1s0pi1fsvfkn28k3x29c1ry5k1ap8kfdl3xnm4n58q") (f (quote (("nightly"))))))

(define-public crate-boyer-moore-magiclen-0.1.4 (c (n "boyer-moore-magiclen") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "0i7gnf2jb45cgz1785fxp27i4dj3xvxc9kabysvf64nh4bmpyvff") (f (quote (("nightly"))))))

(define-public crate-boyer-moore-magiclen-0.1.5 (c (n "boyer-moore-magiclen") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "debug-helper") (r "^0.1") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "03wzn956aymf9nw4cj3mjfsfv6hi8pi45i7jwzsmbar624faa935") (f (quote (("nightly"))))))

(define-public crate-boyer-moore-magiclen-0.1.6 (c (n "boyer-moore-magiclen") (v "0.1.6") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "debug-helper") (r "^0.1") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "1irvwif0ljyfzdhnxppw1mxs64hs81yzsskpp6241r4n069hk00v") (f (quote (("nightly"))))))

(define-public crate-boyer-moore-magiclen-0.1.7 (c (n "boyer-moore-magiclen") (v "0.1.7") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "debug-helper") (r "^0.2") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "0xv2llk4pl5k7jp4kx86pxybhj42s9797fwd8awgqv5swba7fz4c") (f (quote (("nightly"))))))

(define-public crate-boyer-moore-magiclen-0.1.8 (c (n "boyer-moore-magiclen") (v "0.1.8") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "0aw6bxsi5rcv6p3jyq4w6r53468rkyk4ngm4s8mmffambmdxn63v") (f (quote (("nightly"))))))

(define-public crate-boyer-moore-magiclen-0.1.9 (c (n "boyer-moore-magiclen") (v "0.1.9") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "hashmap_core") (r "^0.1.10") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "03xwjin163wx8ds9blxc7b5mn1jf22avh47a11yc74m9aphmvh1m") (y #t)))

(define-public crate-boyer-moore-magiclen-0.1.10 (c (n "boyer-moore-magiclen") (v "0.1.10") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "hashmap_core") (r "^0.1.10") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "13m0f6g32gqrvkkwm2vfrvz3iblmpkymxxlsj4wa6z2ldylgps0i") (y #t)))

(define-public crate-boyer-moore-magiclen-0.1.11 (c (n "boyer-moore-magiclen") (v "0.1.11") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "1d6vq23l29ja4m3fginww1mjqdk6g2i175j4wl9if9gzr5xgqw2s") (f (quote (("std") ("default" "std"))))))

(define-public crate-boyer-moore-magiclen-0.2.0 (c (n "boyer-moore-magiclen") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "01pm6jh376q5dafcj5sqzxlgnhmh2w2im24q68r9cy0wrcd76i9w") (f (quote (("character")))) (y #t)))

(define-public crate-boyer-moore-magiclen-0.2.1 (c (n "boyer-moore-magiclen") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "1j8n1k3hca9zmpmkgjdga4fl0hhx7b3n3dlkirdd4l55f85ilh9v") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.2 (c (n "boyer-moore-magiclen") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "05d83kgn22pg20hqi291qh500zm0i07kwasfi0bisp8skfr9yyjr") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.3 (c (n "boyer-moore-magiclen") (v "0.2.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "183bzsfxbyidw3n29kgwdzbi68d4x9qk29d9yd1n3drw79dyj4mz") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.4 (c (n "boyer-moore-magiclen") (v "0.2.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "1g0nb90i3vcgfriyw6cl5271qmzxix44ac34pvf1v9q0x71rrfy9") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.5 (c (n "boyer-moore-magiclen") (v "0.2.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^1.0.4") (d #t) (k 2)))) (h "1wawb97s25kdw0rspbawkj88vd1m5pbaf4ibwskih93ff16k8hqb") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.6 (c (n "boyer-moore-magiclen") (v "0.2.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)))) (h "0zph102k0si62f6gfzxsq1079lxk8ci21akq2zklmxy24fx5klr5") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.7 (c (n "boyer-moore-magiclen") (v "0.2.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "0dclsqdm4p9sqaf43zi2c9bzq1p7rxxx7z2nhcc5pl0ymllj4qb1") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.8 (c (n "boyer-moore-magiclen") (v "0.2.8") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "1c9q80kbsh0y3px6p6hy5ri81nba5raabq6vf28lrwd37yfwd7l1") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.9 (c (n "boyer-moore-magiclen") (v "0.2.9") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "0r31fgbry15438r54lckwyiggsknhji2vzqs7vxahig58q6idslx") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.10 (c (n "boyer-moore-magiclen") (v "0.2.10") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "0y95nyxch0p9hyfrnc4d6snpjii0qi5xflslffdb3y339flqw9qn") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.11 (c (n "boyer-moore-magiclen") (v "0.2.11") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "0c3hgiyfws8nclz43ss3b38qwkcf86w4lgafwzb2fhz14jdaq023") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.12 (c (n "boyer-moore-magiclen") (v "0.2.12") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "0pjfggmzk9hqdvqzdm34nx3h42pqd203wwlv8y06zhdg51w2ca6k") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.13 (c (n "boyer-moore-magiclen") (v "0.2.13") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "1q13mrf3z9l1kyxac1wqg1jcj9rxs9khnb0c6wjhwpspvgyjcai7") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.14 (c (n "boyer-moore-magiclen") (v "0.2.14") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "1gkqf6mxl2zwf1s334cl71lwq63934lfi13x69cwhdzkcvlffv42") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.15 (c (n "boyer-moore-magiclen") (v "0.2.15") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "0v13vhqyj3rl7n6zaf78lmwhrpqp16qdaaybf6djdhw1xb7a7pgx") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.16 (c (n "boyer-moore-magiclen") (v "0.2.16") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "0d9hcmlj6siy3q1dibshwp09b1za5305d6z4836izxrp79mynxvc") (f (quote (("character"))))))

(define-public crate-boyer-moore-magiclen-0.2.17 (c (n "boyer-moore-magiclen") (v "0.2.17") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "07q91wscmz9bdrmhl2nyc7iw9i4vymbfnfq11h9lxn6l8kvfk7a0") (f (quote (("character")))) (y #t) (r "1.60")))

(define-public crate-boyer-moore-magiclen-0.2.18 (c (n "boyer-moore-magiclen") (v "0.2.18") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "0zia318x2icl1r98ld5vwlscv83awiarapsxv76kxc2px3z7cv8i") (f (quote (("character")))) (r "1.61")))

(define-public crate-boyer-moore-magiclen-0.2.19 (c (n "boyer-moore-magiclen") (v "0.2.19") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "0m8gnzaiv9a1k3zi0dn4n5mh9wn9zpgkz1mkp39d7i5wxc44lkw5") (f (quote (("character")))) (r "1.61")))

(define-public crate-boyer-moore-magiclen-0.2.20 (c (n "boyer-moore-magiclen") (v "0.2.20") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 2)) (d (n "scanner-rust") (r "^2") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 2)))) (h "1i21f96jmbnwgxm7grzvcxajb1aqi3imi7dg7h95nswj5lzj7rlm") (f (quote (("character")))) (r "1.65")))

