(define-module (crates-io bo ni bonitox) #:use-module (crates-io))

(define-public crate-bonitox-0.1.1 (c (n "bonitox") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hf-hub") (r "^0.3.1") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "llama-cpp-2") (r "^0.1.41") (f (quote ("sampler"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xyskxm86kbyjffj9f0ikmfii5d7yb4dbczgblp6a2f83sny5wgn")))

