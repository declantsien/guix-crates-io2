(define-module (crates-io bo gu boguin) #:use-module (crates-io))

(define-public crate-boguin-0.1.0 (c (n "boguin") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 2)) (d (n "clap") (r "^2.31.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "http-with-url") (r "^0.2.0") (d #t) (k 0)) (d (n "httparse") (r "^1.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.5") (d #t) (k 0)))) (h "1ldvz1lsl53pma0k9pmrdm9iz8vn8h9wxpb62r623pklmpl05na3") (y #t)))

