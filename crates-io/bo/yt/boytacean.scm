(define-module (crates-io bo yt boytacean) #:use-module (crates-io))

(define-public crate-boytacean-0.1.1 (c (n "boytacean") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1zc2zapbq8dlzqbjxiph5jgqqb057s97v0nvkiri1ff9yidlg6y9") (f (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-boytacean-0.2.0 (c (n "boytacean") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0fwzmd3hn1pdqz1jf8aq28bbp1aq66jimis8a12622n56n7j1fkn") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.3.0 (c (n "boytacean") (v "0.3.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0agd67sphd96wv4za6v4b6n5i79kib7f2bl6qxdfjx7aj87b6yaq") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.4.0 (c (n "boytacean") (v "0.4.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "10dpzai1866vvd6c8hhwqy5l6rw8cj09p618lyafimxrv2mvmy2d") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.4.1 (c (n "boytacean") (v "0.4.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0ypdh7ck22brrmgx1ylmiqj4h8llzfjxi6nz39bsnyqaiwak77y2") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.4.2 (c (n "boytacean") (v "0.4.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0j4k44rzj8jl8hm2pwra3d7c5hm8g83wk7301knr8d8l4j4jkf20") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.4.3 (c (n "boytacean") (v "0.4.3") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1v02ry2b2ilm5mngv6gm0d23v4w0nv0fi2zrvgbb1w9kh0qn149z") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.4.4 (c (n "boytacean") (v "0.4.4") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "03pm07sjf7q348hhzc55f1fm21lswfcc1ai5c8b4fnf9acvzkvbm") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.4.5 (c (n "boytacean") (v "0.4.5") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "19avyc80hxgf75v1gm78jay7zvsz2jw8srxfgdzxwxhrsnc8jfn2") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.5.0 (c (n "boytacean") (v "0.5.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1gbqh7wa45c3b91pdpmar6zlx57whvk4afn1r9pbn6q3zr063r7g") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.5.1 (c (n "boytacean") (v "0.5.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "10agffygysb0m6i5iyig4083ng8hd6nzjg29l7s8anb832b1zkqr") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.5.2 (c (n "boytacean") (v "0.5.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "19xbcyrq8867nqrc3anj68ljafi5zqb8666xhz740rfkdrkgjyl5") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.5.3 (c (n "boytacean") (v "0.5.3") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1jmcraal6dxkmhvfqjiwavqrdciyc524p125sw4vha2m8zc49sr6") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.5.4 (c (n "boytacean") (v "0.5.4") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "13ywxplig6mb4n9bz08lb3fa9symv16vc515pb8p9md9amnf790i") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.5.5 (c (n "boytacean") (v "0.5.5") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1m4ay1xfyx036k25ix7r23pf2wrqk3rvx1anp9q7jb03wl4hsn73") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.5.6 (c (n "boytacean") (v "0.5.6") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1bki0i3yd6j9j187cqgm6fvhixh7cwjdi1fsn7ak5298zksw83kw") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.5.7 (c (n "boytacean") (v "0.5.7") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1lcnllhmw041lkydz6w16c7sm91cz1gnn3j02za08a4sjlgsc7mv") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.1 (c (n "boytacean") (v "0.6.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1hh7451sprw15nm1a0pa3a4q19m7rica3gr860fnflvk7kvd4rhc") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.2 (c (n "boytacean") (v "0.6.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0zdyy75qayrylk1laj17xgaqbz4mrxfi2p5g6rhganidady38pmh") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.3 (c (n "boytacean") (v "0.6.3") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "188r9q66blgbvkxzl5q1gaj1a8h5f2xxvplr42sjwz8h8ik4fb8b") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.4 (c (n "boytacean") (v "0.6.4") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1snzzv264qcxk1288hk9h806ys8pk7z0x0gnlhqladnj1qwnhv25") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.5 (c (n "boytacean") (v "0.6.5") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1f87x96j3vcbaizcnkhsirjq86nkr0cln4fcb1kn40r9idyrqw1v") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.6 (c (n "boytacean") (v "0.6.6") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "083ywwbw3y68insqpjphhm4qpkdjy2k0j03i4f73j42v5zcj3xjl") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.7 (c (n "boytacean") (v "0.6.7") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "10cgs2y6dz2jp1676mw4vwyz6lzramf3pcj14rr26sc0b11xm0n4") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.8 (c (n "boytacean") (v "0.6.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "num_cpus") (r "^1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0gjh31nyzz80kf6gd6slmr8j64533gfbqmlzs8cqvnjkfi858a6z") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.11 (c (n "boytacean") (v "0.6.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0h0zabf85g71x8s1brg3z90psf1mknxpkwra7y73x29bx2b9pg7w") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

(define-public crate-boytacean-0.6.12 (c (n "boytacean") (v "0.6.12") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1qdrf21qji3nmx745k4zb18fmx12310nx80i302vdb5h3ll6dxyk") (f (quote (("wasm" "wasm-bindgen") ("gen-mock") ("debug"))))))

(define-public crate-boytacean-0.7.0 (c (n "boytacean") (v "0.7.0") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0m7yl24b645j69f4q8d0jwlda23344h6pg729b03k1zbcgzsilkw") (f (quote (("wasm" "wasm-bindgen") ("gen-mock") ("debug"))))))

(define-public crate-boytacean-0.7.1 (c (n "boytacean") (v "0.7.1") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1fb69i5ssc2jbfq1z8z9vcz2wvy70l7636dnmk50r0rgnzp1way0") (f (quote (("wasm" "wasm-bindgen") ("gen-mock") ("debug"))))))

(define-public crate-boytacean-0.7.2 (c (n "boytacean") (v "0.7.2") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "15fhf957d5ddzkrsswf1r4x55hf6sizy2djp8l7j1adbm0hqw2l3") (f (quote (("wasm" "wasm-bindgen") ("gen-mock") ("debug"))))))

(define-public crate-boytacean-0.7.3 (c (n "boytacean") (v "0.7.3") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1lj0masshizf5j1pcla1gb9kn4zf0m32rwq2lxar47pm0qjia3pr") (f (quote (("wasm" "wasm-bindgen") ("gen-mock") ("debug"))))))

(define-public crate-boytacean-0.7.4 (c (n "boytacean") (v "0.7.4") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "02vl8r11n01cp1kr87q67yh02ihghjpwizjcsx63np5anlcbvp1v") (f (quote (("wasm" "wasm-bindgen") ("gen-mock") ("debug"))))))

(define-public crate-boytacean-0.7.5 (c (n "boytacean") (v "0.7.5") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1wn1r6dmarxyafr2wx3n3mg927wkvvp32rnz0v9i4ydsvdnrjx51") (f (quote (("wasm" "wasm-bindgen") ("gen-mock") ("debug"))))))

(define-public crate-boytacean-0.8.0 (c (n "boytacean") (v "0.8.0") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0a9ncsmk6v7d8afwliddsxhmn3c93q0rich0aj9ra37ww3vsqj0p") (f (quote (("wasm" "wasm-bindgen") ("gen-mock") ("debug"))))))

(define-public crate-boytacean-0.9.0 (c (n "boytacean") (v "0.9.0") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0vp1irxhww466a0iys2ggx7x9a9ilwpdyfh310d6jaq9x1gymhvg") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.1 (c (n "boytacean") (v "0.9.1") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "18hywhz8p43yhws4d62a5a3yivwi2701qgjqjjdb64sfy9v5vk58") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.2 (c (n "boytacean") (v "0.9.2") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1haf78q3q1qrry1yafi6c4gnvp637mw10yv1nh4fgpip17h6kyjl") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.3 (c (n "boytacean") (v "0.9.3") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "08i5lz80jpzbnlpwibq51n05qhwllnbnnwz3zg25332995s7w5li") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.4 (c (n "boytacean") (v "0.9.4") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "07rvjwaxf7zi6a4b6d78fbmm8pgm58vw4zsr3kq0a4dq6fbs4bk5") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.5 (c (n "boytacean") (v "0.9.5") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "06856l9chp2bwh0nc0zp1a27p816258caylfhj13y7wb5qhfv5n7") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.6 (c (n "boytacean") (v "0.9.6") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "119mh0w1fz96w5vk205cchliy71vh03y99np2qg6f5z76345yrmw") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.7 (c (n "boytacean") (v "0.9.7") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1w5rg2ca4qv2hili3vfy6s5mjshm6jaz4v8yhxv61alznajs4403") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.8 (c (n "boytacean") (v "0.9.8") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1s109n32x1ihkxk2382nx31pmsyhkssqirjvq9214v991xzfrc4g") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.10 (c (n "boytacean") (v "0.9.10") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "05yykp79xsn4z3b24lxd7kzlii5jrrjhvwxir0vgkjpl28j6r53d") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.9 (c (n "boytacean") (v "0.9.9") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "02j5wmsb95v4220h6l97z318dnj7h970lzycys7b3252p4pbchl5") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.11 (c (n "boytacean") (v "0.9.11") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1x24w3nzmf6wwkw910lwbcn4y8l5b9l8hnm9lh7spcq6rv8y3j69") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.12 (c (n "boytacean") (v "0.9.12") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0cjdmxaph8cfaiwhhxjhy9vrzkbmlyj0nm752n9wd121q6fidmx5") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.13 (c (n "boytacean") (v "0.9.13") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "13sn4pppv4lwlfvcc7f2dii9apm0i0qnwidj9b22l2big3gc6g4a") (f (quote (("wasm" "wasm-bindgen") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.14 (c (n "boytacean") (v "0.9.14") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1bw24b0lk8899am620y8fdzidhkvnh5x590bf51vn99dcx9l6ixx") (f (quote (("wasm" "wasm-bindgen" "js-sys") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.15 (c (n "boytacean") (v "0.9.15") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1mbdib07d7gdbbn6lq68f61v33r50hp9zjcgpb2943wmyhxpi84s") (f (quote (("wasm" "wasm-bindgen" "js-sys") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.16 (c (n "boytacean") (v "0.9.16") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "02jzbx5cpfpambh9qwvc8fif2y4ygm9nh9y04lpsr73qi7833cxn") (f (quote (("wasm" "wasm-bindgen" "js-sys") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.17 (c (n "boytacean") (v "0.9.17") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "17wm6xn07zjzzjh7zyhjddnf4hnd48kqj9bqyiba417hla09fwd0") (f (quote (("wasm" "wasm-bindgen" "js-sys") ("python" "pyo3") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

(define-public crate-boytacean-0.9.18 (c (n "boytacean") (v "0.9.18") (d (list (d (n "built") (r "^0.5") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0g3kkdhqqfyzfwzvd4rc1h81ygcz54avf1gw8ibrh84h932a5qc6") (f (quote (("wasm" "wasm-bindgen" "js-sys") ("python" "pyo3") ("pedantic") ("gen-mock") ("debug") ("cpulog"))))))

