(define-module (crates-io bo ol bool-toggle) #:use-module (crates-io))

(define-public crate-bool-toggle-1.0.0 (c (n "bool-toggle") (v "1.0.0") (h "1chn6qdg6r2hd6zqr4ml5477i1chsk1rk9zba9xd37ihmhs84ms1")))

(define-public crate-bool-toggle-1.1.0 (c (n "bool-toggle") (v "1.1.0") (h "1d5c5ci67nb2777day92ahfa69ilq944lnjfbs11nivszsm9drh9")))

(define-public crate-bool-toggle-1.1.1 (c (n "bool-toggle") (v "1.1.1") (h "0g37kiymal236ckh6bxv2i5l30x10xzdkisz7n5vix1nqwrh0m7j")))

