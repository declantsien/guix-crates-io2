(define-module (crates-io bo ol bool_traits) #:use-module (crates-io))

(define-public crate-bool_traits-0.1.0 (c (n "bool_traits") (v "0.1.0") (h "1q66n6fv35pil7wd8fsf6rbivsyp9dyh5c8294v46jxidz2xqvqz") (r "1.77.0")))

(define-public crate-bool_traits-0.1.1 (c (n "bool_traits") (v "0.1.1") (h "1fzmfmcwvg41q459l2b6wis94jclrx7rmil46q4hfdkkyfzigsa1") (r "1.77.0")))

