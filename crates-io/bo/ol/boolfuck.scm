(define-module (crates-io bo ol boolfuck) #:use-module (crates-io))

(define-public crate-boolfuck-0.1.0 (c (n "boolfuck") (v "0.1.0") (h "04hn6xww5xriz841j2rmy6i0xbm6225lzas6c0537z2cz1fz2lhm") (y #t)))

(define-public crate-boolfuck-1.0.0 (c (n "boolfuck") (v "1.0.0") (h "1smzv77cac7hxd574g8gibrx74c1b8im1vw24favx35zam68slc2") (y #t)))

(define-public crate-boolfuck-1.0.1 (c (n "boolfuck") (v "1.0.1") (h "0bg8xkqc97rc7d2mj09a1r8ndijyrq5fgrr38kxlmjli3ii0827y")))

