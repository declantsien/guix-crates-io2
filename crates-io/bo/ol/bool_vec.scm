(define-module (crates-io bo ol bool_vec) #:use-module (crates-io))

(define-public crate-bool_vec-0.1.0 (c (n "bool_vec") (v "0.1.0") (d (list (d (n "count-macro") (r "^0.2.2") (d #t) (k 0)))) (h "11v3yxphkf3ihfhck8xs1aqj4h8bblyp70vlbxxdz6dn3ly5zax1") (y #t)))

(define-public crate-bool_vec-0.1.1 (c (n "bool_vec") (v "0.1.1") (d (list (d (n "count-macro") (r "^0.2.2") (d #t) (k 0)))) (h "0phla9q8p0kqyvjqaxljbmch1npgbz0kzy93v2i43snisccwx56c") (y #t)))

(define-public crate-bool_vec-0.1.2 (c (n "bool_vec") (v "0.1.2") (d (list (d (n "count-macro") (r "^0.2.2") (d #t) (k 0)))) (h "0kjsrk0j34192slnk23jm8c1p57a9f1qsxmlpall976a2rw9pyl3")))

(define-public crate-bool_vec-0.2.0 (c (n "bool_vec") (v "0.2.0") (d (list (d (n "count-macro") (r "^0.2.2") (d #t) (k 0)))) (h "0vxwqn6a11gmqpbzbbk7jcyvwgls39rvby9aajag0685082cc5b2") (y #t)))

(define-public crate-bool_vec-0.2.1 (c (n "bool_vec") (v "0.2.1") (d (list (d (n "count-macro") (r "^0.2.2") (d #t) (k 0)))) (h "02wawr8pxsk8rpj8j9ikf5ixmgb5qkyd0b9l4f2gfz2vhlk9h4md")))

