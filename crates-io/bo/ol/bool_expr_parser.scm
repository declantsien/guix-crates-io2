(define-module (crates-io bo ol bool_expr_parser) #:use-module (crates-io))

(define-public crate-bool_expr_parser-0.3.13 (c (n "bool_expr_parser") (v "0.3.13") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0jzjhvqc01q2z67z82awnb3zrc5ssd7n5dz3p50bf65h13qlr9is")))

