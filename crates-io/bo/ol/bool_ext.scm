(define-module (crates-io bo ol bool_ext) #:use-module (crates-io))

(define-public crate-bool_ext-0.1.0 (c (n "bool_ext") (v "0.1.0") (h "1im0ihjzy0m4l6jg7aaawawh28jm0pnfg2si04pv827w8ws27abp") (y #t)))

(define-public crate-bool_ext-0.1.1 (c (n "bool_ext") (v "0.1.1") (h "1648c0i0c7dga62jqsv7mp3dkw84zpi2mfxrbp6bk6lhm82j6gj1")))

(define-public crate-bool_ext-0.2.0 (c (n "bool_ext") (v "0.2.0") (h "0i352wibds2m3p3gw2ak97q9z6db54mqdfvkn7256w8haqdj4mwh")))

(define-public crate-bool_ext-0.3.0 (c (n "bool_ext") (v "0.3.0") (h "1r5jgqrc5dmqc864ih9z943dvall8hbdhb0hjnarjn1zsn7zvjjj")))

(define-public crate-bool_ext-0.3.1 (c (n "bool_ext") (v "0.3.1") (h "1lls4v9s70wckscmdnmijrx6l2pwcfcyfs6m05qfs6hqiwq1lxmb")))

(define-public crate-bool_ext-0.4.0 (c (n "bool_ext") (v "0.4.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "0c1ifjy1ll36ld9f8b7pxnpblqgxicv443l34pwb6hwqi8m81b1h")))

(define-public crate-bool_ext-0.4.1 (c (n "bool_ext") (v "0.4.1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "0l6micr39xrziahf1lfw2xq2hvfnx35bd8q2ckn60987immhnzd5") (y #t)))

(define-public crate-bool_ext-0.4.2 (c (n "bool_ext") (v "0.4.2") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "1jpryyz518fqlb636vm1l7cvpgm6k934zz5yyp4d5lp3lgd0xlnb")))

(define-public crate-bool_ext-0.4.3 (c (n "bool_ext") (v "0.4.3") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "0ng2jylw3l2lw7pynkzink39b0f24ydjg0sw16pa24kdnavvdk2a")))

(define-public crate-bool_ext-0.5.0 (c (n "bool_ext") (v "0.5.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "1pgjz6wgh9vvk0sbi2kik9vjrlsiys2rif0fv9wvq5ajvypxw57v")))

(define-public crate-bool_ext-0.5.1 (c (n "bool_ext") (v "0.5.1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "1wayjnr1i5xqc6hxx7w24069ryn854ylzq5ckfjan18pbnpsbapl")))

(define-public crate-bool_ext-0.5.3 (c (n "bool_ext") (v "0.5.3") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "1h7xwpfq3jm5m8shmj010bzqjbgc68g3kij02vfrmndkbwwwyhnz") (f (quote (("std") ("default" "std"))))))

(define-public crate-bool_ext-0.6.0 (c (n "bool_ext") (v "0.6.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "1izr85rm8vwfjpmmlnp2nl32q9fdcx1rwaab1n7aw8w7b1rins6s") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bool_ext-0.6.1 (c (n "bool_ext") (v "0.6.1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "044zsbygb7inwxk1x4v4ksy70q83zgzcbwazid8369p2d9ywc36v") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bool_ext-0.6.2 (c (n "bool_ext") (v "0.6.2") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)))) (h "0q5yk6m4yhbhs7wwf34iy5zr8v9ak7j6vnpgawqyzp5hfxyr65gg") (f (quote (("std") ("default" "std"))))))

