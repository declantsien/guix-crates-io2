(define-module (crates-io bo ol boolean) #:use-module (crates-io))

(define-public crate-boolean-0.3.0 (c (n "boolean") (v "0.3.0") (d (list (d (n "parameterized") (r "^0.1.1") (d #t) (k 2)))) (h "0pf8fcj3li0w7dbxm06li9l4xcxmw7fa2an1hgnj7dj5j7mi5wzi") (f (quote (("has_alloc") ("global_values") ("default" "global_values" "has_alloc"))))))

