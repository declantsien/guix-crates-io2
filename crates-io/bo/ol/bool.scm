(define-module (crates-io bo ol bool) #:use-module (crates-io))

(define-public crate-bool-0.1.0 (c (n "bool") (v "0.1.0") (d (list (d (n "parameterized") (r "^0.1.1") (d #t) (k 2)))) (h "0w21wlm25hplz7nczd7kcy99lv5jgx8yg367g27i9b7hk838hyfg") (f (quote (("global_values") ("default" "global_values"))))))

(define-public crate-bool-0.2.0 (c (n "bool") (v "0.2.0") (d (list (d (n "parameterized") (r "^0.1.1") (d #t) (k 2)))) (h "1a9w0rx9531w95ffni48xsx58ny4s0nlfckwy1dkrihng6m7rnsw") (f (quote (("global_values") ("default" "global_values"))))))

(define-public crate-bool-0.3.0 (c (n "bool") (v "0.3.0") (d (list (d (n "parameterized") (r "^0.1.1") (d #t) (k 2)))) (h "1j48lr9hxigl3kb10f6dz6fzkykjbhixigjymz3dk7vd4sfqaw1b") (f (quote (("has_alloc") ("global_values") ("default" "global_values" "has_alloc"))))))

