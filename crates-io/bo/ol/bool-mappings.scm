(define-module (crates-io bo ol bool-mappings) #:use-module (crates-io))

(define-public crate-bool-mappings-0.1.0 (c (n "bool-mappings") (v "0.1.0") (h "17g57fad7yzdfalgx47a92ryjf7h4yagbf4vnicwcnbv6w92n1x2")))

(define-public crate-bool-mappings-0.1.1 (c (n "bool-mappings") (v "0.1.1") (h "154hjx3d74ywn9hjksm3cv9w88z23swvk6qdfjh9r1w757apybp7")))

(define-public crate-bool-mappings-0.1.2 (c (n "bool-mappings") (v "0.1.2") (h "04pzrpnyngy38f9i844vsls0l304saayyvcx92db1m4fsd3awkqq")))

