(define-module (crates-io bo ol boolean_expression) #:use-module (crates-io))

(define-public crate-boolean_expression-0.1.0 (c (n "boolean_expression") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "005ri042hc92p4cv1a9xbd184a327pfc2x1nb6h1jycivffkmsvg")))

(define-public crate-boolean_expression-0.2.0 (c (n "boolean_expression") (v "0.2.0") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "0d1v9fp6v4p8fks2y1hjhjglc0sdklmyn2b3xf30ak2zdjkl4km2")))

(define-public crate-boolean_expression-0.3.0 (c (n "boolean_expression") (v "0.3.0") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "0nihj0za6mws373rz675bv5gx8kz5h8rdnvdcsn8dp05fa8hdx13")))

(define-public crate-boolean_expression-0.3.2 (c (n "boolean_expression") (v "0.3.2") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "1f47rb2lsr8hj8mga4z68nyd0lzv54bby6710mdxymvkclszv5aa")))

(define-public crate-boolean_expression-0.3.3 (c (n "boolean_expression") (v "0.3.3") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "08hw8qf24sr9br42w2nymirfc2qnq4paczx6amgrr0angdljpgbb")))

(define-public crate-boolean_expression-0.3.4 (c (n "boolean_expression") (v "0.3.4") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "0whhavkmzv6jf8404rdvzx81z57410zdk1xb4laimzdlxly2dbwr")))

(define-public crate-boolean_expression-0.3.5 (c (n "boolean_expression") (v "0.3.5") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "0cdhg80kklb34jl2cj6lgd26p31wf1jqh1ad37phqchrwig2p8cw")))

(define-public crate-boolean_expression-0.3.6 (c (n "boolean_expression") (v "0.3.6") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "1cc6ph9ji5ynxc0il3mxq84p87xcgfa3dhx68xkdwywy7y6b5kq5")))

(define-public crate-boolean_expression-0.3.7 (c (n "boolean_expression") (v "0.3.7") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "1gywdphhjw605fbj05w50b1s4a9s1wd6i1b1jng36g8zmiy6dxnv")))

(define-public crate-boolean_expression-0.3.8 (c (n "boolean_expression") (v "0.3.8") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "1gxf3plrzhq1kf9bn2n00xpjxx7iid7grywpyc5ab033dybsg1j3")))

(define-public crate-boolean_expression-0.3.9 (c (n "boolean_expression") (v "0.3.9") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "114zfvaf34vfmk3lx0dwl5jq4wvdp2dfmza9s3k8k7zvm1zm5g6q")))

(define-public crate-boolean_expression-0.3.10 (c (n "boolean_expression") (v "0.3.10") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "04y95khnj1kxf7i161gnl0d7xgdik7f1agp02dbs3yhywkr4ybf3")))

(define-public crate-boolean_expression-0.3.11 (c (n "boolean_expression") (v "0.3.11") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "10zspcm7h2dsywi4d26h8crj10qf0mn2clnk5wjx58l18iifycvw")))

(define-public crate-boolean_expression-0.4.0 (c (n "boolean_expression") (v "0.4.0") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "1dc5rrkaps962bzav1hhbksq4bnkwmfyb049kl242dz18kikk91x")))

(define-public crate-boolean_expression-0.4.1 (c (n "boolean_expression") (v "0.4.1") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "1lnybvqwj4ipimlwqzgfy20lq2jqlkqahxfdvr829fzpjmafvqaa")))

(define-public crate-boolean_expression-0.4.2 (c (n "boolean_expression") (v "0.4.2") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "1ilaccp28p7069v388b9qwiwdsxxhp29kmj89jagmvwvkdr7nahj") (y #t)))

(define-public crate-boolean_expression-0.4.3 (c (n "boolean_expression") (v "0.4.3") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "1npc72hy6188jnr7j80zhsgv9vh8b0ha9a057p5rah4m77y3vbx3")))

(define-public crate-boolean_expression-0.4.4 (c (n "boolean_expression") (v "0.4.4") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "1h77ny61dvp3r7lag33im0hyp9dx75lfj582bv8rrpzms533p8aj")))

