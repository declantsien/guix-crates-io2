(define-module (crates-io bo ol boolenum) #:use-module (crates-io))

(define-public crate-boolenum-0.1.0 (c (n "boolenum") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 2)) (d (n "syn") (r "^1.0.31") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "1s2xikd5j5p7aywrg4x0qwffi9bv2a12kqcs1br6s0nphpaspj56")))

