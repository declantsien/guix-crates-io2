(define-module (crates-io bo ol boolector) #:use-module (crates-io))

(define-public crate-boolector-0.1.0 (c (n "boolector") (v "0.1.0") (d (list (d (n "boolector-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i8cg112v32yxnvafrh2c008frrf73q6yfgbc8dx7wbhsakbdg8a")))

(define-public crate-boolector-0.1.1 (c (n "boolector") (v "0.1.1") (d (list (d (n "boolector-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14hzx98i7bb31i1bnlvslmbadl56p9ixn48xjaj00nq6qbhv2bk9")))

(define-public crate-boolector-0.1.2 (c (n "boolector") (v "0.1.2") (d (list (d (n "boolector-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lkz93ddk005gqwirx70aq5liinrwir1b8a8vs180gyyxng79bvp")))

(define-public crate-boolector-0.2.0 (c (n "boolector") (v "0.2.0") (d (list (d (n "boolector-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1j3c5n3vfvcb4dicmlgg5w9w1y3yyr1yy89bminiy5br3cz3wi1i")))

(define-public crate-boolector-0.3.0 (c (n "boolector") (v "0.3.0") (d (list (d (n "boolector-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s5ad40g4z0a74318h99lni9rgbr0snc6p6iijdhij35l5b208mx")))

(define-public crate-boolector-0.4.0 (c (n "boolector") (v "0.4.0") (d (list (d (n "boolector-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "0zp7d80kfjnh1zbpi9rcds9vash9linqcfxwi40bj0yfsl1g4sh4")))

(define-public crate-boolector-0.4.1 (c (n "boolector") (v "0.4.1") (d (list (d (n "boolector-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "1liqz03sj27adcwvq158dspv8h1drnm9287jb406ysf4k1hzzmfa")))

(define-public crate-boolector-0.4.2 (c (n "boolector") (v "0.4.2") (d (list (d (n "boolector-sys") (r "^0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "1ah4zvql1i1prgi4g32mqkp8gw6psci2z60ws260wxydl10b9hrd") (f (quote (("vendor-lgl" "boolector-sys/vendor-lgl"))))))

(define-public crate-boolector-0.4.3 (c (n "boolector") (v "0.4.3") (d (list (d (n "boolector-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "08gxqlbsy98p89l46ly5kwyj0js2ykyx5bxd4hbgdwf88sggjrsm") (f (quote (("vendor-lgl" "boolector-sys/vendor-lgl"))))))

