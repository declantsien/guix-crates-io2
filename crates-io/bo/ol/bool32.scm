(define-module (crates-io bo ol bool32) #:use-module (crates-io))

(define-public crate-bool32-0.1.0 (c (n "bool32") (v "0.1.0") (h "0hfpg6as7mx8pbvh0fw9db1hw5vrfl98zj4mxmrz8z115w2mv1qc")))

(define-public crate-bool32-0.1.1 (c (n "bool32") (v "0.1.1") (h "1z26kipfp33csyyl77rydbfhb012z51azhcdnc8asmsy68vrfig8")))

(define-public crate-bool32-1.0.0 (c (n "bool32") (v "1.0.0") (h "0iv8xz6xddwhvzjcx77ydlghqli52sjqdzkxq85gpy2p1djfzl3f")))

