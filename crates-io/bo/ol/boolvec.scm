(define-module (crates-io bo ol boolvec) #:use-module (crates-io))

(define-public crate-boolvec-0.1.0 (c (n "boolvec") (v "0.1.0") (h "0ppc7vzjjdblsliihrnbpahqrh8j1gvd8cnly47ak1a1793cqwz0")))

(define-public crate-boolvec-0.2.0 (c (n "boolvec") (v "0.2.0") (h "07i6lljsvnj8b6qhpak31v2rkb36cz1gfmmp6r7q7py12x9d6mpv") (y #t)))

(define-public crate-boolvec-0.2.1 (c (n "boolvec") (v "0.2.1") (h "0yjdxpf6vzs5v9lz9z07jcl341qjvv77x3dm4lwbjfxi6z96sycr") (y #t)))

(define-public crate-boolvec-0.2.2 (c (n "boolvec") (v "0.2.2") (h "1vxxz40w5v1qsrghwrva3x38m74jblv55bfc45llwyks70a68fl2") (y #t)))

(define-public crate-boolvec-0.2.4 (c (n "boolvec") (v "0.2.4") (h "1v83049pklqymwfvhdk7kf4vpgcxd50h4glyp7gbbbh9d08fygyh")))

(define-public crate-boolvec-0.2.5 (c (n "boolvec") (v "0.2.5") (h "01wziwpxiyln4apjkjd0zagav5q2w9nyhhyrchsxn2l28mfy578x")))

(define-public crate-boolvec-0.2.6 (c (n "boolvec") (v "0.2.6") (h "002csj0x6mvfhz6rw04b4gzfiz03d2jvmqx9qnc4sx8ycgjs6my0")))

