(define-module (crates-io bo ol boolnetevo) #:use-module (crates-io))

(define-public crate-boolnetevo-0.3.0 (c (n "boolnetevo") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pgxrhx92y77b9crwwqwk1ggxg54ar22kaahx90bgjhv6drjbqga")))

(define-public crate-boolnetevo-0.3.1 (c (n "boolnetevo") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02aj1imnqk2mfjx5jb2cspiw45gjqb3pcbkhcyqgjardnr93nnxx")))

(define-public crate-boolnetevo-0.3.2 (c (n "boolnetevo") (v "0.3.2") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j03ksmw06px27mrrmhy056dblqyvp9kcz95llqv57f0k47nkpxd")))

(define-public crate-boolnetevo-0.4.0 (c (n "boolnetevo") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13biklszx7py5jmjydl4a3knf9r1kq1hgan2knpm7kg9bgdqxkz5")))

