(define-module (crates-io bo ol boolector-sys) #:use-module (crates-io))

(define-public crate-boolector-sys-0.1.0 (c (n "boolector-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "1ndzsahjmg8p8s29r5r71jm0lb5ds3x847y8ks41xpbrr018mxhq")))

(define-public crate-boolector-sys-0.1.1 (c (n "boolector-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0dp5ganzax4j4l924f94dwshl13hby9cl5k4h688285m2qrkrq6a")))

(define-public crate-boolector-sys-0.1.2 (c (n "boolector-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0lvdaj40nfhxrgm8cgsr9sp616qzbkdgp4g0k06lwsxqj0b75gg2")))

(define-public crate-boolector-sys-0.2.0 (c (n "boolector-sys") (v "0.2.0") (h "18lkfq5pwc4l5l0zhn6a50dc8kl84j6s5v1vwmianwj13hsnpycs")))

(define-public crate-boolector-sys-0.3.0 (c (n "boolector-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mn5rv4ykd06cb7dx3zizklhw98brdvp4vis1kq8gkym34h2lxal")))

(define-public crate-boolector-sys-0.3.1 (c (n "boolector-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i7fccirv3ggp8plwzdzpc3yh4q4hlgrimc1dd64z7gplcggw6ps")))

(define-public crate-boolector-sys-0.4.0 (c (n "boolector-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kqmg4bbnrw7gsxwa5yfy10n04limvjlc8cyihycv7awjmkfzza7")))

(define-public crate-boolector-sys-0.5.0 (c (n "boolector-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "01xhpak0w6iqrmyq7yz0wzhkinh662jpfk1cwapsk454w2w0rh7h")))

(define-public crate-boolector-sys-0.6.0 (c (n "boolector-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "1ywdwnsrayzllq56yz9pp9bl9s0vf0c7npgkanri3wgxjjwmw7zn") (f (quote (("vendor-lgl" "cc" "cmake")))) (l "boolector")))

(define-public crate-boolector-sys-0.6.1 (c (n "boolector-sys") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "0a79zcjs9mirvndqn0si726mzx6hyrhr2qi44av4dqymzwpvwz65") (f (quote (("vendor-lgl" "cc" "cmake")))) (l "boolector")))

(define-public crate-boolector-sys-0.6.2 (c (n "boolector-sys") (v "0.6.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "07hwrjjpq7g4y56fb2mb802pnjaqx7vl9dq6j4k47q1533bzqdcq") (f (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (l "boolector")))

(define-public crate-boolector-sys-0.6.3 (c (n "boolector-sys") (v "0.6.3") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "1ir7m4farw1m0dd2i99hmqhydpd67z04pbihvp73q4q3fihpl22b") (f (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (l "boolector")))

(define-public crate-boolector-sys-0.7.0 (c (n "boolector-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "0zwxdlig4nqnyzadhnbqh7a6ijb8njaikjf71hcy7r4pw9hrfr8y") (f (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (l "boolector")))

(define-public crate-boolector-sys-0.7.1 (c (n "boolector-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "1rb3b7zd4r82y1psxbsd0phz3xhv8m7i3yqvsm576kfr2hf2s22v") (f (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (l "boolector")))

(define-public crate-boolector-sys-0.7.2 (c (n "boolector-sys") (v "0.7.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "14w0d489qrhb4biy4idz92mzd1xdg6f99whh46wly5rpkkvc5w0p") (f (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (l "boolector")))

