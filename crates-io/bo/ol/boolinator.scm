(define-module (crates-io bo ol boolinator) #:use-module (crates-io))

(define-public crate-boolinator-0.1.0 (c (n "boolinator") (v "0.1.0") (h "0z052qap49ixbikqfm4r64156dv9ynjbz2x4zjxbywzjwbgrmj47")))

(define-public crate-boolinator-2.4.0 (c (n "boolinator") (v "2.4.0") (h "1nccxzb1dfkjfrgzqaw1a90p26zlvv6nah5ckcpj6bn9a4zqga6g")))

