(define-module (crates-io bo t- bot-handler-manager) #:use-module (crates-io))

(define-public crate-bot-handler-manager-0.1.0 (c (n "bot-handler-manager") (v "0.1.0") (d (list (d (n "tts_rust") (r "^0.1.3") (d #t) (k 0)))) (h "0b1c3qv1r1a8gyl9bv9i5rbcggzap3n1pr4y3kssv3vy2ljwfd5p")))

(define-public crate-bot-handler-manager-0.1.1 (c (n "bot-handler-manager") (v "0.1.1") (d (list (d (n "tts_rust") (r "^0.1.3") (d #t) (k 0)))) (h "11621a50gjry3kicxdyks2r08fl07qa096kx2jwpx2xgzzv1fc4k")))

(define-public crate-bot-handler-manager-0.1.2 (c (n "bot-handler-manager") (v "0.1.2") (d (list (d (n "tts_rust") (r "^0.2.0") (d #t) (k 0)))) (h "0a48jy2g0qsivi2w7v8xgicpnrnxx2i1pxrkfashh8lrwrq5cadz")))

(define-public crate-bot-handler-manager-0.1.3 (c (n "bot-handler-manager") (v "0.1.3") (d (list (d (n "tts_rust") (r "^0.2.0") (d #t) (k 0)))) (h "1d0a73ph2n3lil4bl36x9lvq4zdhivswcymdl3kzvicwb3mm2vi5")))

