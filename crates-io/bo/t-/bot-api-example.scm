(define-module (crates-io bo t- bot-api-example) #:use-module (crates-io))

(define-public crate-bot-api-example-0.1.0 (c (n "bot-api-example") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6") (d #t) (k 0)) (d (n "actix-http") (r "^3.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.3") (d #t) (k 0)) (d (n "anthill-di") (r "^1.2.4") (d #t) (k 0)) (d (n "anthill-di-derive") (r "^1.2.4") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-lock") (r "^2.6") (d #t) (k 0)) (d (n "async-trait-with-sync") (r "^0.1") (d #t) (k 0)) (d (n "botx-api") (r "^0.1") (f (quote ("anthill-di"))) (d #t) (k 0)) (d (n "botx-api-framework") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1jg5k6x33d505iini3hyrjnp7a0p4r1w1s0w4ahbwx937nsvq889")))

(define-public crate-bot-api-example-0.1.1 (c (n "bot-api-example") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.6") (d #t) (k 0)) (d (n "actix-http") (r "^3.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.3") (d #t) (k 0)) (d (n "anthill-di") (r "^1.2.4") (d #t) (k 0)) (d (n "anthill-di-derive") (r "^1.2.4") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-lock") (r "^2.6") (d #t) (k 0)) (d (n "async-trait-with-sync") (r "^0.1") (d #t) (k 0)) (d (n "botx-api-framework") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0za7vfqb33hqspbxgd7ywmgmfl09nfphdll023iymaqvjyn4m964")))

