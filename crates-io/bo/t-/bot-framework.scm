(define-module (crates-io bo t- bot-framework) #:use-module (crates-io))

(define-public crate-bot-framework-0.1.0 (c (n "bot-framework") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "telegram-bot") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0mhcyxqp4lq73pj5fv92mm0cwmr1xy518j02plbqw5wk3hx958nx")))

(define-public crate-bot-framework-0.1.1 (c (n "bot-framework") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "telegram-bot") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0ysfs37kw9ib4gwhlb58a02jgzf9j83rpx8511psp5wxmxk8ljhn")))

(define-public crate-bot-framework-0.1.2 (c (n "bot-framework") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "telegram-bot") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "041wf2l71c3wzj7vi8mhk659c1pjlcf324acb5xzpz6d3aw519bv")))

