(define-module (crates-io bo ke bokeh) #:use-module (crates-io))

(define-public crate-bokeh-0.1.0 (c (n "bokeh") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)))) (h "13nayphldk23qwc07raafpy9h8pah35w5ir336j9hr6ijh0ihv08") (f (quote (("default" "image")))) (s 2) (e (quote (("image" "dep:image"))))))

