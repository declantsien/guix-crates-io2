(define-module (crates-io bo id boids) #:use-module (crates-io))

(define-public crate-boids-0.1.0 (c (n "boids") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (f (quote ("serde"))) (d #t) (k 0)) (d (n "puffin") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "puffin") (r "^0.7") (d #t) (k 2)) (d (n "puffin_http") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "raylib") (r "^3.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "045nmpkf7r27sgvklhy0f9780ih07zp82611qrvq6ssaw40kidw8")))

