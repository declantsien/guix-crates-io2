(define-module (crates-io bo id boids-rs) #:use-module (crates-io))

(define-public crate-boids-rs-0.1.0 (c (n "boids-rs") (v "0.1.0") (d (list (d (n "nannou") (r "^0.18") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0rr3bz4n4hzw9yp4wznn31i8k3hwryvyn81fm42pgw2396add3h1")))

(define-public crate-boids-rs-1.0.0 (c (n "boids-rs") (v "1.0.0") (d (list (d (n "nannou") (r "^0.18") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "162m8a84ddm78mir30kp5qjn078rk23hljzmspr46a7qjpwyv76j")))

