(define-module (crates-io bo id boids_rs_bevy) #:use-module (crates-io))

(define-public crate-boids_rs_bevy-0.1.0 (c (n "boids_rs_bevy") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09kp0dsvbwcdc047q6m7x9l3lphwy9x6n8599j95cykhg7mv9x5i")))

