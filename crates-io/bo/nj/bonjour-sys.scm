(define-module (crates-io bo nj bonjour-sys) #:use-module (crates-io))

(define-public crate-bonjour-sys-0.1.0 (c (n "bonjour-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1anmgjsp7zrxphardfs8ranmbvhyx2g074ndfhw98qs0w2lnkdwg")))

(define-public crate-bonjour-sys-0.1.1 (c (n "bonjour-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "0aqlhby7srfvfxs11kgm5bhrhb037jf0z46wzrvm1c8505zlb73l")))

(define-public crate-bonjour-sys-0.2.0 (c (n "bonjour-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "05qslwl2xjv6qyas10kv2k4gh3i3wq6830710p92v600gq9h23hz")))

(define-public crate-bonjour-sys-0.2.1 (c (n "bonjour-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1v3lwr8k7rwwbm1nh1mdmzwj383zdzzvdd1ilz2d1jv4ycp9b4jy")))

(define-public crate-bonjour-sys-0.2.2 (c (n "bonjour-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1jh4aqrfcl67127wrcjhw91hmmj6rrpswbi3akk47p8dsy6bvih9")))

