(define-module (crates-io bo un boundary-api) #:use-module (crates-io))

(define-public crate-boundary-api-0.0.1-alpha.1 (c (n "boundary-api") (v "0.0.1-alpha.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "131nlpzz3d96zzjja9z2kd7llfni21gfjlj9af1599s7vyy5wi57")))

(define-public crate-boundary-api-0.0.1-alpha.2 (c (n "boundary-api") (v "0.0.1-alpha.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "06qdl5f8rvq6xcrr4wcxqc7c87pp0752qsxxvhf6i7y7zra73xv2")))

