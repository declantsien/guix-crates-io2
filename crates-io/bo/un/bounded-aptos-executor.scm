(define-module (crates-io bo un bounded-aptos-executor) #:use-module (crates-io))

(define-public crate-bounded-aptos-executor-0.1.0 (c (n "bounded-aptos-executor") (v "0.1.0") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0wdblpdm2w128bph645p0pjc00mzy98m68c77xh95v4pdylb42f2") (y #t)))

(define-public crate-bounded-aptos-executor-0.1.1 (c (n "bounded-aptos-executor") (v "0.1.1") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "18i0w31qb19j384ys8hr23mwjfp74mxd0msdfqjh1rlk660pbj98") (y #t)))

(define-public crate-bounded-aptos-executor-0.1.2 (c (n "bounded-aptos-executor") (v "0.1.2") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0bmmziwj867yb8bmamr49qa6gszhpz34wcann2kvxpfbgfmg5vd2") (y #t)))

(define-public crate-bounded-aptos-executor-0.1.3 (c (n "bounded-aptos-executor") (v "0.1.3") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "14vimn3zhgc708wllp5061jh7bmgidvdhyh0nlnc2nig4638yanp") (y #t)))

(define-public crate-bounded-aptos-executor-0.1.4 (c (n "bounded-aptos-executor") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0gm5idapymz2z3iw8jjg031nxiz80rly4aq48zfhgalg7qrwn064") (y #t)))

(define-public crate-bounded-aptos-executor-0.1.5 (c (n "bounded-aptos-executor") (v "0.1.5") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1pvz1qxi3r9x3h5dc6aa0s3a6lap50vprgbllwag4f8hz2b0x4l4") (y #t)))

(define-public crate-bounded-aptos-executor-0.1.6 (c (n "bounded-aptos-executor") (v "0.1.6") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "02s2afb52lhkh3qxy7pixpx0gpkxxq3z8mj5ps36vrm67bpx5cl0") (y #t)))

(define-public crate-bounded-aptos-executor-0.1.7 (c (n "bounded-aptos-executor") (v "0.1.7") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "062zih0035iw070aghghb90hsw4j6g7d9p0j5bp11h4ypglnz7y8") (y #t)))

(define-public crate-bounded-aptos-executor-0.2.1 (c (n "bounded-aptos-executor") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0rpxh8z8sziynafdw0zfcsmkl0i656ajd50hpc66kc3wfrb5b0qm") (y #t)))

(define-public crate-bounded-aptos-executor-0.2.2 (c (n "bounded-aptos-executor") (v "0.2.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1gsvpg615i4fnhpc0il9m3hihiym7ps2nsrmbd9vcx587yxdmri7") (y #t)))

(define-public crate-bounded-aptos-executor-0.2.6 (c (n "bounded-aptos-executor") (v "0.2.6") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1jn42c2ixj9z604gi7y74mzh0i2jzhkf16jikhxzkbq5hfvz5c4l") (y #t)))

(define-public crate-bounded-aptos-executor-0.2.7 (c (n "bounded-aptos-executor") (v "0.2.7") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1cxnh0f68fi56ghc8vd5s5019fsvp3zgy7pczv2dr12nq3pkkryc") (y #t)))

