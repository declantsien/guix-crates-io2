(define-module (crates-io bo un boundnum) #:use-module (crates-io))

(define-public crate-boundnum-0.1.0 (c (n "boundnum") (v "0.1.0") (d (list (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "0rdli47s0ghfj85yivp9b4j1s44pcwrh0p3lqbdm5qrq33ws8yxp")))

(define-public crate-boundnum-0.1.1 (c (n "boundnum") (v "0.1.1") (d (list (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "1sb0q3rj4v6n0qh8w1ycm4kd7l1m4n0iag1ivh9lfw6gis7bld3v")))

