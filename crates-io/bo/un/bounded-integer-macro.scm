(define-module (crates-io bo un bounded-integer-macro) #:use-module (crates-io))

(define-public crate-bounded-integer-macro-0.2.0 (c (n "bounded-integer-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.30") (f (quote ("full"))) (d #t) (k 0)))) (h "1sacsvq3vz1v5s98a9w2vi69jn9ncf8qkip6b8kw3ksy5f8l43kb")))

(define-public crate-bounded-integer-macro-0.2.1 (c (n "bounded-integer-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "03vzfcmkk5jp6b4xyng7l4c48pjknb20rxqw095k4fwiz2169wch")))

(define-public crate-bounded-integer-macro-0.2.2 (c (n "bounded-integer-macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "1d21vj6y1vv5valnjs7zj71bpaijsq2xxsq5f1mrb4hy7kphswp6") (f (quote (("serde"))))))

(define-public crate-bounded-integer-macro-0.2.3 (c (n "bounded-integer-macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "108g6lbf2ryjcif3nn2gkx29k11x6bwk90ymbb7dd91c9p7jf3r6") (f (quote (("serde"))))))

(define-public crate-bounded-integer-macro-0.3.0 (c (n "bounded-integer-macro") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.52") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "0vz9b35lv9066inq3yj5s38z93g988qrvqffnj73ppfh95rqmgh9") (f (quote (("step_trait") ("serde"))))))

(define-public crate-bounded-integer-macro-0.4.0 (c (n "bounded-integer-macro") (v "0.4.0") (d (list (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "017ixqfd42vybd8plwhp2yschsr3w3cxfw6lmsfalqiv81r0m645") (f (quote (("step_trait") ("serde"))))))

(define-public crate-bounded-integer-macro-0.5.0 (c (n "bounded-integer-macro") (v "0.5.0") (d (list (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "0p1m8j86pdpv58lvd4vgw7zih37d32si5mqilslxgs41wcralzxf") (f (quote (("generate_tests"))))))

(define-public crate-bounded-integer-macro-0.5.1 (c (n "bounded-integer-macro") (v "0.5.1") (d (list (d (n "num-bigint") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "1bm9y2kfr7j0qn9bw5ymiznml0n5qzh72a7a5kjfpwh9acii31xj") (f (quote (("generate_tests"))))))

(define-public crate-bounded-integer-macro-0.5.2 (c (n "bounded-integer-macro") (v "0.5.2") (d (list (d (n "num-bigint") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "04h4blrh441zpqafwyim69178fzaqnb96sz93j6m4m6kvh3fskx4") (f (quote (("generate_tests"))))))

(define-public crate-bounded-integer-macro-0.5.3 (c (n "bounded-integer-macro") (v "0.5.3") (d (list (d (n "num-bigint") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "10r0x59g5sakinp37vyswixah2dxqwx45zagp5i4b69s2yfr07nr") (f (quote (("generate_tests"))))))

(define-public crate-bounded-integer-macro-0.5.6 (c (n "bounded-integer-macro") (v "0.5.6") (d (list (d (n "num-bigint") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "1fk8f7la4lq4gp0w66jjk9d77bfya0ck8fqqbq06xjfvxhihxr7m") (f (quote (("generate_tests"))))))

