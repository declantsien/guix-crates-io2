(define-module (crates-io bo un bounced) #:use-module (crates-io))

(define-public crate-bounced-0.1.0 (c (n "bounced") (v "0.1.0") (h "1vfw3r85z8cm9i611kirwyv62rcbs5wnlyxx6i28r78qww6l8if5")))

(define-public crate-bounced-0.1.1 (c (n "bounced") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0l3pi1mx17l38xks4kdl7y2vqkkrys6i9223vd0iwkhh4nszb5m2")))

(define-public crate-bounced-0.2.0 (c (n "bounced") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0iaqazcm846mrp8dn57n4rqfrr6gl693w5hq04v1mf099yag3zx5")))

