(define-module (crates-io bo un bound-stl) #:use-module (crates-io))

(define-public crate-bound-stl-0.1.0 (c (n "bound-stl") (v "0.1.0") (h "0aamjkqn3mrvl3yxk7v6vjbba4cijdpi8079fyh3h60a1kqa7dx1") (y #t)))

(define-public crate-bound-stl-0.1.1 (c (n "bound-stl") (v "0.1.1") (h "1s1gmd840s584qcwjkc46cm99kw9cc3g8pbzvyjn3j8fci3rd23c") (y #t)))

(define-public crate-bound-stl-0.1.2 (c (n "bound-stl") (v "0.1.2") (h "0mn3fzrilq6b5dssbbadrlxnv9w4wd51i6l1gaf6r7cbgg278mci")))

