(define-module (crates-io bo un bounds) #:use-module (crates-io))

(define-public crate-bounds-0.1.0 (c (n "bounds") (v "0.1.0") (h "1ws1fvwli7ikw1flkvd99q6y0d8d9yiva7wf2w3mxb7ndw89nf1i")))

(define-public crate-bounds-0.1.1 (c (n "bounds") (v "0.1.1") (h "0dc6jchc5kl3r2v8m2a3vq1iw584aqaxf2cqcjsmkbabx9kkvdaf")))

(define-public crate-bounds-0.2.0 (c (n "bounds") (v "0.2.0") (h "0q46biv2yacdkgbngyi773h6cagyg273m52z4wg8v3lba7lh974j")))

(define-public crate-bounds-0.3.0 (c (n "bounds") (v "0.3.0") (h "1sb3xn5gbfs0pf45n5k1n7ac1hk7qj5lnaqskxcqfh81jmd1wphb")))

(define-public crate-bounds-0.4.0 (c (n "bounds") (v "0.4.0") (h "00nygjj6cwg3qc5k6zr7hq68p1giz0m69jc6adaz074d6lr1ixpl")))

(define-public crate-bounds-0.5.0 (c (n "bounds") (v "0.5.0") (h "0zvampnhvyvnq0ki8f2kf1471kam0j6d125jag7j5hz998zrjaj2")))

(define-public crate-bounds-0.6.2 (c (n "bounds") (v "0.6.2") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0nlq855kfn9ms2n9zh55czrbq433m0w33syd6fnlk9bzxhqqgrc0")))

(define-public crate-bounds-0.7.0 (c (n "bounds") (v "0.7.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0zcby0b89i5nrwkvwhw99vvlp3h7lk513cyylxkms6an0n72kjs2")))

