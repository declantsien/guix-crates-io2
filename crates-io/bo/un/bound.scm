(define-module (crates-io bo un bound) #:use-module (crates-io))

(define-public crate-bound-0.1.0 (c (n "bound") (v "0.1.0") (h "0kxdyvwvjs4qbyn9l2x6w4ni1d830ff6fw94irc0q0920m4p67xa")))

(define-public crate-bound-0.2.0 (c (n "bound") (v "0.2.0") (h "0vd9x6jnsg9rml4iamalr1vbfcwz0rbwzmvwmy6li1a0ipc2jz96")))

(define-public crate-bound-0.2.1 (c (n "bound") (v "0.2.1") (h "1d3kfi02nzgax50dlajihlb646qa5h2sk7n5ibckidakskzgjpdz")))

(define-public crate-bound-0.2.2 (c (n "bound") (v "0.2.2") (h "1i20kc9b0qjff28h2vyz5lmgrjp1ksbi60rv67jfjjpk9dxy3fx1")))

(define-public crate-bound-0.2.3 (c (n "bound") (v "0.2.3") (h "1kfshdp9hy9d16416krc8d9bpzadmi3z8afskwziy3m8xwb84lqr")))

(define-public crate-bound-0.3.0 (c (n "bound") (v "0.3.0") (d (list (d (n "async-lock") (r "^2.5.0") (d #t) (k 2)))) (h "1qx5rnz04gdrn9dn1sbbm7nifrc9rnmj4fqw21zv55pc37j1cfq3")))

(define-public crate-bound-0.3.1 (c (n "bound") (v "0.3.1") (d (list (d (n "async-lock") (r "^2.5.0") (d #t) (k 2)))) (h "0xgwadpfqphrzkbmp0iqnac9j570102l64pqvw5nxhj0phg6vzkz")))

(define-public crate-bound-0.4.1 (c (n "bound") (v "0.4.1") (d (list (d (n "async-lock") (r "^2.6.0") (d #t) (k 2)))) (h "0v9v1ndzylk4yap0c5q8fxm8x6d8w0gb489fimqvds4q899jvhgy")))

(define-public crate-bound-0.5.0 (c (n "bound") (v "0.5.0") (d (list (d (n "async-lock") (r "^2.6.0") (d #t) (k 2)))) (h "042izmcf53zw0w53n7bkmcgffc04f0iwgx4kw2m4mx8nbw4sw8b0")))

