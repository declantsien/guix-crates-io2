(define-module (crates-io bo un bounties_market) #:use-module (crates-io))

(define-public crate-bounties_market-0.1.0 (c (n "bounties_market") (v "0.1.0") (d (list (d (n "codec") (r "^1.3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0-rc5") (k 0)) (d (n "frame-system") (r "^2.0.0-rc5") (k 0)) (d (n "pallet-balances") (r "^2.0.0-rc5") (k 2)) (d (n "sp-core") (r "^2.0.0-rc5") (k 2)) (d (n "sp-io") (r "^2.0.0-rc5") (k 2)) (d (n "sp-runtime") (r "^2.0.0-rc5") (k 0)) (d (n "sp-std") (r "^2.0.0-rc5") (k 0)))) (h "1kcdb4p7p65fq2a8slaf40xgvjnjr15xky9mqw6idgdpfdl6c5m1") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "sp-runtime/std" "sp-std/std") ("default" "std"))))))

