(define-module (crates-io bo un bounded_types) #:use-module (crates-io))

(define-public crate-bounded_types-0.1.0 (c (n "bounded_types") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.13") (f (quote ("constructor"))) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1fsrxfwnz39z61ymjrap2b52skwc450aikwaibmykdgfv3c7d4jh")))

(define-public crate-bounded_types-0.2.0 (c (n "bounded_types") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.13") (f (quote ("constructor"))) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1smhscsq6nc0xy6ybny5vjcy5mwdgfdm26csg8vjqjgh340lkvcz")))

(define-public crate-bounded_types-0.2.1 (c (n "bounded_types") (v "0.2.1") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.13") (f (quote ("constructor"))) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "17alk0x81n64gapad20xgdz2fwynvf9nd91hwbzzjx3jmp20cq44")))

