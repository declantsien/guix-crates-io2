(define-module (crates-io bo un bounded_join_set) #:use-module (crates-io))

(define-public crate-bounded_join_set-0.1.0 (c (n "bounded_join_set") (v "0.1.0") (d (list (d (n "tokio") (r "^1.32") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("macros" "time" "rt-multi-thread"))) (d #t) (k 2)))) (h "0i6nmx22ypxpi32rsrij83nsyv644kv4aw2aanhxv2whbfmm39gr") (r "1.71")))

