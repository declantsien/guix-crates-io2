(define-module (crates-io bo un bounded-spsc-queue) #:use-module (crates-io))

(define-public crate-bounded-spsc-queue-0.0.1 (c (n "bounded-spsc-queue") (v "0.0.1") (d (list (d (n "time") (r "*") (o #t) (d #t) (k 0)))) (h "1wss4cipizar6xhbvh6bpbqi0sprb6rbfd7i4vs8cjdd9j3c5hlw") (f (quote (("nightly") ("default") ("benchmark" "time"))))))

(define-public crate-bounded-spsc-queue-0.1.0 (c (n "bounded-spsc-queue") (v "0.1.0") (h "09b0l672j707pyniw9bk5a4yz9r2j4nh6cwwnz0kjkm1g8idbzbx") (f (quote (("nightly") ("default"))))))

(define-public crate-bounded-spsc-queue-0.1.2 (c (n "bounded-spsc-queue") (v "0.1.2") (h "0wkpfb0cmjk2mbjm36fci1gcqs33yjv7w91f6i98z2lbyvs6fkl6") (f (quote (("nightly") ("default"))))))

(define-public crate-bounded-spsc-queue-0.2.0 (c (n "bounded-spsc-queue") (v "0.2.0") (h "1611wcbwv84ifr71km8fzsfpkxhh036al7dm35capfjw95hna85c") (f (quote (("nightly") ("default"))))))

(define-public crate-bounded-spsc-queue-0.3.0 (c (n "bounded-spsc-queue") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "1brrvraqnhil4j018lva7xhql8inbjnmbyk5yy620pwgylawshvg")))

(define-public crate-bounded-spsc-queue-0.4.0 (c (n "bounded-spsc-queue") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "152i1rvxjw2sgw64q2ndkgxqcv1v6nwvx00fxinwcnwks58cdjg4")))

