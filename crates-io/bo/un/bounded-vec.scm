(define-module (crates-io bo un bounded-vec) #:use-module (crates-io))

(define-public crate-bounded-vec-0.1.0 (c (n "bounded-vec") (v "0.1.0") (h "0fwqbhil7s6dx6ynak0cz2y0fwl11i1p2qns2h0kmd3lghl5fvg9")))

(define-public crate-bounded-vec-0.2.0 (c (n "bounded-vec") (v "0.2.0") (h "1lxy0aqq9pjn9f2kr822l7a4i6cyd1r3vv9ylrablfxqvy65rdf6")))

(define-public crate-bounded-vec-0.3.0 (c (n "bounded-vec") (v "0.3.0") (h "029q4fli21bl53wcb9q6rb3yyx3qqh13md3p6g6mh0sl28p7g68y")))

(define-public crate-bounded-vec-0.4.0 (c (n "bounded-vec") (v "0.4.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "147cnvb2y3hvn6gc8g9fid3nxca3gh492jsj58k6dz75xzzivpdg")))

(define-public crate-bounded-vec-0.5.0 (c (n "bounded-vec") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0718nhiy2vh930gn6g6i0gfhpygzp0xxj2az83z1g559zj1clz5l")))

(define-public crate-bounded-vec-0.6.0 (c (n "bounded-vec") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1diy2m9z1xvddhingrpn1hym0s8nrbk7rfnqdfilv7dzj10bwwik")))

(define-public crate-bounded-vec-0.7.0 (c (n "bounded-vec") (v "0.7.0") (d (list (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hx9h3vphvfm5r2wrgmx1wka56qlz7iln9yxfh44sn2fsgbgv02c") (f (quote (("arbitrary" "proptest" "proptest-derive"))))))

(define-public crate-bounded-vec-0.7.1 (c (n "bounded-vec") (v "0.7.1") (d (list (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kk34q4c5r9kijq17pz024vw57lc4chwycy44c9lnfpnrd44llv8") (f (quote (("arbitrary" "proptest"))))))

