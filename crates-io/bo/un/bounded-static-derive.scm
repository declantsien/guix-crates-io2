(define-module (crates-io bo un bounded-static-derive) #:use-module (crates-io))

(define-public crate-bounded-static-derive-0.1.0 (c (n "bounded-static-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1vfslfvql740f5wbsrjypwm3s19vsgypvgz7bvcmr78vszby7z31") (r "1.58.1")))

(define-public crate-bounded-static-derive-0.2.0 (c (n "bounded-static-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "08hw7sgbk9r9wrajzjp33cj3v6v3abdz2x5g8ya7a4d8zdxc8cmy") (r "1.58.1")))

(define-public crate-bounded-static-derive-0.2.1 (c (n "bounded-static-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "05sbslvh68dzaywc1crcinkb0nrwgs0d3valxb4asq1vwmsdcla0") (r "1.58.1")))

(define-public crate-bounded-static-derive-0.3.0 (c (n "bounded-static-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "00md21d2vvzs89413q7rfpia1qmz0wmfwqmcsvmjzvab96ry7h1g") (r "1.58.1")))

(define-public crate-bounded-static-derive-0.4.0 (c (n "bounded-static-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0x2sd0qa0nd2cgs0dy7xb75d7z12z396xzfr760cccbfppx4xxlb") (r "1.58.1")))

(define-public crate-bounded-static-derive-0.5.0 (c (n "bounded-static-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0sxx57n0m3hnlhj9pbcnp7fci11cwrxdhlr7ipck3xjv6m3x43gi") (r "1.60.0")))

(define-public crate-bounded-static-derive-0.6.0 (c (n "bounded-static-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0q6m8jb9vndqn75ic4ywgj8vhqb4kbj4wqqfcjsh73ci11wd1lhr") (r "1.60.0")))

(define-public crate-bounded-static-derive-0.7.0 (c (n "bounded-static-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0h9n5nbcfa6wal7xhsw0fckgk7zc54zf91xdqwqyc9ry3j4hj6a9") (r "1.61.0")))

