(define-module (crates-io bo un bounding-space) #:use-module (crates-io))

(define-public crate-bounding-space-0.1.0 (c (n "bounding-space") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("std"))) (k 2)))) (h "1pphpqizcsjv3dbgz2f0m289ygjhihc94nj6aci9dppc4yw2fa5a")))

(define-public crate-bounding-space-0.2.0 (c (n "bounding-space") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("std"))) (k 2)))) (h "0xcx91499hcbdjx3lz8p7zml0i7rl4mijm5yr7qv0fxqg1l01vs5")))

