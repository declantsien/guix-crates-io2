(define-module (crates-io bo un bounded-executor) #:use-module (crates-io))

(define-public crate-bounded-executor-0.1.0 (c (n "bounded-executor") (v "0.1.0") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1qcd7vxvckzngacad3vibfhrch7x1xfdw8w9ibyvnzcihp7f3b8i") (y #t)))

