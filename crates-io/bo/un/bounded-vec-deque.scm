(define-module (crates-io bo un bounded-vec-deque) #:use-module (crates-io))

(define-public crate-bounded-vec-deque-0.1.0 (c (n "bounded-vec-deque") (v "0.1.0") (h "03i8fq348rznb9gg9jjlb951xyzb35wd47lqq44giam8qnm5qhkw") (f (quote (("resize_with") ("fused") ("default" "fused"))))))

(define-public crate-bounded-vec-deque-0.1.1 (c (n "bounded-vec-deque") (v "0.1.1") (h "1k8cq5yig9xjs7bwnd9nn6qhxkng6pyb7wgmk1l5jv67mxcba992") (f (quote (("resize_with") ("fused") ("default" "fused"))))))

