(define-module (crates-io bo un bounce-macros) #:use-module (crates-io))

(define-public crate-bounce-macros-0.1.0 (c (n "bounce-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fawjdvic5nxgsf297syhk3y14mqid8xilgjv39nffaxl9bzn70i")))

(define-public crate-bounce-macros-0.1.3 (c (n "bounce-macros") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a89yf5dpjjcnndqclazgryxj7bnw7629dmc9sx9hgd8vb7r0lg5")))

(define-public crate-bounce-macros-0.2.0 (c (n "bounce-macros") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r2h14bqx2z8xhcy7lr28myh5mpkjwnqkc0j9k5qf4p0kdfw5kk0")))

(define-public crate-bounce-macros-0.3.0 (c (n "bounce-macros") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0iw5wgbyb9amjkhhcnqpab1cpqj9xwxgm0h1jzhzsn0vgw5r1m3d")))

(define-public crate-bounce-macros-0.4.0 (c (n "bounce-macros") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pamq469rs0ihfwpf0kvy81mgd77m5vzw5gahc18jrg71bkiz6gf")))

(define-public crate-bounce-macros-0.5.0 (c (n "bounce-macros") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pnm93dy4298sfbr06saskmfjq5rnl5mvhd3iflcjbp1m7w7mya6")))

(define-public crate-bounce-macros-0.6.0 (c (n "bounce-macros") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mqf87cbmywz5cbkx59kg70j1h17ral4jvmz2kq465nyg8rvdixb")))

(define-public crate-bounce-macros-0.6.1 (c (n "bounce-macros") (v "0.6.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rjpzzn9am8x9d3svwnd6ggi9dz0zi3wy30ypai1cdsn83z2yidi")))

(define-public crate-bounce-macros-0.7.0 (c (n "bounce-macros") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dny2pc320a7j56myxjaaz3pkqqrnzpaidlbz4y8c4nhsh3vg2x7")))

(define-public crate-bounce-macros-0.8.0 (c (n "bounce-macros") (v "0.8.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w1x2gy841nnyxipjs5rz0jy0mxy57k5p9x5qwrzjrv2gjzaq9y4") (r "1.64")))

(define-public crate-bounce-macros-0.9.0 (c (n "bounce-macros") (v "0.9.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ql6cxix2jfy52wn2zycqi3ff2sg079vzyqz8979nivwzi0l2n6x") (r "1.64")))

