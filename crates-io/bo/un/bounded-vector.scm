(define-module (crates-io bo un bounded-vector) #:use-module (crates-io))

(define-public crate-bounded-vector-0.1.0 (c (n "bounded-vector") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "160zqg2whlgh4g8s0y8gaq6ndx8irwpbl0iyk33sc09br0aqmykr")))

(define-public crate-bounded-vector-0.1.1 (c (n "bounded-vector") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1j4ab1scyb03nr6544ann90mcrbx1hw5d37sxqpz8gy424jrj7sy")))

(define-public crate-bounded-vector-0.1.2 (c (n "bounded-vector") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1yv0217f6345wwxv24x828jz3c2w0jfwa12ad3q31rrh4svkly9c")))

(define-public crate-bounded-vector-0.1.3 (c (n "bounded-vector") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0qagfz52wdlyb7933cqqljmmksq2wqfn0m516cvbvys8zkhg7vvw")))

(define-public crate-bounded-vector-0.2.0 (c (n "bounded-vector") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0a7bb4vb3pfd7xnhg147vfbm4fs1bwi58zfcqzfq7sbp5p7p2wx8")))

(define-public crate-bounded-vector-0.2.1 (c (n "bounded-vector") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "18qn53znf5scl03wfg39r0gr2f7hbz1x5vmbkdw0687mx0sq4xaj")))

