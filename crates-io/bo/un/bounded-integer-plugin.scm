(define-module (crates-io bo un bounded-integer-plugin) #:use-module (crates-io))

(define-public crate-bounded-integer-plugin-0.1.0 (c (n "bounded-integer-plugin") (v "0.1.0") (d (list (d (n "bounded-integer") (r "^0.1.0") (d #t) (k 2)))) (h "198ca5jrql2kzff0fs61h6z4ndjvkf9vakqyfqqyy0f0m2wga94b")))

(define-public crate-bounded-integer-plugin-0.1.1 (c (n "bounded-integer-plugin") (v "0.1.1") (d (list (d (n "bounded-integer") (r "^0.1.0") (d #t) (k 2)))) (h "1sfvdrcfj86mr5k9bdhcdmbffkbn6wlhyzs6qkgmgyczk81ng5fd")))

