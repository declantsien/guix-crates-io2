(define-module (crates-io bo nz bonzai) #:use-module (crates-io))

(define-public crate-bonzai-0.1.0 (c (n "bonzai") (v "0.1.0") (h "1ypj1y3c9ca3m1h0rg4blasvvw0i3frii7wlhll8vrjnj444s1pg")))

(define-public crate-bonzai-0.1.1 (c (n "bonzai") (v "0.1.1") (h "1zc7vs4z03zbr5msm36cnldwnwsmiawr5fcs8kn0xmb9xay96ia1")))

(define-public crate-bonzai-0.2.0 (c (n "bonzai") (v "0.2.0") (h "1isyjb9l5mammns2wylff1mbr0grni72vzar2r8kcwmhsxmr6fkd")))

(define-public crate-bonzai-0.2.1 (c (n "bonzai") (v "0.2.1") (h "0pnp78k75n5bq80c57nm5wi69nds81rp6026f1wba9hkf6ap4q9n")))

