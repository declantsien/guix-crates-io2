(define-module (crates-io bo op boop) #:use-module (crates-io))

(define-public crate-boop-0.0.1 (c (n "boop") (v "0.0.1") (h "11p686rlw088lnylms95i6gg19vh3dawphjw52jk6iijb0mq0sf4")))

(define-public crate-boop-0.0.2 (c (n "boop") (v "0.0.2") (h "16kam5ig803wvy6w8xwsrj41bb9aa7jm9fznxl8f5zhykqgrbdd7")))

