(define-module (crates-io bo op boop-ping) #:use-module (crates-io))

(define-public crate-boop-ping-0.1.0 (c (n "boop-ping") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "oping") (r "^0.4.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6") (d #t) (k 0)))) (h "11had885kad5m8wj5h1dw2mvh4pasr0xs9gvhxs5v017wnlzw4i6")))

