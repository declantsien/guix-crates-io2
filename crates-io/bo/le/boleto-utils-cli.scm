(define-module (crates-io bo le boleto-utils-cli) #:use-module (crates-io))

(define-public crate-boleto-utils-cli-0.1.1 (c (n "boleto-utils-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "boleto-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "04m029g7af07y73qx6i3lmjj0c40p2imnyxrygy1jwwk3magw8yi")))

(define-public crate-boleto-utils-cli-0.1.2 (c (n "boleto-utils-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "boleto-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "17djcsvbl8vgx6h98nss1ivr0b0zbxbhz57l3f41crm65s6n7xvv")))

(define-public crate-boleto-utils-cli-0.1.3 (c (n "boleto-utils-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "boleto-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "01p4zwv8357n4k65yhx2098rhqa8a0djfqv2lj07vrhv6pcc0nxm")))

