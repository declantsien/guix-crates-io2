(define-module (crates-io bo le bolero-honggfuzz) #:use-module (crates-io))

(define-public crate-bolero-honggfuzz-0.2.0 (c (n "bolero-honggfuzz") (v "0.2.0") (h "171yi0clmhnh898h4vpr8v5f784qdznhby0qmbwbqw1q075qwyjp") (f (quote (("bin"))))))

(define-public crate-bolero-honggfuzz-0.3.0 (c (n "bolero-honggfuzz") (v "0.3.0") (d (list (d (n "bolero-generator") (r "^0.3") (k 0)))) (h "1wb1dkqisd1iwc50vw7havyg3n12jyrayk24v3ly24i6sfgak6hw") (f (quote (("bin"))))))

(define-public crate-bolero-honggfuzz-0.4.0 (c (n "bolero-honggfuzz") (v "0.4.0") (d (list (d (n "bolero-engine") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1hinqpr88v546yzncia0465fvp9ymjglnlf7ldyxqz8m2x5svb35") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-honggfuzz-0.4.1 (c (n "bolero-honggfuzz") (v "0.4.1") (d (list (d (n "bolero-engine") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1gzhjdmm7xvr0x7vz3mywykvv3a8q05g7l8104n3pxxn2ncyq9ps") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-honggfuzz-0.5.0 (c (n "bolero-honggfuzz") (v "0.5.0") (d (list (d (n "bolero-engine") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0c1p8bhrr9kmd3dq3d5fff54dl04fffwf7mxlsz79yaygdr9sgrz") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-honggfuzz-0.6.0 (c (n "bolero-honggfuzz") (v "0.6.0") (d (list (d (n "bolero-engine") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0adxl1zyk8dn45jpimhdjic6lf92djy0kcd5l068njzgsjmx3lyq") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-honggfuzz-0.6.2 (c (n "bolero-honggfuzz") (v "0.6.2") (d (list (d (n "bolero-engine") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0jxvcah13qy5f8h43a6vljdv1091fhx12q01ifaqhdd21sjqghbx") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-honggfuzz-0.7.0 (c (n "bolero-honggfuzz") (v "0.7.0") (d (list (d (n "bolero-engine") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0lwvvhc2hn32f993lbm9c6jc9z62bfy90gkbl76453if10586g8k") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-honggfuzz-0.8.0 (c (n "bolero-honggfuzz") (v "0.8.0") (d (list (d (n "bolero-engine") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0maklr6ixmhwacdgk2qafi4qy5h770ihbf5kp5c26rckipxa75kr") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-honggfuzz-0.9.0 (c (n "bolero-honggfuzz") (v "0.9.0") (d (list (d (n "bolero-engine") (r "^0.9") (o #t) (d #t) (k 0)))) (h "180xlj4brm3ydak1bidjipbcr1icfi1cjvdgw3vp1cfy7304kdfs") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-honggfuzz-0.10.0 (c (n "bolero-honggfuzz") (v "0.10.0") (d (list (d (n "bolero-engine") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1qzqdzqcfh3hsrdnzrra29q7qa5djddn8zknm0h3c9m7n4fmhy5z") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

