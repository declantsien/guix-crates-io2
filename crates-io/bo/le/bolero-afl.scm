(define-module (crates-io bo le bolero-afl) #:use-module (crates-io))

(define-public crate-bolero-afl-0.1.0 (c (n "bolero-afl") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sh2q03daqrgrvq80s6jz5nbz2qbafil17d4sqnc89ah00s34r3z") (f (quote (("bin"))))))

(define-public crate-bolero-afl-0.1.1 (c (n "bolero-afl") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1dh9913gc3bwbgbzr0savd9zwhy8pi11x64yvb8vi5svk6wavi2y") (f (quote (("bin"))))))

(define-public crate-bolero-afl-0.2.0 (c (n "bolero-afl") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1fjpppy3k7pbca7l97jvafcr4a8f1mgrbl26bdij8a5s30yrsvnm") (f (quote (("bin"))))))

(define-public crate-bolero-afl-0.3.0 (c (n "bolero-afl") (v "0.3.0") (d (list (d (n "bolero-generator") (r "^0.3") (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1zh66zg2i0zzrq3zmwrjlacf4csycazypvw9yi349vpjxrgsafy4") (f (quote (("bin"))))))

(define-public crate-bolero-afl-0.4.0 (c (n "bolero-afl") (v "0.4.0") (d (list (d (n "bolero-engine") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0srnywfm3s61axi0vf9j1lp8f9rvi7jbks13wsj4529axsgpnc24") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-afl-0.5.0 (c (n "bolero-afl") (v "0.5.0") (d (list (d (n "bolero-engine") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0qv1xydlw3p12p5n0s4capllqng8iz4qqhi0x6ix7j855rfq3agb") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-afl-0.6.0 (c (n "bolero-afl") (v "0.6.0") (d (list (d (n "bolero-engine") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wmzn425kbi31lfacz5r33i3pzlr7c3r5yj7v714blzalhwipml4") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-afl-0.6.2 (c (n "bolero-afl") (v "0.6.2") (d (list (d (n "bolero-engine") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "06anvjc4h2anlgrxn1nixi022mfk566qycc5wa28qmfcyfaja35x") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-afl-0.7.0 (c (n "bolero-afl") (v "0.7.0") (d (list (d (n "bolero-engine") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1zdvwlvcnxi72n2ab0zz3fw96iwam0m1aj9bql333bvfdl0xxjm6") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-afl-0.8.0 (c (n "bolero-afl") (v "0.8.0") (d (list (d (n "bolero-engine") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "12bx1yyd6rbj3pzrhxsj980m218h9bg7hyzijgp5v1ka3csccfwp") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-afl-0.9.0 (c (n "bolero-afl") (v "0.9.0") (d (list (d (n "bolero-engine") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1c5kqmbcdhfsn7cy9fqfdbgchc8c8xafs1a2669va6aj7dsh3443") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-afl-0.10.0 (c (n "bolero-afl") (v "0.10.0") (d (list (d (n "bolero-engine") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ra0na9rcvg197dj923470jn3w8zzw4xm1sjn1dl49qmvq2lzcz1") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

