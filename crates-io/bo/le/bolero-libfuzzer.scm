(define-module (crates-io bo le bolero-libfuzzer) #:use-module (crates-io))

(define-public crate-bolero-libfuzzer-0.1.0 (c (n "bolero-libfuzzer") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "07h9cb2hn19icwgxdh7wfvxlhi8xl1zyf2l73m8r1qbb7q5l022v")))

(define-public crate-bolero-libfuzzer-0.1.1 (c (n "bolero-libfuzzer") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hhvbklr70hm57d5vqwyw5fnym7p448xwh0wvkzsfac44msg2nim")))

(define-public crate-bolero-libfuzzer-0.2.0 (c (n "bolero-libfuzzer") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1c0l8akvq9fqmngqd117fpgf34115nxdamwasjixm5mqn61aphyj")))

(define-public crate-bolero-libfuzzer-0.3.0 (c (n "bolero-libfuzzer") (v "0.3.0") (d (list (d (n "bolero-generator") (r "^0.3") (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1xvx54l0im7j630rhdxs6j5wpx25jyn36c3r03dpkkdcmbb7x3gh")))

(define-public crate-bolero-libfuzzer-0.4.0 (c (n "bolero-libfuzzer") (v "0.4.0") (d (list (d (n "bolero-engine") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0qjwzhc8n47pyipgvh63h5v7k9dpgf9c3285j07529fwz1jbmwbq") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-libfuzzer-0.5.0 (c (n "bolero-libfuzzer") (v "0.5.0") (d (list (d (n "bolero-engine") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "160pxmsa9k2l0h49w4z0qvqmndkrd73idxyapnzf889025c72amn") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-libfuzzer-0.6.0 (c (n "bolero-libfuzzer") (v "0.6.0") (d (list (d (n "bolero-engine") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0dy25xhh4nf4y5rchcp49rrfjz0ik0zgr5g9y99hsrhyla0sl347") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-libfuzzer-0.6.2 (c (n "bolero-libfuzzer") (v "0.6.2") (d (list (d (n "bolero-engine") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1057fm1jhhfbn6w27ixlzmzj5kbisinvqbwqxzifkgbqx5r4cfrk") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin")))) (y #t)))

(define-public crate-bolero-libfuzzer-0.6.3 (c (n "bolero-libfuzzer") (v "0.6.3") (d (list (d (n "bolero-engine") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "03w6hky39qa53qigpdp7xi976rab6972yna14krrzbmbzmrbylg7") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin")))) (y #t)))

(define-public crate-bolero-libfuzzer-0.6.4 (c (n "bolero-libfuzzer") (v "0.6.4") (d (list (d (n "bolero-engine") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0nqsrqfrpqji8xdnhmx9xqi8qcxq1mp66km5yrqhis0n9gr38xvd") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-libfuzzer-0.7.0 (c (n "bolero-libfuzzer") (v "0.7.0") (d (list (d (n "bolero-engine") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0lsc5k250k3c8i9ip7wb7bi8bfmm2ciq5yaadhvscmavqnn3f716") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-libfuzzer-0.8.0 (c (n "bolero-libfuzzer") (v "0.8.0") (d (list (d (n "bolero-engine") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "162w8sw5dzlrlksy1dbjiirzp73hgmdg25090bch6ixq25s59ifd") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-libfuzzer-0.9.0 (c (n "bolero-libfuzzer") (v "0.9.0") (d (list (d (n "bolero-engine") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1zfc4am2wx6dmwq55why0131dr1xng5h0g5k82d5y5r4cvk3g3i5") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-libfuzzer-0.10.0 (c (n "bolero-libfuzzer") (v "0.10.0") (d (list (d (n "bolero-engine") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0nscj1rjs77azrcj1skgz6fx3khh8yfyakcr272bk2gcwdpgchmv") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

