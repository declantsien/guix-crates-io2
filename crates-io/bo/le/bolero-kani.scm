(define-module (crates-io bo le bolero-kani) #:use-module (crates-io))

(define-public crate-bolero-kani-0.7.0 (c (n "bolero-kani") (v "0.7.0") (d (list (d (n "bolero-engine") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0my1v5saxg10r3p4gc43s1kw0k8dh9jh0sf40zpq57r7a3ffc0mn") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-kani-0.8.0 (c (n "bolero-kani") (v "0.8.0") (d (list (d (n "bolero-engine") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0kmpwfi9jvgfw155s2jsx2x7dypb1wxzj7ivf0ncz8gz7ycpjs10") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-kani-0.9.0 (c (n "bolero-kani") (v "0.9.0") (d (list (d (n "bolero-engine") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1hwfv14qq7dbdaykfr0h4j6rlbm2lvr9xlbyn9vrzgwgf8gdpy8y") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

(define-public crate-bolero-kani-0.10.0 (c (n "bolero-kani") (v "0.10.0") (d (list (d (n "bolero-engine") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1jqrizqx7j53a7l7vxnl1k0rgsq8a41xnw769jpga5x6fb1cwm8f") (f (quote (("lib" "bolero-engine") ("default" "lib") ("bin"))))))

