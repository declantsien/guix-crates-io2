(define-module (crates-io bo le bolero-generator-derive) #:use-module (crates-io))

(define-public crate-bolero-generator-derive-0.3.0 (c (n "bolero-generator-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zbziy6qwm15mrfali7wnr96s4w85a3nywcnwd5xq2i8ffljk66m")))

(define-public crate-bolero-generator-derive-0.4.0 (c (n "bolero-generator-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06s8ll6b23nhhyh5qhida1mvkhgbpzin2fcpk07an8nip3lqwgl4")))

(define-public crate-bolero-generator-derive-0.5.0 (c (n "bolero-generator-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d8dj0jf6kv9zqbl6fc1dksj7crgm7ln6fdn21rm85x5b9wiaqd3")))

(define-public crate-bolero-generator-derive-0.5.2 (c (n "bolero-generator-derive") (v "0.5.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0620hgrrx1l6p8qgj9w3dkfbfwchf31iyqf6z0qajz6ibj2vp6nn")))

(define-public crate-bolero-generator-derive-0.6.0 (c (n "bolero-generator-derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g5vwi2nlwxf39s3h99yqnjjg73zjrvdbnk9iffwv2xk19x20p7y")))

(define-public crate-bolero-generator-derive-0.6.1 (c (n "bolero-generator-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ps2s01jr09zlwjvjzvl6d1qs7jh64jry14xfvpw9q84fd6qk4wr")))

(define-public crate-bolero-generator-derive-0.6.2 (c (n "bolero-generator-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yf6h804sy8f787mdga8xi8pap5w4zswsw7l4is59zr7aqzz0bis")))

(define-public crate-bolero-generator-derive-0.7.0 (c (n "bolero-generator-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vjcagrlmv87rgp3c7yl7yn9c1jj81fn6l12n99h31vd2d7l0zyc")))

(define-public crate-bolero-generator-derive-0.8.0 (c (n "bolero-generator-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hs9qbva0r8c53xb83cc3vvv4vbb3j7vzhm4kfhd2ylnl315fg1v")))

(define-public crate-bolero-generator-derive-0.9.0 (c (n "bolero-generator-derive") (v "0.9.0") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fbxncajw2j90lmsjxmj2hi19n8prgwnfvdmv7ywr456ys0zkymy")))

(define-public crate-bolero-generator-derive-0.9.1 (c (n "bolero-generator-derive") (v "0.9.1") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18zhhhsds0jj88jvm5ys0s2f3zlqgippq20596m86db4n7mp8bvm")))

(define-public crate-bolero-generator-derive-0.9.2 (c (n "bolero-generator-derive") (v "0.9.2") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q6cm2nwf3pi6lpip11n416yz8x6ysmcdhw5rlr2ws6fnph4al0b")))

(define-public crate-bolero-generator-derive-0.10.0 (c (n "bolero-generator-derive") (v "0.10.0") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rs4m7jbymd3wpqyxk8c15pvfjzw9180455agx94ijwwl7ypnfak")))

