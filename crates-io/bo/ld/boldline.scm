(define-module (crates-io bo ld boldline) #:use-module (crates-io))

(define-public crate-boldline-0.1.0 (c (n "boldline") (v "0.1.0") (d (list (d (n "clap") (r "^2.11.3") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1lq9v06j5gl4x776bxj9hfcc0sfna3vhaknxaqz6d5g8mq4hb4wz")))

(define-public crate-boldline-0.1.1 (c (n "boldline") (v "0.1.1") (d (list (d (n "clap") (r "^2.11.3") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1bcg8njpb6f7alkgki2cngcna3z17d56cld6lshzkic6vg456glv")))

