(define-module (crates-io bo fh bofh) #:use-module (crates-io))

(define-public crate-bofh-0.0.0 (c (n "bofh") (v "0.0.0") (h "0cards4w1zpza4lr7ca3aqr1fw7m65j55r3w4i9rnghqsj96dgk7")))

(define-public crate-bofh-0.0.1 (c (n "bofh") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15") (d #t) (k 0)))) (h "1lksv1p2lj46lxyh5c87gkbg2g0qhlh5q0xh3il1nc7l6fclijxg")))

(define-public crate-bofh-0.0.2 (c (n "bofh") (v "0.0.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rpassword") (r "^6.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15") (d #t) (k 0)))) (h "1hmviy3y8rqvwxqj9jz22ypdjwgvpsl6ambgns3y226sap9m49aj")))

(define-public crate-bofh-0.0.3 (c (n "bofh") (v "0.0.3") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rpassword") (r "^6.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15") (d #t) (k 0)))) (h "0x9kxgr9z1cmj2vqmnypsy3rn6wcraih7sk9zy2ws5yz9fcraxpa")))

(define-public crate-bofh-0.0.4 (c (n "bofh") (v "0.0.4") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rpassword") (r "^6.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15") (d #t) (k 0)))) (h "0r6l19dcr7057gz4090s6wwwciigayx2z22as6cpqhd4qwx12bji")))

(define-public crate-bofh-0.0.5 (c (n "bofh") (v "0.0.5") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rpassword") (r "^6.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15") (d #t) (k 0)))) (h "1nj86rvqq6l7ifdxni8qflf34rxf7z84wgx23nn7l7s83s05kjr6")))

(define-public crate-bofh-0.0.6 (c (n "bofh") (v "0.0.6") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rpassword") (r "^6.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15") (d #t) (k 0)))) (h "1zjraszxabmdggz67wa8xb0i107dwl014v6vw36c7pbn321bm908")))

(define-public crate-bofh-0.0.7 (c (n "bofh") (v "0.0.7") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.0") (d #t) (k 0)) (d (n "rustyline") (r "^10") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15") (d #t) (k 0)))) (h "0hsc3fdk262if452m1ai86msnw9mxpn082g33kdr88026svh2p93")))

(define-public crate-bofh-0.0.8 (c (n "bofh") (v "0.0.8") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.0") (d #t) (k 0)) (d (n "rustyline") (r "^10") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15") (d #t) (k 0)))) (h "0a6fdr2gqqqkk79fisi14aj69hjwpsnzc4z692dk95vykjlp7zb0")))

(define-public crate-bofh-0.0.9 (c (n "bofh") (v "0.0.9") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.0") (d #t) (k 0)) (d (n "rustyline") (r "^13") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15") (d #t) (k 0)))) (h "04kryq18yvdl2r5h892dz51ggxllsmvfiqq5njkkcya07m1cd95q")))

