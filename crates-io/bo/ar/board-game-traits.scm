(define-module (crates-io bo ar board-game-traits) #:use-module (crates-io))

(define-public crate-board-game-traits-0.1.0 (c (n "board-game-traits") (v "0.1.0") (h "0ldya9fi4ik028iwcinzz5g8r5yy38qwsbfwnan84l07dj0m9dsw")))

(define-public crate-board-game-traits-0.2.0 (c (n "board-game-traits") (v "0.2.0") (h "1kyw8il8abryd0yz5vkn5sp2k4hr9cagf44j2nswkf4sk7wj5jjp")))

(define-public crate-board-game-traits-0.2.1 (c (n "board-game-traits") (v "0.2.1") (h "0bmi70cpm0l55n46cdylyqnhr5mk60kqfl4kxh5fwv3lm1sjh51g")))

(define-public crate-board-game-traits-0.3.0 (c (n "board-game-traits") (v "0.3.0") (h "08ww0d7ppm07wfqxkmzddql7878hqdls0fb23j7mbvi4cg3hy31z")))

(define-public crate-board-game-traits-0.4.0 (c (n "board-game-traits") (v "0.4.0") (h "16nbls8n6qpyn1ff740xxp2k5l8lmxafxy6zy45z4lxpd3hq9mp1")))

