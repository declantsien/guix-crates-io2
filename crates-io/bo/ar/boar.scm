(define-module (crates-io bo ar boar) #:use-module (crates-io))

(define-public crate-boar-0.1.0-dev (c (n "boar") (v "0.1.0-dev") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 2)))) (h "0nlbjj9lkr9si6phf8l8p9sf9s05fnqpcyvln83a5c8fcjy1qsbl") (f (quote (("unstable"))))))

(define-public crate-boar-0.1.0 (c (n "boar") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 2)))) (h "1d3anfjcc19l0b1cf27if0pywasbyp715a3qjarhz82vckn2zm11") (f (quote (("unstable"))))))

(define-public crate-boar-0.1.1 (c (n "boar") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 2)))) (h "1aphz4y0pk08x2zkh0q6iq51d6rrnsy8q37h8vy41sm4nhjmf8sk") (f (quote (("unstable"))))))

(define-public crate-boar-0.1.2 (c (n "boar") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 2)))) (h "0ll3vgg5jv6mnim6snzvg2zc8qgmd3gnfa6k5p2lcq2zgpan8q74") (f (quote (("unstable"))))))

(define-public crate-boar-0.2.0 (c (n "boar") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)))) (h "0mc4zkw2y4k3pf9vahf94k62g5r51y6vz9wp2l7m2l31caa4kmlq") (f (quote (("unstable"))))))

(define-public crate-boar-0.2.1 (c (n "boar") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)))) (h "01amxfzip72hl72p5d3kchin4vx7dqdf0hpr0gki163c9x34gwpg") (f (quote (("unstable"))))))

(define-public crate-boar-0.2.2 (c (n "boar") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)))) (h "0x868kp6gfizm5bn8rvv3znkglmazasncky7dhrrhplr9xhfj1gy") (f (quote (("unstable"))))))

(define-public crate-boar-0.2.3 (c (n "boar") (v "0.2.3") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)))) (h "1605jvgaw80hq0j3x0ixbjnfxqbrwkcv3plwgiy55aq9xgyd2chi") (f (quote (("unstable"))))))

(define-public crate-boar-0.2.4 (c (n "boar") (v "0.2.4") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)))) (h "0wd3f9z6zwnfddnid1vjlw143c0iwagyslim3bkiaj47fdjz0ys5") (f (quote (("unstable"))))))

