(define-module (crates-io bo ar boardswarm-protocol) #:use-module (crates-io))

(define-public crate-boardswarm-protocol-0.0.1 (c (n "boardswarm-protocol") (v "0.0.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-wkt-build") (r "^0.5.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "18sw2vcxp4cps5c7yv88dyix0ia2fq0kcz4mjxrwzq1i2gl53lyg")))

