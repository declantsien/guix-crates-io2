(define-module (crates-io bo ar board) #:use-module (crates-io))

(define-public crate-board-0.1.0 (c (n "board") (v "0.1.0") (h "0cl00ysvdrsqn678nrfp03n0ch75x8xdyl9g952a7ilgbqacjnx6")))

(define-public crate-board-0.1.1 (c (n "board") (v "0.1.1") (h "14b52w815rxw8ds0z2s0fdwrc02q3k9053dnjxfy2nklph7hkaq9")))

(define-public crate-board-0.2.0 (c (n "board") (v "0.2.0") (h "14h4gdfqp0imnpzx8jxgc15c3q9kqyaplc6p341njh6zjc62x9va")))

(define-public crate-board-0.2.1 (c (n "board") (v "0.2.1") (h "054zbxqg4hhmd35398cnlan9078spzkj6i2y0b798fl9k4c942w7")))

(define-public crate-board-0.2.2 (c (n "board") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "016h4rdgjjvvmz79yacdwh9xkx7kvbgsf65njidisnh7hzfnazc9")))

