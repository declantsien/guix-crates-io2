(define-module (crates-io bo ar board-em-api) #:use-module (crates-io))

(define-public crate-board-em-api-0.1.0 (c (n "board-em-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z1jjs1mhgmxqwv71sxaa9jar1bg21if138n2am2jv7kg1g2jq2n")))

