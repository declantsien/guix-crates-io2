(define-module (crates-io bo ar board-game-geom) #:use-module (crates-io))

(define-public crate-board-game-geom-0.1.0 (c (n "board-game-geom") (v "0.1.0") (h "0839ynjzjhc95a29g52jirmzav9bg9wd1p8fljjjxhfd0s86xk1f")))

(define-public crate-board-game-geom-0.1.1 (c (n "board-game-geom") (v "0.1.1") (h "0cp7yai53a299n5854j2f523ri44vn9gyh4flrvzy9bjlvfcy7yc")))

(define-public crate-board-game-geom-0.1.2 (c (n "board-game-geom") (v "0.1.2") (h "0z0kaqnnpwp544jbl9389bqjdbcj69r2n35r60d44kaslc0lwiks")))

(define-public crate-board-game-geom-0.2.0 (c (n "board-game-geom") (v "0.2.0") (h "0dbpvk8lq4rzij5vz8vs6cs0wchl6rbmgx48lrl5f4ifhavscq8x")))

(define-public crate-board-game-geom-0.3.0 (c (n "board-game-geom") (v "0.3.0") (h "1x0waa7ljgyyik4w7mxr8g1alchf7rmbzaxi2bz2h06yb6qf5vd5") (r "1.56.1")))

