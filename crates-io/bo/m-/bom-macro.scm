(define-module (crates-io bo m- bom-macro) #:use-module (crates-io))

(define-public crate-bom-macro-0.1.0 (c (n "bom-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0c5faypdidj07cxf304razvqnv2ypjd8x7ciihannvb3rqyjsj6s") (y #t)))

