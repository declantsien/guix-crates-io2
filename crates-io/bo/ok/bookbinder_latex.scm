(define-module (crates-io bo ok bookbinder_latex) #:use-module (crates-io))

(define-public crate-bookbinder_latex-0.1.0 (c (n "bookbinder_latex") (v "0.1.0") (d (list (d (n "bookbinder_ast") (r "^0.1.0") (d #t) (k 0)) (d (n "bookbinder_common") (r "^0.1.0") (d #t) (k 0)) (d (n "extended_pulldown") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "temp_file_name") (r "^0.1.0") (d #t) (k 0)))) (h "1v5i888iyr7jwhhgr2z7b12amilbj2xpv1rki2wsqbpmprnhlj9s")))

(define-public crate-bookbinder_latex-0.1.1 (c (n "bookbinder_latex") (v "0.1.1") (d (list (d (n "bookbinder_ast") (r "^0.1.0") (d #t) (k 0)) (d (n "bookbinder_common") (r "^0.1.1") (d #t) (k 0)) (d (n "extended_pulldown") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "temp_file_name") (r "^0.1.0") (d #t) (k 0)))) (h "1zfl685a04qaj2rnbl0qwmm9wi9qg0vq63wmmmhl5s6ia3jq4jkv")))

