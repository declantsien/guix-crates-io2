(define-module (crates-io bo ok bookcase_alloc_macros) #:use-module (crates-io))

(define-public crate-bookcase_alloc_macros-1.0.0 (c (n "bookcase_alloc_macros") (v "1.0.0") (h "0v396knbx93n8pm2516kfiqdsy9l7rq5gk0xbg8k5394hc9yb76c") (r "1.56.0")))

(define-public crate-bookcase_alloc_macros-2.0.0 (c (n "bookcase_alloc_macros") (v "2.0.0") (h "0n46k6wny4p2mhmrh7p69bvngcs6s9fl6vq9a0m1bbbl6d588y7m") (r "1.56.0")))

