(define-module (crates-io bo ok bookstack-exporter) #:use-module (crates-io))

(define-public crate-bookstack-exporter-0.1.0 (c (n "bookstack-exporter") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "confique") (r "^0.2.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1an8wzjjz29xddk8718j7cj9wrw903rfv7hz3f5ganx6bq1v25b4")))

