(define-module (crates-io bo ok bookmarkt) #:use-module (crates-io))

(define-public crate-bookmarkt-0.0.1 (c (n "bookmarkt") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)))) (h "156hyhamm426skjhl9y4clj6yz6fyxqlbwnjjwc7d75f5g53yamr")))

(define-public crate-bookmarkt-0.0.2 (c (n "bookmarkt") (v "0.0.2") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qxv0g4ksa1r173k3gf44vpgm82anrmxcjacg5bsahpa5n7rv8dk")))

(define-public crate-bookmarkt-0.0.3 (c (n "bookmarkt") (v "0.0.3") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sf5fyql6i5ci4r3xcvlh2av14si967jgsrhy9dx5p1xf9izblni")))

