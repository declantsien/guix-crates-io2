(define-module (crates-io bo ok bookbinder) #:use-module (crates-io))

(define-public crate-bookbinder-0.1.0 (c (n "bookbinder") (v "0.1.0") (d (list (d (n "bookbinder_ast") (r "^0.1.0") (d #t) (k 0)) (d (n "bookbinder_common") (r "^0.1.0") (d #t) (k 2)) (d (n "bookbinder_epub") (r "^0.1.0") (d #t) (k 0)) (d (n "bookbinder_latex") (r "^0.1.0") (d #t) (k 0)) (d (n "lopdf") (r "^0.26.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0c1fysp27rh5wlxmvh0ys4s9k0xzi1njndl6v0jfx9v48884qg49")))

