(define-module (crates-io bo ok bookery) #:use-module (crates-io))

(define-public crate-bookery-0.1.0 (c (n "bookery") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1y7wzkbkcanrwbnc6ljh0rzcq2qhhjafhnks8qssbk07vsmy67r7")))

