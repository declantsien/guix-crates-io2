(define-module (crates-io bo ok booklibrs) #:use-module (crates-io))

(define-public crate-booklibrs-0.1.0 (c (n "booklibrs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1kgq32nxx6m08vbj2njcfdilhvgfhm4gwa08p5c6q4dwla10m0y0")))

(define-public crate-booklibrs-0.1.1 (c (n "booklibrs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1wk7kdpgbxnycg1y5lpmil73s4gsvqmcfkf0pzqxxargpf9q8990")))

(define-public crate-booklibrs-0.1.2 (c (n "booklibrs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1ys4a4nnpa2w702agsdgv3w5yvgvsbghg53ka3ihidd4fi0qvqs5")))

(define-public crate-booklibrs-0.1.3 (c (n "booklibrs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "15x9wrzyw6k2nvkgcknqn5xksyd7ilkfv2q85gy7zjl7ps7f9kpa")))

(define-public crate-booklibrs-0.1.4 (c (n "booklibrs") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "08md6cqwcja0ch54dkxkrbr75ivbcb5zhs8msbr3rp1pkslh0hc9")))

(define-public crate-booklibrs-0.1.5 (c (n "booklibrs") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1dfp4myh3ryi2wkmw7vqj5dna139rwxkac7vkk8xg0487py7ghr8")))

(define-public crate-booklibrs-0.1.6 (c (n "booklibrs") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "03gqn59cyvq1fp5clk13kb5kpbjbbzkwvq2izk4s93vl8h2gfwrw")))

(define-public crate-booklibrs-0.1.7 (c (n "booklibrs") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1yb3520aw77mwiq2fgxzcchm2gds96n905226xmcfm5w6lqp7zzl")))

(define-public crate-booklibrs-1.0.0 (c (n "booklibrs") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0mmvhiq21jmigavi2ianz1cfzvbsqbkzrzwmq8djksa1p7laapbf")))

(define-public crate-booklibrs-1.0.1 (c (n "booklibrs") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.13.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0v1nbj6n8i46sp2zhwl0ypsmihi189yafq9cfqxq0iqhswk8n9di")))

(define-public crate-booklibrs-1.0.2 (c (n "booklibrs") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.14.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1mc7fdlqxbzsjxah9jvfqwf2za0hsfk49ywr1nprim9gqnfn51fj")))

(define-public crate-booklibrs-1.0.3 (c (n "booklibrs") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.14.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0hfdl017jkhg1g5s93c6xsvryzjrlwgkbfb1kwy82hsbql0p967x")))

(define-public crate-booklibrs-1.0.4 (c (n "booklibrs") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.14.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0sadhv69p6j73fy0j3x81x6l7aq535bbbnv8zn9ndjfwbrsihqx3")))

(define-public crate-booklibrs-1.0.5 (c (n "booklibrs") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0ya02zxpfsmhz8ds3nb3zk415zpp9inad1i7kmys8dcnaff6zl3a")))

(define-public crate-booklibrs-1.0.6 (c (n "booklibrs") (v "1.0.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "01jbzyx7llmz6ycyzfy6db3z2l5nr4j6zbkgdkb4i9f7rarww2sg")))

(define-public crate-booklibrs-1.0.7 (c (n "booklibrs") (v "1.0.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0f1rcamp6i52w0d1475v5n8zh07lcffn35p492jvykqvsa7590li")))

(define-public crate-booklibrs-1.0.8 (c (n "booklibrs") (v "1.0.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1bw4611blsa78gwh0aqqny6lrvsbgmwhq0vhjzzqd02swaa4djbv")))

(define-public crate-booklibrs-1.0.9 (c (n "booklibrs") (v "1.0.9") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0cpzmvh5cvvrwn5x7mrln8y0i882ann1i9cwik64nymihw8nc43w")))

(define-public crate-booklibrs-1.0.10 (c (n "booklibrs") (v "1.0.10") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1pzn4ilnaqj0dl15mdmwvjha41xmj9h26gkvaadszm188miiga4f")))

(define-public crate-booklibrs-1.0.11 (c (n "booklibrs") (v "1.0.11") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0sabf702hwvxjhbxikkqsnqmgka2k1xx950vyvaxsf7h3iva8rmh")))

(define-public crate-booklibrs-1.0.12 (c (n "booklibrs") (v "1.0.12") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0wnvqkx8hglqaddmsi2v3glyi6lwdz358b30wdm3r3w9w7ks0wza")))

(define-public crate-booklibrs-1.0.13 (c (n "booklibrs") (v "1.0.13") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1glgs42yvalicnd57v9dn292ffxq034p86wk6w58dng3cbc6w4gf")))

(define-public crate-booklibrs-1.0.14 (c (n "booklibrs") (v "1.0.14") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "11jc7p58zvl0bvhgkq1z37d3m6m0qgvrjlp4i1b4wyaxi0gizv00")))

(define-public crate-booklibrs-1.0.15 (c (n "booklibrs") (v "1.0.15") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "052f72qgy8v3nfq8lfafhddshcd24jik0361rbzzxd1crqslbgbc")))

(define-public crate-booklibrs-1.1.0 (c (n "booklibrs") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0lz420gr6f34lf5bxk3vwip8vvnic3q374flvld42idarfd1k8pl")))

(define-public crate-booklibrs-1.1.1 (c (n "booklibrs") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "05pm5nfvraizcgfpm3zn85ljbraad11ngy8ralmbxfd09srsb0l9")))

(define-public crate-booklibrs-1.1.2 (c (n "booklibrs") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0ymlvaypnn5fg9jx7gbhb72ph143cx42y4jzdqscgymkw9kqxx4c")))

(define-public crate-booklibrs-1.1.3 (c (n "booklibrs") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0gijm0z737q6f793hlrpb69rajinfs47bzah64ng8nk1g3grdak1")))

(define-public crate-booklibrs-1.1.4 (c (n "booklibrs") (v "1.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "15vr90pal50vmmij2maicpx0wz6jqvxijx3jm52659gdi129ln29")))

(define-public crate-booklibrs-1.1.5 (c (n "booklibrs") (v "1.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1wmwk5ydymkhkzswwrlhbmq9wbjahf0359b4w2vxy73yhghcrc8h")))

(define-public crate-booklibrs-1.1.6 (c (n "booklibrs") (v "1.1.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1l1wbr6w5d93zbl8a8x9cfcxa8faa6f3b5fq70i35wp03s5d5b4f")))

(define-public crate-booklibrs-1.1.7 (c (n "booklibrs") (v "1.1.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.16") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "08ig9mdbwdkl8hkpk6r7sl1gfrnigflsg60wrbmr88qkcg18yjpd")))

(define-public crate-booklibrs-1.1.8 (c (n "booklibrs") (v "1.1.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.16") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0srrv1cfjblna5clyfmd28vjjrafnbm301hpaldxjm0191bf158y")))

(define-public crate-booklibrs-1.1.9 (c (n "booklibrs") (v "1.1.9") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "fltk") (r "^0.16") (d #t) (k 0)) (d (n "fltk-calendar") (r "^0.2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1ia8gbknv2fkzn1gxvyhzvgq09x0sxah3aisagizp8282gn47bgl")))

