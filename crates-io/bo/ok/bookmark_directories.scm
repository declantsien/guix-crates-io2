(define-module (crates-io bo ok bookmark_directories) #:use-module (crates-io))

(define-public crate-bookmark_directories-0.0.1 (c (n "bookmark_directories") (v "0.0.1") (h "0fr65g0ljbzbgyj79ypv0823ak5zhg4vh2ay1v1dyfnl7cv1i1h6") (y #t)))

(define-public crate-bookmark_directories-0.0.2 (c (n "bookmark_directories") (v "0.0.2") (h "0r14qz2bbgy46gd0vfrak34sfpdkinjjc90294qh7jpwr8h79c8i")))

(define-public crate-bookmark_directories-0.0.3 (c (n "bookmark_directories") (v "0.0.3") (h "1sm48qdgd5khnmg8vv0xgaw96ydnachi3cvfj2ab0wn2zizl93sw")))

(define-public crate-bookmark_directories-0.0.4 (c (n "bookmark_directories") (v "0.0.4") (h "0317sf41631py1wj9s64h9xs6d0w3rnckg9pwcwlmqvjmfq9vb8y")))

(define-public crate-bookmark_directories-0.0.5 (c (n "bookmark_directories") (v "0.0.5") (h "1j3jniw75f44l4y8bnjhvddsmrw3ck7cyspaxzaiwl5svcbl30l9")))

(define-public crate-bookmark_directories-0.0.6 (c (n "bookmark_directories") (v "0.0.6") (h "1kprl5l8s80ij4q3218lrhn57dgffbfb9vds2y54lswqshnhv0ki")))

(define-public crate-bookmark_directories-0.0.7 (c (n "bookmark_directories") (v "0.0.7") (h "163928hwbhxrlrlwi3x5ajrvc1d00ab0wcap6nw4iba4sir1ka6j")))

(define-public crate-bookmark_directories-0.0.8 (c (n "bookmark_directories") (v "0.0.8") (h "0rp6p9pdkrvr665invkb2g1gy2w5fnp7cvdgiwhplhxwqp7llpkn")))

(define-public crate-bookmark_directories-0.0.9 (c (n "bookmark_directories") (v "0.0.9") (h "05jsspb5cjk5ald3afdax0ri0g6f3aiwy3v19nbl6jwy1a6r35xi")))

(define-public crate-bookmark_directories-0.0.10 (c (n "bookmark_directories") (v "0.0.10") (h "1p9lcxqyppi1zcsaz42lxy6ngpay1gdxacjmm14yibm4kkysgd8d")))

(define-public crate-bookmark_directories-0.0.11 (c (n "bookmark_directories") (v "0.0.11") (h "00ycx7jk1i78maq5c1wr7n98sv4khdscjwaqwr56z8j91abqg8y2")))

(define-public crate-bookmark_directories-0.0.12 (c (n "bookmark_directories") (v "0.0.12") (h "0bbkc831ihn2gj6dj47cfvn4w3a21cji7qiy8lzlmzxbxz6zy1hq")))

(define-public crate-bookmark_directories-0.0.13 (c (n "bookmark_directories") (v "0.0.13") (h "09dmx8a22nz40mg8qmy8vr8zsjrsbj26lg906fl18qbjlgpmjna1")))

(define-public crate-bookmark_directories-0.0.14 (c (n "bookmark_directories") (v "0.0.14") (h "0z8rfd730b4c4nj6kyivyka9hfrmg6w1g5ghynwll37395g7icg5")))

(define-public crate-bookmark_directories-0.0.15 (c (n "bookmark_directories") (v "0.0.15") (h "1z9p20g3qm0as3ajfdm9k0vnj9y90n446alp3ns6warpcpygz4n7")))

