(define-module (crates-io bo ok bookgrep) #:use-module (crates-io))

(define-public crate-bookgrep-0.1.0 (c (n "bookgrep") (v "0.1.0") (h "1qsg0ih7qi04jnbznm66284gvcm3ga209nssan91f1pnam07a9f7")))

(define-public crate-bookgrep-0.1.1 (c (n "bookgrep") (v "0.1.1") (h "0gaksxxc0y51c6vb383vs3sv15hni49id1wv94qwj8ci7nxn4xzr")))

(define-public crate-bookgrep-0.1.2 (c (n "bookgrep") (v "0.1.2") (h "01vnsxhaqscd64qq282s75vvkcsgscmnxxsbyk5xapiiz2xvdq8q")))

(define-public crate-bookgrep-0.1.3 (c (n "bookgrep") (v "0.1.3") (h "106c4wg6rf1w6257i19mli22gnz0hmf808a5waqgz64gkv3p6yva")))

