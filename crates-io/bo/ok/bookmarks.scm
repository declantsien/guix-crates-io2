(define-module (crates-io bo ok bookmarks) #:use-module (crates-io))

(define-public crate-bookmarks-0.1.0 (c (n "bookmarks") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "tagsearch") (r "^0.20.1") (d #t) (k 0)))) (h "06zdp9hmhjgfgzg4360igkc3kfn5wqghkza4wzk3ibv2lgrkql67")))

(define-public crate-bookmarks-0.2.0 (c (n "bookmarks") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "tagsearch") (r "^0.20.1") (d #t) (k 0)))) (h "08aiqkfv6gkz239725sfv8diz57lfy3w27a17cmvd3bf67bm2ixg")))

(define-public crate-bookmarks-0.4.0 (c (n "bookmarks") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "tagsearch") (r "^0.20.1") (d #t) (k 0)))) (h "1lmhwljzjy4m0xrf8ibfbspymg6ksgv8fljb7iv348dqys7hxfy6")))

(define-public crate-bookmarks-0.5.0 (c (n "bookmarks") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "tagsearch") (r "^0.20.1") (d #t) (k 0)))) (h "1h89g6y7a46ssp5s9r2whvn1sxvydi4lr9n2mm19vbwjq8q4lh3d")))

(define-public crate-bookmarks-0.6.0 (c (n "bookmarks") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "tagsearch") (r "^0.20.1") (d #t) (k 0)))) (h "0rhrzgfw3pfanaqmaak7dkvxcf09ybbz0ap9wnsv8g1ny7fpiiqd")))

(define-public crate-bookmarks-0.7.0 (c (n "bookmarks") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "tagsearch") (r "^0.20.1") (d #t) (k 0)))) (h "11bh9x46kczpzwvzvp81rvflfxnq9l93m610cdzlcks51mch6qw5")))

(define-public crate-bookmarks-0.8.0 (c (n "bookmarks") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "tagsearch") (r "^0.20.1") (d #t) (k 0)))) (h "0mywc79i8g8jy3phayh1gm20lymvpr9mk0312xj6zhkhdq33q5h8")))

