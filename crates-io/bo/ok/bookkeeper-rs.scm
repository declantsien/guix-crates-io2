(define-module (crates-io bo ok bookkeeper-rs) #:use-module (crates-io))

(define-public crate-bookkeeper-rs-0.0.1 (c (n "bookkeeper-rs") (v "0.0.1") (d (list (d (n "protobuf-codegen-pure") (r "^2.14") (d #t) (k 1)))) (h "1z6p99dvdi8y0xmnxw09vzw6nm9711l50h318qbsy0cd1wg4p9sn")))

(define-public crate-bookkeeper-rs-0.0.2 (c (n "bookkeeper-rs") (v "0.0.2") (d (list (d (n "protobuf-codegen-pure") (r "^2.14") (d #t) (k 1)))) (h "0s0p1d7kdnd6bbwvi76vavcv9nz8cqs4d5s03ibgdma7j28fldhn")))

