(define-module (crates-io bo ok bookkeeping) #:use-module (crates-io))

(define-public crate-bookkeeping-0.1.0 (c (n "bookkeeping") (v "0.1.0") (d (list (d (n "duplicate") (r ">=0.2.9, <0.3.0") (d #t) (k 0)) (d (n "maplit") (r ">=1.0.2, <2.0.0") (d #t) (k 0)))) (h "1rch047zdxxmnwa8c5jvs0v2fc5h13q5z94406lpp58vf03sx4x1")))

(define-public crate-bookkeeping-0.2.0 (c (n "bookkeeping") (v "0.2.0") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "1wcxqah8zzqqw45n6ch4xa6ykbzbcxhpfy6f140y8xm7qa8gj00l")))

(define-public crate-bookkeeping-0.2.1 (c (n "bookkeeping") (v "0.2.1") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "194aqd17a8zg4rffypwm7xv5ph9qb23nvw3px8dczs7n5hk86h9f")))

(define-public crate-bookkeeping-0.3.1 (c (n "bookkeeping") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "1nryx9qsimgrvadgn8z7ndv95v4nf94yf9wbyimg5aby7wjig3fv")))

(define-public crate-bookkeeping-0.4.0 (c (n "bookkeeping") (v "0.4.0") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "10lykc89jsxmf83bcsj4l144kkj0x53ac11d307mi1zg9bjz4ki3") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.4.1 (c (n "bookkeeping") (v "0.4.1") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "017ks8g7mcv9334y9aq2c863iyb8sixw5l1plp469rbbcy0c7ss4") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.4.2 (c (n "bookkeeping") (v "0.4.2") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "0v0cxcnpdy6h12l4kc8ahqijjf6khqxhm442882zpp1lrsj6kxcy") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.4.3 (c (n "bookkeeping") (v "0.4.3") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "0jaqbvn7wv6zp5xd2g3c72vwichjkciylv51lw47gcj4lcsd561p") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.4.4 (c (n "bookkeeping") (v "0.4.4") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "0bcj2hplmsmmls7159lvibsgahmjka1jv5zzwfpd1kzy91bf44x0") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.4.5 (c (n "bookkeeping") (v "0.4.5") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "0z7yqa5ahzn9qfvxjmmqssvk9sjwjm71b1i5j7a673blymcpdlr7") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.4.7 (c (n "bookkeeping") (v "0.4.7") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "1yk9lgpamls7bv0pxyqwb4izb0zv053znnb8wy0i8y1zmjraz2r2") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.5.0 (c (n "bookkeeping") (v "0.5.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "04ncbqag1lyva1wlm0nqc6381d27smccwagpdwhdrqw4hgjyn7mr") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.6.0 (c (n "bookkeeping") (v "0.6.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.2") (d #t) (k 0)))) (h "1q2kjg5smgkrnnfjlja7liw5w7wjwfr4whndlv3iq4gkbcyzckl3") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.6.1 (c (n "bookkeeping") (v "0.6.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.2") (d #t) (k 0)))) (h "1ns7gjg9kxlgnkjh5r13mp60ni863ak81kvqw47ywghz9mgrwjqr") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.6.2 (c (n "bookkeeping") (v "0.6.2") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.2") (d #t) (k 0)))) (h "1prc48jcd95h5h7djrfxs6yvmchvhn84gj16v0msb51pzd6x79aj") (f (quote (("fail-on-warnings"))))))

(define-public crate-bookkeeping-0.6.3 (c (n "bookkeeping") (v "0.6.3") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.2") (d #t) (k 0)))) (h "1anzwhp2whwqj3iiwh91w4c9ia7v0k0n0bvcc11r34a4kw8g9v83") (f (quote (("fail-on-warnings"))))))

