(define-module (crates-io bo ok book-summary) #:use-module (crates-io))

(define-public crate-book-summary-0.1.0 (c (n "book-summary") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)) (d (n "titlecase") (r "^0.10") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "01i83l3rbmzs47203n11jkaysh38lngq1lnmpl9slyhfckjn12di")))

(define-public crate-book-summary-0.2.0 (c (n "book-summary") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)) (d (n "titlecase") (r "^0.10") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0bgab6ll5x1xq611g019vaymyiil7ihik6wwgyihs72qca7ls6xw")))

(define-public crate-book-summary-0.2.1 (c (n "book-summary") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)) (d (n "titlecase") (r "^2.2.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0pwabpshcx9xsfbx6r0saz3gmw139k37rdzk7nvzzhalgx7wbflg")))

