(define-module (crates-io bo ok bookcase_alloc) #:use-module (crates-io))

(define-public crate-bookcase_alloc-0.0.1 (c (n "bookcase_alloc") (v "0.0.1") (d (list (d (n "bookcase_alloc_macros") (r "=1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.37") (d #t) (k 0)))) (h "18y1zlapsd3byvhzlwhhcgaclpdfldlf02cn16q1awyfvyd9xia9") (f (quote (("stable") ("experimental") ("default" "stable") ("beta") ("allocator_api")))) (r "1.56.0")))

(define-public crate-bookcase_alloc-0.0.2 (c (n "bookcase_alloc") (v "0.0.2") (d (list (d (n "bookcase_alloc_macros") (r "=2.0.0") (d #t) (k 0)))) (h "0yr1yalnbbczlkvd8ql02p3pawvd2jkdspb2wrl02fa6a09rhiyc") (f (quote (("stable") ("experimental") ("default" "stable") ("beta") ("allocator_api")))) (r "1.56.0")))

