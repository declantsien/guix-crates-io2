(define-module (crates-io bo ok bookkeep) #:use-module (crates-io))

(define-public crate-bookkeep-0.1.0 (c (n "bookkeep") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-tuple-vec-map") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde-human-readable"))) (d #t) (k 0)))) (h "093m4bq1jgyv867zq53lkais8vd3zvk73c8rwpr5i023fm26pf8x")))

(define-public crate-bookkeep-0.2.0 (c (n "bookkeep") (v "0.2.0") (d (list (d (n "rust_decimal") (r "^1.33") (f (quote ("serde-str"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-tuple-vec-map") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde-human-readable"))) (d #t) (k 0)))) (h "09z4bn1rn45jbspqgjac8gkfi03h91wcb0hbcbby0bf20cwwz3n0")))

