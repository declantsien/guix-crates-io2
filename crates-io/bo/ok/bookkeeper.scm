(define-module (crates-io bo ok bookkeeper) #:use-module (crates-io))

(define-public crate-bookkeeper-0.0.1 (c (n "bookkeeper") (v "0.0.1") (d (list (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite" "offline"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0l0l2s2qykmyml9g6gzbd6abbc8rfzv4a595f6yakln25nw8pkpj")))

