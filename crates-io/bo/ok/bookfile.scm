(define-module (crates-io bo ok bookfile) #:use-module (crates-io))

(define-public crate-bookfile-0.1.0 (c (n "bookfile") (v "0.1.0") (d (list (d (n "aversion") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18yns5m7yqymci6rxb11cf8gsqq4p331zimf4yqzin3zrji2d3xb")))

(define-public crate-bookfile-0.2.0 (c (n "bookfile") (v "0.2.0") (d (list (d (n "aversion") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wfvwccc73v6xqdgjza0kbngy6kn622s4v56spjxr2wvv6n93cvx")))

(define-public crate-bookfile-0.3.0 (c (n "bookfile") (v "0.3.0") (d (list (d (n "aversion") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lz7f5jan3d735r0x0qbxas9l1svbqk0ywqhpknbpq8lch4f58zg")))

