(define-module (crates-io bo ok bookbinder_ast) #:use-module (crates-io))

(define-public crate-bookbinder_ast-0.1.0 (c (n "bookbinder_ast") (v "0.1.0") (d (list (d (n "bookbinder_common") (r "^0.1.0") (d #t) (k 0)) (d (n "extended_pulldown") (r "^0.1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "temp_file_name") (r "^0.1.0") (d #t) (k 0)))) (h "0636vpz1fi97rklxj0mvp7qsgfs6907zlx8glx123yhm4glp1h1a")))

