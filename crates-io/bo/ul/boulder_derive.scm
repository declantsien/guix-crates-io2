(define-module (crates-io bo ul boulder_derive) #:use-module (crates-io))

(define-public crate-boulder_derive-0.1.0 (c (n "boulder_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19gv2gl3jm4rp1my7wakgvkm9cq1m794pjaxp164nskv4rm77zzi")))

(define-public crate-boulder_derive-0.1.1 (c (n "boulder_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jg7w9h3mqzv02391jghb2yf5d7shn6nyib9zkgzigagzhya90my")))

(define-public crate-boulder_derive-0.2.0 (c (n "boulder_derive") (v "0.2.0") (d (list (d (n "persian-rug") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y2gb9jynrvb0sarl77f1yy6qj1w1irrxmchghlfqq43ydw0wpdx") (f (quote (("persian-rug") ("default"))))))

(define-public crate-boulder_derive-0.3.0 (c (n "boulder_derive") (v "0.3.0") (d (list (d (n "persian-rug") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ykwh0mmgwacgv04ppgp4wnrrssdsrbkrnmnlm0n6n9n0hvn0fb5") (f (quote (("persian-rug") ("default"))))))

