(define-module (crates-io bo ul boulderboard) #:use-module (crates-io))

(define-public crate-boulderboard-0.1.0 (c (n "boulderboard") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "dioxus-desktop") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1pnnrxar2d5smxs54y1hkrz86hwh8g8ylp8i9b55pd2vlw2djhhq") (r "1.70")))

