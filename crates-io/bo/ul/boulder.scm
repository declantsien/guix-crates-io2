(define-module (crates-io bo ul boulder) #:use-module (crates-io))

(define-public crate-boulder-0.1.0 (c (n "boulder") (v "0.1.0") (d (list (d (n "boulder_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1dy0dwxac0g0wv27bma2bsz7gzm9065gnbdwa73xp4wcgrv9h5xx")))

(define-public crate-boulder-0.1.1 (c (n "boulder") (v "0.1.1") (d (list (d (n "boulder_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1x253pswrq76j1jg5l57l6c2ip6agdkaq426kws6813zskfx71l9")))

(define-public crate-boulder-0.2.0 (c (n "boulder") (v "0.2.0") (d (list (d (n "boulder_derive") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "persian-rug") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0qc9fzzw3h44nb7rrwhv4mz90395k99c1ghy98gggm2anvw2bm28") (f (quote (("default")))) (s 2) (e (quote (("persian-rug" "dep:persian-rug" "boulder_derive/persian-rug"))))))

(define-public crate-boulder-0.3.0 (c (n "boulder") (v "0.3.0") (d (list (d (n "boulder_derive") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "persian-rug") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0yfzhafzyx9n6ib143nfc2zl48b5hlhqj8s5h4049k6872y5yz35") (f (quote (("default")))) (s 2) (e (quote (("persian-rug" "dep:persian-rug" "boulder_derive/persian-rug"))))))

