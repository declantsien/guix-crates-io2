(define-module (crates-io bo se bose) #:use-module (crates-io))

(define-public crate-bose-0.1.0 (c (n "bose") (v "0.1.0") (h "1am2142f0jj95hrdacj4v34c0wnp7kingyi8ffd7dswj1mhf49bf") (y #t)))

(define-public crate-bose-0.1.1 (c (n "bose") (v "0.1.1") (h "0gpl9b7jg5pxbklsd6wgl2wvvmwy6m9f34vzq12kwlmnymir7s57")))

(define-public crate-bose-0.1.2 (c (n "bose") (v "0.1.2") (h "1cw8k806q38i2082h7b9cgdwfxlapn220bl2hajc5jyj469wig1r")))

(define-public crate-bose-0.1.3 (c (n "bose") (v "0.1.3") (h "0jhs5af0d4vxgdfapx8hcw7612fbykd3v5l10vl089yf2zaidiq0")))

(define-public crate-bose-0.1.4 (c (n "bose") (v "0.1.4") (h "1jlc589i7z0aaxjx3gnm4vk97xg59alj873qlqrx8n5s67hnb078")))

