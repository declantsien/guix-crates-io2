(define-module (crates-io bo xe boxed-slice) #:use-module (crates-io))

(define-public crate-boxed-slice-0.1.0 (c (n "boxed-slice") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0i2n5ixmrjkkjdxbdzpyckzfz0i3p3sp521wazj6kqrhyw6h8yd9")))

(define-public crate-boxed-slice-0.1.1 (c (n "boxed-slice") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1mq19zd9p27m96b7kq2f99383j52ibdljfyv4fc4cwb1f787v3s8")))

