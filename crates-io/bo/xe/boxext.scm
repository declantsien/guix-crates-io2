(define-module (crates-io bo xe boxext) #:use-module (crates-io))

(define-public crate-boxext-0.1.0 (c (n "boxext") (v "0.1.0") (d (list (d (n "allocator_api") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "boxext_derive") (r "^0.1") (d #t) (k 2)) (d (n "static_assertions") (r "^0.2") (d #t) (k 0)))) (h "0ipa0814fnnxwh0n2myfmxv7r4q3nq0d6kx40m255wlkfnpcwwhy") (f (quote (("unstable-rust") ("std") ("default" "std"))))))

(define-public crate-boxext-0.1.1 (c (n "boxext") (v "0.1.1") (d (list (d (n "allocator_api") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "boxext_derive") (r "^0.1") (d #t) (k 2)) (d (n "static_assertions") (r "^0.2") (d #t) (k 0)))) (h "0qwzv7lwym886zysfiav71bm1dwhp9bwkapk47d0p74mh483p4ac") (f (quote (("unstable-rust") ("std") ("default" "std"))))))

(define-public crate-boxext-0.1.2 (c (n "boxext") (v "0.1.2") (d (list (d (n "allocator_api") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "boxext_derive") (r "^0.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "static_assertions") (r "^0.2") (d #t) (k 0)))) (h "1jnxw7dvd0m7zqa7v37cbj5aqpjyggik3pknxqf9h4f3v66dzfz9") (f (quote (("unstable-rust") ("std") ("fallible") ("default" "std"))))))

(define-public crate-boxext-0.1.3 (c (n "boxext") (v "0.1.3") (d (list (d (n "allocator_api") (r ">= 0.2, < 0.4") (o #t) (d #t) (k 0)) (d (n "boxext_derive") (r "^0.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "static_assertions") (r "^0.2") (d #t) (k 0)))) (h "13gvigs9hwsj94dqjq8pykdp3px9frx7mn1hrqm3k4lcmvv66a46") (f (quote (("unstable-rust") ("std") ("fallible") ("default" "std"))))))

(define-public crate-boxext-0.1.4 (c (n "boxext") (v "0.1.4") (d (list (d (n "allocator_api") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "boxext_derive") (r "^0.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "static_assertions") (r "^0.2") (d #t) (k 0)))) (h "0bflvlaqngcf156hffyq0czm4bb97z9mmd1mnpkrisq4piqc8yan") (f (quote (("unstable-rust") ("std") ("fallible") ("default" "std"))))))

(define-public crate-boxext-0.1.5 (c (n "boxext") (v "0.1.5") (d (list (d (n "allocator_api") (r "^0.5") (o #t) (k 0)) (d (n "boxext_derive") (r "^0.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "035c3ncaf1fwf1krd9qkngcch5vp9xaf496qhmcliic352gph9lg") (f (quote (("unstable-rust") ("std") ("fallible") ("default" "std"))))))

(define-public crate-boxext-0.1.6 (c (n "boxext") (v "0.1.6") (d (list (d (n "allocator_api") (r ">= 0.5, < 0.7") (o #t) (k 0)) (d (n "boxext_derive") (r "^0.1") (d #t) (k 2)))) (h "0a41w6ryvgxy1npanldf3rrrw0gwg8vaja4zzbfwkf1axin7zzss") (f (quote (("unstable-rust") ("std") ("fallible") ("default" "std"))))))

