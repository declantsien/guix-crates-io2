(define-module (crates-io bo xe boxext_derive) #:use-module (crates-io))

(define-public crate-boxext_derive-0.1.0 (c (n "boxext_derive") (v "0.1.0") (d (list (d (n "quote") (r ">= 0.4, < 0.6") (d #t) (k 0)) (d (n "syn") (r ">= 0.12, < 0.14") (d #t) (k 0)))) (h "0r919jmlbdpb2f4z583d64avvcly0sr1krybdd8nnjv19zidsyk7")))

(define-public crate-boxext_derive-0.1.1 (c (n "boxext_derive") (v "0.1.1") (d (list (d (n "quote") (r ">= 0.4, < 0.6") (d #t) (k 0)) (d (n "syn") (r ">= 0.12, < 0.14") (d #t) (k 0)))) (h "0zlb9g210z085vn38x005gqfrn1qrk6ahs2z0qf6vv7yv1j4k0pp")))

(define-public crate-boxext_derive-0.1.2 (c (n "boxext_derive") (v "0.1.2") (d (list (d (n "quote") (r ">= 0.4, < 0.6") (d #t) (k 0)) (d (n "syn") (r ">= 0.12, < 0.14") (d #t) (k 0)))) (h "17q04a9vys9n5fmxcs8j4yablgs0j1gp2afr7p8z6l9g27ybd1j5")))

(define-public crate-boxext_derive-0.1.3 (c (n "boxext_derive") (v "0.1.3") (d (list (d (n "quote") (r ">= 0.4, < 0.7") (d #t) (k 0)) (d (n "syn") (r ">= 0.12, < 0.15") (d #t) (k 0)))) (h "028kx05cwk3r5karg48swr8fqf5gpbpw0ja75ijv7q0km5x648dl")))

