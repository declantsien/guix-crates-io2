(define-module (crates-io bo xe boxer) #:use-module (crates-io))

(define-public crate-boxer-0.1.0 (c (n "boxer") (v "0.1.0") (d (list (d (n "clippet") (r "^0.1.8") (d #t) (k 0)))) (h "1b135sky662ncl8fghl96qpb1x5ximdv28iagf6m7fyb19873l7h")))

(define-public crate-boxer-0.1.1 (c (n "boxer") (v "0.1.1") (d (list (d (n "clippet") (r "^0.1.8") (d #t) (k 0)))) (h "0zak7r1yd1vihkrlh6wkkrij6amgkq89aa4x08z9gfajy1c49hdz")))

