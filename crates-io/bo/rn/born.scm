(define-module (crates-io bo rn born) #:use-module (crates-io))

(define-public crate-born-0.0.0 (c (n "born") (v "0.0.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "0lhmfx2wchyfw4k99fflxrr178g7m4r93z48wmk4mhk3xsjqm8g8")))

(define-public crate-born-0.0.1 (c (n "born") (v "0.0.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "0rk4mz262vdb037wyi2m5na2k083bs4sibkrmmrb9vpznxcg8dis")))

