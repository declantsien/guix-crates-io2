(define-module (crates-io bo ns bonsai) #:use-module (crates-io))

(define-public crate-bonsai-0.1.0 (c (n "bonsai") (v "0.1.0") (h "0bc5ycbh05ccx6c0bypvyb0pyvv212x0vxyn3dajcv4g3kgpjw0x")))

(define-public crate-bonsai-0.2.0 (c (n "bonsai") (v "0.2.0") (h "1mjls6fwx69j2prmk73zrlxdwbzrg2px8kagsbc7pdcrcvx4lcg9") (f (quote (("u64") ("u128") ("default" "u64"))))))

(define-public crate-bonsai-0.2.1 (c (n "bonsai") (v "0.2.1") (h "1r61zcdmvfigk8bjfyngpi0blzc6mhz83hbwnv55rq4pb2cnnqkm") (f (quote (("u64") ("u128") ("default" "u64"))))))

