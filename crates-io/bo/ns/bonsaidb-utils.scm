(define-module (crates-io bo ns bonsaidb-utils) #:use-module (crates-io))

(define-public crate-bonsaidb-utils-0.0.0-reserve.0 (c (n "bonsaidb-utils") (v "0.0.0-reserve.0") (h "0pjh6s16dxw73jdrkgx4s0sixfkcm0ka63avjknhbk2wgifsj72a")))

(define-public crate-bonsaidb-utils-0.1.0 (c (n "bonsaidb-utils") (v "0.1.0") (h "0bjkbmxljsw51r87kgrbngly454kv30qicfss8v8ymv8pp6gi65d")))

(define-public crate-bonsaidb-utils-0.1.1 (c (n "bonsaidb-utils") (v "0.1.1") (h "09w9klvmrnbi31ibyy45c19czxhdqs3r9x6w8fmrxksrg0ccv0c3")))

(define-public crate-bonsaidb-utils-0.1.2 (c (n "bonsaidb-utils") (v "0.1.2") (h "07mca621h2c2vb218y72dc22wv6823zjrrxz01xjajh5s7l67p7z")))

(define-public crate-bonsaidb-utils-0.2.0 (c (n "bonsaidb-utils") (v "0.2.0") (h "1k0sgx6nv7wrjd0ng5hcnsnwllz7x11jxkirkqrk2ncb3g5fwkpi") (r "1.58")))

(define-public crate-bonsaidb-utils-0.3.0 (c (n "bonsaidb-utils") (v "0.3.0") (h "129kfhv5mf7sz20nmrskc2xhbvlfy12dr8lfmq3rmsabmmn4z1dj") (r "1.58")))

(define-public crate-bonsaidb-utils-0.4.0 (c (n "bonsaidb-utils") (v "0.4.0") (h "0qd22q5ifgm4haia2bxvdpv5l99zbk519ip4sjic3qnyjkxvnb51") (r "1.58")))

(define-public crate-bonsaidb-utils-0.4.1 (c (n "bonsaidb-utils") (v "0.4.1") (h "17yq58rh6g4fjd23r0dfg56dlcdl55lp31pz1xd9y45fkp3bdgqk") (r "1.58")))

(define-public crate-bonsaidb-utils-0.5.0 (c (n "bonsaidb-utils") (v "0.5.0") (h "14sz5l1vaazd7x7vg5gfscjjp52hjqkvsdw41bxmavafng4mlkhq") (r "1.70")))

