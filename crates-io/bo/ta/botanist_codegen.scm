(define-module (crates-io bo ta botanist_codegen) #:use-module (crates-io))

(define-public crate-botanist_codegen-0.1.0 (c (n "botanist_codegen") (v "0.1.0") (d (list (d (n "juniper") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.15") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (d #t) (k 0)))) (h "0irivci0r76bplsjw9mkxn9k3ndn0mih34025n9a9l7dz3rlxcka")))

