(define-module (crates-io bo ta botan-sys) #:use-module (crates-io))

(define-public crate-botan-sys-0.1.0 (c (n "botan-sys") (v "0.1.0") (h "0xmrzh0n8irgy6djwh75kzlkjz8d4cryzww8hs8x2yl1mhlxxaac") (l "botan-2")))

(define-public crate-botan-sys-0.1.1 (c (n "botan-sys") (v "0.1.1") (h "0n2hkdhzzf7lw0wp25h5kb3ivm2msvj2cp5zv363r1sk5m0vvqhg") (l "botan-2")))

(define-public crate-botan-sys-0.1.2 (c (n "botan-sys") (v "0.1.2") (h "1013p8g9p71qhhhki5shhfnx1qx837v61zn3yv585kkjwni5lgbq") (l "botan-2")))

(define-public crate-botan-sys-0.1.3 (c (n "botan-sys") (v "0.1.3") (h "0ikj8pv5769pd2h72zvncgi49z0xla4sjgqqp7gvw0fwzq0hxajq") (l "botan-2")))

(define-public crate-botan-sys-0.1.4 (c (n "botan-sys") (v "0.1.4") (h "0jh8r631z3wwpdsi5gfv1vy6cqp7g914n1mb8af8cc5zwhqim9qd") (l "botan-2")))

(define-public crate-botan-sys-0.1.5 (c (n "botan-sys") (v "0.1.5") (h "1v73k121qjcyz74ayrnhk7gkgw4hh26wh1fjkdsp6xzy553diqpg") (l "botan-2")))

(define-public crate-botan-sys-0.2.0 (c (n "botan-sys") (v "0.2.0") (h "1fh12dhy67aqr2zi8747k9pdclbm1yblsrbpgqqvcr03rcxj5x1a") (l "botan-2")))

(define-public crate-botan-sys-0.3.0 (c (n "botan-sys") (v "0.3.0") (h "19m8hhmb3r0sqxz7zl4paaw08bv0yq2j1znhvf3wr2j22zzkkrl2") (l "botan-2")))

(define-public crate-botan-sys-0.4.0 (c (n "botan-sys") (v "0.4.0") (h "1l27zh88ya75h5fb9730fa8mh2798g5a18pparr1b19j56lbh1jp") (l "botan-2")))

(define-public crate-botan-sys-0.5.0 (c (n "botan-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0nxbrf00nk3l2ngsb9mqh7c5vjdcly4pg5ab83l8cdw2ivxx9wg0") (l "botan-2")))

(define-public crate-botan-sys-0.6.0 (c (n "botan-sys") (v "0.6.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0fqv6wi44455rmzl75w420apwnr9g5i50i6c6m4y0pfy3pw0c36r") (l "botan-2")))

(define-public crate-botan-sys-0.7.0 (c (n "botan-sys") (v "0.7.0") (d (list (d (n "botan-src") (r "^0.21500") (o #t) (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1hf2i378wgqmaikpbg2jav8h1vvdk5wpbfbg9as6fi1hsr4sf8if") (f (quote (("vendored" "botan-src") ("default")))) (l "botan-2")))

(define-public crate-botan-sys-0.8.0 (c (n "botan-sys") (v "0.8.0") (d (list (d (n "botan-src") (r "^0.21701") (o #t) (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0pscm4kpxhj8mjmmhrw87by12himrcln02i4fa5n6vy73nlcc09q") (f (quote (("vendored" "botan-src") ("no-std") ("default")))) (l "botan-2")))

(define-public crate-botan-sys-0.8.1 (c (n "botan-sys") (v "0.8.1") (d (list (d (n "botan-src") (r "^0.21703") (o #t) (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1m11zblxfanrhl97j7z3ap7n17rr8j0rg91sr7f9j6y2bsniaz1x") (f (quote (("vendored" "botan-src") ("no-std") ("default")))) (l "botan-2")))

(define-public crate-botan-sys-0.9.0 (c (n "botan-sys") (v "0.9.0") (d (list (d (n "botan-src") (r "^0.21903") (o #t) (d #t) (k 1)))) (h "172vmmzbk2jlk13f8amyvjz7hr8r84xrs1zkmfmdiml5dxi2f7vw") (f (quote (("vendored" "botan-src") ("no-std") ("default") ("botan3")))) (l "botan-2")))

(define-public crate-botan-sys-0.9.1 (c (n "botan-sys") (v "0.9.1") (d (list (d (n "botan-src") (r "^0.21903") (o #t) (d #t) (k 1)))) (h "0if6vzp6f692bp68zhg3d7gimlmaxvz7a3ik57gjif8nzhmgh0c8") (f (quote (("vendored" "botan-src") ("no-std") ("default") ("botan3")))) (l "botan-2")))

(define-public crate-botan-sys-0.10.0 (c (n "botan-sys") (v "0.10.0") (d (list (d (n "botan-src") (r "^0.21903") (o #t) (d #t) (k 1)))) (h "0mb5vqb4l2n64wkmbyg98a6bja52lxi6bhrd3xbzy7gs61a851mr") (f (quote (("vendored" "botan-src") ("no-std") ("default") ("botan3")))) (l "botan-2")))

(define-public crate-botan-sys-0.10.1 (c (n "botan-sys") (v "0.10.1") (d (list (d (n "botan-src") (r "^0.21903") (o #t) (d #t) (k 1)))) (h "0v11kw5z1341jgr3mc4mswfm8kbjq339qmn37q2g6ny9q759fka9") (f (quote (("vendored" "botan-src") ("no-std") ("default") ("botan3")))) (l "botan-2") (r "1.58")))

(define-public crate-botan-sys-0.10.2 (c (n "botan-sys") (v "0.10.2") (d (list (d (n "botan-src") (r "^0.21903") (o #t) (d #t) (k 1)))) (h "1zhrjgsya16f653sc2m2m21k7jcadyv66w8bbf21v3wkdvp960yc") (f (quote (("vendored" "botan-src") ("no-std") ("default") ("botan3")))) (l "botan-2") (r "1.58")))

(define-public crate-botan-sys-0.10.3 (c (n "botan-sys") (v "0.10.3") (d (list (d (n "botan-src") (r "^0.21903") (o #t) (d #t) (k 1)))) (h "1cbjr44gc5dhmgl43sfiqzbsma4anfi3h26m4yzsli23yd1lmyf8") (f (quote (("vendored" "botan-src") ("no-std") ("default") ("botan3")))) (l "botan-2") (r "1.58")))

(define-public crate-botan-sys-0.10.4 (c (n "botan-sys") (v "0.10.4") (d (list (d (n "botan-src") (r "^0.30101.1") (o #t) (d #t) (k 1)))) (h "0gw5d5b44yj1a7jh81g9dq5x15jafv8gvld0nm2q9p618nb6ii83") (f (quote (("vendored" "botan-src" "botan3") ("no-std") ("default") ("botan3")))) (l "botan-2") (r "1.58")))

(define-public crate-botan-sys-0.10.5 (c (n "botan-sys") (v "0.10.5") (d (list (d (n "botan-src") (r "^0.30101.2") (o #t) (d #t) (k 1)))) (h "1ji12rxvi4h7pap772cd2hw4xdgqdsgw6m8wqin9klpbp3hxsjcz") (f (quote (("vendored" "botan-src" "botan3") ("no-std") ("default") ("botan3")))) (l "botan-2") (r "1.58")))

