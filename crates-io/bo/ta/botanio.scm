(define-module (crates-io bo ta botanio) #:use-module (crates-io))

(define-public crate-botanio-0.1.0 (c (n "botanio") (v "0.1.0") (d (list (d (n "hyper") (r "^0.6.16") (d #t) (k 0)) (d (n "quick-error") (r "^0.1.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "0qa44wyn4ydd9fax77dkvd2hi4smyp0084c5d214hhakkmyp6481")))

