(define-module (crates-io bo ta botan) #:use-module (crates-io))

(define-public crate-botan-0.1.0 (c (n "botan") (v "0.1.0") (d (list (d (n "botan-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1ywqhrgz2rvknjj70hlx4aaa9abfrzxwks3qwvc4ygrm7463pgw3")))

(define-public crate-botan-0.1.1 (c (n "botan") (v "0.1.1") (d (list (d (n "botan-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1l45px17a1in24ywcgafndv126ldg4cnzjzbxxmlkp1bfj1schnd")))

(define-public crate-botan-0.1.2 (c (n "botan") (v "0.1.2") (d (list (d (n "botan-sys") (r "^0.1.1") (d #t) (k 0)))) (h "04fzx75y6jvdy9qiw61j06pxhknydaxr19hnr12680hcq6blrnqk")))

(define-public crate-botan-0.1.3 (c (n "botan") (v "0.1.3") (d (list (d (n "botan-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1sl04pqaiv6pg47knicidf064bdsykqaaczvkbfrv4p9a610pzxp")))

(define-public crate-botan-0.1.4 (c (n "botan") (v "0.1.4") (d (list (d (n "botan-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0ca2y0c2rjg48c75vhgh5chcnh7fd4avvxab1rgplbk1dr7hq297")))

(define-public crate-botan-0.1.5 (c (n "botan") (v "0.1.5") (d (list (d (n "botan-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0ljjnz9hqnd9fn41iqssbd4fz95r3z2sak9wa7yq3mzqc30nym6w")))

(define-public crate-botan-0.2.0 (c (n "botan") (v "0.2.0") (d (list (d (n "botan-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0md9cq01736pzl73ygndrrrsfqq1xkada3232iwag4vmwg515lay")))

(define-public crate-botan-0.3.0 (c (n "botan") (v "0.3.0") (d (list (d (n "botan-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1zryn0zajy2mkpy822ld7nxkci07g1igqmfbn7024kd59kzvi2z8")))

(define-public crate-botan-0.4.0 (c (n "botan") (v "0.4.0") (d (list (d (n "botan-sys") (r "^0.4.0") (d #t) (k 0)))) (h "14h1hywfnxsw32864l3hv5sgzxnafp79213xhjpalkhbz6gmmacx")))

(define-public crate-botan-0.5.0 (c (n "botan") (v "0.5.0") (d (list (d (n "botan-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "cstr_core") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "13mznxxrmrphpy2nc9asxxr9qi65jd2fbisawsxic78nm57yq9gy") (f (quote (("no-std" "cstr_core/alloc") ("default"))))))

(define-public crate-botan-0.6.0 (c (n "botan") (v "0.6.0") (d (list (d (n "botan-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "cstr_core") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1gljsiz431pqixhzbxjggs89dic6fai5fn0j5881141dzxg1dasw") (f (quote (("no-std" "cstr_core/alloc") ("default"))))))

(define-public crate-botan-0.6.1 (c (n "botan") (v "0.6.1") (d (list (d (n "botan-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "cstr_core") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "05ryxy8wbcpl2mvmqf0v07p2v6vq4gxy3a567mq19g9mh9vdajvh") (f (quote (("no-std" "cstr_core/alloc") ("default"))))))

(define-public crate-botan-0.7.0 (c (n "botan") (v "0.7.0") (d (list (d (n "botan-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "02g47m7n72jxjsi8rvx2k66cqjyrs5a57bp5v51blvlqcc09da6z") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "cstr_core/alloc") ("default"))))))

(define-public crate-botan-0.8.0 (c (n "botan") (v "0.8.0") (d (list (d (n "botan-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "09wz2hagjkqk1szbga90in8pdghmrwr28cjfn47bpibsyzpalnp0") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "cstr_core/alloc") ("default"))))))

(define-public crate-botan-0.8.1 (c (n "botan") (v "0.8.1") (d (list (d (n "botan-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "cstr_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "08bmiyn7c3b0dgx20w6hr28d9jcq7cj78cchr84pc686sb2s41ik") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "cstr_core/alloc") ("default"))))))

(define-public crate-botan-0.9.0 (c (n "botan") (v "0.9.0") (d (list (d (n "botan-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.4") (d #t) (k 2)))) (h "1spciqs6m5f42wgai5qxgxgv0g2vkxsvnrfzd4ksi09cqmvpzia1") (f (quote (("vendored" "botan-sys/vendored") ("no-std") ("default") ("botan3" "botan-sys/botan3"))))))

(define-public crate-botan-0.9.1 (c (n "botan") (v "0.9.1") (d (list (d (n "botan-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.4") (d #t) (k 2)))) (h "0szj6rjhvy0fqq3kbw9yph6kjbj20id0liabk46w0mylvq4bd36j") (f (quote (("vendored" "botan-sys/vendored") ("no-std") ("default") ("botan3" "botan-sys/botan3"))))))

(define-public crate-botan-0.9.2 (c (n "botan") (v "0.9.2") (d (list (d (n "botan-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.4") (d #t) (k 2)))) (h "140p3jzpcyf77n7d8i34nidgmpyn4935rpn6sy9bqppf8lzvwmmp") (f (quote (("vendored" "botan-sys/vendored") ("no-std") ("default") ("botan3" "botan-sys/botan3"))))))

(define-public crate-botan-0.10.0 (c (n "botan") (v "0.10.0") (d (list (d (n "botan-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.5") (d #t) (k 2)))) (h "09shzpl22vxiqlr60jlycild5cbwhijgcndqxxbnlcrapss8w56a") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "botan-sys/no-std") ("default") ("botan3" "botan-sys/botan3"))))))

(define-public crate-botan-0.10.1 (c (n "botan") (v "0.10.1") (d (list (d (n "botan-sys") (r "^0.10.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.5") (d #t) (k 2)))) (h "12y7ramfv04x877hkqwlql43ihik9izcvxlbkzl7yy2q10v2339y") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "botan-sys/no-std") ("default") ("botan3" "botan-sys/botan3")))) (r "1.58")))

(define-public crate-botan-0.10.2 (c (n "botan") (v "0.10.2") (d (list (d (n "botan-sys") (r "^0.10.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.5") (d #t) (k 2)))) (h "0sfifqcxlqmpz6zv13dnsdzcjl7hnh7fwdbcq090vqvg8p93hrh6") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "botan-sys/no-std") ("default") ("botan3" "botan-sys/botan3")))) (r "1.58")))

(define-public crate-botan-0.10.3 (c (n "botan") (v "0.10.3") (d (list (d (n "botan-sys") (r "^0.10.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.5") (d #t) (k 2)))) (h "1vzl5pdysh848zpphsgvj9c40zdi3ynl32zzixsd8vg4vaflhb49") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "botan-sys/no-std") ("default") ("botan3" "botan-sys/botan3")))) (r "1.58")))

(define-public crate-botan-0.10.4 (c (n "botan") (v "0.10.4") (d (list (d (n "botan-sys") (r "^0.10.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.5") (d #t) (k 2)))) (h "0x8i5a9s1jp64rzi3b0rrb5fh4xcqb1xpasn3h340nsvqr5f2a4p") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "botan-sys/no-std") ("default") ("botan3" "botan-sys/botan3")))) (r "1.58")))

(define-public crate-botan-0.10.5 (c (n "botan") (v "0.10.5") (d (list (d (n "botan-sys") (r "^0.10.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.5") (d #t) (k 2)))) (h "09qg39nq9yf692q3vqkp9d9smdyqf94f5f1hzw9ahv2wsypafbna") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "botan-sys/no-std") ("default") ("botan3" "botan-sys/botan3")))) (r "1.58")))

(define-public crate-botan-0.10.6 (c (n "botan") (v "0.10.6") (d (list (d (n "botan-sys") (r "^0.10.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.5") (d #t) (k 2)))) (h "1ccbn29wf68xxrrbn8ji2r0jd5xnb52169apiprqlbdi5mm5nrq4") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "botan-sys/no-std") ("default") ("botan3" "botan-sys/botan3")))) (r "1.58")))

(define-public crate-botan-0.10.7 (c (n "botan") (v "0.10.7") (d (list (d (n "botan-sys") (r "^0.10.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "wycheproof") (r "^0.5") (d #t) (k 2)))) (h "0gn5aznnaxwlf2500q5dk9c24sgy7dasqqzql7w86s1w3apq201m") (f (quote (("vendored" "botan-sys/vendored") ("no-std" "botan-sys/no-std") ("default") ("botan3" "botan-sys/botan3")))) (r "1.58")))

