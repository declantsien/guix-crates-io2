(define-module (crates-io bo ta botanist) #:use-module (crates-io))

(define-public crate-botanist-0.1.0 (c (n "botanist") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.5") (d #t) (k 0)) (d (n "juniper") (r "^0.14.2") (d #t) (k 0)))) (h "1g64xn4jbrkwg4bxkpdaxdwdmn38dhmn1fi34nwlsa7fhrnk7mc4")))

