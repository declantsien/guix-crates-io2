(define-module (crates-io bo rt bortnichuk_my_crate) #:use-module (crates-io))

(define-public crate-bortnichuk_my_crate-0.1.0 (c (n "bortnichuk_my_crate") (v "0.1.0") (h "1vl5d2cyw5gylqa7wsnakk3qz5z11rjh3mmi40q37c4w6950dh6p")))

(define-public crate-bortnichuk_my_crate-0.2.0 (c (n "bortnichuk_my_crate") (v "0.2.0") (h "110xn4ff8xxlyisj8ivnaj9h7xnjs0p8zxcp0gicbjq18rwhbipv")))

