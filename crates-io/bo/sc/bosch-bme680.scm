(define-module (crates-io bo sc bosch-bme680) #:use-module (crates-io))

(define-public crate-bosch-bme680-0.1.0 (c (n "bosch-bme680") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "1000z4l6g5gl90mxh2zhjck6zrcl87yhq82rh7hm6pwyj70yb2lb")))

(define-public crate-bosch-bme680-0.1.1 (c (n "bosch-bme680") (v "0.1.1") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "0y31xxjdvf9zgzdkjsjr3llwbgvdx6byn238flrcykwv5qnf8iww")))

(define-public crate-bosch-bme680-1.0.0 (c (n "bosch-bme680") (v "1.0.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "08rw6v1hzvgnzwj1p4cm22dfacw220na2lq8vii0vqrw345hqbrf") (y #t)))

(define-public crate-bosch-bme680-1.0.1 (c (n "bosch-bme680") (v "1.0.1") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "0qsz95f95vhn36wmwhh4dcgpy8x0frhn7zyh3pdvz59wjk74iw8r")))

(define-public crate-bosch-bme680-1.0.2 (c (n "bosch-bme680") (v "1.0.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "0m8a2s7qpr9dpgqrh6yhx42cr85i9j2hhfjzb9sxz9dx46mpl05r")))

