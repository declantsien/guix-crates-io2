(define-module (crates-io bo uq bouquin) #:use-module (crates-io))

(define-public crate-bouquin-0.1.0 (c (n "bouquin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "0parqz4vhqajiql04ai5jmr0rc9k9mgi7w404d9ab4dwcimyd7rv")))

(define-public crate-bouquin-0.1.1 (c (n "bouquin") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)))) (h "0y57lkid2r6l5ld73z0qs5pvld2z0iy8cjyaz8s4dynshy1jzsh2")))

