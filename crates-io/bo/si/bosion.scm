(define-module (crates-io bo si bosion) #:use-module (crates-io))

(define-public crate-bosion-1.0.0 (c (n "bosion") (v "1.0.0") (d (list (d (n "gix") (r "^0.38.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "19q1igdfvv2j7har6bbd9g0as1wlydrgibzw04zcx5n0wsixzncm") (f (quote (("std") ("reproducible") ("default" "git" "reproducible" "std")))) (s 2) (e (quote (("git" "dep:gix")))) (r "1.64.0")))

(define-public crate-bosion-1.0.1 (c (n "bosion") (v "1.0.1") (d (list (d (n "gix") (r "^0.48") (o #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "1d3mmadl30p19nswh2hzjdw09akdh95ffmi6vw0p30hrgkggmg49") (f (quote (("std") ("reproducible") ("default" "git" "reproducible" "std")))) (s 2) (e (quote (("git" "dep:gix")))) (r "1.64.0")))

(define-public crate-bosion-1.0.2 (c (n "bosion") (v "1.0.2") (d (list (d (n "gix") (r "^0.55.2") (o #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "005g406dm81mna001qyg9rpz2rnzmkk7vmgjygbikjs83znl4293") (f (quote (("std") ("reproducible") ("default" "git" "reproducible" "std")))) (s 2) (e (quote (("git" "dep:gix")))) (r "1.64.0")))

(define-public crate-bosion-1.0.3 (c (n "bosion") (v "1.0.3") (d (list (d (n "gix") (r "^0.62.0") (o #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "11zr3n6qr3bdy73xm4sc4ymb93r0h1sci59v09wg8rr31dlcv4b2") (f (quote (("std") ("reproducible") ("default" "git" "reproducible" "std")))) (s 2) (e (quote (("git" "dep:gix")))) (r "1.64.0")))

(define-public crate-bosion-1.1.0 (c (n "bosion") (v "1.1.0") (d (list (d (n "gix") (r "^0.62.0") (f (quote ("revision"))) (o #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "0vhfq2r7pphwh2xfakx4lrph5013k5f9m0092ijihz4w5fy0bsh3") (f (quote (("std") ("reproducible") ("default" "git" "reproducible" "std")))) (s 2) (e (quote (("git" "dep:gix")))) (r "1.64.0")))

