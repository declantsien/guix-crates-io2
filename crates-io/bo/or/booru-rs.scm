(define-module (crates-io bo or booru-rs) #:use-module (crates-io))

(define-public crate-booru-rs-0.1.0 (c (n "booru-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fmmzr8sn7bxym36cj8c7xz8fqqdv61i6ahlyxdbl4c5q4fbx414")))

(define-public crate-booru-rs-0.1.1 (c (n "booru-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f361xf2x51rs46ifj34w0g8w9p2f7k96nainph6d89yjqdygfic")))

(define-public crate-booru-rs-0.1.2 (c (n "booru-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nbcj5bb53libxgx77psghsbrif4j0ja33gsirq9idzdpz4yn8v9")))

(define-public crate-booru-rs-0.1.3 (c (n "booru-rs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z88vbrax73f6ak76pqyqr3kr7c25x60bhz32nkvkqj7fj7qh5j8")))

(define-public crate-booru-rs-0.2.0 (c (n "booru-rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zbiz1l2yshqhihvibkykwixivgyp8fhv4w1kqssqcspkxxzjgly")))

(define-public crate-booru-rs-0.2.1 (c (n "booru-rs") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sqvb8d5hj42b8957vkglxy79wslncxspzpx58rq4vm442lng79g")))

(define-public crate-booru-rs-0.2.2 (c (n "booru-rs") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yx4sl89psiyrfvb06phdwb408x08vm19gz9i8apd3nh9jg1d299")))

(define-public crate-booru-rs-0.2.3 (c (n "booru-rs") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ygzwww2jx5jm0vxibwwwlx232m3kclphf65ii9asmxy56j4qzk9")))

(define-public crate-booru-rs-0.2.4 (c (n "booru-rs") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vmb661r4j756xgmmf2ih9wgi9gvgncmj613ahvjglcsvr5ljidq")))

(define-public crate-booru-rs-0.2.5 (c (n "booru-rs") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1aspwr1gnk2dfyk50mzisfyfklh0410v0hmmsijrhmgnhl3j042y")))

(define-public crate-booru-rs-0.2.6 (c (n "booru-rs") (v "0.2.6") (d (list (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0763fz4i2ygxzwxawdbp340a81cpk6m0754624dfmdnmk6r6pi7j")))

(define-public crate-booru-rs-0.2.7 (c (n "booru-rs") (v "0.2.7") (d (list (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03l59cvx9hndjkh98r311fgphw2j9rcxjfvkp7npnkzc36rag673")))

(define-public crate-booru-rs-0.2.8 (c (n "booru-rs") (v "0.2.8") (d (list (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vhd7k69xi2c4qz07kl83m336cinzp6nwhaplcjqlr1ww4qs9kjm")))

