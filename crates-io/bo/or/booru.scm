(define-module (crates-io bo or booru) #:use-module (crates-io))

(define-public crate-booru-0.3.0 (c (n "booru") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xx08jl1zjn038fyrhahixl02df2pgyqpp4m4ll4j78xpzpb1rkg")))

(define-public crate-booru-0.3.1 (c (n "booru") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z95p1cnshv4dq6jvd4cx6gr3a2v1ksi4bxiz1kgbhl7zsvr7yiw")))

(define-public crate-booru-0.3.2 (c (n "booru") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hgfs7gbkv1rwg3xrsnca531j8srr6m2694iw0y3jsg316v5ayj6")))

(define-public crate-booru-0.3.3 (c (n "booru") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l2yjskakrwism789knhiwhr1cmw5chfsr28b7dgbpckza43khgs")))

(define-public crate-booru-0.3.4 (c (n "booru") (v "0.3.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rnd31j4qjmisrvjdvijrgk1qmp07q2fskvqc6zsyivv8qhj2288")))

(define-public crate-booru-0.3.5 (c (n "booru") (v "0.3.5") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "169jskmqqqmxm6nkmzp00xbn99vngjvjm52yhg7xrl81dbvlwpan")))

(define-public crate-booru-0.3.6 (c (n "booru") (v "0.3.6") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "00qrzkiyldpqhmm7l7dpw6id1hd0xn0rgx1yw08c61kvd8x2gfl6")))

(define-public crate-booru-0.3.7 (c (n "booru") (v "0.3.7") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "13znk62ir9jy5x7kcyl8z8fwvf8p00a3vz1rjmzklkngi72a5zig")))

(define-public crate-booru-0.3.8 (c (n "booru") (v "0.3.8") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "itoa") (r "^1.0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0i1ph0h9wwac4rnrb87mb4ldhlrpnmy0lpmp67spwiqxzc8m02df")))

(define-public crate-booru-0.4.0 (c (n "booru") (v "0.4.0") (d (list (d (n "itoa") (r "^1.0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1h1r6h1m9znb3kaxfi0rmrmmas9pfy1vzp2bzskgf4vsclmrbysa") (r "1.75.0")))

(define-public crate-booru-0.4.1 (c (n "booru") (v "0.4.1") (d (list (d (n "itoa") (r "^1.0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0pppjfq26adqr50cpinw4i8fpdy2f8i3za6wk8cn90smmkdr1pcf") (r "1.75.0")))

(define-public crate-booru-0.5.0 (c (n "booru") (v "0.5.0") (d (list (d (n "itoa") (r "^1.0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "15srb9dfc5ppdqw5g2xwf4qyd7iv83jc59hf29k32hvxdj91lc0s") (r "1.75.0")))

