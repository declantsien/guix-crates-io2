(define-module (crates-io bo j- boj-cli) #:use-module (crates-io))

(define-public crate-boj-cli-0.0.1 (c (n "boj-cli") (v "0.0.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dizic1z7fp7s685693p19cm32pdkrryz482vmwfzll7ks48ln1j")))

