(define-module (crates-io bo tt bottymcbottyface) #:use-module (crates-io))

(define-public crate-bottymcbottyface-0.1.1 (c (n "bottymcbottyface") (v "0.1.1") (d (list (d (n "clap") (r "^2.13") (d #t) (k 0)) (d (n "hiirc") (r "^0.4") (d #t) (k 0)))) (h "16bcxp2xsmmbb847nknayi6bc9925v43n0wgbv0npfjj50w470pz")))

(define-public crate-bottymcbottyface-0.1.2 (c (n "bottymcbottyface") (v "0.1.2") (d (list (d (n "clap") (r "^2.13") (d #t) (k 0)) (d (n "hiirc") (r "^0.4") (d #t) (k 0)))) (h "07a0qkhsyvd96zxyj0ia9kl8qsiqql57bxbkkl8zds00zy71sqji")))

(define-public crate-bottymcbottyface-0.1.3 (c (n "bottymcbottyface") (v "0.1.3") (d (list (d (n "clap") (r "^2.13") (d #t) (k 0)) (d (n "hiirc") (r "^0.4") (d #t) (k 0)))) (h "0g9qdqknrv7zb78h6rrqifi3z1jy2fq8sjcyz2bqmsdnp5ay557j")))

