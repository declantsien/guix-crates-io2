(define-module (crates-io bo tt bottles) #:use-module (crates-io))

(define-public crate-bottles-0.1.0 (c (n "bottles") (v "0.1.0") (h "1gdgcvknfaw6bw2kl4n9ck6c41y1h0rp9fgs2va8qk7f7wcrqg6k")))

(define-public crate-bottles-0.1.1 (c (n "bottles") (v "0.1.1") (h "0r16zycd0m7lk6hyb3f98581351msif5lbhfys05dy33n0m8m96n")))

