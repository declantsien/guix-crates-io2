(define-module (crates-io bo tt bottomify) #:use-module (crates-io))

(define-public crate-bottomify-0.2.0 (c (n "bottomify") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0aqmvxkif94zmzsi3ysn87i8vrs6gy6gvi1z1160ygiz83z8kn4r")))

(define-public crate-bottomify-1.0.0 (c (n "bottomify") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0cx0scvfprmqd177m4qibnhx1lz0iz29jnninx2fpk3mfvhsk7a0")))

(define-public crate-bottomify-1.1.0 (c (n "bottomify") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 1)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)))) (h "1mzd5km1cklkwk982kjzxnsi4432fr2ccm9gbvygb2aw344wl9k6")))

(define-public crate-bottomify-1.1.1 (c (n "bottomify") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 1)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)))) (h "17bf8czvg7ciicviihfsd1ngpykz1wn93nk7mkyiai02ln7bsf4n")))

(define-public crate-bottomify-1.2.0 (c (n "bottomify") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)))) (h "0s1p877fdp73br61l58ln4plz8k42jgapwhnx2qrfjzpwcvgfdk8")))

