(define-module (crates-io bo tt bottle) #:use-module (crates-io))

(define-public crate-bottle-0.1.0 (c (n "bottle") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 2)))) (h "1yq1h1dgi6xd1mnmv3652i9jijidl6fr4jgynyz3bgnxclhgqbmk")))

(define-public crate-bottle-1.0.0-alpha (c (n "bottle") (v "1.0.0-alpha") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0i3qjawilvnb7yms1gfgajh555qhsdpw5vrb55mllh9vgl0xqm4c")))

(define-public crate-bottle-1.1.0-alpha (c (n "bottle") (v "1.1.0-alpha") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1jmlx15129mprdss4qz0jz0jys6ra7m8skzgkcsd8fmac46f1mhw")))

