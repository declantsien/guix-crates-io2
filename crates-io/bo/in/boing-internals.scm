(define-module (crates-io bo in boing-internals) #:use-module (crates-io))

(define-public crate-boing-internals-0.1.0 (c (n "boing-internals") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1py67xddbsk3rkr7ky7hdf2hcb1zfd5klba6y19ism4qy7hw7y7c")))

