(define-module (crates-io bo in boingboingboing) #:use-module (crates-io))

(define-public crate-boingboingboing-0.1.0 (c (n "boingboingboing") (v "0.1.0") (h "1c65z3hlq3szxjwsi66g38fgz9605ww2ihmxpq2myvn8mwicwy22")))

(define-public crate-boingboingboing-0.1.1 (c (n "boingboingboing") (v "0.1.1") (h "04l3hkmikv6sq1iq5j97wxxa6pzw6s539nm8rwknj9v6fy42fwjr")))

(define-public crate-boingboingboing-0.1.2 (c (n "boingboingboing") (v "0.1.2") (h "1p1718a1b54wyl8fci454v6ghs1myc99cqfh791h6qqqss7612zw")))

