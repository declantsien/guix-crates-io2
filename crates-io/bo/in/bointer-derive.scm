(define-module (crates-io bo in bointer-derive) #:use-module (crates-io))

(define-public crate-bointer-derive-0.1.0 (c (n "bointer-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "1paq8rkfrvqxqhbbzifpg24wkjzfmfcgdi1pxpvwgam0jji45m14") (r "1.56")))

(define-public crate-bointer-derive-0.2.0 (c (n "bointer-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "1i7hn1izrnz566bi4hi9na9h9zjhn05xpzmspsi5s3br36piainc") (r "1.66")))

