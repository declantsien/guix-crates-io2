(define-module (crates-io bo in bointer) #:use-module (crates-io))

(define-public crate-bointer-0.1.0 (c (n "bointer") (v "0.1.0") (d (list (d (n "bointer-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1k8zlf06sl0x07cxk2mwh984b6863g8laxzjn6vj9wwv7as02i6c") (f (quote (("std" "alloc") ("derive" "bointer-derive") ("default" "std" "derive") ("alloc")))) (r "1.56")))

(define-public crate-bointer-0.2.0 (c (n "bointer") (v "0.2.0") (d (list (d (n "bointer-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "175ivkjxifhyrzzvykbvqvhb15fighd8vizg8c0rdb0bs538ls1n") (f (quote (("std" "alloc") ("derive" "bointer-derive") ("default" "std" "derive") ("alloc")))) (r "1.66")))

