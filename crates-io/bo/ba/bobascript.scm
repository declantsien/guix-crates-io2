(define-module (crates-io bo ba bobascript) #:use-module (crates-io))

(define-public crate-bobascript-0.1.0 (c (n "bobascript") (v "0.1.0") (d (list (d (n "bobascript-parser") (r "^0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s9bl8rfjp32wsbyk5jip9p3va83ns1s7bw1v6mhpjw5af6b42my") (f (quote (("super_debug" "debug") ("debug"))))))

(define-public crate-bobascript-0.1.1 (c (n "bobascript") (v "0.1.1") (d (list (d (n "bobascript-parser") (r "^0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14fhgczg0zjn9p3ya8fn8i3mddvl06wsv9dqpxlrdwwaz9dh0iw5") (f (quote (("super_debug" "debug") ("debug")))) (y #t)))

(define-public crate-bobascript-0.1.2 (c (n "bobascript") (v "0.1.2") (d (list (d (n "bobascript-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rkdm1lfjbxwwpyl0asrwxbylddmyds9kpp4y7p7f7rphib68h7s") (f (quote (("super_debug" "debug") ("debug"))))))

(define-public crate-bobascript-0.1.3 (c (n "bobascript") (v "0.1.3") (d (list (d (n "bobascript-parser") (r "^0.1.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fla42kf5dq7ni9hvvl2kfm50scqywvr707rg3sns81f6bh9szi6") (f (quote (("super_debug" "debug") ("debug"))))))

(define-public crate-bobascript-0.1.4 (c (n "bobascript") (v "0.1.4") (d (list (d (n "bobascript-parser") (r "^0.1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vfjmifnjrccwrk7f5brrlrmfx781j2h919qgp3k9k5pcr618656") (f (quote (("super_debug" "debug") ("debug"))))))

