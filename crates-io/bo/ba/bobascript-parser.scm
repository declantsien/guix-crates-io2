(define-module (crates-io bo ba bobascript-parser) #:use-module (crates-io))

(define-public crate-bobascript-parser-0.1.0 (c (n "bobascript-parser") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xfnaghimm5l85q94h0gaydgpc0w0arl802phjjzdivbkd6ad9m2")))

(define-public crate-bobascript-parser-0.1.1 (c (n "bobascript-parser") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12ywnca4ba44y5wpdwyypbrs7y9b14fgb01rlxj8crwlw3dafw1n")))

(define-public crate-bobascript-parser-0.1.2 (c (n "bobascript-parser") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ln7dm5vxpj77mj6sdfkdha82svds1x8340xm4jy42kx2cz699vz")))

(define-public crate-bobascript-parser-0.1.3 (c (n "bobascript-parser") (v "0.1.3") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vw9df6zdw3fg8qq8kn9rz4zc4l4sac08v77xi9zpbrc5g04hf1m")))

(define-public crate-bobascript-parser-0.1.4 (c (n "bobascript-parser") (v "0.1.4") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17zmg37f8a1wx5pnvrpazxj1mg36hrg5q2nkbnsh992k69ssqx4k")))

