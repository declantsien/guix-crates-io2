(define-module (crates-io bo ba boba) #:use-module (crates-io))

(define-public crate-boba-0.0.0 (c (n "boba") (v "0.0.0") (h "1kvfv2855nkprb5cf02ckkr83ha529sn71g55hy9z7r2fkjrmza1")))

(define-public crate-boba-3.0.0 (c (n "boba") (v "3.0.0") (d (list (d (n "bstr") (r "^0.2") (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0452ga8hmp4b03nsms5mkn0v4ksl6r5dvf2ygq3791km2pmnhbmr") (f (quote (("std" "bstr/std") ("default" "std"))))))

(define-public crate-boba-4.0.0 (c (n "boba") (v "4.0.0") (d (list (d (n "bstr") (r "^0.2") (k 0)) (d (n "bubblebabble") (r "^0.1") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0igpvsdyzaii8f6nlqj97pmyf95zl9a5fh4762ww5n716lfpgxh7") (f (quote (("std") ("default" "std"))))))

(define-public crate-boba-4.1.0 (c (n "boba") (v "4.1.0") (d (list (d (n "bstr") (r "^0.2") (k 0)) (d (n "bubblebabble") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0csfnpa0wbv8hxhm332cc9jsn6ia69q6s8villbxhjlcp25g9zlb") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4.1.1 (c (n "boba") (v "4.1.1") (d (list (d (n "bstr") (r "^0.2") (k 0)) (d (n "bubblebabble") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0c9v5y7ni2za300g5ic9zw3cjkls2f4i26kkqimr60yrpr3x0lg1") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4.1.2 (c (n "boba") (v "4.1.2") (d (list (d (n "bstr") (r "^0.2") (k 0)) (d (n "bubblebabble") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1kj1mfhybqbq6nd9f4i74ydmmw6pbh93zh35v9hz14q6dl4sbx46") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4.2.0 (c (n "boba") (v "4.2.0") (d (list (d (n "bstr") (r "^0.2.4") (k 0)) (d (n "bubblebabble") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "08v0qx9b4a7m1ajgnl0kw36pp9q468vz4n3hlj8r125gly88bska") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4.3.0 (c (n "boba") (v "4.3.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1rsccnlrd97arvlbbirxq3fdhkwcn3mlza9pxg90l777zbyaswgw") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4.3.1 (c (n "boba") (v "4.3.1") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1sz6cygf3vl6x0w0fa222m9g789r3amsmvjn1ri6nzms509q4h1w") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-5.0.0 (c (n "boba") (v "5.0.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "02hbi9jn76ri40vlnjac59yia1nsa29bxkxqfa81wl6ipp13cb17") (f (quote (("std") ("default" "std"))))))

