(define-module (crates-io bo wt bowtie) #:use-module (crates-io))

(define-public crate-bowtie-0.2.0 (c (n "bowtie") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.45.0") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)))) (h "1mdkw1lnxvqvx2xnbx5z5g9vxi23j3lf3yxk1n46xf7ii6hwh93m")))

(define-public crate-bowtie-0.2.1 (c (n "bowtie") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)))) (h "0gp8vb6p90mbwkgcsvyjm15x76av0fz99l00dgc4vyz1jqv4fkv8")))

(define-public crate-bowtie-0.2.2 (c (n "bowtie") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)))) (h "0knq9x80pd4nml1g9vpk0lpg2wgxd4qd871xc02w4x6zwayh7akh")))

