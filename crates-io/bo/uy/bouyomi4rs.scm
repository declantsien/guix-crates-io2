(define-module (crates-io bo uy bouyomi4rs) #:use-module (crates-io))

(define-public crate-bouyomi4rs-0.2.0 (c (n "bouyomi4rs") (v "0.2.0") (h "1pl2y635b8fpwh7clc3202i2hh1msaw4xkk1kg13qwb2ijpy6r0l")))

(define-public crate-bouyomi4rs-0.2.1 (c (n "bouyomi4rs") (v "0.2.1") (h "1cyspr78gfvigxxwrcic6qf89v3r7zyvymhpgkp67mfn2b0spijn")))

