(define-module (crates-io bo kk bokken-runtime) #:use-module (crates-io))

(define-public crate-bokken-runtime-0.1.0 (c (n "bokken-runtime") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "bpaf") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0byylp9b1kkzfn4mqagfzgij1iwlvavfwxy5z2bnpnjq7bkp0yyh")))

