(define-module (crates-io bo ur bourse-book) #:use-module (crates-io))

(define-public crate-bourse-book-0.1.0 (c (n "bourse-book") (v "0.1.0") (h "151p2ldnn1hcvmr1gbxra2s8gfzs2ky13vrw1qrxnanab5bw2bv1")))

(define-public crate-bourse-book-0.1.1 (c (n "bourse-book") (v "0.1.1") (h "0clsjrpdqbf7yn6rnkdviskksvlrga0j61z3gxmdan4k9aahrcc3")))

(define-public crate-bourse-book-0.1.2 (c (n "bourse-book") (v "0.1.2") (h "1177aiackl63j6lday2hhfprskha0ihqjw3sk3kndx84916ymm6i")))

(define-public crate-bourse-book-0.2.0 (c (n "bourse-book") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0adsglfrldhxm4pxgvf6jnr24i2gpw5xsz1q1gj6ys2fzavhwkja")))

(define-public crate-bourse-book-0.3.0 (c (n "bourse-book") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0k6z42dq3n4vb2c28zclj3bc845ln00lfk8rc3ncmz9gr6l7azqq")))

(define-public crate-bourse-book-0.3.1 (c (n "bourse-book") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0hqszh5yhp2v43l4w6pxmyxk5gsw7jf03m11fm4j5bmjvcd0s86n")))

(define-public crate-bourse-book-0.4.0 (c (n "bourse-book") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0438zchnsb57gdg0a3ssdfyqw59a0y74ganvpkhbic0hwybh6cpi")))

