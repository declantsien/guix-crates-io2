(define-module (crates-io bo ur bourse-macros) #:use-module (crates-io))

(define-public crate-bourse-macros-0.1.0 (c (n "bourse-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d99incn6h6mwcx64jmxwkgigsd8wvmlzmfwp3q5zsdlkp5xm879")))

(define-public crate-bourse-macros-0.1.1 (c (n "bourse-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pzx0v5apwfxx84a29p2y6kcmpc2j0m8amfvp6mz7andkq7rw0s2")))

(define-public crate-bourse-macros-0.1.2 (c (n "bourse-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zlqlvr92p6g14385wrjlgprb5jnqskzncqsvrhrx1pymqdzk69n")))

(define-public crate-bourse-macros-0.2.0 (c (n "bourse-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0biz85298bxd3yp95y8cjzk2xv0fpz08l25ijwxm10456jr9hdbh")))

(define-public crate-bourse-macros-0.3.0 (c (n "bourse-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n0vycg5n0qivhkj6m40lw9ngsiyfkfpxm25d1klzr8ljfbm42hn")))

(define-public crate-bourse-macros-0.3.1 (c (n "bourse-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zkpr5ynm46czj32nhl811ic2c0p2776wki4sgrvi4c8065p27sx")))

(define-public crate-bourse-macros-0.4.0 (c (n "bourse-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0569cjn8qngfbi71qajwkyvzzlrw4lrggpa5bcfqwcscy16fh25s")))

