(define-module (crates-io bo ur bourse-de) #:use-module (crates-io))

(define-public crate-bourse-de-0.1.0 (c (n "bourse-de") (v "0.1.0") (d (list (d (n "bourse-book") (r "^0.1.0") (d #t) (k 0)) (d (n "bourse-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "kdam") (r "^0.3.0") (d #t) (k 0)))) (h "18bcvj2inj04qbkrxbqv49siaxi8pnyb21ngay2mzfy92pybq434")))

(define-public crate-bourse-de-0.1.1 (c (n "bourse-de") (v "0.1.1") (d (list (d (n "bourse-book") (r "^0.1.1") (d #t) (k 0)) (d (n "bourse-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "kdam") (r "^0.3.0") (d #t) (k 0)))) (h "0hn5fqqic36rxy29afp9kj8g91qc4yvlny8ar6d1p7gqf6wslfh4")))

(define-public crate-bourse-de-0.1.2 (c (n "bourse-de") (v "0.1.2") (d (list (d (n "bourse-book") (r "^0.1.2") (d #t) (k 0)) (d (n "bourse-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "kdam") (r "^0.3.0") (d #t) (k 0)))) (h "1hcrrqc304h2hay6i7187mf9191v4zlwlm812xcfl0ndb3r7icsr")))

(define-public crate-bourse-de-0.2.0 (c (n "bourse-de") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "bourse-book") (r "^0.2.0") (d #t) (k 0)) (d (n "bourse-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "kdam") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "1sk9mmr465i7ca62a7mwqbdqg9zs0gw3rs88s8sjri3d024rlnhm")))

(define-public crate-bourse-de-0.3.0 (c (n "bourse-de") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "bourse-book") (r "^0.3.0") (d #t) (k 0)) (d (n "bourse-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "kdam") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "0smwyc4graqsrsl0p0g17r1xv7y3gkfjk59ag61lmqm24rhpz0ma")))

(define-public crate-bourse-de-0.3.1 (c (n "bourse-de") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "bourse-book") (r "^0.3.1") (d #t) (k 0)) (d (n "bourse-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "kdam") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "0x6r511xj4rrpdhppqi2hg42j7kal127kimy7r8j7vla6axfccl1")))

(define-public crate-bourse-de-0.4.0 (c (n "bourse-de") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "bourse-book") (r "^0.4.0") (d #t) (k 0)) (d (n "bourse-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "kdam") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "19gif7ckvdbi6ac8z1n9ii5jcx1xx6gzzpjqhwn60xxd1ra2hd45")))

