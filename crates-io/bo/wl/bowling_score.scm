(define-module (crates-io bo wl bowling_score) #:use-module (crates-io))

(define-public crate-bowling_score-0.1.0 (c (n "bowling_score") (v "0.1.0") (d (list (d (n "bscore") (r "^0.1") (d #t) (k 0)))) (h "1r37g251110byn9ldnfr5r9bxg62cl7qkgk4ncc9lpgsvhy6wx3r") (y #t)))

(define-public crate-bowling_score-1.0.0 (c (n "bowling_score") (v "1.0.0") (d (list (d (n "bscore") (r "^1.0") (d #t) (k 0)))) (h "0v11rj07n7qw2lw7sz6d3wfwmwcgkdgia4dzxvj1xwnxgjsvq0fb") (y #t)))

(define-public crate-bowling_score-1.0.1 (c (n "bowling_score") (v "1.0.1") (d (list (d (n "bscore") (r "^1.0") (d #t) (k 0)))) (h "0qg2bz1jpkga1aagriahs4c252h95dbaz5linv56bxs28fi419is")))

(define-public crate-bowling_score-1.1.1 (c (n "bowling_score") (v "1.1.1") (d (list (d (n "bscore") (r "^1.1") (d #t) (k 0)))) (h "1p75xlcbwhiaxz86743w8zprf8ar0qjl2sip5y0b2n6kxpq8yiba")))

(define-public crate-bowling_score-1.2.1 (c (n "bowling_score") (v "1.2.1") (d (list (d (n "bscore") (r "^1.2") (d #t) (k 0)))) (h "1rd847nmsbswaw220i5z7jzp93l20x9flkfvq4vvwnc6kk2ms980")))

