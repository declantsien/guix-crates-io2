(define-module (crates-io bo xc boxcar) #:use-module (crates-io))

(define-public crate-boxcar-0.1.0 (c (n "boxcar") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0iyyjklyxy31hiwk1ykar1qsj73ja7vgr3d0i6c45mrwrc9rdj9q")))

(define-public crate-boxcar-0.2.0 (c (n "boxcar") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1n75c4yg01srz3civ5xvzgbg8fvcbd43c7isbbpg5d8nzbmd2i41") (y #t)))

(define-public crate-boxcar-0.2.1 (c (n "boxcar") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1dfln7kh2qhgh0p6n08lg8y499gfc7gddzlwb2irqv5d11lmw82r")))

(define-public crate-boxcar-0.2.2 (c (n "boxcar") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1k96jm9c77cj26dfs4yd0qbbay6vnhkp6n98dlbswifk35anrsqm")))

(define-public crate-boxcar-0.2.3 (c (n "boxcar") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0nw2a1pxf8gl121g0h2pxf3l33inasrc1nyi8ssyqsk07g0m594v")))

(define-public crate-boxcar-0.2.4 (c (n "boxcar") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0abb7w0jx7jgyn5i2xyzrmglfy8hqa0sdayivk06k1yvfm4h25p5")))

(define-public crate-boxcar-0.2.5 (c (n "boxcar") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "13na1cy95hpacmlvh2g8pcq7ysdb1lpp2szg2wrszh8240rr02ji")))

