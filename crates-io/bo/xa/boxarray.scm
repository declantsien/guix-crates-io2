(define-module (crates-io bo xa boxarray) #:use-module (crates-io))

(define-public crate-boxarray-1.0.0 (c (n "boxarray") (v "1.0.0") (d (list (d (n "cargo-husky") (r "^1") (d #t) (k 2)))) (h "0q1992ll90hrlcf440sa4q36ks1c2cq9wndjf88fc7h0ciaa7ngf")))

(define-public crate-boxarray-1.0.1 (c (n "boxarray") (v "1.0.1") (d (list (d (n "cargo-husky") (r "^1") (d #t) (k 2)))) (h "1w5mnnf8maqzg0k16f2rs27ad1fpvwf5f340vhikpd36ac7qaxgx")))

(define-public crate-boxarray-1.1.0 (c (n "boxarray") (v "1.1.0") (d (list (d (n "cargo-husky") (r "^1") (d #t) (k 2)))) (h "0xzjin5dcdbyd2cjxad69kfdkkvb3448vq95izmy55ivzjx9r70r")))

(define-public crate-boxarray-1.2.0 (c (n "boxarray") (v "1.2.0") (d (list (d (n "cargo-husky") (r "^1") (d #t) (k 2)))) (h "0iqnshxr96riybxxz9jbgnygms7lxsbdvihpdrml2dg3x9hd8djj")))

(define-public crate-boxarray-1.2.1 (c (n "boxarray") (v "1.2.1") (d (list (d (n "cargo-husky") (r "^1") (d #t) (k 2)))) (h "04mg5ds37c6snpg1rk8rhg1d0g9pa2nqvxb2y3mymkxvg7wnzv89")))

(define-public crate-boxarray-1.3.0 (c (n "boxarray") (v "1.3.0") (d (list (d (n "cargo-husky") (r "^1") (d #t) (k 2)))) (h "1vs20h2pizz06yd6j6yl0ppg305bg6d9k0ahkpqh04nfkdckkik0")))

