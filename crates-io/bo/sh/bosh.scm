(define-module (crates-io bo sh bosh) #:use-module (crates-io))

(define-public crate-bosh-0.0.0 (c (n "bosh") (v "0.0.0") (d (list (d (n "bosh_compiler") (r "^0.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.0") (d #t) (k 0)))) (h "17lxcxfpvqyxzrf6wwb625scxljfx6g3kgdb6dk9gn17m0dbyaha")))

