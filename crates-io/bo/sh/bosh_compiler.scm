(define-module (crates-io bo sh bosh_compiler) #:use-module (crates-io))

(define-public crate-bosh_compiler-0.0.0 (c (n "bosh_compiler") (v "0.0.0") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.3.0") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0z11h9b1wm06a3difrx994f120kj36vm0dscna4slab5rdb1drk1")))

