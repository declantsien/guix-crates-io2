(define-module (crates-io bo sh bosh-rs) #:use-module (crates-io))

(define-public crate-bosh-rs-0.1.0 (c (n "bosh-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jp4526hlarg0nivq7miqwnrpj6q3s22vlindhq0gcbm4hv8rppc")))

(define-public crate-bosh-rs-0.2.0 (c (n "bosh-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sd4x70myh008j6zn7nz4lafiic37lb634zx9l1fnbs3ih4kxdvb")))

(define-public crate-bosh-rs-0.2.1 (c (n "bosh-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "06y197jbmpq7afmssdww9i7xlniricwfnjndg5nj9j9r3jkjq4ii")))

(define-public crate-bosh-rs-0.3.0 (c (n "bosh-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "read-from") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "069492x465lc6dx85h985g84smdq703bzdi6j6jvb7lcb59k3xih")))

(define-public crate-bosh-rs-0.4.0 (c (n "bosh-rs") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "read-from") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0akwdb379kmf74hwsprb1k91q2947rc7pqm98fxnrhfp1dda8bhz")))

