(define-module (crates-io bo tf botfair) #:use-module (crates-io))

(define-public crate-botfair-0.1.0 (c (n "botfair") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0qbakqzi9ls2qpdgqjx8dzcf724klmy9s5976sprz3x50j6zijji")))

(define-public crate-botfair-0.2.0 (c (n "botfair") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0sbqjl3zivai3m89rf5d964x9bf28fdwpnxzj89zc4kcl78jrd1f")))

(define-public crate-botfair-0.2.1 (c (n "botfair") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0gh2asyi25pkj5yisp4rkrs4rkc0nsapj5k6v7agm9agkld6fqh2")))

(define-public crate-botfair-0.3.0 (c (n "botfair") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "18hj7wrlpvyfmdnq2cyw0xkvkq6pwmlywswjrnjvlfzxi63466lm")))

(define-public crate-botfair-0.3.1 (c (n "botfair") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "11939si4nv975s4wabby7af0gqbag8jwxkah6n7rl96blq2jaydr")))

