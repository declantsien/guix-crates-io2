(define-module (crates-io bo ne bones_input) #:use-module (crates-io))

(define-public crate-bones_input-0.0.0 (c (n "bones_input") (v "0.0.0") (h "1w9fjlf9lciiixr62n0ydnn3ls82dyq0kgbcfsc4g6v48s42d398")))

(define-public crate-bones_input-0.1.0 (c (n "bones_input") (v "0.1.0") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 0)) (d (n "type_ulid") (r "^0.1.0") (d #t) (k 0)))) (h "0b6wy5f18ksiqfyfsmz0r5wn7q7cw6apr5vdch9xwwznaf4n6bpw")))

(define-public crate-bones_input-0.2.0 (c (n "bones_input") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bones_bevy_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "glam") (r "^0.23") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "type_ulid") (r "^0.2.0") (d #t) (k 0)))) (h "04j40x2l9ab2vf3b1ml20pqm4mkp0g33b84p41xcxp1sys2sicrl")))

