(define-module (crates-io bo ne bones_schema) #:use-module (crates-io))

(define-public crate-bones_schema-0.0.0 (c (n "bones_schema") (v "0.0.0") (h "130x573qr39hjyfwf2h3jwz3h5k83d3702gqns0mcyf22hw2dwsl")))

(define-public crate-bones_schema-0.3.0 (c (n "bones_schema") (v "0.3.0") (d (list (d (n "append-only-vec") (r "^0.1.3") (d #t) (k 0)) (d (n "bones_schema_macros") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "bones_utils") (r "^0.3") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3.31") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "humantime") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "sptr") (r "^0.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "19ppaws4b41ps3amk582s86djn8ji76ny5k0acvy5a127ckmgjyg") (f (quote (("derive" "bones_schema_macros") ("default" "derive" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:erased-serde" "ulid/serde" "bones_utils/serde") ("humantime" "dep:humantime" "serde") ("glam" "dep:glam"))))))

