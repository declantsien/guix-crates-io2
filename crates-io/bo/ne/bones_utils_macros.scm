(define-module (crates-io bo ne bones_utils_macros) #:use-module (crates-io))

(define-public crate-bones_utils_macros-0.0.0 (c (n "bones_utils_macros") (v "0.0.0") (h "1qlzzb4irr7vr6c780dz8l9i8mjcwrf2yd3i4anx61m1yjdihvgq")))

(define-public crate-bones_utils_macros-0.3.0 (c (n "bones_utils_macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "venial") (r "^0.5") (d #t) (k 0)))) (h "0s5r6hv6qkqjz0727gd4a1ljr5n3v6ns9841ngmkknj3f35v10p9")))

(define-public crate-bones_utils_macros-0.3.1 (c (n "bones_utils_macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "venial") (r "^0.5") (d #t) (k 0)))) (h "19kji6w3fgmnrd87197aa4p7aspagl1jygfj8hg96m188jf7kvvb")))

