(define-module (crates-io bo ne bones3_core) #:use-module (crates-io))

(define-public crate-bones3_core-0.1.0 (c (n "bones3_core") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (k 0)) (d (n "bones3_test_utils") (r "^0.1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1z9v5qf6py3w9lg5mis9fcc9vbnj4zavjdgkgrhyj6q9bgcl4ihj") (f (quote (("default"))))))

(define-public crate-bones3_core-0.2.0 (c (n "bones3_core") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.1") (k 0)) (d (n "bones3_test_utils") (r "^0.2.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0qqvkky4kdsrpb03dpr2w73c6k8pc731cmw8f86qwmh0gjfpdh0h") (f (quote (("default"))))))

(define-public crate-bones3_core-0.3.0 (c (n "bones3_core") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10.1") (k 0)) (d (n "bones3_test_utils") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0xg9dn446zqa3307mprnfp40sp1wy58va42ldaq414ij2k5ax969") (f (quote (("default"))))))

(define-public crate-bones3_core-0.4.0 (c (n "bones3_core") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11.0") (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "10vqql6n2wmjnx8xs51apjgabxjnhndvhixgnsj05wdsjwc6m13h") (f (quote (("default"))))))

