(define-module (crates-io bo ne bones_schema_macros) #:use-module (crates-io))

(define-public crate-bones_schema_macros-0.0.0 (c (n "bones_schema_macros") (v "0.0.0") (h "1lbs7qkaidsix9170lfyisn1kkicq35ms74h6zlln3hx3gqnflbz")))

(define-public crate-bones_schema_macros-0.3.0 (c (n "bones_schema_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "venial") (r "^0.5") (d #t) (k 0)))) (h "1zhxz9dqj27mwy2mzmmhrf884kfjwgmjqnsnlvrf9979w007fdzy")))

(define-public crate-bones_schema_macros-0.3.1 (c (n "bones_schema_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "venial") (r "^0.5") (d #t) (k 0)))) (h "1jh78cd7wfhdbpphcp034mak45jg4np12zaicjzksgms3ik20frq")))

