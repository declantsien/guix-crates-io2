(define-module (crates-io bo ne bones_lib) #:use-module (crates-io))

(define-public crate-bones_lib-0.0.0 (c (n "bones_lib") (v "0.0.0") (h "1lvpvvmp1f3p51kmbiszhwnmp30lmm2mxnzj08qvks60mglhfx8z")))

(define-public crate-bones_lib-0.1.0 (c (n "bones_lib") (v "0.1.0") (d (list (d (n "bones_asset") (r "^0.1.0") (d #t) (k 0)) (d (n "bones_bevy_utils") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bones_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "bones_input") (r "^0.1.0") (d #t) (k 0)) (d (n "bones_render") (r "^0.1.0") (d #t) (k 0)) (d (n "noise") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "type_ulid") (r "^0.1.0") (d #t) (k 0)))) (h "03zg4r1s8dyfbdsaszpb74hsyw2pblk4rdmj0a6r1cg5dnd6ph91") (s 2) (e (quote (("serde" "dep:serde" "bones_render/serde" "bones_ecs/serde") ("bevy" "bones_asset/bevy" "bones_render/bevy" "dep:bones_bevy_utils"))))))

(define-public crate-bones_lib-0.2.0 (c (n "bones_lib") (v "0.2.0") (d (list (d (n "bones_asset") (r "^0.2.0") (d #t) (k 0)) (d (n "bones_bevy_utils") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bones_ecs") (r "^0.2.0") (d #t) (k 0)) (d (n "bones_input") (r "^0.2.0") (d #t) (k 0)) (d (n "bones_render") (r "^0.2.0") (d #t) (k 0)) (d (n "noise") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "type_ulid") (r "^0.2.0") (d #t) (k 0)))) (h "16wvyi5nf33xv7mnmiqfddhpyqifar9i2lfn1qqnysxvb8cd9lgg") (s 2) (e (quote (("serde" "dep:serde" "bones_render/serde" "bones_ecs/serde") ("bevy" "bones_asset/bevy" "bones_render/bevy" "dep:bones_bevy_utils"))))))

(define-public crate-bones_lib-0.3.0 (c (n "bones_lib") (v "0.3.0") (d (list (d (n "bones_ecs") (r "^0.3") (d #t) (k 0)) (d (n "instant") (r "^0.1.12") (d #t) (k 0)))) (h "19jn815rdp8kwqcfls5kgchmlfi81r2nyan341dzbh681qyklv8h") (f (quote (("glam" "bones_ecs/glam") ("default"))))))

