(define-module (crates-io bo ne bones) #:use-module (crates-io))

(define-public crate-bones-0.0.1 (c (n "bones") (v "0.0.1") (h "1z02hjj076j4b8ba047q2jbdn3rnhzrb77yzbbabslr7x6qhhsna")))

(define-public crate-bones-0.1.0 (c (n "bones") (v "0.1.0") (h "0hlzvfj1l3i41mih8f61sb8r0x88md8rf7ayhb9cvhy4gkraa94f")))

(define-public crate-bones-1.0.0 (c (n "bones") (v "1.0.0") (h "19myzk8vim4yhhi6sy574biwxq0ad7sksm91wvif0lwsjb58fv35")))

