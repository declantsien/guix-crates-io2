(define-module (crates-io bo ne bones_scripting) #:use-module (crates-io))

(define-public crate-bones_scripting-0.0.0 (c (n "bones_scripting") (v "0.0.0") (h "0bzm3blxqvdz9kxxmf09fkpgcxrk81vz24ymdwb2ky04lrcb40vj")))

(define-public crate-bones_scripting-0.3.0 (c (n "bones_scripting") (v "0.3.0") (d (list (d (n "async-channel") (r "^1.9") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.11") (d #t) (k 0)) (d (n "bones_asset") (r "^0.3") (d #t) (k 0)) (d (n "bones_lib") (r "^0.3") (d #t) (k 0)) (d (n "gc-arena") (r "^0.5") (d #t) (k 0)) (d (n "gc-arena-derive") (r "^0.5") (d #t) (k 0)) (d (n "piccolo") (r "^0.3") (d #t) (k 0)) (d (n "piccolo") (r "^0.3") (d #t) (k 2)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "03qdlhyrki97005kvrq0vz87rfnnd628y3vxw33s3aq2883agkiw")))

