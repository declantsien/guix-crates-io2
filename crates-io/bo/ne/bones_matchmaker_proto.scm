(define-module (crates-io bo ne bones_matchmaker_proto) #:use-module (crates-io))

(define-public crate-bones_matchmaker_proto-0.0.0 (c (n "bones_matchmaker_proto") (v "0.0.0") (h "1sfd492nxgswcdymm00yhdcl12hww87x45vqbaf2169zljssscba")))

(define-public crate-bones_matchmaker_proto-0.1.0 (c (n "bones_matchmaker_proto") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f7gqi0j3vhfhhm5nsmj96r45vknymn5hbqjavsajkfv248fgxxz")))

(define-public crate-bones_matchmaker_proto-0.2.0 (c (n "bones_matchmaker_proto") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hwyv5hv4hjs7y0wbf3rgr3plhm408b2xijw2skqlw84vfcr3f2c")))

