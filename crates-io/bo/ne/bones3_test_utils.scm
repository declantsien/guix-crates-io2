(define-module (crates-io bo ne bones3_test_utils) #:use-module (crates-io))

(define-public crate-bones3_test_utils-0.1.0 (c (n "bones3_test_utils") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (k 0)))) (h "19xyjvd9b1kxrapl8br00dpg0jfn0d7h6jcamgx50s4rr2drsviz") (f (quote (("default"))))))

(define-public crate-bones3_test_utils-0.2.0 (c (n "bones3_test_utils") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.1") (k 0)))) (h "152ghs6nhcaywzmybi4n5mixk8872sbaa5f9rfmgli29z8idpl5a") (f (quote (("default"))))))

(define-public crate-bones3_test_utils-0.3.0 (c (n "bones3_test_utils") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10.1") (k 0)))) (h "0nw7ksq025c5b822gqvj94gc8n1ypszyhlask2nknb4v0d4h4npg") (f (quote (("default"))))))

