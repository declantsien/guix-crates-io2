(define-module (crates-io bo ne bones_bevy_utils) #:use-module (crates-io))

(define-public crate-bones_bevy_utils-0.1.0 (c (n "bones_bevy_utils") (v "0.1.0") (d (list (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "type_ulid") (r "^0.1.0") (d #t) (k 0)))) (h "0ghf55k3cl3sxagbh8zp84j6fpn12dhkpvw210qzv1il4r3c9559")))

(define-public crate-bones_bevy_utils-0.2.0 (c (n "bones_bevy_utils") (v "0.2.0") (d (list (d (n "bevy_ecs") (r "^0.10") (d #t) (k 0)) (d (n "type_ulid") (r "^0.2.0") (d #t) (k 0)))) (h "096qmfp43r1qj4vc1s2n9q1swp41cl595awhbhi1d5zg24rby962")))

