(define-module (crates-io bo ne bones_bevy_asset_macros) #:use-module (crates-io))

(define-public crate-bones_bevy_asset_macros-0.1.0 (c (n "bones_bevy_asset_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (k 0)))) (h "0xf2kgx5lkapwm1hqvvwvc979cvds23fpnchdsxqzc1b72c43kfm")))

(define-public crate-bones_bevy_asset_macros-0.2.0 (c (n "bones_bevy_asset_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (k 0)))) (h "1qp932x9rf54dxlw14mzd3kh70j55msncy0q9lxfp781ww363i89")))

