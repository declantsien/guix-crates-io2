(define-module (crates-io bo ne bonerjams-config) #:use-module (crates-io))

(define-public crate-bonerjams-config-0.0.2 (c (n "bonerjams-config") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "196pmlfq3z7hz0yxda0nvamyh9wvhj7wy3z8bgn0v0lld5jadwhz")))

