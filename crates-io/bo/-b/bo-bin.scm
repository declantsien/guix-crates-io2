(define-module (crates-io bo -b bo-bin) #:use-module (crates-io))

(define-public crate-bo-bin-0.4.0 (c (n "bo-bin") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0.81") (d #t) (k 0)) (d (n "structopt") (r "~0.3.22") (d #t) (k 0)) (d (n "tempfile") (r "~3.3.0") (d #t) (k 2)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0v5mq29pm7nhk415yafaa8vn3bwd1dxdm13mvjfk0s3ahqmksw2v")))

