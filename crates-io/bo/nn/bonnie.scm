(define-module (crates-io bo nn bonnie) #:use-module (crates-io))

(define-public crate-bonnie-0.3.2 (c (n "bonnie") (v "0.3.2") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0i7f26rjcs2vyambpnwczxmspcjavl3n2bj8yj3xryggf9vf1rnj")))

