(define-module (crates-io bo nm bonmin-src) #:use-module (crates-io))

(define-public crate-bonmin-src-0.1.0+1.8.9 (c (n "bonmin-src") (v "0.1.0+1.8.9") (d (list (d (n "cbc-src") (r "^0.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "ipopt-src") (r "^0.2") (k 0)))) (h "1vyyhpx8yfbr8pqdjcvrcga2ijjwcnxkaisg9jq84l83vwjwrik0") (f (quote (("filtersqp") ("default" "ipopt-src/default") ("cplex")))) (l "Bonmin")))

(define-public crate-bonmin-src-0.1.1+1.8.9 (c (n "bonmin-src") (v "0.1.1+1.8.9") (d (list (d (n "cbc-src") (r "^0.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "ipopt-src") (r "^0.2") (k 0)))) (h "17d81i32hlk2agprpjb1b497zcs71nqam1s7s2lwkm6cmxfj97p9") (f (quote (("filtersqp") ("default" "ipopt-src/default") ("cplex")))) (l "Bonmin")))

