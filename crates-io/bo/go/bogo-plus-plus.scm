(define-module (crates-io bo go bogo-plus-plus) #:use-module (crates-io))

(define-public crate-bogo-plus-plus-2.0.0 (c (n "bogo-plus-plus") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0z5s1cljd6q0s9xmkc2sasjg7lyr6k6wyjmwh3vxmr562kd7fgn9")))

(define-public crate-bogo-plus-plus-2.0.1 (c (n "bogo-plus-plus") (v "2.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1gqpjl13xsha11k7brfwcg04gfw4x77shz8qa4kfkgckhfxpjp16")))

(define-public crate-bogo-plus-plus-2.1.0 (c (n "bogo-plus-plus") (v "2.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1paylg7xfv1w02wxxn2020xm8i0sm0pfn8xbl5192gnpg7mbp5iz")))

(define-public crate-bogo-plus-plus-2.1.1 (c (n "bogo-plus-plus") (v "2.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "13dqd8a2387yfyk8f2gxf8ixg9ghvvby8kffid2cyfi9x9izlwgi")))

