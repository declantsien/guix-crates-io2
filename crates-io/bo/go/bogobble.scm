(define-module (crates-io bo go bogobble) #:use-module (crates-io))

(define-public crate-bogobble-0.1.0 (c (n "bogobble") (v "0.1.0") (h "1nyxf3bykyr97ypixacnhgrhjbxy223qhjqax4sg75lpnndnqzv8")))

(define-public crate-bogobble-0.1.1 (c (n "bogobble") (v "0.1.1") (h "1m73zfzsymjvf3969rd8ix8w4cifpl00vsykx55h30mblfl6yc98")))

