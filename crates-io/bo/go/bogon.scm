(define-module (crates-io bo go bogon) #:use-module (crates-io))

(define-public crate-bogon-0.1.0 (c (n "bogon") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 1)))) (h "11667v25cv4snhprcapjz8va33bk4rfxhr2wpa12zmk8dh13swkk")))

