(define-module (crates-io bo go bogo-alloc) #:use-module (crates-io))

(define-public crate-bogo-alloc-0.1.0 (c (n "bogo-alloc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.148") (k 0)) (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)))) (h "1nbwp4ca83gk7xlshzlbz26x8ismdvkkg3vvmlhnkw3472miln4i")))

(define-public crate-bogo-alloc-0.1.1 (c (n "bogo-alloc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.148") (k 0)) (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)))) (h "004lmx3g0bpk06m3i2kibcibs7ipfbmb8sj1g1xa8ig1bzrpzpkj")))

