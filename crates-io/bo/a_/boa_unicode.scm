(define-module (crates-io bo a_ boa_unicode) #:use-module (crates-io))

(define-public crate-boa_unicode-0.11.0 (c (n "boa_unicode") (v "0.11.0") (d (list (d (n "unicode-general-category") (r "^0.3.0") (d #t) (k 0)))) (h "0b0is4h10q741jbgn9r2izl2yk01yrszzmnml9vz01ypj6vf2acg")))

(define-public crate-boa_unicode-0.13.0 (c (n "boa_unicode") (v "0.13.0") (d (list (d (n "unicode-general-category") (r "^0.4.0") (d #t) (k 0)))) (h "1zmrshmn8dghmcn1pvbmksjcxak54gaf2h1wjiqa8dgnvz3zj23f")))

(define-public crate-boa_unicode-0.14.0 (c (n "boa_unicode") (v "0.14.0") (d (list (d (n "unicode-general-category") (r "^0.5.1") (d #t) (k 0)))) (h "0sxwhj3d9ikhmsp8hqvi18bf5c1qv2i8r8kac96ip6gwn1c0kpzd") (r "1.58")))

(define-public crate-boa_unicode-0.15.0 (c (n "boa_unicode") (v "0.15.0") (d (list (d (n "unicode-general-category") (r "^0.5.1") (d #t) (k 0)))) (h "1rj9ygbkhz5qws5ssg2sxn1bd6jdd29kqbhk7s7dkggjlcyagfch") (r "1.60")))

(define-public crate-boa_unicode-0.16.0 (c (n "boa_unicode") (v "0.16.0") (d (list (d (n "unicode-general-category") (r "^0.6.0") (d #t) (k 0)))) (h "1hdj7mgilc4vm1hhwhj35lv03nzajqfgyys5vdmlmcrq9dcgcz9z") (r "1.60")))

(define-public crate-boa_unicode-0.16.1 (c (n "boa_unicode") (v "0.16.1") (d (list (d (n "unicode-general-category") (r "^0.6.0") (d #t) (k 0)))) (h "1jl74k3h4s8b6wlv6x332bj7vv2hparxhaix7zr2nf9l60vc5r2k") (r "1.66")))

