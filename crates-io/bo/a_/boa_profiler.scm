(define-module (crates-io bo a_ boa_profiler) #:use-module (crates-io))

(define-public crate-boa_profiler-0.14.0 (c (n "boa_profiler") (v "0.14.0") (d (list (d (n "measureme") (r "^10.0.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "00052b9csdsdhcxb286vq3i68imsqgsi8wiba2zj032vdlw48rmy") (f (quote (("profiler" "measureme" "once_cell")))) (r "1.58")))

(define-public crate-boa_profiler-0.15.0 (c (n "boa_profiler") (v "0.15.0") (d (list (d (n "measureme") (r "^10.0.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "02l8n02mjifjsc4fb6vmzvsnzyilvnp3xric67yzi4vpsl95kg98") (f (quote (("profiler" "measureme" "once_cell")))) (r "1.60")))

(define-public crate-boa_profiler-0.16.0 (c (n "boa_profiler") (v "0.16.0") (d (list (d (n "measureme") (r "^10.1.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (o #t) (d #t) (k 0)))) (h "0z1bs934h8bah5sja0g8y750x0shjw1d6pswvkkfjw3p2r9apb0n") (f (quote (("profiler" "measureme" "once_cell")))) (r "1.60")))

(define-public crate-boa_profiler-0.17.0 (c (n "boa_profiler") (v "0.17.0") (d (list (d (n "measureme") (r "^10.1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0q1d5innbjw2qsb43rwlg0h5yjif2yd1yys30rspjvn5xjhnlkx2") (f (quote (("profiler" "measureme" "once_cell" "rustc-hash")))) (r "1.66")))

(define-public crate-boa_profiler-0.17.1 (c (n "boa_profiler") (v "0.17.1") (d (list (d (n "measureme") (r "^10.1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0jlrzljx8z751wfj3f0wrwihs12kgl9x3pxwa6vk9l2xpfgg2l5w") (f (quote (("profiler" "measureme" "once_cell" "rustc-hash")))) (r "1.69")))

(define-public crate-boa_profiler-0.17.2 (c (n "boa_profiler") (v "0.17.2") (d (list (d (n "measureme") (r "^10.1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "050b6n3zhmgfa1vcr9915ifjnfw2cgn4r42hmncbhjy08j443147") (f (quote (("profiler" "measureme" "once_cell" "rustc-hash")))) (r "1.69")))

(define-public crate-boa_profiler-0.17.3 (c (n "boa_profiler") (v "0.17.3") (d (list (d (n "measureme") (r "^10.1.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0is6x3akprbz6nxr984i5gv7zwyc107n4748jbf4l8j8zqnzk41i") (f (quote (("profiler" "measureme" "once_cell" "rustc-hash")))) (r "1.69")))

(define-public crate-boa_profiler-0.18.0 (c (n "boa_profiler") (v "0.18.0") (d (list (d (n "measureme") (r "^11.0.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (k 0)))) (h "1h2mpnfa94xlrh47lb4dw6hjvd21f63qgpk0l80cdfjw5lpkgn70") (s 2) (e (quote (("profiler" "dep:measureme" "dep:once_cell" "dep:rustc-hash")))) (r "1.74.0")))

