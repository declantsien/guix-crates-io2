(define-module (crates-io bo a_ boa_gc) #:use-module (crates-io))

(define-public crate-boa_gc-0.14.0 (c (n "boa_gc") (v "0.14.0") (d (list (d (n "gc") (r "^0.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "measureme") (r "^10.0.0") (o #t) (d #t) (k 0)))) (h "04a79z9gkpm3f67cr28fljyr8id25b9z0ldcyn5pmgpvb7cwc0m5") (r "1.58")))

(define-public crate-boa_gc-0.15.0 (c (n "boa_gc") (v "0.15.0") (d (list (d (n "gc") (r "^0.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "measureme") (r "^10.0.0") (o #t) (d #t) (k 0)))) (h "0142n4b52masdzbk09m396m86jg4wld2lgfpy7888r79d726f49s") (r "1.60")))

(define-public crate-boa_gc-0.16.0 (c (n "boa_gc") (v "0.16.0") (d (list (d (n "gc") (r "^0.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "measureme") (r "^10.1.0") (o #t) (d #t) (k 0)))) (h "0gxqijfvmn6fhh2f3s1phcnqiknrvw31kv5l2ljmw43bw0nqnyya") (r "1.60")))

(define-public crate-boa_gc-0.17.0 (c (n "boa_gc") (v "0.17.0") (d (list (d (n "boa_macros") (r "^0.17.0") (d #t) (k 0)) (d (n "boa_profiler") (r "^0.17.0") (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "1dzn9wz39rmxzr65g63v2fhw1fijk9dcg9q80vg3cqkkgfnghii3") (f (quote (("thinvec" "thin-vec")))) (r "1.66")))

(define-public crate-boa_gc-0.17.1 (c (n "boa_gc") (v "0.17.1") (d (list (d (n "boa_macros") (r "^0.17.1") (d #t) (k 0)) (d (n "boa_profiler") (r "^0.17.1") (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "19012lpqjzh75r6jschrr0sn3hmiqxl0jgrq9xinln7wn6c0hkpc") (f (quote (("thinvec" "thin-vec")))) (r "1.69")))

(define-public crate-boa_gc-0.17.2 (c (n "boa_gc") (v "0.17.2") (d (list (d (n "boa_macros") (r "^0.17.2") (d #t) (k 0)) (d (n "boa_profiler") (r "^0.17.2") (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "1kwh1kydv0ba4djf2jn1538j0hkp3j32cg78wbjaqiq8pzzvb9bs") (f (quote (("thinvec" "thin-vec")))) (r "1.69")))

(define-public crate-boa_gc-0.17.3 (c (n "boa_gc") (v "0.17.3") (d (list (d (n "boa_macros") (r "^0.17.3") (d #t) (k 0)) (d (n "boa_profiler") (r "^0.17.3") (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.12") (o #t) (d #t) (k 0)))) (h "1cj979786lvqv47vmwc0893qvypx0x3dj5yi88ilbm7rmsz48yy9") (f (quote (("thinvec" "thin-vec")))) (r "1.69")))

(define-public crate-boa_gc-0.18.0 (c (n "boa_gc") (v "0.18.0") (d (list (d (n "boa_macros") (r "~0.18.0") (d #t) (k 0)) (d (n "boa_profiler") (r "~0.18.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash" "raw"))) (k 0)) (d (n "icu_locid") (r "~1.4.0") (o #t) (k 0)) (d (n "thin-vec") (r "^0.2.13") (o #t) (d #t) (k 0)))) (h "0y2572yyrbc7wlpknblhwifw7pva1k4mn6br8w0xp9vyv0yfymf0") (s 2) (e (quote (("thin-vec" "dep:thin-vec") ("icu" "dep:icu_locid")))) (r "1.74.0")))

