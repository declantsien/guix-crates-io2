(define-module (crates-io bo a_ boa_macros) #:use-module (crates-io))

(define-public crate-boa_macros-0.17.0 (c (n "boa_macros") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1x7yixh05y8h5nv06s73dmijsipa3vszw5m7zg61y1h6g0xy8gfa") (r "1.66")))

(define-public crate-boa_macros-0.17.1 (c (n "boa_macros") (v "0.17.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0389j02q6gk26zs18xipwlf500x69p9kagc85bsdsvmj2xbr9qxn") (r "1.69")))

(define-public crate-boa_macros-0.17.2 (c (n "boa_macros") (v "0.17.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "16n37wrcdbjmc4kf4vrhy5px98q3p68bwl30hmlj3n2v10phrwdd") (r "1.69")))

(define-public crate-boa_macros-0.17.3 (c (n "boa_macros") (v "0.17.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1n0ld7nyw53pnhknkcibb6i33fq9sx6b6pm5vmk59010pp2s0pq0") (r "1.69")))

(define-public crate-boa_macros-0.18.0 (c (n "boa_macros") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1g4d45v038nrr8c25rrnhgi53d6l6j3bjxgl38waq3dnjcvwksbb") (r "1.74.0")))

