(define-module (crates-io bo vi bovidae) #:use-module (crates-io))

(define-public crate-bovidae-0.1.0 (c (n "bovidae") (v "0.1.0") (h "1bqcffjw2iqs9j27va9m8q4pqrsiklvlr1fyn4wk9b3cfgfn30fx") (y #t)))

(define-public crate-bovidae-0.1.1 (c (n "bovidae") (v "0.1.1") (h "0psyp13pjpcq72mwcir4b8gf00xqva7i9jb2iigqyvx1slggsg2i") (y #t)))

(define-public crate-bovidae-0.1.2 (c (n "bovidae") (v "0.1.2") (h "1ibi75lljs34dk35n7hq4nj22c5wnlhd1rc52mpfdlzcjh989y01") (y #t)))

(define-public crate-bovidae-0.1.3 (c (n "bovidae") (v "0.1.3") (h "1jx8zaczb4qmp1w9fb7d8sjk7jv5jajppxabqsq8s9xmsf8kfxhi") (y #t)))

(define-public crate-bovidae-0.1.4 (c (n "bovidae") (v "0.1.4") (h "0xyxh04h88vhisbc7b7jghcialrk248srb5ffcchk6ch9gd5l6lj") (y #t)))

(define-public crate-bovidae-0.1.5 (c (n "bovidae") (v "0.1.5") (h "1s58sdnly5fxfsxrx5hhdxw46yg828n118rdw83yphfl47gjbzx3") (y #t)))

(define-public crate-bovidae-0.1.6 (c (n "bovidae") (v "0.1.6") (h "1mpqs4pvfh4jjar7ilg4yjcm5bjywkbv23iyvy9hlr5p56r4pv77") (y #t)))

(define-public crate-bovidae-0.1.7 (c (n "bovidae") (v "0.1.7") (h "0j999cmyx29f13xd4kdz5jbi8wggy8c7ag9g5ififpmsaplpzw4q") (y #t)))

(define-public crate-bovidae-0.1.8 (c (n "bovidae") (v "0.1.8") (h "1ps6czslxyr41zlqhfcywdbqlws1ks70i5r9yry1kq5bm1p234hm") (y #t)))

(define-public crate-bovidae-0.1.9 (c (n "bovidae") (v "0.1.9") (h "0zp5niq1s45drn53hv3zcxj0xi31g2bgvy3vqjijihpv1lkcrpr3") (y #t)))

(define-public crate-bovidae-0.2.0 (c (n "bovidae") (v "0.2.0") (h "0qd95x8nbmfgrad58y2xk6nc6z7y8z9v0w9r55apmi0haqbxsih6") (y #t)))

(define-public crate-bovidae-0.2.1 (c (n "bovidae") (v "0.2.1") (h "0y2x9xpqpj0ghk6mfa3wf54w3v80gmaxa3fd75290ja8yklr8fim") (y #t)))

(define-public crate-bovidae-0.2.2 (c (n "bovidae") (v "0.2.2") (h "00b2yjg7f4y5lbbff5chj8h7czxd45rnl8gda3cax1zj7ac0y3bs") (y #t)))

(define-public crate-bovidae-0.2.3 (c (n "bovidae") (v "0.2.3") (h "0m7nv62604dzzb3j0rx85zja9j2i4yvx6bmc2y7mc8rrp8r8lx71") (y #t)))

(define-public crate-bovidae-0.2.4 (c (n "bovidae") (v "0.2.4") (h "1ibf35r206q31npywcwkxk8xlrrzcqxjw0p2r63lqyg9h5b13l18") (y #t)))

