(define-module (crates-io bo cu bocu1) #:use-module (crates-io))

(define-public crate-bocu1-0.1.0 (c (n "bocu1") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.0") (k 2)) (d (n "log") (r "^0.4") (f (quote ("max_level_trace" "release_max_level_off"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "static_assertions") (r "^0.3.1") (d #t) (k 0)) (d (n "try_from") (r "^0.3.2") (d #t) (k 0)))) (h "11vbxwav7lzbz0agvrq8p2pr53swzf1ia8py1gkwhlrgf5alj05z")))

