(define-module (crates-io bo ml boml) #:use-module (crates-io))

(define-public crate-boml-0.1.0 (c (n "boml") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "167rmds2wzlnnrh15myzafs3z1kx7agjpkwaimn5463k27f8kzhv")))

(define-public crate-boml-0.2.0 (c (n "boml") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "0cg0np5vfkvcxfqp2465cn3nkc6b962d9jzs478xjhcmzj7ny75w")))

(define-public crate-boml-0.3.1 (c (n "boml") (v "0.3.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "1iba2rmj0djqyf1fipxavhgc8jalxbzkg92z61agyfy70hzvkzc5")))

