(define-module (crates-io bo rg borgflux) #:use-module (crates-io))

(define-public crate-borgflux-0.1.0 (c (n "borgflux") (v "0.1.0") (d (list (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01rk2val32h1q6hrvqbfgxvxj46i3z6xx7r20kgl8bl1ng2a5j17")))

