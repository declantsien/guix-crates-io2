(define-module (crates-io bo rg borg_ros) #:use-module (crates-io))

(define-public crate-borg_ros-0.1.0 (c (n "borg_ros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)))) (h "1ni875acks5cf5iy0jw60ghp9fc7mvk5szhg851f6qvbrnbsx30z") (y #t)))

