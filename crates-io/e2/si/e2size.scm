(define-module (crates-io e2 si e2size) #:use-module (crates-io))

(define-public crate-e2size-0.1.0 (c (n "e2size") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1ax0g748s4x7p27f1drsraa58sfkjz8iw1g1v1lzc989g4yzlhjf")))

(define-public crate-e2size-0.1.1 (c (n "e2size") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "11fa3zkmkgr4hsbffzarrr2lflqhn2542qm5xxbbdbk04rfjqqlf")))

(define-public crate-e2size-0.1.2 (c (n "e2size") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1z01v4zqza8am0m49rlzl17636nsplym8wk9m9dxa74jzkh57qcw")))

(define-public crate-e2size-1.0.0 (c (n "e2size") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1gkl2lp9mcgfg3a48klmlg2z466yjz75ah6ml7zgvgc2hx6acmak")))

(define-public crate-e2size-1.0.1 (c (n "e2size") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "174x2i7zilrvinwnghwpizlkb48amx62lqp7hnjz4bslvfin8k3b")))

