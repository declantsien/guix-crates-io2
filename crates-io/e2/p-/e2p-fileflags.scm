(define-module (crates-io e2 p- e2p-fileflags) #:use-module (crates-io))

(define-public crate-e2p-fileflags-0.1.0 (c (n "e2p-fileflags") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "e2p-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "e2p-sys") (r "^0.1.0") (d #t) (k 1)))) (h "11ih11dymq7rks4v9m5bbz0fsz6x4ia9yki8w77fqfhqxpzdmgiy") (s 2) (e (quote (("serde" "dep:serde" "bitflags/serde"))))))

