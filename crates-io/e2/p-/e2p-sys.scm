(define-module (crates-io e2 p- e2p-sys) #:use-module (crates-io))

(define-public crate-e2p-sys-0.1.0 (c (n "e2p-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64") (f (quote ("runtime"))) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "regex") (r "^1.3.9") (d #t) (k 1)))) (h "1kg1whkkis4hbw20kcn7d1f889vwv24qcf5blgv1x6dcl1wmh3r0") (l "e2p")))

