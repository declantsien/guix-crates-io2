(define-module (crates-io e2 fs e2fslibs-sys) #:use-module (crates-io))

(define-public crate-e2fslibs-sys-0.1.0 (c (n "e2fslibs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1wlzyna1hay04x4hw1cqklzhr72hgaa09fbampzm77ncz5fvki5m")))

(define-public crate-e2fslibs-sys-0.2.0 (c (n "e2fslibs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0j9hvrc6rgd3wl7nsw9d5ki8pkrgpz3i8ij3jsh4a54px48izs2h")))

