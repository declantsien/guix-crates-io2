(define-module (crates-io e2 e- e2e-irc) #:use-module (crates-io))

(define-public crate-e2e-irc-3.0.0 (c (n "e2e-irc") (v "3.0.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ircparser-vanten") (r "^0.2.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "pgp") (r "^0.10.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1qphv2qg3ayzr9ddli24a54mdpfiag3dvqjd7hyxrq7sjiihc25x")))

