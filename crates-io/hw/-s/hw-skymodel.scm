(define-module (crates-io hw -s hw-skymodel) #:use-module (crates-io))

(define-public crate-hw-skymodel-0.1.0 (c (n "hw-skymodel") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "include_bytes_aligned") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "03319b5aaxpgm75y54a603gn7i5m0z1wkspvjaq7vx96pz0nls4x")))

(define-public crate-hw-skymodel-0.1.1 (c (n "hw-skymodel") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "include_bytes_aligned") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "13jdfrgixm24kv1bsshayavqxm7qxl83dws3gbnic04mg0dc4g84")))

