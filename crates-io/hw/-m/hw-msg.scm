(define-module (crates-io hw -m hw-msg) #:use-module (crates-io))

(define-public crate-hw-msg-0.1.0 (c (n "hw-msg") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "08fivv9zrz3hz2bgp4kq2y263r47rvgqn1z05y5md5659gpykbar")))

(define-public crate-hw-msg-0.2.0 (c (n "hw-msg") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1cdyry4r8q5i0zrk2fwvpmqvwqww7w26xy6pzrx40f26phnzw8h3")))

(define-public crate-hw-msg-0.3.0 (c (n "hw-msg") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "11ffaz39x198akwkqlxyvnwbpapr7mbqana688hbhcrsdi00kkdy")))

(define-public crate-hw-msg-0.3.1 (c (n "hw-msg") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0q0n9hvw5idc8wgkc3iam919mpx280ir1dl91k62is577b3s23r3")))

(define-public crate-hw-msg-0.4.0 (c (n "hw-msg") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "1jbbhgx0d1ykpswa754klym4rblsy477jcx01b3yn3xffbd8qwrl") (f (quote (("logging")))) (s 2) (e (quote (("default" "dep:log"))))))

(define-public crate-hw-msg-0.4.1 (c (n "hw-msg") (v "0.4.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "1m58k9hjr38xiy1b8j9783ndvdzd2nzvi7as7x2fcbnh515cndc5") (f (quote (("logging")))) (s 2) (e (quote (("default" "dep:log"))))))

