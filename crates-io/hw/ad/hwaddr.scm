(define-module (crates-io hw ad hwaddr) #:use-module (crates-io))

(define-public crate-hwaddr-0.1.0 (c (n "hwaddr") (v "0.1.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "reqwest") (r "^0.6") (d #t) (k 1)))) (h "18bkfgk4j3dpp1kaqnqclfrvizw7lh3n2hzcx59lfk86jpkssz0s")))

(define-public crate-hwaddr-0.1.1 (c (n "hwaddr") (v "0.1.1") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "reqwest") (r "^0.6") (d #t) (k 1)) (d (n "url") (r "^1.4") (d #t) (k 1)))) (h "0295p4mr7xgqzzl1mrk6dkvc7wvg5kzpk9lnavs864ywbjvc0ic6")))

(define-public crate-hwaddr-0.1.2 (c (n "hwaddr") (v "0.1.2") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "reqwest") (r "^0.6") (d #t) (k 1)) (d (n "url") (r "^1.4") (d #t) (k 1)))) (h "0dkmf8qg7nhai7nh97k928hgabp7zkgv32r502572a486v78w409") (f (quote (("database"))))))

(define-public crate-hwaddr-0.1.3 (c (n "hwaddr") (v "0.1.3") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "reqwest") (r "^0.6") (d #t) (k 1)) (d (n "url") (r "^1.4") (d #t) (k 1)))) (h "1w86jdnmsma191rrkhf7dl7cjv2xs7wr41vnmjq87jhgypwyf55z") (f (quote (("database"))))))

(define-public crate-hwaddr-0.1.4 (c (n "hwaddr") (v "0.1.4") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "reqwest") (r "^0.6") (d #t) (k 1)) (d (n "url") (r "^1.4") (d #t) (k 1)))) (h "17jpka8az1v4sfl7jx9495ldinqs7d7p7cyimc1csqb0gvmmczn0") (f (quote (("database"))))))

(define-public crate-hwaddr-0.1.5 (c (n "hwaddr") (v "0.1.5") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.9") (d #t) (k 1)) (d (n "url") (r "^1.4") (d #t) (k 1)))) (h "0rfvcarhws1nwyn1gvi1l13ip3viln0j1hsr6sa4mkid1z41xbjx") (f (quote (("database"))))))

(define-public crate-hwaddr-0.1.6 (c (n "hwaddr") (v "0.1.6") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "reqwest") (r "^0.10") (d #t) (k 1)) (d (n "smol") (r "^0.1") (d #t) (k 1)) (d (n "smol-potat") (r "^0.3") (f (quote ("auto"))) (d #t) (k 1)) (d (n "url") (r "^2") (d #t) (k 1)))) (h "00aba247063n5yidk76nb24l5c5kn86xnpbx0pcbv2lwdgk5nwsj") (f (quote (("database"))))))

(define-public crate-hwaddr-0.1.7 (c (n "hwaddr") (v "0.1.7") (d (list (d (n "phf") (r ">=0.8.0, <0.9.0") (d #t) (k 0)) (d (n "phf_codegen") (r ">=0.8.0, <0.9.0") (o #t) (d #t) (k 1)) (d (n "regex") (r ">=1.0.0, <2.0.0") (o #t) (d #t) (k 1)) (d (n "reqwest") (r ">=0.10.0, <0.11.0") (o #t) (d #t) (k 1)) (d (n "smol") (r ">=1.0.0, <2.0.0") (o #t) (d #t) (k 1)) (d (n "url") (r ">=2.0.0, <3.0.0") (o #t) (d #t) (k 1)))) (h "0ny8jalhl30k9555zi2mg4zfg9c3r1q0d7d2gzlg8f23kqx46574") (f (quote (("database" "smol" "reqwest" "url" "regex" "phf_codegen"))))))

