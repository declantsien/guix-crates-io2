(define-module (crates-io hw lo hwloc2) #:use-module (crates-io))

(define-public crate-hwloc2-2.2.0 (c (n "hwloc2") (v "2.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "13ilzqr9cs321b9djlgp77j1afy5vyzfhxcklvsr70m5a9plnahg") (l "hwloc")))

