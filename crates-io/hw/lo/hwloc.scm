(define-module (crates-io hw lo hwloc) #:use-module (crates-io))

(define-public crate-hwloc-0.1.0 (c (n "hwloc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "09ab3xzm3jd737cjqkmw4g2qmzxgyhvqwz03rvjabx49dyfdbcip")))

(define-public crate-hwloc-0.2.0 (c (n "hwloc") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "errno") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0k8h10add7xh05rls5wwacnh0b30z7jvi0gg4p8v5wsqny8ys16z")))

(define-public crate-hwloc-0.3.0 (c (n "hwloc") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1kv0bzclhi2bzbd3mrxnhbragl7s7wnv8yjgkkqw9y0gbbrd4zb5")))

(define-public crate-hwloc-0.4.0 (c (n "hwloc") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "num") (r "^0.1.13") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0zmldkgxyx5dvh75yp8ivsf3d05wk58aazavgql8dmwkzfwvagz7")))

(define-public crate-hwloc-0.5.0 (c (n "hwloc") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1x70457ljh82axmg5d67m7fn5133r855ykkakfpbrd5qjd4zhd19")))

