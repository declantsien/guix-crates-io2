(define-module (crates-io hw t_ hwt_ui) #:use-module (crates-io))

(define-public crate-hwt_ui-0.1.0 (c (n "hwt_ui") (v "0.1.0") (d (list (d (n "druid") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1d5l87wn487as3y092vc755bfw9zq2w3qwxhqmcbnhwgma3l5vqz")))

(define-public crate-hwt_ui-0.2.0 (c (n "hwt_ui") (v "0.2.0") (d (list (d (n "druid") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "1nsrmmpq5r9qwwldmzb7b2vx4xy22vm09y3fglig2w71zzrvbriv")))

