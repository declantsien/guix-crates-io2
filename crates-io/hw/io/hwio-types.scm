(define-module (crates-io hw io hwio-types) #:use-module (crates-io))

(define-public crate-hwio-types-1.0.0 (c (n "hwio-types") (v "1.0.0") (d (list (d (n "measurements") (r "^0.10.3") (d #t) (k 0)))) (h "1qwvjmxinsn3v3fjrjgi4mcxhiblcfww41kr12jf6qkfhdc6m4iq")))

(define-public crate-hwio-types-1.0.1 (c (n "hwio-types") (v "1.0.1") (d (list (d (n "measurements") (r "^0.10.3") (d #t) (k 0)))) (h "1hin0r9nhh5ahjqjx1z456116n0ckd1fwc47y8av8faw67vp7sl0")))

