(define-module (crates-io hw -e hw-exception) #:use-module (crates-io))

(define-public crate-hw-exception-0.1.0 (c (n "hw-exception") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)))) (h "00hbz5yv2lhq5agg07pirpsks8lx9mzlh2hsflrvcih7vx6dqv5y")))

