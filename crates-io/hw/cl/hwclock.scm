(define-module (crates-io hw cl hwclock) #:use-module (crates-io))

(define-public crate-hwclock-0.2.0 (c (n "hwclock") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1rqbhzl9gn9krf7cq3a43g3sklv9hzr13ih77zs63r4sm6z9lzm8")))

