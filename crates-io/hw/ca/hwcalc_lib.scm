(define-module (crates-io hw ca hwcalc_lib) #:use-module (crates-io))

(define-public crate-hwcalc_lib-0.1.0 (c (n "hwcalc_lib") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("num-bigint"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1y4pwf0m2amhmzg3a2ndjal9ar0nlw6hbva45h87lh8jvzxry1r4") (f (quote (("small_max") ("default"))))))

(define-public crate-hwcalc_lib-0.1.1 (c (n "hwcalc_lib") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("num-bigint"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "132qyg4w4li1s7h9rvrf6m72h2phlqy8irdacbf4hciky9ihj0md") (f (quote (("small_max") ("default")))) (r "1.56")))

(define-public crate-hwcalc_lib-0.2.0 (c (n "hwcalc_lib") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-rational") (r "^0.4") (f (quote ("num-bigint"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0xrrnvracd3ij4k0hkphmsjv8pi0vbqdi2nzlx2ycdn4a2ikqhzl") (f (quote (("small_max") ("default")))) (r "1.56")))

