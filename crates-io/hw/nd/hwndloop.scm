(define-module (crates-io hw nd hwndloop) #:use-module (crates-io))

(define-public crate-hwndloop-0.1.0 (c (n "hwndloop") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi"))) (d #t) (k 0)))) (h "00pfvf4xy1nfi24f77gzgyrps75qdism5yv5ni1hh10lg7przrgs")))

(define-public crate-hwndloop-0.1.1 (c (n "hwndloop") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi"))) (d #t) (k 0)))) (h "0avp3cgz6m64lz2kinjpp298mbgp3a2zml6sisjh696ka60qmypd")))

(define-public crate-hwndloop-0.1.2 (c (n "hwndloop") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi"))) (d #t) (k 0)))) (h "13s3ygzd1jbsndsfdy8l1lkspvvg9n7wb4wrxgmsavvsxzdxvlz5")))

(define-public crate-hwndloop-0.1.3 (c (n "hwndloop") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi"))) (d #t) (k 0)))) (h "1y9z1kh0v5qcgisypf4cz393d46nv3nr229dmz13aj2xlyl9mnip")))

(define-public crate-hwndloop-0.1.4 (c (n "hwndloop") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi"))) (d #t) (k 0)))) (h "1cmlpkiglkyyh0wy236p590pv11dld22q7pb5p178s4cffi07s2p")))

(define-public crate-hwndloop-0.1.5 (c (n "hwndloop") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi"))) (d #t) (k 0)))) (h "1ga2bady0a92wlhyj080v8c7474gnvwx6w6lc1zmqxv72lsj80ai")))

