(define-module (crates-io hw in hwinfo) #:use-module (crates-io))

(define-public crate-hwinfo-0.0.1 (c (n "hwinfo") (v "0.0.1") (d (list (d (n "CoreFoundation-sys") (r "^0.1") (d #t) (k 0)) (d (n "IOKit-sys") (r "^0.1") (d #t) (k 0)) (d (n "dmidecode") (r "^0.3") (d #t) (k 0)) (d (n "mach") (r "^0.2") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (d #t) (k 0)))) (h "1n4jydx1xfsxkfd43z8db1l3bfjdxxq7r6cfpwq1s8yr0b5k50kx")))

