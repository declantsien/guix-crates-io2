(define-module (crates-io hw at hwatch-ansi-parser) #:use-module (crates-io))

(define-public crate-hwatch-ansi-parser-0.9.0 (c (n "hwatch-ansi-parser") (v "0.9.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (k 0)))) (h "18l291gh36wmb93nzcsvhn964aqhxcj0sf1sak96w68m9yqjb6zp") (f (quote (("std" "nom/std") ("default" "std"))))))

