(define-module (crates-io cb fr cbfr) #:use-module (crates-io))

(define-public crate-cbfr-0.1.0 (c (n "cbfr") (v "0.1.0") (h "08lnrdyv6dglbqw5761450qnz5qd29ka4gkq3hhycncclm2zahv8")))

(define-public crate-cbfr-0.1.1 (c (n "cbfr") (v "0.1.1") (h "04lwk436749aa2sip03kx9sfj0pr1m3aifgvpvf4ba15423jiibi")))

(define-public crate-cbfr-0.1.2 (c (n "cbfr") (v "0.1.2") (h "1nmlxlb4xb31pi2hz3nbw8l7n81v4icjq9sf6340wmj9ab4clwyv")))

(define-public crate-cbfr-0.1.3 (c (n "cbfr") (v "0.1.3") (h "0q96kp8hkpy5psh16p0wgwfw5c0xfdv417lxfjl9gpas4vfg67n7")))

(define-public crate-cbfr-0.1.4 (c (n "cbfr") (v "0.1.4") (h "0cihah3a8ad98wbj3hv2vqqacgvgd1a70yaqw4yfg820mbl9aa2l")))

(define-public crate-cbfr-0.1.5 (c (n "cbfr") (v "0.1.5") (h "15lskk6bx1ajg5czhanhk4fqm0p5c6401mvaqpnm0i6hzil64i4c")))

(define-public crate-cbfr-0.1.6 (c (n "cbfr") (v "0.1.6") (h "0hmrszrhcjrpq4c6gw1x0fql3q4qfjyn8gld8z3dfzw21g84qh17")))

