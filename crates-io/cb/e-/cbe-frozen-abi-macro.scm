(define-module (crates-io cb e- cbe-frozen-abi-macro) #:use-module (crates-io))

(define-public crate-cbe-frozen-abi-macro-1.15.0 (c (n "cbe-frozen-abi-macro") (v "1.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0wah0ysppz78zqaxv9bklzyhnacicjmw2ygi0swbkls7nsw5l0ai")))

