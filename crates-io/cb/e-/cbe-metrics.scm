(define-module (crates-io cb e- cbe-metrics) #:use-module (crates-io))

(define-public crate-cbe-metrics-1.15.0 (c (n "cbe-metrics") (v "1.15.0") (d (list (d (n "cbe-sdk") (r "=1.15.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "0sqq7d9ag2sbywf78q95m1lkd9sd8yh5arigzbwxh9pg6rh60bz8")))

