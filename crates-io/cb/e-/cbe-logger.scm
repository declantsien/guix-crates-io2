(define-module (crates-io cb e- cbe-logger) #:use-module (crates-io))

(define-public crate-cbe-logger-1.15.0 (c (n "cbe-logger") (v "1.15.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0kl3vlz5gpi9rq682pcli3zbhxvbvnny86zij06pj0vxj05cgv8p")))

