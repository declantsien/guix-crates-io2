(define-module (crates-io cb e- cbe-config-program) #:use-module (crates-io))

(define-public crate-cbe-config-program-1.15.0 (c (n "cbe-config-program") (v "1.15.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "cbe-program-runtime") (r "=1.15.0") (d #t) (k 0)) (d (n "cbe-sdk") (r "=1.15.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "cbe-logger") (r "=1.15.0") (d #t) (k 2)))) (h "0yq5rba0xjnfkzrw9khcllifz4l33sb9zlxw4ngaiabxka8sdsvy")))

