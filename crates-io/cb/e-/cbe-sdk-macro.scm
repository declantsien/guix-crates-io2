(define-module (crates-io cb e- cbe-sdk-macro) #:use-module (crates-io))

(define-public crate-cbe-sdk-macro-1.15.0 (c (n "cbe-sdk-macro") (v "1.15.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q0zzzzs7w0zirzc7hgwyar2c6m5c1l6ffvyw8i48wv3vmksrkcj")))

