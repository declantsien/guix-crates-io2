(define-module (crates-io cb #{2u}# cb2util) #:use-module (crates-io))

(define-public crate-cb2util-0.0.1 (c (n "cb2util") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "codebreaker") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0gkjc3i1akiv0gkxaymsc0im7dwwif3s4jyvxasyr83a8h29ihg4")))

