(define-module (crates-io cb _f cb_fut) #:use-module (crates-io))

(define-public crate-cb_fut-0.1.0 (c (n "cb_fut") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "0l0lzhzdnmcawk5pwmd8vy89f64mny3x5bx60a73n9z6ysks505g")))

(define-public crate-cb_fut-0.2.0 (c (n "cb_fut") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "1278lyfcb78f28sabhq0vzn5ykifirfppzq1grzs037cdigqmh66") (f (quote (("crossbeam" "crossbeam-channel"))))))

(define-public crate-cb_fut-0.2.1 (c (n "cb_fut") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "1f3j3xgg2j02kfflrs6fnwry077b7nqvy2z29vpccyp0gn4y0w8l") (f (quote (("crossbeam" "crossbeam-channel"))))))

