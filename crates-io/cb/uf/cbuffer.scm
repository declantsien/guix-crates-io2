(define-module (crates-io cb uf cbuffer) #:use-module (crates-io))

(define-public crate-cbuffer-0.1.0 (c (n "cbuffer") (v "0.1.0") (h "07p9g30dq78vfricci02idm1gb8n8xrhd16zkc0idlbw68klp8vj")))

(define-public crate-cbuffer-0.1.1 (c (n "cbuffer") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uninit") (r "^0.4") (d #t) (k 0)))) (h "09zp6f0fi3yqpz9sf55wpaqn5q413xjhs10xql9c151r931znybi")))

(define-public crate-cbuffer-0.1.2 (c (n "cbuffer") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uninit") (r "^0.4") (d #t) (k 0)))) (h "0hvfmwdn1mr5276llfnb71xa7r0n05axq4f7d873i05pnd6hjjsz")))

(define-public crate-cbuffer-0.1.3 (c (n "cbuffer") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uninit") (r "^0.4") (d #t) (k 0)))) (h "1s4l6f9jq5sdfp0d002nqmrpw38hzq4vhyh2xcd1sxxj5cgr4a7w")))

(define-public crate-cbuffer-0.2.0-pre1 (c (n "cbuffer") (v "0.2.0-pre1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uninit") (r "^0.4") (d #t) (k 0)))) (h "17afqvrhgml8ryxw295065wxvbylxbyfnzi888mnnlayip852rgl")))

(define-public crate-cbuffer-0.2.0 (c (n "cbuffer") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uninit") (r "^0.4") (d #t) (k 0)))) (h "014kmkh7m99famg606q1kjry0zcw8i16085nsrbkfpya1zwhp5j2")))

(define-public crate-cbuffer-0.3.0 (c (n "cbuffer") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uninit") (r "^0.4") (d #t) (k 0)))) (h "1k01h6gdr78hfkffxkvwk9q9yv20blhqsk708y027jzjvyjbkb7p")))

(define-public crate-cbuffer-0.3.1 (c (n "cbuffer") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uninit") (r "^0.4") (d #t) (k 0)))) (h "1x8jys1xqyvxa9fw8i0s9acfwj82x9kjr9xccbklqh2nh3h4pyww")))

