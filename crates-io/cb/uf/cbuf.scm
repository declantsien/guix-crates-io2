(define-module (crates-io cb uf cbuf) #:use-module (crates-io))

(define-public crate-cbuf-0.1.0 (c (n "cbuf") (v "0.1.0") (h "1f5xlppxfm7dsx35ihxa36icp191rq9p24da0y4nn1kxjamyklnw") (f (quote (("no_std") ("default"))))))

(define-public crate-cbuf-0.1.1 (c (n "cbuf") (v "0.1.1") (h "1zqzns0mj0fikb7pgzmmvhgnzbl01xhfk60hlapmmmqs9c3sqw4q") (f (quote (("no_std") ("default"))))))

