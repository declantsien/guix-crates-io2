(define-module (crates-io cb it cbitset) #:use-module (crates-io))

(define-public crate-cbitset-0.1.0 (c (n "cbitset") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.6") (k 0)))) (h "1b3yw3qwivnkvlyhxq9sa9fg77ffcfw6f77hwzbjv5335ykszag3")))

(define-public crate-cbitset-0.2.0 (c (n "cbitset") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.8") (k 0)))) (h "0ss6hjrbpqlir2ad4z2db0sb4yg15w5rfax11pxmjq99mqjsvdi9")))

