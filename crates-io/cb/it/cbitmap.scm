(define-module (crates-io cb it cbitmap) #:use-module (crates-io))

(define-public crate-cbitmap-0.1.0 (c (n "cbitmap") (v "0.1.0") (h "0jhl5yz9jscdn4vkqis9yxpm1qpf242f5rcn57jljakibimjzrax")))

(define-public crate-cbitmap-0.1.1 (c (n "cbitmap") (v "0.1.1") (h "188kgna0n2wg78dgq0izxbf1s5h8pqb951i8jspd1m8priiqjn1h")))

(define-public crate-cbitmap-0.2.0 (c (n "cbitmap") (v "0.2.0") (h "123wk75a4k3kvqmh6ra9wa1cr6x3p1x7hfa8frmqzrvrj53qm0xj")))

(define-public crate-cbitmap-0.3.0 (c (n "cbitmap") (v "0.3.0") (h "02xwdl66wvnxvw1w5m9i8czhcmlxyjq2j0zmz0j7vzwrpgsxnz3r")))

(define-public crate-cbitmap-0.3.1 (c (n "cbitmap") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0rq9m1wwxdy7n6818ksrchizamnvsbvk3fpqp1ky5ydgz3yws6j3")))

(define-public crate-cbitmap-0.3.2 (c (n "cbitmap") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1i7nfvxdhn9pgnyf34vk43pfm34wl7204b8gl9vyrhdaxa0j2gr2")))

