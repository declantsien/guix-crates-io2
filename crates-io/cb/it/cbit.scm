(define-module (crates-io cb it cbit) #:use-module (crates-io))

(define-public crate-cbit-0.1.0 (c (n "cbit") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0ir3c2759y66vvp2swwc0qfz29kjk8nprx0ik13gqnwklqa08g7b")))

