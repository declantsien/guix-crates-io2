(define-module (crates-io cb rz cbrzn_msgpack) #:use-module (crates-io))

(define-public crate-cbrzn_msgpack-0.0.1-alpha.1 (c (n "cbrzn_msgpack") (v "0.0.1-alpha.1") (d (list (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0c86rv75mhn7xmqig2zjm6g82g71n4whlsfvxs8aijz6szs652xw") (r "1.67")))

(define-public crate-cbrzn_msgpack-0.0.1-alpha.2 (c (n "cbrzn_msgpack") (v "0.0.1-alpha.2") (d (list (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0c0q3hm4gid1426qf387ickawx8qq3rkdxxw6sz4wdkpywsnqby7") (r "1.67")))

