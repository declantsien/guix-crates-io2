(define-module (crates-io cb c- cbc-mac) #:use-module (crates-io))

(define-public crate-cbc-mac-0.0.0 (c (n "cbc-mac") (v "0.0.0") (h "0c5ri3cxl12492bf5mdfm428yzqsqqqphl5r5d54azqbrg7dlvfz")))

(define-public crate-cbc-mac-0.1.0 (c (n "cbc-mac") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "des") (r "^0.8") (d #t) (k 2)) (d (n "digest") (r "^0.10.2") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0dls453zfjhrnwasaw0xj1qxpyv4mkdc3ziwkmqfk22184zszlv2") (f (quote (("zeroize" "cipher/zeroize") ("std" "digest/std")))) (r "1.56")))

(define-public crate-cbc-mac-0.1.1 (c (n "cbc-mac") (v "0.1.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "des") (r "^0.8") (d #t) (k 2)) (d (n "digest") (r "^0.10.3") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0fl5dqxyq1sbd62clvj6aaqj40g5dmsxrm78pi9zx302h4v9zjqm") (f (quote (("zeroize" "cipher/zeroize") ("std" "digest/std")))) (r "1.56")))

