(define-module (crates-io cb tr cbtr) #:use-module (crates-io))

(define-public crate-cbtr-0.1.0 (c (n "cbtr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "commandstream") (r "^0.2.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "gix") (r "^0.37.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)))) (h "051gva8al89j9rq2hxcrd2rlw7s6am3zbj4jbrdhshb8dk2cpk02")))

