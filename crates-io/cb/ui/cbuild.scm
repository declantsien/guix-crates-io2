(define-module (crates-io cb ui cbuild) #:use-module (crates-io))

(define-public crate-cbuild-0.1.0 (c (n "cbuild") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "1b3kh9i3dba0w6hy8g3yry36r22qlf38s8wx5xhq2z1xyc8gwqv7")))

(define-public crate-cbuild-0.1.1 (c (n "cbuild") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "1gnp5x9nfkiy7i12x86m2lzxqg5m0xyqw7z3qdx4760wz2g7ljck")))

(define-public crate-cbuild-0.1.3 (c (n "cbuild") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "1pik6d9ynci9k6ghq9pikzp5vy2wvml99ifhkk43shgi800ajvvn")))

(define-public crate-cbuild-0.1.4 (c (n "cbuild") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "0r0zfpw7vb7hxviyp1l0ra0l3lyk1hjia0yz81ycj3xhajzj2wz0")))

