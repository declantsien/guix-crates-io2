(define-module (crates-io cb cl cbcloud) #:use-module (crates-io))

(define-public crate-cbcloud-0.0.0 (c (n "cbcloud") (v "0.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("stream"))) (d #t) (k 0)) (d (n "scsys") (r "^0.1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.10") (d #t) (k 0)))) (h "1bcxr2mx2k262i8m70n31jdmhajpiawvrjnx1z7y9grj6siyydaw") (f (quote (("full" "core") ("default" "core") ("core"))))))

