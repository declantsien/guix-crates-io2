(define-module (crates-io cb sk cbsk_mut_data) #:use-module (crates-io))

(define-public crate-cbsk_mut_data-0.1.0 (c (n "cbsk_mut_data") (v "0.1.0") (h "0ndabmp03v2lbvmfva3aw9h0sc6gmi2ng9p12j4zs4bn5w9s4p5p") (f (quote (("vec" "obj") ("ref" "macro") ("obj" "ref") ("macro") ("hash_map" "obj") ("default" "vec" "hash_map")))) (y #t)))

(define-public crate-cbsk_mut_data-0.1.1 (c (n "cbsk_mut_data") (v "0.1.1") (h "1hg9ijrbzkl4g9bqh6gw53bc19gr90h2w9zil1l2bq61fj589vq3") (f (quote (("vec" "obj") ("ref" "macro") ("obj" "ref") ("macro") ("hash_map" "obj") ("default" "vec" "hash_map")))) (y #t)))

(define-public crate-cbsk_mut_data-0.1.2 (c (n "cbsk_mut_data") (v "0.1.2") (h "0pdgyypcpijimqq27bin5gk2v37x3qa8c0y9smw947cjhqq11cb0") (f (quote (("vec" "obj") ("ref" "macro") ("obj" "ref") ("macro") ("hash_map" "obj") ("default" "vec" "hash_map")))) (y #t)))

(define-public crate-cbsk_mut_data-0.1.3 (c (n "cbsk_mut_data") (v "0.1.3") (h "18fdildax4j54n63385qxh12hq0bza7y44hyjr17pjvwkazml7gn") (f (quote (("vec" "obj") ("ref" "macro") ("obj" "ref") ("macro") ("hash_map" "obj") ("default" "vec" "hash_map"))))))

(define-public crate-cbsk_mut_data-0.1.4 (c (n "cbsk_mut_data") (v "0.1.4") (h "0crczrh0jg44g1x675x39d84zswz3k4grixi8474vrdji9m2n1yy") (f (quote (("vec" "obj") ("ref" "macro") ("obj" "ref") ("macro") ("hash_map" "obj") ("default" "vec" "hash_map"))))))

(define-public crate-cbsk_mut_data-1.0.0 (c (n "cbsk_mut_data") (v "1.0.0") (h "07wmrdg4sawzvmgifa6m7280xvjc9hzdd3wcz9sqm37qaqbvp115") (f (quote (("vec" "obj") ("ref" "macro") ("obj" "ref") ("macro") ("hash_map" "obj") ("default" "vec" "hash_map"))))))

(define-public crate-cbsk_mut_data-1.2.0 (c (n "cbsk_mut_data") (v "1.2.0") (h "1p8b14w9q66v6kmngymk0xm66951cgj0g9ldpnhlnf3yf79clvhy") (f (quote (("vec" "obj") ("ref" "macro") ("obj" "ref") ("macro") ("hash_map" "obj") ("default" "vec" "hash_map"))))))

(define-public crate-cbsk_mut_data-1.3.0 (c (n "cbsk_mut_data") (v "1.3.0") (h "10jljqr1jv5v9dwjfvkzk32l78f1n8f5akmh5acmr39zwmcffgav") (f (quote (("vec" "obj") ("ref" "macro") ("obj" "ref") ("macro") ("hash_map" "obj") ("default" "vec" "hash_map"))))))

