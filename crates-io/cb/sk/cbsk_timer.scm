(define-module (crates-io cb sk cbsk_timer) #:use-module (crates-io))

(define-public crate-cbsk_timer-1.1.0 (c (n "cbsk_timer") (v "1.1.0") (d (list (d (n "cbsk_base") (r "^0.1.8") (f (quote ("once_cell" "log" "macro"))) (d #t) (k 0)) (d (n "cbsk_mut_data") (r "^1.0.0") (f (quote ("vec"))) (k 0)) (d (n "fastdate") (r "^0.3.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1jifswb5hsdrvrx17w07l2vqi8c2p9ylmn3h124kjjvwazcybx6g") (f (quote (("debug_mode"))))))

(define-public crate-cbsk_timer-1.2.0 (c (n "cbsk_timer") (v "1.2.0") (d (list (d (n "cbsk_base") (r "^1.2.0") (f (quote ("once_cell" "log" "macro"))) (d #t) (k 0)) (d (n "cbsk_mut_data") (r "^1.2.0") (f (quote ("vec"))) (k 0)) (d (n "fastdate") (r "^0.3.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "027l498bhfb2n9p3g06d5w2b44sb0g2fjb2lkfzc0wag6p0mi20h") (f (quote (("debug_mode"))))))

(define-public crate-cbsk_timer-1.2.1 (c (n "cbsk_timer") (v "1.2.1") (d (list (d (n "cbsk_base") (r "^1.2.0") (f (quote ("once_cell" "log" "macro"))) (d #t) (k 0)) (d (n "cbsk_mut_data") (r "^1.2.0") (f (quote ("vec"))) (k 0)) (d (n "fastdate") (r "^0.3.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1dhv01zgz0ghq6h2hpsz59famp8y91l35f2ic1p8dbmbppm8bpah") (f (quote (("debug_mode"))))))

(define-public crate-cbsk_timer-1.3.0 (c (n "cbsk_timer") (v "1.3.0") (d (list (d (n "cbsk_base") (r "^1.3.0") (f (quote ("once_cell" "log" "macro"))) (d #t) (k 0)) (d (n "cbsk_mut_data") (r "^1.3.0") (f (quote ("vec"))) (k 0)) (d (n "fastdate") (r "^0.3.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1kb15k1g2626dci8srixxrmsz4wg3lzs5gx9ya2ai8mskbapmky7") (f (quote (("debug_mode"))))))

(define-public crate-cbsk_timer-1.3.1 (c (n "cbsk_timer") (v "1.3.1") (d (list (d (n "cbsk_base") (r "^1.3.0") (f (quote ("once_cell" "log" "macro"))) (d #t) (k 0)) (d (n "cbsk_mut_data") (r "^1.3.0") (f (quote ("vec"))) (k 0)) (d (n "fastdate") (r "^0.3.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "199p979x2rrl0sqrvazjbk195yzlzxpxbdsy75r7hss14jy5iwxh") (f (quote (("debug_mode"))))))

(define-public crate-cbsk_timer-1.3.2 (c (n "cbsk_timer") (v "1.3.2") (d (list (d (n "cbsk_base") (r "^1.3.0") (f (quote ("once_cell" "log" "macro"))) (d #t) (k 0)) (d (n "cbsk_mut_data") (r "^1.3.0") (f (quote ("vec"))) (k 0)) (d (n "fastdate") (r "^0.3.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1x7cifshchyfksjmhvwc5fllbqa6zsvmqswyqjlpp5277pkqpagw") (f (quote (("debug_mode"))))))

(define-public crate-cbsk_timer-1.3.3 (c (n "cbsk_timer") (v "1.3.3") (d (list (d (n "cbsk_base") (r "^1.3.0") (f (quote ("once_cell" "log" "macro"))) (d #t) (k 0)) (d (n "cbsk_mut_data") (r "^1.3.0") (f (quote ("vec"))) (k 0)) (d (n "fastdate") (r "^0.3.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1b2nqhrl174275dndabwxpi0nv1x8x1p5085rpkwjqwz4njjb0ai") (f (quote (("debug_mode"))))))

