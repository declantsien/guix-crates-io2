(define-module (crates-io cb -s cb-stm-temp) #:use-module (crates-io))

(define-public crate-cb-stm-temp-0.1.0 (c (n "cb-stm-temp") (v "0.1.0") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)))) (h "10qgdwg055yyajlag2a6ysr6sxjsxqkcbcxgn19isqjhbkhsxpyl") (y #t)))

(define-public crate-cb-stm-temp-0.1.1 (c (n "cb-stm-temp") (v "0.1.1") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)))) (h "19iqc11zryawlhf1vki2dzc9kwlvik33hhxd86aiv9wp1xx1dkqn") (y #t)))

(define-public crate-cb-stm-temp-0.2.0 (c (n "cb-stm-temp") (v "0.2.0") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)))) (h "1clsl8q4mnmbf33nm1v31kfwj9srh1djg4yz0r00wn20c8g1lsll") (y #t)))

(define-public crate-cb-stm-temp-0.3.0 (c (n "cb-stm-temp") (v "0.3.0") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)))) (h "1pax4w7wj4km1wf8lfzjzbqrfspx3d16vc0gfaicx5qn23cdbrc3") (y #t)))

