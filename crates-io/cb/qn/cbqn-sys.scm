(define-module (crates-io cb qn cbqn-sys) #:use-module (crates-io))

(define-public crate-cbqn-sys-0.0.1 (c (n "cbqn-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "04x1mbygvsw1xfngxm8nz4wxvlkkv436cp9s6b8fml0p07jb1r84") (l "cbqn")))

(define-public crate-cbqn-sys-0.0.2 (c (n "cbqn-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0pwydm0achglvs7jqkzjjzn1hdwh14p83c26qq80bg1m2clz2shg") (l "cbqn")))

(define-public crate-cbqn-sys-0.0.3 (c (n "cbqn-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "1g1asfjvqkzs1chay5cda47jcij4ii4w89d2anzhhwb6i7d2i7gm") (y #t) (l "cbqn")))

(define-public crate-cbqn-sys-0.0.4 (c (n "cbqn-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "1d1xqpi5wkwzragkw7c8k40854mvd189br6nfgdg2j5rlna5ar4k") (y #t) (l "cbqn")))

(define-public crate-cbqn-sys-0.0.5 (c (n "cbqn-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "1k9qkkdycm9wzpf0dq3j12vx0wdsvcwybcz4g8xhl3ar1893syql") (l "cbqn")))

(define-public crate-cbqn-sys-0.0.6 (c (n "cbqn-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "1bw7jg3vdhibza7z3jnvgx0r449gw5bm1fa6mlsizjzc5qlzhfqp") (l "cbqn")))

(define-public crate-cbqn-sys-0.0.7 (c (n "cbqn-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0rpvn27v5dnnlnzk5vrpr0z1kwmvhjgk57n3fb88ybk33d4zll5d") (l "cbqn")))

(define-public crate-cbqn-sys-0.0.8 (c (n "cbqn-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0jvdnsz78z1yv163g3g2ijk2hsbxw37i3622zqlq0pi2j462yk27") (l "cbqn")))

(define-public crate-cbqn-sys-0.1.0 (c (n "cbqn-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (o #t) (d #t) (k 1)))) (h "08x8y8swz8kgb9752qyrscqzpxiacs2maz1s51xwy7pwk5nmqqkd") (f (quote (("default" "shared-object")))) (l "cbqn") (s 2) (e (quote (("shared-object" "dep:fs_extra") ("bindgen" "dep:bindgen"))))))

(define-public crate-cbqn-sys-0.2.0 (c (n "cbqn-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 1)))) (h "1qlbcwml3qj096b8nx8z05absa84cqsjxp69v5qny62y76nf18nf") (f (quote (("shared-object") ("default" "shared-object")))) (l "cbqn") (s 2) (e (quote (("bindgen" "dep:bindgen"))))))

