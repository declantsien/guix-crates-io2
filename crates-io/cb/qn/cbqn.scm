(define-module (crates-io cb qn cbqn) #:use-module (crates-io))

(define-public crate-cbqn-0.0.8 (c (n "cbqn") (v "0.0.8") (d (list (d (n "cbqn-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0qr7c538n334iygzj4756sx8d83bp3fymp4d2mhjb1x4s5ynilf0")))

(define-public crate-cbqn-0.1.0 (c (n "cbqn") (v "0.1.0") (d (list (d (n "cbqn-sys") (r "^0.2.0") (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "wasmer") (r "^3.2.1") (o #t) (d #t) (k 0)) (d (n "wasmer-wasix") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "1pka04psrkk187dk681xxjb2msdvx2c1j64kcxljdkwk82p8xjhb") (f (quote (("native-backend" "cbqn-sys/shared-object") ("default" "native-backend")))) (s 2) (e (quote (("wasi-backend" "dep:wasmer" "dep:wasmer-wasix"))))))

(define-public crate-cbqn-0.1.1 (c (n "cbqn") (v "0.1.1") (d (list (d (n "cbqn-sys") (r "^0.2.0") (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "wasmer") (r "^4.0.0") (o #t) (d #t) (k 0)) (d (n "wasmer-wasix") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "10ph52q6iagl78m0ii62cvcm0ibs4b9kx08bvbpkimbgnxqq8apy") (f (quote (("native-backend" "cbqn-sys/shared-object") ("default" "native-backend")))) (s 2) (e (quote (("wasi-backend" "dep:wasmer" "dep:wasmer-wasix"))))))

