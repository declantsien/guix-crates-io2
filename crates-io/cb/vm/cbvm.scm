(define-module (crates-io cb vm cbvm) #:use-module (crates-io))

(define-public crate-cbvm-0.1.0 (c (n "cbvm") (v "0.1.0") (h "0jnrsnbch92f1ldj0d8w9dqmv99v3kxl87wc5dg93ym4bdik5rgw")))

(define-public crate-cbvm-0.1.1 (c (n "cbvm") (v "0.1.1") (h "108j9rpgix33c8gn8lzqnrxvpcg659igfmbyxxcjk7iyz53sg8gy")))

(define-public crate-cbvm-0.1.2 (c (n "cbvm") (v "0.1.2") (h "0m6fnzjb8xla9ajgcj64f5czc6qxmjb8r6m1k7qfsdgmgfj5r71p")))

(define-public crate-cbvm-0.1.3 (c (n "cbvm") (v "0.1.3") (h "1h6iwwv2ryh517l2pk138gvqwr7wm8xyhl6lvlfhm9kmmyppd9n2")))

(define-public crate-cbvm-0.5.4 (c (n "cbvm") (v "0.5.4") (h "1c0lc5r30953lynnjipbgcgwzxbydfd3l5knswg518jm0hfy0s4l")))

(define-public crate-cbvm-0.5.5 (c (n "cbvm") (v "0.5.5") (h "0cvdsbn1mk1yzf5fgp03k9dy1yv9m1vxjkrwri2nyp4qas712ld4")))

(define-public crate-cbvm-0.5.6 (c (n "cbvm") (v "0.5.6") (h "0840hdgy2r9a7jisbkbs4b5v8slx3vn15w0cxd8iv8vqi5p66ssq")))

(define-public crate-cbvm-0.5.7 (c (n "cbvm") (v "0.5.7") (h "1934mgxdxgjmlapvpfdhw6711nsiw7zhks6yghdflh77ch8d7akk")))

(define-public crate-cbvm-0.5.8 (c (n "cbvm") (v "0.5.8") (h "17gak24yqpy8gscbya19w7d4hha8cdsa0mfc9lr4rdqhpmkvc0nk")))

(define-public crate-cbvm-0.6.1 (c (n "cbvm") (v "0.6.1") (d (list (d (n "cbasm") (r "^0.1.4") (d #t) (k 0)))) (h "0xqjsrjp80adqzcy1kxl7fv5kl3irbj1hq9l500bhdvxl0322vpa")))

(define-public crate-cbvm-0.6.2 (c (n "cbvm") (v "0.6.2") (h "1sh7cq1sczl3xda3fd1sma2fr5y334dp6xlfjvg2slvk346dvbzw")))

(define-public crate-cbvm-0.6.3 (c (n "cbvm") (v "0.6.3") (d (list (d (n "cbasm") (r "^0.1.6") (d #t) (k 0)))) (h "18hfmk2jln9pq0x2s33a9yiajdx4gfil5by4w2a2vnfnm5dgp9rw")))

(define-public crate-cbvm-0.6.4 (c (n "cbvm") (v "0.6.4") (d (list (d (n "cbasm") (r "^0.1.6") (d #t) (k 0)))) (h "1lvflq3rkxbn0pdxl0hk9s93wafcskq8s00cidsmqqqpx2pr8sww")))

(define-public crate-cbvm-0.6.5 (c (n "cbvm") (v "0.6.5") (h "00lq3k2cbkqlha5awdn2il4qxxnkhcphvwbw5v5plf0kn4cnra80")))

(define-public crate-cbvm-0.6.6 (c (n "cbvm") (v "0.6.6") (h "0d44cgdd9swrl0r236ifailf6vjqp13aiigxm5v4j2bfk9l9b77q")))

(define-public crate-cbvm-0.6.7 (c (n "cbvm") (v "0.6.7") (h "1nsmahpvmzvh0ik01vn8kw7m1dvckzsv4s88cilsnbanc1awwbfi")))

(define-public crate-cbvm-0.6.8 (c (n "cbvm") (v "0.6.8") (h "07jbffw7b34afd9xzd6vk5lx3a3gny9nyylk88b6cp9qg4q68b8y")))

(define-public crate-cbvm-0.6.9 (c (n "cbvm") (v "0.6.9") (h "141as554iapfgjhwd0iv60gb21n1fc02zad14vr9lz7816ncjdn5")))

(define-public crate-cbvm-0.7.0 (c (n "cbvm") (v "0.7.0") (h "0a5ixjbncki8fvidadsrygivasaka5gfl2ay2hd7h7qwf3iz4arr")))

(define-public crate-cbvm-0.7.1 (c (n "cbvm") (v "0.7.1") (h "0pnbd6ffn0ryxscv3pk2byvnxpj6bjrv6y6q9fdvpxn3v301fqvp")))

(define-public crate-cbvm-0.7.2 (c (n "cbvm") (v "0.7.2") (h "00zdi08y0arwcbwds45x12nxqm034a9f0w2i5isyxsfbp0wgxyja")))

(define-public crate-cbvm-0.7.3 (c (n "cbvm") (v "0.7.3") (h "087bnhf17y4707p7rgj0cr4rm3nvz66jrc3s9l0v0iwydch5fqhs")))

(define-public crate-cbvm-0.7.4 (c (n "cbvm") (v "0.7.4") (h "1gj7bi1z4mjgx13jpbb2pdbqbynq456pb7nbzxwxv9gwcg08ldyr")))

