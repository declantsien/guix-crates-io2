(define-module (crates-io cb or cbor2json) #:use-module (crates-io))

(define-public crate-cbor2json-0.1.0 (c (n "cbor2json") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1sjbfvvg78f1fx312vm0km72dq7h4v0l47fk7k3fp84nxffrd0fs")))

