(define-module (crates-io cb or cborg) #:use-module (crates-io))

(define-public crate-cborg-0.1.1 (c (n "cborg") (v "0.1.1") (h "0ki815nmr7pylrcmypnwkbqnpc844dxi64x170076pfmlniahmw9")))

(define-public crate-cborg-0.2.0 (c (n "cborg") (v "0.2.0") (h "0isnsvvfvvalawqhgdiqyq5lg3zcdvac36ww897775j73ddf7gky")))

(define-public crate-cborg-0.3.0 (c (n "cborg") (v "0.3.0") (h "07rnf51c6v6faqwwvwx6ky9csh9caiz796b66dvfgz68bilh09b1")))

