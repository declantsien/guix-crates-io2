(define-module (crates-io cb or cbor-tools) #:use-module (crates-io))

(define-public crate-cbor-tools-0.2.0 (c (n "cbor-tools") (v "0.2.0") (d (list (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0zb6iva52fmzy6n2scsy5nbgdim40yz1mm2p5qka6kh44r82m3ig")))

(define-public crate-cbor-tools-0.3.0 (c (n "cbor-tools") (v "0.3.0") (d (list (d (n "half") (r "^1.8.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "hex_fmt") (r "^0.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 2)) (d (n "strum_macros") (r "^0.23.1") (d #t) (k 0)))) (h "0inl5dlgapapabsqambnm1m4bs88z6jli100zkk10yc06p5qcyg3") (f (quote (("display") ("default" "display")))) (r "1.46")))

