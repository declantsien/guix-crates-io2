(define-module (crates-io cb or cbored) #:use-module (crates-io))

(define-public crate-cbored-0.1.0 (c (n "cbored") (v "0.1.0") (h "1gafwxwjwwc0ajiici3wn48v717cw2kwsrcqk4f92xpq952ffnah")))

(define-public crate-cbored-0.1.1 (c (n "cbored") (v "0.1.1") (h "0iimpqwz6ig4jhpwawqwk9a46as6zcc9li3mvl0nan4n8qg1wd0a")))

(define-public crate-cbored-0.1.2 (c (n "cbored") (v "0.1.2") (h "1zs3gwbdrb5rhj94hic1l46b2gi2qp9fsa1rip0y44y44r3z8a6m")))

(define-public crate-cbored-0.1.3 (c (n "cbored") (v "0.1.3") (h "1s58q5f0dfglkp813578wkd9hnvnibykxa4kf4jcjxvi1gj1jggi")))

(define-public crate-cbored-0.1.4 (c (n "cbored") (v "0.1.4") (h "1abdqvd95wx0k0cksbs2rcgq1a0dxcsmxj364zypa1cdyn1gx0jr")))

(define-public crate-cbored-0.1.5 (c (n "cbored") (v "0.1.5") (h "0cm5r484974dhzbp6rcyq8bx1p3c45gl9k5gy0camwcg6k4vfpbl")))

(define-public crate-cbored-0.1.6 (c (n "cbored") (v "0.1.6") (h "0j9kz4xa586nv2rss63bwsxddc6113c1z5c8nryl3rdahk72wjjz")))

(define-public crate-cbored-0.1.7 (c (n "cbored") (v "0.1.7") (h "1lv9vnh4wnl1wsgjj6n7wax049pmqsj1y26b3g0fj4y80p77fpqn")))

(define-public crate-cbored-0.1.8 (c (n "cbored") (v "0.1.8") (d (list (d (n "cbored-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "03mqhrwyfw9zffnr2rgsj3f90k49fi53v2v2m1xa17l88v4ma7nq") (f (quote (("derive" "cbored-derive") ("default"))))))

(define-public crate-cbored-0.2.0 (c (n "cbored") (v "0.2.0") (d (list (d (n "cbored-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1vbvmxr48nfpzsnsi1mgywkyx0wf3zwycz9flqk10wbylm4w1mlb") (f (quote (("derive" "cbored-derive") ("default"))))))

(define-public crate-cbored-0.3.0 (c (n "cbored") (v "0.3.0") (d (list (d (n "cbored-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "152xp0dq5asi077szd2ypxz3iwsz33bzvkyca9vqq6qsra5724fi") (f (quote (("derive" "cbored-derive") ("default"))))))

(define-public crate-cbored-0.3.1 (c (n "cbored") (v "0.3.1") (d (list (d (n "cbored-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0pqhdlkw5r0n5vb1plgifl5jf03516zcj1fsdllkp3kzrxa45saq") (f (quote (("derive" "cbored-derive") ("default"))))))

(define-public crate-cbored-0.3.2 (c (n "cbored") (v "0.3.2") (d (list (d (n "cbored-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "05db7kxx31dmchp02zs6ymacf2qd1i0wrbqax27yzjd7i281s7kc") (f (quote (("derive" "cbored-derive") ("default"))))))

(define-public crate-cbored-0.3.3 (c (n "cbored") (v "0.3.3") (d (list (d (n "cbored-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1ak8l07zalbxx31y9zkz9djwc0kknbbh2iav21w360ls3i9adq3w") (f (quote (("derive" "cbored-derive") ("default"))))))

(define-public crate-cbored-0.3.4 (c (n "cbored") (v "0.3.4") (d (list (d (n "cbored-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "16g8a7skqqvw0l459kh7zs5hicfh3x9fz7f1hji0yll2w3n47bw4") (f (quote (("derive" "cbored-derive") ("default"))))))

(define-public crate-cbored-0.3.5 (c (n "cbored") (v "0.3.5") (d (list (d (n "cbored-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1hd9pf6y0jjbwsxcrwhwlhrhpycdiqdqg6qjy2wn5c00avlqy6al") (f (quote (("derive" "cbored-derive") ("default"))))))

(define-public crate-cbored-0.4.0 (c (n "cbored") (v "0.4.0") (d (list (d (n "cbored-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "16k6xmql7hywfd8cgagz1vn7wafld2kislffk0bb9nyichpcqbvq") (f (quote (("derive" "cbored-derive") ("default"))))))

