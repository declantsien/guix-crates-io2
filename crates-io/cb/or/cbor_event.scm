(define-module (crates-io cb or cbor_event) #:use-module (crates-io))

(define-public crate-cbor_event-1.0.0 (c (n "cbor_event") (v "1.0.0") (h "03vs52vcvgmvdra74mjxvgnmynk2ixbx4k63y0zml7rdw4fmma99")))

(define-public crate-cbor_event-1.0.1 (c (n "cbor_event") (v "1.0.1") (h "0vra510c76yr6s99qp0m543xhkldkvi5ayv6ap5bpy0ijpr7f67x")))

(define-public crate-cbor_event-2.0.0 (c (n "cbor_event") (v "2.0.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "08wrax2hlljmja4qfys45sqrdpczmdz8cw6j5biar4n64cjpdax3") (y #t)))

(define-public crate-cbor_event-2.0.1 (c (n "cbor_event") (v "2.0.1") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1sb0dq683vrqz4aq6i7nazwgfr9yzg31pasngvqxy5vjy736sqwk") (y #t)))

(define-public crate-cbor_event-2.0.2 (c (n "cbor_event") (v "2.0.2") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0zryy6g5iblcxgnw684hbvyz5a3jz009f2apnhrwvd4y1wsmpcjb") (y #t)))

(define-public crate-cbor_event-2.1.0 (c (n "cbor_event") (v "2.1.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "10x0kkg5hvg2dwgk8l2xxlj99scj9xkfxldf277vncccxb4kbkh2")))

(define-public crate-cbor_event-2.1.1 (c (n "cbor_event") (v "2.1.1") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "00v1q7wm2xz4w0iaxqcang4svsmv3x3wkya6nf3is5hw9j6vi4cr")))

(define-public crate-cbor_event-2.1.2 (c (n "cbor_event") (v "2.1.2") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "03klfyzw6m06rdp7laxwprwp7l6ppmz344pjkxlf0cxpk8fb7m74")))

(define-public crate-cbor_event-2.1.3 (c (n "cbor_event") (v "2.1.3") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0ld429li4b64dxbn6gg7va2pg96vjyy0dlchwa74h5cqg25dlv1b")))

(define-public crate-cbor_event-2.2.0 (c (n "cbor_event") (v "2.2.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1jrphbn0lcm634gga9x6mwk72ry7a7qjp3ichgcddixni6rdicsj")))

(define-public crate-cbor_event-2.4.0 (c (n "cbor_event") (v "2.4.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0g1d5y6g7wblqir134hvwvq971fqxwqh11hiir7fandws5hh56h8")))

