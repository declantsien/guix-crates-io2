(define-module (crates-io cb or cbor_enhanced_derive_protocol) #:use-module (crates-io))

(define-public crate-cbor_enhanced_derive_protocol-0.1.0 (c (n "cbor_enhanced_derive_protocol") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1sjmw9gnyx1mszlr12idfwd78dd3d3v4g3awsh61v76ndp84jzlz")))

(define-public crate-cbor_enhanced_derive_protocol-0.1.1 (c (n "cbor_enhanced_derive_protocol") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "1hphw9rm6x11z0jpxrbxcjqrzmjsd5kp2njgi36hfzvi41wbfp44")))

