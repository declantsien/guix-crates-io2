(define-module (crates-io cb or cbor-lite) #:use-module (crates-io))

(define-public crate-cbor-lite-0.1.0 (c (n "cbor-lite") (v "0.1.0") (h "026jnydkxcnwyz2prlwgf0mcdh772bj4hs9bzq9s6k2b1vd6j42l")))

(define-public crate-cbor-lite-0.1.1 (c (n "cbor-lite") (v "0.1.1") (h "0rdab25nydzlkpsv6425ws3qdx0vprpi5vsyrmzzp4j30mqkc1b4")))

