(define-module (crates-io cb or cbored-derive) #:use-module (crates-io))

(define-public crate-cbored-derive-0.1.0 (c (n "cbored-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sqk9pgwlza84sy9dcq1wqhsw6z6jnfjla0p13nb4s8w7gn5r3za")))

(define-public crate-cbored-derive-0.2.0 (c (n "cbored-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00csq1d1kfa7sdc7lkgswx2iw7lnjzy41gvz7sawhdiwlakc318f")))

(define-public crate-cbored-derive-0.3.1 (c (n "cbored-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13jphh2sybyvysgnl79nk3sh901l7mk4xdhps8fxcgf563qf08mh")))

(define-public crate-cbored-derive-0.3.2 (c (n "cbored-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lc2x766bfzswr3w0gzgcshaxk75ryivvza48chjif84scbjjv1b")))

(define-public crate-cbored-derive-0.4.0 (c (n "cbored-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "179j4nb6ficca0182wgmnjbzyk674ad28sbf3rfjrw9pqb5hm69a")))

