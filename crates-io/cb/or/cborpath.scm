(define-module (crates-io cb or cborpath) #:use-module (crates-io))

(define-public crate-cborpath-0.1.0 (c (n "cborpath") (v "0.1.0") (d (list (d (n "ciborium") (r "^0.2") (d #t) (k 0)) (d (n "ciborium-io") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)))) (h "0p8kvvhqxbgvjara5qcfx3a92rn0f3vaw2drx0s46hvaykqnvny1")))

(define-public crate-cborpath-0.1.1 (c (n "cborpath") (v "0.1.1") (d (list (d (n "ciborium") (r "^0.2") (d #t) (k 0)) (d (n "ciborium-io") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)))) (h "17cjw7pf84hq06ywkag5jmm4g8yn2f2z96sv8fnx24als2x0j6gf")))

(define-public crate-cborpath-0.1.2 (c (n "cborpath") (v "0.1.2") (d (list (d (n "ciborium") (r "^0.2") (d #t) (k 0)) (d (n "ciborium-io") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)))) (h "0v4nlwr5myn64ddzwz9d5bgsbra2kp2rvvmahj4gm9f5w4823a9z")))

(define-public crate-cborpath-0.2.0 (c (n "cborpath") (v "0.2.0") (d (list (d (n "cbor-data") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)))) (h "0kdk42dfba8np16zg4rg7a3k3wj1bbk89i9yprwykiz116ji0j2p")))

(define-public crate-cborpath-0.2.1 (c (n "cborpath") (v "0.2.1") (d (list (d (n "cbor-data") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)))) (h "17bf4k5ag7c6jdq44j9gxi33ibwg19sgjvrkdx016rgwpv7ml15y")))

(define-public crate-cborpath-0.3.0 (c (n "cborpath") (v "0.3.0") (d (list (d (n "cbor-data") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "12gin31am855n5s70676v9bzc0y46x3wqfd3nwyjy40wnsxlq08k")))

(define-public crate-cborpath-0.3.1 (c (n "cborpath") (v "0.3.1") (d (list (d (n "cbor-data") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "0xhh6cg9nh5ks0a5dd11r4wfn0j65jmwai7kc7gj3pwi26qrc6sc")))

(define-public crate-cborpath-0.4.0 (c (n "cborpath") (v "0.4.0") (d (list (d (n "cbor-data") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "015bqq5g5lqjy8ra4lbn9yavndr302ia4mxwlxa1lzs7sl3jygy3")))

(define-public crate-cborpath-0.4.1 (c (n "cborpath") (v "0.4.1") (d (list (d (n "cbor-data") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "0diydb50xhqg18n9kv1a1bgm1iiz6j8za8i1sqzxb2ds2y19y4bw")))

(define-public crate-cborpath-0.5.0 (c (n "cborpath") (v "0.5.0") (d (list (d (n "cbor-data") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "cbor-diag") (r "^0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "0yyn0134q2r6x4rv79cxh840g7g0gnmayfah5z8j2pqrjkkv72fn")))

