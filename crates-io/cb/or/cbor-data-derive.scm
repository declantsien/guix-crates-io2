(define-module (crates-io cb or cbor-data-derive) #:use-module (crates-io))

(define-public crate-cbor-data-derive-0.8.15 (c (n "cbor-data-derive") (v "0.8.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0j40j74fxz8xwb8b0dd99qf8njidvr4hmfp43mci3r6gf44glpwi")))

