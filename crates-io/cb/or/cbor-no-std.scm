(define-module (crates-io cb or cbor-no-std) #:use-module (crates-io))

(define-public crate-cbor-no-std-0.1.0 (c (n "cbor-no-std") (v "0.1.0") (h "1z95k6zvz52qik61aawc9wzxpbwrabpnpx5vchlqnx0400076p1b") (f (quote (("no_std") ("default" "no_std"))))))

(define-public crate-cbor-no-std-0.2.0 (c (n "cbor-no-std") (v "0.2.0") (h "0f14dzbw6jgmjgn4wbv9c46sap6a868vx39h017bz4v6192j6s78")))

(define-public crate-cbor-no-std-0.3.0 (c (n "cbor-no-std") (v "0.3.0") (h "12k90w59hfwavh69qqkgsc51qh4q39h9s8m69djn8gjxq7xcx70r") (f (quote (("no_std") ("default" "no_std"))))))

