(define-module (crates-io cb or cboritem) #:use-module (crates-io))

(define-public crate-cboritem-0.1.0 (c (n "cboritem") (v "0.1.0") (h "0hxb1vn05wiqzfpb26jd9vkdvdpamz90rn1hqkapl40axfaqf39l")))

(define-public crate-cboritem-0.1.1 (c (n "cboritem") (v "0.1.1") (h "02270kgyls3kk1f142ig2gf7bmyzcjgbvbxslf7j0qz5scq818yv")))

