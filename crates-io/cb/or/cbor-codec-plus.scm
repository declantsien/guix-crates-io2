(define-module (crates-io cb or cbor-codec-plus) #:use-module (crates-io))

(define-public crate-cbor-codec-plus-0.8.0 (c (n "cbor-codec-plus") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "json-codec") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1a6s97czl0z2gcfg760hgrxayaqm12a3d83mwi9wq7sd2zqxfmpm") (f (quote (("random" "quickcheck"))))))

