(define-module (crates-io cb or cbordata-derive) #:use-module (crates-io))

(define-public crate-cbordata-derive-0.5.0 (c (n "cbordata-derive") (v "0.5.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01a39vc0pvnlz2h89mm0w8z122nw8k85yzhjz6v6rjhsl6qckzay")))

(define-public crate-cbordata-derive-0.5.1 (c (n "cbordata-derive") (v "0.5.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n3gx1aszqs83syfwlp2jl9irxqmx3lqh8n4i1fgrwkigldkdigf")))

(define-public crate-cbordata-derive-0.5.2 (c (n "cbordata-derive") (v "0.5.2") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y57ji5jlryvh8ilr69xjqqzlq36csdj2cqgw66rdw3rb7ig5ab7")))

(define-public crate-cbordata-derive-0.5.3 (c (n "cbordata-derive") (v "0.5.3") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ygch8bw74g8jnhq7nnda40r8zvzjrq86hwfxwzdccvgacklvhax")))

(define-public crate-cbordata-derive-0.5.4 (c (n "cbordata-derive") (v "0.5.4") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qb7q2m9gj4x35g4bkckzy3kk061m4i2z41b8hyl8scwcscc8dhf")))

(define-public crate-cbordata-derive-0.6.0 (c (n "cbordata-derive") (v "0.6.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jvxawkd9j8m0dxipn6x8l61ynh2v88r7pnxk9w7cm2izrnx37wa")))

