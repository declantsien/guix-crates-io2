(define-module (crates-io cb la cblas-sys) #:use-module (crates-io))

(define-public crate-cblas-sys-0.0.1 (c (n "cblas-sys") (v "0.0.1") (h "0fajb3q8rhbs62gz02n2c9shi52c1zyv4xi84c698vkmjwirpcj5")))

(define-public crate-cblas-sys-0.1.0 (c (n "cblas-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03hs9ld2hcrypjq0zan5b0hnjfwzlkyczdgzdwi5gdwcwddsha8h")))

(define-public crate-cblas-sys-0.1.1 (c (n "cblas-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xbk3zji67bc8kj6dc8z9jfsciqcs6y1qkwss2415kl5zp0mnqlg")))

(define-public crate-cblas-sys-0.1.2 (c (n "cblas-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08yysf12xymvnz9nx0qj0r5yxzki8rsaawriw9l87d81w4pgkrfi")))

(define-public crate-cblas-sys-0.1.3 (c (n "cblas-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y3w4ky3nsgf5kd28p0bxlr5ksjpnyhq0d6j5f9r1lqmjzyhh3pw")))

(define-public crate-cblas-sys-0.1.4 (c (n "cblas-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rgsn3klhhh09d8qf3b87zl4rwk93l2g0qzh9hhb0lff5kcfrzmn")))

