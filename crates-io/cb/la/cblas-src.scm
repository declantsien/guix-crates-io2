(define-module (crates-io cb la cblas-src) #:use-module (crates-io))

(define-public crate-cblas-src-0.1.0 (c (n "cblas-src") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "15hs7blfkzixv9721abqqibk9rl6yfmiywwav3028zmg21s5ck9n")))

(define-public crate-cblas-src-0.1.1 (c (n "cblas-src") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "cblas") (r "^0.2.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "openblas-src") (r "^0.9.0") (f (quote ("system"))) (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1nc33d1xxnm01j1mzrw6fcnil5abwwc5xk28jsgbx928jfyzsng4")))

(define-public crate-cblas-src-0.1.2 (c (n "cblas-src") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "cblas") (r "^0.2.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "openblas-src") (r "^0.9.0") (f (quote ("system"))) (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "0fpmpggcghr4ja1qxmdf0mkswn06fabjpz79gy4xmbrvf5ba3ngy")))

(define-public crate-cblas-src-0.1.3 (c (n "cblas-src") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "cblas") (r "^0.2.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "openblas-src") (r "^0.9.0") (f (quote ("system"))) (d #t) (k 2)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1ppmdj00hnvdkgjhy6sx28lc8mvqh0n706kl2q3m7d4wmdcdni9a")))

