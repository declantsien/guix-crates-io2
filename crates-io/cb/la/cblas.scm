(define-module (crates-io cb la cblas) #:use-module (crates-io))

(define-public crate-cblas-0.0.1 (c (n "cblas") (v "0.0.1") (h "1q9mxbgz0sn24d4drrwb9fqpagmcvfk9im9q400i5fqcabfkc1b9")))

(define-public crate-cblas-0.1.0 (c (n "cblas") (v "0.1.0") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0gdsdxk90422f35w6wk32sf9z8d87piplwn0g93ivcw6ja2708f1")))

(define-public crate-cblas-0.1.1 (c (n "cblas") (v "0.1.1") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1r2wlazlkgz3brpwddsf2ni2r6krjsi1s04ds7va4rbswqwhxm0w")))

(define-public crate-cblas-0.1.2 (c (n "cblas") (v "0.1.2") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1yafk26cjz6p21gsvik9rwwnk48kgd8ap0gg5ihhcn958130lg9h")))

(define-public crate-cblas-0.1.3 (c (n "cblas") (v "0.1.3") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1c9233zby96lmykqq2r5p7r7r2bs3wmyysz97yg4x82wnl5x3hg2")))

(define-public crate-cblas-0.1.4 (c (n "cblas") (v "0.1.4") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1kvv0mabqfbg38jw2f3p55ib2if1gyh6sblbz31ywrghx4bz8q50")))

(define-public crate-cblas-0.1.5 (c (n "cblas") (v "0.1.5") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0fm1y2kmmjxv4xvbzprrdxs4sjdrwc1x5x05nmvn1k316cic4iff")))

(define-public crate-cblas-0.1.6 (c (n "cblas") (v "0.1.6") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)))) (h "1541jn06llhmibr6947chvxlh6mak16alimva0nnb4sb38nzhhzc") (y #t)))

(define-public crate-cblas-0.2.0 (c (n "cblas") (v "0.2.0") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)))) (h "1nz5xi54xs9i10csyv81fxjzb5lv0iwdha6b856fpv1kvld36byq")))

(define-public crate-cblas-0.3.0 (c (n "cblas") (v "0.3.0") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (k 0)))) (h "12y7171xcj5pv06chycpq0p14v21j7cnn1jwyiwh4spdd01aalq4")))

(define-public crate-cblas-0.4.0 (c (n "cblas") (v "0.4.0") (d (list (d (n "cblas-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)))) (h "0vrj2bza6q9wxl567ij1228sglj8gw8sxyj6pj8yimwffkznvr1x")))

