(define-module (crates-io un bl unblend) #:use-module (crates-io))

(define-public crate-unblend-0.1.0 (c (n "unblend") (v "0.1.0") (d (list (d (n "align-address") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "zip_next") (r "^0.10") (k 0)))) (h "1fcykcav2x6rjds1q9jx21rc57241srxpyvbfb8fnhnkv7wzd1sf")))

