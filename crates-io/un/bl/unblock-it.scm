(define-module (crates-io un bl unblock-it) #:use-module (crates-io))

(define-public crate-unblock-it-1.0.0 (c (n "unblock-it") (v "1.0.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "tetra") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "windres") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)))) (h "0g89kprb092braf5z2zpmhxfwy8v3vxk0fp83rvn1kf8kn41ym64")))

