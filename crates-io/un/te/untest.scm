(define-module (crates-io un te untest) #:use-module (crates-io))

(define-public crate-untest-0.1.0 (c (n "untest") (v "0.1.0") (h "0w0dijbxhz63ix28gzbcq1w3f5anrxf3klrgk9xj84bpr2i12k9l")))

(define-public crate-untest-0.1.1 (c (n "untest") (v "0.1.1") (d (list (d (n "untest_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0yida5l9nv93cdvs0f8a7pdbmp272gkgjh26kdd5ars4cj90d5hl")))

(define-public crate-untest-0.1.2 (c (n "untest") (v "0.1.2") (d (list (d (n "untest_macro") (r "^0.1.1") (d #t) (k 0)))) (h "1yz043xfgp3gvwxqrpfgq2vz3q69va30i7r19jln35s7aj2x892v")))

