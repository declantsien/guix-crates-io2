(define-module (crates-io un te untest_macro) #:use-module (crates-io))

(define-public crate-untest_macro-0.1.0 (c (n "untest_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.104") (f (quote ("full"))) (d #t) (k 0)))) (h "0bhcgg1xkjbw4w8sm6pm2qfkah5b0b7gi827x000l83psks9dnwd")))

(define-public crate-untest_macro-0.1.1 (c (n "untest_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.104") (f (quote ("full"))) (d #t) (k 0)))) (h "14s8c98h7liw2dj9lvmwmajvd5mxllp79abn20hjwb0dxlvnq78n")))

