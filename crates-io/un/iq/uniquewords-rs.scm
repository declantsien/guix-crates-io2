(define-module (crates-io un iq uniquewords-rs) #:use-module (crates-io))

(define-public crate-uniquewords-rs-0.2.0 (c (n "uniquewords-rs") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "18r30rpdjabcldicxn86cb4xqbn2vsnb2h5ig55k8i2sr91va82h")))

(define-public crate-uniquewords-rs-0.2.1 (c (n "uniquewords-rs") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "0hpmdzz74ymhczws80p7mjc334zp72pyykfcp9341h6386zglrnc")))

(define-public crate-uniquewords-rs-0.3.0 (c (n "uniquewords-rs") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "17r21gz3zgihmgw54lg6vlyhg48ackpgkxm1znq2jbg304chrjiv")))

(define-public crate-uniquewords-rs-0.4.0 (c (n "uniquewords-rs") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "06f0rff6mzssyssjaw5sr7fbswh2a81ny35n88lhnb3yrdxdgz33")))

(define-public crate-uniquewords-rs-0.5.0 (c (n "uniquewords-rs") (v "0.5.0") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "16w8fvs87cw264llnhi300aq1lp8y7ibqwv3s03b1z6g9anhaigl")))

(define-public crate-uniquewords-rs-0.5.1 (c (n "uniquewords-rs") (v "0.5.1") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "1zfb720ljsaqfv89iqw7awsj74v0cnp8ywrgxplnki1z8ad6w3ch")))

(define-public crate-uniquewords-rs-0.6.0 (c (n "uniquewords-rs") (v "0.6.0") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "1vbh3mfdd81h9x64iz6pskyhwj733060bghqjll81xikycpq942q")))

(define-public crate-uniquewords-rs-0.7.0 (c (n "uniquewords-rs") (v "0.7.0") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "1lhwp254vclnbj06adkkixqb66ph5iwicbgsnlphz0qnmm6dg8cv")))

(define-public crate-uniquewords-rs-0.7.1 (c (n "uniquewords-rs") (v "0.7.1") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "1pmqina2syffsb0bcydf065r51vbymk1jwpsc2dd7hg89ig26l7b")))

(define-public crate-uniquewords-rs-0.8.0 (c (n "uniquewords-rs") (v "0.8.0") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "1k00ch6x1djks78dd8jaa8dr2z2w10cb2cj63mc30aq9fk68mifk")))

