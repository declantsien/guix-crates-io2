(define-module (crates-io un iq unique_ptr) #:use-module (crates-io))

(define-public crate-unique_ptr-0.1.0 (c (n "unique_ptr") (v "0.1.0") (h "11xyqz232m9pn5rmfqlg5fmjy7akh4nk5w9g57581g3hfjy5fdlf") (y #t)))

(define-public crate-unique_ptr-0.1.1 (c (n "unique_ptr") (v "0.1.1") (h "1zvfac31qw3n1b75rwk6mz6krcgnhs3fy9yjl349j1k1sddb7k5d") (y #t)))

(define-public crate-unique_ptr-0.1.2 (c (n "unique_ptr") (v "0.1.2") (h "1lqzpzdmp72k247zz76rqqcvagyyvldsyy2w2qh4wypf1iwb4prf") (y #t)))

(define-public crate-unique_ptr-0.1.3 (c (n "unique_ptr") (v "0.1.3") (h "0wwi97fhdqhk2rwmvxx9jalvy13sa4y033qggzbmpq68svp4dds1")))

