(define-module (crates-io un iq unique-token) #:use-module (crates-io))

(define-public crate-unique-token-0.1.0 (c (n "unique-token") (v "0.1.0") (d (list (d (n "triomphe") (r "^0.1.5") (k 0)))) (h "1bzwkb4qbi4wni85j0lnkzmqgfmyhaj7fnlw18cmjdxcpl7hhh0k")))

(define-public crate-unique-token-0.1.1 (c (n "unique-token") (v "0.1.1") (d (list (d (n "triomphe") (r "^0.1.5") (k 0)))) (h "1n2nnihf24ma7rk8llh9sxw3fqc96p381br4ray0875ajwg66cm5")))

(define-public crate-unique-token-0.2.0 (c (n "unique-token") (v "0.2.0") (h "0rkv989fml506191f9mwvwlxrlpl5f881cjygyq3byj4x4wrxgnk")))

