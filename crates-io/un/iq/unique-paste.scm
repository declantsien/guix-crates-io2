(define-module (crates-io un iq unique-paste) #:use-module (crates-io))

(define-public crate-unique-paste-0.1.0 (c (n "unique-paste") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1lm7l5q881kqd4cabaihirhwqlvgnb2h5whhvpv2y6kwfda7s85p") (r "1.31")))

