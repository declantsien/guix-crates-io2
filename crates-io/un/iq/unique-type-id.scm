(define-module (crates-io un iq unique-type-id) #:use-module (crates-io))

(define-public crate-unique-type-id-0.1.0 (c (n "unique-type-id") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0m11d6i80p6awfg4d2amqy1dpk4a8dng37rblwh22192yj53clbj")))

(define-public crate-unique-type-id-0.1.1 (c (n "unique-type-id") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0nbx62kq4ffy61s1rrd7l4qp11fa2lwqaj0s766560v8cj0050bi")))

(define-public crate-unique-type-id-0.1.3 (c (n "unique-type-id") (v "0.1.3") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1bzwdnqwk8nrgn1vjhq9jlvy61aclsb3glrxml0br8n235s2zz0b")))

(define-public crate-unique-type-id-0.2.0 (c (n "unique-type-id") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0x1w5aprq0ymlb07ajz8263xw6xvpskp5yd289g7zfg9yvskihzv")))

(define-public crate-unique-type-id-1.0.0 (c (n "unique-type-id") (v "1.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1hv5b54pr0bjzry2i5047dczybplw9hp7rfi2bgff21ylp97ycvg")))

(define-public crate-unique-type-id-1.1.0 (c (n "unique-type-id") (v "1.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1iwmpcvf0d040isjgfcs9h19nwzpigc6d4j0fvijzhz2617y1bnn")))

(define-public crate-unique-type-id-1.2.0 (c (n "unique-type-id") (v "1.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0d1mm4n5dfdscs5i30lrjqafixcgxp4n0pbxdfq3jzp81qva8fp1")))

(define-public crate-unique-type-id-1.3.0 (c (n "unique-type-id") (v "1.3.0") (d (list (d (n "unique-type-id-derive") (r "^1") (d #t) (k 0)))) (h "0svsaaln74gm45cfsyhnigz43pnr6kzl5xv8sxcfndm166zjyin0")))

