(define-module (crates-io un iq uniqs) #:use-module (crates-io))

(define-public crate-uniqs-0.1.0 (c (n "uniqs") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "0ji0shk2acpv6kz5nc4zs822v86x00ra03rqa6mhcwnkly65bw9l")))

