(define-module (crates-io un iq unique_64) #:use-module (crates-io))

(define-public crate-unique_64-1.0.0 (c (n "unique_64") (v "1.0.0") (h "0qrwy0gdra0ky6a323iyyq39iyak80dy47wxb7abnndlh9vdwz4f") (y #t)))

(define-public crate-unique_64-1.0.1 (c (n "unique_64") (v "1.0.1") (h "0mc4m2nbr6yzhwjpkq0gda1jxy99cd37wihrn0cdqhqlfv14zjkc") (y #t)))

(define-public crate-unique_64-1.0.2 (c (n "unique_64") (v "1.0.2") (h "10x7vwxzbh2qz5m0h5a09azfxc7yaqbr64nn6kz2a7ckc4a56b6x")))

