(define-module (crates-io un iq unique-type-id-derive) #:use-module (crates-io))

(define-public crate-unique-type-id-derive-0.1.0 (c (n "unique-type-id-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "unique-type-id") (r "^0.1") (d #t) (k 0)))) (h "043j3s14dpfbh1ls89zsnpn8qnvs0x92ksk7ffl41fg9jqndfhp8")))

(define-public crate-unique-type-id-derive-0.1.1 (c (n "unique-type-id-derive") (v "0.1.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "unique-type-id") (r "^0.1") (d #t) (k 0)))) (h "1dhzd8rpi55rdr5hwgdbwmc6knfmgcbp8pvvssx0ar915c7wc905")))

(define-public crate-unique-type-id-derive-0.2.0 (c (n "unique-type-id-derive") (v "0.2.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "unique-type-id") (r "^0.2") (d #t) (k 0)))) (h "1hq2vr75w2pv3pbfagk9dbg34vqjkmxad82dl9xvq9vlmc415knl")))

(define-public crate-unique-type-id-derive-1.0.0 (c (n "unique-type-id-derive") (v "1.0.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "unique-type-id") (r "^1") (d #t) (k 0)))) (h "0dz1363h0klc303yih50na351i334ipn3f5qmxhymfqywvscczsd")))

(define-public crate-unique-type-id-derive-1.1.0 (c (n "unique-type-id-derive") (v "1.1.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "unique-type-id") (r "^1") (d #t) (k 0)))) (h "0m4zdgkwxxnjqy2p23kj572bcac5svpzr0xfmadx3nsml4nadqh2")))

(define-public crate-unique-type-id-derive-1.2.0 (c (n "unique-type-id-derive") (v "1.2.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "unique-type-id") (r "^1") (d #t) (k 0)))) (h "0rnbim1c9pl2d29li046cvl9wyz351riis093r2qcakfvhwfbah5")))

(define-public crate-unique-type-id-derive-1.3.0 (c (n "unique-type-id-derive") (v "1.3.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "01p45pp83v00xa8i53m1nq3dsajs23nzpnmpp6rqwiiz00h18zj0")))

