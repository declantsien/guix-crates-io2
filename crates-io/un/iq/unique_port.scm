(define-module (crates-io un iq unique_port) #:use-module (crates-io))

(define-public crate-unique_port-0.1.0 (c (n "unique_port") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "01vv7in1daf4nhn0fr9iycxqv9wdvxs9fcpqq30syxcdca972c83")))

(define-public crate-unique_port-0.1.1 (c (n "unique_port") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0pfz2wdqhrk5g80brk73a0pral2svqzs7xr7x1810myr76niz7fp")))

(define-public crate-unique_port-0.2.0 (c (n "unique_port") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0yawh9kd5x4bg0d3l1zkliqpvgvlipw8f8bzi7iwv6h5l09vzp5d")))

(define-public crate-unique_port-0.2.1 (c (n "unique_port") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "023paz20n6ybsdp6n4j2dr96ff1cvfp1dgqb5qjb9g7s5q9h7yfk")))

