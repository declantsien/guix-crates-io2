(define-module (crates-io un iq uniqtoo) #:use-module (crates-io))

(define-public crate-uniqtoo-0.1.0 (c (n "uniqtoo") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07viwf192njk2kz61wbhz96sb6lgpk3h1099c260s90w24qqa1rl")))

(define-public crate-uniqtoo-0.2.0 (c (n "uniqtoo") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0lyplq61cyd2il65xp1fhi95gnazffhsg3swk4xnqrcnpzcl2q24")))

(define-public crate-uniqtoo-0.3.0 (c (n "uniqtoo") (v "0.3.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0h64l6g7wg5fxqca482ajbn8ybzw3ms0xp508yg2crcv8gq4i4rq")))

