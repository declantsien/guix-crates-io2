(define-module (crates-io un iq uniqueid) #:use-module (crates-io))

(define-public crate-uniqueid-0.1.0 (c (n "uniqueid") (v "0.1.0") (d (list (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "14wqcnwpw4ibysp5alxnfshrv9rfpqj82fwqvihar8xcbzmxzgbs")))

(define-public crate-uniqueid-0.2.0 (c (n "uniqueid") (v "0.2.0") (d (list (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "1l00mffbsgc0817s06s3hqaak3mkvrdxkx3k450b0vji27lc0gms")))

(define-public crate-uniqueid-0.2.1 (c (n "uniqueid") (v "0.2.1") (d (list (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "109ygm7f7zd92hgv0jpb9ps2djv0v0rv26rm34f60clml9rmcf8m")))

(define-public crate-uniqueid-0.2.2 (c (n "uniqueid") (v "0.2.2") (d (list (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "1148isn2amrr5q1mkml4hvbaq25vk9k9vcgg85lxaan4cy6khwx0")))

(define-public crate-uniqueid-0.2.6 (c (n "uniqueid") (v "0.2.6") (d (list (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "15h0m1ci5cdm6xi0rmsnn3k2wamlzpz094a1kvd4dn3s5ckmc4ng")))

