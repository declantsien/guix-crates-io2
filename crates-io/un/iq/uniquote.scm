(define-module (crates-io un iq uniquote) #:use-module (crates-io))

(define-public crate-uniquote-0.1.0 (c (n "uniquote") (v "0.1.0") (h "10hhd8r1ga9m43kzzryc01nsa8r9gp5pp6kwhbfrqh0yjm9lf3i4") (f (quote (("std") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-0.1.1 (c (n "uniquote") (v "0.1.1") (h "1p57pgasdavhkpbs5zsbdrxbk56dqf2y84k3ar3amsmy20k2qiy3") (f (quote (("std") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-1.0.0 (c (n "uniquote") (v "1.0.0") (h "14skciz60f4iwpmkiipvkavdsryykhskzrvpkr91zyvx9wgmi5cj") (f (quote (("std") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-1.1.0 (c (n "uniquote") (v "1.1.0") (h "19hfp49i98v46qqj89dzz6qrlbvgvbgxy7ja0lv5zj01iv0cajcb") (f (quote (("std" "alloc") ("min_const_generics") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-1.1.1 (c (n "uniquote") (v "1.1.1") (h "1z9vvvcfdi10yb7mrcjbq2p29l5x377a4v9lhavf2syhygqyym56") (f (quote (("std" "alloc") ("min_const_generics") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-2.0.0 (c (n "uniquote") (v "2.0.0") (h "1capkvz0p90g3nipfq46a3yplj6nkj8rbxdg0rrm40id4jvd6q4b") (f (quote (("std" "alloc") ("min_const_generics") ("default" "std") ("alloc"))))))

(define-public crate-uniquote-3.0.0 (c (n "uniquote") (v "3.0.0") (h "0gqwq3kbzdsj5qsc8jfm5v4qwzgnp4rrfvdpm71ch1593h22y664") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-uniquote-3.1.0 (c (n "uniquote") (v "3.1.0") (h "1bkl0n41yvs415mqny4b434kr456ysnb3dhic1zrrzppwx95jvxa") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.52.0")))

(define-public crate-uniquote-3.1.1 (c (n "uniquote") (v "3.1.1") (h "0yvknyjwpxkksjhj39sv5mljqp584r6a43lbxz01xwpjcwb6x84w") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.52.0")))

(define-public crate-uniquote-3.2.0 (c (n "uniquote") (v "3.2.0") (h "0gd21cl7vmwis0snmnnzwi92vlh85f8n5gc95wn4kpbv36ygi418") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56.0")))

(define-public crate-uniquote-3.2.1 (c (n "uniquote") (v "3.2.1") (h "07bd7p6l6knvwj7dzn5vambjss8jqm24l3v52c0af7c0mj5r5ky4") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56.0")))

(define-public crate-uniquote-3.3.0 (c (n "uniquote") (v "3.3.0") (h "037xznqcdvc3riy9498cfrzzdhj2591cz0dpiy0h9wxfnbp01aal") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.64.0")))

(define-public crate-uniquote-4.0.0 (c (n "uniquote") (v "4.0.0") (h "0yacyz7c09blbvfkkjlzgyyvif7i6n0y940wpbpw6xi29cqnvmqz") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.64.0")))

