(define-module (crates-io un iq unique_type_id_workspace) #:use-module (crates-io))

(define-public crate-unique_type_id_workspace-1.0.0 (c (n "unique_type_id_workspace") (v "1.0.0") (d (list (d (n "unique-type-id") (r "^1") (d #t) (k 0)) (d (n "unique-type-id-derive") (r "^1") (d #t) (k 0)))) (h "0ng7sv2nifb7ffjqnd167isbjrdiqdiin5qn8z1x6457dyqq8njj") (y #t)))

(define-public crate-unique_type_id_workspace-1.1.0 (c (n "unique_type_id_workspace") (v "1.1.0") (d (list (d (n "unique-type-id") (r "^1") (d #t) (k 0)) (d (n "unique-type-id-derive") (r "^1") (d #t) (k 0)))) (h "0manyjlr50qx54m00mw138x0xvss3aghz51778jwha7qn97smz40") (y #t)))

