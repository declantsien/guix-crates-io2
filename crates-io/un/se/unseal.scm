(define-module (crates-io un se unseal) #:use-module (crates-io))

(define-public crate-unseal-0.0.1 (c (n "unseal") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "05h6m6zn1qvpdif0c5sjlanlbhfsq257xzx1anm56rz2wcba8ig4")))

