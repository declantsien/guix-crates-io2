(define-module (crates-io un se unsegen_terminal) #:use-module (crates-io))

(define-public crate-unsegen_terminal-0.0.1 (c (n "unsegen_terminal") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)) (d (n "unsegen") (r "^0.0.1") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "00pqlig6v8slpyflbsgca0ifw741a67ld1cz68r4j52gy4jwks7q")))

(define-public crate-unsegen_terminal-0.1.0 (c (n "unsegen_terminal") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)) (d (n "unsegen") (r "^0.1") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "1677jjgwq0404m90ld7k2lcpdwk3d7sc1aflkz0588jy19kjlfn3")))

(define-public crate-unsegen_terminal-0.1.1 (c (n "unsegen_terminal") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)) (d (n "unsegen") (r "^0.1") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "07f8zpxd62mzr709nnd8gdcivcriixc4hp7z9wzvzi2n5xy5n017")))

(define-public crate-unsegen_terminal-0.2.0 (c (n "unsegen_terminal") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)) (d (n "unsegen") (r "^0.2.1") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "1qr6j6ccbfchlrj3azrrrcwf2mgbvjaj0hxpw12rv3xhwm255bv5")))

(define-public crate-unsegen_terminal-0.2.1 (c (n "unsegen_terminal") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)) (d (n "unsegen") (r "^0.2.1") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "1sjfkqa73qnym2pzqxfzhmrllch5kziiljvcj3czml703ss8lvdg")))

(define-public crate-unsegen_terminal-0.2.2 (c (n "unsegen_terminal") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "unsegen") (r "^0.2.1") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "14y68xn5jxhzv1k26cc2phpid3vw6l0qrlcb68rgsqkgnfq91a7w")))

(define-public crate-unsegen_terminal-0.3.0 (c (n "unsegen_terminal") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "unsegen") (r "^0.3.0") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "06c1m9n0nfnyiy94j1kgx4vca1j61ydzcf49w960r6by4vdy1pcw")))

(define-public crate-unsegen_terminal-0.3.1 (c (n "unsegen_terminal") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "unsegen") (r "^0.3.0") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "1h4by4ikkk785x5qzr0215b8r3w5cjr20nq1f4j85kr7p3wmsnwf")))

