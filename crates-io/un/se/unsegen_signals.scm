(define-module (crates-io un se unsegen_signals) #:use-module (crates-io))

(define-public crate-unsegen_signals-0.0.1 (c (n "unsegen_signals") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unsegen") (r "^0.0.1") (d #t) (k 0)))) (h "1gcvqwbxdvivkggjj89lb03w52vvh0y0a32cv907fmby8d4q3f99")))

(define-public crate-unsegen_signals-0.1.0 (c (n "unsegen_signals") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unsegen") (r "^0.1") (d #t) (k 0)))) (h "1g5aw4fgzrcgb29lfd3d6l3k0lzcgz4jlyv7c1xhj4q1cvniwwv8")))

(define-public crate-unsegen_signals-0.1.1 (c (n "unsegen_signals") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unsegen") (r "^0.1") (d #t) (k 0)))) (h "1qp00mnf6r749q4la50b9zhjvn5ig2b02cydppz965l1spz63da0")))

(define-public crate-unsegen_signals-0.2.0 (c (n "unsegen_signals") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unsegen") (r "^0.2") (d #t) (k 0)))) (h "071hzmmj28p1gh2pr5rpjh3svdwldz6zb7cj0qrzagpil65x0yw2")))

(define-public crate-unsegen_signals-0.3.0 (c (n "unsegen_signals") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unsegen") (r "^0.3") (d #t) (k 0)))) (h "1gcp597zgchv4pgv34ns9r3zzdb6hclhgrxq5qzpkrssbdgnw32z")))

