(define-module (crates-io un se unsegen_jsonviewer) #:use-module (crates-io))

(define-public crate-unsegen_jsonviewer-0.0.1 (c (n "unsegen_jsonviewer") (v "0.0.1") (d (list (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "unsegen") (r "^0.0.1") (d #t) (k 0)))) (h "048fjydipypggqlrzma28r15sqb7qaspg6y8nbpn6sl8b7qcq798")))

(define-public crate-unsegen_jsonviewer-0.1.0 (c (n "unsegen_jsonviewer") (v "0.1.0") (d (list (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "unsegen") (r "^0.1") (d #t) (k 0)))) (h "1306cirm3n9vlh2vdb8dkl4h1gpz4z9dfqdz809dwd4mg5axh47y")))

(define-public crate-unsegen_jsonviewer-0.1.1 (c (n "unsegen_jsonviewer") (v "0.1.1") (d (list (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "unsegen") (r "^0.1") (d #t) (k 0)))) (h "0yf4qr4afyi9f6aamwcvd04jx902vx2cqm4749414yhvl14fdza2")))

(define-public crate-unsegen_jsonviewer-0.2.0 (c (n "unsegen_jsonviewer") (v "0.2.0") (d (list (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "unsegen") (r "^0.2") (d #t) (k 0)))) (h "1bw9xrbhkc7g04rk62847c4xvy0rxg1vxqlbhh241kdwrf642xzl")))

(define-public crate-unsegen_jsonviewer-0.3.0 (c (n "unsegen_jsonviewer") (v "0.3.0") (d (list (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "unsegen") (r "^0.3.0") (d #t) (k 0)))) (h "0hh418h6zzsxkrkihnzdrq2q73xdk2sivf53jx1f747rn367jyg2")))

