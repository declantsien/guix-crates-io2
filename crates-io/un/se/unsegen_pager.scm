(define-module (crates-io un se unsegen_pager) #:use-module (crates-io))

(define-public crate-unsegen_pager-0.1.0 (c (n "unsegen_pager") (v "0.1.0") (d (list (d (n "syntect") (r "^2.0") (d #t) (k 0)) (d (n "unsegen") (r "^0.0.1") (d #t) (k 0)))) (h "1ngp3jyxyn3pwh2kg4yx17m9bwy9cwbkwcsldn7ylf2rncbpx37n") (y #t)))

(define-public crate-unsegen_pager-0.0.1 (c (n "unsegen_pager") (v "0.0.1") (d (list (d (n "syntect") (r "^2.0") (d #t) (k 0)) (d (n "unsegen") (r "^0.0.1") (d #t) (k 0)))) (h "0jpkgds6nh86zcq865dikar8rl10fsclgflgrp7vji91ls17p50i")))

(define-public crate-unsegen_pager-0.1.1 (c (n "unsegen_pager") (v "0.1.1") (d (list (d (n "syntect") (r "^2.0") (d #t) (k 0)) (d (n "unsegen") (r "^0.1") (d #t) (k 0)))) (h "16y2wy9nv75pcwsall00r41gwnk7qlj5lf1cqfmhl4ysishy9bmc")))

(define-public crate-unsegen_pager-0.1.2 (c (n "unsegen_pager") (v "0.1.2") (d (list (d (n "syntect") (r "^2.0") (d #t) (k 0)) (d (n "unsegen") (r "^0.1") (d #t) (k 0)))) (h "19wl9hsskl5gg8lalaagchd5ndczn5fs8sr97viwrq46v3ybdy91")))

(define-public crate-unsegen_pager-0.2.0 (c (n "unsegen_pager") (v "0.2.0") (d (list (d (n "syntect") (r "^2.0") (d #t) (k 0)) (d (n "unsegen") (r "^0.2") (d #t) (k 0)))) (h "095yy9a5wavggmyc9pc2qskrjryl1ba2ypbdf88y3qp4yfphnz8c")))

(define-public crate-unsegen_pager-0.3.0 (c (n "unsegen_pager") (v "0.3.0") (d (list (d (n "syntect") (r "^2.0") (d #t) (k 0)) (d (n "unsegen") (r "^0.3") (d #t) (k 0)))) (h "1fcz9ag9lm1s8qcdi641gls1xy21pahiyr9dgi77iqf15wpkccip")))

