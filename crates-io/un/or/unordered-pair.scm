(define-module (crates-io un or unordered-pair) #:use-module (crates-io))

(define-public crate-unordered-pair-0.1.0 (c (n "unordered-pair") (v "0.1.0") (h "092dqz45cw403c8c1yjh25imb292a4ydgh4lff618aqww4lfigl7")))

(define-public crate-unordered-pair-0.1.1 (c (n "unordered-pair") (v "0.1.1") (h "1yz8f3rr89cq09rnpzda3m8ahkbgpi4qz8ia7d7vglk0x6zzy03m")))

(define-public crate-unordered-pair-0.2.0 (c (n "unordered-pair") (v "0.2.0") (h "16359j1a3dbvpy4kbphd07p51l9xn4j76vzjpbgsdf6sk162ia3z")))

(define-public crate-unordered-pair-0.2.1 (c (n "unordered-pair") (v "0.2.1") (h "1ai8gqyzbqbwhnh98wzb81rhxxy0vc4c2h1lfx7qwsa8vf1fbiwi")))

(define-public crate-unordered-pair-0.2.2 (c (n "unordered-pair") (v "0.2.2") (h "199slsn4029vw83xi5fs21g35kxbwl2m90mqvdqwcaq4fr8wzzrd")))

(define-public crate-unordered-pair-0.2.3 (c (n "unordered-pair") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ww45i8i6yjn0br8jgh5z910mxn9vmp6xs40gpvm5sy3cb94wd4m")))

(define-public crate-unordered-pair-0.2.4 (c (n "unordered-pair") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v5h27bglhb7ln1vqfp0qrm4cavkvwmc5jrfwjs1bpd77q4k8b6q")))

