(define-module (crates-io un re unreal_asset_proc_macro) #:use-module (crates-io))

(define-public crate-unreal_asset_proc_macro-0.1.14 (c (n "unreal_asset_proc_macro") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0ar9clx8xr659n5sqp09r4bb187g09lfmxvjvwpk14d7sshdlx5c")))

(define-public crate-unreal_asset_proc_macro-0.1.15 (c (n "unreal_asset_proc_macro") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0h44vjxam3w4ac1qj6abqg76bqsbpwalv9py7rwjxdn7f4m41hda")))

(define-public crate-unreal_asset_proc_macro-0.1.16 (c (n "unreal_asset_proc_macro") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1wvm9y5rpqqjjjnaikgq9x92s7zihsfnxpsplg3m2185p1d47b9z")))

