(define-module (crates-io un re unreachable) #:use-module (crates-io))

(define-public crate-unreachable-0.0.1 (c (n "unreachable") (v "0.0.1") (d (list (d (n "void") (r "^0") (d #t) (k 0)))) (h "09g0qy6av6clilhybv5n4ncv874rmbbzpk3ys5xc8jif0qhrmp0v")))

(define-public crate-unreachable-0.0.2 (c (n "unreachable") (v "0.0.2") (d (list (d (n "void") (r "^0") (d #t) (k 0)))) (h "19v5jk6kfw1y69hkxph31lha36rnss10f2pjij4dv70nnj8kmvl7")))

(define-public crate-unreachable-0.1.0 (c (n "unreachable") (v "0.1.0") (d (list (d (n "void") (r "^1") (d #t) (k 0)))) (h "0xrp8gi0q3af11abvdcyldyibg09gxkyic7s1b46dsbzph6ajv5s")))

(define-public crate-unreachable-0.1.1 (c (n "unreachable") (v "0.1.1") (d (list (d (n "void") (r "^1") (k 0)))) (h "14gzpm329nrfgjvxf6zh77sp7plxakcnsq8p8xk9474fn7fyaahz")))

(define-public crate-unreachable-1.0.0 (c (n "unreachable") (v "1.0.0") (d (list (d (n "void") (r "^1") (k 0)))) (h "0mps2il4xy2mjqc3appas27hhn2xmvixc3bzzhfrjj74gy3i0a1q")))

