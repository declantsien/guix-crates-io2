(define-module (crates-io un re unrecurse) #:use-module (crates-io))

(define-public crate-unrecurse-0.1.0 (c (n "unrecurse") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.21") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (f (quote ("async-await"))) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0miq80cgzy58vlnq9dxa1z3r8ad52chwbf6ppjrrymrs1xxcx7b1")))

