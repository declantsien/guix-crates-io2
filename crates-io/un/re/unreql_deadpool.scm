(define-module (crates-io un re unreql_deadpool) #:use-module (crates-io))

(define-public crate-unreql_deadpool-0.1.0 (c (n "unreql_deadpool") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deadpool") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "unreql") (r "^0.1.7") (d #t) (k 0)))) (h "0n4yywqb88b3gm0wjsifvym7hs5m767gfzvv9fjjlc3fyzqcx152")))

(define-public crate-unreql_deadpool-0.1.1 (c (n "unreql_deadpool") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deadpool") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "unreql") (r "^0.1.7") (d #t) (k 0)))) (h "0n6ifrb8qf9kda0ln42fb35q2gaj34f3j49bhbmdahvsd9jnlixx")))

