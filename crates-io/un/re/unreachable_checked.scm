(define-module (crates-io un re unreachable_checked) #:use-module (crates-io))

(define-public crate-unreachable_checked-0.1.0 (c (n "unreachable_checked") (v "0.1.0") (h "1pnb154rn37g8xhzlk84b524iaykz1f9szbygfpmpc8a7shjahr4") (f (quote (("panic") ("default"))))))

(define-public crate-unreachable_checked-0.2.0 (c (n "unreachable_checked") (v "0.2.0") (h "08f64rbrnahdnrx5a1ss2yj93phl9abacly6bbkniq2bfch1lvzi") (f (quote (("panic") ("default"))))))

