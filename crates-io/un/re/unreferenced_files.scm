(define-module (crates-io un re unreferenced_files) #:use-module (crates-io))

(define-public crate-unreferenced_files-0.1.0 (c (n "unreferenced_files") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "16rb62f0ilvlpkk7lh59mj2754qvdgfa6kjnaiz4ddda5z6izh01")))

(define-public crate-unreferenced_files-0.2.0 (c (n "unreferenced_files") (v "0.2.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "08l37q1q7pw5ipvwsvcwggdr2b2lbghmik6i6fhfc148im56c8ly")))

(define-public crate-unreferenced_files-0.3.0 (c (n "unreferenced_files") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1m0l56s8mxbjiwrmih1kd5b2j7g5x7vagqy2lsjgicirzbx1j03b")))

(define-public crate-unreferenced_files-0.4.0 (c (n "unreferenced_files") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1k4l625cb06w1jgj18wkcl8rnan97s2jl0gbswvd0fqqvr134wz3")))

(define-public crate-unreferenced_files-0.5.0 (c (n "unreferenced_files") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1nh4632g0vlcfknzcry55bmizvprirnjb0s68zc73xl2n1pj3gi4")))

(define-public crate-unreferenced_files-0.6.0 (c (n "unreferenced_files") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "164yil73d88pq6m6bj9yj3scmjgmrdfkw9d4b18wbyw13bg52ngh")))

(define-public crate-unreferenced_files-0.7.0 (c (n "unreferenced_files") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "09fd9hw0jrla4zp14h6xj5a184mx695c4hjz0yz8lv75nz7scmcb")))

(define-public crate-unreferenced_files-1.0.0 (c (n "unreferenced_files") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0n4awmkxbprdcf6n74qm5vj7rgpq63p1zd1nb7zymjbgsw38xhcn")))

(define-public crate-unreferenced_files-1.0.1 (c (n "unreferenced_files") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1imkk20sr6sihr6jb67ckrdzx8kpg257hajlvjky2q0csxkhvkc7")))

(define-public crate-unreferenced_files-1.1.0 (c (n "unreferenced_files") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "17hpszsax4dl2bxnrxayc3mmxcnhyr6pybjra9yvrds0r32nlakq")))

(define-public crate-unreferenced_files-1.2.0 (c (n "unreferenced_files") (v "1.2.0") (d (list (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1d4fbfsxws9k00b3s5v0lclchzra5acksr3hgnf84slxw0bp43cl")))

(define-public crate-unreferenced_files-1.3.0 (c (n "unreferenced_files") (v "1.3.0") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0dj623lhn269r8dk4ygjfsaw0zp6i3wg2z6435gycf2mv5r5wka6")))

(define-public crate-unreferenced_files-1.4.0 (c (n "unreferenced_files") (v "1.4.0") (d (list (d (n "insta") (r "^1.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0nx7w4l3x2kywyvnj2ii1j35vhrg5s1jwh1hdn5wglbzc33c4b0f")))

(define-public crate-unreferenced_files-1.5.0 (c (n "unreferenced_files") (v "1.5.0") (d (list (d (n "insta") (r "^1.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "126j8sjm9nz82zcbglg3w6sy4mb01fz7a05hmfrwrwywzjk8nhh3")))

(define-public crate-unreferenced_files-1.6.0 (c (n "unreferenced_files") (v "1.6.0") (d (list (d (n "insta") (r "^1.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1li2agmcwas6q177wav633hjli4rsivq6v82ji457g7z9jai9nnx")))

(define-public crate-unreferenced_files-2.0.0 (c (n "unreferenced_files") (v "2.0.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1xdg9a8cnwjj3705q69mwwnr4ln8cv8kzin4d4w0n2qmq2rprhpn")))

