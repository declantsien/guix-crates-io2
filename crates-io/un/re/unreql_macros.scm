(define-module (crates-io un re unreql_macros) #:use-module (crates-io))

(define-public crate-unreql_macros-0.1.0 (c (n "unreql_macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing" "proc-macro" "derive" "printing"))) (k 0)))) (h "0nspirxk2bd8b32ji63xw5ml3lxxlcdwykbjyaj8wqhcyvp2m8gi")))

(define-public crate-unreql_macros-0.1.1 (c (n "unreql_macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing" "proc-macro" "derive" "printing"))) (k 0)))) (h "0dpm1r6ilyxag21f5lfh3k657wv8rj55i8r968zixri8vcn0mh3n")))

