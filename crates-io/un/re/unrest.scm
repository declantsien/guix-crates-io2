(define-module (crates-io un re unrest) #:use-module (crates-io))

(define-public crate-unrest-0.1.0 (c (n "unrest") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0szaf7866f9lcg0x20c7acdkvrsdm2yvpb1kyvv39rxhzjzip05v")))

