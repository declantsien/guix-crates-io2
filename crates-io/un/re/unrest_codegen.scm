(define-module (crates-io un re unrest_codegen) #:use-module (crates-io))

(define-public crate-unrest_codegen-0.1.0 (c (n "unrest_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "unrest_tmp_quote") (r "^0.1.0") (d #t) (k 0)) (d (n "unrest_tmp_syn") (r "^0.1.0") (f (quote ("full" "fold" "parsing" "printing"))) (k 0)))) (h "10s65si2c8z3vcqwqiy0q5rzwjpj8i1ajhv4pp36ld7bx9bhz2xk")))

