(define-module (crates-io un re unrestrictive-url) #:use-module (crates-io))

(define-public crate-unrestrictive-url-0.0.0 (c (n "unrestrictive-url") (v "0.0.0") (h "16f7dwdsi9dn3aiw2cj3bxmflzb8pmr9lgbz7r7pwywqn10dykv9")))

(define-public crate-unrestrictive-url-0.1.0 (c (n "unrestrictive-url") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0nlzdcba63l9hbxvfm38ijppnvd2882l5yi3qqp7w601hvsb225s")))

