(define-module (crates-io un re unrest_tmp_synom) #:use-module (crates-io))

(define-public crate-unrest_tmp_synom-0.1.0 (c (n "unrest_tmp_synom") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)) (d (n "unrest_tmp_quote") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0ij6da5gjx89wjarh1wc7n3aq4xm75mb9wdqdmkdns3bm0mbr0h2") (f (quote (("printing" "unrest_tmp_quote") ("parsing") ("extra-traits") ("default") ("clone-impls"))))))

