(define-module (crates-io un re unreliable-message) #:use-module (crates-io))

(define-public crate-unreliable-message-0.0.1 (c (n "unreliable-message") (v "0.0.1") (d (list (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "11vfvx0b179v531n4m5nc6kh7xlvymnipws8jjs4w8g0shlx008f")))

(define-public crate-unreliable-message-0.0.2 (c (n "unreliable-message") (v "0.0.2") (d (list (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1y07phx53wni86j0ny4ha5pk64qp095qh44r15alpzn79fm4ywgk")))

(define-public crate-unreliable-message-0.0.3 (c (n "unreliable-message") (v "0.0.3") (d (list (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1qkar46y2h3m2pq4s8il5qrx6wmj9k1cihmmvjcc7cgbl7b3ky4s")))

(define-public crate-unreliable-message-0.0.4 (c (n "unreliable-message") (v "0.0.4") (d (list (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "vec_map") (r "^0.0.1") (d #t) (k 0)))) (h "07259qcqa597i6lz3b88nxs8kj6h44wfmgv7nz0c15fky4m7hlcf")))

(define-public crate-unreliable-message-0.1.0 (c (n "unreliable-message") (v "0.1.0") (d (list (d (n "bincode") (r "0.5.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "vec_map") (r "^0.0.1") (d #t) (k 0)))) (h "0cf4xgs3f97yggzc09kc0r64m389cbzns21flyvswcm4zf0vlx5l")))

