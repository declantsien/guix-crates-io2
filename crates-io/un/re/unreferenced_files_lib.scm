(define-module (crates-io un re unreferenced_files_lib) #:use-module (crates-io))

(define-public crate-unreferenced_files_lib-0.1.0 (c (n "unreferenced_files_lib") (v "0.1.0") (d (list (d (n "insta") (r "^1.11.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0mc5v8iffwxxxx9j7ikq8ki8mvrpb9b9p2d5ggvhd73qhwdihc6f")))

