(define-module (crates-io un o- uno-cli) #:use-module (crates-io))

(define-public crate-uno-cli-0.1.0 (c (n "uno-cli") (v "0.1.0") (d (list (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rsg4z634ng2x3prvyyz82hawiwxw2i6isjsdaf0ykl1wv3wigw4")))

(define-public crate-uno-cli-0.1.1 (c (n "uno-cli") (v "0.1.1") (d (list (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wvgqchjnm8mpks63rpdws30vw96b9ayma5m65izc2rr81pmz6jy")))

(define-public crate-uno-cli-0.1.2 (c (n "uno-cli") (v "0.1.2") (d (list (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jq0gnlij726qfl1as16w905bhbq1pl9rl82rs0m2vdq0a4v1qn9")))

(define-public crate-uno-cli-0.1.4 (c (n "uno-cli") (v "0.1.4") (d (list (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11s34aqa2a3hy1c4ywiwnzgs8m6fn3pnym1crlfw59ya1fv9hxrj")))

(define-public crate-uno-cli-0.1.5 (c (n "uno-cli") (v "0.1.5") (d (list (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g5bzmrarb3rkqkwvii7lk3yys1a4av5107sgl8fpi0zrmnkpp3i")))

(define-public crate-uno-cli-0.1.6 (c (n "uno-cli") (v "0.1.6") (d (list (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1awzaf85pqpm3dd1gkfgvflms69xykyf2izv4hxnx8g6f4y8084g")))

(define-public crate-uno-cli-0.1.7 (c (n "uno-cli") (v "0.1.7") (d (list (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jp89j67x2pphdc8fs9axa7xa0da8kylpvzxhvpnvx36q19glgfj")))

