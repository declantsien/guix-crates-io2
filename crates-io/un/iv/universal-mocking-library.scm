(define-module (crates-io un iv universal-mocking-library) #:use-module (crates-io))

(define-public crate-universal-mocking-library-0.1.0 (c (n "universal-mocking-library") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1qji9xb67rvy178kmvnrn6h8kjp039589s1hi7vbm6v7gkqc1gn0")))

