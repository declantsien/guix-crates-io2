(define-module (crates-io un iv universal-hash) #:use-module (crates-io))

(define-public crate-universal-hash-0.0.0 (c (n "universal-hash") (v "0.0.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "084mwibzckd8qypi10d93d3m3afsh1zz31cmj6pwzn5ammm63wxm") (f (quote (("std"))))))

(define-public crate-universal-hash-0.1.0 (c (n "universal-hash") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "0j6191pv5b74x561gi7j7nmi98bq550c1hmbiljdpiihbyq1nmr2") (f (quote (("std"))))))

(define-public crate-universal-hash-0.2.0 (c (n "universal-hash") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "0d0gin0bvrymdpghygxg33m977ljwya0wgx12zvvp7jp4rgdqxc3") (f (quote (("std"))))))

(define-public crate-universal-hash-0.3.0 (c (n "universal-hash") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "00aa241pab99z66f0s464vdrxnk3igs8z1qm6j01chcv5w7r036z") (f (quote (("std"))))))

(define-public crate-universal-hash-0.4.0 (c (n "universal-hash") (v "0.4.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "00hljq64l0p68yrncvyww4cdgkzpzl49vrlnj57kwblkak3b49l3") (f (quote (("std"))))))

(define-public crate-universal-hash-0.4.1 (c (n "universal-hash") (v "0.4.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "=2.4") (k 0)))) (h "01av09i0rqcl8f0xgvn2g07kzyafgbiwdhkfwq0m14kyd67lw8cz") (f (quote (("std"))))))

(define-public crate-universal-hash-0.5.0-pre (c (n "universal-hash") (v "0.5.0-pre") (d (list (d (n "crypto-common") (r "^0.1.6") (d #t) (k 0)) (d (n "subtle") (r "=2.4") (k 0)))) (h "060581rghjfsbrzbj9vxs8178i2am9jnb8xj3p8wdm190axyy8n5") (f (quote (("std" "crypto-common/std")))) (r "1.56")))

(define-public crate-universal-hash-0.5.0-pre.1 (c (n "universal-hash") (v "0.5.0-pre.1") (d (list (d (n "crypto-common") (r "^0.1.6") (d #t) (k 0)) (d (n "subtle") (r "=2.4") (k 0)))) (h "0mzm8i1c2xmw8f1gcklsvssfqgwgx6w7ybfd8qaxqxpzyg5q9pwd") (f (quote (("std" "crypto-common/std")))) (r "1.56")))

(define-public crate-universal-hash-0.5.0-pre.2 (c (n "universal-hash") (v "0.5.0-pre.2") (d (list (d (n "crypto-common") (r "^0.1.6") (d #t) (k 0)) (d (n "subtle") (r "=2.4") (k 0)))) (h "0l0ps0xdb2wyy7v16yl55ik934wif7zai79m9i07pw5xl13a84cl") (f (quote (("std" "crypto-common/std")))) (r "1.56")))

(define-public crate-universal-hash-0.5.0 (c (n "universal-hash") (v "0.5.0") (d (list (d (n "crypto-common") (r "^0.1.6") (d #t) (k 0)) (d (n "subtle") (r "=2.4") (k 0)))) (h "1dfqh2jnf4pz2cr9v4adpyxinz658vadlbwsjgigf6cs7jvn0cbx") (f (quote (("std" "crypto-common/std")))) (r "1.56")))

(define-public crate-universal-hash-0.5.1 (c (n "universal-hash") (v "0.5.1") (d (list (d (n "crypto-common") (r "^0.1.6") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (k 0)))) (h "1sh79x677zkncasa95wz05b36134822w6qxmi1ck05fwi33f47gw") (f (quote (("std" "crypto-common/std")))) (r "1.56")))

(define-public crate-universal-hash-0.6.0-pre.0 (c (n "universal-hash") (v "0.6.0-pre.0") (d (list (d (n "crypto-common") (r "=0.2.0-pre.5") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (k 0)))) (h "1iglsd7z66ai74p6k4d0sz7a1x47vw22qkkr4hfbpxh983rkclx0") (f (quote (("std" "crypto-common/std")))) (r "1.65")))

