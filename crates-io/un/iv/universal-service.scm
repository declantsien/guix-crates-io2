(define-module (crates-io un iv universal-service) #:use-module (crates-io))

(define-public crate-universal-service-0.1.0 (c (n "universal-service") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows-service") (r "^0.6.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows-service-detector") (r "^0.1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1hbmjqmjlyj7wv7ay6xwcpwdw4lf8jqxag1m06wp4wiw79wl295c")))

