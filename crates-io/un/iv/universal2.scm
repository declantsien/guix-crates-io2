(define-module (crates-io un iv universal2) #:use-module (crates-io))

(define-public crate-universal2-0.0.0 (c (n "universal2") (v "0.0.0") (d (list (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cma544bygrhkdxv6fmqm1vriilhhb3g8qh9zs671knn8i6nplzi")))

(define-public crate-universal2-0.0.1 (c (n "universal2") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pnmkl23xygiwg4vn4bgkrazrpqiyi4iv5ma8xmmvhlzx038hsl3")))

(define-public crate-universal2-0.0.2 (c (n "universal2") (v "0.0.2") (d (list (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r2wlwhkf294fhgn0vcjgkzyid63q8q724ykr5a9n39mil3rlhx2")))

