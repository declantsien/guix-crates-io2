(define-module (crates-io un iv universalui_base) #:use-module (crates-io))

(define-public crate-UniversalUI_Base-0.1.0 (c (n "UniversalUI_Base") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1jzh2wmcks58r5vjx0wrdvl69j3dgbn88nlp9wcgdw7biwasxrhb")))

(define-public crate-UniversalUI_Base-0.1.1 (c (n "UniversalUI_Base") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0fwj1mahfrz9yj1l269z1l78cl0zvi7m2fl9mji37zrkiwnfy4ih")))

(define-public crate-UniversalUI_Base-0.1.2 (c (n "UniversalUI_Base") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0wvfc22ni3604bjijkngbkr98cdq42l4in8n9nzhk2izi4vj2fn2")))

