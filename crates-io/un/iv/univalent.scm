(define-module (crates-io un iv univalent) #:use-module (crates-io))

(define-public crate-univalent-0.1.0 (c (n "univalent") (v "0.1.0") (h "0g0qqg5ydfsn17x1s4708v3nllm585mlggg5pc6qjk3q2y9rc41q") (y #t)))

(define-public crate-univalent-0.1.1 (c (n "univalent") (v "0.1.1") (h "0qb6gfjykz03ngzdd3g43bhsx8qlcj8m4yykp2pdbalw6s92ls63")))

