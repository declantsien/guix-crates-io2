(define-module (crates-io un iv univec) #:use-module (crates-io))

(define-public crate-univec-0.1.0 (c (n "univec") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1aqpfk26rplz9yqzs685ri4qm9rw889lndvkmgppkm2y6v8636xb")))

(define-public crate-univec-0.2.0 (c (n "univec") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0k897v1h26gzn9m5r30g7m45cgfl322kilkmyjyclvlbhgyn6syg")))

(define-public crate-univec-0.3.0 (c (n "univec") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1nm80zr01ldfzxzvn0k0jm4lyx435z889l7w8yr6jrf9akhxnhyb")))

(define-public crate-univec-0.4.0 (c (n "univec") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0haa56pmrxcbfim1xhlm735949mxkqsas625a4gnvg7hlmcp7jrk")))

(define-public crate-univec-0.5.0 (c (n "univec") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "155m3zxi2ic02kvcsjz8vax6hnn86c2s7fwag630dr1hkwrnzmsd") (r "1.61.0")))

(define-public crate-univec-0.6.0 (c (n "univec") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1787g14c5gq96r53l5r8w5wlyny6ld443i12gk3fdgc87mv1pjqb") (r "1.61.0")))

(define-public crate-univec-0.6.1 (c (n "univec") (v "0.6.1") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1mv1zz1xsq7gc8w7qazxrwv56ak2shj7g8inh08c90d1x0r10nch") (y #t) (r "1.61.0")))

(define-public crate-univec-0.7.0 (c (n "univec") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "069ahad54jcpaqb96qf4dsh7lhfzbigwzawq6sp7752jhggjya7h") (r "1.61.0")))

(define-public crate-univec-0.8.0 (c (n "univec") (v "0.8.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1w7p81q0vbw9k35z6pdr9alybidg16z339svvqkfpby30k1f1gy5") (r "1.61.0")))

(define-public crate-univec-0.9.0 (c (n "univec") (v "0.9.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1vqzh6ibnv8dcy05x96fb9ax94qmjzmwkcsgwnk9wi3i5szgjl5f") (r "1.61.0")))

(define-public crate-univec-1.0.0 (c (n "univec") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0i8s9y4wz9xwsqcjaazbzjars35wz4f6bw9ln20iz4xyw67irkdm") (r "1.61.0")))

(define-public crate-univec-1.0.1 (c (n "univec") (v "1.0.1") (d (list (d (n "into_index") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "138x29zvyqbahi955f8pwba3h57p5lv844vh493iq6nx28rkyc50") (r "1.61.0")))

