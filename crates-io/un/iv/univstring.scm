(define-module (crates-io un iv univstring) #:use-module (crates-io))

(define-public crate-univstring-0.2.2 (c (n "univstring") (v "0.2.2") (d (list (d (n "widestring") (r "^0.2") (d #t) (k 0)))) (h "0wzpxg26jv1rqsdmqngfjzmi5v2yjdh57mbr2cgd74mv2v8dayys")))

(define-public crate-univstring-0.4.3 (c (n "univstring") (v "0.4.3") (d (list (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "17a40bs9phvsynwybfc89m9ja5gpn8lp242mp2majsjkm7igndcc")))

