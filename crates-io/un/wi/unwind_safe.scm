(define-module (crates-io un wi unwind_safe) #:use-module (crates-io))

(define-public crate-unwind_safe-0.0.1 (c (n "unwind_safe") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1l3452qybqz4g4g1zq54dmhjlyq9zjc40vz82wzqz5i8c9m8vgzh") (f (quote (("ui-tests" "nightly") ("nightly"))))))

(define-public crate-unwind_safe-0.1.0-rc1 (c (n "unwind_safe") (v "0.1.0-rc1") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1nnij6lvm8cfk6c112gicsy7nd149swrplp9yfl6fi88b9syls04") (f (quote (("ui-tests" "nightly") ("nightly"))))))

(define-public crate-unwind_safe-0.1.0 (c (n "unwind_safe") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "18vih4lxnxkw9l266b3hs3iykfy064n2jal9xz27a7rzxxywfxh9") (f (quote (("ui-tests" "nightly") ("nightly"))))))

