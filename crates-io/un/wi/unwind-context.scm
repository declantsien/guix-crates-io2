(define-module (crates-io un wi unwind-context) #:use-module (crates-io))

(define-public crate-unwind-context-0.1.0 (c (n "unwind-context") (v "0.1.0") (d (list (d (n "atomic_ref") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.5") (d #t) (k 2)))) (h "0zipw7ck8v22awlzswb91g6s7n4ax5ffjas31fhqphs6z993cbd7") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("detect-color-support" "dep:supports-color") ("custom-default-colors" "dep:atomic_ref")))) (r "1.70.0")))

(define-public crate-unwind-context-0.2.0 (c (n "unwind-context") (v "0.2.0") (d (list (d (n "atomic_ref") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.5") (d #t) (k 2)))) (h "1y8lss2g9isx4s66f5nzlhwwfav2m78q4wc2mz33n218c8crn6ad") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("detect-color-support" "dep:supports-color") ("custom-default-colors" "dep:atomic_ref")))) (r "1.70.0")))

(define-public crate-unwind-context-0.2.1 (c (n "unwind-context") (v "0.2.1") (d (list (d (n "atomic_ref") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.5") (d #t) (k 2)))) (h "12214fhlfnzbqixa4fghjghfvm6vcvgy2swpkgpq85nj3wfl1xdc") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("detect-color-support" "dep:supports-color") ("custom-default-colors" "dep:atomic_ref")))) (r "1.70.0")))

(define-public crate-unwind-context-0.2.2 (c (n "unwind-context") (v "0.2.2") (d (list (d (n "atomic_ref") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.5") (d #t) (k 2)))) (h "089f002rvh8ai9gklmzkxviwh5fn0428ndk0555qwliww0ch9s56") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("detect-color-support" "dep:supports-color") ("custom-default-colors" "dep:atomic_ref")))) (r "1.70.0")))

