(define-module (crates-io un wi unwind) #:use-module (crates-io))

(define-public crate-unwind-0.1.0 (c (n "unwind") (v "0.1.0") (d (list (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unwind-sys") (r "^0.1.0") (d #t) (k 0)))) (h "16dn2p4j9wf6smmb4si8jsff9nbwh4rns36p27dih22y3v0gh37s") (f (quote (("ptrace" "unwind-sys/ptrace"))))))

(define-public crate-unwind-0.2.0 (c (n "unwind") (v "0.2.0") (d (list (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unwind-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1bpqvn21m2mgj3wmx8027x70n6nyj0jb6anx4h0cb66pcrg6yir5") (f (quote (("ptrace" "unwind-sys/ptrace"))))))

(define-public crate-unwind-0.3.0 (c (n "unwind") (v "0.3.0") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unwind-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1jkrinm4jl9nsbzm45rcalx7fiakq47jl7nzxjjbi9jjf4vlqy49") (f (quote (("ptrace" "unwind-sys/ptrace"))))))

(define-public crate-unwind-0.4.0 (c (n "unwind") (v "0.4.0") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unwind-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1pw6v1l4ydnnjix18a1mlrp0n60vccpb20j0vlb5khjqla5w6dxp") (f (quote (("ptrace" "unwind-sys/ptrace"))))))

(define-public crate-unwind-0.4.1 (c (n "unwind") (v "0.4.1") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unwind-sys") (r "^0.1.3") (d #t) (k 0)))) (h "01syxbbr539k42clgjk368zr98wwgw7cr24i46pxnwbbilydmq7h") (f (quote (("ptrace" "unwind-sys/ptrace"))))))

(define-public crate-unwind-0.4.2 (c (n "unwind") (v "0.4.2") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unwind-sys") (r "^0.1.4") (d #t) (k 0)))) (h "10xh84258xd0l63jy4ghqkpmyj9z8rvcfpzi9dnab6s5z0wh8a9q") (f (quote (("ptrace" "unwind-sys/ptrace"))))))

