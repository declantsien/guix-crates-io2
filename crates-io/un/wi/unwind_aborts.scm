(define-module (crates-io un wi unwind_aborts) #:use-module (crates-io))

(define-public crate-unwind_aborts-0.1.0 (c (n "unwind_aborts") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0cp2sr2s71xky5nbjrnqbqr76xckzz0rdvbqrb1rg7awyzr3hx4b")))

(define-public crate-unwind_aborts-0.1.1 (c (n "unwind_aborts") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1g44zgaacjn50zzvz1ac9zs9fs0b36j1kqkvb5b5zzlvkssq7pjy")))

