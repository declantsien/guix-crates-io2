(define-module (crates-io un de undetected-chromedriver) #:use-module (crates-io))

(define-public crate-undetected-chromedriver-0.1.0 (c (n "undetected-chromedriver") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1kzzs8vl6fqf5l7z9gzjp6r6x992wwyn5r3jv2xzbm9wr66gic71")))

(define-public crate-undetected-chromedriver-0.1.1 (c (n "undetected-chromedriver") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0fsbl952hpbvsd8v5bcrqbkfv9z53jihmni9fdizmzfvwjl5h6mz")))

(define-public crate-undetected-chromedriver-0.1.2 (c (n "undetected-chromedriver") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "00vlbiry3sipm4d0r9cnxwc29gr26b6w3ynwplppq1piirswalsa")))

