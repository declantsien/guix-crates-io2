(define-module (crates-io un de undepend) #:use-module (crates-io))

(define-public crate-undepend-0.1.0 (c (n "undepend") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2") (d #t) (k 0)))) (h "07xpk8gdly8ph36xjjzzfr3mqyj2130yrp39caiz0pl137493p50")))

(define-public crate-undepend-0.1.1 (c (n "undepend") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 2)) (d (n "toml_edit") (r "^0.2") (d #t) (k 0)))) (h "09kysi126f99cr5v8apn8l0g3ks62liqjnc2xs664578spcfvnzb")))

