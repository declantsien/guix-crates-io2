(define-module (crates-io un de underscore_sys) #:use-module (crates-io))

(define-public crate-underscore_sys-0.1.0 (c (n "underscore_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "06p7pms0n537crzhzl0zp6zv9c1ga8q6nqmmxd9iizg7w34wlx35") (y #t)))

(define-public crate-underscore_sys-0.1.1 (c (n "underscore_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "1glfsxi6rf56kpg6lb2i9ls6nwkkf9zh3b20klx10x9p878gsl82") (y #t)))

