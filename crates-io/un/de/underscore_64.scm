(define-module (crates-io un de underscore_64) #:use-module (crates-io))

(define-public crate-underscore_64-0.1.0 (c (n "underscore_64") (v "0.1.0") (d (list (d (n "underscore_sys") (r "^0.1.0") (d #t) (k 0)))) (h "1gy5ncrm8blhnvkm195gjh43x7xnbgrr9n2j1jxxn7iwjs2w18wj") (f (quote (("minsize")))) (y #t)))

(define-public crate-underscore_64-0.2.0 (c (n "underscore_64") (v "0.2.0") (d (list (d (n "_sys") (r "^0.1.1") (d #t) (k 0) (p "underscore_sys")))) (h "0hhnm0z7nxxwxp3z0pgmngj5sc5aacjpcq8cv139dx502wr2sz83") (f (quote (("minsize")))) (y #t)))

