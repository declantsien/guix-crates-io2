(define-module (crates-io un de underthesea_core) #:use-module (crates-io))

(define-public crate-underthesea_core-1.0.4 (c (n "underthesea_core") (v "1.0.4") (d (list (d (n "crfs") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13kxnxynbqxr8x0v1qbl7pklpg5f1a0ingf0x5fwq92gppxw667j")))

