(define-module (crates-io un de under_derive) #:use-module (crates-io))

(define-public crate-under_derive-0.2.3 (c (n "under_derive") (v "0.2.3") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1wd7bn4vy8wc9i2zgli5xlkbp1j8ar36h6jva0g67kk8a7cwjkm7")))

(define-public crate-under_derive-0.2.4 (c (n "under_derive") (v "0.2.4") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11zir0bq7z7kbm405jv5v0w2ldq00df9ijrilbvasymd5ymwwd7g")))

