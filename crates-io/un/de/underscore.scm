(define-module (crates-io un de underscore) #:use-module (crates-io))

(define-public crate-underscore-0.0.1 (c (n "underscore") (v "0.0.1") (h "04m2xlyvk0r82fmigx0lhb285vaxlnk83cibc9jg68mky8f37vps")))

(define-public crate-underscore-0.0.2 (c (n "underscore") (v "0.0.2") (h "1dkaxps3cmmjzg79bak3qdl0b5zb65q8pr5r8yx10cv78rjzdxjw")))

