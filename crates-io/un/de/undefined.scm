(define-module (crates-io un de undefined) #:use-module (crates-io))

(define-public crate-undefined-0.1.0 (c (n "undefined") (v "0.1.0") (d (list (d (n "diesel") (r "^2.1.0") (d #t) (k 0)) (d (n "juniper") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0gnvr4n3j1m5mlkprvvndna01q29grcmlnqlbqaly37qjgw2n9g3") (f (quote (("serde") ("juniper"))))))

(define-public crate-undefined-0.1.1 (c (n "undefined") (v "0.1.1") (d (list (d (n "diesel") (r "^2.1.0") (d #t) (k 0)) (d (n "juniper") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "00mkf97s6j22j56rpziy6crgab3343z1rjl1798xm90cj8pavrak") (f (quote (("serde") ("juniper"))))))

(define-public crate-undefined-0.1.2 (c (n "undefined") (v "0.1.2") (d (list (d (n "diesel") (r "^2.1.0") (d #t) (k 0)) (d (n "juniper") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0mpvp6jwwh9d2wbmgn3knhi9q9877fy1xiy7fh4yz2xd9yys90v1") (f (quote (("serde") ("juniper"))))))

(define-public crate-undefined-0.1.3 (c (n "undefined") (v "0.1.3") (d (list (d (n "diesel") (r "^2.1.0") (d #t) (k 0)) (d (n "juniper") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1xxxamk244k4398br3qiq2hklyh86yisc2gq3wan78b8wfb5fzkn") (f (quote (("serde") ("juniper"))))))

