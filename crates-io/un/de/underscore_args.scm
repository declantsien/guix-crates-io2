(define-module (crates-io un de underscore_args) #:use-module (crates-io))

(define-public crate-underscore_args-0.1.0 (c (n "underscore_args") (v "0.1.0") (h "1bmbjismd0fwzi7npfd9fi387qf6w321vn9pb8mqw56dgkicvycm")))

(define-public crate-underscore_args-0.1.1 (c (n "underscore_args") (v "0.1.1") (h "0s6axsbriaxy80c70qkrq6kzhhpawsbnvxcgynq4cmlgw5sj4871")))

(define-public crate-underscore_args-0.1.2 (c (n "underscore_args") (v "0.1.2") (h "0vqsmd4cd40ihgxxw0m6dnvpx6ymbr9kd9rpp9rwyr155sbkzajy")))

