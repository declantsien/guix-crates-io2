(define-module (crates-io un iu uniui_core) #:use-module (crates-io))

(define-public crate-uniui_core-0.0.1 (c (n "uniui_core") (v "0.0.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "01klnvgz71mwd7mxh23asbhrviags1i64av0xc2r9x544vj8vali")))

(define-public crate-uniui_core-0.0.2 (c (n "uniui_core") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fyg5q11qg72w6rz8lhxxx6am2hxmp6scwhspyck57lsfqlji7yv")))

(define-public crate-uniui_core-0.0.3 (c (n "uniui_core") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dl9faf39jxsr959if6jcsp8rky9jv76ijxycfigzbp6dhmy3rh2")))

(define-public crate-uniui_core-0.0.4 (c (n "uniui_core") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1i7z3xx45bn8sy2ssxk08qnrz9agkdm7amr24za7zy2v7vz63yxx")))

