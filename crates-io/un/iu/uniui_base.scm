(define-module (crates-io un iu uniui_base) #:use-module (crates-io))

(define-public crate-uniui_base-0.0.1 (c (n "uniui_base") (v "0.0.1") (d (list (d (n "uniui_core") (r "^0.0.4") (d #t) (k 0)) (d (n "uniui_gui") (r "^0.0.16") (d #t) (k 0)) (d (n "uniui_layout_linear_layout") (r "^0.0.10") (d #t) (k 0)) (d (n "uniui_widget_button") (r "^0.0.10") (d #t) (k 0)) (d (n "uniui_widget_label") (r "^0.0.10") (d #t) (k 0)) (d (n "uniui_widget_textedit") (r "^0.0.11") (d #t) (k 0)) (d (n "uniui_wrapper_minimalsize") (r "^0.0.2") (d #t) (k 0)))) (h "0clvfm78p8rghg3d5jsyd82yr7hka9s46fdimr6z3h20kascccyy")))

