(define-module (crates-io un iu uniui_gui_macro) #:use-module (crates-io))

(define-public crate-uniui_gui_macro-0.0.1 (c (n "uniui_gui_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hm7yj7fvqy7dz3x807771a2aa3zsknlaz9hin805g7rb9xzxhk4")))

(define-public crate-uniui_gui_macro-0.0.2 (c (n "uniui_gui_macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hvlznaacgdccnxhc2q2jl8kpdwkavll7c2s9v1xnvx2ziyp9wd1")))

(define-public crate-uniui_gui_macro-0.0.5 (c (n "uniui_gui_macro") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.6") (d #t) (k 0)))) (h "1kjp8hh6gm13rlllvpfs7iqbajy6fh11bhly2nd897a386fs4cqk")))

(define-public crate-uniui_gui_macro-0.0.6 (c (n "uniui_gui_macro") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.7") (d #t) (k 0)))) (h "13y16s6zj10fzpp15d1hhgl3m8c1imgji8m6f6j4rxzc3ww3zg1c")))

(define-public crate-uniui_gui_macro-0.0.7 (c (n "uniui_gui_macro") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.8") (d #t) (k 0)))) (h "0q4iprqq2f53dsnrcfia13yqbr7lbbpvsbqhqr59rzb321r2978x")))

(define-public crate-uniui_gui_macro-0.0.8 (c (n "uniui_gui_macro") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.9") (d #t) (k 0)))) (h "0kw87dzqdq0wwbv5s8r0ha7xfcgdra7mf51n60j187lyy0zyvvmg")))

(define-public crate-uniui_gui_macro-0.0.9 (c (n "uniui_gui_macro") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.10") (d #t) (k 0)))) (h "0cqwq0l4s3zv1isi9d6qkwsmsj72nkx0w13pvj6i9wi8fxvcpr0a")))

(define-public crate-uniui_gui_macro-0.0.10 (c (n "uniui_gui_macro") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.11") (d #t) (k 0)))) (h "1znmm86b7ajmc5dpqflpw8w3llwkyb6y0d0522scxbks7gr04azk")))

(define-public crate-uniui_gui_macro-0.0.11 (c (n "uniui_gui_macro") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.12") (d #t) (k 0)))) (h "0lhlvvzs322mg4dkgw4w3hk95adg65qmggrgxnfh3wqd8073m9dy")))

(define-public crate-uniui_gui_macro-0.0.12 (c (n "uniui_gui_macro") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.13") (d #t) (k 0)))) (h "18vswv15krm7mxya73pkrxc5rv6rfxnygjsj0dn3az3s6cdlahzm")))

(define-public crate-uniui_gui_macro-0.0.14 (c (n "uniui_gui_macro") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.14") (d #t) (k 0)))) (h "1sg6jgxkkpvyy0mwm11v3p0s3qb4dgmrw6xd2abcwi0hwjsfwblz")))

(define-public crate-uniui_gui_macro-0.0.15 (c (n "uniui_gui_macro") (v "0.0.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uniui_build") (r "^0.0.15") (d #t) (k 0)))) (h "123g90r2gqbl2p32i6wbybhpg1wqrb104ksgjabn2ccv6da3bk88")))

