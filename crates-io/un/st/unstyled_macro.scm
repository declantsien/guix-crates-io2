(define-module (crates-io un st unstyled_macro) #:use-module (crates-io))

(define-public crate-unstyled_macro-0.1.1 (c (n "unstyled_macro") (v "0.1.1") (d (list (d (n "leptos") (r "^0.4") (o #t) (d #t) (k 0)))) (h "044j4pdnxbyxm6hryx70kv8kfp7gmqklyr79nl7001cjac0jamwx") (f (quote (("leptos-example" "leptos"))))))

(define-public crate-unstyled_macro-0.1.2 (c (n "unstyled_macro") (v "0.1.2") (d (list (d (n "leptos") (r "^0.4") (o #t) (d #t) (k 0)))) (h "10pfh6kaa6fif6fmrfka513l0pvnl6wkkni394szg0ffrps17krm") (f (quote (("leptos-example" "leptos"))))))

(define-public crate-unstyled_macro-0.1.3 (c (n "unstyled_macro") (v "0.1.3") (d (list (d (n "leptos") (r "^0.4") (o #t) (d #t) (k 0)))) (h "18n1npg5dkcll5ml5g2fjymf1znqb91r7q5a8jp03yc6z2kp3qdb") (f (quote (("leptos-example" "leptos") ("css-block-lint"))))))

