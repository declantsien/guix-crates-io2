(define-module (crates-io un st unstringify) #:use-module (crates-io))

(define-public crate-unstringify-0.1.0 (c (n "unstringify") (v "0.1.0") (h "0dfwzag2lxlkqnwgrl1vkj8j82l23wzqc2zjyr13a3l54r9z97xp")))

(define-public crate-unstringify-0.1.1 (c (n "unstringify") (v "0.1.1") (h "14mpvs3iy7jky42fj9vpqbxd6w546s104jf6yi9478mz0i5rcz1h")))

(define-public crate-unstringify-0.1.2 (c (n "unstringify") (v "0.1.2") (h "0z5ddj3cd8zwa00llgw2nqz85fqxblbh1lax7dl1mxh4b8x5jfp7")))

(define-public crate-unstringify-0.1.3 (c (n "unstringify") (v "0.1.3") (h "0fp5z848p3rflvyplckadwvfd8ib93ghy8skb0spj0mzl9vsayyp")))

(define-public crate-unstringify-0.1.4-rc1 (c (n "unstringify") (v "0.1.4-rc1") (h "0w06wlap4z2igr1ijf00f5m731z6nkm07s9y9x4djj7g1q0spwvb")))

(define-public crate-unstringify-0.1.4 (c (n "unstringify") (v "0.1.4") (h "0pkbngmchl95m1vairvz6ky9vrw9jsni3f8mi4s2klpa41jdc4ln")))

