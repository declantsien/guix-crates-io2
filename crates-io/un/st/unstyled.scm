(define-module (crates-io un st unstyled) #:use-module (crates-io))

(define-public crate-unstyled-0.1.0 (c (n "unstyled") (v "0.1.0") (d (list (d (n "leptos") (r "^0.4") (d #t) (k 2)))) (h "17q3mjzmlw5j2a3hy3yckazhnid6gli7qaj688bk3g1af1mx4yd0")))

(define-public crate-unstyled-0.1.1 (c (n "unstyled") (v "0.1.1") (d (list (d (n "unstyled_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1vks2mq0iihz7mrg6l8gyiy7hg3i5n8rh009wdab9kwd8vqy124g")))

(define-public crate-unstyled-0.1.2 (c (n "unstyled") (v "0.1.2") (d (list (d (n "unstyled_macro") (r "^0.1.2") (d #t) (k 0)))) (h "1kh4zy8isaadky0v5aja1xizgxvr1p2v3yqh99pzmkaa8hvryv4b")))

(define-public crate-unstyled-0.1.3 (c (n "unstyled") (v "0.1.3") (d (list (d (n "unstyled_macro") (r "^0.1.3") (d #t) (k 0)))) (h "0czm1g1qdy28hmh6wdz9d68m72z9harcpbb9x90nks7gsw430f4h") (f (quote (("css-block-lint" "unstyled_macro/css-block-lint"))))))

