(define-module (crates-io un im unimarkup-inline) #:use-module (crates-io))

(define-public crate-unimarkup-inline-0.4.0 (c (n "unimarkup-inline") (v "0.4.0") (d (list (d (n "logid") (r "^0.6.1") (f (quote ("diagnostics" "causes"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unimarkup-render") (r "^0") (d #t) (k 0)))) (h "0nll4ng05ipfmxnj05kzksqlzj2ww0rddj05q830vskfc012pa5j")))

