(define-module (crates-io un im unimarkup-render) #:use-module (crates-io))

(define-public crate-unimarkup-render-0.4.0 (c (n "unimarkup-render") (v "0.4.0") (d (list (d (n "logid") (r "^0.6.1") (f (quote ("diagnostics" "causes"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "syntect") (r "^4.6") (d #t) (k 0)))) (h "0zls92rbzrwsn429m8h3b0li9hwh0hvdhs6rbz8sswridzbax9hn")))

