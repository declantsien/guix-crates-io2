(define-module (crates-io un im unimock_macros) #:use-module (crates-io))

(define-public crate-unimock_macros-0.1.0 (c (n "unimock_macros") (v "0.1.0") (d (list (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "0vzmjf1rhhplirighb6bm944fnn45qyxhf5h93kla1fp25l9x9v9")))

(define-public crate-unimock_macros-0.2.0-beta.0 (c (n "unimock_macros") (v "0.2.0-beta.0") (d (list (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0i8vbap1bxhjzshvdqm96alba9drhakz172sy46fskay59fb5f15")))

(define-public crate-unimock_macros-0.2.0 (c (n "unimock_macros") (v "0.2.0") (d (list (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1gr479731gakli2hnqlfx08vsrq8985334cb80xvv8pac95kl3i7")))

(define-public crate-unimock_macros-0.2.1 (c (n "unimock_macros") (v "0.2.1") (d (list (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0lcgnzny6jv4p1n9df58lrrglyf496dpxci3yy9ncxha17d2wakd")))

(define-public crate-unimock_macros-0.3.0 (c (n "unimock_macros") (v "0.3.0") (d (list (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0f5hqsjk8sy5936851gsmighbl3f1yy5k98fm4gs8mz4p7vjlb76")))

(define-public crate-unimock_macros-0.3.1 (c (n "unimock_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0rqfbzzmfy97530wwhnskjrgf68xz445w09b6nkfr3cgvxrikgj9")))

(define-public crate-unimock_macros-0.3.2 (c (n "unimock_macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1wq6pipdw9ybydsz4zahpzdh1580nl4gxmzvxpwvnqd4kjc8cbkw")))

(define-public crate-unimock_macros-0.3.3 (c (n "unimock_macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1xaz83wj4y3w3mra9r57gdmw48g1qbq5bwfm4awc4smvw1d3pb5q")))

(define-public crate-unimock_macros-0.3.4 (c (n "unimock_macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0vhdfk5yzyfwcdalxaskbr1z75rx7z4vqiimxxgyb16rnyv9f9wl")))

(define-public crate-unimock_macros-0.3.5 (c (n "unimock_macros") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ry5hypmqfm5l4d027fzgr9y1lcfagid5kij1caflfyy8681d1g3")))

(define-public crate-unimock_macros-0.3.6 (c (n "unimock_macros") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1b9lizlyvviigg4qjd8qsb6adaz25in462rh80vy4810jxckig0c")))

(define-public crate-unimock_macros-0.3.7 (c (n "unimock_macros") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "111j9wxc6jqxnmd6sfa4knzkvvvq6qv89g6hmn46jg1shrm3g6mg")))

(define-public crate-unimock_macros-0.3.8 (c (n "unimock_macros") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "173y5fxnygvk07y5vxsvz6zcck9x1kcsrsfln713gx3a3hzh0i07")))

(define-public crate-unimock_macros-0.3.9 (c (n "unimock_macros") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1mirglcrpk6byfn1vnyg9ikp7pvaidm2pr33z5c0m1fgi43yp357")))

(define-public crate-unimock_macros-0.3.10 (c (n "unimock_macros") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1k3w1flhak5r55ynlj5g7d53mk4nr77apadmfxs02blpygi29clx")))

(define-public crate-unimock_macros-0.4.0-alpha.0 (c (n "unimock_macros") (v "0.4.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "14q002b0qnvrxyjfylh30rc3jj9bvbzm1xbfpgrcl02445cnrzmn")))

(define-public crate-unimock_macros-0.4.0-alpha.1 (c (n "unimock_macros") (v "0.4.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1imjrvq23bnigrgggr2pz145ds2r4vcffz2wcyh8ai35mybh7ihj")))

(define-public crate-unimock_macros-0.4.0-alpha.2 (c (n "unimock_macros") (v "0.4.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ryp6plb0mmwk110q89bwx4vffp2x3cs9ik3ra7lqwxfdfxhq5ic")))

(define-public crate-unimock_macros-0.4.0-alpha.3 (c (n "unimock_macros") (v "0.4.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0j624dys2a389653gwz486r7myprw0rixp05391spg4f23vqi00q")))

(define-public crate-unimock_macros-0.4.0 (c (n "unimock_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "05f6jk9x6cvyckzbxmw7kcnsqfcfj68chm5lw4pxkygvdiwjr2sy")))

(define-public crate-unimock_macros-0.4.1 (c (n "unimock_macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0d7kffsqkfa55zi3nz2zy7gbq304lk7c9xk51vckdbs0w3lz25na")))

(define-public crate-unimock_macros-0.4.2 (c (n "unimock_macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "09si7swiz4pcwxd5facazdhgi2qynv7n8dp0p1q34qmhhzhq7fvk")))

(define-public crate-unimock_macros-0.4.3 (c (n "unimock_macros") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1la3gl6hqyvhqcc6yw5dwc7rnjycp7mdc2gkkzhkqsn4afa7z2ks")))

(define-public crate-unimock_macros-0.4.4 (c (n "unimock_macros") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0xzm49imacz77dgqrvrh74ir3p83ryc2qypq2n55c0ls437dvf9k")))

(define-public crate-unimock_macros-0.4.5 (c (n "unimock_macros") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "15qv2r4bkfp7rbvr636b06p8jkzvczx9drjszkqlvckla0vw4wac")))

(define-public crate-unimock_macros-0.4.6 (c (n "unimock_macros") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1lg18254wkvs1d8qb5s9j4kzpdz8j183kxqrwzb535nsi62s0pyy")))

(define-public crate-unimock_macros-0.4.7 (c (n "unimock_macros") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0fcphmkhnvvz9zsas2ria2k5613xwqs4hyg98443wansh4bshq4b")))

(define-public crate-unimock_macros-0.4.8 (c (n "unimock_macros") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0mcrq781dlb6b3hvhv9vbflingv1k5r2h0aja7wg7bji7jqz6zf7")))

(define-public crate-unimock_macros-0.4.9 (c (n "unimock_macros") (v "0.4.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0rb1ndznmgqrgzxnxrvw9lz35fxfyvan9v27hmw7g45r6grpqrv4")))

(define-public crate-unimock_macros-0.4.10 (c (n "unimock_macros") (v "0.4.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "15z8lis35f4n3xqsry1ji5bjdypwgjlzy1ra4xpwsiqag0fj509f")))

(define-public crate-unimock_macros-0.4.11 (c (n "unimock_macros") (v "0.4.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1mgqy5sssarkx5xir89r4d55l5whg35shy85x1qf9hfvgw7vpdqw")))

(define-public crate-unimock_macros-0.4.12 (c (n "unimock_macros") (v "0.4.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "05pzsjqbhh06lmzrlgafv6llqf2bgff9bqmf4x8d7r78fnrwjx65")))

(define-public crate-unimock_macros-0.5.0 (c (n "unimock_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0rvqvbwzb82ppzj6n0zbd4c7gzr6y98wa16vnk85zcphcg4vhdg1")))

(define-public crate-unimock_macros-0.5.1 (c (n "unimock_macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0slvrmq66adcajajrnpd7y9iny1isdm17brysqrqgf3ndiqblpwl")))

(define-public crate-unimock_macros-0.5.2 (c (n "unimock_macros") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "173800wq46flrlapf2y550xv3sbvczawns8k9ayfmpsajga61gr1")))

(define-public crate-unimock_macros-0.5.3 (c (n "unimock_macros") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "02vngd800d2rmixxjw2ryy0awi0f6dh78646lsq5y5x0d7vv656c")))

(define-public crate-unimock_macros-0.5.4 (c (n "unimock_macros") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0w4inwqw45gr3vd1w43118s6z3c4rc6gnkd1lwbznv2gfr8nckqv")))

(define-public crate-unimock_macros-0.5.5 (c (n "unimock_macros") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1kqbkryj0n8yh0shvv08j4kkppj9km1ahlzjkk4gdn0ihbaalxlc")))

(define-public crate-unimock_macros-0.5.6 (c (n "unimock_macros") (v "0.5.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "16iqa1ap57clgd0rb4qsf6802fhm4yzlpvwh0dncp95l2l87m8h6")))

(define-public crate-unimock_macros-0.5.7 (c (n "unimock_macros") (v "0.5.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1csc50589hm64rmk8gvxxkzmg527m7a9d37lqpdky6aj9nb5p4w4")))

(define-public crate-unimock_macros-0.5.8 (c (n "unimock_macros") (v "0.5.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1r420c62w780ppsjj2sry2bhf005icz2lzbxvhrjia3sp2hwzz7g")))

(define-public crate-unimock_macros-0.6.0 (c (n "unimock_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1amgabpwpjfpq120iy8c7kx6xcpk6rwpc9wpyd55ih6zn8z217jj")))

(define-public crate-unimock_macros-0.6.1 (c (n "unimock_macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0bws5ggfarq7iwx6zn3qkjqpxkk1yc2kx4nwayhs8fm6v07bp11r")))

(define-public crate-unimock_macros-0.6.2 (c (n "unimock_macros") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "10nwkk20fg6xldzjgaj92adbss0mx6skx14x748k1g0xqlhb98iz")))

(define-public crate-unimock_macros-0.6.3 (c (n "unimock_macros") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "01p31mhhas805mdp47rz1id2hxp25xpn5py3izgh5aw6xzc6caii")))

(define-public crate-unimock_macros-0.6.4 (c (n "unimock_macros") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0yydc9jchdcy8ha5y3gk7snc3apnlq1p851924wnsfxgp9ca3hvq")))

(define-public crate-unimock_macros-0.6.5 (c (n "unimock_macros") (v "0.6.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0qv8ywkl65nzb7r05aqy07mlc60mfnpvflw3n7rkr35w91w969fj")))

(define-public crate-unimock_macros-0.6.6 (c (n "unimock_macros") (v "0.6.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "16ng3bhq3diqlinir80dnjbqdraznv4l782lj9rnx5l6rknisbm0")))

