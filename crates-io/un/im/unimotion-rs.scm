(define-module (crates-io un im unimotion-rs) #:use-module (crates-io))

(define-public crate-unimotion-rs-0.1.0 (c (n "unimotion-rs") (v "0.1.0") (d (list (d (n "base64") (r "0.21.*") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "macaddr") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serialport") (r "4.*") (d #t) (k 0)))) (h "0i72xjb5wapn2b5rqn614dxph6rki13gklc8b0igjpmzgxp6h74f")))

