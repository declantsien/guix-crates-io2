(define-module (crates-io un im unimarkup-lsp) #:use-module (crates-io))

(define-public crate-unimarkup-lsp-0.1.0 (c (n "unimarkup-lsp") (v "0.1.0") (d (list (d (n "insta") (r "^1.29.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "lsp-server") (r "^0.7.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unimarkup-core") (r "^0") (d #t) (k 0)) (d (n "unimarkup-inline") (r "^0") (d #t) (k 0)))) (h "0lqx61d7ac2cmrwc802xxrlq0x305cvyic0vrvq04ygjg9n5sq43")))

