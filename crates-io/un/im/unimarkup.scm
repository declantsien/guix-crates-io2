(define-module (crates-io un im unimarkup) #:use-module (crates-io))

(define-public crate-unimarkup-0.4.0 (c (n "unimarkup") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "logid") (r "^0.6.1") (f (quote ("diagnostics" "causes"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "unimarkup-core") (r "^0") (d #t) (k 0)))) (h "0j3kf1y9sqsgl3xnsscqvabn6lw8ss8arxpihh9nbx3a1vmxzby9")))

