(define-module (crates-io un im unimpl) #:use-module (crates-io))

(define-public crate-unimpl-0.1.0 (c (n "unimpl") (v "0.1.0") (h "1njhhzxrdfvg9cbh2k0sd0v5z7a9lsrxvdg3mm957wdjcqxlnj8b")))

(define-public crate-unimpl-0.1.1 (c (n "unimpl") (v "0.1.1") (d (list (d (n "panic-message") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1rcypxxkclb3kn585vb49xk9qmf0ljfr9bsrd4qx3ws1x7nsk3sd")))

(define-public crate-unimpl-0.1.2 (c (n "unimpl") (v "0.1.2") (d (list (d (n "panic-message") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1xjh05yyw3v13xkayvg0mrp8s9cfb1xsd2pszsf9li5p74c9rmjf")))

