(define-module (crates-io un ba unbase) #:use-module (crates-io))

(define-public crate-unbase-0.0.0 (c (n "unbase") (v "0.0.0") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "sha2") (r "^0.4.0") (d #t) (k 0)))) (h "1zzpd1d6wmfg6ph7wm4pghdaa8p067l3y2yl6bfpx29y8r21g2r2")))

(define-public crate-unbase-0.1.0 (c (n "unbase") (v "0.1.0") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)) (d (n "sha2") (r "^0.4.0") (d #t) (k 0)))) (h "066xq2cp5kilypnh39iqn7ks4m33sw4vww6bdsv57l8aqqpilx8s") (y #t)))

(define-public crate-unbase-0.1.1 (c (n "unbase") (v "0.1.1") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)) (d (n "sha2") (r "^0.4.0") (d #t) (k 0)))) (h "0my8qab9s0rn7wsqhvz96n7mzm1vlify28wak6rw4xzqm5hfqbdb") (y #t)))

(define-public crate-unbase-0.0.2 (c (n "unbase") (v "0.0.2") (d (list (d (n "futures-await") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)) (d (n "sha2") (r "^0.4.0") (d #t) (k 0)))) (h "1j7vgr4ars30sngwzkw49szv806g8pcvbnba5s55q7pya6pa7a5k")))

