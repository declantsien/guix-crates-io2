(define-module (crates-io un kn unknownrori-simple-thread-pool) #:use-module (crates-io))

(define-public crate-unknownrori-simple-thread-pool-0.1.0 (c (n "unknownrori-simple-thread-pool") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1c5vlqnd9qnihvgkwf8ak9smc6vdwivz480a79a914y85nhfbp8j") (f (quote (("mpsc") ("default" "crossbeam")))) (y #t) (s 2) (e (quote (("crossbeam" "dep:crossbeam-channel"))))))

(define-public crate-unknownrori-simple-thread-pool-0.1.1 (c (n "unknownrori-simple-thread-pool") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "17127gmzdga3d1604fy3qfkyz43vvmcalaak7hxi3kgg9bwf5fsb") (f (quote (("mpsc") ("default" "crossbeam")))) (y #t) (s 2) (e (quote (("crossbeam" "dep:crossbeam-channel"))))))

(define-public crate-unknownrori-simple-thread-pool-0.1.2 (c (n "unknownrori-simple-thread-pool") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "052fjmn3iawd5v81gg4jmrf5yiqdxyc8hd8vl33q62y50c5nv8pr") (f (quote (("mpsc") ("default" "crossbeam")))) (y #t) (s 2) (e (quote (("crossbeam" "dep:crossbeam-channel"))))))

(define-public crate-unknownrori-simple-thread-pool-0.1.3 (c (n "unknownrori-simple-thread-pool") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1qw33w66cisdwbv5cdwnrks8zqqvcjhrznhsfbby8a402zapnp7s") (f (quote (("mpsc") ("default" "crossbeam")))) (s 2) (e (quote (("crossbeam" "dep:crossbeam-channel"))))))

(define-public crate-unknownrori-simple-thread-pool-0.1.4 (c (n "unknownrori-simple-thread-pool") (v "0.1.4") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "040x2iksmjzgv1ppgm9f9fga1pxkwpqjbjrqfgw6z6pbfkhwhzx3") (f (quote (("mpsc") ("default" "crossbeam")))) (s 2) (e (quote (("crossbeam" "dep:crossbeam-channel"))))))

(define-public crate-unknownrori-simple-thread-pool-0.1.5 (c (n "unknownrori-simple-thread-pool") (v "0.1.5") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1apzp3k50caa03x9m4alqg3x09h2g1xgiv1kqc4xpdzx7gnr8kvz") (f (quote (("mpsc") ("default" "crossbeam")))) (y #t) (s 2) (e (quote (("crossbeam" "dep:crossbeam-channel"))))))

(define-public crate-unknownrori-simple-thread-pool-0.2.0 (c (n "unknownrori-simple-thread-pool") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "18w7mbai1lcd6y8f9z1pgipc4fmvndgrr5m4jr372bqhq7nc2jk5") (f (quote (("mpsc") ("default" "crossbeam")))) (s 2) (e (quote (("crossbeam" "dep:crossbeam-channel"))))))

