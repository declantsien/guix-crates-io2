(define-module (crates-io un sc unscrupulous_derive) #:use-module (crates-io))

(define-public crate-unscrupulous_derive-0.1.0 (c (n "unscrupulous_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "04fdhvs8y8riila1k8k40hakpry5q1zb18g0d0vrpjm2i5vf077g")))

