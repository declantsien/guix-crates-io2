(define-module (crates-io un sc unscrupulous) #:use-module (crates-io))

(define-public crate-unscrupulous-0.1.0 (c (n "unscrupulous") (v "0.1.0") (d (list (d (n "unscrupulous_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1qv2nvyws3dnvml1a6naw27dd6d1irc1iv8xwaz65ms7gx29zqq1") (s 2) (e (quote (("derive" "dep:unscrupulous_derive"))))))

