(define-module (crates-io un ch unchecked_unwrap) #:use-module (crates-io))

(define-public crate-unchecked_unwrap-0.1.0 (c (n "unchecked_unwrap") (v "0.1.0") (h "16vf7z80z0p2czj5f7nl2xvyk7l1yaxa16yvm382yccb5vm67g1s") (f (quote (("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-0.8.0 (c (n "unchecked_unwrap") (v "0.8.0") (h "1x805vbv9gifx6m9qn5bs99fpqai3p6rzd4bnzxk5q5jy7gb6zw6") (f (quote (("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-0.9.0 (c (n "unchecked_unwrap") (v "0.9.0") (h "0nbr9h2h97gs03ryz9grjl76bps4h5m271d8ypdcr496fy8py32w") (f (quote (("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-1.0.0 (c (n "unchecked_unwrap") (v "1.0.0") (h "07mym4dnj9zjh0bmzdcz5f120kv408wjya7jii64d8lrkc6l6jh6") (f (quote (("doc_include") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-1.0.1 (c (n "unchecked_unwrap") (v "1.0.1") (h "1nswnpff2bcl3rdzsdjxp9712b7y7lv4wg28j2mbz8bv5cxi7ci1") (f (quote (("doc_include") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-2.0.0 (c (n "unchecked_unwrap") (v "2.0.0") (h "17z4iz6lc6h9qp14fyfyv1i9kwp6mvagzrl3ds1b12phj2iw1xw8") (f (quote (("nightly") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-2.0.1 (c (n "unchecked_unwrap") (v "2.0.1") (h "003j3i7mwqwm3jxq70w7lhx8fb9ag1scqryq6rrfcc28c9bskzc3") (f (quote (("nightly") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-2.0.2 (c (n "unchecked_unwrap") (v "2.0.2") (h "0kifjkq1qj85faja0rx4n5rdl73nfn6lpax91bg1vdd10f17b8sd") (f (quote (("nightly") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-3.0.0 (c (n "unchecked_unwrap") (v "3.0.0") (h "1n72gf8sz8mzdpabwl2gml3vhz3p9y38fndwyb8w4h700sig691w") (f (quote (("nightly") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-4.0.0 (c (n "unchecked_unwrap") (v "4.0.0") (h "1220nyxq1458vbdhjjjmvv0bkv0pdkgnljf1aylci3nd49arc99j") (f (quote (("default" "debug_checks") ("debug_checks")))) (r "1.54")))

