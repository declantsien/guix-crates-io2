(define-module (crates-io un ch unchained) #:use-module (crates-io))

(define-public crate-unchained-0.1.0 (c (n "unchained") (v "0.1.0") (h "15kmap19i1xa72bhm8wski7c7c4xzy06wwq1f2w91s43avr06fcw")))

(define-public crate-unchained-0.1.1 (c (n "unchained") (v "0.1.1") (h "1iwhnriknazr9342cxxn7m280lbqddnmw1iinjvsfrxjhdqbsjrh")))

(define-public crate-unchained-0.1.2 (c (n "unchained") (v "0.1.2") (h "03n7q3ii29fk5dm2756qj3bmpmznxl77nihg9z821rdlajrjd74n")))

(define-public crate-unchained-0.1.3 (c (n "unchained") (v "0.1.3") (h "102mcwylxz1aaxh3m84g0fnbl87xi8yiy33pkkyq1w4763fl270v")))

