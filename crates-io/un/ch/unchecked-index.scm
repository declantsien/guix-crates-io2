(define-module (crates-io un ch unchecked-index) #:use-module (crates-io))

(define-public crate-unchecked-index-0.1.0 (c (n "unchecked-index") (v "0.1.0") (h "1qb61ixxi487i8a0mlrh1x4l7g67fv2qksgwb91dmmyfbp5clz52")))

(define-public crate-unchecked-index-0.1.1 (c (n "unchecked-index") (v "0.1.1") (h "1x2mjlxkgn53yr9ra680nhgxigfpl0dkscybgw61izjdmbxhfava")))

(define-public crate-unchecked-index-0.2.0 (c (n "unchecked-index") (v "0.2.0") (h "1rn5ml0lz1p2xdzkzf2lpjif6jg91yjm312m75vrxs7qdfy7gs1p")))

(define-public crate-unchecked-index-0.2.1 (c (n "unchecked-index") (v "0.2.1") (h "01vp51g3z51ji4xci4gv5y74nkn4k7naml90kc2rsyw1dm9h8b9q")))

(define-public crate-unchecked-index-0.2.2 (c (n "unchecked-index") (v "0.2.2") (h "0p6qcai1mjayx59cpgk27d0zgw9hz9r1ira5jiqil66f4ba8dfpf")))

