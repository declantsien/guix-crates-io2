(define-module (crates-io un sy unsynn) #:use-module (crates-io))

(define-public crate-unsynn-0.0.0 (c (n "unsynn") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 2)))) (h "1rgqgpb1hjail3phya00va65dm3zzpng4c08cxk11z6ipd2c7816") (r "1.59.0")))

(define-public crate-unsynn-0.0.1 (c (n "unsynn") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("proc-macro" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 2)))) (h "05pi05k3zzn1xbczpaa7nddrfgvnas0x8ccwiyk3n0h8pzpbkaxn") (r "1.59.0")))

