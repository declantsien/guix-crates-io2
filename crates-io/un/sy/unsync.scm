(define-module (crates-io un sy unsync) #:use-module (crates-io))

(define-public crate-unsync-0.1.0 (c (n "unsync") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt" "sync"))) (d #t) (k 2)))) (h "19mx537igldzqk20cdb6xwzc2w8srlsawq0dvh3zawr3a144qq78") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-unsync-0.1.1 (c (n "unsync") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt" "sync"))) (d #t) (k 2)))) (h "0g3f84sbmrzqaxbk9q8pvyvhyk36da6xp2b28vn7x14d21sc4jc4") (f (quote (("std") ("default" "std"))))))

(define-public crate-unsync-0.1.2 (c (n "unsync") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt" "sync"))) (d #t) (k 2)))) (h "0hm2j6gizzadk35n6mcfnc3v1saqlbaplkmis9jbhl8nj6qwb4qi") (f (quote (("std") ("default" "std")))) (r "1.58")))

