(define-module (crates-io un sy unsync_channel) #:use-module (crates-io))

(define-public crate-unsync_channel-0.1.0 (c (n "unsync_channel") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 2)))) (h "158mhva7rhpf2c2g06jv0l6n9gdrfbz1svfrym1b6mradqqi6w3q")))

