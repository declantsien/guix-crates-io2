(define-module (crates-io un th unthbuf) #:use-module (crates-io))

(define-public crate-unthbuf-0.1.0 (c (n "unthbuf") (v "0.1.0") (h "1q35l1is2j8j2ylkh4i8sg3730638v9xjdp9vfxr1fv98b48q4zj")))

(define-public crate-unthbuf-0.2.0 (c (n "unthbuf") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04kndwy82j3g1ard9aclzn0aswjq37w7firwcydzmzm7102wfgp5")))

(define-public crate-unthbuf-1.0.0 (c (n "unthbuf") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04vmf31v7giwv4vzr6ybzjli1vwsgwgcsrb4hxy0xy415cwgbql2")))

