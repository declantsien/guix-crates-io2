(define-module (crates-io un ra unrar_sys) #:use-module (crates-io))

(define-public crate-unrar_sys-0.1.0 (c (n "unrar_sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "07dq1cf3w59k4ci2558f4j6vff8mm10iwlf9h4imycad1534nc2r")))

(define-public crate-unrar_sys-0.2.0 (c (n "unrar_sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1bgm8c846m22d6dr6jb2n88yrkklmpfhjlsvc6fzbdkzn309rz1j")))

(define-public crate-unrar_sys-0.2.1 (c (n "unrar_sys") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "ntdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q1ck1yr2vli8gwiyx0zp4cbmvpczi22arqhi5fcq2yw12a3j280") (f (quote (("std" "libc/std" "winapi/std") ("default" "std"))))))

(define-public crate-unrar_sys-0.3.0 (c (n "unrar_sys") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "ntdef"))) (d #t) (t "cfg(all(windows, target_env = \"msvc\"))") (k 0)))) (h "1yhy50lr5bdsj17hjjkjr43s21702acj3c5di271zadnghi3drcm") (f (quote (("std" "libc/std" "winapi/std") ("default" "std"))))))

(define-public crate-unrar_sys-0.3.1 (c (n "unrar_sys") (v "0.3.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "ntdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1473jrfr0hcghym813sw0f5kzw9hrs0lj1xfdyjz1b0nf181qs8z") (f (quote (("std" "libc/std" "winapi/std") ("default" "std"))))))

