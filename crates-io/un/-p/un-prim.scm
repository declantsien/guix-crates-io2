(define-module (crates-io un -p un-prim) #:use-module (crates-io))

(define-public crate-un-prim-0.1.0 (c (n "un-prim") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "13x9ixqlx2grivwdcgwrkjw534lvllrq2n3nis65rzk0yszmngj9")))

(define-public crate-un-prim-0.1.1 (c (n "un-prim") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "01hy886hmpmxf6m68z6vm3zpzhj0d2gnywsqzbijizx4br0drzl6")))

(define-public crate-un-prim-0.1.2 (c (n "un-prim") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1g2kavcyz2bvx00zzclpnif5c3pbbbcqi7ir2zs236vxn58bplmm")))

