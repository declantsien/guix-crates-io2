(define-module (crates-io un mp unmp-link) #:use-module (crates-io))

(define-public crate-unmp-link-0.0.0 (c (n "unmp-link") (v "0.0.0") (h "0wdzzxyayar7a2z2hlw7xqwr16a3v97ssb1fvi5vxb5dzdr1mqpl")))

(define-public crate-unmp-link-0.6.0 (c (n "unmp-link") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "1vf63my208v02gfrn9n7alxmnqgyq5rbjajj9m80j871cpah7q4w") (f (quote (("role_router") ("role_client") ("role_center") ("default"))))))

(define-public crate-unmp-link-0.7.0 (c (n "unmp-link") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "1qdwm24fa0y1964cl3i81kg39rbxkkciljpbs4znd6rf1k52kqvl") (f (quote (("role_router") ("role_client") ("role_center") ("default"))))))

