(define-module (crates-io un mp unmp-link-redis) #:use-module (crates-io))

(define-public crate-unmp-link-redis-0.2.2 (c (n "unmp-link-redis") (v "0.2.2") (d (list (d (n "redis") (r "^0.17.0") (d #t) (k 0)) (d (n "unmp") (r "^0.2.0") (d #t) (k 0)))) (h "1idz9hm819mi7xjcnrcq0hw91qj6n1qglrlx2bgzacrbnbvas2g2")))

(define-public crate-unmp-link-redis-0.3.0 (c (n "unmp-link-redis") (v "0.3.0") (d (list (d (n "redis") (r "^0.17.0") (d #t) (k 0)) (d (n "unmp") (r "^0.3") (d #t) (k 0)))) (h "19s4i7ph0r9iwphymvg3a1vknrkawql8m3zniv14n6h8ikp9i6bx")))

(define-public crate-unmp-link-redis-0.3.2 (c (n "unmp-link-redis") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "redis") (r "^0.17.0") (d #t) (k 0)) (d (n "unmp") (r "^0.3") (d #t) (k 0)))) (h "1gw1y4asn56rqm4vir4a54q6jmciib7iafiy40bmvnfqgf7jdiic")))

(define-public crate-unmp-link-redis-0.3.3 (c (n "unmp-link-redis") (v "0.3.3") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)))) (h "1wffnb6r1rrh9bh4gfhrj3j617l7mwxrq3c5mmnvmg65kxf9ln6n")))

(define-public crate-unmp-link-redis-0.5.0 (c (n "unmp-link-redis") (v "0.5.0") (d (list (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "redis") (r "^0.20.0") (f (quote ("async-std-comp"))) (d #t) (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp") (r "^0.6") (d #t) (k 0)))) (h "0qsivnnxj60g46i31q566p72vavnphndn1iwirn43v26lclsgvpd")))

(define-public crate-unmp-link-redis-0.7.0 (c (n "unmp-link-redis") (v "0.7.0") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "redis") (r "^0.20.0") (f (quote ("async-std-comp"))) (d #t) (k 0)) (d (n "unmp") (r "^0.7") (d #t) (k 0)) (d (n "unmp-link") (r "^0.7") (d #t) (k 0)))) (h "0kgl6x2hs9l9wa0lhn6h52nkm744bq98nlg7gy6yfylm3b4ff8ir")))

