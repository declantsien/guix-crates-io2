(define-module (crates-io un mp unmp-center) #:use-module (crates-io))

(define-public crate-unmp-center-0.0.1 (c (n "unmp-center") (v "0.0.1") (d (list (d (n "unmp-protocol-etp") (r "^0.3") (d #t) (k 0)))) (h "0q22bckwk672wcixw0rdn94bqgilxvd8984s1ncid49c4dzibb2d")))

(define-public crate-unmp-center-0.1.1 (c (n "unmp-center") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)) (d (n "unmp-center-core") (r "^0.1.0") (d #t) (k 0)) (d (n "unmp-protocol-etp") (r "^0.3") (d #t) (k 0)) (d (n "unmp-protocol-raw") (r "^0.3") (d #t) (k 0)))) (h "18gjr5wdfg29g71cpjn1jb86gbmg4wfp936bb3ym3npglhgky1i0")))

(define-public crate-unmp-center-0.1.2 (c (n "unmp-center") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)) (d (n "unmp-center-core") (r "^0.1.0") (d #t) (k 0)) (d (n "unmp-protocol-etp") (r "^0.3") (d #t) (k 0)) (d (n "unmp-protocol-raw") (r "^0.3") (d #t) (k 0)))) (h "0snfgzidqx1gl073xjhcwhxvls7i2nbbc658hnhclkncyndscfvj")))

