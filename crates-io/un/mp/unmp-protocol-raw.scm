(define-module (crates-io un mp unmp-protocol-raw) #:use-module (crates-io))

(define-public crate-unmp-protocol-raw-0.1.0 (c (n "unmp-protocol-raw") (v "0.1.0") (d (list (d (n "unmp") (r "^0.1.0") (d #t) (k 0)))) (h "0vggwz6i7yshn95x5wyzwkly78pv2mb9kl88v9nf702dw3hm6npv")))

(define-public crate-unmp-protocol-raw-0.1.1 (c (n "unmp-protocol-raw") (v "0.1.1") (d (list (d (n "unmp") (r "^0.1.6") (d #t) (k 0)))) (h "0fvp0mvdsxgncsy3qfb63skblm828i1alsj7z77wqbsg1ad9zja6")))

(define-public crate-unmp-protocol-raw-0.1.2 (c (n "unmp-protocol-raw") (v "0.1.2") (d (list (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)) (d (n "unmp") (r ">=0.1.6, <0.2.0") (d #t) (k 0)))) (h "0p9qsfzs0fmnkkagkzigykvnc9pg917594li4ny7py83p9n76jzi")))

(define-public crate-unmp-protocol-raw-0.2.0 (c (n "unmp-protocol-raw") (v "0.2.0") (d (list (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)) (d (n "unmp") (r ">=0.0.0, <1.0.0") (d #t) (k 0)))) (h "1m60wr5r37bfi583amz115w7p5xq9kf3ld577lazzzs6s0jn18ha")))

(define-public crate-unmp-protocol-raw-0.2.1 (c (n "unmp-protocol-raw") (v "0.2.1") (d (list (d (n "spin") (r "^0.7") (d #t) (k 0)) (d (n "unmp") (r "^0") (d #t) (k 0)))) (h "0shjw7qaq2xz2bagd0vaxpxfyzm1wvr99hmszxqv7131cz2xzmp2")))

(define-public crate-unmp-protocol-raw-0.3.0 (c (n "unmp-protocol-raw") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)) (d (n "unmp") (r "^0.3") (d #t) (k 0)))) (h "1yy75dg7ndzc5185j9y0wbgaw2qh35w2g61ha6kmsqm6lm8p8w95")))

(define-public crate-unmp-protocol-raw-0.3.1 (c (n "unmp-protocol-raw") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)) (d (n "unmp") (r "^0.3") (d #t) (k 0)))) (h "1wjqncmzrr9zlx351j1w7bi1hk355jf944sxx8pp2mwnq8nzfr0x")))

(define-public crate-unmp-protocol-raw-0.3.2 (c (n "unmp-protocol-raw") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)))) (h "0rqs7cgg2dvybwkr9kw45diwljvk20vh99xqhn0rg7g8xkxx3qkw")))

(define-public crate-unmp-protocol-raw-0.5.0 (c (n "unmp-protocol-raw") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "unmp") (r "^0.6") (d #t) (k 0)))) (h "0hx4sskrnrxkli1z72w4982988p6azlsigv28p0lrc3j6j0idvhf")))

(define-public crate-unmp-protocol-raw-0.7.0 (c (n "unmp-protocol-raw") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "unmp") (r "^0.7") (d #t) (k 0)))) (h "1d64in31rpc3ykg6dj95ysaw59scmgl1mfvsk8jhgzkypy5bf6pr")))

(define-public crate-unmp-protocol-raw-0.7.1 (c (n "unmp-protocol-raw") (v "0.7.1") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "unmp") (r "^0.7.1") (d #t) (k 0)))) (h "1pvxyw1vrmc4vai9yqqny70d47farjglg0i5kl3azjj3m9inq2x8")))

