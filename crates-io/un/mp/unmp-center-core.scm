(define-module (crates-io un mp unmp-center-core) #:use-module (crates-io))

(define-public crate-unmp-center-core-0.0.1 (c (n "unmp-center-core") (v "0.0.1") (h "1rjifqjb3im5cs2r2qyfdhhrd8ysffg4jx1han50c0s786w1rvx5")))

(define-public crate-unmp-center-core-0.0.2 (c (n "unmp-center-core") (v "0.0.2") (h "0mh834q8y2sv1l39snknb72c7zhma5pdylzqk3gy7xa6cii0d2qi")))

(define-public crate-unmp-center-core-0.1.1 (c (n "unmp-center-core") (v "0.1.1") (d (list (d (n "md5") (r "^0.7") (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)))) (h "1lprrp7n34bqyq2xy0xbfdbkfkwdj3glgdx2vpw0029mj1l5fvzw")))

(define-public crate-unmp-center-core-0.1.2 (c (n "unmp-center-core") (v "0.1.2") (d (list (d (n "md5") (r "^0.7") (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)))) (h "1na5ai713498lz479aq13nhbasgscxlzicaa9mg5bcq1yrk3v1pw")))

(define-public crate-unmp-center-core-0.1.3 (c (n "unmp-center-core") (v "0.1.3") (d (list (d (n "md5") (r "^0.7") (k 0)) (d (n "unmp-id") (r "^2.0") (d #t) (k 0)))) (h "0if18h8pmsqdxi4d7508fa4cc4fz0ay4g04bj087npdc0ishlsia")))

