(define-module (crates-io un mp unmp-link-serial) #:use-module (crates-io))

(define-public crate-unmp-link-serial-0.3.0 (c (n "unmp-link-serial") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)) (d (n "unmp") (r "^0.3.0") (d #t) (k 0)))) (h "0kvhiki4z1d3sqbxn5dhsybzizs32y3i6729y7w8vjnaxr26h1zj")))

(define-public crate-unmp-link-serial-0.3.2 (c (n "unmp-link-serial") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "serial") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "unmp") (r "^0.3.0") (d #t) (k 0)))) (h "18x4935hgjbgmx7421kq09sbd1aysdr979kk2favgmrh35db42l8") (f (quote (("std" "serial") ("embedded" "embedded-hal" "nb" "spin") ("default" "embedded"))))))

(define-public crate-unmp-link-serial-0.3.3 (c (n "unmp-link-serial") (v "0.3.3") (d (list (d (n "eds") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)))) (h "0345ndyqgzvmm8fnvkzh5haw84kyhn33n5q270b2gxky2kq6h3mw")))

(define-public crate-unmp-link-serial-0.5.0 (c (n "unmp-link-serial") (v "0.5.0") (d (list (d (n "eds") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp") (r "^0.6") (d #t) (k 0)))) (h "01yxiaimb7j2jcf3yk8iz56a3xnw90g14864b4b5685f1iy2cpkk")))

(define-public crate-unmp-link-serial-0.7.0 (c (n "unmp-link-serial") (v "0.7.0") (d (list (d (n "eds") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp") (r "^0.7") (d #t) (k 0)) (d (n "unmp-link") (r "^0.7") (d #t) (k 0)))) (h "11g86rlri52ifqks7xgk1x1zyxq09fy8qd1jf1zzcqqakdbmzsk0")))

