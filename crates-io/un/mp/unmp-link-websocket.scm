(define-module (crates-io un mp unmp-link-websocket) #:use-module (crates-io))

(define-public crate-unmp-link-websocket-0.3.0 (c (n "unmp-link-websocket") (v "0.3.0") (d (list (d (n "unmp") (r "^0.3") (d #t) (k 0)) (d (n "websocket") (r "^0.26") (d #t) (k 0)))) (h "0cfjzc7hi0ip5idll1jbcsicwgxlpixqbrm3acj21s46mnqk7a3k")))

(define-public crate-unmp-link-websocket-0.3.2 (c (n "unmp-link-websocket") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "unmp") (r "^0.3") (d #t) (k 0)) (d (n "websocket") (r "^0.26") (d #t) (k 0)))) (h "10as7kwm9zcsygy1qjr64flzc264jvs8h50b1cfhma8js0m6c05a")))

(define-public crate-unmp-link-websocket-0.3.3 (c (n "unmp-link-websocket") (v "0.3.3") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)) (d (n "websocket") (r "^0.26") (d #t) (k 0)))) (h "11zkababzkj1jm5px73q43cb2cwgv88mr55q92i2cpd9wndzrxxx")))

(define-public crate-unmp-link-websocket-0.3.4 (c (n "unmp-link-websocket") (v "0.3.4") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)) (d (n "websocket") (r "^0.26") (f (quote ("sync" "sync-ssl"))) (k 0)))) (h "0ibxp70dc0c13pkp4lxkcaqpnldxs1pyyvhd9vv5f8jzkpw799wf")))

(define-public crate-unmp-link-websocket-0.5.0 (c (n "unmp-link-websocket") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "unmp") (r "^0.6") (d #t) (k 0)) (d (n "websocket") (r "^0.26") (f (quote ("sync" "sync-ssl"))) (k 0)))) (h "14i35qhs9c8jxr86x6al1l95x1yxx3gn2wb7y2cmyi3bv5601jzj")))

(define-public crate-unmp-link-websocket-0.7.0 (c (n "unmp-link-websocket") (v "0.7.0") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.13") (f (quote ("async-std-runtime"))) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "unmp") (r "^0.7") (d #t) (k 0)) (d (n "unmp-link") (r "^0.7") (d #t) (k 0)))) (h "0hgq7yjcb2c4xqq2vrgrxywlijshpifjg1m1fqwl2624f0s7qyzd")))

(define-public crate-unmp-link-websocket-0.7.1 (c (n "unmp-link-websocket") (v "0.7.1") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.13") (f (quote ("async-std-runtime"))) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "unmp") (r "^0.7") (d #t) (k 0)) (d (n "unmp-link") (r "^0.7") (d #t) (k 0)))) (h "1m37fm1g71jssk03fcphcs0idg0pjn9s0naz7sfc9pypsi7cn0hf")))

