(define-module (crates-io un mp unmp-link-udp) #:use-module (crates-io))

(define-public crate-unmp-link-udp-0.1.0 (c (n "unmp-link-udp") (v "0.1.0") (d (list (d (n "unmp") (r "^0.1.0") (d #t) (k 0)))) (h "1n41pi4d6s98a5ds3zsk940vgyqqvxb02x2l6rj19ja1lnmz9ri0")))

(define-public crate-unmp-link-udp-0.2.0 (c (n "unmp-link-udp") (v "0.2.0") (d (list (d (n "unmp") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "0wbfidfbhwzq6c4yj5lzj76f2ic152py4shdz55g2vs89gixlq9x")))

(define-public crate-unmp-link-udp-0.2.2 (c (n "unmp-link-udp") (v "0.2.2") (d (list (d (n "unmp") (r "^0.2.0") (d #t) (k 0)))) (h "1hw5dqw6lg1w94vg8g1sdgq4my3j6bib6dpsq9s33v68w9wqgbpj")))

(define-public crate-unmp-link-udp-0.3.0 (c (n "unmp-link-udp") (v "0.3.0") (d (list (d (n "unmp") (r "^0.3.0") (d #t) (k 0)))) (h "1a6v7407ir9cpszmly0mfm2b0fd4fhvkyyn3p7p5vfj168kfxja4")))

(define-public crate-unmp-link-udp-0.3.2 (c (n "unmp-link-udp") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "unmp") (r "^0.3.0") (d #t) (k 0)))) (h "02692qlygnmj685mxf6x90d38k9yj11gbnswn46lncb6gnk065wl")))

(define-public crate-unmp-link-udp-0.3.3 (c (n "unmp-link-udp") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "unmp") (r "^0.4") (d #t) (k 0)))) (h "02jd8j8bn7fx8rq3qsbl4s0m2svrqvfypxsvqawvav8qcbam2lki")))

(define-public crate-unmp-link-udp-0.5.0 (c (n "unmp-link-udp") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eds") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "unmp") (r "^0.6") (d #t) (k 0)))) (h "0m5hg22yqzd3sv9h2gkilr09pbxaiq8v3r76fmnx7xxp7fasx5bz")))

(define-public crate-unmp-link-udp-0.7.0 (c (n "unmp-link-udp") (v "0.7.0") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eds") (r "^0.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "unmp") (r "^0.7") (d #t) (k 0)) (d (n "unmp-link") (r "^0.7") (d #t) (k 0)))) (h "0pnxs67c7c66knhp5hw3wn8yj38gfj5jnlvls2j6dl006v2ma81j")))

