(define-module (crates-io un mp unmp) #:use-module (crates-io))

(define-public crate-unmp-0.0.1 (c (n "unmp") (v "0.0.1") (h "0ifdzc71m6dv2jgqjgl2dm9rpczzzjnk1klxf2apkbgcjc02kqbm")))

(define-public crate-unmp-0.1.0 (c (n "unmp") (v "0.1.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "1l7flg2n6y35mrvf17r8m1fg48n0z37lp39hh6qai2a13n1j63rx")))

(define-public crate-unmp-0.1.1 (c (n "unmp") (v "0.1.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "1m2z7v9821f7g8n3yci4fcns5iw57n5mjss2h6kma8bknmq1yfbg")))

(define-public crate-unmp-0.1.2 (c (n "unmp") (v "0.1.2") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "02wxw64fv9q420sgg1mr7dqcbac91d4dg4fjlshqcc7nxs1mx6qr")))

(define-public crate-unmp-0.1.3 (c (n "unmp") (v "0.1.3") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0y9iy0q5qla263kpb8vr5x9v287vmm2pjvgi0hsc4apzdxd7vw5k")))

(define-public crate-unmp-0.1.4 (c (n "unmp") (v "0.1.4") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0jcnmfl4hz3qg5aryq8zsqri4l5yc4ayyzn4igl7r9hv02xmm46x")))

(define-public crate-unmp-0.1.5 (c (n "unmp") (v "0.1.5") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0lhs0h8ymvfgzmwihczxa4d99520jb2j1fpjzx28ynrysxjhh34b")))

(define-public crate-unmp-0.1.6 (c (n "unmp") (v "0.1.6") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "15dmc54nkfv5pqgpwwg2wacni5m1sn6fqpi4kxsm91y6g3h12d1j")))

(define-public crate-unmp-0.1.7 (c (n "unmp") (v "0.1.7") (d (list (d (n "crc16") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "1b084aafk497631xjnyknazdznsfk8yn5yrank6bjng0mbs0qmq3")))

(define-public crate-unmp-0.1.8 (c (n "unmp") (v "0.1.8") (d (list (d (n "crc16") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "16zxlnjjir723slrdkb14yj18jhsjpippmrdrs4yyy80453rpv03")))

(define-public crate-unmp-0.1.9 (c (n "unmp") (v "0.1.9") (d (list (d (n "crc16") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "1qq6fyk9kan72dvyc960symvzjllbny2z9yicp96i9cg6wwh8l7p")))

(define-public crate-unmp-0.2.0 (c (n "unmp") (v "0.2.0") (d (list (d (n "crc16") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "spin") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "08ag075nw8sy1xqf50lj8brsgxnxyam7bcrskbadcs76z0ipqd56")))

(define-public crate-unmp-0.2.1 (c (n "unmp") (v "0.2.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "133d0mh3kaz1zyxzw95i1xmzdr405biaygfgsiy7h4bgcfpbib4n")))

(define-public crate-unmp-0.2.2 (c (n "unmp") (v "0.2.2") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "0c21ym68gwqjj2fv277a8hzhrmabp5nhdlqb2bn4il7wz5594y97")))

(define-public crate-unmp-0.2.3 (c (n "unmp") (v "0.2.3") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "0zn5pzdgd3dmnc5hdpjww1s762kbzkzl59d5d7xqb2d79xpkmij4")))

(define-public crate-unmp-0.2.4 (c (n "unmp") (v "0.2.4") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "0ricr5a7lvz1236l229qf0p7dhllg3xlz0c5y3qgyrj6zwvgybd0")))

(define-public crate-unmp-0.3.0 (c (n "unmp") (v "0.3.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "10md1g8m1bxw40pvsxany0ypacfg9h0cib2av1811rd8f73lm1fh")))

(define-public crate-unmp-0.3.3 (c (n "unmp") (v "0.3.3") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "1acp3r3x80xl9ass70xmzl0fghdi9kjc367gsmik637sh1icr1yf")))

(define-public crate-unmp-0.3.5 (c (n "unmp") (v "0.3.5") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "0xvbwyx5yw4b1bwlf0fwxb9a4r11p3w0qmfm7jkvjykqf192d07l")))

(define-public crate-unmp-0.3.6 (c (n "unmp") (v "0.3.6") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "198syrxz09m8xd2ci07d5z0b0azfrbj67hwl6fsbyai935wyp0xc")))

(define-public crate-unmp-0.3.8 (c (n "unmp") (v "0.3.8") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "0vxb13gqrzqlpi7hm9kwymrkfgmvs7j9lkzii6mqgm5iv71cgxg9")))

(define-public crate-unmp-0.4.0 (c (n "unmp") (v "0.4.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "1g0h9si1s340ldsbx0svj0hn8jppkvgh9x0mcickrfhh1d98rzbg")))

(define-public crate-unmp-0.4.2 (c (n "unmp") (v "0.4.2") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "03bz1bglrjky3ss8f99rj6shf78mb77qrr1v3bs6nps8264n7m7z") (f (quote (("server") ("router") ("end") ("default" "router"))))))

(define-public crate-unmp-0.4.3 (c (n "unmp") (v "0.4.3") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp-id") (r "^1") (d #t) (k 0)))) (h "1039rc250w8zrscna48rxr89gwpgh3fi3hnlw5x2afc708rjz8sd") (f (quote (("role_server") ("default"))))))

(define-public crate-unmp-0.4.4 (c (n "unmp") (v "0.4.4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp-id") (r "^1") (d #t) (k 0)))) (h "06ah869bls5931mf1ay2gp4q7r31h10s4mh3ckypaqi09nrja6za") (f (quote (("default") ("alloc"))))))

(define-public crate-unmp-0.4.5 (c (n "unmp") (v "0.4.5") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp-id") (r "^1") (d #t) (k 0)))) (h "1jzqjyrzzw8wwc2as3hjgzh3jkcb5acq4d6xf95a4ndqxnybbvak") (f (quote (("role_router") ("role_client") ("role_center") ("default"))))))

(define-public crate-unmp-0.5.0 (c (n "unmp") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp-id") (r "^1") (d #t) (k 0)))) (h "12xl16pcq0vr4131p8y58631ypz1ri0n42z8hlcnqp41yxfp799a") (f (quote (("role_router") ("role_client") ("role_center") ("default"))))))

(define-public crate-unmp-0.6.0 (c (n "unmp") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp-id") (r "^2.0.0") (d #t) (k 0)))) (h "0iwkyvjwwj2xrjy1xyva7zc2sr87llr1ik8cb17sil2z7q1aj3f2") (f (quote (("role_router") ("role_client") ("role_center") ("default"))))))

(define-public crate-unmp-0.7.0 (c (n "unmp") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp-id") (r "^2.0.0") (d #t) (k 0)) (d (n "unmp-link") (r "^0.7.0") (d #t) (k 0)))) (h "1vg1x5fdwm2qinvyyxyxf6gx87cfz8q12zdb7c5m8mrxa7hsr1vb") (f (quote (("role_router") ("role_client") ("role_center") ("default"))))))

(define-public crate-unmp-0.7.1 (c (n "unmp") (v "0.7.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp-id") (r "^2.0.0") (d #t) (k 0)) (d (n "unmp-link") (r "^0.7.0") (d #t) (k 0)))) (h "1w6p1rzn0vgrqbkf9z4vpzikp1jyvh5x65a8afv4ynfwgmxjjfv8") (f (quote (("role_router") ("role_client") ("role_center") ("default"))))))

(define-public crate-unmp-0.7.2 (c (n "unmp") (v "0.7.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp-id") (r "^2.0.0") (d #t) (k 0)) (d (n "unmp-link") (r "^0.7.0") (d #t) (k 0)))) (h "1ldamcrnh42a70ahxpgycawfx4i6y7kdw989myk8bgxsdcpsmp21") (f (quote (("role_router") ("role_client") ("role_center") ("default"))))))

(define-public crate-unmp-0.7.3 (c (n "unmp") (v "0.7.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (f (quote ("alloc"))) (k 0)) (d (n "futures-util") (r "^0.3.13") (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "task-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "unmp-id") (r "^2.0.0") (d #t) (k 0)) (d (n "unmp-link") (r "^0.7.0") (d #t) (k 0)))) (h "0mx941dz9a82qlg35njg2psaajfw5a99xka5idg1psjq0gsg79kv") (f (quote (("role_router") ("role_client") ("role_center") ("default"))))))

