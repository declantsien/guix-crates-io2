(define-module (crates-io un mp unmp-id) #:use-module (crates-io))

(define-public crate-unmp-id-1.0.0 (c (n "unmp-id") (v "1.0.0") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "08m7l8pgv6zz7zdydkj0d8sjvfki477527bn2nj0js4akdk22a12")))

(define-public crate-unmp-id-1.0.1 (c (n "unmp-id") (v "1.0.1") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "0smbmwwivk9ys10biy5kj7a2c6w6b1lsr9k1vrvnm03klygyipf1") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

(define-public crate-unmp-id-1.0.2 (c (n "unmp-id") (v "1.0.2") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "144zsdaryfh9p66vmjhk1qvb781xrk1263wrqfm9gk5nwngbig7k") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

(define-public crate-unmp-id-1.1.0 (c (n "unmp-id") (v "1.1.0") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "06q3xbngbn32jv0p92dwlfbnhfpvhskdrn4s9f6nxq85hh9gbp57") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

(define-public crate-unmp-id-1.1.1 (c (n "unmp-id") (v "1.1.1") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "0nj6hqf71ljiphn9p0w7m1dpk79r028mdr8lj0yfrhwfccrjd68k") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

(define-public crate-unmp-id-1.2.0 (c (n "unmp-id") (v "1.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1sgrfg03ph9wp6x121da16mfvpf6fb2wcmfyb91my91704pz2v9w") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

(define-public crate-unmp-id-1.2.1 (c (n "unmp-id") (v "1.2.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0nm8wl87h3gy0ycik6r2rkw5m1m2b5l1rs79ygpqqhj5vsbplqs4") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

(define-public crate-unmp-id-1.3.0 (c (n "unmp-id") (v "1.3.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1ymahnbnbsm1r55m7cl46b2gvhnlgc0a7s8bmvngy85rr6qql7ik") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

(define-public crate-unmp-id-1.3.1 (c (n "unmp-id") (v "1.3.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1z49c9blspappwj27lnbvy8dcv45bgqpxp0zkhi8yxir7w113jaa") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

(define-public crate-unmp-id-1.4.0 (c (n "unmp-id") (v "1.4.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1brq5c3qd403r16a3ac8lny9bda07kasxshr3vh3b88n76pjah0f") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

(define-public crate-unmp-id-2.0.0 (c (n "unmp-id") (v "2.0.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0mqjf1fz272a6inafav107hkv3r45a9s1miqgyrnl0alrcx6914a") (f (quote (("wasm" "wasm-bindgen") ("default"))))))

