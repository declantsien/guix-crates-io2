(define-module (crates-io un to untools) #:use-module (crates-io))

(define-public crate-untools-1.0.0 (c (n "untools") (v "1.0.0") (h "1yl0fnvvl402gvp7z1s52z8skavim1gqb9c70zwvfqnwb0zpssnl")))

(define-public crate-untools-1.0.1 (c (n "untools") (v "1.0.1") (h "0cd116ssv3yyr3287vgfznv56n075dvl1kgq3jx4wnw70xz5gz97")))

(define-public crate-untools-1.0.2 (c (n "untools") (v "1.0.2") (h "1sc0kg3dfngzsw6hs44gashmngig6jzg9kkmd89p6jy4q5x6bw0r")))

(define-public crate-untools-1.0.3 (c (n "untools") (v "1.0.3") (h "0hqlq52q6by48s14fxjivza9kd776a383x0r5kvrxyz35w45c74h")))

(define-public crate-untools-1.0.4 (c (n "untools") (v "1.0.4") (h "0ynym0gnkk5drj388zxlrvbvfgwh7xiz8gpi0khcbs9vqj0gq0bl")))

(define-public crate-untools-1.0.5 (c (n "untools") (v "1.0.5") (h "0k094cfnln48fa12vc0hfs8xfkli8q4cc0f71m4w4vwrs1y2zw81")))

(define-public crate-untools-1.0.6 (c (n "untools") (v "1.0.6") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)))) (h "07mapblxcjywa2f4wcfna3fbjrk1z0jgwv250nk9qcqbs45gqvw5")))

(define-public crate-untools-1.0.7 (c (n "untools") (v "1.0.7") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)))) (h "0nqkp3g4iim9dcmn3gchg1v5vi64fq48x1zj7wz8drmyx18kys0a")))

(define-public crate-untools-1.0.8 (c (n "untools") (v "1.0.8") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)))) (h "0w24i71bqzfvr3lpyrkhl2bxh7pz258mrkgc5fag01080k01rshc")))

(define-public crate-untools-1.0.9 (c (n "untools") (v "1.0.9") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)))) (h "16gq1cxbyzivgw4lf4ixp50mmilkpkp1j3wr8nrrp1vdpdnk9da4")))

(define-public crate-untools-1.0.10 (c (n "untools") (v "1.0.10") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)))) (h "0rj9q7q2dfgnzj36v1pm1siri0kvbzjc3dg63pna6fh2m90ig51c")))

