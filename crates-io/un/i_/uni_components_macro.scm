(define-module (crates-io un i_ uni_components_macro) #:use-module (crates-io))

(define-public crate-uni_components_macro-0.0.1 (c (n "uni_components_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sdy5j851ybhg0c759hivyb8k8d44mwxvvqryf2rjgairjwrr02d")))

(define-public crate-uni_components_macro-0.0.2 (c (n "uni_components_macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04rxl9kqx0s9m7mr09qj10cpry8l1ijjcdyp3vd4mzhkg37ijp0w")))

(define-public crate-uni_components_macro-0.0.3 (c (n "uni_components_macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qg8kjgl1d1kwp1z27v4hcrysrfn9nrmf2ajbads57vcdzp39arx")))

(define-public crate-uni_components_macro-0.0.4 (c (n "uni_components_macro") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.6") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c91gmlp7mbpvckw1z6vi6cfigjs4sappgi096k5j8jn8frq8lk0")))

(define-public crate-uni_components_macro-0.0.5 (c (n "uni_components_macro") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.6") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lsabcammdgxddjr3li2582s1g4zhyzsaldnajlq9lp2l7bxknj3")))

(define-public crate-uni_components_macro-0.0.6 (c (n "uni_components_macro") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.6") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05z2767p608wrsxnw4lc47bbn998j3vmck0hb2ajscwrbmg5fxsp")))

(define-public crate-uni_components_macro-0.0.7 (c (n "uni_components_macro") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.6") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jrdj2h4pphc74dzdhrmmb3rbh6y6k84z2xdzjcjmkl99gx2r7jd")))

