(define-module (crates-io un i_ uni_serde_urlencoded) #:use-module (crates-io))

(define-public crate-uni_serde_urlencoded-0.6.2 (c (n "uni_serde_urlencoded") (v "0.6.2") (d (list (d (n "dtoa") (r "^0.4") (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1gkmcj4zfxdgwrzm70k6r3vgr0dbzxq1y9s36whh728mjw9hmj5r")))

