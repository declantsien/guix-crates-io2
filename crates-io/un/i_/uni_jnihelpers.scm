(define-module (crates-io un i_ uni_jnihelpers) #:use-module (crates-io))

(define-public crate-uni_jnihelpers-0.0.1 (c (n "uni_jnihelpers") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uni_tmp_jni") (r "^0.18.0") (d #t) (k 0)))) (h "12fidn4m5581fgsaaq67dfgmqjfpccrg9m25251i3h7qqxjkxz7m")))

(define-public crate-uni_jnihelpers-0.0.2 (c (n "uni_jnihelpers") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uni_tmp_jni") (r "^0.18.0") (d #t) (k 0)))) (h "0vwkgivrdigb40j45j6q2aqxjq6mflswv7hz5d7zpxp928xmnhnd")))

