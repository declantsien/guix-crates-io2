(define-module (crates-io un i_ uni_tmp_jni) #:use-module (crates-io))

(define-public crate-uni_tmp_jni-0.17.0 (c (n "uni_tmp_jni") (v "0.17.0") (d (list (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "combine") (r "^4.1.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0g4ys0rjxzqj5fincfgvc48w5vwh24fydrs4gbqxc40klf2xb1kk") (f (quote (("invocation") ("default"))))))

(define-public crate-uni_tmp_jni-0.18.0 (c (n "uni_tmp_jni") (v "0.18.0") (d (list (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "combine") (r "^4.1.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0cpxfylfn24c95vllf9v22h76mybc4ff7qzwk8jizygy277c69qq") (f (quote (("invocation") ("default"))))))

