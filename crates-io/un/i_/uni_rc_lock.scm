(define-module (crates-io un i_ uni_rc_lock) #:use-module (crates-io))

(define-public crate-uni_rc_lock-0.1.0 (c (n "uni_rc_lock") (v "0.1.0") (h "04bp75jhpzw25i3hx68w434wdm3bw7969yg0b10nzqhnf9c9ij02")))

(define-public crate-uni_rc_lock-0.1.1 (c (n "uni_rc_lock") (v "0.1.1") (h "1svkr1vq860jkljf5fbkdvmh8apm1mrzd9ahb31kdgrk81wpgszi")))

(define-public crate-uni_rc_lock-0.1.2 (c (n "uni_rc_lock") (v "0.1.2") (h "1pni5k7fji6gifwnkw9va71dlx5nd15lymlq1fws1r4fcnqzfg3z")))

(define-public crate-uni_rc_lock-0.2.0 (c (n "uni_rc_lock") (v "0.2.0") (h "11hzhcwr5rbxkibqfhknqv9hignr9mqdwxay03b2bvxnd345y4zb")))

