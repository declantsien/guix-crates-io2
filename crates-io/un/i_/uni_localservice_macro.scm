(define-module (crates-io un i_ uni_localservice_macro) #:use-module (crates-io))

(define-public crate-uni_localservice_macro-0.0.1 (c (n "uni_localservice_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15n1pdqnxwk0pvqxgsjfwm2kq4fm9ww0i6gk1la1bv0a8j0lswzf")))

(define-public crate-uni_localservice_macro-0.0.2 (c (n "uni_localservice_macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01268dr2cg9mji57asnp6akzx41s55fpns3a0i2rpx9x121r3xsi")))

