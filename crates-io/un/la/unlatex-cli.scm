(define-module (crates-io un la unlatex-cli) #:use-module (crates-io))

(define-public crate-unlatex-cli-0.1.0 (c (n "unlatex-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "unlatex") (r "^0.1.0") (d #t) (k 0)))) (h "1g0zfc5xkvprky469ganj30kza17kskf02h1k8zja43n2vhl4gl7")))

