(define-module (crates-io un ir uniresid) #:use-module (crates-io))

(define-public crate-uniresid-0.1.0 (c (n "uniresid") (v "0.1.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09rzmzn0k510zpr585jqkj2378da8dl20vby9i5bc7hkr1hfcr43")))

(define-public crate-uniresid-0.1.1 (c (n "uniresid") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "serde_") (r "^1") (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url_") (r "^2.2") (o #t) (d #t) (k 0) (p "url")))) (h "06imx38rlmhx7lb79ykpdhjlsaakidp8xhcihzrvrvzdj04s93hn") (f (quote (("url" "url_") ("serde" "serde_") ("default" "serde" "url"))))))

(define-public crate-uniresid-0.1.2 (c (n "uniresid") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "serde_") (r "^1") (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url_") (r "^2.2") (o #t) (d #t) (k 0) (p "url")))) (h "108ihbj3p08pj7cjldchysyzxri9p2pkzvvc7936a2qzws75af6q") (f (quote (("url" "url_") ("serde" "serde_") ("default" "serde" "url"))))))

(define-public crate-uniresid-0.1.3 (c (n "uniresid") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "serde_") (r "^1") (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url_") (r "^2.2") (o #t) (d #t) (k 0) (p "url")))) (h "1hwpckjd67l6x4frxjrdyhbn41fqdfm3fqqrkb76srg9pcfj993p") (f (quote (("url" "url_") ("serde" "serde_") ("default" "serde" "url"))))))

(define-public crate-uniresid-0.1.4 (c (n "uniresid") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "serde_") (r "^1") (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url_") (r "^2.2") (o #t) (d #t) (k 0) (p "url")))) (h "0p4mbn9lcg3yvdhcgyn1g3p8d32h04ansfry3hp23ssa6y1wb18s") (f (quote (("url" "url_") ("serde" "serde_") ("default" "serde" "url"))))))

(define-public crate-uniresid-0.1.5 (c (n "uniresid") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "serde_") (r "^1") (o #t) (d #t) (k 0) (p "serde")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url_") (r "^2.2") (o #t) (d #t) (k 0) (p "url")))) (h "1c71c6k5xx4n9amfjy33c74s11nr88fxicd1dqw46kzczc3l0nlr") (f (quote (("url" "url_") ("serde" "serde_") ("default" "serde" "url"))))))

