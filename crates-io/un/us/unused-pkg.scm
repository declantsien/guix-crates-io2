(define-module (crates-io un us unused-pkg) #:use-module (crates-io))

(define-public crate-unused-pkg-0.1.0 (c (n "unused-pkg") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.20.2") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.11.0") (d #t) (k 0)) (d (n "package_json_schema") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.202") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1rs0axka8f719s74f9gbcm3cy6473q6kw01qxxxp85zd8iimh08k")))

