(define-module (crates-io un fm unfmt_macros) #:use-module (crates-io))

(define-public crate-unfmt_macros-0.1.0 (c (n "unfmt_macros") (v "0.1.0") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "198wpwrmpw5pp10jbv1qfbi1masqj8zkkjp9ihxm6h1n11z3vfwa")))

(define-public crate-unfmt_macros-0.1.2 (c (n "unfmt_macros") (v "0.1.2") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bap5wz8hjgdwgk491lwc1sl3mgqcdmdyypprxqc61fyziqav1p2")))

(define-public crate-unfmt_macros-0.1.3 (c (n "unfmt_macros") (v "0.1.3") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1xxvib30s1p0dbwag5y0k7ryir8jn5cp745rlcymdxq47aznv53w")))

(define-public crate-unfmt_macros-0.2.0 (c (n "unfmt_macros") (v "0.2.0") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "08zbqcj1bpn4ww7iiy3zvsx9h7mwyb91h71jmm0lcs4v5ph1yw9c")))

(define-public crate-unfmt_macros-0.2.1 (c (n "unfmt_macros") (v "0.2.1") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0lvkdvvfaixc9llz8c2i5d6ldk7bc5b5rcbxn2gvzn31imxlzijd")))

(define-public crate-unfmt_macros-0.2.2 (c (n "unfmt_macros") (v "0.2.2") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "17rhh4676h9ri497p45if4r9a9xm5xbiw60k0jbrmfgxv4z1jalj")))

