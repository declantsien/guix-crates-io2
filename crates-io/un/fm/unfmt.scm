(define-module (crates-io un fm unfmt) #:use-module (crates-io))

(define-public crate-unfmt-0.1.0 (c (n "unfmt") (v "0.1.0") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0agax1nf6bghzgagf2i6hbqj94r6gqqk8h2bkb84brnn8lqk87hy")))

(define-public crate-unfmt-0.1.1 (c (n "unfmt") (v "0.1.1") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "unfmt_macros") (r "^0.1.0") (d #t) (k 0)))) (h "16g9315rh11knn7brxmkj982bdi47zvvihpk9bikk6nw4inw1xaa")))

(define-public crate-unfmt-0.1.2 (c (n "unfmt") (v "0.1.2") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "unfmt_macros") (r "^0.1.2") (d #t) (k 0)))) (h "05h5xwr8a79b8xdfx3ncqnc3jrq1w9dwbp27dx9w68p052m0qb5m")))

(define-public crate-unfmt-0.2.1 (c (n "unfmt") (v "0.2.1") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "unfmt_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0mm0prlcpkdnjlsn74q55qdx7dyq3kymg7vdn46hid6jgdg3n0x1")))

(define-public crate-unfmt-0.2.2 (c (n "unfmt") (v "0.2.2") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "unfmt_macros") (r "^0.2.2") (d #t) (k 0)))) (h "18nifnbbc54b4aa57l3mssh8azanqvs21pym9s9d1jzr0q1d709d")))

