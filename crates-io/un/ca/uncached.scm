(define-module (crates-io un ca uncached) #:use-module (crates-io))

(define-public crate-uncached-0.1.0 (c (n "uncached") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "05z5a8d5ajw60a0dpb02m4ixrgcwdsyd1p296rpk72i9mc8r2x9k")))

