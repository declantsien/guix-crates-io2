(define-module (crates-io un ca uncased) #:use-module (crates-io))

(define-public crate-uncased-0.9.0 (c (n "uncased") (v "0.9.0") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "180hpjap63qdf0d8ifhbd3nxjl7bgmk19ipbib8q08abnxf1xinp") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9.1 (c (n "uncased") (v "0.9.1") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0af63gr2v1ldvma1yjk5z9vhyfagmlq5lbq58q03fzv2726lksrp") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9.2 (c (n "uncased") (v "0.9.2") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1w44g9z0wqwf39l2h7qlw3jkqsyha7c3hm89cd1gfdzaw4z1vsnq") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9.3 (c (n "uncased") (v "0.9.3") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0yfizabkpbf0hw6s7aw7zivfcv5pq46s9jfk84skgib9g7ysg7rn") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-uncased-0.9.4 (c (n "uncased") (v "0.9.4") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0azqvhhm5vfcvg8x7050ykriaq95s3hf9li2hj4s1gvhv85crhli") (f (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-uncased-0.9.5 (c (n "uncased") (v "0.9.5") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0aq8whnlfns8brlzb7ji0bvfrgip31fav17yky93jvb4km33429h") (f (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9.6 (c (n "uncased") (v "0.9.6") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1l3flz044hfdnsddahj08dflqprfydszkm4vkf458l724xryvbjv") (f (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9.7 (c (n "uncased") (v "0.9.7") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "08inqy50xnfa2mk6g9zlsi18gnmd1dw9iq4qrynky2zxn011gc09") (f (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9.8 (c (n "uncased") (v "0.9.8") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1mxx8dgq5qyx8g41qf6x4wqac0kkhi0j4kkfa5f328gnzk822nxv") (f (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9.9 (c (n "uncased") (v "0.9.9") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0s6ajpdc300jr3fhxgh93lwdfkd88g9amhw6mc179gm4d0qwb6wv") (f (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9.10 (c (n "uncased") (v "0.9.10") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "15q6r6g4fszr8c2lzg9z9k9g52h8g29h24awda3d72cyw37qzf71") (f (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

