(define-module (crates-io un fl unflatten) #:use-module (crates-io))

(define-public crate-unflatten-0.1.0 (c (n "unflatten") (v "0.1.0") (h "1lr9hl59016i08bgx980266nq25mb7r2yrvnmfvnv0cdr48k92yf")))

(define-public crate-unflatten-0.1.1 (c (n "unflatten") (v "0.1.1") (h "10jcrdhz0rnafgy2wfrjnx0wf148b1f8wc0nihnpk1ixax6glff0")))

