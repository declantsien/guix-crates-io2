(define-module (crates-io un fl unflatter) #:use-module (crates-io))

(define-public crate-unflatter-0.1.0 (c (n "unflatter") (v "0.1.0") (d (list (d (n "futures") (r "^0") (d #t) (k 2)))) (h "0q68i3nmi6q80wpw7zgin5wbpy555g9g6lyj1lzc13sjmr4xvc8d")))

(define-public crate-unflatter-0.1.1 (c (n "unflatter") (v "0.1.1") (d (list (d (n "futures") (r "^0") (d #t) (k 2)))) (h "0ab3hr24vhxz4jahfi1mzw0ah3jf3m70mxl6finzdncy4v13yaq5")))

