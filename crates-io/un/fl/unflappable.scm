(define-module (crates-io un fl unflappable) #:use-module (crates-io))

(define-public crate-unflappable-0.1.0 (c (n "unflappable") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "18l60g8c1x2bzizb8d0dpyy9prfb6czwx1fhkw8v4iibb28zqdkv")))

(define-public crate-unflappable-0.2.0 (c (n "unflappable") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "115s445r0xir6ssj6hr9f8l6pfnhai10hj2c844z88n3ykdj8w4x")))

