(define-module (crates-io un fl unflow) #:use-module (crates-io))

(define-public crate-unflow-0.0.1 (c (n "unflow") (v "0.0.1") (h "1x9wvzby2w09bgm5ywvclvlxs6l8glcrpcxrda70lnkg6mkjfkms")))

(define-public crate-unflow-0.1.0 (c (n "unflow") (v "0.1.0") (d (list (d (n "antlr-rust") (r "=0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n3kbqw3immzkga3avw509l4946f9zyrs4hjhvm0jfghdc8xl9d8")))

(define-public crate-unflow-0.1.1 (c (n "unflow") (v "0.1.1") (d (list (d (n "antlr-rust") (r "=0.2.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "io-std"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.13") (d #t) (k 0)))) (h "00iwcr5xv6wakimg9v5wfzsspc1rbmh51a3krmp4dlmjxc0i3z2p")))

