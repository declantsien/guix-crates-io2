(define-module (crates-io un if uniffi_checksum_derive) #:use-module (crates-io))

(define-public crate-uniffi_checksum_derive-0.21.1 (c (n "uniffi_checksum_derive") (v "0.21.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1jflzyvyfgb4v5kvdc6ywf5byqxiyjyqpbwpd40l5yfa8rny3dkq") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.22.0 (c (n "uniffi_checksum_derive") (v "0.22.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "19fnr64jab2hcd5xarfw8a8pywv5gwxpwn19xfhkrj0sgv60algm") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.23.0 (c (n "uniffi_checksum_derive") (v "0.23.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "00vwk1c3k4yyrq2ivajsgvjw6jjr0033qxm3hi4svd2278wn3ph3") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.24.0 (c (n "uniffi_checksum_derive") (v "0.24.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0fdfiaj9vahzbadkcdh3n38kzg8ywc80ppay5sa4jpv7jcgryf25") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.24.1 (c (n "uniffi_checksum_derive") (v "0.24.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0268hi67pgdr76k3851m97dhnwda1k3bb4raysgyyslb4f7db65g") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.24.2 (c (n "uniffi_checksum_derive") (v "0.24.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "19h0inklsibfmx0rrss1368v44rg8ln1318p4m8y968lfn14i8cs") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.24.3 (c (n "uniffi_checksum_derive") (v "0.24.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1s27x4539x0wz67jvwbq653wgdhfl5hcs726gmacck35pnlm9cyi") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.25.0 (c (n "uniffi_checksum_derive") (v "0.25.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "07f1w7bxzhkvkby6y84gra5v786w4s6fb4ylzcw9gw7zb9pd96w1") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.25.1 (c (n "uniffi_checksum_derive") (v "0.25.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1j44as6kx2vnc1lpk29x52z3z0r92s3qrz5yjk92v0xfznxv72d1") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.25.2 (c (n "uniffi_checksum_derive") (v "0.25.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0w57dvmpacz2ns4ix9zmh69zknj00ir2dazv4716zbzwy0rjh26f") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.25.3 (c (n "uniffi_checksum_derive") (v "0.25.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0b00ld4k1riwq445ghw95rsq3hxxc7x6cpcqzlq96bbi5w97q4sm") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.26.0 (c (n "uniffi_checksum_derive") (v "0.26.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1p89pf80xaywgkjk44zh0wv7byx89x1dwm67q4wwrvqwgh1hkxwz") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.26.1 (c (n "uniffi_checksum_derive") (v "0.26.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1whfvpib0j7nn4nj9b0plkqhzjrgin2hfc9br7h31nvaz8x5nxvj") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.27.0 (c (n "uniffi_checksum_derive") (v "0.27.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "09dvmnjl40fczi1fw6cr62cbr2qpdsf7sdnhls9mk2ipvv192zqz") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.27.1 (c (n "uniffi_checksum_derive") (v "0.27.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1g2yj3i3hypcbqfvws0q0arjn2qgmyvc0zpmaprc7pmi6dn5lzmf") (f (quote (("nightly") ("default"))))))

(define-public crate-uniffi_checksum_derive-0.27.2 (c (n "uniffi_checksum_derive") (v "0.27.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1jf2lx1s52g4llw6ajxydrlbz17dpsxfqb8kp49blf618k6nrs4m") (f (quote (("nightly") ("default"))))))

