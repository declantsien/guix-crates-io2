(define-module (crates-io un if unified-diff) #:use-module (crates-io))

(define-public crate-unified-diff-0.1.0 (c (n "unified-diff") (v "0.1.0") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 0)))) (h "14qh6sa76dh1wml2jvbmkgrjvwsy4nszllz0smv6kqgz7lh1jdcs")))

(define-public crate-unified-diff-0.1.1 (c (n "unified-diff") (v "0.1.1") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 0)))) (h "1bxhvz8wzvfd165f48brfwj15ibdf16p1ypch8jvznhlz54hlypc")))

(define-public crate-unified-diff-0.1.2 (c (n "unified-diff") (v "0.1.2") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 0)))) (h "0vznjqxaylmp9b51i98fp70dvdc53qqcn9yr8apxgwjzkdaxdwry")))

(define-public crate-unified-diff-0.2.0 (c (n "unified-diff") (v "0.2.0") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 0)))) (h "133nj0gsmh7a6bp1ziqar77n63biybrl06lkscl34ri6akgcml7d")))

(define-public crate-unified-diff-0.2.1 (c (n "unified-diff") (v "0.2.1") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 0)))) (h "0pwd0h7qbxygskaq7cx9nn7i8fyryy8lmkpa3i0hzhyhbqwkssj9")))

