(define-module (crates-io un if uniffi_bindgen_kotlin_multiplatform) #:use-module (crates-io))

(define-public crate-uniffi_bindgen_kotlin_multiplatform-0.1.0 (c (n "uniffi_bindgen_kotlin_multiplatform") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "askama") (r "^0.12") (f (quote ("config"))) (k 0)) (d (n "camino") (r "^1.1.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "std" "derive"))) (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "^0.25.2") (d #t) (k 0)))) (h "09xlwrkdwzf6bddf33gf8i1jfx37zz3z77w76bwnm9zbdbcxq921")))

