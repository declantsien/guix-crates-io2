(define-module (crates-io un if uniffi_testing) #:use-module (crates-io))

(define-public crate-uniffi_testing-0.22.0 (c (n "uniffi_testing") (v "0.22.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vylk67npsgwavimg2nk6kgcxdwjvpp8bblfhsy5mcraxj88rmhc")))

(define-public crate-uniffi_testing-0.23.0 (c (n "uniffi_testing") (v "0.23.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0v7psxvaplm6b0r555qz19kk30n138b94fryw9ydjhgcac4mgc4j")))

(define-public crate-uniffi_testing-0.24.0 (c (n "uniffi_testing") (v "0.24.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "113rig7a65jyx856p96xgs453f85nf81hma2gq945mfl8dkdhmz9")))

(define-public crate-uniffi_testing-0.24.1 (c (n "uniffi_testing") (v "0.24.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1frb5ifjfzpyr4i91dr1yfbrhlsvqy5xh7rq9qkljljnr2i3gd2g")))

(define-public crate-uniffi_testing-0.24.2 (c (n "uniffi_testing") (v "0.24.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cwcm2k3z7pdi4imzkq65ppxbl57lwz1l7dsrrmvpcxs5pdyqar8")))

(define-public crate-uniffi_testing-0.24.3 (c (n "uniffi_testing") (v "0.24.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0faa5ilzf3g7dvirjcng27kqijy788nrbzj183njgp0n932xfj0h")))

(define-public crate-uniffi_testing-0.25.0 (c (n "uniffi_testing") (v "0.25.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "0v3vxvnjxihpkx2z33730czb3sr1pdv9x6h3lpvbvpq4r8x3izj0")))

(define-public crate-uniffi_testing-0.25.1 (c (n "uniffi_testing") (v "0.25.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "11m8iq26mjpzfb5k84cd39gkw85rwhr27v9cq6h68lmsr7y7a1n6")))

(define-public crate-uniffi_testing-0.25.2 (c (n "uniffi_testing") (v "0.25.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "00mk5pipgz5v7syvlcx6d1zq3pkx4ygbq6lcw3b4x6r2jy4j2blg")))

(define-public crate-uniffi_testing-0.25.3 (c (n "uniffi_testing") (v "0.25.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "0g7jv7w979n43nkmkrjk6w9f1drsjgxhalwcpvfpddpwpzg4i10i")))

(define-public crate-uniffi_testing-0.26.0 (c (n "uniffi_testing") (v "0.26.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "0qqnra750r2rlnn3rsv1219g7jvfkh5hjygqjc1wjy3n6ibkvj1m")))

(define-public crate-uniffi_testing-0.26.1 (c (n "uniffi_testing") (v "0.26.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "0la258qzrpy3wknfd9g8lg1fpmq676garilw4g439prfl8cmgjnd")))

(define-public crate-uniffi_testing-0.27.0 (c (n "uniffi_testing") (v "0.27.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "10lix8bjjqv8jl0s7c4zq0djmgmb6wb7f6c5ik2xwadn805cg2mj")))

(define-public crate-uniffi_testing-0.27.1 (c (n "uniffi_testing") (v "0.27.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "1vlysqclnkdvh19mgwzfarn54j7pxnm4w14physqihnz1f6qgkpq")))

(define-public crate-uniffi_testing-0.27.2 (c (n "uniffi_testing") (v "0.27.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "14bcrx1g7vs4f1dmjsrk82rh4va80nf94cayfh958rd2hdy8bzic")))

