(define-module (crates-io un if uniffi_udl) #:use-module (crates-io))

(define-public crate-uniffi_udl-0.25.0 (c (n "uniffi_udl") (v "0.25.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_meta") (r "=0.25.0") (d #t) (k 0)) (d (n "uniffi_testing") (r "=0.25.0") (d #t) (k 0)) (d (n "weedle2") (r "^4.0.0") (d #t) (k 0)))) (h "10jxlf3gfd090hmlmr0irjay44kw22cgc9plbrws5cgw20adsh30")))

(define-public crate-uniffi_udl-0.25.1 (c (n "uniffi_udl") (v "0.25.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_meta") (r "=0.25.1") (d #t) (k 0)) (d (n "uniffi_testing") (r "=0.25.1") (d #t) (k 0)) (d (n "weedle2") (r "^4.0.0") (d #t) (k 0)))) (h "07915r863pzkh8w5dayghg7rqv02kif7viqflln6jrjir585k74y")))

(define-public crate-uniffi_udl-0.25.2 (c (n "uniffi_udl") (v "0.25.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_meta") (r "=0.25.2") (d #t) (k 0)) (d (n "uniffi_testing") (r "=0.25.2") (d #t) (k 0)) (d (n "weedle2") (r "^4.0.0") (d #t) (k 0)))) (h "0ahb43nrcdli8z02hd2bmqacxpj95hnw7cx3dsc34vr5a2g90agv")))

(define-public crate-uniffi_udl-0.25.3 (c (n "uniffi_udl") (v "0.25.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_meta") (r "=0.25.3") (d #t) (k 0)) (d (n "uniffi_testing") (r "=0.25.3") (d #t) (k 0)) (d (n "weedle2") (r "^4.0.0") (d #t) (k 0)))) (h "1hx14kfd3m0vxzsvshfid0s6njpp1h3v9sak1sz8l1y615qxp7l8")))

(define-public crate-uniffi_udl-0.26.0 (c (n "uniffi_udl") (v "0.26.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "uniffi_meta") (r "=0.26.0") (d #t) (k 0)) (d (n "uniffi_testing") (r "=0.26.0") (d #t) (k 0)) (d (n "weedle2") (r "^4.0.1") (d #t) (k 0)))) (h "1gv9v09x62cgpz9hgygzp94gs08ylqmljj6zifxh9682r9f2x6jy")))

(define-public crate-uniffi_udl-0.26.1 (c (n "uniffi_udl") (v "0.26.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "uniffi_meta") (r "=0.26.1") (d #t) (k 0)) (d (n "uniffi_testing") (r "=0.26.1") (d #t) (k 0)) (d (n "weedle2") (r "^5.0.0") (d #t) (k 0)))) (h "0jyvbd4acrk45i54cxi3cbfvzgna4jsrk7s5s38crb0laz0ifs1z")))

(define-public crate-uniffi_udl-0.27.0 (c (n "uniffi_udl") (v "0.27.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "uniffi_meta") (r "=0.27.0") (d #t) (k 0)) (d (n "uniffi_testing") (r "=0.27.0") (d #t) (k 0)) (d (n "weedle2") (r "^5.0.0") (d #t) (k 0)))) (h "0kbipmyvwrl3zgf3m0m3m56kc5d4dqkpyjafxcvx8dg748ngn8s8")))

(define-public crate-uniffi_udl-0.27.1 (c (n "uniffi_udl") (v "0.27.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "uniffi_meta") (r "=0.27.1") (d #t) (k 0)) (d (n "uniffi_testing") (r "=0.27.1") (d #t) (k 0)) (d (n "weedle2") (r "^5.0.0") (d #t) (k 0)))) (h "186fhis7bacv8kminmwfvjb6pnwj201j6bdf7rf0mlm883nwjhwc")))

(define-public crate-uniffi_udl-0.27.2 (c (n "uniffi_udl") (v "0.27.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "uniffi_meta") (r "=0.27.2") (d #t) (k 0)) (d (n "uniffi_testing") (r "=0.27.2") (d #t) (k 0)) (d (n "weedle2") (r "^5.0.0") (d #t) (k 0)))) (h "0jpd73hwxxnsbm6x7jg0ifcrv6chf3rkgk9hxzsrl3d6s5fxs4dg")))

