(define-module (crates-io un if unifont) #:use-module (crates-io))

(define-public crate-unifont-0.1.0 (c (n "unifont") (v "0.1.0") (h "1z2nr04jhzay29z70krzywfb7izy29sgbc4y3f0mri2xlhmz41ix")))

(define-public crate-unifont-1.0.0+data-15.1.05 (c (n "unifont") (v "1.0.0+data-15.1.05") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)))) (h "1gbzbmwc25xcjg1y3fmyigy5d0bzpw2rqmfwf0rf08qx6v0x06mn")))

(define-public crate-unifont-1.1.0+data-15.1.05 (c (n "unifont") (v "1.1.0+data-15.1.05") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "unicode-bidi") (r "^0.3.15") (f (quote ("hardcoded-data"))) (k 2)))) (h "0mp1r9dklxwjxa8p2bh0c26n2vpmf9v79mh1k3qzz9majzsspdcf")))

