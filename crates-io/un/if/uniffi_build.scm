(define-module (crates-io un if uniffi_build) #:use-module (crates-io))

(define-public crate-uniffi_build-0.1.0 (c (n "uniffi_build") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0zsljk0937wvi380h9j55yyycj2kwkbjjlvilr3svs5cc9gq2wgp") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.2.0 (c (n "uniffi_build") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "14x6lnh8m7xn4kzg6ifh8clq42sxcbhgjp6zrw5hpfbf59vr9g8n") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.3.0 (c (n "uniffi_build") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11.3") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1qg1y40gqir87l5rpym9ddyrpbg4skjxdlck97b1h33s7wx63izb") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.4.0 (c (n "uniffi_build") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11.3") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1j0bxvyajqcvr6y1wx8yxhk4ic16jhrm9l259cnc6d23cwx248q9") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.5.0 (c (n "uniffi_build") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0pqgd0sr64ck8905axbng63b0cl2h61wp2ihyrqkiaqlpmkckja2") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.6.0 (c (n "uniffi_build") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0i598zr8f8w94w6m5zia85ndav2rmhrjw11lfcyvwayz5bmxfhvg") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.6.1 (c (n "uniffi_build") (v "0.6.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.6.1") (o #t) (d #t) (k 0)))) (h "0dq1qnr8szygwiybgn9n6s3zyzwqnxpn23jfci2hl30vqvpcbmrq") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.7.0 (c (n "uniffi_build") (v "0.7.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.7.0") (o #t) (d #t) (k 0)))) (h "13kicbzxq4bwxn4vb9db2410anbsd43m2pg12lfj2zh5n2ffv4zb") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.7.1 (c (n "uniffi_build") (v "0.7.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.7.1") (o #t) (d #t) (k 0)))) (h "06hb1wsx13psihcjg7las5vwkihnlrbzd5217rkyyf21nrawbq3c") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.7.2 (c (n "uniffi_build") (v "0.7.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "= 0.7.2") (o #t) (d #t) (k 0)))) (h "0kv6x3k06fkppsz3aa822zqnqdx9mggdai9nzgsg2jxh29ms4hnl") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.8.0 (c (n "uniffi_build") (v "0.8.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.8.0") (o #t) (d #t) (k 0)))) (h "137iah6i0vb16n0ax5gkrmaaik0jizkimvdcj5ykhb6a5l194s7s") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.9.0 (c (n "uniffi_build") (v "0.9.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.9.0") (o #t) (d #t) (k 0)))) (h "0g3v2ccqb91sp2g232j1q070lm9w1xg0b1a6c2mbqh4x81525z0g") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.10.0 (c (n "uniffi_build") (v "0.10.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.10.0") (o #t) (d #t) (k 0)))) (h "0bf0pw4bw9k3g7ymfkb0arb8831npbxphyk3m3l4s110pavc6zpv") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.11.0 (c (n "uniffi_build") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.11.0") (o #t) (d #t) (k 0)))) (h "0k1akh61r1751dvqg65f9zd94l9w3njyamx2451jb7prba928mh1") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.12.0 (c (n "uniffi_build") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.12.0") (o #t) (d #t) (k 0)))) (h "0fn59vcl7mbj248gzy55p4kf2j3nwp0zi01kn7kpxmxsr37mv974") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.13.0 (c (n "uniffi_build") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.13.0") (o #t) (d #t) (k 0)))) (h "19fxlvkhz3a13ydx2rm5sir8yi485fr1778hh56zc81abk9r03fd") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.13.1 (c (n "uniffi_build") (v "0.13.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.13.1") (o #t) (d #t) (k 0)))) (h "1mnp318v9mjvx6ay3hfpyy5vbfk4v3drmzrc2m04fmhxfx2g834r") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.14.0 (c (n "uniffi_build") (v "0.14.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.14.0") (o #t) (d #t) (k 0)))) (h "1g3m27wy2dgjphxgfsh8spz8y4nw814a75f3bkvna9ahrzbgigbv") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.14.1 (c (n "uniffi_build") (v "0.14.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.14.1") (o #t) (d #t) (k 0)))) (h "0ab486xmnj4rfn2caskriky4zgfk3mn1ksxqwphav7ckbrvgk6pa") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.15.0 (c (n "uniffi_build") (v "0.15.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.15.0") (o #t) (d #t) (k 0)))) (h "0hvj7rrrfzn1d90f2b6cgfaf3fhdyy6958vigzja7c1wsz3rgdvm") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.15.1 (c (n "uniffi_build") (v "0.15.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.15.1") (o #t) (d #t) (k 0)))) (h "0praph706sc18559bmwmcav98s3qgz4b5r2bi4azr0850pannksl") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.15.2 (c (n "uniffi_build") (v "0.15.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.15.2") (o #t) (d #t) (k 0)))) (h "0q29dxkhc8bn7brakzjakp4kd9milpa0sbz5yzggw0p3ywhpvvqf") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.16.0 (c (n "uniffi_build") (v "0.16.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.16.0") (o #t) (d #t) (k 0)))) (h "09a29dlz5c43c4xmp3kjr942acjzg1mmclrgd3fcy0wmpqm2jhcy") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.17.0 (c (n "uniffi_build") (v "0.17.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.17.0") (o #t) (d #t) (k 0)))) (h "1fqvlpwggb3janvxw0lzl5d3wi40hk794filfqhw8wy73675vcxp") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.18.0 (c (n "uniffi_build") (v "0.18.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.18.0") (o #t) (d #t) (k 0)))) (h "01q6ngcs82kb20v4a98ql7985ixkvb0q61isq90kbacdzlf41dxw") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.19.0 (c (n "uniffi_build") (v "0.19.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.18.0") (o #t) (d #t) (k 0)))) (h "10v8vpxq766v2cndslbgidb0hf05wdcwk85hbdzcqpfv4498frpw") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.19.1 (c (n "uniffi_build") (v "0.19.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.19.1") (o #t) (d #t) (k 0)))) (h "0mx114lalichhjp9w3klwygqfrnk393bbqb318kv18b9vriq0l4y") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.19.2 (c (n "uniffi_build") (v "0.19.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.19.2") (o #t) (d #t) (k 0)))) (h "1r8z8qprghkh107pmdxbp7yq135b3rw5d4abhrmgk2vf599c61c3") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.19.3 (c (n "uniffi_build") (v "0.19.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.19.3") (o #t) (d #t) (k 0)))) (h "1gdg4ggjb14y0jrcz9l6pmi655krksngdr8p0cgn4kjyc9h0izwg") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.19.4 (c (n "uniffi_build") (v "0.19.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.19.4") (o #t) (d #t) (k 0)))) (h "0fy2q01w047z300l8xfaz5p988fv30m1k0kzbr94x180c8ffnqpv") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.19.5 (c (n "uniffi_build") (v "0.19.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.19.5") (o #t) (d #t) (k 0)))) (h "1dsi86r0zjm6cx7gqyhzbfkbifriicizdz5grg96r716q9xwl4c3") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.19.6 (c (n "uniffi_build") (v "0.19.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.19.6") (o #t) (d #t) (k 0)))) (h "17qm7fagp267k0r89ypp58h9yycsipyzqdq84wkm4d3vz7nnyv4d") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.20.0 (c (n "uniffi_build") (v "0.20.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.20.0") (o #t) (d #t) (k 0)))) (h "19fvd5ljsvk3w81n5jzl1c9wmcbqwpxgd78dpdlh7ixsq3hmmdkl") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.21.0 (c (n "uniffi_build") (v "0.21.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.21.0") (o #t) (d #t) (k 0)))) (h "1nv12lf9ql90mbqhl0hdw82s30kczjk29s7b66vnwf59d31qf0ji") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.21.1 (c (n "uniffi_build") (v "0.21.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.21.1") (o #t) (d #t) (k 0)))) (h "0ps5163m2nl6jmyw1cwljb1jxk7j4sdkpkyd1nmm4ggf6c2fadfh") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.22.0 (c (n "uniffi_build") (v "0.22.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.22.0") (o #t) (k 0)))) (h "0879lcqb2k62gfsqyia2q9lamxnkfryd902p1pa7z1dfap6idcbd") (f (quote (("default") ("builtin-bindgen" "uniffi_bindgen"))))))

(define-public crate-uniffi_build-0.23.0 (c (n "uniffi_build") (v "0.23.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.23.0") (k 0)))) (h "1yr95y4nifri868kdw63i0sc8sb2lqajwgixgrqq6ggzd21s5q8f") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.24.0 (c (n "uniffi_build") (v "0.24.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.24.0") (k 0)))) (h "0v2jr4xjgq2lrng8fwgzjmj4nmbwp6jbl3768ayndj4f5i9xjc27") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.24.1 (c (n "uniffi_build") (v "0.24.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.24.1") (k 0)))) (h "0smsxixfi5msj7yqcz58q90jv7iv3i4im3a3wmlp96bvw41wvdsj") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.24.2 (c (n "uniffi_build") (v "0.24.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.24.2") (k 0)))) (h "0h4y1x9xfisyr3b6aiq4bp016jwviyjm58xzl7435pk7djpx7qgw") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.24.3 (c (n "uniffi_build") (v "0.24.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.24.3") (k 0)))) (h "07fmv8wpvaq53a13ifnqd73iq8zh49ayssw1p4himhjizf9zc82b") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.25.0 (c (n "uniffi_build") (v "0.25.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.25.0") (k 0)))) (h "0ys8z4nayd5f574kyw7p4bslv7zhsk43lnc3907bh7wmryvlwczj") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.25.1 (c (n "uniffi_build") (v "0.25.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.25.1") (k 0)))) (h "0bziw6kp3vp1ncrxb501bk4yn5xr4fhbdvvq5xppm4giaax221hp") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.25.2 (c (n "uniffi_build") (v "0.25.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.25.2") (k 0)))) (h "1x5h1vb62y0zfpski35bv56sxz8i006gp8xchnq6d2js07brw141") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.25.3 (c (n "uniffi_build") (v "0.25.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.25.3") (k 0)))) (h "1sbl3jipbl9jpfb5xdi7pjcnchn3z6n7bbrs9c401ml26vfn8680") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.26.0 (c (n "uniffi_build") (v "0.26.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.26.0") (k 0)))) (h "0wg8jb99ync89gvwxr0wk286jj5maql4h82awwh5n37wm1mqcrwz") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.26.1 (c (n "uniffi_build") (v "0.26.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.26.1") (k 0)))) (h "1flwmsj951ncgrawx0idjnpijbjdl8bdi69iwlfdqwk8iihag8sa") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.27.0 (c (n "uniffi_build") (v "0.27.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.27.0") (k 0)))) (h "03yqjznch1q4iciqdpib9pv4r6kmyy6zmr46vy9m0bn0isb35fxn") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.27.1 (c (n "uniffi_build") (v "0.27.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.27.1") (k 0)))) (h "1d1y0a57m1hxpbfkvl4dc1i3qyls0wawki1h92ssicxpmqks9js5") (f (quote (("default") ("builtin-bindgen"))))))

(define-public crate-uniffi_build-0.27.2 (c (n "uniffi_build") (v "0.27.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1.0.8") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "=0.27.2") (k 0)))) (h "1r2x0xwgvqas1ib8b8hr6l9378q3c86qd8i8v3dxakh74gj92ws9") (f (quote (("default") ("builtin-bindgen"))))))

