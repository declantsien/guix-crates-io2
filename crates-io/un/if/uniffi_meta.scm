(define-module (crates-io un if uniffi_meta) #:use-module (crates-io))

(define-public crate-uniffi_meta-0.19.4 (c (n "uniffi_meta") (v "0.19.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "enum_variant_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "03wydd9pp10cminq3i4vnp9gn8lz6jhbdajbg5088qky0mpbbkbf")))

(define-public crate-uniffi_meta-0.19.5 (c (n "uniffi_meta") (v "0.19.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "enum_variant_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "11j4kawwzh0bs2yyqjvybwjzir6qva67c6qn3l75pvw952z1inzq")))

(define-public crate-uniffi_meta-0.19.6 (c (n "uniffi_meta") (v "0.19.6") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1riqbq36nmzivbx7y94li4yx35hwcsfw7rhsq572j8f7l379kl0h")))

(define-public crate-uniffi_meta-0.20.0 (c (n "uniffi_meta") (v "0.20.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xax77cc34d8nhlm7r3270f8k7gbh8yh791p5k11zlfbjgk6f5zy")))

(define-public crate-uniffi_meta-0.21.0 (c (n "uniffi_meta") (v "0.21.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mrvvk8hp0g85sc16y39b265nyprqpc3hf5r6qa6hdrrcp61g56d")))

(define-public crate-uniffi_meta-0.21.1 (c (n "uniffi_meta") (v "0.21.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.21.0") (d #t) (k 0)))) (h "0f3a56ikj32gr6z064hh57hrh93vz1ncf4f1yyvcjad85m23b63j")))

(define-public crate-uniffi_meta-0.22.0 (c (n "uniffi_meta") (v "0.22.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.22.0") (d #t) (k 0)))) (h "1d7fkvqqiq9c4fg35l15hn36c090r10zbwq4hrsykqlqdwnrm0cz")))

(define-public crate-uniffi_meta-0.23.0 (c (n "uniffi_meta") (v "0.23.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.23.0") (d #t) (k 0)))) (h "19jfbkw721vl6lr306xiv7z8p51kxh245imyj5ipmvba8cnapzb6")))

(define-public crate-uniffi_meta-0.24.0 (c (n "uniffi_meta") (v "0.24.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.24.0") (d #t) (k 0)) (d (n "uniffi_core") (r "=0.24.0") (d #t) (k 0)))) (h "0z1l7m9chhk13c8mf2jczj5a69vpdqhmp8dv74dzmbl2yjd5pn9j")))

(define-public crate-uniffi_meta-0.24.1 (c (n "uniffi_meta") (v "0.24.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.24.1") (d #t) (k 0)) (d (n "uniffi_core") (r "=0.24.1") (d #t) (k 0)))) (h "05xjlyf3ca8lbpps71cdrq48ch7nkb2y3pnbrpqdinhkifjqhczy")))

(define-public crate-uniffi_meta-0.24.2 (c (n "uniffi_meta") (v "0.24.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.24.2") (d #t) (k 0)))) (h "1758j5sq54v6jsz5f019rip20vm0h78kq3i01fklb7srsc8qbf7v")))

(define-public crate-uniffi_meta-0.24.3 (c (n "uniffi_meta") (v "0.24.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.24.3") (d #t) (k 0)))) (h "1rsn8r4w02hgpxif9q3vj1637h36s5ssflww1169b1b5kalbn5gq")))

(define-public crate-uniffi_meta-0.25.0 (c (n "uniffi_meta") (v "0.25.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.25.0") (d #t) (k 0)))) (h "1wfdnz6pwrfvmwg5svy1w52r1xbxd0vjal7i4a8gc4lciyp23h1x")))

(define-public crate-uniffi_meta-0.25.1 (c (n "uniffi_meta") (v "0.25.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.25.1") (d #t) (k 0)))) (h "1rcjfs7yb2pmsy6jkgcs4a3h8zkkcs9s9278c93wh73h7g6bkvmg")))

(define-public crate-uniffi_meta-0.25.2 (c (n "uniffi_meta") (v "0.25.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.25.2") (d #t) (k 0)))) (h "0psz53kza1m7qbv3pbmq44mj0cdb4h4cpq7ac9lysyd351y37vr2")))

(define-public crate-uniffi_meta-0.25.3 (c (n "uniffi_meta") (v "0.25.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.25.3") (d #t) (k 0)))) (h "18hp6jnk8dhv3w92bia06k0bygi7i14a6v9xciqlpb5ilxrqbp3i")))

(define-public crate-uniffi_meta-0.26.0 (c (n "uniffi_meta") (v "0.26.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.26.0") (d #t) (k 0)))) (h "0g5rzmdpwzs5if3n11kraai1s3835vi03g591xzp296wsibzzjbk")))

(define-public crate-uniffi_meta-0.26.1 (c (n "uniffi_meta") (v "0.26.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.26.1") (d #t) (k 0)))) (h "119fzha534mnsvwjkshkispy333hb379qgr9kn6p6wanj2gajr1z")))

(define-public crate-uniffi_meta-0.27.0 (c (n "uniffi_meta") (v "0.27.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.27.0") (d #t) (k 0)))) (h "14rq511j0fmxfysgi068r4rlqfa03hyxls7h708klbcvznhnzgkh")))

(define-public crate-uniffi_meta-0.27.1 (c (n "uniffi_meta") (v "0.27.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.27.1") (d #t) (k 0)))) (h "1f64n0wdsvlr1wknz4igjvdj2kfamda23jlzrb3q3lfgqhi488pp")))

(define-public crate-uniffi_meta-0.27.2 (c (n "uniffi_meta") (v "0.27.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "uniffi_checksum_derive") (r "^0.27.2") (d #t) (k 0)))) (h "1kyfxz4gdp161y366f9ivhky0k1mkbf9q7mgp7zrnjypf2ivss3r")))

