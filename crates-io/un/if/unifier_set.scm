(define-module (crates-io un if unifier_set) #:use-module (crates-io))

(define-public crate-unifier_set-0.2.0 (c (n "unifier_set") (v "0.2.0") (d (list (d (n "dot_graph") (r "^0.2.3") (d #t) (k 2)) (d (n "rpds") (r "^0.12.0") (d #t) (k 0)))) (h "1d1grc9al1113pmn2vsp100pd12i0ggs9pjn5iy7nc9sp1y5b9w9")))

