(define-module (crates-io un if unifi-protect-bulk-download) #:use-module (crates-io))

(define-public crate-unifi-protect-bulk-download-0.1.0 (c (n "unifi-protect-bulk-download") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (d #t) (k 0)))) (h "0bkl0ya96lzkgz14yf1v86s7lajza61jdymh7s9i460l851vnz70")))

(define-public crate-unifi-protect-bulk-download-0.2.0 (c (n "unifi-protect-bulk-download") (v "0.2.0") (d (list (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.1.0") (d #t) (k 0)))) (h "08g4akl2255mhda9gigqzn9vlpw85qjknkxy2cplliqarkf747zc")))

(define-public crate-unifi-protect-bulk-download-0.3.0 (c (n "unifi-protect-bulk-download") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.2.0") (d #t) (k 0)))) (h "0x3vh0ni1bgnx7779mkfm7wddzk50w12xr903z7jxj5x4500qyd2")))

(define-public crate-unifi-protect-bulk-download-0.4.0 (c (n "unifi-protect-bulk-download") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.2") (d #t) (k 0)))) (h "0razga0w2y2pdx6wl50j4r806qvy85p67s72ikwhk4jlm6pg0rfl")))

(define-public crate-unifi-protect-bulk-download-0.4.1 (c (n "unifi-protect-bulk-download") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.3") (d #t) (k 0)))) (h "08n14wd09cppy2k9s58s1x0zlb211fsgwyjxz3rym7zhpmc64ymk")))

(define-public crate-unifi-protect-bulk-download-0.4.4 (c (n "unifi-protect-bulk-download") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.3") (d #t) (k 0)))) (h "1j1irbvxz2yqzirhlj620cz0qbvwgvrp2491m77lq56l3n739pn7")))

(define-public crate-unifi-protect-bulk-download-0.4.5 (c (n "unifi-protect-bulk-download") (v "0.4.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.3") (d #t) (k 0)))) (h "10xf5fxjny1n63xlwgmi42fbm6h33kfd20dna8zqk9rqy191b1pq")))

(define-public crate-unifi-protect-bulk-download-0.4.6 (c (n "unifi-protect-bulk-download") (v "0.4.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "sanitize-filename") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.3") (d #t) (k 0)))) (h "0saskv43kzycbnj3knjhj9kmqbzjm93d1r1dzr8z6lv5ij1q05qj")))

(define-public crate-unifi-protect-bulk-download-0.4.7 (c (n "unifi-protect-bulk-download") (v "0.4.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "sanitize-filename") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.3") (d #t) (k 0)))) (h "1zcxa4b73npbkv5i4nz4j923q29676f5cxpyafp352q6s8r6j82r")))

(define-public crate-unifi-protect-bulk-download-0.4.8 (c (n "unifi-protect-bulk-download") (v "0.4.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "sanitize-filename") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.3") (d #t) (k 0)))) (h "0jam6y59d37az829j3rsdrh48blfy0nk5i0j88pzpjnb17qa98by")))

(define-public crate-unifi-protect-bulk-download-0.4.9 (c (n "unifi-protect-bulk-download") (v "0.4.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "sanitize-filename") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.4") (d #t) (k 0)))) (h "10rig7v3c5l19rs4y2hwyrd5l03ffzwxvp5jhyab34xcsdpz39hk")))

(define-public crate-unifi-protect-bulk-download-0.4.10 (c (n "unifi-protect-bulk-download") (v "0.4.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "sanitize-filename") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.5") (d #t) (k 0)))) (h "08cdgbwj6m9dbzf0a013jkjyyyc0sd3213y8chr0k5wns7qdf65f")))

(define-public crate-unifi-protect-bulk-download-0.4.11 (c (n "unifi-protect-bulk-download") (v "0.4.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "sanitize-filename") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.4.6") (d #t) (k 0)))) (h "0jp2fa17pswfxw6ps7pz0q98x46y1s4f0bpfkyrpfnx1kkchb7f8")))

(define-public crate-unifi-protect-bulk-download-0.5.0 (c (n "unifi-protect-bulk-download") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "sanitize-filename") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.5.1") (d #t) (k 0)))) (h "1fyx017swhlhl06nm3w7m15y2xmcfl585jrg70k1ybyc7h6lnvrp")))

(define-public crate-unifi-protect-bulk-download-0.5.1 (c (n "unifi-protect-bulk-download") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "sanitize-filename") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.5.2") (d #t) (k 0)))) (h "0cgpy05fl3ixi9n5kg40j63j01zz12h9h826vl3gqymivnr1340n")))

(define-public crate-unifi-protect-bulk-download-0.5.2 (c (n "unifi-protect-bulk-download") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "sanitize-filename") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (d #t) (k 0)) (d (n "unifi_protect") (r "^0.5.2") (d #t) (k 0)))) (h "1fzfyabwlqmy617paw3kxvlvngkibhwq71wsh1b62b161vi8c473")))

