(define-module (crates-io un if uniform-cubic-splines) #:use-module (crates-io))

(define-public crate-uniform-cubic-splines-0.1.0 (c (n "uniform-cubic-splines") (v "0.1.0") (d (list (d (n "lerp") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0a2b9aqnl4vhp4zasszfimxkhlfl0x46ssw5zfwzsr7k8ivl0xbw") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-uniform-cubic-splines-0.1.1 (c (n "uniform-cubic-splines") (v "0.1.1") (d (list (d (n "lerp") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0fs8wgl7b9kpkviimykzvhp32dqi0bawslcrbc9lyp76f4fpb8ah") (f (quote (("unstable") ("default"))))))

(define-public crate-uniform-cubic-splines-0.1.2 (c (n "uniform-cubic-splines") (v "0.1.2") (d (list (d (n "lerp") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "06w4m58kd2mam25srwzxs34d5zq49p8r887h8ivjn9xvd91l5w3c") (f (quote (("unstable") ("default"))))))

(define-public crate-uniform-cubic-splines-0.1.3 (c (n "uniform-cubic-splines") (v "0.1.3") (d (list (d (n "lerp") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "17a3jx8587zlxs5g12i43aybyd2391rn845va10r0fr00qr0651w") (f (quote (("unstable") ("default"))))))

(define-public crate-uniform-cubic-splines-0.1.4 (c (n "uniform-cubic-splines") (v "0.1.4") (d (list (d (n "lerp") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "03zlgw9j565jl8ih6bm0gxlk29qmpxzvigx6ga8rfbyhmh6vf0d1") (f (quote (("unstable") ("default"))))))

(define-public crate-uniform-cubic-splines-0.1.5 (c (n "uniform-cubic-splines") (v "0.1.5") (d (list (d (n "lerp") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0zpkalrxd3wvcrgmbd7001bcmkvy775gpixphz8n901gc1zhiwa0") (f (quote (("unstable") ("default"))))))

(define-public crate-uniform-cubic-splines-0.1.6 (c (n "uniform-cubic-splines") (v "0.1.6") (d (list (d (n "is_sorted") (r "^0.1.1") (o #t) (k 0)) (d (n "lerp") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12gi7gmp8naa4yiqq8b91kd2ps8icwks0mgjynj2scf4w056iq54") (f (quote (("use_std" "is_sorted/use_std") ("unstable" "is_sorted/unstable") ("default" "is_sorted"))))))

(define-public crate-uniform-cubic-splines-0.1.7 (c (n "uniform-cubic-splines") (v "0.1.7") (d (list (d (n "is_sorted") (r "^0.1.1") (o #t) (k 0)) (d (n "lerp") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1f5br1b1pv1hydyz5cwgkfpd78h2s8gjlcrf3v3k63dl4s701rgs") (f (quote (("use_std" "is_sorted/use_std") ("unstable" "is_sorted/unstable") ("monotonic_check" "is_sorted") ("default" "monotonic_check"))))))

(define-public crate-uniform-cubic-splines-0.1.8 (c (n "uniform-cubic-splines") (v "0.1.8") (d (list (d (n "is_sorted") (r "^0.1") (o #t) (k 0)) (d (n "lerp") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1w4sjvmvypvmrxqkdxran8hzxvdqy817sldfb8wilwh0m2krrmg5") (f (quote (("unstable" "is_sorted/unstable") ("std" "is_sorted/use_std") ("monotonic_check" "is_sorted") ("default" "monotonic_check"))))))

