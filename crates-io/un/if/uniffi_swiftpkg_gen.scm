(define-module (crates-io un if uniffi_swiftpkg_gen) #:use-module (crates-io))

(define-public crate-uniffi_swiftpkg_gen-0.1.2 (c (n "uniffi_swiftpkg_gen") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1brnrcdfvdzin2kj3vlvza76w6dqcnbdjrhbai2gvvhsmk9p7sb1")))

(define-public crate-uniffi_swiftpkg_gen-0.1.4 (c (n "uniffi_swiftpkg_gen") (v "0.1.4") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0fjnp2x2vys1f0bfiiys45ks952fbj3mwljfyvbq761arhmrb682")))

(define-public crate-uniffi_swiftpkg_gen-0.1.6 (c (n "uniffi_swiftpkg_gen") (v "0.1.6") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0j9jpjwsa8d6ph4pgvgbl1rw3m5zy5nzp9pbzifhgwzd70b3rz65")))

(define-public crate-uniffi_swiftpkg_gen-0.1.8 (c (n "uniffi_swiftpkg_gen") (v "0.1.8") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0mzyc0yqixlzqpbb13dl6lnmcmx1a4x3j0q3b3cimsplfnfpb2jp")))

(define-public crate-uniffi_swiftpkg_gen-0.1.10 (c (n "uniffi_swiftpkg_gen") (v "0.1.10") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1ryjnfr39z9w3yg4jvaj2aap4r1clr5xa86xj9s4iqqzr14fdpy6")))

(define-public crate-uniffi_swiftpkg_gen-0.1.14 (c (n "uniffi_swiftpkg_gen") (v "0.1.14") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "141dv94h4s0xbn0qx83k8i2z9dmb3mvbf65nc7zs947dj21wdcn7")))

(define-public crate-uniffi_swiftpkg_gen-0.1.16 (c (n "uniffi_swiftpkg_gen") (v "0.1.16") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1ghnb6hniah3gj9xf38rmfarwyjyi64jgx5hl2479nkjlj6pb011")))

(define-public crate-uniffi_swiftpkg_gen-0.1.18 (c (n "uniffi_swiftpkg_gen") (v "0.1.18") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0f7hac3x4d8x80bgxmyg3nnlp8sggrndgm1ndnd33k3bhy3sy086")))

(define-public crate-uniffi_swiftpkg_gen-0.2.0 (c (n "uniffi_swiftpkg_gen") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1aap0wqkvkbxssj5di5grd9gzw1vsqsl023y8vy3w91m8zj5m555")))

(define-public crate-uniffi_swiftpkg_gen-0.2.2 (c (n "uniffi_swiftpkg_gen") (v "0.2.2") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "15kbfiad2kq2rai3blvrcicpb1fpqzgp0vl3hm6hncdw200ypa5z")))

(define-public crate-uniffi_swiftpkg_gen-0.2.4 (c (n "uniffi_swiftpkg_gen") (v "0.2.4") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "17kr8h60j9p1513jjczsf7vksfy099iaz7cy1gsq6d7zpacaga1p")))

(define-public crate-uniffi_swiftpkg_gen-0.2.6 (c (n "uniffi_swiftpkg_gen") (v "0.2.6") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0f4kcgsvnyzcpiji7zi12fp7pzbmlc1sb551sand53im26svl9lr")))

(define-public crate-uniffi_swiftpkg_gen-0.2.8 (c (n "uniffi_swiftpkg_gen") (v "0.2.8") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "0zkkd3jf31m6db69xxgccnvgs9i9ivzlkg3pzagjyjiwb26k2f3c")))

