(define-module (crates-io un if unifac) #:use-module (crates-io))

(define-public crate-unifac-0.1.0 (c (n "unifac") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jrbhksys9gybqfhcp97pv9fvnvsbbywdpmm0pg7j5m59ch1wfrj")))

(define-public crate-unifac-0.1.1 (c (n "unifac") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "143msvfg9h1wk17hj11qx1zqs2l9gp9kldn4c4ribisb75bwk9vv")))

(define-public crate-unifac-0.1.2 (c (n "unifac") (v "0.1.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qb7ccjkg76471495hvd53zxvisw1r0zs5wp8f6ak6bgi5vg440p")))

(define-public crate-unifac-0.1.3 (c (n "unifac") (v "0.1.3") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mzx8gfrzdz0l1nzhxj178s4da73jbd0zdx3i4d8xa39mkp5l5dk")))

(define-public crate-unifac-0.1.4 (c (n "unifac") (v "0.1.4") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dwsmzdyg141zyx51djx2r5y07b3xkd2acswxpag4i7hnck78i6v")))

(define-public crate-unifac-0.1.5 (c (n "unifac") (v "0.1.5") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jj1ipbcsdln8z0xvjd3bfkk959hrdqgp83rmwmiq6k305cy7cj3")))

