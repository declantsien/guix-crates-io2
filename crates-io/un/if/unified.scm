(define-module (crates-io un if unified) #:use-module (crates-io))

(define-public crate-unified-0.1.0 (c (n "unified") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cookie") (r "^0.15.1") (d #t) (k 0)) (d (n "ipnet") (r "^2.3.1") (d #t) (k 0)) (d (n "macaddr") (r "^1.0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "11ih6s5nnqlvkmcnvycwykzfajx8nh4966dz21l3h8zli86lya8q") (y #t)))

