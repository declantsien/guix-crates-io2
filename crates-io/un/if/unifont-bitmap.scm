(define-module (crates-io un if unifont-bitmap) #:use-module (crates-io))

(define-public crate-unifont-bitmap-1.0.0+unifont-14.0.01 (c (n "unifont-bitmap") (v "1.0.0+unifont-14.0.01") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "17a6v568sy8zf723d0w2d39m6m5g27il53vgvxqjycwfkm946qc3")))

