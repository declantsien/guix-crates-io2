(define-module (crates-io un if uniform-resource-identifier) #:use-module (crates-io))

(define-public crate-uniform-resource-identifier-0.0.1 (c (n "uniform-resource-identifier") (v "0.0.1") (h "0mcz6vadlw8sqfhkk7ihrrxry0ncxbxzrw4ivl86fkj4v8n88c5i")))

(define-public crate-uniform-resource-identifier-0.0.2 (c (n "uniform-resource-identifier") (v "0.0.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "0h83arzqbc69lnly5zd99zfdf2y9iy2g2iy3b3gpr9x730pzavhr")))

(define-public crate-uniform-resource-identifier-0.0.3 (c (n "uniform-resource-identifier") (v "0.0.3") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "1xpmfipc3vyr1sq17qa4flmv7zm9qjj1dwilp4s5dh49pvkskqq2")))

(define-public crate-uniform-resource-identifier-0.0.4 (c (n "uniform-resource-identifier") (v "0.0.4") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "0xypbdwwfirjf0fq4siydj9a0liy4fbksav0axjwdywz01pd95qy")))

(define-public crate-uniform-resource-identifier-0.0.5 (c (n "uniform-resource-identifier") (v "0.0.5") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "13pjz46s9spcwj0s1ndggh4sjmd6is4j3jqj6avwgk6wfikkw95a")))

(define-public crate-uniform-resource-identifier-0.0.6 (c (n "uniform-resource-identifier") (v "0.0.6") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0a4lvjfjckzrmdlv9lnb7rjrqa7w6vgs77f5vmawbhn45b17vblp")))

(define-public crate-uniform-resource-identifier-0.0.7 (c (n "uniform-resource-identifier") (v "0.0.7") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0zpqi77gnaqwx4xxmbcdfigwy7nyrcmkmdikndn6aq84rixv8d3z")))

(define-public crate-uniform-resource-identifier-0.0.8 (c (n "uniform-resource-identifier") (v "0.0.8") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "16vg50kwpc7lrf92c6yal89vnj4iavkama52vnnyr9pnvxzsxnv3")))

(define-public crate-uniform-resource-identifier-0.0.9 (c (n "uniform-resource-identifier") (v "0.0.9") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "075zjgzqa31rv1mcb8j41wh2632ss3lnvnfhkhvpfmkwr34slzcb")))

(define-public crate-uniform-resource-identifier-0.0.10 (c (n "uniform-resource-identifier") (v "0.0.10") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0czaxf0r9v71jr18bwzc9crch744csrj01kpf7m5h65ph9h7b64q")))

(define-public crate-uniform-resource-identifier-0.0.11 (c (n "uniform-resource-identifier") (v "0.0.11") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1m8wiwb6zfd5cfhz69cg5vila2fkyzqbxx0imqy4k55vj9pkyad6")))

(define-public crate-uniform-resource-identifier-0.0.12 (c (n "uniform-resource-identifier") (v "0.0.12") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "127z3n3k24iys3gyqmd7b11xw1qvrhfrkjalbhm6hjziyhf1j15z")))

