(define-module (crates-io un gr ungrammar) #:use-module (crates-io))

(define-public crate-ungrammar-0.1.0 (c (n "ungrammar") (v "0.1.0") (h "0zdkvbvl793mx3nd3a5qgcj20872mhi300nmjlnwqfmbj542xq8f")))

(define-public crate-ungrammar-1.0.0 (c (n "ungrammar") (v "1.0.0") (h "1jlhp0by2dgwgsg33cbmmsii0b1f1carqgv7iqgna99f6nnizf9b")))

(define-public crate-ungrammar-1.1.0 (c (n "ungrammar") (v "1.1.0") (h "0fciv1q5r6z44a7qch0fnz55b3xzgyadwf0n4ghcx0mxwqmlwcbi")))

(define-public crate-ungrammar-1.1.1 (c (n "ungrammar") (v "1.1.1") (h "1b8a462cdcmcpsaga1ci91n4s1fga12zfp39lkwbrqcfl1c0xqn4")))

(define-public crate-ungrammar-1.1.2 (c (n "ungrammar") (v "1.1.2") (h "1cwj0v8ji18dx07nny140z2phm7dz9pcmykqxaqi9rvvqwm19dms")))

(define-public crate-ungrammar-1.1.3 (c (n "ungrammar") (v "1.1.3") (h "04ky477kyadl2pfhfvizak6hh1q5xs9wrxqk639mixj5bc33jkfa")))

(define-public crate-ungrammar-1.1.4 (c (n "ungrammar") (v "1.1.4") (h "1a8pkv4fjlkr74va2fb372z0v6s7p2nwcvs3w905amdgjfcxh36z")))

(define-public crate-ungrammar-1.2.0 (c (n "ungrammar") (v "1.2.0") (h "1l9my5zig0dja8znlg24rvlb8vd3wmk1dv5rpyq2k9hr24gkcwg0")))

(define-public crate-ungrammar-1.2.1 (c (n "ungrammar") (v "1.2.1") (h "0cq3mmwz3xvf97n3dlwl3aqz6y2w70srzmr6ls2rw7gdz0xap8fm")))

(define-public crate-ungrammar-1.2.2 (c (n "ungrammar") (v "1.2.2") (h "1mv2bqij7csh3wr23w7n6ib2173j7g9s1048wbkpjwv2c2j8ccc7")))

(define-public crate-ungrammar-1.3.0 (c (n "ungrammar") (v "1.3.0") (h "16m5qc680gznjs9syycc0yijjah98jr3w13blafslhxcza9yw4bk")))

(define-public crate-ungrammar-1.4.0 (c (n "ungrammar") (v "1.4.0") (h "1dsgnhxam31x18llk3w1r5canzadyzdsv99c8xa5glnfydwi75b8")))

(define-public crate-ungrammar-1.5.0 (c (n "ungrammar") (v "1.5.0") (h "0mql7ba8xjqw6vnvs8fbzy8cy2k4lip2h2qnaqm8zp1flnnzy6y1")))

(define-public crate-ungrammar-1.6.0 (c (n "ungrammar") (v "1.6.0") (h "1vmpa66bgsmq32mjk64h62hpgblnr32vlc66pj7m8z4gjfvc2v7r")))

(define-public crate-ungrammar-1.7.0 (c (n "ungrammar") (v "1.7.0") (h "02c52v6c7ajzw8ymlv96mvahhraxr2mj8wkwl5lc7m9qc37zqn3p")))

(define-public crate-ungrammar-1.8.0 (c (n "ungrammar") (v "1.8.0") (h "0wpfydm8ccc41vhngvig63vaxj3cdah1jhkwai995y1s821j2fp3")))

(define-public crate-ungrammar-1.9.0 (c (n "ungrammar") (v "1.9.0") (h "14qs5cry0fnnp7sqcwa025zddxa96qcpvlsbs2fm6hmrldsshdxi")))

(define-public crate-ungrammar-1.9.1 (c (n "ungrammar") (v "1.9.1") (h "05ljmgj1y2gijfyr1n2a1iwbb6v940jpd9b7qix4plxxa4nmgyz1")))

(define-public crate-ungrammar-1.9.2 (c (n "ungrammar") (v "1.9.2") (h "12ycnjgf4df3rlprhcnwwz46z53bdvlfhhz8aiixawm884h2x82q")))

(define-public crate-ungrammar-1.9.3 (c (n "ungrammar") (v "1.9.3") (h "0yrdniq44d0aii83fdil152fbxgar0sg3bl0v2ls39pkq1r1747m")))

(define-public crate-ungrammar-1.10.0 (c (n "ungrammar") (v "1.10.0") (h "0p1kil8f173vl30jpzbs0v2n4adlqvplba7b6lx8v1d8fmlqc96f")))

(define-public crate-ungrammar-1.11.0 (c (n "ungrammar") (v "1.11.0") (h "0sja1r428fhqyh1hmpx5525vmiaxa37l4pywl7r4jw1pbmwjkil4")))

(define-public crate-ungrammar-1.12.0 (c (n "ungrammar") (v "1.12.0") (h "0hy3ps93iyjhy27sqlmkrgl51q8dlrarzjh8s15x81cxy0drj48p") (y #t)))

(define-public crate-ungrammar-1.12.1 (c (n "ungrammar") (v "1.12.1") (h "1sll0gnx4c6l1h1bf5lrsf6f567cssz03d8jn714vlklq8jc42z6") (y #t)))

(define-public crate-ungrammar-1.12.2 (c (n "ungrammar") (v "1.12.2") (h "1fkk4dgnp3jl228z6g8ncd0dpjrd2fsa0j9xh3z4ww1hqnkqcrfz")))

(define-public crate-ungrammar-1.13.0 (c (n "ungrammar") (v "1.13.0") (h "17wm4b5r3z5hqail6p609nii37y3j8dr4bxg8x0bkhkc2wa06xkn")))

(define-public crate-ungrammar-1.14.0 (c (n "ungrammar") (v "1.14.0") (h "19z3xlgsgk7q6v408h2ax5ljar74p7179gn4lzjc6zn76mrnvvsh")))

(define-public crate-ungrammar-1.14.1 (c (n "ungrammar") (v "1.14.1") (h "14a8ykklzjy0xyd34ycp47pbdzcz02qrm2jp0kjdippmq306ggix")))

(define-public crate-ungrammar-1.14.2 (c (n "ungrammar") (v "1.14.2") (h "0wh9m3jpraxbink2xmcfd70505rgw458r5hwh6y55jp9d8526d2k")))

(define-public crate-ungrammar-1.14.3 (c (n "ungrammar") (v "1.14.3") (h "044r48vgln32k3hhgm2ss5xpmkmf94fa104xmpn9m2wm7w4zbwhf")))

(define-public crate-ungrammar-1.14.4 (c (n "ungrammar") (v "1.14.4") (h "0nw30rwck011if2hvn6gpj1rdyknqcjzx50mid474d0lggakcvhd")))

(define-public crate-ungrammar-1.14.5 (c (n "ungrammar") (v "1.14.5") (h "1x8f11irp522ywqm4n72wcrd75q1g4gmy7dd320ql2s8ya59hpir")))

(define-public crate-ungrammar-1.14.6 (c (n "ungrammar") (v "1.14.6") (h "05d7bvw6g8yhz8y6yc2zf0w1cray9dfdikmip8vgzck3d1kdc77v")))

(define-public crate-ungrammar-1.14.7 (c (n "ungrammar") (v "1.14.7") (h "0gk7b695hwi13jmwi2vcgchm9q7ng9q4zlci978di1xfi90aklaz")))

(define-public crate-ungrammar-1.14.8 (c (n "ungrammar") (v "1.14.8") (h "0a8m8bb9kkn8myy9g76af1gwgilr54bm0df753xwzjj6mn91hg20")))

(define-public crate-ungrammar-1.14.9 (c (n "ungrammar") (v "1.14.9") (h "1agx2hpp3sirknnx8j17mzcg222024s6vkx6s5v3s3l8zp15kgk6")))

(define-public crate-ungrammar-1.15.0 (c (n "ungrammar") (v "1.15.0") (h "0clpyc7r3iac6sq7k78ijzzf8nh0yq6nirq80mh7s3s505qmc0gd")))

(define-public crate-ungrammar-1.16.0 (c (n "ungrammar") (v "1.16.0") (h "03w8hzmdkrc3l1535wpslh9ci16j3ggzqysyrrci8b57pp5p88x6")))

(define-public crate-ungrammar-1.16.1 (c (n "ungrammar") (v "1.16.1") (h "13ynrv658ikr4lqi3lk1xbcrr1d1qsjnrb8acwfyrwqbgwsdzrd3")))

