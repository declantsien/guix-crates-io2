(define-module (crates-io un gr ungrammar2json) #:use-module (crates-io))

(define-public crate-ungrammar2json-1.0.0 (c (n "ungrammar2json") (v "1.0.0") (d (list (d (n "ungrammar") (r "^1.1.0") (d #t) (k 0)) (d (n "write-json") (r "^0.1.1") (d #t) (k 0)))) (h "0jagx5vf9zlzhn868vi5qq452nz7v6i6k77fckmi88yk69lr7ngp")))

