(define-module (crates-io un hi unhinged) #:use-module (crates-io))

(define-public crate-unhinged-0.1.0 (c (n "unhinged") (v "0.1.0") (d (list (d (n "ci_info") (r "^0.14.5") (d #t) (k 1)) (d (n "clap") (r "^4.0.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "1snypvvil4r3dyv03szkkh90wys9brh38wsd0mm4zv0k8pvirhvv") (y #t)))

(define-public crate-unhinged-0.1.1 (c (n "unhinged") (v "0.1.1") (d (list (d (n "ci_info") (r "^0.14.5") (d #t) (k 1)) (d (n "clap") (r "^4.0.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "17i4gxvv22sd0arvarivx5qkdjxn0ybbkawpyh8inhmrrrc05vwi") (y #t)))

