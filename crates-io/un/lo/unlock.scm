(define-module (crates-io un lo unlock) #:use-module (crates-io))

(define-public crate-unlock-0.0.1 (c (n "unlock") (v "0.0.1") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z67lcfa0kp37fxapabdcsfandm0k5phwck4wgn08axcmq8py3d5") (f (quote (("trace") ("default"))))))

(define-public crate-unlock-0.0.2 (c (n "unlock") (v "0.0.2") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jzi7bsgz3js20z33hc893xch8q64gy4k685cc3g94rk3zf909xv") (f (quote (("trace") ("default"))))))

(define-public crate-unlock-0.0.3 (c (n "unlock") (v "0.0.3") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "10rdymbxsxd0mzjinbknmz4lgngb2yxzanam3bnlwd509ylgcgcj") (f (quote (("trace") ("default"))))))

(define-public crate-unlock-0.0.4 (c (n "unlock") (v "0.0.4") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l2p1wa0sp6zqinv6ixr5lgcpdcs5m0nq34wnd5536ws5gbzf62m") (f (quote (("trace") ("default"))))))

(define-public crate-unlock-0.0.5 (c (n "unlock") (v "0.0.5") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i508py9hzghahw6wjphpjpqdcvzaqhf82i9ynaqc5rs530sn2aj") (f (quote (("trace") ("default")))) (r "1.65")))

(define-public crate-unlock-0.0.6 (c (n "unlock") (v "0.0.6") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "180anz2fyzn40fhdy4pnr846h3x6jx55np7flpnqwijjlad5lhy1") (f (quote (("trace") ("default")))) (r "1.65")))

(define-public crate-unlock-0.0.7 (c (n "unlock") (v "0.0.7") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08xvjnfk7v96c16vvvfmcg31z8lx5m1w0diybpxq30swbivfd16d") (f (quote (("trace") ("default" "parking_lot" "serde")))) (r "1.65")))

(define-public crate-unlock-0.0.8 (c (n "unlock") (v "0.0.8") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ah0ik7jll2366ahwwjfya3581ywq6lm7gr2gxdl9xgr1vdcx5vd") (f (quote (("trace") ("default" "parking_lot" "serde")))) (r "1.65")))

(define-public crate-unlock-0.0.9 (c (n "unlock") (v "0.0.9") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0irndgwjljzp3d3pdc7vg6jc553nzba8y2cqc7j9114qbxa0iban") (f (quote (("trace") ("default" "parking_lot" "serde")))) (r "1.65")))

(define-public crate-unlock-0.0.10 (c (n "unlock") (v "0.0.10") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vnvhnqflv4pv3nc60x3ygsc2znh7w1bl87dknqlixa9kgyhjb2i") (f (quote (("trace") ("default" "parking_lot" "serde")))) (r "1.65")))

(define-public crate-unlock-0.0.11 (c (n "unlock") (v "0.0.11") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zssc7lqd8m30rbl06w3dq0qd0b66c72nnddji4rsq7sm582abnl") (f (quote (("trace") ("default" "parking_lot" "serde")))) (r "1.65")))

(define-public crate-unlock-0.0.12 (c (n "unlock") (v "0.0.12") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mrkdssarnpgyf2j5i7v7237bxbnkrxlpkibh2cfq6f12xm92wby") (f (quote (("trace") ("default" "parking_lot" "serde")))) (r "1.65")))

(define-public crate-unlock-0.0.13 (c (n "unlock") (v "0.0.13") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g69gxivf222dg6nrkn6bbf6hqkd1h3g200bkwbfzndp801vp6xq") (f (quote (("trace") ("default" "parking_lot" "serde")))) (r "1.65")))

