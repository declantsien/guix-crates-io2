(define-module (crates-io un ev uneval) #:use-module (crates-io))

(define-public crate-uneval-0.1.0 (c (n "uneval") (v "0.1.0") (d (list (d (n "batch_run") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0623p6azkbla5qw6vp36y5b3jjlv82qab612j5qpkas6ydfxcx82")))

(define-public crate-uneval-0.1.1 (c (n "uneval") (v "0.1.1") (d (list (d (n "batch_run") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0qlp36pz6vsa9ilsrlkyh175s48kj87angbgqy7lzplrpkgap742")))

(define-public crate-uneval-0.2.0 (c (n "uneval") (v "0.2.0") (d (list (d (n "batch_run") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "155z14x30xxg7hk947pjpvzm5wmsy1lrmidvmmhh907g0a3qfzvn")))

(define-public crate-uneval-0.2.1 (c (n "uneval") (v "0.2.1") (d (list (d (n "batch_run") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "08piwkswnk6yli0hvdcq63bnd77kssq315g1gq9rsjr2n779afyz")))

(define-public crate-uneval-0.2.2 (c (n "uneval") (v "0.2.2") (d (list (d (n "batch_run") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "1nnlg3gnjqwivbwar2qyhq0s5b0lw1788n6x6vffv3gxxsi67bqq")))

(define-public crate-uneval-0.2.3 (c (n "uneval") (v "0.2.3") (d (list (d (n "batch_run") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0fyrj5bl7q60mnz47xcx9k7b70q9kn7mxin2jnsk91viq0mr3m0d")))

(define-public crate-uneval-0.2.4 (c (n "uneval") (v "0.2.4") (d (list (d (n "batch_run") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0wyw25yz7qhhd4i1wg0bf764aw6fkq6gd630x0mpx3b4v0pmvk33")))

