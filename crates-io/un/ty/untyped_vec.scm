(define-module (crates-io un ty untyped_vec) #:use-module (crates-io))

(define-public crate-untyped_vec-0.1.0 (c (n "untyped_vec") (v "0.1.0") (h "1mc5gmndf6rgr6m0dd0w7m26gxzfwxy90f5p1cl5fn6kwfbnmhi5")))

(define-public crate-untyped_vec-0.1.1 (c (n "untyped_vec") (v "0.1.1") (h "0fpyccn068v6cqq537zwd1vn70zmlz7i24ilwwhmb78fc048v9jh")))

