(define-module (crates-io un ty untyped-arena) #:use-module (crates-io))

(define-public crate-untyped-arena-0.1.0 (c (n "untyped-arena") (v "0.1.0") (h "13ik6f2213lj13pbgn189m2axll1ya9fv13pfy8w5fiz1hqwqmcy")))

(define-public crate-untyped-arena-0.1.1 (c (n "untyped-arena") (v "0.1.1") (h "0k5v294av3is10hn01zyxjhpmifmi1xp7brqvmxhyz2801xrhp2m")))

