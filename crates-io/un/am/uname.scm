(define-module (crates-io un am uname) #:use-module (crates-io))

(define-public crate-uname-0.1.0 (c (n "uname") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bb7prg6qligb8xjhxxswajlc3m1f1s1y8v0m57j04rklwv074lw")))

(define-public crate-uname-0.1.1 (c (n "uname") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1j1xd1rryml4j1hf07kahva9d5ym8m9jz9z20hfdpr1jrbq8jbxp")))

