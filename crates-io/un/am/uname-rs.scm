(define-module (crates-io un am uname-rs) #:use-module (crates-io))

(define-public crate-uname-rs-0.1.0 (c (n "uname-rs") (v "0.1.0") (h "1j2zyda9w64qzd5v83mn5agfhsjhmbh3x91d2am5iz8m6vx1pn7n")))

(define-public crate-uname-rs-0.1.1 (c (n "uname-rs") (v "0.1.1") (h "01kr07x0c8r9q7gk6py1bly05hn08i089dkxsmqbkkhqjcjxp634")))

