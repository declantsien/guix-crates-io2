(define-module (crates-io un ce uncertainty) #:use-module (crates-io))

(define-public crate-uncertainty-0.1.0 (c (n "uncertainty") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fiwnpj1s66fdy0kin3jwhiz5hvyfkv382h92smyz2qgm5f9a4k8")))

(define-public crate-uncertainty-0.1.1 (c (n "uncertainty") (v "0.1.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kk0d1q14qd81m66k2dml7v652bh0hfag4ybc8lnalig4zxqhnsh")))

(define-public crate-uncertainty-0.2.0 (c (n "uncertainty") (v "0.2.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m5d9zc298y3879ck174937syxkd7kd4kc41ipcc4v50v3dq3ga3")))

(define-public crate-uncertainty-0.2.1 (c (n "uncertainty") (v "0.2.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gn13xk7zvmwp1klxr7lxy7nfzmggvjsyn00gl77xzbfc1z5yyg5")))

