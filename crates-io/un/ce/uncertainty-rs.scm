(define-module (crates-io un ce uncertainty-rs) #:use-module (crates-io))

(define-public crate-uncertainty-rs-0.1.0 (c (n "uncertainty-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "164a1r9v56j3zaghb8pl58wasdih5fx2ijz66zd6nkyqr9z6zipl") (y #t)))

