(define-module (crates-io un ce uncertain) #:use-module (crates-io))

(define-public crate-uncertain-0.1.0 (c (n "uncertain") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)))) (h "1kr66hnacki7qzgblmql2sfmjj1jd6z2nrhp3mmdfya0zqykb3lx")))

(define-public crate-uncertain-0.2.0 (c (n "uncertain") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)))) (h "1m4j4l83n6sngm820dyb5wcmydf8ah22wnyffnsrf36pxp7pyhwj")))

(define-public crate-uncertain-0.2.1 (c (n "uncertain") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)))) (h "1qgszvdyaywyinf66hw1kmawggrwh3rm9h1036azhwdq6rhrp0yj")))

(define-public crate-uncertain-0.3.0 (c (n "uncertain") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)))) (h "0js3vcjz5hh4nb4p016rvjqvc12f9ibjjj4rvzfric52nkav4n2w")))

(define-public crate-uncertain-0.3.1 (c (n "uncertain") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)))) (h "1khr3q6f5ma9hj92g0i9nd5x4ff0d86xhagxi2ywg7i1j86lc40v")))

