(define-module (crates-io un js unjson) #:use-module (crates-io))

(define-public crate-unjson-0.0.1 (c (n "unjson") (v "0.0.1") (d (list (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)))) (h "0ymhrhj7h84g2x2130djiska7rl1r5hh57azv1zpp0nv7pzvgq0q")))

(define-public crate-unjson-0.0.3 (c (n "unjson") (v "0.0.3") (d (list (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)))) (h "088cypa9xi0ipl78ba16mj8np55qx6l6l3svp3pc3lrsp0apv2n7")))

(define-public crate-unjson-0.0.4 (c (n "unjson") (v "0.0.4") (d (list (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)))) (h "0d7hwkhlh6z0v9p2d4w3cjhf253ljidp4awrliyi9xzi2z4v2y58")))

(define-public crate-unjson-0.0.5 (c (n "unjson") (v "0.0.5") (d (list (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)))) (h "1iw7sp2i9y10ajg3zvlw0dawlr3cmvn9j8ibirn53afc8sn2l9fj")))

