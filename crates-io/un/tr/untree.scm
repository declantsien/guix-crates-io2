(define-module (crates-io un tr untree) #:use-module (crates-io))

(define-public crate-untree-0.9.5 (c (n "untree") (v "0.9.5") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)))) (h "1z22v3sb0idfcqpx0fxvvxk9bfkhsjmzpy35ll728dib0b714y8r")))

(define-public crate-untree-0.9.5-better-docs (c (n "untree") (v "0.9.5-better-docs") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)))) (h "1hgxz8mrg6ivbmjljriwqrd8sl51l277gy6kc6s5c58xf40dna8c")))

(define-public crate-untree-0.9.6 (c (n "untree") (v "0.9.6") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)))) (h "05akk01dnk5j9kma4gyxpq67kkmviid75v6w8ki02rxs9g87svsi")))

(define-public crate-untree-0.9.7 (c (n "untree") (v "0.9.7") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)))) (h "0blz3s21xlr3hqhsy8wfj8ximvvlx827vnnb189054pynd82qyd3")))

(define-public crate-untree-0.9.8 (c (n "untree") (v "0.9.8") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)))) (h "1c1gxid96bl34wjbvgp4lvxmddd843yh7c5kaqrf3n5bby04vv96")))

(define-public crate-untree-0.9.9 (c (n "untree") (v "0.9.9") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive" "wrap_help"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "0h18klil8dv1xfmk8fnwy4k9fj02rkvhkjncicr5r9j839lb0pqr") (f (quote (("default" "build-binary") ("build-binary" "clap"))))))

(define-public crate-untree-0.10.0 (c (n "untree") (v "0.10.0") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive" "wrap_help"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "0mr88iny3qlkgvkr9hl30vw526warw3bcs6w8l1rg77y7ww5p3ay") (f (quote (("default" "build-binary") ("build-binary" "clap"))))))

(define-public crate-untree-0.9.10 (c (n "untree") (v "0.9.10") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive" "wrap_help"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1nnzjxgakv87a6n6a21wbcyij681c3rv4xxl0a8dag6lkrxhb1yq") (f (quote (("default" "build-binary") ("build-binary" "clap"))))))

