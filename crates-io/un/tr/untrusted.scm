(define-module (crates-io un tr untrusted) #:use-module (crates-io))

(define-public crate-untrusted-0.1.0 (c (n "untrusted") (v "0.1.0") (h "0cvn91afbjrnjxw6fdlq0pxpg865n9jnb393l09zsgix1lrannvh") (y #t)))

(define-public crate-untrusted-0.2.0 (c (n "untrusted") (v "0.2.0") (h "1g8a2ssn9ys3lk5xd618zgr55745qqdxh4knjc788mib2y4c3dbx") (y #t)))

(define-public crate-untrusted-0.3.0 (c (n "untrusted") (v "0.3.0") (h "1md10kzcy7kkz8l3wixdsplamwp5b18f5ia2as7b8x24rhkv46iy") (y #t)))

(define-public crate-untrusted-0.3.1 (c (n "untrusted") (v "0.3.1") (h "03accgg26mgv00b3b6491ldih8g284ssry5z3xfrf41swzkc16sx") (y #t)))

(define-public crate-untrusted-0.3.2 (c (n "untrusted") (v "0.3.2") (h "1wwy0bk2r0vyfpnjhj406nh4fxmapkammmfyhgcmylg3291zcg8r") (y #t)))

(define-public crate-untrusted-0.5.0 (c (n "untrusted") (v "0.5.0") (h "0wy632kz6h9sdxd1hlnkiqfkdhv54y12pgfnq3csqspgi4wj8rbb") (y #t)))

(define-public crate-untrusted-0.5.1 (c (n "untrusted") (v "0.5.1") (h "1bm7v5da3i1ihzlnfdpwj2zbpdv81mpmybw74qz86n5ykn0xg4pk") (y #t)))

(define-public crate-untrusted-0.6.0 (c (n "untrusted") (v "0.6.0") (h "0acixwjb54vsvl0whczssfwq7wpbb81j7xdhv5m3yicgiy896cbc") (y #t)))

(define-public crate-untrusted-0.6.1 (c (n "untrusted") (v "0.6.1") (h "0r8k4jklhr0xki9avdbbkz0ssnrc4db9pv1r7hxaa8sxihya9bvh") (y #t)))

(define-public crate-untrusted-0.6.2 (c (n "untrusted") (v "0.6.2") (h "0byf88b7ca1kb5aap8f6npp6xncvg95dnma8ipmnmd4n9r5izkam")))

(define-public crate-untrusted-0.7.0-alpha1 (c (n "untrusted") (v "0.7.0-alpha1") (h "0hdadv9wfxq3x4ksmi18m3wfxinhpsal7kn9jvcgapanzb6hl0jd")))

(define-public crate-untrusted-0.7.0 (c (n "untrusted") (v "0.7.0") (h "1kmfykcwif6ashkwg54gcnhxj03kpba2i9vc7z5rpr0xlgvrwdk0")))

(define-public crate-untrusted-0.7.1 (c (n "untrusted") (v "0.7.1") (h "0jkbqaj9d3v5a91pp3wp9mffvng1nhycx6sh4qkdd9qyr62ccmm1")))

(define-public crate-untrusted-0.8.0 (c (n "untrusted") (v "0.8.0") (h "12gf7da7jilyn95ch2njffl7b03pa4b4nswayxxwy87ws58bfa83") (y #t)))

(define-public crate-untrusted-0.9.0 (c (n "untrusted") (v "0.9.0") (h "1ha7ib98vkc538x0z60gfn0fc5whqdd85mb87dvisdcaifi6vjwf")))

