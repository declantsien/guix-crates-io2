(define-module (crates-io un tr untrustended) #:use-module (crates-io))

(define-public crate-untrustended-0.0.1 (c (n "untrustended") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "untrusted") (r "^0.5.0") (d #t) (k 0)))) (h "0gl03nr8ck3xrp4wm4mxdsp4crm131pi6f0ckq6sfg8wzdhl3p2h")))

(define-public crate-untrustended-0.0.2 (c (n "untrustended") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "untrusted") (r "^0.5.0") (d #t) (k 0)))) (h "0m9nababfc7khfy44xicb56yvgyhyxrzxqr0syj23vzr8qmxx98j")))

(define-public crate-untrustended-0.1.0 (c (n "untrustended") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "untrusted") (r "^0.5.0") (d #t) (k 0)))) (h "0w5869la3v7c0h3rz1kr4b4jwz7wkn2mgji9lmdx36gnpyczws44") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-untrustended-0.2.0 (c (n "untrustended") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "untrusted") (r "^0.6.0") (d #t) (k 0)))) (h "0z0k6ic7c9qpshmz1g7mzq8i9kiwh5z723kpk9148q68qf5q8h89") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-untrustended-0.2.1 (c (n "untrustended") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (k 2)) (d (n "untrusted") (r "^0.6.0") (d #t) (k 0)))) (h "1kn64z4z0gp3g4hj0xcjq0qpvjm1j9lpmp43fplbd3gjrpwdlh9a") (f (quote (("use_std") ("i128" "byteorder/i128") ("default" "use_std"))))))

(define-public crate-untrustended-0.3.0 (c (n "untrustended") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("std"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "untrusted") (r "^0.7") (d #t) (k 0)))) (h "0znckw61icmx70r6mx2a0jm9k7348psni57dckywm739ba8hsar2") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-untrustended-0.4.0 (c (n "untrustended") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("std"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1") (k 2)) (d (n "untrusted") (r "^0.9") (d #t) (k 0)))) (h "0p2pkxr168b882d3i97764jd48bhsa6nznmwgsnmdahzax0j72qz") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-untrustended-0.4.1 (c (n "untrustended") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (f (quote ("std"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (k 2)) (d (n "untrusted") (r "^0.9") (d #t) (k 0)))) (h "05qf9c1p30rf5vj3p6mb24q8xqny4vjbisf3i52ba4ybf57lb87q") (f (quote (("use_std") ("default" "use_std"))))))

