(define-module (crates-io un in unindent) #:use-module (crates-io))

(define-public crate-unindent-0.1.0 (c (n "unindent") (v "0.1.0") (h "1zhzcxhlwa9b1f4z93rq1jpzasflp2sgz6xng7akik5sw46bw21m")))

(define-public crate-unindent-0.1.1 (c (n "unindent") (v "0.1.1") (h "06iysmak92clpv59gasncyi1sqlafxqvyzxhc906h0frgprmz4cp")))

(define-public crate-unindent-0.1.2 (c (n "unindent") (v "0.1.2") (h "191k7rshnlxwhlm18h49v4yrd730pfmxcp5kvfrn727sx9i3yx0h")))

(define-public crate-unindent-0.1.3 (c (n "unindent") (v "0.1.3") (h "1x21ilf78aqcq9xzb9b7i628wm10rhk0jp0chlv06rkc690l8jw3")))

(define-public crate-unindent-0.1.4 (c (n "unindent") (v "0.1.4") (h "1mf82l7nk8fjq4c0w3kghbalm1gc5p1l14g0g1r1kvf9j8md7l67")))

(define-public crate-unindent-0.1.5 (c (n "unindent") (v "0.1.5") (h "14s97blyqgf9hzxk22iazrghj60midajkw2801dfspz3n2iqmwb3")))

(define-public crate-unindent-0.1.6 (c (n "unindent") (v "0.1.6") (h "0hl9l4w9mhv5qacx7cirm6rarrphw35b5syw2plx13vz884dfhdg")))

(define-public crate-unindent-0.1.7 (c (n "unindent") (v "0.1.7") (h "1is1gmx1l89z426rn3xsi0mii4vhy2imhqmhx8x2pd8mji6y0kpi")))

(define-public crate-unindent-0.1.8 (c (n "unindent") (v "0.1.8") (h "1n3kpv78282dprvyiakyzy1qhv1qm06dg945jwvxm03kbnjp4iji")))

(define-public crate-unindent-0.1.9 (c (n "unindent") (v "0.1.9") (h "0i1sjxlhw7rbvq47xgk0lixgpnswfyks21ks6zgzfw75lccybzjj")))

(define-public crate-unindent-0.1.10 (c (n "unindent") (v "0.1.10") (h "04pihkk8r4jfcyanjar05plvkjgzkka1lza39ppnradlvri97vjq")))

(define-public crate-unindent-0.1.11 (c (n "unindent") (v "0.1.11") (h "171may3v15wzc10z64i8sahdz49d031v7424mjsifa205ml6sxp1")))

(define-public crate-unindent-0.2.0 (c (n "unindent") (v "0.2.0") (h "04wkgf2gbrm5l31hbsvlpz0l6cqy3mv46k5ffcn2q80k4a21lzn0")))

(define-public crate-unindent-0.2.1 (c (n "unindent") (v "0.2.1") (h "0kw1yivkklw1f5mpcwakxznwzn6br2g3yvbwg7yfvxqzlmg0z8ss")))

(define-public crate-unindent-0.2.2 (c (n "unindent") (v "0.2.2") (h "15bv56p929ffiwxgkzzc5f5gznp9qlzarjyg3mv6ddnhp4qxk1hg")))

(define-public crate-unindent-0.2.3 (c (n "unindent") (v "0.2.3") (h "1km2iy6fr6gsh2wvr1mxz86pm4wrlh3fjkinb35qfi3mw5rpvpn7")))

