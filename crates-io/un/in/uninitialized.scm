(define-module (crates-io un in uninitialized) #:use-module (crates-io))

(define-public crate-uninitialized-0.0.1 (c (n "uninitialized") (v "0.0.1") (h "1w13lh26ww7ib2f4d05lk8pgnma1ccgccsgh0ldxrqqmk0dndz0j") (f (quote (("uninitialized"))))))

(define-public crate-uninitialized-0.0.2 (c (n "uninitialized") (v "0.0.2") (h "10by0nyjl44a4y7y2lgv1h03yycbbwghnvs0932pd0n3252smhbl") (f (quote (("uninitialized"))))))

