(define-module (crates-io un in uninit) #:use-module (crates-io))

(define-public crate-uninit-0.0.1-alpha (c (n "uninit") (v "0.0.1-alpha") (d (list (d (n "require_unsafe_in_body") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "02kczvg0508igmj57k80ak72lxph7h6xfkcmb65rw7115z3r67ap") (f (quote (("polonius-check" "nightly") ("nightly") ("downcast_as_ReadIntoUninit") ("default") ("chain")))) (y #t)))

(define-public crate-uninit-0.0.1-alpha-2 (c (n "uninit") (v "0.0.1-alpha-2") (d (list (d (n "require_unsafe_in_body") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "1x8fs029a1rf09yfpcz73ghh2m3blpnlr2hajjrfxxdp1s440myp") (f (quote (("polonius-check" "nightly") ("nightly") ("downcast_as_ReadIntoUninit") ("default") ("chain")))) (y #t)))

(define-public crate-uninit-0.0.1 (c (n "uninit") (v "0.0.1") (d (list (d (n "require_unsafe_in_body") (r "^0.2.0") (d #t) (k 0)))) (h "0r62z5ivp5vw793mddqb0x9bb7n4771w58villvzf9sqxzzcjl4n") (f (quote (("polonius-check" "nightly") ("nightly") ("downcast_as_ReadIntoUninit") ("default") ("chain")))) (y #t)))

(define-public crate-uninit-0.1.0 (c (n "uninit") (v "0.1.0") (d (list (d (n "require_unsafe_in_body") (r "^0.2.0") (d #t) (k 0)))) (h "04q75v0hnkm0jnqch06z2a52r9pms22z5asmqq9sf56nyjpxcbsf") (f (quote (("polonius-check" "nightly") ("nightly") ("downcast_as_ReadIntoUninit") ("default") ("chain")))) (y #t)))

(define-public crate-uninit-0.2.0-alpha (c (n "uninit") (v "0.2.0-alpha") (h "17lccqk7pk4yhmb2qn3r39av2rnxkxd6pl9bbr0i4i7rnf6xrj89") (f (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.2.0-alpha-2 (c (n "uninit") (v "0.2.0-alpha-2") (h "11p55pbm4m465dbbasv7pvgp5b5kp8yk6i6csy49h6l0lrlvxf16") (f (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.2.0 (c (n "uninit") (v "0.2.0") (h "0dib7ksijmipnlanj2l0c76yisiwfvs64l20hdmsbqx6fvbb5ixk") (f (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.2.1 (c (n "uninit") (v "0.2.1") (h "0fj1ilaqscmcj8bd37dnwp8vgpyc1fxq0g76prkl8xnzw4sf4c4p") (f (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.2.2 (c (n "uninit") (v "0.2.2") (h "0d4gf92v6f2x3va162ra4iz92jfi3dfdzwghs7zlm1i4zw18lb4q") (f (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain")))) (y #t)))

(define-public crate-uninit-0.3.0 (c (n "uninit") (v "0.3.0") (h "0ij1a6s58hixrc3z2f3ml8splagnaidyblpvwkqhbxf4r9xw085c") (f (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.4.0-rc0 (c (n "uninit") (v "0.4.0-rc0") (h "1zw3lbhzb7slainqw60p5w2arphdkwcr856f6hyipmal17linzcl") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain") ("alloc"))))))

(define-public crate-uninit-0.4.0 (c (n "uninit") (v "0.4.0") (h "1gydi7biypdvs0hxjhf5kimcxs2ax34scsgyxz48f81hcbs85qvc") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain") ("alloc"))))))

(define-public crate-uninit-0.4.1-dev (c (n "uninit") (v "0.4.1-dev") (h "15wzqz3kll3vlcn3pj1bjj182bf85kjfffr8lxiyb6lbj2crvq3j") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain") ("alloc"))))))

(define-public crate-uninit-0.5.0-rc1 (c (n "uninit") (v "0.5.0-rc1") (h "00cfp60jks60x2xn8p1rk9jh7p65j5x6l7krwak0zhy2x2in0fsv") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std" "const_generics") ("const_generics") ("chain") ("better-docs" "nightly") ("alloc"))))))

(define-public crate-uninit-0.5.0 (c (n "uninit") (v "0.5.0") (h "0dh5vmr0rxg7rxbmz643p2a5rxvn5kdpkmmhh3chq7hhs19zmddx") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std" "const_generics") ("const_generics") ("chain") ("better-docs" "nightly") ("alloc"))))))

(define-public crate-uninit-0.5.1-rc1 (c (n "uninit") (v "0.5.1-rc1") (d (list (d (n "extension-traits") (r "^1.0.1") (d #t) (k 0)))) (h "01922qc2pd5rl8g2yxdrva67jzmgm961jw8fimh2120m8gvg6w9q") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std" "const_generics") ("const_generics") ("chain") ("better-docs" "nightly") ("alloc"))))))

(define-public crate-uninit-0.5.1 (c (n "uninit") (v "0.5.1") (d (list (d (n "extension-traits") (r "^1.0.1") (d #t) (k 0)))) (h "1dlbv44c8cvgy4mjpz9pbxgjzy97d21rbzy72gndi9bcshp0y4ry") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std" "const_generics") ("const_generics") ("chain") ("better-docs" "nightly") ("alloc"))))))

(define-public crate-uninit-0.6.0 (c (n "uninit") (v "0.6.0") (d (list (d (n "zerocopy") (r ">=0.1, <0.8") (o #t) (k 0)))) (h "1lxhikbwzvvsh87bcmh3gdps19yz1zjyqix2ns1j4yw5bagx76x6") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("chain") ("better-docs" "nightly") ("alloc")))) (r "1.56")))

(define-public crate-uninit-0.6.1 (c (n "uninit") (v "0.6.1") (d (list (d (n "zerocopy") (r ">=0.1, <0.8") (o #t) (k 0)))) (h "1l4z60kx00f7r6dq69p9qfdg9n3g043ckz1dffvbgl2cshljpbz9") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("chain") ("better-docs" "nightly") ("alloc")))) (r "1.56")))

(define-public crate-uninit-0.6.2 (c (n "uninit") (v "0.6.2") (d (list (d (n "zerocopy") (r ">=0.1, <0.8") (o #t) (k 0)))) (h "1jb8040sv4f1aidsyjz65w0xh649m072wzw403ql9yfzpsmdm7rm") (f (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("chain") ("better-docs" "nightly") ("alloc")))) (r "1.56")))

