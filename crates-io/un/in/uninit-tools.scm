(define-module (crates-io un in uninit-tools) #:use-module (crates-io))

(define-public crate-uninit-tools-0.0.1 (c (n "uninit-tools") (v "0.0.1") (d (list (d (n "ioslice_") (r "^0.6.0-alpha.0") (o #t) (d #t) (k 0) (p "ioslice")))) (h "15k1z8yckyhkswzrr65h82nri24ajx5isv5kv0bx47gmfh5i6y3b") (f (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.2 (c (n "uninit-tools") (v "0.0.2") (d (list (d (n "ioslice_") (r "^0.6.0-alpha.0") (o #t) (d #t) (k 0) (p "ioslice")))) (h "1324ka39q4cc737s5q99qsxhgj3nmmays88mp9k81wf6cfq2s10s") (f (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.3 (c (n "uninit-tools") (v "0.0.3") (d (list (d (n "ioslice_") (r "^0.6.0-alpha.0") (o #t) (d #t) (k 0) (p "ioslice")))) (h "15k7rvzpw70jp7pba6pj8sl7m0w7p01slb3h9knddya98s7dpd9a") (f (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.4 (c (n "uninit-tools") (v "0.0.4") (d (list (d (n "ioslice_") (r "^0.6.0-alpha.0") (o #t) (d #t) (k 0) (p "ioslice")))) (h "0bxffch1d1dws42x5rvcr4s7y98wssiyxx4fr9y67fgly27hx96m") (f (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.5 (c (n "uninit-tools") (v "0.0.5") (d (list (d (n "ioslice_") (r "^0.6.0-alpha.0") (o #t) (d #t) (k 0) (p "ioslice")))) (h "0vnyw8nvq8pdzg4rgjkqyjfk6fx0q7ggjhi3smqd3q8pssr3c0br") (f (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.6 (c (n "uninit-tools") (v "0.0.6") (d (list (d (n "ioslice_") (r "^0.6.0-alpha.0") (o #t) (d #t) (k 0) (p "ioslice")))) (h "1gipdb7v88bdr46lzlf8pmkklip6w5gv08nc9i4b3i7sqk35fxh6") (f (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.7 (c (n "uninit-tools") (v "0.0.7") (d (list (d (n "ioslice_") (r "^0.6.0-alpha.2") (o #t) (d #t) (k 0) (p "ioslice")))) (h "0f8n79xqz1q495k6fa5nkji7f781l973a5x0b82l0s03437bfn7f") (f (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

