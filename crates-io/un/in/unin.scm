(define-module (crates-io un in unin) #:use-module (crates-io))

(define-public crate-unin-0.0.0 (c (n "unin") (v "0.0.0") (d (list (d (n "cfg-if") (r "~0.1") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (o #t) (k 0)) (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)))) (h "0nyrrh0ifyvqa0a2l95wa49f0663jsm0fihk885v089sxcrdmh07") (f (quote (("use_serde" "serde") ("std") ("num" "num-traits") ("nightly") ("default" "std"))))))

(define-public crate-unin-0.0.1 (c (n "unin") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "01zqh6n1iv3z7kb3bbvzxbwy6zc9szvvhx42a4rifwzxdn3svq4h") (f (quote (("use_serde" "serde") ("std") ("num" "num-traits") ("nightly") ("default" "std"))))))

