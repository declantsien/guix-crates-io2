(define-module (crates-io un es unescaper) #:use-module (crates-io))

(define-public crate-unescaper-0.1.0 (c (n "unescaper") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0706b0nigp2mikwcfgzwawc6c7rz9md4lcx47q8ly3pj499ndmz4")))

(define-public crate-unescaper-0.1.1 (c (n "unescaper") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zh04xlr8yh2nk4f9fz2kr5zlwzz1w01lin9hl8xnr77blh86m4r")))

(define-public crate-unescaper-0.1.2 (c (n "unescaper") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j533xil3kxq2gwdwy2swc9xd35xn3blylzl199gnnp226p48sm9")))

(define-public crate-unescaper-0.1.3 (c (n "unescaper") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zsvvnkca0jbkbhm3bvanimvlqxqhxmakddq4a5vm5yjb27gdw6q")))

(define-public crate-unescaper-0.1.4 (c (n "unescaper") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d4xi16mindhksi3lqvn0kzzgj5az9qbgxqmz7gwmcxm5v9nmpqa")))

