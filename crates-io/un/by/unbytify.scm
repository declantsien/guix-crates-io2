(define-module (crates-io un by unbytify) #:use-module (crates-io))

(define-public crate-unbytify-0.1.0 (c (n "unbytify") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1m3i3gzl5qpq6siqm504mmgi5yl0bisgbbcxwf68mf05dzh2hbn3")))

(define-public crate-unbytify-0.1.1 (c (n "unbytify") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1s2xn9z388wq2zgwdfvjqrhnpi31y0jqq3w9dz4rn5hngn96769q")))

(define-public crate-unbytify-0.2.0 (c (n "unbytify") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0scqzkqb6krrdx1zc1z85kjkfpsfy7nd7hyc6lfja36n9wsk3x31")))

