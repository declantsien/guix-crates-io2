(define-module (crates-io un by unbytes) #:use-module (crates-io))

(define-public crate-unbytes-0.1.0 (c (n "unbytes") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)))) (h "10h1y674lvwdhfdi43jr3g9614r8v3g1pwzw2387h7jcpfvzs23r") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-unbytes-0.2.0 (c (n "unbytes") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)))) (h "0dw9b6jd74g8nx6ij2xlcd1q4wlwylw3s5w53if0988d1wnng31f") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-unbytes-0.2.1 (c (n "unbytes") (v "0.2.1") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)))) (h "1hsiry68vyvjg3wydsq2qvfky18bwyy9zmzm8zy22ji40fh6ivqy") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-unbytes-0.2.2 (c (n "unbytes") (v "0.2.2") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)))) (h "19q152fjvj3yyr1kn5kxgqwid5accpmlmn7r4f17jj950nlwa3w6") (f (quote (("std") ("default"))))))

(define-public crate-unbytes-0.3.0 (c (n "unbytes") (v "0.3.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)))) (h "0qzbr684h6b7lpq1vikkwbvg5f6ri84k8009qakj35k56rbv0sf5") (f (quote (("std") ("default"))))))

