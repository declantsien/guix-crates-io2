(define-module (crates-io un ip uniparc_xml_parser) #:use-module (crates-io))

(define-public crate-uniparc_xml_parser-0.1.0 (c (n "uniparc_xml_parser") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1j5p2n688mfm2ckjsm1y48kxy9yad0anp8711s3nn2222xlskd87")))

(define-public crate-uniparc_xml_parser-0.2.0 (c (n "uniparc_xml_parser") (v "0.2.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "11y391lqxrri0rjak6mgax0ijpykklfhqf3pqkc0q3ljgpgjdhig")))

(define-public crate-uniparc_xml_parser-0.2.1 (c (n "uniparc_xml_parser") (v "0.2.1") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "05xm3bdyn1npkry5g5g9ayzka33sxdawnc6630p8mlnw68c0zw22")))

