(define-module (crates-io un ic unicorn) #:use-module (crates-io))

(define-public crate-unicorn-0.1.0 (c (n "unicorn") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0ahsdrmd2m0nx2q72cxlkiy2lw5ma0ppcn4nl2x1y98rjc885gbw")))

(define-public crate-unicorn-0.2.0 (c (n "unicorn") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1gg6721v26si7c303pyw0acvbkp85cj0vdy4cwq9ck39v3c9qafq")))

(define-public crate-unicorn-0.3.0 (c (n "unicorn") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "166hjczyih4l5cr6dqhw1i9ij41mz7fx9hpkdj0p2x8v1svi580m")))

(define-public crate-unicorn-0.4.0 (c (n "unicorn") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "libunicorn-sys") (r "^0.4.0") (d #t) (k 0)))) (h "08qcan96l1hycf7gczyha8pd2yclwrqa10h6bl5zc5s40hzkrz3s") (y #t)))

(define-public crate-unicorn-0.4.1 (c (n "unicorn") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "libunicorn-sys") (r "^0.4.0") (d #t) (k 0)))) (h "07wi0zl31rb3l4ska1vcs60512gaapqllj516rcz8da6yhg23k0k")))

(define-public crate-unicorn-0.6.0 (c (n "unicorn") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "libunicorn-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1nbm0p0cs8gqsn4zp1lchdsk5g82lcsw8jqcfllr4qll2vxs8by4")))

(define-public crate-unicorn-0.7.0 (c (n "unicorn") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libunicorn-sys") (r "^0.7.0") (d #t) (k 0)))) (h "16153fdx0jwwqvkqipfai9cqwxm18jks7gi9gds9k8lss7fd7h3q")))

(define-public crate-unicorn-0.8.0 (c (n "unicorn") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libunicorn-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0n1y4cv7nrkrzbvspk5fs8jy1ilzfsm4rm2rg5j20kmzm39aai0b")))

(define-public crate-unicorn-0.9.0 (c (n "unicorn") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libunicorn-sys") (r "^0.9.0") (d #t) (k 0)))) (h "1bh67467bvy82zpsdsrk0wk552fpfinih0z0vg6ag42hpnck9i1f")))

(define-public crate-unicorn-0.9.1 (c (n "unicorn") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libunicorn-sys") (r "^0.9.1") (d #t) (k 0)))) (h "05hfmy7kvibrpqgypmf84dy261a2gbym48hys5wqzdhhwv9mkg1z")))

