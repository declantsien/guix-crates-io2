(define-module (crates-io un ic unic-char-range) #:use-module (crates-io))

(define-public crate-unic-char-range-0.6.0 (c (n "unic-char-range") (v "0.6.0") (h "0fjga1f5nf2bvpg7asqrai9fgbclb29kpc1q92hk5qa0xjskp73x") (f (quote (("unstable" "exact-size-is-empty" "fused" "trusted-len") ("trusted-len") ("fused") ("exact-size-is-empty") ("default"))))))

(define-public crate-unic-char-range-0.7.0 (c (n "unic-char-range") (v "0.7.0") (h "1s4am9xij8h1kbl30429bzhxdd4wys8zhfy0mxnb5l9ankx8bayr") (f (quote (("unstable" "exact-size-is-empty" "fused" "trusted-len") ("trusted-len") ("std") ("fused") ("exact-size-is-empty") ("default"))))))

(define-public crate-unic-char-range-0.8.0 (c (n "unic-char-range") (v "0.8.0") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10n2c1wlzck102p452wzr9dh6bjhdxczvz19ksbpiajw8x9blckj") (f (quote (("unstable" "exact-size-is-empty" "fused" "trusted-len") ("trusted-len") ("std") ("fused") ("exact-size-is-empty") ("default"))))))

(define-public crate-unic-char-range-0.9.0 (c (n "unic-char-range") (v "0.9.0") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1g0z7iwvjhqspi6194zsff8vy6i3921hpqcrp3v1813hbwnh5603") (f (quote (("unstable" "exact-size-is-empty" "fused" "trusted-len") ("trusted-len") ("std") ("fused") ("exact-size-is-empty") ("default"))))))

