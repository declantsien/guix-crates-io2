(define-module (crates-io un ic uniconv) #:use-module (crates-io))

(define-public crate-uniconv-0.1.1 (c (n "uniconv") (v "0.1.1") (d (list (d (n "mkproj-lib") (r "^0.1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0ldxcrw43f1256pvmiqpaaim9ks0ahjcqkja9ih82wmdsbkgvjlx") (y #t)))

(define-public crate-uniconv-0.1.2 (c (n "uniconv") (v "0.1.2") (d (list (d (n "mkproj-lib") (r "^0.1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "08ng3i2r7wbn0qyvy37srwfiw2k91k3g3s0vzy474l3h2v54l869") (y #t)))

(define-public crate-uniconv-1.1.0 (c (n "uniconv") (v "1.1.0") (d (list (d (n "mkproj-lib") (r "^0.2.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "15x6hz9hcycm2wj0fcbjg0jzc70hwp2ck3h8d6qbc0lxlm5f2dk8") (y #t)))

(define-public crate-uniconv-1.1.1 (c (n "uniconv") (v "1.1.1") (d (list (d (n "mkproj-lib") (r "^0.2.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "19lz4plgdlxw7083l5ikryx1zks4vd1dq3fk4snbimbya8a147sv") (y #t)))

