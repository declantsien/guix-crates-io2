(define-module (crates-io un ic unicode_names2) #:use-module (crates-io))

(define-public crate-unicode_names2-0.2.0 (c (n "unicode_names2") (v "0.2.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)))) (h "060al2v12hnidx9ajpazhb5kyjifxhph1iphbcm6jca8a47260b4") (f (quote (("unstable") ("no_std") ("default")))) (y #t)))

(define-public crate-unicode_names2-0.2.1 (c (n "unicode_names2") (v "0.2.1") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode_names2_macros") (r "^0.2.0") (d #t) (k 2)))) (h "0yjynvws0kjwdbg298klf03vj17yjan93nqvqcin33s6x57c1jrr") (f (quote (("unstable") ("no_std") ("default"))))))

(define-public crate-unicode_names2-0.2.2 (c (n "unicode_names2") (v "0.2.2") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode_names2_macros") (r "^0.2.0") (d #t) (k 2)))) (h "1ybxa0yjw4vkcpm3gycyyzcvqqms62yldhwc93wh0wld1kdcbihd") (f (quote (("unstable") ("no_std") ("default"))))))

(define-public crate-unicode_names2-0.3.0 (c (n "unicode_names2") (v "0.3.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode_names2_macros") (r "^0.2") (d #t) (k 2)))) (h "0y40303h4w5a2w01kkxghvfpjf22fb7aqrn91953v1zzfsw2iad7") (f (quote (("unstable") ("no_std") ("default"))))))

(define-public crate-unicode_names2-0.4.0 (c (n "unicode_names2") (v "0.4.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode_names2_macros") (r ">= 0.3, < 0.5") (d #t) (k 2)))) (h "1wcckqdyfpmp74gp0zdx9bc1ybl0sgs9vcfls2x4lf8ng66ngml7") (f (quote (("unstable") ("no_std") ("default"))))))

(define-public crate-unicode_names2-0.5.0 (c (n "unicode_names2") (v "0.5.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode_names2_macros") (r ">=0.3, <0.6") (d #t) (k 2)))) (h "1p84cjw5ywfwga7x867x9jwkf6rndpqbhixwvrrckrb5lc3yij7f") (f (quote (("unstable") ("no_std") ("default"))))))

(define-public crate-unicode_names2-0.5.1 (c (n "unicode_names2") (v "0.5.1") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode_names2_macros") (r ">=0.3, <0.6") (d #t) (k 2)))) (h "009k1zp43pc127sxi516rqxhyfl522i8zzq42y8zrkiqhb6g9782") (f (quote (("unstable") ("no_std") ("default"))))))

(define-public crate-unicode_names2-0.6.0 (c (n "unicode_names2") (v "0.6.0") (d (list (d (n "rand") (r "^0.5.1") (d #t) (k 2)) (d (n "unicode_names2_macros") (r ">=0.3, <0.7") (d #t) (k 2)))) (h "0nf0dhyg0xs68qgsl1hpiij5cwbic089h2kz91wlfq22vp39cv24") (f (quote (("unstable") ("no_std") ("default"))))))

(define-public crate-unicode_names2-1.0.0 (c (n "unicode_names2") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "unicode_names2_generator") (r "^1.0.0") (d #t) (k 1)) (d (n "unicode_names2_macros") (r ">=0.3, <2.0") (d #t) (k 2)))) (h "16dlbs0sgyrbk3364syqgqcky2jznrnrf981if3bk395lp6p5m77") (f (quote (("unstable") ("no_std") ("default")))) (r "1.63.0")))

(define-public crate-unicode_names2-1.1.0 (c (n "unicode_names2") (v "1.1.0") (d (list (d (n "phf") (r "^0.11.1") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "unicode_names2_generator") (r "^1.1.0") (d #t) (k 1)) (d (n "unicode_names2_macros") (r ">=0.3, <2.0") (d #t) (k 2)))) (h "0y9mijllikgxvdnf2s0fscm5j2bczizcx84rz6bigbhr4sac1ciq") (f (quote (("unstable") ("no_std") ("default")))) (r "1.63.0")))

(define-public crate-unicode_names2-1.2.0 (c (n "unicode_names2") (v "1.2.0") (d (list (d (n "phf") (r "^0.11.1") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "unicode_names2_generator") (r "^1.2.0") (d #t) (k 1)) (d (n "unicode_names2_macros") (r ">=0.3, <2.0") (d #t) (k 2)))) (h "0prs3w163al1rfj764phh7cwrhinymggqllf8vgwn71w5jp0cmax") (f (quote (("unstable") ("no_std") ("default")))) (r "1.63.0")))

(define-public crate-unicode_names2-1.2.1 (c (n "unicode_names2") (v "1.2.1") (d (list (d (n "phf") (r "^0.11.1") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "unicode_names2_generator") (r "^1.2.1") (d #t) (k 1)) (d (n "unicode_names2_macros") (r ">=0.3, <2.0") (d #t) (k 2)))) (h "04bj172jxg8pxfwvdz6majq6w1jpn1q4lfc3hbx9vikd04pyyr5c") (f (quote (("unstable") ("no_std") ("default")))) (r "1.63.0")))

(define-public crate-unicode_names2-1.2.2 (c (n "unicode_names2") (v "1.2.2") (d (list (d (n "phf") (r "^0.11.1") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "unicode_names2_generator") (r "^1.2.2") (d #t) (k 1)) (d (n "unicode_names2_macros") (r ">=0.3, <2.0") (d #t) (k 2)))) (h "0xvnf1zpaqmbmw4bzcrjrjcymg5vgsr9ywjg2shj4yfzjkrfppmd") (f (quote (("unstable") ("no_std") ("generator-timing" "unicode_names2_generator/timing") ("default")))) (r "1.63.0")))

