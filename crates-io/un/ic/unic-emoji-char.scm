(define-module (crates-io un ic unic-emoji-char) #:use-module (crates-io))

(define-public crate-unic-emoji-char-0.7.0 (c (n "unic-emoji-char") (v "0.7.0") (d (list (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "0j4i3wa390a972bq56chvn8jgq3f68dqpmfc4av4rqxrwfpiqs3q")))

(define-public crate-unic-emoji-char-0.8.0 (c (n "unic-emoji-char") (v "0.8.0") (d (list (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "140zzl2kavnvh1ihvkbm7krq765c5kd151wp4gnr9y6bcv0pc132")))

(define-public crate-unic-emoji-char-0.9.0 (c (n "unic-emoji-char") (v "0.9.0") (d (list (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "0ka9fr7s6lv0z43r9xphg9injn35pfxf9g9q18ki0wl9d0g241qb")))

