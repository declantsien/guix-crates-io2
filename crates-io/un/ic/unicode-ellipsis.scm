(define-module (crates-io un ic unicode-ellipsis) #:use-module (crates-io))

(define-public crate-unicode-ellipsis-0.0.0 (c (n "unicode-ellipsis") (v "0.0.0") (h "0cypbiiv9i0fhljp49vpcxdjnk110dn481bwzl99xnkg3aln0n8s")))

(define-public crate-unicode-ellipsis-0.1.0 (c (n "unicode-ellipsis") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.12") (d #t) (k 0)))) (h "0w68kfdz0f4xj8rlr4b68di23fljnm3f61xc049qmhz1jsjwwh3q")))

(define-public crate-unicode-ellipsis-0.1.1 (c (n "unicode-ellipsis") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.12") (d #t) (k 0)))) (h "1xhabanrdnalgnkw0pqihg8m0ddyc2j3in59i6dva5s81kh6mygq")))

(define-public crate-unicode-ellipsis-0.1.2 (c (n "unicode-ellipsis") (v "0.1.2") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.12") (d #t) (k 0)))) (h "0xnf0hvrmwfahl6clj04fwxnxidh0bxl0zyxc9d9151jikxrn20b")))

(define-public crate-unicode-ellipsis-0.1.3 (c (n "unicode-ellipsis") (v "0.1.3") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.12") (d #t) (k 0)))) (h "0n6lka84hfzcsh4bws0aydcsb4ygkihiq1vrk7jhykvmnv8prq55")))

(define-public crate-unicode-ellipsis-0.1.4 (c (n "unicode-ellipsis") (v "0.1.4") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.12") (d #t) (k 0)))) (h "0hy9i13ns4afcp1p6xbb7s37qfhlipbg70f3cvgs9hkq4xnfj1bc")))

(define-public crate-unicode-ellipsis-0.2.0 (c (n "unicode-ellipsis") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.12") (d #t) (k 0)))) (h "1zsdzmy6x1p8s35rgfmc7nx1qcs6j4bcfbfyiimrdngyqfwbajlj")))

