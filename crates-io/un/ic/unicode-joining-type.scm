(define-module (crates-io un ic unicode-joining-type) #:use-module (crates-io))

(define-public crate-unicode-joining-type-0.1.0 (c (n "unicode-joining-type") (v "0.1.0") (h "06vs8hasfsx8xp92j9l8rsih14cfj1z9247imc2gdakvl9hbqc7a")))

(define-public crate-unicode-joining-type-0.2.0 (c (n "unicode-joining-type") (v "0.2.0") (h "189q25fwys38ixvgfl2skx3dg3l845jgi42qq4iz7q3bz2wg32vh")))

(define-public crate-unicode-joining-type-0.3.0 (c (n "unicode-joining-type") (v "0.3.0") (h "1sz17rjskq7m7fnlhcq73zrbzcigyv75w7vcxbjwniidrcnkshh2")))

(define-public crate-unicode-joining-type-0.3.1 (c (n "unicode-joining-type") (v "0.3.1") (h "17jrbgnwj62n3qav1hzp5a9alykwprk1ch584kkz2xghsw8w0cxa")))

(define-public crate-unicode-joining-type-0.4.0 (c (n "unicode-joining-type") (v "0.4.0") (h "1r4p2kshxpd41cwg1fg57ms1y10x53s4i6x3wmldnpl6wkyd55l8")))

(define-public crate-unicode-joining-type-0.5.0 (c (n "unicode-joining-type") (v "0.5.0") (h "1npb7cil49040h3wi8ndzr4qs8rm1vdpyvqsj74jf52hvb39nrvz")))

(define-public crate-unicode-joining-type-0.6.0 (c (n "unicode-joining-type") (v "0.6.0") (h "03d5kwj0cs7xygbkr6njqm32yn4bich6s6zwr6gdk1msb969n1jf")))

(define-public crate-unicode-joining-type-0.7.0 (c (n "unicode-joining-type") (v "0.7.0") (h "1xgn8cjv3r3s9f3zqs3v2k6hv8d43l3z6nkm1047bg5qri3wpy12")))

