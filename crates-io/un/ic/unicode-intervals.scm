(define-module (crates-io un ic unicode-intervals) #:use-module (crates-io))

(define-public crate-unicode-intervals-0.1.0 (c (n "unicode-intervals") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "0i4xqb4wm3zxfkbhcla67hj0rsj84k3q92a1rnlnz01kcimyr4rv") (f (quote (("__benchmark_internals"))))))

(define-public crate-unicode-intervals-0.1.1 (c (n "unicode-intervals") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "0h7fl7gxr2zizbnlj74nprnwvl4ymv78fyh4ygy8xxjjp35071bi") (f (quote (("__benchmark_internals"))))))

(define-public crate-unicode-intervals-0.1.2 (c (n "unicode-intervals") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "0ik4mp4gyfx2scvgl5hqdijrpcgjz5037n7fiwmpkgmfxprv3cqh") (f (quote (("__benchmark_internals"))))))

(define-public crate-unicode-intervals-0.2.0 (c (n "unicode-intervals") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "test-case") (r "^3.1") (d #t) (k 2)))) (h "1f3gsmp0s7bdrffsdb92fvw8arcdv2b58sh1crh5sxsrjymajgpd") (f (quote (("__benchmark_internals"))))))

