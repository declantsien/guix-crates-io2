(define-module (crates-io un ic unicode_skeleton) #:use-module (crates-io))

(define-public crate-unicode_skeleton-0.1.0 (c (n "unicode_skeleton") (v "0.1.0") (d (list (d (n "unicode-normalization") (r "^0.1.5") (d #t) (k 0)))) (h "1p665qk6gbd5yyfrlrxc6qgxkqq74sp17kp37lg5n31yqp2gjrrs")))

(define-public crate-unicode_skeleton-0.1.1 (c (n "unicode_skeleton") (v "0.1.1") (d (list (d (n "unicode-normalization") (r "^0.1.5") (d #t) (k 0)))) (h "04pll21chlyxhy114b871xj3fcb6slndp0xz3wykk3885kdp9gb6")))

