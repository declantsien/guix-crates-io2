(define-module (crates-io un ic unic-char-basics) #:use-module (crates-io))

(define-public crate-unic-char-basics-0.8.0 (c (n "unic-char-basics") (v "0.8.0") (d (list (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 2)))) (h "0pnjw8fw3j00pkfj0qgly96zj0a6i1ib981smd7pnl4izsl2gm4c")))

(define-public crate-unic-char-basics-0.9.0 (c (n "unic-char-basics") (v "0.9.0") (d (list (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 2)))) (h "0jvcl0gqg9glmwvs9wgy5kznb5853lzb3hd04n931533phwx5r90")))

