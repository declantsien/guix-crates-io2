(define-module (crates-io un ic unic-ucd-ident) #:use-module (crates-io))

(define-public crate-unic-ucd-ident-0.7.0 (c (n "unic-ucd-ident") (v "0.7.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.7.0") (d #t) (k 2)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "1xjq4zaqmdwmg900cpz6adn5n7m3akfabaiz0i3c28k8zikp06ay") (f (quote (("xid") ("pattern") ("id") ("default" "xid"))))))

(define-public crate-unic-ucd-ident-0.8.0 (c (n "unic-ucd-ident") (v "0.8.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.8.0") (d #t) (k 2)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "0698bnam82mjp5yi9g7czg0rinlpviw9kkh5qk86b6vd8p8d7g02") (f (quote (("xid") ("pattern") ("id") ("default" "xid"))))))

(define-public crate-unic-ucd-ident-0.9.0 (c (n "unic-ucd-ident") (v "0.9.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 2)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "11v9mfyl9rqnvd9a8hgmbgnzyxd3lcx0dkv7klhskjl10dya6c72") (f (quote (("xid") ("pattern") ("id") ("default" "xid"))))))

