(define-module (crates-io un ic unicode-properties) #:use-module (crates-io))

(define-public crate-unicode-properties-0.1.0 (c (n "unicode-properties") (v "0.1.0") (h "1w12zpvpabl37sq9nsbnp76r9z7lg0f81l63af4a3fpv465iryf7") (f (quote (("general-category") ("emoji") ("default" "general-category" "emoji"))))))

(define-public crate-unicode-properties-0.1.1 (c (n "unicode-properties") (v "0.1.1") (h "14cjbmfs64qw1m4qzpfa673a8rpyhp5h9f412mkg1n958jfrs9g4") (f (quote (("general-category") ("emoji") ("default" "general-category" "emoji"))))))

