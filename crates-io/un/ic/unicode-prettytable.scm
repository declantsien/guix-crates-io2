(define-module (crates-io un ic unicode-prettytable) #:use-module (crates-io))

(define-public crate-unicode-prettytable-0.2.0 (c (n "unicode-prettytable") (v "0.2.0") (h "0grs7ag30rq2xpzwyifw4xmb7j9qkz117z94bkfq38n7rw1dvp1q")))

(define-public crate-unicode-prettytable-1.0.0 (c (n "unicode-prettytable") (v "1.0.0") (d (list (d (n "derive_builder") (r "~0.9.0") (d #t) (k 0)))) (h "1n24xykkfqxkjh1rhpnb0qqh8wiksygn6za7wjxdxzk5gninhg35") (y #t)))

(define-public crate-unicode-prettytable-0.3.0 (c (n "unicode-prettytable") (v "0.3.0") (d (list (d (n "derive_builder") (r "~0.9.0") (d #t) (k 0)))) (h "059xzw062f8xz9kp4si8i2kfycw6drki95cf4blannv3g1bdb7x6")))

(define-public crate-unicode-prettytable-0.3.1 (c (n "unicode-prettytable") (v "0.3.1") (d (list (d (n "derive_builder") (r "~0.9.0") (d #t) (k 0)) (d (n "smart-default") (r "~0.6.0") (d #t) (k 0)))) (h "0b212ahlr3lg4l12b86k46y630q4ky2p7jamib4igrnp7767wrsm")))

