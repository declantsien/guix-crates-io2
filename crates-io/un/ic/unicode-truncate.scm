(define-module (crates-io un ic unicode-truncate) #:use-module (crates-io))

(define-public crate-unicode-truncate-0.1.0 (c (n "unicode-truncate") (v "0.1.0") (d (list (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "141xbs4j096c3j68pkl2jlx8nhg27rsnd5cm4znh9jajnaqjn3wn") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-truncate-0.1.1 (c (n "unicode-truncate") (v "0.1.1") (d (list (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1svvdbhyxsvbmi8yk167h325s6j0n8cs97ahc0p2r7qg3cq25j0s") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-truncate-0.2.0 (c (n "unicode-truncate") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0hlf6k1d7s1qqdvhxwh4xl8wd1wvqm0vr0pazxq74jksbz5fajx0") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-truncate-1.0.0 (c (n "unicode-truncate") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "09lj06lqj408gzs0509aivy5lywjm7yvwkbiiihjqdmyvazblpss") (f (quote (("std") ("default" "std")))) (r "1.63")))

