(define-module (crates-io un ic unicode-ident) #:use-module (crates-io))

(define-public crate-unicode-ident-0.0.0 (c (n "unicode-ident") (v "0.0.0") (h "1bwd3mn3r6fzbgnwlpr60bsalzarw0iwfp56d9hhsajcv4ha8gap") (y #t)))

(define-public crate-unicode-ident-1.0.0 (c (n "unicode-ident") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.9") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 2)))) (h "1vlksh7rxnkakdc5qiwxix6fng9a5cw9v8dfnkf5xsx1zdlg0anj") (r "1.31")))

(define-public crate-unicode-ident-1.0.1 (c (n "unicode-ident") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.9") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 2)))) (h "131niycgp77aiwvgjdyh47389xfnb7fmlc8ybrxys8v0a0kgxljv") (r "1.31")))

(define-public crate-unicode-ident-1.0.2 (c (n "unicode-ident") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.9") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 2)))) (h "19zf5lzhzix2s35lp5lckdy90sw0kfi5a0ii49d24dcj7yk1pihm") (r "1.31")))

(define-public crate-unicode-ident-1.0.3 (c (n "unicode-ident") (v "1.0.3") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.9") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 2)))) (h "1bqswc96ws8l6k7xx56dg521a3l5imi3mhlcz7rsi6a92mxb7xf4") (r "1.31")))

(define-public crate-unicode-ident-1.0.4 (c (n "unicode-ident") (v "1.0.4") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 2)))) (h "1pf59d521libxsf73cd9px9vc345qirphc0i9zw65b3683f13j6w") (r "1.31")))

(define-public crate-unicode-ident-1.0.5 (c (n "unicode-ident") (v "1.0.5") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 2)))) (h "1wznr6ax3jl09vxkvj4a62vip2avfgif13js9sflkjg4b6fv7skc") (r "1.31")))

(define-public crate-unicode-ident-1.0.6 (c (n "unicode-ident") (v "1.0.6") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 2)))) (h "1g2fdsw5sv9l1m73whm99za3lxq3nw4gzx5kvi562h4b46gjp8l4") (r "1.31")))

(define-public crate-unicode-ident-1.0.7 (c (n "unicode-ident") (v "1.0.7") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 2)))) (h "1dzskr2az57d1kfnzy5sx9aa1ngvhm2vj5l86yy9gz6sds812p3p") (r "1.31")))

(define-public crate-unicode-ident-1.0.8 (c (n "unicode-ident") (v "1.0.8") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 2)))) (h "1x4v4v95fv9gn5zbpm23sa9awjvmclap1wh1lmikmw9rna3llip5") (r "1.31")))

(define-public crate-unicode-ident-1.0.9 (c (n "unicode-ident") (v "1.0.9") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 2)))) (h "180zwpsxxf8kw14609yy3h80j9dd9drffcw62y4vhps1yb512n5i") (r "1.31")))

(define-public crate-unicode-ident-1.0.10 (c (n "unicode-ident") (v "1.0.10") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 2)))) (h "0wvfzc0m3a50xikzfzwj8nh6ls0njngl7z60d2hli1x6yhcrl112") (r "1.31")))

(define-public crate-unicode-ident-1.0.11 (c (n "unicode-ident") (v "1.0.11") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 2)))) (h "0g7wmn39nl9yzhjwn9ihacd22ymli8r4nlc2xf3idaas8ypbl6ih") (r "1.31")))

(define-public crate-unicode-ident-1.0.12 (c (n "unicode-ident") (v "1.0.12") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 2)))) (h "0jzf1znfpb2gx8nr8mvmyqs1crnv79l57nxnbiszc7xf7ynbjm1k") (r "1.31")))

