(define-module (crates-io un ic unicode-id-start) #:use-module (crates-io))

(define-public crate-unicode-id-start-1.0.0 (c (n "unicode-id-start") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.9") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-id") (r "^0.3") (d #t) (k 2)))) (h "0sz0iv8xjia0fi2jj959a9rbfb3i8503g21hhsx7wkqndpwvcc3n") (r "1.31")))

(define-public crate-unicode-id-start-1.0.2 (c (n "unicode-id-start") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.9") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-id") (r "^0.3") (d #t) (k 2)))) (h "1rwmkldfgic2bizb7ag3l90p9a36k0fhhk31kbsq6bz69i79pppk") (r "1.31")))

(define-public crate-unicode-id-start-1.0.3 (c (n "unicode-id-start") (v "1.0.3") (d (list (d (n "criterion") (r "^0.4.0") (k 2)) (d (n "fst") (r "^0.4.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10.1") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1.5") (k 2)) (d (n "unicode-id") (r "^0.3.3") (d #t) (k 2)))) (h "0q5ifr7dsqnfwqdnr6qhb4cgfgny5p8qdpi5iym7k10j09bkv2i3") (r "1.31")))

(define-public crate-unicode-id-start-1.1.0 (c (n "unicode-id-start") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-id") (r "^0.3.3") (d #t) (k 2)))) (h "1s5w9js64yhq8j67dp8mc0hpnjgbic16wli58pb367y09ap9hmg1") (r "1.31")))

(define-public crate-unicode-id-start-1.1.1 (c (n "unicode-id-start") (v "1.1.1") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-id") (r "^0.3.3") (d #t) (k 2)))) (h "0j0yr13blg01mcfwkdkb7x5a5zklpw42mahshfp43glmrc8d3vq0") (r "1.31")))

(define-public crate-unicode-id-start-1.1.2 (c (n "unicode-id-start") (v "1.1.2") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-id") (r "^0.3.4") (d #t) (k 2)))) (h "1npmn4d0mmyyjixwfp3h94q02nw7yalarwinvc989d9w6d833xxq") (r "1.31")))

(define-public crate-unicode-id-start-1.0.4 (c (n "unicode-id-start") (v "1.0.4") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)) (d (n "ucd-trie") (r "^0.1") (k 2)) (d (n "unicode-id") (r "=0.3.2") (d #t) (k 2)))) (h "1hfgfwa6iqdw447g3lln8dvgxf9nw5yjr4ljvpzvpjzcjjkbzbh2") (r "1.31")))

