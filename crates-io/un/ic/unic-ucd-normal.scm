(define-module (crates-io un ic unic-ucd-normal) #:use-module (crates-io))

(define-public crate-unic-ucd-normal-0.1.0 (c (n "unic-ucd-normal") (v "0.1.0") (d (list (d (n "unic-ucd-core") (r "^0.1") (d #t) (k 0)))) (h "0lrx4r67pd4lax7kvl2zv06zszl8xbz8ji7cbvlv3hxk7rdfx89n") (f (quote (("unstable") ("default"))))))

(define-public crate-unic-ucd-normal-0.1.1 (c (n "unic-ucd-normal") (v "0.1.1") (d (list (d (n "unic-ucd-core") (r "^0.1.1") (d #t) (k 0)))) (h "0wdddyyqfrn2526p26crbbac4xrmispx309znwbj5bl5fr3ca2a4")))

(define-public crate-unic-ucd-normal-0.1.2 (c (n "unic-ucd-normal") (v "0.1.2") (d (list (d (n "unic-ucd-core") (r "^0.1.0") (d #t) (k 0)))) (h "14akad381hvg39908bwfz257sxrvr1r87ng84i5vbib8li4g9917")))

(define-public crate-unic-ucd-normal-0.2.0 (c (n "unic-ucd-normal") (v "0.2.0") (d (list (d (n "unic-ucd-core") (r "^0.2.0") (d #t) (k 0)))) (h "1cndng7221j7y4bb3w2x88qlzs59swd139mam5wsv8hx0im38vx5")))

(define-public crate-unic-ucd-normal-0.4.0 (c (n "unic-ucd-normal") (v "0.4.0") (d (list (d (n "unic-ucd-core") (r "^0.4.0") (d #t) (k 0)))) (h "0m0aa76adyjlh5yh33s38fhfgk6pvn5cp60b4gbil7nj531nd6m1")))

(define-public crate-unic-ucd-normal-0.5.0 (c (n "unic-ucd-normal") (v "0.5.0") (d (list (d (n "unic-ucd-category") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.5.0") (d #t) (k 2)) (d (n "unic-ucd-core") (r "^0.5.0") (d #t) (k 0)) (d (n "unic-utils") (r "^0.5.0") (d #t) (k 2)))) (h "14hzi72hgawya2sanvkwpn0pj1mzw18v79c6axbaxi1rh8m4z38g") (f (quote (("default"))))))

(define-public crate-unic-ucd-normal-0.6.0 (c (n "unic-ucd-normal") (v "0.6.0") (d (list (d (n "unic-char-property") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.6.0") (d #t) (k 2)) (d (n "unic-ucd-core") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-utils") (r "^0.6.0") (d #t) (k 0)))) (h "1zlc5y02sabq897g0pxlipwfa7yvrpkvj5cqhba0wvw639lnankw") (f (quote (("default"))))))

(define-public crate-unic-ucd-normal-0.7.0 (c (n "unic-ucd-normal") (v "0.7.0") (d (list (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.7.0") (d #t) (k 2)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "0zh3rsxv74xcxdbbfxvdm0ia2v9dnsydxjzdpc0xpl3vqlfk55x3") (f (quote (("default"))))))

(define-public crate-unic-ucd-normal-0.8.0 (c (n "unic-ucd-normal") (v "0.8.0") (d (list (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.8.0") (d #t) (k 2)) (d (n "unic-ucd-hangul") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "0c9p643ci7ixg608f2xc1clab4zxqnc52h6wdrkzfaq9zma5azag") (f (quote (("default"))))))

(define-public crate-unic-ucd-normal-0.9.0 (c (n "unic-ucd-normal") (v "0.9.0") (d (list (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 2)) (d (n "unic-ucd-hangul") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "044laqqf09xqv4gl27f328a2f780gkzabpar72qj4b90p1rxibl6") (f (quote (("default"))))))

