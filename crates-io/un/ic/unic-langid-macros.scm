(define-module (crates-io un ic unic-langid-macros) #:use-module (crates-io))

(define-public crate-unic-langid-macros-0.1.0 (c (n "unic-langid-macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.4") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.1") (d #t) (k 0)))) (h "152s3jir7qsragcxcrhaqrpj6ll95zhlz9ilb3q8qc7ylwri3266")))

(define-public crate-unic-langid-macros-0.2.0 (c (n "unic-langid-macros") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.4") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.2") (d #t) (k 0)))) (h "1xw2ygrwcyqn9jnpxi20d8zrs5sw4yaginbi4v7lvnfr48rc5ip3")))

(define-public crate-unic-langid-macros-0.3.0 (c (n "unic-langid-macros") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.4") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.3") (d #t) (k 0)))) (h "04a4mizkl0lxrahw84hhxxv2cyg520m0mr5x0mzvf237v93fyzhr")))

(define-public crate-unic-langid-macros-0.5.0 (c (n "unic-langid-macros") (v "0.5.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.5") (d #t) (k 0)))) (h "1hk0rqj80xfsqc00rb0qpbmiqhj7w22rf2vsccivd53w1lhs13hx")))

(define-public crate-unic-langid-macros-0.6.0 (c (n "unic-langid-macros") (v "0.6.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.6") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.6") (d #t) (k 0)))) (h "07w50kq0sljcl6j62k0h5rxd7gql870brqyd86is7hvbr1fqjmp8")))

(define-public crate-unic-langid-macros-0.7.0 (c (n "unic-langid-macros") (v "0.7.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.7") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.7") (d #t) (k 0)))) (h "0lbii6cjldra0bczp037chs0ha430ppxvh1gdn988gj3p7ssi95i")))

(define-public crate-unic-langid-macros-0.8.0 (c (n "unic-langid-macros") (v "0.8.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.8") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.8") (d #t) (k 0)))) (h "1pq3ax19g82bcswagdksp2zpjgwf21rl0jpdwdblsqvq29wr1ga9")))

(define-public crate-unic-langid-macros-0.9.0 (c (n "unic-langid-macros") (v "0.9.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.9") (d #t) (k 0)))) (h "1rfn9pvbypvxyr8iyfx6dycafnn9ih9v8r3dhgr0b23yv3b81y8q")))

(define-public crate-unic-langid-macros-0.9.1 (c (n "unic-langid-macros") (v "0.9.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.9") (d #t) (k 0)))) (h "1zn4pm72n7w0jy78i8bbkz2yv9g1yg79ava6y3ziy5llys5n2ph5")))

(define-public crate-unic-langid-macros-0.9.2 (c (n "unic-langid-macros") (v "0.9.2") (d (list (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.9") (d #t) (k 0)))) (h "1gx86rda9j4sij09cg3rk4l2c7rqhggyxljqbybjwsmw7p0559fh") (y #t)))

(define-public crate-unic-langid-macros-0.9.3 (c (n "unic-langid-macros") (v "0.9.3") (d (list (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.9") (d #t) (k 0)))) (h "002n8a4vqsgwzivqp606pqhg6qhbvhcpk89mf7gnjyv2aaamim6l") (y #t)))

(define-public crate-unic-langid-macros-0.9.4 (c (n "unic-langid-macros") (v "0.9.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.9.4") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.9.4") (d #t) (k 0)))) (h "1v435dsl1412x6dv41q92ijz0fhvmp5nlq6f21j83wigp3plr1aw")))

(define-public crate-unic-langid-macros-0.9.5 (c (n "unic-langid-macros") (v "0.9.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.9.5") (d #t) (k 0)) (d (n "unic-langid-macros-impl") (r "^0.9.5") (d #t) (k 0)))) (h "0pi71r5474n7sdmyky7qpnia9rrr42q0d200l5lpag1d0hncv88d")))

