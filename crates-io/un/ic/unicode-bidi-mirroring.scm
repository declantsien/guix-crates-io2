(define-module (crates-io un ic unicode-bidi-mirroring) #:use-module (crates-io))

(define-public crate-unicode-bidi-mirroring-0.1.0 (c (n "unicode-bidi-mirroring") (v "0.1.0") (h "150navn2n6barkzchv96n877i17m1754nzmy1282zmcjzdh25lan")))

(define-public crate-unicode-bidi-mirroring-0.2.0 (c (n "unicode-bidi-mirroring") (v "0.2.0") (h "11mgyp2vclhzcm0kzy8jjn5ivsry4c37368finacb4mwzs7pijr3")))

