(define-module (crates-io un ic unicode-reverse) #:use-module (crates-io))

(define-public crate-unicode-reverse-1.0.0 (c (n "unicode-reverse") (v "1.0.0") (d (list (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "1dbka4lwi4sv3l9y5lvn36di85fnqxz0ma9wxpzxg3mn9dx3vm4r")))

(define-public crate-unicode-reverse-1.0.1 (c (n "unicode-reverse") (v "1.0.1") (d (list (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "00fpd2cv36x1b1klnwsbhx925145m9xg14rsxv735s4s9ayfb22d")))

(define-public crate-unicode-reverse-1.0.2 (c (n "unicode-reverse") (v "1.0.2") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "14pydyg1a9n14k47drdiqy5zimxfn78qlim1hl7fmvjfd9pwl9ri")))

(define-public crate-unicode-reverse-1.0.3 (c (n "unicode-reverse") (v "1.0.3") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.0") (d #t) (k 0)))) (h "0zbzscq9nx9kpw88h5mgldb3brwqrgfs6lhpj6fv8113p17gk3hf")))

(define-public crate-unicode-reverse-1.0.4 (c (n "unicode-reverse") (v "1.0.4") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.0") (d #t) (k 0)))) (h "00czla3w27ic9lmxfzzc6lqnpmgcd05xgpgfrsccg02cix4i8in1")))

(define-public crate-unicode-reverse-1.0.5 (c (n "unicode-reverse") (v "1.0.5") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.0") (d #t) (k 0)))) (h "0rfr30bdshy0dzad1dfj74l8dfb38rxxbfyfhj2gvsg4syvk76c9")))

(define-public crate-unicode-reverse-1.0.6 (c (n "unicode-reverse") (v "1.0.6") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.0") (d #t) (k 0)))) (h "0arihdfsidw0kjy7z2s8smj4jclffs27wnssr4ifv9mjf1i9pka8")))

(define-public crate-unicode-reverse-1.0.7 (c (n "unicode-reverse") (v "1.0.7") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.0") (d #t) (k 0)))) (h "1n5rhcmr699ciis2h5x3szkvyymg0b332pbmd58q13kwjsggq710")))

(define-public crate-unicode-reverse-1.0.8 (c (n "unicode-reverse") (v "1.0.8") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.0") (d #t) (k 0)))) (h "0b4n480vd897pm4klmmz07w8p45kb5ds0037kakd1lmhxfn5vshb")))

(define-public crate-unicode-reverse-1.0.9 (c (n "unicode-reverse") (v "1.0.9") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.0") (d #t) (k 0)))) (h "0xhcybbgy0l8s8n7sfd6hxi854f8znlxqkspzfnr8c62xf44hvsb")))

