(define-module (crates-io un ic unicode-bom) #:use-module (crates-io))

(define-public crate-unicode-bom-0.1.0 (c (n "unicode-bom") (v "0.1.0") (h "1a6s0s0avbay2l52d4qwvxkzrrf3jf738b14hisqah19hzxljh8m")))

(define-public crate-unicode-bom-0.1.1 (c (n "unicode-bom") (v "0.1.1") (h "1lnzv0m77rqlwmvwgg4wb2y8gddjp16cq3zgy23f5rggcfapqdbg")))

(define-public crate-unicode-bom-0.1.2 (c (n "unicode-bom") (v "0.1.2") (h "11lkxcwscv2s986vjbpplnklirrrx3fwfffn8r4bz33wp5a9xs3d")))

(define-public crate-unicode-bom-1.0.0 (c (n "unicode-bom") (v "1.0.0") (h "04mpfbp7nxjdijzp76cgixm8b09agmw4f6262kyfznd9aww8dw63")))

(define-public crate-unicode-bom-1.1.0 (c (n "unicode-bom") (v "1.1.0") (h "0afbah3mfxbmhvm63263jaczdypgg203nij6j0fsj2c9nwvlb72k")))

(define-public crate-unicode-bom-1.1.1 (c (n "unicode-bom") (v "1.1.1") (h "0k4m2439lj5crikla7scx34y957y7m0b4nrj090scr87zcnvmb1s")))

(define-public crate-unicode-bom-1.1.2 (c (n "unicode-bom") (v "1.1.2") (h "145ci99kykclr0bh7m3zvh50bz2q2jnzb7w9mnjqkbk97ylq2ixg")))

(define-public crate-unicode-bom-1.1.3 (c (n "unicode-bom") (v "1.1.3") (h "1wvcsvmr9is3502mah29jjpd8xfc3gdjmyzqmlp4bwxiggqkwh59")))

(define-public crate-unicode-bom-1.1.4 (c (n "unicode-bom") (v "1.1.4") (h "0cpc54ssrlrwm3x8dm7z1dgwj9r9bxjls620ra1vfxfq87snkv33")))

(define-public crate-unicode-bom-2.0.0 (c (n "unicode-bom") (v "2.0.0") (h "06m41064mill1is3588iwjnkwd46lsi5f5dc9k3w38zb99873zww")))

(define-public crate-unicode-bom-2.0.1 (c (n "unicode-bom") (v "2.0.1") (h "0ml2bfkcmd17pacv5kc20p4czwlivsh8nn6z1fm2pplyvrxk1idc")))

(define-public crate-unicode-bom-2.0.2 (c (n "unicode-bom") (v "2.0.2") (h "0lh5ckmw59v908mddgfgv19vv6yb0sm08z8adppd3m7hr5q0rscq")))

(define-public crate-unicode-bom-2.0.3 (c (n "unicode-bom") (v "2.0.3") (h "05s2sqyjanqrbds3fxam35f92npp5ci2wz9zg7v690r0448mvv3y")))

