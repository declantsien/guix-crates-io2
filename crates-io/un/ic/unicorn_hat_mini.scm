(define-module (crates-io un ic unicorn_hat_mini) #:use-module (crates-io))

(define-public crate-unicorn_hat_mini-0.1.0 (c (n "unicorn_hat_mini") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "rppal") (r "^0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1jqgyisr7vza39x308bdwgcw2p7ck4pnni870dnrrawv6vz97ag8")))

