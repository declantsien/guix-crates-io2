(define-module (crates-io un ic unicode-language) #:use-module (crates-io))

(define-public crate-unicode-language-1.0.0 (c (n "unicode-language") (v "1.0.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 1)))) (h "1ywi6fgn3cwcx4p5f2qfp4w4dwgkhl73v5niynb9rpp50qwkxb3j")))

(define-public crate-unicode-language-1.0.1 (c (n "unicode-language") (v "1.0.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 1)))) (h "06f34yfwvdn7vf0mrzv243pnlgcla9sn392zphmfxvwcvn32vi8p")))

(define-public crate-unicode-language-1.1.0 (c (n "unicode-language") (v "1.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 1)))) (h "00lp8ag81ixxi4gfyk0d30y5ir0j5j5msqi37kih8hdi6gc72z5w")))

(define-public crate-unicode-language-1.2.0 (c (n "unicode-language") (v "1.2.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 1)))) (h "1y3g1y9g3adckrinmsq7779p07kgf46waqnr0ysmg3aw3w026i44")))

(define-public crate-unicode-language-2.0.1 (c (n "unicode-language") (v "2.0.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "langtag") (r "^0.3.4") (d #t) (k 1)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 1)))) (h "0xlx312wabqzzpij4yhi9hcmr1zqddg0i9xfbcr3inz9gzln8kg7")))

