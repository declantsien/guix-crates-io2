(define-module (crates-io un ic unicode-canonical-combining-class) #:use-module (crates-io))

(define-public crate-unicode-canonical-combining-class-0.1.0 (c (n "unicode-canonical-combining-class") (v "0.1.0") (h "19p3fbpgdc9c6k394cyvnywf3fp7svww1c6qq19ivc35g6nq69y4")))

(define-public crate-unicode-canonical-combining-class-0.2.0 (c (n "unicode-canonical-combining-class") (v "0.2.0") (h "1qa8kmmz84nxni4n4qn65q80y64cw2sh3sc0crhkcng7qcv6l29v")))

(define-public crate-unicode-canonical-combining-class-0.3.0 (c (n "unicode-canonical-combining-class") (v "0.3.0") (h "08l8ircyfymkci4yamq2if0j1kvqdh4v6drsbic5z3k65a15rkl9")))

(define-public crate-unicode-canonical-combining-class-0.4.0 (c (n "unicode-canonical-combining-class") (v "0.4.0") (h "00x6gy3876gv70cqnpc9pxva9bhjdxx6lckri1wbvfbaf560ikma")))

(define-public crate-unicode-canonical-combining-class-0.5.0 (c (n "unicode-canonical-combining-class") (v "0.5.0") (h "0cjwhs1zk5yas1v1g95cxx4i04w4xm9hhz74273q5096z5m5h9b9")))

