(define-module (crates-io un ic unicorn-rs) #:use-module (crates-io))

(define-public crate-unicorn-rs-0.1.0 (c (n "unicorn-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)))) (h "0y2pcav6y9d04vyw04pc7ji6d5y3cdlix2nnmsgwyqwppyhrdah8") (l "unicorn")))

