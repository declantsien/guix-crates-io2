(define-module (crates-io un ic unicode_categories) #:use-module (crates-io))

(define-public crate-unicode_categories-0.1.0 (c (n "unicode_categories") (v "0.1.0") (h "0za1iddl1x41dgk6lcyi3i3wvw7h52crp10cvb1wgpflvswrgqfj")))

(define-public crate-unicode_categories-0.1.1 (c (n "unicode_categories") (v "0.1.1") (h "0kp1d7fryxxm7hqywbk88yb9d1avsam9sg76xh36k5qx2arj9v1r")))

