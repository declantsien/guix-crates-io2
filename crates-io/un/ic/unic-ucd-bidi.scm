(define-module (crates-io un ic unic-ucd-bidi) #:use-module (crates-io))

(define-public crate-unic-ucd-bidi-0.1.0 (c (n "unic-ucd-bidi") (v "0.1.0") (d (list (d (n "unic-ucd-core") (r "^0.1") (d #t) (k 0)))) (h "0jx12g2fimrrxh8n0gxkyvgf735r777xb2h2lwixfrvbkkk5r06a") (f (quote (("unstable") ("default"))))))

(define-public crate-unic-ucd-bidi-0.1.1 (c (n "unic-ucd-bidi") (v "0.1.1") (d (list (d (n "unic-ucd-core") (r "^0.1.1") (d #t) (k 0)))) (h "1cgmgdl4ydr2l69qvcqgq6752xqr2s3xfyhd99mn14489sbsd5dh")))

(define-public crate-unic-ucd-bidi-0.2.0 (c (n "unic-ucd-bidi") (v "0.2.0") (d (list (d (n "unic-ucd-core") (r "^0.2.0") (d #t) (k 0)))) (h "0q5li7dlwlx833pqmx8rdz6rn640yjp4wk2hprgj0595s5m8zmdy")))

(define-public crate-unic-ucd-bidi-0.4.0 (c (n "unic-ucd-bidi") (v "0.4.0") (d (list (d (n "unic-ucd-core") (r "^0.4.0") (d #t) (k 0)))) (h "1zyy8sna9nxn729q1lnrg5xdyazikfr5p2gcq1hwm0srbb2b251h")))

(define-public crate-unic-ucd-bidi-0.5.0 (c (n "unic-ucd-bidi") (v "0.5.0") (d (list (d (n "unic-ucd-core") (r "^0.5.0") (d #t) (k 0)))) (h "0kyda0xfhiqvasncnm1585sb72kw25apkyp3dkrk628h14sba21m")))

(define-public crate-unic-ucd-bidi-0.6.0 (c (n "unic-ucd-bidi") (v "0.6.0") (d (list (d (n "unic-char-property") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-ucd-core") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-utils") (r "^0.6.0") (d #t) (k 0)))) (h "0p97apm7pjmrvdibkqgqapxnx7zm00wq2rn96xmnqww1yr8svfvh")))

(define-public crate-unic-ucd-bidi-0.7.0 (c (n "unic-ucd-bidi") (v "0.7.0") (d (list (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "1hlvp9bsy9pralzhml4d9x2znyxpfcwvgglml7c15iz59k9byjxw")))

(define-public crate-unic-ucd-bidi-0.8.0 (c (n "unic-ucd-bidi") (v "0.8.0") (d (list (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "1h1l89dfhhj3zjb2ipcfk78r4pr4c4s2lmqzn3daq6rzgngiinin")))

(define-public crate-unic-ucd-bidi-0.9.0 (c (n "unic-ucd-bidi") (v "0.9.0") (d (list (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "131xnlgv76n2jbapgf3254svyc5llv54ikh9h8glwj122asnimfi")))

