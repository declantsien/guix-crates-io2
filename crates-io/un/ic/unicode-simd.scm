(define-module (crates-io un ic unicode-simd) #:use-module (crates-io))

(define-public crate-unicode-simd-0.0.1 (c (n "unicode-simd") (v "0.0.1") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-abstraction") (r "^0.7.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.31") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0zih5f96a53d9y8r6bqhdzzkl628llskgz9xh532cvsf6m9yn89r") (f (quote (("unstable" "simd-abstraction/unstable") ("std" "alloc" "simd-abstraction/std") ("detect" "simd-abstraction/detect") ("default" "std" "detect") ("alloc" "simd-abstraction/alloc")))) (y #t) (r "1.61")))

