(define-module (crates-io un ic unicode-linebreak) #:use-module (crates-io))

(define-public crate-unicode-linebreak-0.0.1 (c (n "unicode-linebreak") (v "0.0.1") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 1)))) (h "1ilg06xqr3cx6did6611qv84vl1rnj3kpb2cqzns0yz7mgg6hsrj")))

(define-public crate-unicode-linebreak-0.1.0 (c (n "unicode-linebreak") (v "0.1.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 1)))) (h "0l5iykjbzv7595z7kpvzl8rb252l5ryba23hvb0f40gsqg1wfc2f")))

(define-public crate-unicode-linebreak-0.1.1 (c (n "unicode-linebreak") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0mclfj20dai2asphlzw0jsw4m8bkdbz7i7q132wi6clas52iz8q5")))

(define-public crate-unicode-linebreak-0.1.2 (c (n "unicode-linebreak") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0grq6bsn967q4vpifld53s7a140nlmpq5vy8ghgr73f4n2mdqlis")))

(define-public crate-unicode-linebreak-0.1.3 (c (n "unicode-linebreak") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0kjwxvn8kryp710s3pz2y21x9n0srfc2fvzh7kfq0mnmra4kp1ds")))

(define-public crate-unicode-linebreak-0.1.4 (c (n "unicode-linebreak") (v "0.1.4") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0drixqb16bzmabd5d8ldvar5760rxy6nxzszhlsqnasl3bisvyn5") (r "1.56")))

(define-public crate-unicode-linebreak-0.1.5 (c (n "unicode-linebreak") (v "0.1.5") (h "07spj2hh3daajg335m4wdav6nfkl0f6c0q72lc37blr97hych29v") (r "1.56")))

