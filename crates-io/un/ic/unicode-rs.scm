(define-module (crates-io un ic unicode-rs) #:use-module (crates-io))

(define-public crate-unicode-rs-0.1.0 (c (n "unicode-rs") (v "0.1.0") (h "1a7zbwffajp1zmdyg1hgvb7qc5qm90i38l3jmask6qnvx4zjl0pv") (y #t)))

(define-public crate-unicode-rs-0.1.1 (c (n "unicode-rs") (v "0.1.1") (h "1kpbpqp5p95naj5b52bb3n1q5qzc3rlgnmrbws97q0a56kzs7v0h") (y #t)))

(define-public crate-unicode-rs-0.1.2 (c (n "unicode-rs") (v "0.1.2") (h "0f17vky6zi067mzm2mjb5bwn735fi9ca0dn23wh8w4p12hp3y0ia") (y #t)))

(define-public crate-unicode-rs-0.1.3 (c (n "unicode-rs") (v "0.1.3") (h "0zrzx2bn3grfavr44kl19cja8mxbb25cwylp2z8nk8jlj0pws4rj") (y #t)))

