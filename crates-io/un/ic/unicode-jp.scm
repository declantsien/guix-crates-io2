(define-module (crates-io un ic unicode-jp) #:use-module (crates-io))

(define-public crate-unicode-jp-0.1.0 (c (n "unicode-jp") (v "0.1.0") (d (list (d (n "clap") (r "^2.9.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "maplit") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "0klm46zrm16q9xpm39l60bgr30s1ydsnfiyisg4wdppf9b1hl9nd")))

(define-public crate-unicode-jp-0.2.0 (c (n "unicode-jp") (v "0.2.0") (d (list (d (n "clap") (r "^2.9.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "0x45xggj8zy0jrawfprfm71j9l16y55qh2lba69p7hrrg12zc9n3")))

(define-public crate-unicode-jp-0.3.0 (c (n "unicode-jp") (v "0.3.0") (d (list (d (n "clap") (r "^2.9.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "0g6m7pk65ki2fj68f6q4vh8mq8b55q590xp4jpa5q8iy3hvp70a1")))

(define-public crate-unicode-jp-0.4.0 (c (n "unicode-jp") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)))) (h "07z8yb7r07dcfh982zlzzll27y0xriyj36v7p41z87rmsz4bgva6")))

