(define-module (crates-io un ic unico) #:use-module (crates-io))

(define-public crate-unico-1.0.0 (c (n "unico") (v "1.0.0") (h "18kcvvw8q9vg8l5gslpiavmwlxwibmr5rv2244nh7i95dyn9gmvb")))

(define-public crate-unico-1.0.1 (c (n "unico") (v "1.0.1") (h "1bnmqm7vfr7jj87jk8ybgnrflkbbi6j8dapcmvdv6jpv94ikrsvp")))

