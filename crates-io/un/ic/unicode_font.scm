(define-module (crates-io un ic unicode_font) #:use-module (crates-io))

(define-public crate-unicode_font-0.1.0 (c (n "unicode_font") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ca52ssma86scznf9wym0wrdzp7bkqhbf0sbxzv3qh1xlznhz7sa") (f (quote (("extension") ("default" "extension"))))))

(define-public crate-unicode_font-0.1.1 (c (n "unicode_font") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zpg3gg3ay6nxrc4yhrxym8gdq1ym8r8xl4mqhgyi6q74w58w0x7") (f (quote (("extension") ("default" "extension"))))))

