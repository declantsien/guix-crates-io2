(define-module (crates-io un ic unicode-collation) #:use-module (crates-io))

(define-public crate-unicode-collation-0.0.1 (c (n "unicode-collation") (v "0.0.1") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.5") (d #t) (k 0)))) (h "1jv9gf5myhxj0aq8h64m6v4ym1z98kb64m2n7h2h25arfz4yh47p") (y #t)))

