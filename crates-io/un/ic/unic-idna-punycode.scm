(define-module (crates-io un ic unic-idna-punycode) #:use-module (crates-io))

(define-public crate-unic-idna-punycode-0.1.0 (c (n "unic-idna-punycode") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "rustc-test") (r "^0.1") (d #t) (k 2)))) (h "0l9vc1wjcj4m7n4mkl2lx30gdzxpdhdhbq5b87dnnk4sgjra4kfq")))

(define-public crate-unic-idna-punycode-0.1.1 (c (n "unic-idna-punycode") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "rustc-test") (r "^0.1") (d #t) (k 2)))) (h "0mi4pj3sx4ggbkcnc81v09y2r9f38ppxshzgy4135r8iq8iv6csd")))

(define-public crate-unic-idna-punycode-0.2.0 (c (n "unic-idna-punycode") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "rustc-test") (r "^0.1") (d #t) (k 2)))) (h "1wp5i5hgwzzygp5xgrfzyq10sh1g86lwvlnxg7qvxqfg4i7d5q0m")))

(define-public crate-unic-idna-punycode-0.4.0 (c (n "unic-idna-punycode") (v "0.4.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "rustc-test") (r "^0.1") (d #t) (k 2)))) (h "19a6naljdb31d1qszxswxbvh00fh5pq2qin2ln8qppy5xg6mqv9p")))

(define-public crate-unic-idna-punycode-0.5.0 (c (n "unic-idna-punycode") (v "0.5.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0ya7p3p2k98xw0fywycqhrkhbah42a5klxxp3h8mfdh6005dfd2i")))

(define-public crate-unic-idna-punycode-0.6.0 (c (n "unic-idna-punycode") (v "0.6.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0mzn5bxj4r6v33jfyis92p7cfc3p4fbj2930h1wqgbbfdy97vs6k")))

(define-public crate-unic-idna-punycode-0.7.0 (c (n "unic-idna-punycode") (v "0.7.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1h5q2wk316a9h5hwxcakws8nabv83bvydbyflmpcb2r4bihnc0qv")))

(define-public crate-unic-idna-punycode-0.8.0 (c (n "unic-idna-punycode") (v "0.8.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0386lx9cpfpy5znl8sj306d1yf8kwvxfx5f0dpg4wd4v28jjn9v5")))

(define-public crate-unic-idna-punycode-0.9.0 (c (n "unic-idna-punycode") (v "0.9.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "12igkz48n999gn0sp1h4gch1b2qbcc6kr0sd2icw47wzpzfaxzh6")))

