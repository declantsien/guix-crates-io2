(define-module (crates-io un ic unicorn_hat_hd) #:use-module (crates-io))

(define-public crate-unicorn_hat_hd-0.1.0 (c (n "unicorn_hat_hd") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "08si20yxbh0x7xn9kwsm7pl7sgc78vwxacqrgd3qpavbh0clnhb6")))

(define-public crate-unicorn_hat_hd-0.1.1 (c (n "unicorn_hat_hd") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "1b1lf922315v308cjlr2pf0ykzn2bwn54lcfilixf37dr1w9vwbx")))

(define-public crate-unicorn_hat_hd-0.1.2 (c (n "unicorn_hat_hd") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "0livj40p9gd6b25ybn8lnbygh24r1sh4l9vywnhr3rpa2g4xfdp6")))

(define-public crate-unicorn_hat_hd-0.1.3 (c (n "unicorn_hat_hd") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "1hxbkxnn5gl00h7wj6fg48gcj2a0jdfr6m9h8mdbl7gsnr5fxs72")))

(define-public crate-unicorn_hat_hd-0.1.4 (c (n "unicorn_hat_hd") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "00ahsbjh8ipvjxl1wsy61pahnb1g2193bfwg41xz1f1dmvv0y4ll")))

(define-public crate-unicorn_hat_hd-0.1.5 (c (n "unicorn_hat_hd") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1gfqf9sihn221z9l8mdfvz2ayvlpl0h8d4jj6jgkdl2lkdnxiqvs") (f (quote (("hardware" "spidev") ("fake-hardware" "ansi_term") ("default" "hardware"))))))

(define-public crate-unicorn_hat_hd-0.2.0 (c (n "unicorn_hat_hd") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1qxfmryg89qsmdxppfswqwn9mmm8mp24xdgw5bwxb22i8aw150qy") (f (quote (("hardware" "spidev") ("fake-hardware" "ansi_term") ("default" "hardware"))))))

(define-public crate-unicorn_hat_hd-0.2.1 (c (n "unicorn_hat_hd") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "190fq6yk58b28i4ngx9rrbsapa424f6krm991006bpbh4mby917y") (f (quote (("hardware" "spidev") ("fake-hardware" "ansi_term") ("default" "hardware"))))))

