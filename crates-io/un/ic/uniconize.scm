(define-module (crates-io un ic uniconize) #:use-module (crates-io))

(define-public crate-uniconize-1.0.0 (c (n "uniconize") (v "1.0.0") (d (list (d (n "x11rb") (r "^0.4") (f (quote ("vendor-xcb-proto"))) (k 0)))) (h "13bxkwcqxarbfi6x460vaxhvjwb9jxxz44x9ds3vna3xywgxk29j")))

(define-public crate-uniconize-1.0.1 (c (n "uniconize") (v "1.0.1") (d (list (d (n "x11rb") (r "^0.5") (k 0)))) (h "1bzdq8n954zcjg9c5rvwcz4bj0d3hfqlfvs77ydymwq34fpli2bn")))

