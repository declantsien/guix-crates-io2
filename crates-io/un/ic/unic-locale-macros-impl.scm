(define-module (crates-io un ic unic-locale-macros-impl) #:use-module (crates-io))

(define-public crate-unic-locale-macros-impl-0.1.0 (c (n "unic-locale-macros-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.4") (d #t) (k 0)))) (h "19xcxahzlfy1gdqw8sgx0q0jdfc3vl7qrlaxh33gp4ps0c9r4xma")))

(define-public crate-unic-locale-macros-impl-0.2.0 (c (n "unic-locale-macros-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.4") (d #t) (k 0)))) (h "051knn5pg0bzkwjh20ac0z6b1m4a9shgya1lc1ffp29vcal3xmjj")))

(define-public crate-unic-locale-macros-impl-0.3.0 (c (n "unic-locale-macros-impl") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.4") (d #t) (k 0)))) (h "1nfhig8x4zn8r6iraxybwfp0xv1gw9s9wx5fsdfylfk1mj73vfjl")))

(define-public crate-unic-locale-macros-impl-0.5.0 (c (n "unic-locale-macros-impl") (v "0.5.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.5") (d #t) (k 0)))) (h "0mf4yppmy5rvmx9vf4yva349snqsfb7p8bqii32a82iy97a917i0")))

(define-public crate-unic-locale-macros-impl-0.6.0 (c (n "unic-locale-macros-impl") (v "0.6.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.6") (d #t) (k 0)))) (h "1jyb549vhykdkwzyjbigh4m04naivg1ymgnln5rqa85a8dvvg7j6")))

(define-public crate-unic-locale-macros-impl-0.7.0 (c (n "unic-locale-macros-impl") (v "0.7.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.7") (d #t) (k 0)))) (h "0wcn9j9738h1a5rmyc3iswkpkfwn1wh2m2fxd0mb4kvi5ks0y0id")))

(define-public crate-unic-locale-macros-impl-0.8.0 (c (n "unic-locale-macros-impl") (v "0.8.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.8") (d #t) (k 0)))) (h "161rb1axchr30iwrcq4lpzna62gpjqas1m8qyjfbiq6xz6mw6yq5")))

(define-public crate-unic-locale-macros-impl-0.9.0 (c (n "unic-locale-macros-impl") (v "0.9.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-locale-impl") (r "^0.9") (d #t) (k 0)))) (h "0n8scyarvg7bwprza5rf67q61wyl5r97sdh9680q9c2yqhcbrjyj")))

(define-public crate-unic-locale-macros-impl-0.9.1 (c (n "unic-locale-macros-impl") (v "0.9.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-locale-impl") (r "^0.9") (d #t) (k 0)))) (h "0hpm4vymnifa933s565lmrl6frip78hhmlkfqipvq153clnmxlqc")))

(define-public crate-unic-locale-macros-impl-0.9.2 (c (n "unic-locale-macros-impl") (v "0.9.2") (d (list (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-locale-impl") (r "^0.9") (d #t) (k 0)))) (h "0ngjh0c2iqj0krgvrywahha2llkb4qkpc7yx34h8gxcl2gfvh4zy") (y #t)))

(define-public crate-unic-locale-macros-impl-0.9.3 (c (n "unic-locale-macros-impl") (v "0.9.3") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-locale-impl") (r "^0.9") (d #t) (k 0)))) (h "0argmi69s1qf8dn7iqnsmj8anxddh0a55y0qshw4rz47xpp9f24a") (y #t)))

(define-public crate-unic-locale-macros-impl-0.9.4 (c (n "unic-locale-macros-impl") (v "0.9.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-locale-impl") (r "^0.9.4") (d #t) (k 0)))) (h "13dxhwqna162ds41aiqwa26qw86wq88jwc8axzqhrvbphaz6wml0")))

(define-public crate-unic-locale-macros-impl-0.9.5 (c (n "unic-locale-macros-impl") (v "0.9.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-locale-impl") (r "^0.9.5") (d #t) (k 0)))) (h "029jx576pv4d5vcgm0j7d60b3b00d5989884xc5b6d422szi920i")))

