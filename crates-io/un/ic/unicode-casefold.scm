(define-module (crates-io un ic unicode-casefold) #:use-module (crates-io))

(define-public crate-unicode-casefold-0.1.0 (c (n "unicode-casefold") (v "0.1.0") (h "11bq70mwz6bjzdrrhs12ry75vz5rzlq1xc3gbvf4258jvm8k6yw7")))

(define-public crate-unicode-casefold-0.2.0 (c (n "unicode-casefold") (v "0.2.0") (h "13wfj1lc6njn3r0r73v6i6mzsvpihr9z7ly63nrjmalciwf6pxmp")))

