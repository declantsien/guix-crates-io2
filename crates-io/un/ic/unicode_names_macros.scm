(define-module (crates-io un ic unicode_names_macros) #:use-module (crates-io))

(define-public crate-unicode_names_macros-0.1.0 (c (n "unicode_names_macros") (v "0.1.0") (d (list (d (n "unicode_names") (r "^0.1.0") (d #t) (k 0)))) (h "0f482h6phydvmbhrsbc0xa91m6ikgyzqp7zkjnx17xsx84s91gb6")))

(define-public crate-unicode_names_macros-0.1.1 (c (n "unicode_names_macros") (v "0.1.1") (d (list (d (n "unicode_names") (r "^0.1.1") (d #t) (k 0)))) (h "1rsm9fpj4fq87abdm12xj5hx4zm2spfxry0pzf4w4mrlhswzwb79")))

(define-public crate-unicode_names_macros-0.1.2 (c (n "unicode_names_macros") (v "0.1.2") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "unicode_names") (r "^0.1.2") (d #t) (k 0)))) (h "1w71vhfa991bf8gwrd70fynpphfjrzq135bfz7g3knjyvqzhdsmb")))

(define-public crate-unicode_names_macros-0.1.3 (c (n "unicode_names_macros") (v "0.1.3") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "unicode_names") (r "^0.1") (d #t) (k 0)))) (h "020hlx4sjf3aign7c5j8lhb6z7dm9bvs7m7n4gnww9r2y8wja8xd")))

(define-public crate-unicode_names_macros-0.1.4 (c (n "unicode_names_macros") (v "0.1.4") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "unicode_names") (r "^0.1") (d #t) (k 0)))) (h "1a8wa62pwd0cm0xwgw42k7hy3k8zv4bk6h2wd5ksc76zw48l1a74")))

(define-public crate-unicode_names_macros-0.1.5 (c (n "unicode_names_macros") (v "0.1.5") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "unicode_names") (r "^0.1") (d #t) (k 0)))) (h "0pwimq7b1gzh29vjqs6l8rahpbxr61jzw0s5l81pnk12r9zx96a7")))

