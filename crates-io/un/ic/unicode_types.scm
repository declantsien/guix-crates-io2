(define-module (crates-io un ic unicode_types) #:use-module (crates-io))

(define-public crate-unicode_types-0.1.0 (c (n "unicode_types") (v "0.1.0") (h "1dgca90qzy8s2jsy950n56z978wxfriakqd7vxqh1kkph10cpczw")))

(define-public crate-unicode_types-0.2.0 (c (n "unicode_types") (v "0.2.0") (d (list (d (n "string_morph") (r "^0.1.0") (d #t) (k 0)))) (h "1bi6jy7pj6qz8dxkmwrlamr6pwizfl0b9d5jl6kc35lm1l9wzmca")))

