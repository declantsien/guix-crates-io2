(define-module (crates-io un ic unic-ucd-block) #:use-module (crates-io))

(define-public crate-unic-ucd-block-0.8.0 (c (n "unic-ucd-block") (v "0.8.0") (d (list (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "0f9wigjan697b3lh3f4n2jsfwixm7ki8mabldv2rkrlkl52xi4xj")))

(define-public crate-unic-ucd-block-0.9.0 (c (n "unic-ucd-block") (v "0.9.0") (d (list (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "1mzinqvb5qfjz2bmwp56264vd8bzdskaag05l4jm7lpcszr1cakb")))

