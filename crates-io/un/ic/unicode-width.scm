(define-module (crates-io un ic unicode-width) #:use-module (crates-io))

(define-public crate-unicode-width-0.0.1 (c (n "unicode-width") (v "0.0.1") (h "04dknylcdyx8v1dcf6kv7d4d4ry5x3nxayk6xljw6fpcxwb427ff")))

(define-public crate-unicode-width-0.1.0 (c (n "unicode-width") (v "0.1.0") (h "0d8klrnn9fw55x1371n2f0vrk7cd31m56rgpm8hdb82mdlmp71dw") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-width-0.1.1 (c (n "unicode-width") (v "0.1.1") (h "0df8xmd9ll594n1wk62zv540b0ns2snsmb2bh1r57axxd9gccmss") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-width-0.1.2 (c (n "unicode-width") (v "0.1.2") (h "1w70j302yqgmspgpx7w1qjm603dbdpj0mhcv8znfhq5p80c83bgr") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-width-0.1.3 (c (n "unicode-width") (v "0.1.3") (h "17n3mdlx1bz0qwx40a8mw6d4lwg1shyah80fxrirz60hrkx24rrd") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-width-0.1.4 (c (n "unicode-width") (v "0.1.4") (h "0pr4hv2ira80rhx0cfp6pbg34fn5alvfmn3lnzf24jkiflvi2fmz") (f (quote (("no_std") ("default") ("bench"))))))

(define-public crate-unicode-width-0.1.5 (c (n "unicode-width") (v "0.1.5") (h "09k5lipygardwy0660jhls08fsgknrazzivmn804gps53hiqc8w8") (f (quote (("no_std") ("default") ("bench"))))))

(define-public crate-unicode-width-0.1.6 (c (n "unicode-width") (v "0.1.6") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "082f9hv1r3gcd1xl33whjhrm18p0w9i77zhhhkiccb5r47adn1vh") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("no_std") ("default") ("bench"))))))

(define-public crate-unicode-width-0.1.7 (c (n "unicode-width") (v "0.1.7") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "0yflmxkxmm89ckrb3sz58whn491aycrj8cxra0hzzlb72x9rvana") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("no_std") ("default") ("bench"))))))

(define-public crate-unicode-width-0.1.8 (c (n "unicode-width") (v "0.1.8") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "1qxizyi6xbcqyi4z79p523ywvmgsfcgfqb3zv3c8i6x1jcc5jdwk") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("no_std") ("default") ("bench"))))))

(define-public crate-unicode-width-0.1.9 (c (n "unicode-width") (v "0.1.9") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "0wq9wl69wlp6zwlxp660g9p4hm5gk91chwk14dp1gl9bxba45mry") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("no_std") ("default") ("bench"))))))

(define-public crate-unicode-width-0.1.10 (c (n "unicode-width") (v "0.1.10") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "12vc3wv0qwg8rzcgb9bhaf5119dlmd6lmkhbfy1zfls6n7jx3vf0") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("no_std") ("default") ("bench"))))))

(define-public crate-unicode-width-0.1.11 (c (n "unicode-width") (v "0.1.11") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "11ds4ydhg8g7l06rlmh712q41qsrd0j0h00n1jm74kww3kqk65z5") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("no_std") ("default") ("bench"))))))

(define-public crate-unicode-width-0.1.12 (c (n "unicode-width") (v "0.1.12") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")) (d (n "unicode-normalization") (r "^0.1.23") (d #t) (k 2)))) (h "1mk6mybsmi5py8hf8zy9vbgs4rw4gkdqdq3gzywd9kwf2prybxb8") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("no_std") ("default"))))))

