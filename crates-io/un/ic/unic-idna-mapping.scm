(define-module (crates-io un ic unic-idna-mapping) #:use-module (crates-io))

(define-public crate-unic-idna-mapping-0.4.0 (c (n "unic-idna-mapping") (v "0.4.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "rustc-test") (r "^0.1") (d #t) (k 2)) (d (n "unic-ucd-core") (r "^0.4.0") (d #t) (k 0)))) (h "0b2nbn2a9k1wdn27xh4599pn727ayx3vl0bmxb8pzcps0kr4x4pl")))

(define-public crate-unic-idna-mapping-0.5.0 (c (n "unic-idna-mapping") (v "0.5.0") (d (list (d (n "unic-ucd-core") (r "^0.5.0") (d #t) (k 0)))) (h "1lf6mj9x30yrxhpvip0kjln249v23gm02lv2mzx4rd0jqb43wh10")))

(define-public crate-unic-idna-mapping-0.6.0 (c (n "unic-idna-mapping") (v "0.6.0") (d (list (d (n "unic-char-range") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-ucd-core") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-utils") (r "^0.6.0") (d #t) (k 0)))) (h "08vllxawqk3wvqz73cakch709yijrnl2ic241ra401igkrbh66fh")))

(define-public crate-unic-idna-mapping-0.7.0 (c (n "unic-idna-mapping") (v "0.7.0") (d (list (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "05pk28hy2filla4kapd2nd5gsdghilqjb7k71xqpnq7c38npkc5v")))

(define-public crate-unic-idna-mapping-0.8.0 (c (n "unic-idna-mapping") (v "0.8.0") (d (list (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "1qxqj7nkbw4cfz8pa1yl4yqj3idvzvvzhpfh3grx9bb33waab2mz")))

(define-public crate-unic-idna-mapping-0.9.0 (c (n "unic-idna-mapping") (v "0.9.0") (d (list (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "0hqkma2hbj8gk3nfzrgnr4kz3cgg734xp82hg8s3f59kwpa0zrsd")))

