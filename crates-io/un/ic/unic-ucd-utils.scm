(define-module (crates-io un ic unic-ucd-utils) #:use-module (crates-io))

(define-public crate-unic-ucd-utils-0.1.0 (c (n "unic-ucd-utils") (v "0.1.0") (h "1l85ndqfgbbzh0qrzicxyd3r9z0d5pyrxsqp94n4ri2fjih7mwk6") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-unic-ucd-utils-0.1.1 (c (n "unic-ucd-utils") (v "0.1.1") (h "1mnbgfz202aaaprx715jvp1masgbq83kk72fkk40y7rnngdgbgp1") (y #t)))

(define-public crate-unic-ucd-utils-0.2.0 (c (n "unic-ucd-utils") (v "0.2.0") (h "0cgx5q7k0f3yl790h005q52qschz9ip766f9dq99hqgancv648k9") (y #t)))

(define-public crate-unic-ucd-utils-0.4.0 (c (n "unic-ucd-utils") (v "0.4.0") (h "16j4zi2pl2y6wh47827y30mfwv0acsbkmmfajrkq63zl9p71zcvx") (y #t)))

