(define-module (crates-io un ic unicase) #:use-module (crates-io))

(define-public crate-unicase-0.0.1 (c (n "unicase") (v "0.0.1") (h "0lclfy8q6ll0z4mhv9jp3bi95r5fmr6scj7fp4fb8yhr4a5lk5r1")))

(define-public crate-unicase-0.0.2 (c (n "unicase") (v "0.0.2") (h "09ajxmnf7bkbfqgrm7ryg0ia8lqn281ng39kf12v1mplnb7qhjkg")))

(define-public crate-unicase-0.0.3 (c (n "unicase") (v "0.0.3") (h "1whb1ca1ak2g8773a0b2fhr8lay0cnc7ks5sgs51m0n8xmxlf8w9")))

(define-public crate-unicase-0.0.4 (c (n "unicase") (v "0.0.4") (h "0x0fsncbgw6yb57f96k748pqfmpwdhqbyfwhrfb7cz7w71bfjk3s")))

(define-public crate-unicase-0.0.5 (c (n "unicase") (v "0.0.5") (h "0wyff8wqqcsnb1y0kz0rm7vd47hsm04qw838kgifvpf7v6i1q770")))

(define-public crate-unicase-0.1.0 (c (n "unicase") (v "0.1.0") (h "1ywb049d2382vj0b41x0zip3x8lpvpy5dcs81pkv7h9vfmxcfdpb")))

(define-public crate-unicase-1.0.0 (c (n "unicase") (v "1.0.0") (h "0fihdyr9j1w30lv6cdq13lvyldws81na52ysw9hskfdv4sz4x1yb")))

(define-public crate-unicase-1.0.1 (c (n "unicase") (v "1.0.1") (h "0kg9szifmfkwrlzkin3q01km7n07pmdc9qk8nbxzz7dwwp411wlh")))

(define-public crate-unicase-1.1.0 (c (n "unicase") (v "1.1.0") (h "1faspx45z7plpgy8320iwc8zpxcr4568b67bkrhg400wvkv32n1h")))

(define-public crate-unicase-1.1.1 (c (n "unicase") (v "1.1.1") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "0kvvxz5s99aspxkljq7g93vbh8hbnm4k15rmhv42lf32kijlq6p9")))

(define-public crate-unicase-1.2.0 (c (n "unicase") (v "1.2.0") (d (list (d (n "heapsize") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "07krxcg4xyh43i35zgmhgwkd5ncw3f7rgv53vqhck1f17sfjvnz1") (f (quote (("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-unicase-1.2.1 (c (n "unicase") (v "1.2.1") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "0f6vsc0gmasf6h9wn9ick4hjx9jxr0ngaf75mxrdpvf66m1s3gxv") (f (quote (("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-unicase-1.3.0 (c (n "unicase") (v "1.3.0") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "19jd4hwvgb2cci1yg0cw7kpfy0l62rz3pyq2bqnvzvbpp1swn2p3") (f (quote (("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-unicase-1.4.0 (c (n "unicase") (v "1.4.0") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "0r1pbznlmis1r079657gwmmbszinfrxmbd0s9fgpk35rl9n9198k") (f (quote (("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-unicase-2.0.0 (c (n "unicase") (v "2.0.0") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "103bgm47zhzqhrmng5gi7za2m8i1xdlsxhva5p6x14h0a91dl09f") (f (quote (("nightly"))))))

(define-public crate-unicase-1.4.1 (c (n "unicase") (v "1.4.1") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0nr809wb9qzs1fl1s405p2xzh8jp7hlflgzlq5snpcb5p61c38rv") (f (quote (("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-unicase-1.4.2 (c (n "unicase") (v "1.4.2") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0cwazh4qsmm9msckjk86zc1z35xg7hjxjykrgjalzdv367w6aivz") (f (quote (("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-unicase-2.1.0 (c (n "unicase") (v "2.1.0") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "12nyyq2k9lr3n3xx1railnj0n6qda4hqqxzxi2z7zmi0nlynsjr8") (f (quote (("nightly"))))))

(define-public crate-unicase-2.2.0 (c (n "unicase") (v "2.2.0") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "143z2k9zjrw8whvb7w86qwnc78lj6x5adw0dzb6cmvdl2km1hclx") (f (quote (("nightly"))))))

(define-public crate-unicase-2.3.0 (c (n "unicase") (v "2.3.0") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1gak4p97z8g41pcdbql1dcbdxibgy8v9anx4f158xnl7z08p5la1") (f (quote (("nightly"))))))

(define-public crate-unicase-2.4.0 (c (n "unicase") (v "2.4.0") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1xmpmkakhhblq7dzab1kwyv925kv7fqjkjsxjspg6ix9n88makm8") (f (quote (("nightly"))))))

(define-public crate-unicase-2.5.0 (c (n "unicase") (v "2.5.0") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0hymz4sx8wl72axqr3raxfxc31hibbws3jqz4402lavxm1nv1pgy") (f (quote (("nightly"))))))

(define-public crate-unicase-2.5.1 (c (n "unicase") (v "2.5.1") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0l01y3clnc4scy6akw160dbyv3b2vgb4zfdy322mjmlywp8nnbif") (f (quote (("nightly"))))))

(define-public crate-unicase-2.6.0 (c (n "unicase") (v "2.6.0") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1xmlbink4ycgxrkjspp0mf7pghcx4m7vxq7fpfm04ikr2zk7pwsh") (f (quote (("nightly"))))))

(define-public crate-unicase-2.7.0 (c (n "unicase") (v "2.7.0") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "12gd74j79f94k4clxpf06l99wiv4p30wjr0qm04ihqk9zgdd9lpp") (f (quote (("nightly"))))))

