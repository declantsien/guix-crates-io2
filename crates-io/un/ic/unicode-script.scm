(define-module (crates-io un ic unicode-script) #:use-module (crates-io))

(define-public crate-unicode-script-0.1.0 (c (n "unicode-script") (v "0.1.0") (d (list (d (n "harfbuzz-sys") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0wasq6lxbz22z0jd1rjbjg9751yqs7hw0ky5ddymppg7fsamwpsx") (f (quote (("harfbuzz" "harfbuzz-sys"))))))

(define-public crate-unicode-script-0.1.1 (c (n "unicode-script") (v "0.1.1") (d (list (d (n "harfbuzz-sys") (r "^0.1") (o #t) (d #t) (k 0)))) (h "03yqjqj0nkl1x3d8zsn5d2rfhdnrvg3iy0nhh0b5a4pj3vi0lhz5") (f (quote (("harfbuzz" "harfbuzz-sys"))))))

(define-public crate-unicode-script-0.2.0 (c (n "unicode-script") (v "0.2.0") (d (list (d (n "harfbuzz-sys") (r "^0.2") (o #t) (d #t) (k 0)))) (h "10bfc2vd3739x7a5hz35a9air21krj5zc1337q8qaa0b0azppgg8") (f (quote (("harfbuzz" "harfbuzz-sys"))))))

(define-public crate-unicode-script-0.3.0 (c (n "unicode-script") (v "0.3.0") (d (list (d (n "harfbuzz-sys") (r "^0.3") (o #t) (d #t) (k 0)))) (h "06gx7s0zzx535cfl9nzy08kzyd7bg7ymp63r9ljdwkzbbzckmw09") (f (quote (("harfbuzz" "harfbuzz-sys"))))))

(define-public crate-unicode-script-0.4.0 (c (n "unicode-script") (v "0.4.0") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "071raja11mlk6idcvk05pz74bc5dcmyn4smgylbninh5x0lmqb2v") (f (quote (("with_std") ("rustc-dep-of-std" "std" "core" "compiler_builtins") ("default_features" "with_std"))))))

(define-public crate-unicode-script-0.5.0 (c (n "unicode-script") (v "0.5.0") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "1lj9ck9vq0k6zhvcfyghqf8879cx3nhn63i5zyj1yc1xpvza6g6w") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("bench"))))))

(define-public crate-unicode-script-0.5.1 (c (n "unicode-script") (v "0.5.1") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "0zgb1ydlsmkym1271kzadgqaw9qxqfyqsm230gmbgd4dx8a39csq") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("bench"))))))

(define-public crate-unicode-script-0.5.2 (c (n "unicode-script") (v "0.5.2") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "11sp2ga4bx71ahxgyw373aqr7nmv21w0j9wq7yvzsik5r5glvgvr") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("bench"))))))

(define-public crate-unicode-script-0.5.3 (c (n "unicode-script") (v "0.5.3") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "1s9s2x6r4yfb635cy2ppxw20xllx43p8dixwz1aws8fff9hwd3h9") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("bench"))))))

(define-public crate-unicode-script-0.5.4 (c (n "unicode-script") (v "0.5.4") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "02gvp27x55cbk26zxqwd5m7zhsnzljlaw5s9cxf0nbszs17r9paq") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("bench"))))))

(define-public crate-unicode-script-0.5.5 (c (n "unicode-script") (v "0.5.5") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "1k4fgc2lhn5x34w9xp2gqvxxqasds62qc9a7rbadzmmyw5ap50bx") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("bench"))))))

(define-public crate-unicode-script-0.5.6 (c (n "unicode-script") (v "0.5.6") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")))) (h "1kf9v1yfxazxjx07g9g9nqg4kw2kzpnyi7syjdd2hpvffbsp33dd") (f (quote (("rustc-dep-of-std" "std" "core" "compiler_builtins") ("bench"))))))

