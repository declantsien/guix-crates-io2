(define-module (crates-io un ic unicode-brackets) #:use-module (crates-io))

(define-public crate-unicode-brackets-0.1.0 (c (n "unicode-brackets") (v "0.1.0") (h "0q89dj014dzjb4imcw3m4sg24d9zqq7pqzf30i8sx6gv9r73akpv")))

(define-public crate-unicode-brackets-0.1.1 (c (n "unicode-brackets") (v "0.1.1") (h "1wska13w2viah7gakmkbb5s6l0asf6xcacgsvf195xlwnldlx8bp")))

