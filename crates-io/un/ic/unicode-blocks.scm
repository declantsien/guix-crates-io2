(define-module (crates-io un ic unicode-blocks) #:use-module (crates-io))

(define-public crate-unicode-blocks-0.1.0 (c (n "unicode-blocks") (v "0.1.0") (h "1gz3h18gng1r8i7j47g2ldf8dl9xvs0mvr0cf6jafb8w2skwl8zh")))

(define-public crate-unicode-blocks-0.1.1 (c (n "unicode-blocks") (v "0.1.1") (h "0irrdgxvzlq0gdhkc9jc38mmnzqhgv23p4k7d3c488s6hilsjbjl")))

(define-public crate-unicode-blocks-0.1.2 (c (n "unicode-blocks") (v "0.1.2") (h "1x8pqj5kwfai4786f41wkv377gwv5lg3jkgd9svax85prja1nv94")))

(define-public crate-unicode-blocks-0.1.3 (c (n "unicode-blocks") (v "0.1.3") (h "1ra0acpd0v40ciabzgrxfs2fdkhr9a29710msqa62alhd2q8k2bi")))

(define-public crate-unicode-blocks-0.1.4 (c (n "unicode-blocks") (v "0.1.4") (h "0wsvqraa7k153mq8z9v6ymfra2z12z4zl9xqndr433mi6w3zz2jq")))

(define-public crate-unicode-blocks-0.1.5 (c (n "unicode-blocks") (v "0.1.5") (h "0w5dg8hjxs4wc1jq04snlvims9mjrc8ycxykff1wwmkgmmmvxqlx")))

(define-public crate-unicode-blocks-0.1.6 (c (n "unicode-blocks") (v "0.1.6") (h "1ihwi2xvapz3jagidcb0h34b14zq3yvwq0885q3msifca0gkygll") (r "1.56")))

(define-public crate-unicode-blocks-0.1.7 (c (n "unicode-blocks") (v "0.1.7") (h "0jhsq2mxqwxb4cl9zxgr9280bk5nnkxyyfzdnfrpkjz6ibpy2s3n") (r "1.56")))

(define-public crate-unicode-blocks-0.1.8 (c (n "unicode-blocks") (v "0.1.8") (h "1kfxm14hj253496wprp0g0c0zml20ci5y5754brgn0n84z2rhhy8") (r "1.56")))

(define-public crate-unicode-blocks-0.1.9 (c (n "unicode-blocks") (v "0.1.9") (h "06sqfv9iz0drwrgl969scjbb6qf2cg1bhsxvm5ik2dq6krfy04kb") (r "1.56")))

