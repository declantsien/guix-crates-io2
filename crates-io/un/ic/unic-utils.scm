(define-module (crates-io un ic unic-utils) #:use-module (crates-io))

(define-public crate-unic-utils-0.5.0 (c (n "unic-utils") (v "0.5.0") (h "1c3wdc72h1ld6kx0wx3wkxf1vvlm3zzapk4g58skbrxzrmzb34nr")))

(define-public crate-unic-utils-0.6.0 (c (n "unic-utils") (v "0.6.0") (d (list (d (n "unic-char-range") (r "^0.6.0") (d #t) (k 0)))) (h "0n2xja8q70wdqkfwn7kj5nzh8v54h6vhg907d7qpwyqhhi0k9n9l")))

