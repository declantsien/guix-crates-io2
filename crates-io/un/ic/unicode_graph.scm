(define-module (crates-io un ic unicode_graph) #:use-module (crates-io))

(define-public crate-unicode_graph-0.9.9 (c (n "unicode_graph") (v "0.9.9") (h "0vxxgp0h6kpsk1x90r0yd1f787yziaq07q4485zcvy0c539sczhw")))

(define-public crate-unicode_graph-0.9.11 (c (n "unicode_graph") (v "0.9.11") (h "10s2a0zlfswa93fp7mpl82cglla54rhhd321y45vxi0ns1liwpcg")))

