(define-module (crates-io un ic unicopy) #:use-module (crates-io))

(define-public crate-unicopy-0.1.0 (c (n "unicopy") (v "0.1.0") (d (list (d (n "copypasta") (r "^0.7.0") (d #t) (k 0)) (d (n "dialog") (r "^0.3.0") (d #t) (k 0)))) (h "1yabbzwwb3r3wrdmqw00l3pjcjap82cc3s8cw6ly90cjh4dm46aw")))

(define-public crate-unicopy-0.1.1 (c (n "unicopy") (v "0.1.1") (d (list (d (n "copypasta") (r "^0.7.0") (d #t) (k 0)) (d (n "dialog") (r "^0.3.0") (d #t) (k 0)))) (h "1a1xqmbr90pwflqpkmy51szp4bqwb2z5spbigvhsm8xq2wk1rchs")))

