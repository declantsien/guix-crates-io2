(define-module (crates-io un ic unichars) #:use-module (crates-io))

(define-public crate-unichars-0.0.1 (c (n "unichars") (v "0.0.1") (h "0fhfbz3hnxv465ac7s32z5iwq0f00zr6gk3pvc8vmkv14im4nw87") (y #t)))

(define-public crate-unichars-0.0.2 (c (n "unichars") (v "0.0.2") (h "1lpvxpqw74xn609xk56kfdp352ax2hs20n5yjr792lajh6vcq3kv") (y #t)))

