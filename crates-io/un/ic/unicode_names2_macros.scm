(define-module (crates-io un ic unicode_names2_macros) #:use-module (crates-io))

(define-public crate-unicode_names2_macros-0.2.0 (c (n "unicode_names2_macros") (v "0.2.0") (d (list (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.2") (d #t) (k 0)))) (h "15166dgkd81hwq8lyrpxc7mn8bvlbdzkmx3khaq44gbg5m9gwflq") (f (quote (("unstable"))))))

(define-public crate-unicode_names2_macros-0.3.0 (c (n "unicode_names2_macros") (v "0.3.0") (d (list (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.3.0") (d #t) (k 0)))) (h "1rjqgrgh9z59487p9bmf9jvrr8nsk5p5m88rh6qzkfza0m4ik640") (f (quote (("unstable"))))))

(define-public crate-unicode_names2_macros-0.4.0 (c (n "unicode_names2_macros") (v "0.4.0") (d (list (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.4.0") (d #t) (k 0)))) (h "04zpdsbidi061lbwq9l8k8a04j4bdxdl9k1n4kzpl9y0ayxrs65b") (f (quote (("unstable"))))))

(define-public crate-unicode_names2_macros-0.6.0 (c (n "unicode_names2_macros") (v "0.6.0") (d (list (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.6.0") (d #t) (k 0)))) (h "0jfq7ava02lynvsfga2qjphq94jcaziy3xs37aqlg42jiahs90ix") (f (quote (("unstable"))))))

(define-public crate-unicode_names2_macros-1.0.0 (c (n "unicode_names2_macros") (v "1.0.0") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "unicode_names2") (r "^1.0.0") (d #t) (k 0)))) (h "1iiw372n17k7xnr3l8b6y5nxkmy8n2czl228i4lfb1x6m704x16h") (f (quote (("unstable"))))))

(define-public crate-unicode_names2_macros-1.1.0 (c (n "unicode_names2_macros") (v "1.1.0") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "unicode_names2") (r "^1.1.0") (d #t) (k 0)))) (h "0ib3wyw5flvjipr44d6igj90gb5jqg2dyjd1ajbyypqa7sv3ysyf") (f (quote (("unstable"))))))

