(define-module (crates-io un ic unic-segment) #:use-module (crates-io))

(define-public crate-unic-segment-0.7.0 (c (n "unic-segment") (v "0.7.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "unic-ucd-common") (r "^0.7.0") (d #t) (k 2)) (d (n "unic-ucd-segment") (r "^0.7.0") (d #t) (k 0)))) (h "17kgxvhkda6njwkqhxsj8vx050sjvh8pv1mmcv8grdczn35lgjn9")))

(define-public crate-unic-segment-0.8.0 (c (n "unic-segment") (v "0.8.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "unic-ucd-common") (r "^0.8.0") (d #t) (k 2)) (d (n "unic-ucd-segment") (r "^0.8.0") (d #t) (k 0)))) (h "0zvz0sqdx7xh49h0nr5g4hyb1kn26ydlxx99dih2x0s0d2cspxkv")))

(define-public crate-unic-segment-0.9.0 (c (n "unic-segment") (v "0.9.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "unic-ucd-common") (r "^0.9.0") (d #t) (k 2)) (d (n "unic-ucd-segment") (r "^0.9.0") (d #t) (k 0)))) (h "08wgz2q6vrdvmbd23kf9pbg8cyzm5q8hq9spc4blzy2ppqk5vvg4")))

