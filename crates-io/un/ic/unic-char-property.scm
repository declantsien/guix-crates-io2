(define-module (crates-io un ic unic-char-property) #:use-module (crates-io))

(define-public crate-unic-char-property-0.6.0 (c (n "unic-char-property") (v "0.6.0") (d (list (d (n "unic-char-range") (r "^0.6.0") (d #t) (k 2)) (d (n "unic-utils") (r "^0.6.0") (d #t) (k 0)))) (h "0vmpih9wc83sawf0gs5zby887gg0jv8csjdk49mv488da2rczd3x")))

(define-public crate-unic-char-property-0.7.0 (c (n "unic-char-property") (v "0.7.0") (d (list (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)))) (h "07mxbng6x18qcbwpkrda9as0zr9l07fz13ygrjygsjkmrvvx6dnf")))

(define-public crate-unic-char-property-0.8.0 (c (n "unic-char-property") (v "0.8.0") (d (list (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)))) (h "084vm3dwif8lwk07yz4npqgv20aj16dzdja594x2ldknqvha1akh")))

(define-public crate-unic-char-property-0.9.0 (c (n "unic-char-property") (v "0.9.0") (d (list (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)))) (h "08g21dn3wwix3ycfl0vrbahn0835nv2q3swm8wms0vwvgm07mid8")))

