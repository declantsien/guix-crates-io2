(define-module (crates-io un ic unic-locale-macros) #:use-module (crates-io))

(define-public crate-unic-locale-macros-0.1.0 (c (n "unic-locale-macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.4") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.1") (d #t) (k 0)))) (h "0bj7d24rsjwy1w816gadvp6irk9z319jl29x2j80hycjl7rla2vk")))

(define-public crate-unic-locale-macros-0.2.0 (c (n "unic-locale-macros") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.4") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.2") (d #t) (k 0)))) (h "0gzcsqp4i1pq4aj40vay90f4wjph1szclkpd22s7j6wsmc7jszsq")))

(define-public crate-unic-locale-macros-0.3.0 (c (n "unic-locale-macros") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.4") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.3") (d #t) (k 0)))) (h "0xjrviznrpm4fl5q5alv2f341gvrc1441cnfqbbdwpf0myb36xb8")))

(define-public crate-unic-locale-macros-0.5.0 (c (n "unic-locale-macros") (v "0.5.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.5") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.5") (d #t) (k 0)))) (h "1jyvzcpa4ci69nl6428lspp7fp673hgkz17yxlk2wh85yzfr352j")))

(define-public crate-unic-locale-macros-0.6.0 (c (n "unic-locale-macros") (v "0.6.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.6") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.6") (d #t) (k 0)))) (h "1prqdk8vz2njvrvzk92m7q93pfcqjbv73sy3mnn43qdmwlcwma7g")))

(define-public crate-unic-locale-macros-0.7.0 (c (n "unic-locale-macros") (v "0.7.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.7") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.7") (d #t) (k 0)))) (h "0vr98f0ipb877ad01gdlcf3a5sssg87k6vvfkzr0mvl4l1dm34iw")))

(define-public crate-unic-locale-macros-0.8.0 (c (n "unic-locale-macros") (v "0.8.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.8") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.8") (d #t) (k 0)))) (h "091frqc6x5m5rpqi2znd6j2v6ff9c8vz4cg4gl5mlmrgi7rh1lq3")))

(define-public crate-unic-locale-macros-0.9.0 (c (n "unic-locale-macros") (v "0.9.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.9") (d #t) (k 0)))) (h "07ndh3bj14zjc01zlxb3zpc1m1krcc8nd3aphssbqs0778m5jyxr")))

(define-public crate-unic-locale-macros-0.9.1 (c (n "unic-locale-macros") (v "0.9.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.9") (d #t) (k 0)))) (h "01fwq1wqnsyc32gfb9g8a94y3vkkcnf6nr7fximw416xanyxm4cb")))

(define-public crate-unic-locale-macros-0.9.2 (c (n "unic-locale-macros") (v "0.9.2") (d (list (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.9") (d #t) (k 0)))) (h "03hkgi0nk4qa0z6hnd7mrnqs8vysfpl9nbny3h5cy19pc138qmfh") (y #t)))

(define-public crate-unic-locale-macros-0.9.3 (c (n "unic-locale-macros") (v "0.9.3") (d (list (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.9") (d #t) (k 0)))) (h "0ni733xa4x2734nrwb8x5l3k19s400ppi2rrv3pxblfbg14h1nsz") (y #t)))

(define-public crate-unic-locale-macros-0.9.4 (c (n "unic-locale-macros") (v "0.9.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.9.4") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.9.4") (d #t) (k 0)))) (h "06s4qxnvqb9ciqaizxbb2xdxbsx1izcsnf3j069m79nlhb1c1xcx")))

(define-public crate-unic-locale-macros-0.9.5 (c (n "unic-locale-macros") (v "0.9.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tinystr") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-locale-impl") (r "^0.9.5") (d #t) (k 0)) (d (n "unic-locale-macros-impl") (r "^0.9.5") (d #t) (k 0)))) (h "13nyyyz7kcvly4j4dqhl30nvl0015rj79dzs5abby6y29lgmw8l5")))

