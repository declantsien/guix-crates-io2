(define-module (crates-io un ic unic-langid-macros-impl) #:use-module (crates-io))

(define-public crate-unic-langid-macros-impl-0.1.0 (c (n "unic-langid-macros-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.4") (d #t) (k 0)))) (h "0ka34hsmp5xb837ij85wxvwp9bjx6xs63r6j9z61r3p89zskxvvb")))

(define-public crate-unic-langid-macros-impl-0.2.0 (c (n "unic-langid-macros-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.4") (d #t) (k 0)))) (h "0gbj1j6g3n2r1nvc03dzcrb9avn2c6s82mi7i5cimdj15gcmmncd")))

(define-public crate-unic-langid-macros-impl-0.3.0 (c (n "unic-langid-macros-impl") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.4") (d #t) (k 0)))) (h "0bijjq4lq49rlnlr80ca04ff4m4bnf7cqg0gsa4whlq1ga83kfdb")))

(define-public crate-unic-langid-macros-impl-0.5.0 (c (n "unic-langid-macros-impl") (v "0.5.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.5") (d #t) (k 0)))) (h "04k81w5h69y6ddr7ij51wyb164cl6smrp1aizjysq5kk7l02ilxv")))

(define-public crate-unic-langid-macros-impl-0.6.0 (c (n "unic-langid-macros-impl") (v "0.6.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.6") (d #t) (k 0)))) (h "0s7wngg26xgd7bfa6lgxz4zi06afslllnha4rk19fmsndaa0rf59")))

(define-public crate-unic-langid-macros-impl-0.7.0 (c (n "unic-langid-macros-impl") (v "0.7.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.7") (d #t) (k 0)))) (h "1wcymlhp1wapajl1pjg7nrinprvwnd4b5600c0zb457wcrf5aklq")))

(define-public crate-unic-langid-macros-impl-0.8.0 (c (n "unic-langid-macros-impl") (v "0.8.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unic-langid-impl") (r "^0.8") (d #t) (k 0)))) (h "1laqx0nfd8rardmw2vdcqjc1i0ma8fqlmpqchnvqykvmpmvqy2g0")))

(define-public crate-unic-langid-macros-impl-0.9.0 (c (n "unic-langid-macros-impl") (v "0.9.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)))) (h "17xaknz6ixhg2cp8x174d54hdlv78akb2s0kw31p8xg2jzynyf99")))

(define-public crate-unic-langid-macros-impl-0.9.1 (c (n "unic-langid-macros-impl") (v "0.9.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)))) (h "1n74gi3l8j8k94535psn3azbx9g69i7kbx23d0plwzwhbg0dwp0z")))

(define-public crate-unic-langid-macros-impl-0.9.2 (c (n "unic-langid-macros-impl") (v "0.9.2") (d (list (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)))) (h "0i79nnn5qv4s1k6k362h2709r5bk93hj4wihszyykg1d6ffngf0f") (y #t)))

(define-public crate-unic-langid-macros-impl-0.9.3 (c (n "unic-langid-macros-impl") (v "0.9.3") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)))) (h "0plfb79bp7bdz8hrpgp1h55my0f9rld76jws3dc15jid6qamb1pv") (y #t)))

(define-public crate-unic-langid-macros-impl-0.9.4 (c (n "unic-langid-macros-impl") (v "0.9.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-langid-impl") (r "^0.9.4") (d #t) (k 0)))) (h "1r0828l6h5p44b7ln8sjrsxl4dhyv4nmwszna75b6kzb1p4a98py")))

(define-public crate-unic-langid-macros-impl-0.9.5 (c (n "unic-langid-macros-impl") (v "0.9.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "unic-langid-impl") (r "^0.9.5") (d #t) (k 0)))) (h "0nsm0hky2sawgkwz511br06mkm3ba70rfc05jm0l54x3gciz9mqy")))

