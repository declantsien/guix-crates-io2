(define-module (crates-io un ic unic-ucd-category) #:use-module (crates-io))

(define-public crate-unic-ucd-category-0.5.0 (c (n "unic-ucd-category") (v "0.5.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "unic-ucd-core") (r "^0.5.0") (d #t) (k 0)))) (h "1iigv07m9bb8pik6hqn8sd35781d7riadbp98isf17i3w13i9jb5")))

(define-public crate-unic-ucd-category-0.6.0 (c (n "unic-ucd-category") (v "0.6.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "unic-char-property") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-ucd-core") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-utils") (r "^0.6.0") (d #t) (k 0)))) (h "1f4b4hj4p115mdhadjzvyrqdwwhmk3hs3l37ck38dj859jrq94f6")))

(define-public crate-unic-ucd-category-0.7.0 (c (n "unic-ucd-category") (v "0.7.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "17jvlmmi09vvpsvsn9hqs65p3d6wacdwm1k2yingnbrnpwb8rll0")))

(define-public crate-unic-ucd-category-0.8.0 (c (n "unic-ucd-category") (v "0.8.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "14iyzsgvhll75sa6xdj94dvg8ccjzd9h505fwmddwinn3g6ak0f8")))

(define-public crate-unic-ucd-category-0.9.0 (c (n "unic-ucd-category") (v "0.9.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "1h4ixzplc2s441vc8mc4zxliw6qfqh1ziaiv8pa1pzpwyn8lb38v")))

