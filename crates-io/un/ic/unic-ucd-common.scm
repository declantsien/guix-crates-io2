(define-module (crates-io un ic unic-ucd-common) #:use-module (crates-io))

(define-public crate-unic-ucd-common-0.7.0 (c (n "unic-ucd-common") (v "0.7.0") (d (list (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.7.0") (d #t) (k 2)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "1az3p667c5x6qm2zm9wwah6bj7ir9wqrlrw689cy48d8qxsf97wa")))

(define-public crate-unic-ucd-common-0.8.0 (c (n "unic-ucd-common") (v "0.8.0") (d (list (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.8.0") (d #t) (k 2)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "1i28ib63j4ygxh6lglqdyhl4hbdy6045j8yldaw9gz8qsx06rj75")))

(define-public crate-unic-ucd-common-0.9.0 (c (n "unic-ucd-common") (v "0.9.0") (d (list (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 2)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "1bglvzn6rs01v0d29898vg2y3v3cgj3h1gsrbjp1mypa1f8qpdz9")))

