(define-module (crates-io un ic unic-ucd-case) #:use-module (crates-io))

(define-public crate-unic-ucd-case-0.6.0 (c (n "unic-ucd-case") (v "0.6.0") (d (list (d (n "unic-char-property") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-ucd-core") (r "^0.6.0") (d #t) (k 0)))) (h "0fwkn32sbm2sfr9gbwjd8kzq18wdfjjbmr3s58w77x17hhdijq1p")))

(define-public crate-unic-ucd-case-0.7.0 (c (n "unic-ucd-case") (v "0.7.0") (d (list (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "068hfqq2hsgi13zj5grvqi4kvyyjnijm44lzj78x9525b0yqc7mg")))

(define-public crate-unic-ucd-case-0.8.0 (c (n "unic-ucd-case") (v "0.8.0") (d (list (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "1yl02kx8azq9xxiznx6gbikr0fa8knci61hmkk14csskz8q5x4h5")))

(define-public crate-unic-ucd-case-0.9.0 (c (n "unic-ucd-case") (v "0.9.0") (d (list (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "1lx6vrgvmamzd54zldyq24p8hz6s5d103vmycv7wdfkrd8jdd61x")))

