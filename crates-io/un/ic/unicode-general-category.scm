(define-module (crates-io un ic unicode-general-category) #:use-module (crates-io))

(define-public crate-unicode-general-category-0.1.0 (c (n "unicode-general-category") (v "0.1.0") (h "1088972npn2yg3q47y018glqzhyaws4l96fayb48hb5x1qjpq74n")))

(define-public crate-unicode-general-category-0.2.0 (c (n "unicode-general-category") (v "0.2.0") (h "0zygqmp4w2j92hrmd8kh65hskpi58qq36sq6kvci19jjw0lg16kz")))

(define-public crate-unicode-general-category-0.3.0 (c (n "unicode-general-category") (v "0.3.0") (h "1v075r9rnfqknnmsdb8ak69g4vdk7f08h70sr8x62hcl3z3f5cga")))

(define-public crate-unicode-general-category-0.4.0 (c (n "unicode-general-category") (v "0.4.0") (h "0hi7zl5kpkpbdm9dn4z44fmid3zm8inwbaizq9n34a2ywhz7wm07")))

(define-public crate-unicode-general-category-0.5.0 (c (n "unicode-general-category") (v "0.5.0") (h "13bq3svbapaiyjq95yxd5gzps5vdz074d08dz38k7cn7d4fjdlb2") (y #t)))

(define-public crate-unicode-general-category-0.5.1 (c (n "unicode-general-category") (v "0.5.1") (h "1mnz1haq2wf7knyln6ff8z992r4p1p3h8hc2l8cmn25qd220j60j")))

(define-public crate-unicode-general-category-0.6.0 (c (n "unicode-general-category") (v "0.6.0") (h "1rv9715c94gfl0hzy4f2a9lw7i499756bq2968vqwhr1sb0wi092")))

