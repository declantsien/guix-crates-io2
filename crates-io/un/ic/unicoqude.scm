(define-module (crates-io un ic unicoqude) #:use-module (crates-io))

(define-public crate-unicoqude-0.1.0 (c (n "unicoqude") (v "0.1.0") (h "0nmxs2zmx04hkzmmys9fhgkj1r7bpgg24zqygjqgvcaghsg49zli")))

(define-public crate-unicoqude-0.1.1 (c (n "unicoqude") (v "0.1.1") (h "1cx7p30rsc5fa7nskp12rwkpxyvbbr59lnnipx14sc5m3ivhrsp9")))

