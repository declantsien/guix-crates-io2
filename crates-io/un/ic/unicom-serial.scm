(define-module (crates-io un ic unicom-serial) #:use-module (crates-io))

(define-public crate-unicom-serial-0.1.0 (c (n "unicom-serial") (v "0.1.0") (d (list (d (n "tokio-serial") (r "^4") (o #t) (k 0)) (d (n "unicom") (r "^0.1.0") (d #t) (k 0)))) (h "0h4vbr1n62chryd0jmci04v2vrnavgx7d3sz3dvay2rayqzd9mkx") (f (quote (("tokio" "tokio-serial" "unicom/tokio") ("rustdoc" "tokio") ("libudev" "tokio-serial/libudev"))))))

(define-public crate-unicom-serial-0.1.1 (c (n "unicom-serial") (v "0.1.1") (d (list (d (n "tokio-serial") (r "^4") (o #t) (k 0)) (d (n "unicom") (r "^0.1") (d #t) (k 0)))) (h "0gr2kcmhs3rxwq8klkm70q77hpdgvgkzirpws0k49zpm6lq8pbdr") (f (quote (("tokio" "tokio-serial" "unicom/tokio") ("rustdoc" "tokio") ("libudev" "tokio-serial/libudev"))))))

(define-public crate-unicom-serial-0.2.0 (c (n "unicom-serial") (v "0.2.0") (d (list (d (n "tokio-serial") (r "^5") (o #t) (k 0)) (d (n "unicom") (r "^0.2.0") (d #t) (k 0)))) (h "0n308bksq7fw28vw8np7l025qnrvz3ac6694154mw23r4jbkninv") (f (quote (("tokio" "tokio-serial" "unicom/tokio") ("rustdoc" "tokio") ("libudev" "tokio-serial/libudev") ("async-std" "unicom/async-std"))))))

(define-public crate-unicom-serial-0.4.0 (c (n "unicom-serial") (v "0.4.0") (d (list (d (n "tokio-serial") (r "^5") (o #t) (d #t) (k 0)) (d (n "unicom") (r "^0.4") (d #t) (k 0)))) (h "0rxfi7gxjkq8r7i4msz1yzj5s4zx7bxj7bz313yxr9jh4czykm90") (f (quote (("tokio" "tokio-serial" "unicom/tokio") ("rustdoc" "tokio") ("libudev" "tokio-serial/libudev") ("async" "unicom/async"))))))

