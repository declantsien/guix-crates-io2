(define-module (crates-io un ic unic-cli) #:use-module (crates-io))

(define-public crate-unic-cli-0.7.0 (c (n "unic-cli") (v "0.7.0") (d (list (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "unic") (r "^0.7.0") (d #t) (k 0)))) (h "04a3r87gk7hbmmaclca1y8p6kgf1kbspq1yz0s7drwqrm4w67jcz")))

(define-public crate-unic-cli-0.8.0 (c (n "unic-cli") (v "0.8.0") (d (list (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "unic") (r "^0.8.0") (d #t) (k 0)))) (h "0kvhy7hd1qadzn3mjwdc2lxlkg6hg5cfdnpz7g61j6d3jwd883ly")))

(define-public crate-unic-cli-0.9.0 (c (n "unic-cli") (v "0.9.0") (d (list (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "unic") (r "^0.9.0") (d #t) (k 0)))) (h "02vr4hcnh0sillgd25mxzj3i30x6dmcjhkamkf17nm28kl98nvv9")))

