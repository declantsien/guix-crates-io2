(define-module (crates-io un ic unicase_serde) #:use-module (crates-io))

(define-public crate-unicase_serde-0.1.0 (c (n "unicase_serde") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1w8iig5nqia75ggppmc5m4q2gqcdq85zh2hnydllv1wxcybkdxbf")))

