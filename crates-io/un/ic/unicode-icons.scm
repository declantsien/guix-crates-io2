(define-module (crates-io un ic unicode-icons) #:use-module (crates-io))

(define-public crate-unicode-icons-0.1.0 (c (n "unicode-icons") (v "0.1.0") (h "1n49phgkvk0dkj7mi76qp6cnvgb0mlllazm2by1pmks3d4n6k0m8")))

(define-public crate-unicode-icons-0.1.2 (c (n "unicode-icons") (v "0.1.2") (h "0px06pn08iqngs5pbf1q25rq8kf6wrqs7ca9gd6svbng8v9yfzkw")))

(define-public crate-unicode-icons-0.1.5 (c (n "unicode-icons") (v "0.1.5") (h "1w8jqzz799y3vfrsc3il00qq6iqarpwsipphcll3sry5y92330wv")))

(define-public crate-unicode-icons-1.0.0 (c (n "unicode-icons") (v "1.0.0") (h "1ahy5p3qa82s5vjfhh96214lx972cfki53s978ghl9sldyikzcyx")))

(define-public crate-unicode-icons-1.0.1 (c (n "unicode-icons") (v "1.0.1") (h "03c77gf4fpsliln09am6d657ga5qmwd5cwdcn27v8lrmkq3kphc9")))

(define-public crate-unicode-icons-2.0.1 (c (n "unicode-icons") (v "2.0.1") (h "03ph0y5mqrs7dcq2636wczml7a4gsj3paqwl7vwy250zi9lqj0i4") (f (quote (("travel_and_places") ("symbols") ("smileys_and_emotion") ("people_and_body") ("objects") ("food_and_drink") ("flags") ("component") ("animals_and_nature") ("activities"))))))

(define-public crate-unicode-icons-2.1.2 (c (n "unicode-icons") (v "2.1.2") (h "0nlfjz7by5bj0m958vzk9c2pad99ig2c95zgwdzi6rlvcjfbsywf") (f (quote (("travel_and_places") ("symbols") ("smileys_and_emotion") ("people_and_body") ("objects") ("food_and_drink") ("flags") ("default" "activities" "animals_and_nature" "component" "flags" "food_and_drink" "objects" "people_and_body" "smileys_and_emotion" "symbols" "travel_and_places") ("component") ("animals_and_nature") ("activities"))))))

