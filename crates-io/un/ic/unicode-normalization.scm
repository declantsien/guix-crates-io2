(define-module (crates-io un ic unicode-normalization) #:use-module (crates-io))

(define-public crate-unicode-normalization-0.0.1 (c (n "unicode-normalization") (v "0.0.1") (h "18b77icrf3inpgd1njlp2iqawgw5a89gh2grllzx8021ppfjns49")))

(define-public crate-unicode-normalization-0.0.2 (c (n "unicode-normalization") (v "0.0.2") (h "07c334pk30nmbajvky0riscp3njdybhzh580snjz499ws8cp8nwb")))

(define-public crate-unicode-normalization-0.0.3 (c (n "unicode-normalization") (v "0.0.3") (h "14684lrzri5ydacdyj3anz9kkvbigvq73fqm0x9q3gqb939k63q3")))

(define-public crate-unicode-normalization-0.1.0 (c (n "unicode-normalization") (v "0.1.0") (h "1gl0w23g7akscmdib68gnylspmyhgwihjg14slksg6h26aca3p27")))

(define-public crate-unicode-normalization-0.1.1 (c (n "unicode-normalization") (v "0.1.1") (h "0y72y07wbp4xlac26pn04ls8nck7nksd1ssndz7wdka55s17ixn1")))

(define-public crate-unicode-normalization-0.1.2 (c (n "unicode-normalization") (v "0.1.2") (h "0whi4xxqcjfsz6ywyrfd5lhgk1a44c86qwgvfqcmzidshcpklr16")))

(define-public crate-unicode-normalization-0.1.3 (c (n "unicode-normalization") (v "0.1.3") (h "17k67c2cjqyayirx239cxpb10lryyd79qql0a73zr40hjvvfk52y")))

(define-public crate-unicode-normalization-0.1.4 (c (n "unicode-normalization") (v "0.1.4") (h "1zw6rmgnc7l408s8b9gvr1yw90kwm8zjgvj1iz7vbvpw4rsa73z2")))

(define-public crate-unicode-normalization-0.1.5 (c (n "unicode-normalization") (v "0.1.5") (h "03wj4m38fb6fbjfwibl6pzwmasdy7fwgks4ibppzg8zgz6gdmk2i")))

(define-public crate-unicode-normalization-0.1.6 (c (n "unicode-normalization") (v "0.1.6") (h "0dj66l7f003w9aambhr3h0jqqr5sdh1629qg325b1nxh278n5mlh")))

(define-public crate-unicode-normalization-0.1.7 (c (n "unicode-normalization") (v "0.1.7") (h "09da6yln74ghdpjg76cpgzzwmi4mrks138dzh9q9hnpwc6y800ba")))

(define-public crate-unicode-normalization-0.1.8 (c (n "unicode-normalization") (v "0.1.8") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "09i49va90rvia1agvgni4gicnqv50y5zy1naw8mr8bcqifh3j4ql")))

(define-public crate-unicode-normalization-0.1.9 (c (n "unicode-normalization") (v "0.1.9") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1kviyqg3bmds4p5hgwf9qgihw8xxvq7ljgyrrk7ygxa2k450gj09")))

(define-public crate-unicode-normalization-0.1.10 (c (n "unicode-normalization") (v "0.1.10") (d (list (d (n "smallvec") (r "^1.0") (d #t) (k 0)))) (h "1hdjs3jjmnw8nqpxrmyxvfg6kyzip1bqiyingxy9ll6rvx9qvngh")))

(define-public crate-unicode-normalization-0.1.11 (c (n "unicode-normalization") (v "0.1.11") (d (list (d (n "smallvec") (r "^1.0") (d #t) (k 0)))) (h "1kxxb5ndb5dzyp1flajjdxnbwyjw6ml9xvy0pz7b8srjn9ky4qdm")))

(define-public crate-unicode-normalization-0.1.12 (c (n "unicode-normalization") (v "0.1.12") (d (list (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "195gb4fzlgg4g9cv6w057ncpmvvnx30r00w9hj114knhmlmm6yal")))

(define-public crate-unicode-normalization-0.1.13 (c (n "unicode-normalization") (v "0.1.13") (d (list (d (n "tinyvec") (r "^0.3.3") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0xx9k05f82yf5wmhniq6nsplvszb4536kpv2l606m37sd7vrrcbg") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.14 (c (n "unicode-normalization") (v "0.1.14") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1inxxq5kas5p8dxcpfb2lpyf68bxzyzzk4ik6h6p6kyqlikqxydp") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.15 (c (n "unicode-normalization") (v "0.1.15") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0w2mgvxxkavxyi5zicnya8jm5cqb8kqxrcy72zxbc65s3nvs1sgi") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.16 (c (n "unicode-normalization") (v "0.1.16") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "01p6mwhrf8c748ad7nd5pf9c6djwsc45874dwpp2mqyvcamn6gm1") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.17 (c (n "unicode-normalization") (v "0.1.17") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1vzajca1hk9plka5d3z3426frmnr7229fd2kidz58zd9r3hzryq7") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.18 (c (n "unicode-normalization") (v "0.1.18") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0jbbri6gkyljg35bacgf5cs8r56g78zxf42f06bz8xy8gb57sw9k") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.19 (c (n "unicode-normalization") (v "0.1.19") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1yabhmg8zlcksda3ajly9hpbzqgbhknxwch8dwkfkaa1569r0ifm") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.20 (c (n "unicode-normalization") (v "0.1.20") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1gbcgz5nqpcn4lq112ss0vf1r6m7yfx5h8f4vrlczf6ahn7ydpl1") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.21 (c (n "unicode-normalization") (v "0.1.21") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1rk0zci96pc26sk21nwk5cdkxb3p6bfani0dhaff2smwyz2bsk45") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.22 (c (n "unicode-normalization") (v "0.1.22") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "08d95g7b1irc578b2iyhzv4xhsa4pfvwsqxcl9lbcpabzkq16msw") (f (quote (("std") ("default" "std"))))))

(define-public crate-unicode-normalization-0.1.23 (c (n "unicode-normalization") (v "0.1.23") (d (list (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1x81a50h2zxigj74b9bqjsirxxbyhmis54kg600xj213vf31cvd5") (f (quote (("std") ("default" "std"))))))

