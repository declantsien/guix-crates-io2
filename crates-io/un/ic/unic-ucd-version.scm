(define-module (crates-io un ic unic-ucd-version) #:use-module (crates-io))

(define-public crate-unic-ucd-version-0.7.0 (c (n "unic-ucd-version") (v "0.7.0") (d (list (d (n "unic-common") (r "^0.7.0") (d #t) (k 0)))) (h "1xlgk4rgyn09nbxb8g06achn1y6gbz2lp5m5wknd1hjkdin5w7yg")))

(define-public crate-unic-ucd-version-0.8.0 (c (n "unic-ucd-version") (v "0.8.0") (d (list (d (n "unic-common") (r "^0.8.0") (d #t) (k 0)))) (h "0469lva28xr4z73iagk8yzpiwjxgkmipca4kcgn74rycvnkzlsvz")))

(define-public crate-unic-ucd-version-0.9.0 (c (n "unic-ucd-version") (v "0.9.0") (d (list (d (n "unic-common") (r "^0.9.0") (d #t) (k 0)))) (h "1i5hnzpfnxkp4ijfk8kvhpvj84bij575ybqx1b6hyigy6wi2zgcn")))

