(define-module (crates-io un ic unic-langid) #:use-module (crates-io))

(define-public crate-unic-langid-0.1.0 (c (n "unic-langid") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0ypkc8a0mrhah44j0ahv6nrybslg530blxn5i904lvywlr8pyv7x")))

(define-public crate-unic-langid-0.2.0 (c (n "unic-langid") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0ymhdfvfqh6fksls404pzg9aa6pkfb5sgqfw4a4vm1dpdxh01jfs")))

(define-public crate-unic-langid-0.3.0 (c (n "unic-langid") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0iipg5v4b3089g60mlh6nqjazmbbd1zqvmmp4m2h086cv6j11sys")))

(define-public crate-unic-langid-0.4.0 (c (n "unic-langid") (v "0.4.0") (d (list (d (n "unic-langid-impl") (r "^0.4") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.1") (d #t) (k 2)))) (h "0zcv9r60k26i07pq3h9jx74s4kclnjlchnmfscqf4cvw2n80bvnc") (f (quote (("macros" "unic-langid-macros") ("default"))))))

(define-public crate-unic-langid-0.4.1 (c (n "unic-langid") (v "0.4.1") (d (list (d (n "unic-langid-impl") (r "^0.4") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.2") (d #t) (k 2)))) (h "0qaplq4yb0cbwrrlyp34dlnj860kv6hmw8288scyvz41nwlpwv44") (f (quote (("macros" "unic-langid-macros") ("default"))))))

(define-public crate-unic-langid-0.4.2 (c (n "unic-langid") (v "0.4.2") (d (list (d (n "unic-langid-impl") (r "^0.4") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.3") (d #t) (k 2)))) (h "15w49acad668cn6l1q29y2s97l5p4514fq99kfkjy5rypiqq8a1k") (f (quote (("macros" "unic-langid-macros") ("default"))))))

(define-public crate-unic-langid-0.5.0 (c (n "unic-langid") (v "0.5.0") (d (list (d (n "unic-langid-impl") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.5") (d #t) (k 2)))) (h "1vvd4hzjjjvd2p17afhcnp2pbym0s6ph18y9s62194sj4nf995qx") (f (quote (("macros" "unic-langid-macros") ("default"))))))

(define-public crate-unic-langid-0.5.2 (c (n "unic-langid") (v "0.5.2") (d (list (d (n "unic-langid-impl") (r "^0.5.2") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.5") (d #t) (k 2)))) (h "092fkl4m9drdjp1w427whiy9411b417ggh7ycrsaq5pw7zw69jg3") (f (quote (("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default"))))))

(define-public crate-unic-langid-0.5.3 (c (n "unic-langid") (v "0.5.3") (d (list (d (n "unic-langid-impl") (r "^0.5.3") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.5") (d #t) (k 2)))) (h "191hg9wf74glmkba1qdy6vf5lh45iidp9abycc5y6rp0nhg1cm1w") (f (quote (("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default")))) (y #t)))

(define-public crate-unic-langid-0.6.0 (c (n "unic-langid") (v "0.6.0") (d (list (d (n "unic-langid-impl") (r "^0.6") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.5") (d #t) (k 2)))) (h "06h26z246pqj241jqi301gzb135hfanwjsh7w8isi8vbw0m53ip8") (f (quote (("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default")))) (y #t)))

(define-public crate-unic-langid-0.6.1 (c (n "unic-langid") (v "0.6.1") (d (list (d (n "unic-langid-impl") (r "^0.6") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.6") (d #t) (k 2)))) (h "033myn0kff57yz3s9nmqadjv7wj45mnmfi8qgkc4ipjjmdjrs83z") (f (quote (("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default"))))))

(define-public crate-unic-langid-0.7.0 (c (n "unic-langid") (v "0.7.0") (d (list (d (n "unic-langid-impl") (r "^0.7") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.7") (d #t) (k 2)))) (h "1j1xdv7wvvhvax3n34sn9dzqiq54r6qy304cz547axzdjdsb9xb5") (f (quote (("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default"))))))

(define-public crate-unic-langid-0.7.1 (c (n "unic-langid") (v "0.7.1") (d (list (d (n "unic-langid-impl") (r "^0.7.1") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.7") (d #t) (k 2)))) (h "08na8dqlf9lskvm5y41mhvy2pl1y38q08zfninzl01i4r8qbadbr") (f (quote (("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default"))))))

(define-public crate-unic-langid-0.8.0 (c (n "unic-langid") (v "0.8.0") (d (list (d (n "unic-langid-impl") (r "^0.8") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.8") (d #t) (k 2)))) (h "1v9m2w3wfq8ahklxzaj5bb5da767219g8m86n4srqxwz2lv13n14") (f (quote (("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default"))))))

(define-public crate-unic-langid-0.9.0 (c (n "unic-langid") (v "0.9.0") (d (list (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 2)))) (h "1rcw8llr3a120qad7rlbv4fb19l744ckxwnx37dhn0qafg6qyckk") (f (quote (("serde" "unic-langid-impl/serde") ("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default"))))))

(define-public crate-unic-langid-0.9.1 (c (n "unic-langid") (v "0.9.1") (d (list (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 2)))) (h "0byg9pqm4vywfx82lcw080sphbgj5z8niq0gz384zd4x4gbrm3rr") (f (quote (("serde" "unic-langid-impl/serde") ("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default"))))))

(define-public crate-unic-langid-0.9.2 (c (n "unic-langid") (v "0.9.2") (d (list (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 2)))) (h "0ygkv6n423diqi6yz6b90hpysyccd29gisbcl6kws776wrcg1ap8") (f (quote (("serde" "unic-langid-impl/serde") ("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default")))) (y #t)))

(define-public crate-unic-langid-0.9.3 (c (n "unic-langid") (v "0.9.3") (d (list (d (n "unic-langid-impl") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 2)))) (h "0yh3107vz8ryrn86gjkwv9m453wv1k64vc34bq67h8xpwzw24xl8") (f (quote (("serde" "unic-langid-impl/serde") ("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default")))) (y #t)))

(define-public crate-unic-langid-0.9.4 (c (n "unic-langid") (v "0.9.4") (d (list (d (n "unic-langid-impl") (r "^0.9.4") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9.4") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9.4") (d #t) (k 2)))) (h "05pm5p3j29c9jw9a4dr3v64g3x6g3zh37splj47i7vclszk251r3") (f (quote (("serde" "unic-langid-impl/serde") ("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default"))))))

(define-public crate-unic-langid-0.9.5 (c (n "unic-langid") (v "0.9.5") (d (list (d (n "unic-langid-impl") (r "^0.9.5") (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9.5") (o #t) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9.5") (d #t) (k 2)))) (h "0i2s024frmpfa68lzy8y8vnb1rz3m9v0ga13f7h2afx7f8g9vp93") (f (quote (("serde" "unic-langid-impl/serde") ("macros" "unic-langid-macros") ("likelysubtags" "unic-langid-impl/likelysubtags") ("default"))))))

