(define-module (crates-io un ic unicode_clusters) #:use-module (crates-io))

(define-public crate-unicode_clusters-0.1.0 (c (n "unicode_clusters") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "09ldc5q2ai4gwl8gn5zihddkdz0jgmal7hxx8996bf0ryac74bnp")))

(define-public crate-unicode_clusters-0.1.1 (c (n "unicode_clusters") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1np647j62bnhlgbrbknvqna1sm00vp7wvw7bwdkh1432h4fmlrax")))

(define-public crate-unicode_clusters-0.1.2 (c (n "unicode_clusters") (v "0.1.2") (d (list (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0g3x43ghx0h5vzfsk3fpycmqcfslsc8ham7qrayllxxq0bwwyvw6")))

