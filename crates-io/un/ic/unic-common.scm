(define-module (crates-io un ic unic-common) #:use-module (crates-io))

(define-public crate-unic-common-0.7.0 (c (n "unic-common") (v "0.7.0") (h "1gvps3vkq26bp3gk4q5ad4wacmndm0hysv71f7czg7ljvrx4m3gz") (f (quote (("unstable") ("default"))))))

(define-public crate-unic-common-0.8.0 (c (n "unic-common") (v "0.8.0") (h "0aivnfj4681m3hziqg1qsn461mn2ip988z28maw8f35d1m146wlj") (f (quote (("unstable") ("default"))))))

(define-public crate-unic-common-0.9.0 (c (n "unic-common") (v "0.9.0") (h "1g1mm954m0zr497dl4kx3vr09yaly290zs33bbl4wrbaba1gzmw0") (f (quote (("unstable") ("default"))))))

