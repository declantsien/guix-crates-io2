(define-module (crates-io un ic unicode_names) #:use-module (crates-io))

(define-public crate-unicode_names-0.1.0 (c (n "unicode_names") (v "0.1.0") (h "11mi6ghlrw6nazx3097f28lps4glzx4hr23vvvd6sccp6cbqx8mr")))

(define-public crate-unicode_names-0.1.1 (c (n "unicode_names") (v "0.1.1") (d (list (d (n "unicode_names_macros") (r "*") (d #t) (k 2)))) (h "07h6y51lkrh2wl4vzq621qnmx00iphhq4ify5hmwgv0rnc84kjbc")))

(define-public crate-unicode_names-0.1.2 (c (n "unicode_names") (v "0.1.2") (d (list (d (n "unicode_names_macros") (r "*") (d #t) (k 2)))) (h "0gy1l1nh0hpfsp4ic8ahhhfy3c4zrhaz6m43cc9nrnn6pyhkmynh")))

(define-public crate-unicode_names-0.1.3 (c (n "unicode_names") (v "0.1.3") (d (list (d (n "unicode_names_macros") (r "*") (d #t) (k 2)))) (h "1ihr5v36blyk32xvm4q4s2cifjcngznnz1qxjxybafzc73nsyw0g")))

(define-public crate-unicode_names-0.1.4 (c (n "unicode_names") (v "0.1.4") (d (list (d (n "unicode_names_macros") (r "*") (d #t) (k 2)))) (h "19q5hyj6zqfhbmkbg7sg0iby6ii6d7nbd96hwa1cz4py3g9zibk3")))

(define-public crate-unicode_names-0.1.5 (c (n "unicode_names") (v "0.1.5") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "unicode_names_macros") (r "*") (d #t) (k 2)))) (h "0gkj5f9qlq3sgfzyys0q9k27h2sdwjb68iyq9r7qilvvf1yjsbsf") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode_names-0.1.6 (c (n "unicode_names") (v "0.1.6") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "unicode_names_macros") (r "*") (d #t) (k 2)))) (h "11wb2mrk5ldgracp6ljvkz6yhzhlpkphd02vwyp87dysm0mjbhyb") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode_names-0.1.7 (c (n "unicode_names") (v "0.1.7") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "unicode_names_macros") (r "*") (d #t) (k 2)))) (h "0qscfivz3yp3780q6qccqggz5nvr28k4pnnl6sdlwnf13xmvki57") (f (quote (("unstable") ("no_std") ("default"))))))

