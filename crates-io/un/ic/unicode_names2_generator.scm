(define-module (crates-io un ic unicode_names2_generator) #:use-module (crates-io))

(define-public crate-unicode_names2_generator-1.0.0 (c (n "unicode_names2_generator") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0nsxaz3c49m0gfjsalmghd0nycs568xapd9pjwx0xc5krakfrl7p") (f (quote (("unstable")))) (r "1.63.0")))

(define-public crate-unicode_names2_generator-1.1.0 (c (n "unicode_names2_generator") (v "1.1.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3, <0.3.21") (d #t) (k 0)))) (h "1xqk069y1kysp8n11n8xar3wqzfs8gm2gclf1ykrk5xyc2mnc3ad") (f (quote (("unstable")))) (r "1.63.0")))

(define-public crate-unicode_names2_generator-1.2.0 (c (n "unicode_names2_generator") (v "1.2.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3, <0.3.21") (d #t) (k 0)))) (h "1rqir07nrg2mhz92p2vvrp1c7413lbbqq9zs6xkbr59y660cdpxn") (f (quote (("unstable")))) (r "1.63.0")))

(define-public crate-unicode_names2_generator-1.2.1 (c (n "unicode_names2_generator") (v "1.2.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3, <0.3.21") (d #t) (k 0)))) (h "0skxb6qrr64p3xq4mvpxw3j6v17s3nj5prc0wl6r7ww03rrnlgq1") (f (quote (("unstable")))) (r "1.63.0")))

(define-public crate-unicode_names2_generator-1.2.2 (c (n "unicode_names2_generator") (v "1.2.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1h16hhz6zxy4m1jmqncdq36c4b9zc1fa7b7za493rzj2l2xvhi7l") (f (quote (("unstable") ("timing" "time") ("default")))) (r "1.63.0")))

