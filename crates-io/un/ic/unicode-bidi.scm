(define-module (crates-io un ic unicode-bidi) #:use-module (crates-io))

(define-public crate-unicode-bidi-0.1.0 (c (n "unicode-bidi") (v "0.1.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "1jga6bcn4bfpbrh604p4am45cdqmz2zs5hy9d765j0zndqh7zl5k")))

(define-public crate-unicode-bidi-0.1.1 (c (n "unicode-bidi") (v "0.1.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "0w27zqqzb8mbd6qxk68s3fllqahghmd658qq34qk4l5apq14bfl6")))

(define-public crate-unicode-bidi-0.1.2 (c (n "unicode-bidi") (v "0.1.2") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "07ndxzn3ch7xmyx9b91c29lpfywym1w146xkpz9jf5n20knhdpqf")))

(define-public crate-unicode-bidi-0.1.3 (c (n "unicode-bidi") (v "0.1.3") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "0fv5w1yg7yg7kcmfqcanz371jp04kzg6xhinq6vlymp9z33b3s8v")))

(define-public crate-unicode-bidi-0.1.4 (c (n "unicode-bidi") (v "0.1.4") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "0g4f936wphv7702d9mh44kjl9yhrd560d0hl1j5ak9n1c6wgqc2a")))

(define-public crate-unicode-bidi-0.1.5 (c (n "unicode-bidi") (v "0.1.5") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "1qg5pg38w2vb1rki6jv8ydjdsn95x8iz2gkx6m0bxkih5i9cgky7")))

(define-public crate-unicode-bidi-0.2.0 (c (n "unicode-bidi") (v "0.2.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "1cvpkjpyr3fi779rljnvziz11xgw615rkz2fwkkk36nl4p703fvp")))

(define-public crate-unicode-bidi-0.2.1 (c (n "unicode-bidi") (v "0.2.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "1yvlzbzlad6dh5b53k2rn5kc9hf3qfmgvk9mfdwir4m5rc5r13a9")))

(define-public crate-unicode-bidi-0.2.2 (c (n "unicode-bidi") (v "0.2.2") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "0wjk63mzjsirnwsf4bwj988b87hfsa8s35ydcl6ac4kwzdn7kx9r")))

(define-public crate-unicode-bidi-0.2.3 (c (n "unicode-bidi") (v "0.2.3") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "0gqbyf6slkgzr14nf6v8dw8a19l5snh6bpms8bpfvzpxdawwxxy1")))

(define-public crate-unicode-bidi-0.2.4 (c (n "unicode-bidi") (v "0.2.4") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "0y5prqsb68l5z3l3l8ql68pbq21yjiy7qxhg6wahy3pxwzri865n")))

(define-public crate-unicode-bidi-0.2.5 (c (n "unicode-bidi") (v "0.2.5") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "0clhiyvqkqpb98sgigrzx6gy0gv9mz956gcwf0dfgh32vpmpi86k")))

(define-public crate-unicode-bidi-0.2.6 (c (n "unicode-bidi") (v "0.2.6") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "14lvj1dw6vizf9g2x65xrwnvdcg3101qq859sh8qcwgcq2pik4ql")))

(define-public crate-unicode-bidi-0.3.0 (c (n "unicode-bidi") (v "0.3.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)))) (h "0k9hgnq4rmwv4611vhx8j6fdzz0jsq28aw4v3cq3ismrzm0vjq15") (f (quote (("with_serde" "serde" "serde_test" "serde_derive") ("default"))))))

(define-public crate-unicode-bidi-0.3.1 (c (n "unicode-bidi") (v "0.3.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)))) (h "00wwjk05a69m1wwvjhfp28x6b8bdzmlkdwvbi25m7qliwry4wkf4") (f (quote (("with_serde" "serde" "serde_test" "serde_derive") ("unstable") ("default"))))))

(define-public crate-unicode-bidi-0.3.2 (c (n "unicode-bidi") (v "0.3.2") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r ">= 0.8, < 2.0") (d #t) (k 2)))) (h "0nbdqn3hq7jr8k0mkmhyr4yq9vn60520c8cvgiqnbn1dfpmijqli") (f (quote (("with_serde" "serde" "serde_derive") ("unstable") ("default"))))))

(define-public crate-unicode-bidi-0.3.3 (c (n "unicode-bidi") (v "0.3.3") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.8, < 2.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r ">= 0.8, < 2.0") (d #t) (k 2)))) (h "0fm4ymh50mykf8pigy8rmwqzlffp7qaq70z7sxf3dp8ff7iw98m6") (f (quote (("with_serde" "serde" "serde_derive") ("unstable") ("default"))))))

(define-public crate-unicode-bidi-0.3.4 (c (n "unicode-bidi") (v "0.3.4") (d (list (d (n "flame") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r ">= 0.8, < 2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r ">= 0.8, < 2.0") (d #t) (k 2)))) (h "1malx8ljgm7v1gbaazkn7iicy5wj0bwcyadj3l727a38ch6bvwj9") (f (quote (("with_serde" "serde") ("unstable") ("flame_it" "flame" "flamer") ("default") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.5 (c (n "unicode-bidi") (v "0.3.5") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "1q07h5kp9jszwf0bkrpa1v5wmv04dv920x3w2xy6pjdikchbxf7f") (f (quote (("with_serde" "serde") ("unstable") ("flame_it" "flame" "flamer") ("default") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.6 (c (n "unicode-bidi") (v "0.3.6") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "11805a54czim5qj0p122lqr42z86bmm73zq6c4y4wykywr14qvr4") (f (quote (("with_serde" "serde") ("unstable") ("std") ("flame_it" "flame" "flamer") ("default" "std") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.7 (c (n "unicode-bidi") (v "0.3.7") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "13v7v8pp7mdqqf0ypk73va78c3b4xzpryvbls9p47nz3cd34008s") (f (quote (("with_serde" "serde") ("unstable") ("std") ("flame_it" "flame" "flamer") ("default" "std") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.8 (c (n "unicode-bidi") (v "0.3.8") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "14p95n9kw9p7psp0vsp0j9yfkfg6sn1rlnymvmwmya0x60l736q9") (f (quote (("with_serde" "serde") ("unstable") ("std") ("hardcoded-data") ("flame_it" "flame" "flamer") ("default" "std" "hardcoded-data") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.9 (c (n "unicode-bidi") (v "0.3.9") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "07dbpldwd8rz21acmhgw1b6xmkacz36fzvg04p1qvxvf2d0bwih0") (f (quote (("with_serde" "serde") ("unstable") ("std") ("hardcoded-data") ("flame_it" "flame" "flamer") ("default" "std" "hardcoded-data") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.10 (c (n "unicode-bidi") (v "0.3.10") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "0n6xjbmzvzxf5dls14h6llf6x249rnxbk3frrdwgvghx5icpainm") (f (quote (("with_serde" "serde") ("unstable") ("std") ("hardcoded-data") ("flame_it" "flame" "flamer") ("default" "std" "hardcoded-data") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.11 (c (n "unicode-bidi") (v "0.3.11") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "0b05m4w6casggsmdsqb8s7vdmdlldhncxk9zy3yh6pnhl6n6hjsj") (f (quote (("with_serde" "serde") ("unstable") ("std") ("hardcoded-data") ("flame_it" "flame" "flamer") ("default" "std" "hardcoded-data") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.12 (c (n "unicode-bidi") (v "0.3.12") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "0fwr07w9xv05cwahsamrkpwh523hxhcfxck9isnqx0vaijb2ql3x") (f (quote (("with_serde" "serde") ("unstable") ("std") ("hardcoded-data") ("flame_it" "flame" "flamer") ("default" "std" "hardcoded-data") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.13 (c (n "unicode-bidi") (v "0.3.13") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "0q0l7rdkiq54pan7a4ama39dgynaf1mnjj1nddrq1w1zayjqp24j") (f (quote (("with_serde" "serde") ("unstable") ("std") ("hardcoded-data") ("flame_it" "flame" "flamer") ("default" "std" "hardcoded-data") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.14 (c (n "unicode-bidi") (v "0.3.14") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "05i4ps31vskq1wdp8yf315fxivyh1frijly9d4gb5clygbr2h9bg") (f (quote (("with_serde" "serde") ("unstable") ("std") ("hardcoded-data") ("flame_it" "flame" "flamer") ("default" "std" "hardcoded-data") ("bench_it"))))))

(define-public crate-unicode-bidi-0.3.15 (c (n "unicode-bidi") (v "0.3.15") (d (list (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "flamer") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=0.8, <2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r ">=0.8, <2.0") (d #t) (k 2)))) (h "0xcdxm7h0ydyprwpcbh436rbs6s6lph7f3gr527lzgv6lw053y88") (f (quote (("with_serde" "serde") ("unstable") ("std") ("hardcoded-data") ("flame_it" "flame" "flamer") ("default" "std" "hardcoded-data") ("bench_it"))))))

