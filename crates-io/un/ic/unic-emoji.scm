(define-module (crates-io un ic unic-emoji) #:use-module (crates-io))

(define-public crate-unic-emoji-0.7.0 (c (n "unic-emoji") (v "0.7.0") (d (list (d (n "unic-emoji-char") (r "^0.7.0") (d #t) (k 0)))) (h "0g0l9p9h27887pw7b9k5zqyz0dh916qn094qwb86h0122afj9ys8")))

(define-public crate-unic-emoji-0.8.0 (c (n "unic-emoji") (v "0.8.0") (d (list (d (n "unic-emoji-char") (r "^0.8.0") (d #t) (k 0)))) (h "1w5v1jph2wac1j7aj5mcc3y9rpxcfm5j5ra8viaxrjn6msi041jp")))

(define-public crate-unic-emoji-0.9.0 (c (n "unic-emoji") (v "0.9.0") (d (list (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "1ywb03m81vwf0y85x79ad63c7j0sdyff4w4yh45x4slnywr3y6bl")))

