(define-module (crates-io un ic unicode-utf8) #:use-module (crates-io))

(define-public crate-unicode-utf8-0.1.1 (c (n "unicode-utf8") (v "0.1.1") (h "10d2l19cipnng1zvhmfcx44d335998cv6jmc7fwl07zb4yn4n81w")))

(define-public crate-unicode-utf8-0.1.2 (c (n "unicode-utf8") (v "0.1.2") (h "1rd98z68mp4szssq19p8pq65331y0q3ndm29b2xscb1p0np5l01z")))

(define-public crate-unicode-utf8-0.1.3 (c (n "unicode-utf8") (v "0.1.3") (h "1789f4fpcgh1dk8wlq3qw6zi3a1zxl13dm60ldsdlwnf9fr30mvg")))

