(define-module (crates-io un ic unic-ucd-name) #:use-module (crates-io))

(define-public crate-unic-ucd-name-0.6.0 (c (n "unic-ucd-name") (v "0.6.0") (d (list (d (n "unic-ucd-core") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-utils") (r "^0.6.0") (d #t) (k 0)))) (h "1qrj2d4ab9b2sjkb1pac1h6vq094x8rkz2lpml17b2mj2vnijzzg")))

(define-public crate-unic-ucd-name-0.7.0 (c (n "unic-ucd-name") (v "0.7.0") (d (list (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "0765y3g9wl3n7jj52g888q3zd9569xqilp9skvr0ryjslrxml022")))

(define-public crate-unic-ucd-name-0.8.0 (c (n "unic-ucd-name") (v "0.8.0") (d (list (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-hangul") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "0qzngpqf72p3b4i51mdds5nrs1cp6cbvzhj5wd9nzswkzqnm86jr")))

(define-public crate-unic-ucd-name-0.9.0 (c (n "unic-ucd-name") (v "0.9.0") (d (list (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-hangul") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "0bj91ydrmhmr6zy4yxsb8f7b617iq5hbywqpvj4i0lxj8mdcb3ww")))

