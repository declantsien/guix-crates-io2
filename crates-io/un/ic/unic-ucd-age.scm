(define-module (crates-io un ic unic-ucd-age) #:use-module (crates-io))

(define-public crate-unic-ucd-age-0.3.0 (c (n "unic-ucd-age") (v "0.3.0") (d (list (d (n "unic-ucd-core") (r "^0.2.0") (d #t) (k 0)))) (h "1xagm5bp7myxq426h2v12wcq84n0fw8n06gyxdbifgjjq64fbis4")))

(define-public crate-unic-ucd-age-0.4.0 (c (n "unic-ucd-age") (v "0.4.0") (d (list (d (n "unic-ucd-core") (r "^0.4.0") (d #t) (k 0)))) (h "0czkwzx1q6zrn6phgz59d3yw1352p4fq1jmh5k5dgcakv7p64af5")))

(define-public crate-unic-ucd-age-0.5.0 (c (n "unic-ucd-age") (v "0.5.0") (d (list (d (n "unic-ucd-core") (r "^0.5.0") (d #t) (k 0)) (d (n "unic-utils") (r "^0.5.0") (d #t) (k 2)))) (h "12rcv7x9kfz2s2c75z3p7smjw7mw3sqxakv86v3rnw4mdq0gn92x")))

(define-public crate-unic-ucd-age-0.6.0 (c (n "unic-ucd-age") (v "0.6.0") (d (list (d (n "unic-char-property") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-ucd-core") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-utils") (r "^0.6.0") (d #t) (k 0)))) (h "1p1h4wqv7cw1wqb44x32nyz2di0llbwmd74wyb7jzgx77n83aygn")))

(define-public crate-unic-ucd-age-0.7.0 (c (n "unic-ucd-age") (v "0.7.0") (d (list (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 0)))) (h "1p2vak6v87ya24r9gwvr9xm9cgd5nv56qflsjl0qq8bbm3rq1s6f")))

(define-public crate-unic-ucd-age-0.8.0 (c (n "unic-ucd-age") (v "0.8.0") (d (list (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 0)))) (h "03kwg829x96hvhpfmsg1k5icnrv68mlxifs0lw40rzkr1f2chmp7")))

(define-public crate-unic-ucd-age-0.9.0 (c (n "unic-ucd-age") (v "0.9.0") (d (list (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 0)))) (h "073727rlfabzhw8m2k7fj8rjy3inrm7w5wkaviqvhimgf7zgv33c")))

