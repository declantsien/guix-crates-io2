(define-module (crates-io un ic unicode-segmentation) #:use-module (crates-io))

(define-public crate-unicode-segmentation-0.0.1 (c (n "unicode-segmentation") (v "0.0.1") (h "0rqn0mvy8pg1b5n7jgcwaxk4c8icyknpamqfynmbapfll5r1p61r")))

(define-public crate-unicode-segmentation-0.1.0 (c (n "unicode-segmentation") (v "0.1.0") (h "1pa6w4yq0scnp0wlhlcfq5jwdbrfh5ng9lgci2bipqz5b813y8fh") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-segmentation-0.1.1 (c (n "unicode-segmentation") (v "0.1.1") (h "0hr75an3vryn2w9fr1y9mfg1vr9gmdscjxf0qjkp8rrzwl6xqz7n") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-segmentation-0.1.2 (c (n "unicode-segmentation") (v "0.1.2") (h "0fvygxsnjap9m5c972ykz7z4x5472wfy6whfdgcfy2qz5byd01dr") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-segmentation-0.1.3 (c (n "unicode-segmentation") (v "0.1.3") (h "1dcqkrlpg3xrn5vshsnbm2jjiwvn41zb7rpzzw2i7c8pxlyl9g63") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.0.0 (c (n "unicode-segmentation") (v "1.0.0") (h "026yvhnhp0xbpiqbdna2vf16q61i9sfb3b6c26wd0dkrq4jsdyvh") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.0.1 (c (n "unicode-segmentation") (v "1.0.1") (h "1pw5vyprd29991ch8znc26821x748zh2zjhz2rkglqqkvz0vvbkv") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.0.3 (c (n "unicode-segmentation") (v "1.0.3") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "1dnlsrjrapsvml2dfaayk3s11bijdrq1q59n1fp7g2ix2z2kclrw") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.1.0 (c (n "unicode-segmentation") (v "1.1.0") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "1wrcdkr7k8fndha3xwp4gpigw4la2cykzfr5ydn2q3lgfn2p44hq") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.2.0 (c (n "unicode-segmentation") (v "1.2.0") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0ikr81rcv3sxg8ifil4av2di32qmwnnz09mfahbaxf029rckq258") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.2.1 (c (n "unicode-segmentation") (v "1.2.1") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "1la46g0f9r9pppv7kdv9k226hiad6ai0za0lpinirzfx2by28q5a") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.3.0 (c (n "unicode-segmentation") (v "1.3.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1a9jqg7rb1yq6w2xc9jgxcs111yk5vxm9afjfvykfnrmzk6z8rqr") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.4.0 (c (n "unicode-segmentation") (v "1.4.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1pyckfjmh07ln27hpjhdw6r0z36p4dy69g8kvdjpqv22fk01am6w") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.5.0 (c (n "unicode-segmentation") (v "1.5.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1mzsxdnv8lzxcddkn6bn0yf2wmamc1gsp1jr5ls7g2zx4mi55xa9") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.6.0 (c (n "unicode-segmentation") (v "1.6.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1h7d48mzpi8hwf5cvnq07warkv86pvapzzzf32hvbjsk20yiagp8") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.7.0 (c (n "unicode-segmentation") (v "1.7.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1bjslc64p8mq8hdrg80sprnprjq7li5b865wv94zz47jcshid1yv") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.7.1 (c (n "unicode-segmentation") (v "1.7.1") (d (list (d (n "bencher") (r ">=0.1.0, <0.2.0") (d #t) (k 2)) (d (n "quickcheck") (r ">=0.7.0, <0.8.0") (d #t) (k 2)))) (h "15n736z0pbj30pj44jb9s9rjavzrmx8v8pzdgsl5yfmfwrxjw3dv") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.8.0 (c (n "unicode-segmentation") (v "1.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0nrqfgxkh00wb5dhl0874z20789i2yjimp6ndgh4ay4yjjd895c8") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.9.0 (c (n "unicode-segmentation") (v "1.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "16gxxda9aya0arcqs9aa9lb31b3i54i34dmyqi6j5xkpszsj123y") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.10.0 (c (n "unicode-segmentation") (v "1.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0nks0izrlfrc26hkbs3v3rdjk7lkrflffv2ajhgf0gbql19g1nqg") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.10.1 (c (n "unicode-segmentation") (v "1.10.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0dky2hm5k51xy11hc3nk85p533rvghd462b6i0c532b7hl4j9mhx") (f (quote (("no_std"))))))

(define-public crate-unicode-segmentation-1.11.0 (c (n "unicode-segmentation") (v "1.11.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "00kjpwp1g8fqm45drmwivlacn3y9jx73bvs09n6s3x73nqi7vj6l") (f (quote (("no_std"))))))

