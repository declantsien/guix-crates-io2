(define-module (crates-io un ic unicode-xid) #:use-module (crates-io))

(define-public crate-unicode-xid-0.0.1 (c (n "unicode-xid") (v "0.0.1") (h "10kzfhcw24p44aiz83vjm2vfnlxyhap15z4z8wa74frhrrvpsz65") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-xid-0.0.2 (c (n "unicode-xid") (v "0.0.2") (h "1ya4w67f90jsjxvzh4n8k2jkvyk1p6xh94v020bnaqhrasi0d5gn") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-xid-0.0.3 (c (n "unicode-xid") (v "0.0.3") (h "1sqvl06884cy3hh14shik5afcv6bhsvb0gh2y267rv5lmyfg1prn") (f (quote (("no_std") ("default"))))))

(define-public crate-unicode-xid-0.0.4 (c (n "unicode-xid") (v "0.0.4") (h "1p5l9h3n3i53cp95fb65p8q3vbwib79ryd9z5z5h5kr9gl6qc7wc") (f (quote (("no_std") ("default") ("bench"))))))

(define-public crate-unicode-xid-0.1.0 (c (n "unicode-xid") (v "0.1.0") (h "1z57lqh4s18rr4x0j4fw4fmp9hf9346h0kmdgqsqx0fhjr3k0wpw") (f (quote (("no_std") ("default") ("bench"))))))

(define-public crate-unicode-xid-0.2.0 (c (n "unicode-xid") (v "0.2.0") (h "0z09fn515xm7zyr0mmdyxa9mx2f7azcpv74pqmg611iralwpcvl2") (f (quote (("no_std") ("default") ("bench"))))))

(define-public crate-unicode-xid-0.2.1 (c (n "unicode-xid") (v "0.2.1") (h "0r6mknipyy9vpz8mwmxvkx65ff2ha1n2pxqjj6f46lcn8yrhpzpp") (f (quote (("no_std") ("default") ("bench"))))))

(define-public crate-unicode-xid-0.2.2 (c (n "unicode-xid") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1wrkgcw557v311dkdb6n2hrix9dm2qdsb1zpw7pn79l03zb85jwc") (f (quote (("no_std") ("default") ("bench"))))))

(define-public crate-unicode-xid-0.2.3 (c (n "unicode-xid") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "015zxvwk0is9ls3v016krn5gpr5rk5smyzg6c9j58439ckrm2zlm") (f (quote (("no_std") ("default") ("bench")))) (r "1.17")))

(define-public crate-unicode-xid-0.2.4 (c (n "unicode-xid") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "131dfzf7d8fsr1ivch34x42c2d1ik5ig3g78brxncnn0r1sdyqpr") (f (quote (("no_std") ("default") ("bench")))) (r "1.17")))

