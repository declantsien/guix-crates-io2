(define-module (crates-io un ic unicom) #:use-module (crates-io))

(define-public crate-unicom-0.1.0 (c (n "unicom") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "14c16m8yhr4w318h939dhh5n0xw0vszavwci84nncaklpxhy1bs0") (f (quote (("rustdoc" "futures") ("futures" "futures-io"))))))

(define-public crate-unicom-0.1.1 (c (n "unicom") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0vd8x6hp65jyz688cxwv4qn394agamam2s7b9nw1m66chcnbf6gi") (f (quote (("rustdoc" "futures") ("futures" "futures-io"))))))

(define-public crate-unicom-0.2.0 (c (n "unicom") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1a9xw3ixvghy27634n690cqc9k61zv793hbfb86p23rjagxa6vs5") (f (quote (("rustdoc" "futures") ("futures" "futures-io"))))))

(define-public crate-unicom-0.4.0 (c (n "unicom") (v "0.4.0") (d (list (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0ppljj6227z8v0wsfjzzss8yvs8qax3xqifj31p6dbkfbl8fkd3j") (f (quote (("rustdoc" "tokio") ("async" "futures-io"))))))

