(define-module (crates-io un ic unicom-unix) #:use-module (crates-io))

(define-public crate-unicom-unix-0.1.0 (c (n "unicom-unix") (v "0.1.0") (d (list (d (n "async-std-rs") (r "^1") (o #t) (d #t) (k 0) (p "async-std")) (d (n "tokio-rs") (r "^0.2") (f (quote ("uds"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "unicom") (r "^0.1.0") (d #t) (k 0)))) (h "1gn4yrq8vvr7giqjlgl7is0ampmlc69sc5gxxys3rwib20pg69gv") (f (quote (("tokio" "tokio-rs" "unicom/tokio") ("rustdoc" "async-std") ("async-std" "async-std-rs" "unicom/async-std"))))))

(define-public crate-unicom-unix-0.1.1 (c (n "unicom-unix") (v "0.1.1") (d (list (d (n "async-std-rs") (r "^1") (o #t) (d #t) (k 0) (p "async-std")) (d (n "tokio-rs") (r "^0.2") (f (quote ("uds"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "unicom") (r "^0.1") (d #t) (k 0)))) (h "0l1hjccg0g2yzsvkw905zqpkig0jkng6w8vbhc0593zinc23gwzc") (f (quote (("tokio" "tokio-rs" "unicom/tokio") ("rustdoc" "async-std") ("async-std" "async-std-rs" "unicom/async-std"))))))

(define-public crate-unicom-unix-0.2.0 (c (n "unicom-unix") (v "0.2.0") (d (list (d (n "async-std-rs") (r "^1") (o #t) (d #t) (k 0) (p "async-std")) (d (n "tokio-rs") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "unicom") (r "^0.2.0") (d #t) (k 0)))) (h "1j0lk8fbap91jddpv6qx6mc1jxm7qxd2pypmsv140j6dw9z2qgl7") (f (quote (("tokio" "tokio-rs" "unicom/tokio") ("rustdoc" "async-std") ("async-std" "async-std-rs" "unicom/async-std"))))))

(define-public crate-unicom-unix-0.4.0 (c (n "unicom-unix") (v "0.4.0") (d (list (d (n "async-net") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "unicom") (r "^0.4") (d #t) (k 0)))) (h "11fg03x9c23rbfa4p1kzc6mb3wxrlbdpwn6y52v5x2s3jalmnjgv") (f (quote (("rustdoc" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "unicom/tokio") ("async" "dep:async-net" "unicom/async"))))))

