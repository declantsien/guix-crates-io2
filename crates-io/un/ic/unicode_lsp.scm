(define-module (crates-io un ic unicode_lsp) #:use-module (crates-io))

(define-public crate-unicode_lsp-0.1.0 (c (n "unicode_lsp") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "ropey") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.19.0") (f (quote ("proposed"))) (d #t) (k 0)))) (h "0ihwxvpjkb93h9bpisrr84rg0x9kssbnxxna1lrj0k516n2aiw47")))

