(define-module (crates-io un ic unicode_reader) #:use-module (crates-io))

(define-public crate-unicode_reader-0.1.0 (c (n "unicode_reader") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^0.1") (d #t) (k 0)))) (h "10zhrysk4yg6gw71avs0gxl6zbvb8hvipzppvcdmiydgc8b0k4v1")))

(define-public crate-unicode_reader-0.1.1 (c (n "unicode_reader") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1k74cybfrpn1dklljp2rz3jgxgif7lx1l1c46mdldnp9ypl2f6q0")))

(define-public crate-unicode_reader-1.0.0 (c (n "unicode_reader") (v "1.0.0") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0d1hrpj7vcc42nacz6zcks5556dxknb8y25i9j0dnrdyl82x91pq")))

(define-public crate-unicode_reader-1.0.1 (c (n "unicode_reader") (v "1.0.1") (d (list (d (n "smallvec") (r "^1.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "06kp5zvcx5cnans47sc2jk9x7s75qm92pvcp5kcsq9qbd4hr2qsv")))

(define-public crate-unicode_reader-1.0.2 (c (n "unicode_reader") (v "1.0.2") (d (list (d (n "smallvec") (r "^1.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0yf1im563iighkzjpk6bc5dwdfpxy0xzxw70ayb4a2yj0ssl73fn")))

