(define-module (crates-io un ic unic-char) #:use-module (crates-io))

(define-public crate-unic-char-0.6.0 (c (n "unic-char") (v "0.6.0") (d (list (d (n "unic-char-property") (r "^0.6.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.6.0") (d #t) (k 0)))) (h "1h3x3x97w0v72qyavzl6a8vd7p17bxf137w8h6lks6rc4wbs57vm")))

(define-public crate-unic-char-0.7.0 (c (n "unic-char") (v "0.7.0") (d (list (d (n "unic-char-property") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)))) (h "08cv0x62l4xlp48pvhaz21cwxw6dhyaazbxh6y7mg3axv14fx27q") (f (quote (("std" "unic-char-range/std") ("default"))))))

(define-public crate-unic-char-0.8.0 (c (n "unic-char") (v "0.8.0") (d (list (d (n "unic-char-basics") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-property") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.8.0") (d #t) (k 0)))) (h "0b2idd408lh1msj47ghf8kxay33nwaqp46qcc6b4yqdrfyx1rra4") (f (quote (("std" "unic-char-range/std") ("default"))))))

(define-public crate-unic-char-0.9.0 (c (n "unic-char") (v "0.9.0") (d (list (d (n "unic-char-basics") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-property") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.9.0") (d #t) (k 0)))) (h "02mc1c15prvmdkihnp90cdig20068yf5swmsi3q0fh8kpmwxy9dg") (f (quote (("std" "unic-char-range/std") ("default"))))))

