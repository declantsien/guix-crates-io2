(define-module (crates-io un ic unic-normal) #:use-module (crates-io))

(define-public crate-unic-normal-0.1.0 (c (n "unic-normal") (v "0.1.0") (d (list (d (n "unic-ucd-normal") (r "^0.1") (d #t) (k 0)))) (h "05imgk838p9kcszc809cddfwz5j6c07cijc017xl8p8gzajnqmsb")))

(define-public crate-unic-normal-0.1.2 (c (n "unic-normal") (v "0.1.2") (d (list (d (n "unic-ucd-normal") (r "^0.1.2") (d #t) (k 0)))) (h "1ahnyiwjssid8wxc04yjk3ngjmkln0166knr59b69g1gz3k0567h")))

(define-public crate-unic-normal-0.2.0 (c (n "unic-normal") (v "0.2.0") (d (list (d (n "unic-ucd-normal") (r "^0.2.0") (d #t) (k 0)))) (h "01p3mxwrg5c3i15swhw04kvi88rwf3k5lm9y65z1b4dp0848grsb")))

(define-public crate-unic-normal-0.4.0 (c (n "unic-normal") (v "0.4.0") (d (list (d (n "unic-ucd-core") (r "^0.4.0") (d #t) (k 0)) (d (n "unic-ucd-normal") (r "^0.4.0") (d #t) (k 0)))) (h "01c73nnjjg6qlvzjgfzc6jyjfcsxg87f3hyypw742nhg2nazra1d")))

(define-public crate-unic-normal-0.5.0 (c (n "unic-normal") (v "0.5.0") (d (list (d (n "unic-ucd-core") (r "^0.5.0") (d #t) (k 2)) (d (n "unic-ucd-normal") (r "^0.5.0") (d #t) (k 0)))) (h "04vgggiqzskx8n4x38sizcqfh1xfv4dpapryp0mdbyym0z65d9rn")))

(define-public crate-unic-normal-0.6.0 (c (n "unic-normal") (v "0.6.0") (d (list (d (n "unic-ucd-core") (r "^0.6.0") (d #t) (k 2)) (d (n "unic-ucd-normal") (r "^0.6.0") (d #t) (k 0)))) (h "0v017ra9h0y90vlz17kc782qlhr1lgv6szyppijhg30d0chlpvir")))

(define-public crate-unic-normal-0.7.0 (c (n "unic-normal") (v "0.7.0") (d (list (d (n "unic-ucd-normal") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.7.0") (d #t) (k 2)))) (h "11arm0cnqrq4f3niyh3cdrj1vl9zf2ni06xxprg87qi1kp7jzh98")))

(define-public crate-unic-normal-0.8.0 (c (n "unic-normal") (v "0.8.0") (d (list (d (n "unic-ucd-normal") (r "^0.8.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.8.0") (d #t) (k 2)))) (h "1gcy59z6cspwsl6rhi6g2wfvxdd3y65cylrpadycwqfg4938k2jj")))

(define-public crate-unic-normal-0.9.0 (c (n "unic-normal") (v "0.9.0") (d (list (d (n "unic-ucd-normal") (r "^0.9.0") (d #t) (k 0)) (d (n "unic-ucd-version") (r "^0.9.0") (d #t) (k 2)))) (h "0qmsdf7b902mmaslhwww0hzmzqn26mzh7sraphl4dac96p9n97gh")))

