(define-module (crates-io un ic unicode-cli) #:use-module (crates-io))

(define-public crate-unicode-cli-0.1.0 (c (n "unicode-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.1") (d #t) (k 0)) (d (n "unic-ucd") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.2.2") (d #t) (k 0)))) (h "1rxj9mszlgqa5ri93137vm9vawl0krrw847xhgdjwz75b51ky7pm")))

