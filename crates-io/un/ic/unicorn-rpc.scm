(define-module (crates-io un ic unicorn-rpc) #:use-module (crates-io))

(define-public crate-unicorn-rpc-0.0.1 (c (n "unicorn-rpc") (v "0.0.1") (d (list (d (n "nanomsg") (r "^0.5.0") (d #t) (k 0)) (d (n "unicorn-messages") (r "^0.0.1") (d #t) (k 0)))) (h "1cq6182l66c90ba8zg6la360z12pkzanyrxcv4hhzig2mv04ssys") (y #t)))

