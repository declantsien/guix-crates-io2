(define-module (crates-io un ic unicode-id) #:use-module (crates-io))

(define-public crate-unicode-id-0.3.0 (c (n "unicode-id") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0v2f300p1cbjnbvzq6zmxdwzjdpdi65if0961aawiyrxx0mxk1a2") (f (quote (("no_std") ("default") ("bench")))) (r "1.17")))

(define-public crate-unicode-id-0.3.1 (c (n "unicode-id") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "01hmgnnmr4ry9mk3x6cxfpg0a7bxhpwakyl3wpsnnzk22m3dj87s") (f (quote (("no_std") ("default") ("bench")))) (r "1.17")))

(define-public crate-unicode-id-0.3.2 (c (n "unicode-id") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1gpd39v3a0lay8rcvdd8qvgkyisf1kyyvd5c89ja747lfj98vzk9") (f (quote (("no_std") ("default") ("bench")))) (r "1.17")))

(define-public crate-unicode-id-0.3.3 (c (n "unicode-id") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "06kz29hz9hf174qm642cxg9gmcwhf7bqihk6hf600dkb4aa682yp") (f (quote (("no_std") ("default") ("bench")))) (r "1.17")))

(define-public crate-unicode-id-0.3.4 (c (n "unicode-id") (v "0.3.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0gq8mvi7cf4a13r9djb6xcf16jm58kic30kv36856s99cgwdxdmi") (f (quote (("no_std") ("default") ("bench")))) (r "1.17")))

