(define-module (crates-io un ic unicase_collections) #:use-module (crates-io))

(define-public crate-unicase_collections-0.1.0 (c (n "unicase_collections") (v "0.1.0") (d (list (d (n "unicase") (r "^2.7.0") (d #t) (k 0)))) (h "02j9q7d1n4jbaih2666zg1pgdf5ncchiqicdfralpsl9729vjpm4")))

(define-public crate-unicase_collections-0.1.1 (c (n "unicase_collections") (v "0.1.1") (d (list (d (n "unicase") (r "^2.7.0") (d #t) (k 0)))) (h "0qf2xwdkc8097nfw4143mn7czcyfrar8c8byfhdxamjfh9frm23m")))

(define-public crate-unicase_collections-0.2.0 (c (n "unicase_collections") (v "0.2.0") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "unicase") (r "^2.7.0") (d #t) (k 0)))) (h "0z73ngjkw9dfnyw29ghc8gzkvjw8znr6la2k1kimvfiwhkflddq8")))

(define-public crate-unicase_collections-0.2.1 (c (n "unicase_collections") (v "0.2.1") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "unicase") (r "^2.7.0") (d #t) (k 0)))) (h "06nxkf3jgqi7pi1a21fbspbbkqak698lzflzs4h37q722dkx7g9f")))

(define-public crate-unicase_collections-0.3.0 (c (n "unicase_collections") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "unicase") (r "^2.7.0") (d #t) (k 0)))) (h "0m7q8x8bajkp2bmrkbwngbsvc10sbp8qg0gcg6csjp2xi8r7mbxx") (f (quote (("bench"))))))

