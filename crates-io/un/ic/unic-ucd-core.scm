(define-module (crates-io un ic unic-ucd-core) #:use-module (crates-io))

(define-public crate-unic-ucd-core-0.1.0 (c (n "unic-ucd-core") (v "0.1.0") (h "167w720cqbfsspyq900kz6rygfds2c0096va5022v2jrhxm1bv92") (f (quote (("unstable") ("default"))))))

(define-public crate-unic-ucd-core-0.1.1 (c (n "unic-ucd-core") (v "0.1.1") (h "15mbhnaf2h5268159b6y5af3ki5d3n3x1js5dicky26cazzvh535")))

(define-public crate-unic-ucd-core-0.2.0 (c (n "unic-ucd-core") (v "0.2.0") (h "0mkl99g4y96gqw25398fhbipad086zfh6wpn2i1x9sfz5zvd4vqi")))

(define-public crate-unic-ucd-core-0.4.0 (c (n "unic-ucd-core") (v "0.4.0") (h "012w8ipvaqw6ainaqm3qmcp4d1ay932408nz1n06rnaxxnhyvc6x")))

(define-public crate-unic-ucd-core-0.5.0 (c (n "unic-ucd-core") (v "0.5.0") (h "1l72xkib40rgggcby6y5zkzqqb5l6vdxzasi6k2n823mwkk4h2qw")))

(define-public crate-unic-ucd-core-0.6.0 (c (n "unic-ucd-core") (v "0.6.0") (h "02hx6y8f09i2w7s21hz1gabinp4cw8cj66r56l3whv0l7351b246")))

