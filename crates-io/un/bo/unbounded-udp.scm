(define-module (crates-io un bo unbounded-udp) #:use-module (crates-io))

(define-public crate-unbounded-udp-1.0.0 (c (n "unbounded-udp") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.123") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.35.0") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lamg0k8k7w49zb81ixmipwnmm5mvf90jcgcnrcfr7ncylzv3jl4")))

(define-public crate-unbounded-udp-1.0.1 (c (n "unbounded-udp") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.123") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.35.0") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1szfgcj5803lxgcbdgpkfzcy1rhar25yvbhyk9lgyzhx4wfjr0b8")))

