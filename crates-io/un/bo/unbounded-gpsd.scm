(define-module (crates-io un bo unbounded-gpsd) #:use-module (crates-io))

(define-public crate-unbounded-gpsd-0.1.0 (c (n "unbounded-gpsd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ryk61n9k4zcip73ljj58319j27rndpsrsz2l1wblvnpl9226v4a") (y #t)))

(define-public crate-unbounded-gpsd-0.1.1 (c (n "unbounded-gpsd") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qsahi40wdnlnplwhy3fr68x93gk6ymmasa169c3rh1gm09xn22j")))

(define-public crate-unbounded-gpsd-0.2.0 (c (n "unbounded-gpsd") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "052n81hxpklsa2ad3ygclnakiizab6vzmj6skj5hnnw7342p8yd9")))

(define-public crate-unbounded-gpsd-0.3.0 (c (n "unbounded-gpsd") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03jy9sp2y9xm7jxf9wi2x8w5cf6582aaniq0b47iykj8rv1i2a78")))

(define-public crate-unbounded-gpsd-0.4.0 (c (n "unbounded-gpsd") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ffhglbj750393vmcfwcbgj833cx652c2m06rz65gm6y3m93w6fa")))

(define-public crate-unbounded-gpsd-0.4.1 (c (n "unbounded-gpsd") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02hc9qjyigm25gjwh94fhn20h7dla43g7qw2k1v6bbf2hdczw7sh")))

(define-public crate-unbounded-gpsd-0.4.2 (c (n "unbounded-gpsd") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a6kqhmggdgpg67sf47facf0lrl4k7bbjxjqy7pk12z2p80p7frm")))

(define-public crate-unbounded-gpsd-0.4.3 (c (n "unbounded-gpsd") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rq6rf86hagi0zh0ys6i8c5c6hy39jhxc08i5cbnk38mz8rrmh19")))

(define-public crate-unbounded-gpsd-0.4.4 (c (n "unbounded-gpsd") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b3knayrd3ffqiafr07f80mfq8ccavfgwk1w9x1jd3vgzzkkrlr2")))

(define-public crate-unbounded-gpsd-0.5.0 (c (n "unbounded-gpsd") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lk0y46hjaf8vm8nvfv8isswri01qcdg982fkkgngky8zyyrvx8a")))

(define-public crate-unbounded-gpsd-0.5.1 (c (n "unbounded-gpsd") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09hx7q3hrdb7xsx4hl003xkmskg0cx12isww069b1azr431jnrzz")))

