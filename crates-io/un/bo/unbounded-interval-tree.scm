(define-module (crates-io un bo unbounded-interval-tree) #:use-module (crates-io))

(define-public crate-unbounded-interval-tree-0.1.0 (c (n "unbounded-interval-tree") (v "0.1.0") (h "16iklrfybn0fw89469pb78p9002kxxnrzz4kbk9mvck8jlw7ajva")))

(define-public crate-unbounded-interval-tree-0.1.1 (c (n "unbounded-interval-tree") (v "0.1.1") (h "0yl1w47wi49a79z1rh634pmkqcdbn9prqphqknrs6jl662mcg5zh")))

(define-public crate-unbounded-interval-tree-0.2.1 (c (n "unbounded-interval-tree") (v "0.2.1") (h "1h8r8g48qvwhrfmxm6gayf6dvlfga5l41wbqnkvm495bg9q0ysy7")))

(define-public crate-unbounded-interval-tree-0.2.2 (c (n "unbounded-interval-tree") (v "0.2.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "00542xfa36kp27vl28xhs4ix54krilgnjkj08ahh3ylamyajpqfs")))

(define-public crate-unbounded-interval-tree-0.2.3 (c (n "unbounded-interval-tree") (v "0.2.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "16naqq2kdps2ccra3am5d95nmli0ggb6mbal52fvjpvx0x6fagn7")))

(define-public crate-unbounded-interval-tree-1.0.0 (c (n "unbounded-interval-tree") (v "1.0.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0ci3x41m7bciqkl6fb0dswl1h11408swnsj8nx4l8my75p78qysa")))

(define-public crate-unbounded-interval-tree-1.1.0 (c (n "unbounded-interval-tree") (v "1.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "140aawasallimndmbvpja46amlva2a4h3v0cc4qmcl0n5p1hfr5d")))

(define-public crate-unbounded-interval-tree-1.1.1 (c (n "unbounded-interval-tree") (v "1.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0019fz49jia2vb2schcr3mf3dd0wps9dz78k5fjnkhzrz2x8kzad")))

(define-public crate-unbounded-interval-tree-1.1.2 (c (n "unbounded-interval-tree") (v "1.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r0jq4csfx5sbavj89g5iacqcw2ajai511smgzyvc5x61dyrdb0g") (s 2) (e (quote (("serde" "dep:serde"))))))

