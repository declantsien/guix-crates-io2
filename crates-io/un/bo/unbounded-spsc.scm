(define-module (crates-io un bo unbounded-spsc) #:use-module (crates-io))

(define-public crate-unbounded-spsc-0.1.2 (c (n "unbounded-spsc") (v "0.1.2") (d (list (d (n "bounded-spsc-queue") (r "0.2.*") (d #t) (k 0)))) (h "0vrbp3wispbw531ljcsy2cg5db9m856xrydb975dygy3qzcankl4")))

(define-public crate-unbounded-spsc-0.1.3 (c (n "unbounded-spsc") (v "0.1.3") (d (list (d (n "bounded-spsc-queue") (r "0.2.*") (d #t) (k 0)))) (h "18na5sivg34zsgz12arx3acwraplksgc2f137zwxa1q9ixx3rv2m")))

(define-public crate-unbounded-spsc-0.1.4 (c (n "unbounded-spsc") (v "0.1.4") (d (list (d (n "bounded-spsc-queue") (r "0.3.*") (d #t) (k 0)))) (h "159d03gq887avwkv5ppsdx288xgx9nmlkd9x68bk4q37i0xplvjs")))

(define-public crate-unbounded-spsc-0.1.5 (c (n "unbounded-spsc") (v "0.1.5") (d (list (d (n "bounded-spsc-queue") (r "0.4.*") (d #t) (k 0)))) (h "1nzd9b9pbwyz937qcvc9rzgx7nndlc6wz3ll5yzr91dncr3rf38n")))

(define-public crate-unbounded-spsc-0.1.6 (c (n "unbounded-spsc") (v "0.1.6") (d (list (d (n "bounded-spsc-queue") (r "0.4.*") (d #t) (k 0)))) (h "01hq3cnk007kmg9kkay07bshr2s4jrgqp2dn370q5bpf2zy5rlsx")))

(define-public crate-unbounded-spsc-0.1.7 (c (n "unbounded-spsc") (v "0.1.7") (d (list (d (n "bounded-spsc-queue") (r "0.4.*") (d #t) (k 0)))) (h "0vv41j1zhahxcnih74fz7lbadbpzgwf99iyfbm77x7g3np1axsj2")))

(define-public crate-unbounded-spsc-0.1.8 (c (n "unbounded-spsc") (v "0.1.8") (d (list (d (n "bounded-spsc-queue") (r "0.4.*") (d #t) (k 0)))) (h "0fah83yhvc70g33h79wwqdrjrv538hnv1x13m5r0mwxwy6hqfdqy")))

(define-public crate-unbounded-spsc-0.1.9 (c (n "unbounded-spsc") (v "0.1.9") (d (list (d (n "bounded-spsc-queue") (r "0.4.*") (d #t) (k 0)))) (h "1ryv7bl8pqc05slrh17cszzpj9fvdvfa7vmpf9hv60z4h91kxnq0")))

