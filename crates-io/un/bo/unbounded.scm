(define-module (crates-io un bo unbounded) #:use-module (crates-io))

(define-public crate-unbounded-0.0.1 (c (n "unbounded") (v "0.0.1") (h "071slcrjmi0f6378l5lwqml3dgs4lw91rlg9nklwcydma7j8zn5j") (y #t)))

(define-public crate-unbounded-0.0.2 (c (n "unbounded") (v "0.0.2") (h "0j8bhjfzvzzmcl48dnaj4c6cmp0qnayfilh9bfxgzjf9npld189q") (y #t)))

(define-public crate-unbounded-0.0.2022 (c (n "unbounded") (v "0.0.2022") (h "10y265s5miq90jlzg0cjfhab525zkmhgar2cn468f513lad8hq7m") (y #t)))

(define-public crate-unbounded-0.0.3 (c (n "unbounded") (v "0.0.3") (h "1cn2za8cmrnykfhk8kr7x183j8cl2l6g45i3mndsiqs94cn580rq") (y #t)))

(define-public crate-unbounded-0.0.4 (c (n "unbounded") (v "0.0.4") (h "1y3rrg4gz1gfahjm8mmgxhjf02np7n55j4ys23c65hj9bc9cml7b") (y #t)))

(define-public crate-unbounded-0.0.5 (c (n "unbounded") (v "0.0.5") (h "1nbx8nk8xiwb22kv7p34bvw1prr6rl0hszhybx3bsnvg2q696syd") (y #t)))

(define-public crate-unbounded-0.0.6-dev.0 (c (n "unbounded") (v "0.0.6-dev.0") (h "1zsif3rgfljaa2ijz67gabr4q7hw58syyb7fj65i39yagj7qb0ij") (y #t)))

(define-public crate-unbounded-0.0.6 (c (n "unbounded") (v "0.0.6") (h "1igvxmcwvgh9lxzrfx0psnbsdq1hs201ig70d10i73c0ygdllf2c") (y #t)))

(define-public crate-unbounded-0.0.7 (c (n "unbounded") (v "0.0.7") (h "1alr4j1mki3v2b9rma4cxjqrr339q79v18xqkz7xcwsqjcvvk3s6") (y #t)))

(define-public crate-unbounded-0.0.8 (c (n "unbounded") (v "0.0.8") (d (list (d (n "you-can") (r ">=0.0.8-dev") (d #t) (k 0)))) (h "065d310p0y30nadm2vfwan5sdymw2qv2nfq3vg1ppasx8hi64zr7") (y #t)))

(define-public crate-unbounded-0.0.9 (c (n "unbounded") (v "0.0.9") (d (list (d (n "you-can") (r "^0.0.9") (d #t) (k 0)))) (h "1qlmfykhnra72qsqgq2cfn2wi6fvs965xw3fa1csk92mvr6kllfb") (y #t)))

(define-public crate-unbounded-0.0.13 (c (n "unbounded") (v "0.0.13") (d (list (d (n "you-can") (r "^0.0.13") (d #t) (k 0)))) (h "1acczl3zyc6pjhnfwf14j2z6wwxa190248nw4c93fbg8675424wz") (y #t)))

(define-public crate-unbounded-0.0.0-- (c (n "unbounded") (v "0.0.0--") (h "0vfifavymgxn7j2pvw043acannfb6kl36v8hjrza8wj177fsg6p2") (y #t)))

(define-public crate-unbounded-0.0.0-removed (c (n "unbounded") (v "0.0.0-removed") (h "0ha21s9jfj2mbll1rnwida9dpl9rk2l81978hv2sy47fv8ciy6fy") (y #t)))

