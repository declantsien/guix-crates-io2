(define-module (crates-io un bo unbound-sys) #:use-module (crates-io))

(define-public crate-unbound-sys-0.1.0 (c (n "unbound-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (d #t) (k 0)))) (h "1a08wdg713qdwqvmrndclcjrcib77rz73yzgdqp8fhik58vjnqw2")))

(define-public crate-unbound-sys-0.2.0 (c (n "unbound-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.8") (d #t) (k 0)))) (h "1hwrw5q0fa495lysbxy58aadqv7m0j5hvshbrwy4ccqglp409xpk")))

(define-public crate-unbound-sys-0.3.0 (c (n "unbound-sys") (v "0.3.0") (d (list (d (n "libbindgen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)))) (h "0pqbg13skpi3f3s8nzxlxkjgv42xjs1qwkd4k7pvnhc1qy1656xq")))

(define-public crate-unbound-sys-0.4.0 (c (n "unbound-sys") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3.41") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 1)))) (h "0w87ymkwprcnzjyvqcgjawzx7aq7n77jvh3kcr5ngddr1wb7gs8x")))

(define-public crate-unbound-sys-0.5.0 (c (n "unbound-sys") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 1)))) (h "0ma71bh258dwwqjp66f60iy351vnf5b3crp1jq5zvnb292vxi8xg")))

(define-public crate-unbound-sys-0.6.0 (c (n "unbound-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 1)))) (h "0y3g439x43fbwlnqa0brsvjrkpv22pwyh5bj0rqcrqvfvjxj4qad")))

