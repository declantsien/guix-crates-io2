(define-module (crates-io un bo unbound) #:use-module (crates-io))

(define-public crate-unbound-0.1.0 (c (n "unbound") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unbound-sys") (r "^0.1") (d #t) (k 0)))) (h "06c6ka6fbjdgixrz57y6219f1qrsyai8xlnd33qrn4rx5fai4gg3")))

(define-public crate-unbound-0.2.0 (c (n "unbound") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unbound-sys") (r "^0.2") (d #t) (k 0)))) (h "1g0qh6dgxmh5s183z9zb9jkswqnsr2dkg403s7z5mrjf4ir0ksxv")))

(define-public crate-unbound-0.3.0 (c (n "unbound") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unbound-sys") (r "^0.3") (d #t) (k 0)))) (h "038mixa5d16xrrpa3a6ck03bg2rp3q61rrwknfv85ly8rqhwxs8d")))

(define-public crate-unbound-0.4.0 (c (n "unbound") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3.41") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 1)) (d (n "unbound-sys") (r "^0.4") (d #t) (k 0)))) (h "1jmzlzyihf3vmqcxdma5g11b7pq6fff22px5yqmczg2maa22xajx") (y #t)))

(define-public crate-unbound-0.5.0 (c (n "unbound") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 1)) (d (n "unbound-sys") (r "^0.5") (d #t) (k 0)))) (h "0slcp27ikia5vyh44i8z3igi6mj6g1z6d891p4d9jw5k4983d436") (y #t)))

(define-public crate-unbound-0.6.0 (c (n "unbound") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 1)) (d (n "unbound-sys") (r "^0.6") (d #t) (k 0)))) (h "125z3hhikyx4iz0h01dkhh5xy84x8h6h6xgg8f9kcqf32lgb859s") (y #t)))

