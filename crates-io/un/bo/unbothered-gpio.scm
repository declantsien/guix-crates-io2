(define-module (crates-io un bo unbothered-gpio) #:use-module (crates-io))

(define-public crate-unbothered-gpio-0.1.0 (c (n "unbothered-gpio") (v "0.1.0") (d (list (d (n "cancellable-timer") (r "^0.1.0") (d #t) (k 0)) (d (n "gpio") (r "^0.4.1") (d #t) (k 0)))) (h "1h640xh21apap5q29gki5i7vg2m1kq1p9zmjnzzcnr209js17jpg")))

