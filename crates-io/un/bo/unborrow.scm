(define-module (crates-io un bo unborrow) #:use-module (crates-io))

(define-public crate-unborrow-0.1.0 (c (n "unborrow") (v "0.1.0") (h "0b5g3qgaglg0zppkd6vg522pb106ihgm3yk44vp6p5clll5ydjr6")))

(define-public crate-unborrow-0.2.0 (c (n "unborrow") (v "0.2.0") (h "115kllz4mx71j97ddzr788bhxa5x1h296ds6d4vk89j13w5c0kzx")))

(define-public crate-unborrow-0.3.0 (c (n "unborrow") (v "0.3.0") (h "0m9aks8ka9iwr25y1qkbgnly0xhnyp464zmqjwq2p8lkrhdf09bk")))

(define-public crate-unborrow-0.3.1 (c (n "unborrow") (v "0.3.1") (h "0zynygv2c37zcn98n8xw2y5gjiibinsmmlbhbpi8wkwy0agrabp9")))

