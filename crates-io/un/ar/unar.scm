(define-module (crates-io un ar unar) #:use-module (crates-io))

(define-public crate-unar-0.1.1 (c (n "unar") (v "0.1.1") (d (list (d (n "ar") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "10bpc9m9xgwfawpj0c8hkhjrpyhcq0398zkxxz87khxq2i20w3yv")))

(define-public crate-unar-0.1.2 (c (n "unar") (v "0.1.2") (d (list (d (n "ar") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0hvkss9w50madq4pa8xqpfwzjwxpzf8z4zar5w49z5bsih5izzf4")))

