(define-module (crates-io un ar unarc-rs) #:use-module (crates-io))

(define-public crate-unarc-rs-0.1.0 (c (n "unarc-rs") (v "0.1.0") (h "0hqhvxkm8g2s28n2ick1s69f68i9brxgdxbbpfhivp4i12lrx8c7")))

(define-public crate-unarc-rs-0.2.0 (c (n "unarc-rs") (v "0.2.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4.0") (d #t) (k 0)) (d (n "delharc") (r "^0.6.1") (d #t) (k 0)) (d (n "salzweg") (r "^0.1.4") (d #t) (k 0)))) (h "0rkzafj1gc0048frabm4h2zzwq4xc9bniadmm7x8ap4lg254zrk0")))

(define-public crate-unarc-rs-0.3.0 (c (n "unarc-rs") (v "0.3.0") (d (list (d (n "bitstream-io") (r "^2.3.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4.0") (d #t) (k 0)) (d (n "delharc") (r "^0.6.1") (d #t) (k 0)) (d (n "salzweg") (r "^0.1.4") (d #t) (k 0)))) (h "1lvi3z9plsvy6g199f67bm84k1xvvkh8cl0y9sz5b334rklcaxiq")))

(define-public crate-unarc-rs-0.4.0 (c (n "unarc-rs") (v "0.4.0") (d (list (d (n "bitstream-io") (r "^2.3.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4.0") (d #t) (k 0)) (d (n "delharc") (r "^0.6.1") (d #t) (k 0)) (d (n "salzweg") (r "^0.1.4") (d #t) (k 0)))) (h "0jy0qkl2vn8jb71rzk21c5fl3ardkwdhmg6yn99in0qhr2332l2g")))

(define-public crate-unarc-rs-0.4.1 (c (n "unarc-rs") (v "0.4.1") (d (list (d (n "bitstream-io") (r "^2.3.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4.0") (d #t) (k 0)) (d (n "delharc") (r "^0.6.1") (d #t) (k 0)) (d (n "salzweg") (r "^0.1.4") (d #t) (k 0)))) (h "0j9ywsg05s9l8z9g7vsinwwk8z6n8d6a138ap223x3wwd0ngksvp")))

