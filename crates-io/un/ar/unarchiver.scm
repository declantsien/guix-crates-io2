(define-module (crates-io un ar unarchiver) #:use-module (crates-io))

(define-public crate-unarchiver-0.0.1 (c (n "unarchiver") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "env"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "io-util" "rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "12yfjbr3x85ay22nm0dk98mgdaxgr6v544qz7d1mbp05fqbz3s8s")))

(define-public crate-unarchiver-0.0.2 (c (n "unarchiver") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "env"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "io-util" "rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "15ikcsifs07n3w3r520cd7xw65zc346ik5k6kwi4wlc223pscjzc")))

(define-public crate-unarchiver-0.0.3 (c (n "unarchiver") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "env"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "io-util" "rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "0w0d3yzgfqa16wws3aid2mjcar7r5fqs92sqzcv72z6d5bgl2iyn")))

