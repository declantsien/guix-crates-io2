(define-module (crates-io un ar unarray) #:use-module (crates-io))

(define-public crate-unarray-0.1.0 (c (n "unarray") (v "0.1.0") (d (list (d (n "test-strategy") (r "^0.2") (d #t) (k 2)))) (h "0kphkczhfj40h1186b14vc67xn775r2j1z1hixq5khh44bbcj9qr")))

(define-public crate-unarray-0.1.1 (c (n "unarray") (v "0.1.1") (d (list (d (n "test-strategy") (r "^0.2") (d #t) (k 2)))) (h "04726lam2vgpbw9a7937r7wprp8jqdvhyiwjp4ylkyvlsjj36hk3")))

(define-public crate-unarray-0.1.2 (c (n "unarray") (v "0.1.2") (d (list (d (n "test-strategy") (r "^0.2") (d #t) (k 2)))) (h "0qr568jrsqpzpv687xl4wfi1wjqnsj3zawarj4qdivwn8n4pii7m")))

(define-public crate-unarray-0.1.3 (c (n "unarray") (v "0.1.3") (d (list (d (n "test-strategy") (r "^0.2") (d #t) (k 2)))) (h "1wnvz3rjgl1fxyifmi3ad81dnvkgyb39rp765h1m6grg05xxlkg8")))

(define-public crate-unarray-0.1.4 (c (n "unarray") (v "0.1.4") (d (list (d (n "test-strategy") (r "^0.2") (d #t) (k 2)))) (h "154smf048k84prsdgh09nkm2n0w0336v84jd4zikyn6v6jrqbspa")))

