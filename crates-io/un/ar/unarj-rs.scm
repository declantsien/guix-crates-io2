(define-module (crates-io un ar unarj-rs) #:use-module (crates-io))

(define-public crate-unarj-rs-0.1.0 (c (n "unarj-rs") (v "0.1.0") (d (list (d (n "crc32fast") (r "^1.4.0") (d #t) (k 0)) (d (n "delharc") (r "^0.6.1") (d #t) (k 0)))) (h "1h9ln5806sq583mlcd4yr0pkyjc6218pf8zzcagj89d3wpxnwzvq")))

(define-public crate-unarj-rs-0.2.0 (c (n "unarj-rs") (v "0.2.0") (d (list (d (n "bitstream-io") (r "^2.3.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4.0") (d #t) (k 0)) (d (n "delharc") (r "^0.6.1") (d #t) (k 0)))) (h "17jg2xx3vn6cfmw24yv4si268g3m4ky98mjrdv7dwasryv5kzr4w")))

(define-public crate-unarj-rs-0.2.1 (c (n "unarj-rs") (v "0.2.1") (d (list (d (n "bitstream-io") (r "^2.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4.0") (d #t) (k 0)) (d (n "delharc") (r "^0.6.1") (d #t) (k 0)))) (h "0fb8hjmjqr0cjvg8kaansgw7xdgsc5dkjnzyamahmms5wny6pskh")))

