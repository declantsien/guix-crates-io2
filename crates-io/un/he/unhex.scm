(define-module (crates-io un he unhex) #:use-module (crates-io))

(define-public crate-unhex-0.0.0 (c (n "unhex") (v "0.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive" "unicode" "string" "env"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1gkbdfsf4a6w5qwbz7340pgv7szgyi2lxrsxc58mn5ha6sbbfai6")))

