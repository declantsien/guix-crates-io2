(define-module (crates-io un sh unshuffle_lib) #:use-module (crates-io))

(define-public crate-unshuffle_lib-0.1.0 (c (n "unshuffle_lib") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)))) (h "1vz1b373lyds4lpxmx20n6afhf13rsm7p1x314pc82s3h36kqxn5") (y #t)))

(define-public crate-unshuffle_lib-0.1.1 (c (n "unshuffle_lib") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)))) (h "1x8kl9dfbbb1kz5d7zz2y11w135jjwlr74jymgn90hmjksxw0czh") (y #t)))

