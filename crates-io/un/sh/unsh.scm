(define-module (crates-io un sh unsh) #:use-module (crates-io))

(define-public crate-unsh-0.1.0 (c (n "unsh") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "rustyline") (r "^8.1.0") (d #t) (k 0)) (d (n "shlex") (r "^1.0.0") (d #t) (k 0)))) (h "0dm0ycbxrsmamcw6gmhvqfizlgvx211c35k7dysbj79s0r6jdwxq")))

