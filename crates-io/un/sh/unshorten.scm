(define-module (crates-io un sh unshorten) #:use-module (crates-io))

(define-public crate-unshorten-0.1.0 (c (n "unshorten") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "urlexpand") (r "^0.2.3") (d #t) (k 0)))) (h "1iij5xxzqi3ywj808swfhlgkvp00kpmshk2nv2djl223661fygdp") (y #t)))

(define-public crate-unshorten-0.1.1 (c (n "unshorten") (v "0.1.1") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "urlexpand") (r "^0.2.3") (d #t) (k 0)))) (h "1x2l8m3ixp8hgiqbz0nr5qzzl28xfpqps9pc3zgy2xrifbwmjmag")))

