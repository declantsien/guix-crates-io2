(define-module (crates-io un sh unshare_petbox) #:use-module (crates-io))

(define-public crate-unshare_petbox-0.7.1 (c (n "unshare_petbox") (v "0.7.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1sf5k9m9ncgk3fz8ys7gjrl7airvl5smwlmg5pwmwfjw8jfh8hnc")))

