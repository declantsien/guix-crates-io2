(define-module (crates-io un sh unshare) #:use-module (crates-io))

(define-public crate-unshare-0.0.1 (c (n "unshare") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)))) (h "0j5gani21w3gjg96h7c2c2xjlywsq6rkmpy38g9wj5i93zp37n76")))

(define-public crate-unshare-0.1.0 (c (n "unshare") (v "0.1.0") (d (list (d (n "argparse") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)))) (h "0wg6n8j9f5i6hgrwmvcnhzm9dz4nvr65ah67yiyg0j4jksvvn2m4")))

(define-public crate-unshare-0.1.1 (c (n "unshare") (v "0.1.1") (d (list (d (n "argparse") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)))) (h "1mgg0wznqldr96z3knp1synp5lbf0z0hkjczzywb0jq6dx604rzl")))

(define-public crate-unshare-0.1.2 (c (n "unshare") (v "0.1.2") (d (list (d (n "argparse") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)))) (h "10ahpvg9c5k9qgd07wr8i7l8h64n3jlqznib48h3hc93c8rsg9vb")))

(define-public crate-unshare-0.1.3 (c (n "unshare") (v "0.1.3") (d (list (d (n "argparse") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)))) (h "1samh94bdgcr382ck8dzq3232ij771bxxqzj4cg9ih4nw161z09f")))

(define-public crate-unshare-0.1.4 (c (n "unshare") (v "0.1.4") (d (list (d (n "argparse") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)))) (h "1xb70d93jnfnbvvax76g48ar5a0araq92blhkmim7hjlffnxxr9y")))

(define-public crate-unshare-0.1.5 (c (n "unshare") (v "0.1.5") (d (list (d (n "argparse") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "0c69d094060bx3hyfzsfp4ic0z3a2bamaghvklawf0y8k0rc3f9z")))

(define-public crate-unshare-0.1.6 (c (n "unshare") (v "0.1.6") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "1pr82fmdh2rzbk05dwgqn6qdmcv1110p0ksiqzf0n40w0adivyax")))

(define-public crate-unshare-0.1.7 (c (n "unshare") (v "0.1.7") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "1m3p0zxwdlvf12nw297d3h3yvik32mb5i4ddhq514pa7kgb7mrwr")))

(define-public crate-unshare-0.1.8 (c (n "unshare") (v "0.1.8") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "1i7xzp32habbx94wr1mclalajman4n3mpysx5jkj6arifs7x77xg") (y #t)))

(define-public crate-unshare-0.1.9 (c (n "unshare") (v "0.1.9") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "1gh4gi5bnxqq00m0mvmcikw72ljkd78jcdlsrhxzbnwipr3y5b45")))

(define-public crate-unshare-0.1.10 (c (n "unshare") (v "0.1.10") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "0ydp6jgxg0hphb6vhqa73igyr0k7mybmmn217lvhgjc4gc4g2kl4")))

(define-public crate-unshare-0.1.11 (c (n "unshare") (v "0.1.11") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "1c1s8iqhi4d5vwa0z00xs07gbm46f6yiw433glwhzmqnj0zxpqcz")))

(define-public crate-unshare-0.1.12 (c (n "unshare") (v "0.1.12") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "08fkhizyxi21zlcrkr1093zlbhxrccc7r93kcyxpm42c8phwwwf3")))

(define-public crate-unshare-0.1.13 (c (n "unshare") (v "0.1.13") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "007qppspldlqpff3qix6zn8d0wls2ri6h1bc7s3l65qyc8py3ln8")))

(define-public crate-unshare-0.1.14 (c (n "unshare") (v "0.1.14") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "1dj3w3rmdn8vwkihbcznaaij47sslnf8ah72ax5q91zj1z9g7zl3")))

(define-public crate-unshare-0.1.15 (c (n "unshare") (v "0.1.15") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "176h8sb4imba8pcghqz9ypgqc12xn899d0vh6bigj5fkwphi6vv3")))

(define-public crate-unshare-0.2.0 (c (n "unshare") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "0haw9djqhyfrjzvffia8kzwyibmmrsqsn0ivc1gc4bwz5zx4rs0z")))

(define-public crate-unshare-0.3.0 (c (n "unshare") (v "0.3.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1xsaqypfdxcdfnipv2kyi94bvs432h6qbykw52nr45vflbp2cznf")))

(define-public crate-unshare-0.4.0 (c (n "unshare") (v "0.4.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0wszxd9ws4i4bl80yr2v92zfsb4qgpd9jq25hjmmzyg4zqf7bl93")))

(define-public crate-unshare-0.5.0 (c (n "unshare") (v "0.5.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "08f1spik8kq369smn9jpw3ck2wza5zr1rl0ikn3qp6i0arhjz6jb")))

(define-public crate-unshare-0.5.1 (c (n "unshare") (v "0.5.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "04p0c8nd7l7c6l3gi2a02x3ymazgn68nmzrfrys0xcr10mlw5ljq")))

(define-public crate-unshare-0.6.0 (c (n "unshare") (v "0.6.0") (d (list (d (n "argparse") (r ">=0.2.1, <0.3.0") (d #t) (k 2)) (d (n "libc") (r ">=0.2.26, <0.3.0") (d #t) (k 0)) (d (n "nix") (r ">=0.11.0, <0.12.0") (d #t) (k 0)) (d (n "rand") (r ">=0.4.2, <0.5.0") (d #t) (k 2)))) (h "08d8nj5p165jrl4vaq9f6ivqdpq1f32wskciwmji2x8n4b1lzz4z")))

(define-public crate-unshare-0.7.0 (c (n "unshare") (v "0.7.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "16vvivmj7r64ymxc0grqgv4bjxmdaiv26j57z24xl7iaanas5v8w")))

