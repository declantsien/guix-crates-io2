(define-module (crates-io un sh unshield) #:use-module (crates-io))

(define-public crate-unshield-0.1.0 (c (n "unshield") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "explode") (r "^0.1") (d #t) (k 0)))) (h "1zaqhqdsiw9f8bnc92apxmmj2vm44mcd9mvx6hpprs3qvbpl8mrv")))

(define-public crate-unshield-0.2.0 (c (n "unshield") (v "0.2.0") (d (list (d (n "async-executor") (r "^0.1") (k 2)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "explode") (r "^0.1") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0p3c84468qkdaa4c619x6wlk421wdgc6j8a0vsvp99nqzz19plkd") (f (quote (("default") ("async" "futures-lite"))))))

