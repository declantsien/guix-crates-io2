(define-module (crates-io un it unit-wasm-sys) #:use-module (crates-io))

(define-public crate-unit-wasm-sys-0.1.0 (c (n "unit-wasm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.43") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1j76zp6z2xsx0bnvw5ja1phy5076vqyv58nm3b6k83a3l86fiyq7") (y #t) (l "unit-wasm")))

(define-public crate-unit-wasm-sys-0.1.1 (c (n "unit-wasm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "0hmshh4akjvp5i0v87j26dv864zg71x9bn0p5q60phd5yfshbilf") (l "unit-wasm")))

(define-public crate-unit-wasm-sys-0.1.2 (c (n "unit-wasm-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1ca9ihf9mpqv50yjw5crnzp91ixsna8ify4acaf3m3v8cfdibc86") (l "unit-wasm")))

(define-public crate-unit-wasm-sys-0.1.3 (c (n "unit-wasm-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1chsnvj5mv742rhmwm3gx2q41rllszcdxm3xgfsxxzk1mr7vinyy") (l "unit-wasm")))

(define-public crate-unit-wasm-sys-0.1.4 (c (n "unit-wasm-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "0ryhcr5xl6f4257bcbfr8mas7bsxxyy7vl27x7lwvdn4swrhgc6p") (l "unit-wasm")))

(define-public crate-unit-wasm-sys-0.2.0 (c (n "unit-wasm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "17141gf50js0is666lqh6lzh21vxybfa8wqyv00f7y6ln2hn925r") (l "unit-wasm")))

(define-public crate-unit-wasm-sys-0.3.0 (c (n "unit-wasm-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "177v8j5xnzksfaw7jf2rml70p8z9l7p9c67kd7nzk7y8wkk5v71s") (l "unit-wasm")))

