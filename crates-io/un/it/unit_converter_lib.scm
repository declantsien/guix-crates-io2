(define-module (crates-io un it unit_converter_lib) #:use-module (crates-io))

(define-public crate-unit_converter_lib-0.1.0 (c (n "unit_converter_lib") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "1mzy3vagqqws4rly6b69fc1iarv7rnd58diaxyj5s09j00c05zjx")))

