(define-module (crates-io un it unit_converter) #:use-module (crates-io))

(define-public crate-unit_converter-0.1.0 (c (n "unit_converter") (v "0.1.0") (d (list (d (n "read_input") (r "^0.8.2") (d #t) (k 0)))) (h "0hnsgv9yx5czvbj7vkzxv2g01p7zcffigniq1kpbsh0dlmpqqmb6")))

(define-public crate-unit_converter-0.1.1 (c (n "unit_converter") (v "0.1.1") (d (list (d (n "read_input") (r "^0.8.2") (d #t) (k 0)))) (h "14xdmvdcf92ndsdqma1r7cz06wrn2hyvpbkxqyrrw8ijny93p28x")))

(define-public crate-unit_converter-0.1.2 (c (n "unit_converter") (v "0.1.2") (d (list (d (n "read_input") (r "^0.8.2") (d #t) (k 0)))) (h "006hxyq6f2r9szizmw7j6sgcng6413vrc4n9jjf5pd1b6r5gfy77")))

(define-public crate-unit_converter-0.1.3 (c (n "unit_converter") (v "0.1.3") (d (list (d (n "read_input") (r "^0.8.2") (d #t) (k 0)))) (h "0wjhj1cn3x8k9zkkh721syvf8pigss6fmb9ks86mz5p389x5dd7s")))

(define-public crate-unit_converter-0.1.4 (c (n "unit_converter") (v "0.1.4") (d (list (d (n "read_input") (r "^0.8.2") (d #t) (k 0)))) (h "095m5y063asw87bqs71krcjb85va6r4i7w4a0jlz18vcmsc07xf5")))

(define-public crate-unit_converter-0.1.5 (c (n "unit_converter") (v "0.1.5") (d (list (d (n "read_input") (r "^0.8.2") (d #t) (k 0)))) (h "05jpgdvzq7m152qzvmnbj1xa5dv3naq5qmdq98j949vi46gxkwrr")))

(define-public crate-unit_converter-0.1.6 (c (n "unit_converter") (v "0.1.6") (d (list (d (n "read_input") (r "^0.8.2") (d #t) (k 0)))) (h "11mz01lmfpsl64dj0d5qy64035vpy054pfl03dacf3brmk9jkbl6")))

