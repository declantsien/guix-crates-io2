(define-module (crates-io un it unit-rs) #:use-module (crates-io))

(define-public crate-unit-rs-0.1.0 (c (n "unit-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "16i3hizzwm9bcxifac80mgw8nn2mif1hk16m6ijcq5k572iv6jja")))

(define-public crate-unit-rs-0.1.1 (c (n "unit-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "15yadkkymic8z54zvblxszm8wwz1jd3m9f9rljdnmq35k685cf06")))

(define-public crate-unit-rs-0.2.0-alpha1 (c (n "unit-rs") (v "0.2.0-alpha1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "http") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "0f9zz3ghnv343lnliihj8qadsswl9k0qw5a9fbqqqq8nw5dqzvyh") (f (quote (("default")))) (s 2) (e (quote (("http" "dep:http"))))))

(define-public crate-unit-rs-0.2.0-alpha3 (c (n "unit-rs") (v "0.2.0-alpha3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "http") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "12fqilq42y8jk44m3i8cwp871nadw4m4qf23rqi59f0kgxxgk1ff") (f (quote (("default" "http")))) (s 2) (e (quote (("http" "dep:http"))))))

(define-public crate-unit-rs-0.2.0 (c (n "unit-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "http") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "0gyhwh8xfx5vism95k8m17bns5y3681m4lsyv83dy1624g70rlkx") (f (quote (("default" "http")))) (s 2) (e (quote (("http" "dep:http"))))))

