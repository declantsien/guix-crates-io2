(define-module (crates-io un it unitig_flipper) #:use-module (crates-io))

(define-public crate-unitig_flipper-0.1.0 (c (n "unitig_flipper") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "jseqio") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1awq1px05n8s2q62m2kgglz1967q1b80qgpjmjbrqb3zxrv9lr3g")))

