(define-module (crates-io un it unity-utils) #:use-module (crates-io))

(define-public crate-unity-utils-0.0.0 (c (n "unity-utils") (v "0.0.0") (d (list (d (n "fast-walker") (r "^0.1.2") (d #t) (k 0)) (d (n "gen-iter") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "trash") (r "^3.0.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1g6qs1l467y4lf6c03z8lmjsqn0n8hfgczfnj7kg2pmpnh6m7zp7") (f (quote (("default"))))))

