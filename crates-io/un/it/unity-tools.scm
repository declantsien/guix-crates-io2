(define-module (crates-io un it unity-tools) #:use-module (crates-io))

(define-public crate-unity-tools-0.0.0 (c (n "unity-tools") (v "0.0.0") (d (list (d (n "gen-iter") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "trash") (r "^3.0.1") (d #t) (k 0)) (d (n "unity-utils") (r "^0.0.0") (d #t) (k 0)))) (h "0jp6mfgaxi2hbxgi0gd6qk4ff92fpmvbri0vm2bmayjd53jy7a8z") (f (quote (("default"))))))

