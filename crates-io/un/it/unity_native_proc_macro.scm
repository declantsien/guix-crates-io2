(define-module (crates-io un it unity_native_proc_macro) #:use-module (crates-io))

(define-public crate-unity_native_proc_macro-0.1.0 (c (n "unity_native_proc_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)) (d (n "unity_native_sys") (r "^0.1.0") (d #t) (k 0)))) (h "1maq40d03j21f7y94s7q58p9nbmhbqpy6fpcy1r9rhwccl5inpd3")))

(define-public crate-unity_native_proc_macro-0.1.1 (c (n "unity_native_proc_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)) (d (n "unity_native_sys") (r "^0.1.1") (d #t) (k 0)))) (h "0cxnh8vckdgahqm73f2dcd0q6fi2vq8dv9z6f3l1wgbqi5kibzf5")))

