(define-module (crates-io un it unity) #:use-module (crates-io))

(define-public crate-unity-0.1.0 (c (n "unity") (v "0.1.0") (h "00yidc3xr15cgwwiidxhx5bfyfhhn3jwx5hbis83nj6cira5sk2b")))

(define-public crate-unity-0.2.0 (c (n "unity") (v "0.2.0") (d (list (d (n "kondo-lib") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0dwflxgfgwgq52wjs8firryf4dzhd28c71jx791in803jc708ln8")))

