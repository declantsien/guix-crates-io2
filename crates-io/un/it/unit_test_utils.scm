(define-module (crates-io un it unit_test_utils) #:use-module (crates-io))

(define-public crate-unit_test_utils-0.1.0 (c (n "unit_test_utils") (v "0.1.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0j3ssrg1g0ygrdg8lbv2li37yyvwqaznqcj581xdi962zcnxslx0")))

(define-public crate-unit_test_utils-0.1.1 (c (n "unit_test_utils") (v "0.1.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "13z00rrgjjydfjmrdjvyafakvd9qss139rbkf5j1bmyq7hg7r5zi")))

(define-public crate-unit_test_utils-0.1.2 (c (n "unit_test_utils") (v "0.1.2") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "09q6qnn20ncn3bnc9wjf03p33aiv6fr665ggrk31cvv63jwvdj2p")))

(define-public crate-unit_test_utils-0.1.3 (c (n "unit_test_utils") (v "0.1.3") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "00bcgj061n8bdf23g8mhwd2q265dgsh6z8lpb01sykfhmgr7dvhb")))

