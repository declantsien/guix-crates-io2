(define-module (crates-io un it unity-unpacker) #:use-module (crates-io))

(define-public crate-unity-unpacker-0.1.0 (c (n "unity-unpacker") (v "0.1.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tar") (r "^0") (d #t) (k 0)))) (h "1fcgx8s6l8siaj5h2gd5y2zlrsrdx41mfam70ycfk1xryb8y9wl1")))

(define-public crate-unity-unpacker-0.1.1 (c (n "unity-unpacker") (v "0.1.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tar") (r "^0") (d #t) (k 0)))) (h "1c8a612yn9hvyzhahzqvz38g24v2ww3kf8nckd8w3r7j5fvadyl7")))

