(define-module (crates-io un it unit_conversion) #:use-module (crates-io))

(define-public crate-unit_conversion-0.2.0 (c (n "unit_conversion") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1xhgsb0ga0k7ykz6b3nfd93gx92nnv5m3wcm3phjq6dzk7yq00zr")))

