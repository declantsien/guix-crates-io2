(define-module (crates-io un it unitval) #:use-module (crates-io))

(define-public crate-unitval-0.1.0 (c (n "unitval") (v "0.1.0") (d (list (d (n "unitval-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1npnvfg7mcyzw7gbvmjx4s9c20myj455jhlj04gl54q4ii8sx8g3")))

(define-public crate-unitval-0.1.1 (c (n "unitval") (v "0.1.1") (d (list (d (n "unitval-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0myj5swvps7gmpphij179pwrrbc6pzrr7w86scxl2j47645pkdx4") (f (quote (("diesel_postgres" "unitval-derive/diesel_postgres") ("default")))) (y #t)))

(define-public crate-unitval-0.1.2 (c (n "unitval") (v "0.1.2") (d (list (d (n "unitval-derive") (r "^0.1.2") (d #t) (k 0)))) (h "15g8lfrp9p9hnfvp2ayqq8yidqm8d5kmiprwrld6kbb0c9awwzkx")))

(define-public crate-unitval-0.1.3 (c (n "unitval") (v "0.1.3") (d (list (d (n "unitval-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0fans6598h947rs2fw42kw837i7yvn0aimp62yn7pj004c80i9l8")))

(define-public crate-unitval-0.2.0 (c (n "unitval") (v "0.2.0") (d (list (d (n "unitval-derive") (r "^0.1.2") (d #t) (k 0)))) (h "15pxxm0xzva7bhqkm47nqnc0d16j02cd9jhhw9p2abgvmrnnxz8n")))

