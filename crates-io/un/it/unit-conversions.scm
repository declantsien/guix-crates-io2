(define-module (crates-io un it unit-conversions) #:use-module (crates-io))

(define-public crate-unit-conversions-0.1.11-alpha (c (n "unit-conversions") (v "0.1.11-alpha") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "00ia7w6wvvpr0js085sv6zdyj8rfdzj2dy4qc3pqqhzkh8nyqlh2") (y #t)))

(define-public crate-unit-conversions-0.1.11-alpha.1 (c (n "unit-conversions") (v "0.1.11-alpha.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0nlq65xp06i9m8p9zvrjwj34cx44dxb10rwscvwcsj45889d1rly") (y #t)))

(define-public crate-unit-conversions-0.1.11-alpha.2 (c (n "unit-conversions") (v "0.1.11-alpha.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "04hdpfc2rhy4r0b7q5xp2x1lrvg5c9qyk7rh5f0kwsbjbn6lhx19") (y #t)))

(define-public crate-unit-conversions-0.1.11-alpha.3 (c (n "unit-conversions") (v "0.1.11-alpha.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0ykmp4hvajnyaxjcgx04dsr6q6cgqd6q3vlfm1dfvrnr6ykasjrr") (y #t)))

(define-public crate-unit-conversions-0.1.11-alpha.4 (c (n "unit-conversions") (v "0.1.11-alpha.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "005pkl3wh385bw317y9818dmrwxbi2vddgasfj6a2hr3r4gq08qq")))

(define-public crate-unit-conversions-0.1.11-alpha.5 (c (n "unit-conversions") (v "0.1.11-alpha.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "13q4isszwnk6msk11ky43hj4py71grajv19wyjb2rhfv19dw4giy")))

(define-public crate-unit-conversions-0.1.13 (c (n "unit-conversions") (v "0.1.13") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0ha8w7aybylr7a16xzhzr7qwa00hph0nvqav02bgnvjzhgd6c06y")))

(define-public crate-unit-conversions-0.1.15 (c (n "unit-conversions") (v "0.1.15") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "16wp2i49s0f0m1b8pbvbb358j2z6cn12zrl1r992ijd5m1gjhg01") (y #t)))

(define-public crate-unit-conversions-0.1.16 (c (n "unit-conversions") (v "0.1.16") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0pq9vs53bdx6hp2z8h8p1k7pc1w3gygvva9frq5dnp07qgq2sjmg")))

