(define-module (crates-io un it unitage) #:use-module (crates-io))

(define-public crate-unitage-0.0.1 (c (n "unitage") (v "0.0.1") (d (list (d (n "const-frac") (r "^0.0") (f (quote ("std" "ratio"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "physical-quantity") (r "^0.0") (f (quote ("std" "full"))) (d #t) (k 0)) (d (n "real-proc") (r "^0.0") (d #t) (k 0)) (d (n "typenum") (r "^1") (f (quote ("no_std"))) (k 0)) (d (n "unit-proc") (r "^0.0") (d #t) (k 0)))) (h "0nwgi77ngz4pv0p4hm65b2fjrrk1jnm0npbrlvda6sybsnxdxcy5")))

(define-public crate-unitage-0.0.2 (c (n "unitage") (v "0.0.2") (d (list (d (n "const-frac") (r "^0.0") (f (quote ("std" "ratio"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "physical-quantity") (r "^0.0") (f (quote ("std" "full"))) (d #t) (k 0)) (d (n "real-proc") (r "^0.0") (d #t) (k 0)) (d (n "typenum") (r "^1") (f (quote ("no_std"))) (k 0)) (d (n "unit-proc") (r "^0.0") (d #t) (k 0)))) (h "0y0xz4yjr9w5r7zplnxm6yipp16k6zp12rs7fzqa52ccydfvy7v2")))

