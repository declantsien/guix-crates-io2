(define-module (crates-io un it unity-native-plugin-sys) #:use-module (crates-io))

(define-public crate-unity-native-plugin-sys-0.1.0 (c (n "unity-native-plugin-sys") (v "0.1.0") (h "060f18y132x55dmi3vjk1rvpakb88aks9rv9ydz5n9dhdam64p8y")))

(define-public crate-unity-native-plugin-sys-0.1.1 (c (n "unity-native-plugin-sys") (v "0.1.1") (h "07dczdrwz4mn3xbghgk7wll8dyzlwlkmw1vvpmp8mz16scqf8rrb")))

(define-public crate-unity-native-plugin-sys-0.3.0 (c (n "unity-native-plugin-sys") (v "0.3.0") (h "08qs8pnrb5x5idwqwf5g3r1zf5yy6h0vk9h4r9fqh07q5qhvnmyf")))

(define-public crate-unity-native-plugin-sys-0.4.0 (c (n "unity-native-plugin-sys") (v "0.4.0") (h "198n0dl9rzw8na0ii7sn71d5264xx027r83rns7j6qx5cpjnfk86")))

(define-public crate-unity-native-plugin-sys-0.5.0 (c (n "unity-native-plugin-sys") (v "0.5.0") (h "1rvpigxcsgffqxkj49dv2gzakdf1292ab0s60wdm33g8l8h9gdh3")))

