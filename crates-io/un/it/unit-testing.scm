(define-module (crates-io un it unit-testing) #:use-module (crates-io))

(define-public crate-unit-testing-0.1.0 (c (n "unit-testing") (v "0.1.0") (h "0bjpvdvv2vqcdy3i98d1s4flag8qxgh9hlgls79mvk0fxz74slgn") (y #t)))

(define-public crate-unit-testing-0.1.1 (c (n "unit-testing") (v "0.1.1") (h "0nv2h63mbaw2my2c4v3dkb7dayid4l9szc26md2dry54f73canl0") (y #t)))

(define-public crate-unit-testing-0.1.2 (c (n "unit-testing") (v "0.1.2") (h "17j1rk971rpdlb5nzksvk1g4ak1x4gv8jxryahxmz643qa84cgyp") (y #t)))

(define-public crate-unit-testing-0.1.3 (c (n "unit-testing") (v "0.1.3") (h "0ibrnn1j9r4vs2qcvks7rli4xd1lg8x4zabgw9cdzaaa94l0913j") (y #t)))

(define-public crate-unit-testing-0.1.4 (c (n "unit-testing") (v "0.1.4") (h "02xrnn0ykcr7w9kkac2vihw05kjxn0rj4mhkaar2afibal2dk5xg") (y #t)))

(define-public crate-unit-testing-0.1.5 (c (n "unit-testing") (v "0.1.5") (h "0m1qr89i7k4nqv05jqp2g3ww0bnvqzf1xxgs5nbs6qxygmscjr6l") (y #t)))

(define-public crate-unit-testing-0.1.6 (c (n "unit-testing") (v "0.1.6") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "0qgjzvmv0g5l6l2amva3ag37qc99ighssihlxwnydk53jy4qw414") (y #t)))

(define-public crate-unit-testing-0.1.7 (c (n "unit-testing") (v "0.1.7") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "099r9pbnpqgxgvzlzn1j1azivwynk2vh8jw0xyallqm29k0vgbcz") (y #t)))

(define-public crate-unit-testing-0.1.8 (c (n "unit-testing") (v "0.1.8") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "1rxk0d07q7vzs1qbq1sdb12m8xf1zhxycgm7c5iri7f5bl56k2vm") (y #t)))

(define-public crate-unit-testing-0.2.1 (c (n "unit-testing") (v "0.2.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)))) (h "0vxdi93bmp6xm5jahcdxdxwc2y1bywjjii18ynansrs1w8sb215w") (y #t)))

(define-public crate-unit-testing-0.3.0 (c (n "unit-testing") (v "0.3.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)))) (h "1ll13g51d2ywx89dgz69wpbjhnl779d9r1202d8vlbp8mzmy9lfi") (y #t)))

(define-public crate-unit-testing-0.3.1 (c (n "unit-testing") (v "0.3.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)))) (h "1xkl3w2kmifja44bv27ckyzvq07qp9421ljbza8m74jny4951m87") (y #t)))

(define-public crate-unit-testing-0.3.2 (c (n "unit-testing") (v "0.3.2") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1dh2b2hqv3zv8bd7xj42j3lvmpv8y4mj63d6s5a6r2cwp21h0a2p") (y #t)))

(define-public crate-unit-testing-0.3.3 (c (n "unit-testing") (v "0.3.3") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1i7fn1n4hl3spwjc94yi9rzhlxsp3yz5q6av31pajp88n6ww5fbn") (y #t)))

(define-public crate-unit-testing-0.3.4 (c (n "unit-testing") (v "0.3.4") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "173qk74ap6x1cdd9f25knfl0h6knjb6j8c9q1ag0q76mrlpccv0i") (y #t)))

(define-public crate-unit-testing-0.3.5 (c (n "unit-testing") (v "0.3.5") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1g2pib3k8zsx2y543xrdv35jpi64ahb0lzp4j5lrb2cjndcz76bl") (y #t)))

(define-public crate-unit-testing-0.4.0 (c (n "unit-testing") (v "0.4.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "17nsair732pz2sfaxv3jzwhya8xnwycjindvcsh90h6nvvnf84yw") (y #t)))

(define-public crate-unit-testing-0.4.1 (c (n "unit-testing") (v "0.4.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1si1rbkv5j9svf80yk13w068n9hfq1cf881srb4cdpq3626vmq4g") (y #t)))

(define-public crate-unit-testing-1.0.0 (c (n "unit-testing") (v "1.0.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1ghvdsj67dxlfsklp5yanna19lnvvmmms5pbl8d4wp194a74af2n") (y #t)))

(define-public crate-unit-testing-1.0.1 (c (n "unit-testing") (v "1.0.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "0781czsi5da621mp3dx48xy8dqin41fyb518l3bxpf8j83x51hxz") (y #t)))

(define-public crate-unit-testing-1.0.2 (c (n "unit-testing") (v "1.0.2") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1lvfwcrbsd537gjafl1zcj10n3cdrqps45lzvd5cyz815fzli5jc") (y #t)))

(define-public crate-unit-testing-1.0.3 (c (n "unit-testing") (v "1.0.3") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "11d6cjbzx8465fr7xzdjgzk30wzrfp7d4x47dy5163lg8risszsy") (y #t)))

(define-public crate-unit-testing-1.0.4 (c (n "unit-testing") (v "1.0.4") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "0hvv9lplrn9z9pmjfj2rfrri2z02wp4w3mb9cn029bzp9q8ny6n2") (y #t)))

(define-public crate-unit-testing-1.1.0 (c (n "unit-testing") (v "1.1.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)))) (h "1xaysxnv1k36vn7bw3cz8z9g0kvsy1k0ns1m9lyvda5acf79r7z6") (y #t)))

(define-public crate-unit-testing-1.1.1 (c (n "unit-testing") (v "1.1.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0n3hyvwg10ir0f2wdqwlivmh8dq37s1544wr7hkwkddwfkrrvlh1") (y #t)))

(define-public crate-unit-testing-1.1.2 (c (n "unit-testing") (v "1.1.2") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0q6rr82xkxx4szv56z53qql9az9g7jc1aq4z2j9vj4knnwdv45zk")))

(define-public crate-unit-testing-1.2.1 (c (n "unit-testing") (v "1.2.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1f9p61dp8a1j9zgh80vqbgfsvlb9q6kcy0k49jcyvw0zh0fkfwl1")))

(define-public crate-unit-testing-2.0.0 (c (n "unit-testing") (v "2.0.0") (d (list (d (n "ansi_codes") (r "^0.2.1") (d #t) (k 0)) (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.5") (d #t) (k 0)) (d (n "test-env-helpers") (r "^0.2.2") (d #t) (k 0)))) (h "1lnyx8b4l7g3b9yn65np70b5pfcbx8c2k92qsrx51994gf40m9f7")))

(define-public crate-unit-testing-2.0.1 (c (n "unit-testing") (v "2.0.1") (d (list (d (n "ansi_codes") (r "^0.2.1") (d #t) (k 0)) (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.5") (d #t) (k 0)) (d (n "test-env-helpers") (r "^0.2.2") (d #t) (k 0)))) (h "17bi2w63wknp8l0xczpmz4jg6simhaz8naxwkyz53253bhcig312")))

(define-public crate-unit-testing-2.0.2 (c (n "unit-testing") (v "2.0.2") (d (list (d (n "ansi_codes") (r "^0.2.1") (d #t) (k 0)) (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.5") (d #t) (k 0)) (d (n "test-env-helpers") (r "^0.2.2") (d #t) (k 0)))) (h "1676az2p8p8wl2xc0ixspz099qqcg62lmg877wq2lxy674n6qm6a")))

(define-public crate-unit-testing-2.0.3 (c (n "unit-testing") (v "2.0.3") (d (list (d (n "ansi_codes") (r "^0.2.1") (d #t) (k 0)) (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.5") (d #t) (k 0)) (d (n "test-env-helpers") (r "^0.2.2") (d #t) (k 0)))) (h "1lc0y4k8yh4jylprnxigv61ppbcaj0apb225npg57rzzfik6lp3j")))

(define-public crate-unit-testing-2.0.4 (c (n "unit-testing") (v "2.0.4") (d (list (d (n "ansi_codes") (r "^0.2.1") (d #t) (k 0)) (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.5") (d #t) (k 0)) (d (n "test-env-helpers") (r "^0.2.2") (d #t) (k 0)))) (h "0lj8jnx35qa7f3rcbdqljhky3kb8c60xi12rcnqrc41nbyl6kzb0")))

(define-public crate-unit-testing-2.0.5 (c (n "unit-testing") (v "2.0.5") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.5") (d #t) (k 0)) (d (n "test-env-helpers") (r "^0.2.2") (d #t) (k 0)))) (h "0qc4h3n71s7wq9hq39gfvk0cyrf90cjp7bn9lq2dkyv08pz17q1j")))

(define-public crate-unit-testing-2.1.0 (c (n "unit-testing") (v "2.1.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0vf741zplhyrg7d88lvlhxz3d7y0k3akcllhnv2kqh566cimrp5a")))

(define-public crate-unit-testing-2.1.1 (c (n "unit-testing") (v "2.1.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1s26j4avmki62jkiy2dmp9sgkg4sbxicyavrgk3qjbaa3wps2qwk")))

(define-public crate-unit-testing-2.1.2 (c (n "unit-testing") (v "2.1.2") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "10c6gfcf63v51p64jpqvjils63f6qzpnwqz3b0rbliwc1g56n4ns")))

