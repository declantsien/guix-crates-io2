(define-module (crates-io un it unitval-derive) #:use-module (crates-io))

(define-public crate-unitval-derive-0.1.0 (c (n "unitval-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0izz9db1wy4gsdpqhfrgbp30x9xvfg6s7yzbg3fc9z79nn41ryv7")))

(define-public crate-unitval-derive-0.1.1 (c (n "unitval-derive") (v "0.1.1") (d (list (d (n "diesel") (r "^1.4.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0wpkakj4gd3mmzvz318s0znv8nbjla5vjz8p9wg32ymm9bk1f5hl") (f (quote (("diesel_postgres" "diesel/postgres") ("default")))) (y #t)))

(define-public crate-unitval-derive-0.1.2 (c (n "unitval-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0d4hlyfmb6vs0bx09qdyw8qvx6anmkm0mk6wzgnvidb0i7y4849z")))

(define-public crate-unitval-derive-0.2.0 (c (n "unitval-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0vkyjnb5nyvvfc76zrwf0mvhz8xpy776qfgv2l850b748hzsyd0g")))

