(define-module (crates-io un it unit-enum) #:use-module (crates-io))

(define-public crate-unit-enum-1.0.0 (c (n "unit-enum") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06wy2rxj3vn1wla8aykhkk3gxm5ipyr8jrlhzwys1dbby4c2swzj")))

