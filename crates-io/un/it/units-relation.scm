(define-module (crates-io un it units-relation) #:use-module (crates-io))

(define-public crate-units-relation-0.0.0 (c (n "units-relation") (v "0.0.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rs-measures") (r "^0.0.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0q24378xb83awcyknsb731kc49gf7yzsxdz12i6d6d1l8lq59xs5")))

(define-public crate-units-relation-0.1.0 (c (n "units-relation") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rs-measures") (r "^0.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0l507f7pd01z1kz8p9bz1gvvwxvm4d5awxisgpgfm662qrp3plf6")))

(define-public crate-units-relation-0.3.0 (c (n "units-relation") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rs-measures") (r "^0.3.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "17axg52a214mlwld2arswr5pw2j7xn8qs4xh3hy0qm49rxcfiila")))

(define-public crate-units-relation-0.4.0 (c (n "units-relation") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rs-measures") (r "^0.4.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fj12ghbcvf54v1rgjpvmv0055pn3bn2gjgv7ywfvm4pvjwjz84n")))

