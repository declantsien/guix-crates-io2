(define-module (crates-io un it unity_rs) #:use-module (crates-io))

(define-public crate-unity_rs-0.1.0 (c (n "unity_rs") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "04yr9cp3gqvid4rl6w6dinzyyxj2fp2hd9gva7gkczhrplv4kd8c")))

