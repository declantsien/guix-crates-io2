(define-module (crates-io un it unity-native-plugin) #:use-module (crates-io))

(define-public crate-unity-native-plugin-0.1.1 (c (n "unity-native-plugin") (v "0.1.1") (d (list (d (n "unity-native-plugin-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0p9zm3fgy3i6z1zl0lbwyx1kanb5k8zy1il9dh0s67ga4icg3r19") (f (quote (("default") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.2.0 (c (n "unity-native-plugin") (v "0.2.0") (d (list (d (n "unity-native-plugin-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1wrnzilvsqc0h1h7s22qpwynxf0pmy62pm1vsa34ry6lknkiswwj") (f (quote (("default") ("d3d12") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.3.0 (c (n "unity-native-plugin") (v "0.3.0") (d (list (d (n "unity-native-plugin-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1xakqd8lkd6k5g25pnzbv59l5kii2glmcvyr2v7n9cldf4wxa3qc") (f (quote (("vulkan") ("default") ("d3d12") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.4.0 (c (n "unity-native-plugin") (v "0.4.0") (d (list (d (n "unity-native-plugin-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1nxpajccm2rcqycp359hj8s0wqpa4ddibcpg6nq2k4rqmsbkzw7b") (f (quote (("profiler") ("default") ("d3d12") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.4.1 (c (n "unity-native-plugin") (v "0.4.1") (d (list (d (n "unity-native-plugin-sys") (r "^0.4.0") (d #t) (k 0)))) (h "07gpcsp7wmzxs1qy6y3fxj95nvw6prcsad4s2l8q42sd5rpjl0zf") (f (quote (("profiler") ("default") ("d3d12") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.5.0 (c (n "unity-native-plugin") (v "0.5.0") (d (list (d (n "unity-native-plugin-sys") (r "^0.5.0") (d #t) (k 0)))) (h "082njfpsa4w117gqp56dq23b08nhi3r23mxz46nggnp11wcb19nf") (f (quote (("profiler_callbacks" "profiler") ("profiler") ("default") ("d3d12") ("d3d11"))))))

