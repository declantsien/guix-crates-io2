(define-module (crates-io un it unity-yaml-rust) #:use-module (crates-io))

(define-public crate-unity-yaml-rust-0.1.0 (c (n "unity-yaml-rust") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0sp3vjllnn63vjyyvvn03kyfxqcp9bav404km9dv9hy65f2azswz")))

(define-public crate-unity-yaml-rust-0.1.1 (c (n "unity-yaml-rust") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "177mw5maasifk2xapc7yb0brzwz9dyj8jf218x04havbnp4lspyv")))

