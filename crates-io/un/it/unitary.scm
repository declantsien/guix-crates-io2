(define-module (crates-io un it unitary) #:use-module (crates-io))

(define-public crate-unitary-0.0.1 (c (n "unitary") (v "0.0.1") (h "0wk3z03djyv1zqrsdqlfmj86dgp7x12xfqm6x5lnmlayl2g9n9db")))

(define-public crate-unitary-0.0.2 (c (n "unitary") (v "0.0.2") (h "0qky11rq1m8x5fml1ws646sbnarr19c6m91x0x0y66rywlrqn269")))

