(define-module (crates-io un it unity_native) #:use-module (crates-io))

(define-public crate-unity_native-0.1.0 (c (n "unity_native") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "unity_native_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "unity_native_sys") (r "^0.1.0") (d #t) (k 0)))) (h "0w5awz17pfq84cp8i2xvxzjfnb812whfh8pwb6c5vc6gv142g83j")))

(define-public crate-unity_native-0.1.1 (c (n "unity_native") (v "0.1.1") (d (list (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "unity_native_proc_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "unity_native_sys") (r "^0.1.1") (d #t) (k 0)))) (h "02ls57wq18hpy97wm8qn7zsjy0zi39j7y9ivgbsxdhh6i4wa0wk1") (f (quote (("profiler") ("default" "log" "profiler")))) (s 2) (e (quote (("log" "dep:log"))))))

