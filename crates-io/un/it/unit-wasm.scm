(define-module (crates-io un it unit-wasm) #:use-module (crates-io))

(define-public crate-unit-wasm-0.1.0-beta (c (n "unit-wasm") (v "0.1.0-beta") (h "0m6dic29hlm7aspv10ayqln149ljq8dfmm61syhpcr5an7291a19") (l "unit-wasm")))

(define-public crate-unit-wasm-0.1.0 (c (n "unit-wasm") (v "0.1.0") (d (list (d (n "unit-wasm-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0ps5fw192hjq5rwnssxsm61dn9bv546m5cws1pfjpz21zkmji8bg")))

(define-public crate-unit-wasm-0.1.1 (c (n "unit-wasm") (v "0.1.1") (d (list (d (n "unit-wasm-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0dx0z2a6b4lcxxp0nr6czf45vwdkwp0vc73m9adaidwaw088lvdh")))

(define-public crate-unit-wasm-0.1.2 (c (n "unit-wasm") (v "0.1.2") (d (list (d (n "unit-wasm-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1vims2hq7v3n5r5a9dpczzdrbxhkwr062ggz9002cl737indc7xb")))

(define-public crate-unit-wasm-0.2.0 (c (n "unit-wasm") (v "0.2.0") (d (list (d (n "unit-wasm-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0w9ms96hzgyf9xgjfj4nmff556pn3c8lbfjfb3rrnhkm4g31x6n8")))

(define-public crate-unit-wasm-0.3.0 (c (n "unit-wasm") (v "0.3.0") (d (list (d (n "unit-wasm-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0mhr0x1mqhsqs9dpvmiac8x5dam9gyxy4nlipl76g2ggmgfxq938")))

