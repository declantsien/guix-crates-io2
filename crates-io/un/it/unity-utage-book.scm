(define-module (crates-io un it unity-utage-book) #:use-module (crates-io))

(define-public crate-unity-utage-book-0.1.0 (c (n "unity-utage-book") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xiphw0mhwvqdffd460y97mc0xqk2aww82gngcq975agn8dsiq36") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

