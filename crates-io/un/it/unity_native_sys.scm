(define-module (crates-io un it unity_native_sys) #:use-module (crates-io))

(define-public crate-unity_native_sys-0.1.0 (c (n "unity_native_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0l96z5g756cgdrifhyambni6jdikjcc168a619ykvjldsa42dzw8")))

(define-public crate-unity_native_sys-0.1.1 (c (n "unity_native_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0baij5shwsyb95qa215j0038alls5q2whs7j8jp1wbbnwyyl5aiv")))

