(define-module (crates-io un it unitdc) #:use-module (crates-io))

(define-public crate-unitdc-0.1.0 (c (n "unitdc") (v "0.1.0") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-rational") (r "^0.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0hv30zs91r25klrfs0n13xsf3ggsjwcg8k81w4pvxmlw594kq559")))

