(define-module (crates-io un it unity-native-plugin-vulkan) #:use-module (crates-io))

(define-public crate-unity-native-plugin-vulkan-0.3.0 (c (n "unity-native-plugin-vulkan") (v "0.3.0") (d (list (d (n "ash") (r "^0.31.0") (d #t) (k 0)) (d (n "unity-native-plugin") (r "^0.3.0") (d #t) (k 0)) (d (n "unity-native-plugin-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1s8zd0b51168w6v0aackr74v8xfm0n9xysjj3rjfw1647k64n1w3")))

(define-public crate-unity-native-plugin-vulkan-0.4.0 (c (n "unity-native-plugin-vulkan") (v "0.4.0") (d (list (d (n "ash") (r "^0.31.0") (d #t) (k 0)) (d (n "unity-native-plugin") (r "^0.4.0") (d #t) (k 0)) (d (n "unity-native-plugin-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1hj51fysz0c21wr8n10hcigj41hcf0fijbq0x0vagly5a24vck5n")))

(define-public crate-unity-native-plugin-vulkan-0.4.1 (c (n "unity-native-plugin-vulkan") (v "0.4.1") (d (list (d (n "ash") (r "^0.31.0") (d #t) (k 0)) (d (n "unity-native-plugin") (r "^0.4.1") (d #t) (k 0)) (d (n "unity-native-plugin-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0kkiadzirlr7aw4wn3khnshh4b1ni50s50vrwh9i0l5344x7wdbf")))

(define-public crate-unity-native-plugin-vulkan-0.5.0 (c (n "unity-native-plugin-vulkan") (v "0.5.0") (d (list (d (n "ash") (r "^0.31.0") (d #t) (k 0)) (d (n "unity-native-plugin") (r "^0.5.0") (d #t) (k 0)) (d (n "unity-native-plugin-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1r0kavjcqaqhbhvcdlzicxkgsghymrcr22m7wkqnr23fyl4s5rl7")))

(define-public crate-unity-native-plugin-vulkan-0.5.1 (c (n "unity-native-plugin-vulkan") (v "0.5.1") (d (list (d (n "ash") (r "^0.33.1") (d #t) (k 0)) (d (n "unity-native-plugin") (r "^0.5.0") (d #t) (k 0)) (d (n "unity-native-plugin-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1y5nmxx71d950rm4qbax16y66ihfjpdgmvkaxghbqj7pxsa8idp3")))

