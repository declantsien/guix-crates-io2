(define-module (crates-io un it unit-sphere) #:use-module (crates-io))

(define-public crate-unit-sphere-0.1.0 (c (n "unit-sphere") (v "0.1.0") (d (list (d (n "angle-sc") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jh5zcw7z8340z0x30pl2drzb2hrarxh6gnn2spnr25dwnhqav4l")))

(define-public crate-unit-sphere-0.1.1 (c (n "unit-sphere") (v "0.1.1") (d (list (d (n "angle-sc") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1anvvkbxbv2i7sf9jmlwlvz29bbjl8igfzknf111952qslshqw5v")))

(define-public crate-unit-sphere-0.1.2 (c (n "unit-sphere") (v "0.1.2") (d (list (d (n "angle-sc") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dpmx2f086wm00q21vwq37xqd88b8bsj1f2v0fvl3ayrpz3zw22j")))

