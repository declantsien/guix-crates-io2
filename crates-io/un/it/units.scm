(define-module (crates-io un it units) #:use-module (crates-io))

(define-public crate-units-0.0.1 (c (n "units") (v "0.0.1") (d (list (d (n "tylar") (r "^0.1.0") (d #t) (k 0)))) (h "0k1cbii98l32yn929wv4ba2nb8nyc062vrmh5s9mpjikr2rlpw1r") (f (quote (("unstable"))))))

(define-public crate-units-0.0.2 (c (n "units") (v "0.0.2") (d (list (d (n "tylar") (r "0.1.*") (d #t) (k 0)))) (h "0wgmqrrybq4ybksidnxyxb0c5s7dwhysn2wnzcpharjw70jhkrm6") (f (quote (("unstable"))))))

(define-public crate-units-0.1.0 (c (n "units") (v "0.1.0") (d (list (d (n "tylar") (r "0.2.*") (d #t) (k 0)))) (h "18v6j9p63bhddn00lx0jzmwzadzg9bj15m2ah12fwl2sav8wmrms") (f (quote (("unstable"))))))

