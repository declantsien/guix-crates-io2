(define-module (crates-io un sl unslip) #:use-module (crates-io))

(define-public crate-unslip-0.1.0 (c (n "unslip") (v "0.1.0") (d (list (d (n "aes-ctr") (r "^0.3") (d #t) (k 0)) (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bcbpi4paq96nz1n9975n0x6sbia1fv4iba47dxxbxy1n4gdd05w")))

