(define-module (crates-io un ht unhtml_derive) #:use-module (crates-io))

(define-public crate-unhtml_derive-0.1.1 (c (n "unhtml_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)))) (h "0krjysy263qq0f4wm8swn276gxjslycl25yq5jk3mzfzg55gcpi8")))

(define-public crate-unhtml_derive-0.2.0 (c (n "unhtml_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)))) (h "0mbjlcf4q7n83kyys411wm3dl6hpdcqf4qc1b3711l1kxfxvndaz")))

(define-public crate-unhtml_derive-0.2.1 (c (n "unhtml_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)))) (h "10g0ak82j1p8kdxlls97pss0iqi3by9pkg1szfidfc1rcnmcy4gc")))

(define-public crate-unhtml_derive-0.2.2 (c (n "unhtml_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.2") (d #t) (k 0)))) (h "1yvmj6p5b0bkkdsas69g6k9wwdhv0dgn9dhgp8mz913wb58vg2xb")))

(define-public crate-unhtml_derive-0.3.0 (c (n "unhtml_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.3.0") (d #t) (k 0)))) (h "19nj9j3d1z7aw9jb71lw0wbgnnrx31qcxnw7124m69dbcsncw4y4")))

(define-public crate-unhtml_derive-0.3.1 (c (n "unhtml_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.3.1") (d #t) (k 0)))) (h "1l0wgalmmiq5gimrbdqvfyb39q0wxvkasb78fnyf75jcsqmqm7x4")))

(define-public crate-unhtml_derive-0.4.0 (c (n "unhtml_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.4") (d #t) (k 0)))) (h "0zhyr5jdinkgzzzrx7xwlnz80cldkrwgk1fjpw1jkzy7ll15ji27")))

(define-public crate-unhtml_derive-0.5.0 (c (n "unhtml_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.4") (d #t) (k 0)))) (h "0y6fp9rqwk75pgkbd30m2fhagswmmpdkjf7wjxykay8zvs6979wf")))

(define-public crate-unhtml_derive-0.5.1 (c (n "unhtml_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.5") (d #t) (k 0)))) (h "0zb279sa1anj1jbm94mkxk5wyxf01sn8m7bm43b60pm5di62f5mz")))

(define-public crate-unhtml_derive-0.5.2 (c (n "unhtml_derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)) (d (n "unhtml") (r "^0.6") (d #t) (k 0)))) (h "1c8hpgl465vdvz7w8snxk48638nki6zzdrllzql7x1cm7z273yvw")))

(define-public crate-unhtml_derive-0.7.0 (c (n "unhtml_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "09mkrbkq0pq4h3qbz00b7y97fn6whmcprzljb4idq87dgm8v9q6q")))

(define-public crate-unhtml_derive-0.7.1 (c (n "unhtml_derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qw9iyvj1xpgiqaxd71mrqxvf4kj002wbqbmbq1qbimxn5sqi3j4")))

(define-public crate-unhtml_derive-0.7.2 (c (n "unhtml_derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c94y3sr86bg6qkrm934bn2qcpv32i0p67acx6jbw002ak3dachx")))

(define-public crate-unhtml_derive-0.7.3 (c (n "unhtml_derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14lmvlyf0kn0pqz854d9f6rjyhrnjxl4kj7j4kx5j284bzba1p69")))

(define-public crate-unhtml_derive-0.7.4 (c (n "unhtml_derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0408in62yydbannii0ziqzlgsva4q1kgy1mzqlks1w9k6xq40ycg")))

(define-public crate-unhtml_derive-0.7.5 (c (n "unhtml_derive") (v "0.7.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w17limy64dp94npxlj4yw59i8a9wk1app35ba4i5blja8131vnz")))

(define-public crate-unhtml_derive-0.8.0 (c (n "unhtml_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vs4y0g4ldrki1prlpv9wnxi2yl1wqj4c3wwr1d4iff4mnmxwj26")))

