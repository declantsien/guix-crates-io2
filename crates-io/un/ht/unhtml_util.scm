(define-module (crates-io un ht unhtml_util) #:use-module (crates-io))

(define-public crate-unhtml_util-0.1.0 (c (n "unhtml_util") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.8.1") (d #t) (k 0)))) (h "0kv4mak8wvcq5xq6h66n3j652nfbrk9508kszyf464pklpsz7zcl")))

