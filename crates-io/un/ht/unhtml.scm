(define-module (crates-io un ht unhtml) #:use-module (crates-io))

(define-public crate-unhtml-0.1.0 (c (n "unhtml") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.8.1") (d #t) (k 0)))) (h "1hisik5c6zsg1gf7sagqddmf2xr0pc3knhs878qriklvq0lgcbbk")))

(define-public crate-unhtml-0.1.1 (c (n "unhtml") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.8.2") (d #t) (k 0)))) (h "0ziik0gcqb0ilsrwrvfcgjhk1f9i5sz2cfkn2k2miba2d9m8dkcb")))

(define-public crate-unhtml-0.1.2 (c (n "unhtml") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.8.2") (d #t) (k 0)))) (h "1igwjjd0wbdikqmv2niq2s08f5v0l6f2z8m4mqw1wrir8jys686a")))

(define-public crate-unhtml-0.3.0 (c (n "unhtml") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)))) (h "1ispkkk3x6w3518jma7yqbmja03x60ixk4pdkgi54nblxrzsmhhm")))

(define-public crate-unhtml-0.3.1 (c (n "unhtml") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)))) (h "1vwwhb96dik456y9n8r9662qn3vw7dp8nc4g4knjmyzla7ajl6x5")))

(define-public crate-unhtml-0.4.0 (c (n "unhtml") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)))) (h "021v7yvfhk93if8glcgr1aiwa190c3qb9h09f7ghgpfn4qqj4187")))

(define-public crate-unhtml-0.5.0 (c (n "unhtml") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)))) (h "10nfhp4a3sags541cjpi54jih0p7v3lr7yj0z2z2a1bkgfm7l1r9")))

(define-public crate-unhtml-0.5.1 (c (n "unhtml") (v "0.5.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)))) (h "1xhyl9v0rvcb870mrpfdr7lmplgaxn3wv2p6j3mr91dqcnq6n5v6")))

(define-public crate-unhtml-0.6.0 (c (n "unhtml") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)))) (h "06wkflirdrng9ivacjfahv0ayslfwp2mb9xw5npgk3lxz9sfmyxn")))

(define-public crate-unhtml-0.6.1 (c (n "unhtml") (v "0.6.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)))) (h "0zmlivixb11lx3jibapz1r0w75vppm2yimfvbw04d3c60xw6mbxd")))

(define-public crate-unhtml-0.7.0 (c (n "unhtml") (v "0.7.0") (d (list (d (n "ego-tree") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1i7ckfzjrdjj6jn4y03r5r46ik3byspnlz3jzxzw3q814ijn2c92") (f (quote (("derive" "unhtml_derive"))))))

(define-public crate-unhtml-0.7.1 (c (n "unhtml") (v "0.7.1") (d (list (d (n "ego-tree") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1z3kz9y86gl1720a4imwdaycvf9pvvg344vvaqmqr5k9bw0h985n") (f (quote (("derive" "unhtml_derive"))))))

(define-public crate-unhtml-0.7.2 (c (n "unhtml") (v "0.7.2") (d (list (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1y6z56rsa2vbyv9pvig09pmsxbm6kn7nywk47qzhnjn8gwx6d0lj") (f (quote (("derive" "unhtml_derive"))))))

(define-public crate-unhtml-0.7.3 (c (n "unhtml") (v "0.7.3") (d (list (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "scraper") (r "^0.10") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1xb6bcx63la3nl18x0jwgph0v9cqsjpp9mhr6990d827z7f82mdw") (f (quote (("derive" "unhtml_derive"))))))

(define-public crate-unhtml-0.7.4 (c (n "unhtml") (v "0.7.4") (d (list (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0qqa7hg4phl7663ciig1bg87z9wrjr49f46k3lphppa7y7q04adb") (f (quote (("derive" "unhtml_derive"))))))

(define-public crate-unhtml-0.7.5 (c (n "unhtml") (v "0.7.5") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1w5my1m7gsjq4sl5qpvdf6hihbnr5yjiy4khm3zsrxa4zbfpjfdq") (f (quote (("derive" "unhtml_derive"))))))

(define-public crate-unhtml-0.8.0 (c (n "unhtml") (v "0.8.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (k 0)) (d (n "unhtml_derive") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1l1s7pqgnm94p54j8rql6lva390plikadnagmgq3bjmfngdqsnx7") (f (quote (("derive" "unhtml_derive"))))))

