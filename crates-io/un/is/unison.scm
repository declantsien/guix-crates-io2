(define-module (crates-io un is unison) #:use-module (crates-io))

(define-public crate-unison-0.1.0 (c (n "unison") (v "0.1.0") (h "1gir5h2hvqcgdl8vmhy1qgsq8msy3s5zqarrf4qpfki55pipfigl")))

(define-public crate-unison-0.2.0 (c (n "unison") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0x5vggnvgw1nvf8p0daafx9kq4hzrsaa3rj8vacvczycnrfd7y38")))

(define-public crate-unison-0.3.0 (c (n "unison") (v "0.3.0") (d (list (d (n "seckey") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "182gjr5fk7c7qn2vngm7pwgi211br5zmmra61j0mkqsghzh1my1r")))

(define-public crate-unison-0.3.1 (c (n "unison") (v "0.3.1") (d (list (d (n "seckey") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1yfczh7vfif4glc501zkdgp4m32hnd9grqqmq1cm52j7yjssjnch")))

(define-public crate-unison-0.3.2 (c (n "unison") (v "0.3.2") (d (list (d (n "seckey") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1z9ww6zvr2lh0adkka7g7x2nk0ibq6iyrlxcvzrc3aarwp6913yh")))

