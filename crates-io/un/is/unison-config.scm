(define-module (crates-io un is unison-config) #:use-module (crates-io))

(define-public crate-unison-config-0.1.0 (c (n "unison-config") (v "0.1.0") (d (list (d (n "camino") (r "^1.0.9") (d #t) (k 0)) (d (n "kdl") (r "^4.3.0") (d #t) (k 0)) (d (n "kdl-schema") (r "^0.1.0") (d #t) (k 0)) (d (n "knuffel") (r "^2.0.0") (d #t) (k 0)) (d (n "miette") (r "^5.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "toposort") (r "^0.1.0") (d #t) (k 0)))) (h "09iv7p66lq6naiv722x2kp8s6792andswjiw8k3kj4406kgcijya") (y #t)))

(define-public crate-unison-config-0.1.1 (c (n "unison-config") (v "0.1.1") (d (list (d (n "camino") (r "^1.0.9") (d #t) (k 0)) (d (n "kdl") (r "^4.3.0") (d #t) (k 0)) (d (n "knuffel") (r "^2.0.0") (d #t) (k 0)) (d (n "miette") (r "^5.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "toposort") (r "^0.1.0") (d #t) (k 0)))) (h "09pm883yshbkllhwkb6fi6xv8hivsnb75fpxd6iyk1g4xkw1wai4") (y #t)))

