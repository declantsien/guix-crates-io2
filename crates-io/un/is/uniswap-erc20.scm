(define-module (crates-io un is uniswap-erc20) #:use-module (crates-io))

(define-public crate-uniswap-erc20-0.1.0 (c (n "uniswap-erc20") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-erc20-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.2.2") (d #t) (k 0)))) (h "079xy2sm85s426vwnbcyicwb4wrfp2nyxb8h5m1648h9mg3ch1mc")))

(define-public crate-uniswap-erc20-0.2.0 (c (n "uniswap-erc20") (v "0.2.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-erc20-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.2.2") (d #t) (k 0)))) (h "0cddz8dwy47a349ba5msf5wfc2gbmvh0v73c2xzg845rq5rqs8l7")))

(define-public crate-uniswap-erc20-0.3.0 (c (n "uniswap-erc20") (v "0.3.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-erc20-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.2.2") (d #t) (k 0)))) (h "041s16ssnr8x4x4kigsdsjvcccg1jkdp776lzfmxa4n4jq57mhsh")))

