(define-module (crates-io un is unisocket) #:use-module (crates-io))

(define-public crate-unisocket-0.1.0 (c (n "unisocket") (v "0.1.0") (h "157mjhldlz127xi1qd8455vhbxxip8v4462frxmsdgmi08qdlgsm")))

(define-public crate-unisocket-1.0.0 (c (n "unisocket") (v "1.0.0") (h "0d3j7c2z1sx71yr9xbj37dyixywgl0hmrinkxaxnyll2mk9jrg3l")))

