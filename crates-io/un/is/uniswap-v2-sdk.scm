(define-module (crates-io un is uniswap-v2-sdk) #:use-module (crates-io))

(define-public crate-uniswap-v2-sdk-0.1.0 (c (n "uniswap-v2-sdk") (v "0.1.0") (d (list (d (n "alloy-primitives") (r "^0.6") (d #t) (k 0)) (d (n "alloy-sol-types") (r "^0.6") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "ruint") (r "^1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uniswap-sdk-core") (r "^0.20.1") (d #t) (k 0)))) (h "0bqrwgbrhcnhfb7vgh8q78m724r2vipnvs817l0wzwk7fywsjky4")))

(define-public crate-uniswap-v2-sdk-0.2.0 (c (n "uniswap-v2-sdk") (v "0.2.0") (d (list (d (n "alloy-primitives") (r "^0.7") (d #t) (k 0)) (d (n "alloy-sol-types") (r "^0.7") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "ruint") (r "^1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uniswap-sdk-core") (r "^0.21.0") (d #t) (k 0)))) (h "052p1xiran6j5vk0v2y47a77b39qakx46mzswn3ipw3ddwgyg07a")))

