(define-module (crates-io un is uniswap-utilities) #:use-module (crates-io))

(define-public crate-uniswap-utilities-0.1.0 (c (n "uniswap-utilities") (v "0.1.0") (d (list (d (n "ethers") (r "^1.0.2") (d #t) (k 0)))) (h "0bv7yvmfxgslc00hs7pggj2gs0j2pfa573hrkqq90ps23wx3ll3h")))

(define-public crate-uniswap-utilities-0.1.1 (c (n "uniswap-utilities") (v "0.1.1") (d (list (d (n "ethers") (r "^1.0.2") (d #t) (k 0)))) (h "0h3phdk4gm37643r270mw2pwzhnj88mp4wyixhzcw4rxibh2bmd0")))

(define-public crate-uniswap-utilities-0.1.2 (c (n "uniswap-utilities") (v "0.1.2") (d (list (d (n "ethers") (r "^1.0.2") (d #t) (k 0)))) (h "1jmfv9a8nl4i3nvqxljymhi4vh1qv4z0ab0jn28al902bqq54k6g")))

(define-public crate-uniswap-utilities-0.1.3 (c (n "uniswap-utilities") (v "0.1.3") (d (list (d (n "ethers") (r "^1.0.2") (d #t) (k 0)))) (h "1c4yhs9haxf3yr8wjj77kiwaqpzbwm81kmhzyw2wpx9iwdkzc820")))

(define-public crate-uniswap-utilities-0.1.4 (c (n "uniswap-utilities") (v "0.1.4") (d (list (d (n "ethers") (r "^1.0.2") (d #t) (k 0)))) (h "1lr7nx0fxx3cm2s6zlymxs2ag4vskwrj4diq9yx541s1x3l9vwwc")))

