(define-module (crates-io un is unisearch) #:use-module (crates-io))

(define-public crate-unisearch-0.0.0 (c (n "unisearch") (v "0.0.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1q4sc6ln7nkm2k9gr48711ng4xjivn97x7hi9r1ggm11qqsg3h16")))

