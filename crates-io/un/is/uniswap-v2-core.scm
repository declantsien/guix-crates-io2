(define-module (crates-io un is uniswap-v2-core) #:use-module (crates-io))

(define-public crate-uniswap-v2-core-0.1.0 (c (n "uniswap-v2-core") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g050hw30p8amry50l88j6c8ky0rg2k3443bkp2b39yx4qsais79") (y #t)))

(define-public crate-uniswap-v2-core-0.1.1 (c (n "uniswap-v2-core") (v "0.1.1") (d (list (d (n "ether-rpc") (r "^0.1.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0") (d #t) (k 0)))) (h "0pxj7j39mckwcn4zjpzy8l9imc4765w5xhfrb8qv7vqjrrj3dm8n")))

