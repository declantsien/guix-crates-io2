(define-module (crates-io un is uniswap_v3_math) #:use-module (crates-io))

(define-public crate-uniswap_v3_math-0.1.0 (c (n "uniswap_v3_math") (v "0.1.0") (d (list (d (n "ethers") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1p70s864zpfasgqllzhlyw9wp1dqqia7ia5wdi7zs6x9jgcqjscr")))

(define-public crate-uniswap_v3_math-0.2.1 (c (n "uniswap_v3_math") (v "0.2.1") (d (list (d (n "ethers") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0c6x6bwnak9a8bdrz8wlffsn771lqyzgasmgpv1087pqdxj3yrk0")))

(define-public crate-uniswap_v3_math-0.2.2 (c (n "uniswap_v3_math") (v "0.2.2") (d (list (d (n "ethers") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1sfcgdnx7140gvnb4xbp9azsddr9ws5vp5fm2jhyhkxh3pg2gaxc")))

(define-public crate-uniswap_v3_math-0.2.3 (c (n "uniswap_v3_math") (v "0.2.3") (d (list (d (n "ethers") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "054kcm1qfqjcalahwdbm8r7v7y239rn0sz4mslf18jc00hzacqpl")))

(define-public crate-uniswap_v3_math-0.2.4 (c (n "uniswap_v3_math") (v "0.2.4") (d (list (d (n "ethers") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1kq54fpdkq82p9j96klvc12cs3x4y91gbax22vyjlsf2kcrvap7c")))

(define-public crate-uniswap_v3_math-0.2.5 (c (n "uniswap_v3_math") (v "0.2.5") (d (list (d (n "ethers") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1ipmg3lnbqvlra7cz5gjl96x3k9pjlrhb6rnxb7wrfirrdshlsk1")))

(define-public crate-uniswap_v3_math-0.2.6 (c (n "uniswap_v3_math") (v "0.2.6") (d (list (d (n "ethers") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "05dn5s7mjpk49brsgqjpd2w0c99g9k76hdsn6983646lcam0x9n8")))

(define-public crate-uniswap_v3_math-0.2.7 (c (n "uniswap_v3_math") (v "0.2.7") (d (list (d (n "ethers") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "18rvlrs07gjd7fy5j3izsspcz0d5q44hpgs8ac3mgn3fq7j9mvbr")))

(define-public crate-uniswap_v3_math-0.2.8 (c (n "uniswap_v3_math") (v "0.2.8") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "07k3c6h0z6qhl4kks4sk1a672gj5kydxd696sx0jxyzj8anzmzq3")))

(define-public crate-uniswap_v3_math-0.2.9 (c (n "uniswap_v3_math") (v "0.2.9") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0gc4ikwk8h5sw2ndc1mzywdsmwvbz76badw3lkn3iqwmrvlfa68d")))

(define-public crate-uniswap_v3_math-0.2.10 (c (n "uniswap_v3_math") (v "0.2.10") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1y17vspykyvacqh7c9ikkhhdxm0y83ribn9j5wcasxn4b4c1g682")))

(define-public crate-uniswap_v3_math-0.2.11 (c (n "uniswap_v3_math") (v "0.2.11") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "03r47yhvidrc2s18m7bwvqw5nm6g17n8j7hpyb4sgss74axpalpk")))

(define-public crate-uniswap_v3_math-0.2.12 (c (n "uniswap_v3_math") (v "0.2.12") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0gkr7bk0dkdndhbwb7ab7a2nzydqmfkmmyh2hz8d742n6gvj9n3p")))

(define-public crate-uniswap_v3_math-0.2.13 (c (n "uniswap_v3_math") (v "0.2.13") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "16yhjk1qdb4b703zg54qq1g8qvw4cak5h5hzjw0qcsihcc5izigm")))

(define-public crate-uniswap_v3_math-0.2.14 (c (n "uniswap_v3_math") (v "0.2.14") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1iixx3d9ag6njdcf6jpsjbxgr7l34mm24qnfga4vnw99xhjr4kwy")))

(define-public crate-uniswap_v3_math-0.2.15 (c (n "uniswap_v3_math") (v "0.2.15") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1y66c22yw5hcinmj8w2szvj59yv2xbjisjjsqi5anc30zysbw2cd")))

(define-public crate-uniswap_v3_math-0.2.16 (c (n "uniswap_v3_math") (v "0.2.16") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0lgkq5kzlgfvln4f8nkw0lnl6azcqx7vjfi7k9ih6q9mwxqjka6i")))

(define-public crate-uniswap_v3_math-0.2.17 (c (n "uniswap_v3_math") (v "0.2.17") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "07gs4q6c6dkb8pzqdw8rj2i14n5lpg0z20iynkvn0rhj30hs7skm")))

(define-public crate-uniswap_v3_math-0.2.18 (c (n "uniswap_v3_math") (v "0.2.18") (d (list (d (n "ethers") (r "^0.17.0") (f (quote ("abigen"))) (d #t) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1b0c2g67sdmy1bxxfnqiay6zgvp8r8whnqym40gnrc2q6camddhb")))

(define-public crate-uniswap_v3_math-0.2.19 (c (n "uniswap_v3_math") (v "0.2.19") (d (list (d (n "ethers") (r "^1.0") (f (quote ("abigen"))) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0fpfsfg4phhdypa1kf4iwn9zxqziwllirimax1cflm8kx7w9xnhq")))

(define-public crate-uniswap_v3_math-0.2.20 (c (n "uniswap_v3_math") (v "0.2.20") (d (list (d (n "ethers") (r "^1.0.2") (f (quote ("abigen"))) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "17f677ijgi1ri7f8wxhw6pq37l4b87fs25bfzc0lmk3z65gsl3ab")))

(define-public crate-uniswap_v3_math-0.2.21 (c (n "uniswap_v3_math") (v "0.2.21") (d (list (d (n "ethers") (r "^1.0.2") (f (quote ("abigen"))) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0fg1di636vp7hdw6yhzbsxj3wjpviqzxa6va1i594cq8g44mhzzr")))

(define-public crate-uniswap_v3_math-0.2.22 (c (n "uniswap_v3_math") (v "0.2.22") (d (list (d (n "ethers") (r "^1.0.2") (f (quote ("abigen"))) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0lwxsfdgp10m22ixyljcjqfijvygvi7sl7farh5vbjrsmlpxg2im")))

(define-public crate-uniswap_v3_math-0.2.23 (c (n "uniswap_v3_math") (v "0.2.23") (d (list (d (n "ethers") (r "^1.0.2") (f (quote ("abigen"))) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0050sq0jwjfgp7infn9bd9dwxj948r1fs5marn4zsz8bdw6mjyca")))

(define-public crate-uniswap_v3_math-0.2.25 (c (n "uniswap_v3_math") (v "0.2.25") (d (list (d (n "ethers") (r "^1.5.0") (f (quote ("abigen"))) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0m89vxw1bldpbcv2zi0qaqvadr1qk64p2w8zz90lwpcn1ir226f5")))

(define-public crate-uniswap_v3_math-0.2.26 (c (n "uniswap_v3_math") (v "0.2.26") (d (list (d (n "ethers") (r "^2.0.0") (f (quote ("abigen"))) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "17jhp9fbc8l4qpnl3s03h8zcb8hf4swyyxs46rimy9sb8sq42kb8")))

(define-public crate-uniswap_v3_math-0.3.0 (c (n "uniswap_v3_math") (v "0.3.0") (d (list (d (n "ethers") (r "^2.0.0") (f (quote ("abigen"))) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0akz5660gxs8yd5340dh2sfifypybm9ngl96ibgii5lqd8civfad")))

(define-public crate-uniswap_v3_math-0.4.0 (c (n "uniswap_v3_math") (v "0.4.0") (d (list (d (n "ethers") (r "^2.0.0") (f (quote ("abigen"))) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1vaddzn1c5h7wplgywqp8xahdr0i50jw0w3rym0ny2nx6giyqnkj")))

(define-public crate-uniswap_v3_math-0.4.1 (c (n "uniswap_v3_math") (v "0.4.1") (d (list (d (n "ethers") (r "^2.0.0") (f (quote ("abigen"))) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0sbrcrzqzqprcwjdvx1gl4rkylkmmxzvlkhnbl34djm8mrywxalw")))

