(define-module (crates-io un is unistring) #:use-module (crates-io))

(define-public crate-unistring-0.1.0 (c (n "unistring") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1hxr323ifwm27klrjcnk160ksr2x8xsky8lmvc2c93pp976wki7p") (l "unistring")))

(define-public crate-unistring-0.1.1 (c (n "unistring") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "04vrm75b43a77lv0s06ixvq7v5mdsrp9jbn25hgsns87rmy1qzy4") (l "unistring")))

