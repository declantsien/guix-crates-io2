(define-module (crates-io un is uniswap-core) #:use-module (crates-io))

(define-public crate-uniswap-core-0.6.1 (c (n "uniswap-core") (v "0.6.1") (d (list (d (n "alloy-primitives") (r "^0.5.4") (d #t) (k 0)) (d (n "bigdecimal") (r "^0.4.2") (d #t) (k 0)) (d (n "eth_checksum") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "0mwr1jydgi73psg2v29fcaa1v4pvlm0d10qgdkl9v8vknhanrb2w") (y #t)))

