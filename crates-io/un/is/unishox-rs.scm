(define-module (crates-io un is unishox-rs) #:use-module (crates-io))

(define-public crate-unishox-rs-0.1.0 (c (n "unishox-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1aikiv1yccyp5s2xc0y8bjddn214d3g2zx3ip7cln9dz2q1ajnvh") (l "unishox2")))

(define-public crate-unishox-rs-0.1.1 (c (n "unishox-rs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1sdvjx57ppsdj3l3ic3wnq987fz2r44zgn63h0sywxm5sj2zn112") (l "unishox2")))

