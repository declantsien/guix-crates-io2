(define-module (crates-io un is unison-fsmonitor) #:use-module (crates-io))

(define-public crate-unison-fsmonitor-0.1.0 (c (n "unison-fsmonitor") (v "0.1.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "0dz67gr26k5gvxfvx4kf4y3rf1mx6wmv50cwc4a9wzx1hyihmgh5")))

(define-public crate-unison-fsmonitor-0.2.0 (c (n "unison-fsmonitor") (v "0.2.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "02rbyn27i3wffqh9zilv17k05b5cvfdc0rhwzmfdy7xm9jl7az48")))

(define-public crate-unison-fsmonitor-0.2.1 (c (n "unison-fsmonitor") (v "0.2.1") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "1gv983gcl4ykyc2rqrfc6mzvb6qh0icrdpxc5v67yj5ycjhybw1i")))

(define-public crate-unison-fsmonitor-0.2.2 (c (n "unison-fsmonitor") (v "0.2.2") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "1j18mjp4z8zblapp8canbslxkkxh04hrim686l99ipxw90dsqiyq")))

(define-public crate-unison-fsmonitor-0.2.3 (c (n "unison-fsmonitor") (v "0.2.3") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "06ka8qv2wns43hcqghvxzgrbz73m3hfd7pwmcyzns9swnvlggmmc")))

(define-public crate-unison-fsmonitor-0.2.4 (c (n "unison-fsmonitor") (v "0.2.4") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "1x7ijkdc1vildgsh852bwwkjyd0rcr7ap3c5vikls56czw7hza08")))

(define-public crate-unison-fsmonitor-0.2.5 (c (n "unison-fsmonitor") (v "0.2.5") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "0khb7h7jzjw9p5p7vddshidm16x9m6w26k6m6a36vwfi6kyifb3h")))

(define-public crate-unison-fsmonitor-0.2.6 (c (n "unison-fsmonitor") (v "0.2.6") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "0c1q4bcqlr18x30pyr9z83xibq3p2f4fg8yxyd31hk8n71f4ji3d")))

(define-public crate-unison-fsmonitor-0.2.7 (c (n "unison-fsmonitor") (v "0.2.7") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "1flfkmp0z06fm1510rkbb1fkya3rn9jasca2hkhmz20m4gxnag4k")))

(define-public crate-unison-fsmonitor-0.3.0 (c (n "unison-fsmonitor") (v "0.3.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "01mcis4jx6bys16pr70mkl4qy057vhjizp8caa2zka77pzh5akqv")))

(define-public crate-unison-fsmonitor-0.3.2 (c (n "unison-fsmonitor") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "0ncrla5xzhl66jiiwpjkv9vlgapr9pdq1wc5gc8ipx9z75j92x5b")))

(define-public crate-unison-fsmonitor-0.3.3 (c (n "unison-fsmonitor") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "0my17jm8146lz9ij8afxdzm9pmj1j7gi8rb0gd2ynpih7xgrirzs")))

(define-public crate-unison-fsmonitor-0.3.4 (c (n "unison-fsmonitor") (v "0.3.4") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "1919xsids63nk1gkz1cm6fgw88vb6aamm5568ndmjpcrsm1b8yyv")))

