(define-module (crates-io un le unleash-datagenerator) #:use-module (crates-io))

(define-public crate-unleash-datagenerator-0.1.0 (c (n "unleash-datagenerator") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "clap-markdown") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "0z6npgn59cbm7r20pqima2c6jp4f4sni4dx7q317hkx2fcr5szs5")))

