(define-module (crates-io un le unleash) #:use-module (crates-io))

(define-public crate-unleash-0.0.0 (c (n "unleash") (v "0.0.0") (h "1y5laqip1d4pjdyg5ysrrkz3ali4av64c822z9qmyrfdwsp0z3cd")))

(define-public crate-unleash-1.0.0-alpha.11 (c (n "unleash") (v "1.0.0-alpha.11") (d (list (d (n "cargo") (r "^0.50.0") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.3") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1snfh20vsqg3s9jf65khi6gxilpjlzfifkmrdja42zks76bbdfd7") (f (quote (("gen-readme" "cargo-readme" "lazy_static" "sha1") ("default"))))))

