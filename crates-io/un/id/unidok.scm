(define-module (crates-io un id unidok) #:use-module (crates-io))

(define-public crate-unidok-0.1.0 (c (n "unidok") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "similar") (r "^1.3") (d #t) (k 2)) (d (n "unidok-parser") (r "^0.1") (d #t) (k 0)) (d (n "unidok-to-html") (r "^0.1") (d #t) (k 0)))) (h "15g8bgfwvca1w6p5d5y3w2dynr1whcvz3a7n2i58y73rm2wl2vz4")))

(define-public crate-unidok-0.2.0 (c (n "unidok") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("color"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "similar") (r "^1.3") (d #t) (k 2)) (d (n "unidok-parser") (r "^0.2") (d #t) (k 0)) (d (n "unidok-repr") (r "^0.2") (d #t) (k 0)) (d (n "unidok-to-html") (r "^0.2") (d #t) (k 0)))) (h "12w29amikbmfmbbddrcji958ffihm41ixhnbcybxb4fs1hgb5i4i")))

