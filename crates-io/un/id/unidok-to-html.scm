(define-module (crates-io un id unidok-to-html) #:use-module (crates-io))

(define-public crate-unidok-to-html-0.1.0 (c (n "unidok-to-html") (v "0.1.0") (d (list (d (n "asciimath-rs") (r "^0.5.8") (d #t) (k 0)) (d (n "unidok-parser") (r "^0.1") (d #t) (k 0)) (d (n "unidok-repr") (r "^0.1") (d #t) (k 0)))) (h "08sv6d7i963rv0snhd7586ksgi8kf03r4w4ajmlxmcwcxjpw1k8q")))

(define-public crate-unidok-to-html-0.2.0 (c (n "unidok-to-html") (v "0.2.0") (d (list (d (n "asciimath-rs") (r "^0.5.8") (d #t) (k 0)) (d (n "unidok-parser") (r "^0.2") (d #t) (k 0)) (d (n "unidok-repr") (r "^0.2") (d #t) (k 0)))) (h "06bzvl1c2vvcmlya47gc0yzbxva305zl5w06aalzrjfbrwcybmaj")))

