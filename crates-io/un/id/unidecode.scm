(define-module (crates-io un id unidecode) #:use-module (crates-io))

(define-public crate-unidecode-0.1.0 (c (n "unidecode") (v "0.1.0") (d (list (d (n "phf") (r "^0.6.13") (d #t) (k 0)) (d (n "phf_macros") (r "^0.6.13") (d #t) (k 0)))) (h "0bm60jls0hfgjf2ksb6i9wfxvzvl4xmi01v93p1bfwyqgl965bwz")))

(define-public crate-unidecode-0.1.1 (c (n "unidecode") (v "0.1.1") (d (list (d (n "phf") (r "^0.6.13") (d #t) (k 0)) (d (n "phf_macros") (r "^0.6.13") (d #t) (k 0)))) (h "0qmjdiymhmsdgw0rh471zbc2dfss3ifziswxf3nghki13n58m4fw")))

(define-public crate-unidecode-0.1.2 (c (n "unidecode") (v "0.1.2") (d (list (d (n "phf") (r "^0.6.13") (d #t) (k 0)) (d (n "phf_macros") (r "^0.6.13") (d #t) (k 0)))) (h "1wcy50jzpigyf10py6myvl0q03c6icjslxna0qgpwwgrl52j2dh8")))

(define-public crate-unidecode-0.1.3 (c (n "unidecode") (v "0.1.3") (d (list (d (n "phf") (r "^0.6.13") (d #t) (k 0)) (d (n "phf_macros") (r "^0.6.13") (d #t) (k 0)))) (h "17jhs2zzgfmp0gkrfq1drp11hw5cca03gnplh2r8qcja102bpgg7")))

(define-public crate-unidecode-0.1.4 (c (n "unidecode") (v "0.1.4") (d (list (d (n "phf") (r "^0.6.13") (d #t) (k 0)) (d (n "phf_macros") (r "^0.6.13") (d #t) (k 0)))) (h "1z5j7qgj25ylaicx5fnikg4d8k6wqvi6f5q1d0i16jgnx242y4nm")))

(define-public crate-unidecode-0.1.5 (c (n "unidecode") (v "0.1.5") (d (list (d (n "phf") (r "^0.6.14") (d #t) (k 0)) (d (n "phf_macros") (r "^0.6.14") (d #t) (k 0)))) (h "1mbmbam7xvhg1iv8llby16yr1bgyb2v6yzrd89brwbarv0fabvpz")))

(define-public crate-unidecode-0.1.6 (c (n "unidecode") (v "0.1.6") (d (list (d (n "phf") (r "^0.6.15") (d #t) (k 0)) (d (n "phf_macros") (r "^0.6.15") (d #t) (k 0)))) (h "09vpqfqnrks65ckfjsca76znkbpm3zf0x85w9i1bskyc9vikwr17")))

(define-public crate-unidecode-0.1.7 (c (n "unidecode") (v "0.1.7") (d (list (d (n "phf") (r "^0.6.18") (d #t) (k 0)) (d (n "phf_macros") (r "^0.6.18") (d #t) (k 0)))) (h "16ibp3mn71waz67z7aj7v5xfhlj2yjk3h2xp67phy4lqba58ihqb")))

(define-public crate-unidecode-0.1.8 (c (n "unidecode") (v "0.1.8") (d (list (d (n "phf") (r "^0.7.2") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.2") (d #t) (k 1)))) (h "1c5n2cpd0fvdwv5f4z0ic4jagrmhx88akyf7sdziag9ch0szlcnj")))

(define-public crate-unidecode-0.1.9 (c (n "unidecode") (v "0.1.9") (d (list (d (n "phf") (r "^0.7.2") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.2") (d #t) (k 1)))) (h "15fvk5rrfp2fk60avnzq7c069hp5p92kf9cmi4y7cj4h3yvp7zsc")))

(define-public crate-unidecode-0.1.10 (c (n "unidecode") (v "0.1.10") (d (list (d (n "phf") (r "^0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.3") (d #t) (k 1)))) (h "05f35pcs10rrcs0nphxx8p9gaiba65gyqkkx0r4j8xyavrcf9dlb")))

(define-public crate-unidecode-0.2.0 (c (n "unidecode") (v "0.2.0") (h "1llzx3x2n3a6yb3kaa82fr5mmhhpgag2s7qk33npkmbww1gbkbfj")))

(define-public crate-unidecode-0.3.0 (c (n "unidecode") (v "0.3.0") (h "1p0sm8j9223kw3iincv60s746s88k09xcaqf8nkx3w83isfv2as0")))

