(define-module (crates-io un id unidok-parser) #:use-module (crates-io))

(define-public crate-unidok-parser-0.1.0 (c (n "unidok-parser") (v "0.1.0") (d (list (d (n "detached-str") (r "^0.1") (d #t) (k 0)) (d (n "unidok-repr") (r "^0.1") (d #t) (k 0)))) (h "1vvic05mcxv4ba11gc54vkfxc42ial5rclm6gha4cy096ipv3w1m")))

(define-public crate-unidok-parser-0.2.0 (c (n "unidok-parser") (v "0.2.0") (d (list (d (n "aho-corasick") (r "^0.7.18") (d #t) (k 0)) (d (n "detached-str") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "unidok-repr") (r "^0.2") (d #t) (k 0)))) (h "05nvbf3f58jajd062g5h5vflhr36cvcghqmjpbz63xkw3azs80nx")))

