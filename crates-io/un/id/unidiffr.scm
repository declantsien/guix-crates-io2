(define-module (crates-io un id unidiffr) #:use-module (crates-io))

(define-public crate-unidiffr-0.2.0 (c (n "unidiffr") (v "0.2.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "scan-rules") (r "^0.2") (d #t) (k 0)))) (h "0cjscc7z32p0r9wvzbbbq1ggndyvi6apx89nhww1xc1z2kjs80mz")))

