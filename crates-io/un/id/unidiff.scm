(define-module (crates-io un id unidiff) #:use-module (crates-io))

(define-public crate-unidiff-0.1.0 (c (n "unidiff") (v "0.1.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0rd659gvhzc373gzyxg8szwww470ybh5qbmapm6fnm0yhnp1ih39") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.1.1 (c (n "unidiff") (v "0.1.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1crwxr5jfg5742xcdxmfjggb6sv2qyb1rplf5lxbkcc0h8xiddhn") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.1.2 (c (n "unidiff") (v "0.1.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "14646l5w0q4xlavm8b7hm2ljkxk11h79c6fk9zj8mdvp48xxyig8") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.1.3 (c (n "unidiff") (v "0.1.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "15bb4bdsk9k479h6lwxyiq78mlvlmmzdd7zir49whfy7631bh9zs") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.1.4 (c (n "unidiff") (v "0.1.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1a0jm28sm0j0hcqwcs69cbnvd5xqjmijcb4xa5k7zkx7544wa9sa") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.2.0 (c (n "unidiff") (v "0.2.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0f3jwhb1k7scqml4s6y1040qw8kl7abvgczp41j2r853by96fsll") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.2.1 (c (n "unidiff") (v "0.2.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1s6f8ixkigrswdghrzi1anl769ckk7c3vc1mcw21nnzgp4xncsw9") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.3.0 (c (n "unidiff") (v "0.3.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0b80fg9x9afkhv9dw28217fxs4mwh8hq2xmninksjl3b4nk0764x") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.3.1 (c (n "unidiff") (v "0.3.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "15hdhcv1rvyajvq0zhgc9bdk85kyzvf7vqm48jjlf3lwgdq9z23g") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.3.2 (c (n "unidiff") (v "0.3.2") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1s9scnljkps1ah0j6ck0c1bwy5f6ldnayg06l56s848yzmk2j59w") (f (quote (("unstable"))))))

(define-public crate-unidiff-0.3.3 (c (n "unidiff") (v "0.3.3") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0b13vhp2x7jlvmkm44h5niqcxklyrmz6afmppvykp4zimhcjg9nq") (f (quote (("unstable") ("encoding" "encoding_rs") ("default" "encoding"))))))

