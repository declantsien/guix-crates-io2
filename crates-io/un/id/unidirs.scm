(define-module (crates-io un id unidirs) #:use-module (crates-io))

(define-public crate-unidirs-0.1.0 (c (n "unidirs") (v "0.1.0") (d (list (d (n "camino") (r "^1.0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 2)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "14f6i5xzx7fikypdpp9p8gv7bmzp0axh58d4vj3ar5w4np2m9wpp") (r "1.56")))

(define-public crate-unidirs-0.1.1 (c (n "unidirs") (v "0.1.1") (d (list (d (n "camino") (r "^1.1.4") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "whoami") (r "^1.4.0") (d #t) (k 0)))) (h "0j2v0vigbn30cvzvx12wp9rnmc48xn7wldyzczya17pmslbswhc5") (r "1.60")))

