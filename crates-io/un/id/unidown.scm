(define-module (crates-io un id unidown) #:use-module (crates-io))

(define-public crate-unidown-0.1.0 (c (n "unidown") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "pulldown-cmark-escape") (r "^0.10.0") (d #t) (k 0)))) (h "01651c0mln6x5zjmk90aw67fxxlnnsgis7iqw4cl3yhz3wyvvyyi")))

(define-public crate-unidown-0.1.1 (c (n "unidown") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "pulldown-cmark-escape") (r "^0.10.0") (d #t) (k 0)))) (h "16mwbs4yj0vj099583ffvcizsspwllxgpfm09rj00y29gvxchmah")))

(define-public crate-unidown-0.1.2 (c (n "unidown") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "pulldown-cmark-escape") (r "^0.10.0") (d #t) (k 0)))) (h "00q1rybk4cc8cmvr3mrsd5328avjsrnsq9wncj6hp5qg7nyzhvhx")))

(define-public crate-unidown-0.2.0 (c (n "unidown") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "pulldown-cmark-escape") (r "^0.10.0") (d #t) (k 0)))) (h "0j2byqiblrbi8306n557cjazafq3fy7qx9qhb8xr1l8w2cvgspps")))

(define-public crate-unidown-0.3.0 (c (n "unidown") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "pulldown-cmark-escape") (r "^0.10.0") (d #t) (k 0)) (d (n "veg") (r "^0.5.0") (d #t) (k 0)))) (h "01xdh7xh54plf5k001fsvl6whykj0b4qj8fqjyvvjlyf0lw8vsf3")))

(define-public crate-unidown-0.4.0 (c (n "unidown") (v "0.4.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "pulldown-cmark-escape") (r "^0.10.0") (d #t) (k 0)) (d (n "veg") (r "^0.5.0") (d #t) (k 0)))) (h "0f1cqz8k52017i8xlpiqcgvb9mr9s00l8hy9yynvmgp2bwnr6j7a")))

(define-public crate-unidown-0.5.0 (c (n "unidown") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "pulldown-cmark-escape") (r "^0.10.0") (d #t) (k 0)) (d (n "veg") (r "^0.5.0") (d #t) (k 0)))) (h "0ixp8ljdmf1c51d12lmj9b42k7iqqk1pd9jwfgim8pmi8kfddaas")))

