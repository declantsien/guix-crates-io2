(define-module (crates-io un id unidok-repr) #:use-module (crates-io))

(define-public crate-unidok-repr-0.1.0 (c (n "unidok-repr") (v "0.1.0") (d (list (d (n "detached-str") (r "^0.1") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)))) (h "08z9phjkw016l44icg765g330sw8n5p8nxj6v4gszm4m0gf79s5r")))

(define-public crate-unidok-repr-0.2.0 (c (n "unidok-repr") (v "0.2.0") (d (list (d (n "detached-str") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)))) (h "0fxcjiaw60fmq5jvvj9z9hfkqcyngjvwiqak1c8ipql92cd460z7") (f (quote (("serde-spans" "serde"))))))

