(define-module (crates-io un co uncon) #:use-module (crates-io))

(define-public crate-uncon-1.0.0 (c (n "uncon") (v "1.0.0") (h "1b9vb48p5qn3m0vv4z6mk566a727rvhxdx9q1ivghvyi1dzq5ivi") (f (quote (("std") ("nightly" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-uncon-1.1.0 (c (n "uncon") (v "1.1.0") (h "00j21h1wwmcz0raxqkny6c1s31yacl4sf3j79h6sz0mkn8hxi6q7") (f (quote (("std") ("nightly" "alloc") ("default" "std") ("alloc"))))))

