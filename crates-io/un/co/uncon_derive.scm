(define-module (crates-io un co uncon_derive) #:use-module (crates-io))

(define-public crate-uncon_derive-1.0.0 (c (n "uncon_derive") (v "1.0.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uncon") (r "^1.0.0") (k 2)))) (h "16xlh9mj7y8cdkffh3yi7a8hycqzqc4rs9grdrbq8ywnrhlyjm81") (f (quote (("std" "uncon/std") ("default" "std"))))))

(define-public crate-uncon_derive-1.0.1 (c (n "uncon_derive") (v "1.0.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uncon") (r "^1.0.0") (k 2)))) (h "1b1c6fryf139jdr5wg8s8iz539llnrg3sqg1h2db3hrmajl8bcyr") (f (quote (("std" "uncon/std") ("default" "std"))))))

(define-public crate-uncon_derive-1.0.2 (c (n "uncon_derive") (v "1.0.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uncon") (r "^1.0.0") (k 2)))) (h "199ppfn4vyd7fbj21sk0qkmda02pifpb94babsy50xf7d1xb8cg6") (f (quote (("std") ("default" "std"))))))

(define-public crate-uncon_derive-1.0.3 (c (n "uncon_derive") (v "1.0.3") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^0.2.3") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uncon") (r "^1.0.0") (k 2)))) (h "0l6155vvc53s9pvl0bhhi3pbl7zbrndwc3b5vdc952xzylqmwyl1") (f (quote (("std") ("default" "std"))))))

(define-public crate-uncon_derive-1.0.4 (c (n "uncon_derive") (v "1.0.4") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^0.2.3") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uncon") (r "^1.1.0") (k 2)))) (h "1wyj2gj7y7vc7g07whrp1nhd12p8f4qwr0v3dqaq60limqjvb0fb") (f (quote (("std") ("default" "std"))))))

(define-public crate-uncon_derive-1.1.0 (c (n "uncon_derive") (v "1.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^0.2.3") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uncon") (r "^1.1.0") (k 2)))) (h "0sib9ina87chmg1rpknk687v477mxxl19907c9aqjrv8hdaflq7k") (f (quote (("std") ("default" "std"))))))

(define-public crate-uncon_derive-1.1.1 (c (n "uncon_derive") (v "1.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "static_assertions") (r "^0.2.3") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "uncon") (r "^1.1.0") (k 2)))) (h "0973kwsyx103c6hvyykj1bb093gibgqmd7h8km3jm14mkiyznbly") (f (quote (("std") ("default" "std"))))))

