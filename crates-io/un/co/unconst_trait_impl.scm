(define-module (crates-io un co unconst_trait_impl) #:use-module (crates-io))

(define-public crate-unconst_trait_impl-0.1.0 (c (n "unconst_trait_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0gqx0fz48d4xk4j3sn033mflivhb9n5132w0cl110dfdnwncwdcr") (r "1.56.1")))

(define-public crate-unconst_trait_impl-0.1.1 (c (n "unconst_trait_impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "10vznr6vwv7lpd1a5q93n1d7z40raphjm1hxd6x1azmm1j59x0ij") (r "1.56.1")))

(define-public crate-unconst_trait_impl-0.1.2 (c (n "unconst_trait_impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0af03ng5w0xj96s0b634kadmjl2709skibr8bdy79s8pr9b4i63i") (r "1.56.1")))

(define-public crate-unconst_trait_impl-0.1.3 (c (n "unconst_trait_impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1755ks1ygpmm6dzkh7fkbqk9fri50yfpbys2q075jbr001f0mq1p") (r "1.56.1")))

(define-public crate-unconst_trait_impl-0.1.4 (c (n "unconst_trait_impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1dm8kc9ndm9h8d8pw4046mhszw6rz8c6y15mrjm0vhnnsimdxh7m") (r "1.56.1")))

(define-public crate-unconst_trait_impl-0.1.5 (c (n "unconst_trait_impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0wyy516imirq9aqyy3sw7gkbl2pz022iqnpfzhwfhznpdsx5pkmn") (r "1.56.1")))

