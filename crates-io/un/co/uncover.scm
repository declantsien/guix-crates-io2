(define-module (crates-io un co uncover) #:use-module (crates-io))

(define-public crate-uncover-0.1.0 (c (n "uncover") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1v0dhg95si7bhis346rq9kdrklvmfxkki48b9ymmgzn7g3rav7hi") (y #t)))

(define-public crate-uncover-0.1.1 (c (n "uncover") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0rfnl2jfvd1qlmnw2m0ffqvaf3rdmh5h5m7g6rhj2w61mvqw5f4k")))

(define-public crate-uncover-0.1.2 (c (n "uncover") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "01lr67xmg31p0xyi5lmfn1523p2z0q0xahzdn8g6ndfg5jqqdsad")))

