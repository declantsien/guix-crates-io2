(define-module (crates-io un co unconst) #:use-module (crates-io))

(define-public crate-unconst-0.0.0 (c (n "unconst") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08x61jrl7w0syqn3mvxlmrl71jhyx2i0mhdqqgn7l7wka9h47s1k") (r "1.62.0")))

(define-public crate-unconst-0.0.1 (c (n "unconst") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10d3r7xx0l0rhjrbr8hppnrh4vbna0kgn9cn3xxpq2ihv907qxml") (r "1.62.0")))

(define-public crate-unconst-0.0.2 (c (n "unconst") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0avwhrh9fi5mqbmlybkhb7l8224whr0xcc8sg23zbj1l42gyls5x") (f (quote (("const")))) (r "1.62.0")))

(define-public crate-unconst-0.7.0 (c (n "unconst") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "extra-traits"))) (o #t) (d #t) (k 0)))) (h "0h7cl8y1q78kiplnl4f7pbj8bbap16wy7zjlnhah23phvmvznw44") (f (quote (("default" "proc-macro2" "quote" "syn") ("const")))) (r "1.62.0")))

(define-public crate-unconst-0.8.0 (c (n "unconst") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "extra-traits"))) (o #t) (d #t) (k 0)))) (h "1ry9a8l9ja0gi84v0b8vwq4q8b726ffs9gvvn5067nn2sdanxm2m") (f (quote (("default" "proc-macro2" "quote" "syn") ("const")))) (r "1.62.0")))

(define-public crate-unconst-0.9.0 (c (n "unconst") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "extra-traits"))) (o #t) (d #t) (k 0)))) (h "1fg9gvbzbz4hvlimi66daancq4gqgha6mybrr08czbiljw648bgb") (f (quote (("default" "proc-macro2" "quote" "syn") ("const")))) (r "1.62.0")))

