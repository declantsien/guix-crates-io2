(define-module (crates-io un ti untildify) #:use-module (crates-io))

(define-public crate-untildify-0.1.0 (c (n "untildify") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1w6pz2vdw914lar5zp3yj7glwxs7crix7f8smq5s53r7q96rg7a4")))

(define-public crate-untildify-0.1.1 (c (n "untildify") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1dvblh0s7jmc8yz73xxirn5cr1j9l3wyw1sa0m11j5yg008rwbcr")))

