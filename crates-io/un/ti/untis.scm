(define-module (crates-io un ti untis) #:use-module (crates-io))

(define-public crate-untis-0.1.0 (c (n "untis") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)))) (h "0r38gc3j16ywjxnkikavnqxl5pg96zwz7rby58v21qp1w9cbi1i1")))

(define-public crate-untis-0.2.0 (c (n "untis") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)))) (h "1i9l0f6rkwhzbyg18g7abdcaaim2nswjcbfpkfiyx2zz0dkhwh3n")))

(define-public crate-untis-0.2.1 (c (n "untis") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)))) (h "08zs45qyh4z0cbpipvf6r3abhbc7anz6bhz0bg19chb4wkchwna0")))

