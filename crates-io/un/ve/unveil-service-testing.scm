(define-module (crates-io un ve unveil-service-testing) #:use-module (crates-io))

(define-public crate-unveil-service-testing-0.1.0 (c (n "unveil-service-testing") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-native-tls" "postgres"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18kimqsbdf9y1pqzns1j7pnrwvm9ci68bpkxrhbyzpy4ck6ch7qj")))

