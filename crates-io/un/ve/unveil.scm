(define-module (crates-io un ve unveil) #:use-module (crates-io))

(define-public crate-unveil-0.1.0 (c (n "unveil") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pdcfjmryi6rq3j1xgm5ckbv154vr44gzci550yva9p6qxmaxm98")))

(define-public crate-unveil-0.2.0 (c (n "unveil") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rd0h34yz3cy6ibkbz8man7gqkddhx81m459s2jimhk85bqqhh3p")))

(define-public crate-unveil-0.2.1 (c (n "unveil") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fhq4grh0rdwbdjkhjjbm81jzwlrn1l5hm5dawa8r8bs403gv2d8")))

(define-public crate-unveil-0.3.0 (c (n "unveil") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c2xvd31zb5jpnsw2l1hhsssplxcdmzw3clpcnjinqnnxm9srz5d")))

(define-public crate-unveil-0.3.1 (c (n "unveil") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sv8ddfwzr1ygzwx85459hgcyxkcvmdb3g0jd2bi3a0bkqi29nmk")))

(define-public crate-unveil-0.3.2 (c (n "unveil") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1v5z9c480qlbkigscq1h46ajxy6m2znnahb9xh0j042rsmkshzsy")))

