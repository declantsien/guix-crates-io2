(define-module (crates-io un ve unveil-service) #:use-module (crates-io))

(define-public crate-unveil-service-0.1.0 (c (n "unveil-service") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0qp5n9i9if9fpbzkd78qm3hjbaacp0x0g0138cnsxiijmqlmrmdl")))

(define-public crate-unveil-service-0.2.0 (c (n "unveil-service") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-native-tls" "postgres"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1qarw1mlqlk6wyas6xpkzcqd4abjgl33hxv35178g12953dcbbca")))

(define-public crate-unveil-service-0.2.1 (c (n "unveil-service") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-native-tls" "postgres"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "0s35b1phvqppwk466iv91rjldkbijbpvc7ywsp5m6b3cf3m00la9")))

