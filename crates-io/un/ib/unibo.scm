(define-module (crates-io un ib unibo) #:use-module (crates-io))

(define-public crate-unibo-0.1.0 (c (n "unibo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde" "unstable-locales" "clock"))) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.4") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (d #t) (k 0)))) (h "0f9jm9zmjj836hv6d1990lihx39zms4b4k48kmjdzj355av4x9fz")))

