(define-module (crates-io un ib unibar) #:use-module (crates-io))

(define-public crate-unibar-0.1.0 (c (n "unibar") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.16") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18.5") (d #t) (k 0)))) (h "1qr2wcnmkas581qkg2a9k6v9g0pm23x4hi1sgh2mb7bblwl14k8k") (y #t)))

(define-public crate-unibar-0.1.1 (c (n "unibar") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.16") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18.5") (d #t) (k 0)))) (h "0rvfvmfncbgav5k31z03h1lcm8pbw377fw790p7cjzi93kvyzzq8")))

(define-public crate-unibar-0.1.2 (c (n "unibar") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.16") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18.5") (d #t) (k 0)))) (h "06vq3gdbbvyqxmai38cc940zgys2n1zwpg1h4iz7z747nic3qwmf")))

(define-public crate-unibar-0.1.3 (c (n "unibar") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.16") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18.5") (d #t) (k 0)))) (h "0cwywkkk1kava6rv5nq69jiz4mdl1dphvccblqvb7494pfs8n8kc")))

