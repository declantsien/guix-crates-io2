(define-module (crates-io un ne unnest) #:use-module (crates-io))

(define-public crate-unnest-0.0.6 (c (n "unnest") (v "0.0.6") (h "0pcvmgj8l2ygfimkj0dyh6kgzrg2y6bwwhn3vd8va4j5cvcg1bmc")))

(define-public crate-unnest-0.1.0 (c (n "unnest") (v "0.1.0") (h "1k5nrqp9da8ch5wz03pnf3lv6jd85x6k41fxbws4amx0xmf98bx3")))

(define-public crate-unnest-0.2.0 (c (n "unnest") (v "0.2.0") (h "05vjxhajna4x1gl179ciw618bm9qs14vxyn3r2x4sgs1drkhzcvx") (y #t)))

(define-public crate-unnest-0.3.0 (c (n "unnest") (v "0.3.0") (h "0k494g93v7l7sgcg5y2qkdvrriq76ql3fhsyyggmrf1787rdqhvx") (y #t)))

(define-public crate-unnest-0.3.1 (c (n "unnest") (v "0.3.1") (h "1f2wy2jcwqi82byh1l7pqg7jxf8xhvn0f53s96zzwjzyj6bka65r")))

