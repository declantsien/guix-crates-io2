(define-module (crates-io un ne unnest-ndjson) #:use-module (crates-io))

(define-public crate-unnest-ndjson-0.1.0 (c (n "unnest-ndjson") (v "0.1.0") (d (list (d (n "iowrap") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 2)))) (h "1gn6wlz7wyr87indlv3d69n7k7wp4q09jmp7pfrbb08b34835l6d")))

(define-public crate-unnest-ndjson-0.1.1 (c (n "unnest-ndjson") (v "0.1.1") (d (list (d (n "iowrap") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 2)))) (h "15k7az6k7q5jr7qwnsg7x8zv09gz8f37qycxa9n5niqi9238n4g1")))

