(define-module (crates-io un i- uni-path) #:use-module (crates-io))

(define-public crate-uni-path-1.51.0 (c (n "uni-path") (v "1.51.0") (h "1lgh4fz0j4bcfahll0i1is2va9rsv4d9dhxryki8d0jjp8brq5ia")))

(define-public crate-uni-path-1.51.1 (c (n "uni-path") (v "1.51.1") (h "0xb279nb7kqmvlbg0r0n30j8aff6gqdp2n3k1v15by5i0pajiqr5")))

