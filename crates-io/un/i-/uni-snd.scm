(define-module (crates-io un i- uni-snd) #:use-module (crates-io))

(define-public crate-uni-snd-0.1.0 (c (n "uni-snd") (v "0.1.0") (d (list (d (n "cpal") (r "^0.8") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "stdweb") (r "^0.4.8") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "uni-app") (r "0.1.*") (d #t) (k 0)))) (h "02hsakc2q694sqxy3vzbp21x6ynh2yb5yhzg6lvc01srxsqayy8d")))

(define-public crate-uni-snd-0.2.0 (c (n "uni-snd") (v "0.2.0") (d (list (d (n "cpal") (r "^0.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "cpal") (r "^0.14") (f (quote ("wasm-bindgen"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "uni-app") (r "0.2.*") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1y33dhwvwxh04p8xc07s8kqmrk67c6yc4lmg2gfla3rxl24plb7n")))

(define-public crate-uni-snd-0.2.1 (c (n "uni-snd") (v "0.2.1") (d (list (d (n "cpal") (r "^0.14") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "cpal") (r "^0.14") (f (quote ("wasm-bindgen"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "uni-app") (r "0.2.*") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1slb6slrk8dyr633mx6bmb682ls84ikkdjf0dls1z2756p1x9bs0")))

