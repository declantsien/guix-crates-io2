(define-module (crates-io un ke unkey) #:use-module (crates-io))

(define-public crate-unkey-0.1.0 (c (n "unkey") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "182z2dd5p0ssdhy2hjwl2cglkc557jimxsiiml2jp7j8ph3izv9i") (r "1.63.0")))

(define-public crate-unkey-0.2.0 (c (n "unkey") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1l48rgdf6jksnar891vxclwwyp71bpm7rh96sivr6g535lpwhcb0") (r "1.63.0")))

(define-public crate-unkey-0.3.0 (c (n "unkey") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zb5pck8b9hknx17fj1gjwl4bszb3ynyfpdd5ywgwjld8sq7di49") (r "1.63.0")))

