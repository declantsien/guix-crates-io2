(define-module (crates-io un em unempty) #:use-module (crates-io))

(define-public crate-unempty-0.1.0 (c (n "unempty") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1l8qn7bpyknrah17dvxsmjqhzms7d56vzllcs6k4kady13i6g5ch") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

