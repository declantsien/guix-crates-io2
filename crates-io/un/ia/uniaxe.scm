(define-module (crates-io un ia uniaxe) #:use-module (crates-io))

(define-public crate-uniaxe-0.1.0 (c (n "uniaxe") (v "0.1.0") (h "1na2i2hzmalm4c3g15gq01rq1casp4fh808wxkrrqy7afyw3miyd")))

(define-public crate-uniaxe-0.1.1 (c (n "uniaxe") (v "0.1.1") (h "0sx89m0wbxd36r9cilqfskl866admqd5fi5jam9w8zvda00s3y14")))

