(define-module (crates-io un oh unohup) #:use-module (crates-io))

(define-public crate-unohup-0.1.0 (c (n "unohup") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1kg73dsksgx8yjxhdizcv1bmbmrmzcjiaf9i3ggviq2258dpdhac")))

