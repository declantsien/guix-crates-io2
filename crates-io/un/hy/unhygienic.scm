(define-module (crates-io un hy unhygienic) #:use-module (crates-io))

(define-public crate-unhygienic-0.1.0 (c (n "unhygienic") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "unhygienic-impl") (r "^0.1.0") (d #t) (k 0)))) (h "1xahsqv4c03m7sjx02jlgsgi22ahpy2c2rca483y4dblqd6z791n")))

(define-public crate-unhygienic-0.1.1 (c (n "unhygienic") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "unhygienic-impl") (r "^0.1.0") (d #t) (k 0)))) (h "14ryxxfg8pdlzk1z4vlhanp4anjr3c74andnf73xn12cly5swn1h")))

