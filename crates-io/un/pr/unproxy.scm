(define-module (crates-io un pr unproxy) #:use-module (crates-io))

(define-public crate-unproxy-0.1.0 (c (n "unproxy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-http-proxy") (r "^1.2.5") (f (quote ("runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.23.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.4") (d #t) (k 0)))) (h "1qk4il35qy87jhkhf1krc8014iamd4csjr2v0mw54vz4i292pd30")))

