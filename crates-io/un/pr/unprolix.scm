(define-module (crates-io un pr unprolix) #:use-module (crates-io))

(define-public crate-unprolix-0.1.0 (c (n "unprolix") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "070a7vccvrf76p3n3nlf5vz8yz4dyib5any6n06zfc58bwdbrjx5")))

