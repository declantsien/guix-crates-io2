(define-module (crates-io un pr unpro) #:use-module (crates-io))

(define-public crate-unpro-0.1.0 (c (n "unpro") (v "0.1.0") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0wgc91h2fp5pk5jkvc1c5mcca5lfpah9f6bki3dx3r26v97ys4jm")))

(define-public crate-unpro-0.2.0 (c (n "unpro") (v "0.2.0") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1cynny72qj283f40066gysb4dhwyzcwp2s5qlla3zrg20bbd7k9q")))

