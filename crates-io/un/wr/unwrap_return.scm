(define-module (crates-io un wr unwrap_return) #:use-module (crates-io))

(define-public crate-unwrap_return-0.1.0 (c (n "unwrap_return") (v "0.1.0") (h "1ji6ivgvjb7kk22dsmz2wdc200m13crzaz0frz5m8kyqvhc5pcpd")))

(define-public crate-unwrap_return-0.1.1 (c (n "unwrap_return") (v "0.1.1") (h "1fkanzz8iv1mfdp55nzpsicyi20x6ym7j0vxxhccdf3zv1sc2ll4")))

(define-public crate-unwrap_return-0.1.2 (c (n "unwrap_return") (v "0.1.2") (h "0jfqkwc1afhpfi2qccbdrjqaf6q9dgzr36dld1aszdfbxhmhwbzc")))

