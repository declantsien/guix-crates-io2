(define-module (crates-io un wr unwrap_all) #:use-module (crates-io))

(define-public crate-unwrap_all-0.1.0 (c (n "unwrap_all") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0j1fjgcw3x0vc0mx8sxp2vfglg7s4jm3xhbv7lckf3byixbfcjj0")))

(define-public crate-unwrap_all-0.2.0 (c (n "unwrap_all") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "08ldjhjnhmfq47ikcdnlm6bcjk2nvvk046ksaki9wqvcmhh22s5v")))

