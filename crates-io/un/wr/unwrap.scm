(define-module (crates-io un wr unwrap) #:use-module (crates-io))

(define-public crate-unwrap-1.0.0 (c (n "unwrap") (v "1.0.0") (h "1c0mma8ih13kp09h8zvniy2cr4l48i4956755zmc22bi75bzc2yp")))

(define-public crate-unwrap-0.2.0 (c (n "unwrap") (v "0.2.0") (h "1fs30rzgvppfmxqi8i6y3iclf60awd493g7r6n0vv5lf2nin155s")))

(define-public crate-unwrap-1.1.0 (c (n "unwrap") (v "1.1.0") (h "0wm4zzm684d5d1lnjvvzzwazlzq7zs7j76x36842zb398bsn34wr")))

(define-public crate-unwrap-1.2.0 (c (n "unwrap") (v "1.2.0") (h "15cjb34cj85hgc4hkyjf70snxiqwqgqala9krby8qqsrvjk7apj6")))

(define-public crate-unwrap-1.2.1 (c (n "unwrap") (v "1.2.1") (h "03y24m63l85ng23z19bg7vwn6g1h1asg6ldyqwifca23sy6n8cvy")))

