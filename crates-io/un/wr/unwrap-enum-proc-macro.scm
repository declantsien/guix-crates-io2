(define-module (crates-io un wr unwrap-enum-proc-macro) #:use-module (crates-io))

(define-public crate-unwrap-enum-proc-macro-0.0.2 (c (n "unwrap-enum-proc-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quasiquote") (r "=0.0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0nghcxsn533xh28ri84hfd7s6z64v1b679m5fi58jxds8hhxa4rf")))

