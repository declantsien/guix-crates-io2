(define-module (crates-io un wr unwrap_or) #:use-module (crates-io))

(define-public crate-unwrap_or-1.0.0 (c (n "unwrap_or") (v "1.0.0") (h "0bblwm8i2j1rzc1siglxglvl2ql6nv8bv0vghly2n25gi5hfkgrg")))

(define-public crate-unwrap_or-1.0.1 (c (n "unwrap_or") (v "1.0.1") (h "0jjcf9ni36579kqhrgkmk7y4d22gk0png96qjz72i5cp97qf2bwg")))

