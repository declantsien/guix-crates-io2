(define-module (crates-io un wr unwrap-ord) #:use-module (crates-io))

(define-public crate-unwrap-ord-0.1.0 (c (n "unwrap-ord") (v "0.1.0") (h "0vg9yvgajrwgff0j7cm2glgg24n5qaglpyk0p1cgxs745zvix0xq")))

(define-public crate-unwrap-ord-0.1.1 (c (n "unwrap-ord") (v "0.1.1") (h "1r53v9qkbml39lg8x55bg34xbc9l5jfmx5sfjysq982jkdfxnisk")))

(define-public crate-unwrap-ord-0.1.2 (c (n "unwrap-ord") (v "0.1.2") (h "0vv7lq3vi6akxjg2b3pyvi5qiy41kwy5ilhvd02x7li6179vxmai")))

(define-public crate-unwrap-ord-0.1.3 (c (n "unwrap-ord") (v "0.1.3") (h "07krnmacn2aml3j9xcsk4xh7igq5q8r8h1hrsv42rh7f3vyaflx2")))

