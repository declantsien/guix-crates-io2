(define-module (crates-io un wr unwrap_or_do) #:use-module (crates-io))

(define-public crate-unwrap_or_do-0.1.0 (c (n "unwrap_or_do") (v "0.1.0") (h "1hxsgmnj2gmc1y88dsf66vgvamrf2xhlz68lmq60fdfhgcv4l42r") (f (quote (("std"))))))

(define-public crate-unwrap_or_do-0.1.1 (c (n "unwrap_or_do") (v "0.1.1") (h "0b2y0x7ybcjhabhv8z8xfjjl9qfcbwf7jaw00gg4ghbb2xzkaq13") (f (quote (("std"))))))

(define-public crate-unwrap_or_do-0.1.2 (c (n "unwrap_or_do") (v "0.1.2") (h "1ys1xpx55sbaahhga54wpfafc82xl2ki3ncg2xg66s9rdwc15y60") (f (quote (("std"))))))

