(define-module (crates-io un wr unwrap_or_panic) #:use-module (crates-io))

(define-public crate-unwrap_or_panic-0.1.0 (c (n "unwrap_or_panic") (v "0.1.0") (h "16v8g9mqj70gggz4zsba6vb7bnki7sdnnbirrpvl517h5b1794vm") (f (quote (("std") ("default"))))))

(define-public crate-unwrap_or_panic-0.1.1 (c (n "unwrap_or_panic") (v "0.1.1") (h "0lz9570f66c94fk4gzp8bnvvh94yrcssjaa9j68h1ivb5991zlpk") (f (quote (("default"))))))

(define-public crate-unwrap_or_panic-0.2.0 (c (n "unwrap_or_panic") (v "0.2.0") (h "1dv29jk31qdwhg4afanxdag1azkwadq70g00wkgk6qf55mdb0cz6") (f (quote (("panic_location") ("default"))))))

(define-public crate-unwrap_or_panic-0.2.1 (c (n "unwrap_or_panic") (v "0.2.1") (h "13n4s2pzz1vyaa6qlxv0yqdv2m0k2jzwibpz6cjd3rbqjnyxw4z6") (f (quote (("panic_location") ("default" "panic_location"))))))

(define-public crate-unwrap_or_panic-0.2.2 (c (n "unwrap_or_panic") (v "0.2.2") (h "105a5d7djiw3zigwi6frirqlwkrpxgihhvrmav97hna173lg02gz") (f (quote (("track_caller") ("panic_location") ("default" "panic_location" "track_caller"))))))

(define-public crate-unwrap_or_panic-0.2.3 (c (n "unwrap_or_panic") (v "0.2.3") (h "0hbjl2pl2cixy17503nw9i5a5nk67z32hf918bgc8cil8xwg6lv4") (f (quote (("track_caller") ("panic_location") ("default" "track_caller"))))))

(define-public crate-unwrap_or_panic-0.3.0 (c (n "unwrap_or_panic") (v "0.3.0") (h "1l5rxvhwp9bd4g3agri3j8a6hhy2jnbssgzclv6r0lm3fprirfsw") (f (quote (("track_caller") ("panic_location") ("never_inline") ("default" "track_caller"))))))

