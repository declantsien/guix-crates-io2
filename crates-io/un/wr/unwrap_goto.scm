(define-module (crates-io un wr unwrap_goto) #:use-module (crates-io))

(define-public crate-unwrap_goto-0.1.0 (c (n "unwrap_goto") (v "0.1.0") (h "1qbx3hxfa2jd7d5j67vhcgxxk9sb3pp1m9y57c1p71c0wf8c3pkm")))

(define-public crate-unwrap_goto-0.1.1 (c (n "unwrap_goto") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 2)))) (h "0g2c2ijfn2x198j0jqb21xbdd6a7jamaivgrk6fzz3hb0bxagcp4")))

(define-public crate-unwrap_goto-0.1.2 (c (n "unwrap_goto") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 2)))) (h "0v20c963f1fdvxhm6b0rfmh9mlvaywz9fn3nglpyf2ny5d9qpbhn")))

