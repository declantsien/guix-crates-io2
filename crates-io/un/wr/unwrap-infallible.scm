(define-module (crates-io un wr unwrap-infallible) #:use-module (crates-io))

(define-public crate-unwrap-infallible-0.1.0 (c (n "unwrap-infallible") (v "0.1.0") (h "1ja59hpjk52ynzm2jbafayk1dnrz74831k3cr18na5x9ylyslx26") (f (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1.1 (c (n "unwrap-infallible") (v "0.1.1") (h "1ah407i353942p5kzpgfr50a37d5i6a5klmhg93avci2k8mq7lcc") (f (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1.2 (c (n "unwrap-infallible") (v "0.1.2") (h "0kgcz5l2mjqsp1spq5wbsnyns8yvyf72wrk60irkmsrkrwpk2yjx") (f (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1.3 (c (n "unwrap-infallible") (v "0.1.3") (h "0mnd8h801190yp31vgm5qfn6f8j3j6094z6vqa2fxsy5qv225hw5") (f (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1.4 (c (n "unwrap-infallible") (v "0.1.4") (h "1c4qqcah8mdx7hvmjh5j85aci32dsjbsgn0qpmvigjwyb6jh4cvz") (f (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1.5 (c (n "unwrap-infallible") (v "0.1.5") (h "1yw1wfv63w5b2r7wpi0b2mrcrqpfxks5gd9r9qn8dhnkg2cw06hm") (f (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

