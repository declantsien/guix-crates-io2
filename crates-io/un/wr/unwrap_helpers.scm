(define-module (crates-io un wr unwrap_helpers) #:use-module (crates-io))

(define-public crate-unwrap_helpers-0.1.0 (c (n "unwrap_helpers") (v "0.1.0") (d (list (d (n "loop_unwrap") (r "^0.2") (d #t) (k 0)))) (h "1d2bdhiy2yrfq92fgyca39522qizw1vzfbfmh865m6fbxpm9d0hc")))

(define-public crate-unwrap_helpers-0.2.0 (c (n "unwrap_helpers") (v "0.2.0") (d (list (d (n "loop_unwrap") (r "^0.2") (d #t) (k 0)))) (h "1y15qc4mm8vkmd077f631v5r30pnkgwzl6k9z66kns0dfdb0lg84")))

(define-public crate-unwrap_helpers-0.2.1 (c (n "unwrap_helpers") (v "0.2.1") (d (list (d (n "loop_unwrap") (r "^0.2") (d #t) (k 0)))) (h "16s4qcbkd87lvpv4276ch2d1wh7vnxk106icj14af448mhqidcyi") (y #t)))

(define-public crate-unwrap_helpers-0.2.2 (c (n "unwrap_helpers") (v "0.2.2") (d (list (d (n "loop_unwrap") (r "^0.2") (d #t) (k 0)))) (h "122fmm4lrwc9aw9s2a5acr5s01pyrh7sxkyllgsviw1ayxqmrwgr")))

(define-public crate-unwrap_helpers-0.3.0 (c (n "unwrap_helpers") (v "0.3.0") (d (list (d (n "loop_unwrap") (r "^0.2") (d #t) (k 0)) (d (n "unwrap_helpers_proc_macros") (r "^0.1") (d #t) (k 0)))) (h "1pkhz0c527msmv35vhgzn3v728fbf5f3cj8km6c3qwkmgjc14rnf")))

