(define-module (crates-io un wr unwrap-gui) #:use-module (crates-io))

(define-public crate-unwrap-gui-0.1.0 (c (n "unwrap-gui") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "native-dialog") (r "^0.7.0") (d #t) (t "cfg(any(target_os = \"windows\", target_os = \"linux\", target_os = \"macos\"))") (k 0)))) (h "00mjkni47978548gjhv7lficiaj6qf7y54wjk3p0ngsn8ag7v1fj")))

