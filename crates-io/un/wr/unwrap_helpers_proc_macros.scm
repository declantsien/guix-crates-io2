(define-module (crates-io un wr unwrap_helpers_proc_macros) #:use-module (crates-io))

(define-public crate-unwrap_helpers_proc_macros-0.1.0 (c (n "unwrap_helpers_proc_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "06h2b7kfyiyixnwj2m0dmkmg9sbadp6kqqybkwmdjg1wc4iicw7r")))

(define-public crate-unwrap_helpers_proc_macros-0.1.1 (c (n "unwrap_helpers_proc_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "065b06j8vg9rcrx08fvgmhrnp2y4brlfp47am4z0fsxl57v1gggj")))

