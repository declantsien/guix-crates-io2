(define-module (crates-io un wr unwrap_none) #:use-module (crates-io))

(define-public crate-unwrap_none-0.1.0 (c (n "unwrap_none") (v "0.1.0") (h "0pix10dnqak94qjsl1myyr8c09nrm716ldc59rcds1qigs0cfkrw")))

(define-public crate-unwrap_none-0.1.1 (c (n "unwrap_none") (v "0.1.1") (h "1a13yhlmzrb45dhj6i04v0110qj30v3ziyrj13ivzcilkaibmkah")))

(define-public crate-unwrap_none-0.1.2 (c (n "unwrap_none") (v "0.1.2") (h "1a9cq0braa4s71qq6vvgv1spk6nw98g9cfisq3n2iizwarchq7a6")))

