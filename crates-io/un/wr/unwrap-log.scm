(define-module (crates-io un wr unwrap-log) #:use-module (crates-io))

(define-public crate-unwrap-log-0.1.0 (c (n "unwrap-log") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nxz5lnw1lcd895g3xn7kcbni5rcqhamfyan5bvp715nhg2ysb7q")))

(define-public crate-unwrap-log-0.1.1 (c (n "unwrap-log") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hi4k4m88gll46a84cmdi5sxf0pflfcdbm1b0ib8ar0vb0m09s9w")))

