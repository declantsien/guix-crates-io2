(define-module (crates-io un wr unwrap_or_log) #:use-module (crates-io))

(define-public crate-unwrap_or_log-0.1.0 (c (n "unwrap_or_log") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 2)))) (h "1v2vp9d63d6vk1j2f1jgjvgkcjvwrqjn56x0rixc66jyhfd5h527")))

(define-public crate-unwrap_or_log-0.2.0 (c (n "unwrap_or_log") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 2)))) (h "0v518yxcv1ydsq74a1g1qz43zy2932rmpl8wzfn0xjjqfxg547cc")))

