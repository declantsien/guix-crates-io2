(define-module (crates-io un ro unros) #:use-module (crates-io))

(define-public crate-unros-0.1.0 (c (n "unros") (v "0.1.0") (d (list (d (n "unros-core") (r "^0") (d #t) (k 0)) (d (n "unros-macros") (r "^0") (d #t) (k 0)))) (h "16v20543f7zalmqgm4hfks0zv1b4rvjfz5v5filczkgymrhg5n1p")))

(define-public crate-unros-0.1.1 (c (n "unros") (v "0.1.1") (d (list (d (n "unros-core") (r "^0") (d #t) (k 0)) (d (n "unros-macros") (r "^0") (d #t) (k 0)))) (h "17dqgnarkdggqx2lmaj7y2kbymhlp80jv48sfbsq6rl4f1ly9s6v")))

