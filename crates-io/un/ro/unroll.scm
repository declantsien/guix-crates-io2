(define-module (crates-io un ro unroll) #:use-module (crates-io))

(define-public crate-unroll-0.1.2 (c (n "unroll") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0219c13cjv2qifnnvajcw3dpkks7i8fbdsikmadvdxvkrwc7f2ml") (f (quote (("unstable" "criterion/real_blackbox"))))))

(define-public crate-unroll-0.1.3 (c (n "unroll") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "13vp084jijn571ak4xvlqpz6kn98l6xk14wvl7sry5fyly4w564v") (f (quote (("unstable" "criterion/real_blackbox"))))))

(define-public crate-unroll-0.1.4 (c (n "unroll") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "02bfxm2fqqhc9ncc9i12v9vij25hsnn4np2pvhzg6kbjv54hp2c5") (f (quote (("unstable" "criterion/real_blackbox"))))))

(define-public crate-unroll-0.1.5 (c (n "unroll") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "067n9inphcx6416dzbadglbsqd9am4hpf1rnz1q1m6vrrg0linas") (f (quote (("unstable" "criterion/real_blackbox"))))))

