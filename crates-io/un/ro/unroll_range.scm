(define-module (crates-io un ro unroll_range) #:use-module (crates-io))

(define-public crate-unroll_range-0.1.0 (c (n "unroll_range") (v "0.1.0") (h "0fxlhvc2g5ar5cz47g1rpqqg3lp6h90fxnzmfglb7kwy4p66iy1y")))

(define-public crate-unroll_range-0.2.0 (c (n "unroll_range") (v "0.2.0") (h "1q4r8xzdlg839fcaivx68g550zkrqqpn0knd8lpslmapybxkiial")))

