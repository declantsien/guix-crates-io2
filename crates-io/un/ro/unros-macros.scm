(define-module (crates-io un ro unros-macros) #:use-module (crates-io))

(define-public crate-unros-macros-0.1.0 (c (n "unros-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a8i52d9x6xmmy3ng91ks1hyai4yzgm39a6dd7245k82sp13ic62")))

(define-public crate-unros-macros-0.1.1 (c (n "unros-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04l8hxn0q1dcv1ig5l37zrlfj0yc7vk04mmfh3z8i77cpzij5nfd")))

