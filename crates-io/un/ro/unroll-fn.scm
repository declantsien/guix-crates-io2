(define-module (crates-io un ro unroll-fn) #:use-module (crates-io))

(define-public crate-unroll-fn-0.1.0 (c (n "unroll-fn") (v "0.1.0") (d (list (d (n "seq-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0sz2c3ilpkpppnc2vqax4bns6rgz37fxbkiacwzs0y4sha2xvbcv")))

(define-public crate-unroll-fn-0.1.1 (c (n "unroll-fn") (v "0.1.1") (d (list (d (n "seq-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0zg8gfdhsvqwma89q2jmxv1zc9ynhrdlzv5b2wvzy3vwc58fa1yk")))

