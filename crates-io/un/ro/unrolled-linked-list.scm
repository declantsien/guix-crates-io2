(define-module (crates-io un ro unrolled-linked-list) #:use-module (crates-io))

(define-public crate-unrolled-linked-list-1.0.0 (c (n "unrolled-linked-list") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "1m73z19jr4pqljj4xhai24aasqpp3bi8qnv86q2fw0ikp3ylsjxz")))

(define-public crate-unrolled-linked-list-1.0.1 (c (n "unrolled-linked-list") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "1vfk0x5kp1bp28xak1gzn0zl6padwg75wfzq2ah4z1p4w7hr3mqz")))

