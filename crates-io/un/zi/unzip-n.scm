(define-module (crates-io un zi unzip-n) #:use-module (crates-io))

(define-public crate-unzip-n-0.1.0 (c (n "unzip-n") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1mf28jicpjdavhz02w0v29r5q61zzgiksn8d5iw23jibn7fgn0gw")))

(define-public crate-unzip-n-0.1.1 (c (n "unzip-n") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1mgk6gx2v2h1r8h8rshi24lc5s7fasl9pclhyvmlx3bljh24gyjl")))

(define-public crate-unzip-n-0.1.2 (c (n "unzip-n") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "173bi6p3vypprk0b07vpj7zcc5n5qimy3460587pyi4n0mdfiry2")))

