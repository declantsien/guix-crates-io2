(define-module (crates-io un zi unzip3) #:use-module (crates-io))

(define-public crate-unzip3-0.1.0 (c (n "unzip3") (v "0.1.0") (h "1q3cnyixlvf8kj8av2k9y3cnigaxd1kqd9ih2zjlrlk0mf5vnv2g") (y #t)))

(define-public crate-unzip3-0.1.1 (c (n "unzip3") (v "0.1.1") (h "19pscnph9d0g6lx00l8pzgd7jxjr1qvryrk8ppnf9659hk7hv6rj") (y #t)))

(define-public crate-unzip3-1.0.0 (c (n "unzip3") (v "1.0.0") (h "1mhsjdpdsc24phgmqxh9nbjyrj55jhpynbq3di3h30mhd8qyrh4r")))

