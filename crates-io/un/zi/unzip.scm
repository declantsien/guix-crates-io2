(define-module (crates-io un zi unzip) #:use-module (crates-io))

(define-public crate-unzip-0.1.0 (c (n "unzip") (v "0.1.0") (d (list (d (n "temporary") (r "^0.6.3") (d #t) (k 2)) (d (n "time") (r "^0.1.38") (d #t) (k 0)) (d (n "transformation-pipeline") (r "^0.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 0)))) (h "1z9x45y9ab7mf2f9h4zqkfq0jjv5x4wvm8fmxih4nnz9m8mrmrch")))

