(define-module (crates-io un zi unzip-rs) #:use-module (crates-io))

(define-public crate-unzip-rs-0.1.0 (c (n "unzip-rs") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y37375zm5yxc34bkmng9p2dyj8j7a7y6b5y3hixzcr3agrnmywf") (y #t)))

(define-public crate-unzip-rs-0.1.1 (c (n "unzip-rs") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d0sc9xf0cla0ay53vbwxfzkxdk18ijhjvdgx2z2plzrrhyp1rfc") (y #t)))

(define-public crate-unzip-rs-0.1.2 (c (n "unzip-rs") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hrinf7vpbdm2l7dv9ns9fg2bv972kp2q88hhrkynlh1qmb1bflf") (y #t)))

(define-public crate-unzip-rs-0.1.3 (c (n "unzip-rs") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0azj2b5hr35vbhfxix3aa1516dyfsj23jikiwilg0wrjh1q4ariq") (y #t)))

(define-public crate-unzip-rs-0.2.0 (c (n "unzip-rs") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0c2vh2fs2s0jy8gpih26jx2sasj03s8bkx6gqppgikmy9r0ma122")))

