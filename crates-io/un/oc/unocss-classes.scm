(define-module (crates-io un oc unocss-classes) #:use-module (crates-io))

(define-public crate-unocss-classes-0.0.1 (c (n "unocss-classes") (v "0.0.1") (d (list (d (n "classes") (r "^1.0.0") (d #t) (k 0)) (d (n "unocss-classes-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "174d13dn82v5656f9kh30whcd6pnm6xbvg9y5bfh8vwbgfydk4cv") (s 2) (e (quote (("runtime" "dep:unocss-classes-utils"))))))

(define-public crate-unocss-classes-1.0.0 (c (n "unocss-classes") (v "1.0.0") (d (list (d (n "classes") (r "^1.0.0") (d #t) (k 0)) (d (n "unocss-classes-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "03vb2fynd4k5klavcz2f3vv9knznrq9xjgzgpdvyhbiilip6fi4z") (s 2) (e (quote (("runtime" "dep:unocss-classes-utils"))))))

(define-public crate-unocss-classes-1.1.0 (c (n "unocss-classes") (v "1.1.0") (d (list (d (n "classes") (r "^1.0.0") (d #t) (k 0)) (d (n "unocss-classes-macros") (r "^1.1.0") (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "16jqc402088zbdjizdqvv562a3dmnsm88b5a4k745pwmr9z4ccg0") (s 2) (e (quote (("runtime" "dep:unocss-classes-utils"))))))

(define-public crate-unocss-classes-1.1.1 (c (n "unocss-classes") (v "1.1.1") (d (list (d (n "classes") (r "^1.0.0") (d #t) (k 0)) (d (n "unocss-classes-macros") (r "^1.1.1") (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "0svi5l7372dzp3v8z5psq4hmf9x98m5zdlj3588zr3pvsh3vxp4m") (s 2) (e (quote (("runtime" "dep:unocss-classes-utils"))))))

(define-public crate-unocss-classes-2.0.0 (c (n "unocss-classes") (v "2.0.0") (d (list (d (n "classes") (r "^1.0.0") (d #t) (k 0)) (d (n "unocss-classes-macros") (r "^2.0.0") (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1r7s5fdmg2zgpcg1yhnv2mjapx0lyvcaqrdkl96sqcg6yr8pwk03") (f (quote (("dioxus")))) (s 2) (e (quote (("runtime" "dep:unocss-classes-utils"))))))

