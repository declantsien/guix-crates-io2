(define-module (crates-io un oc unocss-classes-macros) #:use-module (crates-io))

(define-public crate-unocss-classes-macros-0.0.1 (c (n "unocss-classes-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^0.0.1") (d #t) (k 0)))) (h "0rdxjp9zrd4hida7fdxq1kv9zgqzrc9hjcg9vd0612m4mc760d4g")))

(define-public crate-unocss-classes-macros-1.0.0 (c (n "unocss-classes-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^1.0.0") (d #t) (k 0)))) (h "1jxhjf13sccy68kq9587kksrqp1qdnvglwhfjb5dnw8iasz1q6vl")))

(define-public crate-unocss-classes-macros-1.1.0 (c (n "unocss-classes-macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^1.1.0") (d #t) (k 0)))) (h "1c297d317sqlgzbp8kp8q1ib8g7jbcq4kfrd56wacjiplpmmhfwn")))

(define-public crate-unocss-classes-macros-1.1.1 (c (n "unocss-classes-macros") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^1.1.1") (d #t) (k 0)))) (h "0s1v9gnp337266nf9wcwlkw5prvp6rxzq2gjd9wqixqgc003j05x")))

(define-public crate-unocss-classes-macros-2.0.0 (c (n "unocss-classes-macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "unocss-classes-utils") (r "^2.0.0") (d #t) (k 0)))) (h "0q6ibkrd441hx15cw8icp5hcwv0nbwrniwik7qfq1i2m6haq10l7")))

