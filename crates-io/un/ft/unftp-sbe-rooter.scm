(define-module (crates-io un ft unftp-sbe-rooter) #:use-module (crates-io))

(define-public crate-unftp-sbe-rooter-0.1.0 (c (n "unftp-sbe-rooter") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "libunftp") (r "^0.18.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)) (d (n "unftp-sbe-fs") (r "^0.2.2") (d #t) (k 2)))) (h "0xl6arwyfyx7k8agz9hcv0mmkqpb6kd8z6w0s82951yzi9ivrhks")))

(define-public crate-unftp-sbe-rooter-0.2.0 (c (n "unftp-sbe-rooter") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "libunftp") (r "^0.19.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "unftp-sbe-fs") (r "^0.2.3") (d #t) (k 2)))) (h "14x3iraxhs45c3ifmfwiqw4fk0qxppkf06r6myy7im9c36rqxjqi")))

(define-public crate-unftp-sbe-rooter-0.2.1 (c (n "unftp-sbe-rooter") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "libunftp") (r "^0.20.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "unftp-sbe-fs") (r "^0.2.5") (d #t) (k 2)))) (h "0g4as24xwzsnc9a2vcmirsrkdjj03hhz5z0w9jy1wir1z22kx4mh")))

