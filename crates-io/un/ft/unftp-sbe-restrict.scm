(define-module (crates-io un ft unftp-sbe-restrict) #:use-module (crates-io))

(define-public crate-unftp-sbe-restrict-0.1.0 (c (n "unftp-sbe-restrict") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libunftp") (r "^0.18.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "unftp-sbe-fs") (r "^0.2.2") (d #t) (k 2)))) (h "0999vlb1gv24d9v83cnyxpxvif52vlbczd8ddc9jmmg760vw02pa")))

(define-public crate-unftp-sbe-restrict-0.1.1 (c (n "unftp-sbe-restrict") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "libunftp") (r "^0.19.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "unftp-sbe-fs") (r "^0.2.3") (d #t) (k 2)))) (h "0s5nd718p16wrzss1xcrzkjm31yvnh95q10amvh3jl04cc0yga63")))

(define-public crate-unftp-sbe-restrict-0.1.2 (c (n "unftp-sbe-restrict") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "libunftp") (r "^0.20.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "unftp-sbe-fs") (r "^0.2.5") (d #t) (k 2)))) (h "0fm41kx3s7rvz3n14cnbc9v6czbdiizzfdg7xd0ik44h2m5wc4vw")))

