(define-module (crates-io un c- unc-vm-types) #:use-module (crates-io))

(define-public crate-unc-vm-types-0.1.0 (c (n "unc-vm-types") (v "0.1.0") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1s0p5bbg5ffz4ca3skp0zxm4v70zqjd7iaqkc4bpvqhk4n1bj3km")))

(define-public crate-unc-vm-types-0.2.0 (c (n "unc-vm-types") (v "0.2.0") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0d9pkbm85jh3i4zszviv8ycdg2kgs42m83g0vmbvbl4s4ayii7d8")))

(define-public crate-unc-vm-types-0.5.1 (c (n "unc-vm-types") (v "0.5.1") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1sjl3y5r5nhklwg6s6xhv2m6wkbhrc2hwk59ffrxki4rvvp6y7mq")))

(define-public crate-unc-vm-types-0.5.2 (c (n "unc-vm-types") (v "0.5.2") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0yxqw5x0mrfg62bfr2r1jvcacj6gvkrk2fscwic6d8xkn5yc9nwl")))

(define-public crate-unc-vm-types-0.6.0 (c (n "unc-vm-types") (v "0.6.0") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0fkf4jx4ccf0zh2xfcqplvhq1iw9jj4cwk6d2lz38ql72w1ck4rb")))

(define-public crate-unc-vm-types-0.6.1 (c (n "unc-vm-types") (v "0.6.1") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0b72nyhcv87l47dh9g5c66cinbfq9kgik56h9ka4ssxsrfrqkl72")))

(define-public crate-unc-vm-types-0.6.2 (c (n "unc-vm-types") (v "0.6.2") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0fb1lggprk0bmyi42mmd8q9kl3bh880ab4hgkz50zazan3fxq2dq")))

(define-public crate-unc-vm-types-0.7.0 (c (n "unc-vm-types") (v "0.7.0") (d (list (d (n "bolero") (r "^0.10.0") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0nzv6n4h7k155bx4zh0r3h2rcn41424nqv4pf2i5c5nynx4y31sw")))

(define-public crate-unc-vm-types-0.7.1 (c (n "unc-vm-types") (v "0.7.1") (d (list (d (n "bolero") (r "^0.10.1") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ldid2gasqy6q17l6rdxfxwrdykqi1z47qr7amblqs5ccxc27asm")))

(define-public crate-unc-vm-types-0.7.2 (c (n "unc-vm-types") (v "0.7.2") (d (list (d (n "bolero") (r "^0.10.1") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1759ml6yk99wc4chcgbbdvpmcskqdqrd5hfpnwx53yk06c52n0mp")))

(define-public crate-unc-vm-types-0.7.3 (c (n "unc-vm-types") (v "0.7.3") (d (list (d (n "bolero") (r "^0.10.1") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1lhd97vlyrjsldzlicr0aw3vr03y2l60bj6hkmnxd4251gjy1y6n")))

(define-public crate-unc-vm-types-0.10.0 (c (n "unc-vm-types") (v "0.10.0") (d (list (d (n "bolero") (r "^0.10.1") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0f0s7pwj4x6c19j8i45gg4gr566nk1qyg56p66naqj1wpbg89a3w")))

(define-public crate-unc-vm-types-0.10.2 (c (n "unc-vm-types") (v "0.10.2") (d (list (d (n "bolero") (r "^0.10.1") (f (quote ("arbitrary"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "14lr56s3nnhwjg49f8ivzxib4l34ckr5rgwvlf1rhsrlgmkc8dhp")))

