(define-module (crates-io un c- unc-gas) #:use-module (crates-io))

(define-public crate-unc-gas-0.1.0 (c (n "unc-gas") (v "0.1.0") (d (list (d (n "borsh") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "interactive-clap") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1rn0js9bydcizrjlb2p64baqmx8qvwvdpprx6n2r4fwxhhqvl6g6") (f (quote (("abi" "borsh/unstable__schema" "schemars")))) (r "1.68.0")))

(define-public crate-unc-gas-0.10.0 (c (n "unc-gas") (v "0.10.0") (d (list (d (n "borsh") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "interactive-clap") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1hrhyb36drssz8vjfv6va63jq1v0i7r7w79x9h9mvdkknid7ry7v") (f (quote (("abi" "borsh/unstable__schema" "schemars")))) (r "1.68.0")))

