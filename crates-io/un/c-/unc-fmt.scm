(define-module (crates-io un c- unc-fmt) #:use-module (crates-io))

(define-public crate-unc-fmt-0.1.0 (c (n "unc-fmt") (v "0.1.0") (d (list (d (n "unc-primitives-core") (r "^0.1.0") (d #t) (k 0)))) (h "063chj1cx6hvf3wyh3ax76b5lc83bh3dgq5i8v1ba92mb9mijr2n") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.2.0 (c (n "unc-fmt") (v "0.2.0") (d (list (d (n "unc-primitives-core") (r "^0.2.0") (d #t) (k 0)))) (h "0rhvlc96g8z07b2q5zz01xcmhx2fz5i054x95jvi9nv3bmmhi789") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.5.1 (c (n "unc-fmt") (v "0.5.1") (d (list (d (n "unc-primitives-core") (r "^0.5.1") (d #t) (k 0)))) (h "1wiybdgv2hbvwsmwpkd8kadymlrlfliqknk39s7qj5axi9pkx1va") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.5.2 (c (n "unc-fmt") (v "0.5.2") (d (list (d (n "unc-primitives-core") (r "^0.5.1") (d #t) (k 0)))) (h "0wj67488rgdg2flq1k6rz8asva9zwq925qpws9q50qd2jbycn7mr") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.6.0 (c (n "unc-fmt") (v "0.6.0") (d (list (d (n "unc-primitives-core") (r "^0.6.0") (d #t) (k 0)))) (h "067hiph0w97n5cf32indxc6azxb5yq4bws6w4i9hsbr3q1nzs7v6") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.6.1 (c (n "unc-fmt") (v "0.6.1") (d (list (d (n "unc-primitives-core") (r "^0.6.1") (d #t) (k 0)))) (h "01y7d19szjyqkdydwblsxanzhmqxmrf2jpck1685nq5c0j4q2m5j") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.6.2 (c (n "unc-fmt") (v "0.6.2") (d (list (d (n "unc-primitives-core") (r "^0.6.2") (d #t) (k 0)))) (h "1yzd13prcrhf9fpm63z56wdhl9kxvjhw85sji6qw8ivhr18ak6ps") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.7.0 (c (n "unc-fmt") (v "0.7.0") (d (list (d (n "unc-primitives-core") (r "^0.7.0") (d #t) (k 0)))) (h "0wrfjdkc55fp2xrfzdj1i32ms0d8ixxgnmagj2jaaribaqvfi1n3") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.7.1 (c (n "unc-fmt") (v "0.7.1") (d (list (d (n "unc-primitives-core") (r "^0.7.1") (d #t) (k 0)))) (h "10cfqqzcrha0w770hsnz0kcm4hi9jcjl20ka3v2hds72a5x8dx20") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.7.2 (c (n "unc-fmt") (v "0.7.2") (d (list (d (n "unc-primitives-core") (r "^0.7.2") (d #t) (k 0)))) (h "0jw6y4blyywr13qbirgn0b11rc57rjcnd6rxfv3jygywknasnx22") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.7.3 (c (n "unc-fmt") (v "0.7.3") (d (list (d (n "unc-primitives-core") (r "^0.7.3") (d #t) (k 0)))) (h "03j2j23vjx5ck216yvi0kw6ca4jqqnm0gndyimilp1838snadzz8") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.10.0 (c (n "unc-fmt") (v "0.10.0") (d (list (d (n "unc-primitives-core") (r "^0.10.0") (d #t) (k 0)))) (h "1q1ww2n5wz6dn6wbng5ii0qwnqp19dsfxkxxrhrk31pn28jdfd1p") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

(define-public crate-unc-fmt-0.10.2 (c (n "unc-fmt") (v "0.10.2") (d (list (d (n "unc-primitives-core") (r "^0.10.2") (d #t) (k 0)))) (h "1b872fhbfklmswiisk1dlncmlq52jszayjsh17cbsxv2lnphqscm") (f (quote (("nightly_protocol" "unc-primitives-core/nightly_protocol") ("nightly" "nightly_protocol" "unc-primitives-core/nightly"))))))

