(define-module (crates-io un c- unc-abi-client-macros) #:use-module (crates-io))

(define-public crate-unc-abi-client-macros-0.1.0 (c (n "unc-abi-client-macros") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unc-abi-client-impl") (r "^0.1.0") (d #t) (k 0)))) (h "02cax7mlh22j9hbi7g7jy7z99wc9mkyvcwwmn7cf1a1170fywppd")))

(define-public crate-unc-abi-client-macros-0.10.0 (c (n "unc-abi-client-macros") (v "0.10.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unc-abi-client-impl") (r "^0.10.0") (d #t) (k 0)))) (h "03r2flvdn89a0qnml5z09hjn5ij67k04wjn9hqkvzjs944r2mvs9")))

(define-public crate-unc-abi-client-macros-0.10.1 (c (n "unc-abi-client-macros") (v "0.10.1") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unc-abi-client-impl") (r "^0.10.1") (d #t) (k 0)))) (h "1i8zm649450wxl0ikrq3gbnnvc2d9as1d1z33b1g2cfd03sv67w2")))

