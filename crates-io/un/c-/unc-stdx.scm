(define-module (crates-io un c- unc-stdx) #:use-module (crates-io))

(define-public crate-unc-stdx-0.1.0 (c (n "unc-stdx") (v "0.1.0") (h "07g66lcwcf2dml0cycq55parskmzs1rhqhgdyb5sm19pl038zmhh")))

(define-public crate-unc-stdx-0.2.0 (c (n "unc-stdx") (v "0.2.0") (h "065y885wapmr05rpqwnnbc7f35sc4hs748p5i2ik1dzlx3mxb44p")))

(define-public crate-unc-stdx-0.5.1 (c (n "unc-stdx") (v "0.5.1") (h "1sfnf574r8r12n5z3az39bs26dvy3cr8d4f8v6kwnvrlz6w69yd8")))

(define-public crate-unc-stdx-0.5.2 (c (n "unc-stdx") (v "0.5.2") (h "0y2jnhfk9hr620n92ns2nh3bix7yyvyh1v7jqafbxl3z1m1ra7da")))

(define-public crate-unc-stdx-0.6.0 (c (n "unc-stdx") (v "0.6.0") (h "0q0ry6rvs5qq7mjra566ag7k24h1i6kwlw38r1n1ijn3gvymx6kz")))

(define-public crate-unc-stdx-0.6.1 (c (n "unc-stdx") (v "0.6.1") (h "1kqh20vdbdlx1v08dcsy6rxfk1kgz377mam0vzlckvw80v6vvacw")))

(define-public crate-unc-stdx-0.6.2 (c (n "unc-stdx") (v "0.6.2") (h "1xi07kfb6dy1i61rmawdzibhf3b1rg9984dkmdj4f8dh08rq6qxa")))

(define-public crate-unc-stdx-0.7.0 (c (n "unc-stdx") (v "0.7.0") (h "1dskw037lp14v17pg2qfh3c914ywfcpknlp14ml8ivpvqjnvak57")))

(define-public crate-unc-stdx-0.7.1 (c (n "unc-stdx") (v "0.7.1") (h "03gs0f0qs35a913hlafb9lfawyikg5vrqwhl08bkny5qlm7sfrnf")))

(define-public crate-unc-stdx-0.7.2 (c (n "unc-stdx") (v "0.7.2") (h "1fq9jdb02k45ahid75ysa02fvv39gjcbq2v827fn1qi9isl86pwv")))

(define-public crate-unc-stdx-0.7.3 (c (n "unc-stdx") (v "0.7.3") (h "13krzrz0m1icd75x6r4bw3cczj3p79z0yybd85h6xd6cdr1r47as")))

(define-public crate-unc-stdx-0.10.0 (c (n "unc-stdx") (v "0.10.0") (h "0gaq40nff8mcvbxiycn5sjnl5fipkjni99nlcbc5r5dqpwixh97b")))

(define-public crate-unc-stdx-0.10.2 (c (n "unc-stdx") (v "0.10.2") (h "194jh691hafm33r5js9f9m10s42pn41a7s1wmrb2cl3dw2cfg40c")))

