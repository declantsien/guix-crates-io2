(define-module (crates-io un c- unc-cache) #:use-module (crates-io))

(define-public crate-unc-cache-0.1.0 (c (n "unc-cache") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "140clghcm1ncihc8bsxlzk8ln036hkqjzx4213rpahnhlsdljm0c")))

(define-public crate-unc-cache-0.2.0 (c (n "unc-cache") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0c42pgan6wvpx5ynw8lm01pkrmbdgsfc0iirfdqkmfgh84b97p5g")))

(define-public crate-unc-cache-0.5.1 (c (n "unc-cache") (v "0.5.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "16ajb8jpv8r804mvaxh1y69vrhnrsxby6ss0lzrz3cwz7ynzrjbw")))

(define-public crate-unc-cache-0.5.2 (c (n "unc-cache") (v "0.5.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04v9sn9mxy5csbfdqskfwkxxv6af2sfmv6c0pnadqchvss94635h")))

(define-public crate-unc-cache-0.6.0 (c (n "unc-cache") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1d3nw9d6j4v6074a634bbl7h67xh1mn94dzvjd02dk1n1yqmqyd8")))

(define-public crate-unc-cache-0.6.1 (c (n "unc-cache") (v "0.6.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12f40mkfpyk4x718sk0dgq8i84i1yxcvwx1ccz5rqv7famx2mpcq")))

(define-public crate-unc-cache-0.6.2 (c (n "unc-cache") (v "0.6.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0j1w50783ki8jr1z3wvwnvbk228sprlcjvd675wvrjfy530fyqlb")))

(define-public crate-unc-cache-0.7.0 (c (n "unc-cache") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1w7cilj1a49dgy3scf4jh4rv00wmkyd8jsl08lf8wslmp4hllcnb")))

(define-public crate-unc-cache-0.7.1 (c (n "unc-cache") (v "0.7.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1c6m96c17532zhpz89wkq4xa08baar3m7bn6q97m7fr27ppq9s70")))

(define-public crate-unc-cache-0.7.2 (c (n "unc-cache") (v "0.7.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0x20p8iyzz1ffj49h8cvayayn4agj6n0v14vggagyyqybascbf77")))

(define-public crate-unc-cache-0.7.3 (c (n "unc-cache") (v "0.7.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "103s1h6f94lvfb9caifpx6slcj8laabb104jw71shrmkfcr17d9a")))

(define-public crate-unc-cache-0.10.0 (c (n "unc-cache") (v "0.10.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ihydn67x109w9qsv52ivkbw9gj71q1ll990nlgb9r98daz0y5f4")))

(define-public crate-unc-cache-0.10.2 (c (n "unc-cache") (v "0.10.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lru") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jiwgxq0m0g1ikgrdqlrscjxny9hbzh3c3yxirwdhpz05x3634kq")))

