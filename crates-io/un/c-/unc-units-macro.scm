(define-module (crates-io un c- unc-units-macro) #:use-module (crates-io))

(define-public crate-unc-units-macro-0.1.0 (c (n "unc-units-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "unc-units-core") (r "^0.1.0") (d #t) (k 0)))) (h "08d9vkam69q07kqfmwp8g1497jwr8s2j9adii9kxil27qs15q4hv")))

