(define-module (crates-io un c- unc-contract-standards) #:use-module (crates-io))

(define-public crate-unc-contract-standards-0.1.0 (c (n "unc-contract-standards") (v "0.1.0") (d (list (d (n "unc-sdk") (r "~0.1.0") (f (quote ("legacy"))) (k 0)))) (h "0gi6562x2fj1y9vnw4qjafcpyx2mvbzgqad3vdrk2fvcdgrwwbm5") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-0.7.0 (c (n "unc-contract-standards") (v "0.7.0") (d (list (d (n "unc-sdk") (r "^0.7.0") (f (quote ("legacy"))) (k 0)))) (h "0ci76prx5zzmajdj0jznfhzzzwlc11h5dlff2lp64qgwyxvwxk9j") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-0.7.1 (c (n "unc-contract-standards") (v "0.7.1") (d (list (d (n "unc-sdk") (r "^0.7.0") (f (quote ("legacy"))) (k 0)))) (h "1n3h9zg8vphvcfnbv689rd29vyg59sw8ybqb95yvypz87bwia27j") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-0.7.2 (c (n "unc-contract-standards") (v "0.7.2") (d (list (d (n "unc-sdk") (r "^0.7.2") (f (quote ("legacy"))) (k 0)))) (h "0s6wk186dl30dhmjc1ln7g8693rygscb0hv85wp1zqdxnk2kj0kg") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-0.7.3 (c (n "unc-contract-standards") (v "0.7.3") (d (list (d (n "unc-sdk") (r "^0.7.3") (f (quote ("legacy"))) (k 0)))) (h "1kwjsr5awdw9v70i50ynzyff7n3x75ih43lir9i5wsx4yfsz0pxm") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-0.7.4 (c (n "unc-contract-standards") (v "0.7.4") (d (list (d (n "unc-sdk") (r "^0.7.4") (f (quote ("legacy"))) (k 0)))) (h "1bzxg1c1i70gf7907bsdjwh6w5zvncgn5hbrp1fndhrsk7d1rsc2") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-0.7.5 (c (n "unc-contract-standards") (v "0.7.5") (d (list (d (n "unc-sdk") (r "^0.7.5") (f (quote ("legacy"))) (k 0)))) (h "1by83fla07dlxqym6sr1vgkdqqxkpzphrxhgl0vhpizdpcsrnsvm") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-0.9.0 (c (n "unc-contract-standards") (v "0.9.0") (d (list (d (n "unc-sdk") (r "^0.9.0") (f (quote ("legacy"))) (k 0)))) (h "08sgq7w1mjffy020r40m1860vzbp3w6hqx8lii4l0aa8x52l7wnx") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-2.0.0 (c (n "unc-contract-standards") (v "2.0.0") (d (list (d (n "unc-sdk") (r "^2.0.0") (f (quote ("legacy"))) (k 0)))) (h "085n3ajb1xvzwz7mlignicksddkckcaj0yykm2i737vg6lw98mlb") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-2.0.1 (c (n "unc-contract-standards") (v "2.0.1") (d (list (d (n "unc-sdk") (r "^2.0.0") (f (quote ("legacy"))) (k 0)))) (h "1p2gfapg9wd232fci0q9gqsr8hkrp71gikbrbd8x1lvi0xpq0r3z") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-2.0.2 (c (n "unc-contract-standards") (v "2.0.2") (d (list (d (n "unc-sdk") (r "^2.0.0") (f (quote ("legacy"))) (k 0)))) (h "0n6ajbj1sasaxpl2zqsx7z0qc4ka3yjjwdcxbwzr9qw70rjmz831") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

(define-public crate-unc-contract-standards-2.0.3 (c (n "unc-contract-standards") (v "2.0.3") (d (list (d (n "unc-sdk") (r "^2.0.0") (f (quote ("legacy"))) (k 0)))) (h "0awg3p8qjcchjzm3cxdm0rk13sbqwvjy2g7zbs11hfwljxli0prk") (f (quote (("default") ("abi" "unc-sdk/abi"))))))

