(define-module (crates-io un c- unc-units-core) #:use-module (crates-io))

(define-public crate-unc-units-core-0.1.0 (c (n "unc-units-core") (v "0.1.0") (d (list (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1p4wzmirvsa7y2x1q3q3kj726q31l4125w7g9vd1vyvwra03frn1")))

