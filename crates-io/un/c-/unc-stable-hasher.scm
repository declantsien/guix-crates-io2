(define-module (crates-io un c- unc-stable-hasher) #:use-module (crates-io))

(define-public crate-unc-stable-hasher-0.1.0 (c (n "unc-stable-hasher") (v "0.1.0") (h "0afmznz789f68sw5yrv5jhq7x7w88vh5xyscv32g718ivc0qq5cl")))

(define-public crate-unc-stable-hasher-0.5.1 (c (n "unc-stable-hasher") (v "0.5.1") (h "18bpsqfgnvzq9412xdf838kvvlgi665jn35f37z1pcvgxxy9fczg")))

(define-public crate-unc-stable-hasher-0.5.2 (c (n "unc-stable-hasher") (v "0.5.2") (h "1hm74c1p2zw04qg464i1714zlg7ndwpy2nvayjpsypsybwbr4qfi")))

(define-public crate-unc-stable-hasher-0.6.0 (c (n "unc-stable-hasher") (v "0.6.0") (h "04nplifbg2ckk7ikmw1z4lmrnyq2wlhn812xzb7nwzgp24vk2w1q")))

(define-public crate-unc-stable-hasher-0.6.1 (c (n "unc-stable-hasher") (v "0.6.1") (h "11mdb6wzxqk1sikxx1vjxsws94fncx4yh3vcg3ks4w204nn3nb5h")))

(define-public crate-unc-stable-hasher-0.7.1 (c (n "unc-stable-hasher") (v "0.7.1") (h "1qqj5a7s8i8s4wp20q7vmakw58p30j4hxihhj9wscsicr9lj0vip")))

(define-public crate-unc-stable-hasher-0.7.2 (c (n "unc-stable-hasher") (v "0.7.2") (h "0w5lr3i14jk6gr0d9yy4vzkfibfcvr53ch9y9vvd95zyqzvl2a25")))

(define-public crate-unc-stable-hasher-0.7.3 (c (n "unc-stable-hasher") (v "0.7.3") (h "168hi0f7crnjhk9958q3x4b38rb877v01pff124zi7vfvdlsh39g")))

(define-public crate-unc-stable-hasher-0.10.0 (c (n "unc-stable-hasher") (v "0.10.0") (h "12f73gyj0fdxankjzwsp4ql58as92xgnxspr8n7rs1hbqix3shch")))

(define-public crate-unc-stable-hasher-0.10.2 (c (n "unc-stable-hasher") (v "0.10.2") (h "1hjq7say3bf86xgfvwl3kp7kdgcm9z2kl7aaps0vzdqma261wrw8")))

