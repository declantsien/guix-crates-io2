(define-module (crates-io un c- unc-config-utils) #:use-module (crates-io))

(define-public crate-unc-config-utils-0.1.0 (c (n "unc-config-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "1wxmfijxx479ayp613pd83v6v03dfvhf52arcd290jp9p4rbx9m9")))

(define-public crate-unc-config-utils-0.2.0 (c (n "unc-config-utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "109nglk0zhhzyx0rar984b28h0psyipbw1rzbkfhh7ym12gh1b88")))

(define-public crate-unc-config-utils-0.5.1 (c (n "unc-config-utils") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "1alzyqfc4mlf4np8j97s0b5hj9v99z9av6461hhwacjddgiqqay4")))

(define-public crate-unc-config-utils-0.5.2 (c (n "unc-config-utils") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0x47c30g61s36nkv7pyvy5n3abdbrycl75gxfrc56w587i3gkmag")))

(define-public crate-unc-config-utils-0.6.0 (c (n "unc-config-utils") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "1sdv5pwhw9qf23j99xfdlh3m370s0pj8hqs6x9hffv3zs0xzsqik")))

(define-public crate-unc-config-utils-0.6.1 (c (n "unc-config-utils") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "17d6s7pba525k306g4jdvia2f3qff2imqp0y0g64pgy0nvcvxifa")))

(define-public crate-unc-config-utils-0.6.2 (c (n "unc-config-utils") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0cs9mb219w6276585k6l0ax0pjgpld6yp9jrhp453fx7slcbbv7d")))

(define-public crate-unc-config-utils-0.7.0 (c (n "unc-config-utils") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "1k86vzl3wws1sxkann55mi2alygfrk341mwch85jpyydw0n37gqi")))

(define-public crate-unc-config-utils-0.7.1 (c (n "unc-config-utils") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "1ps03chz8sbqbg133zfbbd5csdlihpzmxi757gb2x4gylhpdym1s")))

(define-public crate-unc-config-utils-0.7.2 (c (n "unc-config-utils") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0h301pg16xrk78pwfs9ay01lzhwsa6mv91km8k0rncx6flxbc5if")))

(define-public crate-unc-config-utils-0.7.3 (c (n "unc-config-utils") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "0xhblgn4xjixjris7divvx176irmnafg0vq81ksdqccgasln7wxs")))

(define-public crate-unc-config-utils-0.10.0 (c (n "unc-config-utils") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "11j925fcjmza6djw7yfwzqdjmj23qik6pbhhy1kzlaqbyfvkmpn5")))

(define-public crate-unc-config-utils-0.10.2 (c (n "unc-config-utils") (v "0.10.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (f (quote ("std"))) (d #t) (k 0)))) (h "19ih8mzqm26gb0x7yf715lxibk795j5bkhrcxnfsxz2dd7lhq5rx")))

