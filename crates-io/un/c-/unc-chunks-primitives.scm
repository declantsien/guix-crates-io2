(define-module (crates-io un c- unc-chunks-primitives) #:use-module (crates-io))

(define-public crate-unc-chunks-primitives-0.1.0 (c (n "unc-chunks-primitives") (v "0.1.0") (d (list (d (n "unc-chain-primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.1.0") (d #t) (k 0)))) (h "1kwc51qqw5j5k2cmjbgk7713hgnva5vwk6ri4w6svs0d8lx89pdb")))

(define-public crate-unc-chunks-primitives-0.5.1 (c (n "unc-chunks-primitives") (v "0.5.1") (d (list (d (n "unc-chain-primitives") (r "^0.5.1") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.5.1") (d #t) (k 0)))) (h "1kfg3hk2fji9vp0fmg395zv9kfzrfhbwkrr13yw2rybkysiy2awp")))

(define-public crate-unc-chunks-primitives-0.5.2 (c (n "unc-chunks-primitives") (v "0.5.2") (d (list (d (n "unc-chain-primitives") (r "^0.5.1") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.5.1") (d #t) (k 0)))) (h "0pjxi0dd6b5vrabcd154l6pabw0viafagl80mq0szmdsgcffijxk")))

(define-public crate-unc-chunks-primitives-0.6.0 (c (n "unc-chunks-primitives") (v "0.6.0") (d (list (d (n "unc-chain-primitives") (r "^0.6.0") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.6.0") (d #t) (k 0)))) (h "1k69glsskrcnxm6qcmk3piw0f7gf5hqr87a7xw2p47xj4nv618ag")))

(define-public crate-unc-chunks-primitives-0.6.1 (c (n "unc-chunks-primitives") (v "0.6.1") (d (list (d (n "unc-chain-primitives") (r "^0.6.1") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.6.1") (d #t) (k 0)))) (h "1y1yjqnprg0dnzwrrxm3dsnwrdy06b44rc7qy9nczsvr3iisp1nr")))

(define-public crate-unc-chunks-primitives-0.7.0 (c (n "unc-chunks-primitives") (v "0.7.0") (d (list (d (n "unc-chain-primitives") (r "^0.7.0") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.7.0") (d #t) (k 0)))) (h "0xmzvfimlf2y06yxl2k5cn3i1naxgw86dpz1zc1byn2l20di2l14")))

(define-public crate-unc-chunks-primitives-0.7.1 (c (n "unc-chunks-primitives") (v "0.7.1") (d (list (d (n "unc-chain-primitives") (r "^0.7.1") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.7.1") (d #t) (k 0)))) (h "1ckpmqnqdd904zbsgwxnfqkfprkmcx9iaa3mgzr051ql74y8lb96")))

(define-public crate-unc-chunks-primitives-0.7.2 (c (n "unc-chunks-primitives") (v "0.7.2") (d (list (d (n "unc-chain-primitives") (r "^0.7.2") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.7.2") (d #t) (k 0)))) (h "0gy15rhc25x2s09if8bn0mijkrfd41m3rgkg3n6la0sqws81kf3p")))

(define-public crate-unc-chunks-primitives-0.7.3 (c (n "unc-chunks-primitives") (v "0.7.3") (d (list (d (n "unc-chain-primitives") (r "^0.7.3") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.7.3") (d #t) (k 0)))) (h "04qlqnyik6x0dqnnms5vpb8hpbdd9c4bxzq3x3r4zxww4qms3fcq")))

(define-public crate-unc-chunks-primitives-0.10.0 (c (n "unc-chunks-primitives") (v "0.10.0") (d (list (d (n "unc-chain-primitives") (r "^0.10.0") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.10.0") (d #t) (k 0)))) (h "09cgk59cj46irh8k0lqa4c24614mcy7bnskxaxpf7yc7p2lksfw5")))

(define-public crate-unc-chunks-primitives-0.10.2 (c (n "unc-chunks-primitives") (v "0.10.2") (d (list (d (n "unc-chain-primitives") (r "^0.10.2") (d #t) (k 0)) (d (n "unc-primitives") (r "^0.10.2") (d #t) (k 0)))) (h "0badr31d58zk0kykhhlr7s0sb9mmc79yh2qs1aw7ckn24m3vqyxr")))

