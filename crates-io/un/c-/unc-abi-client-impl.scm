(define-module (crates-io un c- unc-abi-client-impl) #:use-module (crates-io))

(define-public crate-unc-abi-client-impl-0.1.0 (c (n "unc-abi-client-impl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "near_schemafy_lib") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unc-abi") (r "^0.1.0") (d #t) (k 0)))) (h "1l0dg678b562h950szf2423j39viplfk64zc3072ghs98rgg11xz")))

(define-public crate-unc-abi-client-impl-0.10.0 (c (n "unc-abi-client-impl") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "near_schemafy_lib") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unc-abi") (r "^0.10.2") (d #t) (k 0)))) (h "1fmg53byplgq44zzlxiwmc6sxa4rwbxblsz1isgznk01rdfcj9qp")))

(define-public crate-unc-abi-client-impl-0.10.1 (c (n "unc-abi-client-impl") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "near_schemafy_lib") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unc-abi") (r "^0.10.2") (d #t) (k 0)))) (h "16hx4yx94p4mbijrphvr1mn6v5pmrn9759zhxivmv678im9piz94")))

