(define-module (crates-io un sp unsplash-api) #:use-module (crates-io))

(define-public crate-unsplash-api-0.1.0 (c (n "unsplash-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-aux") (r "^2.2") (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (k 0)))) (h "0mddccsvi7rybpc8py820nvf285c6pzsmjn8d0z5q7v3lnf6236x")))

