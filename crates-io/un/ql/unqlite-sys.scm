(define-module (crates-io un ql unqlite-sys) #:use-module (crates-io))

(define-public crate-unqlite-sys-0.1.0 (c (n "unqlite-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "11fvmrji9zjsbi7bzgz6sxj5lbhy78q212j9w8217d1l7hvv5h40")))

(define-public crate-unqlite-sys-0.2.0 (c (n "unqlite-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "117ndd8malipq0yl7zdvwhrvr68lviz8g8i7mw8fnxiwnxdigb74")))

(define-public crate-unqlite-sys-0.3.0 (c (n "unqlite-sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0hgym57h8x8a11vszxk8f4v8p46x782qrcvjz45b6pk668sck08f") (f (quote (("jx9-enable-math-func") ("jx9-disable-disk-io") ("jx9-disable-builtin-func") ("enable-threads") ("enable-jx9-hash-io") ("default"))))))

(define-public crate-unqlite-sys-0.3.1 (c (n "unqlite-sys") (v "0.3.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bd88ddkhqxzdgpi3987lmq5lma60fwnibpbmsa9w38zphl48cq8") (f (quote (("jx9-enable-math-func") ("jx9-disable-disk-io") ("jx9-disable-builtin-func") ("enable-threads") ("enable-jx9-hash-io") ("default"))))))

(define-public crate-unqlite-sys-0.3.2 (c (n "unqlite-sys") (v "0.3.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nqfblaqva3myy8kvvwxh9njw3miqdqqschwn1lvphj2m8ixq9i3") (f (quote (("jx9-enable-math-func") ("jx9-disable-disk-io") ("jx9-disable-builtin-func") ("enable-threads") ("enable-jx9-hash-io") ("default"))))))

(define-public crate-unqlite-sys-1.0.0 (c (n "unqlite-sys") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1glpr3gc8q2vrvz0r1k7xwjbm087v51d6x7f1hf354nincp2hizq") (f (quote (("jx9-enable-math-func") ("jx9-disable-disk-io") ("jx9-disable-builtin-func") ("enable-threads") ("enable-jx9-hash-io") ("default"))))))

(define-public crate-unqlite-sys-1.1.0 (c (n "unqlite-sys") (v "1.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "01vv5sgbmv6zl5sb9kavaa9vf4pmljav6ai7ynaiz5pvzfws8d2m") (f (quote (("jx9-enable-math-func") ("jx9-disable-disk-io") ("jx9-disable-builtin-func") ("enable-threads") ("enable-jx9-hash-io") ("default"))))))

