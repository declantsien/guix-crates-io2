(define-module (crates-io un ut unutils) #:use-module (crates-io))

(define-public crate-unutils-0.1.0 (c (n "unutils") (v "0.1.0") (h "196xpxbal9rcxp7g93ns0yid9s080fvc90kqjplbf6rn2lnnqqjq")))

(define-public crate-unutils-0.1.1 (c (n "unutils") (v "0.1.1") (h "14dvc8c358rjxpr7bqmd80j4ki160fqq6d2aih144hn1mzpga9jd")))

