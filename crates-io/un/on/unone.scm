(define-module (crates-io un on unone) #:use-module (crates-io))

(define-public crate-unone-0.1.0 (c (n "unone") (v "0.1.0") (h "1mg56gv8734r06h2z393625ib6agrg47cis670vjaycia7xiqkbx") (y #t) (r "1.56")))

(define-public crate-unone-0.0.0 (c (n "unone") (v "0.0.0") (h "1d0jlidrni31w92jc5sip98r2ss0p2xx5s1hn2kmhf3gir5fm73f") (y #t) (r "1.56")))

