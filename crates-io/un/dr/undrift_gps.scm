(define-module (crates-io un dr undrift_gps) #:use-module (crates-io))

(define-public crate-undrift_gps-0.1.0 (c (n "undrift_gps") (v "0.1.0") (h "1ry7yc0wibpbw0177jipyncjcgmdivjvw6knrvzwi7cx1z6l5ypw")))

(define-public crate-undrift_gps-0.1.1 (c (n "undrift_gps") (v "0.1.1") (h "0pvj3sbak8zs1d92hq2358mjybmc3aw9xa7l3xqgffy4vlajdgi8")))

(define-public crate-undrift_gps-0.2.1 (c (n "undrift_gps") (v "0.2.1") (h "0hh1y5fdcn2kddxnfvl0znxkqisbvhii8rj2j9ldl0n2b5b61wwi")))

(define-public crate-undrift_gps-0.3.1 (c (n "undrift_gps") (v "0.3.1") (h "1a2ql49f2zh003gl2ry2rkspraik1b6prnlqqjwnyipqmq1wfal0")))

