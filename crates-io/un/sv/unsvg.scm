(define-module (crates-io un sv unsvg) #:use-module (crates-io))

(define-public crate-unsvg-1.0.0 (c (n "unsvg") (v "1.0.0") (d (list (d (n "resvg") (r "^0.35.0") (d #t) (k 0)))) (h "0wpdgpgc4blnqv2lx57gsfdf8wzdh5il65bw0i7l3i4l7b91g65n")))

(define-public crate-unsvg-1.1.0 (c (n "unsvg") (v "1.1.0") (d (list (d (n "resvg") (r "^0.35.0") (d #t) (k 0)))) (h "026nfwvc2gs9ijgf44rj33q50kn2f1rxhy90jh765xa5xrnmr49z")))

(define-public crate-unsvg-1.1.1 (c (n "unsvg") (v "1.1.1") (d (list (d (n "resvg") (r "^0.35.0") (d #t) (k 0)))) (h "0ngb3p2bl3n0k32snhrga2d7vswzk1i3ppsfhc1ghwhm1p75iw9w")))

(define-public crate-unsvg-1.1.2 (c (n "unsvg") (v "1.1.2") (d (list (d (n "resvg") (r "^0.35.0") (d #t) (k 0)))) (h "0id3jchr8mmkqmk75ca0vjksgy57kk6zfrs0l60fs7lnl0x7296v")))

