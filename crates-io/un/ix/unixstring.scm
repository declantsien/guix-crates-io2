(define-module (crates-io un ix unixstring) #:use-module (crates-io))

(define-public crate-unixstring-0.1.0 (c (n "unixstring") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1hb3flv3387rvr96pi8njhqnzdc6zg35k8y80n6xy1jjnnmgqbgr")))

(define-public crate-unixstring-0.2.0 (c (n "unixstring") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1d8k3v43xdjki687xqipj88dgd5jzqnaijxhalcnc8x0kip3imwg")))

(define-public crate-unixstring-0.2.1 (c (n "unixstring") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1zq1ia00zzv4xfh3xgkjhw8i787j6j7jpqxzy5rix22f4153advm")))

(define-public crate-unixstring-0.2.2 (c (n "unixstring") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "05s185vmh4nixnrwk2llh3b3adhgc85d3s0153k0w3s8g37q77zv")))

(define-public crate-unixstring-0.2.3 (c (n "unixstring") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1y5pk7zpsj07brkz30zbz3zcx4v07k1zxmnflqhk079cdrrilcbg")))

(define-public crate-unixstring-0.2.4 (c (n "unixstring") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0fbxyald6m5b9pcjw8wz5803xlgi5p0j2lc2xsh3mf1kmsixbbbk")))

(define-public crate-unixstring-0.2.5 (c (n "unixstring") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1dpqm4dxkzcnni542ak22zlc0s0sx46fcdz2zqdl68q1pkviwkq9")))

(define-public crate-unixstring-0.2.6 (c (n "unixstring") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)))) (h "090k0hdr2a031yrn393s6jqm519p64vvc4mx1sri0wis7pjvv8fk")))

(define-public crate-unixstring-0.2.7 (c (n "unixstring") (v "0.2.7") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)))) (h "1wxj6bdgxgyznkmb2l7p7fapw26ljrxwqsj7nf2adrybaxb5qv1n")))

