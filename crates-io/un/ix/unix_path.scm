(define-module (crates-io un ix unix_path) #:use-module (crates-io))

(define-public crate-unix_path-1.0.0 (c (n "unix_path") (v "1.0.0") (d (list (d (n "unix_str") (r "^1") (k 0)))) (h "087kr7cd2m199gx23wqgdl6bqk971p6mkm3j03blc7yx0ry0yyg9") (f (quote (("std" "alloc" "unix_str/std") ("shrink_to" "unix_str/shrink_to") ("default" "std") ("alloc" "unix_str/alloc"))))))

(define-public crate-unix_path-1.0.1 (c (n "unix_path") (v "1.0.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "unix_str") (r "^1") (k 0)))) (h "1bryg19y7q2ma4x2d75kiw25p8v8xq5lvcy9v74c8xxffcc2k3mg") (f (quote (("std" "alloc" "unix_str/std") ("shrink_to" "unix_str/shrink_to") ("default" "std") ("alloc" "unix_str/alloc"))))))

