(define-module (crates-io un ix unix_socket) #:use-module (crates-io))

(define-public crate-unix_socket-0.1.0 (c (n "unix_socket") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1gw45r52fv7pxw4khbmv26x7fmf4l69nrhqpkgcmqpnq1nnbs0vl")))

(define-public crate-unix_socket-0.1.1 (c (n "unix_socket") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "^0.0.3") (d #t) (k 2)))) (h "0fkpwrfiglndr4rm1y1q1cw0c9fs6k3p4m0d4sfxk03nbl1lnw82")))

(define-public crate-unix_socket-0.1.2 (c (n "unix_socket") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "^0.0.3") (d #t) (k 2)))) (h "1faw63sy3wfjiyldjvcgr2gmc2zfm596skq7l028awzp6blkp9lz")))

(define-public crate-unix_socket-0.2.0 (c (n "unix_socket") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "^0.0.3") (d #t) (k 2)))) (h "1l3ragbawr3wpiaf929fyfa4g2n23mcmdbdjj5hxx7k05pqhqkzj")))

(define-public crate-unix_socket-0.2.1 (c (n "unix_socket") (v "0.2.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "^0.0.3") (d #t) (k 2)))) (h "0y7dpllgbnjy59w61mbr469rfk0z99ki4hbvcrcxfm7pa8b4zv36")))

(define-public crate-unix_socket-0.2.2 (c (n "unix_socket") (v "0.2.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "^0.0.4") (d #t) (k 2)))) (h "1x0jm7n1l9fm2gsgwjmlivimbzfqbvzbfv52hxglv73y52cfnmfz")))

(define-public crate-unix_socket-0.2.3 (c (n "unix_socket") (v "0.2.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "^0.0.4") (d #t) (k 2)))) (h "12sdng3i8dq1wyxhk02l5zbknjslgmhfhyiw2bka8nzgyxba4kkz")))

(define-public crate-unix_socket-0.3.0 (c (n "unix_socket") (v "0.3.0") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0867dx5gq75rlsp5c2kpf96mm56f1pncdsdd4gn4dylhxhgz40kl")))

(define-public crate-unix_socket-0.3.1 (c (n "unix_socket") (v "0.3.1") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1zvvpgizq20pbr2fn5a8b9zwq3hmp01ydzwj7ahvyzh601ynjk4v")))

(define-public crate-unix_socket-0.3.2 (c (n "unix_socket") (v "0.3.2") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0nrc1grfg57y18wkmdzfqkbgmcznlfjai8dpwr1bg27mndw2nz0b")))

(define-public crate-unix_socket-0.4.0 (c (n "unix_socket") (v "0.4.0") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0k3j08sdfwbqr426166wix1abrj43hxwhyyjd7w6ghic0qh79glq")))

(define-public crate-unix_socket-0.4.1 (c (n "unix_socket") (v "0.4.1") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1lhy7971afj0sq33i4ycb5wzz6d39qda1baipb1hhlf743x5s1dz") (f (quote (("socket_timeout") ("from_raw_fd"))))))

(define-public crate-unix_socket-0.4.2 (c (n "unix_socket") (v "0.4.2") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hl1bixxr1iadlzfrgmz99hxmypakz2nb4zyp122zdgkgjp97cnd") (f (quote (("socket_timeout") ("from_raw_fd"))))))

(define-public crate-unix_socket-0.4.3 (c (n "unix_socket") (v "0.4.3") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1nks0j4k7jqqbzg9n2dmkrknis2gvfdinz1p6l85kfld0q8wcqjh") (f (quote (("socket_timeout") ("from_raw_fd"))))))

(define-public crate-unix_socket-0.4.4 (c (n "unix_socket") (v "0.4.4") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0niaxrn73hrcpk1rci9mx5ca58l03a3m7sa68bx8absrs6km599c") (f (quote (("socket_timeout") ("from_raw_fd"))))))

(define-public crate-unix_socket-0.4.5 (c (n "unix_socket") (v "0.4.5") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1fh4yfjmg464ihqsf01gfhjcnsam0mxjxsifql25zdryqcgrnhjf") (f (quote (("socket_timeout") ("from_raw_fd") ("default" "from_raw_fd"))))))

(define-public crate-unix_socket-0.4.6 (c (n "unix_socket") (v "0.4.6") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ggijmcfsl0xqs8p9gi3qv1vkdx8divxsicnb427q6dkvskynw6m") (f (quote (("socket_timeout") ("from_raw_fd") ("default" "from_raw_fd"))))))

(define-public crate-unix_socket-0.5.0 (c (n "unix_socket") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0r0mxf3mmqvimnx4mpks1f6c4haj6jcxc0k9bs7w61f42w2718ka")))

