(define-module (crates-io un ix unix-cred) #:use-module (crates-io))

(define-public crate-unix-cred-0.1.0 (c (n "unix-cred") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s1610xdrnp5pwbijxysa9mr9jw05pmfh6dri87pmraxsigdsfln")))

(define-public crate-unix-cred-0.1.1 (c (n "unix-cred") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1xiflsb7db453vc6nv84insx22pfyqigki58j4vbaz56bnriclmg")))

