(define-module (crates-io un ix unix_mode) #:use-module (crates-io))

(define-public crate-unix_mode-0.1.0 (c (n "unix_mode") (v "0.1.0") (h "0wwxvbx4902v97zzwk8d5r5ish38c8syvmz49p86fin663yn17k0")))

(define-public crate-unix_mode-0.1.1 (c (n "unix_mode") (v "0.1.1") (h "02vqbpynq0mcil8dxdv1s5cy9mnq441fvisz8339p0xwx7dlmf4w")))

(define-public crate-unix_mode-0.1.2 (c (n "unix_mode") (v "0.1.2") (d (list (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 2)) (d (n "tempfile") (r "^3.2") (d #t) (t "cfg(unix)") (k 2)))) (h "0rrias2r86pdxad21s66biwzg6cqm42b1ix75pvx6l62icd0s34f")))

(define-public crate-unix_mode-0.1.3 (c (n "unix_mode") (v "0.1.3") (d (list (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 2)) (d (n "tempfile") (r "^3.2") (d #t) (t "cfg(unix)") (k 2)))) (h "1vc7lpkfx6r5mfzp25004phvhdqzblh2hx0s8l10z05v613fvarm")))

(define-public crate-unix_mode-0.1.4 (c (n "unix_mode") (v "0.1.4") (d (list (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (t "cfg(unix)") (k 2)))) (h "1vr4s647733h8n0gna6cbgc3qplnz8isz6x4mqr3q6pqcp1yspmm") (s 2) (e (quote (("serde" "dep:serde"))))))

