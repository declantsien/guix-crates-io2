(define-module (crates-io un ix unix-time) #:use-module (crates-io))

(define-public crate-unix-time-0.1.0 (c (n "unix-time") (v "0.1.0") (h "0v288ymvhwzi0w8m3n7dd2k6rq1v2hz1q3d82wa3g8l2vrwf2laf")))

(define-public crate-unix-time-0.1.1 (c (n "unix-time") (v "0.1.1") (h "0a9ia7p5gb5330rkdmbjn90m1b291iz3i6jwzma6q3gf2hvadv8k")))

(define-public crate-unix-time-0.1.2 (c (n "unix-time") (v "0.1.2") (h "1drys51r8fh5mjgkw407hq9q8z8h7jj7iby1162vnx3w001hjl4r")))

(define-public crate-unix-time-0.1.3 (c (n "unix-time") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vl7izc0sqgxrfh3gkyh2iq6dvlr2mlv0zaswlqangd0xrck1i8y") (f (quote (("default" "serde"))))))

(define-public crate-unix-time-0.1.4 (c (n "unix-time") (v "0.1.4") (d (list (d (n "rkyv") (r ">=0.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0divdqw89ygl633chv6zb2k07lrjj6xxhp1204mndf636id4nsw0") (f (quote (("default" "serde" "rkyv"))))))

(define-public crate-unix-time-0.1.5 (c (n "unix-time") (v "0.1.5") (d (list (d (n "rkyv") (r ">=0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1f12dawc27xa833yajc0iin6syiy60a83bqbq07wz9jjm33ikngr") (f (quote (("default" "serde" "rkyv"))))))

