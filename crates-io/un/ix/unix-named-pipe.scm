(define-module (crates-io un ix unix-named-pipe) #:use-module (crates-io))

(define-public crate-unix-named-pipe-0.1.0 (c (n "unix-named-pipe") (v "0.1.0") (d (list (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "13m0w6cq3cfz3cb820svvnz5ak980d1gsfp0dl3f5bgdadyzfhg3")))

(define-public crate-unix-named-pipe-0.2.0 (c (n "unix-named-pipe") (v "0.2.0") (d (list (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "1qns4r23zm98i55anpva5kjs856f7id2nr06p8jmib1nizd57mka")))

