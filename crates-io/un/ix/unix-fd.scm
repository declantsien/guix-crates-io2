(define-module (crates-io un ix unix-fd) #:use-module (crates-io))

(define-public crate-unix-fd-0.0.0 (c (n "unix-fd") (v "0.0.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hhiiv2gn5yhjbx0w8rvqs4sb8vhld4lph6n59grq00zvf8vcgw6")))

(define-public crate-unix-fd-0.0.1 (c (n "unix-fd") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "13fsi7j4l3kkm17ih83s0kni2lkimlrwxjd7b6kckz2cyhy8bhxj") (f (quote (("atomic-rc"))))))

(define-public crate-unix-fd-0.0.2 (c (n "unix-fd") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "10nahjii9rabfxp7ijbkb11wm0d8jil0xcfichbdisnij62m5ay9") (f (quote (("atomic-rc"))))))

(define-public crate-unix-fd-0.1.0 (c (n "unix-fd") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0zpwawdgf365s7yn64pqiqyypj47vc9qjbw64nw7y7lzyzn58ikk") (f (quote (("atomic-rc"))))))

