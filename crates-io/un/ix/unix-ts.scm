(define-module (crates-io un ix unix-ts) #:use-module (crates-io))

(define-public crate-unix-ts-0.1.0 (c (n "unix-ts") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 2)))) (h "04cmnppcj7vsa7l3dx78j5z3iwgkv7by1mq4i5gaaaqy4sbbm2rf")))

(define-public crate-unix-ts-0.2.0 (c (n "unix-ts") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)) (d (n "unix-ts-macros") (r "^0.2") (d #t) (k 0)))) (h "0grl4lrd36v5b02d484z2n5wcggxf41mjdwi91z2dzbpp5gxqmvi")))

(define-public crate-unix-ts-0.3.0 (c (n "unix-ts") (v "0.3.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)) (d (n "unix-ts-macros") (r "^0.3") (d #t) (k 0)))) (h "1kvl58bdsg90rq6z9mpk3pnvn8bfhkcx2jz0zcwzsd37v95lvd5r")))

(define-public crate-unix-ts-0.4.0 (c (n "unix-ts") (v "0.4.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)) (d (n "unix-ts-macros") (r "^0.3") (d #t) (k 0)))) (h "196417x6adzzav62jff5fni4kyyd08cw2655qldidlfwv10x99w3")))

(define-public crate-unix-ts-0.4.1 (c (n "unix-ts") (v "0.4.1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)) (d (n "unix-ts-macros") (r "^0.4") (d #t) (k 0)))) (h "1iy2yi2dzhfy08jc8xvnkb5cbq137vy1r6kr4l7ljmg3v3864r5c")))

(define-public crate-unix-ts-0.6.0 (c (n "unix-ts") (v "0.6.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)) (d (n "unix-ts-macros") (r "^0.4") (d #t) (k 0)))) (h "0rc9wj4a8fg0cx87gwvxa8jgky0i2ckva09q7fwii7n11qm7wksa")))

(define-public crate-unix-ts-1.0.0 (c (n "unix-ts") (v "1.0.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "unix-ts-macros") (r "^1") (d #t) (k 0)))) (h "06jyf3c5ziz4vczh3q933qwbk9brj63aj70f2qy84xcmpsrbm21f")))

