(define-module (crates-io un ix unix-fifo-async) #:use-module (crates-io))

(define-public crate-unix-fifo-async-0.0.3 (c (n "unix-fifo-async") (v "0.0.3") (d (list (d (n "async-std") (r "^0.99") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)))) (h "0dnzlhra4afwf8yq7vns9fbfwfcb277wjb7a30iissbxjyyskhw7")))

