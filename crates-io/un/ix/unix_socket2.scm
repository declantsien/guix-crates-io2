(define-module (crates-io un ix unix_socket2) #:use-module (crates-io))

(define-public crate-unix_socket2-0.5.1 (c (n "unix_socket2") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0xgn0xf96v3kba368q5yjwbmgg02x9vzjbsxiakhlljyrb0ik5by")))

(define-public crate-unix_socket2-0.5.2 (c (n "unix_socket2") (v "0.5.2") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "00ilmcsnhqby5hlix6r3gs3hcmxf9amvfyqdnphcmjbll1kna5v6") (y #t)))

(define-public crate-unix_socket2-0.5.3 (c (n "unix_socket2") (v "0.5.3") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "166llg1ppfxbqg3czxk1mkq71n075rg2sdxhymnmdq9dbxa3y100") (y #t)))

(define-public crate-unix_socket2-0.5.4 (c (n "unix_socket2") (v "0.5.4") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "12hxkasbrqjr3n1i6pnxbdlb1skh6gdqb3m2k35yq03cw6n6wz5m")))

