(define-module (crates-io un ix unix-exec-output-catcher) #:use-module (crates-io))

(define-public crate-unix-exec-output-catcher-0.1.0 (c (n "unix-exec-output-catcher") (v "0.1.0") (d (list (d (n "derive_more") (r ">=0.99.11, <0.100.0") (d #t) (k 0)) (d (n "errno") (r ">=0.2.7, <0.3.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)))) (h "14zij2xq88l8y349jx50y9n9yq1545bb8nqb5kkp2hlrysqkgmcp")))

(define-public crate-unix-exec-output-catcher-0.1.1 (c (n "unix-exec-output-catcher") (v "0.1.1") (d (list (d (n "derive_more") (r ">=0.99.11, <0.100.0") (d #t) (k 0)) (d (n "errno") (r ">=0.2.7, <0.3.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)))) (h "0g9pdkpjd1zz0a1ydcmvbdv2ybn492qa57a51w5rh1rwqvkx792j")))

(define-public crate-unix-exec-output-catcher-0.1.2 (c (n "unix-exec-output-catcher") (v "0.1.2") (d (list (d (n "derive_more") (r ">=0.99.11, <0.100.0") (d #t) (k 0)) (d (n "errno") (r ">=0.2.7, <0.3.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)))) (h "1f1b4hpmc6c29rv56yh3zdbc0xpz7dkzdqypfx29svs2ywmn7y2j")))

(define-public crate-unix-exec-output-catcher-0.1.3 (c (n "unix-exec-output-catcher") (v "0.1.3") (d (list (d (n "derive_more") (r ">=0.99.11, <0.100.0") (d #t) (k 0)) (d (n "errno") (r ">=0.2.7, <0.3.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)))) (h "00s7yqfnb2wy0sdvqj73sfp6ls7m1pqsll08d8zn6bkjxm7mrrrk")))

(define-public crate-unix-exec-output-catcher-0.2.0 (c (n "unix-exec-output-catcher") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1blbbh78c38vg0wnz3bhxllmy8x01iy93qpgdprxnzzm5jxc3s2b")))

(define-public crate-unix-exec-output-catcher-0.2.1 (c (n "unix-exec-output-catcher") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1vv3mvd8cvj3givzri2ilr445yi7w462i60daw9vrjfvpc7aypcd")))

(define-public crate-unix-exec-output-catcher-0.2.2 (c (n "unix-exec-output-catcher") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0gjjxb2vrjh9zphpi69ay55nswh82xbjnwbc5a027gzxnkrqks17")))

(define-public crate-unix-exec-output-catcher-0.2.3 (c (n "unix-exec-output-catcher") (v "0.2.3") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0m80aq4vqjgimlfyjhzg8gjy428fpvi44nl2w5qmi3hjgp4qs31y")))

(define-public crate-unix-exec-output-catcher-0.2.4 (c (n "unix-exec-output-catcher") (v "0.2.4") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 2)))) (h "162i7hlnrz6lrhaxy0svqrig1c9m9b3r7i485mw8zgigq856vhmk")))

