(define-module (crates-io un ix unixcli) #:use-module (crates-io))

(define-public crate-unixcli-0.1.0 (c (n "unixcli") (v "0.1.0") (h "0zf38c2arhfppbf0kg5krl9qx14cc4mqkcnbwxc4aywiqmmfhis9")))

(define-public crate-unixcli-0.1.1 (c (n "unixcli") (v "0.1.1") (h "07ryfmdzc3zb45z39pqfv5x76k4mlfyp1qzfx6gsjfxzyfr2wsip")))

(define-public crate-unixcli-0.1.2 (c (n "unixcli") (v "0.1.2") (h "1y2vi39pi8xq17lvlrbdk7kfchkm7icglgas6ikajxxdx4hqdgxk")))

(define-public crate-unixcli-0.1.3 (c (n "unixcli") (v "0.1.3") (h "09v8p1k5c94f8y3z1f287awlpf8pm25jvm08rbp1n0a5943m224x")))

