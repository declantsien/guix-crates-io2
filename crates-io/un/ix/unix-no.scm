(define-module (crates-io un ix unix-no) #:use-module (crates-io))

(define-public crate-unix-no-0.2.0 (c (n "unix-no") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "118mi7dsdrlwihrdij83m50fa7kba80kip2f54x4yf5zrlihjgpl")))

(define-public crate-unix-no-0.2.1 (c (n "unix-no") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pqazrgjclgfp5lgd1k2hqy6jny0hi2xpq7j4pk49d382637s06i")))

(define-public crate-unix-no-0.2.2 (c (n "unix-no") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zg9lpd9clqr587f4imq7f36chirkdy8rz5a015i6imckzm6wn94")))

(define-public crate-unix-no-0.2.3 (c (n "unix-no") (v "0.2.3") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nlpcbw6k1q3qwxcmj616p4gq1w273z5srka8263f5vlzmrxd285")))

(define-public crate-unix-no-0.2.4 (c (n "unix-no") (v "0.2.4") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wnv1qal8gvfw0nbg6gax3dbl6q92szpvkfc2vixrbdz9qcw9sbd")))

(define-public crate-unix-no-0.2.5 (c (n "unix-no") (v "0.2.5") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "16y6jpg95r2r829aak02rk0ln5ffm2nbxviaysy0yq335qps4v7m")))

(define-public crate-unix-no-0.2.6 (c (n "unix-no") (v "0.2.6") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "12jzbvd4540a029b3yjy1w34izzkcznxcdpc6lvv2rkl3q3j26k8")))

