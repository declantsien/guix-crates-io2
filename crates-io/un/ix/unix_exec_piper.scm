(define-module (crates-io un ix unix_exec_piper) #:use-module (crates-io))

(define-public crate-unix_exec_piper-0.1.0 (c (n "unix_exec_piper") (v "0.1.0") (d (list (d (n "errno") (r "^0.2.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "08n3rs87cchkrkn1piqf2nfm40l3xm63a16gsngdhx7hbnldjk46")))

(define-public crate-unix_exec_piper-0.1.1 (c (n "unix_exec_piper") (v "0.1.1") (d (list (d (n "errno") (r "^0.2.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1s47ncnnbn2sq7fg032rik9cs2a4llzp3dvssh9jffvgv2jqz0ny")))

(define-public crate-unix_exec_piper-0.1.2 (c (n "unix_exec_piper") (v "0.1.2") (d (list (d (n "errno") (r "^0.2.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "0n391vj7xq4bzkhyhpl1pr68krjcax5fkbfhlzbwlz7qghp9snkl")))

(define-public crate-unix_exec_piper-0.1.3 (c (n "unix_exec_piper") (v "0.1.3") (d (list (d (n "errno") (r "^0.2.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1yfpkdg9aha6fkcdh55iqayp31ckgrga0fxlwwapng2p1vq27lyi")))

(define-public crate-unix_exec_piper-0.1.4 (c (n "unix_exec_piper") (v "0.1.4") (d (list (d (n "errno") (r "^0.2.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "13dy4134ld6x7sv56fsvlzb8mc4lm3ma4fnvq8jjb3z9f1zi1gmn")))

