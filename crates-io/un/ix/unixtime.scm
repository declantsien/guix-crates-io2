(define-module (crates-io un ix unixtime) #:use-module (crates-io))

(define-public crate-unixtime-0.1.0 (c (n "unixtime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1vw0r3bly4w4z5qlnvdz8hnzjifjrcvjnmzkmknikjclaywmcy37")))

(define-public crate-unixtime-0.1.1 (c (n "unixtime") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0zajsrjy5a4lhf1y0fb5vgdsqcwyicsm41y6jdrg0sz20s7acwpv")))

(define-public crate-unixtime-0.1.2 (c (n "unixtime") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)))) (h "11b0xp71nklijyrchjcpmvc4l25hynpn5irpmkl0j0vhrnr0c7pg")))

(define-public crate-unixtime-0.2.0 (c (n "unixtime") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "wrap_help"))) (d #t) (k 0)))) (h "1rhb3966zhr7c37hk206s8wd92528lr7bz4pidksxqb447qkv6rl")))

(define-public crate-unixtime-0.2.1 (c (n "unixtime") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "wrap_help"))) (d #t) (k 0)))) (h "0p4dm2wyf4gkpjg7lk4akfpfzmffp6bn1f7zjalk2pm59absmafh")))

