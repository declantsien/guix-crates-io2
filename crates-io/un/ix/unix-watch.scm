(define-module (crates-io un ix unix-watch) #:use-module (crates-io))

(define-public crate-unix-watch-0.1.0 (c (n "unix-watch") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gethostname") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1c7gn5drga4fcbp4a4b9zazc72n3b64allrdc48xjsx11wb6vygp")))

