(define-module (crates-io un ix unix_permissions_ext) #:use-module (crates-io))

(define-public crate-unix_permissions_ext-0.1.0 (c (n "unix_permissions_ext") (v "0.1.0") (h "1b3q6hawivpnyy42n96q37zh3y0a8a1iwrwycx4p31pib6bfafza")))

(define-public crate-unix_permissions_ext-0.1.1 (c (n "unix_permissions_ext") (v "0.1.1") (h "1akn39c26z8m8vizvqpjdcf1g8fl1x1iqfif96ryyif46bbis9m7") (f (quote (("to-string"))))))

(define-public crate-unix_permissions_ext-0.1.2 (c (n "unix_permissions_ext") (v "0.1.2") (h "1s88rl69k43616hxy2n0ws3fxkc19hg0dig92cpn2gz0hn5815vl")))

