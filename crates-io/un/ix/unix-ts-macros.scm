(define-module (crates-io un ix unix-ts-macros) #:use-module (crates-io))

(define-public crate-unix-ts-macros-0.1.0 (c (n "unix-ts-macros") (v "0.1.0") (d (list (d (n "unix-ts") (r "^0.1.0") (d #t) (k 0)))) (h "0ysr2s1mr4xdi711078zphjnp4k56bvm5ysv82lkhm5l81c1s7rs")))

(define-public crate-unix-ts-macros-0.2.0 (c (n "unix-ts-macros") (v "0.2.0") (d (list (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)) (d (n "unix-ts") (r "^0.2.0") (d #t) (k 2)))) (h "00800p3cnp993s600mi2zy26742xgg0dkvgm5wdillaqmd4mpy5b")))

(define-public crate-unix-ts-macros-0.3.0 (c (n "unix-ts-macros") (v "0.3.0") (d (list (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)) (d (n "unix-ts") (r "^0.3.0") (d #t) (k 2)))) (h "1dm2i8w111zy18vv8fz728d6q9s291qcxkqrpdamr6v3jni1grh5")))

(define-public crate-unix-ts-macros-0.4.0 (c (n "unix-ts-macros") (v "0.4.0") (d (list (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)) (d (n "unix-ts") (r "^0.4.0") (d #t) (k 2)))) (h "0j4812smhal1wy0c4p3dvcqkwwv7grxh64p16kr5svg6a2ir12kp")))

(define-public crate-unix-ts-macros-1.0.0 (c (n "unix-ts-macros") (v "1.0.0") (h "0lcrn30klj2w3v07isklfkdhy4j5m2sgm3shvg02fbv0zqrgvxac")))

