(define-module (crates-io un ix unix-tty) #:use-module (crates-io))

(define-public crate-unix-tty-0.1.0 (c (n "unix-tty") (v "0.1.0") (d (list (d (n "i-o") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "null-terminated") (r "^0.2.6") (d #t) (k 0)) (d (n "syscall") (r "^0.2") (d #t) (k 0)) (d (n "unix") (r "^0.3.1") (d #t) (k 0)))) (h "0cdrwcbv5lgh1xy6zpbwfghday327rq7awr1ippskr8l6qn9izmk")))

(define-public crate-unix-tty-0.2.0 (c (n "unix-tty") (v "0.2.0") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "null-terminated") (r "^0.2.6") (d #t) (k 0)) (d (n "syscall") (r "^0.2") (d #t) (k 0)) (d (n "unix") (r "^0.4") (d #t) (k 0)))) (h "1czq5f514yam6cwivr7i74yscdv0g8n26v6yd9d9fh0zfyv7f179")))

(define-public crate-unix-tty-0.3.0 (c (n "unix-tty") (v "0.3.0") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "null-terminated") (r "^0.3") (d #t) (k 0)) (d (n "syscall") (r "^0.2") (d #t) (k 0)) (d (n "unix") (r "^0.6") (d #t) (k 0)))) (h "1amiad80cvmjin7m3hvl4kr218afsymr74sawzaz6fr5m2kzwil8")))

(define-public crate-unix-tty-0.3.1 (c (n "unix-tty") (v "0.3.1") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "null-terminated") (r "^0.3") (d #t) (k 0)) (d (n "syscall") (r "^0.2") (d #t) (k 0)) (d (n "unix") (r "^0.6.5") (d #t) (k 0)))) (h "0xr88zicixvxrd3rkg5480vkbaxypvnsfyjc6566s1y2ymj0zc2f")))

(define-public crate-unix-tty-0.3.2 (c (n "unix-tty") (v "0.3.2") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "null-terminated") (r "^0.3") (d #t) (k 0)) (d (n "syscall") (r "^0.2") (d #t) (k 0)) (d (n "unix") (r "^0.6.5") (d #t) (k 0)))) (h "08lcbg1flm0452w4qh17g1chfgcqy9n6ppb7m19025s2r1hjsafa")))

(define-public crate-unix-tty-0.3.3 (c (n "unix-tty") (v "0.3.3") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "null-terminated") (r "^0.3") (d #t) (k 0)) (d (n "system-call") (r "^0.1") (d #t) (k 0)) (d (n "unix") (r "^0.6.5") (d #t) (k 0)))) (h "18ic9lhva7v989sirs774sm5qrjl952qd2xk4a99w6hnyb2m5r2c")))

(define-public crate-unix-tty-0.3.4 (c (n "unix-tty") (v "0.3.4") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0) (p "libc-interface")) (d (n "null-terminated") (r "^0.3") (d #t) (k 0)) (d (n "system-call") (r "^0.1") (d #t) (k 0)) (d (n "unix") (r "^0.6.5") (d #t) (k 0)))) (h "170zx9vq24hlpaggck6fygmp6zikjcb1ykvww8qk8wwxdrh2ndy8")))

