(define-module (crates-io un sa unsafe_target_feature) #:use-module (crates-io))

(define-public crate-unsafe_target_feature-0.1.0 (c (n "unsafe_target_feature") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0k4i6xy3wbq3zqfnsw47akp63rq4snsp8jvhwgka9iwzwhi78ddr")))

(define-public crate-unsafe_target_feature-0.1.1 (c (n "unsafe_target_feature") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0a67sddpzpchmfrb2884ydmwmdjmn46r6lf764vrqfs6pf9c5j28")))

