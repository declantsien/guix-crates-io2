(define-module (crates-io un sa unsaferawbuf) #:use-module (crates-io))

(define-public crate-unsaferawbuf-0.1.0 (c (n "unsaferawbuf") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.135") (d #t) (k 2)))) (h "1awbacs9dnzsn0wrphpws3qllazv6f6zydbfaw23mlm3x7d2iyrh")))

(define-public crate-unsaferawbuf-0.1.1 (c (n "unsaferawbuf") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.135") (d #t) (k 2)))) (h "00s2ls758d3lvr8wgqzacbvfkb42cac3vljdxnd8ipn57nlf32g2") (r "1.67.0")))

(define-public crate-unsaferawbuf-0.1.2 (c (n "unsaferawbuf") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.135") (d #t) (k 2)))) (h "079frn9s738wcj4x0zsnhvhh15y9rrnx17qhvmv32fnpdqi5p5xp") (r "1.67.0")))

