(define-module (crates-io un sa unsafe-any) #:use-module (crates-io))

(define-public crate-unsafe-any-0.1.0 (c (n "unsafe-any") (v "0.1.0") (h "1h8fyccj79skl50n7r1m63cgn8xc64fcv35qwsbk3nl0yfg8fgqg")))

(define-public crate-unsafe-any-0.1.1 (c (n "unsafe-any") (v "0.1.1") (h "00m3d95fvpkr7zvd5sv8kwn6j127ipwznjfhyz43r3rakfbmjv27")))

(define-public crate-unsafe-any-0.2.0 (c (n "unsafe-any") (v "0.2.0") (h "029f3c7f1c8knk5cz9ikw0j8dy5250962g01bm52fdlb5z7w9s5n")))

(define-public crate-unsafe-any-0.2.1 (c (n "unsafe-any") (v "0.2.1") (h "0g3fswkxikj7vlba63a91cyr6xzqcvs0bnw6zi5mg6sxxm4k1wgr")))

(define-public crate-unsafe-any-0.2.2 (c (n "unsafe-any") (v "0.2.2") (h "0p97ps1qwky54mrhyrl3yidxbikwsvf72xa55gmf5gshxnxvallc")))

(define-public crate-unsafe-any-0.2.3 (c (n "unsafe-any") (v "0.2.3") (h "1nfsrhzaxi4al09vzlpyl19n4i7w5jl161krs7820i7wqxl224s5")))

(define-public crate-unsafe-any-0.2.4 (c (n "unsafe-any") (v "0.2.4") (d (list (d (n "traitobject") (r "*") (d #t) (k 0)))) (h "0g464rl6q5kp5ba2gvwk9njlkvjyvgba1sv7nyhl6fh1vnwv024a")))

(define-public crate-unsafe-any-0.2.5 (c (n "unsafe-any") (v "0.2.5") (d (list (d (n "traitobject") (r "*") (d #t) (k 0)))) (h "057z6jqnws8fwqz5sn0q2wn9l2azzd0q06dpjr4cphyiqzrzjdi1")))

(define-public crate-unsafe-any-0.3.0 (c (n "unsafe-any") (v "0.3.0") (d (list (d (n "traitobject") (r "*") (d #t) (k 0)))) (h "0ddrlqbjy2yvjvw9p7ff9chkcyc5ay45qhbsn29kcy0b3zwrf56p")))

(define-public crate-unsafe-any-0.4.0 (c (n "unsafe-any") (v "0.4.0") (d (list (d (n "traitobject") (r "*") (d #t) (k 0)))) (h "121wdgdp0arjfligfwwg3n569b576sap59shgmrmj2fzn6lm867m")))

(define-public crate-unsafe-any-0.4.1 (c (n "unsafe-any") (v "0.4.1") (d (list (d (n "traitobject") (r "*") (d #t) (k 0)))) (h "0fpx6wsi6l1zk5frhbgcx7jri7c83pb98kxblfp69hpb45h0hldk")))

(define-public crate-unsafe-any-0.4.2 (c (n "unsafe-any") (v "0.4.2") (d (list (d (n "traitobject") (r "^0.1") (d #t) (k 0)))) (h "0zwwphsqkw5qaiqmjwngnfpv9ym85qcsyj7adip9qplzjzbn00zk")))

