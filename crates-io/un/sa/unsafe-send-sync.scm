(define-module (crates-io un sa unsafe-send-sync) #:use-module (crates-io))

(define-public crate-unsafe-send-sync-0.0.0 (c (n "unsafe-send-sync") (v "0.0.0") (h "16hzkmpv3gw05xx8jvvcpgk9cxmwqpsjyf4mribrg85qyyyr2yl3")))

(define-public crate-unsafe-send-sync-0.0.1 (c (n "unsafe-send-sync") (v "0.0.1") (h "0x06agx017wvh1xzksk7na16x3nyl4jnmfhlc1r96fsi5nws2k86")))

(define-public crate-unsafe-send-sync-0.0.2 (c (n "unsafe-send-sync") (v "0.0.2") (h "01l9qnv16bkdp8ymbw989gmaycbp462fqgdhbiqsdrn4zdv0czfk")))

(define-public crate-unsafe-send-sync-0.0.3 (c (n "unsafe-send-sync") (v "0.0.3") (h "1rjcxhjbgl6hvbj0lf55namfy25rkvmhb1sc09hsydwiwx324kwn")))

(define-public crate-unsafe-send-sync-0.1.0 (c (n "unsafe-send-sync") (v "0.1.0") (h "10s4iifhymfh1p456an4wr90900mhl916y38ic3vhss2grs8n0g1")))

