(define-module (crates-io un sa unsafe-storage) #:use-module (crates-io))

(define-public crate-unsafe-storage-0.1.0 (c (n "unsafe-storage") (v "0.1.0") (h "0vfflj2zmlwpblqzmr4igvpgd8sb6ib3g3mbklx2jw8n0isj9dyj")))

(define-public crate-unsafe-storage-0.1.1 (c (n "unsafe-storage") (v "0.1.1") (h "01ndyka5kf2hqkzl5zj601vv57b04gim9af7k6qp8kxfk2bnd7bf")))

