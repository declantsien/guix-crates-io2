(define-module (crates-io un sa unsafesc) #:use-module (crates-io))

(define-public crate-unsafesc-0.0.1 (c (n "unsafesc") (v "0.0.1") (h "1q9vqfml1n8cbbkjpmdl1k7bh7z15gva9abbv1dkwkx2wpdidlhz") (y #t)))

(define-public crate-unsafesc-0.0.2 (c (n "unsafesc") (v "0.0.2") (h "1vksh8axw3ddq1a15p9sgv6nrmhhdfj4sjp09kx6msjgjm5rajz7") (y #t)))

