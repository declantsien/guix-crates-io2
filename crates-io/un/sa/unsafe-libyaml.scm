(define-module (crates-io un sa unsafe-libyaml) #:use-module (crates-io))

(define-public crate-unsafe-libyaml-0.0.0 (c (n "unsafe-libyaml") (v "0.0.0") (h "0qsxknmhakfzm9dicsvvfrwgiig14ph5sb4pqnc809b94zr5msxs") (y #t)))

(define-public crate-unsafe-libyaml-0.1.0 (c (n "unsafe-libyaml") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1ivm4i75sqfy5l377xcc4kz3r29bd7nsflhwz6p3dz9d6n43m8ci") (r "1.56")))

(define-public crate-unsafe-libyaml-0.1.1 (c (n "unsafe-libyaml") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0kfkh2qnjyxbi97y00ky3i1vr30ah4l71cz96bw0xh79w6kpjsxn") (r "1.56")))

(define-public crate-unsafe-libyaml-0.1.2 (c (n "unsafe-libyaml") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0sbin71s16larav2aqmgbrw7xiavb1lab0zx4h66pv2jhapc1w7y") (r "1.56")))

(define-public crate-unsafe-libyaml-0.1.3 (c (n "unsafe-libyaml") (v "0.1.3") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0ix37dq268jccz82nms37z44vz5skydz4pr9shkyqka2jg1jlgil") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.0 (c (n "unsafe-libyaml") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0dwjsyiwj7ghvw4ks5ypaadddl453yw50xymb6vwbkynnxvnn9qk") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.1 (c (n "unsafe-libyaml") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "04zqyzb4l7680lka42x8in1fb0c0nxyw0qnlwllbx48h64vwdhcx") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.2 (c (n "unsafe-libyaml") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1c5fg4b1vyfpqy0845rjb0d1qf41czshypmsck7mnfan98rpj4ck") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.3 (c (n "unsafe-libyaml") (v "0.2.3") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "16f947045ahfz0drx8yvwq3cg9gc8cd6vqf39v5nj1km7nl2bnfh") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.4 (c (n "unsafe-libyaml") (v "0.2.4") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0s3f83hy8rd4q6r0dj4pmwyrgvlhsd0vxmzqaslg3ica7mbzmrf1") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.5 (c (n "unsafe-libyaml") (v "0.2.5") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1lmf2a079x763qim9041r0k92d2359lc7lhsx9wbw1na8jxdhzmw") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.6 (c (n "unsafe-libyaml") (v "0.2.6") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0c9bp408b0vy9f22dx3k9nwbxk5rg432yclhr6vzsn8iskw80yqk") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.7 (c (n "unsafe-libyaml") (v "0.2.7") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0v7g7cgs05nviyavyka22xjs0avkyr5f15cnafzp8f7x592j885d") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.8 (c (n "unsafe-libyaml") (v "0.2.8") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "19l0v20x83dvxbr68rqvs9hvawaqd929hia1nldfahlhamm80r8q") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.9 (c (n "unsafe-libyaml") (v "0.2.9") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "1yjwnz124wp1fhj075rdqkz00n2gahzj9yi5ixnmiinkw79ng17j") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.10 (c (n "unsafe-libyaml") (v "0.2.10") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0jsyc1kqc536wpgx1js61lwj86crniqw16lyvh02va4m1f9r0k5b") (r "1.56")))

(define-public crate-unsafe-libyaml-0.2.11 (c (n "unsafe-libyaml") (v "0.2.11") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "0qdq69ffl3v5pzx9kzxbghzn0fzn266i1xn70y88maybz9csqfk7") (r "1.56")))

