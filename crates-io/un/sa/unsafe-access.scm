(define-module (crates-io un sa unsafe-access) #:use-module (crates-io))

(define-public crate-unsafe-access-0.1.0 (c (n "unsafe-access") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1yzc6blyybzb7nii6yzr9kl5m7i5pkx0drh2d99kpr7w316id5fk")))

(define-public crate-unsafe-access-0.1.1 (c (n "unsafe-access") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "19in5n2wpysd33mhwbx2s1dk8x4a4kzwvhs87ib83z3dzxyhl1p0")))

