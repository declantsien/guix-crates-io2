(define-module (crates-io un sa unsafe-unicorn) #:use-module (crates-io))

(define-public crate-unsafe-unicorn-0.0.0 (c (n "unsafe-unicorn") (v "0.0.0") (h "0zxdb7yxb5jgn2xj2agw54n0bxhyy38n0zihv0s9niszidd8x75m")))

(define-public crate-unsafe-unicorn-0.0.1 (c (n "unsafe-unicorn") (v "0.0.1") (d (list (d (n "git2") (r "^0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ffyghw473h8gh7asiapgpsb432lashjsmacsp0x3xb7cbjfvfg2")))

