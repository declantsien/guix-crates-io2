(define-module (crates-io un sa unsafe-libopus) #:use-module (crates-io))

(define-public crate-unsafe-libopus-0.1.0 (c (n "unsafe-libopus") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "libc-stdhandle") (r "^0.1.0") (d #t) (k 2)))) (h "090pnzmqd2n7jilgb3qgq8d60vk6ldfvicwwn1wadnkxh4cccg9h")))

(define-public crate-unsafe-libopus-0.1.1 (c (n "unsafe-libopus") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "libc-stdhandle") (r "^0.1.0") (d #t) (k 2)))) (h "1ixf21lgg0vbglwq4jkzzrd6d9jkx91d8a71mrzgma9nwklvc62i")))

