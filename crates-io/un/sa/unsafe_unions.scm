(define-module (crates-io un sa unsafe_unions) #:use-module (crates-io))

(define-public crate-unsafe_unions-0.0.0 (c (n "unsafe_unions") (v "0.0.0") (d (list (d (n "interpolate_idents") (r "^0.0.4") (d #t) (k 0)))) (h "1bajhacm7vi6z34v20dn8jsbd2rd3p5zkd95z37g44dsii9zjq72")))

(define-public crate-unsafe_unions-0.0.1 (c (n "unsafe_unions") (v "0.0.1") (h "1zdxgs039bwwg3svh2xb76qwqb2vwawxzyh3n4vf551iyv2b0l81")))

(define-public crate-unsafe_unions-0.0.2 (c (n "unsafe_unions") (v "0.0.2") (h "1ks2j6j8h97q82vkqkv2jdm01s79jgl8llavfh415sblvi2pa7rl")))

