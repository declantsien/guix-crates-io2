(define-module (crates-io un sa unsafe-alias-cell) #:use-module (crates-io))

(define-public crate-unsafe-alias-cell-0.0.0 (c (n "unsafe-alias-cell") (v "0.0.0") (h "1hldgl6bjsyl4ygx2w4spgzs5gvnnv9qlzrxx36hlc55jzjkhn40") (y #t)))

(define-public crate-unsafe-alias-cell-0.0.1 (c (n "unsafe-alias-cell") (v "0.0.1") (h "0gkyhdzqaqn1w0hvdk7bfbhwfdccajnlglzvv5ya8iv6wb8s68pl") (y #t)))

