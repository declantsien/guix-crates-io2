(define-module (crates-io un sa unsafer) #:use-module (crates-io))

(define-public crate-unsafer-0.1.0 (c (n "unsafer") (v "0.1.0") (h "10bhs3psp7yasr61xx4m6hg3px4ikdp03x4lmqskgkg6vy069l6h")))

(define-public crate-unsafer-0.1.1 (c (n "unsafer") (v "0.1.1") (h "11frbml3si609219xhfbhwksg6p3k2g7gxckchjf0wma0fx0632x")))

