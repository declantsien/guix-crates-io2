(define-module (crates-io un sa unsafe_example) #:use-module (crates-io))

(define-public crate-unsafe_example-0.1.0 (c (n "unsafe_example") (v "0.1.0") (h "10zxk3rkkyzabfwirnn615600jr6727751xrf9p4vpphj737cggv")))

(define-public crate-unsafe_example-0.2.0 (c (n "unsafe_example") (v "0.2.0") (h "1ikxwzf4pqfiprv1ihm79gqd81301xc39hx6zdy8dn4i9znmf3fm")))

(define-public crate-unsafe_example-0.3.0 (c (n "unsafe_example") (v "0.3.0") (h "18nnk8b2pyi47f65alfd270h4r87flw5swv8nfg0dk70bmh41jsy")))

(define-public crate-unsafe_example-0.3.1 (c (n "unsafe_example") (v "0.3.1") (h "0qdfw921hxgs0a8h9n98p0yym8krvhlrjf82y0jzar3pbi7nxfvl")))

(define-public crate-unsafe_example-0.3.2 (c (n "unsafe_example") (v "0.3.2") (h "0ihpcsw707z7hhkamj53p69gabh1mmqnrnfg1csk7jsw34zk2q9n")))

(define-public crate-unsafe_example-0.3.3 (c (n "unsafe_example") (v "0.3.3") (h "0kf4scr8cimyxd7vkwc69d7ycaxx0nhaxhij33yicp7yyk06653k")))

