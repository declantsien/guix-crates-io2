(define-module (crates-io un sa unsafe_bst) #:use-module (crates-io))

(define-public crate-unsafe_bst-0.2.0 (c (n "unsafe_bst") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1glq2sdl5868ficj5dys7skqgjqy3yx8f89kwry13jxm5aswgfzs")))

(define-public crate-unsafe_bst-0.2.1 (c (n "unsafe_bst") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0sd8vdyy06byr2ici11hns1f80jnp4dl8gl6wyyxhqbl5qsw4j0v")))

(define-public crate-unsafe_bst-0.2.2 (c (n "unsafe_bst") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1nkfrgb9m20pffy1akag3fi7mxfchyrwpr0abivsmrgws6jrbm5x")))

(define-public crate-unsafe_bst-0.3.0 (c (n "unsafe_bst") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1f1gf9f5yd8k96zxingavdbrlhlpfmj510r4q66q947zx8xsac5d")))

(define-public crate-unsafe_bst-0.3.1 (c (n "unsafe_bst") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "06k4x4gw96djqrs9h3zzf09q89xpg21xkxxy366lwi73z6nsgyvx")))

(define-public crate-unsafe_bst-0.3.2 (c (n "unsafe_bst") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1lh9zqkhijr5ci075j1ayyl8hwqzqmfdp8jzsk6ykffsh6wn6z4z")))

