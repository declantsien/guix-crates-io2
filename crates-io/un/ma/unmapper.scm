(define-module (crates-io un ma unmapper) #:use-module (crates-io))

(define-public crate-unmapper-0.1.0 (c (n "unmapper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "json") (r "^1.0.72") (d #t) (k 0) (p "serde_json")) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "1qj2hbc5ci113lxqv9s9k7ib80mng55w638zf5p4gzkbbbcpfhbw")))

