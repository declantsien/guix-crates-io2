(define-module (crates-io un m_ unm_selector) #:use-module (crates-io))

(define-public crate-unm_selector-0.2.0-pre.2 (c (n "unm_selector") (v "0.2.0-pre.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "unm_types") (r "^0.2.0-pre.1") (d #t) (k 0)))) (h "1q9kcq5mzbv0h6vnf0gia80lgphz9pl2n7h9n5kzngb8lbbyqzay")))

(define-public crate-unm_selector-0.3.0-pre.0 (c (n "unm_selector") (v "0.3.0-pre.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "unm_types") (r "^0.3.0-pre.0") (d #t) (k 0)))) (h "0253bp5sgqrfvd8z151bh72yxv2wkh6srhr0nzlsnwna3yzmmxks")))

(define-public crate-unm_selector-0.3.0-pre.1 (c (n "unm_selector") (v "0.3.0-pre.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "unm_types") (r "^0.3.0-pre.0") (d #t) (k 0)))) (h "1z830zav8r71dcwv80kb2k2li89rvj3ql8brqx076gy8kcvgij3k")))

(define-public crate-unm_selector-0.3.0 (c (n "unm_selector") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "unm_types") (r "^0.3.0-pre.0") (d #t) (k 0)))) (h "1ybnw1sivyxa115khad19c95y50rjkn6fc8nrkjn8ip6g48sh3pb")))

(define-public crate-unm_selector-0.4.0 (c (n "unm_selector") (v "0.4.0") (d (list (d (n "unm_types") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1iabv746swpn5d36s05rv1jz8pdz2n7ygr6b9nbc2h3izb20iij1")))

