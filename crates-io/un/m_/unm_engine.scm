(define-module (crates-io un m_ unm_engine) #:use-module (crates-io))

(define-public crate-unm_engine-0.2.0-pre.2 (c (n "unm_engine") (v "0.2.0-pre.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "unm_types") (r "^0.2.0-pre.1") (d #t) (k 0)))) (h "1yl2ryv7r26k3kd8gnvih9jjxdj6sljwmc9jrykfv7dpcij9ij5k")))

(define-public crate-unm_engine-0.2.0-pre.3 (c (n "unm_engine") (v "0.2.0-pre.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "unm_types") (r "^0.2.0-pre.1") (d #t) (k 0)))) (h "1k731v87ykhv1bfhdgp00fx3ywm192dw5nmsydvq3s5lqrjs9cws")))

(define-public crate-unm_engine-0.2.0-pre.4 (c (n "unm_engine") (v "0.2.0-pre.4") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "unm_types") (r "^0.2.0-pre.1") (d #t) (k 0)))) (h "0clzm2zr34vyka9xh6d6a2xa0i4shmfgl12lrckvjnhizzq875w4")))

(define-public crate-unm_engine-0.3.0-pre.0 (c (n "unm_engine") (v "0.3.0-pre.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "unm_types") (r "^0.3.0-pre.0") (d #t) (k 0)))) (h "1pcbzqh4yf5swbv8yal5marc8hwv9ydy2vbs29lhr7qn380x2jxi")))

(define-public crate-unm_engine-0.3.0 (c (n "unm_engine") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "unm_types") (r "^0.3.0-pre.0") (d #t) (k 0)))) (h "1cv8pk2d2ldg63qvrmks4rb9fg0ip091352ycw6wgi80pzyj15dy")))

(define-public crate-unm_engine-0.4.0 (c (n "unm_engine") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.61") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "unm_types") (r "^0.4.0") (d #t) (k 0)))) (h "0wqlgvv0b80knw6kdmc2k8nvphv4lcqfa59jb7j9bs2a7m6xkk33")))

