(define-module (crates-io un m_ unm_types) #:use-module (crates-io))

(define-public crate-unm_types-0.2.0-pre.2 (c (n "unm_types") (v "0.2.0-pre.2") (d (list (d (n "derive_builder") (r "^0.11.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "typed-builder") (r "^0.10.0") (d #t) (k 0)))) (h "1ppxwraz3zi971zsmrk696lqgmm7mlnq70v4w481iv4r6ac9wv38")))

(define-public crate-unm_types-0.2.0-pre.4 (c (n "unm_types") (v "0.2.0-pre.4") (d (list (d (n "derive_builder") (r "^0.11.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "typed-builder") (r "^0.10.0") (d #t) (k 0)))) (h "04dyn9jqlqvfqsjrdlyrix70zz9v8hx5qs84hlph1nh25djr0x2g")))

(define-public crate-unm_types-0.2.0-pre.5 (c (n "unm_types") (v "0.2.0-pre.5") (d (list (d (n "derive_builder") (r "^0.11.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "typed-builder") (r "^0.10.0") (d #t) (k 0)))) (h "191h9m7an5k91nr37gzq448ksjjvys1aw0rjf5s920r9l0w6pz01")))

(define-public crate-unm_types-0.3.0-pre.0 (c (n "unm_types") (v "0.3.0-pre.0") (d (list (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "typed-builder") (r "^0.10.0") (d #t) (k 0)))) (h "1l2ilvn19yngsl56wf6avghbmghamv59hzrvqynv5rbgbbilhv4q")))

(define-public crate-unm_types-0.3.0 (c (n "unm_types") (v "0.3.0") (d (list (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "typed-builder") (r "^0.10.0") (d #t) (k 0)))) (h "1km6c9lmsx6f7dcyrvm7cma0yri4fri56a19f88dlwy2p5d78rmw")))

(define-public crate-unm_types-0.4.0 (c (n "unm_types") (v "0.4.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "typed-builder") (r "^0.11.0") (d #t) (k 0)))) (h "0nqvs12f0ksgkrldny5hakyirb2a4zibzkz768h8igkb4rkw78cb")))

