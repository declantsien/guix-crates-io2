(define-module (crates-io un io union-utils) #:use-module (crates-io))

(define-public crate-union-utils-0.1.0 (c (n "union-utils") (v "0.1.0") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.3") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0lx0jlg9zdwdmdkwx22nsdjp24ymhxjrnih5fm05yzsf4lqwlhr7")))

(define-public crate-union-utils-0.1.1 (c (n "union-utils") (v "0.1.1") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.5") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1dgcpirbdm3rgj1f0k62fkq7vd58y9pzjgcaygfmiqzwzn4lffpv")))

(define-public crate-union-utils-0.1.2 (c (n "union-utils") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.5") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1qdvpi36nabbx4mzqzg6808x5fpmy351fg2z4lqns3hc1gbyrv2k")))

(define-public crate-union-utils-0.1.3 (c (n "union-utils") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1hgi7dmx7zdwr9z8dynghvyjhkjfq8c57pn2zjbd11k5wm5c1wjx")))

(define-public crate-union-utils-0.1.4 (c (n "union-utils") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1a8afh4m8ajaf3bi6p6fv38rk7f82k8261j0yxxcrmygnr3bpjx6")))

(define-public crate-union-utils-0.1.5 (c (n "union-utils") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0n9x07jb7pyx45ilrhjnipad4p3wir1js2yazzrpdvc523cxhmaw")))

