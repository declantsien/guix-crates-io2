(define-module (crates-io un io unios) #:use-module (crates-io))

(define-public crate-unios-0.1.0 (c (n "unios") (v "0.1.0") (h "0s1hw93r32ql27bf3rnn8fi6nbqdqaxpmfqm9f5hxkhp0q93nsix")))

(define-public crate-unios-0.2.0 (c (n "unios") (v "0.2.0") (h "1giazmcyr8xcs8mxiql4a842s8k8dg4d31sl163c1d4ps8k06f93")))

