(define-module (crates-io un io union-fn) #:use-module (crates-io))

(define-public crate-union-fn-0.1.0 (c (n "union-fn") (v "0.1.0") (d (list (d (n "trybuild") (r "^1.0.60") (f (quote ("diff"))) (d #t) (k 2)) (d (n "union-fn-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0yi41jrnga40q15mad5agdwgxbna0diaf78ylysbbv3yfda6x10j")))

(define-public crate-union-fn-0.2.0 (c (n "union-fn") (v "0.2.0") (d (list (d (n "union-fn-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1drm7c6z0i69m1cvxhcwds1gg2kfizf5wcgxg777zrl38fd87y4v")))

