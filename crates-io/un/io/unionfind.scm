(define-module (crates-io un io unionfind) #:use-module (crates-io))

(define-public crate-unionfind-0.1.0 (c (n "unionfind") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "159dnx3c9mfy5l5s1rwp6b55h0svfnaich9ylqd0670rkrhn52by")))

(define-public crate-unionfind-0.1.1 (c (n "unionfind") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "010xgf6nl4yqb5rgcnj9rawk6r7c489h0h1kq2m150lxwnfldnlv")))

(define-public crate-unionfind-0.1.2 (c (n "unionfind") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0w67kwf5g7gn6cy6jfr2r4wgzc7wpzzbc0ajq73mg5nh078inhpn")))

(define-public crate-unionfind-0.2.0 (c (n "unionfind") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1xb4czwl8r2znyj11q3qs1qfb6h3k8wzj0n8fsy5aqa5dsw1ljm0")))

(define-public crate-unionfind-0.2.1 (c (n "unionfind") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v1fwdkfpw4dy9n0y757m1ayi8s8gf783yy6m8frm0saq0im0bca")))

