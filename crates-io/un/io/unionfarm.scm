(define-module (crates-io un io unionfarm) #:use-module (crates-io))

(define-public crate-unionfarm-0.1.0 (c (n "unionfarm") (v "0.1.0") (d (list (d (n "clap-verbosity-flag") (r "^0.2") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0cyfcmmnasis63azc4n3vgbiyh9625diyx51x4rvlwb5fvjy9p2x")))

(define-public crate-unionfarm-0.1.1 (c (n "unionfarm") (v "0.1.1") (d (list (d (n "clap-verbosity-flag") (r "^0.2") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0bh0a0l17z3njdgwhns2cafp5g4mvi8813zy8jngf5nlyh9ifawj")))

(define-public crate-unionfarm-0.1.2 (c (n "unionfarm") (v "0.1.2") (d (list (d (n "clap-verbosity-flag") (r "^0.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "01k0w5n8id1wp6rjilv121085awghhvj3rhz3lvjqcrvrs4rm8zc")))

(define-public crate-unionfarm-0.1.4 (c (n "unionfarm") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "060yd38q6h8fyvfi3k4yzcq7vh8jljx458igw8l9xzli439182xf")))

