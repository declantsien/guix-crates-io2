(define-module (crates-io un io union-fn-macro) #:use-module (crates-io))

(define-public crate-union-fn-macro-0.1.0 (c (n "union-fn-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rpns41zq1lzw9nii1hbfhh629aazq5fk7vwv568rmmrzmp3d631")))

(define-public crate-union-fn-macro-0.2.0 (c (n "union-fn-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00kd1f02qxcmrm554sxbdf0wal1814saf8nx6nl46zr4hh0c5nw7")))

