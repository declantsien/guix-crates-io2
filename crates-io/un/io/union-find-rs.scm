(define-module (crates-io un io union-find-rs) #:use-module (crates-io))

(define-public crate-union-find-rs-0.1.0 (c (n "union-find-rs") (v "0.1.0") (h "0pi3k3a2nwwg0yhpg6gfvwwgznibm6siwwcf14q6xszzpkswya0m")))

(define-public crate-union-find-rs-0.1.1 (c (n "union-find-rs") (v "0.1.1") (h "14l2b37xicgnjcdgw82cy9p70iyzwm0ykvrfdcsv3psllbycz854")))

(define-public crate-union-find-rs-0.1.2 (c (n "union-find-rs") (v "0.1.2") (h "0fqs28id8pysrmydr8ai47pdb1brwxc3jdsbsvsf406haxbriz2h")))

(define-public crate-union-find-rs-0.1.3 (c (n "union-find-rs") (v "0.1.3") (h "0cv5k3ypraq9g0y1xzlhw3m02n4d2cssvfw1a631i2b68lwlmkhn")))

(define-public crate-union-find-rs-0.2.0 (c (n "union-find-rs") (v "0.2.0") (h "0q66zqhqgzwx57n58p2l5sy5xgq3hcys5vgkdga34s84h9bh95i0")))

(define-public crate-union-find-rs-0.2.1 (c (n "union-find-rs") (v "0.2.1") (h "0pw8kjgj2wbh6c98s3yhdk4qcxcblq5xs3dan255hdc6djpgaz4d")))

