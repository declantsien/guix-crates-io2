(define-module (crates-io un io union-future) #:use-module (crates-io))

(define-public crate-union-future-0.1.0 (c (n "union-future") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.7") (d #t) (k 0)))) (h "1xdb7sjdwg8zadym4a9a29a581xssnj1ib3ps3fvm43b5a3zs495")))

(define-public crate-union-future-0.1.1 (c (n "union-future") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.7") (d #t) (k 0)))) (h "179khi7a40i4cig5lqyly3hl0hzx0d5jgfrqvscks748mzv7m5x8")))

