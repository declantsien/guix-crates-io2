(define-module (crates-io un io union_export) #:use-module (crates-io))

(define-public crate-union_export-0.1.0-alpha.1 (c (n "union_export") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "union_impl") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "1zy7q2rnyvfadbgxsfkr74574z41iqm6gjvb1gka1k681k0y7lvs") (f (quote (("std"))))))

(define-public crate-union_export-0.1.0-alpha.2 (c (n "union_export") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "union_impl") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "0asqndzdds2p9gk6f1nkga5lj3f3fy7r4gw0b20i1n7q4c163iiv") (f (quote (("std"))))))

(define-public crate-union_export-0.1.0 (c (n "union_export") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "union_impl") (r "^0.1.0") (d #t) (k 0)))) (h "16ailvs0sajr0l7jqs57lzv33npxpn2dx05f869wcb097l2jd5sd") (f (quote (("std"))))))

(define-public crate-union_export-0.1.1 (c (n "union_export") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "union_impl") (r "^0.1.1") (d #t) (k 0)))) (h "013pv18psdd9rh5770acb83hir0vdw0g1xdjsq9gh3fw2nhg5zz9") (f (quote (("std"))))))

(define-public crate-union_export-0.1.2 (c (n "union_export") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "union_impl") (r "^0.1.2") (d #t) (k 0)))) (h "0qk9zslap8nh24ig646lzsiqsdr3p442x7cswss83acgr3gi0n28") (f (quote (("std"))))))

(define-public crate-union_export-0.1.3 (c (n "union_export") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "union_impl") (r "^0.1.3") (d #t) (k 0)))) (h "0x72n296nnvps0n6yrldv6qvchzmkngpn8znzi6zjkfm0fk2x4z4") (f (quote (("std"))))))

