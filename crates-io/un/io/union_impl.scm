(define-module (crates-io un io union_impl) #:use-module (crates-io))

(define-public crate-union_impl-0.0.1 (c (n "union_impl") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k01d6dqhzm1sry8v8yjd8scxjasnb7423p4n8whbnrjqpwh2m02") (f (quote (("std"))))))

(define-public crate-union_impl-0.0.2 (c (n "union_impl") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cb80wnd5pmqg8qj7iknbh02g6y1933czdmqr4xzg051crmvbn5i") (f (quote (("std"))))))

(define-public crate-union_impl-0.1.0-alpha.1 (c (n "union_impl") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03zddxr4z9ky2xdzpfjznjaxjiahwcfaklf3y159p1hrpg9pfbbd") (f (quote (("std"))))))

(define-public crate-union_impl-0.1.0-alpha.2 (c (n "union_impl") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mb3rbpyc7c6fy2v9wpq1p5slpxg2blp6jdmfkyl9dg36idang5x") (f (quote (("std"))))))

(define-public crate-union_impl-0.1.0 (c (n "union_impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1basbm12qs7ysqvfwrxrs0als94lkq9z54fgayrlhm011bgyf5mm") (f (quote (("std"))))))

(define-public crate-union_impl-0.1.1 (c (n "union_impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "union") (r "^0.1.2") (d #t) (k 2)))) (h "1c3mhicdb02hkgndsk092hbh0ja9awrayyk1qz6mdcgfsqcd79js") (f (quote (("std"))))))

(define-public crate-union_impl-0.1.2 (c (n "union_impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "union") (r "^0.1.2") (d #t) (k 2)))) (h "0bjp0f2azwdsx67vpgfqkqc2dphswyin47037f533ppgaw21d7z9") (f (quote (("std"))))))

(define-public crate-union_impl-0.1.3 (c (n "union_impl") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "union") (r "^0.1.4") (d #t) (k 2)))) (h "0lcmpqsyhzdlja9l11c4jj48dffxnb70n06h9g3v6xvif2d5ym73") (f (quote (("std"))))))

