(define-module (crates-io un io union-find) #:use-module (crates-io))

(define-public crate-union-find-0.0.1 (c (n "union-find") (v "0.0.1") (h "10riyjdylwcabqpblllhi1x4y36v7lcxwjlpp40a2va8wl8fwx4r")))

(define-public crate-union-find-0.0.2 (c (n "union-find") (v "0.0.2") (h "1sdfdk7g1d8qvmfl0yf7xq6dvj6pg9li230haz2h65xd91s5j93j")))

(define-public crate-union-find-0.0.3 (c (n "union-find") (v "0.0.3") (h "0a663xpv9ndnp7hifviray5xxp9gv27mchw8yrwajm3dm6f3wa4s")))

(define-public crate-union-find-0.1.0 (c (n "union-find") (v "0.1.0") (h "01zpb7v3j56h98fm7bk21glypjvi1gx9pgv2ldfgpk6b3dcliwir")))

(define-public crate-union-find-0.1.1 (c (n "union-find") (v "0.1.1") (h "1zgkcqs5asd6wpp1vgx1x1wkmnd1xkhp2vdw1x0v9x7hxgi2aama")))

(define-public crate-union-find-0.1.2 (c (n "union-find") (v "0.1.2") (h "0nayqqzxi3ixa55xsqyzlvfvygm5zrp3by3wchjh679vh9s7icz0")))

(define-public crate-union-find-0.1.3 (c (n "union-find") (v "0.1.3") (h "1qf43xxzvj38ri472akxr3z43wiac9gkjxyfmw5xwasr06ywczn8")))

(define-public crate-union-find-0.1.4 (c (n "union-find") (v "0.1.4") (h "0nj490rrm00si8kvwsw9waf26w088zj4hhwqyadj3gd612nafgf9")))

(define-public crate-union-find-0.1.5 (c (n "union-find") (v "0.1.5") (h "1fjjgn1xszkybbckajrx8ffvbsgswv3qjfac2p4qap34k1b21q8q") (f (quote (("nightly"))))))

(define-public crate-union-find-0.1.6 (c (n "union-find") (v "0.1.6") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0lqikdkfmgj451hyiwfky7q4dsklyjpg9ns6nyai0hyyicvwj69q") (f (quote (("nightly"))))))

(define-public crate-union-find-0.1.7 (c (n "union-find") (v "0.1.7") (h "13zqqrsg1bv0iv9kybci94i3vlqwswd06r93p9b5nbk74g3qpzj5") (f (quote (("nightly"))))))

(define-public crate-union-find-0.2.0 (c (n "union-find") (v "0.2.0") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "1mgl2pvl9pjyilz60aq92rnhi03q0w8qvslw36sf6247yd4r3xjh") (f (quote (("nightly"))))))

(define-public crate-union-find-0.2.1 (c (n "union-find") (v "0.2.1") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "161sfyn53ydsmysradkc5z4pmz63hd19qbmg9dn70x83y0283da1") (f (quote (("nightly"))))))

(define-public crate-union-find-0.2.2 (c (n "union-find") (v "0.2.2") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "0gzmb3hpdgzgnp5xnlqv5sff6v2k31bsjdk4y0yqaza10qip98g0") (f (quote (("nightly"))))))

(define-public crate-union-find-0.3.0 (c (n "union-find") (v "0.3.0") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)))) (h "04sfipwbphhh86gz99hdyc1dlq81jxyvhy45pa4d9a3da8a8mb8n") (f (quote (("nightly"))))))

(define-public crate-union-find-0.3.1 (c (n "union-find") (v "0.3.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 2)))) (h "1c7l6irkjv64300fflr2316in0b54jm2ylpr1gbvrzrclm7dd80b") (f (quote (("nightly"))))))

(define-public crate-union-find-0.3.2 (c (n "union-find") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "0837ihyp1blazm0pnqdm8zhiiv5zqfl70rs4gabpmzqpi6xa5n6y") (f (quote (("nightly"))))))

(define-public crate-union-find-0.3.3 (c (n "union-find") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "10yxhlggp0fkdbass3n51i3jqdnmag0mc1fxmwr0hmf4cid4pz13") (f (quote (("nightly"))))))

(define-public crate-union-find-0.4.0 (c (n "union-find") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "16hyzh7f3j2fvzgjjc6953c5nxmkg8amni8dcij13di92lwrl2nv") (r "1.56.1")))

(define-public crate-union-find-0.4.1 (c (n "union-find") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "1pcnri49y40ybpv8rd659hjfiqy2lj30b3rmvgfijjwqfmr33vss") (r "1.56.1")))

(define-public crate-union-find-0.4.2 (c (n "union-find") (v "0.4.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "03wkp8yqzrwkhnvicr5sg0y0771mjzhb0rcwyw8xfmr59cgs62f1") (r "1.70.0")))

