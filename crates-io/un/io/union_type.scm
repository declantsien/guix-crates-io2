(define-module (crates-io un io union_type) #:use-module (crates-io))

(define-public crate-union_type-0.0.0 (c (n "union_type") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bnpanp3ly1cbh3195ysd2wzca6kvwma71pr9rshrbwdg3mcsyyw")))

(define-public crate-union_type-0.0.1 (c (n "union_type") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f8si6ffzn2nd3gb5rr2sb6ijvm6w8c325hli0ckmnfqh790ncca")))

(define-public crate-union_type-0.1.0 (c (n "union_type") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sg8dl1n8gsswd69mb5rjz6hhz3bf43aq62vf8r4wfs16rinr4j9")))

