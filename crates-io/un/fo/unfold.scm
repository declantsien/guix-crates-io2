(define-module (crates-io un fo unfold) #:use-module (crates-io))

(define-public crate-unfold-0.1.0 (c (n "unfold") (v "0.1.0") (h "0ccpc80mmap4abhril60j89cl9pjv6pnll859l8gs6bwyd57skwp")))

(define-public crate-unfold-0.1.1 (c (n "unfold") (v "0.1.1") (h "0gk5cgk62fydz5rmi4b9yx272259700y5gnjpf2pf2wgakb3bfry")))

(define-public crate-unfold-0.2.0 (c (n "unfold") (v "0.2.0") (h "0f65x5jyhlwirbz28vfgs01ldbipy7wz1grw2dlskqlbz7gk9plg")))

