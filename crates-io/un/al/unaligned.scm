(define-module (crates-io un al unaligned) #:use-module (crates-io))

(define-public crate-unaligned-0.1.0 (c (n "unaligned") (v "0.1.0") (d (list (d (n "scopeguard") (r "^1.1") (d #t) (k 0)))) (h "12mah2q58kvcnk47095qpdypaic2dibglzz2vca3vvsd0nkd1shx") (f (quote (("std"))))))

(define-public crate-unaligned-0.1.1 (c (n "unaligned") (v "0.1.1") (d (list (d (n "scopeguard") (r "^1.1") (k 0)))) (h "1ji7anh2arjfpl7h7h1iykk8hkiw7jfhq3zx6j5m2v34i1z0r2ml") (f (quote (("std"))))))

