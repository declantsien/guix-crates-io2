(define-module (crates-io un ru unrust-codegen) #:use-module (crates-io))

(define-public crate-unrust-codegen-0.1.0 (c (n "unrust-codegen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "genco") (r "^0.17.5") (d #t) (k 0)) (d (n "inbuilt") (r "^0.1.0") (d #t) (k 0) (p "unrust-inbuilt")) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1r5knb6sim6k5ibw3wqvz2svg9dvd799zsncym54pz7rswajvlyn")))

(define-public crate-unrust-codegen-0.1.1 (c (n "unrust-codegen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "genco") (r "^0.17.5") (d #t) (k 0)) (d (n "inbuilt") (r "^0.1.1") (d #t) (k 0) (p "unrust-inbuilt")) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rng3biz47b2zzpgy5lrphlakb0175y3v4mxxrvqywx0rn8bvdcm")))

(define-public crate-unrust-codegen-0.1.2 (c (n "unrust-codegen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "genco") (r "^0.17.5") (d #t) (k 0)) (d (n "inbuilt") (r "^0.1.2") (d #t) (k 0) (p "unrust-inbuilt")) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0h4kkpigk1yxi7kkx0b7qir82a86w5db9xcc9n0p8hw07ym1h3ds")))

(define-public crate-unrust-codegen-0.1.3 (c (n "unrust-codegen") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "genco") (r "^0.17.5") (d #t) (k 0)) (d (n "inbuilt") (r "^0.1.3") (d #t) (k 0) (p "unrust-inbuilt")) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19yhh3s5n7f14dh7vrwzd9aya0m3csh19j06z50q70lx92r54s5r")))

(define-public crate-unrust-codegen-0.1.4 (c (n "unrust-codegen") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "genco") (r "^0.17.5") (d #t) (k 0)) (d (n "inbuilt") (r "^0.1.4") (d #t) (k 0) (p "unrust-inbuilt")) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03q641zwax3h30m52yyi05zjbwjg8vl8j3y2h3cd2x5y7nadld56")))

(define-public crate-unrust-codegen-0.1.5 (c (n "unrust-codegen") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "genco") (r "^0.17.5") (d #t) (k 0)) (d (n "inbuilt") (r "^0.1.5") (d #t) (k 0) (p "unrust-inbuilt")) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "14hivi40cjidzi3chfd1hki7nnrcgzxw8khi0l0r2idlg8mp54l6")))

(define-public crate-unrust-codegen-0.1.6 (c (n "unrust-codegen") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "genco") (r "^0.17.5") (d #t) (k 0)) (d (n "inbuilt") (r "^0.1.6") (d #t) (k 0) (p "unrust-inbuilt")) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n0jxfhh50rhkirkyjmc4bin6kz07jslpn72fhz9mws31ngijn11")))

