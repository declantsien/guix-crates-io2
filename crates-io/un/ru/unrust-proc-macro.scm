(define-module (crates-io un ru unrust-proc-macro) #:use-module (crates-io))

(define-public crate-unrust-proc-macro-0.1.0 (c (n "unrust-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kls7q4lw15lplsknbmhcfd49rl6vfjm9fnavf97x90jm3avs89f")))

(define-public crate-unrust-proc-macro-0.1.1 (c (n "unrust-proc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "050bl8xzqzll6qsr3r32jjl6c25hcwgycahprpqfgacy99h7fmng")))

(define-public crate-unrust-proc-macro-0.1.2 (c (n "unrust-proc-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g9an3cmj9v0yq43h17fd3wypzxcavdwfj9f6zdm0j35znnz77hq")))

(define-public crate-unrust-proc-macro-0.1.3 (c (n "unrust-proc-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y59fpkfamxyvxp0c91x008rdq7h79xx4i0ssjg2cs4s9v36npkq")))

(define-public crate-unrust-proc-macro-0.1.4 (c (n "unrust-proc-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0abvdw6qw5vh2k9nkcpv9x4cjh43n9gyz0l7wrsvql0c5ms196xx")))

(define-public crate-unrust-proc-macro-0.1.5 (c (n "unrust-proc-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0drdr7dirxipcjv3l3b1glj9dql0jvkrhqxd291xlqn4mrvkr6sd")))

(define-public crate-unrust-proc-macro-0.1.6 (c (n "unrust-proc-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08287cs01h0ssm64323y6v5jg4g2kgbb49xnsqnwzpk93r5y23kk")))

