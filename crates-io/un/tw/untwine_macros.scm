(define-module (crates-io un tw untwine_macros) #:use-module (crates-io))

(define-public crate-untwine_macros-0.1.0 (c (n "untwine_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "04k5wmvnh6w8j8zs43fgvdyn5sa2mpqjskgjslsmdfr09mlz3pjd")))

(define-public crate-untwine_macros-0.2.0 (c (n "untwine_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "0s3zhavai0l910jhlh84hfhpvvgg7hgcz2v56r8q4adb2gs4vlrx")))

(define-public crate-untwine_macros-0.3.2 (c (n "untwine_macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "1z59qxb76jrg9l9mg9a1i1038bgvyf0by4i21l3gqkfrydbsm09z")))

(define-public crate-untwine_macros-0.3.3 (c (n "untwine_macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "02gp0q93wsh4cngaaffx9s5ymbim5bx1860n5h55ah0h9dvjylrr")))

(define-public crate-untwine_macros-0.4.0 (c (n "untwine_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "06cyrs0cz7lbz5c79pziw9dz5cr8sfb15dknzp2ip7ldvdxfmmpw")))

(define-public crate-untwine_macros-0.4.1 (c (n "untwine_macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "1ffdz2by1q9bfhdghhjhh09dzim41r1bzfgca2mvfl2jpmklyawn")))

(define-public crate-untwine_macros-0.5.0 (c (n "untwine_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "0nw4qqfj0sp9lnpdgp7lvl4v1z2psifvn0cix9ddchr8wzd3f4cy")))

(define-public crate-untwine_macros-0.5.1 (c (n "untwine_macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "0dcczzby5kxg46b9kcc6iayjch1n98hk957xfac02yq5rl51jrq9")))

(define-public crate-untwine_macros-0.6.0 (c (n "untwine_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "1xi52g9lvgm5ql1b551hxrbdzsawxckkr6grahv5hyicavx382in")))

(define-public crate-untwine_macros-0.6.1 (c (n "untwine_macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "0jdh69akqk22ar7s25c5z0j9pi0hqagq8l79slpk3lnfc1mn73fs")))

(define-public crate-untwine_macros-0.7.0 (c (n "untwine_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "0f2cznmvg0hyq4p7hdd3qk76q3f8rfd94gwjdw9xd30dfrm8m5pp")))

(define-public crate-untwine_macros-0.8.0 (c (n "untwine_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("visit" "full" "extra-traits"))) (d #t) (k 0)))) (h "0mrpqq48d8gx80dq5k6d63n3fqcnkx9mcsbwj1x87clbp4fg4mak")))

