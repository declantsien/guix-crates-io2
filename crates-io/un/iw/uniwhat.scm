(define-module (crates-io un iw uniwhat) #:use-module (crates-io))

(define-public crate-uniwhat-0.1.0 (c (n "uniwhat") (v "0.1.0") (d (list (d (n "unic-ucd-name") (r "^0.9.0") (d #t) (k 0)))) (h "0lnp0rk8q0l1s4n5gy49cajafg3xm664xg4ixzsamyj1gfv046g3")))

(define-public crate-uniwhat-0.1.1-rc1 (c (n "uniwhat") (v "0.1.1-rc1") (d (list (d (n "unic-ucd-name") (r "^0.9.0") (d #t) (k 0)))) (h "15pd9fngn99ip7cszdyypgay11vq32fx1ifpgas84xnsj7d5h7di")))

(define-public crate-uniwhat-0.1.1 (c (n "uniwhat") (v "0.1.1") (d (list (d (n "unic-ucd-name") (r "^0.9.0") (d #t) (k 0)))) (h "1h2zlfia4s6yi51ccvw70cfb97w6582aa03r5xzpkbyy79r6al4n")))

(define-public crate-uniwhat-0.2.0 (c (n "uniwhat") (v "0.2.0") (d (list (d (n "unic-ucd-name") (r "^0.9.0") (d #t) (k 0)))) (h "0qd0gwhww057mif8i9nkq6kafqq6s54yikq024l1hcnvndi6b53i")))

