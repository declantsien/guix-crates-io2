(define-module (crates-io un ta untanglr) #:use-module (crates-io))

(define-public crate-untanglr-0.3.0 (c (n "untanglr") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0r45662qi0iy25anzfspw4why458mhrjwsxl2j6im4r3s2mxg1m5") (y #t)))

(define-public crate-untanglr-0.4.0 (c (n "untanglr") (v "0.4.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1hyjmppw5k7k8y63mcpi2p5xsc04m5braqkmnn3g1s82pbw8mf8s") (y #t)))

(define-public crate-untanglr-0.4.1 (c (n "untanglr") (v "0.4.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1qd2wmcx9gbyg4xmnfa4f26bnjkb26nln8yd9n0hb4rl9d0baa8s") (y #t)))

(define-public crate-untanglr-0.5.0 (c (n "untanglr") (v "0.5.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0lbhq2ykrx4pfwih36scpjz972rsk8llhhqqscq0ax9cjxr6vhhf") (y #t)))

(define-public crate-untanglr-0.6.0 (c (n "untanglr") (v "0.6.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0mpjh63hivshzy7m9kgf4gfksinmzxmr2222m7nhzmi8bhi31zzi") (y #t)))

(define-public crate-untanglr-1.0.0 (c (n "untanglr") (v "1.0.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "058nrbabh32bgqyyi8992y8x8crahdw124vqh02lz8fhm2bwvzkx")))

(define-public crate-untanglr-1.1.0 (c (n "untanglr") (v "1.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0ap4hx2ka2pzy3gfxzwfj7ychqq8953fx2x2k9s6rvplzhz92lfq")))

