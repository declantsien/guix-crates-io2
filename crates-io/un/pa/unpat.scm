(define-module (crates-io un pa unpat) #:use-module (crates-io))

(define-public crate-unpat-0.1.0 (c (n "unpat") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10va8d5bwbc0pyrjihviqrvm5dklrj3cbdc4db5yrx16xj3lvbsz")))

(define-public crate-unpat-0.1.1 (c (n "unpat") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12vqahngzj19cl8l7qb3pn9smaqs1fmnnpyz252s2h4nvr2227k1")))

(define-public crate-unpat-0.1.2 (c (n "unpat") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d12ad6s8dfqkdzhvmazkfy6prfqrwp37apyd8vcmgig81gkx351")))

(define-public crate-unpat-0.1.3 (c (n "unpat") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08r3wgsvjl1fa6mi0w9361d076jbp0a6wkk4a4vndv8w1wcyz5p9")))

