(define-module (crates-io un _a un_algebra) #:use-module (crates-io))

(define-public crate-un_algebra-0.1.0 (c (n "un_algebra") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.3") (d #t) (k 0)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)))) (h "15008nzg23is15332icjg8na2jfcj2fiaqp2xmrdam7x4r1zadny")))

(define-public crate-un_algebra-0.1.3 (c (n "un_algebra") (v "0.1.3") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.4") (d #t) (k 0)) (d (n "proptest") (r "^0.8.4") (d #t) (k 2)))) (h "15gx0rd278s0kfg2di158a8l2w8hx9cq20808kdkjij4dhqfwf3z")))

(define-public crate-un_algebra-0.1.4 (c (n "un_algebra") (v "0.1.4") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.4") (d #t) (k 0)) (d (n "proptest") (r "^0.8.4") (d #t) (k 2)))) (h "07sghb74hsgx8i6bi16nb0fw40gp55h7zzk5bqkkp652f70l2gvc")))

(define-public crate-un_algebra-0.1.5 (c (n "un_algebra") (v "0.1.5") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "15alzmrrkckcr41icp78sfd0v3qwrhw5hpbfkfq7g3ijpxrkg46j")))

(define-public crate-un_algebra-0.1.6 (c (n "un_algebra") (v "0.1.6") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 0)))) (h "1aq4054zaxijw8mhkyfaf35mrvm3qvg6n3iil5wz09v78kskbq7f")))

(define-public crate-un_algebra-0.1.7 (c (n "un_algebra") (v "0.1.7") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 0)))) (h "00wgrjlh3q98lw4zr95s8s62fnh5jcyd134ldkqgix30q8hrnz1k")))

(define-public crate-un_algebra-0.2.0 (c (n "un_algebra") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 0)))) (h "0pw6fci3p4nncq2ld8hd9n8cqbr6j2ylq48n1bnpvdp2zf9yqw5v")))

(define-public crate-un_algebra-0.2.1 (c (n "un_algebra") (v "0.2.1") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 0)))) (h "10rgb395g7mmmkaflr371wf6vpjjip3rhlwm6ad5q0hwzpdsx08x")))

(define-public crate-un_algebra-0.2.2 (c (n "un_algebra") (v "0.2.2") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 0)))) (h "0k7p56qlpppnnqgcy6b6pvhlgc64l89ldpbch0qw5i1rk7dgcq03")))

(define-public crate-un_algebra-0.3.0 (c (n "un_algebra") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 0)))) (h "03wfwy7g183h6xvc2phbv830kdyvl5alna05h287lni1i1alj7i7")))

(define-public crate-un_algebra-0.4.0 (c (n "un_algebra") (v "0.4.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 0)))) (h "0dshhvgwkl41hxsn2ni4h2vddyjpbpvz7kyw4kxxydjd3lix1931")))

(define-public crate-un_algebra-0.5.0 (c (n "un_algebra") (v "0.5.0") (d (list (d (n "num") (r "0.2.*") (d #t) (k 0)) (d (n "proptest") (r "0.9.*") (d #t) (k 0)) (d (n "proptest-derive") (r "0.1.*") (d #t) (k 0)))) (h "1lgvn25nkljyc6md27jxsjbbcvflj3h4afwl6p7vhazx86wkvijc")))

(define-public crate-un_algebra-0.6.0 (c (n "un_algebra") (v "0.6.0") (d (list (d (n "num") (r "0.2.*") (d #t) (k 0)) (d (n "proptest") (r "0.9.*") (d #t) (k 0)) (d (n "proptest-derive") (r "0.1.*") (d #t) (k 0)))) (h "14bqvkg1hj7xc1ll8h25wcvjli757zx9amw8qqhy730y45dck588")))

(define-public crate-un_algebra-0.7.0 (c (n "un_algebra") (v "0.7.0") (d (list (d (n "num") (r "0.2.*") (d #t) (k 0)) (d (n "proptest") (r "0.9.*") (d #t) (k 0)) (d (n "proptest-derive") (r "0.1.*") (d #t) (k 0)))) (h "0ccfah6xck9fvq04l8m7l89wi87rq7kl88ay8ivrfifkgfdczlxx")))

(define-public crate-un_algebra-0.8.0 (c (n "un_algebra") (v "0.8.0") (d (list (d (n "num") (r "0.2.*") (d #t) (k 0)) (d (n "proptest") (r "0.9.*") (d #t) (k 0)) (d (n "proptest-derive") (r "0.1.*") (d #t) (k 0)))) (h "12k771wvb2kp11gyf2m36imhpjypninhwlbx79pxrwp2p3ki2v7b")))

(define-public crate-un_algebra-0.9.0 (c (n "un_algebra") (v "0.9.0") (d (list (d (n "num") (r "0.2.*") (d #t) (k 0)) (d (n "proptest") (r "0.9.*") (d #t) (k 0)) (d (n "proptest-derive") (r "0.1.*") (d #t) (k 0)))) (h "0n04498wclb339f0czm9rljdnillk5b1gz8kcjnpl0qa90xdcirr")))

