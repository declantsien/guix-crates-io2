(define-module (crates-io un cr uncrx-rs) #:use-module (crates-io))

(define-public crate-uncrx-rs-0.1.0 (c (n "uncrx-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "11lx4qbc6l7bwp8z8j7njv15g0ilgpynvnsdf96kxfzz3gayn6m0")))

(define-public crate-uncrx-rs-0.1.1 (c (n "uncrx-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0m7m8v8rk2yz86mc1qz04l692s9n5izcx7da1qyzi7mii48h9zkz")))

(define-public crate-uncrx-rs-0.1.2 (c (n "uncrx-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "116iac67ziy5j67w4srzflslywmiah53lcks3s4pisfac7r44fmd")))

(define-public crate-uncrx-rs-0.2.0 (c (n "uncrx-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "087j3x3j3jpbskhvfdj7l2vss552ygh1gxls5676l11xsqb2a6ic")))

(define-public crate-uncrx-rs-0.2.2 (c (n "uncrx-rs") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r0sb71ysc0gpsw1gg95x2z48vfvpcp6l4bdzvm9289jzvbxr3b9")))

