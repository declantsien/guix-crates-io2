(define-module (crates-io un li unlink) #:use-module (crates-io))

(define-public crate-unlink-0.0.0-alpha0.0 (c (n "unlink") (v "0.0.0-alpha0.0") (h "17y4kq6jli9qmb6a8d2pavkix69myd2cg2qz495q71pd2b8hq550")))

(define-public crate-unlink-0.0.0-pre-release1.0 (c (n "unlink") (v "0.0.0-pre-release1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "haphazard") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0492m839hqxfk25vidbk8xvg9cp1fqd91rhzz40b9inln2ji9a39")))

(define-public crate-unlink-0.0.0-pre-release2.0 (c (n "unlink") (v "0.0.0-pre-release2.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "haphazard") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0626bgzpmmhkyf4n7nnrpf0a49yhyaf9v80djs4xa5qhr054z29i")))

(define-public crate-unlink-0.0.0-pre-release3.0 (c (n "unlink") (v "0.0.0-pre-release3.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "haphazard") (r "^0.1") (d #t) (k 0)) (d (n "lockfree") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0izx7cq392f5mimvybvwp49b3v8w7fhj1ay5x17j0hj1r35dw1j5")))

(define-public crate-unlink-0.0.0-pre-release3.1 (c (n "unlink") (v "0.0.0-pre-release3.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "haphazard") (r "^0.1") (d #t) (k 0)) (d (n "lockfree") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1nqqd3wgsa6gp2l5mwi1s47c92l3vavh5lj6y4imz9qxxp2svigb")))

