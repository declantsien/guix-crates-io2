(define-module (crates-io un li unlicense) #:use-module (crates-io))

(define-public crate-unlicense-0.1.0 (c (n "unlicense") (v "0.1.0") (h "1c1a20qfkg4r1x4qkl04260377xflkib4l6p7psygv61vfb9bzl1")))

(define-public crate-unlicense-0.2.0 (c (n "unlicense") (v "0.2.0") (h "1d7mm38n6an3kbr77bx0mfm3ba3pkall3zdfl570cjda5k5kbjrz")))

