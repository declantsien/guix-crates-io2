(define-module (crates-io un si unsized-stack) #:use-module (crates-io))

(define-public crate-unsized-stack-0.1.0 (c (n "unsized-stack") (v "0.1.0") (h "0114g9y4likyg1mvnjk7jgmdbgryqqz2i8mn3y312q9p0gj3bzbr") (y #t)))

(define-public crate-unsized-stack-0.1.1 (c (n "unsized-stack") (v "0.1.1") (d (list (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "1gwqak5l3p8snyc45hvpcflavnybfv7jdsl867kpi5hdwsdpxwnx") (y #t)))

(define-public crate-unsized-stack-0.1.2 (c (n "unsized-stack") (v "0.1.2") (d (list (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "1zwbwwhrl1vaqqxj3rmc29gycr1wvc2i6pjgab4g7r6qc7iq071z") (y #t)))

(define-public crate-unsized-stack-0.1.3 (c (n "unsized-stack") (v "0.1.3") (d (list (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "0aq6g5wymdv4x8qvzk03chry5a5gh48ssslp3wz8yr71ba2kz18j") (y #t)))

(define-public crate-unsized-stack-0.2.0 (c (n "unsized-stack") (v "0.2.0") (d (list (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "1q3a0lrd37r6j1jdrjff58fixqmgin2s976syn1fsl75lmpf0c1r")))

