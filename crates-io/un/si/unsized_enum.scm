(define-module (crates-io un si unsized_enum) #:use-module (crates-io))

(define-public crate-unsized_enum-0.0.1 (c (n "unsized_enum") (v "0.0.1") (h "1hsh0i8ywiyrp3sq4lq63aajv6jy131915cfgw9yhxq1mz60qciv")))

(define-public crate-unsized_enum-0.0.2 (c (n "unsized_enum") (v "0.0.2") (h "1yrxzm3ivqz6g2kv5hkknnn3dymmri9vx1ssssr0glc79d6nw73i")))

