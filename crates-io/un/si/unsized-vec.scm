(define-module (crates-io un si unsized-vec) #:use-module (crates-io))

(define-public crate-unsized-vec-0.0.1-alpha.1 (c (n "unsized-vec") (v "0.0.1-alpha.1") (h "1ka35w2pnwwyr52hn2jhrfgmqhkybnhk9sn1xgf0115n5dk7a16r") (y #t)))

(define-public crate-unsized-vec-0.0.1-alpha.2 (c (n "unsized-vec") (v "0.0.1-alpha.2") (h "02hlc4sm3s4dg8rx5cprwzflpck9ijk9yhkdr077vprf26ny8qy9") (y #t)))

(define-public crate-unsized-vec-0.0.1-alpha.3 (c (n "unsized-vec") (v "0.0.1-alpha.3") (h "0namvc8mmq7f1pzy1nhxaadzsxiys4dhkc6lysl84hg5f9ppj5jp") (y #t)))

(define-public crate-unsized-vec-0.0.1-alpha.4 (c (n "unsized-vec") (v "0.0.1-alpha.4") (h "13czngdh9cj6h448qgd8mx8arb010by3s140psrmm56zg61ihfid") (y #t)))

(define-public crate-unsized-vec-0.0.1-alpha.5 (c (n "unsized-vec") (v "0.0.1-alpha.5") (d (list (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "046iay67100av8gbk2prfli3zfnqajrgj2x8fzh2dw9faqcz7mj4") (y #t)))

(define-public crate-unsized-vec-0.0.1-alpha.6 (c (n "unsized-vec") (v "0.0.1-alpha.6") (d (list (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "00yhlp28akc28dhzgm5f3k1s42bycvpahz235b7ld16ff78d2r37") (y #t)))

(define-public crate-unsized-vec-0.0.1-alpha.7 (c (n "unsized-vec") (v "0.0.1-alpha.7") (d (list (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "02fzr01bqnd5jpcz34459i6c5qcpj1w1qjazmwaqb0b5p2l25zna") (y #t)))

(define-public crate-unsized-vec-0.0.1-alpha.8 (c (n "unsized-vec") (v "0.0.1-alpha.8") (d (list (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "14pn17prfaxqw0grqsm56cmzpc0naqslfrarz58l0nv1v47b2g9a") (y #t)))

(define-public crate-unsized-vec-0.0.1-alpha.9 (c (n "unsized-vec") (v "0.0.1-alpha.9") (d (list (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "0hx8s36k1pcczs2aqpagw643m49ngvcr1cra4pilr34pdnv0x64h") (y #t)))

(define-public crate-unsized-vec-0.0.2-alpha.1 (c (n "unsized-vec") (v "0.0.2-alpha.1") (d (list (d (n "emplacable") (r "^0.1.0-alpha.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (k 0)))) (h "0lv9nmwdcj8hkdarjjf0djzxsm6xh3vgyffbir21w1pi5mrc1c4k") (y #t)))

(define-public crate-unsized-vec-0.0.2-alpha.2 (c (n "unsized-vec") (v "0.0.2-alpha.2") (d (list (d (n "emplacable") (r "^0.1.0-alpha.2") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (k 0)))) (h "0a3pxl6kd2q2ldb8byvv4497yw8ak44i3bh6l0v8d9a1k6gjngqr") (y #t)))

(define-public crate-unsized-vec-0.0.2-alpha.3 (c (n "unsized-vec") (v "0.0.2-alpha.3") (d (list (d (n "emplacable") (r "^0.1.0-alpha.3") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (k 0)))) (h "1z3bqcz94p34m46j59qdbx49iac7h50y8bmgm48iy9zcd9xj6l18") (y #t)))

(define-public crate-unsized-vec-0.0.2-alpha.4 (c (n "unsized-vec") (v "0.0.2-alpha.4") (d (list (d (n "emplacable") (r "^0.1.0-alpha.4") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (k 0)))) (h "175acag8qccjlzjd3xl223gjaw418p1z83i60rszv037i5rhkf12") (y #t)))

(define-public crate-unsized-vec-0.0.2-alpha.5 (c (n "unsized-vec") (v "0.0.2-alpha.5") (d (list (d (n "emplacable") (r "^0.1.0-alpha.5") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (k 0)))) (h "014mhwlliazgv3clarj2xkmpwd5pqzznm920kkgc7jyj0fydvwxq") (y #t)))

(define-public crate-unsized-vec-0.0.2-alpha.6 (c (n "unsized-vec") (v "0.0.2-alpha.6") (d (list (d (n "emplacable") (r "^0.1.0-alpha.6") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.158") (o #t) (k 0)))) (h "0rvvys60q5yb9342kcmgwl98pmz70wqjyrmsqc84a7cy03r3shhb") (y #t)))

(define-public crate-unsized-vec-0.0.2-alpha.7 (c (n "unsized-vec") (v "0.0.2-alpha.7") (d (list (d (n "emplacable") (r "^0.1.0-alpha.6") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.158") (o #t) (k 0)))) (h "0x63vnpz5ky76s80yxdx0lac01da32k19xccygxkawqjar5q6v3m") (y #t)))

(define-public crate-unsized-vec-0.0.2-alpha.8 (c (n "unsized-vec") (v "0.0.2-alpha.8") (d (list (d (n "emplacable") (r "^0.1.0-alpha.6") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.192") (o #t) (k 0)))) (h "1747c4lm1arnsq67mr04pg7n17mpgdi4rgjhlqkigdf6xrb1jir5") (y #t)))

(define-public crate-unsized-vec-0.0.2-alpha.9 (c (n "unsized-vec") (v "0.0.2-alpha.9") (d (list (d (n "emplacable") (r "^0.1.0-alpha.6") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (k 0)))) (h "0w2rkkj5a5v2c8g5910hlaly2d5kl6bmj674pv2s8db6kxj3ljfg")))

(define-public crate-unsized-vec-0.0.2-alpha.10 (c (n "unsized-vec") (v "0.0.2-alpha.10") (d (list (d (n "emplacable") (r "^0.1.0-alpha.6") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (k 0)))) (h "1paqqam8iz2k2r37309d6c3sgsfxs23m2d3z4nhfn93xn63xigwi")))

