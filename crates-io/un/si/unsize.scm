(define-module (crates-io un si unsize) #:use-module (crates-io))

(define-public crate-unsize-1.0.0-beta (c (n "unsize") (v "1.0.0-beta") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)))) (h "1igs3yifdd74p3cyh2zhw7qsj673iv86v5kasya9fx0ylxhakwch")))

(define-public crate-unsize-1.0.0-beta.1 (c (n "unsize") (v "1.0.0-beta.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)))) (h "0q4dabl6a3xd1pd189by894m8nkwmd8y7fp9hqi4d3nnfplwz9d7")))

(define-public crate-unsize-1.0.0 (c (n "unsize") (v "1.0.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)))) (h "0xygrji0402cvlbxbcipi5wydgkm7ndv8nqag0jzglmzhrn1vkmi")))

(define-public crate-unsize-1.1.0 (c (n "unsize") (v "1.1.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)))) (h "0fd9lzdhkahygxy9b348m0fs4wlldh5ymp1dcr56d9f16jksg9sg")))

