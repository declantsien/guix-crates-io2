(define-module (crates-io un si unsigned-f64) #:use-module (crates-io))

(define-public crate-unsigned-f64-0.1.0 (c (n "unsigned-f64") (v "0.1.0") (h "0z5kry5r201xqh8jq25xi3vv9ahbg8fqwvsiqz023d3qj5rj2r17")))

(define-public crate-unsigned-f64-0.2.0 (c (n "unsigned-f64") (v "0.2.0") (h "1dldczy09a20ngrxfgrzb7lk0vfndfzv405iqadwkng26qn87fb7")))

