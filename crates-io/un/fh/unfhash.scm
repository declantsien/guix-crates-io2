(define-module (crates-io un fh unfhash) #:use-module (crates-io))

(define-public crate-unfhash-0.1.0 (c (n "unfhash") (v "0.1.0") (d (list (d (n "arrow") (r "^5.3") (f (quote ("csv"))) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "0l98rqgw5fi1x0ypk2v9vpnc5sj3n77n4pfvmlmvpk4dchl0qp3s")))

