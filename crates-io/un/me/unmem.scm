(define-module (crates-io un me unmem) #:use-module (crates-io))

(define-public crate-unmem-0.1.0 (c (n "unmem") (v "0.1.0") (h "16zc53ava4s113x7amhg39jmcq7s98rkhw4xfl1qvmc86kyk0jpc") (y #t)))

(define-public crate-unmem-0.2.0 (c (n "unmem") (v "0.2.0") (h "1n58fyw3xlp3ls4qickfcg3c0s6kmv3zas890vd0zkz25mk5scfa") (y #t)))

(define-public crate-unmem-0.2.1 (c (n "unmem") (v "0.2.1") (h "1wyfzr7mwkqr5sszcn5xbwzq4nlmljxfqyvj409ascb26ypxvi2k") (y #t)))

(define-public crate-unmem-0.2.2 (c (n "unmem") (v "0.2.2") (h "1a6kyp1yy2hxhvk5ibhwgncacgsmxq55pg4n3sxwmmb86g6kzn4a") (y #t)))

(define-public crate-unmem-0.2.3 (c (n "unmem") (v "0.2.3") (h "1pzk3z3957b00ngk09968mxmh7vk8pwidagpgfxh9mqvpiq1d1m4") (y #t)))

(define-public crate-unmem-0.2.4 (c (n "unmem") (v "0.2.4") (h "036hnz22r549dp7b8jj82yky4hgyz9l3p9dfx4nvhvr4qdnjz1pm") (y #t)))

(define-public crate-unmem-0.2.5 (c (n "unmem") (v "0.2.5") (h "04bv07963gdv24xkb0ki0z1lx6s4qkwdnrl7wbbz1qa77kf8cr4c") (y #t)))

(define-public crate-unmem-0.2.6 (c (n "unmem") (v "0.2.6") (h "0r1n550358vjlv6i4qj8najf6bfyvrjy763zhgv7v36x32av8g2x") (y #t)))

