(define-module (crates-io un me unmemftp) #:use-module (crates-io))

(define-public crate-unmemftp-0.1.0 (c (n "unmemftp") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.31") (d #t) (k 0)) (d (n "async_ftp") (r "^4.0.1") (d #t) (k 2)) (d (n "libunftp") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 0)))) (h "05axn7mjfp5k6nqlfdjp9ghrzd3nr49hwmhb5hnwbmg4dnh98jqg")))

