(define-module (crates-io un wa unwalk-gz) #:use-module (crates-io))

(define-public crate-unwalk-gz-0.2.0-beta0 (c (n "unwalk-gz") (v "0.2.0-beta0") (d (list (d (n "file") (r "^1.1.1") (d #t) (k 0)) (d (n "filebuffer") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "tempfile") (r "= 3.0.0") (d #t) (k 2)) (d (n "unwalk-base") (r "^0.2.0-beta0") (d #t) (k 0)))) (h "0v5r7m27czgfdi4k881bpwbfj1gjb78lv3c8isjflqaiy0bk4wrr")))

(define-public crate-unwalk-gz-0.2.0-beta1 (c (n "unwalk-gz") (v "0.2.0-beta1") (d (list (d (n "file") (r "^1.1.1") (d #t) (k 0)) (d (n "filebuffer") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "tempfile") (r "= 3.0.0") (d #t) (k 2)) (d (n "unwalk-base") (r "^0.2.0-beta1") (d #t) (k 0)))) (h "1w54lakjllba7j52grzhj0sqmknpv4w1sb2qp80xbdmrbhmmv2cf")))

