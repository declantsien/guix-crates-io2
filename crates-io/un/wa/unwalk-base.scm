(define-module (crates-io un wa unwalk-base) #:use-module (crates-io))

(define-public crate-unwalk-base-0.2.0-beta0 (c (n "unwalk-base") (v "0.2.0-beta0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "18a4hnh3m1bkdi2z2ppa3g8cb4013zmpz3sr6d7x4lr6cm85p6n8")))

(define-public crate-unwalk-base-0.2.0-beta1 (c (n "unwalk-base") (v "0.2.0-beta1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "072hms66gnv9n0vkf2hk1g27fdaxp9kl5v5m1navbbfrqqbji8mq")))

