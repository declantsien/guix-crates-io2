(define-module (crates-io un du undulate) #:use-module (crates-io))

(define-public crate-undulate-0.1.0 (c (n "undulate") (v "0.1.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0psp1idgknq0cfdkigix3yr1s3sbza9p1jj701fpa5xkwsdgsjgw")))

