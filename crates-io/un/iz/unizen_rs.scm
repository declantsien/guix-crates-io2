(define-module (crates-io un iz unizen_rs) #:use-module (crates-io))

(define-public crate-unizen_rs-0.1.0 (c (n "unizen_rs") (v "0.1.0") (h "0va04smd317qld0pzarcff2pwm7bqj54w3filjsd302zbkqprc2l")))

(define-public crate-unizen_rs-0.1.3 (c (n "unizen_rs") (v "0.1.3") (h "08d0a40i8qqrlqh7i27dah9ki4x0fs90dr4hr3hq507fb6h3q92d")))

