(define-module (crates-io un sq unsquashfs-wrapper) #:use-module (crates-io))

(define-public crate-unsquashfs-wrapper-0.1.0 (c (n "unsquashfs-wrapper") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "14ydcqn7iz6h1hfgmb3sy72hwav4ypadb01px6icv8n8qms8b9jk")))

(define-public crate-unsquashfs-wrapper-0.1.1 (c (n "unsquashfs-wrapper") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1q7kbfn76k3rbw3rr5pg9cby4jb12hk1kd4nxzkmgskiv55v6724")))

(define-public crate-unsquashfs-wrapper-0.2.0 (c (n "unsquashfs-wrapper") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("process"))) (d #t) (k 0)) (d (n "which") (r "^6.0") (d #t) (k 0)))) (h "1ffr4nggipmfaz4xlizhr7g9n49n8m1j9rmyxzk7davfag7jpiy0")))

(define-public crate-unsquashfs-wrapper-0.2.1 (c (n "unsquashfs-wrapper") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("process"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "which") (r "^6.0") (d #t) (k 0)))) (h "03d0pzrim0jhck0x99zcjmggfych0h6cw98cdpfgbrzhxzzgbhjw")))

