(define-module (crates-io hc _m hc_merklicious_sdk) #:use-module (crates-io))

(define-public crate-hc_merklicious_sdk-0.1.0 (c (n "hc_merklicious_sdk") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "rmpv") (r "=1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "rs_merkle") (r "^1.4.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "=0.2.0") (d #t) (k 0)))) (h "05w1n4f22pggxmjz2mf61pl6s2y05gq1arkfk5bjkj1iigy4hn4d")))

