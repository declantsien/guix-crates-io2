(define-module (crates-io hc _i hc_iz_profile) #:use-module (crates-io))

(define-public crate-hc_iz_profile-0.1.0-beta-rc.3 (c (n "hc_iz_profile") (v "0.1.0-beta-rc.3") (d (list (d (n "hdi") (r "=0.2.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ix7rknxsff9346l6xz991agdv7dnb4wwmjffiik7d6rflvz0cc9")))

(define-public crate-hc_iz_profile-0.1.0 (c (n "hc_iz_profile") (v "0.1.0") (d (list (d (n "hdi") (r "=0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0fl85r1wxb5q0nnjgdv94va1khpmvmgppazdlk0fw1sm8rh2xqc8")))

(define-public crate-hc_iz_profile-0.1.1 (c (n "hc_iz_profile") (v "0.1.1") (d (list (d (n "hdi") (r "=0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02dgmlcvlgfiwbgyb667izrafh4k1r7aq9j90bg6aa2s75ypbc20")))

(define-public crate-hc_iz_profile-0.1.2 (c (n "hc_iz_profile") (v "0.1.2") (d (list (d (n "hdi") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "16czfb2rvf2cf5kwhfpmc2afjf01x7w7qd9hy1i9h7b707j75dn5")))

(define-public crate-hc_iz_profile-0.1.2-alpha.0 (c (n "hc_iz_profile") (v "0.1.2-alpha.0") (d (list (d (n "hdi") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1hd9vxnhaj8sk4xbhk8p11pmbyvdcknhkv3im8wmc6ikxakvczi6")))

(define-public crate-hc_iz_profile-0.1.2-alpha.1 (c (n "hc_iz_profile") (v "0.1.2-alpha.1") (d (list (d (n "hdi") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ja7acvgzi5ahxdrlqcjfjn71adxgcj2c2rlvwvd25x4y7sjsd1d")))

(define-public crate-hc_iz_profile-0.2.0 (c (n "hc_iz_profile") (v "0.2.0") (d (list (d (n "hdi") (r "=0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "051ywwnm0cklqljbrv1kr7iylblllmiwlr7cjy74xpb39sppf2jj")))

(define-public crate-hc_iz_profile-0.2.1-beta-dev.0 (c (n "hc_iz_profile") (v "0.2.1-beta-dev.0") (d (list (d (n "hdi") (r "=0.3.1-beta-dev.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "15p8rz7dqd0lyjfi86bk7pfx9mbmimmhyivjjh5g5pp1pqg97m92")))

(define-public crate-hc_iz_profile-0.3.0-beta-dev.5 (c (n "hc_iz_profile") (v "0.3.0-beta-dev.5") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1i86nfng1c18hmnz1zy2sypm1ww8gkjcba9rg87k1q2vnpw7x983")))

(define-public crate-hc_iz_profile-0.2.1 (c (n "hc_iz_profile") (v "0.2.1") (d (list (d (n "hdi") (r "=0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "15s9z3p4mm2iv3zwi5pn3nki2rwrx6g4f7inkxf2ljl23k2y2baw")))

(define-public crate-hc_iz_profile-0.2.2 (c (n "hc_iz_profile") (v "0.2.2") (d (list (d (n "hdi") (r "=0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1mm0vzbhbsp53a9q47wrh81kniqsfkgf6lyqdby5qhbqdyvj2acv")))

(define-public crate-hc_iz_profile-0.2.3 (c (n "hc_iz_profile") (v "0.2.3") (d (list (d (n "hdi") (r "=0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0dgy6asln4xhjp00kh32nmjsbjrn42c76fx2v61k2gpwk4g08y8l")))

(define-public crate-hc_iz_profile-0.2.4-rc.0 (c (n "hc_iz_profile") (v "0.2.4-rc.0") (d (list (d (n "hdi") (r "=0.3.4-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0v6m7n4mdlj8j7zjynka39b2xqrgyh9lk790wv8nxng5sg7qn4dv")))

(define-public crate-hc_iz_profile-0.2.5-rc.0 (c (n "hc_iz_profile") (v "0.2.5-rc.0") (d (list (d (n "hdi") (r "=0.3.5-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0r0cmdbvrxs5lby0hccmv433mv735sbj55n6fz24mn35iw68vzd7")))

(define-public crate-hc_iz_profile-0.3.0-beta-dev.35 (c (n "hc_iz_profile") (v "0.3.0-beta-dev.35") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1a56fgv006w7l8d5xsbvrfqcl6p82ph7qa6s3m5dqgpb5p4cvkkf")))

(define-public crate-hc_iz_profile-0.3.0-beta-dev.35-hotfix.1 (c (n "hc_iz_profile") (v "0.3.0-beta-dev.35-hotfix.1") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0q83rmvywwp5ns98szm9fwk937r5y0sl3iiawh72fcmh1hj342cm")))

(define-public crate-hc_iz_profile-0.3.0-beta-dev.40 (c (n "hc_iz_profile") (v "0.3.0-beta-dev.40") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1hp19ygcxjbn4n9jbq1z542bjq040wg8cpc1glfwiri3gdq0bgp3")))

(define-public crate-hc_iz_profile-0.4.0-dev.2 (c (n "hc_iz_profile") (v "0.4.0-dev.2") (d (list (d (n "hdi") (r "=0.5.0-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "114m95ahnfyzvrrvm8qs7avx7xhg760a6zrxz1fjbswyy65yqczk")))

(define-public crate-hc_iz_profile-0.4.0-dev.3 (c (n "hc_iz_profile") (v "0.4.0-dev.3") (d (list (d (n "hdi") (r "=0.5.0-dev.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1r99ypm1276plngvgci82fdpd31jsrsrrkn9qqrzk5byhbl87cik")))

