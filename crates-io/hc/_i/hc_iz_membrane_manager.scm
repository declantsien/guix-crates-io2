(define-module (crates-io hc _i hc_iz_membrane_manager) #:use-module (crates-io))

(define-public crate-hc_iz_membrane_manager-0.1.0-beta-rc.3 (c (n "hc_iz_membrane_manager") (v "0.1.0-beta-rc.3") (d (list (d (n "hdi") (r "=0.2.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1rcvch031ldkagrvyny9m8g18zga9h7vsvknhs7nsa683fgwfw6h")))

(define-public crate-hc_iz_membrane_manager-0.1.0 (c (n "hc_iz_membrane_manager") (v "0.1.0") (d (list (d (n "hdi") (r "=0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0s9bly79qm871kapfs6qv8v16bd7nw8wyizy99z89r2fdg93vhhh")))

(define-public crate-hc_iz_membrane_manager-0.1.1 (c (n "hc_iz_membrane_manager") (v "0.1.1") (d (list (d (n "hdi") (r "=0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1454dx3jjmb807iwz20qbhmfz3sznqx14qjq4y11js2bcyf64zbl")))

(define-public crate-hc_iz_membrane_manager-0.1.2 (c (n "hc_iz_membrane_manager") (v "0.1.2") (d (list (d (n "hdi") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0h4lmskcljwg58hdd1s8gf7ah61br5pz942n638y7nbiqkjnydv3")))

(define-public crate-hc_iz_membrane_manager-0.1.2-alpha.0 (c (n "hc_iz_membrane_manager") (v "0.1.2-alpha.0") (d (list (d (n "hdi") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1qnq3w7vzp9bb5qm2igd2vmj88kvmyvpcpr9pq2db8sa1hj82g4f")))

(define-public crate-hc_iz_membrane_manager-0.1.2-alpha.1 (c (n "hc_iz_membrane_manager") (v "0.1.2-alpha.1") (d (list (d (n "hdi") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ij6nq52r6ca2f7djlj8vqrgc1vwq0wpgflhfmphc8b7rh0h0kdx")))

(define-public crate-hc_iz_membrane_manager-0.2.0 (c (n "hc_iz_membrane_manager") (v "0.2.0") (d (list (d (n "hdi") (r "=0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1i2s33hyw803n4fn1srli3642pn3s3d6c1l7kivfvikl8zh18h8z")))

(define-public crate-hc_iz_membrane_manager-0.2.1-beta-dev.0 (c (n "hc_iz_membrane_manager") (v "0.2.1-beta-dev.0") (d (list (d (n "hdi") (r "=0.3.1-beta-dev.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10j1yvrfww1d0ccq65c6mr5y7k286l13a6cv055gq2r7ai877brs")))

(define-public crate-hc_iz_membrane_manager-0.3.0-beta-dev.5 (c (n "hc_iz_membrane_manager") (v "0.3.0-beta-dev.5") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02n6ggsabk7fxmq65n8kw2lj9s0q88a8v63ip61nk890di4z5zbv")))

(define-public crate-hc_iz_membrane_manager-0.2.1 (c (n "hc_iz_membrane_manager") (v "0.2.1") (d (list (d (n "hdi") (r "=0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1dxbqhnf878d35cqckmxmlfmc3jhi1iq52dd5n1pwd0gcmdz5rk1")))

(define-public crate-hc_iz_membrane_manager-0.2.2 (c (n "hc_iz_membrane_manager") (v "0.2.2") (d (list (d (n "hdi") (r "=0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "19yjfv1hyn0c4is4r4hxvbqimvjibb9hp6r0y423mad9wq17mv9f")))

(define-public crate-hc_iz_membrane_manager-0.2.3 (c (n "hc_iz_membrane_manager") (v "0.2.3") (d (list (d (n "hdi") (r "=0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0y1n1w3gfqssg5z3dh8awlai6vb199xkgr5kn2fgq2sfd9ndq7wj")))

(define-public crate-hc_iz_membrane_manager-0.2.4-rc.0 (c (n "hc_iz_membrane_manager") (v "0.2.4-rc.0") (d (list (d (n "hdi") (r "=0.3.4-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0b5w6dgh5c7z036bgch51adk2j3ad0r22y7j9x9ci5s8m0lkby38")))

(define-public crate-hc_iz_membrane_manager-0.2.5-rc.0 (c (n "hc_iz_membrane_manager") (v "0.2.5-rc.0") (d (list (d (n "hdi") (r "=0.3.5-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "18jigzy9gv1h8w06ninn4cv4qj6aq5ndkix2igg8c02sypzjvxf1")))

(define-public crate-hc_iz_membrane_manager-0.3.0-beta-dev.35 (c (n "hc_iz_membrane_manager") (v "0.3.0-beta-dev.35") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10cs9mxmxd78pi4fip74fy9bm83fd04pcacx524r1x9wlk161wgl")))

(define-public crate-hc_iz_membrane_manager-0.3.0-beta-dev.35-hotfix.1 (c (n "hc_iz_membrane_manager") (v "0.3.0-beta-dev.35-hotfix.1") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "07bix82qi7ch58jkidy0l8dcfbm3kcx9nq4xfhqr928szib1j5iy")))

(define-public crate-hc_iz_membrane_manager-0.3.0-beta-dev.40 (c (n "hc_iz_membrane_manager") (v "0.3.0-beta-dev.40") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1bcbq0fmm8nig809filvql8n3yk615jjib70yvls58mml9shlrlb")))

(define-public crate-hc_iz_membrane_manager-0.4.0-dev.2 (c (n "hc_iz_membrane_manager") (v "0.4.0-dev.2") (d (list (d (n "hdi") (r "=0.5.0-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0wa5iwbmfz35f26gyfnjhdixw49vyix23j6ab0lrq0a0ghnxsng0")))

(define-public crate-hc_iz_membrane_manager-0.4.0-dev.3 (c (n "hc_iz_membrane_manager") (v "0.4.0-dev.3") (d (list (d (n "hdi") (r "=0.5.0-dev.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02nfjqric543jqfn8jzcv5ngy59pkkxzpyflvl1qzpxhbr2176g8")))

