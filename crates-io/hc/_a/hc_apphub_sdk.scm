(define-module (crates-io hc _a hc_apphub_sdk) #:use-module (crates-io))

(define-public crate-hc_apphub_sdk-0.1.0 (c (n "hc_apphub_sdk") (v "0.1.0") (d (list (d (n "hc_apphub_types") (r "^0.1") (d #t) (k 0)) (d (n "hc_devhub_sdk") (r "^0.1") (d #t) (k 0)) (d (n "hc_dnahub_sdk") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1nv5g84blbd9f3cnav7hamgxlga98q9ck9b1brdqa9k9j8svgfy9")))

(define-public crate-hc_apphub_sdk-0.2.0 (c (n "hc_apphub_sdk") (v "0.2.0") (d (list (d (n "hc_apphub_types") (r "^0.2") (d #t) (k 0)) (d (n "hc_devhub_sdk") (r "^0.1") (d #t) (k 0)) (d (n "hc_dnahub_sdk") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0wa0ciqqhnaw2c5j6fd18c1rcqlk9sag06zrpf9w5xlkljdfjanm")))

