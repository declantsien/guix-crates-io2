(define-module (crates-io hc _z hc_zome_yjs_integrity) #:use-module (crates-io))

(define-public crate-hc_zome_yjs_integrity-0.0.1 (c (n "hc_zome_yjs_integrity") (v "0.0.1") (d (list (d (n "hdi") (r "=0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "028cq28nzhsdd9qnxn7vg66x8lnhzsr2cjcnkr8xiig52f4rrdr5")))

(define-public crate-hc_zome_yjs_integrity-0.0.2 (c (n "hc_zome_yjs_integrity") (v "0.0.2") (d (list (d (n "hdi") (r "=0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0m4d5b4anfngk9jlm96hwkhwi1khwhz7p37rxbpg90nrz1y3hvc1")))

(define-public crate-hc_zome_yjs_integrity-0.0.3 (c (n "hc_zome_yjs_integrity") (v "0.0.3") (d (list (d (n "hdi") (r "=0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0c9ha5ii4350hqd3v0cvp0vf20bmp0arh1aaclnhmyis8ywcd766")))

