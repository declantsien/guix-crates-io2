(define-module (crates-io hc _z hc_zome_yjs_coordinator) #:use-module (crates-io))

(define-public crate-hc_zome_yjs_coordinator-0.0.1 (c (n "hc_zome_yjs_coordinator") (v "0.0.1") (d (list (d (n "hc_zome_yjs_integrity") (r "^0.0.1") (d #t) (k 0)) (d (n "hdk") (r "=0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "143qhh9v2140p53fvwzkzc5z2j608i9q22scvkd6nl552yzk7ga6")))

(define-public crate-hc_zome_yjs_coordinator-0.0.2 (c (n "hc_zome_yjs_coordinator") (v "0.0.2") (d (list (d (n "hc_zome_yjs_integrity") (r "^0.0.2") (d #t) (k 0)) (d (n "hdk") (r "=0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "00mqijc2z6sqhay677y4pk79f7dchfbcihslrcjl2vvamz7cjiaw")))

(define-public crate-hc_zome_yjs_coordinator-0.0.3 (c (n "hc_zome_yjs_coordinator") (v "0.0.3") (d (list (d (n "hc_zome_yjs_integrity") (r "^0.0.3") (d #t) (k 0)) (d (n "hdk") (r "=0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1hyrkbg5n7dvgdw5ppjyppchn790734pk929r1rvjw8qvc9vc4xf")))

