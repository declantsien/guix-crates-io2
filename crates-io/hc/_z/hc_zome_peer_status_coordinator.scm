(define-module (crates-io hc _z hc_zome_peer_status_coordinator) #:use-module (crates-io))

(define-public crate-hc_zome_peer_status_coordinator-0.1.0 (c (n "hc_zome_peer_status_coordinator") (v "0.1.0") (d (list (d (n "hdk") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0z0ca21rijzgk5srqd7fw0bk05n99cc6nhq1nqvvdbzy0xax0va4")))

(define-public crate-hc_zome_peer_status_coordinator-0.2.0 (c (n "hc_zome_peer_status_coordinator") (v "0.2.0") (d (list (d (n "hdk") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "12jha85hzc5zhprz23jkw9liii895qyak0j3252whdg293jqqm9h")))

