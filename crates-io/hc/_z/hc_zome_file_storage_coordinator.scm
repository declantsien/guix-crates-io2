(define-module (crates-io hc _z hc_zome_file_storage_coordinator) #:use-module (crates-io))

(define-public crate-hc_zome_file_storage_coordinator-0.1.0 (c (n "hc_zome_file_storage_coordinator") (v "0.1.0") (d (list (d (n "hc_zome_file_storage_integrity") (r "^0.1.0") (d #t) (k 0)) (d (n "hdk") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1k5kx2988a0sda5z1q05h5g1d1h0qspcwm3k4sa0jsk4888pq27f")))

(define-public crate-hc_zome_file_storage_coordinator-0.2.0 (c (n "hc_zome_file_storage_coordinator") (v "0.2.0") (d (list (d (n "hc_zome_file_storage_integrity") (r "^0.2.0") (d #t) (k 0)) (d (n "hdk") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "07k3g536l65asp8092y9a88pwn79nw3hdz5ys8m8v41gm6fmf937")))

