(define-module (crates-io hc _z hc_zome_file_storage_integrity) #:use-module (crates-io))

(define-public crate-hc_zome_file_storage_integrity-0.1.0 (c (n "hc_zome_file_storage_integrity") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "hdi") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1343svifjsmnvqbk9w80j9qn4ni6a1z37wal94xjndlrgcj0fb5a") (f (quote (("externs") ("default" "externs"))))))

(define-public crate-hc_zome_file_storage_integrity-0.2.0 (c (n "hc_zome_file_storage_integrity") (v "0.2.0") (d (list (d (n "hdi") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "188606pxl11k1kz29y7jx2vjc9bc56jg3rfv5s9rnv5y0mn825ha") (f (quote (("externs") ("default" "externs"))))))

