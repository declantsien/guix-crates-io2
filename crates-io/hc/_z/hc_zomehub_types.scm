(define-module (crates-io hc _z hc_zomehub_types) #:use-module (crates-io))

(define-public crate-hc_zomehub_types-0.1.0 (c (n "hc_zomehub_types") (v "0.1.0") (d (list (d (n "mere_memory_types") (r "^0.91.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.4.2") (d #t) (k 0)))) (h "1y0x065mck03sbsb7zdyxnhqjlmc85cqqs1lf4adw47wh7h9ys1q")))

