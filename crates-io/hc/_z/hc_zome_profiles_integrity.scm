(define-module (crates-io hc _z hc_zome_profiles_integrity) #:use-module (crates-io))

(define-public crate-hc_zome_profiles_integrity-0.0.1 (c (n "hc_zome_profiles_integrity") (v "0.0.1") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "hdi") (r "^0.2.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1l9zl5cnvjdx8ln67pbfa2lz1k8maqbq22p84pfjhhbrgdgjjzha")))

(define-public crate-hc_zome_profiles_integrity-0.1.0 (c (n "hc_zome_profiles_integrity") (v "0.1.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "hdi") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1svw2wwxw95jih4rqj9yzxh29a5v9v9apf7sbv2pzl2xd37c6jbp")))

(define-public crate-hc_zome_profiles_integrity-0.2.0 (c (n "hc_zome_profiles_integrity") (v "0.2.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "hdi") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0n5y62jjmqi7m6ag82vxhjxy2kxn6pr65xx8aybzrzr01rw20sak")))

