(define-module (crates-io hc _z hc_zome_profiles_coordinator) #:use-module (crates-io))

(define-public crate-hc_zome_profiles_coordinator-0.0.1 (c (n "hc_zome_profiles_coordinator") (v "0.0.1") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "hc_zome_profiles_integrity") (r "^0.0.1") (d #t) (k 0)) (d (n "hdk") (r "^0.1.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ksbnh2jb3a3qwpbkdgrbkfxdapfgirn0cj1a7ahsh8s3zx9y460")))

(define-public crate-hc_zome_profiles_coordinator-0.1.0 (c (n "hc_zome_profiles_coordinator") (v "0.1.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "hc_zome_profiles_integrity") (r "^0.1.0") (d #t) (k 0)) (d (n "hdk") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0hlpbg9i0ay1rbkk58sj3yky7zzisrh153l98yf5587rjf0m8inl")))

(define-public crate-hc_zome_profiles_coordinator-0.1.1 (c (n "hc_zome_profiles_coordinator") (v "0.1.1") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "hc_zome_profiles_integrity") (r "^0.1.0") (d #t) (k 0)) (d (n "hdk") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "13gk4k6g7avln9il93bcayazsznr7593pk15qg52y5dcy0q0aimr")))

(define-public crate-hc_zome_profiles_coordinator-0.1.2 (c (n "hc_zome_profiles_coordinator") (v "0.1.2") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "hc_zome_profiles_integrity") (r "^0.1") (d #t) (k 0)) (d (n "hdk") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "06fd7kry6hycaq3kr3sz18d5hmcymyqm8hlaggff16c16gjc5ff2")))

(define-public crate-hc_zome_profiles_coordinator-0.2.0 (c (n "hc_zome_profiles_coordinator") (v "0.2.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "hc_zome_profiles_integrity") (r "^0.2") (d #t) (k 0)) (d (n "hdk") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1ckmilc704hvz164jaxz3vyyhivlsdzq32087zxs3nhbz7lhxbib")))

