(define-module (crates-io hc sk hcskr) #:use-module (crates-io))

(define-public crate-hcskr-0.1.0 (c (n "hcskr") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.35") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "01qlz39cwbvngla5f51frhpwiq10v77lnryhy5r4yy2p33mbpnnr")))

