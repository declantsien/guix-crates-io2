(define-module (crates-io hc s- hcs-12ss59t) #:use-module (crates-io))

(define-public crate-hcs-12ss59t-0.1.0 (c (n "hcs-12ss59t") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)))) (h "080yn9plz6b64324kgqnrj7f2mz9lpli867wjjmcgs3qmvaqn1ij")))

