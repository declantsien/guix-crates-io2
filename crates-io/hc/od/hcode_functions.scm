(define-module (crates-io hc od hcode_functions) #:use-module (crates-io))

(define-public crate-hcode_functions-0.1.0 (c (n "hcode_functions") (v "0.1.0") (h "019zxb3i14qvgv31kmmk9grclh5jbjs0l04xhxc6jh4ll6k22z70")))

(define-public crate-hcode_functions-0.1.1 (c (n "hcode_functions") (v "0.1.1") (h "168w3h75y3ix4li056a3bsvz7y2djk6i6ms9a7px71i3x7y4a1z4")))

(define-public crate-hcode_functions-0.1.2 (c (n "hcode_functions") (v "0.1.2") (h "1ycv1hhns14qgkcz9iywvpy27dvnh2ydffk58svvsc5crwckj2rx")))

