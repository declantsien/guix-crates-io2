(define-module (crates-io hc id hcid) #:use-module (crates-io))

(define-public crate-hcid-0.0.3-alpha (c (n "hcid") (v "0.0.3-alpha") (d (list (d (n "data-encoding") (r "= 2.1.2") (d #t) (k 0)) (d (n "reed-solomon") (r "= 0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hflayafcz57gdbg3p0gb40ck44xk8q1qvl9i5gpdwmkffikwr6h")))

(define-public crate-hcid-0.0.6 (c (n "hcid") (v "0.0.6") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "reed-solomon") (r "= 0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bpxikisfn0nvzramns4n409swfvs3n95i6gbpnjvpqpddzs4ply")))

