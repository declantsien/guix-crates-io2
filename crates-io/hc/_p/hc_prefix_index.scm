(define-module (crates-io hc _p hc_prefix_index) #:use-module (crates-io))

(define-public crate-hc_prefix_index-0.9.0 (c (n "hc_prefix_index") (v "0.9.0") (d (list (d (n "hdk") (r "=0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "=1.0.166") (d #t) (k 0)))) (h "1cacknwiy1w31wipqncg25zhhp85ja3vpwacy0zqm48dqj2qy20y")))

(define-public crate-hc_prefix_index-0.10.0 (c (n "hc_prefix_index") (v "0.10.0") (d (list (d (n "hdk") (r "=0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "=1.0.166") (d #t) (k 0)))) (h "15jmdf21q45hfv3vjs08s9zya3ahdja285hcmn82kvgrypibhpdw")))

