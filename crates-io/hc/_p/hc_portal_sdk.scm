(define-module (crates-io hc _p hc_portal_sdk) #:use-module (crates-io))

(define-public crate-hc_portal_sdk-0.1.0 (c (n "hc_portal_sdk") (v "0.1.0") (d (list (d (n "hc_crud_caps") (r "^0.10.2") (d #t) (k 0)) (d (n "hc_portal_types") (r "^0.9.0") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.4") (d #t) (k 0)))) (h "1mvl4jc2mjxg5i9z9b18029g795lqls41yz72w180380hbqg4fmg")))

(define-public crate-hc_portal_sdk-0.1.1 (c (n "hc_portal_sdk") (v "0.1.1") (d (list (d (n "hc_crud_caps") (r "^0.10.2") (d #t) (k 0)) (d (n "hc_portal_types") (r "^0.9.0") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.4") (d #t) (k 0)))) (h "17hysvlxhvq399m7cir8w44898gjvi5c5pf34z4nlrqz2gq404k5")))

(define-public crate-hc_portal_sdk-0.1.2 (c (n "hc_portal_sdk") (v "0.1.2") (d (list (d (n "hc_crud_caps") (r "^0.10.2") (d #t) (k 0)) (d (n "hc_portal_types") (r "^0.9.0") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.4") (d #t) (k 0)))) (h "1ny4ri7qwpjqw1nnkrv9jdhk7ag1zaq30hj9s9sf9yv48529njk2")))

(define-public crate-hc_portal_sdk-0.1.3 (c (n "hc_portal_sdk") (v "0.1.3") (d (list (d (n "hc_crud_caps") (r "^0.10.2") (d #t) (k 0)) (d (n "hc_portal_types") (r "^0.9.0") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.4") (d #t) (k 0)))) (h "01fx3y2d9cic0y1whn8nr0f7a06b10797rb1wlqy4y989nbzz3x2")))

(define-public crate-hc_portal_sdk-0.2.0 (c (n "hc_portal_sdk") (v "0.2.0") (d (list (d (n "hc_crud_caps") (r "^0.11") (d #t) (k 0)) (d (n "hc_portal_types") (r "^0.10.0") (d #t) (k 0)) (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.5") (d #t) (k 0)))) (h "134i14w8cfp83lmh3hh2j6y4zc28yv7mgs0p2z882fb32kqvjlzs")))

(define-public crate-hc_portal_sdk-0.3.0 (c (n "hc_portal_sdk") (v "0.3.0") (d (list (d (n "hc_crud_caps") (r "^0.12") (d #t) (k 0)) (d (n "hc_portal_types") (r "^0.11.0") (d #t) (k 0)) (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.6") (d #t) (k 0)))) (h "0kkszdiwy4az05wrfxbnrj3mgsiva38i99wyjx1rnxlyw274zjgs")))

(define-public crate-hc_portal_sdk-0.4.0 (c (n "hc_portal_sdk") (v "0.4.0") (d (list (d (n "hc_crud_caps") (r "^0.13") (d #t) (k 0)) (d (n "hc_portal_types") (r "^0.12") (d #t) (k 0)) (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.7") (d #t) (k 0)))) (h "0pybhqrczh466mim70mmxh649zfpkpw7hv0daavrmj0c1yzzs2q8")))

(define-public crate-hc_portal_sdk-0.5.0 (c (n "hc_portal_sdk") (v "0.5.0") (d (list (d (n "hc_crud_caps") (r "^0.14") (d #t) (k 0)) (d (n "hc_portal_types") (r "^0.13") (d #t) (k 0)) (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.8") (d #t) (k 0)))) (h "0kj96lhj10jwj6dnyc7vvpqplrbvvv34fx0wzp7172aw13j94yq0")))

(define-public crate-hc_portal_sdk-0.6.0 (c (n "hc_portal_sdk") (v "0.6.0") (d (list (d (n "hc_crud_caps") (r "^0.15") (d #t) (k 0)) (d (n "hc_portal_types") (r "^0.14") (d #t) (k 0)) (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.9") (d #t) (k 0)))) (h "0bw767zj0cqjm6s2m7p2xg8d6yrnn0gi295s10xj9if4ckq1mqjg")))

