(define-module (crates-io hc _p hc_portal_types) #:use-module (crates-io))

(define-public crate-hc_portal_types-0.3.0 (c (n "hc_portal_types") (v "0.3.0") (d (list (d (n "hc_crud_caps") (r "^0.3.0") (d #t) (k 0)) (d (n "hdi") (r "^0.2.0") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "051la3c3iz1xaks0hra831kizshwfzfndgh976l2v9j92x9l0zdw")))

(define-public crate-hc_portal_types-0.4.0 (c (n "hc_portal_types") (v "0.4.0") (d (list (d (n "hc_crud_caps") (r "^0.5.0") (d #t) (k 0)) (d (n "hdi") (r "^0.3.0-beta-rc.3") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1nyrj7brkmlrxq6vwjca0j9p4sl8sjsj43l3i5g0pd8k4pbjhk9a")))

(define-public crate-hc_portal_types-0.5.0 (c (n "hc_portal_types") (v "0.5.0") (d (list (d (n "hc_crud_caps") (r "^0.6.0") (d #t) (k 0)) (d (n "hdi") (r "^0.4.0-beta-dev.1") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hpmr8vvmwnvif8mi7zi4m0djglj5dy2g4030qfzv8fbwy85qqjy")))

(define-public crate-hc_portal_types-0.6.0 (c (n "hc_portal_types") (v "0.6.0") (d (list (d (n "hc_crud_caps") (r "^0.7.0") (d #t) (k 0)) (d (n "hdi") (r "^0.4.0-beta-dev.5") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gzihr74v2f4k23jpvgrp8a5gx48adbl5h3qxh9xa3crkzn9mdch")))

(define-public crate-hc_portal_types-0.7.0 (c (n "hc_portal_types") (v "0.7.0") (d (list (d (n "hc_crud_caps") (r "^0.8.0") (d #t) (k 0)) (d (n "hdi") (r "^0.3.1-beta-rc.0") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1i51m7ac6qsf0b2dhwkn2hvqcarqvq7bpwb1gz167hbx4rdhxsc8")))

(define-public crate-hc_portal_types-0.8.0 (c (n "hc_portal_types") (v "0.8.0") (d (list (d (n "hc_crud_caps") (r "^0.9.0") (d #t) (k 0)) (d (n "hdi") (r "=0.3.1") (d #t) (k 0)) (d (n "rmpv") (r "=1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1827r1hypggxwd5jch6vs8k2czvyrj20gv7vgkrgzc5ycz5zh6y2")))

(define-public crate-hc_portal_types-0.9.0 (c (n "hc_portal_types") (v "0.9.0") (d (list (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.4") (d #t) (k 0)))) (h "1z7y6ybpnzyr6v29la8adnk85dqjkfaqk30v7jv4msy3c8hib19d")))

(define-public crate-hc_portal_types-0.10.0 (c (n "hc_portal_types") (v "0.10.0") (d (list (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.5") (d #t) (k 0)))) (h "07mcviq86kgw7c2x0q83jr3p5rfhcg66da32jdl5v0mra6699i3g")))

(define-public crate-hc_portal_types-0.11.0 (c (n "hc_portal_types") (v "0.11.0") (d (list (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.6") (d #t) (k 0)))) (h "1i1y3y4br6l529xgrvkwzzywxawd7nzm8yadjfiivhgyw0rh8wvv")))

(define-public crate-hc_portal_types-0.12.0 (c (n "hc_portal_types") (v "0.12.0") (d (list (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.7") (d #t) (k 0)))) (h "0r02mi0xbzmdrkbnq33j4sdxay2izhm5zh3s6k8pb98h2gvpxwwa")))

(define-public crate-hc_portal_types-0.13.0 (c (n "hc_portal_types") (v "0.13.0") (d (list (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.8") (d #t) (k 0)))) (h "09ckcvpqvdxzmjh64hgzhfm0gfix96rq1b23r1acamaglwp690a7")))

(define-public crate-hc_portal_types-0.14.0 (c (n "hc_portal_types") (v "0.14.0") (d (list (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.9") (d #t) (k 0)))) (h "0z8241lkbhwvfdq4525ccc0v9iif7ck5r11ln2vq02hs3i0jni17")))

