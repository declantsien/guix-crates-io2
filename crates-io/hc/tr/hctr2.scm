(define-module (crates-io hc tr hctr2) #:use-module (crates-io))

(define-public crate-hctr2-0.1.0 (c (n "hctr2") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.4") (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "polyval") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "zeroize") (r "^1.5") (o #t) (k 0)))) (h "124nx8l0izp41aas9dmglly3wqvs9kpp0f3mq1z8vn4d4hhp784h")))

(define-public crate-hctr2-0.2.0 (c (n "hctr2") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.4") (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "polyval") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "zeroize") (r "^1.5") (o #t) (k 0)))) (h "02a5zjyrx9zlygx1idsg80rbwf87sfc14k5ssacjw92wlsk4bkv4")))

