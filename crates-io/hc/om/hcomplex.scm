(define-module (crates-io hc om hcomplex) #:use-module (crates-io))

(define-public crate-hcomplex-0.1.0 (c (n "hcomplex") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "072m4fvb24hs62mh0v0vlal1cpwdpayca6p2pfcb0xdki9vd0s7d")))

(define-public crate-hcomplex-0.1.1 (c (n "hcomplex") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 2)))) (h "0g64pyw9gw9v1gha7y1d96xccjn106h4nl60db5y6a0dz2h7jkm0")))

(define-public crate-hcomplex-0.2.0 (c (n "hcomplex") (v "0.2.0") (d (list (d (n "approx") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "0hzgf00743wcspvpykpa3h7vpkaf972qdm0gafxn2c5935zlw9b0") (f (quote (("std" "num-traits/std" "num-complex/std") ("random" "rand" "rand_distr") ("default" "std"))))))

