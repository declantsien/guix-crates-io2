(define-module (crates-io hc al hcal) #:use-module (crates-io))

(define-public crate-hcal-0.1.0 (c (n "hcal") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "15d6c2y27sqp9bvz3iysjb8hn0idsm8hxsk2xq39wwxngxls29gn")))

(define-public crate-hcal-0.1.1 (c (n "hcal") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1ii8dqjns3jxw5w2mpwnxcna2cy8izp3k6bh3dfa0ci5qxgbi2z9")))

(define-public crate-hcal-0.1.2 (c (n "hcal") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0blsz2vxh6knnk7p9hk7yw4wnlf6x0f179kkl8a7an7l74ggpk35")))

(define-public crate-hcal-0.1.4 (c (n "hcal") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1lm786xmn3b7cv57mnfvzvpj3xshcgq0plxd9p0x9rw0nkv9za9g")))

(define-public crate-hcal-0.1.5 (c (n "hcal") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1wpqxy0108kslh98j69xv4jk2m08wf0s9aj2a1qdavwdynnr6pxs")))

(define-public crate-hcal-0.1.6 (c (n "hcal") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "10ssmqrkrl6sylhkq0zkgmfcdsfhkr1lgh64m3nyaw03d4dj42rh")))

(define-public crate-hcal-0.1.7 (c (n "hcal") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1j10y0yfgkb9n8ps9k777mx0a14l1yx2lj7casbdp36509zclhn0")))

(define-public crate-hcal-0.1.8 (c (n "hcal") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0j741s65f7iwlgmc7l14hx40klpwmjdgsdnd0x0jps3435zmcdij")))

(define-public crate-hcal-0.1.9 (c (n "hcal") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0mlyqaali5gzicss7jxxz1s0pr0m4hzsj3hlrpchc7xm87h3r3dr")))

(define-public crate-hcal-0.1.10 (c (n "hcal") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "15nd5wbfdbd48qqzid52xs3r87wy70xwqgihnjvyyb1c7rw6m7ik")))

(define-public crate-hcal-0.1.11 (c (n "hcal") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0wsz050ijr79amx95xf8hx8zpd4akjxr7hryyrdgkz1ypaqdji2i")))

(define-public crate-hcal-0.1.12 (c (n "hcal") (v "0.1.12") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "110bfgpv5z9pyh8ss2p92f62607cpgngd1ryl46zz3za60248srz")))

(define-public crate-hcal-0.1.13-caesar (c (n "hcal") (v "0.1.13-caesar") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0sqw7dajy4mw11vhv1xvvnnwv68zwb5cj71cg2qblyip14a3hsxx")))

(define-public crate-hcal-0.1.14 (c (n "hcal") (v "0.1.14") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1qd7xsaz9am4j5f97jlysqygbmyq7xpvhlf2jzpjbmx1yqmgz2nb")))

(define-public crate-hcal-0.1.15 (c (n "hcal") (v "0.1.15") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0yssi36z5fqgic79zzzniawgfp6yd1x6nff8yj6b2djz256rlfqd")))

(define-public crate-hcal-0.1.16 (c (n "hcal") (v "0.1.16") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0b0krixbwslskcf61q5vad05ygkblb6k47d2r0jnwgj6dgcxdz56")))

(define-public crate-hcal-0.1.17 (c (n "hcal") (v "0.1.17") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0iq2ng0qrc09afi9zpdda3f0jzg3529h1pncqnd8xgzzbxvmns89")))

(define-public crate-hcal-0.1.18 (c (n "hcal") (v "0.1.18") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0x3fxhaimd00kly70zgyray1y56llz5bkijrl1xkhw22fb2dmcpd")))

(define-public crate-hcal-0.1.19 (c (n "hcal") (v "0.1.19") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0wl7b1157val47gha5kgly84nlcwdax00nch0ybh2nx4avfa676z")))

(define-public crate-hcal-0.1.20 (c (n "hcal") (v "0.1.20") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mvzhm5r3azf9rhw0jbrnd5pmps9rjlkqbiy1xbh8dv96fcyk3m0") (y #t)))

(define-public crate-hcal-0.1.21 (c (n "hcal") (v "0.1.21") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xws7rfv86q6p22q1s9hbdpym3j2rqx2hxgn30h1fw898wrqdkjb")))

(define-public crate-hcal-0.1.23 (c (n "hcal") (v "0.1.23") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kp5hhhv9w7p0xznnjlhisdahn50b92zzq2bimzij3k2c1kf745y")))

(define-public crate-hcal-0.1.24 (c (n "hcal") (v "0.1.24") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0iyy0czgq4sdx4ig61fl7svrwmqqvqmwrlfkmpf3xf0ljb7nyr0d")))

(define-public crate-hcal-0.1.25 (c (n "hcal") (v "0.1.25") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1diyyx2i6bj43d3cpjigp4w1zra8h5b1zsnaj9r5fjj4zm290r4g")))

(define-public crate-hcal-0.1.26 (c (n "hcal") (v "0.1.26") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sqd8dbvl8k4pz6dpmndwnnfnyz2fz199pc9aif5s70hlss79vj3")))

(define-public crate-hcal-0.2.0 (c (n "hcal") (v "0.2.0") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "cbb") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fnpkkpslygd0djk3bb7vpzci7m380d7j8qwq03n18yx5gdai23n")))

(define-public crate-hcal-0.2.1 (c (n "hcal") (v "0.2.1") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "cbb") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sgw23056xcx6c7kp0ymliskabr86wvdzldlll0w6w5aqpsynyzr")))

(define-public crate-hcal-0.3.0 (c (n "hcal") (v "0.3.0") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "cbb") (r "^0.1.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sq2w0m0qc06a4axzr11hi98lxpvxi8w6j5ji682qsbx4hbm63qc") (y #t)))

(define-public crate-hcal-0.3.1 (c (n "hcal") (v "0.3.1") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "cbb") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0w592xfagws0arhjfg4fx756dx7xzqc3qaym94szwzc7gq7xczpn")))

(define-public crate-hcal-0.3.2 (c (n "hcal") (v "0.3.2") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "cbb") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02n75y98bnw32z2rfsgvhfbh20521nkb8ac1l9gpf6zwiirscjs2") (y #t)))

(define-public crate-hcal-0.3.3 (c (n "hcal") (v "0.3.3") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "cbb") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qm30r11b18mw1bfyq6rfhr96nhdzvc9zrga72rgdnanw6q5kfrx")))

(define-public crate-hcal-0.3.4 (c (n "hcal") (v "0.3.4") (d (list (d (n "bdays") (r "^0.1.1") (d #t) (k 0)) (d (n "cbb") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0drqsrka52h0fs60yn8z7dhm8bn02ixrw8dkk28xm1m4ax2aq46f")))

(define-public crate-hcal-0.4.0 (c (n "hcal") (v "0.4.0") (d (list (d (n "bdays") (r "^0.1.3") (d #t) (k 0)) (d (n "cbb") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1wifphjf9pbnzvmbkikza9w9zfxs79md6qgix2kik71zn94d63x1")))

(define-public crate-hcal-0.4.1 (c (n "hcal") (v "0.4.1") (d (list (d (n "bdays") (r "^0.1.3") (d #t) (k 0)) (d (n "cbb") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1a7bjf85v6v055q6i5xsnr8nzj53v2gi49ablnkd9dlissf2xwjq")))

(define-public crate-hcal-0.4.3 (c (n "hcal") (v "0.4.3") (d (list (d (n "bdays") (r "^0.1.3") (d #t) (k 0)) (d (n "cbb") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0gbm99saayq2pcz2rayhwl63nqmsqd2y8x6vnn87zpvm2bzjjxwj")))

