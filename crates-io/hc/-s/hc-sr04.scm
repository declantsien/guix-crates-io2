(define-module (crates-io hc -s hc-sr04) #:use-module (crates-io))

(define-public crate-hc-sr04-0.1.0 (c (n "hc-sr04") (v "0.1.0") (d (list (d (n "rppal") (r "^0.13.1") (d #t) (k 0)))) (h "17mj1yj86h4qwfc1amcrbd5asnch33fzg5rhrg48by9i8ywbmync") (r "1.63.0")))

(define-public crate-hc-sr04-0.1.1 (c (n "hc-sr04") (v "0.1.1") (d (list (d (n "rppal") (r "^0.14.1") (d #t) (k 0)))) (h "1j10f96b381frwzxqjspjghzzd2axs0z3125c0x32y1xrmsffnw7") (r "1.63.0")))

(define-public crate-hc-sr04-0.1.2 (c (n "hc-sr04") (v "0.1.2") (d (list (d (n "rppal") (r "^0.17.1") (d #t) (k 0)))) (h "0c9k5rjbjvslc4hhlh0ng42m3qy3nxd81w9qjqz1f4hgayxmjj9v") (r "1.63.0")))

