(define-module (crates-io hc op hcop) #:use-module (crates-io))

(define-public crate-hcop-0.1.0 (c (n "hcop") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0svnl13h5n5haa4ws83vgfz3n3l7b5m6viw04s64p1s146ljcl84")))

