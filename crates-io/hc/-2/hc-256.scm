(define-module (crates-io hc #{-2}# hc-256) #:use-module (crates-io))

(define-public crate-hc-256-0.0.1 (c (n "hc-256") (v "0.0.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1.0.0-pre") (o #t) (d #t) (k 0)))) (h "0xnsnpqyx5fv3xscf6bndxsw42vc1m6bl98cwh9nrjl860hp2236")))

(define-public crate-hc-256-0.1.0 (c (n "hc-256") (v "0.1.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "stream-cipher") (r "^0.4") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "0n9dc7jm46wvxdi5f1b8g6pfykjzn5xij2hrk70p5hxj5zs951rq")))

(define-public crate-hc-256-0.2.0 (c (n "hc-256") (v "0.2.0") (d (list (d (n "stream-cipher") (r "^0.7") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "1p3wbpjwqq7jl728c0vhijl37vwm7khzmv555a28q7bsyicfy49a")))

(define-public crate-hc-256-0.3.0 (c (n "hc-256") (v "0.3.0") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "01h4v6dlnwf8653k4hf90rbzjh2bf37yskj9114lsvkchjya4k7k")))

(define-public crate-hc-256-0.4.0 (c (n "hc-256") (v "0.4.0") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1l9mg4agqra29z61dcklk2bdbkvf42lzx7045jb1vl8ac5x87xvm")))

(define-public crate-hc-256-0.4.1 (c (n "hc-256") (v "0.4.1") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "zeroize") (r "=1.3") (o #t) (k 0)))) (h "174iv6kzvi7gp09wiyq26dr53qm662xdcy8l1hz7cwcg1plv8r9r")))

(define-public crate-hc-256-0.5.0 (c (n "hc-256") (v "0.5.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1vrqgfqc3l2c79vmhvcmyrm7c8k4ps6mnj2mihxwqxm4ppfj3l9z") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std")))) (r "1.56")))

