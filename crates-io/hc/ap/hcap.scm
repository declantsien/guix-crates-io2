(define-module (crates-io hc ap hcap) #:use-module (crates-io))

(define-public crate-hcap-0.0.1 (c (n "hcap") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "19326z6iqigzq4a2rsjchx4qkhv7sxqvwhfa543fq3k1qbi1ax85")))

(define-public crate-hcap-0.0.2 (c (n "hcap") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1b72cff0v23yv5ljgds56q0w94iig8ajv8fz9pg7nxygqh1vy6pa")))

