(define-module (crates-io hc ap hcaptcha_derive) #:use-module (crates-io))

(define-public crate-hcaptcha_derive-2.1.0 (c (n "hcaptcha_derive") (v "2.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "07hzm4qrbniqpk7vfz4bk0n16izmyrq54wd1pv5c2m1yirylzgmj")))

(define-public crate-hcaptcha_derive-2.1.1 (c (n "hcaptcha_derive") (v "2.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0a6jjy5jpnwi5l74k2ljjj71mk2qzad1migff8i15hlq0ac030n2")))

(define-public crate-hcaptcha_derive-2.2.0 (c (n "hcaptcha_derive") (v "2.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1v1cg4a5ym1dmwdhylrixbyrmn2vnk8da2lw1vadrp4pgv6mkicr")))

(define-public crate-hcaptcha_derive-2.2.1 (c (n "hcaptcha_derive") (v "2.2.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0m3dz05pm5k1sq23l4p6x0knl8ywsa1zl9fa3pr5qjm6gzph5q2m")))

(define-public crate-hcaptcha_derive-2.2.2 (c (n "hcaptcha_derive") (v "2.2.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1b6m07ikz5vbyzz3s7zxsxk3c9pn1cq0958vmhjkk469wqf80j86")))

(define-public crate-hcaptcha_derive-2.3.0 (c (n "hcaptcha_derive") (v "2.3.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m51jnb795djsvxgq75yv4nmsz3ayzqi3fpg4f6plqq8vmis5ysh")))

(define-public crate-hcaptcha_derive-2.3.1 (c (n "hcaptcha_derive") (v "2.3.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dzf5whn8if12pf6dgrgs2zim24dvqv446lpfc4gn72vqjjpsc21")))

