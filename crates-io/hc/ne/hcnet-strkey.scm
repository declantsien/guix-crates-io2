(define-module (crates-io hc ne hcnet-strkey) #:use-module (crates-io))

(define-public crate-hcnet-strkey-0.0.8 (c (n "hcnet-strkey") (v "0.0.8") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("std" "derive" "usage" "help"))) (o #t) (k 0)) (d (n "crate-git-revision") (r "^0.0.6") (d #t) (k 1)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0q7ki6z35irfzq2vw8bjdxbdcrszvdyjdcn92xh042qc0swsrk7r") (f (quote (("default")))) (s 2) (e (quote (("cli" "dep:clap")))) (r "1.67.0")))

(define-public crate-hcnet-strkey-0.0.7 (c (n "hcnet-strkey") (v "0.0.7") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1r2bcq666mry0408vs47fns7rif1b9prbbgfbnxnsl7rhwhsdv9b") (r "1.66")))

