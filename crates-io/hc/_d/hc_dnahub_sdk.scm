(define-module (crates-io hc _d hc_dnahub_sdk) #:use-module (crates-io))

(define-public crate-hc_dnahub_sdk-0.1.0 (c (n "hc_dnahub_sdk") (v "0.1.0") (d (list (d (n "hc_devhub_sdk") (r "^0.1") (d #t) (k 0)) (d (n "hc_dnahub_types") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "193gh7ax44darvqrxhvmsypxwv79xwhpzwx4kw10knl3zfa12zpf")))

(define-public crate-hc_dnahub_sdk-0.2.0 (c (n "hc_dnahub_sdk") (v "0.2.0") (d (list (d (n "hc_devhub_sdk") (r "^0.1") (d #t) (k 0)) (d (n "hc_dnahub_types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0j3crc79vcfqxqw0qc8hbf79msaxp1k0wns2k3ga6q9qjhid564y")))

