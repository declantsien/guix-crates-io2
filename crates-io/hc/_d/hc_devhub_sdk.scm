(define-module (crates-io hc _d hc_devhub_sdk) #:use-module (crates-io))

(define-public crate-hc_devhub_sdk-0.1.0 (c (n "hc_devhub_sdk") (v "0.1.0") (d (list (d (n "hc_crud_caps") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.4") (d #t) (k 0)))) (h "0k8gxp1c84lnnmagpa8jr394qdkw32w4qg4jcqnfv2zxb2n7b24w")))

