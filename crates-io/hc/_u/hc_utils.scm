(define-module (crates-io hc _u hc_utils) #:use-module (crates-io))

(define-public crate-hc_utils-0.0.107 (c (n "hc_utils") (v "0.0.107") (d (list (d (n "hdk") (r "^0.0.107") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.7") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f1ryxszzjqbkmzh5r8mnzca3vwq7k6cr9hvnl4p4y03xw2ajf7i")))

(define-public crate-hc_utils-0.0.1 (c (n "hc_utils") (v "0.0.1") (d (list (d (n "hdk") (r "^0") (d #t) (k 0)) (d (n "holo_hash") (r "^0") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04492ymf24cdh09vy7y0q2ff7cri3vygpxkfxb2icq1871kykhxs")))

(define-public crate-hc_utils-0.0.108 (c (n "hc_utils") (v "0.0.108") (d (list (d (n "hdk") (r "^0.0.108") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.7") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ca43j12ix436kn899m9hk24p2rdv0dhnb18yzm8slpcl0hq1saj")))

(define-public crate-hc_utils-0.0.110 (c (n "hc_utils") (v "0.0.110") (d (list (d (n "hdk") (r "=0.0.110") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.9") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w94mx1qnyjxlp1p8jcarzc8xvmbgvnnqnqv7rjc67y9dpifw7hp")))

(define-public crate-hc_utils-0.0.111 (c (n "hc_utils") (v "0.0.111") (d (list (d (n "hdk") (r "=0.0.111") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.10") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "077ci5jx5i1yg02k8ccjbsbnppvkifh0vgicqrgvggpk7vjy69iz")))

(define-public crate-hc_utils-0.0.112 (c (n "hc_utils") (v "0.0.112") (d (list (d (n "hdk") (r "=0.0.112") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.10") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05pm654x857ipcc66qj9092cch5bw9q7pvzms84fk93g2difpscx")))

(define-public crate-hc_utils-0.0.113 (c (n "hc_utils") (v "0.0.113") (d (list (d (n "hdk") (r "=0.0.113") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.11") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ybm3bpqglm6ajz4aljgwiv3pcwhmv9kk3xfv814h71s7ygvs8im")))

(define-public crate-hc_utils-0.0.114 (c (n "hc_utils") (v "0.0.114") (d (list (d (n "hdk") (r "=0.0.114") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.12") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1009czn9kjl8lla9af8nz1d22barhrypkfwj6k16w1cf3qym1z3l")))

(define-public crate-hc_utils-0.0.115 (c (n "hc_utils") (v "0.0.115") (d (list (d (n "hdk") (r "=0.0.115") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.12") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rcih3b0m8vs52yqjyybh88prcnya2ip8z7mjnl2ci78pzya5i4a")))

(define-public crate-hc_utils-0.0.116 (c (n "hc_utils") (v "0.0.116") (d (list (d (n "hdk") (r "=0.0.116") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.13") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mlybhxcrabz2ka0bn4pmjqsnrf38ksrp3bfwar8b69v0w8nz4rj")))

(define-public crate-hc_utils-0.0.117 (c (n "hc_utils") (v "0.0.117") (d (list (d (n "hdk") (r "=0.0.117") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.14") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "087pz5fp86bmxdxk9sxvpfkcaxgsp04m6dkr76bjqs3pg7gydw4c")))

(define-public crate-hc_utils-0.0.118 (c (n "hc_utils") (v "0.0.118") (d (list (d (n "hdk") (r "=0.0.118") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.15") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "109n7wmp9pf56jq6b53i2s02zsz02wsly1iwn023cmwx2y250zhx")))

(define-public crate-hc_utils-0.0.119 (c (n "hc_utils") (v "0.0.119") (d (list (d (n "hdk") (r "=0.0.119") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.16") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kf71d5qcpkzz3d9m4brd3icpc77nrhvvn5cvdyddsc3nvrl267n")))

(define-public crate-hc_utils-0.0.120 (c (n "hc_utils") (v "0.0.120") (d (list (d (n "hdk") (r "=0.0.120") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.17") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f6a3yy5zinmgmkcikg89rkk3n7z7cb7b17ghxbr0mh7l289mp5i")))

(define-public crate-hc_utils-0.0.121 (c (n "hc_utils") (v "0.0.121") (d (list (d (n "hdk") (r "=0.0.121") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.18") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "119h5jg45wa6q70yqanqi0n3qlw8xaswk3lmrvdik3va60yl10zg")))

(define-public crate-hc_utils-0.0.122 (c (n "hc_utils") (v "0.0.122") (d (list (d (n "hdk") (r "=0.0.122") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.19") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b483qk2q0g7k2qjy9hy1hanng4b2gl097k66x04fy0a3f0ayi94")))

(define-public crate-hc_utils-0.0.123 (c (n "hc_utils") (v "0.0.123") (d (list (d (n "hdk") (r "=0.0.123") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.20") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16klr8z1v5dihqqn6qsbksa6jxr2h8id03di5yy9lrqk4zyyshk3")))

(define-public crate-hc_utils-0.0.124 (c (n "hc_utils") (v "0.0.124") (d (list (d (n "hdk") (r "=0.0.124") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.21") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r9grb7v0p11q8p217mk5aczhh2d6mpi7hwg9dz9rzv02xpc803q")))

(define-public crate-hc_utils-0.0.125 (c (n "hc_utils") (v "0.0.125") (d (list (d (n "hdk") (r "=0.0.125") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.21") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hd0ayjxxpvlyyh0ryw8f6vri6zhy9nz620a95fw1gbb9y307xq4")))

(define-public crate-hc_utils-0.0.126 (c (n "hc_utils") (v "0.0.126") (d (list (d (n "hdk") (r "=0.0.126") (d #t) (k 0)) (d (n "holo_hash") (r "=0.0.21") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d9413p4zs9nwywqhm7r02gmpn11ayjd1xqjr485j9mviskszn61")))

(define-public crate-hc_utils-0.0.132 (c (n "hc_utils") (v "0.0.132") (d (list (d (n "hdk") (r "=0.0.132") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06lm5v06wfw89ywzl1kxr51xj64lw8a2s6kmag8mgikfr3dv0zxz")))

(define-public crate-hc_utils-0.0.133 (c (n "hc_utils") (v "0.0.133") (d (list (d (n "hdk") (r "=0.0.133") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09pbsa0skc9snlwaqhm19qdbrqh4m130zjvi0jcdfpj0ha577924")))

(define-public crate-hc_utils-0.0.136 (c (n "hc_utils") (v "0.0.136") (d (list (d (n "hdk") (r "=0.0.136") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0glqzn8iqv5wlnzxxrjzqdak3zkp2qjf8isff7j2kp94nn36yq1c")))

(define-public crate-hc_utils-0.0.138 (c (n "hc_utils") (v "0.0.138") (d (list (d (n "hdk") (r "=0.0.138") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "holochain_deterministic_integrity") (r "^0.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rzwjmb58ghbvbkhp9m3jqfwpsxgy5xkswdr2mhb19ykbsg3xzwm")))

(define-public crate-hc_utils-0.0.138-patch1 (c (n "hc_utils") (v "0.0.138-patch1") (d (list (d (n "hdk") (r "=0.0.138") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "holochain_deterministic_integrity") (r "^0.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14v5gfsgzpwlazwamnx90sizaav9cjk4l8rw9fr0yixibrcdkc3b")))

(define-public crate-hc_utils-0.0.148 (c (n "hc_utils") (v "0.0.148") (d (list (d (n "hdk") (r "=0.0.148") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g0gbc5gjmnlkzrz5g4mgpl4iaffsvjn6hngfwr87i3qkcyahj8n")))

(define-public crate-hc_utils-0.0.150 (c (n "hc_utils") (v "0.0.150") (d (list (d (n "hdk") (r "=0.0.150") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16r5xgkpwikanw9sfw0hhacdka745qbh72l51996lk2p5g6ais0i")))

(define-public crate-hc_utils-0.0.152 (c (n "hc_utils") (v "0.0.152") (d (list (d (n "hdk") (r "=0.0.152") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16f1y25x5krcz7a2v6zich1sygpg062l7rw69m8y7c9qc8y2ljzv")))

(define-public crate-hc_utils-0.0.154 (c (n "hc_utils") (v "0.0.154") (d (list (d (n "hdk") (r "=0.0.154") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jdlzq3k8l2h6ypw67h191afz7cspphc2qvj97vq4wks34biwm5l")))

(define-public crate-hc_utils-0.0.157 (c (n "hc_utils") (v "0.0.157") (d (list (d (n "hdk") (r "=0.0.157") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kn5h29ly7riq2xl596fyhd4by7nlfj2wpv6s35iinhdchbvrhg8")))

(define-public crate-hc_utils-0.0.158 (c (n "hc_utils") (v "0.0.158") (d (list (d (n "hdk") (r "=0.0.158") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kpzvffk6adzv5h4z6wf3wdjbj1dhqcqsh3sm76q8xh7561nqwjc")))

(define-public crate-hc_utils-0.0.159 (c (n "hc_utils") (v "0.0.159") (d (list (d (n "hdk") (r "=0.0.159") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1caaxxa2mhh3g2169d7pyk37347fidm7r33wsl4mlxcaqsyjpq8p")))

(define-public crate-hc_utils-0.0.160 (c (n "hc_utils") (v "0.0.160") (d (list (d (n "hdk") (r "=0.0.160") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iwgjiax5nfxi2hb878j24qbzx4f3akwj5gyglp0dx14qn26616p")))

(define-public crate-hc_utils-0.0.163 (c (n "hc_utils") (v "0.0.163") (d (list (d (n "hdk") (r "=0.0.163") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16nl8dh0gkcmyg8cvcx3jcjmnnnn5l554snm1j264bwja4glppf1")))

(define-public crate-hc_utils-0.1.0-beta-rc.0 (c (n "hc_utils") (v "0.1.0-beta-rc.0") (d (list (d (n "hdk") (r "=0.1.0-beta-rc.0") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "097m7jyrbidna60ffmg97xxji4d42bn9kw4rxjin59r3x7klahla")))

(define-public crate-hc_utils-0.1.0-beta-rc.1 (c (n "hc_utils") (v "0.1.0-beta-rc.1") (d (list (d (n "hdk") (r "=0.1.0-beta-rc.1") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m6nk5g4027n2128vi6a3xcs57l1mliwzjv688a4vvxcf5634ns4")))

(define-public crate-hc_utils-0.1.0-beta-rc.2 (c (n "hc_utils") (v "0.1.0-beta-rc.2") (d (list (d (n "hdk") (r "=0.1.0-beta-rc.2") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07nv0h2cxjqajlgapx3smza0ikn2w27f8fagfk10xqbmldpjcyvi")))

(define-public crate-hc_utils-0.1.0-beta-rc.3 (c (n "hc_utils") (v "0.1.0-beta-rc.3") (d (list (d (n "hdk") (r "=0.1.0-beta-rc.3") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gbi0fsn978hadybbqfmhhma0jmd0pxw2v29mlar3blmdx11mmb9")))

(define-public crate-hc_utils-0.1.0 (c (n "hc_utils") (v "0.1.0") (d (list (d (n "hdk") (r "=0.1.0") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09rnyg0libjzg549r1xf57ajjn83c97xcz05ddwm3wjchc5jck3b")))

(define-public crate-hc_utils-0.1.1 (c (n "hc_utils") (v "0.1.1") (d (list (d (n "hdk") (r "=0.1.1") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i9d9a90y2kgxgc62liq9dzf9k73pza1b4xxmx19n1v0hgiv8ic4")))

(define-public crate-hc_utils-0.1.2 (c (n "hc_utils") (v "0.1.2") (d (list (d (n "hdk") (r "=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09zj2gdl3lnn6zv22z1mwpxz6mk3lrnr7b7mw1h42s6w6jddkkpd")))

(define-public crate-hc_utils-0.1.2-alpha.0 (c (n "hc_utils") (v "0.1.2-alpha.0") (d (list (d (n "hdk") (r "=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f9ydmgdivb1q6fxdqjv44pnd2caah7f7jmi47k680qp90gww0j8")))

(define-public crate-hc_utils-0.1.2-alpha.1 (c (n "hc_utils") (v "0.1.2-alpha.1") (d (list (d (n "hdk") (r "=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lnmsrb1pg0f7lx0fqvjvac1ay0zzxn3g59chdql03x4l0yiddvl")))

(define-public crate-hc_utils-0.2.0 (c (n "hc_utils") (v "0.2.0") (d (list (d (n "hdk") (r "=0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1plsl85a9xkfjx3s1w97ifj0p2sskyqhjqn5rwcxjy3xvl62jyzy")))

(define-public crate-hc_utils-0.2.1-beta-dev.0 (c (n "hc_utils") (v "0.2.1-beta-dev.0") (d (list (d (n "hdk") (r "=0.2.1-beta-dev.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f94lh886nvgjzw5g40li5xhd6mza1qldlyl4lhm4n9v45smlndb")))

(define-public crate-hc_utils-0.3.0-beta-dev.5 (c (n "hc_utils") (v "0.3.0-beta-dev.5") (d (list (d (n "hdk") (r "=0.3.0-beta-dev.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g6g4c9abs49wwi4p6rsxwx9gpq920xgi0sdr8dwyfx5fm699jff")))

(define-public crate-hc_utils-0.2.1 (c (n "hc_utils") (v "0.2.1") (d (list (d (n "hdk") (r "=0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xyawf3qr9phr6khp97ixnp0icfc9v6i2f6sh9vklgzgzhh1s1kn")))

(define-public crate-hc_utils-0.2.2 (c (n "hc_utils") (v "0.2.2") (d (list (d (n "hdk") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vlq1b97k3ak1imqcj8h7m478xmn1h369xrpfp5dx90ndj8bcr9j")))

(define-public crate-hc_utils-0.2.3 (c (n "hc_utils") (v "0.2.3") (d (list (d (n "hdk") (r "=0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1238mb379m5jib5brd4rd5myd2pv7s28sa1yhx2hfvf150p27inl")))

(define-public crate-hc_utils-0.2.4-rc.0 (c (n "hc_utils") (v "0.2.4-rc.0") (d (list (d (n "hdk") (r "=0.2.4-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rjcrhnhaszhzppmlzh47371rz7syx3vihlidszsd6r75bh2hkl5")))

(define-public crate-hc_utils-0.2.5-rc.0 (c (n "hc_utils") (v "0.2.5-rc.0") (d (list (d (n "hdk") (r "=0.2.5-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "159yizg4z1r62xbhih913gkg96xp9f98kkq4i512053hinf4m1ia")))

(define-public crate-hc_utils-0.3.0-beta-dev.35 (c (n "hc_utils") (v "0.3.0-beta-dev.35") (d (list (d (n "hdk") (r "=0.3.0-beta-dev.35") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g1zf0a1gc9r36lqi00a3lx770k15gq1xvd6yvf6jysgfm4zprfi")))

(define-public crate-hc_utils-0.3.0-beta-dev.35-hotfix.1 (c (n "hc_utils") (v "0.3.0-beta-dev.35-hotfix.1") (d (list (d (n "hdk") (r "=0.3.0-beta-dev.35") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xwi05jj6l88lby8llnpb3wjpc71grdv75prz1y3dk9fvlcnhlsy")))

(define-public crate-hc_utils-0.3.0-beta-dev.40 (c (n "hc_utils") (v "0.3.0-beta-dev.40") (d (list (d (n "hdk") (r "=0.3.0-beta-dev.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j9cfn8jz970pa6kkxndanjdrpvbjf92hgwkrqf48lplm8xhkf7n")))

(define-public crate-hc_utils-0.4.0-dev.2 (c (n "hc_utils") (v "0.4.0-dev.2") (d (list (d (n "hdk") (r "=0.4.0-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mbmpz8v6n6qf8l8flqx4fb8br4pg1l0iwgb2dryy340bbwvd21n")))

(define-public crate-hc_utils-0.4.0-dev.3 (c (n "hc_utils") (v "0.4.0-dev.3") (d (list (d (n "hdk") (r "=0.4.0-dev.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ppilvk42512b31m1j8gc5i4k5g3p49r68rpgq94wmwac9hjx4vz")))

