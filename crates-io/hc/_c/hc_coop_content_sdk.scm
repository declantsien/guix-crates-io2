(define-module (crates-io hc _c hc_coop_content_sdk) #:use-module (crates-io))

(define-public crate-hc_coop_content_sdk-0.1.0 (c (n "hc_coop_content_sdk") (v "0.1.0") (d (list (d (n "rmpv") (r "=1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.1") (d #t) (k 0)))) (h "15hxg53chdkn0df6h0yzhwls9kxxkn50zz0vd289qnc0cjq7npal")))

(define-public crate-hc_coop_content_sdk-0.2.0 (c (n "hc_coop_content_sdk") (v "0.2.0") (d (list (d (n "rmpv") (r "=1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.2") (d #t) (k 0)))) (h "1kr7bmww7njwp0lsdmafpwccfy5skjvch2nn3pd8lp8bqps2pq7x")))

(define-public crate-hc_coop_content_sdk-0.2.1 (c (n "hc_coop_content_sdk") (v "0.2.1") (d (list (d (n "hc_coop_content_types") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.4") (d #t) (k 0)))) (h "0dw9znzxaymaz97ri8h495lfcjcp77alnz369g4vdrb4yc61ph1w")))

(define-public crate-hc_coop_content_sdk-0.3.0 (c (n "hc_coop_content_sdk") (v "0.3.0") (d (list (d (n "hc_coop_content_types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.6") (d #t) (k 0)))) (h "0a0w3iirxl0lqijgnm1glwiql4simbi5mz187a8pcc3amzkf81yz")))

(define-public crate-hc_coop_content_sdk-0.4.0 (c (n "hc_coop_content_sdk") (v "0.4.0") (d (list (d (n "hc_coop_content_types") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.7") (d #t) (k 0)))) (h "0ngilpsm12ydbxf6y0py6a66andhcxyd11wkvf34awrn1yd1yrcf")))

(define-public crate-hc_coop_content_sdk-0.5.0 (c (n "hc_coop_content_sdk") (v "0.5.0") (d (list (d (n "hc_coop_content_types") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.9") (d #t) (k 0)))) (h "10lm1xr2is9rnr2j63cjp767xb5a08c6d9h4q7y5yrw31grav1rs")))

