(define-module (crates-io hc _c hc_crud_ceps) #:use-module (crates-io))

(define-public crate-hc_crud_ceps-0.1.0 (c (n "hc_crud_ceps") (v "0.1.0") (d (list (d (n "hdk") (r "^0.0.106") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.6") (f (quote ("hashing" "encoding"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ma4q866pyv7h42dfac6sn3zdj3gd32icmy5853cf8i56m28appf")))

(define-public crate-hc_crud_ceps-0.2.0 (c (n "hc_crud_ceps") (v "0.2.0") (d (list (d (n "hdk") (r "^0.0.106") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.6") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13hc6a5fvhif003sq411lklqf4p68ia16v6xmp7cf50dgykpm13x")))

(define-public crate-hc_crud_ceps-0.2.1 (c (n "hc_crud_ceps") (v "0.2.1") (d (list (d (n "hdk") (r "^0.0.106") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.6") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ya1qpm9xlib9vq79qxsfh795jlskykfzsdkjg1v9vj94h6baycl")))

(define-public crate-hc_crud_ceps-0.2.2 (c (n "hc_crud_ceps") (v "0.2.2") (d (list (d (n "hdk") (r "^0.0.106") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.6") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xvhxzsjkhzg8zbfmw630x4x73g0412napwrk8qb12s238cj24dd")))

(define-public crate-hc_crud_ceps-0.3.0 (c (n "hc_crud_ceps") (v "0.3.0") (d (list (d (n "hdk") (r "^0.0.106") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.6") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mgk87cgv8j129mv8v76p972ypqj5qmvlvbcg0k773qsi8chhidl")))

(define-public crate-hc_crud_ceps-0.3.1 (c (n "hc_crud_ceps") (v "0.3.1") (d (list (d (n "hdk") (r "^0.0.106") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.6") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xm8d96km9b3aymd71kfkqfjv4q4qsm2p7kmwkczfjc160064b30")))

(define-public crate-hc_crud_ceps-0.4.0 (c (n "hc_crud_ceps") (v "0.4.0") (d (list (d (n "hdk") (r "^0.0.107") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.7") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ljg6k0pj2058skxxaq8kswqf7bn7sv7mzmalhlvilmnixmim4ka")))

(define-public crate-hc_crud_ceps-0.5.0 (c (n "hc_crud_ceps") (v "0.5.0") (d (list (d (n "hdk") (r "^0.0.108") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.7") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18gh0v565v5a0h1c3hml6rvw3qx94avjl2izmdcvfvz9kqm2l390")))

(define-public crate-hc_crud_ceps-0.6.0 (c (n "hc_crud_ceps") (v "0.6.0") (d (list (d (n "hdk") (r "^0.0.109") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.8") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k3k1s0nn7xl20fjxbv8aid2csg3g8lbypl56kmzh4vnl6bvbilz")))

(define-public crate-hc_crud_ceps-0.6.1 (c (n "hc_crud_ceps") (v "0.6.1") (d (list (d (n "hdk") (r "^0.0.109") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.8") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "118mk4miyspslgwqvscqk1c4rrzwaqjx69l5i3wzpvxd21p4yv6w")))

(define-public crate-hc_crud_ceps-0.13.0 (c (n "hc_crud_ceps") (v "0.13.0") (d (list (d (n "hdk") (r "^0.0.116") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.13") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "031359q9mg47kqbylnlph1jsic1isa81f69f3i1inyi7rcxqya4s")))

(define-public crate-hc_crud_ceps-0.17.0 (c (n "hc_crud_ceps") (v "0.17.0") (d (list (d (n "hdk") (r "^0.0.120") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.17") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "182m82930slq4752pw2h7wqip6s3yvz0x1nvwz3ndm7m3rda2x5l")))

(define-public crate-hc_crud_ceps-0.20.0 (c (n "hc_crud_ceps") (v "0.20.0") (d (list (d (n "hdk") (r "^0.0.123") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.20") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b21a3hwwkfzsn5696qq7qpi9lzag7jydyw9wblgq40klhilgrrn")))

(define-public crate-hc_crud_ceps-0.24.0 (c (n "hc_crud_ceps") (v "0.24.0") (d (list (d (n "hdk") (r "^0.0.127") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.22") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02qaiaibapq2scvd7ip34gl8y4mlfw4r2l00mp68r5djn8scw890")))

(define-public crate-hc_crud_ceps-0.29.0 (c (n "hc_crud_ceps") (v "0.29.0") (d (list (d (n "hdk") (r "^0.0.132") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.25") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1khjxzkdanjpianr2dc1s0a6nrc71xd5j1ywidzpgjlqqq54swfb")))

(define-public crate-hc_crud_ceps-0.33.0 (c (n "hc_crud_ceps") (v "0.33.0") (d (list (d (n "hdk") (r "^0.0.136") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.27") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12kr8z3qvbwfnzwbz7i6b8qir6449spqr8gp9xphh0b5z7s6qffs")))

(define-public crate-hc_crud_ceps-0.33.1 (c (n "hc_crud_ceps") (v "0.33.1") (d (list (d (n "hdk") (r "^0.0.136") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.27") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ps72n5ljan1nh3jgp21gxifmx6jd2hygqca65nz5yg0pxwdgc6k")))

(define-public crate-hc_crud_ceps-0.50.0 (c (n "hc_crud_ceps") (v "0.50.0") (d (list (d (n "hdk") (r "^0.0.142") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.30") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0whmyh6msfr6bbm7qmmb0q2fgwyf6wdk0203wijc8iyv5v3ra8rr")))

(define-public crate-hc_crud_ceps-0.50.1 (c (n "hc_crud_ceps") (v "0.50.1") (d (list (d (n "hdk") (r "^0.0.142") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.30") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00bvjvfmxymznrgyi3nlkz0r7pmjkp80phdglzbs975sn5nn6iqf")))

(define-public crate-hc_crud_ceps-0.53.0 (c (n "hc_crud_ceps") (v "0.53.0") (d (list (d (n "hdk") (r "^0.0.145") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.31") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fis9m6vzj5j7521r0z11n6mqbb86mwnx84g2zgzw0zjca3b6q9p")))

(define-public crate-hc_crud_ceps-0.55.0 (c (n "hc_crud_ceps") (v "0.55.0") (d (list (d (n "hdk") (r "^0.0.147") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.31") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lvkrwc84sicvd7rrn9c0lyq5f26z1j8f3gz2ldwxvh2i6cl5nxr")))

(define-public crate-hc_crud_ceps-0.59.0 (c (n "hc_crud_ceps") (v "0.59.0") (d (list (d (n "hdk") (r "^0.0.151") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.31") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mf397dcw228xg2g96j4nv7k8j57lm48c28n8sf1kzzqprl3rqf6")))

(define-public crate-hc_crud_ceps-0.68.0 (c (n "hc_crud_ceps") (v "0.68.0") (d (list (d (n "hdk") (r "^0.0.160") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.35") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y6yk4mca9w9b0gy30a0i7alb3hnj92lx1jfc4gdknqxx100wwwy")))

(define-public crate-hc_crud_ceps-0.71.0 (c (n "hc_crud_ceps") (v "0.71.0") (d (list (d (n "hdk") (r "^0.0.163") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.35") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01m5qnkx78fwgchh9mrgx5x9rsjslsbn2yw2rqdxk626m7vj9bkh")))

(define-public crate-hc_crud_ceps-0.72.0 (c (n "hc_crud_ceps") (v "0.72.0") (d (list (d (n "hdk") (r "^0.1.0-beta-rc.0") (d #t) (k 0)) (d (n "holo_hash") (r "^0.1.0-beta-rc.0") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cqcr81ckdwzzh8v63nh2ky4aq1239va9ybnw275dsavq08rsl4g")))

(define-public crate-hc_crud_ceps-0.73.0 (c (n "hc_crud_ceps") (v "0.73.0") (d (list (d (n "hdk") (r "^0.1.0-beta-rc.2") (d #t) (k 0)) (d (n "holo_hash") (r "^0.1.0-beta-rc.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jxldnkb3721yz7naw4gc18q258lmhllwxjxaa8x135x55z6dh4x")))

(define-public crate-hc_crud_ceps-0.74.0 (c (n "hc_crud_ceps") (v "0.74.0") (d (list (d (n "hdk") (r "^0.1.0-beta-rc.3") (d #t) (k 0)) (d (n "holo_hash") (r "^0.1.0-beta-rc.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1svpf1y1n97962m7l8myyk3wapdv6zvcll287xqvks0cqfcp2x7v")))

(define-public crate-hc_crud_ceps-0.75.0 (c (n "hc_crud_ceps") (v "0.75.0") (d (list (d (n "hdk") (r "^0.1.0") (d #t) (k 0)) (d (n "holo_hash") (r "^0.1.0-beta-rc.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g6m7srpxv2vknx6vmnmrbz72f649zd0fl7snk0kfk6kambwsb8r")))

(define-public crate-hc_crud_ceps-0.76.0 (c (n "hc_crud_ceps") (v "0.76.0") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.1") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.0-beta-rc.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "191idvp49jq3ca9jdgs1lka2xhi50zjkscymzqxl5ndyns6yrhwg")))

(define-public crate-hc_crud_ceps-0.77.0 (c (n "hc_crud_ceps") (v "0.77.0") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.4") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.0-beta-rc.3") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yk04yvdjyas07vc6qyp7xqzgr195qib2crkv4nly3wy1amy1y2r")))

(define-public crate-hc_crud_ceps-0.78.0 (c (n "hc_crud_ceps") (v "0.78.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.2") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04j33d5yrz453g1fidyl1zrb5l0bk25idpsv837w8q3jcxfzj87d")))

(define-public crate-hc_crud_ceps-0.79.0 (c (n "hc_crud_ceps") (v "0.79.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.7") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.4") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1nsjiyyr4r70j62cvqlcm438n009mn8bxj5mm6jral8bsm2mp4nl")))

(define-public crate-hc_crud_ceps-0.80.0 (c (n "hc_crud_ceps") (v "0.80.0") (d (list (d (n "hdk") (r "^0.2.1-beta-rc.0") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.1-beta-rc.0") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07wihkb72z9a7mlkcqcx05rxbk98sivpdqkibi1bdzvs7m0n913y")))

