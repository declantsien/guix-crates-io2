(define-module (crates-io hc _c hc_coop_content_types) #:use-module (crates-io))

(define-public crate-hc_coop_content_types-0.1.0 (c (n "hc_coop_content_types") (v "0.1.0") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.4.2") (d #t) (k 0)))) (h "1h00i6wj77pg55r2vv955vx2ihify5ddg197ymm6v7lywh1x6q06")))

(define-public crate-hc_coop_content_types-0.2.0 (c (n "hc_coop_content_types") (v "0.2.0") (d (list (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.6") (d #t) (k 0)))) (h "19hq0rrxq02v7mw1chkfrkh2akz1gc0aszbnrsr0is0cf1a7ma10")))

(define-public crate-hc_coop_content_types-0.3.0 (c (n "hc_coop_content_types") (v "0.3.0") (d (list (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.7") (d #t) (k 0)))) (h "03vwcmkgnd5gqd14v0r2jqhwq46mvk41f3jdh7s2jkyjdmlm8lk1")))

(define-public crate-hc_coop_content_types-0.4.0 (c (n "hc_coop_content_types") (v "0.4.0") (d (list (d (n "rmpv") (r "^1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.9") (d #t) (k 0)))) (h "042hc4g9zhniz44xfh2lb6r5vz2g0ar7z8aizgydnfi54mqj3qm7")))

