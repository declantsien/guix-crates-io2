(define-module (crates-io hc _c hc_crud_caps) #:use-module (crates-io))

(define-public crate-hc_crud_caps-0.1.0 (c (n "hc_crud_caps") (v "0.1.0") (d (list (d (n "hdk") (r "^0.0.160") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.35") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g56i8ksfivh21gmxgkl9dd15l445iqc91izmsmpqnlvkxq06y9g")))

(define-public crate-hc_crud_caps-0.1.1 (c (n "hc_crud_caps") (v "0.1.1") (d (list (d (n "hdk") (r "^0.0.160") (d #t) (k 0)) (d (n "holo_hash") (r "^0.0.35") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1w516abl3mkmybz4jwzrrnp92jsxhaljdwqvnqxlpl07m52i35wp")))

(define-public crate-hc_crud_caps-0.2.0 (c (n "hc_crud_caps") (v "0.2.0") (d (list (d (n "hdk") (r "^0.1.0-beta-rc.2") (d #t) (k 0)) (d (n "holo_hash") (r "^0.1.0-beta-rc.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fh99hssh3lwmxxglkqxs4w1nj619wmarabjzbdpq5ln1ip039gx")))

(define-public crate-hc_crud_caps-0.3.0 (c (n "hc_crud_caps") (v "0.3.0") (d (list (d (n "hdk") (r "^0.1.0") (d #t) (k 0)) (d (n "holo_hash") (r "^0.1.0") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gf8dissgcvadb79q956slz8zpqh6v7qw2ippszpnwd8h7b9a2ld")))

(define-public crate-hc_crud_caps-0.4.0 (c (n "hc_crud_caps") (v "0.4.0") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.1") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.0-beta-rc.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12yx6fxnixbn6aw0msnwzfixx12jwg3kqk4n91qr2csg90gzp2g0")))

(define-public crate-hc_crud_caps-0.5.0 (c (n "hc_crud_caps") (v "0.5.0") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.4") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.0-beta-rc.3") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1aqlsaiyy69a5d0x0njh41hv2avpipv4kyjwsab0qxglg6yr44bk")))

(define-public crate-hc_crud_caps-0.6.0 (c (n "hc_crud_caps") (v "0.6.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.2") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0q4jwq5qwppk6qbm1k4zw5r8dk904hnkbmh8d23wsvii48ibv1i4")))

(define-public crate-hc_crud_caps-0.7.0 (c (n "hc_crud_caps") (v "0.7.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.7") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.4") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1y8a9ii4pivkp86m9a2lsbzzvldpqk7xcqzb1s6ndhnyqfibibg2")))

(define-public crate-hc_crud_caps-0.8.0 (c (n "hc_crud_caps") (v "0.8.0") (d (list (d (n "hdk") (r "^0.2.1-beta-rc.0") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.1-beta-rc.0") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wbfq3jni9jphw2k5b2rb1yj9h4cpzrxz1995jn03jmwx944v93z")))

(define-public crate-hc_crud_caps-0.9.0 (c (n "hc_crud_caps") (v "0.9.0") (d (list (d (n "hdk") (r "=0.2.1") (d #t) (k 0)) (d (n "holo_hash") (r "=0.2.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1spjdykplks2r18s52nqj8v7jaxm18hlxiny0zak7j9y06xbxlk5")))

(define-public crate-hc_crud_caps-0.9.1 (c (n "hc_crud_caps") (v "0.9.1") (d (list (d (n "hdk") (r "=0.2.1") (d #t) (k 0)) (d (n "holo_hash") (r "=0.2.1") (f (quote ("hashing" "encoding"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0irqs0p5xhybndzc7x40si4lqsmclmqqw78xaxaqrl928msk999z")))

(define-public crate-hc_crud_caps-0.10.0 (c (n "hc_crud_caps") (v "0.10.0") (d (list (d (n "holo_hash") (r "=0.2.2") (f (quote ("hashing"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "=0.4.0") (d #t) (k 0)))) (h "0hkmz7d1s63qllnwiak4mi2j4h473c0ph591gq1fmsl8m68dcsjg")))

(define-public crate-hc_crud_caps-0.10.1 (c (n "hc_crud_caps") (v "0.10.1") (d (list (d (n "holo_hash") (r "=0.2.2") (f (quote ("hashing"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "=0.4.0") (d #t) (k 0)))) (h "1p0dadsi0xz8v11zw1r2y640hh4d73x1w1pyyffl5n5k4kbaicy9")))

(define-public crate-hc_crud_caps-0.10.2 (c (n "hc_crud_caps") (v "0.10.2") (d (list (d (n "holo_hash") (r "^0.2.2") (f (quote ("hashing"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.4") (d #t) (k 0)))) (h "0wl1llfy1yrwbr2pk5iikxckif4i35a43gkvfami4smy3gavx240")))

(define-public crate-hc_crud_caps-0.10.3 (c (n "hc_crud_caps") (v "0.10.3") (d (list (d (n "holo_hash") (r "^0.2.2") (f (quote ("hashing"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.4") (d #t) (k 0)))) (h "1jyzwfy56w4bavwyjzmja7k9kyg0vav2b2yifvw61056asqncmlb")))

(define-public crate-hc_crud_caps-0.11.0 (c (n "hc_crud_caps") (v "0.11.0") (d (list (d (n "holo_hash") (r "^0.2.6") (f (quote ("hashing"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.5") (d #t) (k 0)))) (h "11a6s8q264q0w7a02lkzw5shyxlvmsv4msb22jq6rwm8grv0sgx9")))

(define-public crate-hc_crud_caps-0.12.0 (c (n "hc_crud_caps") (v "0.12.0") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.24") (f (quote ("hashing"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.6") (d #t) (k 0)))) (h "0kkc5axvzz64iybzsiqym7n3syys11r8clw8iyr9iyvsv804whp1")))

(define-public crate-hc_crud_caps-0.13.0 (c (n "hc_crud_caps") (v "0.13.0") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.26") (f (quote ("hashing"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.7") (d #t) (k 0)))) (h "0jrh2qjhxllf4agmx2phnls55mhhv1cw5r2y37g0il0s1q0v49j2")))

(define-public crate-hc_crud_caps-0.14.0 (c (n "hc_crud_caps") (v "0.14.0") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.28") (f (quote ("hashing"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.8") (d #t) (k 0)))) (h "0asv6gizhqmyz62imigxh698b7aw2l2gb4yr3dva6bl267s0vk51")))

(define-public crate-hc_crud_caps-0.15.0 (c (n "hc_crud_caps") (v "0.15.0") (d (list (d (n "holo_hash") (r "^0.4.0-dev.1") (f (quote ("hashing"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "whi_hdk_extensions") (r "^0.9") (d #t) (k 0)))) (h "1iwv0q2ll05z83qszxcy5sfxxs4qh9fhkls51bz67bc2ksd9v8jb")))

