(define-module (crates-io hc _c hc_cz_profile) #:use-module (crates-io))

(define-public crate-hc_cz_profile-0.1.0-beta-rc.3 (c (n "hc_cz_profile") (v "0.1.0-beta-rc.3") (d (list (d (n "hc_iz_profile") (r "^0.1.0-beta-rc.3") (d #t) (k 0)) (d (n "hc_utils") (r "=0.1.0-beta-rc.3") (d #t) (k 0)) (d (n "hdk") (r "=0.1.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v5j09a1jxb61hw6azssya7k5ki4jp5whqx8j0vn5n6bfh2pvy0p")))

(define-public crate-hc_cz_profile-0.1.0 (c (n "hc_cz_profile") (v "0.1.0") (d (list (d (n "hc_iz_profile") (r "^0.1.0") (d #t) (k 0)) (d (n "hc_utils") (r "=0.1.0") (d #t) (k 0)) (d (n "hdk") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zxs281qji5b36jcmrml8b16p8y0532qp8gnccs2mcgndp3j84wi")))

(define-public crate-hc_cz_profile-0.1.1 (c (n "hc_cz_profile") (v "0.1.1") (d (list (d (n "hc_iz_profile") (r "^0.1.1") (d #t) (k 0)) (d (n "hc_utils") (r "=0.1.1") (d #t) (k 0)) (d (n "hdk") (r "=0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01r1dpbd8n1b6384p2kvy7pa4409y0jsfya91ag54ahhnxs1f084")))

(define-public crate-hc_cz_profile-0.1.2 (c (n "hc_cz_profile") (v "0.1.2") (d (list (d (n "hc_iz_profile") (r "^0.1.2") (d #t) (k 0)) (d (n "hc_utils") (r "=0.1.2") (d #t) (k 0)) (d (n "hdk") (r "=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09imx4gm93yd0n4hjym3q7hqk2h5sq9j6vv0m2rnb6wmhdsbw4im")))

(define-public crate-hc_cz_profile-0.1.2-alpha.0 (c (n "hc_cz_profile") (v "0.1.2-alpha.0") (d (list (d (n "hc_iz_profile") (r "^0.1.2") (d #t) (k 0)) (d (n "hc_utils") (r "=0.1.2-alpha.0") (d #t) (k 0)) (d (n "hdk") (r "=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05fbicbsx74mq14ax7w0aq2lgsi60mz5mnmbrk6l8mabzg8hdm9r")))

(define-public crate-hc_cz_profile-0.1.2-alpha.1 (c (n "hc_cz_profile") (v "0.1.2-alpha.1") (d (list (d (n "hc_iz_profile") (r "^0.1.2") (d #t) (k 0)) (d (n "hc_utils") (r "=0.1.2-alpha.1") (d #t) (k 0)) (d (n "hdk") (r "=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vjig40jny6gg6wjr241vf4cb24rx5246ysx32wmada5dmp45c82")))

(define-public crate-hc_cz_profile-0.2.0 (c (n "hc_cz_profile") (v "0.2.0") (d (list (d (n "hc_iz_profile") (r "^0.2.0") (d #t) (k 0)) (d (n "hc_utils") (r "=0.2.0") (d #t) (k 0)) (d (n "hdk") (r "=0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ini1rgwvqxpm6v99nv8pv328xzbrkz3gdm3mfr95cfqwwp8pyib")))

(define-public crate-hc_cz_profile-0.2.1-beta-dev.0 (c (n "hc_cz_profile") (v "0.2.1-beta-dev.0") (d (list (d (n "hc_iz_profile") (r "^0.2.1-beta-dev.0") (d #t) (k 0)) (d (n "hc_utils") (r "=0.2.1-beta-dev.0") (d #t) (k 0)) (d (n "hdk") (r "=0.2.1-beta-dev.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q3dfl7p93mj7vhdm71lknf76lqq64gp9ggfw5wzwnmizqrl7dv3")))

(define-public crate-hc_cz_profile-0.3.0-beta-dev.5 (c (n "hc_cz_profile") (v "0.3.0-beta-dev.5") (d (list (d (n "hc_iz_profile") (r "^0.3.0-beta-dev.5") (d #t) (k 0)) (d (n "hc_utils") (r "=0.3.0-beta-dev.5") (d #t) (k 0)) (d (n "hdk") (r "=0.3.0-beta-dev.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mz3qsnhbid1sllzk0jj5v39y0wyy4h942b1ca6rqbwyqj67gl1r")))

(define-public crate-hc_cz_profile-0.2.1 (c (n "hc_cz_profile") (v "0.2.1") (d (list (d (n "hc_iz_profile") (r "^0.2.1") (d #t) (k 0)) (d (n "hc_utils") (r "=0.2.1") (d #t) (k 0)) (d (n "hdk") (r "=0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vc3qdxpgf2md3qy0anlgnfbkvvjqzjhh7nkfb0rygl55sqwwivi")))

(define-public crate-hc_cz_profile-0.2.2 (c (n "hc_cz_profile") (v "0.2.2") (d (list (d (n "hc_iz_profile") (r "^0.2.2") (d #t) (k 0)) (d (n "hc_utils") (r "=0.2.2") (d #t) (k 0)) (d (n "hdk") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fyxsa5rdmrfw1fgnxb1jv4wkbsb7183jc4d1ah4xqya8gscmzqr")))

(define-public crate-hc_cz_profile-0.2.3 (c (n "hc_cz_profile") (v "0.2.3") (d (list (d (n "hc_iz_profile") (r "^0.2.3") (d #t) (k 0)) (d (n "hc_utils") (r "=0.2.3") (d #t) (k 0)) (d (n "hdk") (r "=0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qvf57543zc9ysi0q4ja84riaq5nr4kii9c5c3nijgd7xp187j12")))

(define-public crate-hc_cz_profile-0.2.4-rc.0 (c (n "hc_cz_profile") (v "0.2.4-rc.0") (d (list (d (n "hc_iz_profile") (r "^0.2.4-rc.0") (d #t) (k 0)) (d (n "hc_utils") (r "=0.2.4-rc.0") (d #t) (k 0)) (d (n "hdk") (r "=0.2.4-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x1v51fk8pnal29clmfi8c6ydra2jzhzz5z4pgafzzfqcp55df8k")))

(define-public crate-hc_cz_profile-0.2.5-rc.0 (c (n "hc_cz_profile") (v "0.2.5-rc.0") (d (list (d (n "hc_iz_profile") (r "^0.2.5-rc.0") (d #t) (k 0)) (d (n "hc_utils") (r "=0.2.5-rc.0") (d #t) (k 0)) (d (n "hdk") (r "=0.2.5-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hjilkannl7kzaax66s7xnvky8ia32h797xahqgamgp5ay1f1bv5")))

(define-public crate-hc_cz_profile-0.3.0-beta-dev.35 (c (n "hc_cz_profile") (v "0.3.0-beta-dev.35") (d (list (d (n "hc_iz_profile") (r "^0.3.0-beta-dev.35") (d #t) (k 0)) (d (n "hc_utils") (r "=0.3.0-beta-dev.35") (d #t) (k 0)) (d (n "hdk") (r "=0.3.0-beta-dev.35") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "177n3xnp4p86a3x7c2cxdd2a2ybrq0xcw3hvczh81ihix23nd4ad")))

(define-public crate-hc_cz_profile-0.3.0-beta-dev.35-hotfix.1 (c (n "hc_cz_profile") (v "0.3.0-beta-dev.35-hotfix.1") (d (list (d (n "hc_iz_profile") (r "^0.3.0-beta-dev.35") (d #t) (k 0)) (d (n "hc_utils") (r "=0.3.0-beta-dev.35-hotfix.1") (d #t) (k 0)) (d (n "hdk") (r "=0.3.0-beta-dev.35") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1537h529b97ksbsbij6h92gqy10idy5vq60agsawz5cxi0di85gn")))

(define-public crate-hc_cz_profile-0.3.0-beta-dev.40 (c (n "hc_cz_profile") (v "0.3.0-beta-dev.40") (d (list (d (n "hc_iz_profile") (r "^0.3.0-beta-dev.40") (d #t) (k 0)) (d (n "hc_utils") (r "=0.3.0-beta-dev.40") (d #t) (k 0)) (d (n "hdk") (r "=0.3.0-beta-dev.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13an4f071dnc89vlds06hs4q0vi8wr94fmw11j93b9m522r50xyr")))

(define-public crate-hc_cz_profile-0.4.0-dev.2 (c (n "hc_cz_profile") (v "0.4.0-dev.2") (d (list (d (n "hc_iz_profile") (r "^0.4.0-dev.2") (d #t) (k 0)) (d (n "hc_utils") (r "=0.4.0-dev.2") (d #t) (k 0)) (d (n "hdk") (r "=0.4.0-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0aj53j1j281fziihagvq5ls45n4c9f76magdci24691ba11kj6xw")))

(define-public crate-hc_cz_profile-0.4.0-dev.3 (c (n "hc_cz_profile") (v "0.4.0-dev.3") (d (list (d (n "hc_iz_profile") (r "^0.4.0-dev.3") (d #t) (k 0)) (d (n "hc_utils") (r "=0.4.0-dev.3") (d #t) (k 0)) (d (n "hdk") (r "=0.4.0-dev.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kfhpqih6rf0frvw6f3rf8nckzh0l4vfi6gpvpnwj2pr9zi3b9bg")))

