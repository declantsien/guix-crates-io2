(define-module (crates-io hc l- hcl-template) #:use-module (crates-io))

(define-public crate-hcl-template-0.1.0 (c (n "hcl-template") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hcl-rs") (r "^0.16.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "0gncfjilx4zk1vgaym9v4kabzy6lh594m6nb0nm8p9l4nyn9q2vm")))

(define-public crate-hcl-template-0.1.1 (c (n "hcl-template") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hcl-rs") (r "^0.16.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "03d7ga5dy8451c5j6pyawjgz97f142a74ib8w0slfaxxjs47l6fp")))

