(define-module (crates-io hc #{12}# hc12-at) #:use-module (crates-io))

(define-public crate-hc12-at-0.1.0 (c (n "hc12-at") (v "0.1.0") (d (list (d (n "at-commands") (r "^0.5.0") (d #t) (k 0)) (d (n "debugless-unwrap") (r "^0.0.4") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "06kb3jal8ips2khq8y4m1dsnnbf4c3v54kqm0jik5z55xiydsqah")))

(define-public crate-hc12-at-0.2.0 (c (n "hc12-at") (v "0.2.0") (d (list (d (n "at-commands") (r "^0.5.0") (d #t) (k 0)) (d (n "debugless-unwrap") (r "^0.0.4") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "1lnpxc0yr05fll3xzabsq4p2yzl610kc6rhrcixr0pw45iybw46n")))

