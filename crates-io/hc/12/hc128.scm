(define-module (crates-io hc #{12}# hc128) #:use-module (crates-io))

(define-public crate-hc128-0.1.0 (c (n "hc128") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0gdlrb70xsbvr4x2yslrk625pkmydfd8hnkb5szjbwyrgsl4i7mx")))

(define-public crate-hc128-0.1.1 (c (n "hc128") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1bj1sdb76gv5sr7z6jvmyjw4fh5l326lcxccmr916x044xzibrld")))

(define-public crate-hc128-0.1.2 (c (n "hc128") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "0i2s6gaa8v7lhcq1p9cw1swxlrbl7s0lsawqv36kbi6hfr2m22cf")))

(define-public crate-hc128-0.1.3 (c (n "hc128") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5") (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "0fxgyrr17w6lbivgid313yrrkmqh2v8591v9fdkmf3rz62p9ikdl")))

(define-public crate-hc128-0.1.4 (c (n "hc128") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.5") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "08ndpc0bvrw0d1smv0bv8f0yyaw9i8q4s92sa577qqfxfk4m5xxl")))

(define-public crate-hc128-0.1.5 (c (n "hc128") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "11f6fd8il3rr3mnmzfw8i2sgxgrz9z08rwll12cqilsd6jwfk3hb")))

(define-public crate-hc128-0.1.6 (c (n "hc128") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "08vhwkha1s63718cf5j0hfdcyypa4r8i455sylzqb5vccl6zxls0")))

