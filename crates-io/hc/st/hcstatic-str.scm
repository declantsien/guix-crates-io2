(define-module (crates-io hc st hcstatic-str) #:use-module (crates-io))

(define-public crate-hcstatic-str-0.1.0 (c (n "hcstatic-str") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0s1wl7am4gj5risk730k8m6dbx7h3bbdiyar456r0xmlclr2xj8v")))

(define-public crate-hcstatic-str-0.1.1 (c (n "hcstatic-str") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "09ywj4fkzip27dnml3m8jnh37wzd40pxd5bg84nbqsizncpnrdvy")))

