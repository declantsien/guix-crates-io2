(define-module (crates-io ta c_ tac_cart) #:use-module (crates-io))

(define-public crate-tac_cart-0.1.0 (c (n "tac_cart") (v "0.1.0") (d (list (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "0g2fh5w3g11447ralcnfhda2g414l0wzj5wji62pnl8cxkrr7l9i")))

(define-public crate-tac_cart-0.2.0 (c (n "tac_cart") (v "0.2.0") (d (list (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "0fy60115r0lgml3i7sblk378jc0br2nq2mpy6h124qidwlyghhk1")))

