(define-module (crates-io ta nd tandem_garble_interop) #:use-module (crates-io))

(define-public crate-tandem_garble_interop-0.1.0 (c (n "tandem_garble_interop") (v "0.1.0") (d (list (d (n "garble_lang") (r "^0.1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tandem") (r "^0.1.0") (d #t) (k 0)))) (h "166agwawavna2glk1bxlpiglvwnbj5phql3n86llz69a0d0s9pnn") (r "1.60.0")))

(define-public crate-tandem_garble_interop-0.2.1 (c (n "tandem_garble_interop") (v "0.2.1") (d (list (d (n "garble_lang") (r "^0.1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tandem") (r "^0.2.1") (d #t) (k 0)))) (h "0xhv56maikqki86ad9w0r8fhrgblrmc1k66swrc7c6qldxgcbab6") (r "1.60.0")))

(define-public crate-tandem_garble_interop-0.3.0 (c (n "tandem_garble_interop") (v "0.3.0") (d (list (d (n "garble_lang") (r "^0.1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tandem") (r "^0.3.0") (d #t) (k 0)))) (h "0xmldqhxry7dk2hr8p9mw6i7fdij2l2xmkb9w5mik70xq8x40ri7") (r "1.60.0")))

