(define-module (crates-io ta rg target-test-dir) #:use-module (crates-io))

(define-public crate-target-test-dir-0.1.0 (c (n "target-test-dir") (v "0.1.0") (d (list (d (n "target-test-dir-support") (r "^0.1.0") (d #t) (k 0)))) (h "1z2h5aq773yqylpxn0hvr6ihlmz7gdw62h3sixpvgpy7a3bj8xwa")))

(define-public crate-target-test-dir-0.1.1 (c (n "target-test-dir") (v "0.1.1") (d (list (d (n "target-test-dir-support") (r "^0.1.1") (d #t) (k 0)))) (h "0ahdpzs9s867gk0x704ixv9bm14rqwwmsq9n0xpxbp47xa0qxdwr")))

(define-public crate-target-test-dir-0.2.0 (c (n "target-test-dir") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "target-test-dir-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0dv2dkbkrfbv9jyn5jbv40n1qm1q35jmlzkdr4bp68q11k7vnv40")))

(define-public crate-target-test-dir-0.3.0 (c (n "target-test-dir") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "anyhow-std") (r "^0.1.1") (d #t) (k 0)) (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "target-test-dir-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1dni37fha4xk9x3d8v39h6s1n27jzjzlh71gzbb5wfwf8ly0rzkx")))

