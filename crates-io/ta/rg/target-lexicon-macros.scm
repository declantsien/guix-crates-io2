(define-module (crates-io ta rg target-lexicon-macros) #:use-module (crates-io))

(define-public crate-target-lexicon-macros-0.1.0-alpha.1 (c (n "target-lexicon-macros") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "target-lexicon") (r "^0.12.4") (d #t) (k 0)))) (h "1gvkqa1r0y46acwc3bskd24f1wqgvya0v307887qfdpfig7nmxwx")))

