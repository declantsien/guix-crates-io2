(define-module (crates-io ta rg target-lexicon) #:use-module (crates-io))

(define-public crate-target-lexicon-0.0.0 (c (n "target-lexicon") (v "0.0.0") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.1") (k 0)))) (h "11c598npqn2dmg6bq6qh8bpr52276sd01n35s6lk2rvjmc1b1kmx") (f (quote (("std") ("default" "std"))))))

(define-public crate-target-lexicon-0.0.1 (c (n "target-lexicon") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.1") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1wx9h347iip5ywnpjm91fibsasmj152sqdmc5x1fjrrdwjqiq2cl") (f (quote (("std") ("default" "std"))))))

(define-public crate-target-lexicon-0.0.2 (c (n "target-lexicon") (v "0.0.2") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.1") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0l52s0chjd0m1s1icv7afqccmrkclkxkj84iklc97xgglidskc3i") (f (quote (("std") ("default" "std"))))))

(define-public crate-target-lexicon-0.0.3 (c (n "target-lexicon") (v "0.0.3") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.1") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0zr4czslpqg46yaf8dbh0x8mhpf59ppxgw7m174nz8mmcfyjchm3") (f (quote (("std") ("default" "std"))))))

(define-public crate-target-lexicon-0.1.0 (c (n "target-lexicon") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.1") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0dhqja6ac5ns6zfv4aayqcpnc2bnxbjcm57z0fpqb4ga1kc3wxfd") (f (quote (("std") ("default" "std"))))))

(define-public crate-target-lexicon-0.2.0 (c (n "target-lexicon") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1fb4gm1csgw8ngsp5n04c4i09zvfm6bbf91p3mcpv20bgwif5xaa") (f (quote (("std") ("default" "std"))))))

(define-public crate-target-lexicon-0.3.0 (c (n "target-lexicon") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0czndcq18rmbwwzk30vldvfjh75pi9njamj7h4lbvdafrrs3k4nn") (f (quote (("std") ("default" "std"))))))

(define-public crate-target-lexicon-0.4.0 (c (n "target-lexicon") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1jrdch22pvm8r9fwx6d051l4yhac16lq6sn4q5fc6ic95fcb82hv") (f (quote (("std") ("default" "std"))))))

(define-public crate-target-lexicon-0.5.0 (c (n "target-lexicon") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0629py64x670x6nqdn4mwnc1pb46jjidk64pwfc2n7qd8nnavwyq") (f (quote (("std") ("default" "std"))))))

(define-public crate-target-lexicon-0.6.0 (c (n "target-lexicon") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "07112wxvvv7m34mlpkif8hliz4qhwmihzjrzc8w89idm9yyilxak")))

(define-public crate-target-lexicon-0.7.0 (c (n "target-lexicon") (v "0.7.0") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0f5vy6d83bmdji117dnng6y6nv7pdhl4hxkx6jic6nj05i7rin8l")))

(define-public crate-target-lexicon-0.8.0 (c (n "target-lexicon") (v "0.8.0") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1m5vnp4y0gzcrmhpjbv8nmq92y45z58wbj73fcr076fxa0rzmbqi")))

(define-public crate-target-lexicon-0.8.1 (c (n "target-lexicon") (v "0.8.1") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "06kl5bd5llhscla9bpkgfmj195qmn0mll0651ccppmrpdwncnxbr")))

(define-public crate-target-lexicon-0.9.0 (c (n "target-lexicon") (v "0.9.0") (h "1d44wvvgf17afwdb7gr4wc5qr0vz5z5iy4cyb8q8ydrqga512k3g") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.10.0 (c (n "target-lexicon") (v "0.10.0") (h "17diw9c3d1vb5rmwwk2ghsyhfs0gj5jm78hrwxxhmd67vhw743mb") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.11.0 (c (n "target-lexicon") (v "0.11.0") (h "1cm8wzip45zk48gi239cn1hv8jsyif2rhgdm9kyh2aj45aaka9py") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.11.1 (c (n "target-lexicon") (v "0.11.1") (h "1fgw1n3na1j8h811w035rwb5199jq7binfjc60d27dvga27akraf") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.11.2 (c (n "target-lexicon") (v "0.11.2") (h "15gax4765vm2inh45m7cvaf4mgd2mb2mn0i87np0i1d95qhla822") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.11.3 (c (n "target-lexicon") (v "0.11.3") (h "0khq669rji1l0liv9mdxvddklqhxxpk6r06wbdhc18hiilw9xck2") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-target-lexicon-0.12.0 (c (n "target-lexicon") (v "0.12.0") (h "0d7q8hj3qi6jz7r82ha4bqhzwz9b8zfsvnrv2aw18jqy50wkpbk4") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.12.1 (c (n "target-lexicon") (v "0.12.1") (h "1yx646j9nj4hjhza6lvbj192g7idbxn9zdr2xplha40jqjj2srdh") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.12.2 (c (n "target-lexicon") (v "0.12.2") (h "1zsvillq0zsggg3fb0mfmcia0f68wfclahaqc0zgln14pkfzrgyr") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.12.3 (c (n "target-lexicon") (v "0.12.3") (h "1hbs7gs6br3r7hkjw011bnfbddpfla2i9h5klvy8bjrs0iapxynp") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.12.4 (c (n "target-lexicon") (v "0.12.4") (h "1hfk4v8gbhczr6jwsy1ja6yg4npkvznym6b7r4fbgjc0fw428960") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.12.5 (c (n "target-lexicon") (v "0.12.5") (h "0bgiahxqlk0sy3yiz4idzwg2r7gjc3grbyqrwpq9879vhpvd044l") (f (quote (("std") ("default"))))))

(define-public crate-target-lexicon-0.12.6 (c (n "target-lexicon") (v "0.12.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ra8ffg9x2ncn1gx5s4fqnbjkpnqsm1zdipnwbpgrcqxmc69isca") (f (quote (("std") ("serde_support" "serde" "std") ("default"))))))

(define-public crate-target-lexicon-0.12.7 (c (n "target-lexicon") (v "0.12.7") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1if83lyqp0krqac1asii5izrm74vjf567w66dglw6q0dchvs66zx") (f (quote (("std") ("serde_support" "serde" "std") ("default"))))))

(define-public crate-target-lexicon-0.12.8 (c (n "target-lexicon") (v "0.12.8") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1b3zfzynclr8qw4gy7jfimkg6823d6rr7gapf5172imrkqipy70v") (f (quote (("std") ("serde_support" "serde" "std") ("default"))))))

(define-public crate-target-lexicon-0.12.9 (c (n "target-lexicon") (v "0.12.9") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "180iwqwvdk586b1b27anfddq5zbfckisgi3yjhdzaqbsfp5pg3nz") (f (quote (("std") ("serde_support" "serde" "std") ("default"))))))

(define-public crate-target-lexicon-0.12.10 (c (n "target-lexicon") (v "0.12.10") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13mf96n5sqlwb4v7vqw522fzkfp0k36s9camaa9qkasrazpswbqx") (f (quote (("std") ("serde_support" "serde" "std") ("default"))))))

(define-public crate-target-lexicon-0.12.11 (c (n "target-lexicon") (v "0.12.11") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12nwfd1ylqysn1mqf967hc33wcvg0jyvq7hfhmiy5j2825mr23lx") (f (quote (("std") ("serde_support" "serde" "std") ("default"))))))

(define-public crate-target-lexicon-0.12.12 (c (n "target-lexicon") (v "0.12.12") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02lk65ik5ffb8vl9qzq02v0df8kxrp16zih78a33mji49789zhql") (f (quote (("std") ("serde_support" "serde" "std") ("default") ("arch_zkasm"))))))

(define-public crate-target-lexicon-0.12.13 (c (n "target-lexicon") (v "0.12.13") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bmgpdq766zn61f16py0x9139fv314d054xkrkj9iw3q5vd8nxb9") (f (quote (("std") ("serde_support" "serde" "std") ("default") ("arch_zkasm"))))))

(define-public crate-target-lexicon-0.12.14 (c (n "target-lexicon") (v "0.12.14") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bzzr5cq1n56nmjp5fkf2h1g9a27lmkbld3qqfvwy6x2j4w41z71") (f (quote (("std") ("serde_support" "serde" "std") ("default") ("arch_zkasm"))))))

