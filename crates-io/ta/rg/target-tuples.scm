(define-module (crates-io ta rg target-tuples) #:use-module (crates-io))

(define-public crate-target-tuples-0.1.0 (c (n "target-tuples") (v "0.1.0") (h "1makq20m0jfncsl82zy3sss4hfc47zzcrnisa2h3aj1l4r7bg7v0")))

(define-public crate-target-tuples-0.2.0 (c (n "target-tuples") (v "0.2.0") (h "0gmm8q41jj1sfxy3rd3fwmq1h7panhz3dw5am0wjr9j3jrdglcj9")))

(define-public crate-target-tuples-0.2.1 (c (n "target-tuples") (v "0.2.1") (h "0qnby58xnnpc5gyfaz3yygcy2h9x80s9bixzix52ili4fkk9iysy")))

(define-public crate-target-tuples-0.2.2 (c (n "target-tuples") (v "0.2.2") (h "1xgqvrpck5lgyaz6agv0l7w23m91q7306bgrpkxhjm10fmlw0mk3")))

(define-public crate-target-tuples-0.2.3 (c (n "target-tuples") (v "0.2.3") (h "1kxm6x4qz71k01rjdk18baw2cj3kcd14d5srp0avsrfdg12wnqmj")))

(define-public crate-target-tuples-0.3.0 (c (n "target-tuples") (v "0.3.0") (h "0fih6j71slqyxcrhxd05rxknnh5fdxmnik0q58hsq15znnrqffz0")))

(define-public crate-target-tuples-0.4.0 (c (n "target-tuples") (v "0.4.0") (h "03rb933vv5s7dipbcm8z7pbavk83frdnkhmxgd8500awp2c2yx8y") (y #t)))

(define-public crate-target-tuples-0.4.1 (c (n "target-tuples") (v "0.4.1") (h "1zl12km7dygfdav8gzx4z1nhsp804ag6fj2w2x90z1m7mpv803m5")))

(define-public crate-target-tuples-0.4.2 (c (n "target-tuples") (v "0.4.2") (h "1wn4wbrvsw22b6jz3sz6cbf75xnakmvnq6a3dj7fylbilzyiv013")))

(define-public crate-target-tuples-0.5.0 (c (n "target-tuples") (v "0.5.0") (h "13044amq27d8ga0ql4gbw838iyzl1vpgq6b8qwqkmsf37nkxfrxn") (y #t)))

(define-public crate-target-tuples-0.5.1 (c (n "target-tuples") (v "0.5.1") (h "1lsfh5n29mzyj2p6wwgviwv2lw3m4fqbjd79j2cf4wz288yji28q") (y #t)))

(define-public crate-target-tuples-0.5.2 (c (n "target-tuples") (v "0.5.2") (h "16q5hq4kiidpk3lsdmsv1ap4cg265xai9yi3xaxbirb4y93wq19x") (y #t)))

(define-public crate-target-tuples-0.5.3 (c (n "target-tuples") (v "0.5.3") (h "13yf3l9zwxj69j8km9ini1nm0pzs9nj65w969mh3hz1gz4cffbfa") (y #t)))

(define-public crate-target-tuples-0.5.4 (c (n "target-tuples") (v "0.5.4") (h "0zx9mq82jc1xq4gv7shrmcxdmx3dxkj2vha929dn0mdd58khbisi") (y #t)))

(define-public crate-target-tuples-0.5.5 (c (n "target-tuples") (v "0.5.5") (h "1fac0icq1xap72f005ah831ig189gx0525plkmhvad17b4j1f9i5") (y #t)))

(define-public crate-target-tuples-0.5.6 (c (n "target-tuples") (v "0.5.6") (h "0a5yv0z1xr4q793y3af4wjk9hf4sapv6s26knzvzp6qg4s89rl7w")))

(define-public crate-target-tuples-0.5.7 (c (n "target-tuples") (v "0.5.7") (h "0rvqmf32qr62kap0blzls866dz48q39qz5prscb5c2ak7sybwr7p") (y #t)))

(define-public crate-target-tuples-0.5.8 (c (n "target-tuples") (v "0.5.8") (h "0m18061y2m3rfqdzi49ka4j7fnmv0j0qh5nk94ds83c8ma8r6jqa")))

(define-public crate-target-tuples-0.5.9 (c (n "target-tuples") (v "0.5.9") (h "1scsx8wbjrsbqsfn18d4jnn7zrzg8772f3w1b5ybp7c8ql3f46m9")))

(define-public crate-target-tuples-0.5.10 (c (n "target-tuples") (v "0.5.10") (h "08l81k1ilyac4n50rj28l05vx8x0gp2bznfy9y2zly0vhzfnm0yd")))

(define-public crate-target-tuples-0.5.11 (c (n "target-tuples") (v "0.5.11") (h "0s8115mq1q7vgbf15ph0zl0cav7sard7l8nlqvr6fw56h2hdv44x")))

(define-public crate-target-tuples-0.5.12 (c (n "target-tuples") (v "0.5.12") (h "0h01dbaxxgwa72v0k3b0bc2qb9gbk8wzixagyhqw1i23wc9f5z1n")))

(define-public crate-target-tuples-0.5.13 (c (n "target-tuples") (v "0.5.13") (h "126mc8j3k3b620cbpf6ln62zqmnzjxij22h79ja3d9lkc3ll9wkr")))

