(define-module (crates-io ta rg target_build_utils) #:use-module (crates-io))

(define-public crate-target_build_utils-0.1.0 (c (n "target_build_utils") (v "0.1.0") (d (list (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "1sxlk1brjh2xgrwq3rgp8vp8as1lji4k0him8pz4r7ai8p63k5zv")))

(define-public crate-target_build_utils-0.1.1 (c (n "target_build_utils") (v "0.1.1") (d (list (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1aind54awcw1xcfaz3pwcda516ahvm7y13chjxb4x3lh9n6y26vs")))

(define-public crate-target_build_utils-0.1.2 (c (n "target_build_utils") (v "0.1.2") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0z5p6y05marhyf5w6xfv9d3n2z23lnzr4pmp6i9x73314vi51ial")))

(define-public crate-target_build_utils-0.2.0 (c (n "target_build_utils") (v "0.2.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "06pif8g1c6vhhqgl7k96j9qgbr4c6xpjcwm4svxy12dnlldbdrhv")))

(define-public crate-target_build_utils-0.2.1 (c (n "target_build_utils") (v "0.2.1") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1aavjaa6lv22c81c85bwlm8wwfbagrzdn1frla6lw5qm2y6lbzka")))

(define-public crate-target_build_utils-0.3.0 (c (n "target_build_utils") (v "0.3.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.9") (o #t) (d #t) (k 0)))) (h "17v71iswrniikx91pa723zpzbr2fj1igj6yxifjwc68c11cc0bgl") (f (quote (("default" "serde_json"))))))

(define-public crate-target_build_utils-0.3.1 (c (n "target_build_utils") (v "0.3.1") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0p7713x4bpbwi11l196z1mi8ym8qj1cdnab1mm2ffpm2wi516g81") (f (quote (("default" "serde_json"))))))

