(define-module (crates-io ta rg target-triple) #:use-module (crates-io))

(define-public crate-target-triple-0.1.0 (c (n "target-triple") (v "0.1.0") (h "03sk7s362z4ngp1lky1xqm4g7hkaf6rr77qckcrj4kzw6d2nyr8d") (r "1.56")))

(define-public crate-target-triple-0.1.1 (c (n "target-triple") (v "0.1.1") (h "1ay0kirfy7v7grjs8siizpiila6ssh6927ygcsx6gg1ig90rsi21") (r "1.58")))

(define-public crate-target-triple-0.1.2 (c (n "target-triple") (v "0.1.2") (h "0bgqxyix6znplz15zrji8022fv991rplphgf4n9yari67xf9crza") (r "1.58")))

(define-public crate-target-triple-0.1.3 (c (n "target-triple") (v "0.1.3") (h "0y1hq4v4k1706rr3rdmw92as4kchchdxj7qkr2plb025vc6db922") (r "1.58")))

