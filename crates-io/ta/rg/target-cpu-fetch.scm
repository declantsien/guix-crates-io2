(define-module (crates-io ta rg target-cpu-fetch) #:use-module (crates-io))

(define-public crate-target-cpu-fetch-0.1.0 (c (n "target-cpu-fetch") (v "0.1.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "18s8938pn0rhzk06ax2r24gl0jlfcn3x7ih0i0dhb8p4k07hj9sk")))

(define-public crate-target-cpu-fetch-0.1.1 (c (n "target-cpu-fetch") (v "0.1.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "1vsxw1lm1fpvdm57iz0l4f78v9p57msz0gkylw1jqw6akr4c4qmk")))

(define-public crate-target-cpu-fetch-0.1.2 (c (n "target-cpu-fetch") (v "0.1.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "1r75980smcqzgiilvf05101nc8r4v17rwmxv4vgxmg6qgcj3nczn") (y #t)))

(define-public crate-target-cpu-fetch-0.1.3 (c (n "target-cpu-fetch") (v "0.1.3") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "0c8h4d8s6547hxdllfh8jh9wmr6przhq18mif48fm0c9314wj866")))

