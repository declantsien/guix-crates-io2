(define-module (crates-io ta rg targets) #:use-module (crates-io))

(define-public crate-targets-0.1.0 (c (n "targets") (v "0.1.0") (h "0f2r9s0b2s2vc5kyrpdfanbvdp02495i4cqfsjkhd91dfkl83bxa")))

(define-public crate-targets-0.1.1 (c (n "targets") (v "0.1.1") (h "06qvkg9lhv07dl0yssxwmfp6jysy1r062pcrr75kjjj2k7m3b1v4")))

