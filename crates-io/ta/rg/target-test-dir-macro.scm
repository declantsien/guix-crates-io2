(define-module (crates-io ta rg target-test-dir-macro) #:use-module (crates-io))

(define-public crate-target-test-dir-macro-0.2.0 (c (n "target-test-dir-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1bgbpxl137lcjypdlxbiq35jbdpvmfl5fx78gckclz08dicbc2ly")))

(define-public crate-target-test-dir-macro-0.3.0 (c (n "target-test-dir-macro") (v "0.3.0") (d (list (d (n "prettyplease") (r "^0.2.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1v8j4vd3adzfrqg4pnwvry4wi7cg419sdip0nqhv2yql15004d1r")))

