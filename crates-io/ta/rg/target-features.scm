(define-module (crates-io ta rg target-features) #:use-module (crates-io))

(define-public crate-target-features-0.1.0 (c (n "target-features") (v "0.1.0") (h "10rwf0ms3mxj9vq3gwmmh48mlqnbhm5g0yqvp4h5z51kdzhfcl8q") (r "1.61")))

(define-public crate-target-features-0.1.1 (c (n "target-features") (v "0.1.1") (h "1r1mxgpydigm9yy3a35llfhz6jrayzy631fvnna3yg3jbbys8l1s") (r "1.61")))

(define-public crate-target-features-0.1.2 (c (n "target-features") (v "0.1.2") (h "06ncm82b5siyqxqlwnx6wg5yilfn4sfh8i812zzabr251sjsg0hv") (r "1.61")))

(define-public crate-target-features-0.1.3 (c (n "target-features") (v "0.1.3") (h "0dhy95kq2sgh0bk486cjrq0iapksfaykv280r1spsw6103l0v114") (r "1.61")))

(define-public crate-target-features-0.1.4 (c (n "target-features") (v "0.1.4") (h "03hl609f5wwwjhzkn6jzi5nann7gm1k4snyzy56dv6kzqdrv9xh6") (r "1.61")))

(define-public crate-target-features-0.1.5 (c (n "target-features") (v "0.1.5") (h "1gb974chm9aj8ifkyibylxkyb5an4bf5y8dxb18pqmck698gmdfg") (r "1.61")))

(define-public crate-target-features-0.1.6 (c (n "target-features") (v "0.1.6") (h "1m8y0ksw30gnkidjsjvnmhlpj165mgyj8ylk0lbs0qy4qprvkfy1") (r "1.61")))

