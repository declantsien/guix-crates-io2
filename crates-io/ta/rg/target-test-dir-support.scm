(define-module (crates-io ta rg target-test-dir-support) #:use-module (crates-io))

(define-public crate-target-test-dir-support-0.1.0 (c (n "target-test-dir-support") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "033zk8a44vgxrwax1danpf9icxpy9y7pn53vdb71ybd9cx9wbzbv")))

(define-public crate-target-test-dir-support-0.1.1 (c (n "target-test-dir-support") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "038rrnvkbg5bv4r478w9wpnf30psszy2blm3y89g5i0xrpcmpmfs")))

