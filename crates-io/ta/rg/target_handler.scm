(define-module (crates-io ta rg target_handler) #:use-module (crates-io))

(define-public crate-target_handler-0.1.1 (c (n "target_handler") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "042sqjjzicd569b0j4szrv25knx4jif4ymsd1f5c6ddiyjvzixsw")))

