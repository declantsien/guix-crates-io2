(define-module (crates-io ta rg target) #:use-module (crates-io))

(define-public crate-target-0.0.0 (c (n "target") (v "0.0.0") (h "0bra9izlfa13hp5qsdxp2q9rlwnyr84zcdcbi386q2kf25r4bq7s")))

(define-public crate-target-1.0.0 (c (n "target") (v "1.0.0") (h "0ai25bjyj15gsii5z55xwv089zc4na8qjrj4gb433h0cpdjh800h")))

(define-public crate-target-1.1.0 (c (n "target") (v "1.1.0") (d (list (d (n "executable-path") (r "^1.0.0") (d #t) (k 2)))) (h "131a58b65qp6gh8cbx1233g2lzjq8qjhsy7pfny225afqz4zpr6i")))

(define-public crate-target-2.0.0 (c (n "target") (v "2.0.0") (d (list (d (n "executable-path") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "0p5hi5vxcs8w95qmg9hsv985g8kaxjrzjlgsybmf4h13a1qjx1ds")))

(define-public crate-target-2.0.1 (c (n "target") (v "2.0.1") (d (list (d (n "executable-path") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "10n2ikn1jp99hf897azi3djn1va5s64cap4m7gmjkk67801nppzl")))

