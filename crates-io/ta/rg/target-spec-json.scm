(define-module (crates-io ta rg target-spec-json) #:use-module (crates-io))

(define-public crate-target-spec-json-0.0.0 (c (n "target-spec-json") (v "0.0.0") (h "0lla5kysz0jg9acplgmy8s7l4zlv31l2z7c2ydlsgl81jjxp7jhv") (y #t)))

(define-public crate-target-spec-json-0.1.0 (c (n "target-spec-json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "13wbjn9p288n8vjw5ah8i83q4l8ijyknd4yg1dsrll36n0g2af5p") (r "1.57")))

(define-public crate-target-spec-json-0.1.1 (c (n "target-spec-json") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0yqfcrk0k21b931fw79d5hz2xq1rjqg1hwk4q7rxi6gpv83jplab") (r "1.57")))

(define-public crate-target-spec-json-0.1.2 (c (n "target-spec-json") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0qyql8llkq5wjvda03wipfxl96g5fixw4q0n16pahpvz8pr2fw24") (r "1.57")))

(define-public crate-target-spec-json-0.1.3 (c (n "target-spec-json") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "14lgxvl548cgv7mjii69j0gkmjbykl0pmczaanjv5n4ym79dj9sg") (r "1.57")))

(define-public crate-target-spec-json-0.1.4 (c (n "target-spec-json") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0inqlnal54z0if2pj96h6n0chhn2b8cr0lm6kx1klaxxqhfx0hc1") (r "1.57")))

(define-public crate-target-spec-json-0.1.5 (c (n "target-spec-json") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "19cnbbmln3sxjva02dxy94ln1nvh5cxfv47b4sk9spb150c25a56") (r "1.57")))

(define-public crate-target-spec-json-0.1.6 (c (n "target-spec-json") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1ja52vqgl168jf756y0nywqh7kyp0nalds0yzjzwb8qcilxx3fva") (r "1.57")))

(define-public crate-target-spec-json-0.1.7 (c (n "target-spec-json") (v "0.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1wss6liliyclk5b89s6gq15x210gb2k7hmhpciwiw0pamka0cs7r") (r "1.57")))

(define-public crate-target-spec-json-0.1.8 (c (n "target-spec-json") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1sndz0jm9l0v48nk9gi9gz27l65b121vry8zif3qiw2k99p1zrl7") (r "1.57")))

(define-public crate-target-spec-json-0.1.9 (c (n "target-spec-json") (v "0.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "137h74v08vz27c9gj7vdsn44arbxc2i05d7brjfkrqxy3iisrsyc") (r "1.57")))

(define-public crate-target-spec-json-0.1.10 (c (n "target-spec-json") (v "0.1.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0h1nzxb1syc4cd9y378k19ah54bmxkr3k9i2xvbj3vk251cm4hav") (r "1.57")))

(define-public crate-target-spec-json-0.1.11 (c (n "target-spec-json") (v "0.1.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0hkb2d0l0ikcdlvbjgshlyzg8zxyavnbq23bkdmz9gw3z5470hah") (r "1.57")))

(define-public crate-target-spec-json-0.1.12 (c (n "target-spec-json") (v "0.1.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "14mgfax38igskymh3bs4hd6y0mrzz3yi8jcmfvsanvrc0331581g") (r "1.57")))

(define-public crate-target-spec-json-0.1.13 (c (n "target-spec-json") (v "0.1.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "05w96pzpsnzp2rp3d6ahcyvpw8xr00mngbkmbh30qvfwb7f0n4k6") (r "1.57")))

(define-public crate-target-spec-json-0.1.14 (c (n "target-spec-json") (v "0.1.14") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0p60x1fans8c4vyrv74chz1hgq914xsrv767pl90q699gan1zb5m") (r "1.57")))

(define-public crate-target-spec-json-0.1.15 (c (n "target-spec-json") (v "0.1.15") (d (list (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "06r549shaibyi4278hymdh8z0bs0j5w7phg36kyy4lvsy6p7w0ds") (r "1.57")))

(define-public crate-target-spec-json-0.1.16 (c (n "target-spec-json") (v "0.1.16") (d (list (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1d329l7fd0jnm6arznq1v5gdbip98dfhgmhwwbrhfqmfs78khrlz") (r "1.57")))

