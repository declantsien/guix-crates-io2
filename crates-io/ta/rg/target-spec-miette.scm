(define-module (crates-io ta rg target-spec-miette) #:use-module (crates-io))

(define-public crate-target-spec-miette-0.1.0 (c (n "target-spec-miette") (v "0.1.0") (d (list (d (n "guppy-workspace-hack") (r "^0.1") (d #t) (k 0)) (d (n "miette") (r "^5.3.0") (d #t) (k 0)) (d (n "target-spec") (r "^1.2.1") (d #t) (k 0)))) (h "1b9wxs2y4f9rqr44mjh55zpvj1fsw1090k5h391wimlsrpr9jpyh") (r "1.58")))

(define-public crate-target-spec-miette-0.2.0 (c (n "target-spec-miette") (v "0.2.0") (d (list (d (n "guppy-workspace-hack") (r "^0.1") (d #t) (k 0)) (d (n "miette") (r "^5.9.0") (d #t) (k 0)) (d (n "target-spec") (r "^2.0.0") (d #t) (k 0)))) (h "052xld5qqhm29yjncbdwblijnpvmrwc7zhpxzyjlnlmgsfq40d0z") (r "1.66")))

(define-public crate-target-spec-miette-0.3.0 (c (n "target-spec-miette") (v "0.3.0") (d (list (d (n "guppy-workspace-hack") (r "^0.1") (d #t) (k 0)) (d (n "miette") (r "^5.9.0") (d #t) (k 0)) (d (n "target-spec") (r "^3.0.0") (d #t) (k 0)))) (h "102pj1ymzmfnp485prz0dx8kbnh4qir6k5bifs4dp5j0ww04gilm") (r "1.66")))

(define-public crate-target-spec-miette-0.4.0 (c (n "target-spec-miette") (v "0.4.0") (d (list (d (n "guppy-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "miette") (r "^7.0.0") (d #t) (k 0)) (d (n "target-spec") (r "^3.1.0") (d #t) (k 0)))) (h "0n5wnjsxglf93bk8dnm4dnsnjxg4d0xxnxymmax10v18llgdy4gy") (r "1.73")))

