(define-module (crates-io ta rg target-cpu-macro) #:use-module (crates-io))

(define-public crate-target-cpu-macro-0.1.0 (c (n "target-cpu-macro") (v "0.1.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1.0") (d #t) (k 0)))) (h "0qpxx7h3nrjq0psv3wlajipiic49rjwf6api8xz01732h69wjzpk")))

(define-public crate-target-cpu-macro-0.1.1 (c (n "target-cpu-macro") (v "0.1.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1.1") (d #t) (k 0)))) (h "1025lb4v5357g37zslspk2d0mg9hyc5mfzpz8rbzcg587hp68aiq")))

(define-public crate-target-cpu-macro-0.1.2 (c (n "target-cpu-macro") (v "0.1.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1.1") (d #t) (k 0)))) (h "0l9gzlaf82vvszkah9gkxh1s1vk04b1z8p13rhb3aplp7xiyin90") (y #t)))

(define-public crate-target-cpu-macro-0.1.3 (c (n "target-cpu-macro") (v "0.1.3") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1.3") (d #t) (k 0)))) (h "02vnjybama8n86vm63cf0vsl2g4r0d5wiikpy9mxmgsmrirzp8yn")))

