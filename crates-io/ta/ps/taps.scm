(define-module (crates-io ta ps taps) #:use-module (crates-io))

(define-public crate-taps-0.1.0 (c (n "taps") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "011y4q44wizg3n8wki41c0z535s2czc5b7nfkczin5xbh8i0dv9n")))

(define-public crate-taps-0.2.0 (c (n "taps") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1xhnbc7x3k95ps0r36fwbgsc82aazg4y0mz8ghljprlsv48k59mx")))

(define-public crate-taps-0.2.1 (c (n "taps") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1qvl7q7jk6ddz6828hpwkik9s3sv9qckg2i0blg4rvy4vpkg1m3n")))

(define-public crate-taps-0.2.2 (c (n "taps") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "05sddym24lj9vgp4vgqlqdlsb6ry443znvms6nxqyxq9ifxn6gn1")))

