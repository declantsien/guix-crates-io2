(define-module (crates-io ta sv tasvideos-api-rs) #:use-module (crates-io))

(define-public crate-tasvideos-api-rs-0.1.0 (c (n "tasvideos-api-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fmwsx884z9cgx5g1nxxw9y8s9ypaiyjz3agmnqhfrds740nbsga")))

(define-public crate-tasvideos-api-rs-0.2.0 (c (n "tasvideos-api-rs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1y1aimd4bmb2a70wg0gmc20m7a01785lxign0q64wscgy134g4d1")))

(define-public crate-tasvideos-api-rs-0.2.1 (c (n "tasvideos-api-rs") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r ">1.0.0, <=1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0q5vir3s1j357vgy9y7rwjvndjvf39idf74xzzibhw2248r2wk2k")))

(define-public crate-tasvideos-api-rs-0.2.2 (c (n "tasvideos-api-rs") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "074yfbn0l2ij6j6jd8jp4j32zqqniqbwx40k6zhdcr8dxx2cicsi")))

(define-public crate-tasvideos-api-rs-0.3.0 (c (n "tasvideos-api-rs") (v "0.3.0") (d (list (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (f (quote ("json"))) (d #t) (k 0)))) (h "04sd23fq86piimsji0gr49vvbaiq3cy8sb9rvs569wq4117ihm5b")))

