(define-module (crates-io ta gn tagname_derive) #:use-module (crates-io))

(define-public crate-tagname_derive-0.1.0 (c (n "tagname_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y4l36055r9bvsm758w7c7874j6jgrsw86fjhicn34fcd38n58j3") (y #t)))

(define-public crate-tagname_derive-0.1.1 (c (n "tagname_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02hf2kgzglxyryfslf63szdydx65qjpd8mdrcb7j5glfgnm3zqh6")))

(define-public crate-tagname_derive-0.2.0 (c (n "tagname_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gyak8dvxcmxr9p6mppzj56c5db0pl2dp7za6fghwdsw9jpaikjf")))

(define-public crate-tagname_derive-0.2.1 (c (n "tagname_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yzvc806429i3r28d582qlc17k5f495i6v3h1vwadbzshc7c5h8a")))

(define-public crate-tagname_derive-0.3.0 (c (n "tagname_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bf3w16kzvz6zpbmi8nsijgd1ajxdhhsd6lv118xks1gqffmg3n8")))

(define-public crate-tagname_derive-0.3.1 (c (n "tagname_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0almvx52g5f7ifwr1rzw28zkyyvgpdh3pm8zjk2si523nvrd1mf6")))

