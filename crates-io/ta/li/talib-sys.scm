(define-module (crates-io ta li talib-sys) #:use-module (crates-io))

(define-public crate-talib-sys-0.1.0 (c (n "talib-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "zip-extract") (r "^0.1.2") (d #t) (k 1)))) (h "16wbbvad559glnyc4mjrcahj2fm8zb0wpczjlpy1wr0n5hv710z6")))

