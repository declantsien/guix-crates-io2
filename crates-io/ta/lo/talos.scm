(define-module (crates-io ta lo talos) #:use-module (crates-io))

(define-public crate-talos-0.1.0 (c (n "talos") (v "0.1.0") (d (list (d (n "alea") (r "^0.2") (d #t) (k 0)) (d (n "compute") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "reverse") (r "^0.2") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)) (d (n "talos_procs") (r "^0.1") (d #t) (k 0)))) (h "030rv8wk7d3plh2qvypbdhznfnjm2si5zl816nd72chxx1phg7ff")))

