(define-module (crates-io ta lo talos_common_utils) #:use-module (crates-io))

(define-public crate-talos_common_utils-0.1.0 (c (n "talos_common_utils") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "1ld6hnmg5wsxq645gky5zjxxia9xsb1w4n2bdns608w6863j6ris") (y #t)))

(define-public crate-talos_common_utils-0.2.0 (c (n "talos_common_utils") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "1xyn2szs5rwihzb3sqfcirya3mhrbxpicp3kwgpdnl6vfngj32cn") (y #t)))

(define-public crate-talos_common_utils-0.2.4 (c (n "talos_common_utils") (v "0.2.4") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "038qp2zbvyslnw3rlxv5771cpas6jyshaqk6lxwya046hm39fcxm") (y #t)))

(define-public crate-talos_common_utils-0.2.6 (c (n "talos_common_utils") (v "0.2.6") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "0khzyv908xmj5hmlm12gqyip1g3zs0xqjxg6mhayr033phpriz2q")))

(define-public crate-talos_common_utils-0.2.8 (c (n "talos_common_utils") (v "0.2.8") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "11djlg1sxqdjfqv6kxzganjqiblym9kgy4n42mscf422gc5h6bvs") (y #t)))

(define-public crate-talos_common_utils-0.2.9 (c (n "talos_common_utils") (v "0.2.9") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "07k49q8xdn6c07mskml8hxpbic1imcs4n0jxz3lzjmn115biv2dl") (y #t)))

(define-public crate-talos_common_utils-0.2.10 (c (n "talos_common_utils") (v "0.2.10") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "00kfmp9lgj98f6zjlgybz9nawl3gyxykwz7x9x25arxli4ziv53s")))

