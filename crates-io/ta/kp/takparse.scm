(define-module (crates-io ta kp takparse) #:use-module (crates-io))

(define-public crate-takparse-0.1.0 (c (n "takparse") (v "0.1.0") (h "0i7axksh408l6k1vkdygxfvzc70hlrf0r065wj37vmbwibbyqdnf")))

(define-public crate-takparse-0.2.0 (c (n "takparse") (v "0.2.0") (h "0j7aichb5n0ilii2xwg5gj601296js5905cqq6l1qh330iaqk2zz")))

(define-public crate-takparse-0.2.1 (c (n "takparse") (v "0.2.1") (h "1acmm22nb7rk8h5g85g7k9f4ww2z23g1l08v0j19wyls7ss37k8w")))

(define-public crate-takparse-0.3.0 (c (n "takparse") (v "0.3.0") (h "1m79vnjgds4cnqhllmscz0zqv8jazn2s4i2bp7i0n9245kl4js56")))

(define-public crate-takparse-0.4.0 (c (n "takparse") (v "0.4.0") (h "0y31s546pjn6lmzl32knf40pqfvigd7i4hiy9g1yfgjisv4k8f3f")))

(define-public crate-takparse-0.5.0 (c (n "takparse") (v "0.5.0") (h "0fpwfa9x52fjk4q3bwh8rgam24s05kwb3z71lj8zq7l8w7wsgp3h")))

(define-public crate-takparse-0.5.1 (c (n "takparse") (v "0.5.1") (h "0w48rx83f6229i9gd3128cm1kr5ncdc4bdmdzb9lflm4z5sn5ry1")))

(define-public crate-takparse-0.5.2 (c (n "takparse") (v "0.5.2") (h "1v8s0mx79431p83hkgypd7dcmhsikb95g3qpw6wqxwnq8x44zn6p")))

(define-public crate-takparse-0.5.3 (c (n "takparse") (v "0.5.3") (h "0gvh2q2q6g27cyr6gmgq60c7h7zm2v5cmc0vibyh7k17kjg4q7mb")))

(define-public crate-takparse-0.5.4 (c (n "takparse") (v "0.5.4") (h "1kbwnks14clfxxljdm35x1g7n5vhzdqm2kdfk8n9fj3rxziary3y")))

(define-public crate-takparse-0.5.5 (c (n "takparse") (v "0.5.5") (h "0ij81y5ysb92jjvbzms1kpd1i4w0blxb02rynsz8b59jd9ighsv1")))

(define-public crate-takparse-0.6.0 (c (n "takparse") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0xvn3x3bs5yc0vlxfghsq9nvcz7hvarfdraz7jb6ppwcs54f83ji") (r "1.65")))

