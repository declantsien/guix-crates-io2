(define-module (crates-io ta na tanaka) #:use-module (crates-io))

(define-public crate-tanaka-0.1.0 (c (n "tanaka") (v "0.1.0") (d (list (d (n "document-features") (r "^0.2.7") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)) (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "xz-decom") (r "^0.2.0") (d #t) (k 1)))) (h "0b4qw2vyw53r11is025zlswrxil4bw13vpiy0c5hb52m90pkiww3") (f (quote (("include_subset") ("include") ("default" "include" "include_subset"))))))

