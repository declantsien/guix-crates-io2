(define-module (crates-io ta ct tactic) #:use-module (crates-io))

(define-public crate-tactic-1.0.0 (c (n "tactic") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dm7i9dj8cn3zwv2314jnns3blil77xi32j9n9pjzcjwdlb4a1sw")))

(define-public crate-tactic-1.1.0 (c (n "tactic") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0421775ik119lpba8i3nhdhmsz2nczqbl6g9jm7ji2a3rnnxqjmc")))

