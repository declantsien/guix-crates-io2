(define-module (crates-io ta la talamel) #:use-module (crates-io))

(define-public crate-talamel-0.2.0 (c (n "talamel") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lfbh6ipdh2hs8150qvi8ab5v99wgx2ww5zgmvkivn1vja9n3s8w")))

