(define-module (crates-io ta in tain) #:use-module (crates-io))

(define-public crate-tain-0.1.0 (c (n "tain") (v "0.1.0") (h "13iavfza3x4w3rm74gjp05xhxgryyd6laf53azgqql1nx6b9bhs1")))

(define-public crate-tain-0.2.0 (c (n "tain") (v "0.2.0") (d (list (d (n "fake") (r "^2.9.2") (d #t) (k 0)) (d (n "testcontainers") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 0)))) (h "101mlswv4gcq5xhsghvia9z4gnpxgkllvpf5pwd8n2rkn230sv8q")))

