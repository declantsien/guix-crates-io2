(define-module (crates-io ta n_ tan_lint) #:use-module (crates-io))

(define-public crate-tan_lint-0.5.0 (c (n "tan_lint") (v "0.5.0") (d (list (d (n "lsp-types") (r "^0.94") (d #t) (k 0)) (d (n "tan") (r "^0.5") (d #t) (k 0)))) (h "18df0zmsr6mw7piyvirfgz3gn6qm8fyk69ghn59260923lbsxax7")))

(define-public crate-tan_lint-0.6.0 (c (n "tan_lint") (v "0.6.0") (d (list (d (n "lsp-types") (r "^0.94") (d #t) (k 0)) (d (n "tan") (r "^0.6") (d #t) (k 0)))) (h "0yzn6dsgw8rigdximlp8x2nqzq0r0dgcdxb685pbsj016xffdia3")))

