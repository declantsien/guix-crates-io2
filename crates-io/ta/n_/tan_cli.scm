(define-module (crates-io ta n_ tan_cli) #:use-module (crates-io))

(define-public crate-tan_cli-0.3.0 (c (n "tan_cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^10") (d #t) (k 0)) (d (n "tan") (r "^0.3") (d #t) (k 0)) (d (n "tan_fmt") (r "^0.3") (d #t) (k 0)))) (h "1lkqk53p2nrawc9g2w90sp8jhirv0m2hg4x6vv108n5yx10shd5n")))

(define-public crate-tan_cli-0.4.0 (c (n "tan_cli") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^10") (d #t) (k 0)) (d (n "tan") (r "^0.4") (d #t) (k 0)) (d (n "tan_fmt") (r "^0.4") (d #t) (k 0)))) (h "0npkfcgz697qdz2wy7p62nw5ksv4wvfc8sz0j759gwkbbpsni7nz")))

(define-public crate-tan_cli-0.5.0 (c (n "tan_cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^10") (d #t) (k 0)) (d (n "tan") (r "^0.5") (d #t) (k 0)) (d (n "tan_fmt") (r "^0.5") (d #t) (k 0)))) (h "1jc090mjib3hfbgvy5w4nz6src7vswn2727pkkh17rkw8nb2nis4")))

(define-public crate-tan_cli-0.6.0 (c (n "tan_cli") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "tan") (r "^0.6") (d #t) (k 0)) (d (n "tan_fmt") (r "^0.6") (d #t) (k 0)) (d (n "tan_lint") (r "^0.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1ap81kacs9lb3bi88lld4k8ip7699qnalzlvx0gjyvfd2i9avlbm")))

