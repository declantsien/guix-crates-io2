(define-module (crates-io ta n_ tan_fmt) #:use-module (crates-io))

(define-public crate-tan_fmt-0.3.0 (c (n "tan_fmt") (v "0.3.0") (d (list (d (n "tan") (r "^0.3") (d #t) (k 0)))) (h "0lykclvwilrazksyyk2xbx99h5absvc1hwyvfl7cgv13462jkpqb")))

(define-public crate-tan_fmt-0.4.0 (c (n "tan_fmt") (v "0.4.0") (d (list (d (n "tan") (r "^0.4") (d #t) (k 0)))) (h "1aym0zmj1lhl8bm8apd4gc1j1c040x19gvwv1f75v2ysrzmgjd1k")))

(define-public crate-tan_fmt-0.5.0 (c (n "tan_fmt") (v "0.5.0") (d (list (d (n "tan") (r "^0.5") (d #t) (k 0)))) (h "1ya2g6pki7al4ihvb53wip2y39mdv2c555kdiky3sj2ikf7ggbha")))

(define-public crate-tan_fmt-0.6.0 (c (n "tan_fmt") (v "0.6.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.6") (d #t) (k 0)))) (h "1829m41968wdpzs46yhdk5my8fwpyyxyff1p47mwkrs8rn15cdzh")))

(define-public crate-tan_fmt-0.7.0 (c (n "tan_fmt") (v "0.7.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.7") (d #t) (k 0)))) (h "14rbpa26v6sd57jv85xa80f51c9hmp20hpkp0pgz3f1y4pw2mch4")))

