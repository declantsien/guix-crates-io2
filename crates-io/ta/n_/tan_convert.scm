(define-module (crates-io ta n_ tan_convert) #:use-module (crates-io))

(define-public crate-tan_convert-0.5.0 (c (n "tan_convert") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.5") (d #t) (k 0)) (d (n "tan_fmt") (r "^0.5") (d #t) (k 0)))) (h "0wa6qwm43a2c5m4ilil48q6isd9d5xa01bbzk00vp86r1687mk2s")))

(define-public crate-tan_convert-0.6.0 (c (n "tan_convert") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.6") (d #t) (k 0)) (d (n "tan_fmt") (r "^0.6") (d #t) (k 0)))) (h "1fwqihy1y0ynp07fad4w2p976r19qnhnr1r6kb67zg6kv06if1lh")))

