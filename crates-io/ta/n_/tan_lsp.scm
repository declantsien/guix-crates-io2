(define-module (crates-io ta n_ tan_lsp) #:use-module (crates-io))

(define-public crate-tan_lsp-0.4.0 (c (n "tan_lsp") (v "0.4.0") (d (list (d (n "lsp-types") (r "=0.93.1") (d #t) (k 0)))) (h "0rbpzv6wxicnwa1h0vyd1kbxnvmddjnmmm2yxnbgrg8lwxhawf5v")))

(define-public crate-tan_lsp-0.5.0 (c (n "tan_lsp") (v "0.5.0") (d (list (d (n "lsp-types") (r "^0.94") (d #t) (k 0)))) (h "0iqg37h0p7w16ihnsbfxg5lf42370aair3dywhpjf4zylgqfv460")))

