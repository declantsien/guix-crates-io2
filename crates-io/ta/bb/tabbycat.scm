(define-module (crates-io ta bb tabbycat) #:use-module (crates-io))

(define-public crate-tabbycat-0.1.0 (c (n "tabbycat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0z4ij98yvi3y0b7zs4m8ws32xhwz6if1dgc6hf9160bj74xbbhv0") (f (quote (("attributes"))))))

(define-public crate-tabbycat-0.1.1 (c (n "tabbycat") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17p7c90pkds3jf4wk61avpgaad41ym6c650j2ba9j3fx3rxjqqqp") (f (quote (("attributes"))))))

(define-public crate-tabbycat-0.1.2 (c (n "tabbycat") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18sny85iqln63pmc073xbw3vpirwpjr1f6xy8m2pn6arz3q90mf4") (f (quote (("attributes"))))))

(define-public crate-tabbycat-0.1.3 (c (n "tabbycat") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wzizd5292xzh9hlsjly7fprrw0mriajnlp5dgx10r4qz0nqk1fb")))

