(define-module (crates-io ta bb tabby) #:use-module (crates-io))

(define-public crate-tabby-0.1.0 (c (n "tabby") (v "0.1.0") (h "0nk17ly7x4fqyl91alps4y5xwgrbamgzhf7pc7xclcni4ph767ys") (y #t)))

(define-public crate-tabby-0.0.1 (c (n "tabby") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.14") (d #t) (k 0)))) (h "13ifx7w2y46alcws830jgjg55qhy3lpwai4bdlxbg3wdnh3c2v8s")))

(define-public crate-tabby-0.0.2 (c (n "tabby") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.14") (d #t) (k 0)))) (h "1s6rn1niqzp7hjl4yns1lspn1pklm7vz535hw0ia70vvhkqb3pvh")))

(define-public crate-tabby-0.0.3 (c (n "tabby") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.14") (d #t) (k 0)))) (h "0zbdi006ydx6z3pqfa908f1y84a59idylff3j0xkclnc8bh4r6gw")))

(define-public crate-tabby-0.0.4 (c (n "tabby") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.14") (d #t) (k 0)))) (h "0lakjd8wcyiq9spd8y9vyawrwbr1bin7jqqh5hcm98wqm030l1jn")))

(define-public crate-tabby-0.0.5 (c (n "tabby") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.14") (d #t) (k 0)))) (h "1ghh2fz9gc4cpfzk94q7dcmb0p9wxml4jdvlfcsz6951jb14rqbf")))

(define-public crate-tabby-0.0.6 (c (n "tabby") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.14") (d #t) (k 0)))) (h "12s4hxvfqmgr4ij4sdqw2zrh00iyw8ipbscpdrlnnyzx1ni0b9sf")))

