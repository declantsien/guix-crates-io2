(define-module (crates-io ta bb tabb) #:use-module (crates-io))

(define-public crate-tabb-0.1.0 (c (n "tabb") (v "0.1.0") (d (list (d (n "atty") (r "0.2.*") (d #t) (k 0)) (d (n "structopt") (r "0.2.*") (d #t) (k 0)) (d (n "termion") (r "1.*") (d #t) (k 0)))) (h "1lzj3zxyl8gahymm6jg7g4s8w5rx72a9m0ga5sfidll7jl3xaq96")))

