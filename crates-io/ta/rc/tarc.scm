(define-module (crates-io ta rc tarc) #:use-module (crates-io))

(define-public crate-tarc-0.1.0 (c (n "tarc") (v "0.1.0") (h "027v2pvb79mqk5fxrldw4csvi4wrllvzn0zm336s1wp7qraawb6x") (y #t)))

(define-public crate-tarc-0.1.1 (c (n "tarc") (v "0.1.1") (h "1hv5dz5vf41db9bidqaw4mpa3w1wcj3791nyg6clb4l8r9s2cl7c")))

(define-public crate-tarc-0.1.2 (c (n "tarc") (v "0.1.2") (h "1xc352vr8b02rzk3xaajzr28aagf571c19jbqpj905jms0ha5kdn") (f (quote (("std") ("default" "std"))))))

(define-public crate-tarc-0.1.3 (c (n "tarc") (v "0.1.3") (h "18libz04i4mgnhazfci200aw6iqsk5xmp6iw4pz4pk43gas2bwc7") (f (quote (("std") ("default" "std"))))))

(define-public crate-tarc-0.1.4 (c (n "tarc") (v "0.1.4") (h "071fxvs8g0rxsdzw1fclqxhy01g31qqw5q6s5bglg3vi6k6rgnl8") (f (quote (("std") ("default" "std"))))))

