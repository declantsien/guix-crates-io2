(define-module (crates-io ta ck tackt-macros) #:use-module (crates-io))

(define-public crate-tackt-macros-0.1.0 (c (n "tackt-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1ml5dkydvw4m963cjm8w6xvps10szhiq83fsc3md5zk89wwqd44c") (y #t)))

(define-public crate-tackt-macros-0.1.1 (c (n "tackt-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "06hdpzszisimizddj9acvs7bx20n3gna83cwihbyg1g6lqaw1k53")))

