(define-module (crates-io ta ck tack) #:use-module (crates-io))

(define-public crate-tack-0.1.0-alpha (c (n "tack") (v "0.1.0-alpha") (h "19hb8dv8c0gd1hlkjvrly71qx5mm0y0i18ycv53agrw5pi5p6qbj")))

(define-public crate-tack-0.1.1-alpha (c (n "tack") (v "0.1.1-alpha") (h "13mb917fdivn38xkhgbrccvn8g34qiljn60wjizlhmq8ih9syadx")))

