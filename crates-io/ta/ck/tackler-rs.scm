(define-module (crates-io ta ck tackler-rs) #:use-module (crates-io))

(define-public crate-tackler-rs-0.1.0 (c (n "tackler-rs") (v "0.1.0") (h "0bg1c15qh3krdz4m218pgl6jpfkl2xsyhpz169iwidjdgbhs0x5b")))

(define-public crate-tackler-rs-0.1.1 (c (n "tackler-rs") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tackler-api") (r "^0.1.0") (d #t) (k 0)) (d (n "tackler-core") (r "^0.1.0") (d #t) (k 0)))) (h "0jah977ai0bgmw25n3knq312qhgd3p83vfc8zbcbqr8kybk6dq3n")))

(define-public crate-tackler-rs-0.1.2 (c (n "tackler-rs") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tackler-api") (r "^0.1.0") (d #t) (k 0)) (d (n "tackler-core") (r "^0.1.0") (d #t) (k 0)))) (h "1jjlma3c29zqhbnjr68s2ssq5jihiy25srk9p1mwjrliz83i6f0m")))

(define-public crate-tackler-rs-0.1.3 (c (n "tackler-rs") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tackler-api") (r "^0.1.0") (d #t) (k 0)) (d (n "tackler-core") (r "^0.1.0") (d #t) (k 0)))) (h "1h87gmpg0dkmhjzv65fkwbkz4j296y303dal809grlfixm14m05n")))

(define-public crate-tackler-rs-0.2.0 (c (n "tackler-rs") (v "0.2.0") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0wvvq3p08adlxsfs6rkgcjmdbrhfb307pv63lm2fpwwcngq1mpkq")))

