(define-module (crates-io ta ck tackt) #:use-module (crates-io))

(define-public crate-tackt-0.1.0 (c (n "tackt") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "tcp" "http1" "http2"))) (k 2)) (d (n "pin-project") (r "^1.0") (k 0)) (d (n "tackt-macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt"))) (k 2)) (d (n "tower-service") (r "^0.3") (k 0)))) (h "13rvl1sd47yg0pfxh6kkybr4cqz4qwacf3frl91hv8izs7bfsx38") (f (quote (("macros" "tackt-macros") ("default" "macros")))) (y #t)))

(define-public crate-tackt-0.1.1 (c (n "tackt") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "tcp" "http1" "http2"))) (k 2)) (d (n "pin-project") (r "^1.0") (k 0)) (d (n "tackt-macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt"))) (k 2)) (d (n "tower-service") (r "^0.3") (k 0)))) (h "1nfm3hkn0pfivndr7djqqiihf0cbmk5v2jn5w6iiwjx92wzfwnqa") (f (quote (("macros" "tackt-macros") ("default" "macros"))))))

(define-public crate-tackt-0.1.2 (c (n "tackt") (v "0.1.2") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "tcp" "http1" "http2"))) (k 2)) (d (n "pin-project") (r "^1.0") (k 0)) (d (n "tackt-macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt"))) (k 2)) (d (n "tower-service") (r "^0.3") (k 0)))) (h "0csc22bha9pdxi30ssffmin364b83kikic0r8q21d04nbg1k7s9f") (f (quote (("macros" "tackt-macros") ("default" "macros"))))))

