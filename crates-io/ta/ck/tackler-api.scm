(define-module (crates-io ta ck tackler-api) #:use-module (crates-io))

(define-public crate-tackler-api-0.1.0 (c (n "tackler-api") (v "0.1.0") (h "1p10py59kx4lcqjd2zpiyjf22yl17436xk0rj69wbjdr0xcqkbyf")))

(define-public crate-tackler-api-0.2.0 (c (n "tackler-api") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock" "serde"))) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29") (f (quote ("serde-with-arbitrary-precision"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("std" "arbitrary_precision"))) (k 0)) (d (n "serde_regex") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("serde"))) (k 0)) (d (n "tackler-rs") (r "^0.2.0") (d #t) (k 2)))) (h "0pm1jy1nzf58vpimmjvfxbjxsv73d2ra0v0mcsvcvrwq4jcvnffa")))

