(define-module (crates-io ta ck tackler) #:use-module (crates-io))

(define-public crate-tackler-0.1.0 (c (n "tackler") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tackler-api") (r "^0.1.0") (d #t) (k 0)) (d (n "tackler-core") (r "^0.1.0") (d #t) (k 0)))) (h "0i0c5h8dkcria01m6l2p5qgzvb1isl455ryfscpqmas2mdxki29n")))

(define-public crate-tackler-0.2.0 (c (n "tackler") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tackler-api") (r "^0.2.0") (d #t) (k 0)) (d (n "tackler-core") (r "^0.2.0") (d #t) (k 0)) (d (n "tackler-rs") (r "^0.2.0") (d #t) (k 0)))) (h "0lr5x90nd37gyrl96r20a6yg3r3x3z1v20p283c51mram7r76dqd")))

