(define-module (crates-io ta ck tack-it-on) #:use-module (crates-io))

(define-public crate-tack-it-on-1.0.0 (c (n "tack-it-on") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)))) (h "0lyn1b00m7zxzl6j2knxdq1r67ny7vpjl822gczglbvb7jzf900h")))

(define-public crate-tack-it-on-1.0.1 (c (n "tack-it-on") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)))) (h "1xqlgyxk7yydzi860l7anlk5gkbqaim3n3lc190mkpa0ndilg306")))

