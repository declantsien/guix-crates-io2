(define-module (crates-io ta ck tackler-core) #:use-module (crates-io))

(define-public crate-tackler-core-0.1.0 (c (n "tackler-core") (v "0.1.0") (d (list (d (n "antlr-rust") (r "^0.3.0-beta") (d #t) (k 0)))) (h "0sg6jkrwnm5gxqc6aj3rjj7m6sxd5nlcx7x0i8j1p63pappaihap")))

(define-public crate-tackler-core-0.2.0 (c (n "tackler-core") (v "0.2.0") (d (list (d (n "antlr-rust") (r "^0.3.0-beta") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "digest") (r "^0.10.6") (d #t) (k 0)) (d (n "gix") (r "^0.43.0") (f (quote ("max-performance-safe"))) (k 0)) (d (n "gix-hash") (r "^0.10.3") (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29") (f (quote ("serde-with-arbitrary-precision"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "tackler-api") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("serde"))) (k 0)) (d (n "tackler-rs") (r "^0.2.0") (d #t) (k 2)))) (h "07559ads5bl5qidyy0qyxw2b4g7dyf0ih1x64q964jfbka9dmg8n")))

