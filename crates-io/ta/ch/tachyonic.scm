(define-module (crates-io ta ch tachyonic) #:use-module (crates-io))

(define-public crate-tachyonic-0.0.1 (c (n "tachyonic") (v "0.0.1") (d (list (d (n "nalgebra") (r "0.10.*") (d #t) (k 0)) (d (n "orbclient") (r "^0.2.11") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.11") (d #t) (k 0)))) (h "1dk2mambldvbnax0nhc3gh6gqf2yyjc1lxdjhha5h4b97csm86q4")))

