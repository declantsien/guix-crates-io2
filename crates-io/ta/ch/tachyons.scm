(define-module (crates-io ta ch tachyons) #:use-module (crates-io))

(define-public crate-tachyons-0.1.0 (c (n "tachyons") (v "0.1.0") (h "0jry31w7f7scbplp2l9d0vls3m1hnsc03pvrzwgszz3zmbqvlxqc")))

(define-public crate-tachyons-0.1.1 (c (n "tachyons") (v "0.1.1") (d (list (d (n "html-index") (r "^0.1.0") (d #t) (k 2)))) (h "1xyjj65pcs0fbg2d152kvibycnvrb2ghvv7zda7x9d1j2mxhrm16")))

