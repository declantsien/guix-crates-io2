(define-module (crates-io ta g_ tag_nominatim) #:use-module (crates-io))

(define-public crate-tag_nominatim-0.1.0 (c (n "tag_nominatim") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "114lp3ig6xkzc06yraiky0sdkpka7a8vy8ki5ksr2zqjmkjlckgs") (f (quote (("rustls" "reqwest/rustls-tls") ("default" "reqwest/default-tls"))))))

