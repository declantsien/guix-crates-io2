(define-module (crates-io ta g_ tag_safe) #:use-module (crates-io))

(define-public crate-tag_safe-0.0.1 (c (n "tag_safe") (v "0.0.1") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "0xaqm5aihi8lwlmkqqsmpk8xx7k17icdh6if9ccilysq3jqic457")))

(define-public crate-tag_safe-0.0.3 (c (n "tag_safe") (v "0.0.3") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "1h2m2xma6y9280zbv7b207lxm6290hnl9yrkmrrpmdlwdfbcj441")))

(define-public crate-tag_safe-0.0.4 (c (n "tag_safe") (v "0.0.4") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "17as0q3spr0app1l6g4vrh08k2r1p06ww91p7121j0hmkd5q1dal")))

(define-public crate-tag_safe-0.0.5 (c (n "tag_safe") (v "0.0.5") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "0xajdx3j00qx6i9x3wlhl6asb7aqv47jnnlsfv3ba5jrxj0arsiy")))

(define-public crate-tag_safe-0.0.6 (c (n "tag_safe") (v "0.0.6") (d (list (d (n "compiletest_rs") (r "^0.0") (d #t) (k 2)))) (h "1b3iqsvqg7gb1v6qwajqyiyh0860rgzsy7hvvjgc5bcqhphx5pv3")))

(define-public crate-tag_safe-0.0.7 (c (n "tag_safe") (v "0.0.7") (d (list (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)))) (h "1mvfvcsvdlws280v6b4846dphdixl1lmbx779aw8bqaqamfc6hk3")))

(define-public crate-tag_safe-0.0.8 (c (n "tag_safe") (v "0.0.8") (d (list (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)))) (h "013cda272yk5gvshw2dnidldmrfsl1wjndqkcazhfhnkv146fqpf")))

(define-public crate-tag_safe-0.1.0 (c (n "tag_safe") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)))) (h "0pg4v7drmgrn9cx2rqgb683shl725nf1l71q6kiwvl4njbchfiir")))

(define-public crate-tag_safe-0.1.1 (c (n "tag_safe") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)))) (h "1knqqlpj411kxdqf7k2b164d0ipvhgvwdbm01pvg6b6qfpdlb9jb")))

(define-public crate-tag_safe-0.2.0 (c (n "tag_safe") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "1wlpfwhmlwcpp3r89pdy56qr9pv65i4kcnks2dkqcfq6hxm8hq81")))

(define-public crate-tag_safe-0.2.1 (c (n "tag_safe") (v "0.2.1") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0skzcnpbyhk7bz0hhgisivl03a9hg48y011vsvydmzxz8d3vc5qr")))

(define-public crate-tag_safe-0.2.2 (c (n "tag_safe") (v "0.2.2") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0cajks3wazgl0y3ijlffj5haci7752jiccihvrkrrdb1qf84za9y")))

(define-public crate-tag_safe-0.2.3 (c (n "tag_safe") (v "0.2.3") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "1vhl1y4im3ddvp6glwyygw7zd3ssrrsh33kxccfv42zn3z16jz0w")))

(define-public crate-tag_safe-0.2.4 (c (n "tag_safe") (v "0.2.4") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0lpldbbszfw81vk48m4kwlgzj5vlhxx3fqvg2gzhxbyaa9f8b1gx")))

(define-public crate-tag_safe-0.2.5 (c (n "tag_safe") (v "0.2.5") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "1wlrsciyxz5w4gs7a6idi77n4acr7v8vqqlzaigv5ywr6pwxr3kg")))

(define-public crate-tag_safe-0.2.6 (c (n "tag_safe") (v "0.2.6") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "01rm5y64zcsrzhnrv7zkfbj7bgd73g67n8qn1lnd5pfqbdh0gk74")))

(define-public crate-tag_safe-0.2.7 (c (n "tag_safe") (v "0.2.7") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "1z7i720wh7pjnlzj3765an26j7zgrdx23igky51d66hhrmbxisdr")))

(define-public crate-tag_safe-0.2.8 (c (n "tag_safe") (v "0.2.8") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0yrb46ks70kv74rp87w6v3gxd94b7s3jpzn281x0q518m0m5y4iv")))

(define-public crate-tag_safe-0.2.11 (c (n "tag_safe") (v "0.2.11") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "020q7ypfkc2qngrqcb52j7kl87wv1h1dpyg0x0iblls3w29bqsr9")))

(define-public crate-tag_safe-0.2.12 (c (n "tag_safe") (v "0.2.12") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1rvcr9d82zqy30njy3z0nixyb6pj7h0q5nxlhh7ds703m3nf4yig")))

(define-public crate-tag_safe-0.2.13 (c (n "tag_safe") (v "0.2.13") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0nxydj3kq7vsqkmpbzfxsbgfj0jnjhxmdjcak38kj29gf2gh17sx")))

(define-public crate-tag_safe-0.2.14 (c (n "tag_safe") (v "0.2.14") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0gsal82gv2l809pwpalfk61s817bgd1qkc32mlf57q8a1qd2y162")))

(define-public crate-tag_safe-0.2.15 (c (n "tag_safe") (v "0.2.15") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0269pz1djdmfqd0gfj25p8xry523h9pxbhqw78qr3wwjcn96wdhw")))

(define-public crate-tag_safe-0.2.16 (c (n "tag_safe") (v "0.2.16") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0dkf3yh7h2bni2vncxf08xicrs7c85k0rw62ssbrd7dy7pwfcn9q")))

(define-public crate-tag_safe-0.2.17 (c (n "tag_safe") (v "0.2.17") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "10fwb1rjil9nlb1wgr4h8j85n79l3h7ysxy2h94vm8fpl2dnlpka")))

(define-public crate-tag_safe-0.2.18 (c (n "tag_safe") (v "0.2.18") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1dmvd2v4n2b9pfj51ljlk8rhh3z2if5qfjkirm6gzvc7ska2h7by")))

(define-public crate-tag_safe-0.2.19 (c (n "tag_safe") (v "0.2.19") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1076bndqqsasjapf5h81xjq9wzd101m49x3cxzs0slm0dqbpf4gi")))

(define-public crate-tag_safe-0.2.20 (c (n "tag_safe") (v "0.2.20") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0n76vfhrscjlv6xw6fvz2p0blkq0x9jvx1sa3zhkm30n774avsv7")))

(define-public crate-tag_safe-0.2.21 (c (n "tag_safe") (v "0.2.21") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1zb68iqnz09c0q4kha9qdkibx36bja0a6fnz5wdy3w1x38xfkdgb")))

(define-public crate-tag_safe-0.2.22 (c (n "tag_safe") (v "0.2.22") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "161m6mjhq5qlyb6xjz90s6b9dmdwp1nz7iiv3cbhk3hkvxzk6li0")))

(define-public crate-tag_safe-0.2.23 (c (n "tag_safe") (v "0.2.23") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "199h0r2siw1zbb3i4vbmfk0180wwxg9d61mvbd4h0wg9jwq1ahpi")))

(define-public crate-tag_safe-0.2.24 (c (n "tag_safe") (v "0.2.24") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "02bj9nb12hszmx6fx0g4001p3yky916icrikrg77idbly6j74f3r")))

(define-public crate-tag_safe-0.2.25 (c (n "tag_safe") (v "0.2.25") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1c3ndi6cip2bba2m62vr1az50nny1biwjaxwjcrg9sdravykb4dm")))

