(define-module (crates-io ta ic taichi-sys) #:use-module (crates-io))

(define-public crate-taichi-sys-0.0.1+1.3.0 (c (n "taichi-sys") (v "0.0.1+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 1)))) (h "0kif0dyp22yqwh7l8zrrl8azzqahw9dh976i1yg70pv7z5gpbr7w") (l "taichi_c_api")))

(define-public crate-taichi-sys-0.0.2+1.3.0 (c (n "taichi-sys") (v "0.0.2+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 1)))) (h "10sg9500i24aky3056bkac7lja4kkjzr10z7ps8qrszxmij9ky6n") (l "taichi_c_api")))

(define-public crate-taichi-sys-0.0.3+1.3.0 (c (n "taichi-sys") (v "0.0.3+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 1)))) (h "1l3cbdk6038pb5dzvsahacbipv568gjgm1x1a00ij6wz2hj8vn03") (l "taichi_c_api")))

(define-public crate-taichi-sys-0.0.4+1.3.0 (c (n "taichi-sys") (v "0.0.4+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 1)))) (h "0r8pf7i5bh5x4avr45arwgyy4839crvgl2yhhqyvzdrna4jji7rc") (l "taichi_c_api")))

(define-public crate-taichi-sys-0.0.5+1.3.0 (c (n "taichi-sys") (v "0.0.5+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 1)))) (h "0p2gl9byh5qrf68cifcpfpwjw3wgyvkhf5x8ly7irs3vdb9rjfqm") (l "taichi_c_api")))

(define-public crate-taichi-sys-0.0.6+1.3.0 (c (n "taichi-sys") (v "0.0.6+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 1)))) (h "0jykq9hl0ny0ys4xa44sv85xr2vs3y8yd50gsqz7jk1as3vyd0a1") (l "taichi_c_api")))

(define-public crate-taichi-sys-0.0.7+1.3.0 (c (n "taichi-sys") (v "0.0.7+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 1)))) (h "13pq3ziqc78c3myy0cds3nsf71gylxql7j8hbx2hrm8m79mbdykn") (l "taichi_c_api")))

(define-public crate-taichi-sys-0.0.8+1.3.0 (c (n "taichi-sys") (v "0.0.8+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 1)))) (h "0bsi22p7q9f3l4m4kajba7hriji5izpbhjcglby6l49899529zrf") (l "taichi_c_api")))

(define-public crate-taichi-sys-0.0.9+1.6.0 (c (n "taichi-sys") (v "0.0.9+1.6.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 1)))) (h "0l8j31djj6pxklajvsp0imqqpkcbsfyac54vxazrllirwab1qp0k") (l "taichi_c_api")))

