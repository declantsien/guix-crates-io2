(define-module (crates-io ta ic taichi-runtime) #:use-module (crates-io))

(define-public crate-taichi-runtime-0.0.1+1.3.0 (c (n "taichi-runtime") (v "0.0.1+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "taichi-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0drkj0pfxi86bn9yp8y2f4ir0cp914jlq1fph2fh0c6aic8am6pk")))

(define-public crate-taichi-runtime-0.0.2+1.3.0 (c (n "taichi-runtime") (v "0.0.2+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "taichi-sys") (r "^0.0.4") (d #t) (k 0)))) (h "17mvg919i92xs2jhhgggpx5xx4n17wfl4ypsbi58k8r0r2js14mm")))

(define-public crate-taichi-runtime-0.0.3+1.3.0 (c (n "taichi-runtime") (v "0.0.3+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "taichi-sys") (r "^0.0.5") (d #t) (k 0)))) (h "07ynyi2jcji411mjd4pxlds9ibpvrvd4q68xg7incp8w26m6bvy1")))

(define-public crate-taichi-runtime-0.0.4+1.3.0 (c (n "taichi-runtime") (v "0.0.4+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "taichi-sys") (r "^0.0.5") (d #t) (k 0)))) (h "0iqlhvnmnc05h0frjmh78rj05x505j8zl46gsswn1xpk83dlqbwm")))

(define-public crate-taichi-runtime-0.0.5+1.3.0 (c (n "taichi-runtime") (v "0.0.5+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "taichi-sys") (r "^0.0.5") (d #t) (k 0)))) (h "12d66ifpv6f5ngx4ikwh155r4cifn0d6hzvca76jm2xahhj3b5c4")))

(define-public crate-taichi-runtime-0.0.7+1.3.0 (c (n "taichi-runtime") (v "0.0.7+1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "taichi-sys") (r "^0.0.8") (d #t) (k 0)))) (h "1jq4xnci7d5wgnk68cjaii4av77xngpcfn4da4gwmbs04q5y08wb")))

(define-public crate-taichi-runtime-0.0.9+1.6.0 (c (n "taichi-runtime") (v "0.0.9+1.6.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "taichi-sys") (r "^0.0.9") (d #t) (k 0)))) (h "0yr6vv8vrpqj06cbqqph27iqnkfhiykhzl7y78y38izrnps06zdk")))

