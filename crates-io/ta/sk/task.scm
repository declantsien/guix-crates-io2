(define-module (crates-io ta sk task) #:use-module (crates-io))

(define-public crate-task-0.0.0 (c (n "task") (v "0.0.0") (h "1k7jpmw4llakndd1zwcxs8v9xrn0hpr7xk3a6ycwy0wlrsrbn555")))

(define-public crate-task-0.0.1 (c (n "task") (v "0.0.1") (d (list (d (n "crontab") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "threadpool") (r "1.7.*") (d #t) (k 0)) (d (n "time") (r "0.1.*") (d #t) (k 0)))) (h "1xnj0ls51hvqwnw2rdfdjcyj7vf500vm90jwlmzy6lkwmqvhs79c")))

