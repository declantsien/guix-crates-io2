(define-module (crates-io ta sk task-rs) #:use-module (crates-io))

(define-public crate-task-rs-0.0.0 (c (n "task-rs") (v "0.0.0") (d (list (d (n "iced") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0k8kh9580qsly4wl6g14a3svyqn8fjqwm1kbiapzkfgfw2klqwph") (y #t)))

