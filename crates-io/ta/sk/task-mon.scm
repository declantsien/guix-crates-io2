(define-module (crates-io ta sk task-mon) #:use-module (crates-io))

(define-public crate-task-mon-0.1.0 (c (n "task-mon") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("vec_map"))) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "parameterized_test") (r "^0.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "1jdql975m1w6n89frkk8zshywnx9ql6can5fxdklda7k02az06gw")))

(define-public crate-task-mon-0.2.0 (c (n "task-mon") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("vec_map"))) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "parameterized_test") (r "^0.1") (d #t) (k 2)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (d #t) (k 0)))) (h "1jd9v99nxc0l7s5df9nfls5v92n86sc1al4jibqki125g7rziabf") (f (quote (("static_ssl" "openssl/vendored"))))))

(define-public crate-task-mon-0.3.0 (c (n "task-mon") (v "0.3.0") (d (list (d (n "clap") (r "~3.0") (f (quote ("std" "derive" "env" "cargo"))) (k 0)) (d (n "clap_derive") (r "~3.0") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "parameterized_test") (r "^0.1") (d #t) (k 2)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "0rbbwb7sxkf2xn76qilcrha0n4j75h455wra9hd6zwdlz7mb5wdq") (f (quote (("static_ssl" "openssl/vendored"))))))

