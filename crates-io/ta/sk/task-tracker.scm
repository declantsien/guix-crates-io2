(define-module (crates-io ta sk task-tracker) #:use-module (crates-io))

(define-public crate-task-tracker-0.1.2 (c (n "task-tracker") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1zrfqr7d7qmybm4m3lnz5k85wwsxx8m4n4pl4ff1p67n5sh0ikkd")))

(define-public crate-task-tracker-0.2.0 (c (n "task-tracker") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "1w5vm4lm2l3ymnd9pyrhp3v552468qi6024whm4hnkxwxwmc44fz")))

