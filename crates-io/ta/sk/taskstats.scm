(define-module (crates-io ta sk taskstats) #:use-module (crates-io))

(define-public crate-taskstats-0.1.0 (c (n "taskstats") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)) (d (n "zerocopy") (r "^0.3.0") (d #t) (k 0)))) (h "1w5hbq245yylszxai9mvq611bcx0bpql6r90njc2xdssw4wdqa0h")))

(define-public crate-taskstats-0.1.1 (c (n "taskstats") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)) (d (n "zerocopy") (r "^0.3.0") (d #t) (k 0)))) (h "1ff62n6xcd79l1mrgacbrbbfp7d4jyhyjmrky1gn6w38r52v4afk")))

