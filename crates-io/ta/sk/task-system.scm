(define-module (crates-io ta sk task-system) #:use-module (crates-io))

(define-public crate-task-system-0.0.0 (c (n "task-system") (v "0.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trauma") (r "^2.2.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "0gb7whjqz5hi2pkyhq0nv01avavnf9mpc9352a576wj6ld19xfq3") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde"))))))

(define-public crate-task-system-0.1.0 (c (n "task-system") (v "0.1.0") (d (list (d (n "tokio") (r "^1.26.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.7") (d #t) (k 2)))) (h "1pnjql1q3zm8xxp45xgnj9958zmba8jzjn6vvnyqz59ms4y2sz3l") (f (quote (("default"))))))

