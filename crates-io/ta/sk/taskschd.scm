(define-module (crates-io ta sk taskschd) #:use-module (crates-io))

(define-public crate-taskschd-0.0.1 (c (n "taskschd") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("taskschd" "combaseapi" "objbase" "memoryapi" "processthreadsapi" "psapi" "winbase" "synchapi" "threadpoolapiset"))) (d #t) (k 0)))) (h "1866rk0kyacxflnxvymmlvp55byif5m751zdblcrg8681jx4x1r6")))

(define-public crate-taskschd-0.1.0 (c (n "taskschd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "comedy") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("errhandlingapi" "minwindef" "ntdef" "oaidl" "oleauto" "sysinfoapi" "taskschd" "winbase" "winerror" "winnt" "winreg" "wtypes"))) (d #t) (k 0)) (d (n "wio") (r "^0.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "16vcw7r50rqsxm97kp038km88zk6p3wiz6ya8a9l9z4d7jz009kr")))

(define-public crate-taskschd-0.1.1 (c (n "taskschd") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "comedy") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("errhandlingapi" "minwindef" "ntdef" "oaidl" "oleauto" "sysinfoapi" "taskschd" "winbase" "winerror" "winnt" "winreg" "wtypes"))) (d #t) (k 0)))) (h "16mj9ldp5hzhg40zwd78p3w559p4zxiaxlz13i3dkjdcn8rfhx2a")))

