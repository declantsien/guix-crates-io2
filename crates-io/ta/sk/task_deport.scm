(define-module (crates-io ta sk task_deport) #:use-module (crates-io))

(define-public crate-task_deport-0.1.0 (c (n "task_deport") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rustis") (r "^0.11.0") (f (quote ("tokio-runtime"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0ig1nsk41x1jj61262l85yjpp81q0if6kwgs5ivqmsxh5bkjgxig")))

