(define-module (crates-io ta sk task-manager) #:use-module (crates-io))

(define-public crate-task-manager-0.1.0 (c (n "task-manager") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "11dkc4cklk37j1kr5jarqzv32syycygmqad69jwxjwh7a1kcck6s")))

