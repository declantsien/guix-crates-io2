(define-module (crates-io ta sk tasks-framework) #:use-module (crates-io))

(define-public crate-tasks-framework-0.1.0 (c (n "tasks-framework") (v "0.1.0") (d (list (d (n "lock-free-stack") (r "^0.1.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "1v0xzhcakb7277cd3qa31yc8bsabdsa59xqb0difihdd00zq5ycy")))

