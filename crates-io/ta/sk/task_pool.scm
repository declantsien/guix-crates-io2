(define-module (crates-io ta sk task_pool) #:use-module (crates-io))

(define-public crate-task_pool-0.1.0 (c (n "task_pool") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.28") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.2") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)))) (h "0babv8s4kiw1sf4787abqxkx3qvnchmppkmxii8iyqj8ikjscjkm")))

(define-public crate-task_pool-0.1.1 (c (n "task_pool") (v "0.1.1") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.28") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.2") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)))) (h "06vgmh84w4x6ybwwvm631x3dxgsd5lsz3zsh0akqq8yph22v9b1i")))

(define-public crate-task_pool-0.1.2 (c (n "task_pool") (v "0.1.2") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.28") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.2") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)))) (h "16cqxz2pwr6fm2fd40m3q6wvrk8wprsbxb9sc7bp6rb9a22vkmfa")))

(define-public crate-task_pool-0.1.3 (c (n "task_pool") (v "0.1.3") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.28") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.2") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)))) (h "0wbac97i3anv81vp7r4ab8gjd55kvmk1x623q93dnpqhpn64y813")))

(define-public crate-task_pool-0.1.4 (c (n "task_pool") (v "0.1.4") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.28") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "priority-queue") (r "^1.3.2") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)))) (h "1gw93kdj4qpcbc6bcic6q1kd7nbcwrvydd7zqkhhx68glkayv31p")))

