(define-module (crates-io ta sk taskwarrior-hooks) #:use-module (crates-io))

(define-public crate-taskwarrior-hooks-0.1.0 (c (n "taskwarrior-hooks") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.44") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0yhwgrs65c4xx68sz9gb0zsnmchh8z74sbx8vrlpj2wzwzdx4zrb")))

(define-public crate-taskwarrior-hooks-0.2.0 (c (n "taskwarrior-hooks") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.44") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "task-hookrs") (r "^0.7.0") (d #t) (k 0)))) (h "13miksm1lb56bdwhbpby7x132a1dpafk1dvjr8q8b4lp4csbgg97")))

(define-public crate-taskwarrior-hooks-0.2.1 (c (n "taskwarrior-hooks") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.44") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "task-hookrs") (r "^0.7.0") (d #t) (k 0)))) (h "1kjz124dl6zhwvsvrhr6jq5c0iipnvjxqsj1m13509qldz2zadzz")))

(define-public crate-taskwarrior-hooks-0.2.2 (c (n "taskwarrior-hooks") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.44") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "task-hookrs") (r "^0.7.0") (d #t) (k 0)))) (h "1bsd7i6s60s031ai5bjjws4bhpckw1417wwn2dviipyksyibrvs4")))

