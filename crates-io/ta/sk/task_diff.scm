(define-module (crates-io ta sk task_diff) #:use-module (crates-io))

(define-public crate-task_diff-0.1.0 (c (n "task_diff") (v "0.1.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "ncurses") (r "^5.94.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sfamx28sxjk7w244ml4cfdn4w77c0ldzzsx67jmafkvd4h13r0x")))

(define-public crate-task_diff-0.2.1 (c (n "task_diff") (v "0.2.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "ncurses") (r "^5.94.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unescape") (r "^0.1") (d #t) (k 0)))) (h "071kvslrkkcr7h5rz8l051rf1bxfi08hzkg80azynhnzrnp7n2dv")))

