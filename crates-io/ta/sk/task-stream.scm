(define-module (crates-io ta sk task-stream) #:use-module (crates-io))

(define-public crate-task-stream-0.1.0 (c (n "task-stream") (v "0.1.0") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.13") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "0wp9l93ga4nig34lbxpr4zj1ik1v92hs2c12hzjn4d1lfpjjyg8f")))

(define-public crate-task-stream-0.1.1 (c (n "task-stream") (v "0.1.1") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.13") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "1cxq8ps8nlr2n76i2a1i51pvs6mwbkm4gqayc5khp0kqpi479czw")))

(define-public crate-task-stream-0.1.2 (c (n "task-stream") (v "0.1.2") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.13") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "0jjazasn87swr0rynl75a3ydncm9n0sc8ms92mddsmbssy1cyqdk")))

(define-public crate-task-stream-0.2.0 (c (n "task-stream") (v "0.2.0") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.13") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "06sn8210z3by17rw40lfiq5s8pc915s3gn82gh49swi712cgak8p")))

(define-public crate-task-stream-0.2.1 (c (n "task-stream") (v "0.2.1") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.13") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "04ciqp111hny8dvfqnqdjl9iq71gxgggjbq6mjyx4lpirwwhlnym")))

(define-public crate-task-stream-0.3.0 (c (n "task-stream") (v "0.3.0") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "async-task") (r "^4.0.3") (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.13") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "1qccixf7qin81gy3a87cidl53mdgkbar9k1ypa0qfxfdb02i13k4")))

(define-public crate-task-stream-0.3.1 (c (n "task-stream") (v "0.3.1") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "async-task") (r "^4.0.3") (k 0)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.13") (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "1rly7hsd72h7s1vk0vb0zj7vqcp8372x0wdwasm5sxw1winkq4al")))

(define-public crate-task-stream-0.3.2 (c (n "task-stream") (v "0.3.2") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "async-task") (r "^4.0.3") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "fixed-queue") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.13") (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "1xzq71as0krynhlicfx3n9qbg654abpi5xqw8jfd05s7mmwm6why")))

(define-public crate-task-stream-0.3.3 (c (n "task-stream") (v "0.3.3") (d (list (d (n "async-std") (r "^1.7") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "async-task") (r "^4.0.3") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.13") (k 0)) (d (n "futures-intrusive") (r "^0.4.0") (k 0)) (d (n "heapless") (r "^0.7.3") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "049n3zw7nax6p912ynxf0alwncz0yicz2hp3j3k54lf101p3j2zp")))

(define-public crate-task-stream-0.3.4 (c (n "task-stream") (v "0.3.4") (d (list (d (n "ach-mpmc") (r "^0.1") (d #t) (k 0)) (d (n "async-task") (r "^4.1") (k 0)) (d (n "async-tick") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "futures-executor") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "0ja8kabq860a2rrxxdx09nqvnxa4rlzh257bzly02y9qwkhgfdbr")))

(define-public crate-task-stream-0.3.5 (c (n "task-stream") (v "0.3.5") (d (list (d (n "ach-mpmc") (r "^0.1") (d #t) (k 0)) (d (n "async-task") (r "^4.1") (k 0)) (d (n "async-tick") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "futures-executor") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "1w3wn7d9kdd5fbzj18yypw4nal12rmmqv2whjbarzg2yjyxdg2s6")))

(define-public crate-task-stream-0.3.6 (c (n "task-stream") (v "0.3.6") (d (list (d (n "ach-mpmc") (r "^0.1") (d #t) (k 0)) (d (n "async-task") (r "^4.1") (k 0)) (d (n "async-tick") (r "^0.1") (f (quote ("std"))) (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "futures-executor") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "04xzpjlbj51ak9zfckh9169arhsrxm1pmzvyap10ibsra4i6i187")))

(define-public crate-task-stream-0.3.7 (c (n "task-stream") (v "0.3.7") (d (list (d (n "ach-mpmc") (r "^0.2") (d #t) (k 0)) (d (n "async-task") (r "^4.1") (k 0)) (d (n "async-tick") (r "^0.1") (f (quote ("std"))) (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "futures-executor") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "07ck9m7mx83dk0izv5y1cc68933gsnfjzjskmnnk8zl4xvc68kgc")))

