(define-module (crates-io ta sk taskpipe) #:use-module (crates-io))

(define-public crate-taskpipe-0.0.1 (c (n "taskpipe") (v "0.0.1") (h "1rd2m51xkjc5mg0kz04y504qq8vcif5sb5m2flhm0969ks1g8wxq")))

(define-public crate-taskpipe-0.1.1 (c (n "taskpipe") (v "0.1.1") (h "18qxhwddflb6vv8iq8148ykg29jvczad8xhmz4ccji4i4fhmjl57")))

(define-public crate-taskpipe-0.1.2 (c (n "taskpipe") (v "0.1.2") (h "19v6cvddqmb4abiggjsiw6d8aama814facm85cssn0myknjp589z")))

