(define-module (crates-io ta sk task-grep) #:use-module (crates-io))

(define-public crate-task-grep-0.1.0 (c (n "task-grep") (v "0.1.0") (d (list (d (n "lock-free-stack") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tasks-framework") (r "^0.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3") (d #t) (k 0)))) (h "0aqj3m62mlvy91rd16787iqk1agffv1smw7vx71ziaxay00pjdsx") (y #t)))

