(define-module (crates-io ta sk taskzk) #:use-module (crates-io))

(define-public crate-taskzk-0.1.0 (c (n "taskzk") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0x8knxaal3a77n40bhd7a6585b9k4gcrrxqzmwkz4cvjk71nw71f")))

(define-public crate-taskzk-0.1.1 (c (n "taskzk") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1b397y1x3792c8ilpa3ms26c05c457p5z2lg42bgr4nlihfzdqfh")))

(define-public crate-taskzk-0.1.2 (c (n "taskzk") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "00x3zfs3g7h5j0fzvxc0lhw56pwa4mnz230p2vbsyxjchm7nnsbl")))

(define-public crate-taskzk-0.1.3 (c (n "taskzk") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1jkc0p2s6np63brv6cdivca605r9zljhci1kvqw7gmwzv13yimbf")))

