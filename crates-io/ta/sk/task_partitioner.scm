(define-module (crates-io ta sk task_partitioner) #:use-module (crates-io))

(define-public crate-task_partitioner-0.1.0 (c (n "task_partitioner") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "0qpbgz85j68jfw68n506zz670ayxwvrlh40a5hicmr1aki3yxvw5")))

(define-public crate-task_partitioner-0.1.1 (c (n "task_partitioner") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "0cahyf7mgbgld28nkmvb0cfyhzxrfdz2dm8y7ivahckzi58zkprg")))

