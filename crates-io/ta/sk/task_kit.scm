(define-module (crates-io ta sk task_kit) #:use-module (crates-io))

(define-public crate-task_kit-0.1.0 (c (n "task_kit") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1bkdd11n1yla24rcmqz94xrij7w1s1a7xsga4sgyx2iqlci39v89")))

(define-public crate-task_kit-0.1.1 (c (n "task_kit") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1dxvm5vakrhm0qxrh397251ry494cqpidrfif0k7cjp35xv4bnb3")))

(define-public crate-task_kit-0.2.0 (c (n "task_kit") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 2)))) (h "1jbwa75haz4w8g1dd6xzk0qzahks7f8yfscylklg960i4lmj806b") (f (quote (("futures_support" "futures"))))))

