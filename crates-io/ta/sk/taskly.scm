(define-module (crates-io ta sk taskly) #:use-module (crates-io))

(define-public crate-taskly-0.1.0 (c (n "taskly") (v "0.1.0") (d (list (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "01dcxayycp79vabqql22sl0sc1rbq78w3167615pf7mh859zbd62") (y #t)))

(define-public crate-taskly-0.1.1 (c (n "taskly") (v "0.1.1") (d (list (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0cck58sh8yzgrmifblmmzpkf323i8iqh3k5q4dvx2jxn321w61dw") (y #t)))

(define-public crate-taskly-0.1.2 (c (n "taskly") (v "0.1.2") (d (list (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0g65rid0gbjkylafn215iq5018i83gf6lzg3n3fxgzid61jb14zh") (y #t)))

(define-public crate-taskly-0.1.3 (c (n "taskly") (v "0.1.3") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "09nic1sqaimmynhv5s3dc40ccn71lnyhj72rla7h6zcai0bf2ri4")))

(define-public crate-taskly-0.1.4 (c (n "taskly") (v "0.1.4") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "enable-ansi-support") (r "^0.2.1") (d #t) (k 0)) (d (n "owo-colors") (r "^3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "09msnrgvqk8k6z16jn4fd2vcyw2wr6n2dj1741rya3z7wrp4gm07")))

