(define-module (crates-io ta sk taskqueue) #:use-module (crates-io))

(define-public crate-taskqueue-0.1.0 (c (n "taskqueue") (v "0.1.0") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)))) (h "19akskq9sl4mf9bkb7vgbghydqs2r1cmfkhm57qn01dm3bsxs2yk")))

(define-public crate-taskqueue-0.1.1 (c (n "taskqueue") (v "0.1.1") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)))) (h "13yw18xk4va5r7v53h4j9q034qjd2vh4axjpj18inrf6av5f3m0i")))

