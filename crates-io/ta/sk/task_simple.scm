(define-module (crates-io ta sk task_simple) #:use-module (crates-io))

(define-public crate-task_simple-0.1.0 (c (n "task_simple") (v "0.1.0") (d (list (d (n "gloo-worker") (r "^0.5.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xl2wc1p5n2g8f1f26rlrrvbkyk75pj86yar09r75ck2vfqrdqf7")))

(define-public crate-task_simple-0.2.0 (c (n "task_simple") (v "0.2.0") (d (list (d (n "gloo-worker") (r "^0.5.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vh831dy18rg3nx9815ygx715mflv6wv4j43nr3mmnp95i45njcq")))

(define-public crate-task_simple-0.3.0 (c (n "task_simple") (v "0.3.0") (d (list (d (n "gloo-worker") (r "^0.5.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h4xb3sbjpqfwhx0haab6c6igjak3h15s2wpgj4jm16hgbcfjzcz")))

(define-public crate-task_simple-0.4.0 (c (n "task_simple") (v "0.4.0") (d (list (d (n "gloo-worker") (r "^0.5.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ls37i65a313xiyvps13hz3m9m8d7f5inidgznc9dz883ar3g66g")))

(define-public crate-task_simple-0.4.1 (c (n "task_simple") (v "0.4.1") (d (list (d (n "gloo-worker") (r "^0.5.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "11ijd9pkq47gmi957yhq568sqnr1jcmaqw72l8gklvqhj2p4p0sk")))

