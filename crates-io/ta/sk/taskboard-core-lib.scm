(define-module (crates-io ta sk taskboard-core-lib) #:use-module (crates-io))

(define-public crate-taskboard-core-lib-0.1.0 (c (n "taskboard-core-lib") (v "0.1.0") (h "0fi578mz23pvgrgprd7q9ljfy3f006a0nxnz4sqgmwcj7l3aivc0")))

(define-public crate-taskboard-core-lib-0.1.1 (c (n "taskboard-core-lib") (v "0.1.1") (h "1ay64xqdwkliz7bk0f9xypf78cfxx1n4qw5lljpvzapznic9jsf5")))

(define-public crate-taskboard-core-lib-0.1.2 (c (n "taskboard-core-lib") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05fwc2nfr0r7d57jmllg1zy3il1yx6kmzav8igk8wx87bdhf852s")))

(define-public crate-taskboard-core-lib-0.1.3 (c (n "taskboard-core-lib") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yqkcxpjizzx9niqnzjfplhny65qgxs74hhg31rb2liqpnixrfiy")))

(define-public crate-taskboard-core-lib-0.1.4 (c (n "taskboard-core-lib") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "028na2vh74wvbhpgkwb1qh69ybbsfmbp2ij182k69adng4q6rgqd")))

(define-public crate-taskboard-core-lib-0.1.5 (c (n "taskboard-core-lib") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "0pm7ys5l1mfvs4lgsxysrpvapakgbsisfcx5kd3kjbn6p71picki")))

(define-public crate-taskboard-core-lib-0.1.6 (c (n "taskboard-core-lib") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "1jsscj9w1zj4wk9zq3mcfx4gzxc2q4hpipbnf6sr1d2csckallgd")))

(define-public crate-taskboard-core-lib-0.1.7 (c (n "taskboard-core-lib") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "09jh5f039pz1v2vikk834vs7zn9ihpw2g1g1lhfyv8k48815mhqy")))

(define-public crate-taskboard-core-lib-0.1.8 (c (n "taskboard-core-lib") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "1h7w33i8p59w68d7nxjc00g6mk6vmksz9a0carw1zfq1p8l4rz0q")))

(define-public crate-taskboard-core-lib-0.1.9 (c (n "taskboard-core-lib") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "094yilb88pz04jhbn51bj94rxmyvh08n2imwfxr4z1lpi02k9nic")))

(define-public crate-taskboard-core-lib-0.1.10 (c (n "taskboard-core-lib") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "10hkajql79dwsdlyxk7nb0464l6s5b4y401xgy79sjlndxqxf535")))

(define-public crate-taskboard-core-lib-0.1.11 (c (n "taskboard-core-lib") (v "0.1.11") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "0hxa4iym076sbqs4zbfrlm4r8d358n9x6i36mqwk3lvwz6j0pyzz")))

(define-public crate-taskboard-core-lib-0.1.12 (c (n "taskboard-core-lib") (v "0.1.12") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "1x317hkm61lb34ym2jg62nxjm1c5kqgzpdja6jkyzn4cgwyd1nda")))

(define-public crate-taskboard-core-lib-0.1.13 (c (n "taskboard-core-lib") (v "0.1.13") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "1cf3jv1yw22rlvayw4ml98n04a1if1g9vy07jisx3hjkl0h6jpsn")))

(define-public crate-taskboard-core-lib-0.1.14 (c (n "taskboard-core-lib") (v "0.1.14") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "132afr9qq6q4jlk60ail8pr5q7zl3daqkgbxqlqrb6xh70bd4aac")))

(define-public crate-taskboard-core-lib-0.1.15 (c (n "taskboard-core-lib") (v "0.1.15") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "1hn335jw8a1f6l0dgcnbzja6qhgs7l2661chfc5yc499sg2qqhxf")))

(define-public crate-taskboard-core-lib-0.1.16 (c (n "taskboard-core-lib") (v "0.1.16") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "1wlay492jmpgh9nisfq85cihpbh7878368y3ymwmdzqcmq55qb6m")))

(define-public crate-taskboard-core-lib-0.1.17 (c (n "taskboard-core-lib") (v "0.1.17") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "12q4q1p5hkzyai6jx6h2ly5gqm4ijp1kh1zjpfapsdaps844r1kl")))

(define-public crate-taskboard-core-lib-0.1.18 (c (n "taskboard-core-lib") (v "0.1.18") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "1csggi3nh8s1vj9r7z1fi4hxbjd5429h758ghizryn9h4vri18g1")))

(define-public crate-taskboard-core-lib-0.1.19 (c (n "taskboard-core-lib") (v "0.1.19") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "0aayb70fksaszlkxjjy31x9zh95jcfl6skjfxmn5y9rjw4q20izl")))

