(define-module (crates-io ta sk task-group) #:use-module (crates-io))

(define-public crate-task-group-0.1.0 (c (n "task-group") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt" "sync"))) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ccw6lpzsh0hdwyh9582ilyxpmg8v14rpnsjbbbl39z9z3ljmxx0")))

(define-public crate-task-group-0.2.0 (c (n "task-group") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt" "sync"))) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "160yyks4yccxwsjb5l4f82dj79ksgnih9dybbxdavg609xay8x6v")))

(define-public crate-task-group-0.2.1 (c (n "task-group") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt" "sync"))) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0kcaq52yzwm9nzh3fh077ksgc74j3rdxpcr9s0lvfr3kkg13n7wb")))

(define-public crate-task-group-0.2.2 (c (n "task-group") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt" "sync"))) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1kz9bvfj1ipfspcm1a3jfglpgjw02fkrqaslgl41yvkjgc27mp83")))

