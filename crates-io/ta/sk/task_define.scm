(define-module (crates-io ta sk task_define) #:use-module (crates-io))

(define-public crate-task_define-0.1.0 (c (n "task_define") (v "0.1.0") (h "0wh45v4nyfcw531cc9yqx62ncb0i773gc2q0fglc2srzv11d2lap") (r "1.78")))

(define-public crate-task_define-0.1.2 (c (n "task_define") (v "0.1.2") (h "1a4qspf11jsbpgdj43ndkbibgpg25l0x996ys341xdn43wba5ib4") (r "1.78")))

(define-public crate-task_define-0.1.3 (c (n "task_define") (v "0.1.3") (h "0wqhfbb9pi6nxnq6wjnc07vd0gwjml6x81anqsasyvjnq83pki7s") (r "1.78")))

(define-public crate-task_define-0.1.4 (c (n "task_define") (v "0.1.4") (h "1s0apzh9zh2bqwqr2lzw8f19swcppwihmljnh54cwql3dmq99cg2") (r "1.78")))

(define-public crate-task_define-0.1.5 (c (n "task_define") (v "0.1.5") (h "1rbrslr4c6wr05ydir8vs15svn237r778va3sv53mhmcjfkmkayc") (r "1.79")))

