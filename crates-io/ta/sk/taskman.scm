(define-module (crates-io ta sk taskman) #:use-module (crates-io))

(define-public crate-taskman-0.1.0 (c (n "taskman") (v "0.1.0") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0c9wkgfrlx9037pf00bwz0a6j0h1pfgzal4h07fd06a5wiy6yp1d") (y #t)))

(define-public crate-taskman-0.1.1 (c (n "taskman") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "tabled") (r "^0.12.1") (d #t) (k 0)))) (h "1s03w0hbzic02pl1f98c1wipy17wqswvlyw1mcymkq838ipa0mld")))

