(define-module (crates-io ta sk task-list) #:use-module (crates-io))

(define-public crate-task-list-1.0.0 (c (n "task-list") (v "1.0.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "0lywfrazxd9wchgrhzn09152pgz45da6jsr1arzwq8azsgqv5l2m")))

(define-public crate-task-list-1.0.1 (c (n "task-list") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "0zbzyl063gjl86bb6932wpsshshxyv2p99c28vsk08wfmqxf63zg")))

(define-public crate-task-list-1.0.2 (c (n "task-list") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)))) (h "150c93zmphsdr7sh2pms3ws7a0mirgw8pvi0libs9n3pfvgag43l")))

