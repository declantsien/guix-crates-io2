(define-module (crates-io ta sk tasklib) #:use-module (crates-io))

(define-public crate-tasklib-0.1.0 (c (n "tasklib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0h43xbcrj1dy7z42sm3dxcfbmsvb385garbx2pdxqbgrzcsz29xg")))

(define-public crate-tasklib-0.1.1 (c (n "tasklib") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1sk3768zfxld69iwx56crymzqa14vac5hr1dlk099x4qmx0c4imm")))

(define-public crate-tasklib-0.1.2 (c (n "tasklib") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "002vg9v4pgns4d6x77bkzclmyv16adminly926g31n9ilk5fv5cz")))

(define-public crate-tasklib-0.3.0 (c (n "tasklib") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1wkr31r5zzqbkkkqk3gnd94lk0hf0fbb7cm39lv6q3s08wjx0gq7")))

(define-public crate-tasklib-0.3.1 (c (n "tasklib") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1bs1i82i9w4y6h1pi5mnrcv5qwzlw53x90w9w0d79dxaps7jif9g")))

(define-public crate-tasklib-0.3.2 (c (n "tasklib") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "05vlg3jsqrizpyr8lw589x96jd12fgpbxc9k1sxhqqpfl8y4qws3")))

