(define-module (crates-io ta sk taskpool) #:use-module (crates-io))

(define-public crate-taskpool-0.0.1 (c (n "taskpool") (v "0.0.1") (d (list (d (n "stainless") (r "*") (d #t) (k 0)))) (h "1jmscwjyrzx7z40cfifq6bj5blivcz71s00vq8p3c1akfla5arzp")))

(define-public crate-taskpool-0.1.0 (c (n "taskpool") (v "0.1.0") (d (list (d (n "stainless") (r "*") (d #t) (k 0)))) (h "1a3vrd9y57b5vzhxsk5lbkqs6ivij8rm7gbzqm1ar5riqi95zrqp")))

