(define-module (crates-io ta sk taskwait) #:use-module (crates-io))

(define-public crate-taskwait-0.1.0 (c (n "taskwait") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.12") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1lq0s1vfa42l9wlxpyabr2c9z6rvzwj9w0hqwfjbf8b9hi4rybms")))

(define-public crate-taskwait-0.2.0 (c (n "taskwait") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3.12") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1j248qii4p9hki9wmsqz2dh9nzd5iwkn0bs23kdp7xx4fwn1c2l4")))

(define-public crate-taskwait-0.2.1 (c (n "taskwait") (v "0.2.1") (d (list (d (n "futures-util") (r "^0.3.12") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0k0hx6v43w89kapmqpkdjz469920xxclq2afi2cipqpw61la4zq0")))

(define-public crate-taskwait-0.3.0 (c (n "taskwait") (v "0.3.0") (d (list (d (n "futures-util") (r "^0.3.12") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "05rsxqn1ry6g7gdj62plfq3vc4gndxmqyvylhfg60nj0ad2f5n24")))

(define-public crate-taskwait-0.3.1 (c (n "taskwait") (v "0.3.1") (d (list (d (n "futures-util") (r "^0.3.12") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1y6i83mwi0isvpvg9iqfpngmxhhwiyjya2clpy7qk82s8lgfdqsw")))

(define-public crate-taskwait-0.4.0 (c (n "taskwait") (v "0.4.0") (d (list (d (n "futures-util") (r "^0.3.12") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0nbjgqzaaak1yhygxyizqr2bnpn6qa2grylwwqlnfba166x362y3")))

(define-public crate-taskwait-0.4.1 (c (n "taskwait") (v "0.4.1") (d (list (d (n "futures-util") (r "^0.3.12") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0hclp7mi2h8lmax0bcz8mjq3y6bzk3391lb7c3dhalf0hddaqwa4")))

