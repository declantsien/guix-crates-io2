(define-module (crates-io ta sk taskchampion-lib) #:use-module (crates-io))

(define-public crate-taskchampion-lib-0.5.0 (c (n "taskchampion-lib") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ffizz-header") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.136") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "taskchampion") (r "^0.5.0") (d #t) (k 0)))) (h "0nw8m3nmrxp5wxr22kyxh3qci5lniyn2qp1qxr1gcd4a795k724c")))

