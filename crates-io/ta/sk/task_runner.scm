(define-module (crates-io ta sk task_runner) #:use-module (crates-io))

(define-public crate-task_runner-0.0.1 (c (n "task_runner") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "14y7g57dv7kpi37jhs4gc5x85cq6nwdg9scp7d4h4mqmzfs5dmhb")))

