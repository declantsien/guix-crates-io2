(define-module (crates-io ta sk taskio) #:use-module (crates-io))

(define-public crate-taskio-0.1.0-alpha-1 (c (n "taskio") (v "0.1.0-alpha-1") (d (list (d (n "nb") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)))) (h "00ay1ibq6zyhxa2p1xkwpzavf69a7fbz4ms9j3m3hv9a26ihrh6i") (f (quote (("generators") ("full" "nb"))))))

