(define-module (crates-io ta sk tasktrace) #:use-module (crates-io))

(define-public crate-tasktrace-0.1.0 (c (n "tasktrace") (v "0.1.0") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "scoped-trace") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1v1i2vi4z7zjy9wljqqays6f6kq39cagi27rzy2dli62ncx0mgb7")))

(define-public crate-tasktrace-0.1.1 (c (n "tasktrace") (v "0.1.1") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "scoped-trace") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "19byz0cjabihh475dv1m1wir2k419mhklkf5l8pbhk5z5fn6vgym")))

