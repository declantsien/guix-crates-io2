(define-module (crates-io ta sk task_queue) #:use-module (crates-io))

(define-public crate-task_queue-0.0.1 (c (n "task_queue") (v "0.0.1") (h "1h8x6q87jkqw0483ywnsmgam4hb4yicah307vzxfkffiax297728")))

(define-public crate-task_queue-0.0.2 (c (n "task_queue") (v "0.0.2") (h "1d1bcf5b48p11bf33lhzbixgpdi4v0p397355k4la6jqqs8l044z")))

(define-public crate-task_queue-0.0.3 (c (n "task_queue") (v "0.0.3") (h "0ma0bsynkw7s02qjvyd16k7vav54b20c1iz7bnsyd9i3dkbgjp23")))

(define-public crate-task_queue-0.0.4 (c (n "task_queue") (v "0.0.4") (h "13xxp67w2y1l8ngn5mdrl8kjfaw5z3a42gx7npvai1xh1cqvmgv4")))

(define-public crate-task_queue-0.0.5 (c (n "task_queue") (v "0.0.5") (h "1cc6rbxdci0ig0a2y0zmw956c9z3a5y6bdsd2w133nh9zkmp3wbs")))

(define-public crate-task_queue-0.0.6 (c (n "task_queue") (v "0.0.6") (h "1wc94yswfrwi1zxn5biynmy603jb6f073wv8080shkswalbsmv4m")))

(define-public crate-task_queue-0.0.7 (c (n "task_queue") (v "0.0.7") (h "0fmngfrs6cad5165vzfwd0ich5kdxa4iz6c8drjgw3fxc7bnadfc")))

