(define-module (crates-io ta sk taskmanager) #:use-module (crates-io))

(define-public crate-taskmanager-0.1.0 (c (n "taskmanager") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "waitgroup") (r "^0.1.2") (d #t) (k 0)))) (h "117s35hbpm9d8h2k0ynfhjzbflywwkv8l9mnzvlwlml6p9a7b1f0")))

(define-public crate-taskmanager-0.1.1 (c (n "taskmanager") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "loga") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "waitgroup") (r "^0.1.2") (d #t) (k 0)))) (h "19q6m3h88flnyaszjnrpmksfdf97dbxg31a51n6kifhsnkdraprr")))

(define-public crate-taskmanager-0.1.2 (c (n "taskmanager") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "loga") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (d #t) (k 0)) (d (n "waitgroup") (r "^0.1.2") (d #t) (k 0)))) (h "1ny9xl8skjhdha2657wvrbnml373a1ncmkncv9438aa1l7k1krmm")))

(define-public crate-taskmanager-0.2.0 (c (n "taskmanager") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "loga") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "waitgroup") (r "^0.1") (d #t) (k 0)))) (h "0pfl97v3y002k2lr11ljl0c6abcfd6w4j23v16m8qr9z318vmxnv")))

(define-public crate-taskmanager-0.3.0 (c (n "taskmanager") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "loga") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "waitgroup") (r "^0.1") (d #t) (k 0)))) (h "19nj00372h5c9saiixj0k38v1kmv0smfkn67kdxnfcq4zxrpsr9r")))

(define-public crate-taskmanager-0.4.0 (c (n "taskmanager") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "loga") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "waitgroup") (r "^0.1") (d #t) (k 0)))) (h "0rvh4jfpya8a2vq50vgq5kk7xy92whjcakr02z2p6pyyhyxrx7k1")))

(define-public crate-taskmanager-0.4.1 (c (n "taskmanager") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "loga") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "waitgroup") (r "^0.1") (d #t) (k 0)))) (h "03i9ayvkafv7ni6wwhar3hr1zy71adhb58ykdsv1cpc3wiqwq4q4")))

(define-public crate-taskmanager-0.4.2 (c (n "taskmanager") (v "0.4.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "loga") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "waitgroup") (r "^0.1") (d #t) (k 0)))) (h "1av2axk5ysgf5m4pm15i459bi0bmn9580y7q8vjbzj202pz93j2h")))

(define-public crate-taskmanager-0.5.0 (c (n "taskmanager") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "loga") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "waitgroup") (r "^0.1") (d #t) (k 0)))) (h "1n1ksg5zb0acii2r8nx8md8img52bl5vv3571snbk6wcb6nzmh81")))

(define-public crate-taskmanager-0.5.1 (c (n "taskmanager") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "loga") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "macros" "time" "sync" "rt"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "waitgroup") (r "^0.1") (d #t) (k 0)))) (h "045whk6j67glcn1i48dh4bbj36l7wji4ckx8dryhkxg36phkn6yw")))

