(define-module (crates-io ta sk taskt) #:use-module (crates-io))

(define-public crate-taskt-0.1.0 (c (n "taskt") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)))) (h "1bl39sg2gajssd1pji1zp7ff1xm42wfj485blfsa252qn555qm3s")))

(define-public crate-taskt-0.1.1 (c (n "taskt") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)))) (h "1p482v4fb7q2hcyf1rl9nglykl2csmjzk7a98kccvmjvl6gmn6ss")))

(define-public crate-taskt-0.2.0 (c (n "taskt") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)))) (h "05y7z3r3k41jkbzjkdk26j3ff2klqvhg8mng3w62ld0wy3wpw2xw")))

(define-public crate-taskt-0.3.0 (c (n "taskt") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0kmj0lgk3vmmck57kp5r51ikqyl0hc6risrij91i3qwnkcd2id84")))

