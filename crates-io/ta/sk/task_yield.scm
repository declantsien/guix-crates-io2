(define-module (crates-io ta sk task_yield) #:use-module (crates-io))

(define-public crate-task_yield-0.1.0 (c (n "task_yield") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1jcvazkzq7gzvygvl0i9p7sdhgnmmirs03mnrdbjs2v7gg3l8hk6")))

