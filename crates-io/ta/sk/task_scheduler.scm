(define-module (crates-io ta sk task_scheduler) #:use-module (crates-io))

(define-public crate-task_scheduler-0.1.0 (c (n "task_scheduler") (v "0.1.0") (h "119wzl2g9mm0yjkcrzbsqwhb5vgggm8ynmh6kaiw3x78mf07g7sg")))

(define-public crate-task_scheduler-0.2.0 (c (n "task_scheduler") (v "0.2.0") (h "19zv70s9qg6pjlqkzs0d72z5gynr213qyb2lqq2xdxwkf9dysg02")))

