(define-module (crates-io ta sk taskwarrior-hooks-rust) #:use-module (crates-io))

(define-public crate-taskwarrior-hooks-rust-0.1.0 (c (n "taskwarrior-hooks-rust") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.44") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0w4yrqsxfmxzahwv81mfcz1fccbzds9mnjl4z7hhffmdqmxqwa5r")))

