(define-module (crates-io ta sk taskx) #:use-module (crates-io))

(define-public crate-taskx-0.1.0 (c (n "taskx") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "toml") (r "^0.8.6") (d #t) (k 0)))) (h "0j073aqha91kacms50yw78dd42zijvbawy0lzx3y79j1bkcs9abc")))

(define-public crate-taskx-0.1.1 (c (n "taskx") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "toml") (r "^0.8.6") (d #t) (k 0)))) (h "09zh37hjl0iy35mg572qyddm4x7961s99pz84zg2pzkhyl4r62vm")))

