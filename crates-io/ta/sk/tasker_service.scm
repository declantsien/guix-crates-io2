(define-module (crates-io ta sk tasker_service) #:use-module (crates-io))

(define-public crate-tasker_service-0.1.0 (c (n "tasker_service") (v "0.1.0") (d (list (d (n "tasker") (r "^0.2.2") (d #t) (k 0)))) (h "1yhk0nkl424mrz2zsswqz1a3jp74lvqdaxfplbbv211013k9qhzf")))

(define-public crate-tasker_service-0.1.1 (c (n "tasker_service") (v "0.1.1") (d (list (d (n "tasker") (r "^0.2.2") (d #t) (k 0)))) (h "1xbbh310q84l53l6bhqf0vakagr530fgrgz15hgvj3y5qqc3rbbd")))

(define-public crate-tasker_service-0.1.2 (c (n "tasker_service") (v "0.1.2") (d (list (d (n "tasker") (r "^0.2.2") (d #t) (k 0)))) (h "1xmcfmk9lwkpcmfbjvpvhr0qrpddkqwcdm0d7pkcf0lkjzcc7rz0")))

(define-public crate-tasker_service-0.1.3 (c (n "tasker_service") (v "0.1.3") (d (list (d (n "tasker") (r "^0.3.0") (d #t) (k 0)))) (h "19ad74pkfh7xy4126fzl4z3dmd9cgy83y3xy2q713l2c2ajdvgia")))

(define-public crate-tasker_service-0.1.6 (c (n "tasker_service") (v "0.1.6") (d (list (d (n "tasker_lib") (r "^0.0.2") (d #t) (k 0)))) (h "12kw3h9niwlrp227svziphacwzc1lmyx3cbahvprshmy5ph9cqh3")))

(define-public crate-tasker_service-0.1.7 (c (n "tasker_service") (v "0.1.7") (d (list (d (n "tasker_lib") (r "^0.0.2") (d #t) (k 0)))) (h "0lljijvcx9jr4f75wa8h9mj709la4fp9k3pvkh79a85y4alr393i")))

(define-public crate-tasker_service-0.1.8 (c (n "tasker_service") (v "0.1.8") (d (list (d (n "tasker_lib") (r "^0.0.3") (d #t) (k 0)))) (h "1r6di1j8dqhhn23j2dqkck96jn8n0r3fypiqgqc7zcj4gkg2zp5v")))

