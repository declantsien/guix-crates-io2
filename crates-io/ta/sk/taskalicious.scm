(define-module (crates-io ta sk taskalicious) #:use-module (crates-io))

(define-public crate-taskalicious-0.1.0 (c (n "taskalicious") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0706l22rb4y351qnzg9alqk4zvnwlrnx2ybljxsb1gplap477ng7")))

(define-public crate-taskalicious-0.1.1 (c (n "taskalicious") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1a5ahfqlh52qhmshgrr5mpcm7im7ys37q3md7mr98vhq93lfqc7z")))

(define-public crate-taskalicious-0.2.0 (c (n "taskalicious") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1v8l7ky2jh4ln4cbqc9da3zbz995vafpvq6gah4l4flhf05na6j2")))

(define-public crate-taskalicious-0.3.0 (c (n "taskalicious") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0nw3hrx44jk79shgcf7cm433cs5cdjwyl5frvy3ak6861df9smaw")))

(define-public crate-taskalicious-0.3.1 (c (n "taskalicious") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0lq8bpg2cv3xwcdgb33687k0kr79xi7w955dam2jy3lwcfs445kx")))

(define-public crate-taskalicious-0.3.2 (c (n "taskalicious") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0ykcl5f1vzcj68nsn07is4kxyb3gxdjd625gbzpc211jy8az1kxw")))

(define-public crate-taskalicious-0.3.3 (c (n "taskalicious") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0ssc098py4iv47556a75i3bjnajfdfpfligrl9rkpc8ajgs6zgqp")))

(define-public crate-taskalicious-0.3.4 (c (n "taskalicious") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1pp3y39ai8n10jd973xma6cd091qgjk2isqx5p8acfbd48n0y666")))

(define-public crate-taskalicious-0.4.0 (c (n "taskalicious") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "02c0rji3dnj9w67xd8xpiq3dzjxbpj2bwq40ki0ssvr9lgmwvcfk")))

(define-public crate-taskalicious-0.5.0 (c (n "taskalicious") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0wvgi8vl8dhfgq74yxkzg2ljx6kxqz8rwbr2bp7r8418k1f7iz2a")))

(define-public crate-taskalicious-0.6.0 (c (n "taskalicious") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0w5mk8aka70qd48gqn6i369bpnny1slxxl97ch8wrz728q8y5933")))

(define-public crate-taskalicious-0.7.0 (c (n "taskalicious") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0w6xbhqcg4zbhkassgpyzgf0250622lqicxnxiga7qb0k3x8zxj1")))

(define-public crate-taskalicious-0.7.1 (c (n "taskalicious") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0x92gnx0r2r20c24k5lvbgzqxmbyg4db6z3jh92c1r4y9nvizdm0")))

(define-public crate-taskalicious-0.8.0 (c (n "taskalicious") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0lk80wc56pc56hklvrk545fnppfqi9gg014qh51y7zs9v6q8vym5")))

(define-public crate-taskalicious-1.0.0 (c (n "taskalicious") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0vzy70l2gvyr59j5jkd3wi4k5b31h1ylvpyr4jk2qs76i7rnvag1")))

(define-public crate-taskalicious-1.1.0 (c (n "taskalicious") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1jqcw102q9390dr4mii6cxhgarvhb55gl6g4lr029fvv39xhpypd")))

(define-public crate-taskalicious-1.2.0 (c (n "taskalicious") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1b8s81yhc1zhvj1c4nkziliyc40syzry550nxh3b6jn97bkd27p9")))

