(define-module (crates-io ta sk task_scope) #:use-module (crates-io))

(define-public crate-task_scope-0.1.0 (c (n "task_scope") (v "0.1.0") (d (list (d (n "async-std") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.2.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1xymrf9hrarrzivd8z54d5rsz6pkzrn63wzw8l4w871l6k3zcbqx") (f (quote (("default" "tokio"))))))

(define-public crate-task_scope-0.1.1 (c (n "task_scope") (v "0.1.1") (d (list (d (n "async-std") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.2.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1p85j47bmd3d0zkbqhl244kbpf3c5229mahkgq2rdq4pj2yngksi") (f (quote (("default" "tokio"))))))

