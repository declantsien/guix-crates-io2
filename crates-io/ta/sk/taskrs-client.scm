(define-module (crates-io ta sk taskrs-client) #:use-module (crates-io))

(define-public crate-taskrs-client-0.0.1 (c (n "taskrs-client") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "libtaskrs") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "rustls" "blocking"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09ifspxs38b2r0z9i8z6rjl3c4chcxp0m4ig2bxjh9cr1yj2q92k")))

