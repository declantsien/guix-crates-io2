(define-module (crates-io ta sk tasklet) #:use-module (crates-io))

(define-public crate-tasklet-0.1.0 (c (n "tasklet") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cron") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)) (d (n "time") (r "^0.2.23") (d #t) (k 0)))) (h "1a8kca577vmyczhya5dpbla2vn2wrymwb1y02iqvirgw5mp8ysz8")))

(define-public crate-tasklet-0.1.1 (c (n "tasklet") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cron") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)) (d (n "time") (r "^0.2.25") (d #t) (k 0)))) (h "0jr6nf27p4r608s1x4if2q47kcr0yry936cls1n5xj5f2plhdzic")))

(define-public crate-tasklet-0.1.2 (c (n "tasklet") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 2)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "04h9m1f8k1zyczp0i75c90swinbn5ynafhhpaf56vq897yxwy502") (y #t)))

(define-public crate-tasklet-0.1.3 (c (n "tasklet") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 2)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "0asgi9nkxikqh8rvq2lgbh7fa07ylx7yj8fkcia7w1w3lwikihq7")))

(define-public crate-tasklet-0.1.4 (c (n "tasklet") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 2)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "05gsysdi673hk2fwvj3smla4p189vvsmqlx772pz3sxpkx9i1zly") (r "1.75.0")))

(define-public crate-tasklet-0.1.5 (c (n "tasklet") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "cron") (r "^0.12.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 2)) (d (n "time") (r "^0.3.36") (d #t) (k 0)))) (h "05mshf0rm0mnb8la7fi5i5w7pd8z9i06y6m5aqnywndyrcwv9jqv") (r "1.75.0")))

(define-public crate-tasklet-0.2.0 (c (n "tasklet") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "cron") (r "^0.12.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "simple_logger") (r "^5.0.0") (d #t) (k 2)) (d (n "time") (r "^0.3.36") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "1v3hk6l7sjw0z2hymabmxv256qgd4anniki88vjsh3la5ydp3ym0") (r "1.75.0")))

(define-public crate-tasklet-0.2.1 (c (n "tasklet") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "cron") (r "^0.12.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "simple_logger") (r "^5.0.0") (d #t) (k 2)) (d (n "time") (r "^0.3.36") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "0lw5mabj32jh0sarawhv0b6423zrvbbwnid2vr008zds9k1xd07y") (r "1.75.0")))

(define-public crate-tasklet-0.2.2 (c (n "tasklet") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "cron") (r "^0.12.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "simple_logger") (r "^5.0.0") (d #t) (k 2)) (d (n "time") (r "^0.3.36") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "1z2swfd208brk9pi0g71zkyi7g17r7768cir7xg1433fjaq7ycs6") (r "1.75.0")))

