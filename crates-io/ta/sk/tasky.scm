(define-module (crates-io ta sk tasky) #:use-module (crates-io))

(define-public crate-tasky-1.0.0 (c (n "tasky") (v "1.0.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "01fgjq0cn8fpr0lpc4q9sdaf2fsf2spmgr085jdqdjs9frlhk70k")))

(define-public crate-tasky-2.0.0 (c (n "tasky") (v "2.0.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1jp7485cjihswiq6rwyb6h0qcsbyx3qcq6p7b7ivmc48fdxwsk7s")))

(define-public crate-tasky-3.0.0 (c (n "tasky") (v "3.0.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)))) (h "00wmq49780gjabj86vxc7x3lh2m5ykh6s5jbzwfg97zdsqblgkxw")))

(define-public crate-tasky-3.0.1 (c (n "tasky") (v "3.0.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)))) (h "12kc1lvzp26bk2xgn6kqigqj0aygkgmj54nvs7im99q6mxxi2cs4")))

(define-public crate-tasky-4.0.0 (c (n "tasky") (v "4.0.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)))) (h "0l10ywbb4gz7mq29sgxc8hkm8k5f56kmhg8dwszqb5mizdz7mq9c")))

(define-public crate-tasky-5.0.0 (c (n "tasky") (v "5.0.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)))) (h "1a064cilcxszknvavbja0izr5m7gzj6r41wydp5p1gb8jrz1mn75")))

(define-public crate-tasky-6.0.0 (c (n "tasky") (v "6.0.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)))) (h "0nqwvyci5kkcc46iark32d3w00ci211gm9p1whxis4j48mxw3nqp")))

