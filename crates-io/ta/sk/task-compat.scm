(define-module (crates-io ta sk task-compat) #:use-module (crates-io))

(define-public crate-task-compat-0.1.0 (c (n "task-compat") (v "0.1.0") (d (list (d (n "futures01") (r "^0.1") (d #t) (k 0) (p "futures")) (d (n "futures03") (r "^0.3") (k 0) (p "futures")) (d (n "futures03") (r "^0.3") (f (quote ("compat"))) (d #t) (k 2) (p "futures")) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros" "sync"))) (d #t) (k 2)))) (h "0y6mi2wk3gq51dpivra0p9xwz0zl2c9pcjbsfkiak3mr8qy7ha03")))

