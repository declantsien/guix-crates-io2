(define-module (crates-io ta sk task_log) #:use-module (crates-io))

(define-public crate-task_log-0.1.0 (c (n "task_log") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0d0ncxk60al8l18d404r8d82j5in4lj6dkcpyrnvww3f0sxd03hw")))

(define-public crate-task_log-0.1.1 (c (n "task_log") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ipbaj4mimgfs4qa061iayfqixch9zln943ani885cpgm4p9mqm7")))

(define-public crate-task_log-0.1.2 (c (n "task_log") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "142gjawjwd4pv3d8lbj2g4az3l1bir689h2948k5vilakjbljwxy")))

(define-public crate-task_log-0.1.3 (c (n "task_log") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fh9kqrdyfa153a1sqvqgnzvf916bf4dpmbrnpzair434h8x7673")))

(define-public crate-task_log-0.1.4 (c (n "task_log") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0589lgqy8kavlajk3lyyfmcnwd4q9bn6y5qallyqn0f65yhxdvz5")))

(define-public crate-task_log-0.1.5 (c (n "task_log") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gbll0ips2npl9svip60f6j8cyr82jh3ib51l2d3z8l26r02fsvw")))

(define-public crate-task_log-0.1.6 (c (n "task_log") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0n9npznlxgpjxhnjaav9w2vm04vfsnwkpf5rsk8j1ifjcrvysalp")))

