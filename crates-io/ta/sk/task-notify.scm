(define-module (crates-io ta sk task-notify) #:use-module (crates-io))

(define-public crate-task-notify-0.1.0 (c (n "task-notify") (v "0.1.0") (h "0pcn3hr36jfrlgashx5ql9lx33jaz0czy0i6c8zac3m1pr5p05l3")))

(define-public crate-task-notify-1.0.0 (c (n "task-notify") (v "1.0.0") (h "0ak2gag1yxj4nnvkm0xpmbk4a1vyp8qvgld12df1sybbzmpcs959")))

