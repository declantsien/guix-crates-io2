(define-module (crates-io ta o- tao-core-video-sys) #:use-module (crates-io))

(define-public crate-tao-core-video-sys-0.2.0 (c (n "tao-core-video-sys") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "metal") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)))) (h "1xlqdcp7a5hni0jwc7yn7p4mij68f86ffv0cfb8did4w53mm0517") (f (quote (("opengl") ("io_suface") ("display_link" "opengl") ("direct3d") ("default") ("all" "display_link" "direct3d" "io_suface" "opengl"))))))

