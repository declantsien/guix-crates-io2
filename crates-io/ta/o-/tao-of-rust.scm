(define-module (crates-io ta o- tao-of-rust) #:use-module (crates-io))

(define-public crate-tao-of-rust-0.1.0 (c (n "tao-of-rust") (v "0.1.0") (d (list (d (n "csv_challenge") (r "^0.1") (d #t) (k 0)) (d (n "failures_crate") (r "^0.1") (d #t) (k 0)))) (h "1xijhiqwccj4yyny2nn12n12fggxq5f8spvjcr1x5474dlvbzvp0")))

(define-public crate-tao-of-rust-1.0.0 (c (n "tao-of-rust") (v "1.0.0") (d (list (d (n "csv_challenge") (r "^0.1") (d #t) (k 0)) (d (n "failures_crate") (r "^0.1") (d #t) (k 0)))) (h "0yp0n9ljxkzgqfjcv9apvzakv0p8ix305004hici60vv1ajyvxz6")))

(define-public crate-tao-of-rust-1.0.1 (c (n "tao-of-rust") (v "1.0.1") (d (list (d (n "csv_challenge") (r "^0.1") (d #t) (k 0)) (d (n "failures_crate") (r "^0.1") (d #t) (k 0)))) (h "0i83lg5r336w00fvakpvkbvv955wnbsblalbqaz56ys2mjj7z90a")))

(define-public crate-tao-of-rust-1.0.2 (c (n "tao-of-rust") (v "1.0.2") (d (list (d (n "csv_challenge") (r "^0.1") (d #t) (k 0)) (d (n "failures_crate") (r "^0.1") (d #t) (k 0)))) (h "1my8xpnlwvlyfchi2sfr609f3wfcn1qwvssa7ndn34nf73p7imbh")))

