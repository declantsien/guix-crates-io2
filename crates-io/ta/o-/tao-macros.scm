(define-module (crates-io ta o- tao-macros) #:use-module (crates-io))

(define-public crate-tao-macros-0.1.0 (c (n "tao-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02gx598zsgfnpjrmwa3vnl77ahp25bcq6lbir3zkjnnl8n1csvrv") (f (quote (("tray") ("default")))) (r "1.56")))

(define-public crate-tao-macros-0.1.1 (c (n "tao-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0idlb50wnfgx7cwyjv5fvfbbkgz7qzzvv2a54dc4clpbqnya89rv") (f (quote (("tray") ("default")))) (r "1.56")))

(define-public crate-tao-macros-0.1.2 (c (n "tao-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hhqgmr9yw60nwmw8xwxh44w2h2qz18nhvhkkdk8n5axa214a4gc") (f (quote (("tray") ("default")))) (r "1.56")))

