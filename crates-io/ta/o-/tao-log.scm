(define-module (crates-io ta o- tao-log) #:use-module (crates-io))

(define-public crate-tao-log-0.1.0 (c (n "tao-log") (v "0.1.0") (d (list (d (n "log") (r ">= 0.4.6, < 0.5") (d #t) (k 0)))) (h "0ld6iczda0fri4178qd426hszfdfgk0jf5wcn0hnahz9xkz6ph5f")))

(define-public crate-tao-log-0.2.0 (c (n "tao-log") (v "0.2.0") (d (list (d (n "log") (r ">= 0.4.6, < 0.5") (d #t) (k 0)) (d (n "parking_lot") (r ">= 0.7.1, < 0.8") (d #t) (k 2)))) (h "17ldbzj64a1mcsbj62cw04r76id24g7kqwmm0rlpi3m4szsh70cr") (f (quote (("std" "log/std"))))))

(define-public crate-tao-log-0.2.1 (c (n "tao-log") (v "0.2.1") (d (list (d (n "log") (r ">= 0.4.6, < 0.5") (d #t) (k 0)) (d (n "parking_lot") (r ">= 0.9.0, < 0.10") (d #t) (k 2)))) (h "1i5frskp3fs58mak2cfd0swq73j0drlc782dkz13fkx8qrp3mid6") (f (quote (("std" "log/std"))))))

(define-public crate-tao-log-1.0.0 (c (n "tao-log") (v "1.0.0") (d (list (d (n "log") (r ">= 0.4.6, < 0.5") (d #t) (k 0)) (d (n "parking_lot") (r ">= 0.9.0, < 0.10") (d #t) (k 2)))) (h "1g8chzfr3pd2ywkk6g1rgq03s0ccz3fx642w7g0d5frz2dvnh6x8") (f (quote (("std" "log/std"))))))

(define-public crate-tao-log-1.0.1 (c (n "tao-log") (v "1.0.1") (d (list (d (n "log") (r ">=0.4.6, <0.5") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.9.0, <0.10") (d #t) (k 2)))) (h "1l3r0hc61cj41v69ic39h1rz88n1vk6dfznqcdjq4kxmxg3p5nqv") (f (quote (("std" "log/std"))))))

