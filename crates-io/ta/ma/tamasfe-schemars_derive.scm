(define-module (crates-io ta ma tamasfe-schemars_derive) #:use-module (crates-io))

(define-public crate-tamasfe-schemars_derive-0.8.0 (c (n "tamasfe-schemars_derive") (v "0.8.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.25") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "045b025xvpf9vhyr5m1kw4gzykyz3clr47fzn46dx7p5nk3xsrcv")))

