(define-module (crates-io ta ma tamata) #:use-module (crates-io))

(define-public crate-tamata-0.1.0 (c (n "tamata") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "tamata-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tamata-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1hgal4hf9bn0rc2q0z2q5ja65gb05p19ncy4a9fnaizm1bjyh01k")))

