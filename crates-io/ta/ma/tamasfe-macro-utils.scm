(define-module (crates-io ta ma tamasfe-macro-utils) #:use-module (crates-io))

(define-public crate-tamasfe-macro-utils-0.1.0 (c (n "tamasfe-macro-utils") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f6bdivg2w3q5hd2bfq41ssdmrqh85a1dl5nir5c0cji7wzx292l")))

(define-public crate-tamasfe-macro-utils-0.1.1 (c (n "tamasfe-macro-utils") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qcw0z5yymcwjzrmx0ayh5bv7x0yavsisjp669976i4n98grcicx")))

(define-public crate-tamasfe-macro-utils-0.1.2 (c (n "tamasfe-macro-utils") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i2p077n7w7zmx6lsndh9agr2yb8xwyihpga78ra99asbn2gpfp6")))

