(define-module (crates-io ta ma tamasfe-lsp-types) #:use-module (crates-io))

(define-public crate-tamasfe-lsp-types-0.85.0 (c (n "tamasfe-lsp-types") (v "0.85.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "17lc9c17idqx3m6hnh6sdjkdjr4f9lldwfa951pd73yn92id5pl4") (f (quote (("proposed") ("default"))))))

