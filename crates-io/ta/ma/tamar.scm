(define-module (crates-io ta ma tamar) #:use-module (crates-io))

(define-public crate-tamar-0.1.0 (c (n "tamar") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0clv13yimn39hb3n5pdynba3kia5lavbgkap7vbg033xid9vrkps")))

(define-public crate-tamar-0.3.0 (c (n "tamar") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19566svfvi3apkjw4ymf6p406f5h30gbv2j1clr47ghxl2ynpzrr")))

(define-public crate-tamar-0.4.0 (c (n "tamar") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qbbz2msvy0h946md0r127dlf8i841abdmfifx6nn1jnp4qxx1mp")))

(define-public crate-tamar-0.4.1 (c (n "tamar") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0swzkx6yv55hhn30l7ay42z5527c7szgkj3cc5dkhr8bgjxj6w8a")))

(define-public crate-tamar-0.5.0 (c (n "tamar") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1crd3njhabs6skh88rqg59lzbgy0xaxbv9yjbxnp3w6zmfrhgcxc")))

(define-public crate-tamar-0.5.1 (c (n "tamar") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sswm4c3p2q81izhyiv7j7r0jv8dkqyql9gpdagb5bfam65nl9y3")))

