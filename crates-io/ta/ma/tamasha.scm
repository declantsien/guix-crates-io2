(define-module (crates-io ta ma tamasha) #:use-module (crates-io))

(define-public crate-tamasha-0.1.0 (c (n "tamasha") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1qppvhjqsi6lp96pdm1gmwycp2cnwinjv91jgj2nmpjfkd44cx5f")))

