(define-module (crates-io ta ma tamasfe-json-rpc-types) #:use-module (crates-io))

(define-public crate-tamasfe-json-rpc-types-0.1.0 (c (n "tamasfe-json-rpc-types") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "11d2d71f338y8iibv1j4ylrh9xrck4h9ap0y7v3bnzr11kw2l9fq")))

(define-public crate-tamasfe-json-rpc-types-0.1.1 (c (n "tamasfe-json-rpc-types") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "00vliv1w414n8c70c4rh1hh50ak1836lwnzndr5clnky687abaa7")))

(define-public crate-tamasfe-json-rpc-types-0.1.2 (c (n "tamasfe-json-rpc-types") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1faz49z9c0k3yfgnlaq27xakagmnjppr32ifkh7jrg0h213fjhdd")))

