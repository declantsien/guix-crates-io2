(define-module (crates-io ta ma tamata-macros) #:use-module (crates-io))

(define-public crate-tamata-macros-0.1.0 (c (n "tamata-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "tamata-core") (r "^0.1.0") (d #t) (k 0)))) (h "17bnpzkdzng28vrb874wdzv5x7pwvchpr14lfqwh0b98gz42mfs7")))

