(define-module (crates-io ta ro tarolog) #:use-module (crates-io))

(define-public crate-tarolog-0.1.0 (c (n "tarolog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("default" "process"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde-json-core") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tarantool") (r "^4.0.0") (f (quote ("picodata"))) (d #t) (k 0)) (d (n "va_list-helper") (r "^0.0.2") (d #t) (k 2)))) (h "0p7i94905j5dqhrrrv4cr87vaimzxc6sp4ylczbg3qaws7q14d1w")))

