(define-module (crates-io ta ro taro) #:use-module (crates-io))

(define-public crate-taro-0.1.0 (c (n "taro") (v "0.1.0") (d (list (d (n "assert_cli") (r ">= 0.6.3") (d #t) (k 2)) (d (n "assert_cmd") (r ">= 0.10") (d #t) (k 2)) (d (n "clap") (r ">= 2.32.0") (f (quote ("color" "suggestions" "wrap_help"))) (d #t) (k 0)) (d (n "filetime") (r ">= 0.2.4") (d #t) (k 0)) (d (n "generic-array") (r ">= 0.12.0") (d #t) (k 2)) (d (n "lazy_static") (r ">= 1.1.0") (d #t) (k 2)) (d (n "sha2") (r ">= 0.1.0") (d #t) (k 2)) (d (n "walkdir") (r ">= 2.0") (d #t) (k 2)))) (h "0f4g918544wjqzrvy591apsgf7am50wi3lmnqsvcqi69fs67fc00")))

