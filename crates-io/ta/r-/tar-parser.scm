(define-module (crates-io ta r- tar-parser) #:use-module (crates-io))

(define-public crate-tar-parser-0.0.1 (c (n "tar-parser") (v "0.0.1") (d (list (d (n "nom") (r "~0.3.2") (d #t) (k 0)))) (h "0jq2l5c1kls40mhw9girr41yxg9zbai3sp8q93ibmqav6hx5jcmm")))

(define-public crate-tar-parser-0.1.0 (c (n "tar-parser") (v "0.1.0") (d (list (d (n "nom") (r "~0.3.4") (d #t) (k 0)))) (h "0knp0z1ldk3rfjrywnvsgp0sfi7kkmm1r29ff7mfrn9a2982qi7q")))

(define-public crate-tar-parser-0.2.0 (c (n "tar-parser") (v "0.2.0") (d (list (d (n "nom") (r "~1.0.0") (d #t) (k 0)))) (h "0ydgi3awld56n6d1l7z1nf2plwq6iw36yxzb07k4hch2wxh3mfzv")))

(define-public crate-tar-parser-0.2.1 (c (n "tar-parser") (v "0.2.1") (d (list (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "183xp3y6mgp7kw2jh1mfsgxpjl5v13b1w85rhr2adg3sf0zj3knx")))

(define-public crate-tar-parser-0.3.0 (c (n "tar-parser") (v "0.3.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "0yp4a07hk8iz18a3silwzs6xxg3ls7m6l8wd52hyaw19wrbwr1id")))

(define-public crate-tar-parser-0.3.1 (c (n "tar-parser") (v "0.3.1") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "0180wchga7nywqgf87897r734alywip8p2c6d37325rs7nyrxv98")))

(define-public crate-tar-parser-0.4.0 (c (n "tar-parser") (v "0.4.0") (d (list (d (n "nom") (r "^2.0.0") (d #t) (k 0)))) (h "0jls1awzq292qpa53lpiqjqsk72bplkazy108yzw5c63dim0wj87")))

(define-public crate-tar-parser-0.4.1 (c (n "tar-parser") (v "0.4.1") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "16ff7b8s7wh28cxndl8jp0ip4aram60jpshasnv01x4nr6mjpr41") (y #t)))

(define-public crate-tar-parser-0.4.2 (c (n "tar-parser") (v "0.4.2") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "17s1naqdjbcchbsd9cslcy7rpxwas2m1r1dswhbmi6z286kwdxhi")))

(define-public crate-tar-parser-0.4.3 (c (n "tar-parser") (v "0.4.3") (d (list (d (n "nom") (r "^2.1.0") (d #t) (k 0)))) (h "0rnzbrlcngfsi58abgrbsjd8igw4vd4jh6ygi0c4x6v5jkapxw6c")))

(define-public crate-tar-parser-0.5.0 (c (n "tar-parser") (v "0.5.0") (d (list (d (n "nom") (r "^2.1.0") (d #t) (k 0)))) (h "1krb919d2ag20i3hbf95hn7p389dx861v11xkzk6vv09lz1138sj")))

(define-public crate-tar-parser-0.6.0 (c (n "tar-parser") (v "0.6.0") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)))) (h "1ig2ks0wl8m1czspi7w6xndg054fag92mkivzwri8ar2mgxlxckh")))

