(define-module (crates-io ta r- tar-no-std) #:use-module (crates-io))

(define-public crate-tar-no-std-0.1.0 (c (n "tar-no-std") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "1x9v85s7s8ynf4hhwjibvn6g6cpvr978c5g5vnzr3cyxpd9j4zlb") (y #t)))

(define-public crate-tar-no-std-0.1.1 (c (n "tar-no-std") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "1qdan6ban8l18qczkblw3d9gxf8yy5c2yh60nl12wqb3a70myika") (y #t)))

(define-public crate-tar-no-std-0.1.2 (c (n "tar-no-std") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "0a14iadsv80ks9p8n5m46r65waccnzwnmajspbiy2r1gqjfywhdi")))

(define-public crate-tar-no-std-0.1.3 (c (n "tar-no-std") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "1wb856jjp0qw39pz1qannka8azpd0wvbgni0r2ygl0ckjvlqp7bp")))

(define-public crate-tar-no-std-0.1.4 (c (n "tar-no-std") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "0s2hfpm0sycgz86nl7f4n5npz9k1ip78fbs2mb4pk57j5a8i37d8")))

(define-public crate-tar-no-std-0.1.5 (c (n "tar-no-std") (v "0.1.5") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "1mq17ifj20gdjz4irgvvf4i87cljw5rvgjgyhrwkg486xh9263nm") (f (quote (("default") ("alloc") ("all" "alloc"))))))

(define-public crate-tar-no-std-0.1.6 (c (n "tar-no-std") (v "0.1.6") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "0ly59qzlw2jh8n8j2x4yd87x3w6v5s3wxrslkzr4fnwna6sr23hq") (f (quote (("default") ("alloc") ("all" "alloc"))))))

(define-public crate-tar-no-std-0.1.7 (c (n "tar-no-std") (v "0.1.7") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "0sl36qggnz6f1zqfhifflk3px93rrlf8dgpwwg1z27mbaq499rgc") (f (quote (("default") ("alloc") ("all" "alloc"))))))

(define-public crate-tar-no-std-0.1.8 (c (n "tar-no-std") (v "0.1.8") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "1d7gwigjmh3hf8lbbl5zsadczgsqfq1yh466rrkdl88qjphvg8ns") (f (quote (("default") ("alloc") ("all" "alloc"))))))

(define-public crate-tar-no-std-0.2.0 (c (n "tar-no-std") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)))) (h "1i9zdwkjfa7qpvs647qir9xcd0492iwjkf50skvax9xd1rsqhxnq") (f (quote (("default") ("alloc"))))))

(define-public crate-tar-no-std-0.3.0 (c (n "tar-no-std") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "memchr") (r "^2.7") (k 0)) (d (n "num-traits") (r "~0.2") (k 0)))) (h "0h9hf8crwn28vfmkjz35rc2ar7bcbr3l8vd3m5h0dj9y5r3yl44a") (f (quote (("unstable") ("default") ("alloc")))) (r "1.76.0")))

(define-public crate-tar-no-std-0.3.1 (c (n "tar-no-std") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "memchr") (r "^2.7") (k 0)) (d (n "num-traits") (r "~0.2") (k 0)))) (h "1im22jzl4qlx3dzbq0cc8d4df6l88ciqigy75isiadh3xs87g2ad") (f (quote (("unstable") ("default") ("alloc")))) (r "1.76.0")))

