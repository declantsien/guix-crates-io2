(define-module (crates-io ta r- tar-parser2) #:use-module (crates-io))

(define-public crate-tar-parser2-0.7.0 (c (n "tar-parser2") (v "0.7.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1a9vabfi77pv5h4jkdmwsq9p3h8agkp2yrxq1550bic50hdvqx3n")))

(define-public crate-tar-parser2-0.7.1 (c (n "tar-parser2") (v "0.7.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1r841mbiy1cipjq3a9wnsh5j350fv6f5z4284c8lpnkgzb3dmfpz")))

(define-public crate-tar-parser2-0.8.0 (c (n "tar-parser2") (v "0.8.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)))) (h "1gkdskx24fmqkk2y6axvlddg3vr2psvg2lj9gm3nzcsnp9fsmlya")))

(define-public crate-tar-parser2-0.9.0 (c (n "tar-parser2") (v "0.9.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)))) (h "08madp75rcb7ls39ssghpf72qgxmi110szimgvp8l601pyx7n3kz")))

(define-public crate-tar-parser2-0.9.1 (c (n "tar-parser2") (v "0.9.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)))) (h "0bc8xj9hs6b0m0921f3gh4zpjyyj5ncx8sd09cpbjvpiks6g8qkj")))

