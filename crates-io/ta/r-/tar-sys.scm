(define-module (crates-io ta r- tar-sys) #:use-module (crates-io))

(define-public crate-tar-sys-0.2.0 (c (n "tar-sys") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1bg8jgsvv7v4qzdk1jw53jy8q8h2wxgcgg93xw4gr6kd4zjsp536")))

(define-public crate-tar-sys-0.2.1 (c (n "tar-sys") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1l15f11px1l0c6d98v1k98qyk7g3r9hcpb78mvz09myg29735wxd")))

(define-public crate-tar-sys-0.2.2 (c (n "tar-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rlpiahq301llwcpybfv5n7x5l8y1p3rahk1fmk1rgns6acjfhvg")))

(define-public crate-tar-sys-0.3.0 (c (n "tar-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xn1c2dq6g1xqfnwnfsggcwws02rvl2n9v6rjzvy87cz3503175l")))

