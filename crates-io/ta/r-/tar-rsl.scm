(define-module (crates-io ta r- tar-rsl) #:use-module (crates-io))

(define-public crate-tar-rsl-0.1.0 (c (n "tar-rsl") (v "0.1.0") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^1.0") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0zdljj4rkmmnxfkp0s31951fxsmy3s09md26y46i1nzb8nvh7q1p") (f (quote (("default" "xattr"))))))

(define-public crate-tar-rsl-0.1.1 (c (n "tar-rsl") (v "0.1.1") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "jdks") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^1.0") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0nfzb8axvzf8wxrwl31rq6kzlvyyf45hcddizwcsvddqr55shvhj") (f (quote (("default" "xattr"))))))

