(define-module (crates-io ta r- tar-wasi) #:use-module (crates-io))

(define-public crate-tar-wasi-0.4.37 (c (n "tar-wasi") (v "0.4.37") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1lwfs5dg5rdkmz6hag5vyllhaf7vi6474fggw3yc1mk942swkj1x") (f (quote (("default" "xattr"))))))

(define-public crate-tar-wasi-0.4.38 (c (n "tar-wasi") (v "0.4.38") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "15w85zxyd3hmrcbhpr3ciz1j0iv5i2wzb2w4y3a5pwcgm8wxwgvw") (f (quote (("default" "xattr"))))))

