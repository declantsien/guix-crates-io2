(define-module (crates-io ta rn tarnished) #:use-module (crates-io))

(define-public crate-tarnished-0.1.0 (c (n "tarnished") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bio") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0mgwyjgzbi7hyj71lah0jbix40qwmjkzgyv450i07pmrdbs6ifcz")))

(define-public crate-tarnished-0.2.0 (c (n "tarnished") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bio") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0ryjpwjc1gg62f1n97nadpspxxqw2psk8y00fkisrqz7z7d0h553")))

(define-public crate-tarnished-0.3.0 (c (n "tarnished") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bio") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0vgfd4xd69hvv2bharjm9bd0yj05rcq22r7biq548y6hsi8kqckl")))

(define-public crate-tarnished-0.4.0 (c (n "tarnished") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bio") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "09p8vf83gcjh8n451h63p71825a321h46pqqzv396ccqkb0vswgp")))

