(define-module (crates-io ta il tailwind_fuse) #:use-module (crates-io))

(define-public crate-tailwind_fuse-0.1.0 (c (n "tailwind_fuse") (v "0.1.0") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "tailwind_fuse_macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0w1p425m1lsaw1syl2sxh6xhmq6f6lxbkfcp0vr72li4mdx2i0yl") (f (quote (("variant" "tailwind_fuse_macro") ("debug"))))))

(define-public crate-tailwind_fuse-0.1.1 (c (n "tailwind_fuse") (v "0.1.1") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tailwind_fuse_macro") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0vjzl9q1ks61xbkb18a0zvpdk4zx0wzl7hsbsd7l78k1c45yv8bb") (f (quote (("variant" "tailwind_fuse_macro") ("debug"))))))

(define-public crate-tailwind_fuse-0.2.0 (c (n "tailwind_fuse") (v "0.2.0") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tailwind_fuse_macro") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1a1lfq5yzbhkm7zk5z1hyzq8g50jr13svz0cbpbr1rnl7dnq2h6m") (f (quote (("variant" "tailwind_fuse_macro") ("debug"))))))

(define-public crate-tailwind_fuse-0.3.0 (c (n "tailwind_fuse") (v "0.3.0") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tailwind_fuse_macro") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1fymnxhhnli9crk8bcccahlaai7pzh9x8l5i1idmh4p8h4wzjvzw") (f (quote (("variant" "tailwind_fuse_macro") ("debug"))))))

