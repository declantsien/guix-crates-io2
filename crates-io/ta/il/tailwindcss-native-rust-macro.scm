(define-module (crates-io ta il tailwindcss-native-rust-macro) #:use-module (crates-io))

(define-public crate-tailwindcss-native-rust-macro-0.1.0 (c (n "tailwindcss-native-rust-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1bj1saacz6vh6247ry9wcbn5hq2wl5axaigkw9grfc1fslkcbsbj") (r "1.77.2")))

