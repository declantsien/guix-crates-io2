(define-module (crates-io ta il tailor) #:use-module (crates-io))

(define-public crate-tailor-0.1.0 (c (n "tailor") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "fgen") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.41") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.41") (d #t) (k 0)))) (h "1hsqv6a5v8l1n843xz2h6sj7s5ghxvgg62wnp410ysxv5bc3n6n4")))

