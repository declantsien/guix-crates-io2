(define-module (crates-io ta il tail) #:use-module (crates-io))

(define-public crate-tail-0.1.0 (c (n "tail") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "inotify") (r "^0.5.0") (d #t) (k 0)))) (h "1zkb3icyjp1fzdmib0ncbj62jwrfzfzcqqvydd1yq9h5l2q6wskr")))

(define-public crate-tail-0.2.0 (c (n "tail") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "inotify") (r "^0.5.0") (d #t) (k 0)))) (h "1ikvmvm12765x3fa6gvalairm6lw2qiacv1rr6w14210sxbnflj6")))

(define-public crate-tail-0.3.0 (c (n "tail") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "inotify") (r "^0.5.0") (d #t) (k 0)))) (h "0xq3dc4f8ahvpfkjkw66rw4dycnbk1glvcgzha4dyj2ypdriyx16")))

