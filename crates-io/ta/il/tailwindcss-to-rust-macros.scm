(define-module (crates-io ta il tailwindcss-to-rust-macros) #:use-module (crates-io))

(define-public crate-tailwindcss-to-rust-macros-0.1.0 (c (n "tailwindcss-to-rust-macros") (v "0.1.0") (h "01pligwc1965w5nyz04z198ypmk5xx9mw4ls2rk62nmsqxxs33h4")))

(define-public crate-tailwindcss-to-rust-macros-0.1.1 (c (n "tailwindcss-to-rust-macros") (v "0.1.1") (h "16s5r5jjfqj5lirib49hzbnlv44ca0gginb94m3b2vk3m1frw7x6")))

(define-public crate-tailwindcss-to-rust-macros-0.1.2 (c (n "tailwindcss-to-rust-macros") (v "0.1.2") (h "10k69gjryism09lasidgbir1irx6pjfnljj46kzqllsnyx2jdw8g")))

(define-public crate-tailwindcss-to-rust-macros-0.1.3 (c (n "tailwindcss-to-rust-macros") (v "0.1.3") (h "0r4rp5g931dhp94n3m8fdjylnh99xcpbp9m02ksac8mjgpi31pkq")))

