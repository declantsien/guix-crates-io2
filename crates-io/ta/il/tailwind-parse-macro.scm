(define-module (crates-io ta il tailwind-parse-macro) #:use-module (crates-io))

(define-public crate-tailwind-parse-macro-0.8.0 (c (n "tailwind-parse-macro") (v "0.8.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "11bxq3a50334pslx1s0x7jrvnpv5204b4gh0l4jl323i5mxrsi27")))

(define-public crate-tailwind-parse-macro-0.8.1 (c (n "tailwind-parse-macro") (v "0.8.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0xxzzi0fpx1phwghawi1i056h57z90dgagln0mskhc65s4vr363w")))

(define-public crate-tailwind-parse-macro-0.9.0 (c (n "tailwind-parse-macro") (v "0.9.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1mg58zml764w33w3rcg79lrda1qv8rszd8fs8l0k4p6m6pfp8xv0")))

(define-public crate-tailwind-parse-macro-0.10.0 (c (n "tailwind-parse-macro") (v "0.10.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0kwp09mlhcsdfpkk12qgiz75b76x4gygbm32bidsqvnh79hkiqfj")))

(define-public crate-tailwind-parse-macro-0.11.0 (c (n "tailwind-parse-macro") (v "0.11.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1b27kd7vixzyvy85d0v6l8sqkxkhb9qdglwvfqf9x51zaby8z9w8")))

(define-public crate-tailwind-parse-macro-0.11.1 (c (n "tailwind-parse-macro") (v "0.11.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0y5n9swvasgxdss77hrgi2d93ad1lk12srmaiv4y8vmsc0i0lad2")))

(define-public crate-tailwind-parse-macro-0.12.0 (c (n "tailwind-parse-macro") (v "0.12.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0v69g1fbcliqn5wmb6x2a7nmvc9c8bwlldfzflmfwlss52jccs67")))

(define-public crate-tailwind-parse-macro-0.13.0 (c (n "tailwind-parse-macro") (v "0.13.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0hk54i1x0mwv1rhwyyg6ndc9s8mgdzxi0ii8wv1hsv0pwm99crjx")))

(define-public crate-tailwind-parse-macro-0.14.0 (c (n "tailwind-parse-macro") (v "0.14.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1p4xmdgiirhk13pn5l27babgv0c0i22gii9sanf3bq3b3pv825r5")))

(define-public crate-tailwind-parse-macro-0.15.0 (c (n "tailwind-parse-macro") (v "0.15.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0fsjsn0j0a9rqxc3mpbx6rqnyp8871dvw0zhjrrq6pxijp2a4w0g")))

(define-public crate-tailwind-parse-macro-0.16.0 (c (n "tailwind-parse-macro") (v "0.16.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0yjdk6nvph13a2lp9fa3pxc15kqxiw1kh94ajlch742gh17qwkm0")))

(define-public crate-tailwind-parse-macro-0.17.0 (c (n "tailwind-parse-macro") (v "0.17.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "172ar672127r9lg5swai6vn4adznp7j525jq788klsrarf53gkcw")))

