(define-module (crates-io ta il taildir) #:use-module (crates-io))

(define-public crate-taildir-0.1.0 (c (n "taildir") (v "0.1.0") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1nd7ligls8882m08033cvn1j76bcbm572iv5l5iayj7fldbxjwch")))

(define-public crate-taildir-0.1.1 (c (n "taildir") (v "0.1.1") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0kdx4vsvk2jb8185ccyxc41ckvxcmv98zwb3clp7d47kjyifgrd4")))

(define-public crate-taildir-0.1.2 (c (n "taildir") (v "0.1.2") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1s4xlzg8fan0hrnb0s6x50axsb1snswcahgrnmrxvxcfiqx1dfwz")))

(define-public crate-taildir-0.2.0 (c (n "taildir") (v "0.2.0") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14gpif429m50j3d4rva0aqxqnbywix2v2ijnsibvx8sz711x9ibl")))

