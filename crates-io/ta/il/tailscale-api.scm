(define-module (crates-io ta il tailscale-api) #:use-module (crates-io))

(define-public crate-tailscale-api-0.1.0 (c (n "tailscale-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("chrono" "uuid"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c0wdzx3nqv7fpwbqdqynxggakv907ca6qr21a5290vl53iwl58h")))

(define-public crate-tailscale-api-0.1.1 (c (n "tailscale-api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b215w74cll5kdnyc9znrr8yd62vc9r56gb0ffxp9aivvaqnyw36")))

(define-public crate-tailscale-api-0.1.2 (c (n "tailscale-api") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q6wn46wk6k2dba9lkvyp8888szrh5jqf93m44g76fx2pikca24d")))

(define-public crate-tailscale-api-0.1.3 (c (n "tailscale-api") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vmyqn8w6x6gk1p5cdjshsb7vxmv2fr6nr066q18q896m94sl24w")))

(define-public crate-tailscale-api-0.1.4 (c (n "tailscale-api") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rwzgc8b0d1s6xwisgg2aldv43sz4kb5v1y30k4zly7666gviwbl")))

(define-public crate-tailscale-api-0.1.5 (c (n "tailscale-api") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m70fpb0rpw9b9dqby2s7ss7h5c3kx9hm2b0vip77kzfg1pinwgi")))

