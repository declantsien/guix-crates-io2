(define-module (crates-io ta il tailwind_fuse_macro) #:use-module (crates-io))

(define-public crate-tailwind_fuse_macro-0.1.0 (c (n "tailwind_fuse_macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ybgphb6025djxg11nszw5dgwsixyadmm39msg5qaadawdjhw7qk")))

(define-public crate-tailwind_fuse_macro-0.1.1 (c (n "tailwind_fuse_macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1blpmrvjl4i54kf4kclrh4smj1m7g8caffr04wi8291yzq7yqnjc")))

(define-public crate-tailwind_fuse_macro-0.2.0 (c (n "tailwind_fuse_macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0iif9ic38w3gfyp7bkskc2vdss55my7sb6jdg5khbw4wl1v0lfs7")))

(define-public crate-tailwind_fuse_macro-0.3.0 (c (n "tailwind_fuse_macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0fxnn8r68m0g1cya1k4kgxhsmszj76bhi3bsbgb57imdyszrznwk")))

