(define-module (crates-io ta il tail-reader) #:use-module (crates-io))

(define-public crate-tail-reader-0.1.0 (c (n "tail-reader") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "196df4p9hbwfzfy8l32xmmhyhfsldjpk6qbn327mzfsi2m10qwhl") (y #t)))

(define-public crate-tail-reader-0.0.3 (c (n "tail-reader") (v "0.0.3") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1m1ypmy8cmads3n1w6v7c1r6xsgj3bq9fhf3m0m5qxm9hk34hbiz") (y #t)))

(define-public crate-tail-reader-0.0.4 (c (n "tail-reader") (v "0.0.4") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "11mwab4cnpxz18s8h0760vfvx1laxv049fsisi296pq2vxd6dh60")))

(define-public crate-tail-reader-0.0.5 (c (n "tail-reader") (v "0.0.5") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "13a9hj705m2xdvnbjanv4j6d5plhdxf8kix5n8inhysizn1hv0mg")))

(define-public crate-tail-reader-0.0.6 (c (n "tail-reader") (v "0.0.6") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1387bf1sgbgbh3lfy9fa1j5z72w3c1l8xim58j89f450wr8fsh4y")))

(define-public crate-tail-reader-0.0.7 (c (n "tail-reader") (v "0.0.7") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "00sw28hdg2cpyk3nhx9qlprq006v3hdr9f3rxsgylbh6gs3mkwy6")))

(define-public crate-tail-reader-0.0.10 (c (n "tail-reader") (v "0.0.10") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1jc28h5s9qb369c57rp6cjvjz0ia1p06w3igh736czj5piy3zwwx")))

(define-public crate-tail-reader-0.1.1 (c (n "tail-reader") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "10ilgfrpl10b9ajsq6336hl0m8s04d98j0gn4dsq9kp189x84bk0")))

(define-public crate-tail-reader-1.0.0 (c (n "tail-reader") (v "1.0.0") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "1ndq5idl234f64r81yly4kx5s0kx7vq3p82if2d7vykln31nmlsw")))

(define-public crate-tail-reader-1.0.1 (c (n "tail-reader") (v "1.0.1") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "1vj4j1f43bz1g6id3g58gzc6vfxw66av7w54plfccmlxf001nppi")))

(define-public crate-tail-reader-2.0.0 (c (n "tail-reader") (v "2.0.0") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "0fg57sx3z277p61pvzani80m666ahflrsxzrp9knlvd3gi89mhwm")))

(define-public crate-tail-reader-2.0.1 (c (n "tail-reader") (v "2.0.1") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "18p7zryrzb193lvsa7h330pfvhjy6snjwzs5ag78qjhly0r76r22")))

(define-public crate-tail-reader-2.0.2 (c (n "tail-reader") (v "2.0.2") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "16cndlk00lc4yll8cpr2cvhp29wr32ybqi3sbfcbn4q9dnhis6ff")))

(define-public crate-tail-reader-2.0.3 (c (n "tail-reader") (v "2.0.3") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "0fjinsys2vi5vpcnq1agxax2dgk14aajp2j8qxagjwgj9fcj8drl")))

(define-public crate-tail-reader-2.0.4 (c (n "tail-reader") (v "2.0.4") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "1shziwncxbc2hqw4f8qrsrk4scqhsbhjlcwx7a5bc8riyzb6d029")))

(define-public crate-tail-reader-2.0.5 (c (n "tail-reader") (v "2.0.5") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "18lxvgy6migzl927vkasz1aza4zjvglxfifqzzmxavzvvn4f8q18")))

(define-public crate-tail-reader-2.1.0 (c (n "tail-reader") (v "2.1.0") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "0qjbd0yr0chi7w518jgi6nq391mp6a7zrdjizjf19rbjmnxyqhn8")))

(define-public crate-tail-reader-2.1.1 (c (n "tail-reader") (v "2.1.1") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "0b806q0zqd4ykpmhd41vjngqy1f07z2079y4l0kszlkshj22sfls")))

(define-public crate-tail-reader-2.1.2 (c (n "tail-reader") (v "2.1.2") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "0pswcncgj1yxfdjbi79pswapxmidwzk2yfj54i43fmdh7csgz6bn")))

(define-public crate-tail-reader-2.1.3 (c (n "tail-reader") (v "2.1.3") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "0srzvsja8793yrh77s7i3js0fb5f8qavbwwxgr9jnfc9mrcif6cj")))

(define-public crate-tail-reader-3.0.0 (c (n "tail-reader") (v "3.0.0") (d (list (d (n "chashmap") (r "^2.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 0)))) (h "1sjjz2p9y4k63fpy13ykwnkfq81arbpgw9qz1srs5h7ghbrgzz48")))

