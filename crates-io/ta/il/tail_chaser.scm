(define-module (crates-io ta il tail_chaser) #:use-module (crates-io))

(define-public crate-tail_chaser-0.1.0 (c (n "tail_chaser") (v "0.1.0") (h "1rl8w2v5y7fn22z8myi2l3hhb8wni1caxykw5gfbasxapdvs2jl9") (y #t)))

(define-public crate-tail_chaser-0.1.1 (c (n "tail_chaser") (v "0.1.1") (h "0vnnfpgrprm4iybdja2igyp7dx9vp7m965xn7i18yz85i0ci8g5h") (y #t)))

(define-public crate-tail_chaser-0.1.2 (c (n "tail_chaser") (v "0.1.2") (h "05mfii28z2844j044288kg9xmfgm4j98dpi7bjhmiprra4rlh9c1") (y #t)))

(define-public crate-tail_chaser-0.1.3 (c (n "tail_chaser") (v "0.1.3") (h "15d84x20gdcbr8jflb3di9s8dhh3xc7530s13q7fnfswxnv8al9y") (y #t)))

(define-public crate-tail_chaser-0.1.4 (c (n "tail_chaser") (v "0.1.4") (h "0f876vixgd41an4l5pb6p0yc9pc9iayy7zsbcbbfxbs5fjzfjn75") (y #t)))

