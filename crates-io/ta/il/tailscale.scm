(define-module (crates-io ta il tailscale) #:use-module (crates-io))

(define-public crate-tailscale-0.1.0 (c (n "tailscale") (v "0.1.0") (d (list (d (n "ipnetwork") (r "^0.16.0") (d #t) (k 0)) (d (n "pnet") (r "^0.26.0") (d #t) (k 0)))) (h "0j498cyvvixxlc2lrfjrq8i70l11f6nxlnr91k9sj1l83v01cpi4")))

(define-public crate-tailscale-0.1.1 (c (n "tailscale") (v "0.1.1") (d (list (d (n "ipnetwork") (r "^0.16.0") (d #t) (k 0)) (d (n "pnet") (r "^0.26.0") (d #t) (k 0)))) (h "12k6ppc5x1qlbs1nsfa7dmdz66pzqwfyjvshkgy97v3f95k1xa7k")))

