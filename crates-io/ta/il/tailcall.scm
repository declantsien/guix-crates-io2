(define-module (crates-io ta il tailcall) #:use-module (crates-io))

(define-public crate-tailcall-0.1.0 (c (n "tailcall") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.40") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0nh7kql9v9yxza16zinydd73zpsndkz0qx3ndn0pgd929gfsq3wl")))

(define-public crate-tailcall-0.1.1 (c (n "tailcall") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.40") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1ddh14raq8rf1a8safb2hw45qip2gcfia5zzbz9frlxzjpy20cxs") (y #t)))

(define-public crate-tailcall-0.1.2 (c (n "tailcall") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.40") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0nxj5czbad91285im6g7dj2ljn7p9qsx4a00jglz5v5wvsd75mqi")))

(define-public crate-tailcall-0.1.4 (c (n "tailcall") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3.40") (d #t) (k 2)) (d (n "tailcall-impl") (r "^0.1.4") (d #t) (k 0)))) (h "04glpvm71w5c3cviwayq28ikc9qfhac1m866l518xlqcp0sv9avz")))

(define-public crate-tailcall-0.1.5 (c (n "tailcall") (v "0.1.5") (d (list (d (n "backtrace") (r "^0.3.40") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "tailcall-impl") (r "^0.1.4") (d #t) (k 0)))) (h "1kvlxsc4wa3xl50d6ca3lijmmqalqgymg1g226fm348c1akrliw7") (y #t)))

(define-public crate-tailcall-0.1.6 (c (n "tailcall") (v "0.1.6") (d (list (d (n "backtrace") (r "^0.3.40") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "tailcall-impl") (r "^0.1.6") (d #t) (k 0)))) (h "0f79951pxrxr0bvka1ksda1l6nnhbas1w6s6pmkp585cm759ifjx")))

(define-public crate-tailcall-1.0.0 (c (n "tailcall") (v "1.0.0") (d (list (d (n "backtrace") (r "^0.3.40") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "tailcall-impl") (r "^1.0.0") (d #t) (k 0)))) (h "0krg68k2hq0l9g2q31b5kdqa97vzcp8c07wl64h2g7ayfxj78gmh")))

(define-public crate-tailcall-1.0.1 (c (n "tailcall") (v "1.0.1") (d (list (d (n "backtrace") (r "~0.3") (d #t) (k 2)) (d (n "bencher") (r "~0.1") (d #t) (k 2)) (d (n "tailcall-impl") (r "^1.0.1") (d #t) (k 0)))) (h "1fxhdb2xifqbg9qzvmcyimpdsz1wl82y2fnh57z6bc7aidrbhp39")))

