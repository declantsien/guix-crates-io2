(define-module (crates-io ta il tailwind-ast) #:use-module (crates-io))

(define-public crate-tailwind-ast-0.1.0 (c (n "tailwind-ast") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "021pwg8rynlp050w7ib2k29llzwjmnsy095lvjhprk9hl8qnqwas") (f (quote (("default"))))))

(define-public crate-tailwind-ast-0.2.0 (c (n "tailwind-ast") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0nnqlmzpp61qv8i50g5aav8jlw1ca2fsj1p224a8v65ngccxkfxm") (f (quote (("default"))))))

(define-public crate-tailwind-ast-0.3.0 (c (n "tailwind-ast") (v "0.3.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "10r0k8ggr717yv0s02hqzwgx965zyzfsdwwgh19wfm1hwirc6g5z") (f (quote (("default"))))))

(define-public crate-tailwind-ast-0.3.1 (c (n "tailwind-ast") (v "0.3.1") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "03ad48bw26gin8ggab7w40h4ck16wb4rh31xav2xxhklbmlw4058") (f (quote (("default"))))))

(define-public crate-tailwind-ast-0.3.2 (c (n "tailwind-ast") (v "0.3.2") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "02b9c6skqfjyr9vnhvsyzy0i2s9dq7k3bk4j3lhr5b8csps1ibqx") (f (quote (("default"))))))

(define-public crate-tailwind-ast-0.4.0 (c (n "tailwind-ast") (v "0.4.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "1hxfv3hc7djfqwd9n375q12zq6izwd2cvm72cr5z64ifiiv3h9nf") (f (quote (("default"))))))

(define-public crate-tailwind-ast-0.4.1 (c (n "tailwind-ast") (v "0.4.1") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "03j2lvf5pqjdg8wc5zmsnfz2widx8pg4j2mapsvr71lqzmxi4hdc") (f (quote (("default"))))))

