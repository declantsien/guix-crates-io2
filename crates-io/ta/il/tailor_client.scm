(define-module (crates-io ta il tailor_client) #:use-module (crates-io))

(define-public crate-tailor_client-0.1.0 (c (n "tailor_client") (v "0.1.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)))) (h "15kx8wa2s2j07pbawahgb6dyzawygp6wkw3d3vvnw2abh1jri4qg")))

(define-public crate-tailor_client-0.1.1 (c (n "tailor_client") (v "0.1.1") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)))) (h "0r5pfi6zaiyhha82wmimnq2yi41x1b7kfknmwjjn1dm02p9sdk48")))

(define-public crate-tailor_client-0.1.2 (c (n "tailor_client") (v "0.1.2") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)))) (h "064r7aaprir1qkx52gwwmzcmh4yxkfn9jamnyki8imcgr660iyvl")))

(define-public crate-tailor_client-0.1.3 (c (n "tailor_client") (v "0.1.3") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)))) (h "0sc4v6yd4cr9xcp6ip9r2k3488sc9qya4qxvwhmpb9p1rjmgvk03")))

(define-public crate-tailor_client-0.1.4 (c (n "tailor_client") (v "0.1.4") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)))) (h "1xsfpssnfrc1hpsa891ky5dnj690prcphhyx80b9i0y6qm2xwp0n")))

(define-public crate-tailor_client-0.1.5 (c (n "tailor_client") (v "0.1.5") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)))) (h "1llkbcaysjpbl435ir7r6wyx7xjcmzw70hjm6y9a4lilj8ia9zyf")))

(define-public crate-tailor_client-0.1.6 (c (n "tailor_client") (v "0.1.6") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)))) (h "05ajrm3fdy38fmap266hw5aibydkvfp1r0p5lwjhmr9px95p60yg")))

(define-public crate-tailor_client-0.1.7 (c (n "tailor_client") (v "0.1.7") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)))) (h "1xl86dld40g76rcvsfabpy1px1528c3bbvys3fcdpdi13689gqmz")))

(define-public crate-tailor_client-0.1.8 (c (n "tailor_client") (v "0.1.8") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)))) (h "0pmaijwmlk25ilm2hrbrp4drjrd9kkjnvwax8bn6lcs2rr2zh961")))

(define-public crate-tailor_client-0.2.0 (c (n "tailor_client") (v "0.2.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1mxhmhag5yjj8z902r2qj1m8yaxqadzcsjxdpj8gplj61y951nl8")))

(define-public crate-tailor_client-0.2.1 (c (n "tailor_client") (v "0.2.1") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tailor_api") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zbus") (r "^3") (f (quote ("tokio"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1r29hqi049qmnfh08wg3j5lsn2330fndy7732mpzkv0vfy3i4nr3")))

