(define-module (crates-io ta il tail_cbc) #:use-module (crates-io))

(define-public crate-tail_cbc-0.1.0 (c (n "tail_cbc") (v "0.1.0") (d (list (d (n "cipher") (r "^0.4.3") (d #t) (k 0)) (d (n "cipher") (r "^0.4.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "15dd98c70xi1hgv7h56hh4q5pa5m1ikrzafmfb1kr24d1v65s4y2")))

(define-public crate-tail_cbc-0.1.1 (c (n "tail_cbc") (v "0.1.1") (d (list (d (n "cipher") (r ">=0.4.3") (d #t) (k 0)) (d (n "cipher") (r "^0.4.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "111khsmbwknk4f8avysh6k0jlxi8hnh9i8yngsym20w0y5pjkl7z")))

(define-public crate-tail_cbc-0.1.2 (c (n "tail_cbc") (v "0.1.2") (d (list (d (n "cipher") (r ">=0.4.3") (d #t) (k 0)) (d (n "cipher") (r "^0.4.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "1wxcxam2hq0baa8rf7kydyb3h5jaz5ak5cs7pmwnm9xsrp1gzdlj")))

