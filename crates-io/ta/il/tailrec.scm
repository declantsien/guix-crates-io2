(define-module (crates-io ta il tailrec) #:use-module (crates-io))

(define-public crate-tailrec-0.0.1 (c (n "tailrec") (v "0.0.1") (d (list (d (n "free") (r "*") (d #t) (k 0)))) (h "167iw68cn6vg25sm85p11rdjzm87wyryn7f4xa0kb0p3wxxs9y57")))

(define-public crate-tailrec-0.0.2 (c (n "tailrec") (v "0.0.2") (h "08c9ac7pb0l9xghir1c80jpp6r44v0hlx5hk6mlzgn7s2vhwarf4")))

(define-public crate-tailrec-0.0.3 (c (n "tailrec") (v "0.0.3") (h "0n1iq7mcmk693r3hji3c6fdi1fgjcfqg2a5ld66swvgssjf09h1x")))

(define-public crate-tailrec-0.0.4 (c (n "tailrec") (v "0.0.4") (h "01y3h2zsalsi1kflnrd389iww44bn2qarjsccnvhplz143dcs4zr")))

