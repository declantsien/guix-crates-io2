(define-module (crates-io ta il tailwind-config) #:use-module (crates-io))

(define-public crate-tailwind-config-0.8.0 (c (n "tailwind-config") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "18i4c7q7p5daiqcghx4bbxkbll3fkbqja9f66ds5bmdsw5vsz3fd")))

(define-public crate-tailwind-config-0.8.1 (c (n "tailwind-config") (v "0.8.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i5y4di6240lv943jy7d6grzp5zvz5wzqiqi8a3d583lzl0djh22")))

(define-public crate-tailwind-config-0.9.0 (c (n "tailwind-config") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "03rzng8rzwl237qsy5pwx3f156j6izh8jy1rcjjp72j2w8hwmm5r")))

(define-public crate-tailwind-config-0.10.0 (c (n "tailwind-config") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v4p5qkcay5f0c590a2cbiz6bwzyz5cba89lgp1r4hq3kiai4ykp")))

(define-public crate-tailwind-config-0.11.0 (c (n "tailwind-config") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zkd3n323g8vj02s77cym04s9wxqi65slqzxx647wcylg6fjn7wb")))

(define-public crate-tailwind-config-0.11.1 (c (n "tailwind-config") (v "0.11.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "18b572rj7frmymq6z9j6a1pjgnlq43qxvi4r0mvjmh1xwxjlfzs4")))

(define-public crate-tailwind-config-0.12.0 (c (n "tailwind-config") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w8axwbig4ysnvg44mbrb21gvbpa7kwws0y552afdhgp2bdkyafj")))

(define-public crate-tailwind-config-0.13.0 (c (n "tailwind-config") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "03pld79vfjyxbhqglzj8khrmaqfddj9d1l2vz0vy8qdxhazwv16b")))

(define-public crate-tailwind-config-0.14.0 (c (n "tailwind-config") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y7ls6dvl2v32k9a1pd4pd656lf4hxf1fdqvpk7vwvidh4k850rk")))

(define-public crate-tailwind-config-0.15.0 (c (n "tailwind-config") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sj8k40jszvxiw5msnjaaii2ak73kk88kkk20bljki3w09fkwcrc")))

(define-public crate-tailwind-config-0.16.0 (c (n "tailwind-config") (v "0.16.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ai430y0wy140wahwwm30kqhvfv3488z0h6b9i6pf1b9gmm1cil2")))

(define-public crate-tailwind-config-0.17.0 (c (n "tailwind-config") (v "0.17.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b4l6bpm864a550yx0gy8nvib9521g8si0sawdmfq61ywqka9c48")))

