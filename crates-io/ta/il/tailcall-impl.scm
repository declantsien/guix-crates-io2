(define-module (crates-io ta il tailcall-impl) #:use-module (crates-io))

(define-public crate-tailcall-impl-0.1.4 (c (n "tailcall-impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1jywc5r90r7mgdgbk5vw6bv3dnfyyji7jdcm3qh8sbk9ih43z976")))

(define-public crate-tailcall-impl-0.1.5 (c (n "tailcall-impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1l5zh615swa2av7y5j8ws360mwyrrgcwf4j3d3997qmgi8b587bv") (y #t)))

(define-public crate-tailcall-impl-0.1.6 (c (n "tailcall-impl") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0db3ia420r3jhcndlhy28lghn51ac01qwk54gcvibr56vv4gxr4n")))

(define-public crate-tailcall-impl-1.0.0 (c (n "tailcall-impl") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "16ffl5ncfyqjff4brs7514kf40gjskid367mz1c65j6iyhmlgyaf")))

(define-public crate-tailcall-impl-1.0.1 (c (n "tailcall-impl") (v "1.0.1") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "04vf85hacy1fxrp9wfrsjgxb3j9vnqfzvxilddqfig2c9wibzdqh")))

