(define-module (crates-io ta il tailor_api) #:use-module (crates-io))

(define-public crate-tailor_api-0.1.0 (c (n "tailor_api") (v "0.1.0") (d (list (d (n "atoi") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zpjsjswkssyjw7qmvxgvjj93pqbqdwsksr9kjmhw80h27lijxjf")))

(define-public crate-tailor_api-0.1.1 (c (n "tailor_api") (v "0.1.1") (d (list (d (n "atoi") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rnlahidxn4hbgcchfszfch64fdhn878hc7kn3vz5ky8dfdcyjgy")))

(define-public crate-tailor_api-0.2.0 (c (n "tailor_api") (v "0.2.0") (d (list (d (n "atoi") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c0b0ap9q42k79h668gm6pps81mh3kvdg2n93pc51zb4d9q4gkm2")))

(define-public crate-tailor_api-0.2.1 (c (n "tailor_api") (v "0.2.1") (d (list (d (n "atoi") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rhw0qv8jjz5csljwqlv8p4mz5vxgmiiv2gm7cm08pld3mdnk811")))

