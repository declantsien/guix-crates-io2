(define-module (crates-io ta rr tarrasque) #:use-module (crates-io))

(define-public crate-tarrasque-0.1.0 (c (n "tarrasque") (v "0.1.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "0gm3mlf26ldc6hs6ah3izlr5xvaw86cmz7zg9nlcc8l6z006343m")))

(define-public crate-tarrasque-0.2.0 (c (n "tarrasque") (v "0.2.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "086zfalqbn4wfs6jabnd9y6cvf9sx72sw9fclmxw1hdz83imx1yf")))

(define-public crate-tarrasque-0.3.0 (c (n "tarrasque") (v "0.3.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "0bphnxvwcsaby846adjwzc5lyxni4vhhxshilzz0dvpvnksc08xh")))

(define-public crate-tarrasque-0.4.0 (c (n "tarrasque") (v "0.4.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "1pmvgv807q84s5n20wmn7vz7awvn2dsgnc82s5j77zyy2qdcfr0h")))

(define-public crate-tarrasque-0.5.0 (c (n "tarrasque") (v "0.5.0") (h "0vxk1xqmj5pr3yf1w4l6jgbjby52hhc2njqg8dhl11dp0r6kwgwx")))

(define-public crate-tarrasque-0.6.0 (c (n "tarrasque") (v "0.6.0") (d (list (d (n "tarrasque-macro") (r "^0.6.0") (d #t) (k 0)))) (h "0hq38b7m9aipdiirfagmcracqbwnw9c4hhkxzfxqs7zmcw0j7wab")))

(define-public crate-tarrasque-0.7.0 (c (n "tarrasque") (v "0.7.0") (d (list (d (n "tarrasque-macro") (r "^0.7.0") (d #t) (k 0)))) (h "1b7wj5asll1l7wpcwkc27pji1q9vzyk4wnajczgxlvk0b7m9mzz9")))

(define-public crate-tarrasque-0.8.0 (c (n "tarrasque") (v "0.8.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "tarrasque-macro") (r "^0.8.0") (d #t) (k 0)))) (h "0wc33192ry6njk9fwm63zy2k4vz2d6f39n645nfh0a46b8md1141")))

(define-public crate-tarrasque-0.9.0 (c (n "tarrasque") (v "0.9.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "tarrasque-macro") (r "^0.9.0") (d #t) (k 0)))) (h "12hg5dyxq70749wcjr376gk59f5mzc28nn0nxailxffp4s47yhq5")))

(define-public crate-tarrasque-0.10.0 (c (n "tarrasque") (v "0.10.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "tarrasque-macro") (r "^0.10.0") (d #t) (k 0)))) (h "04lyk8xil39fdhwchkxidw31v970snsx5xnnqj2dnbwv9n1aakkv")))

