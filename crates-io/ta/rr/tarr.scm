(define-module (crates-io ta rr tarr) #:use-module (crates-io))

(define-public crate-tarr-0.1.0 (c (n "tarr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0nirh6n1m174fzf4ajg2p2h4prfqk76z1rras8f2hflhc22i8awp")))

(define-public crate-tarr-0.1.2 (c (n "tarr") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.47") (f (quote ("std"))) (k 0)) (d (n "argh") (r "^0.1.6") (k 0)) (d (n "fehler") (r "^1.0.0") (k 0)) (d (n "humansize") (r "^1.1.1") (k 0)) (d (n "tar") (r "^0.4.37") (k 0)) (d (n "tempfile") (r "^3.2.0") (k 0)) (d (n "unicode-width") (r "^0.1.9") (k 0)))) (h "15ihqq6cnyg4d44x7mvkm999glp0ly42yig4dcclmhmwakwm27ir")))

