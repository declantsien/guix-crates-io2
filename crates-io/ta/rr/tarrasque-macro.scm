(define-module (crates-io ta rr tarrasque-macro) #:use-module (crates-io))

(define-public crate-tarrasque-macro-0.6.0 (c (n "tarrasque-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1qrwvp80vrmhgnc708ly1nbnc7q1i8lv5nh92vr8dgz59j4w1skw")))

(define-public crate-tarrasque-macro-0.7.0 (c (n "tarrasque-macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "05xpsiwjz4z3m2b0dhd1234bgj5jkxvcy6rssg44mdh031164dcg")))

(define-public crate-tarrasque-macro-0.8.0 (c (n "tarrasque-macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "06rkvcm2y0y0wpz8gliy66l7vf763q4f0a195by9q2lz1fjg4gxw")))

(define-public crate-tarrasque-macro-0.9.0 (c (n "tarrasque-macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "08z8m1y2005ywhszq8z69yz0m94w225d1756dqvynmdp9fn5nhcy")))

(define-public crate-tarrasque-macro-0.10.0 (c (n "tarrasque-macro") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1ynkv76vmynrfdk36bimw1j88iavmd0qlyanan0y7cy58yxzd31r")))

