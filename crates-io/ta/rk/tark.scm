(define-module (crates-io ta rk tark) #:use-module (crates-io))

(define-public crate-tark-0.1.0 (c (n "tark") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)))) (h "0kknk4x3ps4p946ycc95qnia4pbibrh8jg4a992z88630bm3pqb3")))

(define-public crate-tark-0.1.1 (c (n "tark") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)))) (h "13mxspz9nvvn03qbarkhg3sa5vi5cy8ng3xbjm64ghvhhv0d2xl0")))

(define-public crate-tark-0.1.2 (c (n "tark") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)))) (h "1qikn7rslzzlxhjhl9sywf2y6w598lx2ls95qxdqz5n0nszn6val")))

(define-public crate-tark-0.1.3 (c (n "tark") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 2)))) (h "1ybi7bd8cgmfiaran49id67q2ll2yvyd74vacsqwid83vz8i5zgd")))

