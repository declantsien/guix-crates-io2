(define-module (crates-io ta gr tagref) #:use-module (crates-io))

(define-public crate-tagref-0.0.1 (c (n "tagref") (v "0.0.1") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1q0ajqggjsy1bwg9g4dd16k9vnwwddvhnkl83m2sg65nmpv1xi5d")))

(define-public crate-tagref-0.0.2 (c (n "tagref") (v "0.0.2") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07h3sy6lkyixrcnm3kik7a18k8k5s0rw01qj92zx2wzn42b4lgd2")))

(define-public crate-tagref-0.0.3 (c (n "tagref") (v "0.0.3") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rbvavb0znr468sfbkymzhj262zc6sd61vjh73b2mdylfxz23v01")))

(define-public crate-tagref-0.0.4 (c (n "tagref") (v "0.0.4") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0y43fgfx3ppzzsamin1xdk52sbbhwn5k274k7ghiqk2i7fkdqzsr")))

(define-public crate-tagref-0.0.5 (c (n "tagref") (v "0.0.5") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0l7facz21z7yicvmpcrdvdffs0cvid54a1hf7pccpxhrbxr4bnfv")))

(define-public crate-tagref-0.0.6 (c (n "tagref") (v "0.0.6") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00ahb6vjh1lqgl7si0c714r3svgnb32rkzy3gxzp2a808pkvncfc")))

(define-public crate-tagref-0.0.7 (c (n "tagref") (v "0.0.7") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p2qngwjj7cjbjx8vk6fdd4dy9wrnzxzyd40db0jfp3raiyjlmbk")))

(define-public crate-tagref-0.0.8 (c (n "tagref") (v "0.0.8") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05v0wdiwvklp3c83nr73k0sfbchgnhcdmbhzc982lig5lkfvfm7j")))

(define-public crate-tagref-0.1.0 (c (n "tagref") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xqrzqrcfngg84q7qdsmib4my383iv0nrhhfxymxr579jiw5al4n")))

(define-public crate-tagref-1.0.0 (c (n "tagref") (v "1.0.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bnp350g915mwy8rwrn0dzz2chv38hbzi6hspafg2cddqxqnr2wh")))

(define-public crate-tagref-1.1.0 (c (n "tagref") (v "1.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "091wd8l0dq4xv2fks9cz2bzahciq2db5m7jaxcmf0c3jk8rl684k")))

(define-public crate-tagref-1.2.0 (c (n "tagref") (v "1.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jyd1wsf2lmfpa8h74fg4hsijanv4wjq3aj8aghc3xyls80qn3pn")))

(define-public crate-tagref-1.2.1 (c (n "tagref") (v "1.2.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1pj1f3jbzvkxfv96lhnd908vgrlf6l6zb20vam9044j92928gi5q")))

(define-public crate-tagref-1.3.0 (c (n "tagref") (v "1.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07m4qhclk563wkj9v3is68rmhjbqdv07ylwcbcqj7jzmkah19smc")))

(define-public crate-tagref-1.3.1 (c (n "tagref") (v "1.3.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0q5rb4a620c9cpacrafk9kjihgy6wg90yc8md690sxl9adcwrkbp")))

(define-public crate-tagref-1.3.2 (c (n "tagref") (v "1.3.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09fqvsfwjkg0vzg7i4619ic9cy4rc54p8s0khyq1805rsfsc7da4")))

(define-public crate-tagref-1.3.3 (c (n "tagref") (v "1.3.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10vslxpy0xrpz8m4p4q96fkq15jnzhq18i5lpvvg1i01zvnmvlqn")))

(define-public crate-tagref-1.4.0 (c (n "tagref") (v "1.4.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0h6jkf4xhd3f64xyd0slj5fz0qnxcfgfga15912wsl2ik0ny74j0")))

(define-public crate-tagref-1.4.1 (c (n "tagref") (v "1.4.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0a7rira316rq26nisvs1akrps8cqbdrnfhgf491j22qz57fgjahv")))

(define-public crate-tagref-1.5.0 (c (n "tagref") (v "1.5.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1034g6x93ksdrclp7yzf2lnddfq3ipihawza4x0r0in02vrclgr1")))

(define-public crate-tagref-1.6.0 (c (n "tagref") (v "1.6.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colonnade") (r "^1") (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "154d68bbbv64k6irgz9l64018yqa4n8snivkwjnaz3s4s8057laq")))

(define-public crate-tagref-1.6.1 (c (n "tagref") (v "1.6.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colonnade") (r "^1") (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1z32k1b4kjafdmh2r2lkmjd579zgfdy74w508irc8020vrw34xr3")))

(define-public crate-tagref-1.7.0 (c (n "tagref") (v "1.7.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jikw9kz0iasfjaf4g58dc01pxfjmzhhky9lycr7jqva5spybm97")))

(define-public crate-tagref-1.8.0 (c (n "tagref") (v "1.8.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0apinpipk68c2qqh53s65rz7yp7hdiyp2ygqc5zhab5frxbza3pj")))

(define-public crate-tagref-1.8.1 (c (n "tagref") (v "1.8.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09v301n1k3v0iqj3bs3m78g1azdsg1b637xfdm9dgq14sffcyxg4")))

(define-public crate-tagref-1.8.2 (c (n "tagref") (v "1.8.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1f5s7bslx14315v78knrzqxds7xi3l39c3ms31xqvsqjdzchp7hw")))

(define-public crate-tagref-1.8.3 (c (n "tagref") (v "1.8.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jivicazkhh11m9yddwmf3pd6fcwk63pkvcahpj9bjfawq250ip6")))

(define-public crate-tagref-1.8.4 (c (n "tagref") (v "1.8.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "089gz8hs57gdzz74c73f5k99lq98652nndmclpcvxnn7q29v5i3v")))

(define-public crate-tagref-1.8.5 (c (n "tagref") (v "1.8.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wnaa9ih6v2z3if6ljszba1f36mn7076amxp4jc7naw0frac2zvn")))

(define-public crate-tagref-1.9.0 (c (n "tagref") (v "1.9.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gh1bniysdzi9b7b3w0ygvf0iwzn137skbbvj894rvpd6knpbkbg")))

(define-public crate-tagref-1.9.1 (c (n "tagref") (v "1.9.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pf6g6vavmbr90rhryqsfl3f2grmmbws5ifq62544cmldzx6nk0g")))

(define-public crate-tagref-1.10.0 (c (n "tagref") (v "1.10.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1fjzxvgi6mh0hk786qirpxdxhpkb10lgjjvmbpxc74h5mq7gqz7i")))

