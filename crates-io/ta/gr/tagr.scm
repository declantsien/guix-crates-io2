(define-module (crates-io ta gr tagr) #:use-module (crates-io))

(define-public crate-tagr-0.1.1 (c (n "tagr") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)))) (h "15kjjk6w2c3sxnapzp1kifg3ad3cd10sq9ca5rbrkb785rsn7x3b")))

(define-public crate-tagr-0.1.2 (c (n "tagr") (v "0.1.2") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)))) (h "0cbakragn4kjkyhddr0d9bf94dl17na7dvi5w9w5wvl2b2ixzxsi")))

