(define-module (crates-io ta gr tagrun) #:use-module (crates-io))

(define-public crate-tagrun-0.1.0 (c (n "tagrun") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.127") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)))) (h "0x27k480p5g5mqzsc2jpqmv956q7z68wja6212czf2nrjpfpsapc")))

