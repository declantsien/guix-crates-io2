(define-module (crates-io ta gl taglib) #:use-module (crates-io))

(define-public crate-taglib-0.1.0 (c (n "taglib") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0w4ykpkkmnr91wwbw5fx0bdnw3kfc91b211da2sckmv0cp8c2v8x") (f (quote (("use-pkgconfig" "pkg-config") ("default"))))))

(define-public crate-taglib-0.2.0 (c (n "taglib") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "taglib-sys") (r "^0.2.0") (d #t) (k 0)))) (h "041fjwg9h1sqh73khw0nm744kibdlpzfdlq9hxcrz35h244b34lw") (f (quote (("use-pkgconfig" "taglib-sys/use-pkgconfig") ("default"))))))

(define-public crate-taglib-1.0.0 (c (n "taglib") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "taglib-sys") (r "^1.0.0") (d #t) (k 0)))) (h "04r0mm35nr9pjjk27c59ysx8bgjbas5ag7j5xpzfyva2gvrmx7xq") (f (quote (("use-pkgconfig" "taglib-sys/use-pkgconfig") ("default"))))))

