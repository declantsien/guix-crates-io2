(define-module (crates-io ta gl taglib-sys) #:use-module (crates-io))

(define-public crate-taglib-sys-0.2.0 (c (n "taglib-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "09lfr58p863ddl6c1b6mvqkvllhnx200d16xzxflrpvvn0ddczvy") (f (quote (("use-pkgconfig" "pkg-config") ("default"))))))

(define-public crate-taglib-sys-1.0.0 (c (n "taglib-sys") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0kia1mlg7jvdx3b43fynsvjr58g3x3w4qs8nxb6fiw83lqifl84y") (f (quote (("use-pkgconfig" "pkg-config") ("default")))) (l "tag_c")))

