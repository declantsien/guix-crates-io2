(define-module (crates-io ta pp tapper) #:use-module (crates-io))

(define-public crate-tapper-0.1.0 (c (n "tapper") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "testanything") (r "^0.1.2") (d #t) (k 0)))) (h "0v5pd7ihplz9z5ci2ng8wsnhcnndrx6kk43iiwy7z3qlzlf8ikjs")))

(define-public crate-tapper-0.1.1 (c (n "tapper") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "testanything") (r "^0.1.2") (d #t) (k 0)))) (h "1fb3b9v6lvglmkh27dmb2zc7gcv6hqiarjpsli721iaddllblmpw")))

(define-public crate-tapper-0.1.2 (c (n "tapper") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "testanything") (r "^0.1.2") (d #t) (k 0)))) (h "1z1pjwm10k2l785kz1lkf3fjg14h0vaa3zna7vds6yg6q6b0al60")))

(define-public crate-tapper-0.1.3 (c (n "tapper") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "testanything") (r "^0.2.1") (d #t) (k 0)))) (h "0sphswr9rf00xlj90h20b808pqbbwhrwb2ra7cb8x1pfs8zgrqmn")))

