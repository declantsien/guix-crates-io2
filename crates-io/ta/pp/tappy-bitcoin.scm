(define-module (crates-io ta pp tappy-bitcoin) #:use-module (crates-io))

(define-public crate-tappy-bitcoin-0.1.0 (c (n "tappy-bitcoin") (v "0.1.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "miniscript") (r "^9.0.1") (f (quote ("rand" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1xygrc6xldcfm1qyjz1ma8r7f1cqgj1d8h236cr4xag5iqfm17vg")))

