(define-module (crates-io ta pp tappet-derive) #:use-module (crates-io))

(define-public crate-tappet-derive-0.3.1 (c (n "tappet-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mkk9d80zf4whqg1w19pa01ik18pmjycdvn7pzls6z393326qxqh")))

(define-public crate-tappet-derive-0.3.2 (c (n "tappet-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1h3ys7126glx0vqlwanqlg2ck3www6n62lv1s29fymxysa21jbxa")))

