(define-module (crates-io ta bo tabout) #:use-module (crates-io))

(define-public crate-tabout-0.1.0 (c (n "tabout") (v "0.1.0") (d (list (d (n "termwiz") (r "^0.7") (d #t) (k 0)))) (h "1nfsl68r6213n2aby5jvxf8ksprmvfzjmlnjksyr7736i8z0rn42")))

(define-public crate-tabout-0.2.0 (c (n "tabout") (v "0.2.0") (d (list (d (n "termwiz") (r "^0.7") (d #t) (k 0)))) (h "11q45vxrx6j3sdzds48ymz4ksnq1rb4ygxcl7qpy9ngn3fmcqk76")))

(define-public crate-tabout-0.3.0 (c (n "tabout") (v "0.3.0") (d (list (d (n "termwiz") (r "^0.8") (d #t) (k 0)))) (h "1rkjnm1p9j900vjjk71w4fl8sm0drl7xqkaglb5ill5iq024vjcs")))

