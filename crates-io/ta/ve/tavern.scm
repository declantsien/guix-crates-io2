(define-module (crates-io ta ve tavern) #:use-module (crates-io))

(define-public crate-tavern-0.0.0 (c (n "tavern") (v "0.0.0") (h "0gnyw2m25kcgnbqj5sylp6vir2f22rcnlxipjhml7sivscccfpk4") (y #t) (r "1.56")))

(define-public crate-tavern-0.0.1 (c (n "tavern") (v "0.0.1") (h "12mz9s0l25cyavgfix3y8hckkapqppr5wxmp76nwbhxphsr2xj6r") (y #t) (r "1.56")))

