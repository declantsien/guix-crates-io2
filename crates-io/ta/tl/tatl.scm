(define-module (crates-io ta tl tatl) #:use-module (crates-io))

(define-public crate-tatl-0.1.0 (c (n "tatl") (v "0.1.0") (d (list (d (n "biometrics") (r "^0.1") (d #t) (k 0)) (d (n "one_two_eight") (r "^0.1") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0k7cnn38kv6kzb9hnyh9shandhnkwpi212hrppnir92j3wimhjcz")))

(define-public crate-tatl-0.2.0 (c (n "tatl") (v "0.2.0") (d (list (d (n "biometrics") (r "^0.2") (d #t) (k 0)) (d (n "one_two_eight") (r "^0.1") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1nljp5493lwd0fgcaj2rmp5wfsh8x1hjb57fk2mznbkcfkzyl42c")))

(define-public crate-tatl-0.2.1 (c (n "tatl") (v "0.2.1") (d (list (d (n "biometrics") (r "^0.3") (d #t) (k 0)) (d (n "one_two_eight") (r "^0.2") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1xks830dh5saxv0m28g0wbkkpjdv5i9y0lzgdlq99x408193wjc8")))

(define-public crate-tatl-0.3.0 (c (n "tatl") (v "0.3.0") (d (list (d (n "biometrics") (r "^0.4") (d #t) (k 0)) (d (n "one_two_eight") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1mvj2bwq0zdn2d1qpvk2mdy85lhas4mjm32zcihcmlv4jpxknvq0")))

(define-public crate-tatl-0.4.0 (c (n "tatl") (v "0.4.0") (d (list (d (n "biometrics") (r "^0.5") (d #t) (k 0)) (d (n "one_two_eight") (r "^0.4") (d #t) (k 0)) (d (n "prototk") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0w39f2l4krbhidgg72sz0wkbsxzbnwkh9ig1jm3rji81dv305b5l")))

(define-public crate-tatl-0.5.0 (c (n "tatl") (v "0.5.0") (d (list (d (n "biometrics") (r "^0.6") (d #t) (k 0)) (d (n "one_two_eight") (r "^0.4") (d #t) (k 0)) (d (n "prototk") (r "^0.6") (d #t) (k 0)))) (h "0y92q61qs3bynmilvd6hskdch7m76ddwxs78nh2lwd130ja6k5bv")))

