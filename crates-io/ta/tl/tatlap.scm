(define-module (crates-io ta tl tatlap) #:use-module (crates-io))

(define-public crate-tatlap-0.1.0 (c (n "tatlap") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "img_hash") (r "^3.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0zas30xbkq9rbbx548gaxyj68hgz5vkwx3ljb9qd55lvf21awyh2")))

(define-public crate-tatlap-0.1.1 (c (n "tatlap") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "img_hash") (r "^3.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1rgv4z9656jv7rns1g932i7w5pr39gg6smlipd66jpay9zwsv8l8")))

