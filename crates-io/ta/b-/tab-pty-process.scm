(define-module (crates-io ta b- tab-pty-process) #:use-module (crates-io))

(define-public crate-tab-pty-process-0.2.0 (c (n "tab-pty-process") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (f (quote ("unstable" "bilock"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "io-driver" "signal"))) (d #t) (k 0)))) (h "0bdb8wsvs58df333s2lx6a9m9wc0kz01ixar88a9569y51aj9fgl")))

