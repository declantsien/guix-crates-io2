(define-module (crates-io ta b- tab-hash) #:use-module (crates-io))

(define-public crate-tab-hash-0.1.0 (c (n "tab-hash") (v "0.1.0") (d (list (d (n "array-init") (r "^0.0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.35") (d #t) (k 1)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "06vwzk1ikxjf20xll1x7vn1lv00s6gm04968nynkx4a5swwz0r0x")))

(define-public crate-tab-hash-0.1.1 (c (n "tab-hash") (v "0.1.1") (d (list (d (n "array-init") (r "^0.0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.35") (d #t) (k 1)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1nvxfrkqn6dfmib5f91l6vyc56kyi573k299wzvp9g4pmy3421y8")))

(define-public crate-tab-hash-0.1.2 (c (n "tab-hash") (v "0.1.2") (d (list (d (n "array-init") (r "^0.0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.35") (d #t) (k 1)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1kg4k2hrsmqasnn2vncsxf5r3p7d37bma2ks7dwyyla0h5j1mrqb")))

(define-public crate-tab-hash-0.2.0 (c (n "tab-hash") (v "0.2.0") (d (list (d (n "array-init") (r "^0.1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1mv2y5045b30593hi4dbdag69q6bbffvq04k1nwqw4kqb6kzbyi4")))

(define-public crate-tab-hash-0.2.1 (c (n "tab-hash") (v "0.2.1") (d (list (d (n "array-init") (r "^0.1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0z2w471z2h9417skkffjc0h2fkqs78hyjcqkckzr862071vvdaap")))

(define-public crate-tab-hash-0.3.0 (c (n "tab-hash") (v "0.3.0") (d (list (d (n "array-init") (r "^0.1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 2)) (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rq4vf7q3acm60vaz8qy8wff9dyzfgf1v0c9cmf68hfi0fy2grgg")))

