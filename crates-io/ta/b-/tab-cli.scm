(define-module (crates-io ta b- tab-cli) #:use-module (crates-io))

(define-public crate-tab-cli-0.1.0 (c (n "tab-cli") (v "0.1.0") (h "1gsd1l9ldfv7wlcjjsbzciqjlfxdb2md5c4sag65yl9aj95w6pqb")))

(define-public crate-tab-cli-0.2.0 (c (n "tab-cli") (v "0.2.0") (h "1y2c2m26v8cvvs4bga107w11cf32l8npckzi874r3gn4mh83l74f")))

