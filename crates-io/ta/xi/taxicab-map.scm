(define-module (crates-io ta xi taxicab-map) #:use-module (crates-io))

(define-public crate-taxicab-map-0.0.0 (c (n "taxicab-map") (v "0.0.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "0smw5x8kaww6alp4b99pzib4kc6zx6bs06fdkbb966bfclhyhjw1") (f (quote (("default"))))))

(define-public crate-taxicab-map-0.1.0 (c (n "taxicab-map") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ordered-float") (r "^3.6.0") (d #t) (k 0)) (d (n "pathfinding") (r "^4.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j8y4w538pf6bka1lapyclcmyvbh9bz51qbf9s9i57lkvmhlxphz") (f (quote (("default"))))))

(define-public crate-taxicab-map-0.1.1 (c (n "taxicab-map") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ordered-float") (r "^3.6.0") (d #t) (k 0)) (d (n "pathfinding") (r "^4.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cgc8kp2zmhx1riwwrajqpn82a6lqsv2dvdggsxc24hwhshjpq3m") (f (quote (("default"))))))

(define-public crate-taxicab-map-0.1.2 (c (n "taxicab-map") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ordered-float") (r "^3.6.0") (d #t) (k 0)) (d (n "pathfinding") (r "^4.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "074hvzps7gkhiclv9gh7x52p5wcxa55i0g80kdhdz9qwiir4kmxc") (f (quote (("default"))))))

(define-public crate-taxicab-map-0.1.3 (c (n "taxicab-map") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ordered-float") (r "^3.6.0") (d #t) (k 0)) (d (n "pathfinding") (r "^4.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "01drnaijf2g5cf8qycisgylhf0jmi654ihhw6w762w3y7yadhsly") (f (quote (("default"))))))

