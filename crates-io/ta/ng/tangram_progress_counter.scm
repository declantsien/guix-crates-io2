(define-module (crates-io ta ng tangram_progress_counter) #:use-module (crates-io))

(define-public crate-tangram_progress_counter-0.4.0 (c (n "tangram_progress_counter") (v "0.4.0") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "1d1v30cgx2xfmx134jwra9lkzpnwadkb5jymm0yf2s4df7llq7f3")))

(define-public crate-tangram_progress_counter-0.5.0 (c (n "tangram_progress_counter") (v "0.5.0") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "1lm3hygk5xa0hlrs9498gmjfrw23rn6dmgwccasqz76kps680yzv")))

(define-public crate-tangram_progress_counter-0.6.0 (c (n "tangram_progress_counter") (v "0.6.0") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "0rl6whs2myrh578fivnhniq64p7wibkn1cq3l0nansbnbqqm6k7x")))

(define-public crate-tangram_progress_counter-0.7.0 (c (n "tangram_progress_counter") (v "0.7.0") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "1z3n0yr3gcpymnwr18z50ckcs8d1b6cacxm5ja0z503nxzszn6zm")))

