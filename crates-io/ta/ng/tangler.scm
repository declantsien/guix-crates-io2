(define-module (crates-io ta ng tangler) #:use-module (crates-io))

(define-public crate-tangler-0.1.0 (c (n "tangler") (v "0.1.0") (d (list (d (n "comrak") (r "^0.15.0") (d #t) (k 0)))) (h "0zfsjrh2jlkn3y9zg3v7176x6nz3j9chbnmqlhr4yhvfqavpjs4z")))

(define-public crate-tangler-0.2.0 (c (n "tangler") (v "0.2.0") (d (list (d (n "comrak") (r "^0.15.0") (d #t) (k 0)))) (h "1bn6gy5yx25n8lvdzf1i7ynw63flvxq2qah1i1i0d2q84q4h68xj")))

(define-public crate-tangler-0.3.0 (c (n "tangler") (v "0.3.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "testresult") (r "^0.3.0") (d #t) (k 2)))) (h "0h415ah61prphnpxms43k6cp5nz5jy5a7hb41x1a0mp56m2mkc8n")))

