(define-module (crates-io ta ng tangram_zip) #:use-module (crates-io))

(define-public crate-tangram_zip-0.4.0 (c (n "tangram_zip") (v "0.4.0") (h "10mrnz5jixcm6112pqia2n1s7bpwbb9k72i90pf529vgf6k43pw0")))

(define-public crate-tangram_zip-0.5.0 (c (n "tangram_zip") (v "0.5.0") (h "0nh9d46jwpjyy1n5pr2vfrqvqi3icxj4mw3lq7j8cqgw7dn5v81q")))

(define-public crate-tangram_zip-0.6.0 (c (n "tangram_zip") (v "0.6.0") (h "1ipzbxlsf85d2xlndzkgf2kd57gn0cv21ng59ga9vllkadd4sp3p")))

(define-public crate-tangram_zip-0.7.0 (c (n "tangram_zip") (v "0.7.0") (h "0193vlh0cng4may134v9q71smvqzf83n1qwwmaxza3rs4z249z3r")))

