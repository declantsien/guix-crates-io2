(define-module (crates-io ta ng tangra) #:use-module (crates-io))

(define-public crate-tangra-1.0.0 (c (n "tangra") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1djh7d72a962a5pj0h9apjm5acgpxyx279mwr5v3y2wvimz1qrm6")))

(define-public crate-tangra-1.0.1 (c (n "tangra") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "07g8xayybsgn38nr27sby3196x74na9p88nby4axlfyzqwz17n2s")))

(define-public crate-tangra-1.1.0 (c (n "tangra") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "0f2br48hc9ff5sfw8r9wqjskrnbjbb1k153y1lbcpdyb1fidw3yg")))

(define-public crate-tangra-1.1.1 (c (n "tangra") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "02j4k8gb6kvkcgdyvpm4b4yk81vydjz1jvi3gsw360kb43q9rirp")))

(define-public crate-tangra-1.2.0 (c (n "tangra") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "0h29q9ipgj379p2j95sj0x58c60jkdd64cjkg5pqv8cdzb9194s1")))

(define-public crate-tangra-1.2.1 (c (n "tangra") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1iwb0jq61sch3r12vw3wvy21l4lv8wl2i2zkbjfnx11skjg3cpzw")))

(define-public crate-tangra-1.3.0 (c (n "tangra") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1cz8lyjwwzhrg002l5yrs0bz25sx13ajrya05jzcmhn7lcid56m3")))

