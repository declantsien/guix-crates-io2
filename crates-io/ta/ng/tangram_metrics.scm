(define-module (crates-io ta ng tangram_metrics) #:use-module (crates-io))

(define-public crate-tangram_metrics-0.4.0 (c (n "tangram_metrics") (v "0.4.0") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tangram_zip") (r "^0.4") (d #t) (k 0)))) (h "0pzfr5c37s9jniclwly3hlmdv77d3b45szr567xrn0mdhisw4w14")))

(define-public crate-tangram_metrics-0.5.0 (c (n "tangram_metrics") (v "0.5.0") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tangram_zip") (r "=0.5.0") (d #t) (k 0)))) (h "0l4l92spqynbvfk0647vs5cn7kwm37qhlzqqlwbcxi3d6h37q7qd")))

(define-public crate-tangram_metrics-0.6.0 (c (n "tangram_metrics") (v "0.6.0") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tangram_zip") (r "=0.6.0") (d #t) (k 0)))) (h "1rw72lqbhx9xyri0a6h9fnl9ghmgfwbxc8adfb2ag3gd47sn06yd")))

(define-public crate-tangram_metrics-0.7.0 (c (n "tangram_metrics") (v "0.7.0") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tangram_zip") (r "^0.7.0") (d #t) (k 0)))) (h "022wib9sphkscbn7cvky3i40salm2k9s4p4v2j9iykjmchs91v34")))

