(define-module (crates-io ta ng tangram_model) #:use-module (crates-io))

(define-public crate-tangram_model-0.4.0 (c (n "tangram_model") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "buffalo") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tangram_linear") (r "^0.4") (d #t) (k 0)) (d (n "tangram_text") (r "^0.4") (d #t) (k 0)) (d (n "tangram_tree") (r "^0.4") (d #t) (k 0)))) (h "0vbsnq37ndnl9v5g5v85ix32i1454cwg9zzbnxhq1w3abbm2rqbn")))

(define-public crate-tangram_model-0.5.0 (c (n "tangram_model") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "buffalo") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tangram_linear") (r "=0.5.0") (d #t) (k 0)) (d (n "tangram_text") (r "=0.5.0") (d #t) (k 0)) (d (n "tangram_tree") (r "=0.5.0") (d #t) (k 0)))) (h "17zcrz7j0v56slzw2c3mrmsfyk6h3a02nldf7dsqayjjdhib4ris")))

(define-public crate-tangram_model-0.6.0 (c (n "tangram_model") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "buffalo") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tangram_linear") (r "=0.6.0") (d #t) (k 0)) (d (n "tangram_text") (r "=0.6.0") (d #t) (k 0)) (d (n "tangram_tree") (r "=0.6.0") (d #t) (k 0)))) (h "0g86b4gg9djxnm9pf6pppbjsfg7msbsdcqyynlwkfny6hgiyispv")))

(define-public crate-tangram_model-0.7.0 (c (n "tangram_model") (v "0.7.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "buffalo") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tangram_linear") (r "^0.7.0") (d #t) (k 0)) (d (n "tangram_text") (r "^0.7.0") (d #t) (k 0)) (d (n "tangram_tree") (r "^0.7.0") (d #t) (k 0)))) (h "053v100k22zqij6p49qmgyrd1c92xwk4784lbcf9rdydny8ig94h")))

