(define-module (crates-io ta ng tangram_id) #:use-module (crates-io))

(define-public crate-tangram_id-0.4.0 (c (n "tangram_id") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0876yp3yvzf60ay6ymp5m5w2b4ij1svby5pnqm1ivhryjgbdmlhb")))

(define-public crate-tangram_id-0.5.0 (c (n "tangram_id") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "144sjacbqa280r2g5vday8gqqd7nm310w8nid968p6ppnkr30im2")))

(define-public crate-tangram_id-0.6.0 (c (n "tangram_id") (v "0.6.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sysn8pd3f0n82c4nq8qxpb9ai292pvsh0qk3b7wzlq3qbxrqxfp")))

(define-public crate-tangram_id-0.7.0 (c (n "tangram_id") (v "0.7.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "108xnzw3k4pr44rzc5cykrdpjfs4agznbrbk78ijb45zya1k31p7")))

