(define-module (crates-io ta ng tangram_macro) #:use-module (crates-io))

(define-public crate-tangram_macro-0.4.0 (c (n "tangram_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0142hkxxaw8y5rajpgp3rsvahnhpv0pxkfp79xcirvllb2mrdr9x")))

(define-public crate-tangram_macro-0.5.0 (c (n "tangram_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1124qv38d42vr2sd2b34nypwclj10by1yynp64j3abvss8l69535")))

(define-public crate-tangram_macro-0.6.0 (c (n "tangram_macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zpknq9zzyyj7pjjghvazwa221xz8sca10ya5ggqpwvfzyshsby4")))

(define-public crate-tangram_macro-0.7.0 (c (n "tangram_macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rpiawins5vjsgxqawiwd27jbrlvpr2m2smy7yb887yg5j2sgv3w")))

