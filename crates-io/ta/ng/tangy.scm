(define-module (crates-io ta ng tangy) #:use-module (crates-io))

(define-public crate-tangy-0.1.0 (c (n "tangy") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "tangy-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0zg4g821md19xb2657b9m9zh2sz5jy0y8c73rw4hfc99aqrg79mj")))

(define-public crate-tangy-0.1.1 (c (n "tangy") (v "0.1.1") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "tangy-lib") (r "^0.1.1") (d #t) (k 0)))) (h "0mzmwzfwy1ic8hvza2pfcgqh50hkvpj6n7xw8abw70w5fx58rbaj")))

(define-public crate-tangy-0.2.0 (c (n "tangy") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "tangy-lib") (r "^0.1.3") (d #t) (k 0)))) (h "05j8q4mh1bc4p045s7arfk1kqwd388p40myj1fxxnjv73cj7kq42")))

(define-public crate-tangy-0.2.1 (c (n "tangy") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "tangy-lib") (r "^0.1.6") (d #t) (k 0)))) (h "0ds2gkcj0mx10z9w182jhmvwyq6143pk1bs35cm756ghfw6haa1k")))

(define-public crate-tangy-0.2.2 (c (n "tangy") (v "0.2.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "tangy-lib") (r "^0.1.6") (d #t) (k 0)))) (h "1hpxqwrprgnidm9g9fxqq6v4fmrpxkcxlp5kc9spxrls7y9driy9")))

(define-public crate-tangy-0.2.3 (c (n "tangy") (v "0.2.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "tangy-lib") (r "^0.1.6") (d #t) (k 0)))) (h "1g1vbmk2r0186ivhhijaybfaba0f6mj8am46km6ivprmygrr2fcr")))

(define-public crate-tangy-0.2.4 (c (n "tangy") (v "0.2.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "tangy-lib") (r "^0.1") (d #t) (k 0)))) (h "0k1pvpxy9yjvfs44xn93s2zr1sal6pws9cmr7s5brwwl46ipxn3c")))

(define-public crate-tangy-0.2.6 (c (n "tangy") (v "0.2.6") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "tangy-lib") (r "^0.1") (d #t) (k 0)))) (h "0a398gzlby2a1kqx3lm0z7aprh6rl1k29bzniwj2ljr66p848f2w")))

