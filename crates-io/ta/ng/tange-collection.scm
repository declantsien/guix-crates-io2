(define-module (crates-io ta ng tange-collection) #:use-module (crates-io))

(define-public crate-tange-collection-0.1.0 (c (n "tange-collection") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tange") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mxjw8bcaalnfgxj5wg6fk9awx7jpl635q89vv72an3s3vrk758n")))

(define-public crate-tange-collection-0.1.1 (c (n "tange-collection") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2.5") (d #t) (k 0)) (d (n "tange") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1j2sxbhnqziqnlz5l1xy6gyiqy8jwr0r2qvims6x9lq8xl14dsdi")))

(define-public crate-tange-collection-0.1.2 (c (n "tange-collection") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2.5") (d #t) (k 0)) (d (n "tange") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cpnn3k8xdq5csprckm6j22q52invmkzf0dp3rfnn36h0bgbv7l5")))

