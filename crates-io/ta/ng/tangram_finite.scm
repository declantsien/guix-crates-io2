(define-module (crates-io ta ng tangram_finite) #:use-module (crates-io))

(define-public crate-tangram_finite-0.4.0 (c (n "tangram_finite") (v "0.4.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gqlzm7xfkkrpsirlznyxm48j7rs7zg5678q92i1zp71m53hwavr")))

(define-public crate-tangram_finite-0.5.0 (c (n "tangram_finite") (v "0.5.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "095klm1rklrb8qcnn9l84y9s38zn23ghh20smpa76p9kyrfi6gl8")))

(define-public crate-tangram_finite-0.6.0 (c (n "tangram_finite") (v "0.6.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ady23acljinl6bq9zwilhr07vg10g176qfxjb96ix3ngmhhzr7n")))

(define-public crate-tangram_finite-0.7.0 (c (n "tangram_finite") (v "0.7.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m1nnjzb8bl3xppndqvzj2wpqc79l3fbgpavxylmx1gp78zcfs5i")))

