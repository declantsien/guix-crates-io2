(define-module (crates-io ta ng tango-client-sys) #:use-module (crates-io))

(define-public crate-tango-client-sys-0.1.0 (c (n "tango-client-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1w3gpykqxzib22svk2y3xx6pr3fbxdjhjdlzxh3plsysq773byw4")))

(define-public crate-tango-client-sys-0.1.1 (c (n "tango-client-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1q0dvrl16nayf66nmkg9nn9pk65c0lahs63kjm4di2wcwfazvwkg")))

(define-public crate-tango-client-sys-0.1.2 (c (n "tango-client-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01s0q1zfi9pb8cz6jnl1kqhf3fwsrgx3k57gvz3awbgpvjiv8gmp")))

(define-public crate-tango-client-sys-0.1.3 (c (n "tango-client-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "195fl7xgavgdq0yhxyvkl38hh2ri2xwxqb19yil07fzgyrja1xzk")))

(define-public crate-tango-client-sys-0.1.4 (c (n "tango-client-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1g6b1gc3868mj72gr2fj7v58jz7a9g03zkbq00pq9p34009dwxmg")))

(define-public crate-tango-client-sys-0.1.5 (c (n "tango-client-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "02k9sagpdk979avnn9ij6g3h4gl1bn6cz1ikalx27s455hwnfia8")))

