(define-module (crates-io ta ng tangram_text) #:use-module (crates-io))

(define-public crate-tangram_text-0.4.0 (c (n "tangram_text") (v "0.4.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vbg3qj0481cimcdc02blh2n1vkbl6gdbsfnjr3lffsfg72cnp35")))

(define-public crate-tangram_text-0.5.0 (c (n "tangram_text") (v "0.5.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04zqsxjbh1hsjrhqyfbahkdsmvgqm4w5jzxgw475afs4xkcijr90")))

(define-public crate-tangram_text-0.6.0 (c (n "tangram_text") (v "0.6.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k5bmjw9r7wkskcz0dyk059ivv5kwhnfa3lw5c7zsmhy36dn2lwy")))

(define-public crate-tangram_text-0.7.0 (c (n "tangram_text") (v "0.7.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lfysf4bkbbczamllmw65gdjypb75chzqdny3r6hwr55pvszg0rw")))

