(define-module (crates-io ta ng tangle) #:use-module (crates-io))

(define-public crate-tangle-0.2.0 (c (n "tangle") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "threadpool") (r "^1.0.0") (d #t) (k 0)))) (h "1py3qh4hb769xbs06ka6lipcsrnz997vjs1bnili731hwv4p3wa0")))

(define-public crate-tangle-0.3.0 (c (n "tangle") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "threadpool") (r "^1.0.0") (d #t) (k 0)))) (h "0rwzacbgvmna62xjag9nqf0cm8lbgiz8x20kdmbdbfzw2bchb826")))

(define-public crate-tangle-0.4.0 (c (n "tangle") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "threadpool") (r "^1.0.0") (d #t) (k 0)))) (h "076jy1028948wl3rj35mfrdyz2bm58jadx0c1hfhdnmqdk1hrknl")))

