(define-module (crates-io ta ng tange) #:use-module (crates-io))

(define-public crate-tange-0.1.0 (c (n "tange") (v "0.1.0") (d (list (d (n "jobpool") (r "^0.3.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "priority-queue") (r "^0.5.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1pm9i0azwrr22rlw6x55hpvkrdfdf2rbaslkqq7g722p0a5nwjws")))

(define-public crate-tange-0.1.1 (c (n "tange") (v "0.1.1") (d (list (d (n "jobpool") (r "^0.3.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "priority-queue") (r "^0.5.1") (d #t) (k 0)))) (h "078z5241vnxp0v9vkhwlbvw5zblbh96s21r5zx36hw7q4m994fak")))

