(define-module (crates-io ta ng tangram-vision-sdk) #:use-module (crates-io))

(define-public crate-tangram-vision-sdk-0.2.0 (c (n "tangram-vision-sdk") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "conv") (r ">=0.3.2") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "minisign-verify") (r "^0.1") (d #t) (k 1)) (d (n "opencv") (r "^0.53") (d #t) (k 2)) (d (n "regex") (r "^1.5") (d #t) (k 1)) (d (n "rustls") (r "^0.19") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.1") (f (quote ("tls"))) (d #t) (k 1)) (d (n "webpki-roots") (r "^0.21") (d #t) (k 1)))) (h "11mkc9lymnqwa8xqv3ims501klj5yi67d062277dgymmxah7yp2g")))

