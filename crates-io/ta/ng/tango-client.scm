(define-module (crates-io ta ng tango-client) #:use-module (crates-io))

(define-public crate-tango-client-0.1.0 (c (n "tango-client") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "07s2y9kbsq0rkjv944rmn567lflarkgqhcsbqlw0dmzmpwi3dhbp")))

(define-public crate-tango-client-0.1.1 (c (n "tango-client") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "05r7yi31br5hk9jmggilmd25kldy7gvrffv87ywyfgjq69n3qbqn")))

(define-public crate-tango-client-0.2.0 (c (n "tango-client") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1yh8w5m8c1g2bwhv65mamx6cbl96nwsvkdshqslmvxp695r0xpkk")))

(define-public crate-tango-client-0.2.1 (c (n "tango-client") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0lfvpmsrs2fq13bx1xhna01zd9k5rggcb14fzdmjkad4rv025vl7")))

(define-public crate-tango-client-0.2.2 (c (n "tango-client") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0vc0l8y1smjz65kb3nk2acp8yrbx1jhvai6bflflxvjhjmva3080")))

(define-public crate-tango-client-0.2.3 (c (n "tango-client") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0in4pd8gcsvnsy5xwzfklbgdm9g91cix2f1w850sxk62qf2rm600")))

(define-public crate-tango-client-0.3.0 (c (n "tango-client") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "19anb0zldnznanqr4qwlg539dx7i6rcmlc33d7aqj2kz67kzf75l")))

(define-public crate-tango-client-0.3.1 (c (n "tango-client") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0mh0znvpn0h8s59sjps2fz0p1lj8vf4xd561y3b305hlw2ylkh0v")))

(define-public crate-tango-client-0.3.2 (c (n "tango-client") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "155p0qdlly2pf806wpm2ra8cjhdxll0z4ia5yc48n5xjwzgd3wby")))

(define-public crate-tango-client-0.4.0 (c (n "tango-client") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "05lh5irkaa5zdmpbzx2lfnran4g0wrc7ydjpqq9xcdbhsrlmk4p4")))

(define-public crate-tango-client-0.4.1 (c (n "tango-client") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1dc169ji12aq7rbaags75lgpl468xszx7gcz25gsqsnvid39y4lz")))

(define-public crate-tango-client-0.4.2 (c (n "tango-client") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "tango-client-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0ay3my4wv2jr33lcpg7gb1k106kd2fa02knlzx7xi1wqwvr03d9x")))

