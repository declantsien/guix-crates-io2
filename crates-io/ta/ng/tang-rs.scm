(define-module (crates-io ta ng tang-rs) #:use-module (crates-io))

(define-public crate-tang-rs-0.1.0 (c (n "tang-rs") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.4") (k 0)) (d (n "tokio") (r "^0.2.16") (f (quote ("rt-core" "rt-threaded" "time"))) (k 0)) (d (n "tokio") (r "^0.2.16") (f (quote ("macros"))) (d #t) (k 2)))) (h "0xw2xbzgw9f1frqj1kssk5c3q5y2xqqvb260k36bl8vkisy8cj0y") (f (quote (("default"))))))

(define-public crate-tang-rs-0.2.0 (c (n "tang-rs") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 2)))) (h "15bmhz3nwb152znl8f3p21094fbmcybpnbhxl63iifzpfrywn9dn") (f (quote (("no-send") ("default"))))))

