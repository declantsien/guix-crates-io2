(define-module (crates-io ta ng tangled) #:use-module (crates-io))

(define-public crate-tangled-0.0.1 (c (n "tangled") (v "0.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (f (quote ("trace"))) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter" "fmt"))) (d #t) (k 2)))) (h "0v4a522wy9n0nc0kcr8rc8zlss0096gzlcviakc5z16w05a34acf")))

(define-public crate-tangled-0.1.0 (c (n "tangled") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (f (quote ("trace"))) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter" "fmt"))) (d #t) (k 2)))) (h "1yqsscf9j6ajj4pxq4p4j13vvw3yjp2bbh9x8bd9rq7llcvrw9wg")))

