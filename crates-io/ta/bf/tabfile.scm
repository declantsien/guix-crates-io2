(define-module (crates-io ta bf tabfile) #:use-module (crates-io))

(define-public crate-tabfile-0.2.0 (c (n "tabfile") (v "0.2.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "199ybm0q3qjzbvg1wha2lgcv1z4frdm1i8dshfcgxadjgr64a3zb")))

(define-public crate-tabfile-0.2.1 (c (n "tabfile") (v "0.2.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0dppgwpw3x30a8chv6jqwqlvabr8apqry5gzf7zghq044w0qgy31")))

