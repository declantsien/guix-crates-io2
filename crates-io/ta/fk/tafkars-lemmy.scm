(define-module (crates-io ta fk tafkars-lemmy) #:use-module (crates-io))

(define-public crate-tafkars-lemmy-0.1.0 (c (n "tafkars-lemmy") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "lemmy_api_common") (r "^0.17.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tafkars") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "080sdii3lbx58kq0xkgxlh8sl0vd7p8ql7wddmq6cvy653pvx04d")))

