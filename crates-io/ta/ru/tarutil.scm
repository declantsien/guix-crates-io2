(define-module (crates-io ta ru tarutil) #:use-module (crates-io))

(define-public crate-tarutil-0.1.0 (c (n "tarutil") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "13n1mnjn144vgba13y8r0pn2lg8qdsqkys4fxlh2yvavbhf6vsd1")))

(define-public crate-tarutil-0.2.0 (c (n "tarutil") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0wv8zqfgzffxq7ydqyj4yspghafpgi5h09xdbayfbls7ghhgfw7w")))

(define-public crate-tarutil-0.3.0 (c (n "tarutil") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0mi6bl85dkk7cv7p6vcq264sbly3qrya8apha107zgsf4jkcsh46")))

(define-public crate-tarutil-0.3.1 (c (n "tarutil") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1dccijcy7bla6db43cgyhdxdq9q4g9qacrn3cb4n6pil70l6dbh2")))

(define-public crate-tarutil-0.4.0 (c (n "tarutil") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "165b0jz1y2cqi0qm5jq7gfy0my72x5dh9nxg422dvbw8gxvnnxfv")))

(define-public crate-tarutil-0.5.0 (c (n "tarutil") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1g5gap3f8agma2sp9njv9lwx0c9idgsyb4v4hb1w3jay9dh78myf")))

