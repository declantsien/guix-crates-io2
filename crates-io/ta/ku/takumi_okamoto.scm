(define-module (crates-io ta ku takumi_okamoto) #:use-module (crates-io))

(define-public crate-takumi_okamoto-0.1.0 (c (n "takumi_okamoto") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1pdwhdnb92qy2i69xiyz0ms1vccamwrslsm6n42hrcnmg7pm5jn1") (y #t)))

(define-public crate-takumi_okamoto-0.2.0 (c (n "takumi_okamoto") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0ymwh0d6dpw0cfpanhj1nbi7qgm23csmazwg3i91885q5115jc9b")))

(define-public crate-takumi_okamoto-0.3.0 (c (n "takumi_okamoto") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.7.1") (d #t) (k 0)))) (h "1wf2m5vkl1dj9jrslx2aspszgllgqpsljnw8jdqcngyrrwzq1s1a")))

