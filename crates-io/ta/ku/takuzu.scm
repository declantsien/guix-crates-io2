(define-module (crates-io ta ku takuzu) #:use-module (crates-io))

(define-public crate-takuzu-0.2.0 (c (n "takuzu") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1dadj0zs0836layd7lzbn489gq06rwaav2vms85gb9s918fnqk7j")))

(define-public crate-takuzu-0.2.1 (c (n "takuzu") (v "0.2.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0qr6h13ahl6hp6qcjc9dj5bkfvsywk4ppbwbxmcbb11hmrqi3jc8")))

(define-public crate-takuzu-0.2.2 (c (n "takuzu") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ygsp1fdyjbjc9x95m16ha9jffwx270dn1d4vgy9jvqk7f28yfzv")))

(define-public crate-takuzu-0.3.0 (c (n "takuzu") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p8w9lz4qphpmj6h80sfpcxgamw0wflz9ycwy8hgi5dnxrcfh4ah")))

(define-public crate-takuzu-0.3.1 (c (n "takuzu") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06fn6s2dzizc7x3lg0wygcyws9h1lrvsmvswvp4ng6sn6vrxsmff")))

(define-public crate-takuzu-0.3.2 (c (n "takuzu") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n550wry1ggva9yx1pn5yvj6iyl6fbvqpwqxhf4y764523vjp5kg")))

(define-public crate-takuzu-0.3.3 (c (n "takuzu") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lnnidyn406kzdvrkdv0g84cxvlfh71z4ql3pdw5x4b29gyibywf")))

(define-public crate-takuzu-0.4.0 (c (n "takuzu") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "167irlmmgql9hlp21lgw9z0yh407105a24vv0ynjaqnkfhvhzglk")))

(define-public crate-takuzu-0.5.0 (c (n "takuzu") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cyvc1fivhbxps60fkjlpxinl315yr2m3cmahqjpf36zangxw9c4")))

(define-public crate-takuzu-0.5.1 (c (n "takuzu") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rfdccx89xvlbh604am18vk0n41h8f0s24jhgsxj96ar73aw1d4q")))

(define-public crate-takuzu-0.6.0 (c (n "takuzu") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05n3d9yvs9mjlhnf3mqblczlysjh5c5fxqmjivyr766xvcqrjzav")))

(define-public crate-takuzu-1.0.0 (c (n "takuzu") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06apnrv12xih05b95cd4gxzvppcp9z8m2ki0k77v8fs4z67fgvwf")))

(define-public crate-takuzu-1.0.1 (c (n "takuzu") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s94kq12m3yh2ghvlswcfbiffbyvdsrh0w7wd934sz52h8601q7c")))

(define-public crate-takuzu-1.1.0 (c (n "takuzu") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08w0j3r2xhfn4sifg9jcnq0dlgh3n2s8184p87kpzw232m58frgy")))

(define-public crate-takuzu-1.1.1 (c (n "takuzu") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17g8xzs0l90yyqmgc1nqiwrlnhzb3nnkyafmxkj9hd39m1nbgh4r")))

