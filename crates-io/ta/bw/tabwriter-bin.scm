(define-module (crates-io ta bw tabwriter-bin) #:use-module (crates-io))

(define-public crate-tabwriter-bin-0.1.1 (c (n "tabwriter-bin") (v "0.1.1") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "1sk2rnmdy6lk0jrcgzlf7hnwj77is4694piw3rc214b5b2hwwb53")))

(define-public crate-tabwriter-bin-0.2.0 (c (n "tabwriter-bin") (v "0.2.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "19dx7h7d7mgyhpr4claf1fn68ijn337q3r19b1ymfkfh1d05qdxb")))

(define-public crate-tabwriter-bin-0.2.1 (c (n "tabwriter-bin") (v "0.2.1") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "0w6g1p66jfjpcghz6yn6wwjbgwbvlm9zar4qf3z4z7xfryybgcdh")))

