(define-module (crates-io ta bw tabwriter) #:use-module (crates-io))

(define-public crate-tabwriter-0.1.1 (c (n "tabwriter") (v "0.1.1") (d (list (d (n "docopt") (r "~0.6.8") (d #t) (k 0)))) (h "17h97rgl6yfpwhv0w9s2qxv9yc0ycjvpva6i366lylfhldiv2mm5")))

(define-public crate-tabwriter-0.1.2 (c (n "tabwriter") (v "0.1.2") (d (list (d (n "docopt") (r "~0.6.8") (d #t) (k 0)))) (h "10y6ki7sj151i1r8xawjmjwpf9yy53fcgfciirds8hq3isdvxb1f")))

(define-public crate-tabwriter-0.1.3 (c (n "tabwriter") (v "0.1.3") (d (list (d (n "docopt") (r "~0.6.8") (d #t) (k 0)))) (h "1p3s05wsmm6ysgci2szas52svi7r7fi5dl85zk9jizqinqj8ai1j")))

(define-public crate-tabwriter-0.1.5 (c (n "tabwriter") (v "0.1.5") (d (list (d (n "docopt") (r "*") (d #t) (k 0)))) (h "0jl1h77pf7aa0yd3q2g416b43z6xpz5dmygw65qjsb5r8r75l96d")))

(define-public crate-tabwriter-0.1.6 (c (n "tabwriter") (v "0.1.6") (d (list (d (n "docopt") (r "*") (d #t) (k 0)))) (h "12pwzii9h2c3g209bs52bj4cjbxn1nmjpngcn6hcybci033hc864")))

(define-public crate-tabwriter-0.1.8 (c (n "tabwriter") (v "0.1.8") (d (list (d (n "docopt") (r "*") (d #t) (k 0)))) (h "0kls1ifx8jmi7pw7kncqgkjf6fs89hp3lkw15i2fz3f4vi1ivx05")))

(define-public crate-tabwriter-0.1.9 (c (n "tabwriter") (v "0.1.9") (d (list (d (n "docopt") (r "*") (d #t) (k 0)))) (h "0cfq5gj8csl5xwy4j6lpcxinjspp71k516b01qls0f3qgq7x22bf")))

(define-public crate-tabwriter-0.1.10 (c (n "tabwriter") (v "0.1.10") (d (list (d (n "docopt") (r "*") (d #t) (k 0)))) (h "1m8iwb441gmisq6z6zmx6n8chmdhqz7rjgmn0b5gyc5q0iy23gkq")))

(define-public crate-tabwriter-0.1.11 (c (n "tabwriter") (v "0.1.11") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0fw8i17xmiq96kljc4mvbfbbc4mhr7injv9h3a1j3snz7jd9v4a3")))

(define-public crate-tabwriter-0.1.12 (c (n "tabwriter") (v "0.1.12") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0cdfyjddsb9vvyyx0dba3xxzargpgqmmnyaciz8mza0ci1sbj7qb")))

(define-public crate-tabwriter-0.1.13 (c (n "tabwriter") (v "0.1.13") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "03sw1q2hr3lqhsvxsskz71zs4m0ppd5i65jvldm0il12bva697v5")))

(define-public crate-tabwriter-0.1.14 (c (n "tabwriter") (v "0.1.14") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0vx29x3b2w8d42f8gpz2g1s7m7yrshvx8xhlgp485y56ppqfsfgr")))

(define-public crate-tabwriter-0.1.15 (c (n "tabwriter") (v "0.1.15") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0f5m9hjby2n80r8c0vi5m883crjgyhbmszdfdrkrwsbfrfb74822")))

(define-public crate-tabwriter-0.1.16 (c (n "tabwriter") (v "0.1.16") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1sszl7jlsg8ryl9mji9czcdzsvqr1g3kwny4kh95pz6ncwb3q73h")))

(define-public crate-tabwriter-0.1.17 (c (n "tabwriter") (v "0.1.17") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0w850610ls8a1rgxpw3f8zwbay7g90csmn1gml54w3yi0gj9xlcx")))

(define-public crate-tabwriter-0.1.18 (c (n "tabwriter") (v "0.1.18") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "14wsl7hafvb4c0nll90gz1pdx021l2qyss1jzc5kmymscz7ksaiv")))

(define-public crate-tabwriter-0.1.19 (c (n "tabwriter") (v "0.1.19") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "05nbgwg2bwyhs5jd84x2snafjhcwyvf9w5dk2hcqv462ppipl43s")))

(define-public crate-tabwriter-0.1.20 (c (n "tabwriter") (v "0.1.20") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0g22crjfrgll288b7ad816yjm5cxd152cnv7779rj30fh49dlvky")))

(define-public crate-tabwriter-0.1.21 (c (n "tabwriter") (v "0.1.21") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "16k90fdk25yhs8p1d91glqlqqxqv25gr89ndffsb7isl4r1r0ph0")))

(define-public crate-tabwriter-0.1.22 (c (n "tabwriter") (v "0.1.22") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1f8ffrpz8w6i9848qsm4vwm11y1j8sqb4cd4jkgan96wkpxqm75x")))

(define-public crate-tabwriter-0.1.23 (c (n "tabwriter") (v "0.1.23") (d (list (d (n "unicode-width") (r "*") (d #t) (k 0)))) (h "12pvspdfdx1rni8z1vywnfvrfw8qq3vcbxzynsn1nyzbjcy8p7p2")))

(define-public crate-tabwriter-0.1.24 (c (n "tabwriter") (v "0.1.24") (d (list (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1ffv3xxshs1ga7cz0w4rsn0nnw53n6znvbhacz9cjkby40jkj9d3")))

(define-public crate-tabwriter-0.1.25 (c (n "tabwriter") (v "0.1.25") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0fzd4iwfg2ddjwgzz4bnyr1h25jzbf6g03mhyigdb498v9izbnw5") (f (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1.0.0 (c (n "tabwriter") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0lvi8pkqljdibr4w187dkmsls0x02x729bgkgkh5mqrnw8jp04ih") (f (quote (("default") ("ansi_formatting" "regex" "lazy_static")))) (y #t)))

(define-public crate-tabwriter-1.0.1 (c (n "tabwriter") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "08wdvp6mgvs44a915pvqk2a0np9adwxzd420g1504rq1za03f95j") (f (quote (("default") ("ansi_formatting" "regex" "lazy_static")))) (y #t)))

(define-public crate-tabwriter-1.0.2 (c (n "tabwriter") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0h4k749j4j88wag0md3dzqygz3xfbsbyq6fzzhjfw3s8y9xjgb8y") (f (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1.0.3 (c (n "tabwriter") (v "1.0.3") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0piq2b5svgrpi53x2dijvwi607rdvdnx3gx9r4nyp8n05cb10y1v") (f (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1.0.4 (c (n "tabwriter") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1pf4xaw5qrg5qr94jbjd47yg10i1kslb12jnwh9x2w9a3v3rmasn") (f (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1.1.0 (c (n "tabwriter") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1fsfri1alvmxd66jklj3wiscp7qldql2lwarmm64jlcy2jly6a4i") (f (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1.2.0 (c (n "tabwriter") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3") (f (quote ("std"))) (o #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "13hjd6h19pr3wp2kmr1579cgg7zybrrprv87dh5afdh4199vambn") (f (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1.2.1 (c (n "tabwriter") (v "1.2.1") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3") (f (quote ("std"))) (o #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "048i0mj3b07zlry9m5fl706y5bzdzgrswymqn32drakzk7y5q81n") (f (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1.3.0 (c (n "tabwriter") (v "1.3.0") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1wgf6ajrq3ryrvmps2c8k2aizk8lwdmdi5gyjlq1lra1wqz1gq88") (f (quote (("default") ("ansi_formatting"))))))

(define-public crate-tabwriter-1.4.0 (c (n "tabwriter") (v "1.4.0") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1xp5j7v8jsk92zcmiyh4ya9akhrchjvc595vwcvxrxk49wn2h9x3") (f (quote (("default") ("ansi_formatting"))))))

