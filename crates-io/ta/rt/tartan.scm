(define-module (crates-io ta rt tartan) #:use-module (crates-io))

(define-public crate-tartan-0.1.0 (c (n "tartan") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "imageproc") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std"))) (k 0)))) (h "04mm6kg1ymph0l8rb16gszjj47y5x13d83faja670i0lmgcr2jbh") (f (quote (("cli" "clap" "image" "imageproc"))))))

(define-public crate-tartan-0.2.0 (c (n "tartan") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std"))) (k 0)) (d (n "tartanlib") (r "^0.2.0") (d #t) (k 0)))) (h "07kzn2gqhx73bxwm4bpqjrxapw5q7lkx42l4vv5y9b5l6wzkzfc1")))

(define-public crate-tartan-0.3.0 (c (n "tartan") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std"))) (k 0)) (d (n "tartanlib") (r "^0.3.0") (d #t) (k 0)))) (h "0flyirlhg8cs8jyrpmqa6px2zb48qbqabdiyrjfcis15yj317r99")))

(define-public crate-tartan-0.4.0 (c (n "tartan") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std"))) (k 0)) (d (n "tartanlib") (r "^0.4.0") (d #t) (k 0)))) (h "160m4g9q3ns3yyixbrvf83n91s3k9asnh4pavmc882338zg6bici")))

