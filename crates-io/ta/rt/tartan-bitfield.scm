(define-module (crates-io ta rt tartan-bitfield) #:use-module (crates-io))

(define-public crate-tartan-bitfield-1.0.0 (c (n "tartan-bitfield") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "02qphhnv5ksj83k6jh0p9zx3klqaxai6cnnaqls3a4369jdqhjvx")))

(define-public crate-tartan-bitfield-1.1.0 (c (n "tartan-bitfield") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "1rb4d5i15gm8gxm2ki56zw9fzq3k8i5b2v6295nzfq89d2dszdni")))

(define-public crate-tartan-bitfield-1.2.0 (c (n "tartan-bitfield") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "0v3jicciw7qiqnsxlkjqmmxsl02k225q1z56c81zydia9s87da5z")))

