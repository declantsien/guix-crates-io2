(define-module (crates-io ta rt tartanlib) #:use-module (crates-io))

(define-public crate-tartanlib-0.2.0 (c (n "tartanlib") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std"))) (k 0)))) (h "05pm4m3md9arcbw1620ffy6wghwrcf9l649lzzkdvjdx062k2vqq")))

(define-public crate-tartanlib-0.3.0 (c (n "tartanlib") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std"))) (k 0)))) (h "0dx53pb99sy8yqqkddnn9pv22lrc0bj4kdfa0asygbj64nw82mvb")))

(define-public crate-tartanlib-0.4.0 (c (n "tartanlib") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "no-std-compat") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std"))) (k 0)))) (h "1cv2d6syaf0khq6i1n271jpsg339nk15j3k7g353j5v19zhd6kcp") (f (quote (("std") ("default" "std"))))))

