(define-module (crates-io ta ge tagesschau_api) #:use-module (crates-io))

(define-public crate-tagesschau_api-0.1.0 (c (n "tagesschau_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "time") (r "^0.3.26") (f (quote ("local-offset" "formatting" "macros" "serde" "parsing"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0kcpmb3ajhv8pgbngcbd3dxc5g1hs3svkqsli2wxzzszn86hcfch") (y #t)))

