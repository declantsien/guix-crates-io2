(define-module (crates-io ta ky takyon) #:use-module (crates-io))

(define-public crate-takyon-0.1.0 (c (n "takyon") (v "0.1.0") (d (list (d (n "io-uring") (r "^0.6.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nohash") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0ggm8lk823pbsadisg2fsgn7bvqiqkrhnl6zvxwpj1m3g3zpr0hz")))

(define-public crate-takyon-0.2.0 (c (n "takyon") (v "0.2.0") (d (list (d (n "io-uring") (r "^0.6.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nohash") (r "^0.2.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "1vsph6g4s9wbs91ak1czg7q9svgabbfd354w86m4sq5h6wccadf3")))

