(define-module (crates-io ta vo tavol) #:use-module (crates-io))

(define-public crate-tavol-0.1.0 (c (n "tavol") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0icp008xk81ka14j80ps8gjfiwinb26ybjb883jppav6b74b1x5m") (y #t)))

