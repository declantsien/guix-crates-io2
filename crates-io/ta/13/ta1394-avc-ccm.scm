(define-module (crates-io ta #{13}# ta1394-avc-ccm) #:use-module (crates-io))

(define-public crate-ta1394-avc-ccm-0.1.0 (c (n "ta1394-avc-ccm") (v "0.1.0") (d (list (d (n "ta1394-avc-general") (r "^0.1") (d #t) (k 0)))) (h "0zq1rd0i8ifqzkh0rxrsjjl3wg9lkp6avkk3x6lzyvb52qbqrjal")))

(define-public crate-ta1394-avc-ccm-0.2.0 (c (n "ta1394-avc-ccm") (v "0.2.0") (d (list (d (n "ta1394-avc-general") (r "^0.2") (d #t) (k 0)))) (h "05fn0lgy0fyraad686vpa5bzbl63inlrhrs71f7fvf1qfqrh4nyv")))

