(define-module (crates-io ta #{13}# ta1394-avc-general) #:use-module (crates-io))

(define-public crate-ta1394-avc-general-0.1.0 (c (n "ta1394-avc-general") (v "0.1.0") (d (list (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "1hbbvj7fhl5zpcpr7b9dx51imbscl6cjr1wfgihc108g4w05chm2")))

(define-public crate-ta1394-avc-general-0.2.0 (c (n "ta1394-avc-general") (v "0.2.0") (d (list (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "0jy2wx0dp3fdx4yfxjx74idw9iw9vkq9qpd77r62krgp8sv75pwz")))

