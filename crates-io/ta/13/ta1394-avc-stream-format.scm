(define-module (crates-io ta #{13}# ta1394-avc-stream-format) #:use-module (crates-io))

(define-public crate-ta1394-avc-stream-format-0.1.0 (c (n "ta1394-avc-stream-format") (v "0.1.0") (d (list (d (n "ta1394-avc-general") (r "^0.1") (d #t) (k 0)))) (h "11d8ildg4drl53vnzgzc4wcifyvqrxwy66cdrjg626g7c3vlkzcf")))

(define-public crate-ta1394-avc-stream-format-0.2.0 (c (n "ta1394-avc-stream-format") (v "0.2.0") (d (list (d (n "ta1394-avc-general") (r "^0.2") (d #t) (k 0)))) (h "0aj2gzpj3r4gyd8793jmh0xxbksrf2hin051d6sg2bscw5g5jzqz")))

