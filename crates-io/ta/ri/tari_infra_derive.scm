(define-module (crates-io ta ri tari_infra_derive) #:use-module (crates-io))

(define-public crate-tari_infra_derive-0.0.1 (c (n "tari_infra_derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "10ia6lf6fv8fj3wnhvkgarl63878ahfmk83rm43l589sx13ghizk")))

(define-public crate-tari_infra_derive-0.0.5 (c (n "tari_infra_derive") (v "0.0.5") (d (list (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (d #t) (k 0)))) (h "0chv3zk1yryai8wsrvpn5s6dl7fbxlgb1p311d484018hr5mrlml")))

(define-public crate-tari_infra_derive-0.0.6 (c (n "tari_infra_derive") (v "0.0.6") (d (list (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (d #t) (k 0)))) (h "1r1gq4clfdlssf60f3cck9hcf7yah05ijxa14616m1jw9b5l8wqx")))

(define-public crate-tari_infra_derive-0.0.7 (c (n "tari_infra_derive") (v "0.0.7") (d (list (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (d #t) (k 0)))) (h "1775vdgra1wrfycpjsk26n1ch6qf83b6sprrv4wndk8h87nn5a0x")))

(define-public crate-tari_infra_derive-0.0.9 (c (n "tari_infra_derive") (v "0.0.9") (d (list (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (d #t) (k 0)))) (h "0agzklxfj3w4g88f02jjrzfd7xvd2j2sjvwm678rffdjqv3qdkhy")))

(define-public crate-tari_infra_derive-0.0.10 (c (n "tari_infra_derive") (v "0.0.10") (d (list (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (d #t) (k 0)))) (h "13g51z7fmw3c5zc9pj4aa7cgrgj30q4f20g86x9ahgx6zq1c5ijf")))

(define-public crate-tari_infra_derive-0.8.1 (c (n "tari_infra_derive") (v "0.8.1") (d (list (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (d #t) (k 0)))) (h "0zi7ggdschys2dyqcbfvd3k70z7hicyyvcq4bymj8vj1gv5lq7zb")))

(define-public crate-tari_infra_derive-0.9.5 (c (n "tari_infra_derive") (v "0.9.5") (d (list (d (n "blake2") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (d #t) (k 0)))) (h "1gwfx32flr2zdwpp26sfwxm67c1s8gc4gcqlich7q9n9zchqfx31")))

