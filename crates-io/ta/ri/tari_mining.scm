(define-module (crates-io ta ri tari_mining) #:use-module (crates-io))

(define-public crate-tari_mining-0.0.5 (c (n "tari_mining") (v "0.0.5") (h "1lzcg45xnw4gmgvcvv1kbr3p37agmgisyshrx3fcb0b8zyq9ngay")))

(define-public crate-tari_mining-0.0.6 (c (n "tari_mining") (v "0.0.6") (h "122967cqwpc3myc6issxxdr0njhd6mriby4dwgc88ih46nc475l8")))

(define-public crate-tari_mining-0.0.7 (c (n "tari_mining") (v "0.0.7") (h "1hqz5ngi32zp7xf3lxaakwhncb60f9341bzqmympf8jfaccbhqf3")))

