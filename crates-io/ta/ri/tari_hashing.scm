(define-module (crates-io ta ri tari_hashing) #:use-module (crates-io))

(define-public crate-tari_hashing-1.0.0-pre.9 (c (n "tari_hashing") (v "1.0.0-pre.9") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 2)) (d (n "borsh") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "tari_crypto") (r "^0.20.0") (d #t) (k 0)))) (h "1jb7bwhdihzs15wmchg1z145f0ny69ykdljxvrrw4dn1mbrizwah")))

