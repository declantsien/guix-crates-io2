(define-module (crates-io ta ri tari_metrics) #:use-module (crates-io))

(define-public crate-tari_metrics-0.1.0 (c (n "tari_metrics") (v "0.1.0") (h "1sq4d0rcgdv7kirbgrmsjhv3l9qhinr1d32figm7n09j2qi6kz7z")))

(define-public crate-tari_metrics-1.0.0-rc.5 (c (n "tari_metrics") (v "1.0.0-rc.5") (d (list (d (n "anyhow") (r "^1.0.53") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (o #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (o #t) (k 0)))) (h "12p5fpfxk4ayzjzrya41jlw9ikjxf1qqvfilm3gv0mkj1cxi3r96") (f (quote (("server" "pull" "push" "log" "anyhow" "futures") ("push" "reqwest" "tokio") ("pull" "warp"))))))

