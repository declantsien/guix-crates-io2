(define-module (crates-io ta ri tari_broadcast_channel) #:use-module (crates-io))

(define-public crate-tari_broadcast_channel-0.0.6 (c (n "tari_broadcast_channel") (v "0.0.6") (d (list (d (n "arc-swap") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "futures") (r "= 0.3.0-alpha.19") (d #t) (k 0) (p "futures-preview")) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "0kk9bgf9q9412nhhaspp9y9sf2nbbpdqkmpy6hz6c330z2pp3zpw")))

(define-public crate-tari_broadcast_channel-0.0.7 (c (n "tari_broadcast_channel") (v "0.0.7") (d (list (d (n "arc-swap") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (d #t) (k 2)))) (h "007b9gpqq38qa6wvnldpji37ib6nx296zsf7rqfsi6cdyr9b9zgn")))

(define-public crate-tari_broadcast_channel-0.1.0 (c (n "tari_broadcast_channel") (v "0.1.0") (d (list (d (n "arc-swap") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.10") (d #t) (k 2)))) (h "1av80rvgl0rhvzw5wwj63500bbra0mm1cd32rcjskj8yy2l7ins1")))

(define-public crate-tari_broadcast_channel-0.1.1 (c (n "tari_broadcast_channel") (v "0.1.1") (d (list (d (n "arc-swap") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.10") (d #t) (k 2)))) (h "01p0d32y7nyl40hmnqjwgf4jg04w4pni04q1ajw14xy0j5pnmi13")))

(define-public crate-tari_broadcast_channel-0.2.0 (c (n "tari_broadcast_channel") (v "0.2.0") (d (list (d (n "arc-swap") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.10") (d #t) (k 2)))) (h "0zbxx3glmh8wxpydjyjfgmwcibqj3pxsdzxcg0gm5p7z94wyx0bq")))

(define-public crate-tari_broadcast_channel-0.3.0 (c (n "tari_broadcast_channel") (v "0.3.0") (d (list (d (n "arc-swap") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0ncwi7sabvhaf6b18ipwzmvha1knzggpc0fyp7wjicxb8hkbwzyp")))

