(define-module (crates-io ta ri tari_template_macros) #:use-module (crates-io))

(define-public crate-tari_template_macros-0.1.0 (c (n "tari_template_macros") (v "0.1.0") (d (list (d (n "indoc") (r "^1.0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tari_bor") (r "^0.1.0") (d #t) (k 0)) (d (n "tari_template_abi") (r "^0.1.0") (d #t) (k 0)))) (h "17inx2kmml770mz43pa28g9g09f47vqh8fd90vqv8vzi25s7w121")))

(define-public crate-tari_template_macros-0.3.0 (c (n "tari_template_macros") (v "0.3.0") (d (list (d (n "indoc") (r "^1.0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tari_bor") (r "^0.3.0") (d #t) (k 0)) (d (n "tari_template_abi") (r "^0.3.0") (d #t) (k 0)))) (h "1h8pwxlzg75k19pyjxrd3bi795b3n5b88arkwpr0rj5xc6xy10dc")))

(define-public crate-tari_template_macros-0.7.0 (c (n "tari_template_macros") (v "0.7.0") (d (list (d (n "indoc") (r "^1.0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tari_bor") (r "^0.7") (d #t) (k 0)) (d (n "tari_template_abi") (r "^0.7") (d #t) (k 0)))) (h "042qn4dq8cl2f57g4jv96k5rvxspirznasf8zjv1njb1vkg3i8k1")))

