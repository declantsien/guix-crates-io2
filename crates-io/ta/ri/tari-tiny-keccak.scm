(define-module (crates-io ta ri tari-tiny-keccak) #:use-module (crates-io))

(define-public crate-tari-tiny-keccak-2.0.2 (c (n "tari-tiny-keccak") (v "2.0.2") (d (list (d (n "borsh") (r "^1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "1715pr8dbd12w32lx005fiqf5bp3y33znbqc3qfc6rxc9gqd19ds") (f (quote (("tuple_hash" "cshake") ("sp800" "cshake" "kmac" "tuple_hash") ("shake") ("sha3") ("parallel_hash" "cshake") ("kmac" "cshake") ("keccak") ("k12") ("fips202" "keccak" "shake" "sha3") ("default") ("cshake"))))))

