(define-module (crates-io ta ri tari-jwt) #:use-module (crates-io))

(define-public crate-tari-jwt-0.1.0 (c (n "tari-jwt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "base64ct") (r "^1.5.2") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "chrono") (r "^0.4.35") (k 2)) (d (n "const-decoder") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-buffer-serde") (r "^0.4.0") (d #t) (k 2)) (d (n "jwt-compact") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 2)) (d (n "tari_crypto") (r "^0.20.0") (f (quote ("std"))) (k 0)))) (h "1xw70alfx1f1k2ixf6p7grpbacralx9k2rc7nd11fcrckkawr73g")))

