(define-module (crates-io ta ri tari_protobuf_build) #:use-module (crates-io))

(define-public crate-tari_protobuf_build-0.0.6 (c (n "tari_protobuf_build") (v "0.0.6") (d (list (d (n "prost-build") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0vb15qrj9vqyrqiivq8gki1ybyvzhxbj055l01n62klxf4nkrbpg")))

(define-public crate-tari_protobuf_build-0.0.7 (c (n "tari_protobuf_build") (v "0.0.7") (d (list (d (n "prost-build") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0rkb57sgf3v2igyxm5fafyk62nx2f6i3r1vjasw1lnw0qv3zm9bv")))

