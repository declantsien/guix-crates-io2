(define-module (crates-io ta ri tari_shutdown) #:use-module (crates-io))

(define-public crate-tari_shutdown-0.0.6 (c (n "tari_shutdown") (v "0.0.6") (d (list (d (n "futures") (r "= 0.3.0-alpha.19") (d #t) (k 0) (p "futures-preview")) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "0k4mvmkgzkzaq3f9a4lciwxm2hljilw5251dn2knfd0xnc4z0kl8")))

(define-public crate-tari_shutdown-0.0.7 (c (n "tari_shutdown") (v "0.0.7") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 2)))) (h "1zb3c9k3516s3ff9pliwgwi8lszyi5zzrd9icdrzvcad2419xqv7")))

(define-public crate-tari_shutdown-0.0.9 (c (n "tari_shutdown") (v "0.0.9") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 2)))) (h "0gvy7jnqpx97vl4ls02jkid0l6qb3h0y6jbxcc005qal6mq8b7hd")))

(define-public crate-tari_shutdown-0.0.10 (c (n "tari_shutdown") (v "0.0.10") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 2)))) (h "1ajzi38ch712fz1kd7xsj7zk8i5m3s7x1473lsvbmq8cs5h95i5h")))

(define-public crate-tari_shutdown-0.2.3 (c (n "tari_shutdown") (v "0.2.3") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 2)))) (h "0b578l189v0jjlqhkn8fxbs2blnmi5rhmd64k3gx1b9z8ri4l1wc")))

(define-public crate-tari_shutdown-0.2.7 (c (n "tari_shutdown") (v "0.2.7") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 2)))) (h "142c6zyixci9kaiiqng7y0pdwkkgrvgvwp991h0gnjpgv3g2vch8")))

(define-public crate-tari_shutdown-0.8.1 (c (n "tari_shutdown") (v "0.8.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 2)))) (h "1p7b38aw353snxs8rfk1vfx9dy6qp7421r2l7i2l00352zb69hhl")))

(define-public crate-tari_shutdown-0.32.0 (c (n "tari_shutdown") (v "0.32.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (k 2)))) (h "1psb94cynagj2v97c2q94rm0c79i1p3x18nf27q05nb7b3g1rgcr")))

(define-public crate-tari_shutdown-1.0.0-rc.5 (c (n "tari_shutdown") (v "1.0.0-rc.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (k 2)))) (h "1akn4fbvzj7rdwmbxp7mvjdp9l5imnrpg761dvh3qyw8nwh6id0g")))

