(define-module (crates-io ta ri tari_script) #:use-module (crates-io))

(define-public crate-tari_script-0.1.0 (c (n "tari_script") (v "0.1.0") (h "19gwyij1d9syigsxhgrfsdv9n1x0mr5cr2j9vajs7m8fc2vjn8q1")))

(define-public crate-tari_script-1.0.0-rc.5 (c (n "tari_script") (v "1.0.0-rc.5") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "borsh") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "tari_crypto") (r "^0.20") (d #t) (k 0)) (d (n "tari_utilities") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07fscsmxnjgzwa927pysf8mzwr86g9yipb9xnrzvggsy4fcbw3kv")))

