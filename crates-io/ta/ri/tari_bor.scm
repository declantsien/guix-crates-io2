(define-module (crates-io ta ri tari_bor) #:use-module (crates-io))

(define-public crate-tari_bor-0.1.0 (c (n "tari_bor") (v "0.1.0") (d (list (d (n "ciborium") (r "^0.2.1") (k 0)) (d (n "ciborium-io") (r "^0.2.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lz7k80m8z69xpxggbvw10f8wxfal55mr9w92rshh5j1zzrwprfb") (f (quote (("std" "ciborium/std" "serde/std") ("json_encoding") ("default" "std") ("alloc" "serde/alloc" "ciborium-io/alloc"))))))

(define-public crate-tari_bor-0.3.0 (c (n "tari_bor") (v "0.3.0") (d (list (d (n "ciborium") (r "^0.2.1") (k 0)) (d (n "ciborium-io") (r "^0.2.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0jihdl0m8if88pcr7mildlmllpfxnkg3yszc7fimw24fsvlrqarx") (f (quote (("std" "ciborium/std" "serde/std") ("json_encoding") ("default" "std") ("alloc" "serde/alloc" "ciborium-io/alloc"))))))

(define-public crate-tari_bor-0.7.0 (c (n "tari_bor") (v "0.7.0") (d (list (d (n "ciborium") (r "^0.2.2") (d #t) (k 0)) (d (n "ciborium-io") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0icqh3anjm4rkj25vdiarj4lj2isx2bpdfggj716dfnz5lxxbrf5") (f (quote (("std" "ciborium/std" "serde/std") ("json_encoding") ("default" "std") ("alloc" "serde/alloc" "ciborium-io/alloc"))))))

