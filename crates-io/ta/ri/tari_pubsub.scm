(define-module (crates-io ta ri tari_pubsub) #:use-module (crates-io))

(define-public crate-tari_pubsub-0.0.6 (c (n "tari_pubsub") (v "0.0.6") (d (list (d (n "futures") (r "= 0.3.0-alpha.19") (f (quote ("async-await"))) (d #t) (k 0) (p "futures-preview")) (d (n "tari_broadcast_channel") (r "^0.0") (d #t) (k 0)))) (h "14mbd3y0knqmwj1qprbsgpf10r6j6xgmd5ml9mnra4hvl895ky5f")))

(define-public crate-tari_pubsub-0.0.7 (c (n "tari_pubsub") (v "0.0.7") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tari_broadcast_channel") (r "^0.0") (d #t) (k 0)))) (h "0kyqj41dddncray502bah8yn6bh2qh4w6f6d3dwd2fd6x8jf4wiz")))

(define-public crate-tari_pubsub-0.1.0 (c (n "tari_pubsub") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tari_broadcast_channel") (r "^0.1") (d #t) (k 0)))) (h "1vc17p5iz0k6lpi2ra8ksln9nz6kg1h8bapj14dw96m6qarblh7s")))

(define-public crate-tari_pubsub-0.2.1 (c (n "tari_pubsub") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tari_broadcast_channel") (r "^0.2") (d #t) (k 0)))) (h "19hgw7x091g6wmaw111wvgxbzqdqw5an0kn944s1x205cv5cpx0v")))

(define-public crate-tari_pubsub-0.3.0 (c (n "tari_pubsub") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "tari_broadcast_channel") (r "^0.3") (d #t) (k 0)))) (h "0m2cdd6nhmzr0mid5ld91l5ppiayfgas8h99dfal5f2bmi5jnl7i")))

