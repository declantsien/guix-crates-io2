(define-module (crates-io ta ri tariff) #:use-module (crates-io))

(define-public crate-tariff-0.1.0 (c (n "tariff") (v "0.1.0") (d (list (d (n "bson") (r "^0.1.3") (d #t) (k 0)) (d (n "docopt") (r "^0.6.70") (d #t) (k 0)) (d (n "mongodb") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0jd09sn59nz0649sdl44l9sgxbkgnwbsg6c74kxnsm8bnw5g2hm2")))

