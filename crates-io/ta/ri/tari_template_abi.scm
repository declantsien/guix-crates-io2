(define-module (crates-io ta ri tari_template_abi) #:use-module (crates-io))

(define-public crate-tari_template_abi-0.1.0 (c (n "tari_template_abi") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "tari_bor") (r "^0.1.0") (k 0)))) (h "1qafh60yx5xj24d8468g0x710047p0sazgn0k5dggdh2ififww43") (f (quote (("std" "tari_bor/std") ("default" "std") ("alloc" "hashbrown"))))))

(define-public crate-tari_template_abi-0.3.0 (c (n "tari_template_abi") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "tari_bor") (r "^0.3.0") (k 0)))) (h "00h8pqzvymdf8bdc13a11pfkjbcjby18k3bx5pqfpgs2b54x8x1y") (f (quote (("std" "tari_bor/std") ("default" "std") ("alloc" "hashbrown"))))))

(define-public crate-tari_template_abi-0.7.0 (c (n "tari_template_abi") (v "0.7.0") (d (list (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "tari_bor") (r "^0.7") (k 0)) (d (n "ts-rs") (r "^7.1") (f (quote ("chrono-impl" "no-serde-warnings" "indexmap-impl"))) (o #t) (d #t) (k 0)))) (h "0j6ras2qh3g7lc6bk45m7gkkqx2qj1g6dq1wa2gmj7dlfif650ri") (f (quote (("ts" "ts-rs") ("std" "tari_bor/std") ("default" "std") ("alloc" "hashbrown"))))))

