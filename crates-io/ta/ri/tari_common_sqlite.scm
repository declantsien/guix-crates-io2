(define-module (crates-io ta ri tari_common_sqlite) #:use-module (crates-io))

(define-public crate-tari_common_sqlite-1.0.0-rc.5 (c (n "tari_common_sqlite") (v "1.0.0-rc.5") (d (list (d (n "diesel") (r "^2.0.3") (f (quote ("sqlite" "r2d2" "serde_json" "chrono" "64-column-tables"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "tari_utilities") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("sync" "macros" "rt"))) (d #t) (k 0)))) (h "1340cclsygnapmv476fiq3x2kmysgnx05pjl787wzwvf3mlawjaf")))

