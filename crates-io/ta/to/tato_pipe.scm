(define-module (crates-io ta to tato_pipe) #:use-module (crates-io))

(define-public crate-tato_pipe-0.1.0 (c (n "tato_pipe") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "tato") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "16ldjihjxyz79973hpdrdbx2p0ila674lmjpw13wk4riia0dllwg")))

(define-public crate-tato_pipe-0.1.1 (c (n "tato_pipe") (v "0.1.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "tato") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "04ih0nzn103g6w82gql0f2p9g9ajxcxq9qp364cp0h9gxk8bspmw")))

(define-public crate-tato_pipe-0.1.2 (c (n "tato_pipe") (v "0.1.2") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "tato") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "0xkjllxf1cpj2bghcvzw3yqqwms1j8bpprqsiry98b3crs1q7sid")))

