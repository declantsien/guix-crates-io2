(define-module (crates-io ta to tato) #:use-module (crates-io))

(define-public crate-tato-0.1.0 (c (n "tato") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (k 0)))) (h "0gf03qaxfjg0a7xbcq5zqh038l0mlbjbaca8ww9i32i9njaxg4sz") (f (quote (("std"))))))

(define-public crate-tato-0.1.1 (c (n "tato") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (k 0)))) (h "0awdpqva5p1dqawqxlvq6rd63cqrp0ph34brv8fzdn9v4n8p84j2") (f (quote (("std"))))))

(define-public crate-tato-0.1.3 (c (n "tato") (v "0.1.3") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (k 0)))) (h "0cf8m7kf2xyn82aqnj5iz91bpyy7k1rc1x1w61c9bn41fvlhyy43") (f (quote (("std"))))))

(define-public crate-tato-0.1.4 (c (n "tato") (v "0.1.4") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (k 0)))) (h "1284xpcf2mixj2hmkxw1mm7g0i7zc1rlhcl3dpg2674ivsz7clqk") (f (quote (("std"))))))

