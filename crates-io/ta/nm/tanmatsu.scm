(define-module (crates-io ta nm tanmatsu) #:use-module (crates-io))

(define-public crate-tanmatsu-0.1.0 (c (n "tanmatsu") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "1nwmz95xz8isssks01i2wc8sn9j9wljh9w70wm3hrb58cg015cfr")))

(define-public crate-tanmatsu-0.1.1 (c (n "tanmatsu") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.19") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "0jzm02akfs7xk8md2ckfwmmja8g6bs5ljdc596bc902f8d25w1zg")))

(define-public crate-tanmatsu-0.2.0 (c (n "tanmatsu") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "0vl4vpgf9lvbx077b9wykr10k5v2kdjl7lswcbmr2nb3f8p2cvp6")))

(define-public crate-tanmatsu-0.2.5 (c (n "tanmatsu") (v "0.2.5") (d (list (d (n "crossterm") (r "^0.19") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "1pwj0sgjrxbbrl7lb9pjr4r800z8f47hbdmq6csk3x8f7bkg0jb3")))

(define-public crate-tanmatsu-0.3.0 (c (n "tanmatsu") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "18f8k13c4r35bqrmxyawdhrx9bcric0rz45wwv71x9j3rfa91sq0")))

(define-public crate-tanmatsu-0.4.0 (c (n "tanmatsu") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "1kzlxp5bry8hyk1ycgralwcclk7334xca9gp1dj8ldsc84awqqyq")))

(define-public crate-tanmatsu-0.5.0 (c (n "tanmatsu") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "0nnydhm4yczk2k1g94dy1kviin41760q81sxynyylmsdlmfbz8m1")))

(define-public crate-tanmatsu-0.6.0 (c (n "tanmatsu") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "1zblvfx53v8c5chdi3828jbb6jzd6l8i3rcgq5akfj7dxclfpxr9")))

(define-public crate-tanmatsu-0.6.1 (c (n "tanmatsu") (v "0.6.1") (d (list (d (n "crossterm") (r "^0.20") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "14mawqnn2f8y3z4fhy47s6qng803wck6qyxq82vfvmylndpc4psp")))

(define-public crate-tanmatsu-0.6.2 (c (n "tanmatsu") (v "0.6.2") (d (list (d (n "crossterm") (r "^0.20") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "1wck3lbx436xaprcvxpr3zk2pflpcm76ajawn8vpq65ipib9rd71")))

(define-public crate-tanmatsu-0.6.3 (c (n "tanmatsu") (v "0.6.3") (d (list (d (n "crossterm") (r "^0.20") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "16nd6n03w30k5m9szicw9xk8i4m7xf87gdkhrg7s188ma04dvg5b")))

(define-public crate-tanmatsu-0.6.4 (c (n "tanmatsu") (v "0.6.4") (d (list (d (n "crossterm") (r "^0.20") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "1lajcr67gfqi09qjdgrq236b82fvsdd48zhfwldlxx4ki5frcy3g")))

