(define-module (crates-io ta ke takeoff_cli) #:use-module (crates-io))

(define-public crate-takeoff_cli-0.1.1 (c (n "takeoff_cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "takeoff") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "1inpwswjyrlibl36b6l1w8blbndwyp5m21djalvki5q42al3dv00") (y #t)))

