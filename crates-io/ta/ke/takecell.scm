(define-module (crates-io ta ke takecell) #:use-module (crates-io))

(define-public crate-takecell-0.1.0 (c (n "takecell") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)))) (h "0qr8s2vydcpmw1rim9di93yk4pgsrs5kncmvzzj4xmq7q2rpaxyh")))

(define-public crate-takecell-0.1.1 (c (n "takecell") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)))) (h "0gpzcnazzwl6mq7xzbk9jf5zd0959h6310ls1ibapp3ccwwl7wr0")))

