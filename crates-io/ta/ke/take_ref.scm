(define-module (crates-io ta ke take_ref) #:use-module (crates-io))

(define-public crate-take_ref-0.1.0 (c (n "take_ref") (v "0.1.0") (h "0ls3zxbr240rpnk2mfiiq1sn4vlva9y3cr4lkqg4cn7cy808d4ll")))

(define-public crate-take_ref-0.2.0 (c (n "take_ref") (v "0.2.0") (h "0bkxc406jxfca8mabwjdk9r52wrqvbim0a85rclgbppa6wl70cin") (f (quote (("use_std") ("default" "use_std"))))))

