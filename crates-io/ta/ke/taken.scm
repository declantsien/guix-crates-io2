(define-module (crates-io ta ke taken) #:use-module (crates-io))

(define-public crate-taken-0.1.0 (c (n "taken") (v "0.1.0") (h "0zsphfwhs6hbakyyx8i9mlmdq8b2d8l6v18dvg0xrsn76lj461qb")))

(define-public crate-taken-0.1.1 (c (n "taken") (v "0.1.1") (h "1r3ddqc4pq67va9hav03xyp667aj8qgzhh2ywnj10gyhmbz4vrc4")))

