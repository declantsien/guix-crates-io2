(define-module (crates-io ta ke take-cell-option) #:use-module (crates-io))

(define-public crate-take-cell-option-0.1.0 (c (n "take-cell-option") (v "0.1.0") (h "0jza3jdcp6fjhcvr4nkar2wvb837s3sx4wxs0g518y3jady68np9")))

(define-public crate-take-cell-option-0.1.1 (c (n "take-cell-option") (v "0.1.1") (h "0dq4cqigylv0kfvlh4yv65nk5war68kyhyw9nmndi2dx7hmxzczv")))

(define-public crate-take-cell-option-0.1.2 (c (n "take-cell-option") (v "0.1.2") (h "1jd7q6pmgdj5a4445rlh7mbrc3679z73xzlhx3p0gf06maiahacm")))

