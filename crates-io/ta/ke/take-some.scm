(define-module (crates-io ta ke take-some) #:use-module (crates-io))

(define-public crate-take-some-0.1.0 (c (n "take-some") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 2)))) (h "1i6drbd5jrwy6sv38xzbvd4zyc74lyszsjs8iqrgr19ygm83yg7j")))

(define-public crate-take-some-0.1.1 (c (n "take-some") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 2)))) (h "13csif3gggfq33cifmc6lsnjs873n07dz9441s4idsj5xxj0w7hi")))

(define-public crate-take-some-0.1.2 (c (n "take-some") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 2)))) (h "02y09p6r1r2r1j9ayjdzwy92rgkjyzf7vp3ams9wjlgcs4h5k5l5")))

