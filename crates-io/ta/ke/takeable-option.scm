(define-module (crates-io ta ke takeable-option) #:use-module (crates-io))

(define-public crate-takeable-option-0.1.0 (c (n "takeable-option") (v "0.1.0") (h "1llccmrg920fvgrdk4lv3w3id7vl10fg5pwxn1b928knzk9ym3g5")))

(define-public crate-takeable-option-0.2.0 (c (n "takeable-option") (v "0.2.0") (h "12hnx4012ykjq1f2sabimgkpsra3sx2g4syvz5cgm1c3vvlshr8d")))

(define-public crate-takeable-option-0.3.0 (c (n "takeable-option") (v "0.3.0") (h "0g7x9vvg0pfdhccqq1y438nrg1zf9lpra1284m51gzd8rjrwaiwv")))

(define-public crate-takeable-option-0.4.0 (c (n "takeable-option") (v "0.4.0") (h "0hvd6vk4ksgg2y99498jw52ric4lxm0i6ygpzqm95gdrhvsxyynp")))

(define-public crate-takeable-option-0.5.0 (c (n "takeable-option") (v "0.5.0") (h "182axkm8pq7cynsfn65ar817mmdhayrjmbl371yqp8zyzhr8kbin")))

