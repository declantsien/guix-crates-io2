(define-module (crates-io ta ke take-breath) #:use-module (crates-io))

(define-public crate-take-breath-0.1.0 (c (n "take-breath") (v "0.1.0") (d (list (d (n "notify-rust") (r "^4.5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full" "time"))) (d #t) (k 0)) (d (n "user-idle") (r "^0.5.0") (d #t) (k 0)))) (h "1198n2s1fm5mrnf76npckcw3px50wpqkv2yl6wvsi41y82s0xn9k")))

(define-public crate-take-breath-0.1.1 (c (n "take-breath") (v "0.1.1") (d (list (d (n "notify-rust") (r "^4.5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full" "time"))) (d #t) (k 0)) (d (n "user-idle") (r "^0.5.0") (d #t) (k 0)))) (h "1sll6lzxc9jhk3zaipdp5fllp1454s42d3n7mix66bz8h2jkybgy")))

(define-public crate-take-breath-0.1.2 (c (n "take-breath") (v "0.1.2") (d (list (d (n "notify-rust") (r "^4.5.2") (d #t) (k 0)) (d (n "user-idle") (r "^0.5.0") (d #t) (k 0)))) (h "07wxy9n7r7rdbz13zij90bzbx612ywpvyih341pj5gnpxpji4m6b")))

(define-public crate-take-breath-0.1.3 (c (n "take-breath") (v "0.1.3") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "user-idle") (r "^0.5.0") (d #t) (k 0)))) (h "08jh8z8nqqss1jymp6hcq37ral10bl2g30nx7klh4iyk7jlxwb0v")))

(define-public crate-take-breath-0.1.4 (c (n "take-breath") (v "0.1.4") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "user-idle") (r "^0.5.0") (d #t) (k 0)))) (h "07h31p86wr1gmr9b5adw02vv5472dh5bg3ns0zz7dn66a6fzlwsd")))

(define-public crate-take-breath-0.1.5 (c (n "take-breath") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (o #t) (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (o #t) (d #t) (k 0)) (d (n "user-idle") (r "^0.5.0") (d #t) (k 0)))) (h "1bn57q3zmdqfjsw5ri0hnmb0qmd5q94pdhf9450cn48sg8kba45z") (f (quote (("notify" "notify-rust") ("default" "notify" "config" "cli") ("config" "serde/derive" "humantime-serde" "toml" "dirs") ("cli" "clap"))))))

