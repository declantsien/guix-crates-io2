(define-module (crates-io ta ke take-static) #:use-module (crates-io))

(define-public crate-take-static-0.1.0 (c (n "take-static") (v "0.1.0") (d (list (d (n "call-once") (r "^0.1") (d #t) (k 0)))) (h "1pibsdw8psdr1665lm3f9aq1ixbf4jw7kxc4miiq6pxl478jyypq")))

(define-public crate-take-static-0.1.1 (c (n "take-static") (v "0.1.1") (d (list (d (n "call-once") (r "^0.1") (d #t) (k 0)))) (h "01h9jvsiswa8fmsrq2ysrwvj850vxlvl8gy6p85m8s33wxvkgs6f")))

(define-public crate-take-static-0.1.2 (c (n "take-static") (v "0.1.2") (d (list (d (n "call-once") (r "^0.1") (d #t) (k 0)))) (h "00gyqi0m1igfnxh043dhi2zvv4zwp72pajz1ymfxn7wnh05fnryg")))

