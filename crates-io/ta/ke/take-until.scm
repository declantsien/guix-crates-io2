(define-module (crates-io ta ke take-until) #:use-module (crates-io))

(define-public crate-take-until-0.1.0 (c (n "take-until") (v "0.1.0") (h "0fzy344njqfgc6mi2jlji7h70d163hyd777m989qqyh6k22pvqdl")))

(define-public crate-take-until-0.2.0 (c (n "take-until") (v "0.2.0") (h "1fqbg8pk6yl03k9pr411h6fvj8ygknx42w3bwv0khyx6vyh6znwb")))

