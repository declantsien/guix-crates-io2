(define-module (crates-io ta ke take_mut) #:use-module (crates-io))

(define-public crate-take_mut-0.1.0 (c (n "take_mut") (v "0.1.0") (h "0ph5afdz81zch4nwkyql0aiawgfg5vdkavx381nlzbqz9dbhmy6p")))

(define-public crate-take_mut-0.1.1 (c (n "take_mut") (v "0.1.1") (h "17w06a8qk2kpnfk3m3r2b3vhy3ycvd1iih83nm11sakl1bdxag3i")))

(define-public crate-take_mut-0.1.2 (c (n "take_mut") (v "0.1.2") (h "192csxl45y11skqix6zd66vssjjqqcg368lmhxsw9ks5c1h1vs27") (y #t)))

(define-public crate-take_mut-0.1.3 (c (n "take_mut") (v "0.1.3") (h "02112b0sql9j016yb9p52vq0bglv70ch2z5jp3yf2x8diaqwx1kr")))

(define-public crate-take_mut-0.2.0 (c (n "take_mut") (v "0.2.0") (h "1iglhzj5g9d5fm0hj3g43n0gfg48adr0x2lfffssxx2d2yhi1fah")))

(define-public crate-take_mut-0.2.1 (c (n "take_mut") (v "0.2.1") (h "14f7n3qk0z85w5bm0p59xcmkphs5vhiyn9a1s03zvr9kqcpj94yb")))

(define-public crate-take_mut-0.2.2 (c (n "take_mut") (v "0.2.2") (h "0q2d7w6nd5bl7bay5csq065sjg8fw0jcx6hl1983cpzf25fh0r7p")))

