(define-module (crates-io ta ke takeout-metadata) #:use-module (crates-io))

(define-public crate-takeout-metadata-0.1.0 (c (n "takeout-metadata") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "08i78j0mr8i82fi9bbqdpikwy5dwhj8h23wxwgabkpsw4qigrpkl") (r "1.70")))

(define-public crate-takeout-metadata-0.1.1 (c (n "takeout-metadata") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "10nvjfi50rgqlfhdg609jpzljzqgidykc774w9d6mfnzsds2bv2i") (r "1.70")))

