(define-module (crates-io ta ke takeable) #:use-module (crates-io))

(define-public crate-takeable-0.1.0 (c (n "takeable") (v "0.1.0") (d (list (d (n "unreachable") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mr53igsjcmyg7h98dqg7jwkg9qyr5nivwqpn2h6ljc7kgfs8m25") (f (quote (("microoptimization" "unreachable") ("default" "microoptimization"))))))

(define-public crate-takeable-0.2.0 (c (n "takeable") (v "0.2.0") (h "0igcjinwdqxaswrgf3mxzfzxp2fmi3z3m3nnb0mrb8hq8i0g9b37")))

(define-public crate-takeable-0.2.1 (c (n "takeable") (v "0.2.1") (h "1ac8xmrkwfb7wkhxf6zh582yj5hxjdckbz054rwdmpwjy08fhiwm")))

(define-public crate-takeable-0.2.2 (c (n "takeable") (v "0.2.2") (h "0xwllv6z39a0wn1r01agvp82p7kc1ig8bgsf6r0j09kj2z813zky")))

