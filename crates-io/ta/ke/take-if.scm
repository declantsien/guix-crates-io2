(define-module (crates-io ta ke take-if) #:use-module (crates-io))

(define-public crate-take-if-0.1.0 (c (n "take-if") (v "0.1.0") (h "069ydrzbgs7k4194gqqzvxslniqwla7ysnjcw99sfwl9p5mwm67w")))

(define-public crate-take-if-1.0.0 (c (n "take-if") (v "1.0.0") (h "1w69z6f691ddfzf2kl00sifrp69gfvy3s1i44s9rsn1ykdfa9mcd")))

