(define-module (crates-io ta sq tasque) #:use-module (crates-io))

(define-public crate-tasque-0.0.1 (c (n "tasque") (v "0.0.1") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)))) (h "14g051pjy01nsns4xf6221ljxjkzgayyig12y79sppv9jdfjkw10")))

(define-public crate-tasque-0.1.0 (c (n "tasque") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)))) (h "1b3mmhpd8k39gyns37ks33f9rly4gqybzbnjgdiyprz78dm3kx0z")))

(define-public crate-tasque-0.1.1 (c (n "tasque") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "prometrics") (r "^0.1") (d #t) (k 0)))) (h "064z2ryg05cva840f3l6vzp20k9lnfmqkvw7vzbqdpihv4zdia06")))

(define-public crate-tasque-0.1.2 (c (n "tasque") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "prometrics") (r "^0.1") (d #t) (k 0)))) (h "0l7vb3k2sj8jwihqcqnyfan90mwyg71npyfssc3l057afa7rp8lp")))

(define-public crate-tasque-0.1.3 (c (n "tasque") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "prometrics") (r "^0.1") (d #t) (k 0)))) (h "1qjhz9n4ih6aida9ifahw4ljaqzkryfxjfhanzinhv5pwqarl5sh")))

(define-public crate-tasque-0.1.4 (c (n "tasque") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "prometrics") (r "^0.1") (d #t) (k 0)))) (h "038d98i5qkswmfvac0331528mf40xsfak5w8i3z57xwnl3c5sicf")))

(define-public crate-tasque-0.1.5 (c (n "tasque") (v "0.1.5") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "prometrics") (r "^0.1") (d #t) (k 0)))) (h "0lspflkx562jd5s19hwa10i7frdaqc4ipdpj5ivixcgbs40m029q")))

