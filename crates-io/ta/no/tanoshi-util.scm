(define-module (crates-io ta no tanoshi-util) #:use-module (crates-io))

(define-public crate-tanoshi-util-0.1.0 (c (n "tanoshi-util") (v "0.1.0") (d (list (d (n "tanoshi-lib") (r "^0.23.0") (d #t) (k 0)))) (h "188mb7yzqgkmgp1x07gaz4f7g7khkrpsj9110v7mzb025yhc9fk4")))

(define-public crate-tanoshi-util-0.1.1 (c (n "tanoshi-util") (v "0.1.1") (d (list (d (n "tanoshi-lib") (r "^0.23.1") (d #t) (k 0)))) (h "08j4y09qqsddkvkrqcnvs1pz8v70sz50dxkjc8b2bqvl3dfsjbzz")))

(define-public crate-tanoshi-util-0.1.2 (c (n "tanoshi-util") (v "0.1.2") (d (list (d (n "tanoshi-lib") (r "^0.23.3") (d #t) (k 0)))) (h "0xxcxbsssm61b93s1rln83cxiwwj3yvr7116b7v573x9xbc1fbl0")))

(define-public crate-tanoshi-util-0.2.0 (c (n "tanoshi-util") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tanoshi-lib") (r "^0.24.0") (d #t) (k 0)))) (h "19awgcdniibaw35qv8xavkmbhnv9rnjywxcjasi5478kvrzdp1lp") (f (quote (("host" "reqwest") ("__test" "reqwest"))))))

(define-public crate-tanoshi-util-0.2.1 (c (n "tanoshi-util") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tanoshi-lib") (r "^0.24.0") (d #t) (k 0)) (d (n "ureq") (r "^2") (o #t) (d #t) (k 0)))) (h "1z334lvq0sg0wgkvprfmwk0zwa9km5ax0ysgdw77gbzwf32gxp3v") (f (quote (("host" "ureq") ("__test" "ureq"))))))

(define-public crate-tanoshi-util-0.3.0 (c (n "tanoshi-util") (v "0.3.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2") (o #t) (d #t) (k 0)))) (h "0vyly4pr4mdy6l5h6wfvd1l34nzncwfay4631pkxiri0a4faj207") (f (quote (("host" "ureq") ("__test" "ureq"))))))

