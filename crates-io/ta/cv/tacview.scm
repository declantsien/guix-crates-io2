(define-module (crates-io ta cv tacview) #:use-module (crates-io))

(define-public crate-tacview-0.1.0 (c (n "tacview") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "1dq7lnv3wmhfazgvjlal0xysdjs2m6kq1kp4jpwmrsqj857i51zz")))

(define-public crate-tacview-0.1.1 (c (n "tacview") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "1a4q50s6fw4j1mcd8rx61d205hb0rzcgc6pj1zyaxw2y8nz1krkg")))

(define-public crate-tacview-0.1.2 (c (n "tacview") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "04mqx9xxmlvsya3afndxs03gsgg5dpvljmy0q795n9zzs6a3klpz")))

(define-public crate-tacview-0.1.3 (c (n "tacview") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "034zc6yn9ydm37wxb2dqn678xcny9bbka9a91pd6d4naza99frxr")))

(define-public crate-tacview-0.1.4 (c (n "tacview") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "10jxij0fiz5i1ypkrfrbhrf2sk8gg0szkyvbxnkr9d0giln7pb7l")))

(define-public crate-tacview-0.1.5 (c (n "tacview") (v "0.1.5") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "1sbda7qcljrhzjl697awc3v6gd2fgmh1iri3wwb6qslq7lf1f7sx")))

