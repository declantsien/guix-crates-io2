(define-module (crates-io ta ur tauri-app-vue-example) #:use-module (crates-io))

(define-public crate-tauri-app-vue-example-0.0.1 (c (n "tauri-app-vue-example") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (f (quote ("shell-open"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1") (d #t) (k 1)))) (h "0k3xz0vb790nwvmzgpnchhkss5i658awsjynrj3a7bw2n1xg3f4b") (f (quote (("custom-protocol" "tauri/custom-protocol"))))))

