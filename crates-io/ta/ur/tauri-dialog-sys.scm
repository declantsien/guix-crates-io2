(define-module (crates-io ta ur tauri-dialog-sys) #:use-module (crates-io))

(define-public crate-tauri-dialog-sys-0.1.0 (c (n "tauri-dialog-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "00kc2p4yzixfgzsfa71mjghq333k02plhmmw4x527h2nr683ifiy")))

(define-public crate-tauri-dialog-sys-0.1.1 (c (n "tauri-dialog-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jmhvj8cgyc3vzjylgkvd09vlm9cm9abhpxaf6q6qlilq0mnllcd")))

