(define-module (crates-io ta ur tauri-plugin-gamepad) #:use-module (crates-io))

(define-public crate-tauri-plugin-gamepad-0.0.2 (c (n "tauri-plugin-gamepad") (v "0.0.2") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "gilrs") (r "^0.10.4") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^1.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)))) (h "0ac54li7yd7sycfq50kn7jry61cihsx90vzcjz69gmmkvz65rayj") (r "1.60")))

