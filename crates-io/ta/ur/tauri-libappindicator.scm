(define-module (crates-io ta ur tauri-libappindicator) #:use-module (crates-io))

(define-public crate-tauri-libappindicator-0.1.0 (c (n "tauri-libappindicator") (v "0.1.0") (d (list (d (n "glib") (r "^0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.14") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tauri-libappindicator-sys") (r "^0.1") (d #t) (k 0)))) (h "0cxapwx7iv87k5zhqxn19k52mh0pvskn68jd7xxdi7kmxmphjhic")))

(define-public crate-tauri-libappindicator-0.1.1 (c (n "tauri-libappindicator") (v "0.1.1") (d (list (d (n "glib") (r "^0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.14") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tauri-libappindicator-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0r2z1gz12a3lrz3fmsliv6rq672mzzaca3xkwd4rc3dqh0v93wxb")))

(define-public crate-tauri-libappindicator-0.1.2 (c (n "tauri-libappindicator") (v "0.1.2") (d (list (d (n "glib") (r "^0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.14") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tauri-libappindicator-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1wlyh4xwwzqdhkqimn2rvd044iwadl9x96hmv8i125033fzmfq5g")))

