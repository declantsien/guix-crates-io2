(define-module (crates-io ta ur tauri-plugin-zippy) #:use-module (crates-io))

(define-public crate-tauri-plugin-zippy-0.0.0 (c (n "tauri-plugin-zippy") (v "0.0.0") (d (list (d (n "tauri") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.1") (f (quote ("build"))) (d #t) (k 1)) (d (n "zippy-core") (r "^0.0.0") (d #t) (k 0)))) (h "1fvjrq1ili6y5g1r1v83mg8gz6pi38mhirldnzp1xh4anjs33s7r") (y #t) (l "tauri-plugin-zippy") (r "1.76.0")))

