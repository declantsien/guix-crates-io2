(define-module (crates-io ta ur tauri-react) #:use-module (crates-io))

(define-public crate-tauri-react-0.1.0 (c (n "tauri-react") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^0.10") (d #t) (k 0)))) (h "11j7wxq3fj6i5h0gxgyn6bwlfi1phhgbfhcpvsci00ffhmb00b1l")))

