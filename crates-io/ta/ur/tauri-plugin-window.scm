(define-module (crates-io ta ur tauri-plugin-window) #:use-module (crates-io))

(define-public crate-tauri-plugin-window-2.0.0-alpha.0 (c (n "tauri-plugin-window") (v "2.0.0-alpha.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-alpha.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0029sz3a2kq0g68v4ldsmsfn7iv7w95lp4j04jg990d900dfz7r0") (f (quote (("icon-png" "tauri/icon-png") ("icon-ico" "tauri/icon-ico") ("devtools"))))))

(define-public crate-tauri-plugin-window-2.0.0-alpha.1 (c (n "tauri-plugin-window") (v "2.0.0-alpha.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-alpha.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ni2843rzmmbs8lxfqsgb72fhbmah3dviyg76ywb58g11bac3wq6") (f (quote (("icon-png" "tauri/icon-png") ("icon-ico" "tauri/icon-ico") ("devtools"))))))

(define-public crate-tauri-plugin-window-2.0.0-alpha.2 (c (n "tauri-plugin-window") (v "2.0.0-alpha.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-alpha.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0f8whjxqf9m232l5b94imvf44jr5dfra7darc2sk31n558clsv34") (f (quote (("icon-png" "tauri/icon-png") ("icon-ico" "tauri/icon-ico") ("devtools"))))))

