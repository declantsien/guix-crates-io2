(define-module (crates-io ta ur tauri-specta-macros) #:use-module (crates-io))

(define-public crate-tauri-specta-macros-0.0.2 (c (n "tauri-specta-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1ixcc43acj1ld5r4a4gvw9dsv7cg8xqqd10d32ky5q0m95lq9ryg") (y #t)))

(define-public crate-tauri-specta-macros-2.0.0-rc.1 (c (n "tauri-specta-macros") (v "2.0.0-rc.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0km8naz0h5dxmkn54fwnad1z15b2rf12ww8ac1r9q7sl9jxxkhb7")))

(define-public crate-tauri-specta-macros-2.0.0-rc.2 (c (n "tauri-specta-macros") (v "2.0.0-rc.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1pdicja84y04zx3vy3gj39sp6d9dhx76xj9yyry02h9lmyqrbi5m")))

(define-public crate-tauri-specta-macros-2.0.0-rc.3 (c (n "tauri-specta-macros") (v "2.0.0-rc.3") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "19wvkclmmldp4agywss4hz2qx6yl7ymk4xs6rfmdi08wvmkw112h")))

(define-public crate-tauri-specta-macros-2.0.0-rc.4 (c (n "tauri-specta-macros") (v "2.0.0-rc.4") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1padjf2wl6nb1zfsdpprrayphfff5ydyhk7pkri4dbp56f9wp610")))

(define-public crate-tauri-specta-macros-2.0.0-rc.5 (c (n "tauri-specta-macros") (v "2.0.0-rc.5") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "04l0k59iawb5z478ayy7qzlq6w97avvip6h2qki7fa01y85ykyg6")))

