(define-module (crates-io ta ur tauri-plugin-openurl) #:use-module (crates-io))

(define-public crate-tauri-plugin-openurl-0.1.0-beta.1 (c (n "tauri-plugin-openurl") (v "0.1.0-beta.1") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-beta.14") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.11") (f (quote ("build"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0a1d1cwxwdnbgb5czi6i02dha22dnbnyddh6a3slskd1lnq7q5ps") (l "tauri-plugin-openurl") (r "1.77")))

(define-public crate-tauri-plugin-openurl-0.1.0-beta.2 (c (n "tauri-plugin-openurl") (v "0.1.0-beta.2") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-beta.14") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.11") (f (quote ("build"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0gxiwf3ca7zwdr6w4yh3ibjq2c9jl1244c89ychjgk50slnf0qsk") (l "tauri-plugin-openurl") (r "1.77")))

(define-public crate-tauri-plugin-openurl-0.1.0-beta.3 (c (n "tauri-plugin-openurl") (v "0.1.0-beta.3") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-beta") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta") (f (quote ("build"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0q6xnxrl12g31iayrhrhz76rf5l41vbw0hg2fxzyd59bakqzinl3") (l "tauri-plugin-openurl") (r "1.77.2")))

