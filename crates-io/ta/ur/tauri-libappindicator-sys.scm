(define-module (crates-io ta ur tauri-libappindicator-sys) #:use-module (crates-io))

(define-public crate-tauri-libappindicator-sys-0.1.0 (c (n "tauri-libappindicator-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "gtk-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "117zqyrjlz8a7gzbll4dw1ahckwrhsxr0jsdlqpybf0gjbgl44f1")))

(define-public crate-tauri-libappindicator-sys-0.1.1 (c (n "tauri-libappindicator-sys") (v "0.1.1") (d (list (d (n "gtk-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0q7f35rr08hv4wkqf712qaa9lqxrr3pp42i9a718xscad0z3dvwr")))

(define-public crate-tauri-libappindicator-sys-0.1.2 (c (n "tauri-libappindicator-sys") (v "0.1.2") (d (list (d (n "gtk-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1b9xybcv3w32d4sz6c4fjia7wdnl1wvajgqysffxc9fgf5qq22n4")))

