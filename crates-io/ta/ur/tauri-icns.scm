(define-module (crates-io ta ur tauri-icns) #:use-module (crates-io))

(define-public crate-tauri-icns-0.1.0 (c (n "tauri-icns") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "png") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0c8l5f31i36v8nxyv8qng0vyx2p61r11frvapallnwj31m6ypdq3") (f (quote (("pngio" "png") ("default" "pngio"))))))

