(define-module (crates-io ta ur tauri-plugin-nosleep) #:use-module (crates-io))

(define-public crate-tauri-plugin-nosleep-0.1.0 (c (n "tauri-plugin-nosleep") (v "0.1.0") (d (list (d (n "nosleep") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^1.0.0-rc.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08v8n80cdkzr9gq2mlgcwx6s2bgjiwv9ljnmd4yvvmflmdqpnrz1") (r "1.57")))

(define-public crate-tauri-plugin-nosleep-2.0.0-beta.1 (c (n "tauri-plugin-nosleep") (v "2.0.0-beta.1") (d (list (d (n "nosleep") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-beta.5") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.3") (f (quote ("build"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z0340rkv35q1bjy8p4py56sm5bnvfw5qa3za7x09rfna7zyic2f") (l "tauri-plugin-nosleep") (r "1.70")))

