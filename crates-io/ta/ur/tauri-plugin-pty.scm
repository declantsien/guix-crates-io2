(define-module (crates-io ta ur tauri-plugin-pty) #:use-module (crates-io))

(define-public crate-tauri-plugin-pty-0.0.0 (c (n "tauri-plugin-pty") (v "0.0.0") (d (list (d (n "tauri") (r "^1.5.3") (d #t) (k 0)))) (h "13brb2r4dqg1nri6azxcx19sji1y6f6137378vxmblv2aqqv6hv7") (r "1.60")))

(define-public crate-tauri-plugin-pty-0.0.1 (c (n "tauri-plugin-pty") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tauri") (r "^1.5") (d #t) (k 0)) (d (n "winpty-rs") (r "^0.3.13") (d #t) (k 0)))) (h "0j8sgf3la3i1gskhg3457x3k0axxvdnxpz2xxj6bbj4aqdh7dz40") (r "1.60")))

(define-public crate-tauri-plugin-pty-0.0.3 (c (n "tauri-plugin-pty") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tauri") (r "^1.5") (d #t) (k 0)) (d (n "winpty-rs") (r "^0.3.13") (d #t) (k 0)))) (h "06l4l9j14r18ai97ff0mpgrljjzyjidsl64q71a6yam2ajvv1n52") (r "1.60")))

(define-public crate-tauri-plugin-pty-0.0.5 (c (n "tauri-plugin-pty") (v "0.0.5") (d (list (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tauri") (r "^1.5") (d #t) (k 0)) (d (n "winpty-rs") (r "^0.3.13") (d #t) (k 0)))) (h "15w12g865bm03ygxr49xwngn7cpmlimjk9cfcdcbqzfjqhhh5vvd") (r "1.60")))

(define-public crate-tauri-plugin-pty-0.0.6 (c (n "tauri-plugin-pty") (v "0.0.6") (d (list (d (n "portable-pty") (r "^0.8.1") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tauri") (r "^1.5.4") (d #t) (k 0)))) (h "18cqfrzgj4a45zc0llzl3h3rj2wrc9164dag56ry50yw8x0nyg1r") (r "1.60")))

(define-public crate-tauri-plugin-pty-0.0.8 (c (n "tauri-plugin-pty") (v "0.0.8") (d (list (d (n "portable-pty") (r "^0.8.1") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tauri") (r "^1.5.4") (d #t) (k 0)))) (h "1z7shhkagkqdfdl48v663g9nqkw8l0fahdcyim64cnqwyrx6k0i8") (r "1.60")))

