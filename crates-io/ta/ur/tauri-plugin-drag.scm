(define-module (crates-io ta ur tauri-plugin-drag) #:use-module (crates-io))

(define-public crate-tauri-plugin-drag-0.1.0 (c (n "tauri-plugin-drag") (v "0.1.0") (d (list (d (n "drag") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0z57cw7qi22i5zpdzrihbx2njbdiihkcn6w7zv6mwfpqgpldmmgm") (r "1.60")))

(define-public crate-tauri-plugin-drag-0.2.0 (c (n "tauri-plugin-drag") (v "0.2.0") (d (list (d (n "drag") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10r5rq10i0ipjk151qj262zx1j88hn7973xrajsbn7kbzkqqilg5") (r "1.60")))

(define-public crate-tauri-plugin-drag-0.3.0 (c (n "tauri-plugin-drag") (v "0.3.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "drag") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09nd0cq7wfkdzp3gr0hs9p8ssv8jdjb2dhdvljdx9x505309n5my") (r "1.60")))

(define-public crate-tauri-plugin-drag-0.3.1 (c (n "tauri-plugin-drag") (v "0.3.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "drag") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0z3c0gvhflvr5ri5lvdhasb5r6p05kx86yrrz3q90kr303nmlhgj") (r "1.60")))

