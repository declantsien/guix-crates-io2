(define-module (crates-io ta ur taur) #:use-module (crates-io))

(define-public crate-taur-0.1.1 (c (n "taur") (v "0.1.1") (d (list (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "raur") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0xk5dz77rsrgril81m0p7pbgw6l2zk2x8hyrfkf08383wwjkkbyv")))

(define-public crate-taur-0.1.2 (c (n "taur") (v "0.1.2") (d (list (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "raur") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "18w8ymnfgdjl9m0yi86mca2m6c3jynr7wpaps4jcyckss3fzmis1")))

(define-public crate-taur-0.1.3 (c (n "taur") (v "0.1.3") (d (list (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "raur") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0qi6ss68m8ilgdkdhgyb3if1mf442b02fp3jiiabwhyhan6giqza")))

(define-public crate-taur-0.1.4 (c (n "taur") (v "0.1.4") (d (list (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "raur") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("blocking" "macros" "rt-threaded"))) (d #t) (k 0)))) (h "1w45dw5irmfz5kr16syyip06rl7bn2lkxqk9yp6qsm0hb84cfsrr")))

(define-public crate-taur-0.1.6 (c (n "taur") (v "0.1.6") (d (list (d (n "directories") (r ">=3.0.0, <4.0.0") (d #t) (k 0)) (d (n "git2") (r ">=0.13.0, <0.14.0") (d #t) (k 0)) (d (n "raur") (r ">=4.0.0, <5.0.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.0, <2.0.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.2.0, <0.3.0") (f (quote ("blocking" "macros" "rt-threaded"))) (d #t) (k 0)))) (h "193bk487kjdn8xsghd65jwk5c22xzlv78h15rn8hdxpz35ip09cx")))

(define-public crate-taur-0.1.7 (c (n "taur") (v "0.1.7") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "help" "usage" "error-context" "wrap_help"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "raur") (r "^6.0") (d #t) (k 0)) (d (n "termion") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "120w54zs4y9qkpjdn2rmvj0xl3rdkikad55h3hb7i7q4b4j8wz69")))

(define-public crate-taur-0.2.0 (c (n "taur") (v "0.2.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive" "help" "usage" "error-context" "wrap_help"))) (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("std"))) (d #t) (k 0)) (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "raur") (r "^7.0") (d #t) (k 0)) (d (n "termion") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1lx7n0k7f3pxkzjng271kxgvw25xn5vavn0c36pndc6jvh3wfh58")))

