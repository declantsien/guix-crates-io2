(define-module (crates-io ta ur tauri-plugin-printer-sujin999) #:use-module (crates-io))

(define-public crate-tauri-plugin-printer-sujin999-1.0.10 (c (n "tauri-plugin-printer-sujin999") (v "1.0.10") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "1kyxz9kpnyfkq9d8c420d70bcgf3vwl514d5851dh48vbw5xjy05") (r "1.60")))

(define-public crate-tauri-plugin-printer-sujin999-1.0.13 (c (n "tauri-plugin-printer-sujin999") (v "1.0.13") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "1izr7zr6278b4my4x51nj08b6sj8jq18iihqjaicd30fh6d7n8qr") (r "1.60")))

(define-public crate-tauri-plugin-printer-sujin999-1.0.14 (c (n "tauri-plugin-printer-sujin999") (v "1.0.14") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "1fqs74zwkjbvpdr71fx4vhvb3s243cm8rain2lmb8nmdvhgqgwds") (r "1.60")))

(define-public crate-tauri-plugin-printer-sujin999-1.0.15 (c (n "tauri-plugin-printer-sujin999") (v "1.0.15") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "0pbppczsmw78if4a2jf0jlvqq8rd6ldnsn9hll51xl74xmjbcfhz") (r "1.60")))

