(define-module (crates-io ta ur tauri-bindgen-ts-macro) #:use-module (crates-io))

(define-public crate-tauri-bindgen-ts-macro-0.1.0-alpha02 (c (n "tauri-bindgen-ts-macro") (v "0.1.0-alpha02") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zjvjdbykfi2glmpm0m5faxlaygfpzwiz2nccib1phsyr7dd4sr0")))

