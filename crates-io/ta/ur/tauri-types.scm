(define-module (crates-io ta ur tauri-types) #:use-module (crates-io))

(define-public crate-tauri-types-0.0.1 (c (n "tauri-types") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0b1ah5774k5agq0i3r31wvq97wvma8jhwb9bsl2b463ayhshvggl")))

(define-public crate-tauri-types-0.0.2 (c (n "tauri-types") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1r4imwwrcprfj2q23fhqz4cw51c9zrylqrhik5fqqzlkjr89f5p8")))

