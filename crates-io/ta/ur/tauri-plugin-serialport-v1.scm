(define-module (crates-io ta ur tauri-plugin-serialport-v1) #:use-module (crates-io))

(define-public crate-tauri-plugin-serialport-v1-0.1.0 (c (n "tauri-plugin-serialport-v1") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serialport5") (r "^5.0") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yzz7la167nvhqql7cz1770xrk999mlaz7jkcjliqjyplsp36b8p") (r "1.76")))

