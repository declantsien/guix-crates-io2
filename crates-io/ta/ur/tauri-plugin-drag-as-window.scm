(define-module (crates-io ta ur tauri-plugin-drag-as-window) #:use-module (crates-io))

(define-public crate-tauri-plugin-drag-as-window-0.1.0 (c (n "tauri-plugin-drag-as-window") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "drag") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zblk3p9j3ycfhdw73j4w4r96cpr2mkjnq1pqpfnn1k031jbghws") (f (quote (("global-js")))) (r "1.60")))

(define-public crate-tauri-plugin-drag-as-window-0.1.1 (c (n "tauri-plugin-drag-as-window") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "drag") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0shvxs7w1w5j1y8j540jy1in8m4zzd8a2fwd0pmg0cq9xrc2lk9b") (f (quote (("global-js")))) (r "1.60")))

