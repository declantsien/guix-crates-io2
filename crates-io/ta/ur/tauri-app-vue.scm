(define-module (crates-io ta ur tauri-app-vue) #:use-module (crates-io))

(define-public crate-tauri-app-vue-1.0.0 (c (n "tauri-app-vue") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (f (quote ("shell-open"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1") (d #t) (k 1)))) (h "1ipfi2pijfjiz7yripjy607qimyxi8498sibdzq7v5bq9jy8lx0d") (f (quote (("custom-protocol" "tauri/custom-protocol"))))))

(define-public crate-tauri-app-vue-1.0.1 (c (n "tauri-app-vue") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (f (quote ("shell-open"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1") (d #t) (k 1)))) (h "1p2s44m436y29m3qrrpckrf3zahxbh6h4r728ah5x2pdcbqgzrk1") (f (quote (("custom-protocol" "tauri/custom-protocol"))))))

