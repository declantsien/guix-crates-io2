(define-module (crates-io ta ur tauri-plugin-app) #:use-module (crates-io))

(define-public crate-tauri-plugin-app-2.0.0-alpha.0 (c (n "tauri-plugin-app") (v "2.0.0-alpha.0") (d (list (d (n "tauri") (r "^2.0.0-alpha.9") (d #t) (k 0)))) (h "1spakk4a9m91l243c1li7p2dqdm2g0qp3ms8n7cbb26wbpv7k1n8")))

(define-public crate-tauri-plugin-app-2.0.0-alpha.1 (c (n "tauri-plugin-app") (v "2.0.0-alpha.1") (d (list (d (n "tauri") (r "^2.0.0-alpha.11") (d #t) (k 0)))) (h "1i6jwb65zq7r7rrz4cnl1bf1rpci102r8895fjfxd261hc78w752")))

(define-public crate-tauri-plugin-app-2.0.0-alpha.2 (c (n "tauri-plugin-app") (v "2.0.0-alpha.2") (d (list (d (n "tauri") (r "^2.0.0-alpha.12") (d #t) (k 0)))) (h "1kfw01pqi08rgci4pd3hrjv45vbw7lpwzb6fqxbnci0fzcym8pmm")))

