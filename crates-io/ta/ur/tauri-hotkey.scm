(define-module (crates-io ta ur tauri-hotkey) #:use-module (crates-io))

(define-public crate-tauri-hotkey-0.1.0 (c (n "tauri-hotkey") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "tauri-hotkey-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y7q3s9cl8s07qz69zm5cac3gbjs83x4768m32v8ga9d4arskaf2")))

(define-public crate-tauri-hotkey-0.1.1 (c (n "tauri-hotkey") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "tauri-hotkey-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bqcl6jkl06zjayj4vwjyqjabk0vwc84kxfjr1d234xbaanwgavn")))

(define-public crate-tauri-hotkey-0.1.2 (c (n "tauri-hotkey") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "tauri-hotkey-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "067ixx8kz5zjs76wr8bpncnc6s0ni4lk6r69y247qnz7308gfv0c")))

