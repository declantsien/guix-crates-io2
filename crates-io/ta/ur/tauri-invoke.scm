(define-module (crates-io ta ur tauri-invoke) #:use-module (crates-io))

(define-public crate-tauri-invoke-0.1.0 (c (n "tauri-invoke") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "1k9xwz1kb0h7qrrhw9vsjr89iqliah0x8bgr7ba7vh1izlw3vd1b")))

(define-public crate-tauri-invoke-0.1.1 (c (n "tauri-invoke") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "0azpgmis1gc59jrzsy9fqlkdh4cw1ircyqc6ranxkz5rsszrz8aa")))

(define-public crate-tauri-invoke-0.2.0 (c (n "tauri-invoke") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "1jcxijdhy3q6wqykv4yc7pr363a8aarxb4354d77p74mg8j98n0z") (f (quote (("default"))))))

