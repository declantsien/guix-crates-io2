(define-module (crates-io ta ur tauri-plugin-printer) #:use-module (crates-io))

(define-public crate-tauri-plugin-printer-0.1.0 (c (n "tauri-plugin-printer") (v "0.1.0") (d (list (d (n "tauri") (r "^1.3.0") (d #t) (k 0)))) (h "0n52x90h43mkkr3cqjsqxz0bqkw29drvswilxajrk1pnc744za24") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.1.1 (c (n "tauri-plugin-printer") (v "0.1.1") (d (list (d (n "tauri") (r "^1.3.0") (d #t) (k 0)))) (h "1dwjcxyvk9yrzzh2zig3di7l3s4maa2sc1qwf9kjk3fbl3mw4rdi") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.1.3 (c (n "tauri-plugin-printer") (v "0.1.3") (d (list (d (n "tauri") (r "^1.3.0") (d #t) (k 0)))) (h "024kxrr8z39af7m6g8np5z5x409171wpiav7wz2j9wb7yq7vvz1g") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.1.4 (c (n "tauri-plugin-printer") (v "0.1.4") (d (list (d (n "tauri") (r "^1.3.0") (d #t) (k 0)))) (h "1cjgsmbaaxcp2f34a3bjvpmn50ih31d35zzj1ibnx629gbdgrpka") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.1.5 (c (n "tauri-plugin-printer") (v "0.1.5") (d (list (d (n "tauri") (r "^1.3.0") (d #t) (k 0)))) (h "0d7xv8yf1mxvsiq0hkxdfwpfpvcd4j48lvqjij5djvhj2f5n1w18") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.2.0 (c (n "tauri-plugin-printer") (v "0.2.0") (d (list (d (n "tauri") (r "^1.3.0") (d #t) (k 0)))) (h "01d9q654l0d93rq379rjs45a73qy62i1nh6z10862hwlnmxk8b20") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.2.1 (c (n "tauri-plugin-printer") (v "0.2.1") (d (list (d (n "tauri") (r "^1.3.0") (d #t) (k 0)))) (h "0pia2xnv6q086wbwp6y3y5vlkn4i3flg6db6xkz7ndqrp745f4ky") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.2.2 (c (n "tauri-plugin-printer") (v "0.2.2") (d (list (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1.3") (d #t) (k 1)))) (h "0x3xhggh1003v11xmw5bga4k7232z14ysf854cm1f31cdi99rw2x") (f (quote (("custom-protocol" "tauri/custom-protocol")))) (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.2.3 (c (n "tauri-plugin-printer") (v "0.2.3") (d (list (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)))) (h "0df52czjkj14238cmq3arjpzv4jmnqplhwq04an7svags1d9bpiw") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.2.4 (c (n "tauri-plugin-printer") (v "0.2.4") (d (list (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)))) (h "162rp1libr9jbd7ail9j4knrprqh7ihflk6h5g4yvi03jxrwxd25") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.2.5 (c (n "tauri-plugin-printer") (v "0.2.5") (d (list (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)))) (h "03qr8v2m42c5bhm5cndz3y4wp6waq7lzcw7b7qm8jm2sxpxxhk2r") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.2.6 (c (n "tauri-plugin-printer") (v "0.2.6") (d (list (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)))) (h "086g8ybksrzjy7hr1g8bkafv881ra6ljrhb5mhmj3bwjddy6blmi") (y #t) (r "1.60")))

(define-public crate-tauri-plugin-printer-0.2.7 (c (n "tauri-plugin-printer") (v "0.2.7") (d (list (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)))) (h "0g010qlczgrm3siffnwjchbdkxlgy4dw8iywini587zkbrgn1xfz") (r "1.60")))

(define-public crate-tauri-plugin-printer-0.3.0 (c (n "tauri-plugin-printer") (v "0.3.0") (d (list (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)))) (h "0pbkkd8s9l78zxbl5b3zjy1k6n5cjmi96n48d6arzhs75ka2dizv") (r "1.60")))

(define-public crate-tauri-plugin-printer-0.4.0 (c (n "tauri-plugin-printer") (v "0.4.0") (d (list (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)))) (h "00gw64z2h1w2h0i2grbm451fvs7m455rpwc77mffp7l14dy3ba7f") (r "1.60")))

(define-public crate-tauri-plugin-printer-0.4.1 (c (n "tauri-plugin-printer") (v "0.4.1") (d (list (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)))) (h "1n07g6kscqfgm3w66alwxmninshh2mp5k0pl8i00fynjcjb3hr8h") (r "1.60")))

(define-public crate-tauri-plugin-printer-0.5.0 (c (n "tauri-plugin-printer") (v "0.5.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("dialog-all" "shell-open"))) (d #t) (k 0)))) (h "1hpmi3p8i1fl95g85r18vvrhppscc59k6rf2aa30956wpnjxz88k") (r "1.60")))

(define-public crate-tauri-plugin-printer-0.5.1 (c (n "tauri-plugin-printer") (v "0.5.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all"))) (d #t) (k 0)))) (h "17brimlyhxlxzsp4d2wn735v72hs0wmpixfr4m9sxivgjfzzg7sn") (r "1.60")))

(define-public crate-tauri-plugin-printer-0.5.2 (c (n "tauri-plugin-printer") (v "0.5.2") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all"))) (d #t) (k 0)))) (h "0sbi558fabdagghhkqbcy27dzdg05lhwf82mh5fiir1vmjm500gq") (r "1.60")))

(define-public crate-tauri-plugin-printer-1.0.2 (c (n "tauri-plugin-printer") (v "1.0.2") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "1y63wfs9jj8gvwnsirpxy447z53dxvgm58nm4p6dpmxbn5f2qqxw") (r "1.60")))

(define-public crate-tauri-plugin-printer-1.0.3 (c (n "tauri-plugin-printer") (v "1.0.3") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "0hx16p83dr1d4ly7gz77i1hwmxdjfnyn06kg3pb5xx4lx05ayqy2") (r "1.60")))

(define-public crate-tauri-plugin-printer-1.0.5 (c (n "tauri-plugin-printer") (v "1.0.5") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "1q5fz0v7cc8jyj4859r93lr2gaajl7yy29symngr2661mysw35cq") (r "1.60")))

(define-public crate-tauri-plugin-printer-1.0.6 (c (n "tauri-plugin-printer") (v "1.0.6") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "1q0ka285q9bmssfxq570k62fcf8mlswkywcz0d5ffj1qi4z8sw1v") (r "1.60")))

(define-public crate-tauri-plugin-printer-1.0.7 (c (n "tauri-plugin-printer") (v "1.0.7") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "0k3q6dfp2pil00wizwryir5hz4xa12zzhgp1l6v17g6x0s4v8i14") (r "1.60")))

(define-public crate-tauri-plugin-printer-1.0.9 (c (n "tauri-plugin-printer") (v "1.0.9") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "1xv7lnp12irqgj7w95daaj4cffmbxr1rk9x6395x2hdfrphr5r0s") (r "1.60")))

(define-public crate-tauri-plugin-printer-1.0.10 (c (n "tauri-plugin-printer") (v "1.0.10") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "tauri") (r "^1.3") (f (quote ("shell-all" "window-create" "window-close" "window-data-url" "http-all"))) (d #t) (k 0)))) (h "0zbp9bqg6hadqskyqmf3jyf9kn4hipfjrn52yrh6khllw6xvc4ic") (r "1.60")))

