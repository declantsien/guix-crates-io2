(define-module (crates-io ta ur tauri-plugin-system-info) #:use-module (crates-io))

(define-public crate-tauri-plugin-system-info-0.1.0 (c (n "tauri-plugin-system-info") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "starship-battery") (r "^0.8.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.8") (d #t) (k 0)) (d (n "tauri") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.35.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ngaqf8dlipbdp15infbff32wpbwz95jni3n2zpwfkknk4zslbk2") (r "1.60")))

(define-public crate-tauri-plugin-system-info-0.1.1 (c (n "tauri-plugin-system-info") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "starship-battery") (r "^0.8.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.8") (d #t) (k 0)) (d (n "tauri") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.35.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0r7va63clgvjdhsngv07w5iddrzmz9h6g3bvmi7sn4xrxvv04g5i") (r "1.60")))

(define-public crate-tauri-plugin-system-info-0.2.0 (c (n "tauri-plugin-system-info") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "starship-battery") (r "^0.8.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)) (d (n "tauri") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.35.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0fii7zbc4mi4jzi8z5wnj281s0dwf28lf37pcj5d1kc0jsqmf16f") (r "1.60")))

