(define-module (crates-io ta ur tauriless) #:use-module (crates-io))

(define-public crate-tauriless-0.2.0 (c (n "tauriless") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tauriless_common") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_macro") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_serde") (r "^0.2") (d #t) (k 0)) (d (n "wry") (r "^0.37.0") (d #t) (k 0)))) (h "0gl53nfb1sq6ag8811zf3mxwm1yljijiq1a46fy15rx1c7l634gy")))

(define-public crate-tauriless-0.2.1 (c (n "tauriless") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tauriless_common") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_macro") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_serde") (r "^0.2") (d #t) (k 0)) (d (n "wry") (r "^0.37.0") (d #t) (k 0)))) (h "1383rx6yk9zf7sc7kdha7kpi1jrbgh1nzd1a9yww489jas9rkpr2")))

(define-public crate-tauriless-0.2.2 (c (n "tauriless") (v "0.2.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauriless_common") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_macro") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_serde") (r "^0.2") (d #t) (k 0)) (d (n "wry") (r "^0") (d #t) (k 0)))) (h "0hvwagd3gra1q437r3ikm741gg5vlvw4sh9lh8zgv93n0450is4i")))

(define-public crate-tauriless-0.2.3 (c (n "tauriless") (v "0.2.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauriless_common") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_macro") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_serde") (r "^0.2") (d #t) (k 0)) (d (n "wry") (r "^0") (d #t) (k 0)))) (h "0jbm603yy9k73j367bviqs1bk1wgy1lkfpzqdzhavxcv3cwlrgcx")))

(define-public crate-tauriless-0.2.4 (c (n "tauriless") (v "0.2.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauriless_common") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_macro") (r "^0.2") (d #t) (k 0)) (d (n "tauriless_serde") (r "^0.2") (d #t) (k 0)) (d (n "wry") (r "^0") (d #t) (k 0)))) (h "16mp8knngidzf7dx9ihhhqxqcf0vfj5nhpgc7jw2wqw4n4n7pa2p")))

