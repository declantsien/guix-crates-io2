(define-module (crates-io ta ur tauriless_common) #:use-module (crates-io))

(define-public crate-tauriless_common-0.1.0 (c (n "tauriless_common") (v "0.1.0") (h "16b4yf4ws8k7yy3r3p3z5m53l7spsdmi7rfshwghx6yxnkxf4jm9")))

(define-public crate-tauriless_common-0.2.0 (c (n "tauriless_common") (v "0.2.0") (h "1r9i6r6bbf8msdlxb8agih0r35r5xfs0zzwwm35d4p0kxvw108rp")))

