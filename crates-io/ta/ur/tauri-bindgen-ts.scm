(define-module (crates-io ta ur tauri-bindgen-ts) #:use-module (crates-io))

(define-public crate-tauri-bindgen-ts-0.1.0-alpha01 (c (n "tauri-bindgen-ts") (v "0.1.0-alpha01") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "ts-rs") (r "^6.2.0") (d #t) (k 0)))) (h "10bcxgxgzms06rj870krbb8lnvqpy51gy1v657gik711j7hvjy52")))

(define-public crate-tauri-bindgen-ts-0.1.0-alpha02 (c (n "tauri-bindgen-ts") (v "0.1.0-alpha02") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tauri-bindgen-ts-macro") (r "^0.1.0-alpha02") (d #t) (k 0)) (d (n "ts-rs") (r "^6.2.0") (d #t) (k 0)))) (h "1gic54qbxv7m2qni117wq004ibvd28kj7796wx10kiwwb1d5nyk6")))

