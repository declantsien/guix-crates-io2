(define-module (crates-io ta ur tauri-plugin-open) #:use-module (crates-io))

(define-public crate-tauri-plugin-open-0.1.0 (c (n "tauri-plugin-open") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-alpha.10") (d #t) (k 0)) (d (n "tauri-build") (r "^2.0.0-alpha.6") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "webkit2gtk") (r "^0.19.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "026la6893rj19pr10jkxbb35lgagdrawc7sswihjwxxmcbsqb4d2") (l "tauri-plugin-open") (r "1.65")))

(define-public crate-tauri-plugin-open-0.1.1 (c (n "tauri-plugin-open") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-alpha.14") (d #t) (k 0)) (d (n "tauri-build") (r "^2.0.0-alpha.8") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "webkit2gtk") (r "^1.1.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1pzxw8953dwadq5paiyamc61vhqgb31iiyv12d6bdg0d82pa39dj") (l "tauri-plugin-open") (r "1.65")))

(define-public crate-tauri-plugin-open-0.1.2 (c (n "tauri-plugin-open") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-alpha.15") (d #t) (k 0)) (d (n "tauri-build") (r "^2.0.0-alpha.9") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13mks4mfgq2cyk4agz3i0cgnjdmha1pchvmx7ja400a8cl0q9zgd") (l "tauri-plugin-open") (r "1.65")))

