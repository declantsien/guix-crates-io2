(define-module (crates-io ta ur tauri-hotkey-sys) #:use-module (crates-io))

(define-public crate-tauri-hotkey-sys-0.1.0 (c (n "tauri-hotkey-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1kdw3gwjs4qcggm6kzlzxvw3gga23sdbgrhr37ia3hz87b603vjx")))

(define-public crate-tauri-hotkey-sys-0.1.1 (c (n "tauri-hotkey-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0grglv58g0gy0i9adb3d8959dpqphlz96bkdj0mpd0vgjbawsz8p")))

(define-public crate-tauri-hotkey-sys-0.1.2 (c (n "tauri-hotkey-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1mzy2phjzc6mp3pqy7z2lylaxlwxvyrhpjrbb79fyz0p0r0ia93h")))

