(define-module (crates-io ta ur tauri-named-invoke) #:use-module (crates-io))

(define-public crate-tauri-named-invoke-1.0.0 (c (n "tauri-named-invoke") (v "1.0.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1zm01m1vpsppiqj1g66y78vcvccnf70gw14biv6s8m7rhzpl8x93") (y #t)))

(define-public crate-tauri-named-invoke-1.0.1 (c (n "tauri-named-invoke") (v "1.0.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "15gyaq90c4hp4bp9vqaaaqyj7vf48nhqjfzg80bh4gnirrqh5l3a") (y #t)))

(define-public crate-tauri-named-invoke-1.0.2 (c (n "tauri-named-invoke") (v "1.0.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0c6l4da0dxl2ljw37y7nww05ris7sxwjvg6g67ia09wg87nnrrn3") (y #t)))

(define-public crate-tauri-named-invoke-1.0.3 (c (n "tauri-named-invoke") (v "1.0.3") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0b9fkymcny8zpw1xppvnvfw2lyp33dgyhh585km5wv8z6yr5kzhy") (y #t)))

(define-public crate-tauri-named-invoke-1.0.4 (c (n "tauri-named-invoke") (v "1.0.4") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0vknrcd8af4l4azr9fmrx4ir4hajbbf1925pfp3ld1031ydblqyq")))

