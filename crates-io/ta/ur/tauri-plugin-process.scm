(define-module (crates-io ta ur tauri-plugin-process) #:use-module (crates-io))

(define-public crate-tauri-plugin-process-2.0.0-alpha.0 (c (n "tauri-plugin-process") (v "2.0.0-alpha.0") (d (list (d (n "tauri") (r "^2.0.0-alpha.9") (d #t) (k 0)))) (h "0m5iw8x8q6dp8259z764znzyg7bkhflys2i9z36fclzdjz2pa9r5")))

(define-public crate-tauri-plugin-process-2.0.0-alpha.1 (c (n "tauri-plugin-process") (v "2.0.0-alpha.1") (d (list (d (n "tauri") (r "^2.0.0-alpha.11") (d #t) (k 0)))) (h "1nq4aphrxg5qqxqjy0rgjcd8sy1g249j5m7w8b0jxskh0h3f7w5g")))

(define-public crate-tauri-plugin-process-2.0.0-alpha.2 (c (n "tauri-plugin-process") (v "2.0.0-alpha.2") (d (list (d (n "tauri") (r "^2.0.0-alpha.12") (d #t) (k 0)))) (h "06lnlbn47kn6fgl1bcwfsninmdlm7m3walx80a6ac0l3znh7qi3w")))

(define-public crate-tauri-plugin-process-2.0.0-alpha.3 (c (n "tauri-plugin-process") (v "2.0.0-alpha.3") (d (list (d (n "tauri") (r "^2.0.0-alpha.16") (d #t) (k 0)))) (h "1n4kjbs6w0hxz83v4p0zq3lgxya0g825ibd721l2xz51hr5n1idn") (r "1.70")))

(define-public crate-tauri-plugin-process-2.0.0-alpha.4 (c (n "tauri-plugin-process") (v "2.0.0-alpha.4") (d (list (d (n "tauri") (r "^2.0.0-alpha.17") (d #t) (k 0)))) (h "0xfcc1y347smh73a9allzlcyg75i5vk4ppw5qfcyr5liiyd46d3r") (r "1.70")))

(define-public crate-tauri-plugin-process-2.0.0-alpha.5 (c (n "tauri-plugin-process") (v "2.0.0-alpha.5") (d (list (d (n "tauri") (r "^2.0.0-alpha.18") (d #t) (k 0)))) (h "108wq5crcqb2l9a53z6crr7pv55jlgd28agn7hpbyilsnp7wazp2") (r "1.70")))

(define-public crate-tauri-plugin-process-2.0.0-alpha.6 (c (n "tauri-plugin-process") (v "2.0.0-alpha.6") (d (list (d (n "tauri") (r "^2.0.0-alpha.20") (d #t) (k 0)))) (h "1cv01c73cl301kvkaiwklj9fab7sghiaj76mabkkvg1722f18p5m") (r "1.70")))

(define-public crate-tauri-plugin-process-2.0.0-beta.0 (c (n "tauri-plugin-process") (v "2.0.0-beta.0") (d (list (d (n "tauri") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.0") (f (quote ("build"))) (d #t) (k 1)))) (h "051l3jkpjhk5dz7dsxxrr0975f9i50vnd7p1fyrqfycbn3b585zc") (l "tauri-plugin-process") (r "1.70")))

(define-public crate-tauri-plugin-process-2.0.0-beta.1 (c (n "tauri-plugin-process") (v "2.0.0-beta.1") (d (list (d (n "tauri") (r "^2.0.0-beta.4") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.3") (f (quote ("build"))) (d #t) (k 1)))) (h "1d17hpsnwcmjcsx9glhfa1g8511zyzi5ba7pmj0hdf8jir6p4k9j") (l "tauri-plugin-process") (r "1.75")))

(define-public crate-tauri-plugin-process-2.0.0-beta.2 (c (n "tauri-plugin-process") (v "2.0.0-beta.2") (d (list (d (n "tauri") (r "^2.0.0-beta.9") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.7") (f (quote ("build"))) (d #t) (k 1)))) (h "07fhvjl33rdpqjl0cv1a07mpzkql84b2zg8vjhrr30nfzjlakqx0") (l "tauri-plugin-process") (r "1.75")))

(define-public crate-tauri-plugin-process-2.0.0-beta.3 (c (n "tauri-plugin-process") (v "2.0.0-beta.3") (d (list (d (n "tauri") (r "^2.0.0-beta.12") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.10") (f (quote ("build"))) (d #t) (k 1)))) (h "00agglxmw3jc2j8l771ysk0qnl8zn3j1ld1pgzlr14192lv5q88i") (l "tauri-plugin-process") (r "1.75")))

(define-public crate-tauri-plugin-process-2.0.0-beta.4 (c (n "tauri-plugin-process") (v "2.0.0-beta.4") (d (list (d (n "tauri") (r "^2.0.0-beta.17") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.13") (f (quote ("build"))) (d #t) (k 1)))) (h "1bsvdld6bdnzfw4w1x3yvwam5vyhrl9r9gkcm4zjycwm6jy0hsjx") (l "tauri-plugin-process") (r "1.75")))

