(define-module (crates-io ta ur tauri-plugin-oauth) #:use-module (crates-io))

(define-public crate-tauri-plugin-oauth-0.0.0-alpha.0 (c (n "tauri-plugin-oauth") (v "0.0.0-alpha.0") (d (list (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "04k9j0kl1gpq7mk4hszajjl5c9wah9285xly88dr6g40j5b47y6x") (r "1.64")))

