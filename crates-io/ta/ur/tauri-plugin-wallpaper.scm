(define-module (crates-io ta ur tauri-plugin-wallpaper) #:use-module (crates-io))

(define-public crate-tauri-plugin-wallpaper-0.0.1 (c (n "tauri-plugin-wallpaper") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tauri") (r "^1.2.4") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "08syi1n1lz7bmh4x6gfiyvszjnkc1mgisvcyy2lb2sc2591vh0zf") (r "1.68")))

