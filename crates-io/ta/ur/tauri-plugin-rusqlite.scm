(define-module (crates-io ta ur tauri-plugin-rusqlite) #:use-module (crates-io))

(define-public crate-tauri-plugin-rusqlite-0.4.1 (c (n "tauri-plugin-rusqlite") (v "0.4.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tauri") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fxiv1y51jhq8l6c8k1mayxd0aidwd8dzjn9ki94157jn8x9s0kj") (r "1.60")))

(define-public crate-tauri-plugin-rusqlite-0.4.2 (c (n "tauri-plugin-rusqlite") (v "0.4.2") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tauri") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pprw2xjm6wj8ikiylwh5v92dm39jqgms2bf5q0g9wp9qwahskdd") (r "1.60")))

(define-public crate-tauri-plugin-rusqlite-0.4.4 (c (n "tauri-plugin-rusqlite") (v "0.4.4") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tauri") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x5rb42k33wbb16dlgqxzjll37zjrx9b8h74scrdvvalblvbvjq5") (r "1.60")))

