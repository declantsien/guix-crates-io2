(define-module (crates-io ta ur tauri_includedir) #:use-module (crates-io))

(define-public crate-tauri_includedir-0.5.0 (c (n "tauri_includedir") (v "0.5.0") (d (list (d (n "flate2") (r "^1.0.13") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "0m0g2hla6jkfzp430y7k475mlrmfq0k4k87726xqykavz5mdggz9") (f (quote (("default" "flate2"))))))

(define-public crate-tauri_includedir-0.6.0 (c (n "tauri_includedir") (v "0.6.0") (d (list (d (n "flate2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "0chh79cf9x8hj5dy19ka887d5q26bi6kirpn5ks064mi212n98y7") (f (quote (("default" "flate2"))))))

(define-public crate-tauri_includedir-0.6.1 (c (n "tauri_includedir") (v "0.6.1") (d (list (d (n "flate2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "120qknwk8hkk3dxgc5jc3b4v6wwp5rjl1f0cd159m7w0h84cy6v4") (f (quote (("default" "flate2"))))))

