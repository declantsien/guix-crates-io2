(define-module (crates-io ta ur tauri-winres) #:use-module (crates-io))

(define-public crate-tauri-winres-0.1.0 (c (n "tauri-winres") (v "0.1.0") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "1n7sjkb55ldg45y9xwj0iab2msc13ibaqrjyh6mvapzp0kf7hyhv")))

(define-public crate-tauri-winres-0.1.1 (c (n "tauri-winres") (v "0.1.1") (d (list (d (n "embed-resource") (r "^2.1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "1yy4pxgqg6kxpx7py624yvrmgj27qhg3v4l889br6hslkq9dr4sr")))

