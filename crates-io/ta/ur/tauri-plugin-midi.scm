(define-module (crates-io ta ur tauri-plugin-midi) #:use-module (crates-io))

(define-public crate-tauri-plugin-midi-0.0.0 (c (n "tauri-plugin-midi") (v "0.0.0") (d (list (d (n "coremidi-hotplug-notification") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "midir") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "specta") (r "=2.0.0-rc.7") (d #t) (k 0)) (d (n "tauri") (r "^1.5.4") (d #t) (k 0)) (d (n "tauri-specta") (r "=2.0.0-rc.4") (f (quote ("javascript" "typescript"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("time"))) (d #t) (k 0)))) (h "084c9dqy77aa9pwsc17v0jk2nmg26qnyaap5n4d3s3yr19gj6znp") (r "1.60")))

