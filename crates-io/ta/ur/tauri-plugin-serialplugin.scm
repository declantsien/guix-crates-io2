(define-module (crates-io ta ur tauri-plugin-serialplugin) #:use-module (crates-io))

(define-public crate-tauri-plugin-serialplugin-2.0.0-beta.0 (c (n "tauri-plugin-serialplugin") (v "2.0.0-beta.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.0") (f (quote ("build"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0x125hsw5qqp37xh0y8xkrh1sa09ijj3k6qmirks3q7kflx3dkq2") (l "tauri-plugin-serialplugin") (r "1.70")))

(define-public crate-tauri-plugin-serialplugin-2.0.0-beta.1 (c (n "tauri-plugin-serialplugin") (v "2.0.0-beta.1") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)) (d (n "tauri") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "tauri-plugin") (r "^2.0.0-beta.0") (f (quote ("build"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1imw0k7cgnnxqqdb5cc0ff5cn4x2qc0vqk80hwk4c59l9mqw34i8") (l "tauri-plugin-serialplugin") (r "1.70")))

