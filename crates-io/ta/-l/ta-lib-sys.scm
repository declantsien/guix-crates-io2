(define-module (crates-io ta -l ta-lib-sys) #:use-module (crates-io))

(define-public crate-ta-lib-sys-0.1.0 (c (n "ta-lib-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "00a5p7hcww4wgrxlyjghi2khprcwbp0zhmllgqr3xwfa605idwza") (f (quote (("use_system_lib"))))))

(define-public crate-ta-lib-sys-0.1.1 (c (n "ta-lib-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0k8crn2q8ra85byajrlipmrkxdal8g7hgf252iwyk8zdy6zhxzyd") (f (quote (("use_system_lib"))))))

(define-public crate-ta-lib-sys-0.1.2 (c (n "ta-lib-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "0s4a60nmvcbffz8a9jrpz3zzcm2mq38ps1axml4vvdmyc63mz4w1") (f (quote (("use_system_lib"))))))

