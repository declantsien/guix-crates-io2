(define-module (crates-io ta -l ta-lib-wrapper) #:use-module (crates-io))

(define-public crate-ta-lib-wrapper-0.1.0 (c (n "ta-lib-wrapper") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "1nnz457745wkayndnanprhg4mw5797pb198vzs0bg1rf2zqinsvc")))

(define-public crate-ta-lib-wrapper-0.1.1 (c (n "ta-lib-wrapper") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "00yhklmndg0x9z5mk66cjixfg6y7pphh9x6rnbylz66fgnz4qj0k")))

(define-public crate-ta-lib-wrapper-0.2.0 (c (n "ta-lib-wrapper") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "19wg6yqmk03rkkpip8lmj9fvr932s2jw5vxwgqx4a25mi9fxwgmf")))

