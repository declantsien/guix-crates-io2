(define-module (crates-io ta -l ta-lib) #:use-module (crates-io))

(define-public crate-ta-lib-0.1.0 (c (n "ta-lib") (v "0.1.0") (d (list (d (n "concat-idents") (r "^1.1.3") (d #t) (k 0)) (d (n "ta-lib-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0q5g49xvsa6288gmp6zrjnz0avmsvfkd54j6ac2asm4f9wsrv9rj") (f (quote (("use_system_lib" "ta-lib-sys/use_system_lib"))))))

(define-public crate-ta-lib-0.1.1 (c (n "ta-lib") (v "0.1.1") (d (list (d (n "concat-idents") (r "^1.1.3") (d #t) (k 0)) (d (n "ta-lib-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0zcpg1rqhb4dyw40aapabvm1dyw5r5d4sabha8s1jad3kvb3yqb3") (f (quote (("use_system_lib" "ta-lib-sys/use_system_lib"))))))

(define-public crate-ta-lib-0.1.2 (c (n "ta-lib") (v "0.1.2") (d (list (d (n "concat-idents") (r "^1.1.4") (d #t) (k 0)) (d (n "ta-lib-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0njkdg2ay8rfjvsagwx86pchbza2nkqafx3gqyqa9wlnjq1p80ps") (f (quote (("use_system_lib" "ta-lib-sys/use_system_lib"))))))

