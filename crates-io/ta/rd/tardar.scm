(define-module (crates-io ta rd tardar) #:use-module (crates-io))

(define-public crate-tardar-0.0.0 (c (n "tardar") (v "0.0.0") (d (list (d (n "miette") (r "^5.1.0") (k 0)) (d (n "vec1") (r "^1.8.0") (k 0)))) (h "11qi6rk9pny13mj945k49650xnhh7lq1vf6j8a46mvcs100gjdc9") (r "1.56.0")))

(define-public crate-tardar-0.1.0 (c (n "tardar") (v "0.1.0") (d (list (d (n "miette") (r "^5.10.0") (k 0)) (d (n "vec1") (r "^1.10.0") (k 0)))) (h "05imkznxr59dqp0s37i7qkrpyjx2zz9lmxy8ijcvka5nhcpr834h") (r "1.57.0")))

