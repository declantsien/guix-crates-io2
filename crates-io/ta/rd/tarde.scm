(define-module (crates-io ta rd tarde) #:use-module (crates-io))

(define-public crate-tarde-0.1.0 (c (n "tarde") (v "0.1.0") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "1pdhfchac9gazbgj8cn2zh05x12llg2df1iwx6wx857jdpj9b5s3")))

(define-public crate-tarde-0.1.1 (c (n "tarde") (v "0.1.1") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "1l6h0krvsn2xh8jwk7qg3k2mlr32wcmlxbmx215r5rflx5is96sl")))

(define-public crate-tarde-0.1.2 (c (n "tarde") (v "0.1.2") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "191w1hawjagv7qkzir2z8k63k17mzs9cvrhbvyy3g97hivkdd44a")))

(define-public crate-tarde-0.2.0 (c (n "tarde") (v "0.2.0") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "1bww2yh1y8xp2i5i8rkphwp33d58miq9l1xyybi2mxqs1gx9nm15")))

