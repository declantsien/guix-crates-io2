(define-module (crates-io ta tk tatk) #:use-module (crates-io))

(define-public crate-tatk-0.2.0 (c (n "tatk") (v "0.2.0") (h "0kmlkx3nr4sg0yc75y749np74c5nh1idnm9ql27qgl55g4klrn19") (f (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

(define-public crate-tatk-0.2.1 (c (n "tatk") (v "0.2.1") (h "1al2w3rv6g0dckra4b72rk8ip1q6i6l87qvsqaqsi9r09wzz8jq8") (f (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

(define-public crate-tatk-0.2.3 (c (n "tatk") (v "0.2.3") (h "17hxpwmxvw1l75dqvvlhnz8sb6kd01jn4ylakw4k2bqh6084jh44") (f (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

(define-public crate-tatk-0.2.4 (c (n "tatk") (v "0.2.4") (h "168fiflbhhgbhnzhym8c8cnzw3f5j3pr0zlglk0nz9h5w667w9fj") (f (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

(define-public crate-tatk-0.3.0 (c (n "tatk") (v "0.3.0") (d (list (d (n "tatk_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0qy5gwr8rhhxx1l3grz5c2fjmpwry22sjy1vaq157kvzqz2h2iph") (f (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

