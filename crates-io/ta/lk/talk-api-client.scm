(define-module (crates-io ta lk talk-api-client) #:use-module (crates-io))

(define-public crate-talk-api-client-0.2.0 (c (n "talk-api-client") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.3.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0qim67vfsffi2wyqvgiy9m92j9kf0hsbzn6gqiydv0bff21ihy7g") (y #t)))

