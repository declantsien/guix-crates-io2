(define-module (crates-io ta lk talk-timer) #:use-module (crates-io))

(define-public crate-talk-timer-0.1.0 (c (n "talk-timer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "16l2q8k458d7r3vsb9w6a91r2p0x1xka8mqg4qallywvqkkg804b")))

(define-public crate-talk-timer-0.2.0 (c (n "talk-timer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0fz48kq4sp0zb9zr6631vghicvy6ysm4vmfaj8zlgdrybwn9k5qj")))

(define-public crate-talk-timer-0.2.1 (c (n "talk-timer") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "yansi") (r "^1.0.1") (d #t) (k 0)))) (h "0mnvaxsrjkcylhddkflnhkrzwpk6d3lz21ykvkq62sxhjhhm16a6")))

