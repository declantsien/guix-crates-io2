(define-module (crates-io ta lk talk-loco-commands) #:use-module (crates-io))

(define-public crate-talk-loco-commands-0.1.0 (c (n "talk-loco-commands") (v "0.1.0") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "loco-protocol") (r "^1.0.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "talk-loco-commands-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1jm8nzlmyx8inpaa4ww2m9xv96s0sw59c5zzi7yw50i0mk4wp74y") (y #t)))

(define-public crate-talk-loco-commands-0.2.0 (c (n "talk-loco-commands") (v "0.2.0") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "loco-protocol") (r "^1.0.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "talk-loco-commands-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0d4a5nn49yfl5dq8j8c387764qdxwv6iawbs5v9h3l43klrmcnj7") (y #t)))

