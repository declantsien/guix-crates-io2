(define-module (crates-io ta os taos-macros) #:use-module (crates-io))

(define-public crate-taos-macros-0.2.0 (c (n "taos-macros") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "16zznp7m36miv2amym375zy0rrdswmv867jn7sjjhcd405zpbhmj")))

(define-public crate-taos-macros-0.2.1 (c (n "taos-macros") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1xcaxql6rvkcq1m54zj5hgxlxmv2fwwkqn6klhv169bc6kchjic9")))

(define-public crate-taos-macros-0.2.2 (c (n "taos-macros") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1s4cxrr23j0c3g3jfhfmvi2skl64nandpdp98jns0j2v964ii11d")))

(define-public crate-taos-macros-0.2.3 (c (n "taos-macros") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1wyqdwq9x1hzhni1gzszfr148nyw971vwld3x78kaw6s1l99wmnw")))

(define-public crate-taos-macros-0.2.4 (c (n "taos-macros") (v "0.2.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1zifymm679cmczmqmwpmrd1kvqhmq3wqm8gg4b5321iim5h2rzr1")))

(define-public crate-taos-macros-0.2.5 (c (n "taos-macros") (v "0.2.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "01w5pi13kzbsglmgwvvf78dz7nda545singbvv076j8daqpz9fjm")))

(define-public crate-taos-macros-0.2.6 (c (n "taos-macros") (v "0.2.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1k3pvzalqkhs1ndjg10s78xv13ka4mp84p4wazs15kh052nnkk8x")))

(define-public crate-taos-macros-0.2.7 (c (n "taos-macros") (v "0.2.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "0yykl6qa1dvp73wrj3ncpv6y38vk98lmj76qyiqn8kmlj7nciz65")))

(define-public crate-taos-macros-0.2.8 (c (n "taos-macros") (v "0.2.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1j4fpcjffd9vlvjvskza3rd9kw3bmnzplnrp5y6nby50dqh3p2vk")))

(define-public crate-taos-macros-0.2.9 (c (n "taos-macros") (v "0.2.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "0bw9qks2pwy0qnf0lwpnglm87r1zl736l2wmgznnc6zs9ipmgpjc")))

(define-public crate-taos-macros-0.2.10 (c (n "taos-macros") (v "0.2.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1jmrc7z747n0sqpxmx06py7nfmk68z5vwfqn491l43n1k34v2msk")))

(define-public crate-taos-macros-0.2.11 (c (n "taos-macros") (v "0.2.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1dqqg239q2f6q6pwj2k2c4lxpk149dj6j3sph9d7r537gkxb7n2l")))

(define-public crate-taos-macros-0.2.12 (c (n "taos-macros") (v "0.2.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "09l1inlbwy7li1smsaphv7cqm5ys090inla7yxa9ysi1379pp1bc")))

(define-public crate-taos-macros-0.2.13 (c (n "taos-macros") (v "0.2.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "0pgi33r9gaybwf1n5dq6bx38635z2d0q0q6gsaz7ym4c4yf1d8pa")))

(define-public crate-taos-macros-0.2.14 (c (n "taos-macros") (v "0.2.14") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "09qkbxlfiq664vxjr3f01nciz7klwhpjmya34xl7wdh3941pn3jg")))

(define-public crate-taos-macros-0.2.15 (c (n "taos-macros") (v "0.2.15") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "0krc0pbr5sn8jwr12pkg7z4lnv22my5hs42b5z7799hvsrh9rrv1")))

(define-public crate-taos-macros-0.2.16 (c (n "taos-macros") (v "0.2.16") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1vcn8qkc2hr29a2vxrkv6dvcp9c6ml3ij9dl7r29zsaalwbghaq5")))

