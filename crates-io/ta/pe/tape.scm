(define-module (crates-io ta pe tape) #:use-module (crates-io))

(define-public crate-tape-0.0.1 (c (n "tape") (v "0.0.1") (d (list (d (n "libtar-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1wwl2vnn2r608759ckg1hynm60mymg5p7iyp24k3glbmrpcdwlwv")))

(define-public crate-tape-0.0.2 (c (n "tape") (v "0.0.2") (d (list (d (n "libtar-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0p4pax7gn8lz9hwxswp28543m33j8i1ydd3hz4bhs3bgv6d3s2vk")))

(define-public crate-tape-0.0.3 (c (n "tape") (v "0.0.3") (d (list (d (n "libtar-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0gnj6s590d7sw1pd7rhyv5sfl68ky6n64zghj0k28jkba1njcvki")))

(define-public crate-tape-0.0.4 (c (n "tape") (v "0.0.4") (d (list (d (n "libtar-sys") (r "^0.0.4") (d #t) (k 0)))) (h "1582kc4z9cm1y4bwg2vl36rmvl59wsc8cliq1jwi722gfzi60w2p")))

(define-public crate-tape-0.0.5 (c (n "tape") (v "0.0.5") (d (list (d (n "libtar-sys") (r "^0.0.5") (d #t) (k 0)))) (h "187n67hfb9zcxh49azfb8khlkyv2y35j6rhr3bry2bc4ng8x6xqb")))

(define-public crate-tape-0.0.6 (c (n "tape") (v "0.0.6") (d (list (d (n "libtar-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0350211iyxl96ncnv6n920k884vynwq68hx4xqyx73nll72fym93")))

(define-public crate-tape-0.0.7 (c (n "tape") (v "0.0.7") (d (list (d (n "libtar-sys") (r "^0.0.7") (d #t) (k 0)))) (h "1ydkbxjmfsaijgp2vmaw7ifjcnb0jjcfvrbk3c3j4hzr3lj3hflk")))

(define-public crate-tape-0.0.8 (c (n "tape") (v "0.0.8") (d (list (d (n "libtar-sys") (r "^0.0.8") (d #t) (k 0)))) (h "0iw8yx8krr3bldp2f4ylh6w1ncjbrgmp39g33bmy6py9pyxgzfqk")))

(define-public crate-tape-0.0.9 (c (n "tape") (v "0.0.9") (d (list (d (n "libtar-sys") (r "^0.0.9") (d #t) (k 0)))) (h "1a755gnwfjwz8skd5ynvi8ih1w45yl4rhqfgvavyy2a2c9na6bqb")))

(define-public crate-tape-0.1.0 (c (n "tape") (v "0.1.0") (d (list (d (n "libtar-sys") (r "^0.1") (d #t) (k 0)))) (h "1f4x057g4mcgsfr4jdk4fa12lrh6dmygyr8l07fzmxbbxsipq0cx")))

(define-public crate-tape-0.1.1 (c (n "tape") (v "0.1.1") (d (list (d (n "libtar-sys") (r "^0.1") (d #t) (k 0)))) (h "0ca0r2j2sqmfnybi6dh0jdjsidwx71lb365cdcg5qc7pllzdpzb1")))

(define-public crate-tape-0.1.2 (c (n "tape") (v "0.1.2") (d (list (d (n "libtar-sys") (r "^0.1") (d #t) (k 0)))) (h "09ikjvljkrkmz5hbvai22ipkd9shza26797f1ghishyj10l3d9ab")))

(define-public crate-tape-0.1.3 (c (n "tape") (v "0.1.3") (d (list (d (n "libtar-sys") (r "^0.1") (d #t) (k 0)))) (h "1ql2km69534i30hf545wzwplg9wrwrdjiji1nb7sdwsycqb9vl0h")))

(define-public crate-tape-0.1.4 (c (n "tape") (v "0.1.4") (d (list (d (n "libtar-sys") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "082zvd6s4h743lm7nvcwwc4kyc2s1irlmza6gpnvziz1iz7ng2gd")))

(define-public crate-tape-0.1.5 (c (n "tape") (v "0.1.5") (d (list (d (n "libtar-sys") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "08gs24l5wlz8rpxw10qx19n3li7v2j8f2f6h1czymv8jn1cpxs0l")))

(define-public crate-tape-0.1.6 (c (n "tape") (v "0.1.6") (d (list (d (n "libtar-sys") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "08jgn4r4pb2dl7h4wxng512jjhrwzqnp82zl7j7940ar5h2mzv1g")))

(define-public crate-tape-0.1.7 (c (n "tape") (v "0.1.7") (d (list (d (n "libtar-sys") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0dwmfkv2gpy4lvaz2id7lylzhj5ak5nhhmsnz54pvrq845kgdliy")))

(define-public crate-tape-0.1.8 (c (n "tape") (v "0.1.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libtar-sys") (r "^0.1") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "189lmmlqpw0b5nfv3zkjj1027ndi4wf3kfjahzr47x1sz8zpqy86")))

(define-public crate-tape-0.2.0 (c (n "tape") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libtar-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0yz8wq9hkls44dxg2m114rhv28104pd49mpwvc6avilgpisiy7rv")))

(define-public crate-tape-0.2.1 (c (n "tape") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tar-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "067isdcr1vwzl07fwjxldyja5gpdf81ivcifffrnj5242w12d79r")))

(define-public crate-tape-0.3.0 (c (n "tape") (v "0.3.0") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tar-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0bisvzm9hb1vkzxacr392560i6avg8j76r1vxrl4av6c6lc5qz98")))

(define-public crate-tape-0.4.0 (c (n "tape") (v "0.4.0") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tar-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1hwy4j8bn21s8j5si00r8hp4k5lmcf3bc67wd88x4v15sckqlp38")))

(define-public crate-tape-0.4.1 (c (n "tape") (v "0.4.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tar-sys") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "04ccf1k0zlj725gxqfnv8dvcbmsgf89gi84x17xgvb9kf4dmrbya")))

(define-public crate-tape-0.5.0 (c (n "tape") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tar-sys") (r "^0.3") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1z2kh3321bsp9w5wrbg1px1llm6x4x7nj2d30rx59kzpil8hspq6")))

(define-public crate-tape-0.5.1 (c (n "tape") (v "0.5.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tar-sys") (r "^0.3") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "04s3nmgyq4wnyjasjvzr2wxshydx31alkdw8ld386x3aacv2xxd7")))

