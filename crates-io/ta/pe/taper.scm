(define-module (crates-io ta pe taper) #:use-module (crates-io))

(define-public crate-taper-0.1.0 (c (n "taper") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (o #t) (d #t) (k 0)))) (h "0p0yr1m0iazfq1qwz2mrvz9ji8q4jszsqbqqs98s1kqplss8sdl3") (f (quote (("default") ("async" "tokio")))) (y #t)))

(define-public crate-taper-0.2.0 (c (n "taper") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (o #t) (d #t) (k 0)))) (h "19giajngfn96zwfyb92n5xc9rl2yfhxkvaf19w81p9fvpxi09vw0") (f (quote (("default") ("async" "tokio")))) (y #t)))

(define-public crate-taper-0.2.1 (c (n "taper") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (o #t) (d #t) (k 0)))) (h "012jlcc3rb3xkh4pf4sp0xvp54xjxlf97a83g1880iywcvfvxb5n") (f (quote (("default") ("async" "tokio")))) (y #t)))

(define-public crate-taper-0.2.2 (c (n "taper") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (o #t) (d #t) (k 0)))) (h "1xaqxq1k44ax86mgcarmxg983hi05ig600grbm160pxpp5a9v5hy") (f (quote (("default") ("async" "tokio")))) (y #t)))

(define-public crate-taper-0.2.3 (c (n "taper") (v "0.2.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (o #t) (d #t) (k 0)))) (h "0g2rkj3v30p9856zwq9skk6i5gkirsslsrbhlpdvhr54c5wz2x2a") (f (quote (("default") ("async" "tokio")))) (y #t)))

(define-public crate-taper-0.2.4 (c (n "taper") (v "0.2.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (o #t) (d #t) (k 0)))) (h "0ly5cbmfbn21ij726ls6w06z92g152js78h6rq3v9xmhbh06krm9") (f (quote (("default") ("async" "tokio")))) (y #t)))

