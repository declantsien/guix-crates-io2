(define-module (crates-io ta lr talrost) #:use-module (crates-io))

(define-public crate-talrost-0.1.0 (c (n "talrost") (v "0.1.0") (h "009ls05jfwn8cm1f6wpj4q67mmxx9kdc9pxfjk1f83y8dg1qjx7z")))

(define-public crate-talrost-0.1.1 (c (n "talrost") (v "0.1.1") (h "030a8zcvsrq69vmlh1piymlsmk31sd0hdl06znc39a2ybpxrr49f")))

(define-public crate-talrost-0.1.2 (c (n "talrost") (v "0.1.2") (h "0ppacjzs3n1x77nn9xkdh9srf2d7lwin09kimn97q80i4ldnzkkm")))

