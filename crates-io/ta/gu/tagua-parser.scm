(define-module (crates-io ta gu tagua-parser) #:use-module (crates-io))

(define-public crate-tagua-parser-0.1.0 (c (n "tagua-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "~0.1") (d #t) (k 0)) (d (n "nom") (r "^1.2.3") (f (quote ("regexp" "regexp_macros"))) (d #t) (k 0)) (d (n "quickcheck") (r "~0.3") (d #t) (k 2)) (d (n "regex") (r "~0.1") (d #t) (k 0)))) (h "0plhm6zp2dvc95n8s4wd5l8532vh3ak39dqf0s10yi69hyrqdcfh")))

