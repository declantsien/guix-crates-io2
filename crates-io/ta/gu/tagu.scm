(define-module (crates-io ta gu tagu) #:use-module (crates-io))

(define-public crate-tagu-0.1.0 (c (n "tagu") (v "0.1.0") (h "1kyzmjz9xsz8gnj3qbcw1n4ag3p77an8klsn1gkzqzsjc2s2y9mi") (y #t)))

(define-public crate-tagu-0.1.1 (c (n "tagu") (v "0.1.1") (h "1nr433vwdi1n0w637rsrvj1m9f7hk87ylsp4z483wq9z9f5h3jr4") (y #t)))

(define-public crate-tagu-0.1.2 (c (n "tagu") (v "0.1.2") (h "0xcm4fzaxbk885qb77fsa2lv8ry8rvll8mmgnzyqznhqk3nwjm70") (y #t)))

(define-public crate-tagu-0.1.3 (c (n "tagu") (v "0.1.3") (h "02kbi7bysczsaf36294h1414kj6hrm9c1ip8v8bikrhw0147ppv7") (y #t)))

(define-public crate-tagu-0.1.4 (c (n "tagu") (v "0.1.4") (h "1nng1rqv1iybrnzgkfqq663g08fzac247xcyyxmz0qf77zm057i4") (y #t)))

(define-public crate-tagu-0.1.5 (c (n "tagu") (v "0.1.5") (h "02s33gm0wfd4952xi501xwhy4d9l3k11ay50rvlc93bc8lqzy1hz") (y #t)))

(define-public crate-tagu-0.1.6 (c (n "tagu") (v "0.1.6") (h "0sjpvqv29bpnf4lzmydwl2y71s5nw5p6jgfa3z99xfhgs836pnzd")))

