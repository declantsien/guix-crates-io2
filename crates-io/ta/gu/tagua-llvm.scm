(define-module (crates-io ta gu tagua-llvm) #:use-module (crates-io))

(define-public crate-tagua-llvm-0.0.1 (c (n "tagua-llvm") (v "0.0.1") (d (list (d (n "libc") (r "~0.2.7") (d #t) (k 0)) (d (n "llvm-sys") (r "~0.3.0") (d #t) (k 0)))) (h "0p04hkdybvc4jppx1pngi7lysclccxrzbh00b6c4w4mac51jspl2")))

(define-public crate-tagua-llvm-0.1.0 (c (n "tagua-llvm") (v "0.1.0") (d (list (d (n "libc") (r "~0.2.7") (d #t) (k 0)) (d (n "llvm-sys") (r "~0.3.0") (d #t) (k 0)))) (h "1vrg33px8fwsxgvw52g0bzz4vzg57yx747v7h1xzd015prfzhqsf")))

