(define-module (crates-io ta kk takkerus) #:use-module (crates-io))

(define-public crate-takkerus-0.2.0 (c (n "takkerus") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "zero_sum") (r "^1.1") (f (quote ("with_tak"))) (d #t) (k 0)))) (h "0yhdhim7i6y3jj7ic8yqw2akansrwz813rw76x1l55015c50v9hf") (y #t)))

(define-public crate-takkerus-0.3.0 (c (n "takkerus") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "zero_sum") (r "^1.2") (f (quote ("with_tak"))) (d #t) (k 0)))) (h "194q71zdhiswhpgdwfg5n8njs7r37lj6kw656p8c7in5l8wk5xwd") (y #t)))

