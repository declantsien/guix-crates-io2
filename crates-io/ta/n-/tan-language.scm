(define-module (crates-io ta n- tan-language) #:use-module (crates-io))

(define-public crate-tan-language-0.7.0 (c (n "tan-language") (v "0.7.0") (h "1r8njn0jcxw54xz4n8i0s9js2qhpxk4a8j7v5c5kc34w8hc3ic66")))

(define-public crate-tan-language-0.8.0 (c (n "tan-language") (v "0.8.0") (h "1ns95nwjld9qna92iy18p1xfcx00j6aw2jgj3wzhw41pb6ld6rm5")))

