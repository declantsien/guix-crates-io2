(define-module (crates-io ta n- tan-lints) #:use-module (crates-io))

(define-public crate-tan-lints-0.7.0 (c (n "tan-lints") (v "0.7.0") (d (list (d (n "lsp-types") (r "^0.94") (d #t) (k 0)) (d (n "tan") (r "^0.7") (d #t) (k 0)))) (h "106dymg037akgl8729m79z3lqdzq8ka6ka08vpsxj8g9gymfv9hk")))

(define-public crate-tan-lints-0.8.0 (c (n "tan-lints") (v "0.8.0") (d (list (d (n "lsp-types") (r "^0.94") (d #t) (k 0)) (d (n "tan") (r "^0.8") (d #t) (k 0)))) (h "1rfi8kkgc6sjqw8g7071i2g5pmp8a6jbwz06j59ixh5wszs0a6mv")))

(define-public crate-tan-lints-0.9.0 (c (n "tan-lints") (v "0.9.0") (d (list (d (n "lsp-types") (r "^0.94") (d #t) (k 0)) (d (n "tan") (r "^0.9") (d #t) (k 0)))) (h "02iaqr5kq6as6jl3vw2v9s01dcnhbpxzimiyb5jvymqrrqq3m5db")))

(define-public crate-tan-lints-0.10.0 (c (n "tan-lints") (v "0.10.0") (d (list (d (n "lsp-types") (r "^0.94") (d #t) (k 0)) (d (n "tan") (r "^0.10") (d #t) (k 0)))) (h "1np661icjf4lhdqcpvjwf0pdifr0qgm5qw199w5bcj8q21dcgpjr")))

(define-public crate-tan-lints-0.11.0 (c (n "tan-lints") (v "0.11.0") (d (list (d (n "lsp-types") (r "^0.95") (d #t) (k 0)) (d (n "tan") (r "^0.11") (d #t) (k 0)))) (h "1ky0cfz9gsdj1rmnn1viph6m8k9hx9ivngm94n60ml8zzsx70s8c")))

(define-public crate-tan-lints-0.12.0 (c (n "tan-lints") (v "0.12.0") (d (list (d (n "lsp-types") (r "^0.95") (d #t) (k 0)) (d (n "tan") (r "^0.12") (d #t) (k 0)))) (h "1y1jsn7hf04r5wpd5p87gj18vgvp5n0hhrjwdv1s1nfskqz15y9c")))

(define-public crate-tan-lints-0.13.0 (c (n "tan-lints") (v "0.13.0") (d (list (d (n "lsp-types") (r "^0.95") (d #t) (k 0)) (d (n "tan") (r "^0.13") (d #t) (k 0)))) (h "17l2s4xdhsbxcxbg0sfys2mx5nn9f8s0bfpsd6cvd13kl5mp0bqh")))

