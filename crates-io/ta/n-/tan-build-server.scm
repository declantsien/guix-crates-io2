(define-module (crates-io ta n- tan-build-server) #:use-module (crates-io))

(define-public crate-tan-build-server-0.12.0 (c (n "tan-build-server") (v "0.12.0") (h "0nayfsa78lks7z4jxf9rj3xdvkir2fbpwbgzpim7522ycam7a1vl")))

(define-public crate-tan-build-server-0.13.0 (c (n "tan-build-server") (v "0.13.0") (h "1b3sk81h179344c45yn7k187vi7bd2f4ci2lxds1pq7mmappqzh3")))

