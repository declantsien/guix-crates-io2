(define-module (crates-io ta n- tan-tools) #:use-module (crates-io))

(define-public crate-tan-tools-0.7.1 (c (n "tan-tools") (v "0.7.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "tan") (r "^0.7") (d #t) (k 0)) (d (n "tan-formatting") (r "^0.7") (d #t) (k 0)) (d (n "tan-lints") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0absxwcx64jyn2lsbhscl4yvizfhqqmxnk419x7f1jvl7a7f886y")))

(define-public crate-tan-tools-0.8.0 (c (n "tan-tools") (v "0.8.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "tan") (r "^0.8") (d #t) (k 0)) (d (n "tan-formatting") (r "^0.8") (d #t) (k 0)) (d (n "tan-lints") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1hnw7svxdsxpwan7fglcdpd5s83sq6zjz0ng0fq3wxmfn2mgnynk")))

(define-public crate-tan-tools-0.9.0 (c (n "tan-tools") (v "0.9.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "tan") (r "^0.9") (d #t) (k 0)) (d (n "tan-formatting") (r "^0.9") (d #t) (k 0)) (d (n "tan-lints") (r "^0.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0ba48pdzw4r97asj2jb6la39znzcq9hl9mbnibbaqbryrfqp8z1n")))

