(define-module (crates-io ta n- tan-shell) #:use-module (crates-io))

(define-public crate-tan-shell-0.11.0 (c (n "tan-shell") (v "0.11.0") (h "0ifhcnqvgyhcy0949w3nkid59ac419jyy6zz66glacjaks99hhv9")))

(define-public crate-tan-shell-0.12.0 (c (n "tan-shell") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)) (d (n "tan") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "08vq7w4d8a6mmx8j28pcrf7ma8lgrx1y55kcr6lxs2rvg985hjkn")))

(define-public crate-tan-shell-0.13.0 (c (n "tan-shell") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)) (d (n "tan") (r "^0.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0h60ps1kgdck5x6hgxm5arjzg6i3k775l0h91ckgl277lw3lill9")))

