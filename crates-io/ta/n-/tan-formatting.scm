(define-module (crates-io ta n- tan-formatting) #:use-module (crates-io))

(define-public crate-tan-formatting-0.7.0 (c (n "tan-formatting") (v "0.7.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.7") (d #t) (k 0)))) (h "1pl5vwkg1ha81wdiaqhaqzmhki4lg5pvx244z4hpa56pj594q2qn")))

(define-public crate-tan-formatting-0.8.0 (c (n "tan-formatting") (v "0.8.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.8") (d #t) (k 0)))) (h "1hih437pxjayx3f5aznq839ngx3gfgad9mlpyzqynn6kpqvh37qf")))

(define-public crate-tan-formatting-0.9.0 (c (n "tan-formatting") (v "0.9.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.9") (d #t) (k 0)))) (h "1n27rhqz4d6d28a7n0829fvpk0yk0lv0wn2xf7rqiaw6jgj0g2pw")))

(define-public crate-tan-formatting-0.10.0 (c (n "tan-formatting") (v "0.10.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.10") (d #t) (k 0)))) (h "1hhzwm32ch4sz6y692qk0976h34ag7jjsrxhm5y0v0vl7kp9idrg")))

(define-public crate-tan-formatting-0.11.0 (c (n "tan-formatting") (v "0.11.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.11") (d #t) (k 0)))) (h "02r6ivaq9ndy4gsp6jbg880pqniigsqqpxnk82vlrqr08na1iv0f")))

(define-public crate-tan-formatting-0.12.0 (c (n "tan-formatting") (v "0.12.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.12") (d #t) (k 0)))) (h "06iw0nc80gryvx0aaia2cwqlbl9kwxnlqwvi3wpl0a2096bfcq64")))

(define-public crate-tan-formatting-0.13.0 (c (n "tan-formatting") (v "0.13.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tan") (r "^0.13") (d #t) (k 0)))) (h "07ywrw3fcs7cs5k8qgnx95ylm6r6ckil2wm9amx4qrh8v5mhhxia")))

