(define-module (crates-io ta n- tan-analysis) #:use-module (crates-io))

(define-public crate-tan-analysis-0.7.0 (c (n "tan-analysis") (v "0.7.0") (d (list (d (n "tan") (r "^0.6") (d #t) (k 0)))) (h "1x1kvyy02qwmksqi4ccnswrx5ii10z2dvi8fyv3bhk9f1j647a4x")))

(define-public crate-tan-analysis-0.8.0 (c (n "tan-analysis") (v "0.8.0") (d (list (d (n "tan") (r "^0.8") (d #t) (k 0)))) (h "014fwrla1pv6arwbxxzpc4r49ydimq8a84q2nvszg53dml8z6s64")))

