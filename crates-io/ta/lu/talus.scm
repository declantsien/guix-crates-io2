(define-module (crates-io ta lu talus) #:use-module (crates-io))

(define-public crate-talus-0.1.0 (c (n "talus") (v "0.1.0") (d (list (d (n "cpython") (r "^0.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "kdtree") (r "^0.5.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0if949im5fr1qd7h6rfbamjb92ykmlrrx2jw6hl6qx4wdzy8pjim")))

