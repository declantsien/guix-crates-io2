(define-module (crates-io ta -c ta-common) #:use-module (crates-io))

(define-public crate-ta-common-0.1.0 (c (n "ta-common") (v "0.1.0") (h "0pzikbh54whxnipwsidk227arq0mmlnv1j50by38i1l8vqvr3rh4")))

(define-public crate-ta-common-0.1.1 (c (n "ta-common") (v "0.1.1") (h "199nd7x1xvml7x467a9hf4xvl2apj0mi56k5g99gmjp65lpfqjv2")))

(define-public crate-ta-common-0.1.2 (c (n "ta-common") (v "0.1.2") (h "1s3qpv509339a1gqlm179jm7sg94k31qj6418mcgayjm793w9jg8")))

(define-public crate-ta-common-0.1.3 (c (n "ta-common") (v "0.1.3") (h "0s9crpfmbc1jjyl6b58kw0qz5wycvvvja8ffjza9m1dmzn8abimm")))

