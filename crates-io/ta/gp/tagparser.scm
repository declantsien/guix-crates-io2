(define-module (crates-io ta gp tagparser) #:use-module (crates-io))

(define-public crate-tagparser-0.1.0 (c (n "tagparser") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1fs8r4hb3fwnrggwgnsb2bvxn1yirx9i7kb1bzb3phz2jzrhzai6") (r "1.75.0")))

(define-public crate-tagparser-0.2.0 (c (n "tagparser") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1r2c32b5lqncd4k3rqk50qaf30hxzm8mr7np37gavn1jhzbk2ghi") (r "1.72.0")))

