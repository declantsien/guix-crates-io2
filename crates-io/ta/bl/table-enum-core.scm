(define-module (crates-io ta bl table-enum-core) #:use-module (crates-io))

(define-public crate-table-enum-core-0.1.0 (c (n "table-enum-core") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0wpm52ayxamyhf85k6gpzp5wafp9cgk53jlvxasm9psf7bm2rnfm")))

(define-public crate-table-enum-core-0.2.0 (c (n "table-enum-core") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1qz4l1v19pwrz7s2z3yf8kgxvks4qmly2vrmkdpk6rbhnaqvi36d")))

