(define-module (crates-io ta bl tabled_derive) #:use-module (crates-io))

(define-public crate-tabled_derive-0.1.0 (c (n "tabled_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1g3qwz6bkxk53mmmdbgqlydghs6znlfn9w2bc1ccjcpidhjdw0hz")))

(define-public crate-tabled_derive-0.1.1 (c (n "tabled_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1di67l6dvr13iv05w5bd75akkim6azl3z4pxjdizqsx354ill7ri")))

(define-public crate-tabled_derive-0.1.2 (c (n "tabled_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0pfkr3j2lnx1sf3z0swpq2pm8ysagi4jbanyswdv86rj8928h1il")))

(define-public crate-tabled_derive-0.1.3 (c (n "tabled_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0yhmn0p10yvshvgjqdsnpd9km5jijkinf2pnavdg51505csc8mkw")))

(define-public crate-tabled_derive-0.1.4 (c (n "tabled_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1gz1f1hgrv6jrdypw1q0r0rrqlhj71kr3syqljz2pf98ny9bbq9q")))

(define-public crate-tabled_derive-0.1.5 (c (n "tabled_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wlcqxqbj8b8iqgi0jd15qag9wxcjv1grgy3pg17ijqaks7h9izn")))

(define-public crate-tabled_derive-0.1.6 (c (n "tabled_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "160afn4rf74p39wz5ya3q8hrnldmfmx0sd50kp3iapyp9nr5y78j")))

(define-public crate-tabled_derive-0.1.7 (c (n "tabled_derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1h6fxh9l281zjvlw89iqxx8jc8ym9icp2ljcis0q5hxhjbyy4nr9")))

(define-public crate-tabled_derive-0.1.8 (c (n "tabled_derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1fglibi12lih964k144khny782v5lffz4n94kch7ls1b2m5a0rmk")))

(define-public crate-tabled_derive-0.1.9 (c (n "tabled_derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06plwkj0l859pprvnc60n8dm9w2vnh8v8k3b84hpjril5pr5fal9") (y #t)))

(define-public crate-tabled_derive-0.2.0 (c (n "tabled_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0rb4ba74afd8nr66zyy8skjhgvcf9gf92f6wa9pg0c4cpif76952")))

(define-public crate-tabled_derive-0.3.0 (c (n "tabled_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0hy9s2dshp99a6clyklw7jj4z8gpi6h9haalw1lmm37f3j9a73i7")))

(define-public crate-tabled_derive-0.4.0 (c (n "tabled_derive") (v "0.4.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1a5bzhihvmvsrpy9y0402h8mnxrlgxbwkig1jgssp5zl0a2n3vpr")))

(define-public crate-tabled_derive-0.5.0 (c (n "tabled_derive") (v "0.5.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mqsy00j6q2jiajdgl7zs77pqnrip7cqi2w5vxajfkzbmi71pjmy")))

(define-public crate-tabled_derive-0.6.0 (c (n "tabled_derive") (v "0.6.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1r1z7qj37h1x4nyqbxq9jvbd713qvgpjlf1w18pz1x2lifh8ixlr")))

(define-public crate-tabled_derive-0.7.0 (c (n "tabled_derive") (v "0.7.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05sbyjc8warywzldz1bn8v4cyd2nc5ix5nhw6wx6apby6ycqy4sc")))

