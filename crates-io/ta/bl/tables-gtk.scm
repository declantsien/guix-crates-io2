(define-module (crates-io ta bl tables-gtk) #:use-module (crates-io))

(define-public crate-tables-gtk-0.1.0 (c (n "tables-gtk") (v "0.1.0") (d (list (d (n "gio") (r "^0.9") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.9") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "tables-api") (r "^0.1.0") (d #t) (k 0)))) (h "0pnm9zamc7k8kmf76gfqxm4qlnzl3liv8vvi41si15dq6pa3wp4v")))

