(define-module (crates-io ta bl tablelize) #:use-module (crates-io))

(define-public crate-tablelize-0.1.0 (c (n "tablelize") (v "0.1.0") (h "11i83m398f6zfr6y70piazf3l24shbl3nhc489mcpbi24krn0pbp") (y #t)))

(define-public crate-tablelize-0.1.1 (c (n "tablelize") (v "0.1.1") (h "01c38xbzv5m2lqmg7zlnng1m2g16bxdv1ymmv0yd35z76ir035bj")))

