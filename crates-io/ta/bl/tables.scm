(define-module (crates-io ta bl tables) #:use-module (crates-io))

(define-public crate-tables-0.1.1 (c (n "tables") (v "0.1.1") (d (list (d (n "io_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.9") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "1lp997sfya42q272gmgby8c28q2jpjlhfbhc6917hhja18kj1lrd")))

(define-public crate-tables-0.1.2 (c (n "tables") (v "0.1.2") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.9") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "02ia8rc46m8cd9fs9kkdwsbi6dm18c1bd8m31m37lqfnjskj8lya")))

(define-public crate-tables-0.1.3 (c (n "tables") (v "0.1.3") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "1i5m726lqf6n2qn8fgjzflxjm7cpcqlv7ksx74p9wfvqqnchzga1")))

(define-public crate-tables-0.1.4 (c (n "tables") (v "0.1.4") (d (list (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "0nh8k4sa2x33rw9fn5b1ib0mjvkwp7avf7da34bhf7igc43dsh40")))

(define-public crate-tables-0.1.5 (c (n "tables") (v "0.1.5") (d (list (d (n "io_utils") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "1kiynkrmnmk7ncs8wv5zqy2y2a10ygpgrjnp7ax95r90wqrcgq4f")))

