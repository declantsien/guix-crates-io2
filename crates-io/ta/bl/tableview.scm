(define-module (crates-io ta bl tableview) #:use-module (crates-io))

(define-public crate-tableview-0.5.0 (c (n "tableview") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "18jiyc3bjkmjq7iqy651byzl2d4vc3bs52y7in0rx94ykn7b1g8q") (y #t)))

