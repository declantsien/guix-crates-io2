(define-module (crates-io ta bl tablier) #:use-module (crates-io))

(define-public crate-tablier-0.1.0 (c (n "tablier") (v "0.1.0") (h "0ws26qxhxsj3wg1jij3yswwslcd59ad0chi7isflkf9r3yqgby4p")))

(define-public crate-tablier-0.1.1 (c (n "tablier") (v "0.1.1") (h "1gya3ij9xhzw9b297kh9ralcy03fwz2bfx780qchiyrxsf8lp2lz")))

(define-public crate-tablier-0.1.2 (c (n "tablier") (v "0.1.2") (h "160x0l6pjr1ldz8lwgrs23namsvnz79vdc3359in5i250p8an3l3")))

(define-public crate-tablier-0.1.3 (c (n "tablier") (v "0.1.3") (h "1v2axq15hy7li420d7n55jkj1f1dlkr9xinfzhmzagj0jy0np43b")))

