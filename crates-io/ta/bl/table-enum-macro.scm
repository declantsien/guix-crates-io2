(define-module (crates-io ta bl table-enum-macro) #:use-module (crates-io))

(define-public crate-table-enum-macro-0.1.0 (c (n "table-enum-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "table-enum-core") (r "^0.1.0") (d #t) (k 0)))) (h "1dlld6rcrw3zvh0byf4accvmqqf4f34kvg6fw4xx3njaq8pzpsld")))

(define-public crate-table-enum-macro-0.2.0 (c (n "table-enum-macro") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "table-enum-core") (r "^0.2.0") (d #t) (k 0)))) (h "1h4hggr4wzgi92s10d33vfgr6z0s0iap5h808593z4ihp01mxr76")))

