(define-module (crates-io ta bl table_formatter) #:use-module (crates-io))

(define-public crate-table_formatter-0.1.0 (c (n "table_formatter") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1x23xfdb1x7argz5kf63ljqijxy15xyr25v31w9ncqax4vh4pghj")))

(define-public crate-table_formatter-0.2.0 (c (n "table_formatter") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1s35wyviz8agifi7by0rnn826m0p514b899ppyiq7bhx26mfbrg4")))

(define-public crate-table_formatter-0.3.0 (c (n "table_formatter") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "050hkh49104x8h123i15mbg7ab8rfy4qrq8zbiys92pzg5q2fy4l")))

(define-public crate-table_formatter-0.3.1 (c (n "table_formatter") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "18qh2glbmzk4yglkprcbbrxfhkgm10kfqs6c2jrl8ylsv0njx881")))

(define-public crate-table_formatter-0.3.2 (c (n "table_formatter") (v "0.3.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1iyph1vpxbbq67pqqg9m4wwipg51yqdbp71c1fl93i3zd86yqlwg")))

(define-public crate-table_formatter-0.4.0 (c (n "table_formatter") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "09dpn0n90n7144929snv8gsarbwxw5nflpwy3r1rn07npffhqccw")))

(define-public crate-table_formatter-0.5.0 (c (n "table_formatter") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "12gr09fxhqfv8f77ipmbx9vf5ijhbcxfw3h7q4b6w8421pnfcgha")))

(define-public crate-table_formatter-0.5.1 (c (n "table_formatter") (v "0.5.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "03jh42r3zmzqq01izbnw084p6r51v855gg63vvqa486ia3gwpcbk") (y #t)))

(define-public crate-table_formatter-0.6.0 (c (n "table_formatter") (v "0.6.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1jnd34imgl2k4y1w5p7a3as88mcaray9drdz9aa8h6rfmpp0rh88")))

(define-public crate-table_formatter-0.6.1 (c (n "table_formatter") (v "0.6.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0zf1080zzscg2jwrzbkzgwr97vlsdbg4js1884fr2b27slzmvvxy")))

