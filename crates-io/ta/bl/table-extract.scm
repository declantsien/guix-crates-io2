(define-module (crates-io ta bl table-extract) #:use-module (crates-io))

(define-public crate-table-extract-0.1.0 (c (n "table-extract") (v "0.1.0") (d (list (d (n "scraper") (r "^0.4.0") (d #t) (k 0)))) (h "08r9rd2nqy90nyhl3qavyjfijcgv5lpz04a28wczv08f4ksdn0vn")))

(define-public crate-table-extract-0.2.0 (c (n "table-extract") (v "0.2.0") (d (list (d (n "scraper") (r "^0.4.0") (d #t) (k 0)))) (h "1pirj6rakmhgfaybzch52agf31r1nwrrj62vc5nn7ra9rrd0iq8y")))

(define-public crate-table-extract-0.2.1 (c (n "table-extract") (v "0.2.1") (d (list (d (n "scraper") (r "^0.4.0") (d #t) (k 0)))) (h "0z2vdia30pknq5l6bhrv7hd6minhc9lwvzqiidpxh98pa1f28ya3")))

(define-public crate-table-extract-0.2.2 (c (n "table-extract") (v "0.2.2") (d (list (d (n "scraper") (r "^0.11") (d #t) (k 0)))) (h "0kzj3arw3g10060mb0c95rzdnpza8r93hyxsraxfi72hnxgv2k18")))

(define-public crate-table-extract-0.2.3 (c (n "table-extract") (v "0.2.3") (d (list (d (n "scraper") (r "^0.13") (d #t) (k 0)))) (h "0his9gr0cs9vf414522xjpafqffa8mgyczkslig2rin0i9hajbhr")))

