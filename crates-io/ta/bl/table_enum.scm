(define-module (crates-io ta bl table_enum) #:use-module (crates-io))

(define-public crate-table_enum-0.1.0 (c (n "table_enum") (v "0.1.0") (d (list (d (n "table-enum-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1akkyd25q5iagsyvpklm6i0yih8r5mwcf9f0978k2f71ldhbnqls")))

(define-public crate-table_enum-0.1.1 (c (n "table_enum") (v "0.1.1") (d (list (d (n "table-enum-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0vh1qf2z6ihzpfjxnkij5flkcj2jcqqw8xmvp1xiv9kanq4sf5gh")))

(define-public crate-table_enum-0.1.2 (c (n "table_enum") (v "0.1.2") (d (list (d (n "table-enum-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1l50hrb5qv5wpammab1ph16jq2y513d6mkda316y0z116zrn7mxg")))

(define-public crate-table_enum-0.2.0 (c (n "table_enum") (v "0.2.0") (d (list (d (n "table-enum-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1bamiwq1x26zfmlrd1m0s2v892jp2s1x35frc7dlj3prd276dphm")))

