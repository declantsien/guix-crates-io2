(define-module (crates-io ta bl tablestream) #:use-module (crates-io))

(define-public crate-tablestream-0.1.0 (c (n "tablestream") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 2)) (d (n "unicode-truncate") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0h7w1j5rvif2kmpn9yk8ah6kqahqaidy0jfzqn6pvi02mvhfh19j")))

(define-public crate-tablestream-0.1.1 (c (n "tablestream") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 2)) (d (n "unicode-truncate") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "19lcb2gs9aqrcwm08a2plwws47pisxqwn46nz7npf9knmnh52q52")))

(define-public crate-tablestream-0.1.2 (c (n "tablestream") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 2)) (d (n "unicode-truncate") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0kxfqw0p8y8rqkazyxhfnc8l3hi1fjvs9rsqbki9qdyz8vh5lkx6")))

(define-public crate-tablestream-0.1.3 (c (n "tablestream") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 2)) (d (n "unicode-truncate") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0r8vchi4v9vwkamyv9fx82v5iv519gd47nz9c1qr3y7rmp3bx87y")))

