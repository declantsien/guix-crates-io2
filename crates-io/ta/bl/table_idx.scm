(define-module (crates-io ta bl table_idx) #:use-module (crates-io))

(define-public crate-table_idx-1.0.0 (c (n "table_idx") (v "1.0.0") (h "003kwik345shvzzjd82rsqixln5vxpd3p27gcrk9sg9n79j472fj")))

(define-public crate-table_idx-1.0.1 (c (n "table_idx") (v "1.0.1") (h "0jsfic0nsz22cn06vnrs1z0r2a7yr0jpkxf4hxpa6jg4ssddw3i0")))

