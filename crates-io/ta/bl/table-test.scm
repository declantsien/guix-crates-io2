(define-module (crates-io ta bl table-test) #:use-module (crates-io))

(define-public crate-table-test-0.1.0 (c (n "table-test") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "0g2zyrqgm42pdbsc6srbhzm4n71axsd0m7k68svz28v7qh1ginln")))

(define-public crate-table-test-0.1.1 (c (n "table-test") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "1qrnwp68bnilpnkwpv45a2cz6136dlwwmsgpfzj3nqv9pryjh7nk")))

(define-public crate-table-test-0.2.0 (c (n "table-test") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "0dmyk5bhampv7qnhq5vphcp26022hl0g7mcqrkw4w34gsyaiqgij")))

(define-public crate-table-test-0.2.1 (c (n "table-test") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "mock-it") (r "^0.1.0") (d #t) (k 2)))) (h "0s0yrlhi0dh76q3nds2mid9vbj00k3k0qff5w3jifirqk9zwhwsg")))

