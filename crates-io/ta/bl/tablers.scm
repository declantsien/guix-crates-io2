(define-module (crates-io ta bl tablers) #:use-module (crates-io))

(define-public crate-tablers-1.0.0 (c (n "tablers") (v "1.0.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "printpdf") (r "^0.3.2") (d #t) (k 0)) (d (n "rust-embed") (r "^5.6.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1m2zs2cx0gq22ypvf8m3c7v3l6r4447cldwik621i98a97pcgh5g")))

(define-public crate-tablers-1.0.1 (c (n "tablers") (v "1.0.1") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "printpdf") (r "^0.3.2") (k 0)) (d (n "rust-embed") (r "^5.6.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1fm1zy7lxs3axnvhr5572lbs26lj520pc0nkazwz63d5i94bk314")))

(define-public crate-tablers-1.1.0 (c (n "tablers") (v "1.1.0") (d (list (d (n "csv") (r ">=1.1.4, <2.0.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "printpdf") (r ">=0.3.3, <0.4.0") (k 0)) (d (n "rust-embed") (r ">=5.6.0, <6.0.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.22, <2.0.0") (d #t) (k 0)))) (h "18np55psnqndxff20n3nidcg12jg1h663j770ks6qi52zjza4922")))

(define-public crate-tablers-1.1.1 (c (n "tablers") (v "1.1.1") (d (list (d (n "csv") (r "^1.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "printpdf") (r "^0.3.4") (k 0)) (d (n "rust-embed") (r "^5.6.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "12793sgkgifp7j8vc1dwhcdd44y2r46696qladx0caq0lz3pqa1s")))

(define-public crate-tablers-1.2.0 (c (n "tablers") (v "1.2.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "printpdf") (r "^0.5.2") (k 0)) (d (n "rust-embed") (r "^6.4.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1d2407k4wnzmqi0g08jr5i2wvw4rb4cpn1qzh6py9hijil67vy79")))

