(define-module (crates-io ta bl tablesalt) #:use-module (crates-io))

(define-public crate-tablesalt-0.1.0 (c (n "tablesalt") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.2.7") (f (quote ("use-pkg-config"))) (d #t) (k 0)))) (h "1v7pb6zq7sjc37gysdl6yqlx9jf60bdb8rc4s7dhvm98baz5idbc")))

(define-public crate-tablesalt-0.2.0 (c (n "tablesalt") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (k 0)))) (h "1qqj99jaygflwrizjmywkqm028rk8jspki9vras2d37r27i88h76")))

(define-public crate-tablesalt-0.3.0 (c (n "tablesalt") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (k 0)))) (h "16gdd5lk416sarg4k55ad6csd3mg6hgg418mbxgk8x4piwqzyv34") (y #t)))

(define-public crate-tablesalt-0.3.1 (c (n "tablesalt") (v "0.3.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (k 0)))) (h "035xnblk8f5blh43acq0c2h779iyaxf3nay7f9rzfl2rs62hi45k")))

