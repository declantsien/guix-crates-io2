(define-module (crates-io ta bl table-format) #:use-module (crates-io))

(define-public crate-table-format-0.0.1 (c (n "table-format") (v "0.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1s3835xy0k5rlr6w5wdjlas165y94wwc186j3xb67c5x9qmskx9l")))

(define-public crate-table-format-0.0.2 (c (n "table-format") (v "0.0.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1mvvww9yfwyb8hxipmipr0dx0p2qzpbz7wkbhmsygxmmkdby85di")))

(define-public crate-table-format-0.0.3 (c (n "table-format") (v "0.0.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "delim") (r "^0.1.0") (d #t) (k 0)))) (h "1fwx10px5mrnzzwdv0ca66agq9viqk387rm81f15fdqilpxrim3q")))

(define-public crate-table-format-0.0.4 (c (n "table-format") (v "0.0.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "06gsnsx77lsjyj6gzfzaavimajqvi9a6c2yhnaznz321xfqv4vbv")))

(define-public crate-table-format-0.0.5 (c (n "table-format") (v "0.0.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0m6nyib187qhpn1a8pi8wwg5mv90nqdclcv8abrnyy07nbg4yhlq")))

