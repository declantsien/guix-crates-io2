(define-module (crates-io ta bl tablefy) #:use-module (crates-io))

(define-public crate-tablefy-0.1.0 (c (n "tablefy") (v "0.1.0") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "tablefy_derive") (r "^0.1.0") (d #t) (k 0)))) (h "18mn7knfknkb3pzkihzvsjfgc2y7ar0305n3pxzjmjw90ihrrgvk")))

(define-public crate-tablefy-0.1.1 (c (n "tablefy") (v "0.1.1") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "tablefy_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1y88bc2w5j75kdj2fz9ww0m0a716fr7wl0xb886azw0zgnl71f0f")))

(define-public crate-tablefy-0.1.2 (c (n "tablefy") (v "0.1.2") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "tablefy_derive") (r "^0.1.1") (d #t) (k 0)))) (h "15vn31pd9lzd772rg6i78j1a24nc885i6xgydf5hmbdvriz1jffq")))

(define-public crate-tablefy-0.1.3 (c (n "tablefy") (v "0.1.3") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "05z6k31fcxbhmix276r5abkgj1mrz20dkknj3lhidy7791idp3m2")))

