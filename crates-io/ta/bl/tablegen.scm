(define-module (crates-io ta bl tablegen) #:use-module (crates-io))

(define-public crate-tablegen-0.1.0 (c (n "tablegen") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "13kr7n5fcfxs033k7mq3ima77c42758dgxff98621528xjn2n83h")))

(define-public crate-tablegen-0.1.1 (c (n "tablegen") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "13z76jxyn3c6jwsab4ppszkprp3p3hmvjixq1sfgvmld756n3d2c")))

(define-public crate-tablegen-0.1.2 (c (n "tablegen") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "102m10jvbsm6bm8firv5qdgn6g9sg0j2w6rhsiqar1filf9f29i1") (y #t)))

