(define-module (crates-io ta bl table_to_html) #:use-module (crates-io))

(define-public crate-table_to_html-0.2.0 (c (n "table_to_html") (v "0.2.0") (d (list (d (n "tabled") (r "^0.11") (f (quote ("std"))) (k 0)) (d (n "tabled") (r "^0.11") (f (quote ("std" "derive"))) (k 2)))) (h "1ak5k8bzm8jdwd15wrfqvwja98g7vbc5ps6f83k4jwxd8gz20knx")))

(define-public crate-table_to_html-0.3.0 (c (n "table_to_html") (v "0.3.0") (d (list (d (n "tabled") (r "^0.12") (f (quote ("std"))) (k 0)) (d (n "tabled") (r "^0.12") (f (quote ("std" "derive"))) (k 2)))) (h "1ypynx9njv874rjmsg67xknwxdkk40dzl3sgn8zjwzh89z0sla56")))

(define-public crate-table_to_html-0.4.0 (c (n "table_to_html") (v "0.4.0") (d (list (d (n "tabled") (r "^0.15") (f (quote ("std"))) (k 0)) (d (n "tabled") (r "^0.15") (f (quote ("std" "derive"))) (k 2)))) (h "1ab1620kcahc0345pryhbaw3i8a6fimzn5d245ngzibl6z64bxqd") (f (quote (("macros" "tabled/macros") ("derive" "tabled/derive") ("ansi" "tabled/ansi"))))))

