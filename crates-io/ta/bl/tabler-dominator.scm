(define-module (crates-io ta bl tabler-dominator) #:use-module (crates-io))

(define-public crate-tabler-dominator-0.1.0 (c (n "tabler-dominator") (v "0.1.0") (d (list (d (n "dominator") (r "^0.5.34") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "19g8nfzm7l9ajps0mk3ywgq1qj9jgfryj13s7q1a8r0b3bxp4qy7")))

(define-public crate-tabler-dominator-0.1.1 (c (n "tabler-dominator") (v "0.1.1") (d (list (d (n "dominator") (r "^0.5.34") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.66") (f (quote ("console"))) (d #t) (k 0)))) (h "01agjwik05c2wlzisc31p141nwly3hy7zsjkcq0n4gvllw0ci07k")))

