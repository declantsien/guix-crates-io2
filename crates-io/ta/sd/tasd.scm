(define-module (crates-io ta sd tasd) #:use-module (crates-io))

(define-public crate-tasd-0.1.0 (c (n "tasd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)))) (h "0n1frjb7p49xxgydxmg9cyzhib6db0m9lw9k2kgb15hg4phqrfyg")))

(define-public crate-tasd-0.1.1 (c (n "tasd") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)))) (h "08623nipx25p32j4kgw87w0qi6dbmxskc820c6l6r41c7rg3ja9k")))

(define-public crate-tasd-0.2.0 (c (n "tasd") (v "0.2.0") (d (list (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "17nv6r6cass6i2x70q3rimzv3mli328sz9mirkp6hyxx63pbjwbk")))

(define-public crate-tasd-0.2.1 (c (n "tasd") (v "0.2.1") (d (list (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "0afwzhnifnqp8f07y3ix14d41h0db9v6n845p26cgpc0q1gl01xr")))

(define-public crate-tasd-0.3.0 (c (n "tasd") (v "0.3.0") (d (list (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "085fbflkjx52xjydwhl4f9nsc0481jqp9j5iarvyv1q75xns73hp")))

(define-public crate-tasd-0.4.0 (c (n "tasd") (v "0.4.0") (d (list (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "18a0c36kcsh909ybw9nrgkx1ydxm51baknlqa8id83p0jz6zf2aw")))

