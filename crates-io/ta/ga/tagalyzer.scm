(define-module (crates-io ta ga tagalyzer) #:use-module (crates-io))

(define-public crate-tagalyzer-0.0.1 (c (n "tagalyzer") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ygb786p0dzl1yy7cwj61s8fk2l73pjkwhjyy3dqjg2kncw5kla3")))

(define-public crate-tagalyzer-0.0.2 (c (n "tagalyzer") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1b30g73xzkzhv2rs6rm30al0hv0rsmx7028g6r7mlvw02ygai6fz")))

(define-public crate-tagalyzer-0.1.0 (c (n "tagalyzer") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ajgwbizqc1yhllfajch25iazfk250lqkb89kad1kwyjpxzqfm9m")))

(define-public crate-tagalyzer-0.1.1 (c (n "tagalyzer") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wam17rwmp85r38ccjdwkyxmnb1q6sng3fgrza3p9fgww876ss07")))

(define-public crate-tagalyzer-0.2.0 (c (n "tagalyzer") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0sp9av2y16ch60jkzrpz3qx0mljg6xw6bl3bspmcsza83izgb7xv")))

(define-public crate-tagalyzer-0.3.0 (c (n "tagalyzer") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "04cb65s3lhcjwa14rhxnwjfg7y96q8vvc3ywspcqiv1xixjsyhxh")))

