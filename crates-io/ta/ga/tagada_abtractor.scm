(define-module (crates-io ta ga tagada_abtractor) #:use-module (crates-io))

(define-public crate-tagada_abtractor-0.1.0 (c (n "tagada_abtractor") (v "0.1.0") (d (list (d (n "clap") (r "2.33.*") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "12r5l48qs0mpyrkfslwapvgwx67myf3n2wk4vqyp9m9qx4jm8l20") (y #t)))

