(define-module (crates-io ta r2 tar2arx) #:use-module (crates-io))

(define-public crate-tar2arx-0.2.0 (c (n "tar2arx") (v "0.2.0") (d (list (d (n "arx") (r "^0.2.0") (f (quote ("cmd_utils" "zstd"))) (d #t) (k 0) (p "libarx")) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "jbk") (r "^0.2.0") (d #t) (k 0) (p "jubako")) (d (n "tar") (r "^0.4.39") (d #t) (k 0)))) (h "02ji9ivl9vs7ng4jlbxi27q55yxa67qz9fd7sb4pnf52799xz0pq")))

(define-public crate-tar2arx-0.2.1 (c (n "tar2arx") (v "0.2.1") (d (list (d (n "arx") (r "^0.2.1") (f (quote ("cmd_utils" "zstd"))) (d #t) (k 0) (p "libarx")) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.0") (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.20") (d #t) (k 0)) (d (n "human-panic") (r "^1.2.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "jbk") (r "^0.2.1") (d #t) (k 0) (p "jubako")) (d (n "niffler") (r "^2.5.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.39") (d #t) (k 0)))) (h "1300zpacvv5k0m24l33ir71wxij9bqr1326pra7f1187wc5n6rc0")))

