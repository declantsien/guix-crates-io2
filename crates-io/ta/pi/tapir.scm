(define-module (crates-io ta pi tapir) #:use-module (crates-io))

(define-public crate-tapir-1.0.0 (c (n "tapir") (v "1.0.0") (h "0j95nx5705fmk1rgh5c8h6ab3qfgw5954kx0mlj726f0mycqp1s2")))

(define-public crate-tapir-1.0.1 (c (n "tapir") (v "1.0.1") (h "1x0357hnln0bpf0jzy1d4yfg684wgf0yysvvy1ypgagrha8pa1al")))

