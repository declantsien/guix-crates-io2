(define-module (crates-io ta pi tapioca) #:use-module (crates-io))

(define-public crate-tapioca-0.0.0 (c (n "tapioca") (v "0.0.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tapioca-codegen") (r "^0") (d #t) (k 0)))) (h "1y2gwcsa1afvj88ai1h6nng06jy0x3527k06k3rh723ia8sfwajq")))

(define-public crate-tapioca-0.0.1 (c (n "tapioca") (v "0.0.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tapioca-codegen") (r "^0.0.1") (d #t) (k 0)))) (h "0f22pz2cyva4b1662x7hl3i51dwgy56q9bl2k7m2ly4v0cc3qjfn")))

