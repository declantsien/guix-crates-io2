(define-module (crates-io ta pi tapioca-codegen) #:use-module (crates-io))

(define-public crate-tapioca-codegen-0.0.0 (c (n "tapioca-codegen") (v "0.0.0") (d (list (d (n "Inflector") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "1dqrcrkj2sjz5x33fxz1gnjpjlgwv13c1iv9hbg4j0vwd9njj4cs")))

(define-public crate-tapioca-codegen-0.0.1 (c (n "tapioca-codegen") (v "0.0.1") (d (list (d (n "Inflector") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0vy0g42f33b4kjp5ih5v4d9mm1xv1y07s76pba3r7pbsmjxm1ffh")))

