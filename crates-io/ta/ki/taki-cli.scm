(define-module (crates-io ta ki taki-cli) #:use-module (crates-io))

(define-public crate-taki-cli-0.1.0 (c (n "taki-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (d #t) (k 0)))) (h "1dvkz8zmhabfyfg6bcyz458amrqhihbbfkbqilxc8a9i87pirqsh")))

(define-public crate-taki-cli-0.1.1 (c (n "taki-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (d #t) (k 0)))) (h "1zyh1a9jv9c4p01r983ajm5q92pkfb2chj4xxjnhi6avkz94g757")))

