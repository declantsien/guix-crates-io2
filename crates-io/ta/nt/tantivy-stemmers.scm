(define-module (crates-io ta nt tantivy-stemmers) #:use-module (crates-io))

(define-public crate-tantivy-stemmers-0.1.0 (c (n "tantivy-stemmers") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.199") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.199") (d #t) (k 0)) (d (n "tantivy") (r "^0.22.0") (d #t) (k 2)) (d (n "tantivy-tokenizer-api") (r "^0.3.0") (d #t) (k 0)))) (h "1807cavwv4rvq8rqwrxhfliihrnhqrkzddr3k100f0svmwq8q2zz")))

(define-public crate-tantivy-stemmers-0.2.0 (c (n "tantivy-stemmers") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.199") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.199") (d #t) (k 0)) (d (n "tantivy") (r "^0.22.0") (d #t) (k 2)) (d (n "tantivy-tokenizer-api") (r "^0.3.0") (d #t) (k 0)))) (h "0h83b6wxhmg18divlbka29j2sfria1yy85wm94hhwhmy46bgrrgp")))

