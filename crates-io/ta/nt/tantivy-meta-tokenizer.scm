(define-module (crates-io ta nt tantivy-meta-tokenizer) #:use-module (crates-io))

(define-public crate-tantivy-meta-tokenizer-0.3.0 (c (n "tantivy-meta-tokenizer") (v "0.3.0") (d (list (d (n "fast2s") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "jieba-rs") (r "^0.6") (f (quote ("default-dict"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pinyin") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tantivy") (r "^0.19") (k 0)))) (h "1h6978hmjf9p4xfwhdhjmivc01rjya1m4vwnk81wycmzi9p3zp5y") (f (quote (("default")))) (s 2) (e (quote (("pinyin" "dep:pinyin" "dep:itertools"))))))

