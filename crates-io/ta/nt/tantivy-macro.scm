(define-module (crates-io ta nt tantivy-macro) #:use-module (crates-io))

(define-public crate-tantivy-macro-0.1.0 (c (n "tantivy-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0kc66gbh5mlvnjak1x8xbfyr0ap7hybscb1rc0yr7b1w0602h545")))

