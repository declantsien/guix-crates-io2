(define-module (crates-io ta nt tantivy-tokenizer-api) #:use-module (crates-io))

(define-public crate-tantivy-tokenizer-api-0.1.0 (c (n "tantivy-tokenizer-api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ikgsv0444mz5wql52klxakd1z1d7v7yjsxzykznpgjc6lckbvlb")))

(define-public crate-tantivy-tokenizer-api-0.1.1 (c (n "tantivy-tokenizer-api") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "00120ry4bpn9in1qnc87nz5lxzrmg18kpqkm88f3lsz0nq0nh634")))

(define-public crate-tantivy-tokenizer-api-0.2.0 (c (n "tantivy-tokenizer-api") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0riv8kwviijsmm9rkzj7v25sn9xqqkikix14s82kavdj2ar6yyrl")))

(define-public crate-tantivy-tokenizer-api-0.3.0 (c (n "tantivy-tokenizer-api") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "011v0n907m9nyga6vgrnk6c13zzdkhn96zgiwv7qk6l14pgcl39a")))

