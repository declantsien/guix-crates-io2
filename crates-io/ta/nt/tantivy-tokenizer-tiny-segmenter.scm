(define-module (crates-io ta nt tantivy-tokenizer-tiny-segmenter) #:use-module (crates-io))

(define-public crate-tantivy-tokenizer-tiny-segmenter-0.1.0 (c (n "tantivy-tokenizer-tiny-segmenter") (v "0.1.0") (d (list (d (n "tantivy") (r "^0.8") (d #t) (k 0)) (d (n "tinysegmenter") (r "^0.1") (d #t) (k 0)))) (h "0agpc0cidrsjiki62z380avms83mb791ha634h5w4wgfcpwbr1n1")))

(define-public crate-tantivy-tokenizer-tiny-segmenter-0.2.0 (c (n "tantivy-tokenizer-tiny-segmenter") (v "0.2.0") (d (list (d (n "tantivy") (r "^0.9") (d #t) (k 0)) (d (n "tinysegmenter") (r "^0.1") (d #t) (k 0)))) (h "1w1k8nmg0yh5mgg06l2wgv8as0z1h8psnwif7vmlav2q7jmx2b4w")))

(define-public crate-tantivy-tokenizer-tiny-segmenter-0.3.0 (c (n "tantivy-tokenizer-tiny-segmenter") (v "0.3.0") (d (list (d (n "tantivy") (r "^0.10.2") (d #t) (k 0)) (d (n "tinysegmenter") (r "^0.1.1") (d #t) (k 0)))) (h "17hkrghifgnqx6nbj8ksh678b26ay5qrbwl7rnpalira2awqhnja")))

