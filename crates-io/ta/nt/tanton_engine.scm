(define-module (crates-io ta nt tanton_engine) #:use-module (crates-io))

(define-public crate-tanton_engine-1.0.0 (c (n "tanton_engine") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "criterion") (r "^0.2.10") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tanton") (r "^1.0.0") (d #t) (k 0)))) (h "1v842bdicd91ibfg0zfgxdzjxkzji48l7zayn4wli5vai8kqcvpb") (f (quote (("nightly") ("default"))))))

