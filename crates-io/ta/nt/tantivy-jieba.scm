(define-module (crates-io ta nt tantivy-jieba) #:use-module (crates-io))

(define-public crate-tantivy-jieba-0.1.0 (c (n "tantivy-jieba") (v "0.1.0") (d (list (d (n "jieba-rs") (r "^0.2") (d #t) (k 0)) (d (n "tantivy") (r "^0.8") (d #t) (k 0)))) (h "1a9kyx9iqz9ks60wcnmymdp7bdfzyrvadvz9yljjbm4xwh9g3smy")))

(define-public crate-tantivy-jieba-0.1.1 (c (n "tantivy-jieba") (v "0.1.1") (d (list (d (n "jieba-rs") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "tantivy") (r "^0.8") (d #t) (k 0)))) (h "0c11hkzbsz1fykx78j2adjp6xvbxwdjb2ik5w1hxwqvlw543x62v")))

(define-public crate-tantivy-jieba-0.2.0 (c (n "tantivy-jieba") (v "0.2.0") (d (list (d (n "jieba-rs") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tantivy") (r "^0.10") (d #t) (k 0)))) (h "0x6yb7w9q6l842gk18k816aj3k4n2glnr9nflcgm65ad7frqsfpi")))

(define-public crate-tantivy-jieba-0.4.0 (c (n "tantivy-jieba") (v "0.4.0") (d (list (d (n "jieba-rs") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tantivy") (r "^0.12") (d #t) (k 0)))) (h "0nf12grkg45qis8qqj147j97p2ilzx9n3d18vcsbq5mrjrx5h9mm")))

(define-public crate-tantivy-jieba-0.5.0 (c (n "tantivy-jieba") (v "0.5.0") (d (list (d (n "jieba-rs") (r "^0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.15.1") (d #t) (k 0)))) (h "1lxpzv1b6r35d6zlfr0xqn67zwv683sxjy8x2gvds8fhsxi74vba")))

(define-public crate-tantivy-jieba-0.5.1 (c (n "tantivy-jieba") (v "0.5.1") (d (list (d (n "jieba-rs") (r "^0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.15.1") (d #t) (k 0)))) (h "0k8g3i00karcfn1i6mw2pks89n26sfdzyib01lsszil7lyvikxxs")))

(define-public crate-tantivy-jieba-0.6.0 (c (n "tantivy-jieba") (v "0.6.0") (d (list (d (n "jieba-rs") (r "^0.6.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.18.0") (d #t) (k 0)))) (h "0hsjwj8i845shv9anbklr74625xdyf6jkk71si6h51qhv0xbsk9l")))

(define-public crate-tantivy-jieba-0.7.0 (c (n "tantivy-jieba") (v "0.7.0") (d (list (d (n "jieba-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.0") (d #t) (k 0)))) (h "1sn2fz25s0gnn4gjyx4qlgcx68rk15h04k6gvk72bdki1rsfyizj")))

(define-public crate-tantivy-jieba-0.8.0 (c (n "tantivy-jieba") (v "0.8.0") (d (list (d (n "jieba-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.20.2") (d #t) (k 0)))) (h "0dzpwhg5qifxn44qgxrmnhk6388gg0mlwagqv682gjmddlxba9sw")))

(define-public crate-tantivy-jieba-0.9.0 (c (n "tantivy-jieba") (v "0.9.0") (d (list (d (n "jieba-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.20.2") (d #t) (k 2)) (d (n "tantivy-tokenizer-api") (r "^0.1.1") (d #t) (k 0)))) (h "0kjmzi7bd8phxr4r57j4hcw3qj0frkfbmfixgibzrk4zy8pi1s54")))

(define-public crate-tantivy-jieba-0.10.0 (c (n "tantivy-jieba") (v "0.10.0") (d (list (d (n "jieba-rs") (r "^0.6.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.21.0") (d #t) (k 2)) (d (n "tantivy-tokenizer-api") (r "^0.2.0") (d #t) (k 0)))) (h "1960pamr3fhwg3kx8czy610kjgbdh08jz6s30f78g3raq69j40j4")))

(define-public crate-tantivy-jieba-0.11.0 (c (n "tantivy-jieba") (v "0.11.0") (d (list (d (n "jieba-rs") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.22.0") (d #t) (k 2)) (d (n "tantivy-tokenizer-api") (r "^0.3.0") (d #t) (k 0)))) (h "1h8n5w788jlq1lksclgvvbq56yr8p7xyjb0gdz87c3az29ffcbqg")))

