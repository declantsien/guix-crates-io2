(define-module (crates-io ta nt tantivy-object-store) #:use-module (crates-io))

(define-public crate-tantivy-object-store-0.1.0 (c (n "tantivy-object-store") (v "0.1.0") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "object_store") (r ">=0.6.1") (d #t) (k 0)) (d (n "stop-words") (r "^0") (d #t) (k 2)) (d (n "tantivy") (r ">=0.19") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)) (d (n "wikidump") (r "^0.2.2") (d #t) (k 2)))) (h "1lzknxqf0br0hdymqikz5q63f1mdrzkfh38i7ln3rr1napxba08l") (r "1.65")))

