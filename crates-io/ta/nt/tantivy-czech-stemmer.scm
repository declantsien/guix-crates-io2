(define-module (crates-io ta nt tantivy-czech-stemmer) #:use-module (crates-io))

(define-public crate-tantivy-czech-stemmer-0.1.1 (c (n "tantivy-czech-stemmer") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.199") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.199") (d #t) (k 0)) (d (n "tantivy-tokenizer-api") (r "^0.3.0") (d #t) (k 0)))) (h "101w6js2c03yck6gnxl529wxrqsci15cimqjjq79vczcd3vqd1pv")))

(define-public crate-tantivy-czech-stemmer-0.2.1 (c (n "tantivy-czech-stemmer") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.199") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.199") (d #t) (k 0)) (d (n "tantivy") (r "^0.22.0") (d #t) (k 2)) (d (n "tantivy-tokenizer-api") (r "^0.3.0") (d #t) (k 0)))) (h "04iwgp3cp2a2bk3xi0ag4vyadb8428h4sci21chj8gl75k9fcnvl")))

