(define-module (crates-io ta nt tantivy-pinyin) #:use-module (crates-io))

(define-public crate-tantivy-pinyin-0.1.0 (c (n "tantivy-pinyin") (v "0.1.0") (d (list (d (n "pinyin") (r "^0.9") (d #t) (k 0)) (d (n "tantivy") (r "^0.18") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "utf8_slice") (r "^1") (d #t) (k 0)))) (h "1ikjmwir8rghj5nj0iqpw1kgnb1ppv2k0n4j9xkdi6xh9pv9g2i7")))

(define-public crate-tantivy-pinyin-0.1.1 (c (n "tantivy-pinyin") (v "0.1.1") (d (list (d (n "pinyin") (r "^0.9") (d #t) (k 0)) (d (n "tantivy") (r "^0.18") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "utf8_slice") (r "^1") (d #t) (k 0)))) (h "1ailbn479clyzg81rwhnc1i19nqkb20bd0lnxzbpnpv3hk4hqiyw") (f (quote (("stop_words") ("defalut"))))))

(define-public crate-tantivy-pinyin-0.1.2 (c (n "tantivy-pinyin") (v "0.1.2") (d (list (d (n "pinyin") (r "^0.9") (d #t) (k 0)) (d (n "tantivy") (r "^0.18") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "utf8_slice") (r "^1") (d #t) (k 0)))) (h "1nchy89kh7v4qrlg45wjvm8w2jfrjpl23w81rf08inkn0gwp9bbj") (f (quote (("stop_words") ("defalut"))))))

(define-public crate-tantivy-pinyin-0.1.3 (c (n "tantivy-pinyin") (v "0.1.3") (d (list (d (n "pinyin") (r "^0.9") (d #t) (k 0)) (d (n "tantivy") (r "^0.18") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "utf8_slice") (r "^1") (d #t) (k 0)))) (h "0b3z7jysvj60awvklfj126aiyx26r78h813yy06qc1kv2xzgwpq7") (f (quote (("stop_words") ("defalut"))))))

