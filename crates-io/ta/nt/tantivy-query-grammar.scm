(define-module (crates-io ta nt tantivy-query-grammar) #:use-module (crates-io))

(define-public crate-tantivy-query-grammar-0.11.0 (c (n "tantivy-query-grammar") (v "0.11.0") (d (list (d (n "combine") (r ">= 3.6.0, < 4.0.0") (d #t) (k 0)))) (h "1bdmpgj9l1r732y1s8mq3cs64l3a0g18vy0vwhwy9inrv43kghqk")))

(define-public crate-tantivy-query-grammar-0.12.0 (c (n "tantivy-query-grammar") (v "0.12.0") (d (list (d (n "combine") (r "^4") (d #t) (k 0)))) (h "077j36qx29pqny1wrddn9iq01rj3xswr2rqiiw70ndbxlf6hj3wh")))

(define-public crate-tantivy-query-grammar-0.13.0 (c (n "tantivy-query-grammar") (v "0.13.0") (d (list (d (n "combine") (r "^4") (k 0)))) (h "0x8c03c0zfpnpmvlcgy5vf79v2kwaa8bgykxzk6g97ya4j13p82y")))

(define-public crate-tantivy-query-grammar-0.14.0 (c (n "tantivy-query-grammar") (v "0.14.0") (d (list (d (n "combine") (r "^4") (k 0)))) (h "16p15j6j6bkj9cpxgww7s4kc9qgc81j50xisypw5mk8ynf2l11kh")))

(define-public crate-tantivy-query-grammar-0.15.0 (c (n "tantivy-query-grammar") (v "0.15.0") (d (list (d (n "combine") (r "^4") (k 0)))) (h "1s872cysc8ycv2f1qj5lf2ra3y82miqjb2z3fdm2g6ra8wc04vj6")))

(define-public crate-tantivy-query-grammar-0.18.0 (c (n "tantivy-query-grammar") (v "0.18.0") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std"))) (k 0)))) (h "10xm9fkgc20hvxs16ls42frlaavangzsq9gf4jzdrf7jk77bsssx")))

(define-public crate-tantivy-query-grammar-0.19.0 (c (n "tantivy-query-grammar") (v "0.19.0") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std" "unicode"))) (k 0)))) (h "0cakzkhdrp8g4d74zpgzh0a62xlwrqhql3wnyr9hjj0w9kd3lgil")))

(define-public crate-tantivy-query-grammar-0.20.0 (c (n "tantivy-query-grammar") (v "0.20.0") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std" "unicode"))) (k 0)))) (h "15wphwsmmlv237mspvad1m8p61bc6867d816spyz190xmmw8yv8h")))

(define-public crate-tantivy-query-grammar-0.21.0 (c (n "tantivy-query-grammar") (v "0.21.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0xysz8iizcq9hcsand34lmyv3f1air9hg2qcdv4i1b0066hcaf8x")))

(define-public crate-tantivy-query-grammar-0.22.0 (c (n "tantivy-query-grammar") (v "0.22.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "10hbkm6mqa7lgx1alk16ark5cv3sf3qv9caakwq2xcspmza38x44")))

