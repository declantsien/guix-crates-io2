(define-module (crates-io ta nt tantivy_warc_indexer) #:use-module (crates-io))

(define-public crate-tantivy_warc_indexer-0.2.0 (c (n "tantivy_warc_indexer") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib-ng-compat"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tantivy") (r "^0.15") (d #t) (k 0)) (d (n "warc") (r "^0.2") (d #t) (k 0)))) (h "1nzbfn38y1kq69yg9gj7ciqz9acsfgbyfkpkshjllp9hvj65mjaf")))

