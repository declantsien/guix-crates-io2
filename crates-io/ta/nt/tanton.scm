(define-module (crates-io ta nt tanton) #:use-module (crates-io))

(define-public crate-tanton-1.0.0 (c (n "tanton") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2.10") (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mucow") (r "^0.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0p6cmbsdvkjg9k8pzq6v7q050r8v35kqckbyc2ndbkj54831lhrm") (f (quote (("nightly") ("default"))))))

