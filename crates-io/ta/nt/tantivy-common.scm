(define-module (crates-io ta nt tantivy-common) #:use-module (crates-io))

(define-public crate-tantivy-common-0.1.0 (c (n "tantivy-common") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1nc8xvav92ddi7xrq56lbanik2jrladxlq46yfilx3rj7q3l83kn")))

(define-public crate-tantivy-common-0.2.0 (c (n "tantivy-common") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "14s7pqnzvvzwxxcn3whnx3gzkq3jhjn3x0gccv6v4vp4qw9csy10")))

(define-public crate-tantivy-common-0.3.0 (c (n "tantivy-common") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0l4pzm9mpqjs420r1hq0hkv82lhpp1d9ki1vcphkm5pdndar3hfy")))

(define-public crate-tantivy-common-0.4.0 (c (n "tantivy-common") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1jq50xk4j7zggvqlm4s2anh50bms76myrn1cp6jgj3dn5ccg9zhl")))

(define-public crate-tantivy-common-0.5.0 (c (n "tantivy-common") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.5") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("serde-well-known"))) (d #t) (k 0)))) (h "0brlrs9s5x4sp2rcnl29fdkwapm8jv30770j5ybf1xy0dvfjzlg7")))

(define-public crate-tantivy-common-0.6.0 (c (n "tantivy-common") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.6") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("serde-well-known"))) (d #t) (k 0)))) (h "1qbich9b8474nvx7gx6ywqjs1rz10m2s01hidfx2ljk0bsbkljmg")))

(define-public crate-tantivy-common-0.7.0 (c (n "tantivy-common") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("serde-well-known"))) (d #t) (k 0)))) (h "1x110rp82l3yq2jmygbwz65v6yzm8bzi67j91cwa287xpk5f66c0")))

