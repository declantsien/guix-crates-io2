(define-module (crates-io ta nt tantivy-ik) #:use-module (crates-io))

(define-public crate-tantivy-ik-0.1.0 (c (n "tantivy-ik") (v "0.1.0") (d (list (d (n "ik-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.0") (d #t) (k 0)))) (h "0il07f4hy7yj2wscjvc0w5xxxa2jzv58zab5aspzm0x0arnimfv0")))

(define-public crate-tantivy-ik-0.3.0 (c (n "tantivy-ik") (v "0.3.0") (d (list (d (n "ik-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.0") (d #t) (k 0)))) (h "005rlsbnfxkighyrdj8shnnrv0c0myhx5ipc1y2lmcj68vdh704a")))

(define-public crate-tantivy-ik-0.3.1 (c (n "tantivy-ik") (v "0.3.1") (d (list (d (n "ik-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.0") (d #t) (k 0)))) (h "0a5vbzq5wyb7kh0ak2iapsxy1rc698n64zcyxpj40smlbmdcb9a6")))

(define-public crate-tantivy-ik-0.3.2 (c (n "tantivy-ik") (v "0.3.2") (d (list (d (n "ik-rs") (r "^0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.0") (d #t) (k 0)))) (h "0ykn2hk438fdz7nbg474vhngsddzvdm3axawws3mjfbs6qwppbbj")))

(define-public crate-tantivy-ik-0.3.3 (c (n "tantivy-ik") (v "0.3.3") (d (list (d (n "ik-rs") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.0") (d #t) (k 0)))) (h "02mbvca8bmxcmnj65a5241v1rp309ni7qzzrm2dcmply9gjcwnxq")))

(define-public crate-tantivy-ik-0.3.5 (c (n "tantivy-ik") (v "0.3.5") (d (list (d (n "ik-rs") (r "^0.3.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.0") (d #t) (k 0)))) (h "13m7zdakpr2jdvhmaf9gvyb76d1fnmdiniwgb7mmk709gj9qdxik")))

(define-public crate-tantivy-ik-0.5.0 (c (n "tantivy-ik") (v "0.5.0") (d (list (d (n "ik-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.0") (d #t) (k 0)))) (h "0ay6zrz45ccjnmdibqi17l81slhrxmz96ys0dpvn4l7yi0lqgqnm")))

(define-public crate-tantivy-ik-0.6.0 (c (n "tantivy-ik") (v "0.6.0") (d (list (d (n "ik-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.0") (d #t) (k 0)))) (h "1bwhl0w3l3idajkz3m8smn5vkqs1jmapcwm104rl2kgm30bs67r4")))

(define-public crate-tantivy-ik-0.6.1 (c (n "tantivy-ik") (v "0.6.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "ik-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("deadlock_detection" "hardware-lock-elision"))) (o #t) (d #t) (k 0)) (d (n "tantivy") (r "^0.21.1") (d #t) (k 0)) (d (n "tantivy-tokenizer-api") (r "^0.2.0") (d #t) (k 0)))) (h "0vdydy8vksg6wgfjx3jhznjrb2h3vb5s37pbb3vg9z8wvwh7m1n7") (f (quote (("use-std-sync") ("default" "use-parking-lot")))) (y #t) (s 2) (e (quote (("use-parking-lot" "dep:parking_lot"))))))

(define-public crate-tantivy-ik-0.6.2 (c (n "tantivy-ik") (v "0.6.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "ik-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("deadlock_detection" "hardware-lock-elision"))) (o #t) (d #t) (k 0)) (d (n "tantivy-tokenizer-api") (r "^0.2.0") (d #t) (k 0)))) (h "0krdmgl63an5cckaws6p0l5nwada0241q0lxgc45igv1zd5s1d6d") (f (quote (("use-std-sync") ("default" "use-parking-lot")))) (y #t) (s 2) (e (quote (("use-parking-lot" "dep:parking_lot"))))))

(define-public crate-tantivy-ik-0.7.0 (c (n "tantivy-ik") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "ik-rs") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("deadlock_detection" "hardware-lock-elision"))) (o #t) (d #t) (k 0)) (d (n "tantivy") (r "^0.21.0") (d #t) (k 0)) (d (n "tantivy-tokenizer-api") (r "^0.2.0") (d #t) (k 0)))) (h "10qwsaz632firinbxw746zn2wqsw3m19ssvmaxdvf58mycs1jzgm") (f (quote (("use-std-sync") ("default" "use-parking-lot")))) (s 2) (e (quote (("use-parking-lot" "dep:parking_lot"))))))

