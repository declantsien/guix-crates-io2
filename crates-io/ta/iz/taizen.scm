(define-module (crates-io ta iz taizen) #:use-module (crates-io))

(define-public crate-taizen-0.1.0 (c (n "taizen") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cursive") (r "^0.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "0sskznsp9sgv6xx6d97nm4kxss3aq3gc0vx29j40xai5bkk8g7kb")))

