(define-module (crates-io ta ig taiga) #:use-module (crates-io))

(define-public crate-taiga-0.0.1 (c (n "taiga") (v "0.0.1") (d (list (d (n "hyper") (r "^0.6.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1zrncjxgfx00hdf8kx404xkiq5igaaf07msl7a11jmqp7y2mg4j9")))

(define-public crate-taiga-0.0.2 (c (n "taiga") (v "0.0.2") (d (list (d (n "hyper") (r "^0.6.14") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_codegen") (r "*") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "*") (d #t) (k 1)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1a7n5rnbzlzrbln3qi9ssfhkgmzzdclj0746zlqdcrn651ycck4y") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

