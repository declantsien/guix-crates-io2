(define-module (crates-io ta pa tapa-trait-serde-derive) #:use-module (crates-io))

(define-public crate-tapa-trait-serde-derive-0.1.0 (c (n "tapa-trait-serde-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "07fdd35d0zr25lz35r151p0zv3kdb03rmxj5887hkjp25wdz80cr")))

(define-public crate-tapa-trait-serde-derive-0.1.1 (c (n "tapa-trait-serde-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1zgqqs758n7fimr4bs50imvaydysv3w7l5iikzmwyf4cks1yhxxg")))

(define-public crate-tapa-trait-serde-derive-0.1.2 (c (n "tapa-trait-serde-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1sm3gyza7gmvb0j40rcnbba5yz73735iqwc0rpdssgvrkbk2mjqx")))

(define-public crate-tapa-trait-serde-derive-0.2.0 (c (n "tapa-trait-serde-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1gv0hgv6h7z8knj230zd2y0fviv0ng9fk175nw22mnpvws2b570h")))

(define-public crate-tapa-trait-serde-derive-0.3.0 (c (n "tapa-trait-serde-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0r296sv11vnrpyy32lmnjbch2sm39byb5c3hlnv19m148v0aqch2")))

