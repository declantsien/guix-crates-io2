(define-module (crates-io ta pa tapa-cgloop-nats) #:use-module (crates-io))

(define-public crate-tapa-cgloop-nats-0.1.0 (c (n "tapa-cgloop-nats") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-nats") (r "^0.9.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0dh81zcqmlpwa1gsk7rcg3nx04l12varbjxlcf69h8nw3is8prjd") (y #t)))

(define-public crate-tapa-cgloop-nats-0.1.1 (c (n "tapa-cgloop-nats") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-nats") (r "^0.9.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1fvfx51vpkn2qvlgrrz39ss0dw55yk75cs6lfsvdhim6y17zr2pa") (y #t)))

(define-public crate-tapa-cgloop-nats-0.1.2 (c (n "tapa-cgloop-nats") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-nats") (r "^0.9.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0w2fwzm6bp6zl1ivgkcbi9d8f42aqc0kch85511vs4zjrsw5l47k") (y #t)))

(define-public crate-tapa-cgloop-nats-0.1.3 (c (n "tapa-cgloop-nats") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-nats") (r "^0.9.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "195k2an25hn3rwizpch730wdbv3i7xqidwzdviajq5q4v3zc8z7s") (y #t)))

(define-public crate-tapa-cgloop-nats-0.2.0 (c (n "tapa-cgloop-nats") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-nats") (r "^0.9.4") (d #t) (k 0)) (d (n "blocking") (r "^1.0.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nats") (r "^0.9.4") (d #t) (k 0)))) (h "1gnvifi6chh6ansn6ix5lpvz7anbpnrvsyagk2dn1a6b9729i5y4")))

(define-public crate-tapa-cgloop-nats-0.3.0 (c (n "tapa-cgloop-nats") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "blocking") (r "^1.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nats") (r "^0.16.0") (d #t) (k 0)))) (h "1d1bpad4wj2v499vxhs0gss44vk65qq8h0frwm0f24977y100f05")))

