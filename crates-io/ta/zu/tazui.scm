(define-module (crates-io ta zu tazui) #:use-module (crates-io))

(define-public crate-tazui-0.1.0 (c (n "tazui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "taz") (r "^0.1.0") (d #t) (k 0)))) (h "0yp9mq1wb70klnv4g9mb8k9m7hrxp1kqbsk9npnbm7mk9i2nswin")))

(define-public crate-tazui-0.1.1 (c (n "tazui") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "taz") (r "^0.1.0") (d #t) (k 0)))) (h "1grsp7sl88j9w902xd7193x16bk01clb0llcld4bws2rlixw6g55")))

