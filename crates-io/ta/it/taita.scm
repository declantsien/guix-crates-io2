(define-module (crates-io ta it taita) #:use-module (crates-io))

(define-public crate-taita-0.1.0 (c (n "taita") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1l1g5cd9v4sryr4sqkpx53z9ba7qz39r3kfjw7z6ql6i4ny0avr0")))

(define-public crate-taita-0.1.1 (c (n "taita") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1zjv9w161mw9qnqf7jglp61xsf8frpj5s4b5p42074zis2g565n2")))

(define-public crate-taita-0.1.2 (c (n "taita") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "00gv6qhcg8y8jqydbsqk942ddd549470z2zbcmiqs9cj0abhxwy1")))

(define-public crate-taita-0.1.3 (c (n "taita") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1s370giqf9hp0bppdv3v56y4jvjfhv134fzcdagfacrn749rmr76")))

(define-public crate-taita-0.1.4 (c (n "taita") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "190ani9qzrxhzmn4aqi7bmj5mlxxl69i0xkw8ll3i4vgxcz2dh0c")))

(define-public crate-taita-0.1.5 (c (n "taita") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "19w8303kzl9nvh5awsmwalp4mhfq1iqb9i3p1nda9xdwnl1r9vb0")))

(define-public crate-taita-0.1.6 (c (n "taita") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "07aw4xbs2984vy02qzii05vs889yz52fmd57frvqpxjfnpzpnn7m")))

(define-public crate-taita-0.1.7 (c (n "taita") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1fs3kdff00ia1jdn3fsnab3zkq7w5rvw8a9c2sdi6hxzndnyajvp")))

