(define-module (crates-io ta st tasty-ntree) #:use-module (crates-io))

(define-public crate-tasty-ntree-0.1.0 (c (n "tasty-ntree") (v "0.1.0") (h "0i05mv0lcbr6yb61zsxlwqlra7clw23zvhdg0zjc8pp7if7f11nc") (f (quote (("display") ("debug"))))))

(define-public crate-tasty-ntree-0.2.0 (c (n "tasty-ntree") (v "0.2.0") (h "00991yb64pn507jpddkn3pgx9y7b68igvm1mjszksqxryi9kwlzc") (f (quote (("display") ("debug"))))))

