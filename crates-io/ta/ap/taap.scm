(define-module (crates-io ta ap taap) #:use-module (crates-io))

(define-public crate-taap-0.1.0 (c (n "taap") (v "0.1.0") (h "0cb2iibjd8ahx0flm95rgz61203cxysi9kzsi9mkl1wjk7j8npkf")))

(define-public crate-taap-0.1.1 (c (n "taap") (v "0.1.1") (h "00b67m126aybv4byayincrx8xa1g7kf9m7xck3wjm9fvc6zsi565")))

(define-public crate-taap-0.1.2 (c (n "taap") (v "0.1.2") (h "0p969yrzavi0kprfyal6f9fjkb77hqdr6xq74bxyxviapwhk6zvi") (y #t)))

(define-public crate-taap-0.1.3 (c (n "taap") (v "0.1.3") (h "074ig3hjifcgbc41flyblp0wy77qsp53iqxjbgw0f5217pkhmac2")))

(define-public crate-taap-0.1.4 (c (n "taap") (v "0.1.4") (h "0lyj054i2w8ddmlga97lbzvy9clapnvjs29krxbmmnn1q7bh9jaa")))

