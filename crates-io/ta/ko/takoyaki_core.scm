(define-module (crates-io ta ko takoyaki_core) #:use-module (crates-io))

(define-public crate-takoyaki_core-1.2.0 (c (n "takoyaki_core") (v "1.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colorsys") (r "^0.6.7") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "133jkxb40qlkdzckb40v1af5j341ggr9130fcpsykyhrysfmd6a1")))

