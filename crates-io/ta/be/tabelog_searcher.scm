(define-module (crates-io ta be tabelog_searcher) #:use-module (crates-io))

(define-public crate-tabelog_searcher-0.1.0 (c (n "tabelog_searcher") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ykaqhwpapd13bcqib17abgwsjd5hjqsjpsa46pycf21xq03bx4h")))

