(define-module (crates-io ta x_ tax_ids) #:use-module (crates-io))

(define-public crate-tax_ids-0.1.0 (c (n "tax_ids") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (o #t) (d #t) (k 0)))) (h "0f63p3d8ilx7rafc5slrjhdzsnfs41mv019a8zi3ql902kgkfn8d") (f (quote (("no_vat" "toml") ("gb_vat") ("eu_vat" "roxmltree") ("default" "eu_vat") ("ch_vat" "roxmltree"))))))

