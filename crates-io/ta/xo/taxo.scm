(define-module (crates-io ta xo taxo) #:use-module (crates-io))

(define-public crate-taxo-0.1.0 (c (n "taxo") (v "0.1.0") (d (list (d (n "docopt") (r "~0.6.78") (d #t) (k 0)) (d (n "glob") (r "~0.2.11") (d #t) (k 0)) (d (n "regex") (r "~0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "0j7r76zd4bslch7nsn7hbzrgpa1nvyh6pf9r2fpqjfmxvkc2hg6j")))

