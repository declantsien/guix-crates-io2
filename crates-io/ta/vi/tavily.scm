(define-module (crates-io ta vi tavily) #:use-module (crates-io))

(define-public crate-tavily-0.1.0 (c (n "tavily") (v "0.1.0") (h "124zpm99zsr164dpl2218s43amjnl7h52ii0wsvvz59bx97h664x")))

(define-public crate-tavily-0.2.0 (c (n "tavily") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ms80cqyysz7xj66359i2gbyas4m7aba71f4vm8n34r0zc598jin")))

(define-public crate-tavily-0.2.1 (c (n "tavily") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "04v54wbqm98gxiyz4a96042dxm58wzd3vk1kv9kcpvg1cssjsp1f")))

(define-public crate-tavily-0.2.2 (c (n "tavily") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fzwqykar5f7yxgd670fm85ab66fmd47bjm9qwqrn6398wwp346g")))

(define-public crate-tavily-1.0.0 (c (n "tavily") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bjfzis5w0jqslwmlflkqd35p248ln05pxkv538g5x8szmay1655")))

