(define-module (crates-io ta gg tagged) #:use-module (crates-io))

(define-public crate-tagged-0.1.0 (c (n "tagged") (v "0.1.0") (h "0paqg0mmb195yvid4zxc9qrsdpl8d4nw6kp9jf7r7k5av6krn37j")))

(define-public crate-tagged-0.1.1 (c (n "tagged") (v "0.1.1") (h "022n1jhkvh7c13w66iyw1gj80g98b2lzvhrns2psc58hyfs7c8av")))

