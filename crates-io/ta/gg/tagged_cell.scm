(define-module (crates-io ta gg tagged_cell) #:use-module (crates-io))

(define-public crate-tagged_cell-0.1.0 (c (n "tagged_cell") (v "0.1.0") (h "0bpn82shnf82ck9ffqpmbb4vhsj9blncs5xxczb45qd4kcsvb80f")))

(define-public crate-tagged_cell-0.1.1 (c (n "tagged_cell") (v "0.1.1") (h "1v8jg1zzcidllvxbbxi5a4ibxf1fa5ycbn8hccgynhzqwpv567ca")))

(define-public crate-tagged_cell-0.1.2 (c (n "tagged_cell") (v "0.1.2") (h "1jf5kavssxrva5cq3dy8bsh75m1akb4cbjkjxzjwajzxj710r2kw")))

(define-public crate-tagged_cell-0.1.3 (c (n "tagged_cell") (v "0.1.3") (h "1nr9r2h26rlhiaja3252y98lhh4cvxmzw7dn0k84482gm661dz2w")))

