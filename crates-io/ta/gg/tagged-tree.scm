(define-module (crates-io ta gg tagged-tree) #:use-module (crates-io))

(define-public crate-tagged-tree-0.1.0 (c (n "tagged-tree") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 2)) (d (n "mockall") (r "^0.10.2") (d #t) (k 2)))) (h "0pjrjq502955kg8nd96qn20gn9d91azp7vr587g5vv12vayg9bln")))

(define-public crate-tagged-tree-0.2.0 (c (n "tagged-tree") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 2)) (d (n "mockall") (r "^0.10.2") (d #t) (k 2)))) (h "0sp8c3xp3dvxmq1z8i963hkbwcbv38b3i2zz3h0pyb3h8hxxbqjl")))

(define-public crate-tagged-tree-0.3.0 (c (n "tagged-tree") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 2)) (d (n "mockall") (r "^0.10.2") (d #t) (k 2)))) (h "1g0b4d5rfwqwymhili1rfvsdi7hsc6acs2vdz1f5zan4rh4xvba1")))

(define-public crate-tagged-tree-0.4.0 (c (n "tagged-tree") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 2)) (d (n "mockall") (r "^0.10.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("std" "derive"))) (o #t) (k 0)))) (h "0fd5x5x0ddh3611smllwr8pps5nmkn7zb1p65p0d177zgyrg0w8h")))

