(define-module (crates-io ta gg tagged-serde) #:use-module (crates-io))

(define-public crate-tagged-serde-0.1.0 (c (n "tagged-serde") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yj6i559x6ijbiari63351riwb6313xc4zdzsbnjd7sbhq7xj9qg")))

