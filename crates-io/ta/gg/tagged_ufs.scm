(define-module (crates-io ta gg tagged_ufs) #:use-module (crates-io))

(define-public crate-tagged_ufs-0.1.0 (c (n "tagged_ufs") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1bxrh82w53r1z5qq3hyxfkazki3rajr85n73iishl650r23sm76k")))

