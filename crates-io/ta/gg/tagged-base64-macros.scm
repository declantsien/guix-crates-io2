(define-module (crates-io ta gg tagged-base64-macros) #:use-module (crates-io))

(define-public crate-tagged-base64-macros-0.3.2 (c (n "tagged-base64-macros") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09i8vsk2798ylsksvsflhhic4i9jgj2jbl6qnji9grkcyc13ck7q") (f (quote (("serde") ("default" "serde"))))))

(define-public crate-tagged-base64-macros-0.3.3 (c (n "tagged-base64-macros") (v "0.3.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12bm1z4dxx6fygzlir4whcw5lb84v0mf4b0qd7cs6vhbyjb6lqvi") (f (quote (("serde") ("default" "serde"))))))

(define-public crate-tagged-base64-macros-0.4.0 (c (n "tagged-base64-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b45mr48ym9w2qwhjmxsap27ykr6qcz1fshryz9gm6082p0a7pmw") (f (quote (("serde") ("default" "serde"))))))

