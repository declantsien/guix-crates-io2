(define-module (crates-io ta gg tagged-box) #:use-module (crates-io))

(define-public crate-tagged-box-0.1.0 (c (n "tagged-box") (v "0.1.0") (h "16mhmh70ckhqmnavrwxfkg75lyrcdw4c8lyqvnsmfd01f5z82k7w") (f (quote (("default" "48bits") ("57bits") ("56bits") ("55bits") ("54bits") ("53bits") ("52bits") ("51bits") ("50bits") ("49bits") ("48bits"))))))

(define-public crate-tagged-box-0.1.1 (c (n "tagged-box") (v "0.1.1") (h "1h4dbpbcj8n5iw0nk4bnca47b6b8xhivnrgxzmnmhkbfkdrlp77y") (f (quote (("default" "48bits") ("57bits") ("56bits") ("55bits") ("54bits") ("53bits") ("52bits") ("51bits") ("50bits") ("49bits") ("48bits"))))))

