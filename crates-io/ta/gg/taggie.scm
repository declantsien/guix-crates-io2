(define-module (crates-io ta gg taggie) #:use-module (crates-io))

(define-public crate-taggie-0.1.0 (c (n "taggie") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "taglib") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 0)))) (h "0y6f1906brfgnhk1i78ddy0zlmyl02z87cx98f80y2lq403qrc0m")))

