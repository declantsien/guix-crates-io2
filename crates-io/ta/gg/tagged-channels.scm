(define-module (crates-io ta gg tagged-channels) #:use-module (crates-io))

(define-public crate-tagged-channels-0.0.1 (c (n "tagged-channels") (v "0.0.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "async-stream") (r "^0.3") (d #t) (k 2)) (d (n "axum") (r "^0.6") (f (quote ("ws"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "154va7lmp35494vmqqani1rrbzindgc75jjaqjk6jp0cmcm2hpb5")))

