(define-module (crates-io ta gg tagged-hybrid) #:use-module (crates-io))

(define-public crate-tagged-hybrid-0.1.0 (c (n "tagged-hybrid") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tap") (r "^1.0") (d #t) (k 0)))) (h "1g26qinfb388rqn1dxqw3fvip49nywpwyfim6kdp7mr13m1gx4yb")))

