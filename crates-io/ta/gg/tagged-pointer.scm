(define-module (crates-io ta gg tagged-pointer) #:use-module (crates-io))

(define-public crate-tagged-pointer-0.1.0 (c (n "tagged-pointer") (v "0.1.0") (d (list (d (n "typenum") (r "^1.13") (d #t) (k 0)))) (h "0sw05yv4r3ws0yv1asnhgbl15bpn87n5ynnwszl7niscrf90idgn") (f (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.1.1 (c (n "tagged-pointer") (v "0.1.1") (d (list (d (n "typenum") (r "^1.13") (d #t) (k 0)))) (h "0dg825nhc4hrvz53pnilcbq679arvxx17q13ipb7lf8cqg3d96c2") (f (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.1.2 (c (n "tagged-pointer") (v "0.1.2") (d (list (d (n "typenum") (r "^1.13") (d #t) (k 0)))) (h "135a0dha8sb40ypncsb0dz4r0grdj08303np9s5ncbhjwwwa2g30") (f (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.1.3 (c (n "tagged-pointer") (v "0.1.3") (d (list (d (n "typenum") (r "^1.13") (d #t) (k 0)))) (h "1jnx6h6y9cwk3vvvf6kr9n17n4g0rv280iy3z54mpric4i4ap9w8") (f (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.2.0 (c (n "tagged-pointer") (v "0.2.0") (h "1qay7qbx2x3khn52kpv2g5sxrjmb1k16qq51vq52f7qgmrfx237a") (f (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.2.1 (c (n "tagged-pointer") (v "0.2.1") (h "0a3mx45g2g8blnrfl20lbr68fqd8ybkyc26aizyf0whscq90v6bq") (f (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.2.2 (c (n "tagged-pointer") (v "0.2.2") (h "0bcx3y3rwxslnvg283pmycd3fhn582iw76x3473v060ljbvrmfl6") (f (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.2.3 (c (n "tagged-pointer") (v "0.2.3") (h "05ff60453a94ln9yahypyarrlkcr5nkj9d7ss1j7ll10d6j2x7d0") (f (quote (("fallback")))) (r "1.51")))

(define-public crate-tagged-pointer-0.2.4 (c (n "tagged-pointer") (v "0.2.4") (h "0ha8138v8cskj21rv04f9jdrphl4jld6lk1c3j2rsk78ar66gbf4") (f (quote (("fallback")))) (r "1.51")))

(define-public crate-tagged-pointer-0.2.5 (c (n "tagged-pointer") (v "0.2.5") (h "1h40i0ww57665gkrlcgm1njvnrvmjczaryg214z14jrlm5mpyqsp") (f (quote (("fallback")))) (r "1.51")))

(define-public crate-tagged-pointer-0.2.6 (c (n "tagged-pointer") (v "0.2.6") (h "0mp6iiqqbh4372pj5zsi18y7rg6gdpvlbv6jjlakcnszb5kn3kmk") (f (quote (("fallback")))) (r "1.51")))

(define-public crate-tagged-pointer-0.2.7 (c (n "tagged-pointer") (v "0.2.7") (d (list (d (n "compiletest_rs") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0bpimmrkci67c62p795a5p5wq3g9ggdhx5zrqwfvf5jlzdssgbi1") (f (quote (("fallback")))) (r "1.51")))

