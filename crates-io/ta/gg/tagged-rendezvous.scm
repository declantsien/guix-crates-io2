(define-module (crates-io ta gg tagged-rendezvous) #:use-module (crates-io))

(define-public crate-tagged-rendezvous-0.1.0 (c (n "tagged-rendezvous") (v "0.1.0") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "murmur3") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (o #t) (d #t) (k 0) (p "rayon")))) (h "1v9d7gcqyvyfjmmlszxzxxx7hl9qvpdcjb66r6s104phkimq8yw1") (f (quote (("rayon" "rayon_crate" "dashmap/rayon"))))))

(define-public crate-tagged-rendezvous-0.1.1 (c (n "tagged-rendezvous") (v "0.1.1") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "murmur3") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon_crate") (r "^1") (o #t) (d #t) (k 0) (p "rayon")))) (h "19as1hgvpz0kv4pv6ppf358qbww82krbs8ivd9h5dv6mp7m690f8") (f (quote (("rayon" "rayon_crate" "dashmap/rayon"))))))

