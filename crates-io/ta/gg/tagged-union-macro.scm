(define-module (crates-io ta gg tagged-union-macro) #:use-module (crates-io))

(define-public crate-tagged-union-macro-0.0.1 (c (n "tagged-union-macro") (v "0.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "full" "extra-traits"))) (d #t) (k 0)))) (h "15hzkyg0pjzv3640qad9pvlingaq6zd6cqq81chs0r9hvjhf4kzb")))

