(define-module (crates-io ta gg tagger) #:use-module (crates-io))

(define-public crate-tagger-0.1.0 (c (n "tagger") (v "0.1.0") (h "04mw1nh0l2ci2dwymfy7hn7prm6m9dacrvw8fwysszi57b5gqq2c")))

(define-public crate-tagger-0.2.0 (c (n "tagger") (v "0.2.0") (h "1bipkj4pcdva7ma7x9jn08lm1p914716za9hhbqpqbszdbw5bd6g")))

(define-public crate-tagger-0.2.1 (c (n "tagger") (v "0.2.1") (h "1k0daq8kn5cjjxlplgvqw91ngqd3vk7gpippsx5smm2rl1wbf2x0")))

(define-public crate-tagger-0.3.0 (c (n "tagger") (v "0.3.0") (h "02k1djb316wdc98d8y4dd8k056mr8z61rnz9fwi9f129lbzwvzmd")))

(define-public crate-tagger-0.3.1 (c (n "tagger") (v "0.3.1") (h "1gbizmai0myvlqqwajz5g52s9akipyl0m9f5ri72582r6mwdhmvh")))

(define-public crate-tagger-0.3.2 (c (n "tagger") (v "0.3.2") (h "0c4jsxpzlvww7ajgbsp8lh07dfrp85i3ahkw88dw6fadx1y3ald5")))

(define-public crate-tagger-0.4.0 (c (n "tagger") (v "0.4.0") (h "0b5sq5zdvqlrqvc92kvpy058z6hyhrxivh35vgj4fvyq5pxb5ija")))

(define-public crate-tagger-0.4.1 (c (n "tagger") (v "0.4.1") (h "03j1lnkk4yi7nhdsyfw37ghh7vj07gkcyxy2s7dpw58b15k1wbl1")))

(define-public crate-tagger-0.4.2 (c (n "tagger") (v "0.4.2") (h "1w6pmm28imn9mjxj9z1nyjrrza9sgyv2vphk4rbgd31wh4yal1s4")))

(define-public crate-tagger-0.4.3 (c (n "tagger") (v "0.4.3") (h "13qdh2p1w27bzym7z584s9rp2l5imszbcvbrciqb3bahb7mn694n")))

(define-public crate-tagger-0.4.5 (c (n "tagger") (v "0.4.5") (h "0g6nlrsgjib3kl359rrc3lvsk4q4fmaycq9rinak53pns37xi30z")))

(define-public crate-tagger-0.4.6 (c (n "tagger") (v "0.4.6") (h "10db871ivm8xzg01f3pzm52ija8mjn7wddb2r2skx4nx3b57z8dd")))

(define-public crate-tagger-0.5.0 (c (n "tagger") (v "0.5.0") (h "0mfr5n3xzybb0rk9qc854xq9b6apvh5w9mia6h96m0zj9dfcq4xx")))

(define-public crate-tagger-0.6.0 (c (n "tagger") (v "0.6.0") (h "1bv1amsn1pfsxz5l4237yzc3gdz9xc8rq0lz1wx4zgaln09mirx7")))

(define-public crate-tagger-0.6.1 (c (n "tagger") (v "0.6.1") (h "17sdp7b0g6q5zdm3sy7ycfy7cc81y94manyn7hssn7s3r2hh3xll")))

(define-public crate-tagger-0.6.2 (c (n "tagger") (v "0.6.2") (h "0m6bwa81kzq7sxy9as8j4grrj1a7nd4hgw6lvw6j5grv4bg6bsm6")))

(define-public crate-tagger-0.7.0 (c (n "tagger") (v "0.7.0") (h "16lc2y1xh9nnn58y5yx95bfzlyk1praik9p6yikjvnn81fl29wjs")))

(define-public crate-tagger-0.7.1 (c (n "tagger") (v "0.7.1") (h "07h2vbi56x8f2hj97yis3x7sa3q25fv05l16jjihi546j3fd3kqa")))

(define-public crate-tagger-1.0.0 (c (n "tagger") (v "1.0.0") (h "1rwpmw56yhkarldgj0r5vjqvqw03dbny3g62pssdd85idz18kmiz")))

(define-public crate-tagger-1.0.1 (c (n "tagger") (v "1.0.1") (h "1bd97p2a0y304324r9w03vqig23wwx8w4aj6d3l3aszg5631alv5")))

(define-public crate-tagger-2.0.0 (c (n "tagger") (v "2.0.0") (h "00ba7idndmdshnvy3rsjxwzs3k695nx7vpkfsrhr78m3b0qysysw") (y #t)))

(define-public crate-tagger-2.0.1 (c (n "tagger") (v "2.0.1") (h "0kxqa8wfh7rh2y5czy1fqr8zpvgdc1bccw7msh5d5p2c7caw44i1") (y #t)))

(define-public crate-tagger-2.1.0 (c (n "tagger") (v "2.1.0") (h "1zpl51886ak0l0cc3pxajvia4qrj0h90dimp0hf2w864vdyq0h0s") (y #t)))

(define-public crate-tagger-2.1.1 (c (n "tagger") (v "2.1.1") (h "0dn6wrk72pz6mpgdhy4ymqq43wmpvh914nvqjhjpygzlvsfa1vf8") (y #t)))

(define-public crate-tagger-2.1.2 (c (n "tagger") (v "2.1.2") (h "10za5w5md0z2cik2wf8h0292zyybgglfwprqh4q22n041r8sl92d")))

(define-public crate-tagger-2.2.0 (c (n "tagger") (v "2.2.0") (h "08c3jrq7zd1s598lzf246kjqj7jynr0wjaqyxaha18vqsw759kyh")))

(define-public crate-tagger-2.2.1 (c (n "tagger") (v "2.2.1") (h "0k82kfbj1dxnxcx1xcvhnzfi82n4m6ik6pybj32wh7l91lx871nj")))

(define-public crate-tagger-3.0.0 (c (n "tagger") (v "3.0.0") (h "0n06q5z1yyzd0jj9wmjm3k7bskavzvjh5xsyilyacgav891hzbia") (y #t)))

(define-public crate-tagger-3.0.1 (c (n "tagger") (v "3.0.1") (h "1wwcfs01lqis8m1a499sk4g39vcfrizk3vb1a6gg5x4sf6jl6zyb") (y #t)))

(define-public crate-tagger-3.0.2 (c (n "tagger") (v "3.0.2") (h "1gmffhws6vyrb07k5wd0q2smh9zx6gc1j364scndmqkp0zbn8vcf") (y #t)))

(define-public crate-tagger-3.0.4 (c (n "tagger") (v "3.0.4") (h "0zp8klvvm4gr780gl0php0ncbjng3dr5iz08qfkzalzrc8c7dm00") (y #t)))

(define-public crate-tagger-3.0.5 (c (n "tagger") (v "3.0.5") (h "1k9b165a79jw7l5i567iricnvw81xbh50i0am3hz72baazvzpjqn") (y #t)))

(define-public crate-tagger-3.0.6 (c (n "tagger") (v "3.0.6") (h "17g0jbmyiwy1fnkx1dpf0nxdsddqcy61r021i94p6qx7qz086dzz") (y #t)))

(define-public crate-tagger-3.1.0 (c (n "tagger") (v "3.1.0") (h "0ahy8m9nnj8srvx13i5l3hsqr9q4n7r891bxaxnvkb1pkllagvvr") (y #t)))

(define-public crate-tagger-3.1.1 (c (n "tagger") (v "3.1.1") (h "0sgvcif0qzchb9v678gbryhblp4zwvy8nvvw4f43iw6qsp8q7fcj") (y #t)))

(define-public crate-tagger-3.1.2 (c (n "tagger") (v "3.1.2") (h "0w45vl3c5174n2n4kn2nwp6r6qnwcvq8532xnk6s65a3vivim5v0")))

(define-public crate-tagger-3.2.0 (c (n "tagger") (v "3.2.0") (h "1anms5pi0m2xjd2wiff9fxahsygfpci0s5nqvi6n4hxvm8yhidry")))

(define-public crate-tagger-3.2.1 (c (n "tagger") (v "3.2.1") (h "0kyja69b9spzc6mc28vclgv0yz13mlskwms97w2rnl3d34kfccy9")))

(define-public crate-tagger-3.3.0 (c (n "tagger") (v "3.3.0") (h "0riy8ykkrmghq963zzk2vzi8idszq1nplpd6vrhaj2s9ixcbbgby") (y #t)))

(define-public crate-tagger-3.3.1 (c (n "tagger") (v "3.3.1") (h "117c2qc5sk76825pvmqpv2x76yc1brlcnw33sfy69zwg57whd4rs") (y #t)))

(define-public crate-tagger-3.3.2 (c (n "tagger") (v "3.3.2") (h "04zbvvixxc9j3yswvh12lzqwi5j14ay4yrcqlp3m3nxi10caagfv") (y #t)))

(define-public crate-tagger-4.0.0 (c (n "tagger") (v "4.0.0") (h "03a5grrgsxkwqr9m9jxyq5818xri4fcir9sh6ccxa5iv0h48n98z") (y #t)))

(define-public crate-tagger-4.0.1 (c (n "tagger") (v "4.0.1") (h "0k55209nzmsjqw64vgfavi97cvgyg5k1m36yv4nrv6r7npjkfrbv") (y #t)))

(define-public crate-tagger-4.1.0 (c (n "tagger") (v "4.1.0") (h "1d56pp5z14csqaiybqfacs5mdj0hha099cfrhy82sq1rzcz78g30") (y #t)))

(define-public crate-tagger-4.2.0 (c (n "tagger") (v "4.2.0") (h "1gq6vykl8idix7hm31335y1jsr2yv3vjgjq4blgjl7agk3wa34wi")))

(define-public crate-tagger-4.2.1 (c (n "tagger") (v "4.2.1") (h "0pl3gc1hgznl7hxnyakb6s3fv5qhsr7r1aqflfjwjxp5pb0qsjvq")))

(define-public crate-tagger-4.3.1 (c (n "tagger") (v "4.3.1") (h "0x20zwxj1nhrq2fd3k1zq9x7jbsv2gc2dz5hpc1rzcv19cm92la8")))

(define-public crate-tagger-4.3.2 (c (n "tagger") (v "4.3.2") (h "1i6mai98gvlp4z3fzv062zxazigacs7nsgdbghwkxdgmf3w06msp")))

(define-public crate-tagger-4.3.3 (c (n "tagger") (v "4.3.3") (h "08dj0hg547avw32gcqm1gdfflwwyxwf27v106shs62yjgpy7ipbp")))

(define-public crate-tagger-4.3.4 (c (n "tagger") (v "4.3.4") (h "0qsbsfml1adidq85x3ilaqba6xavy65ryvi8s16aw7axcifnzaka")))

(define-public crate-tagger-4.3.5 (c (n "tagger") (v "4.3.5") (h "1if3chr1nhsdd6z09vi1x3cpxcrkjfi676z4n438b6nysrj9yk09")))

