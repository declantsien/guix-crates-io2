(define-module (crates-io ta gm tagmap) #:use-module (crates-io))

(define-public crate-tagmap-0.0.1 (c (n "tagmap") (v "0.0.1") (d (list (d (n "eva-common") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "0x6ks85ra0c583ca5hqdf8dw4s04kfzj8kf5wn96n1firnjgpq4r")))

(define-public crate-tagmap-0.0.2 (c (n "tagmap") (v "0.0.2") (d (list (d (n "eva-common") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "0r25jcd3j36nz22arijk6bsim2w9qjia2x18z3ndmc0lhmlcfzsq")))

(define-public crate-tagmap-0.0.3 (c (n "tagmap") (v "0.0.3") (d (list (d (n "eva-common") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "1jil3nn46njabp06mhfzr24bzrwz03akcaszk9ykafnfmcnpgizn")))

(define-public crate-tagmap-0.0.4 (c (n "tagmap") (v "0.0.4") (d (list (d (n "eva-common") (r "^0.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "085xcajvlinhd5z2qwcj65znjsc1xnmppm9cicdk2mpz1vrv9qgf")))

(define-public crate-tagmap-0.0.5 (c (n "tagmap") (v "0.0.5") (d (list (d (n "eva-common") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ddxx98rxilbbxg6dycvy4k4bzw0g84r6h51h2p8q0rxzfvqhrbr")))

(define-public crate-tagmap-0.0.6 (c (n "tagmap") (v "0.0.6") (d (list (d (n "eva-common") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n87b162gpyil14cpskk0wqcl9qwyqjpvhx90jz02ga0qlddmj9r")))

(define-public crate-tagmap-0.0.7 (c (n "tagmap") (v "0.0.7") (d (list (d (n "eva-common") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hvm1w88d7d10mfqqm2cv3dm830v1x9rd1a0vavc1b41n02n1qw6")))

(define-public crate-tagmap-0.0.8 (c (n "tagmap") (v "0.0.8") (d (list (d (n "eva-common") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vyzv3zw2ajja40q0lgw2gi2sdai7iv7r8n9iivz3lygd4r2fwr9")))

(define-public crate-tagmap-0.0.9 (c (n "tagmap") (v "0.0.9") (d (list (d (n "eva-common") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "0znyfyysmrv1mm1p99p7sr8ahdvnzzpnvz49x045gq18l9g819n2")))

