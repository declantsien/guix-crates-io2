(define-module (crates-io ta co tacoda_grrs) #:use-module (crates-io))

(define-public crate-tacoda_grrs-0.1.0 (c (n "tacoda_grrs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "0qv645w5vwdskrqjc4y8i25a83f2dlw9sg0pfcvrfmkkyhdcqw72")))

(define-public crate-tacoda_grrs-0.1.1 (c (n "tacoda_grrs") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0kpapxgv092xrpjrgcz4c2gb6n48v913l6krd7zpf47rs6wfzall")))

(define-public crate-tacoda_grrs-0.1.2 (c (n "tacoda_grrs") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1ivbbj28x3gmxhfwykf0pkrs4ypzcvfwcaicclhk0cl4zgn52cbr")))

(define-public crate-tacoda_grrs-0.1.3 (c (n "tacoda_grrs") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "097ad5dbs9ljskcis49kx427012sszrgffzlygazrdlx8qbr7bra")))

