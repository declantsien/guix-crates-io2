(define-module (crates-io ta bs tabs) #:use-module (crates-io))

(define-public crate-tabs-0.1.0 (c (n "tabs") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cb4d7nmqv29adv3044axp3r8i63x54frmlvr8wgbyb6ng4a3jil")))

(define-public crate-tabs-0.1.1 (c (n "tabs") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ic263h53klz86qjwyp3vwv4dp43qqyfdj6fprnnav6ixqlqf5gm")))

