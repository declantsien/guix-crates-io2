(define-module (crates-io ta bs tabset) #:use-module (crates-io))

(define-public crate-tabset-0.1.0 (c (n "tabset") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("color"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "0hgaxy1ciksmd4r4jp0hh9d856r7da33inz9pb8pg5g3bq0hza15")))

