(define-module (crates-io ta p_ tap_parser) #:use-module (crates-io))

(define-public crate-tap_parser-0.1.0 (c (n "tap_parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)))) (h "0m4dqvgrm2h88x1clhkcp059pvr1xqjhngi816zidfn8rf5qlz0c")))

(define-public crate-tap_parser-0.1.1 (c (n "tap_parser") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)))) (h "1qcfgplizg9h3izlg4irqi5fziijd60qgpmi5671czadf2nwp49w")))

