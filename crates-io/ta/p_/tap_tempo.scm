(define-module (crates-io ta p_ tap_tempo) #:use-module (crates-io))

(define-public crate-tap_tempo-0.1.0 (c (n "tap_tempo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0i2qb93dw6wn3a64dicxcwr6h2bi58x961y2bb34qcvbwd5jgf4v")))

(define-public crate-tap_tempo-0.1.1 (c (n "tap_tempo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1mz277ywim7blg646ks54lgkjhrkln9czqvxrwjid8yk0qvliqs1")))

