(define-module (crates-io ta rp tarpit-log-parser) #:use-module (crates-io))

(define-public crate-tarpit-log-parser-0.1.0 (c (n "tarpit-log-parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "0vi9g90zr8r6942dg2yn8dlz7g698zkr0y8awa0cgmibs85lf0kl")))

(define-public crate-tarpit-log-parser-0.2.0 (c (n "tarpit-log-parser") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1qywrslsh32xj4ii0nwcxhrw9yd51q89fjyhmv1c7b4li9xx79rr")))

(define-public crate-tarpit-log-parser-0.2.1 (c (n "tarpit-log-parser") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "0rk4siiw3ifp29ww1cl7kiz6m8ni65wjad9xgjwf5qq4imslpl6a")))

