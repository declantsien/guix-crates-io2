(define-module (crates-io ta rp tarpc-trace) #:use-module (crates-io))

(define-public crate-tarpc-trace-0.1.0 (c (n "tarpc-trace") (v "0.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04h7gl338cn50q5bg6qshm78gxwmnq3v050y7ili6mgidks3l9vz")))

(define-public crate-tarpc-trace-0.2.0 (c (n "tarpc-trace") (v "0.2.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jygqsp0srr4gq813yd2p2i7vn1vqijwaik9cnhll1v2nrw3qqly")))

(define-public crate-tarpc-trace-0.3.0 (c (n "tarpc-trace") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02mfy2fzys6l2wgpa97y0rvlnxxg4fmnamxk9zzyma8j32vyvrbv")))

