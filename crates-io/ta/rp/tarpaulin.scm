(define-module (crates-io ta rp tarpaulin) #:use-module (crates-io))

(define-public crate-tarpaulin-0.1.0 (c (n "tarpaulin") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "tarpc") (r "^0.9.0") (d #t) (k 0)) (d (n "tarpc-plugins") (r "^0.2.0") (d #t) (k 0)))) (h "1bcxxslkw2idbhzwn0vrzv30q61h9f66a4dkl4fk5p8zaad7qlkp")))

