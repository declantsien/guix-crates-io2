(define-module (crates-io ta rp tarpit) #:use-module (crates-io))

(define-public crate-tarpit-0.0.1 (c (n "tarpit") (v "0.0.1") (h "1r78iiangcwkxqx1wakn19hvcbxaf7lsgpan2hjmlnq38z3iz6j5") (y #t)))

(define-public crate-tarpit-0.1.1 (c (n "tarpit") (v "0.1.1") (h "1r43p6agki90ydqhhaj044kia273cmrksbcvldmis3l1rfn2xq0w") (y #t)))

(define-public crate-tarpit-0.1.3 (c (n "tarpit") (v "0.1.3") (h "1hn3cd4d5pbaf5yk5brj2fkqi7z7zcnc2h6csmjdymn9hwzj46pp") (y #t)))

(define-public crate-tarpit-0.1.4 (c (n "tarpit") (v "0.1.4") (h "1smginlljd16wrijby0f0831lfziw2n4q8ll28igg2vjlygm6s8p") (y #t)))

(define-public crate-tarpit-0.1.77 (c (n "tarpit") (v "0.1.77") (h "0li85j1xh27bl3m16fpflvrvfr8gnwml7bw0xfnylin8x3mf1gji") (y #t)))

(define-public crate-tarpit-0.77.0 (c (n "tarpit") (v "0.77.0") (h "0hxc6r448amkpxmwd6bvbcx28knj5fy23g3wrkpa3siiqlkaaz0m") (y #t)))

(define-public crate-tarpit-7.7.18 (c (n "tarpit") (v "7.7.18") (h "0bwlz9clk5wx8jnrrql4djhjss55wjg2hlhcl35439ri98crc3j7") (y #t)))

(define-public crate-tarpit-2018.7.7 (c (n "tarpit") (v "2018.7.7") (h "00749bwp8qghb0wqddx9xbhi6pdr58pc260pg609driqrics8r0r") (y #t)))

(define-public crate-tarpit-2019.12.13 (c (n "tarpit") (v "2019.12.13") (h "1prn12b19l9c9qxzjz1p12gxy84v3jwfl8wgx5cvsbpwj1prjzqw") (y #t)))

(define-public crate-tarpit-9999.999.99 (c (n "tarpit") (v "9999.999.99") (h "1j9v7d8f0v6hzjp9c24sg9fc0ibm9186ybhg85cpg00pldvmf741") (y #t)))

(define-public crate-tarpit-9.9.9 (c (n "tarpit") (v "9.9.9") (h "1zirgslmz0ywljhdlap01d3vqfcgc0v7yp79m36d5lyxdc49rn0g") (y #t)))

(define-public crate-tarpit-99999.99999.99999 (c (n "tarpit") (v "99999.99999.99999") (h "16zfgcykhwkr3z2bdsvjxpqwn6zca17z7hmmy659jd5mfjbnagnm") (y #t)))

(define-public crate-tarpit-9999999.9999999.9999999 (c (n "tarpit") (v "9999999.9999999.9999999") (h "1khj05n8vakr5y1v1hbfl8n8xkwqq5gghq4kzmvar9vk3afz2glb") (y #t)))

(define-public crate-tarpit-9999999.9999999.99999990 (c (n "tarpit") (v "9999999.9999999.99999990") (h "1ry57ccxkza3cjzccs3cjybln3cgl05xz1ml6d2mnxadmq7c14cz") (y #t)))

(define-public crate-tarpit-999999999.999999999.999999999 (c (n "tarpit") (v "999999999.999999999.999999999") (h "148f5kjpn3gdbm1z54s943hssnxqvdrhwgszcx003l3xxxxl78qw")))

