(define-module (crates-io ta rp tarpc-plugins) #:use-module (crates-io))

(define-public crate-tarpc-plugins-0.1.0 (c (n "tarpc-plugins") (v "0.1.0") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)))) (h "17y0fl87n616d3rprvv10aqlf2kl71s0yfp43b1ixrsh536x3i3d")))

(define-public crate-tarpc-plugins-0.1.1 (c (n "tarpc-plugins") (v "0.1.1") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)))) (h "1c5qpf49lm7pzhxyfj86g755zav7j3yz6y366mnwbsj706vg4yph")))

(define-public crate-tarpc-plugins-0.2.0 (c (n "tarpc-plugins") (v "0.2.0") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)))) (h "129yfrkrb5pg1jxqpki8j209d46j7fx3cmb1vkwqashvn6sz7sgg")))

(define-public crate-tarpc-plugins-0.3.0 (c (n "tarpc-plugins") (v "0.3.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "0gsg3li8bl9an54gy3c4aah7a66id1hrvf1wjnqkckyl3lgvmc6v")))

(define-public crate-tarpc-plugins-0.4.0 (c (n "tarpc-plugins") (v "0.4.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "0cs5mvh2q45qgx0nvmwccfghkk4yfmqv26k64hf8db1mw3c3iycz")))

(define-public crate-tarpc-plugins-0.5.0 (c (n "tarpc-plugins") (v "0.5.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wgfx40cldbix7b6c7lli9r48m6a7lyh88ws50l291fp92jxqkjj")))

(define-public crate-tarpc-plugins-0.5.1 (c (n "tarpc-plugins") (v "0.5.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j0k700y8hz8dxavcsfmf02qr9icmp9g3qiv1kqcq9k1x8c3j3ma")))

(define-public crate-tarpc-plugins-0.6.0 (c (n "tarpc-plugins") (v "0.6.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0d14w6y7wwqd5ikwfdhj71626k4caw59454fck5laffyiiq73d79") (f (quote (("serde1")))) (y #t)))

(define-public crate-tarpc-plugins-0.6.1 (c (n "tarpc-plugins") (v "0.6.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1022rzavs9b2lsagfn211l40n2ala4pir72f7wfw6xhx3pq7h4h6") (f (quote (("serde1"))))))

(define-public crate-tarpc-plugins-0.7.0 (c (n "tarpc-plugins") (v "0.7.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "184n6bs98fqw4qpbxx3jw8r69p6lbqy13az00qmm1fqgipczhyhk") (f (quote (("serde1"))))))

(define-public crate-tarpc-plugins-0.8.0 (c (n "tarpc-plugins") (v "0.8.0") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1drbp152yrkicx8cj3lkdwnvlihyi5xa867abdasp8m0xqngkfpd") (f (quote (("serde1"))))))

(define-public crate-tarpc-plugins-0.9.0 (c (n "tarpc-plugins") (v "0.9.0") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ddg0mq0nhycb9iq5sja0c7xb2213lfsfnq89rrra49bl9w0697k") (f (quote (("serde1"))))))

(define-public crate-tarpc-plugins-0.10.0 (c (n "tarpc-plugins") (v "0.10.0") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n8schkjw904xjgspilh8j80j7qdqz1w63z6ca26gsz56wjf3qcl") (f (quote (("serde1"))))))

(define-public crate-tarpc-plugins-0.11.0 (c (n "tarpc-plugins") (v "0.11.0") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gggnxk23y313gkmwfska4x6psnb72ww29kl3nb1vy3mdj78307a") (f (quote (("serde1"))))))

(define-public crate-tarpc-plugins-0.12.0 (c (n "tarpc-plugins") (v "0.12.0") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03y1v4crjy79yjvrcb616g6darzbgcd53gsy73hbq5wzam72pr0f") (f (quote (("serde1"))))))

(define-public crate-tarpc-plugins-0.13.0 (c (n "tarpc-plugins") (v "0.13.0") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dj5fkr4g7ifnq0dgfcc767g49zc72pyw663pkikwvc38d5j62yb") (f (quote (("serde1")))) (r "1.56")))

(define-public crate-tarpc-plugins-0.13.1 (c (n "tarpc-plugins") (v "0.13.1") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nma9fyah81kij0mspmrhfkk5vdln0a39li5n00jp2pvlaz050xd") (f (quote (("serde1")))) (r "1.56")))

