(define-module (crates-io ta p- tap-consooomer) #:use-module (crates-io))

(define-public crate-tap-consooomer-0.1.0 (c (n "tap-consooomer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1x6ca77nskl06nhgmcb4ns5lx93hbahw58774ifzg9ym4384f836") (r "1.64.0")))

