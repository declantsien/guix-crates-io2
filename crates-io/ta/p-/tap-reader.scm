(define-module (crates-io ta p- tap-reader) #:use-module (crates-io))

(define-public crate-tap-reader-1.0.0 (c (n "tap-reader") (v "1.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0sgy8b64v9sfh26vcrzgq38mxzsjbnapipgqs1fl2qxf4lnqsyv9")))

(define-public crate-tap-reader-1.0.1 (c (n "tap-reader") (v "1.0.1") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1wxnxzmszmg8lqc3cmzzpilp7w2r1ir7k9lvaba8qnj13lqgq4wg")))

