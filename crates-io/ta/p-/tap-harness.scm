(define-module (crates-io ta p- tap-harness) #:use-module (crates-io))

(define-public crate-tap-harness-0.1.0 (c (n "tap-harness") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "testanything") (r "^0.4") (d #t) (k 0)))) (h "0vwl9w47nnf5wiff6p02jpcfcwnp7pvkfb748i277kpdb7wmjg0v")))

(define-public crate-tap-harness-0.2.0 (c (n "tap-harness") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "testanything") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0p95l62w08plwrbc6afsg530rqxlpk01xpp678iz4k7kpr7q4vsv")))

(define-public crate-tap-harness-0.3.0 (c (n "tap-harness") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "testanything") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "13bn7gbsi517pc6x9d1vqww5xkfmadr7lliyzc5wdd9lwr0i593h")))

