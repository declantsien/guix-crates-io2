(define-module (crates-io ta p- tap-trait) #:use-module (crates-io))

(define-public crate-tap-trait-0.0.0 (c (n "tap-trait") (v "0.0.0") (d (list (d (n "pipe-trait") (r "^0.3.2") (d #t) (k 2)))) (h "1c2crwpkh4dnwrrv1kw7v1iqf3i0s511g56wyjlhaqrdcq2fwd7i")))

(define-public crate-tap-trait-1.0.0 (c (n "tap-trait") (v "1.0.0") (d (list (d (n "pipe-trait") (r "^0.3.2") (d #t) (k 2)))) (h "0pb7ph9l6a3xzy866qa4ybg8mxjfblinjy1mgm941n10pkc1c2cb")))

