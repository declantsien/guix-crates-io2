(define-module (crates-io ta p- tap-windows) #:use-module (crates-io))

(define-public crate-tap-windows-0.1.0 (c (n "tap-windows") (v "0.1.0") (d (list (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "combaseapi" "ioapiset" "winioctl" "setupapi" "synchapi" "netioapi" "fileapi"))) (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "0kaghddbddpji5iy7dvxgji38yg80znwir9axz5fqkalxhmpl8mx") (y #t)))

(define-public crate-tap-windows-0.1.1 (c (n "tap-windows") (v "0.1.1") (d (list (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "combaseapi" "ioapiset" "winioctl" "setupapi" "synchapi" "netioapi" "fileapi"))) (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "1ghs65lw5v8avc3sq65r4php2ki5h78kl2pnazqhi4f4d94j487n") (y #t)))

(define-public crate-tap-windows-0.1.2 (c (n "tap-windows") (v "0.1.2") (d (list (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "combaseapi" "ioapiset" "winioctl" "setupapi" "synchapi" "netioapi" "fileapi"))) (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "1jyb9lcvs6qjixknn76frx02zbmb3wpp56pkmpp9v1ais7vagj0d")))

