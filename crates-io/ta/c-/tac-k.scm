(define-module (crates-io ta c- tac-k) #:use-module (crates-io))

(define-public crate-tac-k-0.1.0 (c (n "tac-k") (v "0.1.0") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "06yaabd3nhr5vkrfab2k4f30qmskpifsb4d7d19b2fzmsd0xxkmv") (r "1.70")))

(define-public crate-tac-k-0.1.1 (c (n "tac-k") (v "0.1.1") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "0z40anr8s46fzj1a29lxcfad2bf8rhcqw186krv5hib6mhjjjq3h") (r "1.70")))

(define-public crate-tac-k-0.1.2 (c (n "tac-k") (v "0.1.2") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "1da2f4dkws4l1y6adf1pkwj2h2z7wx35lszdl3hc51sdqfhicxw2") (y #t) (r "1.70")))

(define-public crate-tac-k-0.1.3 (c (n "tac-k") (v "0.1.3") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "1x8vms7iv9inm51lh81rq2c075yvz2ibapka7g1l66hmspgmws98") (r "1.70")))

(define-public crate-tac-k-0.2.0 (c (n "tac-k") (v "0.2.0") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "0wccwv8290anzwzlmb73gr71d0yizw68b9bqzwzdmxv312h90dja") (r "1.70")))

(define-public crate-tac-k-0.2.1 (c (n "tac-k") (v "0.2.1") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "03fsmvf4hv1y4jmnzjiims39x3cf9mp8s7lgxa5gqi8az3iprysn") (r "1.70")))

(define-public crate-tac-k-0.3.0 (c (n "tac-k") (v "0.3.0") (d (list (d (n "clap") (r "~4.4") (f (quote ("std" "help" "usage" "cargo"))) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "18z6h7kji316nxpwi0x06madv6v63askk2dhv1w2xv68vl578zhh") (r "1.70")))

(define-public crate-tac-k-0.3.1 (c (n "tac-k") (v "0.3.1") (d (list (d (n "clap") (r "~4.4") (f (quote ("std" "help" "usage" "cargo"))) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "0xbaycd98d0wa9165mrj6qiq8jv2d4006z12amglj2njvypyklck") (r "1.70")))

