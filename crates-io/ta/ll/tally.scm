(define-module (crates-io ta ll tally) #:use-module (crates-io))

(define-public crate-tally-0.2.0 (c (n "tally") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1b19qysa0m4w4r44r6hisyal0kkfr303wdf77y9h3nl2kdvkvrgw")))

(define-public crate-tally-0.2.1 (c (n "tally") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0kmpnykx1g6nahhc7928qiw4wgwyyj0ffvdlrhjxz629kysz0jwz")))

(define-public crate-tally-0.2.2 (c (n "tally") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1sihik0kb1mkd94dygr7w69662ghca9sh33wa151wxm6n1zihj40")))

(define-public crate-tally-0.3.0 (c (n "tally") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0qgnbf6gszfvm0xzbh5q0zgzvlng99d889m5v6zylw4gi81wn70w")))

(define-public crate-tally-0.3.1 (c (n "tally") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "161s376giw0j6ckl5hs1wzpg844slf31wzlmx1gd4sz6zsg5rqg4")))

(define-public crate-tally-0.3.2 (c (n "tally") (v "0.3.2") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0zxf603dqal19vibym69rjhl9sksv2k25rm17vfjxw9cxy36zbnq")))

(define-public crate-tally-0.4.0 (c (n "tally") (v "0.4.0") (d (list (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1vz6pdvpc29knk4bprvvkhgl36hczyq77viyx8q90pyw48v8i1h6")))

(define-public crate-tally-0.4.1 (c (n "tally") (v "0.4.1") (d (list (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0gzs8w4s5farmhg74nk4qdhp7vji9s93a1jlaanpj59r5dx8279c")))

(define-public crate-tally-0.4.2 (c (n "tally") (v "0.4.2") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0bzlpqcrl18f2bs3s9gpdwdbjflsbzkphbj84i5vps9rfaz9rl7n")))

(define-public crate-tally-0.4.3 (c (n "tally") (v "0.4.3") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0b03x654ipqkpvp0wd47bjil7l704mi3241g03qaakrvqpala3ci")))

