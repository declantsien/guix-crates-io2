(define-module (crates-io ta ll tallymarks) #:use-module (crates-io))

(define-public crate-tallymarks-0.1.0 (c (n "tallymarks") (v "0.1.0") (h "05i6zsra9kyb7fvs5dpbqyi08h34sxyxpmqdgzlwdwwx8jpxmg2w")))

(define-public crate-tallymarks-0.1.1 (c (n "tallymarks") (v "0.1.1") (h "1i447y07pbv6lqsypbm1c9ccfdg9n0gba09ybz3bjfr0wg0h4r83")))

