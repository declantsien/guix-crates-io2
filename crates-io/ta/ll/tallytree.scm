(define-module (crates-io ta ll tallytree) #:use-module (crates-io))

(define-public crate-tallytree-0.1.0 (c (n "tallytree") (v "0.1.0") (d (list (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1m4c13xq68bk9ww76n3cw0gda944vhcjxg3q9vf922p171fxim00") (f (quote (("fail-on-warnings"))))))

(define-public crate-tallytree-0.2.0 (c (n "tallytree") (v "0.2.0") (d (list (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "0cn3pa16965mrin2dnx5hykviwy0aq2i9w70400bg0x5qczi6ilj") (f (quote (("fail-on-warnings"))))))

(define-public crate-tallytree-0.2.1 (c (n "tallytree") (v "0.2.1") (d (list (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "09fgmzgdfj3x27bs8l004x48cscx6fhzl9cbh3vvf2c4fvdzcg90") (f (quote (("fail-on-warnings"))))))

(define-public crate-tallytree-0.2.2 (c (n "tallytree") (v "0.2.2") (d (list (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0yh4z4kzp2yzvss2vch25akqrpx5xmdd7hka0qfkpyq3wk33glqj") (f (quote (("fail-on-warnings"))))))

(define-public crate-tallytree-0.2.3 (c (n "tallytree") (v "0.2.3") (d (list (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "sha2") (r "0.10.*") (d #t) (k 0)))) (h "0ncl35mzxkn1szpqrbap3agxwlryghsnrh6c8xbqfb81pfsh1jg2") (f (quote (("with-serde" "serde" "serde_json") ("fail-on-warnings") ("default" "with-serde"))))))

(define-public crate-tallytree-0.2.4 (c (n "tallytree") (v "0.2.4") (d (list (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "sha2") (r "0.10.*") (d #t) (k 0)))) (h "0v6f1z44b2z6k5vs3jy3j0hjc0s0iw4p4j2c5cmpdrjd6w87vq16") (f (quote (("with-serde" "serde" "serde_json") ("with-benchmarks") ("fail-on-warnings") ("default" "with-serde"))))))

(define-public crate-tallytree-0.3.4 (c (n "tallytree") (v "0.3.4") (d (list (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "sha2") (r "0.10.*") (d #t) (k 0)))) (h "0kj1ahiigxliyfb4bvb8yf43jhrgdnl60vmbz90lm7325sygravl") (f (quote (("with-serde" "serde" "serde_json") ("with-benchmarks") ("fail-on-warnings") ("default" "with-serde"))))))

