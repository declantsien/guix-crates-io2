(define-module (crates-io ta al taalika) #:use-module (crates-io))

(define-public crate-taalika-0.1.5 (c (n "taalika") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1v3hr78xpldv87vmw4x8z29nwrs7hbndxi5n4s38sksx8xy661cr") (f (quote (("default" "unicode-width")))) (y #t)))

(define-public crate-taalika-0.1.6 (c (n "taalika") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0rjhwj3cbw2903h3c20w57fsfs08p4h2mvqnzn7sqz4g8jicd183") (f (quote (("default" "unicode-width")))) (y #t)))

