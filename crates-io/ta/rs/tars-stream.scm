(define-module (crates-io ta rs tars-stream) #:use-module (crates-io))

(define-public crate-tars-stream-0.0.1 (c (n "tars-stream") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "153x4n51cgbg3clg3jsm8vv1la27srqhrcf2qj8bvc2whbrssss3")))

(define-public crate-tars-stream-0.0.2 (c (n "tars-stream") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "01hyp4gz89q17yc7axl05sm70jbqx9b2bggjqa400v8wajz7dmxi")))

(define-public crate-tars-stream-0.0.3 (c (n "tars-stream") (v "0.0.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "0qp904k09cmv8cgskm8xm151xncs8cp7dwzq5n1r7nciiyvg3zc7")))

(define-public crate-tars-stream-0.0.4 (c (n "tars-stream") (v "0.0.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "0im8pddm305q53vid1zfgq4x0v6wmc6x1g6w9yx5qvzhbf8z4m7m")))

(define-public crate-tars-stream-0.1.0 (c (n "tars-stream") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "1vbm6pchakf0vi8j8zzfb4n4b37l5w9wmswbchpfp7djw55hd31b")))

(define-public crate-tars-stream-0.1.1 (c (n "tars-stream") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 2)))) (h "164i6d16vws0ligmbbbskmmhapnqhjqjlp4wppll38cjzsw2425c")))

