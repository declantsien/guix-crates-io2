(define-module (crates-io ta rs tarsila) #:use-module (crates-io))

(define-public crate-tarsila-0.1.0 (c (n "tarsila") (v "0.1.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-macroquad") (r "^0.1.0") (d #t) (k 0) (p "egui-macroquad-fork")) (d (n "lapix") (r "^0.1.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "rfd") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dm91l2wavq37sz453x0z0bn07hjcrkac6b12jvmr6ywyax0snbc")))

