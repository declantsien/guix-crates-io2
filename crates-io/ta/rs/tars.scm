(define-module (crates-io ta rs tars) #:use-module (crates-io))

(define-public crate-tars-0.1.0 (c (n "tars") (v "0.1.0") (h "0p8zsjhchfs163h5b89fk7fz3p4d5zkg7w7f49cpwi8fv5nlapl2")))

(define-public crate-tars-0.1.1 (c (n "tars") (v "0.1.1") (h "0m3q4aha29s1jn4gh46wznyfslhngw1np9k5s8c2kpbk47cc1jal")))

(define-public crate-tars-0.1.2 (c (n "tars") (v "0.1.2") (h "06iy7qv2b57qlxmgij5wvkihv3q1czg49y8vv32fqys7wvfwql5j")))

(define-public crate-tars-0.1.3 (c (n "tars") (v "0.1.3") (h "1d1xx0h2svhs315byf6r3kzfmgxr4wrp7nfzpf7m8cc2dxcc6bzj")))

