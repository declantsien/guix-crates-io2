(define-module (crates-io ta me tame-protocol) #:use-module (crates-io))

(define-public crate-tame-protocol-0.2.0 (c (n "tame-protocol") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "rill-engine") (r "^0.31.0") (o #t) (d #t) (k 0)) (d (n "rill-protocol") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1v36apjdfnak612pkndjkxj5hzzls4pc4z3qa0sp94gzk2hi5ay6") (f (quote (("tracers" "rill-engine") ("default" "tracers"))))))

