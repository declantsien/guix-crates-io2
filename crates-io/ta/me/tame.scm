(define-module (crates-io ta me tame) #:use-module (crates-io))

(define-public crate-tame-0.1.0 (c (n "tame") (v "0.1.0") (h "07rhr5x80b0zj8ljv5xak5n3nfhxa3ddqv7q3k5pa1rjdcb37f22")))

(define-public crate-tame-0.2.0 (c (n "tame") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "meio") (r "^0.89.4") (d #t) (k 0)) (d (n "rill-engine") (r "^0.31.0") (d #t) (k 0)) (d (n "rill-protocol") (r "^0.31.0") (d #t) (k 0)) (d (n "tame-protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.6") (f (quote ("sync" "io-util"))) (d #t) (k 0)))) (h "1f1fy0hw44af694sb2wqxs0a6ahl2s46s2xwn0x61n82ypinvk0s")))

