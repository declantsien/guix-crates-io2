(define-module (crates-io ta dm tadm) #:use-module (crates-io))

(define-public crate-tadm-0.1.0 (c (n "tadm") (v "0.1.0") (h "0x9iwwv2z8ply5n9h2j5xkfrkxdmic9a653wdbklbipv669l5v5p") (r "1.58")))

(define-public crate-tadm-0.1.1 (c (n "tadm") (v "0.1.1") (h "080k7d6cldcwdvm810bnsaqmxhrq0pxygnnhs1p9pp5vmwbivrla") (r "1.58")))

