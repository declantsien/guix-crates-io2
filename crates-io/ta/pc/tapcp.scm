(define-module (crates-io ta pc tapcp) #:use-module (crates-io))

(define-public crate-tapcp-0.1.0 (c (n "tapcp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-tftp") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lnhzb6ialcpsjswcr06i9djsrggibadfl2nkd9b8c8d8kyqa4k6")))

(define-public crate-tapcp-0.2.1 (c (n "tapcp") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "casper_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "kstring") (r "^2") (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tftp_client") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0y5fny4g3z550dxx2dj0z5kdca0md5falaky273bp8wmk5qnnxm6")))

