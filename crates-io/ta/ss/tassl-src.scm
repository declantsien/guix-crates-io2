(define-module (crates-io ta ss tassl-src) #:use-module (crates-io))

(define-public crate-tassl-src-0.1.0 (c (n "tassl-src") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 0)))) (h "0a9yjl75vk3w6zadvkzyv4q05achdjh58ysb1q9ir4cprfnrg4yh")))

(define-public crate-tassl-src-0.1.1 (c (n "tassl-src") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 0)))) (h "15cs3i9xampsgi63r4gybs0jrccm3zcwmy489jna37fwqa6iw16q")))

(define-public crate-tassl-src-0.1.2 (c (n "tassl-src") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 0)))) (h "085khybiqan33d7c84z7i6s9hiv01xi40acz1ba3gwl64bjwq2kc")))

(define-public crate-tassl-src-0.1.3 (c (n "tassl-src") (v "0.1.3") (h "1lmryxc62f13503vngxpkxk99nzjxhik1zbj4wriibwpkizg3j5f")))

(define-public crate-tassl-src-0.1.4 (c (n "tassl-src") (v "0.1.4") (h "0n0rchi21l9miprhhlzjk6c6lqsxsqa9by5w3b0b8dbvdissjf8c")))

