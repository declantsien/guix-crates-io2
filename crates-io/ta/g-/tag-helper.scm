(define-module (crates-io ta g- tag-helper) #:use-module (crates-io))

(define-public crate-tag-helper-0.2.2 (c (n "tag-helper") (v "0.2.2") (d (list (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "semver") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)))) (h "060qps8kfz97q3znijv9nyp77gl2wq8a12xvzfz8la9ym01h5rwv")))

(define-public crate-tag-helper-0.2.4 (c (n "tag-helper") (v "0.2.4") (d (list (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "semver") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)))) (h "02inr8rdmxiffh2v5lf78mk4fsvxqrv4w6i8vy855swg2n4s3l9s")))

(define-public crate-tag-helper-0.3.0 (c (n "tag-helper") (v "0.3.0") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "16r0jh155dsk3z55kk7vrw4hyryfgan2116dd5m0s9hahnaq20la")))

(define-public crate-tag-helper-0.3.1 (c (n "tag-helper") (v "0.3.1") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0kqnl1w3yp2vfpgv3rp9wb42xpli2x418jfngnhzbj2jc57r56bp")))

(define-public crate-tag-helper-0.3.2 (c (n "tag-helper") (v "0.3.2") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "06pj86df805dwvwzpxgak24afvjjvgrsrdj21p1mkalj0iyclffy")))

(define-public crate-tag-helper-0.4.0 (c (n "tag-helper") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "13l3qbaai7mdnk7s9mjmsw12f33g7bzwphi02qscf0px5sxwbd7f")))

(define-public crate-tag-helper-0.4.1 (c (n "tag-helper") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "04mwpp4jq65h5icr7g1xzb95lh2nb5r9pzqs32jshzzhax90wf11")))

(define-public crate-tag-helper-0.5.0 (c (n "tag-helper") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)))) (h "19myz4cvim7w5vpjirhxrx1qqclnsnsvvfzv4dk7dggyqfxp47p7")))

(define-public crate-tag-helper-0.5.1 (c (n "tag-helper") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)))) (h "1ribvhv0590ig6w7ylk7j15sq3fa5s8iidplck6cxbjfzw2d2kzk")))

