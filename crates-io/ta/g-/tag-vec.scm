(define-module (crates-io ta g- tag-vec) #:use-module (crates-io))

(define-public crate-tag-vec-0.0.1 (c (n "tag-vec") (v "0.0.1") (h "03pic39cchwcj2zyljp9ahd9mcn6kapp9jv8b4fbpixx0lip1kx4")))

(define-public crate-tag-vec-0.0.2 (c (n "tag-vec") (v "0.0.2") (h "1q4ilbcm84bkkw2zcs8dwf6s8bx94cs1v8x15n5h83limq7s1lxl")))

