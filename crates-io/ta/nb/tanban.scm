(define-module (crates-io ta nb tanban) #:use-module (crates-io))

(define-public crate-tanban-0.1.0 (c (n "tanban") (v "0.1.0") (d (list (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0rhdp83fdsb35vmw20xcwsicx0shv7pzl1yig4jl6wlxzcjdzzzk") (y #t)))

(define-public crate-tanban-0.1.1 (c (n "tanban") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0gp5ccalqxsfslq4fx2p98z70zvvlg17clhr5pj8dck3d4wb14xz") (y #t)))

(define-public crate-tanban-0.1.2 (c (n "tanban") (v "0.1.2") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("toml"))) (d #t) (k 0)))) (h "0lf47drx2n1wq6xcw98rrqq324yzwc8m77y89ywfh23wi0i90nlk") (y #t)))

(define-public crate-tanban-0.1.3 (c (n "tanban") (v "0.1.3") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("toml"))) (d #t) (k 0)))) (h "1gwg34yd9dprsnvfy3krbahjwl0vswc1b3byyh48ffr2fcagc4j2") (y #t)))

