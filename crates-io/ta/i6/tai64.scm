(define-module (crates-io ta i6 tai64) #:use-module (crates-io))

(define-public crate-tai64-0.1.0 (c (n "tai64") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0w36ggqs3w0n6wbd41s2hdb4fka3lfgl65w0qqfybaskv9gkvz9w")))

(define-public crate-tai64-0.2.0 (c (n "tai64") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0jzj2h9qla6biqvcmqrbi3vpyl0l1m2xv22pfymqppc0w6dw3ryc") (f (quote (("default"))))))

(define-public crate-tai64-0.2.1 (c (n "tai64") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "10b1m28faqnfci9qsswlv6hfgzx6mdf4iq32vln6s39hfwnvv339") (f (quote (("default"))))))

(define-public crate-tai64-0.2.2 (c (n "tai64") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0lkyzywvf1acyg4zn8j9lsj8qh089xaj2winip0zcbifvwbh40ih") (f (quote (("default"))))))

(define-public crate-tai64-0.2.3 (c (n "tai64") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "02nzfjb1hv1950f3lr71d9v7nwa6h64ljk14lf2hxngb366nayl6") (f (quote (("default"))))))

(define-public crate-tai64-1.0.0 (c (n "tai64") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "00b9ignld2xwzbz2gzd3j6b5s5iaj55q1nhkh3cmy1cgnf29rixf") (f (quote (("default"))))))

(define-public crate-tai64-2.0.0 (c (n "tai64") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("failure_derive"))) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "0ic1a2qljdc2x64cknbrfxq8jwwv6k4a4nzn25bxk9w6nk3q9scd") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-tai64-2.0.1 (c (n "tai64") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("failure_derive"))) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "0hh9ddqq8jrx9mlpil6d1xdsnc93nc4acqwhd61bqrrz3fkjli2z") (f (quote (("std") ("default" "std"))))))

(define-public crate-tai64-3.0.0 (c (n "tai64") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "01z2m69a08miflrnp9fqd03nffz9i4l8xz9s1pj2yhdib6j2izxz") (f (quote (("std") ("default" "std"))))))

(define-public crate-tai64-3.1.0 (c (n "tai64") (v "3.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "10kb8lbwspbwrbqayscraz62ywbkgqj36rp81a46i09b7nf2h520") (f (quote (("std") ("default" "std"))))))

(define-public crate-tai64-4.0.0 (c (n "tai64") (v "4.0.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "zeroize") (r "^1.1") (o #t) (k 0)))) (h "1p597n6rnl6d2s065rinkyb86wr8z3kzayhzdhp17x1521102x7d") (f (quote (("std") ("default" "std")))) (r "1.56")))

