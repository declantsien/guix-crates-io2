(define-module (crates-io ta bu tabular) #:use-module (crates-io))

(define-public crate-tabular-0.1.0 (c (n "tabular") (v "0.1.0") (d (list (d (n "unicode-width") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0igyi4mxnfxhvn742p9ljr1la51f4znwc5jd13vw740fsbs1kr89") (f (quote (("default" "unicode-width"))))))

(define-public crate-tabular-0.1.1 (c (n "tabular") (v "0.1.1") (d (list (d (n "unicode-width") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1pmjzspi0m5c6g5z2ar6kcym8hv25czgyq19s64l74vm350xp986") (f (quote (("default" "unicode-width"))))))

(define-public crate-tabular-0.1.2 (c (n "tabular") (v "0.1.2") (d (list (d (n "unicode-width") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "02hfch6d214p3rp31rrid1h0l1y6lq8ddrmz79qdscvdjm7h6vgx") (f (quote (("default" "unicode-width"))))))

(define-public crate-tabular-0.1.3 (c (n "tabular") (v "0.1.3") (d (list (d (n "unicode-width") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1wcd3rfivkmmhqmbwsmwclhmc8bkqins8r0kvvgjzf9z0fl1r00f") (f (quote (("default" "unicode-width")))) (y #t)))

(define-public crate-tabular-0.1.4 (c (n "tabular") (v "0.1.4") (d (list (d (n "unicode-width") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0fsci58lxkklc1gdd87y08dan7i4kn36nnq6fi5fdvyw0bp5pqz7") (f (quote (("default" "unicode-width"))))))

(define-public crate-tabular-0.2.0 (c (n "tabular") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1ld3j7zx5ri87wf379n9mhdqgn6wibbfj3gr7nbs3027a4n8i8nr") (f (quote (("default" "unicode-width") ("ansi-cell" "strip-ansi-escapes"))))))

