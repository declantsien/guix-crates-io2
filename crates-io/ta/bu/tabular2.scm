(define-module (crates-io ta bu tabular2) #:use-module (crates-io))

(define-public crate-tabular2-0.1.0 (c (n "tabular2") (v "0.1.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "06h7yqm3fz8d1c06r49lsr3qw0pbd1kvvcyzw3gx3fma664q6qz1")))

(define-public crate-tabular2-1.0.0 (c (n "tabular2") (v "1.0.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "17qc484iiaawrz7rb0i24lsp5jx0hlsbzgknhd9syb9wkfs64wi3")))

