(define-module (crates-io ta bu tabula) #:use-module (crates-io))

(define-public crate-tabula-0.1.0 (c (n "tabula") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "jni") (r "^0.19") (f (quote ("invocation"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pi0khyz6d41bc0sz3fkplh27bm8z2qrbhsklbbmpmjdf8yvyl9q")))

(define-public crate-tabula-0.1.1 (c (n "tabula") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "jni") (r "^0.19") (f (quote ("invocation"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a7kbswkq1895s8y5bknsmfabxln95nyl8g1gh8b889zp71dxkrb")))

