(define-module (crates-io ta bu tabu) #:use-module (crates-io))

(define-public crate-tabu-0.1.0 (c (n "tabu") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "13g1xyykqzrnxkck8v4vhgwvpiv25asxvc661a2imhr0bn3qyp37")))

(define-public crate-tabu-0.1.1 (c (n "tabu") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "1hpsf3vx2l5ma5p4rznw2mz4g09mj1cbw6zqdmfc1r3x2w7syrcg")))

