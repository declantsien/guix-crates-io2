(define-module (crates-io ta sm tasm-lib) #:use-module (crates-io))

(define-public crate-tasm-lib-0.1.0 (c (n "tasm-lib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)) (d (n "num-traits") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "triton-opcodes") (r "^0.31.0") (d #t) (k 0)) (d (n "triton-vm") (r "^0.31.1") (d #t) (k 0)) (d (n "twenty-first") (r "^0.30.0") (d #t) (k 0)))) (h "10hg4iikl4kdz7d0l9nr1cdpk7s569sxqc6qp4xlmscp4bgmjmwp")))

