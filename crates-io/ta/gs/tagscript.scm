(define-module (crates-io ta gs tagscript) #:use-module (crates-io))

(define-public crate-tagscript-0.1.0 (c (n "tagscript") (v "0.1.0") (d (list (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0k4q1dh2ilx81zfjik17cbfrd67jz7hzp5vs8lf0xvlpd8by009m")))

(define-public crate-tagscript-0.1.1 (c (n "tagscript") (v "0.1.1") (d (list (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0cplv1ndv9qrfpw9917khzlqn5sklg6kwrlr4cylr8bd7x0rwlaj")))

(define-public crate-tagscript-0.1.2 (c (n "tagscript") (v "0.1.2") (d (list (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1v73qaasixr5awcd31252as2szfw1dhlnn4zqrnxmxkjk5x5zrfg")))

