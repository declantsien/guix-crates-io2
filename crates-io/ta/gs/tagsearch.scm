(define-module (crates-io ta gs tagsearch) #:use-module (crates-io))

(define-public crate-tagsearch-0.9.0 (c (n "tagsearch") (v "0.9.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1bnxd1jw312virhrfig1d28hhqzqk0hlrxlfnjlsxl06zxjid1d9")))

(define-public crate-tagsearch-0.9.1 (c (n "tagsearch") (v "0.9.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0h9z6xfc83kpg5ffgadi7gfzzpphr6jbf9vk7njmh40qv6x9yz5k")))

(define-public crate-tagsearch-0.11.0 (c (n "tagsearch") (v "0.11.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0s5ca83kb7pp89ckbdf086qrijfnvc3w28s9g5w92a041wfd9s9f")))

(define-public crate-tagsearch-0.12.0 (c (n "tagsearch") (v "0.12.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0s2zsb4pmba8pzcrza8h05cbg73y6iy5fbdwfmi4h8q0wjq8xj5p")))

(define-public crate-tagsearch-0.13.0 (c (n "tagsearch") (v "0.13.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1py8x8jcaxxl91fv5mkpq14n7p3kjmcb9srdkqncb5and7k17v39")))

(define-public crate-tagsearch-0.14.0 (c (n "tagsearch") (v "0.14.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "09w4a578j0wypvrx5jynriwdlp2im6jj7wwqiwf7zmqmp1n7fcg7")))

(define-public crate-tagsearch-0.15.0 (c (n "tagsearch") (v "0.15.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1b2x51qf2n57x607b7xbvralrcxf5hl0dnlf47p6dnlpqxznmghx")))

(define-public crate-tagsearch-0.17.0 (c (n "tagsearch") (v "0.17.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1qz6pmnc42bny9az9f94c4vl3xbanxb9qk4gcbfqmfk6hy5p7xcw")))

(define-public crate-tagsearch-0.18.0 (c (n "tagsearch") (v "0.18.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1ji185n8nwgh5c1233wnl5j7y24bwz218ngzxjn73njzn5byv0jm")))

(define-public crate-tagsearch-0.19.0 (c (n "tagsearch") (v "0.19.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1kb8bn6ikpndnl0av7h5fr6z2qq3wd7p459v65yl78acilnmz4nm")))

(define-public crate-tagsearch-0.19.1 (c (n "tagsearch") (v "0.19.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1i9zyl9hzw01pw78yzrmzxkv9yrpwmljn041sg27j8765b1gcfrn")))

(define-public crate-tagsearch-0.20.0 (c (n "tagsearch") (v "0.20.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1k4qb8602q5vlch02jkwcgyygvm87zgzywb24gfail0aswiv5kyx")))

(define-public crate-tagsearch-0.20.1 (c (n "tagsearch") (v "0.20.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "00dawba5dlr3c1gzvncfw7rdzii4rdvln8av0nh4cl4qk4qnhnkm")))

(define-public crate-tagsearch-0.21.0 (c (n "tagsearch") (v "0.21.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "18hqgc4byjv270vnq5mdjgdkm8i2jj1wg7d3pa3z9n9vy7wsi8p6")))

(define-public crate-tagsearch-0.22.0 (c (n "tagsearch") (v "0.22.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1rl5mf6zpkl47jjvqsgqfvms4jxqdf2y2hj7b3z8bncp65l5yjkx")))

(define-public crate-tagsearch-0.24.0 (c (n "tagsearch") (v "0.24.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1lnlcm6zqgimyk9v15y7nahrfj12jyx1z3qfhwaahx4aawwp178q")))

(define-public crate-tagsearch-0.27.0 (c (n "tagsearch") (v "0.27.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1mdhd0y0rxbym6v3zg23bafhc2qgy72c3jz4qm2bmkygg345vn8c")))

(define-public crate-tagsearch-0.28.0 (c (n "tagsearch") (v "0.28.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "18imzdx5jgnj6is63zr0mkafwlp1lzpx6557vnzvax2fnkcj5sjy")))

(define-public crate-tagsearch-0.32.0 (c (n "tagsearch") (v "0.32.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1k0jk5wa8jz8dv2hc3abjqhmkqp2w684qfbc59whcd6hjzn5a1sf")))

(define-public crate-tagsearch-0.34.0 (c (n "tagsearch") (v "0.34.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08zapm0y41hz1jcdaxjgr9y8pbrc40jd320v4xsjsdvn2xh0rmkv")))

(define-public crate-tagsearch-0.35.0 (c (n "tagsearch") (v "0.35.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1w0rgh3bwy70zdf0l6smahkbf83vsp0655niri0n58c059xsmmjm")))

(define-public crate-tagsearch-0.37.0 (c (n "tagsearch") (v "0.37.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1rmf4zwb4jy3na258wbj1fpkcbl7qmd0jd6fkd9196kg8524xw43")))

