(define-module (crates-io ta ra tarantool-proc) #:use-module (crates-io))

(define-public crate-tarantool-proc-0.1.0 (c (n "tarantool-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vs6m4x0xnas29x4xiirvap529r6yfg0abi4xk1dlw5hi7kqdl06")))

(define-public crate-tarantool-proc-0.1.1 (c (n "tarantool-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1077v9qy5hbl2crzg43fanqkalib4wp4pvb45a82jx1ng5wmrrkj")))

(define-public crate-tarantool-proc-0.1.2 (c (n "tarantool-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cp2rjk7wv60xx6ncx9zblvxrj65bbrqm43xxcdx6r2f3a293qxs")))

(define-public crate-tarantool-proc-0.1.3 (c (n "tarantool-proc") (v "0.1.3") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1694hmw61nvjzi598ax7vr5mywlccmw4gn17kfrkpqsfa8nvbcby") (r "1.59")))

(define-public crate-tarantool-proc-0.2.0 (c (n "tarantool-proc") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1smll9wa7g24ww8xd95vwwr1lxkibqrj69qrrp1xwaz389w9fd2m") (r "1.61")))

(define-public crate-tarantool-proc-0.2.1 (c (n "tarantool-proc") (v "0.2.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "072ipxmrpzqcpjp5zyjzf5s2xwglaml3ngc9sxg7855qgfncwiyh") (r "1.61")))

(define-public crate-tarantool-proc-0.2.2 (c (n "tarantool-proc") (v "0.2.2") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d50hwbglkpg5b0vz20fv56h5wklinyaibpca7lb9m1kdh56mm3f") (y #t) (r "1.61")))

(define-public crate-tarantool-proc-1.0.0 (c (n "tarantool-proc") (v "1.0.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hql2dw17q52fvc8vnxcghd0k560p4hjc3s8ggabgzyc6dj3whny") (r "1.61")))

(define-public crate-tarantool-proc-3.0.0 (c (n "tarantool-proc") (v "3.0.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01byz0s778678kj80ma15vzn0jsgirh3rpyq7hx19z4g1pzvln3q") (r "1.61")))

