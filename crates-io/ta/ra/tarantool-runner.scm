(define-module (crates-io ta ra tarantool-runner) #:use-module (crates-io))

(define-public crate-tarantool-runner-0.1.0 (c (n "tarantool-runner") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "17w1dwgh0xs409l9k5qakhy6qb3hkzs9kck632sp2wfdg2l5wz5q")))

