(define-module (crates-io uh id uhidrs-sys) #:use-module (crates-io))

(define-public crate-uhidrs-sys-1.0.0 (c (n "uhidrs-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "1amx3nlzs63pm6ddvcw5niagy80jarzi5jdw06sw77bnl1l2m2yp")))

(define-public crate-uhidrs-sys-1.0.1 (c (n "uhidrs-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.54") (k 1)))) (h "0vlim703i6zggwnl1y93gd75315bzwkw9cv41vzbqf4k34d3pk5z")))

(define-public crate-uhidrs-sys-1.0.2 (c (n "uhidrs-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.63") (k 1)))) (h "061wbn0x85mr997fg4gc0f32z2lvy7m51i6y8dc2saa3p06v4ck4")))

(define-public crate-uhidrs-sys-1.0.3 (c (n "uhidrs-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.69.1") (k 1)))) (h "17rgcl5i0v6c0my4pxdarh8ksmnifd99ljqhv4g5f4g5rvxq0j72")))

