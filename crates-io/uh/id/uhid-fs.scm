(define-module (crates-io uh id uhid-fs) #:use-module (crates-io))

(define-public crate-uhid-fs-0.0.1 (c (n "uhid-fs") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "enumflags2") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uhid-sys") (r "^1") (d #t) (k 0)))) (h "0f1ixzv5dicw5wrq8fyj6zxb2ic4jaddb351021mgsq4qkh0gcym")))

