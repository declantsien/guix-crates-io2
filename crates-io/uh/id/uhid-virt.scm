(define-module (crates-io uh id uhid-virt) #:use-module (crates-io))

(define-public crate-uhid-virt-0.0.2 (c (n "uhid-virt") (v "0.0.2") (d (list (d (n "enumflags2") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uhid-sys") (r "^1") (d #t) (k 0)))) (h "08fqr46jm8ajagm746nlvcx99wsfqnylfnhbk0xym1xphpkavijp")))

(define-public crate-uhid-virt-0.0.3 (c (n "uhid-virt") (v "0.0.3") (d (list (d (n "enumflags2") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uhidrs-sys") (r "^1.0") (d #t) (k 0)))) (h "0h3338iha8s51h9gphl5wxw9p0i0pkp8i6lpvp2p72z38y3y7kh3")))

(define-public crate-uhid-virt-0.0.4 (c (n "uhid-virt") (v "0.0.4") (d (list (d (n "enumflags2") (r "^0.6.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "uhidrs-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0360jvimb59d6kxp2yz7gbvwxxsv6cwp6m90qabzsq7r10kcnzh5")))

(define-public crate-uhid-virt-0.0.5 (c (n "uhid-virt") (v "0.0.5") (d (list (d (n "enumflags2") (r "^0.6.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "uhidrs-sys") (r "^1.0.1") (d #t) (k 0)))) (h "08n8rys35bb948q2ydglinl4713r38k64bc0yjcvf6r8n3xpncby")))

(define-public crate-uhid-virt-0.0.6 (c (n "uhid-virt") (v "0.0.6") (d (list (d (n "enumflags2") (r "^0.7.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "uhidrs-sys") (r "^1.0.0") (d #t) (k 0)))) (h "16cl8pwbwmipvahghdshy5pzf2paqy3bg338c23dzlvsz1jy25jg")))

(define-public crate-uhid-virt-0.0.7 (c (n "uhid-virt") (v "0.0.7") (d (list (d (n "enumflags2") (r "^0.7.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "uhidrs-sys") (r "^1.0.3") (d #t) (k 0)))) (h "17r42j8p32d2d9hg5707gwzkm63a97j1plyd4ivg0vdr0gvg2w2g")))

