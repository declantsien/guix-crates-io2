(define-module (crates-io uh d- uhd-sys) #:use-module (crates-io))

(define-public crate-uhd-sys-0.1.0 (c (n "uhd-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "0xpidgmnvm0rj639mid26wvy3nrsm1lhrc4wgbzbkv6vwxnakw6a") (l "uhd")))

(define-public crate-uhd-sys-0.1.1 (c (n "uhd-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "1fdbfr50d6mp5gqlpgswgzipw0bj30rc1d82q3vpsfkf6jkz180s") (l "uhd")))

(define-public crate-uhd-sys-0.1.2 (c (n "uhd-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "1m9mn90bx2aa7rdzmdx8z8r26y89j0wdmrivlc2i3rq51ddnwsbm") (l "uhd")))

