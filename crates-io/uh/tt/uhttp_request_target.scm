(define-module (crates-io uh tt uhttp_request_target) #:use-module (crates-io))

(define-public crate-uhttp_request_target-0.5.0 (c (n "uhttp_request_target") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0hxj6mx0qf8khw6nqhrzidnnlzrrz5hrr7777zwsqhh3n9rx4kv6")))

(define-public crate-uhttp_request_target-0.6.0 (c (n "uhttp_request_target") (v "0.6.0") (h "1xg10n51kb8cdrim0n0k99h0gwc7mwygsx1v4cfb3hmh1w75nskp")))

