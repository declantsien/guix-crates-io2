(define-module (crates-io uh tt uhttp_sse) #:use-module (crates-io))

(define-public crate-uhttp_sse-0.5.0 (c (n "uhttp_sse") (v "0.5.0") (h "1kkhidr8akk1ryrplwj17dpmwqda1a028n6zh7hpbk1s1r7q16gv")))

(define-public crate-uhttp_sse-0.5.1 (c (n "uhttp_sse") (v "0.5.1") (h "0i4lln00csnifm2h4cbm7d2djgd6w7na66mvn4q64852bcs97zy6")))

