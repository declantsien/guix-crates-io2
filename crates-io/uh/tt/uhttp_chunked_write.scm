(define-module (crates-io uh tt uhttp_chunked_write) #:use-module (crates-io))

(define-public crate-uhttp_chunked_write-0.5.0 (c (n "uhttp_chunked_write") (v "0.5.0") (h "0my605jzi3y0pvwjsp572gmgjidliyh2bbpipzlmy0icpwsvij7d")))

(define-public crate-uhttp_chunked_write-0.5.1 (c (n "uhttp_chunked_write") (v "0.5.1") (h "1kns0vr8fna3v2brk6k19z4fc9sjcv5qwb96f0j328kax9vviv8a")))

