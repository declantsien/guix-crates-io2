(define-module (crates-io uh tt uhttp_body_bytes) #:use-module (crates-io))

(define-public crate-uhttp_body_bytes-0.5.0 (c (n "uhttp_body_bytes") (v "0.5.0") (h "1zhw1bgyyc3ak0qcqwjkipkfajabg7jrjqrlqm8d1c6al4wzxq4z")))

(define-public crate-uhttp_body_bytes-0.5.1 (c (n "uhttp_body_bytes") (v "0.5.1") (h "1w63phkdl8nfw0g1q6069ws0ass9x0c9p18my5yy0mwi21023n60")))

(define-public crate-uhttp_body_bytes-0.5.2 (c (n "uhttp_body_bytes") (v "0.5.2") (h "1b6fa807ikj8divbfgbllv68z8kprzx6pibi2i9y7lca7f227c6b")))

