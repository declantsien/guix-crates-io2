(define-module (crates-io uh tt uhttp_content_encoding) #:use-module (crates-io))

(define-public crate-uhttp_content_encoding-0.5.0 (c (n "uhttp_content_encoding") (v "0.5.0") (h "1fhmmpapx5hg35cr2fwgqsa28zvy52x4cpqj166rskm4q5lmgzjq")))

(define-public crate-uhttp_content_encoding-0.5.1 (c (n "uhttp_content_encoding") (v "0.5.1") (h "1h9c3c54h52zl044mwxx81x2zcan3f2d7v19h1pq5v0la8sg2r0m")))

