(define-module (crates-io uh tt uhttp_request) #:use-module (crates-io))

(define-public crate-uhttp_request-0.5.0 (c (n "uhttp_request") (v "0.5.0") (d (list (d (n "memchr") (r "^1.0") (d #t) (k 0)))) (h "11s6p8c1nl6gwc2r6n450rp0m2cj489sqgdnqwyhv45id6wah115")))

(define-public crate-uhttp_request-0.5.1 (c (n "uhttp_request") (v "0.5.1") (d (list (d (n "memchr") (r "^1.0") (d #t) (k 0)))) (h "10qjmdga4hn4wgm8pgdmzgfg6hy97h3vfwviqbacf9lnrik97qgs")))

