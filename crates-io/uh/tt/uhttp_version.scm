(define-module (crates-io uh tt uhttp_version) #:use-module (crates-io))

(define-public crate-uhttp_version-0.5.0 (c (n "uhttp_version") (v "0.5.0") (h "1xbz1qgzffwngmhx1zgfi2wcwsss4w6597nhs13wbq2af8cbz96q")))

(define-public crate-uhttp_version-0.6.0 (c (n "uhttp_version") (v "0.6.0") (h "1nv786wkq1w4j53fg6dpx7xw2rlqlls3vdxrm2s27aa45jrg6gmm")))

