(define-module (crates-io uh pp uhppote-derive) #:use-module (crates-io))

(define-public crate-uhppote-derive-0.1.0 (c (n "uhppote-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "09zkpv13j4kfj0s9abmwsanmj88y8sa6jkmkn16cl62rgkr4vn9s")))

