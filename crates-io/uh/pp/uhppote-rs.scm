(define-module (crates-io uh pp uhppote-rs) #:use-module (crates-io))

(define-public crate-uhppote-rs-0.0.1 (c (n "uhppote-rs") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "c_vec") (r "^2.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "uhppote-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0mci7hl3j4izbjmdkm9hiqdja0r36ddm8inzadk9qpzq82mfq4l3")))

(define-public crate-uhppote-rs-0.0.2 (c (n "uhppote-rs") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "c_vec") (r "^2.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "uhppote-sys") (r "^0.0.2") (d #t) (k 0)))) (h "12x42gqxm7chy0h3aa9q10n6v14h1iydxxh6l3apfaki9z8j3943")))

(define-public crate-uhppote-rs-0.0.3 (c (n "uhppote-rs") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "c_vec") (r "^2.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "uhppote-sys") (r "^0.0.2") (d #t) (k 0)))) (h "14q4sw9aypjb4rjyq8v277g3zxhsi5zmrn308fxgzn9n6hzhr82z")))

(define-public crate-uhppote-rs-0.0.4 (c (n "uhppote-rs") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "c_vec") (r "^2.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "uhppote-sys") (r "^0.0.3") (d #t) (k 0)))) (h "163ya3nd9hl75ddai16fhfislhw2qn4wap6gbwvsqnr9k3kdrgag")))

(define-public crate-uhppote-rs-0.0.5 (c (n "uhppote-rs") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (f (quote ("derive" "serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "uhppote-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0z7fc533lxnkxrvjvm5hzl6nlms6nrcfdx7gm71y0vrk1m981pv7")))

(define-public crate-uhppote-rs-0.1.0 (c (n "uhppote-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (f (quote ("derive" "serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "uhppote-derive") (r "^0.1.0") (d #t) (k 0)))) (h "18cm93n2faazcwlid9n4bnl2rmah9jmcypzmlv5hh96qq69yvf6v")))

