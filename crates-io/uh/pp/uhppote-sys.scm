(define-module (crates-io uh pp uhppote-sys) #:use-module (crates-io))

(define-public crate-uhppote-sys-0.0.1 (c (n "uhppote-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0cif33ygm8wnjk410262dbsakcha70dl1v8h54aarcxhbv8qbqvl") (y #t)))

(define-public crate-uhppote-sys-0.0.2 (c (n "uhppote-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1h88xjwlic72vid9brkd8lr6kzqfigrhlxchbn4gccskjx7rdfzx") (y #t)))

(define-public crate-uhppote-sys-0.0.3 (c (n "uhppote-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1xgqy2sy9zp0l22zw4wxf7w1n0w52jrs68jsa95iw4hg0jf1sjxl") (y #t)))

(define-public crate-uhppote-sys-0.0.4 (c (n "uhppote-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0ffbclyhn9x5zmcv2cb30z66lh7rmag88qrinjz5hda9yyl767d5") (y #t)))

