(define-module (crates-io gd #{-i}# gd-icon-renderer) #:use-module (crates-io))

(define-public crate-gd-icon-renderer-1.0.0 (c (n "gd-icon-renderer") (v "1.0.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)))) (h "1870h9b5yispl9dn7yj46rj34p0ma7hs0540c1hlpmhq5lkjm230")))

(define-public crate-gd-icon-renderer-1.0.1 (c (n "gd-icon-renderer") (v "1.0.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)))) (h "0plgzsb0l67687hy9kasjkv763dqw363dd4zr3ny0cxx7g99m6kc")))

(define-public crate-gd-icon-renderer-1.0.2 (c (n "gd-icon-renderer") (v "1.0.2") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)))) (h "0fv5ppxpmxbdx1l1a8aw877isxwq6avjzvxh3c8l5lql652b2hzy")))

(define-public crate-gd-icon-renderer-1.1.0 (c (n "gd-icon-renderer") (v "1.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)))) (h "0gm7npppy1wcvngps52i4l7bcw8vlrkyc9ppp769065z6l3yv123")))

(define-public crate-gd-icon-renderer-1.2.0 (c (n "gd-icon-renderer") (v "1.2.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)))) (h "1hjrgx3ngri465h8mwprpcvm1h34rpcck4z7j7zsdlnkcnci3gjr")))

(define-public crate-gd-icon-renderer-2.0.0 (c (n "gd-icon-renderer") (v "2.0.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)))) (h "18nl28rdw09dwlbanx87yrzb5ffs91hhrm28vzws8233h9gjmd5y")))

