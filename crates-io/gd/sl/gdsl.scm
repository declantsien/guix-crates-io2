(define-module (crates-io gd sl gdsl) #:use-module (crates-io))

(define-public crate-gdsl-0.0.1 (c (n "gdsl") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "min-max-heap") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1s4qnzm0vr8bh3g5ahpc2sf11likzglx3x8smw61mfmb8man9i54")))

(define-public crate-gdsl-0.1.0 (c (n "gdsl") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "min-max-heap") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "04h5250xhn6mv0f65ggax4jf17i8wlwz6xpxm6np35ll5k50wgx8")))

(define-public crate-gdsl-0.1.1 (c (n "gdsl") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "min-max-heap") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "1v1sphjsi5nlzl752cdcff4b0j5jh9pj2f8417n89dr3a1z822qn")))

(define-public crate-gdsl-0.2.0 (c (n "gdsl") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "min-max-heap") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "0v097pc82rqakgwpr82wdh3pdq8n9z7dwnjvd11a3wspvbw75p6v")))

(define-public crate-gdsl-0.2.1 (c (n "gdsl") (v "0.2.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "045waz7p96ibgqq0xn6pigzn4ygcz4s74x1js0wmjl960rm1g593")))

