(define-module (crates-io gd bm gdbm_compat) #:use-module (crates-io))

(define-public crate-gdbm_compat-0.1.0 (c (n "gdbm_compat") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0yyc2hf2dhh06slp1xd0lw7yzn6xdqpiiw55gcr1w470knspy8z5")))

(define-public crate-gdbm_compat-0.1.1 (c (n "gdbm_compat") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "16vgzn9y7g2w123bdgfbw7j77prq75vqss2acjs7k9fr32j0527d")))

