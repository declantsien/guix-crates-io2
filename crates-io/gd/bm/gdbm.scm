(define-module (crates-io gd bm gdbm) #:use-module (crates-io))

(define-public crate-gdbm-0.1.0 (c (n "gdbm") (v "0.1.0") (d (list (d (n "bitflags") (r "~0.8") (d #t) (k 0)) (d (n "gdbm-sys") (r "~0.2") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "0ql690klccbn6vvj2q0qjk1jlg2fb966q3hbb1sw2p1q37syqgnp")))

(define-public crate-gdbm-0.1.1 (c (n "gdbm") (v "0.1.1") (d (list (d (n "bitflags") (r "~0.8") (d #t) (k 0)) (d (n "gdbm-sys") (r "~0.2") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "16jxxabar6yb5igshhdncv96mk9ad2shayvyadvwpxzjkjamlsfx")))

