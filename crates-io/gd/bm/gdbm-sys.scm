(define-module (crates-io gd bm gdbm-sys) #:use-module (crates-io))

(define-public crate-gdbm-sys-0.1.0 (c (n "gdbm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "~0.24") (d #t) (k 1)))) (h "0ddbha9rhdi7lxa34yhm90ss107df7p7376aqw567b9qiv6l1s6j")))

(define-public crate-gdbm-sys-0.2.1 (c (n "gdbm-sys") (v "0.2.1") (h "1f5s27263lgnddsv5sybdddmq2nd97crlavvdnmb5y6gx6x421pa")))

(define-public crate-gdbm-sys-0.2.2 (c (n "gdbm-sys") (v "0.2.2") (h "07s9knk322car3b9sq4rhndy39kwjqwbh79ad2nrwgca11vp31zc")))

(define-public crate-gdbm-sys-0.3.0 (c (n "gdbm-sys") (v "0.3.0") (h "1mjz7vjymmfm960pf0m4dn7drl91034z2wfffyiqw7bzmfckrjqc")))

