(define-module (crates-io gd eh gdeh0154d67) #:use-module (crates-io))

(define-public crate-gdeh0154d67-0.1.0 (c (n "gdeh0154d67") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0sazqaqsannrrh2sa1f6nnskilkis7xyndmkmm15irqi1pswmbvq")))

(define-public crate-gdeh0154d67-0.1.1 (c (n "gdeh0154d67") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0c08v0fsg7w6rxnj5r5p337q9n3r4xpk6gy5d2vw8wyygsvgm3zz")))

(define-public crate-gdeh0154d67-0.1.2 (c (n "gdeh0154d67") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "15hn3q6ql8xdg1i671srn72zb1vfh92c4j3w8pva831y1g2qjjcb")))

(define-public crate-gdeh0154d67-0.2.0 (c (n "gdeh0154d67") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-rc.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "02s9izq404cw257lnrv8k1hfg5n672sj9wn9m2s3gx651c1lziff") (f (quote (("std") ("heap_buffer"))))))

