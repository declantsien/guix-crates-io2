(define-module (crates-io gd b- gdb-command) #:use-module (crates-io))

(define-public crate-gdb-command-0.1.0 (c (n "gdb-command") (v "0.1.0") (h "093v344vj4ngnyg78vssps3v3v87vqm9fhy15s2wc7vswn4y7ms6")))

(define-public crate-gdb-command-0.2.0 (c (n "gdb-command") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1rp067x4b6h4xii3irbpp5d4vwnhz86mb116jlzbzvw89mqfwzka")))

(define-public crate-gdb-command-0.3.0 (c (n "gdb-command") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "11pmhz3qhgisi3nxay3yjl5dsziq7cqqffmwi0h5x4rnv65k0fwp")))

(define-public crate-gdb-command-0.4.0 (c (n "gdb-command") (v "0.4.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0m9f81jnlx8yva8dqvcfnw009bsqdibh0hjyk9jm2vc1vzzy4n71")))

(define-public crate-gdb-command-0.5.0 (c (n "gdb-command") (v "0.5.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0j6q5xbpppfwqn7yh34sb4aqha6g2yxyjfn00z4wancvq5xnbk8l")))

(define-public crate-gdb-command-0.5.1 (c (n "gdb-command") (v "0.5.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "02834nknvmx21c1bsvngscbvrx3ik3dmrj4xlxmbd2dq2bjna76q")))

(define-public crate-gdb-command-0.5.2 (c (n "gdb-command") (v "0.5.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1z5s4zc68w206fczib9ja4831338h8k6qpvk8bnw8spk2ipch5j8")))

(define-public crate-gdb-command-0.6.0 (c (n "gdb-command") (v "0.6.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1d6y3ifw83z3s0chrb9ni07hj92n6n0szw9zj4wjrps5wc8gkxli")))

(define-public crate-gdb-command-0.6.1 (c (n "gdb-command") (v "0.6.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "07nir0s898w3chwhws4vfg948z40frm7lhf2iz9s6bmwa93qbia6")))

(define-public crate-gdb-command-0.6.2 (c (n "gdb-command") (v "0.6.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0vxkr98qiqxphi7ahzhgj9gmkc40hkwnhw8f82y9yxgsya7w9dpx")))

(define-public crate-gdb-command-0.6.3 (c (n "gdb-command") (v "0.6.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "08m4xmhs958346pnq60bxrdn0zczmccqj6rc31qaqr7ip09gv489")))

(define-public crate-gdb-command-0.7.0 (c (n "gdb-command") (v "0.7.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "024y1cv7psh66wfsga4zxhbzjwsix73klzhs3x2rssdwd1f21nqh")))

(define-public crate-gdb-command-0.7.1 (c (n "gdb-command") (v "0.7.1") (d (list (d (n "regex") (r "~1.5") (d #t) (k 0)))) (h "0c4ragx59sb3hlxkhffd5ahdyx6jkr3kw5fq3qi7255v65g14s1y")))

(define-public crate-gdb-command-0.7.2 (c (n "gdb-command") (v "0.7.2") (d (list (d (n "regex") (r "~1.5") (d #t) (k 0)))) (h "14asswh6af1lq4nk0z25ki5ysg7v7qnwy007ck51i3xs1fkg2vq5")))

(define-public crate-gdb-command-0.7.3 (c (n "gdb-command") (v "0.7.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16zwykgkh0qqsar590yvakkp67snjf7gxrgr923az7k3ahlcwsl6")))

(define-public crate-gdb-command-0.7.4 (c (n "gdb-command") (v "0.7.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14sid1r33b5spsh7298w35d8g3b29122va7i287hqdmypwgqvk2n")))

(define-public crate-gdb-command-0.7.5 (c (n "gdb-command") (v "0.7.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "05w9x2lc1cn6x8wfxvh49mq8cxych04pzi40grzzhiylgxikh2rp")))

(define-public crate-gdb-command-0.7.6 (c (n "gdb-command") (v "0.7.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "0pacm5g8ssr8chhnfxy318jwamrgdm9kfgbx6p1cgpxkkv26g829")))

(define-public crate-gdb-command-0.7.7 (c (n "gdb-command") (v "0.7.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "1ls0r7apbnhvvdqhif8sw3mv64mi1gryhg878rmis5qh0nsgsdkk")))

(define-public crate-gdb-command-0.7.8 (c (n "gdb-command") (v "0.7.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "06b3wvaf05nc43hg95k4x6a9jn9irzvyjcvv2ghnh1rc5wp7dckk")))

