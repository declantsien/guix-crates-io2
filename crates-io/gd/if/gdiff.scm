(define-module (crates-io gd if gdiff) #:use-module (crates-io))

(define-public crate-gdiff-0.0.1 (c (n "gdiff") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "tui") (r "^0.13.0") (f (quote ("crossterm"))) (k 0)))) (h "0g719q181zn0hq1fdmfgsy6q1qf2x21bv67r1dlx9jyz20z63sw6")))

