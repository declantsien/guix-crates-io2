(define-module (crates-io gd bs gdbstub_arch) #:use-module (crates-io))

(define-public crate-gdbstub_arch-0.1.0 (c (n "gdbstub_arch") (v "0.1.0") (d (list (d (n "gdbstub") (r "^0.5") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1gz96b06ca19capyadb59jkbnm3k0hci288mmvifrpy6c8j5r92d")))

(define-public crate-gdbstub_arch-0.1.1 (c (n "gdbstub_arch") (v "0.1.1") (d (list (d (n "gdbstub") (r "^0.5") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "05iwqfbn74zif9g4pdw7p4497102nmxy8qlh15kax3j6w70bjn73")))

(define-public crate-gdbstub_arch-0.2.0 (c (n "gdbstub_arch") (v "0.2.0") (d (list (d (n "gdbstub") (r "^0.6") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1nsm3fn35yfvyfg64kxs314ky7qak1nmg6qgw90rf3piwd7lvls0")))

(define-public crate-gdbstub_arch-0.2.1 (c (n "gdbstub_arch") (v "0.2.1") (d (list (d (n "gdbstub") (r "^0.6") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1q1axdx954rwq87wl2hsi9pdzg95w1wf9w4lx4p2s2kyk08abibn")))

(define-public crate-gdbstub_arch-0.2.2 (c (n "gdbstub_arch") (v "0.2.2") (d (list (d (n "gdbstub") (r "^0.6") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0dsdjd2lxc37ykz6q7mii4xjg01dsdnrdh05hqgd4xmc31blpp2i")))

(define-public crate-gdbstub_arch-0.2.3 (c (n "gdbstub_arch") (v "0.2.3") (d (list (d (n "gdbstub") (r "^0.6") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0s52yy3bcmm163rl9bpcjv7zq9c06f55mwvd7l35lv2mm6dlcky2")))

(define-public crate-gdbstub_arch-0.2.4 (c (n "gdbstub_arch") (v "0.2.4") (d (list (d (n "gdbstub") (r "^0.6") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "177a3p17k1qg80hskg0dybjirs5hppdp946y1nh96df4amn57jzf")))

(define-public crate-gdbstub_arch-0.3.0 (c (n "gdbstub_arch") (v "0.3.0") (d (list (d (n "gdbstub") (r "^0.7") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0zcpqwn7dxa365fvdl1i8b8nb25k1ap2f4v0lq4zq0rjpmbi6fsf")))

