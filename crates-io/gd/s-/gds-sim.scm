(define-module (crates-io gd s- gds-sim) #:use-module (crates-io))

(define-public crate-gds-sim-0.1.0 (c (n "gds-sim") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1i6za29y9c7yvv5y6npmim5ckp0jczzj0j9kzad8zcy5iq5fg5fs")))

(define-public crate-gds-sim-0.1.1 (c (n "gds-sim") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q5q1dn9953f74zazmk7kqf91vckwsnphy205dvq059qkj0amayd")))

