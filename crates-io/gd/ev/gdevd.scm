(define-module (crates-io gd ev gdevd) #:use-module (crates-io))

(define-public crate-gdevd-0.2.0 (c (n "gdevd") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "dbus-tree") (r "^0.9.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "rusb") (r "^0.9.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (k 0)))) (h "1swbj7bzq9maq5y2c8yndaczypbcpbyaacj3p42d8cd7mv5w71rc")))

