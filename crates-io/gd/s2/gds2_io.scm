(define-module (crates-io gd s2 gds2_io) #:use-module (crates-io))

(define-public crate-gds2_io-0.2.0 (c (n "gds2_io") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d2asby5l5l2813rgmqq7dxvlxin0mjk0q6ig12m4l9bv4h9xsw5")))

(define-public crate-gds2_io-0.2.1 (c (n "gds2_io") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1imnbxmh7yybq3pmp8r0hx07qwwpms5pih85nhvdygb99w1a9axp")))

