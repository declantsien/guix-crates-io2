(define-module (crates-io gd va gdvariants) #:use-module (crates-io))

(define-public crate-gdvariants-0.1.0-pre.0 (c (n "gdvariants") (v "0.1.0-pre.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "1kxjssik821x5gcjw1d3mxgrzmgcm16qy9whzb9yhm1m3pbynh05") (y #t)))

(define-public crate-gdvariants-0.1.0-pre.1 (c (n "gdvariants") (v "0.1.0-pre.1") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "0c3qx8z0qrvmxabygvs4fr363y0xg5527ykag9dvgvk50wswrahq") (y #t)))

(define-public crate-gdvariants-0.1.0-pre.2 (c (n "gdvariants") (v "0.1.0-pre.2") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "0jzk4xs7z73klcf68vs80pj9xpwhb11542b179kyf5lfcyqaw35d") (y #t)))

(define-public crate-gdvariants-0.1.0 (c (n "gdvariants") (v "0.1.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "1i232c6dn53kpbj7saf9zb4qarx5c5pbmzaiad8d5d17l60mycm3")))

(define-public crate-gdvariants-0.2.0 (c (n "gdvariants") (v "0.2.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "12dzw7laba4rsvfwivy14jcvfzlsdam3v10f5q5cw2p7qrg85315")))

(define-public crate-gdvariants-0.3.0 (c (n "gdvariants") (v "0.3.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "11a3wksx3f6jmkaqv1lz3q360mib406ipkvhaqk51yra2r92l50h")))

(define-public crate-gdvariants-0.4.0 (c (n "gdvariants") (v "0.4.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "13dncng618jqbxjffp7dfh1ncz8v0vnqkp0g1vg9m9mi3yxz9fbw")))

(define-public crate-gdvariants-0.5.0 (c (n "gdvariants") (v "0.5.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "11rl53yljf7rzwk18wdxhyi86x6v1j2q0x470sga7m8pfwag532l")))

(define-public crate-gdvariants-0.6.0 (c (n "gdvariants") (v "0.6.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "03q6imr3ifk5289s31d1zfphmgsm3c69ggb92nrx8mx2nhvb86bg")))

(define-public crate-gdvariants-0.7.0 (c (n "gdvariants") (v "0.7.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "1pzhaqdp435jbnbd4sf5iq2b25rvsbxfk1z5rpx6hzqi0d44bmdc")))

(define-public crate-gdvariants-1.0.0 (c (n "gdvariants") (v "1.0.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)))) (h "0zwzj94wr0wkz7pzrm98j4n43hvz5p48jdl2prwdryzf4b1k1bb3")))

(define-public crate-gdvariants-1.1.0 (c (n "gdvariants") (v "1.1.0") (d (list (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0j9j67mlfimbxggwcgsa6xr62gxw144hydbr0mwnxwdqw79pynpx") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

