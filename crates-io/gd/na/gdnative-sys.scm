(define-module (crates-io gd na gdnative-sys) #:use-module (crates-io))

(define-public crate-gdnative-sys-0.1.0 (c (n "gdnative-sys") (v "0.1.0") (h "1cr4z8c7nx91ql0ka3z33s8xdagqjz1gi2k2icj7f5ga7kwyp640")))

(define-public crate-gdnative-sys-0.1.1 (c (n "gdnative-sys") (v "0.1.1") (h "0q9yggj8rx89d0zd94y2m3zsx4vny09x90acgfb9370xm39ak0cc")))

(define-public crate-gdnative-sys-0.2.0 (c (n "gdnative-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wi0bvj3857rsyydlcrlpq1jnwg15hjygxg18iknjs0nc1sy6fxs")))

(define-public crate-gdnative-sys-0.3.0 (c (n "gdnative-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yrhmmbyh7q12lbbx5vk8ykffx61f6xyax4v9fy66phwkhryyfz0")))

(define-public crate-gdnative-sys-0.5.0 (c (n "gdnative-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1glhkjfi177wzqvyvbwrq80jh3dkdjhp05f3cdz6zks7hna5vlph")))

(define-public crate-gdnative-sys-0.6.0 (c (n "gdnative-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bafmzgy6fidjj1ywjl90s70vvp7n0hk5i9747a89l7z981i19sc")))

(define-public crate-gdnative-sys-0.7.0 (c (n "gdnative-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.51.1") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0g1s4gvyfs94066a2d14rb8af28ap42iw75anky4clkrxm94ywmc")))

(define-public crate-gdnative-sys-0.8.0 (c (n "gdnative-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.51.1") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0d0sk6z55n9a7m44mk56nkv1df7gsk7zjhfp8l6l1spqph4a0knz")))

(define-public crate-gdnative-sys-0.8.1 (c (n "gdnative-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.51.1") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0ryizl0j010p1qvhjn8g5g1g511kkkp74j3p3ximld1s2k85lbiz")))

(define-public crate-gdnative-sys-0.9.0-preview.0 (c (n "gdnative-sys") (v "0.9.0-preview.0") (d (list (d (n "bindgen") (r "^0.54.0") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "=0.1.13") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "199s6r6ip4znbmx3z4804zx8ng7gbgzf749szqd26hgr5fh1mrcm")))

(define-public crate-gdnative-sys-0.9.0 (c (n "gdnative-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "=0.1.13") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "1bqdqj3ras0hxm9z1pcvsgay2706qgjvfa321wyb9yvkmif9ahb2")))

(define-public crate-gdnative-sys-0.9.1 (c (n "gdnative-sys") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "=0.1.13") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "1kxwmk2ghwaxpda1w44pavgymalfw986d0k5hks15gqdybkycnjb")))

(define-public crate-gdnative-sys-0.9.2 (c (n "gdnative-sys") (v "0.9.2") (d (list (d (n "bindgen") (r "^0.56.0") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "=0.1.13") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "1mv5jnm3gzyymddmsbmhiyhcw3rjmg7mj84g9zvw77yyal0jmz7g") (y #t)))

(define-public crate-gdnative-sys-0.9.3 (c (n "gdnative-sys") (v "0.9.3") (d (list (d (n "bindgen") (r "^0.56.0") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "=0.1.13") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "1wppxl8yyp3yxa7hk1fmzykwfb9jpgz1kkw8fz74aww76gk1z4cp")))

(define-public crate-gdnative-sys-0.10.0-rc.0 (c (n "gdnative-sys") (v "0.10.0-rc.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "041zyfls5bshmb562yabd7grnj87sbgjg9ysf9fhgy0iiv86d11l")))

(define-public crate-gdnative-sys-0.10.0 (c (n "gdnative-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.10") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "1phsy0lj6m6pdy4dhwp6ja9dvsr3l7mwm8vfzi57sa67mm7rffl1") (r "1.56")))

(define-public crate-gdnative-sys-0.10.1 (c (n "gdnative-sys") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.60.1") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.16") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "0x94y007qangh41bh5zldw0r8bv8j0354qj1dzrd4bx67zx6z0cy") (r "1.56")))

(define-public crate-gdnative-sys-0.10.2 (c (n "gdnative-sys") (v "0.10.2") (d (list (d (n "bindgen") (r "^0.60.1") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.16") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "1wzm5dyc22dyccd2h23b9cc9lplcavjscgajlp9qkwikdzaigvci") (r "1.56")))

(define-public crate-gdnative-sys-0.11.0 (c (n "gdnative-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.60.1") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.16") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "1l8lm772phlk6y4xvzi9p6hd5a2nyy44m3qf94n94pmgzivscwp5") (r "1.63")))

(define-public crate-gdnative-sys-0.11.1 (c (n "gdnative-sys") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.16") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "02xj3x9manqipbh2p4w2fz5sy51y7vxrqh8xwlgyrs90qqrk40sk") (r "1.63")))

(define-public crate-gdnative-sys-0.11.2 (c (n "gdnative-sys") (v "0.11.2") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.16") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "0izf4ff02vsi6jc9gpvc467q3kn8mhjbsjfiflcnhc53lil3bw9l") (r "1.63")))

(define-public crate-gdnative-sys-0.11.3 (c (n "gdnative-sys") (v "0.11.3") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.16") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)) (d (n "semver") (r "^1") (d #t) (k 1)))) (h "0z2d10knvddlf0qal89scf9sin0rinwmy4rrv17h0w147k7nafqg") (r "1.63")))

