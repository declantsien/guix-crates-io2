(define-module (crates-io gd na gdnative-editor) #:use-module (crates-io))

(define-public crate-gdnative-editor-0.5.0 (c (n "gdnative-editor") (v "0.5.0") (d (list (d (n "gdnative-animation") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative-ui") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "0dszs90g639ijjymbkp323zkz02y44ph252ng18h4amgxbaj06hf") (f (quote (("gd_test"))))))

(define-public crate-gdnative-editor-0.6.0 (c (n "gdnative-editor") (v "0.6.0") (d (list (d (n "gdnative-animation") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative-ui") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0k13vd1sn1x8j3gccn629pbchry5s756nk3zidpixqxngj90rp40") (f (quote (("gd_test"))))))

(define-public crate-gdnative-editor-0.6.1 (c (n "gdnative-editor") (v "0.6.1") (d (list (d (n "gdnative-animation") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative-ui") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "05ifx2flc1qzzza0snjrzilmb1a6xqxv31bqs5rm5xp5sn63swsk") (f (quote (("gd_test"))))))

