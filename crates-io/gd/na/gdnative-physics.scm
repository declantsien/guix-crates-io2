(define-module (crates-io gd na gdnative-physics) #:use-module (crates-io))

(define-public crate-gdnative-physics-0.5.0 (c (n "gdnative-physics") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "1ly2g5q35my5x4g5c00sbbwjfqh504zqf5plfdj4qf6vwabqk6pj") (f (quote (("gd_test"))))))

(define-public crate-gdnative-physics-0.6.0 (c (n "gdnative-physics") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0ksgfps26cifg0s3p2ymmd6a339nw2vkk0ah3694icaq0d7rlkwv") (f (quote (("gd_test"))))))

(define-public crate-gdnative-physics-0.6.1 (c (n "gdnative-physics") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "1dx275ng274bqc9gyvz4c7ff0kwkl8w6m3hvi031ynd08dkkfyf8") (f (quote (("gd_test"))))))

