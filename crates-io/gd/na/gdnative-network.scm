(define-module (crates-io gd na gdnative-network) #:use-module (crates-io))

(define-public crate-gdnative-network-0.5.0 (c (n "gdnative-network") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "0a1smahqh8v18xy706pxi67yspp3ihx4pnbmp0yvw5c5j0kmxvqq") (f (quote (("gd_test"))))))

(define-public crate-gdnative-network-0.6.0 (c (n "gdnative-network") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "131gjf5snhb8s1ym7l5ck8dpjiph30j3rin3c2rsvlih2kn5dbcv") (f (quote (("gd_test"))))))

(define-public crate-gdnative-network-0.6.1 (c (n "gdnative-network") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0yjv1bxz7c07yrqn2caxmkpvxyn31j4z52flj0sim6vnrf0r91df") (f (quote (("gd_test"))))))

