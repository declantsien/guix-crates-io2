(define-module (crates-io gd na gdnative-input) #:use-module (crates-io))

(define-public crate-gdnative-input-0.5.0 (c (n "gdnative-input") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "1sg8ghrbsc6cqdh15h92ck2ghdzj07dm92g66h54g3wskmwpipjn") (f (quote (("gd_test"))))))

(define-public crate-gdnative-input-0.6.0 (c (n "gdnative-input") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0r8a0vmyi7a91cly7nn95vg7p5pcli0fjspwkba5qb573rvj7c8w") (f (quote (("gd_test"))))))

(define-public crate-gdnative-input-0.6.1 (c (n "gdnative-input") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "1xvnz2zp780r1nr4haybil11aanznnnrdf61swjk1psgp30fzm7l") (f (quote (("gd_test"))))))

