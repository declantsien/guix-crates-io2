(define-module (crates-io gd na gdnative-project-utils) #:use-module (crates-io))

(define-public crate-gdnative-project-utils-0.1.0 (c (n "gdnative-project-utils") (v "0.1.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "01rj27fy735p3bdjkas1h9cng0plp7jabmdd9sql4ryc13lm1m6i")))

(define-public crate-gdnative-project-utils-0.1.1 (c (n "gdnative-project-utils") (v "0.1.1") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "00h7f9nmiiqpx866k0vadja4q45f62p7dmgacrwj321q318iwqcp")))

(define-public crate-gdnative-project-utils-0.1.2 (c (n "gdnative-project-utils") (v "0.1.2") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "15vw71083jsjgsii861dbfd0blsa1xz4jg1jq04pxwjzx8qnp4h0")))

(define-public crate-gdnative-project-utils-0.1.3 (c (n "gdnative-project-utils") (v "0.1.3") (d (list (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1xkfsb2nv1r3x0aci5jxxwd1wib1y5mascirsxv5hcw7cm7g76m1") (f (quote (("default" "build_script") ("build_script"))))))

(define-public crate-gdnative-project-utils-0.1.4 (c (n "gdnative-project-utils") (v "0.1.4") (d (list (d (n "dunce") (r "^1.0.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1n7zbnclmm5klgbdpyvswin6ba2s3a94bhfk1xjzkj7dzxay9jwb") (f (quote (("default" "build_script") ("build_script"))))))

