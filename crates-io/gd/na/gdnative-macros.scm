(define-module (crates-io gd na gdnative-macros) #:use-module (crates-io))

(define-public crate-gdnative-macros-0.1.0 (c (n "gdnative-macros") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syntex") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (d #t) (k 0)))) (h "1al45lp6msl2jr5hmkl00g0f0n4hy1nckrs6xxzii9l26g3a09mi")))

(define-public crate-gdnative-macros-0.2.0 (c (n "gdnative-macros") (v "0.2.0") (h "1b2bv00r45pafacgyhqvnisrg3cnjgz0m0swfkkhw7jbl8nh6y57")))

