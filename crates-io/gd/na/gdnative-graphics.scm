(define-module (crates-io gd na gdnative-graphics) #:use-module (crates-io))

(define-public crate-gdnative-graphics-0.5.0 (c (n "gdnative-graphics") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "1h9xfcay75kzmm3apg929k7kmv1ibqzpnn6cj1dn28ahcizqfd4w") (f (quote (("gd_test"))))))

(define-public crate-gdnative-graphics-0.6.0 (c (n "gdnative-graphics") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0vmpzapgyrfjjimc9sqxnkmb9j5j6qbrgy1d35qf653ivcjd0ziy") (f (quote (("gd_test"))))))

(define-public crate-gdnative-graphics-0.6.1 (c (n "gdnative-graphics") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "1zralxm8bsgndp0dnpx6iadh4r3afcmwb2j32lk1qhwjkr6a5g0d") (f (quote (("gd_test"))))))

