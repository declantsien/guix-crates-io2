(define-module (crates-io gd na gdnative-visual_script) #:use-module (crates-io))

(define-public crate-gdnative-visual_script-0.5.0 (c (n "gdnative-visual_script") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "1257blprbcw775a7ynmid6k683hg8v448v56kpm56n8wv9j180fw") (f (quote (("gd_test"))))))

(define-public crate-gdnative-visual_script-0.6.0 (c (n "gdnative-visual_script") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0rq9mwmx4awxyam2yvv0f2ga7hp6ngjifsz89v3v64nx35gcsqha") (f (quote (("gd_test"))))))

(define-public crate-gdnative-visual_script-0.6.1 (c (n "gdnative-visual_script") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0v3al9sm6yp1izdwdasqkp2qdl0k268v6hl9mm455jh5kx36gcml") (f (quote (("gd_test"))))))

