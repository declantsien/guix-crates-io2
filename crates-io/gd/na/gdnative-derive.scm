(define-module (crates-io gd na gdnative-derive) #:use-module (crates-io))

(define-public crate-gdnative-derive-0.7.0 (c (n "gdnative-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1dsjj34hrg0bzq4x6smkfa61r7hv7kgfy6a988airqz0cfizd7nz")))

(define-public crate-gdnative-derive-0.8.0 (c (n "gdnative-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0zc9ipja8dfh70q00hc5vmb7za162vy3w47g5ks0ynjibpmlqmal")))

(define-public crate-gdnative-derive-0.8.1 (c (n "gdnative-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0kn4kvdj0cws2wygh8dd20ypcinz76mffhlb9df8jhg43a05hkvc")))

(define-public crate-gdnative-derive-0.9.0-preview.0 (c (n "gdnative-derive") (v "0.9.0-preview.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "131qql4rqcwik88xv6xg2narwfr79d3b7vnk1gdm2kga1z8n1ch4")))

(define-public crate-gdnative-derive-0.9.0 (c (n "gdnative-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0jwkdvpcncw0mag5xcvf589h3jknpwhyg362bdh5kg42mgdi4c00")))

(define-public crate-gdnative-derive-0.9.1 (c (n "gdnative-derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0swihaqvjnvkkavx9bsic61w7ks0hmyhlaak3jl3qy1dlp84ahj9")))

(define-public crate-gdnative-derive-0.9.2 (c (n "gdnative-derive") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "01dawwpm26167szwcxmaqf7rn5x8w3jgcs2f8pzrhy5xqhqc3kf8") (y #t)))

(define-public crate-gdnative-derive-0.9.3 (c (n "gdnative-derive") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1kgis861as56nzqrg2i9ycxkcwnq285zhrzm5ln3vnyaglygr842")))

(define-public crate-gdnative-derive-0.10.0-rc.0 (c (n "gdnative-derive") (v "0.10.0-rc.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0sawgc9hz3nkzp01420vvzz17f13w2c3synzfdf0sn5z8sc9mzq0")))

(define-public crate-gdnative-derive-0.10.0 (c (n "gdnative-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0wx9c3zs34c4rnhjhwijjcr0l68v8ippcqb76hcb9v6jqwhjj7cg") (r "1.56")))

(define-public crate-gdnative-derive-0.10.1 (c (n "gdnative-derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0a95azv3av950bh9fddzwp75vckvj205wsjh763r3inksv4rmplw") (r "1.56")))

(define-public crate-gdnative-derive-0.10.2 (c (n "gdnative-derive") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1v9bm8y9nlbi8jny0vkqhrp47db65xw8dcm0lgqxx9hn1iyzl21r") (r "1.56")))

(define-public crate-gdnative-derive-0.11.0 (c (n "gdnative-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "109wbb303mfkmdhzbb5y4anyw0s1vz2z66fix40ck5vpfwnwnywg") (r "1.63")))

(define-public crate-gdnative-derive-0.11.1 (c (n "gdnative-derive") (v "0.11.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1zp1y2dd79a0a41c3d5m7wkdvm89hxkb13bvwgz6lz8mn3wvzral") (r "1.63")))

(define-public crate-gdnative-derive-0.11.2 (c (n "gdnative-derive") (v "0.11.2") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1pa1m6181rbp1zsaz1s9cl7wp6i65id5a9fv3gbkg5scsc2ri718") (r "1.63")))

(define-public crate-gdnative-derive-0.11.3 (c (n "gdnative-derive") (v "0.11.3") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1w25ywd8mar2jr0v302vrpmj9i11wlgs28pv6pkkqb9yabj7g6ps") (r "1.63")))

