(define-module (crates-io gd na gdnative-animation) #:use-module (crates-io))

(define-public crate-gdnative-animation-0.5.0 (c (n "gdnative-animation") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "1pk8k6glri7v56wbisi6v7933mz6ag3fp7hvrq7sjmnfp3428yxr") (f (quote (("gd_test"))))))

(define-public crate-gdnative-animation-0.6.0 (c (n "gdnative-animation") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0ilrw0knasyf221kq3sn5z1dswfw4av6k66f0hjqynv45a3l0c4y") (f (quote (("gd_test"))))))

(define-public crate-gdnative-animation-0.6.1 (c (n "gdnative-animation") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0w8xjz3fi1j4g16vdkwqgjx4wj1zncf67zbp82d97rr3khhci07k") (f (quote (("gd_test"))))))

