(define-module (crates-io gd na gdnative-impl-proc-macros) #:use-module (crates-io))

(define-public crate-gdnative-impl-proc-macros-0.9.0-preview.0 (c (n "gdnative-impl-proc-macros") (v "0.9.0-preview.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0d8lm96dzfg7afivk205fdfjvi63mpb5s0v3fxkbsbphl8y6jj87")))

(define-public crate-gdnative-impl-proc-macros-0.9.0 (c (n "gdnative-impl-proc-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "14fz7y47avvy2vli4f8xwh6dl1dplsgridzmfcmk23dsw4w587y8")))

(define-public crate-gdnative-impl-proc-macros-0.9.1 (c (n "gdnative-impl-proc-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1mkhxkmlxnyhqd891vvw4awcpliipcka76gbs6khafn7pzidiqra")))

(define-public crate-gdnative-impl-proc-macros-0.9.2 (c (n "gdnative-impl-proc-macros") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "16x0n24ybn523cw24y32blw9q2z11s3mxmp9nfh023vmxs8wqmj3") (y #t)))

(define-public crate-gdnative-impl-proc-macros-0.9.3 (c (n "gdnative-impl-proc-macros") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "11mgh5114vk6lgfdbcag3nxv07i0g6kl49xj9zxjx303nkr8690x")))

(define-public crate-gdnative-impl-proc-macros-0.10.0-rc.0 (c (n "gdnative-impl-proc-macros") (v "0.10.0-rc.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0192kb31yih8w09cl42vw2bp6wlc6gh0xidzm57kg9nsv13r5hwc")))

(define-public crate-gdnative-impl-proc-macros-0.10.0 (c (n "gdnative-impl-proc-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0b3yd7zsb5k7davy59akps76pqffkr128p4rq64n806xg1vidzdk") (r "1.56")))

(define-public crate-gdnative-impl-proc-macros-0.10.1 (c (n "gdnative-impl-proc-macros") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "14rgz63yf77qdsrmm65xknpn1wrszdsbrgda2gk94iv312pz1ws0") (r "1.56")))

(define-public crate-gdnative-impl-proc-macros-0.10.2 (c (n "gdnative-impl-proc-macros") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "13gp8j5iygwz46yfz2azgkxyvii5n9c2qchjcj9574d3mx0zgi18") (r "1.56")))

(define-public crate-gdnative-impl-proc-macros-0.11.0 (c (n "gdnative-impl-proc-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1plhij4bgry2j6ckx0qirrf7hic523z1vl026zl1fqlk6a633nnr") (r "1.63")))

(define-public crate-gdnative-impl-proc-macros-0.11.1 (c (n "gdnative-impl-proc-macros") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0hbk0rjrrsi0cfzp4lrh1cgs3bmvx93gcfw7dz70a4q1a3bm2hbs") (r "1.63")))

(define-public crate-gdnative-impl-proc-macros-0.11.2 (c (n "gdnative-impl-proc-macros") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1lrzfqiak1i7pgj4pix356gmzl4sl6c7arffgw5wd2r5ryf3m555") (r "1.63")))

(define-public crate-gdnative-impl-proc-macros-0.11.3 (c (n "gdnative-impl-proc-macros") (v "0.11.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "13ah8js39rqamxa97h5fr6yjiw3k7kfcp3di3ng3vgpbcfj6w64z") (r "1.63")))

