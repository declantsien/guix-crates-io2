(define-module (crates-io gd na gdnative-doc-cli) #:use-module (crates-io))

(define-public crate-gdnative-doc-cli-0.0.1 (c (n "gdnative-doc-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "gdnative-doc") (r "^0.0.1") (d #t) (k 0)))) (h "0lrwzjdqv7i1axaj3vcf6c15qpxlyfzqvsv70lx6l4snrm4l082i")))

(define-public crate-gdnative-doc-cli-0.0.2 (c (n "gdnative-doc-cli") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "gdnative-doc") (r "^0.0.2") (d #t) (k 0)))) (h "0d1r17yv263bckljqpbwzr58jnsalv53rl680w98k0ci8f6vxvam")))

(define-public crate-gdnative-doc-cli-0.0.3 (c (n "gdnative-doc-cli") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "gdnative-doc") (r "^0.0.3") (d #t) (k 0)))) (h "1zhv1y5jymqazivy8izfa4yi3lkl233pd8zf110pw892dnhh8r47")))

(define-public crate-gdnative-doc-cli-0.0.4 (c (n "gdnative-doc-cli") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "gdnative-doc") (r "^0.0.4") (d #t) (k 0)))) (h "1gl2bpg1zmiwsswas4hidyf2madpryh6a79bckqny0r4gr56s8sr")))

(define-public crate-gdnative-doc-cli-0.0.5 (c (n "gdnative-doc-cli") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "gdnative-doc") (r "^0.0.5") (d #t) (k 0)))) (h "1qgji8rgmyqjy3i2ppk604wrmi59nsd972qwgr7sp3pax6cbjl8z")))

(define-public crate-gdnative-doc-cli-0.0.6 (c (n "gdnative-doc-cli") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.11") (d #t) (k 0)) (d (n "gdnative-doc") (r "^0.0.6") (d #t) (k 0)))) (h "0ra1m2n6snp57pq4mmf2v4f3y0s6ym812yhmpvb0vnx5vg8q5g3y")))

