(define-module (crates-io gd na gdnative-bindings-lily) #:use-module (crates-io))

(define-public crate-gdnative-bindings-lily-0.9.3 (c (n "gdnative-bindings-lily") (v "0.9.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "gdnative-core") (r "=0.9.3") (d #t) (k 0)) (d (n "gdnative-sys") (r "^0.9.3") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "=0.9.3") (d #t) (k 1)) (d (n "heck") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ki7s4z3hhhp6jjrjasl4gc3ai0wk8g7bzliqr8v18x6ap4j8svy") (f (quote (("one_class_one_file") ("formatted"))))))

