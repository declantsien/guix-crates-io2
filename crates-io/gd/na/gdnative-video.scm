(define-module (crates-io gd na gdnative-video) #:use-module (crates-io))

(define-public crate-gdnative-video-0.5.0 (c (n "gdnative-video") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "0csvyl547almj593xdpf08m0d6grljripapb61kp7l56pgs3mw6y") (f (quote (("gd_test"))))))

(define-public crate-gdnative-video-0.6.0 (c (n "gdnative-video") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "03dax6qjhvsaaqnc860a7xyf1dv5ilsgjf2hw3mkh1xnm0v1s71y") (f (quote (("gd_test"))))))

(define-public crate-gdnative-video-0.6.1 (c (n "gdnative-video") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "13gjgm69mkl5cbfs9sa74bljj5nh361bfpp9rap9n24x5wclmw6n") (f (quote (("gd_test"))))))

