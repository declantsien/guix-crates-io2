(define-module (crates-io gd na gdnative-arvr) #:use-module (crates-io))

(define-public crate-gdnative-arvr-0.5.0 (c (n "gdnative-arvr") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "04j176hpm425dd0xaxxpravg2g6jdds2kqi8al3zlzi4vffh5327") (f (quote (("gd_test"))))))

(define-public crate-gdnative-arvr-0.6.0 (c (n "gdnative-arvr") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0f9x14mm0nlc0l4j3s7aaf2h8j4f8p1m32qjb6axmp2l603dl925") (f (quote (("gd_test"))))))

(define-public crate-gdnative-arvr-0.6.1 (c (n "gdnative-arvr") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "1w2f8vl4snj6vzciv2lsb9cy4hn8dr41ka8h88dy18vx5ad9wkx3") (f (quote (("gd_test"))))))

