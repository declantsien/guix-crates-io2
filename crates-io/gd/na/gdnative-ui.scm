(define-module (crates-io gd na gdnative-ui) #:use-module (crates-io))

(define-public crate-gdnative-ui-0.5.0 (c (n "gdnative-ui") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative-video") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "0w6fbzcp20hihliab4q1yq93chdqy277vqfsrrd2z3k7k6diyrz1") (f (quote (("gd_test"))))))

(define-public crate-gdnative-ui-0.6.0 (c (n "gdnative-ui") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative-video") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "1ccfz5yz1b54hniaamjx826lmnawzfmq7564fp5vi52q09cnjypg") (f (quote (("gd_test"))))))

(define-public crate-gdnative-ui-0.6.1 (c (n "gdnative-ui") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative-video") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0jyb5zkdsib1f2jxcmx8slzyhwwirpn87xaw9z8klc44qa6xafi2") (f (quote (("gd_test"))))))

