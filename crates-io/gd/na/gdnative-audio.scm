(define-module (crates-io gd na gdnative-audio) #:use-module (crates-io))

(define-public crate-gdnative-audio-0.5.0 (c (n "gdnative-audio") (v "0.5.0") (d (list (d (n "gdnative-common") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.2.0") (d #t) (k 1)))) (h "02zdb7k0cp8q27wmbxpxg5l5r846khk6iyy5fsnc6khzcqml3hsr") (f (quote (("gd_test"))))))

(define-public crate-gdnative-audio-0.6.0 (c (n "gdnative-audio") (v "0.6.0") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "0p6afzwnszrd4y7wxxnzwz9bnv6ydjk70kfnzgrddl6fsfxhng0a") (f (quote (("gd_test"))))))

(define-public crate-gdnative-audio-0.6.1 (c (n "gdnative-audio") (v "0.6.1") (d (list (d (n "gdnative-common") (r "^0.6.0") (d #t) (k 0)) (d (n "gdnative_bindings_generator") (r "^0.3.0") (d #t) (k 1)))) (h "1xd5aw9lrbh15wr68yi2zzmb3yzlbr64jhkssbyh6vwb1c8zbpya") (f (quote (("gd_test"))))))

