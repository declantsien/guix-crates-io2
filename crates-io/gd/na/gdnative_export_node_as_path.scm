(define-module (crates-io gd na gdnative_export_node_as_path) #:use-module (crates-io))

(define-public crate-gdnative_export_node_as_path-0.1.2 (c (n "gdnative_export_node_as_path") (v "0.1.2") (d (list (d (n "gdnative") (r "^0.11.3") (d #t) (k 2)) (d (n "heck") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "18dcpydlpzc09jv2lymdfg3lq9d3g67c10p13xilyjpk4hdgddh0")))

