(define-module (crates-io gd na gdnative_geom) #:use-module (crates-io))

(define-public crate-gdnative_geom-0.2.0 (c (n "gdnative_geom") (v "0.2.0") (d (list (d (n "euclid") (r "^0.17.0") (d #t) (k 0)))) (h "1gl0y77yliywqnsjxg4y1gqqzgbb66ksizmxz1wxvv4pf26xkql9")))

(define-public crate-gdnative_geom-0.3.0 (c (n "gdnative_geom") (v "0.3.0") (d (list (d (n "euclid") (r "^0.17.0") (d #t) (k 0)))) (h "1ivydyswh1mibvykmz87xfc8xh8m6dhr6zlh4a2b3vwpj199cgn1")))

(define-public crate-gdnative_geom-0.5.0 (c (n "gdnative_geom") (v "0.5.0") (d (list (d (n "euclid") (r "^0.19.0") (d #t) (k 0)))) (h "0qmlmli318myjjdnzxwp91jf2w27vx9vzsdqgnyhsyxp3gxzqjd7")))

(define-public crate-gdnative_geom-0.6.0 (c (n "gdnative_geom") (v "0.6.0") (d (list (d (n "euclid") (r "^0.19.0") (d #t) (k 0)))) (h "0q931f6gi54xck4hqb3xmlz2gfrwk7za8q7rpfz32mjfgl9g8by6")))

(define-public crate-gdnative_geom-0.6.1 (c (n "gdnative_geom") (v "0.6.1") (d (list (d (n "euclid") (r "^0.19.0") (d #t) (k 0)))) (h "1qhk6p28njiskr82cw1d2fmd46xvwpdxah08jq4qhbgxyi4hyrs9")))

