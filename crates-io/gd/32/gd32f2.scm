(define-module (crates-io gd #{32}# gd32f2) #:use-module (crates-io))

(define-public crate-gd32f2-0.6.0 (c (n "gd32f2") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0xacdhswqymwvnjqpnr0bcncxsnjxizfm1ck5zcn86kxdb0880wj") (f (quote (("rt" "cortex-m-rt/device") ("gd32f207") ("gd32f205") ("default"))))))

(define-public crate-gd32f2-0.7.0 (c (n "gd32f2") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ma7kb0gmbg16sdbw3l0ms5xg0g015z5jgla13865b7b8zwz6a7r") (f (quote (("rt" "cortex-m-rt/device") ("gd32f207") ("gd32f205") ("default"))))))

(define-public crate-gd32f2-0.8.0 (c (n "gd32f2") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0jczj648a65qy70bqfi13348xj0dx7ys2222ikz9f6308px229pk") (f (quote (("rt" "cortex-m-rt/device") ("gd32f207") ("gd32f205") ("default"))))))

(define-public crate-gd32f2-0.9.0 (c (n "gd32f2") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "17avsfs04652qljp4l9ib1z7s8q7dv1i8rijq31805km8n9kgk58") (f (quote (("rt" "cortex-m-rt/device") ("gd32f207") ("gd32f205") ("default"))))))

(define-public crate-gd32f2-0.9.1 (c (n "gd32f2") (v "0.9.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1yy12djwql4pqg5spi91q0965kb2947y2qv58nm886rs5l4gyz4d") (f (quote (("rt" "cortex-m-rt/device") ("gd32f207") ("gd32f205") ("default"))))))

