(define-module (crates-io gd #{32}# gd32f3x0-pac) #:use-module (crates-io))

(define-public crate-gd32f3x0-pac-0.1.0 (c (n "gd32f3x0-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0dd1p2aqlyxf0mblishfp496agb6sms93ic8whsvcci2xyy1bm23") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

