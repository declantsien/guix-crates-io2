(define-module (crates-io gd #{32}# gd32vf103-pac) #:use-module (crates-io))

(define-public crate-gd32vf103-pac-0.1.0 (c (n "gd32vf103-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1vbzn5vnb2jlliia3p1j1m9ibp6lk3kpxn3dk6vv1vgsdvx22nxr") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-gd32vf103-pac-0.2.0 (c (n "gd32vf103-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1bg8qf67w27cma7bqvnpp4cddxqs0m98xrbrzx3z4rx6krpvbli3") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-gd32vf103-pac-0.3.0 (c (n "gd32vf103-pac") (v "0.3.0") (d (list (d (n "bare-metal") (r "= 0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.5.6") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1s2rwvd1g2svbk5hlxsja2gf6xxy4kxkf9kbr6sd1qhqxnqg7bgc") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-gd32vf103-pac-0.3.1 (c (n "gd32vf103-pac") (v "0.3.1") (d (list (d (n "bare-metal") (r "=0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.5.6") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "051dqfvcnaisj0sbn06yiqgy3bx0pd6fnk0ml9pgglg74bk9k2kv") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-gd32vf103-pac-0.4.0 (c (n "gd32vf103-pac") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "10qfnqg9h74chgf6g3r0przar9vrpv55h6r4q38xp7m46w3l7zaz") (f (quote (("rt"))))))

(define-public crate-gd32vf103-pac-0.5.0 (c (n "gd32vf103-pac") (v "0.5.0") (d (list (d (n "critical_section") (r "^1.1.1") (o #t) (d #t) (k 0) (p "critical-section")) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1757a58yl40a5waxhr1nfhnnfpx428h650vnv44341vy7km1w2hr") (f (quote (("rt") ("critical-section" "critical_section" "riscv/critical-section-single-hart"))))))

