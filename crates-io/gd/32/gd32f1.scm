(define-module (crates-io gd #{32}# gd32f1) #:use-module (crates-io))

(define-public crate-gd32f1-0.1.0 (c (n "gd32f1") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "02kmvrrl7v28z23zz7cwzlw5xp2q6liw3vwn28x2r7lpmqrs7sdy") (f (quote (("rt" "cortex-m-rt/device") ("gd32f170") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.2.0 (c (n "gd32f1") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1673bfxm4hlg232f79dlxfksz1jfs4nk20vc8nn23bc74lax1p6d") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.2.1 (c (n "gd32f1") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "04rpmib7hrwl2pja2112kck4021qd4m5sdzz7n2qxgxb7q4yqsvg") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.3.0 (c (n "gd32f1") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1zhp86kg4bz6vfaw11z35zakr4ph64aq7nwfr23mx1sga7rim5vc") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.4.0 (c (n "gd32f1") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1m5fv5cppv8by9vxw5092a38xdx7p2pq3x17gbisih6h96q3qbcy") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.5.0 (c (n "gd32f1") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "19rnw7lmc6h618l6xybdklaag47gnxwm9gy34an6q1spyrwfv4j0") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.6.0 (c (n "gd32f1") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0im9k5kpirg8bk8bzp38srj333ifsawixhwj4zbjjcbvdbclwj3z") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.7.0 (c (n "gd32f1") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0r9q3dlsh114pq0byl886hd8z7j3hy4i61d4c7f1r6r4vbnrbbby") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.8.0 (c (n "gd32f1") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1j3wb5jlx3qlniv0r120ji8v8shnr95mm0kf055frvbjdbyr4i6m") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.9.0 (c (n "gd32f1") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0r0rvb85j7ljwr0adarzcj29kqd67dan1sikq4x7dpkk3ym8w3wr") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

(define-public crate-gd32f1-0.9.1 (c (n "gd32f1") (v "0.9.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0krfd9k9gcllsihkxjd0lplc6x3am7iwj8q209cjwjmwjpgjidrf") (f (quote (("rt" "cortex-m-rt/device") ("gd32f190") ("gd32f170") ("gd32f150") ("gd32f130") ("default"))))))

