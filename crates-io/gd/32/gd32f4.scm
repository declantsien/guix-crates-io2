(define-module (crates-io gd #{32}# gd32f4) #:use-module (crates-io))

(define-public crate-gd32f4-0.1.0-alpha.1 (c (n "gd32f4") (v "0.1.0-alpha.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "19gd49xp67zzffxwflmp2vv6cfha2wkjdsznginki9538qzrz3mr") (f (quote (("rt" "cortex-m-rt/device") ("gd32f403") ("default" "rt"))))))

