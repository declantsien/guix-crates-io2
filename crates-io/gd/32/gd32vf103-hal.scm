(define-module (crates-io gd #{32}# gd32vf103-hal) #:use-module (crates-io))

(define-public crate-gd32vf103-hal-0.0.0 (c (n "gd32vf103-hal") (v "0.0.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gd32vf103-pac") (r "^0.1") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0n5c11ybqv5i4ijjr2vdk39k88lv2djl2gzx2gira5vjrrc4mk53")))

(define-public crate-gd32vf103-hal-0.0.1 (c (n "gd32vf103-hal") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gd32vf103-pac") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.7") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)) (d (n "riscv") (r "^0.5") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1.0") (k 0)))) (h "0p40z56l2dc6bgsf580qki5cavkzrgl71801lkw17kiyp783731c")))

(define-public crate-gd32vf103-hal-0.0.2 (c (n "gd32vf103-hal") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gd32vf103-pac") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.7") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)) (d (n "riscv") (r "^0.5") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1.0") (k 0)))) (h "1ypn720syrbmrn1dq5jwcc1myzn1kppf5h6yf1pmbv1qn3jz2yn6")))

(define-public crate-gd32vf103-hal-0.0.3 (c (n "gd32vf103-hal") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gd32vf103-pac") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.7") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)) (d (n "riscv") (r "^0.5") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1.0") (k 0)))) (h "0k2vbh8gjyzqaz3v6lzlpfrcgpmmdjfzmxha94s2rq5fbykj2ii2")))

(define-public crate-gd32vf103-hal-0.0.4 (c (n "gd32vf103-hal") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gd32vf103-pac") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5") (d #t) (k 0)) (d (n "void") (r "^1.0") (k 0)))) (h "1wrrhfxa50jh207mc1jqp3fqcmyq6v28zg1nwqnl09blg6p5c5sk") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-gd32vf103-hal-0.0.5 (c (n "gd32vf103-hal") (v "0.0.5") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "gd32vf103-pac") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5") (d #t) (k 0)))) (h "1hkzrwx8fbd2bvnca6h0lf7pdb0329lzz6mpagh5gis5cvv7121j") (f (quote (("inline-asm" "riscv/inline-asm"))))))

(define-public crate-gd32vf103-hal-0.0.6 (c (n "gd32vf103-hal") (v "0.0.6") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "gd32vf103-pac") (r "^0.4") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)))) (h "0qpr5csg078kzhfhvw0r4ax7cq9ny0grkni6bchavfs0l3099fdf") (f (quote (("inline-asm" "riscv/inline-asm"))))))

