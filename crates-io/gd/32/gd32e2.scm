(define-module (crates-io gd #{32}# gd32e2) #:use-module (crates-io))

(define-public crate-gd32e2-0.5.0 (c (n "gd32e2") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0qfgi3zzm6wrwlrplr8v29bsrdxq6d5d0q67y2pshi0qbk5jd2fm") (f (quote (("rt" "cortex-m-rt/device") ("gd32e231") ("gd32e230") ("default"))))))

(define-public crate-gd32e2-0.6.0 (c (n "gd32e2") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0681l3j3wib9afiawfjvjlvy5ix6y5gk13q0q5zcriw4wk1gdq8s") (f (quote (("rt" "cortex-m-rt/device") ("gd32e231") ("gd32e230") ("default"))))))

(define-public crate-gd32e2-0.7.0 (c (n "gd32e2") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0pmp04ivnh2f0sma2wj1d2w6jbii2jyjwkc7x3kgd1abrm65dzaj") (f (quote (("rt" "cortex-m-rt/device") ("gd32e231") ("gd32e230") ("default"))))))

(define-public crate-gd32e2-0.8.0 (c (n "gd32e2") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xkvk3npf40pjh1fzvkvp3s6l7ibaa678969p6g9v4xsdybdq52c") (f (quote (("rt" "cortex-m-rt/device") ("gd32e231") ("gd32e230") ("default"))))))

(define-public crate-gd32e2-0.9.0 (c (n "gd32e2") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0643vlrdi89f5q27k4dfd0ms8bqggkwqhf0bry7xzfbhbz89j3rq") (f (quote (("rt" "cortex-m-rt/device") ("gd32e231") ("gd32e230") ("default"))))))

(define-public crate-gd32e2-0.9.1 (c (n "gd32e2") (v "0.9.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0fbaiqgrdqrzhfmxnwz7zanizj9p1q0xpdwwy2hyd3pjfxm8mwmj") (f (quote (("rt" "cortex-m-rt/device") ("gd32e231") ("gd32e230") ("default"))))))

