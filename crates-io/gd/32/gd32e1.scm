(define-module (crates-io gd #{32}# gd32e1) #:use-module (crates-io))

(define-public crate-gd32e1-0.7.0 (c (n "gd32e1") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1i74wy32gmxcahrpsfcp4azsvxc7yg5hcpcxrhbq0hg6pac16q74") (f (quote (("rt" "cortex-m-rt/device") ("gd32e103") ("default"))))))

(define-public crate-gd32e1-0.8.0 (c (n "gd32e1") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1wwrnbh4kcc4m3578jmz66s66wwxanx2vwr77fhnaphzk09gj7ax") (f (quote (("rt" "cortex-m-rt/device") ("gd32e103") ("default"))))))

(define-public crate-gd32e1-0.9.0 (c (n "gd32e1") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "14da0vw26cp297g0jcnr1637pky99jq65cg1v97zxm70fq33nrsz") (f (quote (("rt" "cortex-m-rt/device") ("gd32e103") ("default"))))))

(define-public crate-gd32e1-0.9.1 (c (n "gd32e1") (v "0.9.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10n1wpnwdgv4dp75adn6dayhdp40jaczbzdvw4kzcvnh5qpg6kyk") (f (quote (("rt" "cortex-m-rt/device") ("gd32e103") ("default"))))))

