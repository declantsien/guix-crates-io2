(define-module (crates-io gd #{32}# gd32e5) #:use-module (crates-io))

(define-public crate-gd32e5-0.7.0 (c (n "gd32e5") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xmly4vg8j2vzbw87i8r1brwwmhbckkgndqxfrny20n6q5j51979") (f (quote (("rt" "cortex-m-rt/device") ("gd32e508") ("gd32e507") ("gd32e505") ("gd32e503") ("default"))))))

(define-public crate-gd32e5-0.8.0 (c (n "gd32e5") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1x8zci9mb6ah4m95vvc48crx953kbnii4dqs46fyf8blwqfy4vnw") (f (quote (("rt" "cortex-m-rt/device") ("gd32e508") ("gd32e507") ("gd32e505") ("gd32e503") ("default"))))))

(define-public crate-gd32e5-0.9.0 (c (n "gd32e5") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1567g4dxfs40a51nks66vwvv24ra4sw6ip3xdvl6snib3dhpj167") (f (quote (("rt" "cortex-m-rt/device") ("gd32e508") ("gd32e507") ("gd32e505") ("gd32e503") ("default"))))))

(define-public crate-gd32e5-0.9.1 (c (n "gd32e5") (v "0.9.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0abs8hxqv55xnifkfq26krm96grnkkgbq0p8gp1dz3ykj08h5jyx") (f (quote (("rt" "cortex-m-rt/device") ("gd32e508") ("gd32e507") ("gd32e505") ("gd32e503") ("default"))))))

