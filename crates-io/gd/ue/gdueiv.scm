(define-module (crates-io gd ue gdueiv) #:use-module (crates-io))

(define-public crate-gdueiv-0.1.0 (c (n "gdueiv") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "windows") (r ">=0.52.0") (f (quote ("Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "112nf4y15v9vs04jk64hh10rl19wpjrdxf2kpmis9vlq8jp6vd5x") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gdueiv-0.1.1 (c (n "gdueiv") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "windows") (r ">=0.52.0") (f (quote ("Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12v352hjbz80fia2jlnx8q576qlkkykrj3m5ry9b7vwms37jskfm") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gdueiv-0.1.2 (c (n "gdueiv") (v "0.1.2") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "windows") (r ">=0.52.0") (f (quote ("Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "142a2yssjv1scby7rhy65s5yr5j0wh02rj1dadahdljglfl3sc7j") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gdueiv-0.1.3 (c (n "gdueiv") (v "0.1.3") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "windows") (r ">=0.52.0") (f (quote ("Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nwc8rbc14nhlqf7pv0k0yhlqbxxcxysbsqx5gn5mjy8cwclzgjf") (s 2) (e (quote (("serde" "dep:serde"))))))

