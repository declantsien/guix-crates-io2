(define-module (crates-io gd k- gdk-pixbuf-hvif) #:use-module (crates-io))

(define-public crate-gdk-pixbuf-hvif-0.1.0 (c (n "gdk-pixbuf-hvif") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.14") (k 0)) (d (n "hvif") (r "^0.1") (f (quote ("cairo-rs"))) (d #t) (k 0)))) (h "1b3345ap5ppdlhdlklvi9nijj6ikkjbwjdk38m1ca7vdg7jghysj")))

(define-public crate-gdk-pixbuf-hvif-0.1.1 (c (n "gdk-pixbuf-hvif") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.14") (k 0)) (d (n "hvif") (r "^0.1") (f (quote ("cairo-rs"))) (d #t) (k 0)))) (h "1ay3gb04sn55m4b3bv9rh7a8hi1a2kbf8bac4dmzkdqvfc5whpqw")))

