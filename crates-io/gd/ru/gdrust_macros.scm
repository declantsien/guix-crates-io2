(define-module (crates-io gd ru gdrust_macros) #:use-module (crates-io))

(define-public crate-gdrust_macros-0.1.0 (c (n "gdrust_macros") (v "0.1.0") (d (list (d (n "gdnative") (r "^0.9") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cs2p5i91wf0fkq2d2bi14fq7z2pk7wk06hyw0mdvf64jmn7nn9z")))

