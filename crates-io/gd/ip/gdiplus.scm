(define-module (crates-io gd ip gdiplus) #:use-module (crates-io))

(define-public crate-gdiplus-0.0.1 (c (n "gdiplus") (v "0.0.1") (h "14pfi567lkwlakkrprqj0dzv3ismzd7032nhmzs2jwbdcv67gqp3")))

(define-public crate-gdiplus-0.0.2 (c (n "gdiplus") (v "0.0.2") (d (list (d (n "gdiplus-sys2") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("impl-default" "libloaderapi" "winerror" "winuser"))) (d #t) (k 2)))) (h "0da9vq61cl1a0g1hg6q1xhklc2v97m2g6mxw352aid3hnzzfz6kb")))

