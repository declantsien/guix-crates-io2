(define-module (crates-io gd lo gdlogue) #:use-module (crates-io))

(define-public crate-gdlogue-0.1.0 (c (n "gdlogue") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "1vp064dc90ycx0jprc1r2asvckagy58i9i78g2lw83h8pzsjy01z")))

