(define-module (crates-io gd p_ gdp_rs) #:use-module (crates-io))

(define-public crate-gdp_rs-0.0.1 (c (n "gdp_rs") (v "0.0.1") (d (list (d (n "frunk_core") (r "^0.4.1") (d #t) (k 0)))) (h "0qx8w4mllc4gmjykinyha8qismq5k3ywfrbkqp7ra9wbk7d1qzc6")))

(define-public crate-gdp_rs-0.0.2 (c (n "gdp_rs") (v "0.0.2") (d (list (d (n "frunk_core") (r "^0.4.1") (d #t) (k 0)))) (h "065yf743w8ivaps8dpq1fws2pr99912rkyvqmwx113xr373q31fw")))

(define-public crate-gdp_rs-0.0.3 (c (n "gdp_rs") (v "0.0.3") (d (list (d (n "frunk_core") (r "^0.4.1") (d #t) (k 0)))) (h "1djxrlfwxn8li26m4489axjpwy12nah75i4j33652nz078jdblvv")))

(define-public crate-gdp_rs-0.1.0 (c (n "gdp_rs") (v "0.1.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "frunk_core") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1l58l0ph4qs70wms1dfr87vpiqis8jvgpk4sbxbkvsvnsqvr5zy8") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gdp_rs-0.1.1 (c (n "gdp_rs") (v "0.1.1") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "frunk_core") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "093fd484i8izpp0755xccmjf8v6bnm74md1nf83pw40mw1jkfsqs") (s 2) (e (quote (("serde" "dep:serde"))))))

