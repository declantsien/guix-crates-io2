(define-module (crates-io gd -r gd-rs) #:use-module (crates-io))

(define-public crate-gd-rs-0.1.0 (c (n "gd-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "plist") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)))) (h "04s5ywb2f5303bahb0389qc8rz7vq3cps5m6z9788namzbsmr0c9")))

(define-public crate-gd-rs-0.1.1 (c (n "gd-rs") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "plist") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)))) (h "11haalxc7srbc2dj90b9z6ln8wj6pi66408ym9962ysldyz9ir2s")))

