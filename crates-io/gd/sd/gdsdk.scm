(define-module (crates-io gd sd gdsdk) #:use-module (crates-io))

(define-public crate-gdsdk-0.1.0 (c (n "gdsdk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "1dambqf0x001ii8876bbjb9mw7sz08ip1w8zm0a6hrxyxg87zpnx")))

