(define-module (crates-io gd db gddb) #:use-module (crates-io))

(define-public crate-gddb-0.1.0 (c (n "gddb") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "gdnative") (r "^0.9") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0p96vjp5004pb25ghzpkwszak72d44mwvhnj4xgp9z0y7yyk42bk")))

(define-public crate-gddb-0.1.1 (c (n "gddb") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "gdnative") (r "^0.9") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "09vwfyw936cn28fpcls4mnmksh1dlpn67wyp1qxhswk14i5x7fpg")))

(define-public crate-gddb-0.2.0 (c (n "gddb") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z1hywwkc9j94d5425fsnwlf65zi1kfdnzifmj2pqyyzffc006bi")))

(define-public crate-gddb-0.3.0 (c (n "gddb") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "07pp2v71imqmmnmvlpzj6ih9r18c69cfp5vdk9l1jf30p1q0l6c5")))

(define-public crate-gddb-0.3.1 (c (n "gddb") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "gdnative") (r "^0.10.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "05fizqscpr6cim1k799vhz662gjs575rkp7fi9kc8m9dblhbcw4d")))

