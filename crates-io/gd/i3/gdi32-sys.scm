(define-module (crates-io gd i3 gdi32-sys) #:use-module (crates-io))

(define-public crate-gdi32-sys-0.0.1 (c (n "gdi32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "19ynzshvl1bl1n3j63ni2i38ri8asnvbk674bfv02ndpd5q29r4l")))

(define-public crate-gdi32-sys-0.0.2 (c (n "gdi32-sys") (v "0.0.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1j3vfgyn53mxswdzqgrxr40cgmdn44in84k8y7ailcw0m01dxssl")))

(define-public crate-gdi32-sys-0.0.3 (c (n "gdi32-sys") (v "0.0.3") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0mb1w09l7jsn498qvda7w1mdl2zrdrg7kf6hjl5vxfarcxi8rnr5")))

(define-public crate-gdi32-sys-0.0.4 (c (n "gdi32-sys") (v "0.0.4") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0a2z8ljz8xbai81h9k32akzpwxw3ynyk5b9skjwmswwgqkfx0p35")))

(define-public crate-gdi32-sys-0.1.0 (c (n "gdi32-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0ab50lp8g9a93qh328kns9xb5idmm8z639nvdwjhwpbc7ncg4ka6")))

(define-public crate-gdi32-sys-0.1.1 (c (n "gdi32-sys") (v "0.1.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "16nm7xd5mmc7aqgzy78bm656kq54asws677wbgqfd4i5vk26w9b5")))

(define-public crate-gdi32-sys-0.2.0 (c (n "gdi32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0605d4ngjsspghwjv4jicajich1gnl0aik9f880ajjzjixd524h9")))

(define-public crate-gdi32-sys-0.1.2 (c (n "gdi32-sys") (v "0.1.2") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "04bcwaia0q46k4rajwpivdvlfyc2gw5vnvkbz247hlh724nbjglf")))

