(define-module (crates-io gd al gdal-sys) #:use-module (crates-io))

(define-public crate-gdal-sys-0.1.0 (c (n "gdal-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "181mr72p86ws25npmq3hjf9ax97rhxca4q0rq44ns028dqf4glpy")))

(define-public crate-gdal-sys-0.2.0 (c (n "gdal-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0y893yjyzf80l3d0vdrvyy06w56bv0lwisjs0prd7kvkfdv88hzi") (f (quote (("min_gdal_version_2_2") ("min_gdal_version_2_1") ("min_gdal_version_2_0") ("min_gdal_version_1_11") ("default" "min_gdal_version_1_11"))))))

(define-public crate-gdal-sys-0.3.0 (c (n "gdal-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.56") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^0.11") (d #t) (k 1)))) (h "0v5sgirrgfvqa1gkvrrrsyaibh59sncwyz32wh0w5r15583c606n")))

(define-public crate-gdal-sys-0.3.1 (c (n "gdal-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.56") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^0.11") (d #t) (k 1)))) (h "1abczkgkl8jf6dix2jl52j9ifwsclf24dm30vbs8dnajcdxg68kv")))

(define-public crate-gdal-sys-0.4.0 (c (n "gdal-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^0.11") (d #t) (k 1)))) (h "07s94jypamq694yynqgmngrwhi0sscxl33dr3q0rb8q96bjj18rh")))

(define-public crate-gdal-sys-0.5.0 (c (n "gdal-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "18acc3yx33v86l7ql6a6xvdiacn2axzwvxwg72xxhmhrb92vwg0s")))

(define-public crate-gdal-sys-0.6.0 (c (n "gdal-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "03szaxhd3wh9k3r7yxa33cvpksyfxml58y324hzw0ck0zlzascc3")))

(define-public crate-gdal-sys-0.7.0 (c (n "gdal-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "0v8ccq8izl9bakinp29q2sm2skdgib9njxlgw4g15yyama0rjicm")))

(define-public crate-gdal-sys-0.8.0 (c (n "gdal-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.61") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "1b9dmzqasf90z5mz8kjvfhcxp091b57i7vqhq7gm1jd9hj58nmyw")))

(define-public crate-gdal-sys-0.9.0 (c (n "gdal-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.65") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "1z9afh1v4b906yb26hwkdc8z3kgh7bf9jk666g6n3aryazk4ps5i") (l "gdal") (r "1.58")))

(define-public crate-gdal-sys-0.9.1 (c (n "gdal-sys") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.66") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "0f1z7srqrmg1pg0v0pdykhlm7w08mswgnbz00xdzn86ns34scwxb") (l "gdal") (r "1.58")))

