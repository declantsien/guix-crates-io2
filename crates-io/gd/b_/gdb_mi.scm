(define-module (crates-io gd b_ gdb_mi) #:use-module (crates-io))

(define-public crate-gdb_mi-0.1.0 (c (n "gdb_mi") (v "0.1.0") (d (list (d (n "env_logger") (r "0.3.*") (d #t) (k 2)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "regex") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (d #t) (k 0)))) (h "0rs8xnilxi4kzx43g6knkc6k29cijwkjb5567fvrx0qx1b6riww5")))

(define-public crate-gdb_mi-0.1.1 (c (n "gdb_mi") (v "0.1.1") (d (list (d (n "env_logger") (r "0.4.*") (d #t) (k 2)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "regex") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (d #t) (k 0)))) (h "1z3674x7g6bq1hl1xa98pnfsspphwgcaacqsl6bwg463zxd1zpb8")))

(define-public crate-gdb_mi-0.1.2 (c (n "gdb_mi") (v "0.1.2") (d (list (d (n "env_logger") (r "0.4.*") (d #t) (k 2)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "regex") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (d #t) (k 0)))) (h "0rx60ymc8l207mf1d4wbd5nb2wsddy5gnf5zysbq38vbqx0lx9sc")))

(define-public crate-gdb_mi-0.1.3 (c (n "gdb_mi") (v "0.1.3") (d (list (d (n "env_logger") (r "0.4.*") (d #t) (k 2)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "regex") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (d #t) (k 0)))) (h "0l5adihbvb0zhscb5847zcf8b9j4bcn0cb2z520raa9yp9ll08rr")))

