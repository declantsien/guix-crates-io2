(define-module (crates-io gd b_ gdb_probe) #:use-module (crates-io))

(define-public crate-gdb_probe-0.1.0 (c (n "gdb_probe") (v "0.1.0") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0s0pzqncpjgxp44giryhvi8wzci4xvdsm7q7f77sndi7w5l9l73q")))

(define-public crate-gdb_probe-0.1.1 (c (n "gdb_probe") (v "0.1.1") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1krv5zf10c3q7b9gb52ah97pxidhfavjnpcxbhsdfp85zz2ra3bn")))

(define-public crate-gdb_probe-0.1.2 (c (n "gdb_probe") (v "0.1.2") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0cmy02iyhc0hclbz1qlab44220jrflz4rd1w27w283l5did62cg2")))

