(define-module (crates-io gd b_ gdb_breakpoint) #:use-module (crates-io))

(define-public crate-gdb_breakpoint-0.1.0 (c (n "gdb_breakpoint") (v "0.1.0") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0k8rnsqcfgr5f6hgcg9fqdybxm4k3nw5f8bvqkljrxrk595826jn")))

(define-public crate-gdb_breakpoint-0.1.1 (c (n "gdb_breakpoint") (v "0.1.1") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "10pfyrjq6x26ahqggy7316483bq9nivrxq6gjah5pllk8jhm6ga5")))

(define-public crate-gdb_breakpoint-0.1.2 (c (n "gdb_breakpoint") (v "0.1.2") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0vphc9wicbzfg9rv48gcg9by54fk9cgp778ic70yaljxall08crx")))

(define-public crate-gdb_breakpoint-0.1.3 (c (n "gdb_breakpoint") (v "0.1.3") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0d87xi3l5yki3wfdb0f8n7681h3ny7jyfw2wygkzl3xkq0b75lrn")))

(define-public crate-gdb_breakpoint-0.1.4 (c (n "gdb_breakpoint") (v "0.1.4") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0xnv98na4ll014smylz65gg1nbwxz6dx93npkcv54kmscmfksc9f")))

(define-public crate-gdb_breakpoint-0.1.5 (c (n "gdb_breakpoint") (v "0.1.5") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "18qd08dyr51ljpi3h0jc9mr2q5b5sz54kkss67bhxg14wha19nhl")))

(define-public crate-gdb_breakpoint-0.1.6 (c (n "gdb_breakpoint") (v "0.1.6") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "10gaf22ixrfwfv8hdy0gza71cbvc4r8v9jz492f1ciwxw4gshyj7")))

(define-public crate-gdb_breakpoint-0.1.7 (c (n "gdb_breakpoint") (v "0.1.7") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0qmw3zxsi3d0sp1p1s1wlxdj8pl46zz0hwnsyc6q6gaghgr7nnha")))

(define-public crate-gdb_breakpoint-0.1.8 (c (n "gdb_breakpoint") (v "0.1.8") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "0yz1k0n6b1y2ycx27wsc1nmp6zwi9a62llf2hzwlhsxfkhm3cx8h")))

(define-public crate-gdb_breakpoint-0.1.9 (c (n "gdb_breakpoint") (v "0.1.9") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "18a6rrkd35yhnsk9gi7xm3df7m747ky856d8a19nx4ck20xy7vxa")))

(define-public crate-gdb_breakpoint-0.2.0 (c (n "gdb_breakpoint") (v "0.2.0") (d (list (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "018hgn8iwki6q1f06xr91f1j95yxqcbfwi7hvyasi27rc0wwqbba")))

