(define-module (crates-io gd ri gdrive-search) #:use-module (crates-io))

(define-public crate-gdrive-search-0.1.0 (c (n "gdrive-search") (v "0.1.0") (d (list (d (n "async-recursion") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "~4") (d #t) (k 0)) (d (n "google-drive3") (r "~2.0.9") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "skim") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)) (d (n "yup-oauth2") (r "~5") (d #t) (k 0)))) (h "0p7rw0c2cwpgj6sfmmlg5z4qnr9bbpg1pc6x2x2sg9rqpgyjjkk6")))

