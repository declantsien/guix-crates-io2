(define-module (crates-io gd er gderu) #:use-module (crates-io))

(define-public crate-gderu-0.1.0 (c (n "gderu") (v "0.1.0") (h "0n6rk1b9y61jf2355ci5ias4nf0xfrzj2imczl0lq7apn74rw94q")))

(define-public crate-gderu-0.2.0 (c (n "gderu") (v "0.2.0") (h "0173xizkwaj491mhaar7p1n25a5afd95vcv73xrw7xi5a54hhqbp")))

