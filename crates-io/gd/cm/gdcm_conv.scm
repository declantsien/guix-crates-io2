(define-module (crates-io gd cm gdcm_conv) #:use-module (crates-io))

(define-public crate-gdcm_conv-0.1.0 (c (n "gdcm_conv") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qb23cwaxj0pcsfpdcnlniac66czqidg5cr76gh4shd2m4gn3ghq")))

(define-public crate-gdcm_conv-0.1.1 (c (n "gdcm_conv") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "09hw493igmjc8kxvasyllpzqi1kh9bzi9y8q36k9pwr2b1kbcy0i") (y #t)))

(define-public crate-gdcm_conv-0.1.2 (c (n "gdcm_conv") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0n9iyzp0vagkylywb3hmfva1y7dn5dnbl0dk8567j6rnaffqxd01") (y #t)))

(define-public crate-gdcm_conv-0.1.3 (c (n "gdcm_conv") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0ydnrss6r2yk49k1dgfgld0avsciibkdsdh3srbcc2zpg691m7vs") (y #t)))

(define-public crate-gdcm_conv-0.1.4 (c (n "gdcm_conv") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1j35dfkh29kmyvq9yy9sfpny85ng3d8s4lwgklpq0m27q4b8gk15") (y #t)))

(define-public crate-gdcm_conv-0.1.5 (c (n "gdcm_conv") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "06c0mx3jqq12mw33rqc6plaa21rh0x172z80b95f3fwcpfi413aa")))

(define-public crate-gdcm_conv-0.1.6 (c (n "gdcm_conv") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0sxdm251bdnnb36jpllbycbm6bcsjqbb0ing6lpldw6ih9kbqqkv")))

(define-public crate-gdcm_conv-0.1.7 (c (n "gdcm_conv") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "14rxhdqldl15hq5k124sxv9ki987anass1k4vxiryajc82vac2p2")))

