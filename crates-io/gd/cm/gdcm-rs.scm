(define-module (crates-io gd cm gdcm-rs) #:use-module (crates-io))

(define-public crate-gdcm-rs-0.1.0 (c (n "gdcm-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "1g313myc1qa14lpcy96mg68j3m4s39gq2k70278ikxzjw1v94cw5")))

(define-public crate-gdcm-rs-0.2.0 (c (n "gdcm-rs") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0aj6w4hzilj0wrwfjmrpnwglnfwxnwvxl4qwcbqir93lar9xj2hn")))

(define-public crate-gdcm-rs-0.3.0 (c (n "gdcm-rs") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "1fdxq47x0rg681x5cr0q7f0ryz4icm9rhrwg9yps7gaqj1dk07hy")))

(define-public crate-gdcm-rs-0.4.0 (c (n "gdcm-rs") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.2") (d #t) (k 0)))) (h "1npbfqihv5nm0vqm2l0m5xzj1y033l6ynqdlvzr281kpb8vpd987")))

(define-public crate-gdcm-rs-0.5.0 (c (n "gdcm-rs") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.2") (d #t) (k 0)))) (h "1xy9w7b1brj4qv25sarbw14d594prw72ad589r0ycj9dpcbcnnpf") (f (quote (("default" "charls") ("charls"))))))

(define-public crate-gdcm-rs-0.6.0 (c (n "gdcm-rs") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "snafu") (r "^0.8.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "0blfafw67yqgql945nsmbnzmyszvr3jbwprapfm3fj5dfjwik9yy") (f (quote (("default" "charls") ("charls"))))))

