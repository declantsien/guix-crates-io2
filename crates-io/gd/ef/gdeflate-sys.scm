(define-module (crates-io gd ef gdeflate-sys) #:use-module (crates-io))

(define-public crate-gdeflate-sys-0.3.0 (c (n "gdeflate-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)))) (h "1kw202vvd8wc1hm2ad9r17si8kd2l2bkvf10vy6jkgbflbm9m2iq")))

