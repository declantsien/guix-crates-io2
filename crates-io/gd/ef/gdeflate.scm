(define-module (crates-io gd ef gdeflate) #:use-module (crates-io))

(define-public crate-gdeflate-0.1.0 (c (n "gdeflate") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.102") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.102") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1c2hmqkadpx1s8d15n25a4wryfm4gkw6fvb2vyz083k11yk708j7") (y #t)))

(define-public crate-gdeflate-0.1.1 (c (n "gdeflate") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0.102") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.102") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1s4z2x9q4k7h3jn2h2y0fjffwv1r28wk2x7gdvd3wz0zv1k587vm")))

(define-public crate-gdeflate-0.1.3 (c (n "gdeflate") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 1)) (d (n "cxx") (r "^1.0.104") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.104") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1aic61rlvazncp283hjc5rbn8wxha9mpgw0v6ffr15g4gf7q4k80")))

(define-public crate-gdeflate-0.2.0 (c (n "gdeflate") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "cxx") (r "^1.0.119") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.119") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "04wllsa6i5n96xz45bb6fc22rncf5cxg62abwwmzd6jmj5mmm6ab")))

(define-public crate-gdeflate-0.3.0 (c (n "gdeflate") (v "0.3.0") (d (list (d (n "gdeflate-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0p89x835adm8v0k18wv4ynbq2l1933y0fvkmn0b5jh2699lgqm4s")))

(define-public crate-gdeflate-0.3.1 (c (n "gdeflate") (v "0.3.1") (d (list (d (n "gdeflate-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1da8x7nbfryydd1z0di0jamskqp0axmabz9slsyk515qj63j3m7z") (f (quote (("default"))))))

(define-public crate-gdeflate-0.4.0 (c (n "gdeflate") (v "0.4.0") (d (list (d (n "gdeflate-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0jjmjimjlpy7q3jpgs79jv55q74al5ky0brj9h6ybbl4wqgki059") (f (quote (("default"))))))

