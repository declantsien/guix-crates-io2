(define-module (crates-io gd l- gdl-parser) #:use-module (crates-io))

(define-public crate-gdl-parser-0.0.1 (c (n "gdl-parser") (v "0.0.1") (d (list (d (n "peg") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "04v1k3h0w8vq89wy0cpr3004r3hzj2pgiz08i2y2p8g96swp99p8")))

(define-public crate-gdl-parser-0.0.2 (c (n "gdl-parser") (v "0.0.2") (d (list (d (n "peg") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1hxzlfswfrs93j6h79jhs3dj71wdchpw9jvdlbdx7yxjabv2lhxd")))

(define-public crate-gdl-parser-0.0.3 (c (n "gdl-parser") (v "0.0.3") (d (list (d (n "peg") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0n9ra52zdgy3p6h6vjb1s65pr7pf57570wrml8rv9y8kcvrhdzry")))

(define-public crate-gdl-parser-0.0.4 (c (n "gdl-parser") (v "0.0.4") (d (list (d (n "peg") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1rbfysnnqibrfp9gp2c393bpj09qwdn1xsjal6g9vpjld8w7xaj5")))

(define-public crate-gdl-parser-0.0.5 (c (n "gdl-parser") (v "0.0.5") (d (list (d (n "peg") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0988rf4mhz77jdqyr42s0xj9qz9dqignph1fcj2b8kgvj10asli3")))

(define-public crate-gdl-parser-0.0.6 (c (n "gdl-parser") (v "0.0.6") (d (list (d (n "peg") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1kgzdqx5h7pk8k888ymyjxkh9c8mq8lgifwiz43nv28a4mny6k2v")))

(define-public crate-gdl-parser-0.0.7 (c (n "gdl-parser") (v "0.0.7") (d (list (d (n "peg") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1qvqgcq260ngp72wxqvvh539cww6al1d84blybzc56kpj05rymak")))

(define-public crate-gdl-parser-0.0.8 (c (n "gdl-parser") (v "0.0.8") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "04qfdc80p43daw9ybnham1s33fsahz5z48rgd82b9gdwz24k9z71")))

