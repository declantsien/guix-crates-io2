(define-module (crates-io bp xe bpxe-internal-macros) #:use-module (crates-io))

(define-public crate-bpxe-internal-macros-0.2.1 (c (n "bpxe-internal-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1hhw8ffywlws7mgpbwz3iis8km47c7k78gkb0nmzrl5ngmdz7hwf")))

