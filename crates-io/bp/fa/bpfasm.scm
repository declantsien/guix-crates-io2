(define-module (crates-io bp fa bpfasm) #:use-module (crates-io))

(define-public crate-bpfasm-1.0.0-beta1 (c (n "bpfasm") (v "1.0.0-beta1") (d (list (d (n "pest") (r "~2") (d #t) (k 0)) (d (n "pest_generator") (r "~2") (d #t) (k 1)) (d (n "quote") (r "~1") (d #t) (k 1)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "0r25ckwhgggq6frlcnzys313mswpvxsrzf6ynfgschp3w9mvdq27")))

(define-public crate-bpfasm-1.0.0 (c (n "bpfasm") (v "1.0.0") (d (list (d (n "pest") (r "~2") (d #t) (k 0)) (d (n "pest_generator") (r "~2") (d #t) (k 1)) (d (n "quote") (r "~1") (d #t) (k 1)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "0igfmw5hlrdlqm0ic72zx054sqxyv4kpnm1ykgp3f5nprafx610c")))

