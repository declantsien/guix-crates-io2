(define-module (crates-io bp sf bpsfuck) #:use-module (crates-io))

(define-public crate-bpsfuck-0.1.0 (c (n "bpsfuck") (v "0.1.0") (d (list (d (n "chumsky") (r "^0.6") (d #t) (k 0)))) (h "0hk5h7v0nvsgqyh59r709ww7knhpl57jm956zwvxa39jf4d7hkhx")))

(define-public crate-bpsfuck-0.1.1 (c (n "bpsfuck") (v "0.1.1") (d (list (d (n "chumsky") (r "^0.6") (d #t) (k 0)))) (h "12yp80qi9gbfaxbcn2n6c59qcgj3r73vg68ka2fn8l9qcnvfwns6")))

(define-public crate-bpsfuck-0.1.2 (c (n "bpsfuck") (v "0.1.2") (d (list (d (n "chumsky") (r "^0.6") (d #t) (k 0)))) (h "0ag1xgw5cw2vknciwsmy04hsij1pm1l0i0l9khlgi53cdik744gb")))

