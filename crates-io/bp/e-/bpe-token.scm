(define-module (crates-io bp e- bpe-token) #:use-module (crates-io))

(define-public crate-bpe-token-0.0.1 (c (n "bpe-token") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a226gr97725ilq7vgkcxhkajdvfprd6lj0bvkvxa78gw4fmmrfp") (f (quote (("test-bpf") ("no-entrypoint"))))))

