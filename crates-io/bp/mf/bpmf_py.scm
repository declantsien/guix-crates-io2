(define-module (crates-io bp mf bpmf_py) #:use-module (crates-io))

(define-public crate-bpmf_py-0.1.0 (c (n "bpmf_py") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "sugars") (r "^3.0.1") (d #t) (k 0)))) (h "11z3br0wqf89r0bnbhn4axqhf5ggyj1344jahbn3s4dsak919bna")))

