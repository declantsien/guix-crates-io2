(define-module (crates-io bp fm bpfman-csi) #:use-module (crates-io))

(define-public crate-bpfman-csi-1.8.0 (c (n "bpfman-csi") (v "1.8.0") (d (list (d (n "prost") (r "^0.12.4") (f (quote ("prost-derive" "std"))) (k 0)) (d (n "prost-types") (r "^0.12.4") (k 0)) (d (n "tonic") (r "^0.11.0") (f (quote ("codegen" "prost"))) (k 0)))) (h "1mr2jdb6a16dc7ld2gsy1df447nx3jh8lsrh4lq6i55xw3q3rq4x")))

