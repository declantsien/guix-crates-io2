(define-module (crates-io bp ht bpht) #:use-module (crates-io))

(define-public crate-bpht-1.0.0 (c (n "bpht") (v "1.0.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xmwm699xknpzw5rw9bynx07n4wsv2hyf5z06rabifshhj14wcwi")))

