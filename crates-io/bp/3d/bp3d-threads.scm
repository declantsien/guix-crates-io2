(define-module (crates-io bp #{3d}# bp3d-threads) #:use-module (crates-io))

(define-public crate-bp3d-threads-1.0.0 (c (n "bp3d-threads") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1iwkxxpw9nkfcmgw6pk67m5zm2rawnzric63ihd757nspmw8fhvn")))

(define-public crate-bp3d-threads-1.1.0 (c (n "bp3d-threads") (v "1.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)))) (h "1dsq4psws8g6a4hh2rgabdj3q93f1bxb97dvwx9fsp0cfy80lgjj")))

(define-public crate-bp3d-threads-2.0.0-pre.1.0.0 (c (n "bp3d-threads") (v "2.0.0-pre.1.0.0") (d (list (d (n "crossbeam") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.8") (o #t) (d #t) (k 0)))) (h "0sn184ip0k7zfnjn5x1kpd0wvqp4x24w2dj3js3z0pi2qv2mdp05") (f (quote (("thread-pool" "crossbeam-deque" "crossbeam-queue") ("crossbeam-scopes" "crossbeam") ("atomic-cell"))))))

