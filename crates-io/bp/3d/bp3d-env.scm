(define-module (crates-io bp #{3d}# bp3d-env) #:use-module (crates-io))

(define-public crate-bp3d-env-1.0.0 (c (n "bp3d-env") (v "1.0.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "0bxijhzxahy039nqhkjyk37g9hd8l3cq8rc4w29h0wsqhxakihdn")))

(define-public crate-bp3d-env-1.0.1 (c (n "bp3d-env") (v "1.0.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "1knnp5ap54ai0wxsqjndqfrm61lpbs0ijkqnvgi4133gj83226w9")))

(define-public crate-bp3d-env-1.0.2 (c (n "bp3d-env") (v "1.0.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "14iwd182zs4bw2dzw7f475icirxjdmjfv77jp969fp8l3d752fli")))

