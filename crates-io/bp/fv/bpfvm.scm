(define-module (crates-io bp fv bpfvm) #:use-module (crates-io))

(define-public crate-bpfvm-0.1.0 (c (n "bpfvm") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.135") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0p2qf346q6rprw93ss89apbc27j7zhl0lk9cv1g943ccx54xslqx")))

(define-public crate-bpfvm-0.1.1 (c (n "bpfvm") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.135") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "03ssrn4wb1bkaz5zlj0qhf3c2vr1zyfbcy4yzm6vly8qnz151hqz") (f (quote (("vm") ("default" "vm"))))))

(define-public crate-bpfvm-0.1.2 (c (n "bpfvm") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.135") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1gwzdl4vwcs15209l76nbpanf6fn0khsajxk8rbnq3zaj9l776b1") (f (quote (("vm") ("default" "vm"))))))

