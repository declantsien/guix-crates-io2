(define-module (crates-io bp x- bpx-api-types) #:use-module (crates-io))

(define-public crate-bpx-api-types-0.1.0 (c (n "bpx-api-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.33.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rqx4lrabrlzp2xn02jbmkr4xam9hvkijqwc1ylligpdsi7gb963")))

(define-public crate-bpx-api-types-0.1.1 (c (n "bpx-api-types") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.33.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bpwh7pkh7nvpsgbzk1rl2h2hrvdcxh8y6iln0qjvarpnamxwbyi")))

(define-public crate-bpx-api-types-0.1.2 (c (n "bpx-api-types") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.33.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xzrhwp0nxm2grr2z4q35jgb6rws3rmg3jn11mzq6djigrafcfs7")))

