(define-module (crates-io bp f- bpf-ins) #:use-module (crates-io))

(define-public crate-bpf-ins-0.1.0 (c (n "bpf-ins") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "0phrh5alv4gzpl26bh46bc5lrkndwlm3pv6asfjrks3m3mayri0k")))

(define-public crate-bpf-ins-0.2.0 (c (n "bpf-ins") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "1fvk9sa1jrvxvnc84571i1vnqr767b1xfkmf65y38f984af4cybl")))

(define-public crate-bpf-ins-0.3.0 (c (n "bpf-ins") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "0zhprjcvjbql1y21hvmvyh650lgr7y894ic4gmgw1q2nq4xl7d8p")))

(define-public crate-bpf-ins-0.4.0 (c (n "bpf-ins") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "1qfll893lajk0gr0aiq250dx852inchnyp79c9jj4zs40g8g2dn7")))

(define-public crate-bpf-ins-0.5.0 (c (n "bpf-ins") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "1jwqh5j6pc9r1ficp3d52icdby30jrr1klkwdj589qkg2kmm38yk")))

(define-public crate-bpf-ins-0.6.0 (c (n "bpf-ins") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "04p63jib288aq4x5r99ckn2r6bwsx2zi6v6s0m798xpv12x13wa4")))

(define-public crate-bpf-ins-0.6.1 (c (n "bpf-ins") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "09d5vgjz4vfp9f9alqsyx99376pp8d73xihdg9rcvkm0ylxh8rwl")))

(define-public crate-bpf-ins-0.7.0 (c (n "bpf-ins") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1klkrbp77p6xcwpwndiazdamv5bfywxrfjw6xfnz22052p3ci8js")))

(define-public crate-bpf-ins-0.7.1 (c (n "bpf-ins") (v "0.7.1") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0rwbbgpq040wgsq3lbwbmhxwf7wqhb2058zs5xsivkkpqnxzyfwy")))

(define-public crate-bpf-ins-0.7.2 (c (n "bpf-ins") (v "0.7.2") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0j6bzwax09xcmv57aj3armrdyljrkavg2z91fwgsk10d2rcsj6k8")))

(define-public crate-bpf-ins-0.7.3 (c (n "bpf-ins") (v "0.7.3") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0363hz75mzqpx78cs6xsm2v68bq0njjph7gfpkh53m9dp5nv5kzr")))

(define-public crate-bpf-ins-0.7.4 (c (n "bpf-ins") (v "0.7.4") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "05q72gzq3frd3y86i9vm9h3909gcnssb7xnwwkaf1jhiw64smdi3")))

