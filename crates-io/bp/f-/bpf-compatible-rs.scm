(define-module (crates-io bp f- bpf-compatible-rs) #:use-module (crates-io))

(define-public crate-bpf-compatible-rs-0.1.0 (c (n "bpf-compatible-rs") (v "0.1.0") (d (list (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "uname-rs") (r "^0.1.1") (d #t) (k 0)))) (h "06dmxj4xldv2f44x2wb1za7ffrhw1wvhywgagr53hxvsrsm9d2qw")))

(define-public crate-bpf-compatible-rs-0.1.1 (c (n "bpf-compatible-rs") (v "0.1.1") (d (list (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "uname-rs") (r "^0.1.1") (d #t) (k 0)))) (h "1dpasaxcchqc1l38w4qb9jd03rbmdyrvg4i86ni12mrly3smpv02")))

(define-public crate-bpf-compatible-rs-0.1.2 (c (n "bpf-compatible-rs") (v "0.1.2") (d (list (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "uname-rs") (r "^0.1.1") (d #t) (k 0)))) (h "16irbfkw29wvajmws98dci9f446377k8lrfqjpw76hjns5nkr8vq")))

(define-public crate-bpf-compatible-rs-0.1.3 (c (n "bpf-compatible-rs") (v "0.1.3") (d (list (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "uname-rs") (r "^0.1.1") (d #t) (k 0)))) (h "0165cix8c63bb0sff5zwjmayn475v0dn8dp5knxf6984wgz34zni")))

