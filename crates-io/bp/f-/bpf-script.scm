(define-module (crates-io bp f- bpf-script) #:use-module (crates-io))

(define-public crate-bpf-script-0.1.0 (c (n "bpf-script") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpf-ins") (r "^0.6.0") (d #t) (k 0)) (d (n "btf") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.2.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.2.0") (d #t) (k 0)))) (h "1dc4w1ayciibzh7i9iqz1hjhjimj2qmmjmp8rkq58kbp3caary74")))

(define-public crate-bpf-script-0.1.1 (c (n "bpf-script") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpf-ins") (r "^0.6.0") (d #t) (k 0)) (d (n "btf") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.2.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.2.0") (d #t) (k 0)))) (h "0qz5fy3vqp2rf4wrixxfi6m59pv4dc2kms2cq0wv67rscw7rrz9k")))

(define-public crate-bpf-script-0.1.2 (c (n "bpf-script") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpf-ins") (r "^0.6.0") (d #t) (k 0)) (d (n "btf") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.2.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.2.0") (d #t) (k 0)))) (h "12v7qd2a1r3jglxp8mxnkd5fs4jnsdpprgpj9ps3afsa2jxgp44p")))

(define-public crate-bpf-script-0.1.3 (c (n "bpf-script") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpf-ins") (r "^0.6.1") (d #t) (k 0)) (d (n "btf") (r "^0.3.1") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)))) (h "1rzj45ssbr6rqi420902pkj1v89dns2vr8jdjfiirnqbfnp74ll6")))

(define-public crate-bpf-script-0.1.4 (c (n "bpf-script") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpf-ins") (r "^0.6.1") (d #t) (k 0)) (d (n "btf") (r "^0.3.1") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)))) (h "0shg1av0nsp91kplx8bllb3f1bd1hn33kalnbb5kq3ifhyspakfs")))

(define-public crate-bpf-script-0.1.5 (c (n "bpf-script") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpf-ins") (r "^0.6.1") (d #t) (k 0)) (d (n "btf") (r "^0.3.1") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)))) (h "1ch6n5ina29pn9mrz5zyzgr4djw2vj9yj7vk9smkidd1m4470cv9")))

(define-public crate-bpf-script-0.1.6 (c (n "bpf-script") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpf-ins") (r "^0.6.1") (d #t) (k 0)) (d (n "btf") (r "^0.3.1") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)))) (h "1fp8qlsfwflhkl262a91p39br7xjz841zsrb6kla2jzwzj6nk0v2")))

(define-public crate-bpf-script-0.1.7 (c (n "bpf-script") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpf-ins") (r "^0.6.1") (d #t) (k 0)) (d (n "btf") (r "^0.3.1") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)))) (h "1qknavlzikzzxnns8j037n0vibp1nv2bwa83mv6j4k5w9pbp15q3")))

(define-public crate-bpf-script-0.4.0 (c (n "bpf-script") (v "0.4.0") (d (list (d (n "bpf-ins") (r "^0.6.1") (d #t) (k 0)) (d (n "btf") (r "^0.4.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1432f4fikg2d2ciaxxqs5d2pv11pjk05ngfihigfm7h4s7ikryvy")))

(define-public crate-bpf-script-0.5.0 (c (n "bpf-script") (v "0.5.0") (d (list (d (n "bpf-ins") (r "^0.6.1") (d #t) (k 0)) (d (n "btf") (r "^0.4.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1fa6zdycw5ji5n2jij05ankfh2ffk2xpd7fhzlwiydkkn3ai7dgk")))

(define-public crate-bpf-script-0.5.1 (c (n "bpf-script") (v "0.5.1") (d (list (d (n "bpf-ins") (r "^0.6.1") (d #t) (k 0)) (d (n "btf") (r "^0.4.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "149flh3h8rzhx37lhwh6j7jhmr7xj11rmkw0z2vlcmgn54ya2h5i")))

(define-public crate-bpf-script-0.5.2 (c (n "bpf-script") (v "0.5.2") (d (list (d (n "bpf-ins") (r "^0.7") (d #t) (k 0)) (d (n "btf") (r "^0.4") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1k5ncdilbkswjv93qin5fy89qmifnj8gyw7y44x9v13bz1gbvj3z")))

(define-public crate-bpf-script-0.5.3 (c (n "bpf-script") (v "0.5.3") (d (list (d (n "bpf-ins") (r "^0.7") (d #t) (k 0)) (d (n "btf") (r "^0.5") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0zki2kd5k70i4zycgsiz64991v9bjnylrpizhzkas5p2s63m8yq6")))

(define-public crate-bpf-script-0.5.4 (c (n "bpf-script") (v "0.5.4") (d (list (d (n "bpf-ins") (r "^0.7") (d #t) (k 0)) (d (n "btf") (r "^0.5") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0brsm52i5acbw01k59m3jz45h0if1cidlnz27jrvpajsfglx0x0r")))

