(define-module (crates-io bp f- bpf-script-derive) #:use-module (crates-io))

(define-public crate-bpf-script-derive-0.4.0 (c (n "bpf-script-derive") (v "0.4.0") (d (list (d (n "bpf-script") (r "^0.4") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12dlc50a0d1krr5l38m5kifjqkh8wqpzhaj9srj2rcaprpl26qj6")))

(define-public crate-bpf-script-derive-0.5.0 (c (n "bpf-script-derive") (v "0.5.0") (d (list (d (n "bpf-script") (r "^0.5") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0iz5svqngy0ra3pi07b4a0q7hinzi5lf5x3qrph01glb41k0qmv0")))

(define-public crate-bpf-script-derive-0.5.1 (c (n "bpf-script-derive") (v "0.5.1") (d (list (d (n "bpf-script") (r "^0.5.1") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wdq02p6pi23b9bqwm62wrcv38wnl71m0kpv40vlsdrc4660sjgg")))

(define-public crate-bpf-script-derive-0.5.2 (c (n "bpf-script-derive") (v "0.5.2") (d (list (d (n "bpf-script") (r "^0.5.2") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dyzn3q5x2d48pilbzvkni35734yn0ppsj7ds6qhmlrg68izqhzc")))

(define-public crate-bpf-script-derive-0.5.3 (c (n "bpf-script-derive") (v "0.5.3") (d (list (d (n "bpf-script") (r "^0.5.3") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kv6yv79cdlqa7wg1chqhnnyg2pznn7axzy9nsfl2wwd4gfmw0j3")))

(define-public crate-bpf-script-derive-0.5.4 (c (n "bpf-script-derive") (v "0.5.4") (d (list (d (n "bpf-script") (r "^0.5.4") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rna8675z3dhrpmbs3qz3vx3zn112y6fwyssdav7fjjj09j014jc")))

