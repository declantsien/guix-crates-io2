(define-module (crates-io bp f- bpf-sys) #:use-module (crates-io))

(define-public crate-bpf-sys-0.1.0 (c (n "bpf-sys") (v "0.1.0") (d (list (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0vda7lagidb5m6hdk0jv1pxx3fzf075i8x5zp5m68n0n28adr56x") (l "bpf")))

(define-public crate-bpf-sys-0.1.1 (c (n "bpf-sys") (v "0.1.1") (d (list (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0kpay5n1wmlsnm6hq91h4gmyi1x09p8nlmw0ihzy532ys1yx8qwk") (l "bpf")))

(define-public crate-bpf-sys-0.2.1 (c (n "bpf-sys") (v "0.2.1") (d (list (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0bcxmnk2523x9smwp5f1y06ns57hd6z12n9s6lsf3w9s85z4qqhm") (l "bpf")))

(define-public crate-bpf-sys-0.3.1 (c (n "bpf-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "1fm3p5f3b8jgy56x135p5jfw4h7g7h8889pp4d21nhrmq2ihiwip") (l "bpf")))

(define-public crate-bpf-sys-0.3.3 (c (n "bpf-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "1kphxnbd8cpnpl30jdm211k5yxcflbrk66jhwbks8y86zzh5zghj") (l "bpf")))

(define-public crate-bpf-sys-0.9.0 (c (n "bpf-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "111ywjxaq0a4ll7f5pj99y5nkyh6n8clgqn0mvl0v3562pd8swfp") (l "bpf")))

(define-public crate-bpf-sys-0.9.1 (c (n "bpf-sys") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "031h5rijir0s01z2dyy0qn00xajy68l1m6rqzmxnicif2016pvzi") (l "bpf")))

(define-public crate-bpf-sys-0.9.2 (c (n "bpf-sys") (v "0.9.2") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0l791cvib57aqcaqvpp96hhjj94v1k8vjagcd8kg02z5q9ljjxqg") (l "bpf")))

(define-public crate-bpf-sys-0.9.3 (c (n "bpf-sys") (v "0.9.3") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "04cj7bdcad3zm1w123rilf1k5ifb5bkhi9dc21hwmfkibwz7wwbf") (l "bpf")))

(define-public crate-bpf-sys-0.9.4 (c (n "bpf-sys") (v "0.9.4") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0jycvi1rb4kh9c3j7175zlb4pq79x23dv92rfm3079dff4p415m3") (l "bpf")))

(define-public crate-bpf-sys-0.9.7 (c (n "bpf-sys") (v "0.9.7") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "1qklvhb0fypqya1v9jjzh791zhdcl8fivqhv34la1kh9mfj61h3s") (l "bpf")))

(define-public crate-bpf-sys-0.9.8 (c (n "bpf-sys") (v "0.9.8") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "11y7abga3qaqr8hgn5p4dgvq9b4pbcy3npsylcsjq161z0dyjxm2") (l "bpf")))

(define-public crate-bpf-sys-0.9.9 (c (n "bpf-sys") (v "0.9.9") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "01qasg4wrmcsx9a8inpk18ryrc6hcd0w58dgiwkxi47ximi3wszz") (l "bpf")))

(define-public crate-bpf-sys-0.9.10 (c (n "bpf-sys") (v "0.9.10") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0calz0gvhz96w9x01c7mn2y41xsb8wy8ddvkawmwxv3qfrh905id") (l "bpf")))

(define-public crate-bpf-sys-0.9.11 (c (n "bpf-sys") (v "0.9.11") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "1xnp21yzww506d8i6r9ga1dsn6n1di9rmcajvfnlr7jfssm581mj") (l "bpf")))

(define-public crate-bpf-sys-0.9.12 (c (n "bpf-sys") (v "0.9.12") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0mw7w80y84ldxn0d6p4l4brvchs36s941m8px52c9rdm7ycicdz9") (l "bpf")))

(define-public crate-bpf-sys-0.9.13 (c (n "bpf-sys") (v "0.9.13") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "18va9fb501mbqdqy0p6p1jr56g3g4q7f33rn3y1qy00rl7r9m37k") (l "bpf")))

(define-public crate-bpf-sys-1.0.0 (c (n "bpf-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0jmly0w4s07xcs9v5bd3jg5sxwmd40bf20dkwqb8xssc7ljny1p2") (l "bpf")))

(define-public crate-bpf-sys-1.0.1 (c (n "bpf-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0hqrdl06bpbn4dg3mxm73fy9zvra5lnl4rkv0mwvdn2yr3k53v30") (l "bpf")))

(define-public crate-bpf-sys-1.1.0 (c (n "bpf-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "1bwbq2qwm06r5dkygpg1lpgvyl80hqc5xpavbm5fx2qzzy5ak5rk") (l "bpf")))

(define-public crate-bpf-sys-1.1.1 (c (n "bpf-sys") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "11wf17r3grlrhh1w85z61dqp021zv8iiqnd28ciczyrqmqwhzxgs") (l "bpf")))

(define-public crate-bpf-sys-1.1.2 (c (n "bpf-sys") (v "1.1.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "1iy6yklvx6q6cwf3513sxmksrnqkxfb1bfpaibri804z5j6naix8") (l "bpf")))

(define-public crate-bpf-sys-1.2.0 (c (n "bpf-sys") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0b3n2bsxkfdblrdrc5ias45m13my6dn6j10m8vpr0gxqw8lmsjxq") (l "bpf")))

(define-public crate-bpf-sys-1.3.0 (c (n "bpf-sys") (v "1.3.0") (d (list (d (n "bindgen") (r "^0.55") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0n1md5b9qhjf7rl8h9y389a68bxjxa513fzz70pww2pysm0405qa") (l "bpf")))

(define-public crate-bpf-sys-2.0.0-rc1 (c (n "bpf-sys") (v "2.0.0-rc1") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "166m1x7wydsvfldmnljb7cp19na74hvh8rm0w4238jwl1fjjdbsr") (l "bpf")))

(define-public crate-bpf-sys-2.0.0-rc2 (c (n "bpf-sys") (v "2.0.0-rc2") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "1lzqz1rwb1n7a7gx49wfvjb3y2rx9ql6454bapri9qx09fb374jy") (l "bpf")))

(define-public crate-bpf-sys-2.0.0-rc3 (c (n "bpf-sys") (v "2.0.0-rc3") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0vnfzpj7pn28lx3gd02s78ci8fygjsrvd3fwk5w56ww6ddq0gry9") (l "bpf")))

(define-public crate-bpf-sys-2.0.0 (c (n "bpf-sys") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "00b0hd1pl4kr3rwyfxk28wgxki2brcmfnnsiaaimw76qlrvclfmj") (l "bpf")))

(define-public crate-bpf-sys-2.0.1 (c (n "bpf-sys") (v "2.0.1") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0r45vadfrim08p74cf7ms4m78l3jmz8kqr6jsjm8vai8gza8qrr9") (l "bpf")))

(define-public crate-bpf-sys-2.0.2 (c (n "bpf-sys") (v "2.0.2") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0b14rqva1y8dqvhyv8srgw7hxhhphyxkhfz1l0hyidm2yw7lvafn") (l "bpf")))

(define-public crate-bpf-sys-2.1.0 (c (n "bpf-sys") (v "2.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0hawdv7v4yjv5i8cwszjm9v6z43yp6q63mhidk5nx8iwmccjf2mr") (l "bpf")))

(define-public crate-bpf-sys-2.2.0 (c (n "bpf-sys") (v "2.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "06zjc6yk4a9d0sqxsclp7gbyji4jz3xcqmi9yhh5p7bkchq7kaar") (l "bpf")))

(define-public crate-bpf-sys-2.3.0 (c (n "bpf-sys") (v "2.3.0") (d (list (d (n "bindgen") (r "^0.59.2") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0r9dx5j5n0p96nskqcl38sh837b21amf80sw2754glz6ril0l9by") (l "bpf")))

