(define-module (crates-io bp f- bpf-rs) #:use-module (crates-io))

(define-public crate-bpf-rs-0.0.1 (c (n "bpf-rs") (v "0.0.1") (d (list (d (n "libbpf-sys") (r "^0.7.0") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0z7p3ipbbgj6bi5xjyw7c3grx11jbycjsccgsrrksaw0i89ypjzq") (y #t)))

(define-public crate-bpf-rs-0.0.2 (c (n "bpf-rs") (v "0.0.2") (d (list (d (n "libbpf-sys") (r "^0.7.0") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1c875cc0bg9zsrkij7dinhd0p349qciaf481m9ddfyn5a6rf26sz") (y #t)))

(define-public crate-bpf-rs-0.0.3 (c (n "bpf-rs") (v "0.0.3") (d (list (d (n "libbpf-sys") (r "^0.7.0") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "02zy9f2sf9pw0xxpx7v5h8372vbh4w6vl3sl1a6vxyr5yzylq2c4")))

(define-public crate-bpf-rs-0.0.4 (c (n "bpf-rs") (v "0.0.4") (d (list (d (n "libbpf-sys") (r "^0.7.0") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1pvr20bfj00rgfjq320qz403hy29k8g6w2hk4bjxyf15fw6ak2mb")))

(define-public crate-bpf-rs-0.0.5 (c (n "bpf-rs") (v "0.0.5") (d (list (d (n "libbpf-sys") (r "^0.7.1") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1l4ckabkxd0pi8s2m775ygrjrm144c8ddahwk37bj313as414gvh")))

(define-public crate-bpf-rs-0.0.6 (c (n "bpf-rs") (v "0.0.6") (d (list (d (n "libbpf-sys") (r "^0.7.1") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "17vjbdlgnpahap7x966pkd5lgw86bqch53m1gi2fbiqfvkm83yjm")))

(define-public crate-bpf-rs-0.0.7 (c (n "bpf-rs") (v "0.0.7") (d (list (d (n "libbpf-sys") (r "^0.7.1") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04ksvpasldvn23mda87wlwqg1my4g0fs2b47camr9l92kkq7y95l")))

(define-public crate-bpf-rs-0.0.8 (c (n "bpf-rs") (v "0.0.8") (d (list (d (n "bpf-rs-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.7.1") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0kkxw4mxi4wys9xcd190ljkrqxpagpw4xdcbi51d17xz1kjsmg9k") (s 2) (e (quote (("serde" "dep:serde" "bpf-rs-macros/serde"))))))

(define-public crate-bpf-rs-0.0.9 (c (n "bpf-rs") (v "0.0.9") (d (list (d (n "bpf-rs-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.7.1") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0drs055vxls62sk3n0nqgf4k2si9pxww1012qng8x9gbivm3cbyc") (s 2) (e (quote (("serde" "dep:serde" "bpf-rs-macros/serde"))))))

(define-public crate-bpf-rs-0.1.0 (c (n "bpf-rs") (v "0.1.0") (d (list (d (n "bpf-rs-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.8.0") (d #t) (k 0) (p "bpfdeploy-libbpf-sys")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0m1ndyp1l96jchxh0v38kvm33pws2ygrwny2gqdr8q3nby5w63ls") (s 2) (e (quote (("serde" "dep:serde" "bpf-rs-macros/serde"))))))

(define-public crate-bpf-rs-0.2.0 (c (n "bpf-rs") (v "0.2.0") (d (list (d (n "bpf-rs-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "bpfdeploy-libbpf-sys") (r "^0.8.0") (d #t) (k 2)) (d (n "libbpf-sys") (r "^0.8.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zdkr4i4b40l3v9mh1lrkis34vzlbfg586fghlr47b60bgg1qbdv") (s 2) (e (quote (("serde" "dep:serde" "bpf-rs-macros/serde"))))))

(define-public crate-bpf-rs-0.3.0 (c (n "bpf-rs") (v "0.3.0") (d (list (d (n "bpf-rs-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "bpfdeploy-libbpf-sys") (r "^1.0.1") (d #t) (k 2)) (d (n "libbpf-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "12mdbv4gdv0a59aw82m060r8cjkm3p3phihfqcjjj21kad3n7sr5") (s 2) (e (quote (("serde" "dep:serde" "bpf-rs-macros/serde")))) (r "1.60")))

