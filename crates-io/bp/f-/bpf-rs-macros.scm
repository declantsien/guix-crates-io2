(define-module (crates-io bp f- bpf-rs-macros) #:use-module (crates-io))

(define-public crate-bpf-rs-macros-0.0.1 (c (n "bpf-rs-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1b7b75x5nmj4acsglwsl0n81lrzxi19rqqffwfkzyx8167qq5h02") (f (quote (("serde"))))))

(define-public crate-bpf-rs-macros-0.0.2 (c (n "bpf-rs-macros") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 2)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "11vqlva50qrdca5wd5n6g0xbry39zzf305lfsb956dv5s8f17vmy") (f (quote (("serde"))))))

(define-public crate-bpf-rs-macros-0.1.0 (c (n "bpf-rs-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 2)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1ddwbaqf8q8ax3nrz73llfngv01w38j0glr874nyi8s7ham0nayx") (f (quote (("serde"))))))

(define-public crate-bpf-rs-macros-0.2.0 (c (n "bpf-rs-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1cw1c0qw1727siiiykx53172qyz75n6y3dkrmhxp1w7bmgg2y6g2") (r "1.60")))

