(define-module (crates-io bp f- bpf-oci) #:use-module (crates-io))

(define-public crate-bpf-oci-0.1.0 (c (n "bpf-oci") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "oci-distribution") (r "^0.9.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1kcww1wnzhl5rd0c371hr5kxjk06i58c1sv03vinj8dr719s4bwy")))

