(define-module (crates-io bp fd bpfdeploy-libbpf-sys) #:use-module (crates-io))

(define-public crate-bpfdeploy-libbpf-sys-0.7.0+v0.7.0 (c (n "bpfdeploy-libbpf-sys") (v "0.7.0+v0.7.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0x7qn99y9wf0a6wcwsn1fnzksai16czxi1x03gka8y5qrkafjszl") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-bpfdeploy-libbpf-sys-0.7.1+v0.7.0 (c (n "bpfdeploy-libbpf-sys") (v "0.7.1+v0.7.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jnrk1qg39vdyw6r0n2f9bsdwk48zd5a1rvzcfy81fd8ziy7n09b") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-bpfdeploy-libbpf-sys-0.7.2+v0.7.0 (c (n "bpfdeploy-libbpf-sys") (v "0.7.2+v0.7.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "16za5vnj9i787j4mbz9f11sy9ik49nmz8bsa47qsva2dah57aj57") (f (quote (("novendor")))) (l "libbpf-vendored")))

(define-public crate-bpfdeploy-libbpf-sys-0.8.0+v0.8.0 (c (n "bpfdeploy-libbpf-sys") (v "0.8.0+v0.8.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0iqn7c7hg567s8yzzrdarmh4q44rxf6kd6sszi3ikab8aibmdzwq") (f (quote (("novendor")))) (l "libbpf-vendored")))

(define-public crate-bpfdeploy-libbpf-sys-1.0.0+v1.0.0 (c (n "bpfdeploy-libbpf-sys") (v "1.0.0+v1.0.0") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0pw7i9kby0q8n6k9py783kg11g3bczh0jyz9y5z8fz0damwydndb") (f (quote (("static") ("novendor")))) (l "libbpf")))

(define-public crate-bpfdeploy-libbpf-sys-1.0.1+v1.0.0 (c (n "bpfdeploy-libbpf-sys") (v "1.0.1+v1.0.0") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0g39n0ydkgg4wrg94crvy136akgf6y7l3g0qpsvikpq7nj448zyv") (f (quote (("static") ("novendor")))) (l "libbpf-bpfdeploy")))

