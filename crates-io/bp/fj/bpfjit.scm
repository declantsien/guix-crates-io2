(define-module (crates-io bp fj bpfjit) #:use-module (crates-io))

(define-public crate-bpfjit-0.1.0 (c (n "bpfjit") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04kzl9vfabpkjgxsd9ynpbwjxdjyw4cik1sn2y0y6dl7ls1kr43j")))

(define-public crate-bpfjit-0.1.1 (c (n "bpfjit") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1w2achy0blgbfinq96y5r33547y1dc4hhr9lgbxwsz3r2g39w8cm")))

