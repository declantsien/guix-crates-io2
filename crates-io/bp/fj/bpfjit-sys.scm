(define-module (crates-io bp fj bpfjit-sys) #:use-module (crates-io))

(define-public crate-bpfjit-sys-1.0.0 (c (n "bpfjit-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g980w1yzxipzpfxgs3pgfdzc9x2hmr6na7irjsxal1wrrlqmr2a")))

(define-public crate-bpfjit-sys-1.0.1 (c (n "bpfjit-sys") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ibivdlbj7qgiar2ycqb3dn47rk6zz0ih7yhzcf3a31nxyv975nm")))

(define-public crate-bpfjit-sys-1.0.2 (c (n "bpfjit-sys") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xcinaw9xxdzm7sy266m3dzjc298sxy4fw1pf53rh5921gwz4lwd")))

(define-public crate-bpfjit-sys-1.1.0 (c (n "bpfjit-sys") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vscgnw5kdmcivm3vyvgp8ffcminhgd70xxvn5a2p6bzgkby69cs")))

(define-public crate-bpfjit-sys-2.0.0 (c (n "bpfjit-sys") (v "2.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04gpp6ic5r673l0ayqyqr6d0f4rnxd6xjh2v2f6fsjvc3rc4anz5")))

(define-public crate-bpfjit-sys-2.1.0 (c (n "bpfjit-sys") (v "2.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n97lxf7a4h2gj3wk4dzc3i4jsdid3las2v57b19rgps6qm1lwsb")))

