(define-module (crates-io bp _a bp_analyser) #:use-module (crates-io))

(define-public crate-bp_analyser-0.0.0 (c (n "bp_analyser") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hd3sfcppl5bs4pkdr4zfzvapqf4izr0kg4aigk1rw59mbdcl1a3")))

