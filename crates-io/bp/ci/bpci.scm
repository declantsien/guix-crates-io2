(define-module (crates-io bp ci bpci) #:use-module (crates-io))

(define-public crate-bpci-0.1.0-alpha.1 (c (n "bpci") (v "0.1.0-alpha.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "10bwdk75i1kml63ril0rm9ip49b4sdf5yg2m21vh6ahwrhd369km") (f (quote (("default"))))))

(define-public crate-bpci-0.1.0-alpha.2 (c (n "bpci") (v "0.1.0-alpha.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0a42zxyczyvhh14fhcy9iin84axnnccnhpay6kvw0hyikjkkg0k5") (f (quote (("default"))))))

(define-public crate-bpci-0.1.0-beta.1 (c (n "bpci") (v "0.1.0-beta.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16xs5fq20sh1p6j3nqqslhqxwqpqar43wizwlcvnca450glldalz") (f (quote (("default"))))))

(define-public crate-bpci-0.1.0-beta.2 (c (n "bpci") (v "0.1.0-beta.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1fmhzkayqm70c7mxa4dfkiw1bdvbvr57z6ylfwxnnsigkqivs83h") (f (quote (("default"))))))

(define-public crate-bpci-0.1.0-beta.3 (c (n "bpci") (v "0.1.0-beta.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "05szd0q66flgbji1mhc407s78bl93k1a7r3xckcpdh5bwfp72da5") (f (quote (("default"))))))

(define-public crate-bpci-0.1.0-beta.4 (c (n "bpci") (v "0.1.0-beta.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0dlc84hgx4yrrilzywqvdb6898kdc9wq4s2ji7jq2mjz35jql1xn") (f (quote (("default"))))))

(define-public crate-bpci-0.1.0-beta.5 (c (n "bpci") (v "0.1.0-beta.5") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ls5vgvrkib272vx8r5i09dbd0d0j3xa2hki95xn861m886mrcmc") (f (quote (("default"))))))

(define-public crate-bpci-0.1.0-beta.6 (c (n "bpci") (v "0.1.0-beta.6") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "12f194s7ycmjkkjk6jdziw2hxhsq44d74knjfjnx310wbicbc0ic") (f (quote (("default"))))))

(define-public crate-bpci-0.1.0-beta.7 (c (n "bpci") (v "0.1.0-beta.7") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1gjc66ic2788djzr9940kjh3jqr3b03jrg2ypc7818j0z56z02kz") (f (quote (("default"))))))

(define-public crate-bpci-0.1.0 (c (n "bpci") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "19q6wymcbqwwma38glgapz4glbvkfq6wkfajy7mhlpzhpfp3yb2m") (f (quote (("default"))))))

