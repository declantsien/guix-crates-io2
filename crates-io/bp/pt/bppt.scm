(define-module (crates-io bp pt bppt) #:use-module (crates-io))

(define-public crate-bppt-0.2.4 (c (n "bppt") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lg423samqg6zyjrgk6pbnps7r831b2sbxb8cxpvv53y1v7nnrnr")))

(define-public crate-bppt-0.2.5 (c (n "bppt") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yazk4ygkd01spx4ncm14qnglnibn1cww200qcparw0lk7fvzchh")))

