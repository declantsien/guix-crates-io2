(define-module (crates-io bp -a bp-asset-hub-kusama) #:use-module (crates-io))

(define-public crate-bp-asset-hub-kusama-0.0.0 (c (n "bp-asset-hub-kusama") (v "0.0.0") (h "0447v7v9qjbnv0y5ixz0bsl0x49lpngpvfgzi5v4m818v2y7q4aa")))

(define-public crate-bp-asset-hub-kusama-0.1.0 (c (n "bp-asset-hub-kusama") (v "0.1.0") (d (list (d (n "bp-xcm-bridge-hub-router") (r "=0.2.0-dev.1") (k 0)) (d (n "codec") (r "^3.1.5") (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "=24.0.0-dev.1") (k 0)) (d (n "scale-info") (r "^2.9.0") (f (quote ("derive"))) (k 0)))) (h "0akmv2j9hh4zynxf25k2j4yjzw3wx0nh07i7k8ng4p4v2m3lyhwa") (f (quote (("std" "bp-xcm-bridge-hub-router/std" "codec/std" "frame-support/std" "scale-info/std") ("default" "std"))))))

(define-public crate-bp-asset-hub-kusama-0.2.0 (c (n "bp-asset-hub-kusama") (v "0.2.0") (d (list (d (n "bp-xcm-bridge-hub-router") (r "^0.3.0") (k 0)) (d (n "codec") (r "^3.1.5") (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^25.0.0") (k 0)) (d (n "scale-info") (r "^2.10.0") (f (quote ("derive"))) (k 0)))) (h "0x703g7cdhz6wciffgdgdzf5j8qq69j03n6x6ymz5c1nhfgkfj39") (f (quote (("std" "bp-xcm-bridge-hub-router/std" "codec/std" "frame-support/std" "scale-info/std") ("default" "std"))))))

