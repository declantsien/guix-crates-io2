(define-module (crates-io bp -a bp-asset-hub-polkadot) #:use-module (crates-io))

(define-public crate-bp-asset-hub-polkadot-0.0.0 (c (n "bp-asset-hub-polkadot") (v "0.0.0") (h "05b9f5d37bd5qhbzyifllx3vsgjmh471bmiaxmp31ylbnibqbgkq")))

(define-public crate-bp-asset-hub-polkadot-0.1.0 (c (n "bp-asset-hub-polkadot") (v "0.1.0") (d (list (d (n "bp-xcm-bridge-hub-router") (r "=0.2.0-dev.1") (k 0)) (d (n "codec") (r "^3.1.5") (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "=24.0.0-dev.1") (k 0)) (d (n "scale-info") (r "^2.9.0") (f (quote ("derive"))) (k 0)) (d (n "sp-runtime") (r "=27.0.0-dev.1") (k 0)))) (h "12xi0n4rs7sqrzqf0lxdqajjvqr62zfxmy6w9s1c8ccjz05p5373") (f (quote (("std" "bp-xcm-bridge-hub-router/std" "codec/std" "frame-support/std" "scale-info/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-bp-asset-hub-polkadot-0.2.0 (c (n "bp-asset-hub-polkadot") (v "0.2.0") (d (list (d (n "bp-xcm-bridge-hub-router") (r "^0.3.0") (k 0)) (d (n "codec") (r "^3.1.5") (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^25.0.0") (k 0)) (d (n "scale-info") (r "^2.10.0") (f (quote ("derive"))) (k 0)) (d (n "sp-runtime") (r "^28.0.0") (k 0)))) (h "0n4bsb0h17qwj3h0194fsnl7n4759n28g60rrliklvz0fllkypd0") (f (quote (("std" "bp-xcm-bridge-hub-router/std" "codec/std" "frame-support/std" "scale-info/std" "sp-runtime/std") ("default" "std"))))))

