(define-module (crates-io bp -a bp-asset-hub-wococo) #:use-module (crates-io))

(define-public crate-bp-asset-hub-wococo-0.0.0 (c (n "bp-asset-hub-wococo") (v "0.0.0") (h "0hc93dj001wp4cx6pkg40yvs9v7mpgh3bwrk6k00rlpyz5rlj9v9")))

(define-public crate-bp-asset-hub-wococo-0.1.0 (c (n "bp-asset-hub-wococo") (v "0.1.0") (d (list (d (n "bp-xcm-bridge-hub-router") (r "^0.3.0") (k 0)) (d (n "codec") (r "^3.1.5") (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^25.0.0") (k 0)) (d (n "scale-info") (r "^2.10.0") (f (quote ("derive"))) (k 0)))) (h "1r3qwa1kzbb3rs4zi1kwp7ah763wvgc0vh24ix96k4awzkdgf78h") (f (quote (("std" "bp-xcm-bridge-hub-router/std" "codec/std" "frame-support/std" "scale-info/std") ("default" "std"))))))

