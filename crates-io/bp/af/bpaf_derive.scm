(define-module (crates-io bp af bpaf_derive) #:use-module (crates-io))

(define-public crate-bpaf_derive-0.1.0 (c (n "bpaf_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08238cz0gw5drjsmd0clc7ya20i9n089bg217a7cs87zpi8aljm2")))

(define-public crate-bpaf_derive-0.1.1 (c (n "bpaf_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15yrjxx0m1hcbx8mzlkb9bvzfmrb2vrbakrxn9qh179syval18mq")))

(define-public crate-bpaf_derive-0.1.2 (c (n "bpaf_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g4ppbp0c333fm8n0ap56wrxzmx100152gxnh010yhbjhxgq244l")))

(define-public crate-bpaf_derive-0.1.3 (c (n "bpaf_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17n1dcq6qpr2ym0v71mpzcx8jzldcb60ybybfwphjkks0ic0zdjc")))

(define-public crate-bpaf_derive-0.1.4 (c (n "bpaf_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12l2gyqqb1jk38in924hsn72fmqvrxwwhz68llzngyf58a09k2yi")))

(define-public crate-bpaf_derive-0.2.0 (c (n "bpaf_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08nw4jyx9k2gqlasvg98fkgqcmg7pgkrg7s260v5p73hlhs085k9")))

(define-public crate-bpaf_derive-0.2.1 (c (n "bpaf_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18wqfp5y3fp2qi4s5g7zfgimjjvda6x8glailiff73lnyzp5494w")))

(define-public crate-bpaf_derive-0.2.2 (c (n "bpaf_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14nxslhq8l1l04lsbcv41mp3dgq2gxwcsgpqsd8ycm4n3s6jqpkj")))

(define-public crate-bpaf_derive-0.3.0 (c (n "bpaf_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b01x5xfynj5f7yxs21s0qy33ihc71j0rm9yq74m40zyi38z4big")))

(define-public crate-bpaf_derive-0.3.1 (c (n "bpaf_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "140pfhgm1gpmcig0789fr7h2vq17fnyqk9j86nmclfxihr3i1v7n")))

(define-public crate-bpaf_derive-0.3.2 (c (n "bpaf_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1va9arfssda2sfablklyf8nz9rpsar3p5pxz9r0z2qk86i1jzdy4")))

(define-public crate-bpaf_derive-0.3.3 (c (n "bpaf_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j27gdaf68gjq7pdjzd51q008x2c0kqcs6hkvljvpadkpw9k3mjy")))

(define-public crate-bpaf_derive-0.3.4 (c (n "bpaf_derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15d1179apdksdb4l4k4z7pqb6c98qby4b6ax66gqry9lf2g3qgr2")))

(define-public crate-bpaf_derive-0.3.5 (c (n "bpaf_derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d1jv15aklb896dcvlhy482q986g7y3lngqmm0whii4bw0yf57mf")))

(define-public crate-bpaf_derive-0.4.0 (c (n "bpaf_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "054k03v5dnpfzx105qg2myalhv9ab7xgj1r1xmk45q93g6hgr9r9")))

(define-public crate-bpaf_derive-0.4.1 (c (n "bpaf_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y50kjg5f1q3cb5gaavbnaj1192vywjwnlqmzs993i68fk0isg5g")))

(define-public crate-bpaf_derive-0.5.0 (c (n "bpaf_derive") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17cb1mb39438izlwdaw0bixizfbx872jh0yw7p9z2m6kjmmylq6b")))

(define-public crate-bpaf_derive-0.5.1 (c (n "bpaf_derive") (v "0.5.1") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x0bis7c4gnp1v4qidghdjj517wvkb38krijk4dr7fyprxfvxdzn")))

(define-public crate-bpaf_derive-0.5.2 (c (n "bpaf_derive") (v "0.5.2") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xp0brfzfavyqbpc1fv9aigcazbplwwgb9n50mw202k21ghzb25c")))

(define-public crate-bpaf_derive-0.5.3 (c (n "bpaf_derive") (v "0.5.3") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01q3wc84387vsas1kgbzagsykqjlir2s2w0m67xahhhdz2212v8k")))

(define-public crate-bpaf_derive-0.5.5 (c (n "bpaf_derive") (v "0.5.5") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fa5jvp8yppd3xcra72x7a7md91q63cg1sal1ndvdjxvc2iapjr2")))

(define-public crate-bpaf_derive-0.5.6 (c (n "bpaf_derive") (v "0.5.6") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y9zfhfhdm6aflq67gwvsk80xacxjhm7hvbs26gmfqv8f0sp56vv")))

(define-public crate-bpaf_derive-0.5.7 (c (n "bpaf_derive") (v "0.5.7") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q3rzlifr6m8ad3185h2ql1j8crqlrb8i2fgbm2dw0l1bybv5spg")))

(define-public crate-bpaf_derive-0.5.10 (c (n "bpaf_derive") (v "0.5.10") (d (list (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "177q9dfrj35n8rs9i1ciyr3h1566qb4bd2mcpgjnh47syw8mp3cs")))

