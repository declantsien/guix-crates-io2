(define-module (crates-io bp af bpaf) #:use-module (crates-io))

(define-public crate-bpaf-0.2.0 (c (n "bpaf") (v "0.2.0") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "1dsi5fy1abj8r9xyhal6hra1kfqq5a988rvmvwzcxkqvnpm6zj2f")))

(define-public crate-bpaf-0.2.1 (c (n "bpaf") (v "0.2.1") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "1smbspaylkplsick45v7f1x0cgk5x67jrm5p4qbd6x79zmnh3r8b")))

(define-public crate-bpaf-0.3.0 (c (n "bpaf") (v "0.3.0") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "1k8j3fbp4azia2fhmyscj0xgics5ybxy444b3m3wl54k1mf3x8fv")))

(define-public crate-bpaf-0.3.1 (c (n "bpaf") (v "0.3.1") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "0di3wkls05wx41hcn51agrigbksswxbkn82qb1gc3r9mm7gzg6bs")))

(define-public crate-bpaf-0.3.2 (c (n "bpaf") (v "0.3.2") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "00banigyjm7q9ax69x28j524hylqmbdcv1aywkclidapdh0w6i2l")))

(define-public crate-bpaf-0.4.0 (c (n "bpaf") (v "0.4.0") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "0gq1nnn350ncwb09kkhdapgak23dxwr3hbhc657vypvdhmbxgcma") (f (quote (("derive" "bpaf_derive"))))))

(define-public crate-bpaf-0.4.1 (c (n "bpaf") (v "0.4.1") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "01an1mmvlh1cmvs89x94za02kf637r21d78kjz82gj6y180ir8vj") (f (quote (("derive" "bpaf_derive"))))))

(define-public crate-bpaf-0.4.2 (c (n "bpaf") (v "0.4.2") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "1bwkvmkf0swcb7i2k66qnlzvg5gf4ayi928vaway99277ndbkx62") (f (quote (("derive" "bpaf_derive"))))))

(define-public crate-bpaf-0.4.3 (c (n "bpaf") (v "0.4.3") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "1d87rmhfz6y7ln5zx7iv7ay1d8k399nhbnw31plny8yrn8x2pbnk") (f (quote (("derive" "bpaf_derive"))))))

(define-public crate-bpaf-0.4.4 (c (n "bpaf") (v "0.4.4") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "044i6ps96dhwf0jbi5ib45v6z00ki1b4grrmjaq0d6hf7wjq0nhw") (f (quote (("derive" "bpaf_derive")))) (r "1.56")))

(define-public crate-bpaf-0.4.5 (c (n "bpaf") (v "0.4.5") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "0agmp3jkv5kimj143kix1xxgzx1iw90803x2qqac37m0aarslcz6") (f (quote (("derive" "bpaf_derive")))) (r "1.56")))

(define-public crate-bpaf-0.4.6 (c (n "bpaf") (v "0.4.6") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "1910jfkpvq4ych70c4z14chm7by4w3jip8v2f1hd39c5x6lba70c") (f (quote (("derive" "bpaf_derive")))) (r "1.56")))

(define-public crate-bpaf-0.4.7 (c (n "bpaf") (v "0.4.7") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "0w7rm6l7spsdm4s3ki4c1im854phpahinymv5ydkwrk3g85aw080") (f (quote (("derive" "bpaf_derive")))) (r "1.56")))

(define-public crate-bpaf-0.4.8 (c (n "bpaf") (v "0.4.8") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "0f32zl1fas5mnxgmdf5c83krdw3d1iz6h6nkf6sgicdrhfp767w1") (f (quote (("derive" "bpaf_derive")))) (r "1.56")))

(define-public crate-bpaf-0.4.9 (c (n "bpaf") (v "0.4.9") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "1dabfn643jmylb5bahak0rg81l18s0sa1fx3cizdsz426xjirfsr") (f (quote (("derive" "bpaf_derive")))) (r "1.56")))

(define-public crate-bpaf-0.4.10 (c (n "bpaf") (v "0.4.10") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "1zsswq25klbddxmrcnk7ibhbydpgd4wjcydaf6vl7gq35h6s52ji") (f (quote (("derive" "bpaf_derive")))) (r "1.56")))

(define-public crate-bpaf-0.4.11 (c (n "bpaf") (v "0.4.11") (d (list (d (n "bpaf_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "05gg3kjfghxmx2bhf7hq3z2xgrni0s4xm9ym1yrgiqzjlpljkbrm") (f (quote (("derive" "bpaf_derive")))) (r "1.56")))

(define-public crate-bpaf-0.4.12 (c (n "bpaf") (v "0.4.12") (d (list (d (n "bpaf_derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 2)))) (h "185qzyjsgm00d7lafashp3vwnbam558ldns917wsbymcimg78123") (f (quote (("derive" "bpaf_derive")))) (r "1.56")))

(define-public crate-bpaf-0.5.0 (c (n "bpaf") (v "0.5.0") (d (list (d (n "bpaf_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1zq5n2bbx0jzw14w0jps6i7nr6h1q3wnphr81xsgkwqidnfb4z6l") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries")))) (r "1.56")))

(define-public crate-bpaf-0.5.1 (c (n "bpaf") (v "0.5.1") (d (list (d (n "bpaf_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "14mq0i1pw17c9m6in68cbbd3wbwcxf5krcf2py01v79ai9hisq7v") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries")))) (y #t) (r "1.56")))

(define-public crate-bpaf-0.5.2 (c (n "bpaf") (v "0.5.2") (d (list (d (n "bpaf_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0d5c02lsn4crycjafhf27fjq1kb4rm7knlxbmdnh9x423dbcaf8d") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries")))) (r "1.56")))

(define-public crate-bpaf-0.5.3 (c (n "bpaf") (v "0.5.3") (d (list (d (n "bpaf_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0xrqzhqxj8nlrb9pzk3kqrksx0abv7p4khc73dqssb3z71a5czvl") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries")))) (r "1.56")))

(define-public crate-bpaf-0.5.4 (c (n "bpaf") (v "0.5.4") (d (list (d (n "bpaf_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0pld5i8x6lrpy1l5jpmcz7waavi14hbgggwk01h3492ppsnyvi2v") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries")))) (r "1.56")))

(define-public crate-bpaf-0.5.5 (c (n "bpaf") (v "0.5.5") (d (list (d (n "bpaf_derive") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0hasji9xzd3m7qf9c108pc4bwgf5832mxakqdn2jni0bici1bjrp") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.5.6 (c (n "bpaf") (v "0.5.6") (d (list (d (n "bpaf_derive") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "037c04gkw9mn6gzbww09n6alqix5wh1s99n124262s1h3dqm8snm") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.5.7 (c (n "bpaf") (v "0.5.7") (d (list (d (n "bpaf_derive") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "058jkwrsbhbk9y72mz8awq7wkhc5qmagn2kax80wp84381wjyzig") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.6.0 (c (n "bpaf") (v "0.6.0") (d (list (d (n "bpaf_derive") (r "=0.3.0") (o #t) (d #t) (k 0)))) (h "1s03amx47mjbn494xzwaw9jn5hm0ylgzibnhjsa4dnqbxpz88sx2") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.6.1 (c (n "bpaf") (v "0.6.1") (d (list (d (n "bpaf_derive") (r "=0.3.0") (o #t) (d #t) (k 0)))) (h "00lwq9lhvxrikj98xwg5cg03wj46vbnw90ikfsk36r4bd1pcdvs2") (f (quote (("extradocs") ("derive" "bpaf_derive") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.0 (c (n "bpaf") (v "0.7.0") (d (list (d (n "bpaf_derive") (r "=0.3.1") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)))) (h "0g936p4lqrkqkb6vfp6rfz1q5js8n14f5hf8w0fmbvzdrf2dl3z0") (f (quote (("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.1 (c (n "bpaf") (v "0.7.1") (d (list (d (n "bpaf_derive") (r "=0.3.1") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)))) (h "05n31f9fxrbla67g5dbnigam3r55z534f8gk38nqc6n95yz6vrma") (f (quote (("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.2 (c (n "bpaf") (v "0.7.2") (d (list (d (n "bpaf_derive") (r "=0.3.1") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)))) (h "0wryvi1bnn0g4x7g5lpmpa0v6kr0myml8axj1cv2pq1vz92gvvm1") (f (quote (("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.3 (c (n "bpaf") (v "0.7.3") (d (list (d (n "bpaf_derive") (r "=0.3.2") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)))) (h "1pgzsvc6sxlwn4wszi8zs70wvaxvfyv52n85ab1ba0zmi40739im") (f (quote (("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.4 (c (n "bpaf") (v "0.7.4") (d (list (d (n "bpaf_derive") (r "=0.3.3") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)))) (h "0x0bxqslrvlr0n9lx3z4brm8pw8k2q2lvaa078xja6gs9zf2xn61") (f (quote (("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.5 (c (n "bpaf") (v "0.7.5") (d (list (d (n "bpaf_derive") (r "=0.3.3") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)))) (h "1b71s0xxi5da7dz11k121jzg7082c01pmc45jy7k4yq87pp61f5v") (f (quote (("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.6 (c (n "bpaf") (v "0.7.6") (d (list (d (n "bpaf_derive") (r "=0.3.3") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)))) (h "1566kkhy3ds99kn13bz5kc7k5hx92f35rzdb5i5vw9mwl68ryvjh") (f (quote (("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.7 (c (n "bpaf") (v "0.7.7") (d (list (d (n "bpaf_derive") (r "=0.3.3") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0kqbc0w9f0b06yp5a10m1r4kkvracrhg4fs1pqhxqvpa9m3b358f") (f (quote (("manpage" "roff") ("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.8 (c (n "bpaf") (v "0.7.8") (d (list (d (n "bpaf_derive") (r "=0.3.3") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0g72ydhma0flwdcm70kf0f5ch89lkg6x19mkpgwyniayfwhhng46") (f (quote (("manpage" "roff") ("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.9 (c (n "bpaf") (v "0.7.9") (d (list (d (n "bpaf_derive") (r "=0.3.4") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1bl124d00agcva403ai9mw88j2i9c6agp0bc22aq6qb7kkgckx1i") (f (quote (("manpage" "roff") ("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.7.10 (c (n "bpaf") (v "0.7.10") (d (list (d (n "bpaf_derive") (r "=0.3.5") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "10rprnf4ia9p1k8k39phmwz874k63kjcmgw6yihwbhic72xayfs2") (f (quote (("manpage" "roff") ("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.8.0 (c (n "bpaf") (v "0.8.0") (d (list (d (n "bpaf_derive") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0jvcbpmr2wacdln034yyaivxr55sj8kpqcb1f17ys5sxb4qf3rbs") (f (quote (("manpage" "roff") ("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.8.1 (c (n "bpaf") (v "0.8.1") (d (list (d (n "bpaf_derive") (r "=0.4.1") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (o #t) (d #t) (k 0)) (d (n "roff") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0hpkb306h8bbz89plyhqhl2b78pcng14hgclz1hsf6s9hwi29b4b") (f (quote (("manpage" "roff") ("extradocs") ("dull-color" "color") ("derive" "bpaf_derive") ("color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.0 (c (n "bpaf") (v "0.9.0") (d (list (d (n "bpaf_derive") (r "=0.5.0") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0q342sz12af9cay2wvsc8x8dlwq8a0xifgcbvsmgr3bs1dvnsb8a") (f (quote (("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.1 (c (n "bpaf") (v "0.9.1") (d (list (d (n "bpaf_derive") (r "=0.5.1") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0w9qaj1fdigly82jpdvci7qa0k5ni9d7iz602kbp6cf8pyblb5vj") (f (quote (("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.2 (c (n "bpaf") (v "0.9.2") (d (list (d (n "bpaf_derive") (r "=0.5.2") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1fvjyai0vvvyb8rc6r9gbbzsdk7kzxj4s1npj1mn64js0f2l3ymg") (f (quote (("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.3 (c (n "bpaf") (v "0.9.3") (d (list (d (n "bpaf_derive") (r "=0.5.3") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1g5i8p1s3jig2x25hliywnszs9s6mcngzyi96w5fldy4avv1bggn") (f (quote (("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.4 (c (n "bpaf") (v "0.9.4") (d (list (d (n "bpaf_derive") (r "=0.5.3") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1ycmbn6pyg4hrjgyp449wfy1yn83k4l4pwisx0w347gm8bkwanf3") (f (quote (("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.5 (c (n "bpaf") (v "0.9.5") (d (list (d (n "bpaf_derive") (r "=0.5.5") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1nanzqs2hi6c62bsx7ib4izmrlpzx3pc71iml3p1d3aacnyv3hqx") (f (quote (("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.6 (c (n "bpaf") (v "0.9.6") (d (list (d (n "bpaf_derive") (r "=0.5.6") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1lra9asx43lgmr8h20v9iagnrjcwnsk98z4jrj4w3kyzdniqhmjn") (f (quote (("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.7 (c (n "bpaf") (v "0.9.7") (d (list (d (n "bpaf_derive") (r "=0.5.7") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "01s4aavavnjjnlkm8bh8v64ipnlgjbjp7idql84dl3rq3v3ms8k4") (f (quote (("unstable-doc" "derive" "extradocs" "autocomplete" "batteries" "docgen") ("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.8 (c (n "bpaf") (v "0.9.8") (d (list (d (n "bpaf_derive") (r "=0.5.7") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "088yn4dq3jpdyncgwygn9v9acmx483m8vgfsys9xk4jkhmyjs8qr") (f (quote (("unstable-docs" "derive" "extradocs" "autocomplete" "batteries" "docgen") ("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.9 (c (n "bpaf") (v "0.9.9") (d (list (d (n "bpaf_derive") (r "=0.5.7") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r ">=3.5, <5.0") (o #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0gadx73p0a88wjgqvh1gi3rszf91nyqwam3g36hbxn9i1fs35jgd") (f (quote (("unstable-docs" "derive" "extradocs" "autocomplete" "batteries" "docgen") ("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.10 (c (n "bpaf") (v "0.9.10") (d (list (d (n "bpaf_derive") (r "=0.5.10") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r ">=3.5, <5.0") (o #t) (k 0)) (d (n "supports-color") (r ">=2.0.0, <4.0") (o #t) (d #t) (k 0)))) (h "1hggs4cflbjysrasvajf9gyl74dqiz3wg583x6lmq36224r063k7") (f (quote (("unstable-docs" "derive" "extradocs" "autocomplete" "batteries" "docgen") ("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.11 (c (n "bpaf") (v "0.9.11") (d (list (d (n "bpaf_derive") (r "=0.5.10") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r ">=3.5, <5.0") (o #t) (k 0)) (d (n "supports-color") (r ">=2.0.0, <4.0") (o #t) (d #t) (k 0)))) (h "10gng04f16v50xm7g6wfq4dgqri3gcj2lyqn3gqhs42llzqcazsn") (f (quote (("unstable-docs" "derive" "extradocs" "autocomplete" "batteries" "docgen") ("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

(define-public crate-bpaf-0.9.12 (c (n "bpaf") (v "0.9.12") (d (list (d (n "bpaf_derive") (r "=0.5.10") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r ">=3.5, <5.0") (o #t) (k 0)) (d (n "supports-color") (r ">=2.0.0, <4.0") (o #t) (d #t) (k 0)))) (h "0kbjcgkgy393nf8dazg6kdyj2yp4rs5dqrwvrz17gg36dp7yz01j") (f (quote (("unstable-docs" "derive" "extradocs" "autocomplete" "batteries" "docgen") ("extradocs") ("dull-color" "color") ("docgen") ("derive" "bpaf_derive") ("color" "supports-color" "owo-colors") ("bright-color" "color") ("batteries") ("autocomplete")))) (r "1.56")))

