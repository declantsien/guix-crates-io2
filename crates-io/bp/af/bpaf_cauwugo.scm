(define-module (crates-io bp af bpaf_cauwugo) #:use-module (crates-io))

(define-public crate-bpaf_cauwugo-0.1.0 (c (n "bpaf_cauwugo") (v "0.1.0") (d (list (d (n "bpaf") (r "^0.6.1") (f (quote ("derive" "autocomplete"))) (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.1") (d #t) (k 0)))) (h "1sj61m5vphgh4cfhyrdbkz0c4l2i9g4akp1ynvscwd2xqh0ms6d3")))

