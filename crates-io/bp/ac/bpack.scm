(define-module (crates-io bp ac bpack) #:use-module (crates-io))

(define-public crate-bpack-0.1.0 (c (n "bpack") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06ky3qb6h2fifgff0r77p9m63bp6sqjgyxzw78q7wbxd1dk8kkn0")))

(define-public crate-bpack-0.1.1 (c (n "bpack") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qg6ifxfv3zqngazwzzqklzwb35dp37in60752y9zbxq6a245889")))

