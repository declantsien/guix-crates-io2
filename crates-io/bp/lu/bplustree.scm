(define-module (crates-io bp lu bplustree) #:use-module (crates-io))

(define-public crate-bplustree-0.1.0 (c (n "bplustree") (v "0.1.0") (d (list (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (f (quote ("const_generics"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g4m3w08ysnyhmxa93a5bpyq40dr6sm4mk4rc7ap0r6s4gh54vks")))

