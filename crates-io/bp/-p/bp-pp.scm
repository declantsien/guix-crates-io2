(define-module (crates-io bp -p bp-pp) #:use-module (crates-io))

(define-public crate-bp-pp-0.1.0 (c (n "bp-pp") (v "0.1.0") (d (list (d (n "k256") (r "^0.13.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "merlin") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0z65czjyhi6l1aq3qd3x6c0206ci60c04gpy5j6z2fa1zlj7dllj")))

(define-public crate-bp-pp-0.1.1 (c (n "bp-pp") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "k256") (r "^0.13.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "merlin") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1xgpf2z4s3p684hwvzlfsw1r2wjx5h0bgkdnfzq2v7psx4a7v7jj")))

