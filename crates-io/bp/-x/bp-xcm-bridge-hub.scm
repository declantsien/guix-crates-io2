(define-module (crates-io bp -x bp-xcm-bridge-hub) #:use-module (crates-io))

(define-public crate-bp-xcm-bridge-hub-0.0.0 (c (n "bp-xcm-bridge-hub") (v "0.0.0") (h "1icmzvzh8l25j6xggrpkd4dl46wfk87x5lccjxfwr7yi4jvi7b62")))

(define-public crate-bp-xcm-bridge-hub-0.1.0 (c (n "bp-xcm-bridge-hub") (v "0.1.0") (d (list (d (n "sp-std") (r "^13.0.0") (k 0)))) (h "19nl9m5wzcj097l4hxl7ypxlydpwi4va75c553i593gbgpal520i") (f (quote (("std" "sp-std/std") ("default" "std"))))))

(define-public crate-bp-xcm-bridge-hub-0.2.0 (c (n "bp-xcm-bridge-hub") (v "0.2.0") (d (list (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "0rzvaw3dnmq0pr67nmcsqg1b2jwgviy5kkr8rz7k0pj7klby0qv6") (f (quote (("std" "sp-std/std") ("default" "std"))))))

(define-public crate-bp-xcm-bridge-hub-0.3.0 (c (n "bp-xcm-bridge-hub") (v "0.3.0") (d (list (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "13sjgrapbyhiv2lc9jk5lzfwhzivzd4f9g1as7x7nfqxiy808a0r") (f (quote (("std" "sp-std/std") ("default" "std"))))))

