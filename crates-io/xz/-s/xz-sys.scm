(define-module (crates-io xz -s xz-sys) #:use-module (crates-io))

(define-public crate-xz-sys-0.0.1 (c (n "xz-sys") (v "0.0.1") (h "1h1bhk44nzcphpmnschg47q8vn20b9xj7npwax6ai547zsc9cg0q")))

(define-public crate-xz-sys-0.1.0 (c (n "xz-sys") (v "0.1.0") (d (list (d (n "lzma-sys") (r "^0.1") (d #t) (k 0)))) (h "0bcrrfx1kgzavwj6nyri2w9qq98zd1nmz8msj9rrvwjz2ybpax4f")))

