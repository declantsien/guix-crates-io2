(define-module (crates-io xz -e xz-embedded-sys) #:use-module (crates-io))

(define-public crate-xz-embedded-sys-0.1.1 (c (n "xz-embedded-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0185piq3gibj7xhk12lwxdhqh2vry659m2bqjchjs3flfn5vggfp")))

