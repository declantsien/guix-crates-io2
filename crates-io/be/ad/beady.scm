(define-module (crates-io be ad beady) #:use-module (crates-io))

(define-public crate-beady-0.0.1 (c (n "beady") (v "0.0.1") (h "1vgh65g9g1cgdg6fq12y3mqgp6b8qmrvx3qwarnihsc6fy6cg8w0")))

(define-public crate-beady-0.0.2 (c (n "beady") (v "0.0.2") (h "1zx77i6j5dxmzajhf2hknxypvc7iq6zj0vr7kcyf2w5xzfk730wq")))

(define-public crate-beady-0.1.0 (c (n "beady") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "0d2hr948kmf5g2w7nan1f64a8smrmjrv8cj1pnfs13ks3qzxiy4d")))

(define-public crate-beady-0.2.0 (c (n "beady") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "16gdkcjzghirlwygv4z14waq33rn7f0bx02f2q3c7z19sf3jnls3")))

(define-public crate-beady-0.2.1 (c (n "beady") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "120i4fw77334af8giwb7ra6shm7v3qyq71czdx7a66qg2sa22fjr")))

(define-public crate-beady-0.3.0 (c (n "beady") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "0k0csldxc01p9bga53r5ly4vq40wn65i8b8i8r6j8zgrgr36xw5g")))

(define-public crate-beady-0.4.0 (c (n "beady") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1iz8d2xn7iigw8x8jn8q9pvzffhhv7qw7bja3rgdxyy78p39wran")))

(define-public crate-beady-0.5.0 (c (n "beady") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1wkzbm7r2fpaschz7zhscsq0hgw55dc205zjp86shjjbbjyw7z85")))

(define-public crate-beady-0.6.0 (c (n "beady") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0jyvjc9lkj39yh0wy77brsl1blnjvh0zx4w2byvh6jl3cwfbmskk")))

(define-public crate-beady-0.6.1 (c (n "beady") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0kfd97n04bk3fk7lvffi8l0qnqr11gx17swhafahqlsnj8l1nyga")))

