(define-module (crates-io be ad bead) #:use-module (crates-io))

(define-public crate-bead-0.1.0 (c (n "bead") (v "0.1.0") (d (list (d (n "bead_core") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1f3slzfdr7zv96kzb32q4yn5fsg6lyv78hmhnyyyfpqfwx336jfy")))

