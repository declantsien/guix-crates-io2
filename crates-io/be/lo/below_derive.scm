(define-module (crates-io be lo below_derive) #:use-module (crates-io))

(define-public crate-below_derive-0.1.0 (c (n "below_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut" "fold" "extra-traits"))) (d #t) (k 0)))) (h "150n51illx7gazqjz9ga0nplpirg4n5v3n6xg0y0bnp8h7d54hsm")))

(define-public crate-below_derive-0.2.0 (c (n "below_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1cp3vymcl4k50527j93vhyn5snvpbz2dnsw4bmm8cjkjdgc97sii")))

(define-public crate-below_derive-0.3.0 (c (n "below_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1n2m39b0g5n0sa4hn21yjqkgw9gqvgpjmh78qvkzfgv4xj8mv5bx")))

(define-public crate-below_derive-0.4.0 (c (n "below_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0vs96all5h3hzg6ckdn4d35g8cz0lzfzgcjrdr78njgff1w3ilri")))

(define-public crate-below_derive-0.4.1 (c (n "below_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "08g4w44fp6w78hif4jl6cbc3p1v1ygcw0kdm2l1v9cnbwmnvx1z1")))

(define-public crate-below_derive-0.5.0 (c (n "below_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1qh41nh33lyqn4x2c9cri6850z8x2cp0niiihlgd0d747cj32n5p")))

(define-public crate-below_derive-0.6.1 (c (n "below_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "00i3h12h7nggnw5l97hfx87zi1b9gdbqxw6dmk6c1gb4d0cqrgzr")))

(define-public crate-below_derive-0.6.2 (c (n "below_derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1ifacri2r0rrpxs75vq8p3j6lx18l4dmqf4p7c4y9mrkn7mczbxi")))

(define-public crate-below_derive-0.6.3 (c (n "below_derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1wpydawh0y3aw5da0z5wpbd958f13nzgznfmcyq7hygvffjld721")))

(define-public crate-below_derive-0.7.0 (c (n "below_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "01rycmcs5i7ylpk5ifal8kc1nzc8z2brw1jxn0mz1ii1x2iwdvna")))

(define-public crate-below_derive-0.7.1 (c (n "below_derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.64") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "18g9z86mqniymkq5xi3hy0k8n4nh56agrslp20v9z3d11pcl1xhr")))

(define-public crate-below_derive-0.8.0 (c (n "below_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0fk8pwa68b2vlm89bnmwk4d0riv2fip39lz56lqa10xzfbgnvypa")))

(define-public crate-below_derive-0.8.1 (c (n "below_derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "18xnz8q2s69z6nvmrsc8racig6pgb4fcxxa4zg4ym9ncinvcmxcw")))

