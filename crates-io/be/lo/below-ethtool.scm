(define-module (crates-io be lo below-ethtool) #:use-module (crates-io))

(define-public crate-below-ethtool-0.8.0 (c (n "below-ethtool") (v "0.8.0") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1c0pwjbzx6ck7iq51x087578wg9x6r78xfybyid2dcvvzi2fpka8")))

(define-public crate-below-ethtool-0.8.1 (c (n "below-ethtool") (v "0.8.1") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "14yr3l3fqc4pzz88haw3hf7jhrfyv5gfw2mm2nbz2hcbfagaxhfn")))

