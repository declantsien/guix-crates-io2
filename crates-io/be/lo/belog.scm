(define-module (crates-io be lo belog) #:use-module (crates-io))

(define-public crate-belog-0.1.0 (c (n "belog") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "08xla0syymyqrjxyhl4znx2si57pfmb4kvgzc38r6j2ybjscrbmk")))

