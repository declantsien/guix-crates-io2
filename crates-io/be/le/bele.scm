(define-module (crates-io be le bele) #:use-module (crates-io))

(define-public crate-bele-0.0.2 (c (n "bele") (v "0.0.2") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)))) (h "10a7lznmfzxdi8yiq8kz6amqfjp5xgzhp59vi53ljbpwk3550bhc")))

(define-public crate-bele-0.0.3 (c (n "bele") (v "0.0.3") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)))) (h "1cv723h62z7grw32073krb3mfqcscf31gwj0mkfz0z9f6j7sdfid")))

(define-public crate-bele-0.0.4 (c (n "bele") (v "0.0.4") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0") (d #t) (k 0)))) (h "06qwcb15mw356gvlzsj8xaamr7l59g4dzlsh79xj3cngsjslr2bk")))

(define-public crate-bele-0.0.5 (c (n "bele") (v "0.0.5") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0") (d #t) (k 0)))) (h "08y3b2r5bi0pfa6yl92bx2zymixmd1ngf4lr4yfb5p8jim8iafi8")))

(define-public crate-bele-0.0.6 (c (n "bele") (v "0.0.6") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0") (d #t) (k 0)))) (h "130mj9ri1azwclc7sr3wf6fbs3dwaahg1hsrnp62wkjv4hzldvp9")))

(define-public crate-bele-0.1.0 (c (n "bele") (v "0.1.0") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0") (d #t) (k 0)))) (h "09l8qrbvx9byiqa5rqckyfa04lk1wahlvw9hsgdgywnfwvkg21ak")))

(define-public crate-bele-0.2.0 (c (n "bele") (v "0.2.0") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0") (d #t) (k 0)))) (h "0gc99w6dvndkqrnm2kf9k6mm371yczm9mpfr88b29gdwh6a60vc2")))

(define-public crate-bele-0.2.1 (c (n "bele") (v "0.2.1") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0") (d #t) (k 0)))) (h "04g05h15r5sgwp3q7c9pjmvcx2cqd3y4yisk3pknvx16yx8p6dwa")))

(define-public crate-bele-0.3.0 (c (n "bele") (v "0.3.0") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0") (d #t) (k 0)))) (h "0s5znbmxr6grln8dg46jf13i718hbc67bdcqv8930yvkx4yfdj1s")))

(define-public crate-bele-0.4.0 (c (n "bele") (v "0.4.0") (d (list (d (n "dia-go") (r "^0") (d #t) (k 0)) (d (n "kib") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0") (d #t) (k 0)))) (h "0h5ldcdsq5m8mxgvbjr6c24z4mxv5bzf1l5iqfygsc3w1813vl71")))

(define-public crate-bele-0.5.0 (c (n "bele") (v "0.5.0") (d (list (d (n "dia-args") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "kib") (r "^2") (o #t) (d #t) (k 0)))) (h "0pm032c1fw0i9w7j9ifkkajb0sqh3q4xna2i0iigc9j1lzmgipyi") (f (quote (("bin" "dia-args" "kib"))))))

(define-public crate-bele-0.6.0 (c (n "bele") (v "0.6.0") (d (list (d (n "dia-args") (r "^0.46") (o #t) (d #t) (k 0)) (d (n "kib") (r "^2") (o #t) (d #t) (k 0)))) (h "07vg243hgzxx5miz6bj94153s80sywdjhz3gcgn6kjwvwamqq635") (f (quote (("bin" "dia-args" "kib"))))))

(define-public crate-bele-0.7.0 (c (n "bele") (v "0.7.0") (d (list (d (n "dia-args") (r "^0.49") (o #t) (d #t) (k 0)) (d (n "kib") (r "^2") (o #t) (d #t) (k 0)))) (h "0vi6p3b3rl52h1v1k8wnf0wn9423bwzlk8mfvs1ym2lcikhgdk9q") (f (quote (("bin" "dia-args" "kib"))))))

(define-public crate-bele-0.8.0 (c (n "bele") (v "0.8.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "kib") (r "^2") (o #t) (d #t) (k 0)))) (h "1af2l1qz4xj1yn0fph1jfzw2s7552bmwzdfi0vnx2ybz0cs24xvy") (f (quote (("bin" "dia-args" "kib"))))))

(define-public crate-bele-0.9.0 (c (n "bele") (v "0.9.0") (d (list (d (n "dia-args") (r ">=0.59.3, <0.60") (o #t) (d #t) (k 0)) (d (n "kib") (r ">=7.0.1, <8") (o #t) (d #t) (k 0)))) (h "015yhr42cz7xnk6zx82czsj0ksla8hxax9q16a8ydm5bx0prrpsy") (f (quote (("bin" "dia-args" "kib"))))))

