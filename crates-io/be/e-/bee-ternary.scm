(define-module (crates-io be e- bee-ternary) #:use-module (crates-io))

(define-public crate-bee-ternary-0.1.0-alpha (c (n "bee-ternary") (v "0.1.0-alpha") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "125m4aw86dqqsxaiw3rinb8574babrab43pl5zq2yrx2fk3ndi4f") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.2.0-alpha (c (n "bee-ternary") (v "0.2.0-alpha") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1cib7n3ygp1iq6ngam7mvpilg5pp6d7d8a2crh5xh74fa1j6pfyc") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.3.0-alpha (c (n "bee-ternary") (v "0.3.0-alpha") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zzsp7dfcbws8vpvfq6xyfw9lkrvw5gwijsh8sil5fjmlrw2635f") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.3.1-alpha (c (n "bee-ternary") (v "0.3.1-alpha") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09p3x3myxpf2lz1dw6ca6cbjl7ak4bh7ck5zhg8vsxisd136y5lr") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.3.2-alpha (c (n "bee-ternary") (v "0.3.2-alpha") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0yircgjhx12w9jfgrwl7lqh822z9rh21ka3b1ks8mbhzdw8dcv13") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.3.4-alpha (c (n "bee-ternary") (v "0.3.4-alpha") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1mzhkwbcw5hikmivwl95nglrfjk7vdkjfpck1srm8m6cxy7jmyrl") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.4.0-alpha (c (n "bee-ternary") (v "0.4.0-alpha") (d (list (d (n "autocfg") (r "^1.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fzq0dvnady6xi5jzn52wwd9agm1d562bqwmanqcrpasag9adnii") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.4.1-alpha (c (n "bee-ternary") (v "0.4.1-alpha") (d (list (d (n "autocfg") (r "^1.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ybs7gd3vsiws9zjpxws7vk2m6nv7q8d5wfs009ryrh22ckzzwrc") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.4.2-alpha (c (n "bee-ternary") (v "0.4.2-alpha") (d (list (d (n "autocfg") (r "^1.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01ffa5dp2rkyf31jlgj3r66jccry1rpmmgs11kyinkyk6najbh3f") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.5.0 (c (n "bee-ternary") (v "0.5.0") (d (list (d (n "autocfg") (r "^1.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dl8fkph1qy7mjvk3cay5hfz3dzpgmbk75q9i5kb8mmdlcl15dja") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.5.1 (c (n "bee-ternary") (v "0.5.1") (d (list (d (n "autocfg") (r "^1.0.0") (k 1)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (k 0)))) (h "0irf3px7jsgw5d71i4fgy5f4mvmvij4ckiwq28hl7x5whqi1wcz3") (f (quote (("serde1" "serde"))))))

(define-public crate-bee-ternary-0.5.2 (c (n "bee-ternary") (v "0.5.2") (d (list (d (n "autocfg") (r "^1.0.0") (k 1)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (k 0)))) (h "0rns3mgsrn7linh7cwlp9rvp2xzplbk1dbvdsg5g76wfbiapk14f") (f (quote (("std") ("serde1" "serde"))))))

(define-public crate-bee-ternary-0.6.0 (c (n "bee-ternary") (v "0.6.0") (d (list (d (n "autocfg") (r "^1.1.0") (k 1)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (k 0)))) (h "088d4k99qsiy86mi058ha8x4701zbmv3qi7iyklk28p4d2d4ifdh") (f (quote (("std"))))))

(define-public crate-bee-ternary-1.0.0-alpha.1 (c (n "bee-ternary") (v "1.0.0-alpha.1") (d (list (d (n "autocfg") (r "^1.1.0") (k 1)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "serde") (r "^1.0.139") (o #t) (k 0)))) (h "1cl157dan2z1pmxs93zv02zfqd33qavdlvf0xyq67a6rvmjjn9d9") (f (quote (("std"))))))

(define-public crate-bee-ternary-1.0.0 (c (n "bee-ternary") (v "1.0.0") (d (list (d (n "autocfg") (r "^1.1.0") (k 1)) (d (n "hex") (r "^0.4.3") (f (quote ("std"))) (k 2)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("min_const_gen" "std" "std_rng"))) (k 2)) (d (n "serde") (r "^1.0.143") (o #t) (k 0)) (d (n "serde") (r "^1.0.137") (k 2)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 2)))) (h "1csmrdm61yvq2hjw7y3hn02ykhc7bp52cc8381b662sfj9n08fih") (f (quote (("std"))))))

