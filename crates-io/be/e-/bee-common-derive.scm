(define-module (crates-io be e- bee-common-derive) #:use-module (crates-io))

(define-public crate-bee-common-derive-0.1.0-alpha (c (n "bee-common-derive") (v "0.1.0-alpha") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "04hc0gb53br2f3hvp4ryjplryy869z8vmfwk7kzq5hpd9yrg5qmc")))

(define-public crate-bee-common-derive-0.1.1-alpha (c (n "bee-common-derive") (v "0.1.1-alpha") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1frsgsi2ym7i1cd61kw06jhrbqbff0w0a9mpfgpy13b7nf86xy99")))

