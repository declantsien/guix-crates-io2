(define-module (crates-io be e- bee-storage) #:use-module (crates-io))

(define-public crate-bee-storage-0.0.0 (c (n "bee-storage") (v "0.0.0") (h "1pd5h7q8wl9pnk5wa1ygsdb5j3p27scjgkhihv3lmracdgw6s3ha")))

(define-public crate-bee-storage-0.1.0-alpha (c (n "bee-storage") (v "0.1.0-alpha") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zpb9n3sd0f9ijcckz57n5dza12q0jihj7nsymb26kk8vxiqm391")))

(define-public crate-bee-storage-0.2.0-alpha (c (n "bee-storage") (v "0.2.0-alpha") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s1mc3nnb5p78lvzc2ycs8wqk3b5qx64sq3g2wlv85bh664bqkzf")))

(define-public crate-bee-storage-0.3.0 (c (n "bee-storage") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bee-common") (r "^0.4.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w78rs11dw2lrl908s1qfxn1samg5nyxz8kz92vyg21ww1lf5xjr")))

(define-public crate-bee-storage-0.4.0 (c (n "bee-storage") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bee-common") (r "^0.4.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f4xvhzwz4vkm0ln1fqxxfdv5ra3zfbs275j9pp81zf8vcsfpnfc")))

(define-public crate-bee-storage-0.5.0 (c (n "bee-storage") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bee-common") (r "^0.4.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s8866ajhyv119w0ig36a9ch4ipapi07gl46m4sx0kvv9h7g7add")))

(define-public crate-bee-storage-0.6.0 (c (n "bee-storage") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bee-common") (r "^0.4.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mzpqsbib0jghyrk5mbj4sp0pjsgp33vh73s0jng8dy8mcni5d4l")))

(define-public crate-bee-storage-0.7.0 (c (n "bee-storage") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bee-common") (r "^0.4.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14im7jmxnqcn1fh1r3la3kjs7vb3wb6bc3njbncjpxiw7k5hx8f0")))

(define-public crate-bee-storage-0.9.0 (c (n "bee-storage") (v "0.9.0") (d (list (d (n "bee-common") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xlpxkv9fwavh6vip08zdfgm1fn7xaiv0fh8jmhqvsxs77py1xzi")))

(define-public crate-bee-storage-0.8.0 (c (n "bee-storage") (v "0.8.0") (d (list (d (n "bee-common") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "118jz8312n6igwcqnkl7c6llli3d6yf423xz6blhrq9x8wx2blhn")))

(define-public crate-bee-storage-0.11.0 (c (n "bee-storage") (v "0.11.0") (d (list (d (n "bee-common") (r "^0.6.0") (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)))) (h "0ddsmb4i6qapqjiizjpsyn25h9dx13xnpnnj332fw39s3d4ndp70")))

(define-public crate-bee-storage-1.0.0-beta.1 (c (n "bee-storage") (v "1.0.0-beta.1") (d (list (d (n "packable") (r "^0.5.0") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (k 0)))) (h "1xb1l403aw1nmyp6d29r3bx0hwhac9c9wmclnzjllhmksqd7ljh3")))

(define-public crate-bee-storage-1.0.0 (c (n "bee-storage") (v "1.0.0") (d (list (d (n "packable") (r "^0.6.2") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.32") (k 0)))) (h "0hhif33vm5r1q0blg45air1xpagq9ng5f7qpcv6s0p84v5pv2pck")))

