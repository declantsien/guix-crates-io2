(define-module (crates-io be e- bee-runtime) #:use-module (crates-io))

(define-public crate-bee-runtime-0.1.0-alpha (c (n "bee-runtime") (v "0.1.0-alpha") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bee-storage") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0bqm922bdpfvfp1xlq058gnnp90662335iq3pv6qfd2c0zgdjvw3")))

(define-public crate-bee-runtime-0.1.1-alpha (c (n "bee-runtime") (v "0.1.1-alpha") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bee-storage") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "time" "macros"))) (d #t) (k 2)))) (h "11kn7d0cy616nqfkrfpcbn4xkwhk3b5i9j55zm5ky34ffdrqzs6b")))

(define-public crate-bee-runtime-1.0.0-beta.1 (c (n "bee-runtime") (v "1.0.0-beta.1") (d (list (d (n "async-trait") (r "^0.1.56") (k 0)) (d (n "bee-storage") (r "^1.0.0-beta.1") (k 0)) (d (n "dashmap") (r "^5.3.4") (k 0)) (d (n "futures") (r "^0.3.21") (f (quote ("std" "alloc"))) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("rt" "macros" "time"))) (k 2)))) (h "1m6dp27j8qlkil1xbg4iamh4ddj7v4rmapm6y3lycvxlbhg0hf5r")))

(define-public crate-bee-runtime-1.0.0 (c (n "bee-runtime") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.57") (k 0)) (d (n "bee-storage") (r "^1.0.0") (k 0)) (d (n "dashmap") (r "^5.3.4") (k 0)) (d (n "futures") (r "^0.3.21") (f (quote ("std" "alloc"))) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt" "macros" "time"))) (k 2)))) (h "0a5rl8nmfw0hjnw75yplq5397r1hsh7kmy1cc9h6d40dq3y4id6v")))

