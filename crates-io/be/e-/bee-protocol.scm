(define-module (crates-io be e- bee-protocol) #:use-module (crates-io))

(define-public crate-bee-protocol-0.0.0 (c (n "bee-protocol") (v "0.0.0") (h "0s4d9k2dyqikxa9s5x9kg39fp83bvk71sck21vcagw7m4rxr8abs")))

(define-public crate-bee-protocol-0.1.0 (c (n "bee-protocol") (v "0.1.0") (d (list (d (n "bee-message") (r "^0.1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "bee-network") (r "^0.1.0") (k 0)))) (h "1gq8cvjiihgk52w0rdph482zwf1n3nx0k692px98k99a5zgkbc97")))

(define-public crate-bee-protocol-0.1.1 (c (n "bee-protocol") (v "0.1.1") (d (list (d (n "bee-message") (r "^0.1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "bee-network") (r "^0.2.2") (d #t) (k 0)))) (h "15maif9qhxdhsndspr60v53x2mq4r4bz50608ilsh1ffh4rpdgv0")))

