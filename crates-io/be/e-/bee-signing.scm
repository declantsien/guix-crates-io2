(define-module (crates-io be e- bee-signing) #:use-module (crates-io))

(define-public crate-bee-signing-0.0.0 (c (n "bee-signing") (v "0.0.0") (h "10ba7zj6vsp5k17jqv9qv26ccs5b6r9j6jf44mzc3whcick7l1vr")))

(define-public crate-bee-signing-0.1.0-alpha (c (n "bee-signing") (v "0.1.0-alpha") (d (list (d (n "bee-common-derive") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "bee-crypto") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "bee-ternary") (r "^0.3.1-alpha") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0kzx66fww70s4gyh8fxv06r6z0wzgm2ijhkgkig9kbhm6mx2qizr")))

(define-public crate-bee-signing-0.1.1-alpha (c (n "bee-signing") (v "0.1.1-alpha") (d (list (d (n "bee-common-derive") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "bee-crypto") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "bee-ternary") (r "^0.3.1-alpha") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "1gfq9wc6r340mk293kmg6cg2w8fkj5mf0yl88hljjvcqi38pzy0m")))

(define-public crate-bee-signing-0.2.0 (c (n "bee-signing") (v "0.2.0") (d (list (d (n "bee-common-derive") (r "^0.1.1-alpha") (k 0)) (d (n "bee-crypto") (r "^0.3.0") (k 0)) (d (n "bee-ternary") (r "^0.5.2") (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("std" "std_rng"))) (k 0)) (d (n "sha3") (r "^0.9.1") (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)) (d (n "zeroize") (r "^1.4.2") (k 0)))) (h "13bxsnnrnq1azk38g0nlllzfvdpz36pwxr9hi4hpxpw8gm58kzk2")))

