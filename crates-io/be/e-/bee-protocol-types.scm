(define-module (crates-io be e- bee-protocol-types) #:use-module (crates-io))

(define-public crate-bee-protocol-types-1.0.0-beta.1 (c (n "bee-protocol-types") (v "1.0.0-beta.1") (d (list (d (n "bee-block") (r "^1.0.0-beta.2") (f (quote ("serde"))) (k 0)) (d (n "bee-gossip") (r "^1.0.0-beta.1") (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (k 0)))) (h "1yp6yglry9j0cy1qzx7gd5paz1hd9zg5ncc6b75sgnli48jbv2a9")))

(define-public crate-bee-protocol-types-1.0.0-beta.2 (c (n "bee-protocol-types") (v "1.0.0-beta.2") (d (list (d (n "bee-block") (r "^1.0.0-beta.3") (f (quote ("serde"))) (k 0)) (d (n "bee-gossip") (r "^1.0.0-beta.1") (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (k 0)))) (h "0n6nbmry2n202lw0bsbcr5lrbxc32bxi5p28f5k2zwhivcy6jcva")))

(define-public crate-bee-protocol-types-1.0.0-beta.3 (c (n "bee-protocol-types") (v "1.0.0-beta.3") (d (list (d (n "bee-block") (r "^1.0.0-beta.6") (f (quote ("serde"))) (k 0)) (d (n "bee-gossip") (r "^1.0.0-beta.2") (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (k 0)))) (h "1dzkc4sybbpl5ky6ajaz0f98abn4xivsyhbq32v76474z9xilqg6")))

(define-public crate-bee-protocol-types-1.0.0-beta.4 (c (n "bee-protocol-types") (v "1.0.0-beta.4") (d (list (d (n "bee-block") (r "^1.0.0-beta.7") (f (quote ("serde"))) (k 0)) (d (n "bee-gossip") (r "^1.0.0-beta.2") (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (k 0)))) (h "1vv2m79xh1wpdylns33rbyvld5d2gy7cf10cbiwz32ivh1xxwl3v")))

(define-public crate-bee-protocol-types-1.0.0 (c (n "bee-protocol-types") (v "1.0.0") (d (list (d (n "bee-block") (r "^1.0.0") (f (quote ("serde"))) (k 0)) (d (n "bee-gossip") (r "^1.0.0") (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (k 0)))) (h "12lq19k6gzskqvvv4320xamrwdafi9nlly078brsafnvkwwv9jy9")))

(define-public crate-bee-protocol-types-1.0.1 (c (n "bee-protocol-types") (v "1.0.1") (d (list (d (n "bee-block") (r "^1.0.1") (f (quote ("serde"))) (k 0)) (d (n "bee-gossip") (r "^1.0.0") (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (k 0)))) (h "0jqg48wgxkz5y2l3f8a90f704797xq19kagbiq6vpcq7g6awyswf")))

