(define-module (crates-io be e- bee-pow) #:use-module (crates-io))

(define-public crate-bee-pow-0.0.0 (c (n "bee-pow") (v "0.0.0") (h "0j7gxf5pc4w87hmgnisrzrw8vhyq383p8calja62sai5zab1rrkk")))

(define-public crate-bee-pow-0.1.0 (c (n "bee-pow") (v "0.1.0") (d (list (d (n "bee-crypto") (r "^0.2.1-alpha") (d #t) (k 0)) (d (n "bee-ternary") (r "^0.4.2-alpha") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.4.2") (f (quote ("blake2b" "digest"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pk7qxrjcc1442drrp7i50wb2w1lns6hb9ri04pm2h64rgmm4931")))

(define-public crate-bee-pow-0.2.0 (c (n "bee-pow") (v "0.2.0") (d (list (d (n "bee-ternary") (r "^0.5.2") (k 0)) (d (n "iota-crypto") (r "^0.9.1") (f (quote ("blake2b" "digest" "curl-p"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)))) (h "0vxz6znbzi6mdg3c3rckq5lmy5nzjc025a6ymv205n5nqvdna5jg")))

(define-public crate-bee-pow-0.3.0 (c (n "bee-pow") (v "0.3.0") (d (list (d (n "bee-ternary") (r "^0.6.0") (k 0)) (d (n "iota-crypto") (r "^0.9.2") (f (quote ("blake2b" "digest" "curl-p"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)) (d (n "trace-tools") (r "^0.3.0") (o #t) (k 0)) (d (n "tracing") (r "^0.1.29") (o #t) (k 0)))) (h "0n8wz90bwbxjfn2j1f1i07vllr94z7mx3fbx327cbc6cr3b6wv3i") (f (quote (("trace" "trace-tools/tokio-console" "tracing") ("default"))))))

(define-public crate-bee-pow-1.0.0-alpha.1 (c (n "bee-pow") (v "1.0.0-alpha.1") (d (list (d (n "bee-ternary") (r "^1.0.0-alpha.1") (k 0)) (d (n "iota-crypto") (r "^0.13.0") (f (quote ("blake2b" "digest" "curl-p"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (k 0)))) (h "1h8h89r13zv1l3xz8ymdh7mg9pl5jx0vhx7nqiqqd3y99mr6xqyq")))

(define-public crate-bee-pow-1.0.0-beta.1 (c (n "bee-pow") (v "1.0.0-beta.1") (d (list (d (n "bee-ternary") (r "^1.0.0-alpha.1") (k 0)) (d (n "iota-crypto") (r "^0.14.0") (f (quote ("blake2b" "digest" "curl-p"))) (k 0)) (d (n "thiserror") (r "^1.0.32") (k 0)))) (h "1w05qbkbygxpbwc6h8birpaxlfhhssx8iqdi4fvpjml9nxja0z7r")))

(define-public crate-bee-pow-1.0.0 (c (n "bee-pow") (v "1.0.0") (d (list (d (n "bee-ternary") (r "^1.0.0") (k 0)) (d (n "iota-crypto") (r "^0.14.3") (f (quote ("blake2b" "digest" "curl-p"))) (k 0)) (d (n "thiserror") (r "^1.0.32") (k 0)))) (h "19lqdv02028gxn6vi0jqz1pivg0lnwyw0cm4jcw62hfpa3jn633l")))

(define-public crate-bee-pow-0.2.1 (c (n "bee-pow") (v "0.2.1") (d (list (d (n "iota-crypto") (r "^0.17.1") (f (quote ("blake2b" "digest" "curl-p" "ternary_encoding"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)))) (h "0ppsmhzzl16w0s5magwz7n7vrild88qqvp5775xm64mp700vmzng")))

