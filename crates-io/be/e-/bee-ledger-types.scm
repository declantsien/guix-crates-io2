(define-module (crates-io be e- bee-ledger-types) #:use-module (crates-io))

(define-public crate-bee-ledger-types-1.0.0-beta.1 (c (n "bee-ledger-types") (v "1.0.0-beta.1") (d (list (d (n "bee-block") (r "^1.0.0-beta.1") (f (quote ("std"))) (k 0)) (d (n "packable") (r "^0.5.0") (f (quote ("serde" "io"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (k 0)))) (h "105yjinjn5w5wv0888mbh37h2p8pxd724nscd4xk6zjs62rp7p8q")))

(define-public crate-bee-ledger-types-1.0.0-beta.2 (c (n "bee-ledger-types") (v "1.0.0-beta.2") (d (list (d (n "bee-block") (r "^1.0.0-beta.2") (f (quote ("std"))) (k 0)) (d (n "packable") (r "^0.5.0") (f (quote ("serde" "io"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (k 0)))) (h "1f6pr1l9sxwb3apy0nwchznhh26mvb3rmyvp47p1nzvxk5xvlyqw") (f (quote (("rand" "bee-block/rand"))))))

(define-public crate-bee-ledger-types-1.0.0-beta.3 (c (n "bee-ledger-types") (v "1.0.0-beta.3") (d (list (d (n "bee-block") (r "^1.0.0-beta.3") (f (quote ("std"))) (k 0)) (d (n "packable") (r "^0.5.0") (f (quote ("serde" "io"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (k 0)))) (h "0hw3q1a2kv4s1na1isr1q44kxzl4vzz304rv4b0js2hj3l0925pp") (f (quote (("rand" "bee-block/rand"))))))

(define-public crate-bee-ledger-types-1.0.0-beta.4 (c (n "bee-ledger-types") (v "1.0.0-beta.4") (d (list (d (n "bee-block") (r "^1.0.0-beta.6") (f (quote ("std"))) (k 0)) (d (n "packable") (r "^0.5.0") (f (quote ("serde" "io"))) (k 0)) (d (n "thiserror") (r "^1.0.32") (k 0)))) (h "19b1f71yq336hjl3mg8j44js9sj030zchk6ra5sypv6am8f1kibs") (f (quote (("rand" "bee-block/rand"))))))

(define-public crate-bee-ledger-types-1.0.0-beta.5 (c (n "bee-ledger-types") (v "1.0.0-beta.5") (d (list (d (n "bee-block") (r "^1.0.0-beta.7") (f (quote ("std"))) (k 0)) (d (n "packable") (r "^0.6.1") (f (quote ("serde" "io"))) (k 0)) (d (n "thiserror") (r "^1.0.32") (k 0)))) (h "0qll998bcamyh3zl0jfy4jzvnybz5jxqhlj8kgwj7h54lxg4inhg") (f (quote (("rand" "bee-block/rand"))))))

(define-public crate-bee-ledger-types-1.0.0 (c (n "bee-ledger-types") (v "1.0.0") (d (list (d (n "bee-block") (r "^1.0.0") (f (quote ("std"))) (k 0)) (d (n "packable") (r "^0.6.2") (f (quote ("serde" "io"))) (k 0)) (d (n "thiserror") (r "^1.0.32") (k 0)))) (h "0nnh5w6lkgj90yv52qna2pzq8nlqn3zccdpywr1i4dxzqnwggf4b") (f (quote (("rand" "bee-block/rand"))))))

(define-public crate-bee-ledger-types-1.0.1 (c (n "bee-ledger-types") (v "1.0.1") (d (list (d (n "bee-block") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "packable") (r "^0.6.2") (f (quote ("serde" "io"))) (k 0)) (d (n "thiserror") (r "^1.0.32") (k 0)))) (h "0maj502l3nn3cnrrs4mxc36hfw0wn1pvz4xg8y4fym8g0gaf73i2") (f (quote (("rand" "bee-block/rand"))))))

