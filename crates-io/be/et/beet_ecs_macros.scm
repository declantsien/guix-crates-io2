(define-module (crates-io be et beet_ecs_macros) #:use-module (crates-io))

(define-public crate-beet_ecs_macros-0.0.1 (c (n "beet_ecs_macros") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "extend") (r "^1.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1p1kj48dgscdipygifgpd7brsq0f0f9yl559xms0dqdy3flwq7nc")))

