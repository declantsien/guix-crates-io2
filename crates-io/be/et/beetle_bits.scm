(define-module (crates-io be et beetle_bits) #:use-module (crates-io))

(define-public crate-beetle_bits-0.1.0 (c (n "beetle_bits") (v "0.1.0") (h "1d4crgfqrjcq3h9k8vkm7bamijkbd2dsjpwrxp1ggb7l2lbl43v5")))

(define-public crate-beetle_bits-0.2.0 (c (n "beetle_bits") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0wjs6qfw22p5c0byzhksyzf817l3jyi7n4h0s0d9dyhifhjhvgmk")))

(define-public crate-beetle_bits-0.2.1 (c (n "beetle_bits") (v "0.2.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1h5y5gyav9vkr0y8rh725hkzslyliw2q5ry7sjvaway5xs5sdqc0")))

(define-public crate-beetle_bits-0.2.2 (c (n "beetle_bits") (v "0.2.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "150k74q2jcap5820q1jlddg19sbk2i3xb2p3kmnkbkljw2klkqr5")))

(define-public crate-beetle_bits-0.2.3 (c (n "beetle_bits") (v "0.2.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1ja6hm893hdw7098k33fikz6m5mbpidhxcb5zgp9ypviz877vrjb")))

(define-public crate-beetle_bits-0.2.4 (c (n "beetle_bits") (v "0.2.4") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0svi34anm5c8ciqqfjs8chf3x725p9l5jkslh70s6j2wg0l3iqpw")))

(define-public crate-beetle_bits-0.2.5 (c (n "beetle_bits") (v "0.2.5") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1f79wwd2903hrjxivbfyd7kfbn4ypr1waahzfhqs88gkkq9x80w0")))

