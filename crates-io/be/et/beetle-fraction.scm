(define-module (crates-io be et beetle-fraction) #:use-module (crates-io))

(define-public crate-beetle-fraction-0.1.0 (c (n "beetle-fraction") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1i1c7jkaaaf2a0979k07p3jmy0pf9gx79n99qbajf1my1k1hdgsp")))

(define-public crate-beetle-fraction-0.2.0 (c (n "beetle-fraction") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03fl7iva9fjv9ly0ms531g5n0vjqprbmsi2j4xqzy4rbrl6h9zc0")))

(define-public crate-beetle-fraction-0.2.1 (c (n "beetle-fraction") (v "0.2.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03fvzfjdmjp9pgivdnydf0a7k07z5gkh1dqm7dambxv67n2hmv1q")))

(define-public crate-beetle-fraction-0.2.2 (c (n "beetle-fraction") (v "0.2.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0qwh0zbj1l5q7rx0ah3v8mhfng8pd13kwg5nvx41164bqfv1w37f")))

(define-public crate-beetle-fraction-0.2.3 (c (n "beetle-fraction") (v "0.2.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0pv7grmkdf23z7c4m59g4djq21j787zzarabk6rv1f4pdpzgqqv8")))

(define-public crate-beetle-fraction-0.2.4 (c (n "beetle-fraction") (v "0.2.4") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0k4vg57k3pqgzdwbql5ljqf2drlkpgz5s0nkasz5yx2m8bkfabjs")))

(define-public crate-beetle-fraction-0.3.0 (c (n "beetle-fraction") (v "0.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "07zffzx294sn7iwyishql8rmhbbybamgg9966n3j72lqm57pv2cg")))

(define-public crate-beetle-fraction-0.3.1 (c (n "beetle-fraction") (v "0.3.1") (d (list (d (n "digitize") (r "^0.1.0") (d #t) (k 0)) (d (n "floating-cat") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)))) (h "1wxj57n8jzfd4r3cgsqrxig74kygxy0ksjjkzn7bw1ir72awi7zs")))

(define-public crate-beetle-fraction-0.3.3 (c (n "beetle-fraction") (v "0.3.3") (d (list (d (n "digitize") (r "^0.1.0") (d #t) (k 0)) (d (n "floating-cat") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)))) (h "03wmbg1qwngcrhdbhhl42c71kkx5ni6n148i1g4r5ljzhnxxc165")))

(define-public crate-beetle-fraction-0.3.4 (c (n "beetle-fraction") (v "0.3.4") (d (list (d (n "digitize") (r "^0.1.0") (d #t) (k 0)) (d (n "floating-cat") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)))) (h "1vflgz6w55kl659nnd6d14s19nvvza1f812l317aqwp30djkjija")))

