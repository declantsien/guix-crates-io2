(define-module (crates-io be et beetle-nonzero) #:use-module (crates-io))

(define-public crate-beetle-nonzero-0.1.0 (c (n "beetle-nonzero") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1ddyw9rjf89byd82wji8jyjwzlpyrp18kvaks7caiqqa1sg0h9kv")))

(define-public crate-beetle-nonzero-0.2.0 (c (n "beetle-nonzero") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0v8y2a97dimibyf8mihp006jg4kymwg0ji7cd6i3gdwmpm6k8qvk")))

(define-public crate-beetle-nonzero-0.2.1 (c (n "beetle-nonzero") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0jb03ass49jq5523lgxysyrnm5jz2kl67n036ac0kinqs0p8dl3p")))

(define-public crate-beetle-nonzero-0.2.2 (c (n "beetle-nonzero") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "06wvwmdaj7yp646mh1cvm1jv9m6jbjycs5llqnyj2b3k3shq690k")))

(define-public crate-beetle-nonzero-0.2.3 (c (n "beetle-nonzero") (v "0.2.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1g0wlr3mxam434plaw8s4x9r1vj13n7m7nw6qy794wcfwqx24ckb")))

(define-public crate-beetle-nonzero-0.2.4 (c (n "beetle-nonzero") (v "0.2.4") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1b79qa985lj94sspbx6x4v71qclc6ykpbawjyd3pws2fap087vyw")))

(define-public crate-beetle-nonzero-0.2.5 (c (n "beetle-nonzero") (v "0.2.5") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1kcw0kvib8znir2nrn44m4yw17smvyab5i4mmzmlhcn0xn5kwmdl")))

(define-public crate-beetle-nonzero-0.2.6 (c (n "beetle-nonzero") (v "0.2.6") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0f0c5kb02wji570l9dx6kni1gbdaj9hcziiiy7r7qyvin2q5jk7w")))

(define-public crate-beetle-nonzero-0.2.7 (c (n "beetle-nonzero") (v "0.2.7") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0ancvvgfzl0l1bibvm4c39h1b3724a5v7cf8wvdbgdan4hl1g6r3")))

(define-public crate-beetle-nonzero-0.2.8 (c (n "beetle-nonzero") (v "0.2.8") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1wy8d10slylbkq90gv8nbw7yvz178f7s3iikx9j4h9kmzkb445mi")))

(define-public crate-beetle-nonzero-0.2.9 (c (n "beetle-nonzero") (v "0.2.9") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "04cd874gvvllw1bi8b5668ab8ylk15278yzjfzalpc9p2rw65pm9")))

(define-public crate-beetle-nonzero-0.3.0 (c (n "beetle-nonzero") (v "0.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0l7h1vrf0v9yn5a5qylf7cd4k1x5ci1wpvl43s0fswksdj5c841d")))

(define-public crate-beetle-nonzero-0.3.1 (c (n "beetle-nonzero") (v "0.3.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1jxhagwykwbnkxmqspx1s3libg8jw6qcsp1sh9fv1q1l94f0dxi5")))

(define-public crate-beetle-nonzero-0.3.2 (c (n "beetle-nonzero") (v "0.3.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0czgx94njygc4cbj4n9a4jx4zs98np9sd7l2aj9fjfa2iprxw38s")))

(define-public crate-beetle-nonzero-0.3.3 (c (n "beetle-nonzero") (v "0.3.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "030gix6awy2lzbzd7mqvs1rc5bynnl6hf0rfi1c8nynqxlgrh4f6")))

(define-public crate-beetle-nonzero-0.3.4 (c (n "beetle-nonzero") (v "0.3.4") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1yghfaaa58wp007a3pz6pj0hs1g156n0yxjgnrsqx4n09ym0lk7n")))

(define-public crate-beetle-nonzero-0.3.5 (c (n "beetle-nonzero") (v "0.3.5") (d (list (d (n "default-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1m8qy16p4h4fip7r18wgl1zxb3iffbcnknckw3x6awvvj08lwhir")))

(define-public crate-beetle-nonzero-0.3.6 (c (n "beetle-nonzero") (v "0.3.6") (d (list (d (n "default-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "parity-map") (r "^0.1.0") (d #t) (k 0)))) (h "02i81wvd030763706qpgsvv33m08rjxzb85in3q9mk91b6pcvxp2")))

(define-public crate-beetle-nonzero-0.3.7 (c (n "beetle-nonzero") (v "0.3.7") (d (list (d (n "default-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "071risrz40d57j6ihs4m86cmhk3afqfbwprndgmfpkm523cv6rsb")))

(define-public crate-beetle-nonzero-0.3.8 (c (n "beetle-nonzero") (v "0.3.8") (d (list (d (n "default-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0h370cf7i8v5l34smxksxgf01b5j3kyab1ga7lhwlhagl7j1sxig")))

(define-public crate-beetle-nonzero-0.3.9 (c (n "beetle-nonzero") (v "0.3.9") (d (list (d (n "default-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "03acm2cmspifily7k3f7cfwab3ndkwk6yzdbgkx4hgg5rw1gpfkj")))

