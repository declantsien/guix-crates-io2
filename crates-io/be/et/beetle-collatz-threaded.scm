(define-module (crates-io be et beetle-collatz-threaded) #:use-module (crates-io))

(define-public crate-beetle-collatz-threaded-0.1.0 (c (n "beetle-collatz-threaded") (v "0.1.0") (d (list (d (n "beetle-collatz") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "098mpkhbivgmma3cjgcr136v556gfz6m300xnxshyk1jwsaa5gs6")))

(define-public crate-beetle-collatz-threaded-0.1.1 (c (n "beetle-collatz-threaded") (v "0.1.1") (d (list (d (n "beetle-collatz") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "1jcz47nssqyfdwn607ccxc2bxkv71m2v3vncy84gpy7h09p210wp")))

(define-public crate-beetle-collatz-threaded-0.1.2 (c (n "beetle-collatz-threaded") (v "0.1.2") (d (list (d (n "beetle-collatz") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "179hnww11y2qx1d2dv8ay8c4zsqgf69a7qwxc8r1bwb1ia99nbd6")))

(define-public crate-beetle-collatz-threaded-0.1.4 (c (n "beetle-collatz-threaded") (v "0.1.4") (d (list (d (n "beetle-collatz") (r "^0.1.4") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "1jq963jcdb50p54f865c6c8sgxd97h027fjkca2fnlzigf283hwf")))

(define-public crate-beetle-collatz-threaded-0.1.5 (c (n "beetle-collatz-threaded") (v "0.1.5") (d (list (d (n "beetle-collatz") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "12bbp86l2c78x4h6a17sdbg25nf1wap7z21ivypqjwzmap285nv8")))

(define-public crate-beetle-collatz-threaded-0.2.0 (c (n "beetle-collatz-threaded") (v "0.2.0") (d (list (d (n "beetle-collatz") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "1sdq74vwa0r8sylhcm46w19j24jb16qhnjd7rmksbw7qr64djd6k")))

(define-public crate-beetle-collatz-threaded-0.2.1 (c (n "beetle-collatz-threaded") (v "0.2.1") (d (list (d (n "beetle-collatz") (r "^0.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "0cqkyz9ijg5xn7lw7gviv4i1101k0lsn8n3mrljl3y85hskma91z")))

(define-public crate-beetle-collatz-threaded-0.2.2 (c (n "beetle-collatz-threaded") (v "0.2.2") (d (list (d (n "beetle-collatz") (r "^0.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "0l6gm7ll8pqf35893vq616vzk24504qw051galj0y5i8ly3dnd9s")))

(define-public crate-beetle-collatz-threaded-0.2.3 (c (n "beetle-collatz-threaded") (v "0.2.3") (d (list (d (n "beetle-collatz") (r "^0.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "1vllhfq3sqrvbsbwv341wl8vckz2qc8qa8x7bmrwj7cb5f0m1npg")))

