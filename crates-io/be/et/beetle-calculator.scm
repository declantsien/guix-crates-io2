(define-module (crates-io be et beetle-calculator) #:use-module (crates-io))

(define-public crate-beetle-calculator-0.1.0 (c (n "beetle-calculator") (v "0.1.0") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)))) (h "0jzrvj5c4kpbqp4pn8lzg37pz84hw63gxjf517c65vv5ix84ll3l")))

(define-public crate-beetle-calculator-0.1.1 (c (n "beetle-calculator") (v "0.1.1") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)))) (h "102p4988ixywmna5bpg99mvxqhw8n7rlbjyar1v7fr310xr4i1mn")))

(define-public crate-beetle-calculator-0.1.2 (c (n "beetle-calculator") (v "0.1.2") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)))) (h "09d4ry3acfbi8zmnfh9s76510zfr3igba1kf7yiw7b8wrhy8xhdc")))

(define-public crate-beetle-calculator-0.1.3 (c (n "beetle-calculator") (v "0.1.3") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)))) (h "1n4v625ab3hsy7zirw5qv1ya27gll8fr1jfbqlpxgr42gc76gl78")))

(define-public crate-beetle-calculator-0.2.0 (c (n "beetle-calculator") (v "0.2.0") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)))) (h "1ja9qa3x3bm72aknzdqng58z7rslwkly4x67waycmkckm92737yj")))

(define-public crate-beetle-calculator-0.2.1 (c (n "beetle-calculator") (v "0.2.1") (d (list (d (n "inquire") (r "^0.6.2") (d #t) (k 0)))) (h "1wzlyh5sjkrp2gwa6kvardg6d17hlmph5v5v2j2bji8614vccdmc")))

