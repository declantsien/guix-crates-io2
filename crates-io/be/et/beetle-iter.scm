(define-module (crates-io be et beetle-iter) #:use-module (crates-io))

(define-public crate-beetle-iter-0.1.0 (c (n "beetle-iter") (v "0.1.0") (h "1r9mqcaf43aqrrgljn1hzg1bhykn8x8p7v6y6mzgschsx3zncn2d")))

(define-public crate-beetle-iter-0.1.1 (c (n "beetle-iter") (v "0.1.1") (h "12z7m6dv4r9ksb8bfjwgngnpxll413aj001kk3swqzdy11r6n8i8")))

