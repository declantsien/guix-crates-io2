(define-module (crates-io be et beetle-collatz) #:use-module (crates-io))

(define-public crate-beetle-collatz-0.1.0 (c (n "beetle-collatz") (v "0.1.0") (h "154k9i77mk4jjr7j5app1rnp4qac8jry9zcqz2nmil8z77k5x57l")))

(define-public crate-beetle-collatz-0.1.1 (c (n "beetle-collatz") (v "0.1.1") (h "15f6h5c02zrfy543czk9r2mi980d269f1b68q1sav77kqfn987rr")))

(define-public crate-beetle-collatz-0.1.2 (c (n "beetle-collatz") (v "0.1.2") (h "05bh4m2bapqzm1mpvfyal96znypb9ghxm391wwkyn9dnyb8z1lyf")))

(define-public crate-beetle-collatz-0.1.3 (c (n "beetle-collatz") (v "0.1.3") (h "1i6avjn5r67inl3ghgpgnbrlm9d5j85ihkc126h4b9naby84jkmz")))

(define-public crate-beetle-collatz-0.1.4 (c (n "beetle-collatz") (v "0.1.4") (h "1facf62vx2s44838psnvcmbspx533xhkn3wzd61fr9apzizz2zp8")))

(define-public crate-beetle-collatz-0.1.5 (c (n "beetle-collatz") (v "0.1.5") (h "0pnyywz3w0hrzk9zb704g6hx16ly29srnrz3ypshckxqcl29gvmp")))

(define-public crate-beetle-collatz-0.1.6 (c (n "beetle-collatz") (v "0.1.6") (h "1fr1x8lrdmzig6hbj3xsg4l5cb25cvyfg0wgvzmia4qj791mlaj9")))

(define-public crate-beetle-collatz-0.1.7 (c (n "beetle-collatz") (v "0.1.7") (h "1pvcman585qr9z8fx7xfmlha314riylnmb0hrmk8753s39kgv1cl")))

(define-public crate-beetle-collatz-0.2.0 (c (n "beetle-collatz") (v "0.2.0") (h "0lrjqcsq0sqzxy3pn4ps3xqdwdfighah89v2afwqqp5wr6dnlf0z")))

(define-public crate-beetle-collatz-0.2.1 (c (n "beetle-collatz") (v "0.2.1") (h "0wlvis96bmxifirmgn2gd1h659694hh5lxh88bbphc861z1m90sx")))

(define-public crate-beetle-collatz-0.3.0 (c (n "beetle-collatz") (v "0.3.0") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "097k4qn8pszhw04zn2p24ajqdvrafwkz0fcij8cxlvvfizy3yrsb") (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.3.1 (c (n "beetle-collatz") (v "0.3.1") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "1jw7fpy1jwib5dhcb9mlv4w5q6mid3s3ljipq81k0jlr89d9s006") (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.3.2 (c (n "beetle-collatz") (v "0.3.2") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "04ms8g4qnaw9l496v8jwqk135fqn1rzs6qa0mp2k18bzyz5b939p") (f (quote (("default")))) (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.3.3 (c (n "beetle-collatz") (v "0.3.3") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "0wb93qbwh6hqn7x721xg3f5spz7nl5wmpipsvp707amxab6mczah") (f (quote (("default")))) (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.3.4 (c (n "beetle-collatz") (v "0.3.4") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "0ffgvgrnp3rsiiv7xkhzw15gcbxrd29vq3kjks9zs5xgch6lwdx2") (f (quote (("default")))) (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.3.5 (c (n "beetle-collatz") (v "0.3.5") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "0k0axxnb2cxpjqzm9l3azkwywfwl7mkv8lv4kc6nkhc11d37lp4w") (f (quote (("default")))) (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.3.6 (c (n "beetle-collatz") (v "0.3.6") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "0gkw7faay0kgkrpg10gmggb2sfb8bz3vqcz92zi04xd5112cmfwv") (f (quote (("default")))) (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.3.7 (c (n "beetle-collatz") (v "0.3.7") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "1hnrbh80y8i4vwqx1b5fd0vr4j6vp9vghxfkkrl1jbp4m25z4nbq") (f (quote (("default")))) (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.3.8 (c (n "beetle-collatz") (v "0.3.8") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "1k5bhp2mhx17rqcdpzxmlqjr96lrin03h4dvcnkhia7ryyypw7q7") (f (quote (("default")))) (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.3.9 (c (n "beetle-collatz") (v "0.3.9") (d (list (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "0ak89z33q5pb10dba7d8lxpbdrjimzwibcwprkb34xa2vwaj62n5") (f (quote (("default")))) (s 2) (e (quote (("threaded" "dep:rayon"))))))

(define-public crate-beetle-collatz-0.4.0 (c (n "beetle-collatz") (v "0.4.0") (d (list (d (n "no-panic") (r "^0.1.20") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "151hrddi9m0ms2952qd7drgs8rjjsg9hap654gh7f2dy32b46wad") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.4.21 (c (n "beetle-collatz") (v "0.4.21") (d (list (d (n "beetle-nonzero") (r "^0.1.0") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.20") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0z7y3dx65l6zflinnfbrj6sgkrf0y8x25ygdwsnl2xsaq92bzy1a") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.4.22 (c (n "beetle-collatz") (v "0.4.22") (d (list (d (n "beetle-nonzero") (r "^0.1.0") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.20") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0brg0lnr47kj56babgzxpb0g07h7bb0nj7cicias7ynqz57fa2a8") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.4.5 (c (n "beetle-collatz") (v "0.4.5") (d (list (d (n "beetle-nonzero") (r "^0.2.3") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.20") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "15mgi5j7yqm26zjhiq3bzf2j5anhag8d3r7607x1ci1355k8ng8y") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.0 (c (n "beetle-collatz") (v "0.5.0") (d (list (d (n "beetle-nonzero") (r "^0.2.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "08srbkjms458ijzl3wskkxxqmwnaqsrckf95fkw1ih01iscsdb5k") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.1 (c (n "beetle-collatz") (v "0.5.1") (d (list (d (n "beetle-nonzero") (r "^0.2.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0bflvp8x33v7m85p4kgc94r0w3ljc31gy02qmqz0087g4vhbzx1k") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.2 (c (n "beetle-collatz") (v "0.5.2") (d (list (d (n "beetle-nonzero") (r "^0.2.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1yzcmva4dgjf3dsxvsscybzpyv159g26a1bf8gybvx6lr3kikbm9") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.3 (c (n "beetle-collatz") (v "0.5.3") (d (list (d (n "beetle-nonzero") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0hsiqdca4qz5672f38zha4rylcw38d564yyv9fmlrqam90i75jnz") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.4 (c (n "beetle-collatz") (v "0.5.4") (d (list (d (n "beetle-nonzero") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1hvlf9aswpppdlznigr28zcf3sczc7jn7a073bkrrzaapjpgdw0i") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.5 (c (n "beetle-collatz") (v "0.5.5") (d (list (d (n "beetle-nonzero") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1cf6zi613gsbf2cqdn3adzd9m6w7dks6wm1a25bg5grdyn1zws8j") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.6 (c (n "beetle-collatz") (v "0.5.6") (d (list (d (n "beetle-nonzero") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0b4dgnxnwxvxlwhp8qw5wk01xwn7w7rx1f209ws89y2vis6rqbf8") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.7 (c (n "beetle-collatz") (v "0.5.7") (d (list (d (n "beetle-nonzero") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "12zlcq79cn6bq310ni3sbnh3xnahyvp573kha0hqinamxa8f6dfy") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.8 (c (n "beetle-collatz") (v "0.5.8") (d (list (d (n "beetle-nonzero") (r "^0.2.9") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "17i72hp3vgxpyya49p2h1rf7f7hz156bx99r9lk60b6afyfs1j8d") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.9 (c (n "beetle-collatz") (v "0.5.9") (d (list (d (n "beetle-nonzero") (r "^0.3.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "03n91pahzr8y10yydn1nf40sqysbfq8fjv581rlirgaqm57m7brl") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.10 (c (n "beetle-collatz") (v "0.5.10") (d (list (d (n "beetle-nonzero") (r "^0.3.3") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1k7w6fkqxvbnbmd6v8pmsjkw3bvv92ligxg1s9m6lziwjn34xfbz") (f (quote (("default"))))))

(define-public crate-beetle-collatz-0.5.11 (c (n "beetle-collatz") (v "0.5.11") (d (list (d (n "beetle-nonzero") (r "^0.3.9") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0q09f58kh8sr6s4wnni7m3zdx027nm4nnmxyf2d49jfkfykrqx9h")))

