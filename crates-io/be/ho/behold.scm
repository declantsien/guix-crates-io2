(define-module (crates-io be ho behold) #:use-module (crates-io))

(define-public crate-behold-0.1.0 (c (n "behold") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "0r56anh9gxcrfdh009jics34mhfgvd8hbmfyy3ykpm4cchg73zm9") (f (quote (("unstable"))))))

(define-public crate-behold-0.1.1 (c (n "behold") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "0wcrin6vzff1wsln5ld3x5dyr6m07ycrjy2fp7f2zg3jc4y18g9j") (f (quote (("unstable"))))))

(define-public crate-behold-0.1.2 (c (n "behold") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "06fgskzmn9b8ksaaj42bws2dl0sjh01nbcnqnf1vb7rlfzd6h0k5") (f (quote (("unstable"))))))

(define-public crate-behold-0.1.3 (c (n "behold") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "1v94lnvxvmhsf1611lnsal1gpbn3r4rlz5azpg9w19n9grb05fi4") (f (quote (("unstable"))))))

