(define-module (crates-io be eh beehive) #:use-module (crates-io))

(define-public crate-beehive-0.1.0 (c (n "beehive") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0hl4yn29qx3jm7936xql0y1c4ggdzm6hck8arg9zh1agg7xwb8nh") (f (quote (("serde-1" "serde") ("rand-07" "rand") ("default" "serde-1" "collections") ("collections" "thiserror"))))))

(define-public crate-beehive-0.1.1 (c (n "beehive") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dr3gqfd7xqakfki4qdcxlifv9l8w0kbdxix3kn1l7v9zv60jv0n") (f (quote (("serde-1" "serde") ("rand-07" "rand") ("default" "serde-1" "collections") ("collections" "thiserror"))))))

