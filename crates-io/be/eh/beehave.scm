(define-module (crates-io be eh beehave) #:use-module (crates-io))

(define-public crate-beehave-0.0.1 (c (n "beehave") (v "0.0.1") (h "0haj3mrcdw5dhqqww1ksg5lhqa03wspzzmkr66fyyfi9ks0lwn15")))

(define-public crate-beehave-0.0.2 (c (n "beehave") (v "0.0.2") (h "022yqixh1cfw1q3b315dcbk7zvlnlxjzzian1b9da1dzygxq9als")))

(define-public crate-beehave-0.0.3 (c (n "beehave") (v "0.0.3") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "1yiqapgd4m8mmzlqsb20vsa4x5ccagdx68s26qv6pc26c04dhxih")))

(define-public crate-beehave-0.0.4 (c (n "beehave") (v "0.0.4") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1vv49izgcrvijhnzrhawvp6pbhapmkl52fxhim3axgk26vi2kqdj")))

