(define-module (crates-io be si besida) #:use-module (crates-io))

(define-public crate-besida-0.1.0 (c (n "besida") (v "0.1.0") (h "1r9hhj5gn4gxkbrlknn4by6rq0ww833hgxhx9w4ryskw0fzb55lk")))

(define-public crate-besida-0.1.1 (c (n "besida") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1ihsk7ayli0qak8s5cbq69xjdjpz9sxzki50sn9zzy42k9x7yjmk")))

