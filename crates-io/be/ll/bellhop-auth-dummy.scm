(define-module (crates-io be ll bellhop-auth-dummy) #:use-module (crates-io))

(define-public crate-bellhop-auth-dummy-0.2.0 (c (n "bellhop-auth-dummy") (v "0.2.0") (d (list (d (n "bellhop") (r "^0.2.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.0") (f (quote ("handlebars_templates"))) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "01rp4jz2ifaapimsi3f7c7xvfqkw0b523dx22fbcfqx6n4n4yvgm")))

(define-public crate-bellhop-auth-dummy-0.2.1 (c (n "bellhop-auth-dummy") (v "0.2.1") (d (list (d (n "bellhop") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.0") (f (quote ("handlebars_templates"))) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "09zjmzvjr42vjjnclaj84jm0sfhxsp787jf8zvs8l3nxqczsyw8q")))

