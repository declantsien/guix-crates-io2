(define-module (crates-io be ll bellperson-emulated) #:use-module (crates-io))

(define-public crate-bellperson-emulated-0.1.0 (c (n "bellperson-emulated") (v "0.1.0") (d (list (d (n "bellperson") (r "^0.25.0") (k 0)) (d (n "ff") (r "^0.13.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "pasta_curves") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1b0cm5yv3d7lmplq6jbvkzzbjh4kladzwl3w868lqpj1wj24qli0")))

(define-public crate-bellperson-emulated-0.2.0 (c (n "bellperson-emulated") (v "0.2.0") (d (list (d (n "bellperson") (r "^0.25.0") (k 0)) (d (n "ff") (r "^0.13.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "pasta_curves") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ryzrhqf1m78gq5m1bi3yrm4hxzjz25gbv430mk6xihzsdzv8gbp")))

