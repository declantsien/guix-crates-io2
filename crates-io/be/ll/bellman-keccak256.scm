(define-module (crates-io be ll bellman-keccak256) #:use-module (crates-io))

(define-public crate-bellman-keccak256-0.1.0 (c (n "bellman-keccak256") (v "0.1.0") (d (list (d (n "bellman") (r "^0.2.0") (k 0)) (d (n "bitvec") (r "^0.17.4") (d #t) (k 2)) (d (n "ff") (r "^0.5.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 2)) (d (n "pairing") (r "^0.15.0") (d #t) (k 2)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak" "sha3"))) (d #t) (k 2)))) (h "0mpf804lb7mq06rn9gdlcghnzk7amifcm0s4xspmrzjzcmdyfl4k")))

