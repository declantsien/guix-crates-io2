(define-module (crates-io be ll bellhop-hook-jenkins) #:use-module (crates-io))

(define-public crate-bellhop-hook-jenkins-0.2.0 (c (n "bellhop-hook-jenkins") (v "0.2.0") (d (list (d (n "bellhop") (r "^0.2.0") (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "0zli23fcfbcdpaysjnpmxssd30mz99g9a4l0v271kl4q2vl4z32s")))

(define-public crate-bellhop-hook-jenkins-0.2.1 (c (n "bellhop-hook-jenkins") (v "0.2.1") (d (list (d (n "bellhop") (r "^0.2.1") (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "06s1cikf6fyc9c6z7iy4wk4dyi8j1793c62qrxv04vwpjpv87sxs")))

