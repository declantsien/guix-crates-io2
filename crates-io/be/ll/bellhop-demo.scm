(define-module (crates-io be ll bellhop-demo) #:use-module (crates-io))

(define-public crate-bellhop-demo-0.2.0 (c (n "bellhop-demo") (v "0.2.0") (d (list (d (n "bellhop") (r "^0.2.0") (d #t) (k 0)) (d (n "bellhop-auth-dummy") (r "^0.2.0") (d #t) (k 0)) (d (n "bellhop-auth-header") (r "^0.2.0") (d #t) (k 0)) (d (n "bellhop-hook-jenkins") (r "^0.2.0") (d #t) (k 0)))) (h "0b5dbakd0ql02prhmxxjzhivgpgf1n4qvm2v9f92l6jrdxf40d7f")))

(define-public crate-bellhop-demo-0.2.1 (c (n "bellhop-demo") (v "0.2.1") (d (list (d (n "bellhop") (r "^0.2.1") (d #t) (k 0)) (d (n "bellhop-auth-dummy") (r "^0.2.1") (d #t) (k 0)) (d (n "bellhop-auth-header") (r "^0.2.1") (d #t) (k 0)) (d (n "bellhop-hook-jenkins") (r "^0.2.1") (d #t) (k 0)))) (h "1asr0hbbfmk2ccgg3zlfysad4ld6hcia1v67m7mdvd3cbjx9h76x")))

