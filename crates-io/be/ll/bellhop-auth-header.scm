(define-module (crates-io be ll bellhop-auth-header) #:use-module (crates-io))

(define-public crate-bellhop-auth-header-0.2.0 (c (n "bellhop-auth-header") (v "0.2.0") (d (list (d (n "bellhop") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)))) (h "1ylxbn9px5dxglqv34i6djmm7a5wva956n6dm2h71fr7izj7xwfl")))

(define-public crate-bellhop-auth-header-0.2.1 (c (n "bellhop-auth-header") (v "0.2.1") (d (list (d (n "bellhop") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)))) (h "0xbs3higf63v4ph92snzm75y8ci88z74wm135k3ip3viqilmpqnz")))

