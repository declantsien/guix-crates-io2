(define-module (crates-io be ll bell) #:use-module (crates-io))

(define-public crate-bell-0.1.0 (c (n "bell") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "1hrxxia4zx6yakadcjcsydjiakhn2r1b69l0qqmafy6h85d26nm7")))

(define-public crate-bell-0.1.1 (c (n "bell") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "1fzjgpkj054757pwdzy23w686ph02m3x19pb2rmh9pks2lla8l80")))

