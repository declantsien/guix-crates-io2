(define-module (crates-io be ll bellhop-cli) #:use-module (crates-io))

(define-public crate-bellhop-cli-0.2.1 (c (n "bellhop-cli") (v "0.2.1") (d (list (d (n "bellhop-client") (r "^0.2.1") (d #t) (k 0)) (d (n "directories") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1k7nicvz5k74rxv23n24zx8lnzx2w96ik65xa0x0z4acg7pjfyr2")))

