(define-module (crates-io be ll bells307-test-proj) #:use-module (crates-io))

(define-public crate-bells307-test-proj-0.1.0 (c (n "bells307-test-proj") (v "0.1.0") (h "0544vfl8agrr3h5kipy8c39spb27y6ryiycdlsncjla2zl4arvvl") (y #t)))

(define-public crate-bells307-test-proj-0.1.1 (c (n "bells307-test-proj") (v "0.1.1") (h "0x7d037dfb0mxhd0diz9c78wd077p1dp1a7g4r75220wz3zyyadc") (y #t)))

(define-public crate-bells307-test-proj-1.0.0 (c (n "bells307-test-proj") (v "1.0.0") (h "08221w372g7b4j24lgm0llaazyy95zzpjpypiprka2ghqbw6m003") (y #t)))

