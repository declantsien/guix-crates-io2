(define-module (crates-io be ll bellperson-sha512) #:use-module (crates-io))

(define-public crate-bellperson-sha512-0.1.0 (c (n "bellperson-sha512") (v "0.1.0") (d (list (d (n "bellperson") (r "^0.24.1") (d #t) (k 0)) (d (n "blstrs") (r "^0.6.1") (d #t) (k 0)) (d (n "ff") (r "^0.12.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "191q7z50mbk2g9ik9ihfr7zp3zhgp8zgb6q9qr2q1q788ppxjxin")))

(define-public crate-bellperson-sha512-0.2.0 (c (n "bellperson-sha512") (v "0.2.0") (d (list (d (n "bellperson") (r "^0.25.0") (d #t) (k 0)) (d (n "blstrs") (r "^0.7.0") (d #t) (k 0)) (d (n "ff") (r "^0.13.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1768cfznwm5rwjyixdzhia4svfd4d7f2s7c61fy6lx2haw3s3hps")))

