(define-module (crates-io be ll bellhop-client) #:use-module (crates-io))

(define-public crate-bellhop-client-0.2.1 (c (n "bellhop-client") (v "0.2.1") (d (list (d (n "reqwest") (r "~0.9") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "06zm5v28r1iwi6pljvygj50fkkd420fp09ljnn7yxah2q8ikp9ri")))

