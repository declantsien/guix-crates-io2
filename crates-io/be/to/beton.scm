(define-module (crates-io be to beton) #:use-module (crates-io))

(define-public crate-beton-0.1.0 (c (n "beton") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "heckcheck") (r "^2.0.1") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (d #t) (k 2)))) (h "0qszrxqq9m61rm0saj67lzm5lzd3frgj5rc8vbs80yl3sx5cd70x")))

