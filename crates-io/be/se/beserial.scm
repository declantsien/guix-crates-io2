(define-module (crates-io be se beserial) #:use-module (crates-io))

(define-public crate-beserial-0.0.0 (c (n "beserial") (v "0.0.0") (h "0bxhyb13gc1j77fnp235i0wn4lv955qhpganxq0iv8npgh42w2dr")))

(define-public crate-beserial-0.1.0 (c (n "beserial") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0slk3i7i9wms1qrzcb8a3l8li21isrrk6vjaa348ilvl1gmp3pph")))

(define-public crate-beserial-0.2.0 (c (n "beserial") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1jn2rlvz225452ci8nwzs3ni5bbnd4688fzcz39cyv46ahybrj3f")))

