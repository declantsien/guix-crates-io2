(define-module (crates-io be zi bezier_easing) #:use-module (crates-io))

(define-public crate-bezier_easing-0.1.0 (c (n "bezier_easing") (v "0.1.0") (h "08p4l906kjs9ykm1a1jxnlm0jq26m5k00bjsh2xzjhhg9yqbjss0")))

(define-public crate-bezier_easing-0.1.1 (c (n "bezier_easing") (v "0.1.1") (h "1pdy4s2nyry7s5b1c5b6z4bjmg87w1lzj4s2skfbcklcl95p790g")))

