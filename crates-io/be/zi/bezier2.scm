(define-module (crates-io be zi bezier2) #:use-module (crates-io))

(define-public crate-bezier2-0.1.0 (c (n "bezier2") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "16jx9kypk8d5587dxjka5ybydggjycffv8dnf5b2iwrd8ydi1sc3")))

(define-public crate-bezier2-0.1.1 (c (n "bezier2") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "17ri6gs5qxzyd9fk7p5j2b5fx59h5yp1649bhnj7f3kwkk57h62b")))

(define-public crate-bezier2-0.1.2 (c (n "bezier2") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "0dz14b3r8yjrdadnpkdbas7vjpkfilmw2nrr2k4m15qlgif7wfmn")))

(define-public crate-bezier2-0.2.0 (c (n "bezier2") (v "0.2.0") (d (list (d (n "cast_trait") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.2") (d #t) (k 0)))) (h "0j403mzb3ijnqfivpg6y25qa9m2i7v86lkdl920az1y9crzy6ndh")))

