(define-module (crates-io be zi bezier-interpolation) #:use-module (crates-io))

(define-public crate-bezier-interpolation-0.1.0 (c (n "bezier-interpolation") (v "0.1.0") (d (list (d (n "freetype-rs") (r "^0.7") (d #t) (k 0)))) (h "0hb8i68894pcs00bf389mnirnj1xxb69xf6q9rdysbm2vqzp0986") (y #t)))

(define-public crate-bezier-interpolation-0.2.0 (c (n "bezier-interpolation") (v "0.2.0") (d (list (d (n "freetype-rs") (r "^0.7") (d #t) (k 0)))) (h "0w707kb3gsplkyd43nla6r5xs5fhp6g2q7sw04lfm11zihijk788") (y #t)))

(define-public crate-bezier-interpolation-0.3.0 (c (n "bezier-interpolation") (v "0.3.0") (d (list (d (n "freetype-rs") (r "^0.7") (d #t) (k 0)))) (h "03l47wb5k094zvfn4rlc5q52k4nd88467ml11l37jkk5zx7jjk2r")))

