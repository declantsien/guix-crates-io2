(define-module (crates-io be zi bezier-rs) #:use-module (crates-io))

(define-public crate-bezier-rs-0.1.0 (c (n "bezier-rs") (v "0.1.0") (d (list (d (n "glam") (r "^0.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "1l2sqhjdzz15v1b55qvn99n74iv25kzgdd69d7bizkqgqqp62dj8") (r "1.65.0")))

(define-public crate-bezier-rs-0.2.0 (c (n "bezier-rs") (v "0.2.0") (d (list (d (n "dyn-any") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ny8kgl0fp28209b8l8n9r7mivjb5dcmakqr6d38qch5y8v8s8nn") (r "1.66.0")))

(define-public crate-bezier-rs-0.3.0 (c (n "bezier-rs") (v "0.3.0") (d (list (d (n "dyn-any") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1dgcnh3a3j45wxppj7bf0i9zzr13fdpagv8d5v562gmh2idmyvyd") (r "1.66.0")))

(define-public crate-bezier-rs-0.4.0 (c (n "bezier-rs") (v "0.4.0") (d (list (d (n "dyn-any") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1pj1ax64kgqinxc45kgjq39yx4sbqscv7p0sz22gkqi68cqsmqxx") (r "1.66.0")))

