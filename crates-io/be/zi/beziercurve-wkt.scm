(define-module (crates-io be zi beziercurve-wkt) #:use-module (crates-io))

(define-public crate-beziercurve-wkt-0.1.0 (c (n "beziercurve-wkt") (v "0.1.0") (d (list (d (n "quadtree-f32") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "1asi9h5ysy2w7gfcynzpa3wddq0ggrsxvnym3kbal42sykr4hzpr") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-beziercurve-wkt-0.1.1 (c (n "beziercurve-wkt") (v "0.1.1") (d (list (d (n "quadtree-f32") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fv50y5mky5ca024lsc2n1p79dl1mpw4my237szk8prssm5kyab1") (f (quote (("serialization" "serde") ("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-beziercurve-wkt-0.1.2 (c (n "beziercurve-wkt") (v "0.1.2") (d (list (d (n "quadtree-f32") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1p1zwwga8dn7xarh9x8y2fnsamjidj01l7gkmpznzpr4flrq841m") (f (quote (("serialization" "serde") ("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-beziercurve-wkt-0.1.3 (c (n "beziercurve-wkt") (v "0.1.3") (d (list (d (n "quadtree-f32") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1b1lyidja97czm86my18g4g7vbiwzmnpbas0ajqg81gfrxlbgxl8") (f (quote (("serialization" "serde") ("parallel" "rayon") ("default" "parallel"))))))

