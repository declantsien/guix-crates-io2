(define-module (crates-io be zi bezier-nd) #:use-module (crates-io))

(define-public crate-bezier-nd-0.1.0 (c (n "bezier-nd") (v "0.1.0") (d (list (d (n "geo-nd") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "1dp4zjy700mzjmkr310r1w7r2lwid3h1bsd3azx8w2r1dyqwvrv2")))

(define-public crate-bezier-nd-0.1.1 (c (n "bezier-nd") (v "0.1.1") (d (list (d (n "geo-nd") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "0vamgf917rhg93n28i24b4m5pqi4lms6p2dfzfznxif4vi0yp9md")))

(define-public crate-bezier-nd-0.1.2 (c (n "bezier-nd") (v "0.1.2") (d (list (d (n "geo-nd") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "1zdpnhbwhrnz89kcavhp1pslqx7wdamclirid7d042h19pzwf1rh")))

(define-public crate-bezier-nd-0.1.3 (c (n "bezier-nd") (v "0.1.3") (d (list (d (n "geo-nd") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "1wwfdq3rxk0qlb57v9cgi3whjk4rxwhj4ajlkyk7bpx43yhxvx7i")))

(define-public crate-bezier-nd-0.1.4 (c (n "bezier-nd") (v "0.1.4") (d (list (d (n "geo-nd") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "1f81waghlpkfid8wywncpqqjfjlnyyaw2zh2r4gm3dl0n7am97pr")))

(define-public crate-bezier-nd-0.5.0 (c (n "bezier-nd") (v "0.5.0") (d (list (d (n "geo-nd") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "0sqpzq6193fi03bjnh0c74wim0mml6nhhf15xa4qdxwpn412ldc5")))

