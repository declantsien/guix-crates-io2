(define-module (crates-io be zi bezier1) #:use-module (crates-io))

(define-public crate-bezier1-0.1.0 (c (n "bezier1") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "1bp4kvwl46zjzv6d476fqnlkj1acpsbq7hg8i0wvyh56rcq4wzan")))

(define-public crate-bezier1-0.2.0 (c (n "bezier1") (v "0.2.0") (d (list (d (n "cast_trait") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gccdqm3j1rxg19m3gnxkzd9j0s9s91gq1rk24gs3djnpsc4wlnw")))

