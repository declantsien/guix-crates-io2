(define-module (crates-io be st bestagon) #:use-module (crates-io))

(define-public crate-bestagon-0.1.0 (c (n "bestagon") (v "0.1.0") (h "1ky3s7h6fpw8cv3gc8l87pfaahj07l3w87warissyvlkgpg1p2ck")))

(define-public crate-bestagon-0.2.0 (c (n "bestagon") (v "0.2.0") (h "0ljn2x1dqph7fkv4i9b4w58qdb3gzvva2b92p11gnhpxkshcjvjm")))

