(define-module (crates-io be st best) #:use-module (crates-io))

(define-public crate-best-0.1.0 (c (n "best") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "005x9lxhbzfdyjmrbznsk026gglqgv610kprvwd1bgb3q41qvj1w")))

(define-public crate-best-0.2.0 (c (n "best") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1snbqlwbprpjw9whkcmz4kdxdmjfgqy0r95yqd2v4kamdyywzvq2")))

(define-public crate-best-0.3.0 (c (n "best") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ihcxkmc0qazj2xd9zlmrl1y9jdpimzjawfj5h8ldsz6yya05w1d")))

(define-public crate-best-0.4.0 (c (n "best") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1zz33p3lczsb2h1ggwhp8iph50kcdy3g488wkrjxhbnbmdf5nn29")))

(define-public crate-best-0.5.0 (c (n "best") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1vxr51bchnnmda2b34l3sxhmxg067z3zdhyv3snrknvxp0z0p62p")))

(define-public crate-best-0.6.0 (c (n "best") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0pks0imf3c6rir7ymdl1cad2fgnxbk8lhhdkl6fqxfbybl70c2p7")))

(define-public crate-best-0.7.0 (c (n "best") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1i1jqkb6zwrvhg76lajyqxccpiwb8fcgnfar2ql4sk33mmkm8p1l")))

(define-public crate-best-0.8.0 (c (n "best") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1ka6bihf3p9kcnikydf3y68rfzkcgnjiwcw5jpacgl8y0035yl3h")))

(define-public crate-best-0.9.0 (c (n "best") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0iw67sngy2cg993v2nsyi26llnn7wiwr93hvpi5zqkcljy8w6rfh")))

(define-public crate-best-0.10.0 (c (n "best") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "14qi7k5ziw7slmfl5z1fg89d41ym55ndvskhw2yf8xiz0wknyy2v")))

(define-public crate-best-0.11.0 (c (n "best") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1swk5yfkbdjj0v5jdj9vfgpw8i3wf9yfchazsrrcwhfqfqjzsgcf")))

(define-public crate-best-0.12.0 (c (n "best") (v "0.12.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0d6np2537f8g57943jrn77m1rqnp7z0rrrcczrx53kq0r5csz4cm")))

(define-public crate-best-0.13.0 (c (n "best") (v "0.13.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0fhl72m7majqxg2qiav1fa8hx3xbh991hs4hnyi8ivbwsh5f6gpd") (f (quote (("serialize" "serde"))))))

