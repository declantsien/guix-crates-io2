(define-module (crates-io be st bestchange_api) #:use-module (crates-io))

(define-public crate-bestchange_api-0.0.1 (c (n "bestchange_api") (v "0.0.1") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "144lv502k5r0rph7ygchpbal7wlcpp3m96h22b9zbzjgli91rmrk") (y #t)))

(define-public crate-bestchange_api-0.0.2 (c (n "bestchange_api") (v "0.0.2") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0c6hkh7g5q2snw1vcnv3nd92gi6gx5y5wcl0ykfkcik6wxx0f34b") (y #t)))

(define-public crate-bestchange_api-0.0.3 (c (n "bestchange_api") (v "0.0.3") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1f9qv9g1xzayfqaf6yz25cyvlxkxigvbknf63q91jpz2w54n3q0y")))

