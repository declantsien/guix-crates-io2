(define-module (crates-io be st best-path) #:use-module (crates-io))

(define-public crate-best-path-0.1.0 (c (n "best-path") (v "0.1.0") (d (list (d (n "codec") (r ">=1.0.0") (f (quote ("derive"))) (o #t) (k 0) (p "parity-scale-codec")) (d (n "num-traits") (r ">=0.2.1") (f (quote ("libm"))) (k 0)) (d (n "scale-info") (r ">=0.1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0n36d8dii6lwbbpjjf051cysn95741sns6r0vi45x009bdlq2pgr") (f (quote (("std" "codec/std" "scale-info/std") ("scale" "codec" "scale-info") ("default" "std"))))))

(define-public crate-best-path-0.1.1 (c (n "best-path") (v "0.1.1") (d (list (d (n "codec") (r ">=1.0.0") (f (quote ("derive"))) (o #t) (k 0) (p "parity-scale-codec")) (d (n "num-traits") (r ">=0.2.1") (f (quote ("libm"))) (k 0)) (d (n "scale-info") (r ">=0.1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "143s0257l1wmblrg0vv1x0hs688xhqvkdcr7qlmqbprh89cdy47c") (f (quote (("std" "codec/std" "scale-info/std") ("scale" "codec" "scale-info") ("default" "std"))))))

