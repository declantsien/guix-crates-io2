(define-module (crates-io be st best_macros) #:use-module (crates-io))

(define-public crate-best_macros-0.1.0 (c (n "best_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "106lprn18kynj75mzrl7f6kdpf59yjbzdxz6r06pyx1br3364hdz")))

(define-public crate-best_macros-0.1.1 (c (n "best_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06blj85zffnn6w47ss8fxldqv8m42csa19kgxf1la09nj9lwxjpb")))

(define-public crate-best_macros-0.1.2 (c (n "best_macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hjc36gg72hr3l36p957nydr2b8a9m9snzwgszfzff3yf8dj4kkf")))

(define-public crate-best_macros-0.1.3 (c (n "best_macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kfj112mh54f5zq7jr8mgxpz2jl6mnrxx7igpair68m36s67xrcp")))

