(define-module (crates-io be st best_skn_message) #:use-module (crates-io))

(define-public crate-best_skn_message-1.0.0 (c (n "best_skn_message") (v "1.0.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)))) (h "1zg46y5pj6irjaiwjdzl9cj46zkmqd4sz3ak3gv10w2rxi6g8x2k") (r "1.78.0")))

(define-public crate-best_skn_message-1.0.1 (c (n "best_skn_message") (v "1.0.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)))) (h "0a121ic758lrzj5j8338m394m7ll765xjbxkz97viskcbibqy2hs") (r "1.78.0")))

(define-public crate-best_skn_message-1.1.0 (c (n "best_skn_message") (v "1.1.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)))) (h "0x4sh39lvsgj5n1xdhxifcj9dvs5n8z1wgim0if2nchx4jmch0a1") (r "1.78.0")))

