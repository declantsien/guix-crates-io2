(define-module (crates-io be id beid_build_info) #:use-module (crates-io))

(define-public crate-beid_build_info-0.1.0 (c (n "beid_build_info") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "git2") (r "^0.15") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l9zcq3pp6gb7f3pwap30m2kwzp50lfi6y19344pmdkk4kp99x6l") (y #t)))

(define-public crate-beid_build_info-0.1.1 (c (n "beid_build_info") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "git2") (r "^0.15") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ndyqc9db786996y33278ff0jpndicyb6dqv4g0cb4i7w7bz12gn")))

(define-public crate-beid_build_info-0.1.2 (c (n "beid_build_info") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "git2") (r "^0.15") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xsbmy7ljb5m0s6jrlp1ycq33vg3w06gv3d1cfhpjzpnvfcg6557")))

(define-public crate-beid_build_info-0.2.0 (c (n "beid_build_info") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "git2") (r "^0.17") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14mg6lkbcfavls20wi8j93wvjh0908ji25rgfak4n0gmdgks1iz3")))

(define-public crate-beid_build_info-0.3.0 (c (n "beid_build_info") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zg7rkhpng4xzndm2yz7q1zy4wgiwl4snn4jj6534s887aafx99n")))

