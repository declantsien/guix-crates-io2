(define-module (crates-io be au beau_collector) #:use-module (crates-io))

(define-public crate-beau_collector-0.1.0 (c (n "beau_collector") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 2)))) (h "1a3lmv00w9vfy1nmmrgcydqb9z11hxk0q8k9v93frk58ya9dy6fq")))

(define-public crate-beau_collector-0.1.1 (c (n "beau_collector") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 2)))) (h "1fmywlbypjaprw4ricz211k3apjmjg13868221x8kwx6p1prajg8")))

(define-public crate-beau_collector-0.2.0 (c (n "beau_collector") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 2)))) (h "0bl8hain51frgy0w54rai35yma1m1p2s7b8kags20xw1lj8f6x4i")))

(define-public crate-beau_collector-0.2.1 (c (n "beau_collector") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 2)))) (h "0kyzy2fnl7clzf4iblwkhak343p596ac6lavq4r3vg9wdl34789k")))

