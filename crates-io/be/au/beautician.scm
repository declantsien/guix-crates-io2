(define-module (crates-io be au beautician) #:use-module (crates-io))

(define-public crate-beautician-0.1.0 (c (n "beautician") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0haa5klfnslcfiywpb4s5q18kqp3k81lp1rm71m81g14pccmbqg5")))

(define-public crate-beautician-0.1.1 (c (n "beautician") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0lfy0asr3iba952fg51xgpb6nkjjdw8kbm9rh900k5l6v7dfn0gj")))

