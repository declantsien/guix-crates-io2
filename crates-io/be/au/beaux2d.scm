(define-module (crates-io be au beaux2d) #:use-module (crates-io))

(define-public crate-beaux2d-0.1.0 (c (n "beaux2d") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "02fwk3ds02xwap5gv51ysxvnan9ha2adr5n21p3gsqf81g7859m9")))

(define-public crate-beaux2d-0.1.1 (c (n "beaux2d") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "1984a39r6wphn77wp5dsgi4wcj2racbrl19cg825ciy4fb3q8mnx")))

(define-public crate-beaux2d-0.1.2 (c (n "beaux2d") (v "0.1.2") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "11d5y6c8rqs2nyp62ndd6m2bsnzyawxnz5j77g67h1f1qj5pmnqg")))

(define-public crate-beaux2d-0.1.3 (c (n "beaux2d") (v "0.1.3") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "021pa1lr3jwi71kdzhw48v5lgdfv8r2zx4criwkjsnz0hz75sfg8")))

(define-public crate-beaux2d-0.1.4 (c (n "beaux2d") (v "0.1.4") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "1aav7h10dd5jra6q1qsxqshw2gb8kzci80bgkmzq28dchv9x6wr3")))

(define-public crate-beaux2d-0.2.0 (c (n "beaux2d") (v "0.2.0") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "1q9h7phqpmbsyfr9q5g9jiyhy9gdjab52svzpndh516izz1fgaff")))

(define-public crate-beaux2d-0.2.1 (c (n "beaux2d") (v "0.2.1") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0mc90lfcws7knfjcmcl6yg1yq1458w8ad9s4g8xyc3ijv4mppk9x")))

(define-public crate-beaux2d-0.2.2 (c (n "beaux2d") (v "0.2.2") (d (list (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "19lxv6j7cibm8q2n3lsknggp2kb4wd6y52mry41ribb6xgmjrvnl")))

