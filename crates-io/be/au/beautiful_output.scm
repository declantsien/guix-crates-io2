(define-module (crates-io be au beautiful_output) #:use-module (crates-io))

(define-public crate-beautiful_output-0.1.0 (c (n "beautiful_output") (v "0.1.0") (h "0f7l8a6ikmyidm130hvldpkmqj1vmz9kg3jrsw83s66h9q9h8isd")))

(define-public crate-beautiful_output-0.1.1 (c (n "beautiful_output") (v "0.1.1") (h "1jqh3sd7p7lm90r8kg83h5mf664ylwmqkdcv9l6ygbcsiqzy1ivx")))

(define-public crate-beautiful_output-0.1.2 (c (n "beautiful_output") (v "0.1.2") (h "0bdwdyxi9sdy2h0fvnd1qjzaq7v61z6b9iswscsz73rrknw10604")))

(define-public crate-beautiful_output-0.1.3 (c (n "beautiful_output") (v "0.1.3") (h "0is1957l32wq4fx8ignqd3qnnz6c8rqa7h0j83fll2n7hny50rv1")))

