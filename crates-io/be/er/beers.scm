(define-module (crates-io be er beers) #:use-module (crates-io))

(define-public crate-beers-0.1.0 (c (n "beers") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s6x6rgq16m3gv6v5s83zf6n7mk9ys27jj8c8n1cf72a88pc8xn7")))

