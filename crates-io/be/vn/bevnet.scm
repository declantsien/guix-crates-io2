(define-module (crates-io be vn bevnet) #:use-module (crates-io))

(define-public crate-bevnet-0.1.0 (c (n "bevnet") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jxyvk7rma5b7hda0fcnr9sym0xmy3cncnw762scpn5wz4q550wc")))

