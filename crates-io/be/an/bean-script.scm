(define-module (crates-io be an bean-script) #:use-module (crates-io))

(define-public crate-bean-script-0.1.1 (c (n "bean-script") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0ng3bhkaya9zcxafnks2lbfna8k786669y0cypsvysgf27michsy")))

(define-public crate-bean-script-0.1.2 (c (n "bean-script") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0nzdjwzd3i1dfjix18ab47aaj2rq9qki7qpi6x6cv0r6dpgf9zhj")))

(define-public crate-bean-script-0.1.3 (c (n "bean-script") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0yaa5ag352zgp4a4h8y01rilg6w1xl3nqzba0py3cbi9gx7vwy3m")))

(define-public crate-bean-script-0.1.4 (c (n "bean-script") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "1p0v41kp9qybh2zrxhbjn2g6far78n4nbvp7lrjsdan9cxc0cvbp")))

(define-public crate-bean-script-0.2.0 (c (n "bean-script") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0kiy25wd0id765h5f678w463bgdmj5kp3jp0cahifdif0rwhs0lr")))

(define-public crate-bean-script-0.2.1 (c (n "bean-script") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "1agbnr2q4mawnqwgx7gakbidp8bv8ps4sbas2z4xpvng621kzvhk")))

(define-public crate-bean-script-0.2.2 (c (n "bean-script") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0q63cg518vhv5a47hlj0g2sypc742li5mil3hw90g91vwddwyvra")))

(define-public crate-bean-script-0.2.3 (c (n "bean-script") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "1xki22bmh8646r5n1ldibbph8p23x4kv56khdbcz59y3vf1pdyrk")))

(define-public crate-bean-script-0.2.4 (c (n "bean-script") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0lg9ryp317ibq2x66plkvmpfqgjgd0q6h6lvhcxb2s10x2ql64fz")))

(define-public crate-bean-script-0.2.5 (c (n "bean-script") (v "0.2.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0kj2k6xjr8lpnx3vsxjigjvlvg17ancm1qbkbl2gm3y1122hrxb1")))

(define-public crate-bean-script-0.2.6 (c (n "bean-script") (v "0.2.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "1sl1zcw6j157vvh43465ypzjqc1kzb1xi71fxj56s2crapqlidmy")))

(define-public crate-bean-script-0.2.7 (c (n "bean-script") (v "0.2.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0qbfd8gq3ajv26d07k3lhrcjq9mx0k2hy61p3s98cvrmblzplkpi")))

(define-public crate-bean-script-0.2.8 (c (n "bean-script") (v "0.2.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0dzvd672x23hx30mrl1kd4vl32laab7av8l7fs1q9frbhsffk1ms")))

(define-public crate-bean-script-0.2.9 (c (n "bean-script") (v "0.2.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "1j73hyga21zaga41icphlvyp3675cjzbna2q9k64bgklpshf3x93")))

(define-public crate-bean-script-0.2.10 (c (n "bean-script") (v "0.2.10") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0ygjp20qwprq6vfcrg4k08h8h4yfnpg3pj4j8w7461i7dgm61izm")))

(define-public crate-bean-script-0.2.11 (c (n "bean-script") (v "0.2.11") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)))) (h "0wyaczbcfwv49219qg544nvng7rygahbimcqi0lp59zspkz65vdc")))

