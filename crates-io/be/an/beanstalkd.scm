(define-module (crates-io be an beanstalkd) #:use-module (crates-io))

(define-public crate-beanstalkd-0.0.0 (c (n "beanstalkd") (v "0.0.0") (h "033byicn0za6lgw4j7x7x8lvd0m5mz0bs1ghkscjyscnclfvzxby")))

(define-public crate-beanstalkd-0.0.1 (c (n "beanstalkd") (v "0.0.1") (h "0am87qfsb74ynx72qn3q4yhyimwdllphnqab3prjafpz3r2qp4gb")))

(define-public crate-beanstalkd-0.1.0 (c (n "beanstalkd") (v "0.1.0") (h "03xvv5wxsg5r0kyckznk7hs4q686k2iwd7y4bkd2yx7v0b9cic3g")))

(define-public crate-beanstalkd-0.2.0 (c (n "beanstalkd") (v "0.2.0") (h "197g8asgrwa69d76wqqd2sq28c1d5sqljwz2j105vlhjhgwlnx14")))

(define-public crate-beanstalkd-0.2.1 (c (n "beanstalkd") (v "0.2.1") (h "0a3jmyg9i87c9mavf6nxbaicf6s1p1bbdfpkfma8av207bb4nr0h")))

(define-public crate-beanstalkd-0.2.2 (c (n "beanstalkd") (v "0.2.2") (h "1h99lhx409hf8nsasribjcp1ijzmlw1cxl0a80247vyz5a1kghjb")))

(define-public crate-beanstalkd-0.2.3 (c (n "beanstalkd") (v "0.2.3") (h "1vg0y25zf5f571ip1dkqjz2i90rr4ncng23lhikaabyrnyg8bv6j")))

(define-public crate-beanstalkd-0.2.4 (c (n "beanstalkd") (v "0.2.4") (h "052f6a76rqvbwaa8f802rn2shscq2x0dyh275pm09d3fjwv6cj6x")))

(define-public crate-beanstalkd-0.3.0 (c (n "beanstalkd") (v "0.3.0") (h "126q0kvng7k9x22mr3p3620w9w9ylhrj652n8hfx030a59mzdg1w")))

(define-public crate-beanstalkd-0.3.1 (c (n "beanstalkd") (v "0.3.1") (h "1w7fjnfzj7mzs1brjncncjwb11mbq0midwxgqayynfchallf6g6c")))

(define-public crate-beanstalkd-0.3.2 (c (n "beanstalkd") (v "0.3.2") (h "060m229dhq0qbf6kf3a0sgfyvzfk1qxf39mj40lxdg82dnh8rqkk")))

(define-public crate-beanstalkd-0.4.1 (c (n "beanstalkd") (v "0.4.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)))) (h "11avbgdxqyh9pbcmrdmavry1i7f4x0r4wwhpq9hd2ykkfidg8srh")))

