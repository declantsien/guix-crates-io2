(define-module (crates-io be an beancount-account) #:use-module (crates-io))

(define-public crate-beancount-account-0.1.0 (c (n "beancount-account") (v "0.1.0") (d (list (d (n "delegate") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "momo") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)) (d (n "watt") (r "=0.4.3") (d #t) (k 0)))) (h "0b0sq64vmda3kxphd0w26xrsg4np03jhdncx5czvz3prjzrpdqk3") (r "1.71")))

