(define-module (crates-io be an beancount_parser_2) #:use-module (crates-io))

(define-public crate-beancount_parser_2-1.0.0-alpha.0 (c (n "beancount_parser_2") (v "1.0.0-alpha.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "10z9vzd1dshirxh5vkxl1k91iz16a1gq9f874czb819ib6aszm2x") (r "1.69.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.1 (c (n "beancount_parser_2") (v "1.0.0-alpha.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "07f1q3lvnmsr9nja4dnwlz1waj905f3yz5y78s14cnvs4spn1qiw") (r "1.69.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.2 (c (n "beancount_parser_2") (v "1.0.0-alpha.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "0yyngydkk3iib12jpgncixgs3dfpd03v2zwad4klc6l2hj44zbzp") (r "1.69.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.3 (c (n "beancount_parser_2") (v "1.0.0-alpha.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "1lzjnilm2l8q0m1iz2h3bghb7q59mxcqkvn0iv2zmyv56v612v69") (r "1.69.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.4 (c (n "beancount_parser_2") (v "1.0.0-alpha.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "0i4g2lc2h766ccpgdz1i9ixiw4zvcdsnc2smn4v1nd4k0yfmxmjj") (r "1.69.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.5 (c (n "beancount_parser_2") (v "1.0.0-alpha.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "0nj3dgrxkxqra1cf3xdpqa1kb1y5gcjcg34fxv7dzh4arxar826g") (r "1.69.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.6 (c (n "beancount_parser_2") (v "1.0.0-alpha.6") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "1d1vvmwhgpwgibyn23z0y84ladazmj0hsvn20ykz4plia0ivb84b") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.7 (c (n "beancount_parser_2") (v "1.0.0-alpha.7") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "1jwr3bc18lc3m54g2zyryqcvm0nkcissc0c0hqi8gf0573y9jd3f") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.8 (c (n "beancount_parser_2") (v "1.0.0-alpha.8") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "0m7gba3ralvrp9wxbch453pddz7gpy5n70sn09h9hdk59b6xyaxl") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.9 (c (n "beancount_parser_2") (v "1.0.0-alpha.9") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "1fafaz7zc6ak71pg5gjjj53jmks2k7x597fhpnjv210ch8517k6r") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.10 (c (n "beancount_parser_2") (v "1.0.0-alpha.10") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "1sfs1x8kqvblvqxksni6zxln2zyva47vpf72kc0dw9bzrmm3pyx2") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.11 (c (n "beancount_parser_2") (v "1.0.0-alpha.11") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "190j27wk5bsjk9fhyh5i8mhvpsfv56gnbvd5i9mmcagm2bsgb9pf") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-alpha.12 (c (n "beancount_parser_2") (v "1.0.0-alpha.12") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.29") (k 2)))) (h "1yag752ypqylb1ln9bz8vli6jh1p8fwh53r6vz73ah4nip67gkkk") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-beta.1 (c (n "beancount_parser_2") (v "1.0.0-beta.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1.0") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.30.0") (k 2)))) (h "0s2gnzll23fghiikv1kdgslrhc3yjxdmpgmim7q23n7hi2gvyscp") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-beta.2 (c (n "beancount_parser_2") (v "1.0.0-beta.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1.0") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.30.0") (k 2)))) (h "0ldfm88kqihlqcjzkpmw2dk7imf6j2sinm23w5pk8llkfdld7l1i") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-beta.3 (c (n "beancount_parser_2") (v "1.0.0-beta.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1.0") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.30.0") (k 2)))) (h "1xwyyznpnvmvikvzx92nsf1wfn7i89i2v6ziyb65bz0zrqir9bck") (r "1.70.0")))

(define-public crate-beancount_parser_2-1.0.0-beta.4 (c (n "beancount_parser_2") (v "1.0.0-beta.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (f (quote ("std"))) (k 0)) (d (n "nom_locate") (r "^4.1.0") (k 0)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "rust_decimal") (r "^1.30.0") (k 2)))) (h "0spnf45fzm54nxkvxd8jmc5p9isdnfav1v6ssnx4qrdq3b0cz3dp") (r "1.70.0")))

