(define-module (crates-io be an beancount-language-server-treesitter-utils) #:use-module (crates-io))

(define-public crate-beancount-language-server-treesitter-utils-0.1.0 (c (n "beancount-language-server-treesitter-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93.2") (d #t) (k 0)) (d (n "ropey") (r "^1.5") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "11sdx79cw5xglrl8shcn17nl2b133xxx9p77nc50lkibkvijmz5g") (y #t)))

