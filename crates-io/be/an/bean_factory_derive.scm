(define-module (crates-io be an bean_factory_derive) #:use-module (crates-io))

(define-public crate-bean_factory_derive-0.1.0 (c (n "bean_factory_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c1gl0j4fwg4b9287m7pjj8phvfr0pixlr9yy71nmk5bb5hjp3f5")))

(define-public crate-bean_factory_derive-0.1.1 (c (n "bean_factory_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y2y54vs71bfdls4wzf38s5gxjn1gmmc7bz9z2kzyna9p9qpq34w")))

(define-public crate-bean_factory_derive-0.1.2 (c (n "bean_factory_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z2qczzc63b0bv55yidb3gla4lrjngbgmccnl83h88zan0nqd0gh")))

(define-public crate-bean_factory_derive-0.1.3 (c (n "bean_factory_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a90zd2jzrjj43b2byahgh1qpgx0b7l92vpv1qfqfg7pciigzdnb")))

(define-public crate-bean_factory_derive-0.1.4 (c (n "bean_factory_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1swh5fwp6ggwj8llv9ac48g5vscfmw3fyk7r65np4vj88z4p2dw3")))

