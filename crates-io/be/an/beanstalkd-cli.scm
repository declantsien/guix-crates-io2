(define-module (crates-io be an beanstalkd-cli) #:use-module (crates-io))

(define-public crate-beanstalkd-cli-0.0.1 (c (n "beanstalkd-cli") (v "0.0.1") (d (list (d (n "beanstalkd") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "15dqlr799igsrk8yn87ih37wllw9qvqv1xlpr8nbii682svgmcyh")))

(define-public crate-beanstalkd-cli-0.1.0 (c (n "beanstalkd-cli") (v "0.1.0") (d (list (d (n "beanstalkd") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "036rkxb5dsm987649hv8nwd3bcdzh9jy0x7f8k8y763bsn03qhd4")))

(define-public crate-beanstalkd-cli-0.3.0 (c (n "beanstalkd-cli") (v "0.3.0") (d (list (d (n "beanstalkd") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "16g7pqsyjqca99sa0c7lhq7yh4x6y9vzk645iw6xm61d49y77ygm")))

(define-public crate-beanstalkd-cli-0.4.0 (c (n "beanstalkd-cli") (v "0.4.0") (d (list (d (n "beanstalkd") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0a04yqmcd4bk546r6329ikf5xhpfk11fsrmq1sa3i6bqw8c99ydb")))

