(define-module (crates-io be an bean-macro) #:use-module (crates-io))

(define-public crate-bean-macro-0.1.0 (c (n "bean-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dkii6qfripbgbv70r00yif8nxx1yx9izw89h5vjrdnpqljyyya6") (y #t)))

(define-public crate-bean-macro-0.1.1 (c (n "bean-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s7xy1qh8318ja6ha0v8ijvzsm48brz5bb1k3wrdg0cw72n4hj0m") (y #t)))

(define-public crate-bean-macro-0.1.2 (c (n "bean-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l08plncvnpv3rb436gmif24pwkzpv9cqj5zlh64c40kb82qj37b") (y #t)))

(define-public crate-bean-macro-0.1.4 (c (n "bean-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zkafh42zxzrg09xg2g1csrxm9grwrgmv0ii4agb590pzz19gr4z")))

