(define-module (crates-io be an beancount-sort) #:use-module (crates-io))

(define-public crate-beancount-sort-0.1.0 (c (n "beancount-sort") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.43") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derivative") (r ">=2.2.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.14") (d #t) (k 0)) (d (n "regex") (r ">=1.5.4") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.22") (d #t) (k 0)))) (h "0yfxgvarzhm50lh21vwhplbhy02m625m66lpgnf7xm6q9q02czf8")))

(define-public crate-beancount-sort-0.1.1 (c (n "beancount-sort") (v "0.1.1") (d (list (d (n "anyhow") (r ">=1.0.43") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derivative") (r ">=2.2.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.14") (d #t) (k 0)) (d (n "regex") (r ">=1.5.4") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.22") (d #t) (k 0)))) (h "1a4vmpzmnw5q7haxhc9a0br347b4ahyd82q7agk7s7xhsky8z230")))

(define-public crate-beancount-sort-0.1.2 (c (n "beancount-sort") (v "0.1.2") (d (list (d (n "anyhow") (r ">=1.0.43") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derivative") (r ">=2.2.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.14") (d #t) (k 0)) (d (n "regex") (r ">=1.5.4") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.22") (d #t) (k 0)))) (h "058wpwrb3lvx59xv56vsfi3b7yym6b43cnbbvhr9f95l5h2kysz3")))

(define-public crate-beancount-sort-0.1.3 (c (n "beancount-sort") (v "0.1.3") (d (list (d (n "anyhow") (r ">=1.0.43") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derivative") (r ">=2.2.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.14") (d #t) (k 0)) (d (n "regex") (r ">=1.5.4") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.22") (d #t) (k 0)))) (h "07cbi17aq12gfxc1k5pw5vb8dpxhhc3q85aw30nwpvjrplfimwmh")))

(define-public crate-beancount-sort-0.1.4 (c (n "beancount-sort") (v "0.1.4") (d (list (d (n "anyhow") (r ">=1.0.43") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derivative") (r ">=2.2.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.14") (d #t) (k 0)) (d (n "regex") (r ">=1.5.4") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.22") (d #t) (k 0)))) (h "0gs4bg18rwfmi6fa39fwh67gximdssk9racg0f80ff5qm1l3yqdz")))

(define-public crate-beancount-sort-0.1.5 (c (n "beancount-sort") (v "0.1.5") (d (list (d (n "anyhow") (r ">=1.0.43") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derivative") (r ">=2.2.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.14") (d #t) (k 0)) (d (n "regex") (r ">=1.5.4") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.22") (d #t) (k 0)))) (h "0zjlz80jbd045j8d69qahj54c9qpd2hjbnaxcwsif1f2jylxvmgm")))

