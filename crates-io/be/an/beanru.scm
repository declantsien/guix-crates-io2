(define-module (crates-io be an beanru) #:use-module (crates-io))

(define-public crate-beanru-0.1.0 (c (n "beanru") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "beancount-parser") (r "^2.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.0") (d #t) (k 0)))) (h "1fiz2x2vl6mcyvddhw1q6k4lj8msf7jvlhirn1q4kq9iilcmllp1")))

