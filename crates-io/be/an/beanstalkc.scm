(define-module (crates-io be an beanstalkc) #:use-module (crates-io))

(define-public crate-beanstalkc-0.2.0 (c (n "beanstalkc") (v "0.2.0") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1ydqb9934bplccahx93hvgijnwpqgzfgpsl9br4q0w9a3mvpv5dw")))

(define-public crate-beanstalkc-0.2.1 (c (n "beanstalkc") (v "0.2.1") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "11rv8hw5y8cirsjl8dqh8vyhwbw52ynk7b3cszz7chcqv3d82jsw")))

(define-public crate-beanstalkc-0.2.2 (c (n "beanstalkc") (v "0.2.2") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0r8075l2vhwvbb2a48zwfgynr0n2nm9249ybp28dbisv2g686yn6")))

(define-public crate-beanstalkc-0.2.3 (c (n "beanstalkc") (v "0.2.3") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "05drqglnqirrdpak0vb73xbh1y4zza55lxsybqdlsl5icj1l5g2z")))

(define-public crate-beanstalkc-0.2.4 (c (n "beanstalkc") (v "0.2.4") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "09p2f5r1xqfqnyr5xlwmqj11g89vgh0qmbqa79b9x7s40yp0najv")))

(define-public crate-beanstalkc-0.2.5 (c (n "beanstalkc") (v "0.2.5") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1bhrrfa131bjnq3n68wq7f5gj9y4j4lyyqs8a6kqb36shw700qpx")))

(define-public crate-beanstalkc-0.2.6 (c (n "beanstalkc") (v "0.2.6") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0jcz2drfs8c4zk4r3zfjm34knd7ivp22c0yj2s78y2nwm5qxv6g8")))

(define-public crate-beanstalkc-0.2.7 (c (n "beanstalkc") (v "0.2.7") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "04vqsmgrzl0401m00jcwak8apa85v641nx04q23ys7pxnkgmkqpf")))

(define-public crate-beanstalkc-0.2.8 (c (n "beanstalkc") (v "0.2.8") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "08ibzxq4fnpdgsg637656mw1ls5vywkm016j9qxrzs9j2iv2c99v")))

(define-public crate-beanstalkc-0.2.9 (c (n "beanstalkc") (v "0.2.9") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1cvqjqnibs9d59c96h67syz8l2b09jklkay4sy54dd4drm38ym1j")))

(define-public crate-beanstalkc-1.0.0 (c (n "beanstalkc") (v "1.0.0") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.17") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "08drjmgv2qj5hb79r7i6qj3fwdn8x4z8wx4003sgq5xpivc0jzbc")))

