(define-module (crates-io be an bean) #:use-module (crates-io))

(define-public crate-bean-0.1.0 (c (n "bean") (v "0.1.0") (h "196q6xalwm5vk1268drd6c7dw37f5nmx2y32hl0x6w7mq9cc6pkh") (y #t)))

(define-public crate-bean-0.1.2 (c (n "bean") (v "0.1.2") (d (list (d (n "bean-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1kfnfdki9djg79irih2hapi8f2d7sl4iwr3wzvds80fzkd46rbjk") (y #t)))

(define-public crate-bean-0.1.3 (c (n "bean") (v "0.1.3") (d (list (d (n "bean-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1l31yq7dz0d14bx7kydd17jrdwzybqg1hp35vn2f35av8mrbhcpl") (y #t)))

(define-public crate-bean-0.1.4 (c (n "bean") (v "0.1.4") (d (list (d (n "bean-macro") (r "^0.1.4") (d #t) (k 0)))) (h "02rfqayh850i0bhacb9y4xf88268hygw1vbql91vixxsm5m39kv5")))

