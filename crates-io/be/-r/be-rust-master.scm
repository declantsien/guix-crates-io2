(define-module (crates-io be -r be-rust-master) #:use-module (crates-io))

(define-public crate-be-rust-master-0.1.0 (c (n "be-rust-master") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03dj2nmcrk0crzi10ij4jwlg1pdkx7nk0n9wf18p0zw248pcaik2")))

