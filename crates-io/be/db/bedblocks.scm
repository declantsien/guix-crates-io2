(define-module (crates-io be db bedblocks) #:use-module (crates-io))

(define-public crate-bedblocks-0.1.0 (c (n "bedblocks") (v "0.1.0") (d (list (d (n "bio") (r "^1.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)))) (h "04x2qr061dxzzszkbmzhk837va5zfbg942lzk8v154jr9hflj09j")))

