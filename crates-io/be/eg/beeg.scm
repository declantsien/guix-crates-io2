(define-module (crates-io be eg beeg) #:use-module (crates-io))

(define-public crate-beeg-0.1.0 (c (n "beeg") (v "0.1.0") (d (list (d (n "comat") (r "^0.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1kb7pbzwksbgz29xffnw02vz069izm22if4vdwbaa0krw8jg8x0c")))

(define-public crate-beeg-0.1.1 (c (n "beeg") (v "0.1.1") (d (list (d (n "clipp") (r "^0.1.0") (d #t) (k 0)) (d (n "comat") (r "^0.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0zqr9zy430vhxvx47lhld46fmiznhvv5hx0vadz15rjpdsz5iqka")))

(define-public crate-beeg-0.1.2 (c (n "beeg") (v "0.1.2") (d (list (d (n "clipp") (r "^0.1.0") (d #t) (k 0)) (d (n "comat") (r "^0.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0jv3f634zdcs46rz9a056qqg7fk5pr0j9l3i7k4x8xxb0ww2i6c0")))

