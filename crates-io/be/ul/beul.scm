(define-module (crates-io be ul beul) #:use-module (crates-io))

(define-public crate-beul-0.1.0 (c (n "beul") (v "0.1.0") (h "0xpqri6pcx3ydx3fswr6wk0a9a2si0hv4vv1by2b2g2wz9z95zl9")))

(define-public crate-beul-0.1.1 (c (n "beul") (v "0.1.1") (h "1jcfhxjfjrqyabfbfipd70iafskc3lr40hm1sbda8ahlbkxx8yj3") (r "1.51")))

(define-public crate-beul-1.0.0 (c (n "beul") (v "1.0.0") (h "0byxzr8a6sjd6ipvxkg1nsisyllz693hj859kpk63c9py3nvmhyk") (r "1.68")))

