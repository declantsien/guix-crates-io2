(define-module (crates-io be ng beng-beng) #:use-module (crates-io))

(define-public crate-beng-beng-0.1.0 (c (n "beng-beng") (v "0.1.0") (h "0lqvgqfs5ac8jslf512vn9cd6sj6rmbn247x6pswpxm0fwpn31a5") (y #t)))

(define-public crate-beng-beng-0.1.1 (c (n "beng-beng") (v "0.1.1") (h "06s1rs65wbij88rmkflzw0r4z6i63b3if4fxl05gdils0iy79vs0") (y #t)))

(define-public crate-beng-beng-0.1.2 (c (n "beng-beng") (v "0.1.2") (h "0l1kc7a2qlslbyhdyfx7mbkzqdld5d5754417xj288df7rd29pi3") (y #t)))

