(define-module (crates-io be ng bengbenge) #:use-module (crates-io))

(define-public crate-bengbenge-0.1.0 (c (n "bengbenge") (v "0.1.0") (h "0i0vlb8b3wqy1biy5sj5qii7n7q0s1hy1i6yln9w9p9jdxlcn023") (r "1.56")))

(define-public crate-bengbenge-0.2.0 (c (n "bengbenge") (v "0.2.0") (h "05lnw49z72vfm3zyij4gr2xm9kisyksbiph44140cngjnr9dj17j") (r "1.56")))

(define-public crate-bengbenge-0.3.0 (c (n "bengbenge") (v "0.3.0") (h "1qzvz2grk9ydrjd7c97a5ivi8h8xj4b3h21gb6yi2njp5qml022k") (r "1.56")))

(define-public crate-bengbenge-0.3.1 (c (n "bengbenge") (v "0.3.1") (h "0n2fdmajan6x5xf4sv11l20ff1q86vygl12simr4vzj3j42sipxg") (r "1.56")))

(define-public crate-bengbenge-0.3.1-alpha.2 (c (n "bengbenge") (v "0.3.1-alpha.2") (h "043qfnr1db5cs06w487riyhdq485nq77iq9kclksk7fwdhm9qmsw") (r "1.56")))

(define-public crate-bengbenge-0.3.1-alpha.3 (c (n "bengbenge") (v "0.3.1-alpha.3") (h "0mlk0vy0rapqbna63xvh1zhqbldqial3vl22iw7h2ajlh0v4lc2k") (r "1.56")))

