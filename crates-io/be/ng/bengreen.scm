(define-module (crates-io be ng bengreen) #:use-module (crates-io))

(define-public crate-bengreen-0.1.0 (c (n "bengreen") (v "0.1.0") (d (list (d (n "x86") (r "^0.7") (d #t) (k 0)))) (h "10dkm819sxqaxks103x3br0wv7fj1xgl184ayyxalbisqf22wka2")))

(define-public crate-bengreen-0.1.1 (c (n "bengreen") (v "0.1.1") (d (list (d (n "x86") (r "^0.7") (d #t) (k 0)))) (h "0rpchdvg1gr6s5zaszi0rry20jxg2595xf4z8k8vc63hn9sbm05j")))

