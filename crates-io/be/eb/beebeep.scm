(define-module (crates-io be eb beebeep) #:use-module (crates-io))

(define-public crate-beebeep-0.1.1 (c (n "beebeep") (v "0.1.1") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "notifica") (r "^3.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "teloxide") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "06f4fsbgzxs5n6byviqhl9yid9lj8fbwjm2m7pn917bynrbws484")))

