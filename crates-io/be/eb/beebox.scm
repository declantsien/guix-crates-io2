(define-module (crates-io be eb beebox) #:use-module (crates-io))

(define-public crate-beebox-0.1.0 (c (n "beebox") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)))) (h "0wxrzp67giqirry3w2323f95mcx2kvfj1pwnyifsgjxwfxpipxvp")))

(define-public crate-beebox-0.1.1 (c (n "beebox") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)))) (h "16vn76plr1ayfmbk92r09x0ij7wc9h4m038xlh45adj4bjb3ij23")))

