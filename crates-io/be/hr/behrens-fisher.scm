(define-module (crates-io be hr behrens-fisher) #:use-module (crates-io))

(define-public crate-behrens-fisher-0.1.0 (c (n "behrens-fisher") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "special") (r "^0.8") (d #t) (k 0)))) (h "0808r4qcmslxpdgm3ryhjak86b3aaq17k1970xspjhcpzfdwcys7")))

(define-public crate-behrens-fisher-0.2.0 (c (n "behrens-fisher") (v "0.2.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "special") (r "^0.8") (d #t) (k 0)))) (h "0mkri7nshz3fcdfrwc4rwbski5ii7r1zsh01z64i1wzb92jidb92")))

