(define-module (crates-io be tr betrusted-rt) #:use-module (crates-io))

(define-public crate-betrusted-rt-0.0.1 (c (n "betrusted-rt") (v "0.0.1") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.2") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.2") (d #t) (k 2)))) (h "1z0b8w73qikcmjb5r3g8j7n9706w5fryzsmavv7gqw5vjgv5rqhc") (f (quote (("inline-asm" "vexriscv/inline-asm"))))))

