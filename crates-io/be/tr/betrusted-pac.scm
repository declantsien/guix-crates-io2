(define-module (crates-io be tr betrusted-pac) #:use-module (crates-io))

(define-public crate-betrusted-pac-0.0.1 (c (n "betrusted-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "riscv") (r "^0.0.2") (d #t) (k 0) (p "vexriscv")) (d (n "riscv-rt") (r "^0.0.1") (d #t) (k 0) (p "betrusted-rt")) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1y630zc7az7jh4d4dqlgd72lqjzr12p2igihcmb34dkiaz8y274g")))

