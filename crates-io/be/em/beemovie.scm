(define-module (crates-io be em beemovie) #:use-module (crates-io))

(define-public crate-beemovie-0.1.2 (c (n "beemovie") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0g4x9nbgkwx42zzwbrai5wffmjsc5qymlqvrc18r2ywcqxrgwcjp")))

(define-public crate-beemovie-0.1.3 (c (n "beemovie") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "00hzqnwghisifqns59lyvbjrwa77cziih6wgiwfrbx5alr1p7m7m")))

(define-public crate-beemovie-0.1.4 (c (n "beemovie") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1rfwh4wx9lyvrhnh6i34prcrpcghha09x35di7algf2ls4f1rw3w")))

(define-public crate-beemovie-0.1.5 (c (n "beemovie") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0xsafdwgkq3qx53waly2ws27rq17abzdcmdkmhhx394ppgiqcm4s")))

(define-public crate-beemovie-0.1.6 (c (n "beemovie") (v "0.1.6") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0p5fhyzhcc0qb3wn43nkxzq2zw6x8mfss8wl7gkwjnrvk5piv03m")))

(define-public crate-beemovie-0.1.7 (c (n "beemovie") (v "0.1.7") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0qx9h21yrsvx150nnnp8zn9zl70g5g27bcykdnvmj3x4b5gyifx6") (y #t)))

(define-public crate-beemovie-0.2.0 (c (n "beemovie") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "11y8sqg52iv6mkpw4y7qgscr3p72xwdjrgvhrb36398i0j035cqc")))

(define-public crate-beemovie-0.2.1 (c (n "beemovie") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1dwmr4vay8m9bg5zyfy747m7i7gbbnk4sz2wq2y68z3x97k15mk2") (y #t)))

(define-public crate-beemovie-0.2.2 (c (n "beemovie") (v "0.2.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04d6n6ybgzg54mjsk1s6lp612ba6rmqymnai7xlvxlslqf33swbf")))

(define-public crate-beemovie-0.2.3 (c (n "beemovie") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0s8zkz09d8gbswgp6ybq3287hs24jbxdc0nhkbss30nk0fi3dl4p")))

(define-public crate-beemovie-1.0.0 (c (n "beemovie") (v "1.0.0") (d (list (d (n "promptly") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0n735p3rlm0kp5szbbympj24swhg738j1rlnm3cbs094c3lpcjcm")))

(define-public crate-beemovie-1.0.1 (c (n "beemovie") (v "1.0.1") (d (list (d (n "promptly") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1lzkknb5yfw084n8mxhyzybiij9akwvpljwpbpgw41crs4lggsn9")))

