(define-module (crates-io be em beemovie-gui) #:use-module (crates-io))

(define-public crate-beemovie-gui-0.1.0 (c (n "beemovie-gui") (v "0.1.0") (d (list (d (n "beemovie") (r "^0.1.6") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (o #t) (d #t) (k 0)) (d (n "gio") (r "^0.9.0") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "glib") (r "^0.10.0") (d #t) (k 0)) (d (n "gtk") (r "^0.9.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.10") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "0kzfmzrkan6mmikb6hsplzj5nqgnc9b9c9v20yh8imdk4ivlpsbn") (f (quote (("notify" "notify-rust") ("discord" "reqwest" "dirs"))))))

