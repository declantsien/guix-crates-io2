(define-module (crates-io be em beemovie-cli) #:use-module (crates-io))

(define-public crate-beemovie-cli-0.1.0 (c (n "beemovie-cli") (v "0.1.0") (d (list (d (n "beemovie") (r "^0.2.2") (d #t) (k 0)))) (h "1l2ggiykl4ldmp4k8jd246r09dl3v7ijz8kfgwjs253m6dpkhy2j")))

(define-public crate-beemovie-cli-0.1.1 (c (n "beemovie-cli") (v "0.1.1") (d (list (d (n "beemovie") (r "^0.2.3") (d #t) (k 0)))) (h "1ly9sqcq0qz8nl8m12v9sxncr7jpl2pj1mxc7dkljpfp6zlirasw")))

(define-public crate-beemovie-cli-0.1.2 (c (n "beemovie-cli") (v "0.1.2") (d (list (d (n "beemovie") (r "^1.0.0") (d #t) (k 0)))) (h "0ggwwbphrldmx0xb2w32nah1qx0c6m4bz2xfwslkrlzp7xx0qdfg")))

(define-public crate-beemovie-cli-0.1.3 (c (n "beemovie-cli") (v "0.1.3") (d (list (d (n "beemovie") (r "^1.0.1") (d #t) (k 0)))) (h "0bm2jhnbz9dj5dmykkqgbpsxbva45y0gzl6kn57mfk1ssfg2lk25")))

