(define-module (crates-io be as beast) #:use-module (crates-io))

(define-public crate-beast-1.0.0-alpha.1 (c (n "beast") (v "1.0.0-alpha.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "melon") (r "^0.11") (d #t) (k 0)) (d (n "pest") (r "^1") (d #t) (k 0)) (d (n "pest_derive") (r "^1") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0p94grjbzqjar6768h95qkxhamrlzfa8jp2id3qymhcj5q34cbwk")))

(define-public crate-beast-1.0.0-alpha.2 (c (n "beast") (v "1.0.0-alpha.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "melon") (r "^0.13") (d #t) (k 0)) (d (n "pest") (r "^1") (d #t) (k 0)) (d (n "pest_derive") (r "^1") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "192vcdwjxcy3w7z81jszvyh5jy17w5fq3mhhxnmrihyv2rmqa9g9")))

