(define-module (crates-io be as beastlink) #:use-module (crates-io))

(define-public crate-beastlink-1.0.0 (c (n "beastlink") (v "1.0.0") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.4") (d #t) (k 2)))) (h "1mqnqv84amn4nz1yh733l5y136a7f6jpmz5xfy7n50dsmlwdjkhm")))

