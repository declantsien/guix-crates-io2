(define-module (crates-io be nr benri) #:use-module (crates-io))

(define-public crate-benri-0.1.0 (c (n "benri") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0ym8l2zgm9551m7hib2xg96g164x5h6i8pzhlm06j9kq561d5y63") (f (quote (("default"))))))

(define-public crate-benri-0.1.1 (c (n "benri") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0vj4f2wynxhlpzqnvw6grv6ylvbxmkhsjaxppy32capnn87dx0y7") (f (quote (("default"))))))

(define-public crate-benri-0.1.2 (c (n "benri") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0iwycjigg13qahxl6rzk2gmm5nlg9zz01d8zszzbib8hs5kqzpk1") (f (quote (("default"))))))

(define-public crate-benri-0.1.3 (c (n "benri") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "1crxgkf00f371hl2pg0fbwgdamwmz99k12qyrc1sngx1agvzj4pb") (f (quote (("default"))))))

(define-public crate-benri-0.1.4 (c (n "benri") (v "0.1.4") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "1bx8malqk8dv5ffi308ka41mwhskgfmakmaa3nkk60zg0hkz8sj3") (f (quote (("default"))))))

(define-public crate-benri-0.1.5 (c (n "benri") (v "0.1.5") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "1v4ccgk6kf9j16jr72sy73371wdxkipbarkhv52js69mmap5852a") (f (quote (("default"))))))

(define-public crate-benri-0.1.6 (c (n "benri") (v "0.1.6") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "07arvydmqy4y82hbig349lkifqp8wrl07c97xd7ym9cy8nllkxyw") (f (quote (("default"))))))

(define-public crate-benri-0.1.7 (c (n "benri") (v "0.1.7") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "195k07d4ck1885crxhn924zl1087l46yyjlcqwgj39f2v9s4257g") (f (quote (("default"))))))

(define-public crate-benri-0.1.8 (c (n "benri") (v "0.1.8") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0ag104c7p7hk2c2p88k8z82fn9xvf1xqcyl4gk40damcc4d3rbd7") (f (quote (("default"))))))

(define-public crate-benri-0.1.9 (c (n "benri") (v "0.1.9") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "14alj3iizk5fca2ma42yznw41walxa9irddb7bqzgx675kh901s8") (f (quote (("default"))))))

(define-public crate-benri-0.1.10 (c (n "benri") (v "0.1.10") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0a22xb8d1kv6pbxp4nzkxfr09vhqfaaljvkim56p2npfwl8jj725") (f (quote (("default"))))))

(define-public crate-benri-0.1.11 (c (n "benri") (v "0.1.11") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0rnvd65sfvy4wwwcaxg5wpq88nx6mrwwvs50faq7kw8wzd8cp1b1") (f (quote (("default"))))))

(define-public crate-benri-0.1.12 (c (n "benri") (v "0.1.12") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0r9qpkj2vk791hdmcx64adinp30rxnkrz2s6gaph4yisdj9rk97m") (f (quote (("default"))))))

