(define-module (crates-io be ry beryl) #:use-module (crates-io))

(define-public crate-beryl-0.1.0 (c (n "beryl") (v "0.1.0") (h "1jr63d8m37nl18i8rq6w3n0l3l6jsr2m62kv1ms9fbb2g1z3n5fz")))

(define-public crate-beryl-0.1.1 (c (n "beryl") (v "0.1.1") (h "0xhj8dk3f1b92505wqxd4imfxy1li4dprm2szzfgqpld83xbmcym")))

