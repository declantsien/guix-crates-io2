(define-module (crates-io be rm bermuda) #:use-module (crates-io))

(define-public crate-bermuda-0.0.1 (c (n "bermuda") (v "0.0.1") (d (list (d (n "dirs2") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s3fn5ihxr51x2sn7nvmcy9v9rcihagrnnpzkclpv9jniydn1sfc")))

