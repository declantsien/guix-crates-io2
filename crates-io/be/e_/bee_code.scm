(define-module (crates-io be e_ bee_code) #:use-module (crates-io))

(define-public crate-bee_code-0.2.0 (c (n "bee_code") (v "0.2.0") (h "1n1hh1l2dsrf7hkqm23bwqb4w4a4dlrh5rvrqq9c7s9mv8ns4d9m")))

(define-public crate-bee_code-0.3.0 (c (n "bee_code") (v "0.3.0") (h "04m8xa41mvqf36y4pdxg3ljhbrc3s5mfrbbksfnnm2whrfh7czbr")))

