(define-module (crates-io be ch bech32grs) #:use-module (crates-io))

(define-public crate-bech32grs-0.10.0-beta (c (n "bech32grs") (v "0.10.0-beta") (h "0i7hg08gh2yzhgd4ang08jayz8ik9j297fk85n6wy5xyiynhl6af") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-bech32grs-0.11.0 (c (n "bech32grs") (v "0.11.0") (h "12s9m0wah3y947rihqwbzj8ixkqqrnzpl7jnk6z7znrd04frwgjs") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

