(define-module (crates-io be ch bech32-utils) #:use-module (crates-io))

(define-public crate-bech32-utils-0.2.0 (c (n "bech32-utils") (v "0.2.0") (d (list (d (n "bech32") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)))) (h "02911a5wj20sn4a1wam6gx4a176d3pc51r4f99jiqldy3yvs1mvs")))

