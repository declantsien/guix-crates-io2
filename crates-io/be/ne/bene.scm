(define-module (crates-io be ne bene) #:use-module (crates-io))

(define-public crate-bene-0.1.0 (c (n "bene") (v "0.1.0") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1pam7qjmix2kq77pkqspi8rax74xnwf2jdyg1mmadv365kwi01n5")))

(define-public crate-bene-0.1.1 (c (n "bene") (v "0.1.1") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1hxfi9gvzfymviahz8nm3nkaj0syhrjg54q6vrp1j4i75n5zgb9g")))

