(define-module (crates-io be ne benemalloc) #:use-module (crates-io))

(define-public crate-benemalloc-0.1.0-BETA (c (n "benemalloc") (v "0.1.0-BETA") (d (list (d (n "allocations") (r "^0.1.0-BETA") (d #t) (k 0)))) (h "07fyf4zz44fgzc3xr4rb2zacjpngkhgdrz43gsbchgbwlb9qjykb") (f (quote (("track_allocations") ("default"))))))

(define-public crate-benemalloc-0.1.1-BETA (c (n "benemalloc") (v "0.1.1-BETA") (d (list (d (n "allocations") (r "^0.1.0-BETA") (d #t) (k 0)))) (h "05nrqhbm679mzf092mww1pnnygq2834yvxifccj11x1dqvbcdcb1") (f (quote (("track_allocations") ("default"))))))

