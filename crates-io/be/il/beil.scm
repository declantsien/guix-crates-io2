(define-module (crates-io be il beil) #:use-module (crates-io))

(define-public crate-beil-0.1.0 (c (n "beil") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "object") (r "^0.28.3") (d #t) (k 0)) (d (n "symbolic") (r "^8.6.0") (f (quote ("demangle"))) (k 0)))) (h "1mjdvvs4bypkdyrx2c9vhzkc96y1x3kdi8r2kdbmr26gc1m0nlkg")))

(define-public crate-beil-0.2.0 (c (n "beil") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "object") (r "^0.28.3") (d #t) (k 0)) (d (n "symbolic") (r "^8.6.0") (f (quote ("demangle"))) (k 0)) (d (n "uuid") (r "^0.4.0") (d #t) (k 0)))) (h "1b2i5vh6w5543d40v0wg80sn2v55p8bkb1ac85px82qng2zp10ah")))

