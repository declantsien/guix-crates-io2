(define-module (crates-io be rr berryimu) #:use-module (crates-io))

(define-public crate-berryimu-0.1.0 (c (n "berryimu") (v "0.1.0") (d (list (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)))) (h "0macmns0nbxkpdjslfvwa8mkxjqmfkil0sy3g3qb9abcj9h999gs")))

(define-public crate-berryimu-0.2.0 (c (n "berryimu") (v "0.2.0") (d (list (d (n "i2cdev") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "spidev") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "1d6f7c7pkidnsw8jmgb07gynibmvifividhaacivhrwshc471k87") (f (quote (("spi" "spidev") ("i2c" "i2cdev") ("default" "i2c" "spi"))))))

