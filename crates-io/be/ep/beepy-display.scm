(define-module (crates-io be ep beepy-display) #:use-module (crates-io))

(define-public crate-beepy-display-0.1.0 (c (n "beepy-display") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1njxnfshnys7zalja7vvaakdpjaxzdfaa9g078ipfyvcgbdy20jr")))

(define-public crate-beepy-display-0.1.1 (c (n "beepy-display") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1wf2hjia12z6fmlrwq1b8nfkyz48hgx6mdznhnsj2sdz7cgz3xv0")))

