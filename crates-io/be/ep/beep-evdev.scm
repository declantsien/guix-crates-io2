(define-module (crates-io be ep beep-evdev) #:use-module (crates-io))

(define-public crate-beep-evdev-0.1.0 (c (n "beep-evdev") (v "0.1.0") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "01jmdk8zra8v9kdvh8z9n3cn0pz6idk0j72xqbmsa2al6237mlwm")))

(define-public crate-beep-evdev-0.1.1 (c (n "beep-evdev") (v "0.1.1") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "1zps770dy48q03zagz4fim4as20ij585nd2hfw1r9hi0sx4q0lvh")))

(define-public crate-beep-evdev-0.1.2 (c (n "beep-evdev") (v "0.1.2") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0g325i333ix0hjf9xgl4a6izfinh984h1gsdz3ip6v8y42wcvicp") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-beep-evdev-0.2.0 (c (n "beep-evdev") (v "0.2.0") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sggpqqcajch5sm2zc07lzny8ms3ycq66mfhn68p55dqiyz30qrf") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-beep-evdev-0.3.0 (c (n "beep-evdev") (v "0.3.0") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0iw3dzgrqwchn5kiskr5312klv629l7d033kjrci8d2a012iq9zp") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-beep-evdev-0.3.1 (c (n "beep-evdev") (v "0.3.1") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "053q9sxcgnqjpab87lykffvw0bg7mrgphp1v884kiqyrmcia4pi5") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-beep-evdev-0.3.2 (c (n "beep-evdev") (v "0.3.2") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15nbp4aanw73icgyc5p28dckf2sfdij9cpl6n693q9b7ysjbpalm") (s 2) (e (quote (("serde" "dep:serde"))))))

