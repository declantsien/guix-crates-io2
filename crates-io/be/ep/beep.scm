(define-module (crates-io be ep beep) #:use-module (crates-io))

(define-public crate-beep-0.1.0 (c (n "beep") (v "0.1.0") (d (list (d (n "dimensioned") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0n7an4z42rqksj406ahdhd62vscgmc6bfba3c2vw40pksdan8ikh")))

(define-public crate-beep-0.2.0 (c (n "beep") (v "0.2.0") (d (list (d (n "dimensioned") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (k 0)))) (h "0hlqxpfsmncg8346cq6rzf4z4y6ln9lfbx7j9ks7gqsn0js4afc5")))

(define-public crate-beep-0.2.1 (c (n "beep") (v "0.2.1") (d (list (d (n "dimensioned") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.2") (k 0)))) (h "0g0iq122g5czbdcfifpff3g4lhmjx1gvki043nr3a48ybcsfc679")))

(define-public crate-beep-0.3.0 (c (n "beep") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)))) (h "0nmx061sy2cbdjh2dq4rszsib271qq0vw13gd4jyaagswsw9mndd")))

