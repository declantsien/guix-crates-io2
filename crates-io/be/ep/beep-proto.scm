(define-module (crates-io be ep beep-proto) #:use-module (crates-io))

(define-public crate-beep-proto-0.0.1 (c (n "beep-proto") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1229606kqgyw8nhznr0003f5wnacma4ph92mp84m6fw22rpfwb90")))

(define-public crate-beep-proto-0.0.2 (c (n "beep-proto") (v "0.0.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "foreign-types-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.10.16") (d #t) (k 0)))) (h "0vinqng45f4bs94pq2mv6fg58cn1qpsb59l06fh7c3qppgqgdw51")))

