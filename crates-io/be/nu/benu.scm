(define-module (crates-io be nu benu) #:use-module (crates-io))

(define-public crate-benu-0.0.1-rc0 (c (n "benu") (v "0.0.1-rc0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0hxicsrv9941cis7d967r1x8qgkm1qrp2v0sggc9z3cdj4rq2dgv") (f (quote (("salt" "rand") ("default" "salt" "debug") ("debug"))))))

(define-public crate-benu-0.0.1-rc2 (c (n "benu") (v "0.0.1-rc2") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1v8aazh885pmpma4593ln47n12imchk60pwkhqsjr0g9km3kgxzp") (f (quote (("salt" "rand") ("default" "salt" "debug") ("debug"))))))

