(define-module (crates-io be lt belt-ctr) #:use-module (crates-io))

(define-public crate-belt-ctr-0.0.0 (c (n "belt-ctr") (v "0.0.0") (h "0xn466p9n5kvxy81vlzc7mapswmkya9vicvhnxykchj46g38i0nh")))

(define-public crate-belt-ctr-0.1.0 (c (n "belt-ctr") (v "0.1.0") (d (list (d (n "belt-block") (r "^0.1") (d #t) (k 0)) (d (n "belt-block") (r "^0.1") (d #t) (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "02s8q0hmavc47sgzhrxasqh9kkzpjki86yd7axcnmschzx51691z") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("alloc" "cipher/alloc")))) (r "1.56")))

