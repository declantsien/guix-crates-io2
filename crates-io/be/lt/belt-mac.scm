(define-module (crates-io be lt belt-mac) #:use-module (crates-io))

(define-public crate-belt-mac-0.0.0 (c (n "belt-mac") (v "0.0.0") (h "1z1gvbn6c2vfgkbdrmxxd2m39cffy2diivqkcqi3yg6bn96flyhr")))

(define-public crate-belt-mac-0.1.0 (c (n "belt-mac") (v "0.1.0") (d (list (d (n "belt-block") (r "^0.1") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.10.3") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "18kfr752bp0n79dccg40i1qnlpqi1an12s07wx8hszyw4i8i9hm2") (f (quote (("zeroize" "cipher/zeroize") ("std" "digest/std")))) (r "1.57")))

