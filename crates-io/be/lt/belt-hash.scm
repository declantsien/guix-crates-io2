(define-module (crates-io be lt belt-hash) #:use-module (crates-io))

(define-public crate-belt-hash-0.0.0 (c (n "belt-hash") (v "0.0.0") (h "1w1bhxbn2dlw2fpq5ggk3ma49qrl0l4v7rcbkihgcz2h4yl5ggrv")))

(define-public crate-belt-hash-0.1.0 (c (n "belt-hash") (v "0.1.0") (d (list (d (n "belt-block") (r "^0.1.1") (k 0)) (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "149x7k7z4gyhd5j9wm389lmwbhbcahr80775q7g97i7wzlxf93jd") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std")))) (r "1.57")))

(define-public crate-belt-hash-0.1.1 (c (n "belt-hash") (v "0.1.1") (d (list (d (n "belt-block") (r "^0.1.1") (k 0)) (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "12nv3prfh5j4fk9m7q0jdmaa05lmxvyl5ygdk80nwbs7p2rhbi7v") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std")))) (r "1.57")))

(define-public crate-belt-hash-0.2.0-pre.0 (c (n "belt-hash") (v "0.2.0-pre.0") (d (list (d (n "belt-block") (r "^0.1.1") (k 0)) (d (n "digest") (r "=0.11.0-pre.3") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "0yvz6n89hbb55zjjqm97qn7ay46b4fffx29jbcyq073pgb4zjnvi") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (r "1.71")))

(define-public crate-belt-hash-0.2.0-pre.1 (c (n "belt-hash") (v "0.2.0-pre.1") (d (list (d (n "belt-block") (r "^0.1.1") (k 0)) (d (n "digest") (r "=0.11.0-pre.4") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1dfmiqs08bma937f8gadgdqr47fvhcp6sfcyd5rkh6dy17qaq986") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (r "1.71")))

(define-public crate-belt-hash-0.2.0-pre.2 (c (n "belt-hash") (v "0.2.0-pre.2") (d (list (d (n "belt-block") (r "^0.1.1") (k 0)) (d (n "digest") (r "=0.11.0-pre.7") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1kpc43yr4xpr4hczx5dihyfpapri5si9y349rg6sy69vap12qrfw") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (r "1.71")))

(define-public crate-belt-hash-0.2.0-pre.3 (c (n "belt-hash") (v "0.2.0-pre.3") (d (list (d (n "belt-block") (r "^0.1.1") (k 0)) (d (n "digest") (r "=0.11.0-pre.8") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1ihf9gfzf5rh2wyx8n9jsp51c5vhmij807ryny599fahm2qw69sg") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (r "1.71")))

