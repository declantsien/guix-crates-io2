(define-module (crates-io be nj benjamin_batchly) #:use-module (crates-io))

(define-public crate-benjamin_batchly-0.1.0 (c (n "benjamin_batchly") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros" "rt"))) (d #t) (k 2)))) (h "0gfh6drwckq0fn7cafl2cdjfypc2d0xl0vi0220x1if3wbwbjfvn")))

(define-public crate-benjamin_batchly-0.1.1 (c (n "benjamin_batchly") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros" "rt"))) (d #t) (k 2)))) (h "06g4f3f7r0xjblw35v00aaffzggv15hzib84cp7k0wibsxl666xw")))

