(define-module (crates-io be dr bedrock-hematite-nbt) #:use-module (crates-io))

(define-public crate-bedrock-hematite-nbt-0.4.2 (c (n "bedrock-hematite-nbt") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bxw735m7gzb43rxm468q8nxgr9lyc64qqkpw293ss2a14p8lv9d") (f (quote (("default" "serde"))))))

