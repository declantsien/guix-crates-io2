(define-module (crates-io be dr bedrs_derive) #:use-module (crates-io))

(define-public crate-bedrs_derive-0.1.0 (c (n "bedrs_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0bh49nl45d42v5w1kpjvjvahnlsxxrynl6jfgkgqyyljh41ajnqp")))

