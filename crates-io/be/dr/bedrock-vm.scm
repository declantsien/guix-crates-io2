(define-module (crates-io be dr bedrock-vm) #:use-module (crates-io))

(define-public crate-bedrock-vm-0.1.0 (c (n "bedrock-vm") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)))) (h "1gp8jk9k2pkdijn1s5avk520im11idi5jw967z6znd1h9hpxdni2")))

(define-public crate-bedrock-vm-0.1.1 (c (n "bedrock-vm") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)))) (h "176g52nvy9chkn7sl7kyhnd6lcccq1vw8ahsnznzmy898va3qpmv")))

(define-public crate-bedrock-vm-0.1.2 (c (n "bedrock-vm") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)))) (h "19iyb0fwg79h0w86dxjiw4yvzfc5r15q9vdxynymgnj036gqv679")))

(define-public crate-bedrock-vm-0.2.0 (c (n "bedrock-vm") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)))) (h "17al78pxf8a9l8mxm9bzblllck6j1j4fv2qsxj49gahnnvv6whav")))

(define-public crate-bedrock-vm-0.2.1 (c (n "bedrock-vm") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)))) (h "0d1a3v62qb5akxq0d7f3zxy64j747dc7v7imbw56pkya0nmblng5")))

