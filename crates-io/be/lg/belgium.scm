(define-module (crates-io be lg belgium) #:use-module (crates-io))

(define-public crate-belgium-0.1.0 (c (n "belgium") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.19") (o #t) (d #t) (k 0)))) (h "0fkzb2wb8qsx99sndncj33krgvbml5li86bs7s05zxd46nkvrxk0") (f (quote (("default" "getopts"))))))

(define-public crate-belgium-0.2.0 (c (n "belgium") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.19") (o #t) (d #t) (k 0)))) (h "172kqr0r89a8jldcnn2d9cvplqahvx2w8fm1fc0mzxwvih2m08qs") (f (quote (("default" "getopts"))))))

