(define-module (crates-io be ss bessie_bin) #:use-module (crates-io))

(define-public crate-bessie_bin-0.0.0 (c (n "bessie_bin") (v "0.0.0") (d (list (d (n "bessie") (r "^0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0zh6rkk4y4m0vf9yhi721ndb039w1ppfj6rbd4hsi44isnrf33k5")))

(define-public crate-bessie_bin-0.0.1 (c (n "bessie_bin") (v "0.0.1") (d (list (d (n "bessie") (r "^0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1fy96p9d1miradhwz7dpl1kx8zijxd1yx3smkv9qzarnsx1gm790")))

(define-public crate-bessie_bin-0.0.2 (c (n "bessie_bin") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "bessie") (r "^0.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1niz08skh0i3ig4lx8m3nd66r8xrjdy84s1671hmq1w44ppdna2l")))

