(define-module (crates-io be ss bessie) #:use-module (crates-io))

(define-public crate-bessie-0.0.0 (c (n "bessie") (v "0.0.0") (d (list (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "page_size") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1msxhhwdik5wq5s9pmp3nb4l182gb02aazgmf734ndss6bc68kbq")))

(define-public crate-bessie-0.0.1 (c (n "bessie") (v "0.0.1") (d (list (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "page_size") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "15m1w5wbsd70xami3v0psh79dsvmdh0s621pyp2bnwaywwi7ch1h")))

(define-public crate-bessie-0.0.2 (c (n "bessie") (v "0.0.2") (d (list (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "page_size") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1zqqx43js3kxisbb4xws3nbwnha2im8y6rb1gavq9yqvmkmwsy8k")))

