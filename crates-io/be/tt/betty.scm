(define-module (crates-io be tt betty) #:use-module (crates-io))

(define-public crate-betty-0.1.0 (c (n "betty") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "04vwwbgr0rgf35f432zvqzr0qshrkbq7g3mcgg2qng3pq2qbnvsg")))

(define-public crate-betty-0.1.1 (c (n "betty") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1dl2lp8977jnqqgpax1xciphwqgk1axfhlzg4c1lk79w9ma1wqyr")))

