(define-module (crates-io be tt better_todos) #:use-module (crates-io))

(define-public crate-better_todos-1.1.0 (c (n "better_todos") (v "1.1.0") (d (list (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "11a5c1fh73k6wrb1xayjj3mq595h08lxnddl2h5pag427dw5h56w")))

(define-public crate-better_todos-1.1.1 (c (n "better_todos") (v "1.1.1") (d (list (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1rm0dg5iwblyb83dgnkq8i9c2rspzxw8rng2xwdx54aaa7a6jma4")))

(define-public crate-better_todos-1.1.2 (c (n "better_todos") (v "1.1.2") (d (list (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1sa0n985nypjk56zikzv9z51x2paf9ijg4v45wa6j4hmf1bc5lpy")))

