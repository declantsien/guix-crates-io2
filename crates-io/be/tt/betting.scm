(define-module (crates-io be tt betting) #:use-module (crates-io))

(define-public crate-betting-0.1.0 (c (n "betting") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0i95prcyfr0vlcja0zysgs60afp2fmcbjkh5jqjyrv8fq8gpq1iv")))

(define-public crate-betting-0.1.1 (c (n "betting") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0pbh4mkj1i9zcrqk80jlqc6pqhzfpl6x5byyz5i5m5nh55z9j3vq")))

(define-public crate-betting-0.1.3 (c (n "betting") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)))) (h "19w1il5vad5bxcapr8hq4pd78fsnv77klkk4s6fb1jhy2llyw228")))

(define-public crate-betting-0.1.4 (c (n "betting") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hn9sd15kvda0gmk5apfnd7wmqq2f9cdl0rc4xrn652abm3l5qkf")))

(define-public crate-betting-0.1.5 (c (n "betting") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ldjgcbifa01vhn7q0shxq5ic3n0fma2fs9njh2rny0rz3lscapn")))

(define-public crate-betting-0.1.6 (c (n "betting") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11k6kqwnw6s09kzmm2vs33ar0ay2ikdwzzqb8y9sd8ml30csssim")))

(define-public crate-betting-0.1.7 (c (n "betting") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0slp1lx6lx2s6bss99h9d9hdcilw135f99jxhap9swbwdsmnd4c8")))

(define-public crate-betting-0.2.0 (c (n "betting") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w79ydrnpszgkpg4w8qckrk5slmn8l9j31mi72cigxv4bfckgbwn")))

(define-public crate-betting-0.2.1 (c (n "betting") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07spzrcswz8g2s92jy0shfa3jvzggm2912lxgf6253ddx5xabycz")))

(define-public crate-betting-0.2.2 (c (n "betting") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0snv345z56w6yhkiv0yyblr2k125dk2inyidr6fgsmv6bv82cfn4")))

(define-public crate-betting-0.2.3 (c (n "betting") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vyr2pg5n81rdj1kn3ixgbn2zyin674s7mdw8jl2yr47nid43s5p")))

(define-public crate-betting-0.2.4 (c (n "betting") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s7mk6vcifvf9ch2z3v204shdhvgn4a8vr5siwnkmrjp01b5w2k4")))

(define-public crate-betting-0.2.5 (c (n "betting") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r855xiyfc9ksqqdvgiwsymvrymbm0y7kksmsg3wlwydr9jzx9y3")))

