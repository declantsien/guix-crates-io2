(define-module (crates-io be tt better_peekable) #:use-module (crates-io))

(define-public crate-better_peekable-0.1.2 (c (n "better_peekable") (v "0.1.2") (h "1qxm6hjwkkflk5xd5ym47gb7yrip1sfwv6bgd542n6fx2mbs67hv")))

(define-public crate-better_peekable-0.1.3 (c (n "better_peekable") (v "0.1.3") (h "1zryzp178hp8jhzpnm7vx54sv80217j8r0appss9pa6l52zsvr1g")))

(define-public crate-better_peekable-0.2.1 (c (n "better_peekable") (v "0.2.1") (h "1ydfdlfprz81gdsp3l7d8izgj2fycrs4s9r6h7ysp9685s6hwc5z") (y #t)))

(define-public crate-better_peekable-0.2.0 (c (n "better_peekable") (v "0.2.0") (h "11bl2jh7rgbx6rn1fbpm7izwq392blaj654m4rhg0sragdbn95vj")))

(define-public crate-better_peekable-0.2.2 (c (n "better_peekable") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jk46r5dbarhy2slbc392w5bml423aqnhk8l7z0wpg0rc5qws5md")))

(define-public crate-better_peekable-0.2.3 (c (n "better_peekable") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vh70f667mpvv4bfh0qyaq5mp5snlmy28dazw2ikpvw4bry8x0jj")))

(define-public crate-better_peekable-0.2.4 (c (n "better_peekable") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jfyz8vgjhk6ql70xgcr2q720c3whqy296j9ci14hkcjxjd2v6k2")))

