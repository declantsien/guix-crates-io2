(define-module (crates-io be tt better-bae-macros) #:use-module (crates-io))

(define-public crate-better-bae-macros-0.1.7 (c (n "better-bae-macros") (v "0.1.7") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1q51da6n0qhdrb58m231b76i6a5fpxf47gy18p4mh2q2j4rr02hf")))

(define-public crate-better-bae-macros-0.1.8 (c (n "better-bae-macros") (v "0.1.8") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1z9864k0m5d6z95kiqp449v9ajwj40k7jzxmj6i2ch18c43nx065")))

(define-public crate-better-bae-macros-0.1.9 (c (n "better-bae-macros") (v "0.1.9") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1g1dxcixvsyw50z1936abf4h3zrqb61yc8i4hh89kbqiq92323vb")))

