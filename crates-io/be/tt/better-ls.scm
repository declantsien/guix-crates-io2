(define-module (crates-io be tt better-ls) #:use-module (crates-io))

(define-public crate-better-ls-0.0.1 (c (n "better-ls") (v "0.0.1") (d (list (d (n "comfy-table") (r "^6.0.0") (d #t) (k 0)))) (h "0j0i1r2vbc9cbpb8v6f7r2nph48mgha1q7287y9jcwlg88i7cq6s")))

(define-public crate-better-ls-0.0.2 (c (n "better-ls") (v "0.0.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "comfy-table") (r "^6.0.0") (d #t) (k 0)))) (h "0pda3as8bvgwc06k2zqf4ci9d6g7pa5cpz1igxdyx8q5cn6fz5yp")))

