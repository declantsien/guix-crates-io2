(define-module (crates-io be tt better-todo-cli) #:use-module (crates-io))

(define-public crate-better-todo-cli-0.1.0 (c (n "better-todo-cli") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0yri0868mlh0ibfa80bhd8r03x2kl6v7vh9j8iv3sjkmcpdqfxli")))

(define-public crate-better-todo-cli-0.1.1 (c (n "better-todo-cli") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0aghvsx4py2wp4lsvyf08sgh8pbj6ny7rxyc8sy3g65w8q3bx55c")))

