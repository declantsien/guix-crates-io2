(define-module (crates-io be tt better) #:use-module (crates-io))

(define-public crate-better-0.1.0 (c (n "better") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1dmrbhav2vk3pjxf6i6n0x60mviamb88r733843cmiys2clx0kyi")))

(define-public crate-better-0.1.1 (c (n "better") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1v70h1ps14l9brv3vdqh0y8qx44fmy3ddrpzi6fxqfmg91m9s0ka")))

