(define-module (crates-io be tt better-macro) #:use-module (crates-io))

(define-public crate-better-macro-1.0.0 (c (n "better-macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1cxblnawa0vdcxqzyb00r9kl12hzccv15nzrhvxwl6lflrz2p4j2")))

(define-public crate-better-macro-1.0.1 (c (n "better-macro") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1yx4q1n21n84sbvxplnwad7prmv61kpyphm3143swks2qra0jc5w")))

(define-public crate-better-macro-1.0.2 (c (n "better-macro") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1vap2sl0zi3n3rk91w3n81ibvnx60d5yymbviwq7ldd3zny3r058")))

(define-public crate-better-macro-1.0.4 (c (n "better-macro") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0xy149n3vafqzw0yyrk0hizlzmpblc5l83f57r5by3nqfmdswf0m")))

