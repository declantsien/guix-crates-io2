(define-module (crates-io be tt better-as) #:use-module (crates-io))

(define-public crate-better-as-0.1.0 (c (n "better-as") (v "0.1.0") (h "0wa3s9pwsikvcq2plm8szjg7cff49gpf4aki2q9md6f20k6wn7xg") (y #t)))

(define-public crate-better-as-0.2.0 (c (n "better-as") (v "0.2.0") (h "1f5jy1kgva9yd2xxyjwi6rp4v6lyhp9gmdirxwjlym421kvkrf76") (f (quote (("std") ("default" "std"))))))

