(define-module (crates-io be tt betterops) #:use-module (crates-io))

(define-public crate-betterops-0.1.0 (c (n "betterops") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0pflzlxhjhkajm2nslvnlhgr919146v849b72hm9hai4d2gcz5bp")))

(define-public crate-betterops-0.1.1 (c (n "betterops") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0653cl16c5ggpn8jafy133c9mcv5p9ygxg6lgiwcnvsg0gy9n5i2")))

