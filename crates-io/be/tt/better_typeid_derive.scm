(define-module (crates-io be tt better_typeid_derive) #:use-module (crates-io))

(define-public crate-better_typeid_derive-0.1.0 (c (n "better_typeid_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h4i9p7056h2ihywv8whbdg8qnvpb8y59iwhrk9kyjm0x402ymyy")))

(define-public crate-better_typeid_derive-0.1.1 (c (n "better_typeid_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qr6kaqxarf1g7fr3iin7lf53lzbq8676vznsfvh0lya2awfrvix")))

