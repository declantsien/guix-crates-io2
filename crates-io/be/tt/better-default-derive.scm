(define-module (crates-io be tt better-default-derive) #:use-module (crates-io))

(define-public crate-better-default-derive-0.1.0 (c (n "better-default-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0xqc4dj5hrs06az3zvfs7hgasz2dxdycjlr9fnwjdjhdxamz02zw")))

(define-public crate-better-default-derive-0.1.1 (c (n "better-default-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0nlq1bgd4mxsbbr4iabwwj7jsxszhf2q14ww671w1vjsxid0bsyg")))

