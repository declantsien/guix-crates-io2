(define-module (crates-io be tt better-qs) #:use-module (crates-io))

(define-public crate-better-qs-2.2.0 (c (n "better-qs") (v "2.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "17l4fghlc6iqy4ssn9kj6mlbj6siq55zlfgksqv42dqn6vad0srk") (f (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

(define-public crate-better-qs-2.3.0 (c (n "better-qs") (v "2.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0sq99wml6y93hiygw7alg4b5ppsw9rkbypazbdbwdzqwl5b4sn1k") (f (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

(define-public crate-better-qs-2.3.1 (c (n "better-qs") (v "2.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1r7s6r601aam91pahh6fdk1il7zzs1nph7q6lxswin4jxy8xjrm9") (f (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

