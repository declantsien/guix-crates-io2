(define-module (crates-io be tt better-web-view) #:use-module (crates-io))

(define-public crate-better-web-view-0.5.0 (c (n "better-web-view") (v "0.5.0") (d (list (d (n "boxfnonce") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0") (d #t) (k 0)) (d (n "webview-sys") (r "^0.1.1") (d #t) (k 0)))) (h "09wzcw14mq64kgn6y4ihszlfkdg0xxpmznipnr7k0rfyf57a8srg") (f (quote (("default" "V1_30") ("V1_30"))))))

(define-public crate-better-web-view-0.6.0 (c (n "better-web-view") (v "0.6.0") (d (list (d (n "boxfnonce") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0") (d #t) (k 0)) (d (n "webview-sys") (r "^0.1.1") (d #t) (k 0)))) (h "10l0v6s9cgc0rnlkxpc1af7wjdr2hmbw384fxk5byyn24448fxf0") (f (quote (("default" "V1_30") ("V1_30"))))))

