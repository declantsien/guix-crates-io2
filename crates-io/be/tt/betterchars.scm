(define-module (crates-io be tt betterchars) #:use-module (crates-io))

(define-public crate-betterchars-0.1.0 (c (n "betterchars") (v "0.1.0") (h "08bclmw5ywsqm41j1wbgllm9bkd848pwrry69b1cbn3p4abgwn7w")))

(define-public crate-betterchars-0.1.1 (c (n "betterchars") (v "0.1.1") (h "1ymwl5xgznndf3mgvnb2advjgmni2fbh1xlvwnhgnbs8fgxpc078")))

(define-public crate-betterchars-0.1.2 (c (n "betterchars") (v "0.1.2") (h "0f0zssk4v0g5k7dqf4hpx1y0q2dxr0clm7rdxqpyy3n779vx1ysy")))

(define-public crate-betterchars-0.1.3 (c (n "betterchars") (v "0.1.3") (h "19h5hi1c8x554c4knmnfmmnffvsznm1s2qcfvv2mmgpdbfiibb5a")))

(define-public crate-betterchars-0.1.4 (c (n "betterchars") (v "0.1.4") (h "1xvxsnw661bjqx0dz87v1nrsmh34ss9vf8wdcymc45nhy0nx0042")))

