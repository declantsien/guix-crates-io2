(define-module (crates-io be tt betterschool-sdk) #:use-module (crates-io))

(define-public crate-betterschool-sdk-1.0.0 (c (n "betterschool-sdk") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1p3np78azwwj01dg0vdynngwk9xisfkqxv3s0914kkggdxaxxxhg")))

(define-public crate-betterschool-sdk-1.0.1 (c (n "betterschool-sdk") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1rnlrbrhapqjy4f76z0j4lcdqi40xvx00srln2hrdfn7j7pknniy")))

(define-public crate-betterschool-sdk-1.0.2 (c (n "betterschool-sdk") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1lqgrw28689rcb203ijrla9z2d4fmymcyx8qwzs9kxw2jm03k9rd")))

(define-public crate-betterschool-sdk-1.0.3 (c (n "betterschool-sdk") (v "1.0.3") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "183l56zddvnwqrfxmfl4sz6l4ziycj1rhi7159rlh98ymvnlxviw")))

(define-public crate-betterschool-sdk-1.0.4 (c (n "betterschool-sdk") (v "1.0.4") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "13krvx8lggxg2annm2fzdpyszj7jbkx0jms9j7ga1ifm5p6lvn7h")))

