(define-module (crates-io be tt better_term) #:use-module (crates-io))

(define-public crate-better_term-0.1.0 (c (n "better_term") (v "0.1.0") (h "1608ygjx62mnz81hv7qnpnrd25319lz1f64yxrrzb3jpag5az2kz")))

(define-public crate-better_term-0.1.1 (c (n "better_term") (v "0.1.1") (h "0ianpvsrxbg48q2d90sghbscq4w5s5yv4jf7p85z1a22aynag1fj")))

(define-public crate-better_term-1.0.0 (c (n "better_term") (v "1.0.0") (h "06bgvh1nvra416n4hxz2qh1ycxafqxh7alsif9j128jcg14l51qq")))

(define-public crate-better_term-1.0.1 (c (n "better_term") (v "1.0.1") (h "0ifh2vclzrbvqqxxffkfg7sxf4j71fp621jx30pma70j7kb9v576")))

(define-public crate-better_term-1.0.2 (c (n "better_term") (v "1.0.2") (h "0w42pyv1mhbfr7ih5h5ji3p0iaxz963wvwgl21vc6pma26r2v87b")))

(define-public crate-better_term-1.0.3 (c (n "better_term") (v "1.0.3") (h "1sja88q9f92dkygfv1yirg8n1xka3zic45yjnq8s4rialwhr5xbm")))

(define-public crate-better_term-1.0.4 (c (n "better_term") (v "1.0.4") (h "0a9cw6fm9qshgs4j2wr7acindkp957j0svd110hf9lxlsyclvcs4")))

(define-public crate-better_term-1.2.0 (c (n "better_term") (v "1.2.0") (h "1fsaxw1r3qbv17vq9k3w7giw2rc8nsm9xl66jhiblpp5k1r8yh6k") (f (quote (("input") ("default" "input")))) (y #t)))

(define-public crate-better_term-1.3.0 (c (n "better_term") (v "1.3.0") (h "13mj37jb9pxd3zypknx9b37maqb1hj08gd0air2i2wws3izxgw49") (f (quote (("input") ("default" "input")))) (y #t)))

(define-public crate-better_term-1.4.0 (c (n "better_term") (v "1.4.0") (h "039czb2mvd9spj2k7xw89pp8dlwd45wxj0h4r6x89slzb6d7cvjf") (f (quote (("input") ("default" "input")))) (y #t)))

(define-public crate-better_term-1.5.0 (c (n "better_term") (v "1.5.0") (h "1z9n16vi7csxrgln1abh64mn3v1sjxfd7gyjz0b92a45kasfryx1") (f (quote (("input") ("default" "input")))) (y #t)))

(define-public crate-better_term-1.3.2 (c (n "better_term") (v "1.3.2") (h "19ssj9him3h0aspajjxxazfm79ixa73crlhkmxlaj3gaafyanqr2") (f (quote (("input") ("default" "input"))))))

(define-public crate-better_term-1.3.3 (c (n "better_term") (v "1.3.3") (h "13jkdkalr1w49k2xcs75c6dl8kzih07943p5hf6c9p9wn8d2yq37") (f (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1.3.4 (c (n "better_term") (v "1.3.4") (h "199bmgc42s2r3j8wmcynmfp496qqb9wb03xncgm37r7six2d05qz") (f (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1.3.5 (c (n "better_term") (v "1.3.5") (h "1z54xys3kb5whvfn1kxh5h4dvk7yxqb9lj1h7ipgywphyw3hz5jb") (f (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1.3.6 (c (n "better_term") (v "1.3.6") (h "19min9iwqp77i6rf6y2k5dq4m25pckny5vaprbbfd556scbb5rsa") (f (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1.3.7 (c (n "better_term") (v "1.3.7") (h "0nrbpmwh99si2d2q9rzjghx9dm44hggfpsziyl6m5y4r2w8gkjc8") (f (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1.3.8 (c (n "better_term") (v "1.3.8") (h "1ji207xh3hsfikcydqhxw3g8wyi8rh9qwa4xnbsi1rb7mgy3lawd") (f (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1.3.9 (c (n "better_term") (v "1.3.9") (h "1i5q0yz9z04wq271rhbimnnjijs5im5wijdx26wm48xlhk37pw0g") (f (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1.4.1 (c (n "better_term") (v "1.4.1") (h "11h8gcs1p27yfwkykivc7rxvx7m33zadcwnpfpc15r0gi2h60xyk") (f (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1.4.2 (c (n "better_term") (v "1.4.2") (h "19a7xm3a2ksnnf5k8hd1sbfafq5xk1avz7560iqjxlc0ibgg8y43") (f (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1.4.3 (c (n "better_term") (v "1.4.3") (h "1w4qh9azl0ynkpncxys6byp69fzvvbh83by133w7q5a79n91dbzy") (f (quote (("output") ("input") ("fancy") ("default" "input" "output")))) (y #t)))

(define-public crate-better_term-1.4.4 (c (n "better_term") (v "1.4.4") (h "00m1gl5r89m7c5drpz1b3ibqhb15l4ka84fq6kh1z693wgfypp1z") (f (quote (("output") ("input") ("fancy") ("default" "input" "output"))))))

(define-public crate-better_term-1.4.5 (c (n "better_term") (v "1.4.5") (h "1fjhc7pjdjwz9y45zmqvrk6c446kslkgxbzvdal1zj0z7xf50cbb") (f (quote (("output") ("input") ("fancy") ("default" "input" "output"))))))

(define-public crate-better_term-1.4.6 (c (n "better_term") (v "1.4.6") (h "0sd7ijgfwmlw2krndp924hkzav2i3syqlkm3lbicd67wcrnfim5i") (f (quote (("output") ("input") ("fancy") ("default" "input" "output"))))))

(define-public crate-better_term-1.4.61 (c (n "better_term") (v "1.4.61") (h "08kjblx1zm0hyp3qqrax61lxpm7844cr2yjzic12w8h6mcxnqnwm") (f (quote (("output") ("input") ("fancy") ("default" "input" "output"))))))

