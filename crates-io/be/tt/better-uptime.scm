(define-module (crates-io be tt better-uptime) #:use-module (crates-io))

(define-public crate-better-uptime-0.1.0 (c (n "better-uptime") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1y3vb9q0q9r8smfipkrg5r8fdwbrvjgdil8psv88fyaclfibyczh")))

