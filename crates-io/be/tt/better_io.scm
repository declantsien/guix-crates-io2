(define-module (crates-io be tt better_io) #:use-module (crates-io))

(define-public crate-better_io-0.0.0-alpha.0 (c (n "better_io") (v "0.0.0-alpha.0") (h "05hiij9ghrqwwa9n6yirrb34wwqjik2v19pl97yrlbzqli8bvzyf")))

(define-public crate-better_io-0.0.0 (c (n "better_io") (v "0.0.0") (h "1sapww55r4am6yykcyvlv3sdms12nsaa1ddcixczpar4ibxk6lb5")))

(define-public crate-better_io-0.0.1 (c (n "better_io") (v "0.0.1") (h "1x2sqcq2jfig352k50gmgkcr97vig2y8x5x67v2zk374q239rnbl")))

(define-public crate-better_io-0.1.0 (c (n "better_io") (v "0.1.0") (h "0a0dmxv14vn6zd8fhsmwji418xqbaclzz3bzl2r11fp7j5zy3zcj")))

