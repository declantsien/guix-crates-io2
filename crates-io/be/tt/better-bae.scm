(define-module (crates-io be tt better-bae) #:use-module (crates-io))

(define-public crate-better-bae-0.1.7 (c (n "better-bae") (v "0.1.7") (d (list (d (n "better-bae-macros") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10gq2s8w8gfia7arih2k75yh9h946jz913yd0kwsq55dzj4kzxjm")))

(define-public crate-better-bae-0.1.8 (c (n "better-bae") (v "0.1.8") (d (list (d (n "better-bae-macros") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ns1169yal7xhm33vnv69spwi1j0yijavq4im784mllhivwjrwxh")))

(define-public crate-better-bae-0.1.9 (c (n "better-bae") (v "0.1.9") (d (list (d (n "better-bae-macros") (r "^0.1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i9b8al1kzs7xd7gcyyx9nikpycd8x8ahfspwsw9r3avabq5c8n4")))

