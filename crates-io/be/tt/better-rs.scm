(define-module (crates-io be tt better-rs) #:use-module (crates-io))

(define-public crate-better-rs-0.1.0 (c (n "better-rs") (v "0.1.0") (h "1qgh0343xb8vjwfw5wngm5ajzi5shx6hxd0ws3kpjci0lk43k8ly") (y #t)))

(define-public crate-better-rs-0.1.1 (c (n "better-rs") (v "0.1.1") (h "12ixkjd54kcs9pbgjwhw5nh16qlkdllb16fhhvv1dvvghqymp7fn") (y #t)))

(define-public crate-better-rs-0.1.2 (c (n "better-rs") (v "0.1.2") (h "1klv300za0kdrzwka925k1jfrhz8ix5lxjhazpmrj3x50v1wcf0g") (y #t)))

(define-public crate-better-rs-0.1.3 (c (n "better-rs") (v "0.1.3") (h "151ck2aj6qysvb7sy75ndsvwpcn8i05hv227021fajpdm3kwg61g") (y #t)))

(define-public crate-better-rs-0.1.4 (c (n "better-rs") (v "0.1.4") (h "14s9yyiidihacpmghhpfkhfizgf9vla3ybzbp2bf80qji3bx22p5") (y #t)))

(define-public crate-better-rs-0.1.5 (c (n "better-rs") (v "0.1.5") (h "1z0zimi86jdqfvzbdkvk0dr4pg53p8l5vvhf3paypb0z0qrfq30x") (y #t)))

(define-public crate-better-rs-0.1.6 (c (n "better-rs") (v "0.1.6") (h "04cpi955iwq3fv8zrzniwij93bsdfps6nx4dcbsx08bcv8i1hz3b") (y #t)))

(define-public crate-better-rs-0.1.7 (c (n "better-rs") (v "0.1.7") (h "1j3m622nbzls0d2p3lkbp6x86pi37p5r134fmsab2yk4qkbhgzsr") (y #t)))

