(define-module (crates-io be tt better_any) #:use-module (crates-io))

(define-public crate-better_any-0.1.0 (c (n "better_any") (v "0.1.0") (d (list (d (n "better_typeid_derive") (r "=0.1.0") (d #t) (k 0)) (d (n "macrotest") (r "=1.0") (d #t) (k 2)) (d (n "trybuild") (r "=1.0") (d #t) (k 2)))) (h "180bz02vhj78vil28i1bxbbsphmjdgr3150s2f29y7m4d8mmcsqv")))

(define-public crate-better_any-0.1.1 (c (n "better_any") (v "0.1.1") (d (list (d (n "better_typeid_derive") (r "=0.1.1") (d #t) (k 0)) (d (n "macrotest") (r "=1.0") (d #t) (k 2)) (d (n "trybuild") (r "=1.0") (d #t) (k 2)))) (h "186l77d2db2d48r72gn5956g10q841hvdz4yw4jwf5vwjfyswndk")))

(define-public crate-better_any-0.2.0-dev.1 (c (n "better_any") (v "0.2.0-dev.1") (d (list (d (n "better_typeid_derive") (r "=0.1.1") (o #t) (d #t) (k 0)) (d (n "macrotest") (r "=1.0") (d #t) (k 2)) (d (n "trybuild") (r "=1.0") (d #t) (k 2)))) (h "0w8r1vdfdywym2gj49vkpxmlbd01qd546112bdql1gqhb4nf3l6a") (f (quote (("nightly") ("derive" "better_typeid_derive") ("default" "any") ("any"))))))

(define-public crate-better_any-0.2.0 (c (n "better_any") (v "0.2.0") (d (list (d (n "better_typeid_derive") (r "=0.1.1") (o #t) (d #t) (k 0)) (d (n "macrotest") (r "=1.0") (d #t) (k 2)) (d (n "trybuild") (r "=1.0") (d #t) (k 2)))) (h "0gfb086dzgs0vdv08kl6q8bfrzqymf40cpk8wvxiyyga833yp58p") (f (quote (("nightly") ("derive" "better_typeid_derive") ("default" "any") ("any"))))))

