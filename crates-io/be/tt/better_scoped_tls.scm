(define-module (crates-io be tt better_scoped_tls) #:use-module (crates-io))

(define-public crate-better_scoped_tls-0.1.0 (c (n "better_scoped_tls") (v "0.1.0") (d (list (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)))) (h "08r1i91hblc6rp3js2vbdawcqxwgxnwd134yn6iqms9rxk6qwgmp")))

(define-public crate-better_scoped_tls-0.1.1 (c (n "better_scoped_tls") (v "0.1.1") (d (list (d (n "scoped-tls") (r "^1.0.1") (d #t) (k 0)))) (h "1ph14yy6h8is61l2sy6vx97knrj5zn9z04daxi5bn1zvng4xqkkr")))

