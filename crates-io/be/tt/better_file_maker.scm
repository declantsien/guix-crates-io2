(define-module (crates-io be tt better_file_maker) #:use-module (crates-io))

(define-public crate-better_file_maker-0.1.0 (c (n "better_file_maker") (v "0.1.0") (h "003kpvxpr7k1bj1m371fyp8ykd88hfj84d2r80wc4i31g0y9il2c")))

(define-public crate-better_file_maker-0.1.1 (c (n "better_file_maker") (v "0.1.1") (h "01fd7lvxbpnb6b0ln8i7a3aaax2mlk2rfqipiqfz9kd2nmagh8rb")))

(define-public crate-better_file_maker-0.1.2 (c (n "better_file_maker") (v "0.1.2") (h "018aicji6rpwp48b1h7pa0xj0wk6r5hzrx6nnmybvgk7imfj0pl9")))

(define-public crate-better_file_maker-0.1.3 (c (n "better_file_maker") (v "0.1.3") (h "1c582ac0h7kxp17fp000lfiijjnsxqgc94w1ddibsw9m6x9zng5m")))

(define-public crate-better_file_maker-0.1.4 (c (n "better_file_maker") (v "0.1.4") (h "0yq0p2fhlxgmnwasa3iq0pwg7f2n38b3szqwi0npprfvp66d4wcv")))

(define-public crate-better_file_maker-0.1.5 (c (n "better_file_maker") (v "0.1.5") (h "0g9zxzbwds79flr8mh4r12kap52qsl1m1ffbx35z05phf46aq03j")))

(define-public crate-better_file_maker-0.1.6 (c (n "better_file_maker") (v "0.1.6") (h "1jskvg1bpn1nqscj5b8n1ky0bazqv0n9mbbsl5rsbm2dcgqj3yak")))

(define-public crate-better_file_maker-0.1.7 (c (n "better_file_maker") (v "0.1.7") (h "08vlvgrjizv3c3ynv7krdf6v14d5hz4bvzwc641gbayvgqcabq22")))

(define-public crate-better_file_maker-0.1.8 (c (n "better_file_maker") (v "0.1.8") (h "0rqsrd5s1i5ccv6fyih56127267xv8s6mka8x19xniphji96kpm7")))

(define-public crate-better_file_maker-0.1.9 (c (n "better_file_maker") (v "0.1.9") (h "0grlr07azrrbqnfsq5gv6xkv4c8ajg3zg6ij40an39b3ncfbj7i6")))

