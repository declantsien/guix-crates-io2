(define-module (crates-io be tt better-debug) #:use-module (crates-io))

(define-public crate-better-debug-1.0.0 (c (n "better-debug") (v "1.0.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1j3zr5hrq5a360ddwyh5xiafa612fzdrh0n4b411sssaaxnm07nx")))

(define-public crate-better-debug-1.0.1 (c (n "better-debug") (v "1.0.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0qyjymxcwaxqiy2f7a8y5bvi2l49zkrc5vwz48g1nqw2c2nwhpsx")))

