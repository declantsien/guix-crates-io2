(define-module (crates-io be tt better-path) #:use-module (crates-io))

(define-public crate-better-path-0.1.0 (c (n "better-path") (v "0.1.0") (d (list (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1d9hlf5y7ic630n3vlfsn7msr7760fdikpdf8rgh1md4f3zml1j0") (f (quote (("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-better-path-0.0.1 (c (n "better-path") (v "0.0.1") (d (list (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "049wdghy3wljhwlbrh6vs0a0q8b06brz8px2g35wnlyy5sanny9n") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

