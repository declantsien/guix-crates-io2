(define-module (crates-io be tt better-panic) #:use-module (crates-io))

(define-public crate-better-panic-0.1.0 (c (n "better-panic") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.30") (f (quote ("std" "libbacktrace" "libbacktrace" "libunwind" "dladdr"))) (k 0)) (d (n "console") (r "^0.7.7") (d #t) (k 0)) (d (n "syntect") (r "^3.2.0") (o #t) (d #t) (k 0)))) (h "1hnzvxixs67yh0bx4nw84yxqmj909cdmcgvbm9nmjzj0rffn9jjv")))

(define-public crate-better-panic-0.1.1 (c (n "better-panic") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.30") (f (quote ("std" "libbacktrace" "libbacktrace" "libunwind" "dladdr"))) (k 0)) (d (n "console") (r "^0.7.7") (d #t) (k 0)) (d (n "syntect") (r "^3.2.0") (o #t) (d #t) (k 0)))) (h "15s5riwriadq8y55gipd9vwnxsx6idj68wxhnj0x1snsxjp1akkk")))

(define-public crate-better-panic-0.1.2 (c (n "better-panic") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.30") (f (quote ("std" "libbacktrace" "libbacktrace" "libunwind" "dladdr" "dbghelp"))) (k 0)) (d (n "console") (r "^0.7.7") (d #t) (k 0)) (d (n "syntect") (r "^3.2.0") (o #t) (d #t) (k 0)))) (h "0mikas34acbpa5x3rj6wavic7mqg965bf8x9hlgnk090xdq4jwb4")))

(define-public crate-better-panic-0.2.0 (c (n "better-panic") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.37") (d #t) (k 0)) (d (n "console") (r "^0.9.0") (k 0)) (d (n "syntect") (r "^3.2.0") (o #t) (d #t) (k 0)))) (h "0xl48v6pd9ys7wp0ni62i6q73xpd1nhf92z09sjc9n3lrj0ac4ix")))

(define-public crate-better-panic-0.3.0 (c (n "better-panic") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3.37") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (k 0)) (d (n "syntect") (r "^4.6.0") (o #t) (d #t) (k 0)))) (h "0djh7qs39z0mbkzxs4nrc9ngnyjpsxq67lqfv75q91i63b8y3abg")))

