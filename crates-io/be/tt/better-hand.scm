(define-module (crates-io be tt better-hand) #:use-module (crates-io))

(define-public crate-better-hand-0.1.0 (c (n "better-hand") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "0pv8xlpdq09iiw5m6ipn7brk6pii1ygmbl10zg4f05bn9zn5mvrv")))

(define-public crate-better-hand-0.1.1 (c (n "better-hand") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "14masfpkk1aimqmpq26cb7fkxxd46c9zfvjxdhrwnkpbhrlm7hk2")))

(define-public crate-better-hand-0.2.0 (c (n "better-hand") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "0v8f72vnpdlxzqcf9q9ngw9smpqj0jv1l9fcpvkgqmxkm69b71ml")))

(define-public crate-better-hand-1.0.0 (c (n "better-hand") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "0ad3vnxrrpc7xpd43g2fh9fdgb9nl8vp745qri0gfpdkmphck12f")))

(define-public crate-better-hand-1.0.1 (c (n "better-hand") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "1xbzrzrqgycpznpfx93krdx2nz98v171gxxgfn6rgzjq21bnr6q3")))

(define-public crate-better-hand-1.1.1 (c (n "better-hand") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "0fc10jzzhf8kmrk1pbhw5nh1irxb8drn77g1df8r0bxzaff56057")))

(define-public crate-better-hand-1.1.2 (c (n "better-hand") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "1mdjlq3n5490vdj2gikk4yp6gmw5q02myxca2i32icq7845lxmhr")))

(define-public crate-better-hand-1.1.3 (c (n "better-hand") (v "1.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "1m6xaib5i23c16h7h4jw6s26p7b1208k86hgq69hfadj412d4l6p")))

(define-public crate-better-hand-1.1.4 (c (n "better-hand") (v "1.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "1l1xgf73pfy0nmswgdciqm4jcdqlpqvvq61fml93x7nkck83yfxw")))

(define-public crate-better-hand-1.1.5 (c (n "better-hand") (v "1.1.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "0j3bkbxraqv6snq6ffjp9qrmmak2ax6sz9h7l6p8rl2i6b374yii")))

(define-public crate-better-hand-1.1.6 (c (n "better-hand") (v "1.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "1mn4jrgaq19whrfyvz444mdi1zkv2c78w5xsbb3z2dbvv77ab4y7")))

(define-public crate-better-hand-1.1.7 (c (n "better-hand") (v "1.1.7") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "0sxjgwp0kf2ybmy572w9n6rqz2kgz6m4z8n8ksaka7s87f11s4vg")))

(define-public crate-better-hand-1.1.8 (c (n "better-hand") (v "1.1.8") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rs_poker") (r "^1.0.0") (d #t) (k 0)))) (h "02bkz59snc3pr91anny1bhx7ifqzyf9pza0yfzxzgzyj3s407xpm")))

