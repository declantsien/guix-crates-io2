(define-module (crates-io be tt better-stopwatch) #:use-module (crates-io))

(define-public crate-better-stopwatch-0.1.0 (c (n "better-stopwatch") (v "0.1.0") (h "1k8y13yc4lzg81dqll1dq0r3885c8ba546j5p5f64hxfsjzicgdm")))

(define-public crate-better-stopwatch-0.2.0 (c (n "better-stopwatch") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0izg1sq7smkh3zbn5wn8rmimqfvv9javr26jnqqv6vxniiplwf9y") (y #t)))

