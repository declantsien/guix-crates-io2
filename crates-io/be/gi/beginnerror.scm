(define-module (crates-io be gi beginnerror) #:use-module (crates-io))

(define-public crate-beginnerror-0.1.0 (c (n "beginnerror") (v "0.1.0") (h "1bcxgdh8wbp91zf7g2zvr2dmv373s34n323y3d6c6ka7qccbglxh") (y #t)))

(define-public crate-beginnerror-0.1.1 (c (n "beginnerror") (v "0.1.1") (h "0x79ri159bay38avnnpmxiag60didd467xg3vzvl4bfzgljxx7hb") (y #t)))

(define-public crate-beginnerror-0.1.2 (c (n "beginnerror") (v "0.1.2") (h "105wv7pip7dxzwk9lywl379kf7990dmy29vl3lcjmw7s38083vja")))

