(define-module (crates-io be gi beginner_tools) #:use-module (crates-io))

(define-public crate-beginner_tools-0.1.0 (c (n "beginner_tools") (v "0.1.0") (h "1aaczwyc6icsx77pg66zgvx40pmydw1j6yhd5w4607rzdgi6a4j9")))

(define-public crate-beginner_tools-0.1.1 (c (n "beginner_tools") (v "0.1.1") (h "09m69nd3vx1nffbjr0djxswa0c9v48na97hd5fdy90gc4z8d9jqx")))

(define-public crate-beginner_tools-1.0.0 (c (n "beginner_tools") (v "1.0.0") (h "1dch35y6sxvzhv3ilclb4qk3v8wahifmwp736f9n84dqqk4fp884")))

(define-public crate-beginner_tools-1.1.0 (c (n "beginner_tools") (v "1.1.0") (h "1p5n94q1i4xn5fiyqgi4sq3f9bdicjhwkkz9p2m4fk9dpafiyiq8")))

(define-public crate-beginner_tools-1.1.1 (c (n "beginner_tools") (v "1.1.1") (h "1c2523dc3crl2bqd42ycpz8z9ig0xcm4ah8hhcy9jdnsdsb4d3kr")))

