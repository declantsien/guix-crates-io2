(define-module (crates-io be vy bevy_property_derive) #:use-module (crates-io))

(define-public crate-bevy_property_derive-0.1.0 (c (n "bevy_property_derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "100vzhnwpcz7567shijrn33978jd8midr0wdiw3a9npjd2ynay6k")))

(define-public crate-bevy_property_derive-0.1.3 (c (n "bevy_property_derive") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cp7kk1ifhi34nh09h8x2i2ii8p2gdsn8kxpkbqgbkzica6ixld8")))

(define-public crate-bevy_property_derive-0.2.0 (c (n "bevy_property_derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z9mh2acypf2xay168l7q9hxhrlx3nx4fazzl7n532y311jmw3v0")))

(define-public crate-bevy_property_derive-0.2.1 (c (n "bevy_property_derive") (v "0.2.1") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f1nsxl85m8q4zx5hr7gvb0an5j0hjzim4ialmwlbs8lbrvf0phm")))

(define-public crate-bevy_property_derive-0.3.0 (c (n "bevy_property_derive") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03y1c169xc7x6kyfbdqz2yksjaaxb5ymjqp60pf621d962ya7ix3")))

