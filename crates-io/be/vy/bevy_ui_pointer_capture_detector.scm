(define-module (crates-io be vy bevy_ui_pointer_capture_detector) #:use-module (crates-io))

(define-public crate-bevy_ui_pointer_capture_detector-0.1.0 (c (n "bevy_ui_pointer_capture_detector") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)))) (h "0azfcn0ifjpqm1kmhd9kbbgpyizn5r989acvxanl8sdnyv4w65mb")))

(define-public crate-bevy_ui_pointer_capture_detector-0.1.1 (c (n "bevy_ui_pointer_capture_detector") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)))) (h "070hawa3y1z30jrhihxnss7nhrcbzny27ajs7mfbj0lxrqzpzzl9")))

(define-public crate-bevy_ui_pointer_capture_detector-0.2.0 (c (n "bevy_ui_pointer_capture_detector") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)))) (h "0m9dlfsaix1xv050zd934vnkm6l2nymbahi4bhpbygyysrb8axih")))

(define-public crate-bevy_ui_pointer_capture_detector-0.3.0 (c (n "bevy_ui_pointer_capture_detector") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)))) (h "0ib9y75y52qa7xs044v54dyh73y6dr37z2aam2ha787inp4rd6nh")))

(define-public crate-bevy_ui_pointer_capture_detector-0.3.1 (c (n "bevy_ui_pointer_capture_detector") (v "0.3.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)))) (h "0i7bh5mrzfqinkpmhd9a5v95f45h5l2x5z4pylg6jsg27l5qljcb")))

