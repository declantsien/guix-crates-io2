(define-module (crates-io be vy bevy_rome) #:use-module (crates-io))

(define-public crate-bevy_rome-0.0.1 (c (n "bevy_rome") (v "0.0.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("filesystem_watcher"))) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s8sh6781cfxwmw7ngiibgp934jixcrpmsh6j8dixgz3zwb35dpj")))

