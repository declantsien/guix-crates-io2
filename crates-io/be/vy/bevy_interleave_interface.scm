(define-module (crates-io be vy bevy_interleave_interface) #:use-module (crates-io))

(define-public crate-bevy_interleave_interface-0.1.0 (c (n "bevy_interleave_interface") (v "0.1.0") (h "061b4xngkd29328q06sqay7wa887vqzr5zwwf1fb24nk4sy3nx74")))

(define-public crate-bevy_interleave_interface-0.1.1 (c (n "bevy_interleave_interface") (v "0.1.1") (h "1z1ngbjhlzmwl1ljw4r9chi414iif32x3bmgnf2xkn9xbg6wswcb")))

(define-public crate-bevy_interleave_interface-0.2.0 (c (n "bevy_interleave_interface") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render"))) (k 0)))) (h "0zp5ngx0xrq0lflfhy9g69jmzdcz6zxvcr6bfxxfxx01hy7nrv1h")))

(define-public crate-bevy_interleave_interface-0.2.1 (c (n "bevy_interleave_interface") (v "0.2.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render"))) (k 0)))) (h "1b165z2qxc9km3bms2diwgnivygp82p9mj1yhsx32znr2fibld9k")))

