(define-module (crates-io be vy bevy_dioxus_core) #:use-module (crates-io))

(define-public crate-bevy_dioxus_core-0.1.1 (c (n "bevy_dioxus_core") (v "0.1.1") (d (list (d (n "bevy_dioxus_macro") (r "^0.1") (d #t) (k 0)) (d (n "fermi") (r "^0.2") (d #t) (k 0)))) (h "00w8091qryj00rfxi5wpz9k0gs9n8wkswgh1b6ql35l3jdj31lh3")))

