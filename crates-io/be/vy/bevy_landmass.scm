(define-module (crates-io be vy bevy_landmass) #:use-module (crates-io))

(define-public crate-bevy_landmass-0.1.0 (c (n "bevy_landmass") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (k 0)) (d (n "glam") (r "^0.24.0") (d #t) (k 0)) (d (n "landmass") (r "^0.1.1") (d #t) (k 0)))) (h "1687h3y037slpkqhffw7sajmajdbbwdd5k3ydfmknnpxvd16dxcv") (f (quote (("mesh-utils" "bevy/bevy_render"))))))

(define-public crate-bevy_landmass-0.2.0 (c (n "bevy_landmass") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.0") (k 0)) (d (n "glam") (r "^0.24.0") (d #t) (k 0)) (d (n "landmass") (r "^0.1.1") (d #t) (k 0)))) (h "0alspkza13nvjkga8q3npdqw2nlw0qf049iziz6j1fr4iv2f68ks") (f (quote (("mesh-utils" "bevy/bevy_render"))))))

(define-public crate-bevy_landmass-0.3.0 (c (n "bevy_landmass") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12.0") (k 0)) (d (n "glam") (r "^0.24.2") (d #t) (k 0)) (d (n "landmass") (r "^0.2.0") (d #t) (k 0)))) (h "0b6ra33wxxjbjgrg6kici6x13y9k4sq2mj0sm24zvs9lqc9lfqdx") (f (quote (("mesh-utils" "bevy/bevy_render"))))))

(define-public crate-bevy_landmass-0.4.0 (c (n "bevy_landmass") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "landmass") (r "^0.3.3") (d #t) (k 0)))) (h "1853zmwc4l9scklkd6lif1c6azv7hz1vah96gjwlyx0dx9fxsmhd") (f (quote (("mesh-utils" "bevy/bevy_render"))))))

(define-public crate-bevy_landmass-0.4.1 (c (n "bevy_landmass") (v "0.4.1") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "landmass") (r "^0.3.3") (d #t) (k 0)))) (h "0nc87zcy9cbrdgvlybp37k0kdl6pijan732mrsz4srln6kxlzzpf") (f (quote (("mesh-utils" "bevy/bevy_render"))))))

(define-public crate-bevy_landmass-0.5.0 (c (n "bevy_landmass") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "landmass") (r "^0.4.0") (d #t) (k 0)))) (h "0n4ajg7fsxvxk7fl19q9wgldnyd030mc2ly7yir1ng268y1l1fch") (f (quote (("mesh-utils" "bevy/bevy_render"))))))

