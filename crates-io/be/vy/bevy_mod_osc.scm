(define-module (crates-io be vy bevy_mod_osc) #:use-module (crates-io))

(define-public crate-bevy_mod_osc-0.1.0 (c (n "bevy_mod_osc") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_async_task") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "1bbc46yhzhis3xj2wr1xnpyx8m85ym73j8zznvcws4nms0iqhly6")))

(define-public crate-bevy_mod_osc-0.1.1 (c (n "bevy_mod_osc") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_async_task") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "1rmsq5n16vjn0x6d62y99nbv7f1r4zaqsr2915ng5xcmh44zh5ak")))

(define-public crate-bevy_mod_osc-0.1.2 (c (n "bevy_mod_osc") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_async_task") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "1bqxfzg95qczks0v2y9nwh69830pcl6n3kc64p6np6zw26hxf4v4")))

(define-public crate-bevy_mod_osc-0.1.3 (c (n "bevy_mod_osc") (v "0.1.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_async_task") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "1pl008306l20ddx8grn1lbss2dq9pa4c2gi0w7wi37qk6kpv6pz8")))

(define-public crate-bevy_mod_osc-0.1.4 (c (n "bevy_mod_osc") (v "0.1.4") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_async_task") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "0igb8k61k4ik7is6rvwri9kyz0pj53dg11xa316v9lmb6bpcp3ls")))

(define-public crate-bevy_mod_osc-0.1.5 (c (n "bevy_mod_osc") (v "0.1.5") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_async_task") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "1834ckf4d0fz88g4rknw0wblch926bh9fggzbsxdg18qzm5cvl0y")))

(define-public crate-bevy_mod_osc-0.1.6 (c (n "bevy_mod_osc") (v "0.1.6") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_async_task") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "051fpbhx4yqnlvjdsgh1rrakpj3xb8kd29achl9iyrlk6226q4ax")))

(define-public crate-bevy_mod_osc-0.1.7 (c (n "bevy_mod_osc") (v "0.1.7") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "multi-threaded"))) (k 0)) (d (n "bevy_async_task") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "0alchv1f8yqnb2qf6b382q3blbbyyyic6ni0kzlw1bpycwv17098")))

(define-public crate-bevy_mod_osc-0.1.8 (c (n "bevy_mod_osc") (v "0.1.8") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "multi-threaded"))) (k 0)) (d (n "bevy_async_task") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rosc") (r "~0.10") (d #t) (k 0)))) (h "1mk32wlnrls7i75zi4nwjjrihh04rjlgmsz767ij474wggdj3phf")))

