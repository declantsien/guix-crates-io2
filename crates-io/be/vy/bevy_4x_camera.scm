(define-module (crates-io be vy bevy_4x_camera) #:use-module (crates-io))

(define-public crate-bevy_4x_camera-0.1.0 (c (n "bevy_4x_camera") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ks1g98nzfxdwzhk96bfl8jf1vhmxa62nkswfmpdwf3vb00hmz5a")))

(define-public crate-bevy_4x_camera-0.1.1 (c (n "bevy_4x_camera") (v "0.1.1") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1zqhgd9m7ahmi2mznkfyap2irqblpj2w8flgcng8n8l1vp922hp4")))

(define-public crate-bevy_4x_camera-0.1.2 (c (n "bevy_4x_camera") (v "0.1.2") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0yngwya4399wf6qkkd1rm8bilnzwi86znalgi0kq76x8s07d7rm3")))

