(define-module (crates-io be vy bevy_points) #:use-module (crates-io))

(define-public crate-bevy_points-0.1.0 (c (n "bevy_points") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 2)))) (h "04b8hph6kklfld5hqaw7rxgqbcld3wnq1qvj32avzv77yzlpg7lm") (f (quote (("examples" "bevy/render" "bevy/bevy_winit" "bevy/x11")))) (y #t)))

(define-public crate-bevy_points-0.1.1 (c (n "bevy_points") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 2)))) (h "1gk7bv2h4fyaqbr1x3nxsz7n2flc3g1fn94295z84rqnbh0aws4k") (f (quote (("examples" "bevy/render" "bevy/bevy_winit" "bevy/x11"))))))

(define-public crate-bevy_points-0.1.2 (c (n "bevy_points") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 2)))) (h "1mf8x56cvj42wfkw98xqy0bkymnj6836i33q1klzkc6v6052k8ad") (f (quote (("examples" "bevy/render" "bevy/bevy_winit" "bevy/x11"))))))

(define-public crate-bevy_points-0.1.3 (c (n "bevy_points") (v "0.1.3") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 2)))) (h "00kgjs69wypzqamdjilyb78q8girsbz96qwh7dhvbzqk131kvclr") (f (quote (("examples" "bevy/render" "bevy/bevy_winit" "bevy/x11"))))))

(define-public crate-bevy_points-0.2.0 (c (n "bevy_points") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "1sybwl1d3a8c62xalzpypdrppjv18qifgcymgxw1zqfrhl0capkm") (f (quote (("examples" "bevy/bevy_render" "bevy/bevy_pbr" "bevy/bevy_core_pipeline" "bevy/bevy_winit" "bevy/x11")))) (y #t)))

(define-public crate-bevy_points-0.2.1 (c (n "bevy_points") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "0wwwwmjnn5cma3jr5mmilh2whdljh1p4r3b8rv17z34m2mk5cvaa") (f (quote (("examples" "bevy/bevy_render" "bevy/bevy_pbr" "bevy/bevy_core_pipeline" "bevy/bevy_winit" "bevy/x11"))))))

(define-public crate-bevy_points-0.3.0 (c (n "bevy_points") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "1glaaliw9lnlzkzp5056ngg3s2d1kn8bs01vld9a089nn3gsx0sd") (f (quote (("examples" "bevy/bevy_render" "bevy/bevy_pbr" "bevy/bevy_core_pipeline" "bevy/bevy_winit" "bevy/x11"))))))

(define-public crate-bevy_points-0.4.0 (c (n "bevy_points") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset" "tonemapping_luts"))) (k 0)) (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_render" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "0w07sfhbzrf2s7dz61c5ylgg3h58imd7dbrazfc2i04mq731dqih") (f (quote (("examples" "bevy/bevy_render" "bevy/bevy_pbr" "bevy/bevy_core_pipeline" "bevy/bevy_winit" "bevy/x11"))))))

(define-public crate-bevy_points-0.5.0 (c (n "bevy_points") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset" "tonemapping_luts"))) (k 0)) (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_render" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "0l8lwyxd5z9h35ckgavsircrjzdzxbyym68j9lzx9dpi466r09ki") (f (quote (("examples" "bevy/bevy_render" "bevy/bevy_pbr" "bevy/bevy_core_pipeline" "bevy/bevy_winit" "bevy/x11"))))))

(define-public crate-bevy_points-0.5.1 (c (n "bevy_points") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset" "tonemapping_luts"))) (k 0)) (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_render" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "0sy8naw5bcbqf0hjpzgrbldh7jh9zyrk3rw1x82p9070j09bk6hv") (f (quote (("examples" "bevy/bevy_render" "bevy/bevy_pbr" "bevy/bevy_core_pipeline" "bevy/bevy_winit" "bevy/x11"))))))

(define-public crate-bevy_points-0.5.2 (c (n "bevy_points") (v "0.5.2") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset" "tonemapping_luts"))) (k 0)))) (h "1axr04wz0bhw8yagpxnrlqr81vy3krh0z79fqqj4c00s1jlsyvil") (f (quote (("examples" "bevy/bevy_core_pipeline" "bevy/bevy_winit" "bevy/x11"))))))

