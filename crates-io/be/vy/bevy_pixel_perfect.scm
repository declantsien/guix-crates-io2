(define-module (crates-io be vy bevy_pixel_perfect) #:use-module (crates-io))

(define-public crate-bevy_pixel_perfect-0.1.0 (c (n "bevy_pixel_perfect") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_core_pipeline"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)))) (h "0bjfwbrhmgaxlqmylpkczppyvgk3bdmvvfc6j8bx6pnn35f591jm")))

(define-public crate-bevy_pixel_perfect-0.1.1 (c (n "bevy_pixel_perfect") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_core_pipeline"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)))) (h "0wagr72qhck0n4v67xjmfd973fbzgy7bjj0md3423nkim7l0k3qg")))

