(define-module (crates-io be vy bevy_health_bar3d) #:use-module (crates-io))

(define-public crate-bevy_health_bar3d-1.0.0 (c (n "bevy_health_bar3d") (v "1.0.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.17.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "111s7c7mlx6gkhmblcyll3i0k424f0hxksq62nbkyd53qv9nl7f8")))

(define-public crate-bevy_health_bar3d-1.1.1 (c (n "bevy_health_bar3d") (v "1.1.1") (d (list (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "1sjdh1l9qlwn821fws1r7b1dxxl7zh7vz8afkwffighfx3y5ia5n")))

(define-public crate-bevy_health_bar3d-1.2.0 (c (n "bevy_health_bar3d") (v "1.2.0") (d (list (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.7.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "0nz0xbqvim34bjvaw8bqqwzf08sjyqgrrk2k6wha2ai9yb4zl8cx")))

(define-public crate-bevy_health_bar3d-1.2.1 (c (n "bevy_health_bar3d") (v "1.2.1") (d (list (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.7.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "1z32vqggh7vcxijplg2viaxhsjmfcn2yycrq95wn0r2m9ngi76j0")))

(define-public crate-bevy_health_bar3d-1.3.0 (c (n "bevy_health_bar3d") (v "1.3.0") (d (list (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.7.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "03ywi80xjlp843rffbfy0iczp7wkh1hb9jb3vp1hmg235kgaz4dh")))

(define-public crate-bevy_health_bar3d-1.4.0 (c (n "bevy_health_bar3d") (v "1.4.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "0b4hmvwlpkh1d6hvb64bfha41dk0pgdvc8mdz2cxjnqgf0sxs0q1")))

(define-public crate-bevy_health_bar3d-1.4.1 (c (n "bevy_health_bar3d") (v "1.4.1") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "1ni9bf5104dc25m2g55xmz64l351qldgn8gvkklk1z9bpk96jxav")))

(define-public crate-bevy_health_bar3d-1.4.2 (c (n "bevy_health_bar3d") (v "1.4.2") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "1s0hpk9sqgrrkgspppb3s7fnvxnz7qf4gbd6xay3n9xza3kpf3c0")))

(define-public crate-bevy_health_bar3d-1.4.3 (c (n "bevy_health_bar3d") (v "1.4.3") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "1x8yhf1g3xx50zj6qmsaq62r0i765r2l4dcz62a1y4cr6mbasrkl")))

(define-public crate-bevy_health_bar3d-1.4.4 (c (n "bevy_health_bar3d") (v "1.4.4") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "10v6kycdvarlq0zkpn016k33asnl0yyfd6r2czskgns344sv0cf2")))

(define-public crate-bevy_health_bar3d-2.0.0 (c (n "bevy_health_bar3d") (v "2.0.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_pbr" "bevy_render"))) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.21.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.9.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "01h7w51hz854sc2p2langa73lzc27spsfbdrpmir53x1gr0nyrin")))

(define-public crate-bevy_health_bar3d-2.0.1 (c (n "bevy_health_bar3d") (v "2.0.1") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_pbr" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.21.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.9.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "06k0h9dfjcmv0ak6s7mgym6bv76g4v76981jv6j7c3p66b5jaark")))

(define-public crate-bevy_health_bar3d-3.0.0 (c (n "bevy_health_bar3d") (v "3.0.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_pbr" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.21.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.9.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "04s0ndk7a7hkzrryr1y506zsg7rivvsgr5vymav55nzxnpqxh53l")))

(define-public crate-bevy_health_bar3d-3.1.0 (c (n "bevy_health_bar3d") (v "3.1.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_pbr" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.21.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.9.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "0763a0lpd1yhyn3y1y403kc8f2kpg3diqnwsq0wfyn23m4m94hhv")))

(define-public crate-bevy_health_bar3d-3.2.0 (c (n "bevy_health_bar3d") (v "3.2.0") (d (list (d (n "bevy") (r "^0.13.1") (f (quote ("bevy_pbr" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13.1") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.23.4") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.10.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.9.2") (d #t) (k 0)))) (h "13ha4ykjblblj0blq8s089jkqg1dxnks8mh5kfxl0ww3k07khmir")))

(define-public crate-bevy_health_bar3d-3.2.1 (c (n "bevy_health_bar3d") (v "3.2.1") (d (list (d (n "bevy") (r "^0.13.1") (f (quote ("bevy_pbr" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13.1") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.23.4") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.10.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.9.2") (d #t) (k 0)))) (h "11m60j2gjkg02x8pgc0h9xy47d5gj9gv3rf4nx06941k7qibdi1v")))

(define-public crate-bevy_health_bar3d-3.0.0-hotfix.0 (c (n "bevy_health_bar3d") (v "3.0.0-hotfix.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_pbr" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.21.0") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.9.0") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)))) (h "0y5jdpjszrfrnmd0q4xiqb28cw2s2a3f1pc0gvz4s84x5h1dpb81")))

