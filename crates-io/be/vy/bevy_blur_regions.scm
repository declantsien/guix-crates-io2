(define-module (crates-io be vy bevy_blur_regions) #:use-module (crates-io))

(define-public crate-bevy_blur_regions-0.1.0 (c (n "bevy_blur_regions") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "0c9f14mhhy1r600bzgwpkqp855ag1kzzi7r9pb9i2xnjl0b9ylyx")))

(define-public crate-bevy_blur_regions-0.2.0 (c (n "bevy_blur_regions") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.26") (o #t) (d #t) (k 0)))) (h "01d6j0jxnw326grj50z97ilngmq5291s5xhmz098nfmwmhk9fakb") (f (quote (("default" "bevy_ui") ("bevy_ui" "bevy/bevy_ui") ("all" "bevy_ui" "egui")))) (s 2) (e (quote (("egui" "dep:bevy_egui"))))))

