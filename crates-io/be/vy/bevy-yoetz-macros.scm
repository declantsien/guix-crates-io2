(define-module (crates-io be vy bevy-yoetz-macros) #:use-module (crates-io))

(define-public crate-bevy-yoetz-macros-0.1.0 (c (n "bevy-yoetz-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1qfh3rxsvcljb9lh5ah2x8z4ih6b9cw4nf0paj0kvs6laff1zxwh")))

