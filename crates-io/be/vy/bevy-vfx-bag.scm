(define-module (crates-io be vy bevy-vfx-bag) #:use-module (crates-io))

(define-public crate-bevy-vfx-bag-0.1.0 (c (n "bevy-vfx-bag") (v "0.1.0") (d (list (d (n "alsa") (r "=0.6.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "bevy") (r "^0.9.0") (f (quote ("bevy_asset" "render" "png" "tga"))) (k 0)) (d (n "bevy") (r "^0.9.0") (f (quote ("tga"))) (d #t) (k 2)) (d (n "color-eyre") (r "^0.6") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)))) (h "176mj9ywy4n02qcwkn95wcwklgm86x28m8fpcnbfxi9c1mqxmbaw") (f (quote (("dev"))))))

(define-public crate-bevy-vfx-bag-0.2.0 (c (n "bevy-vfx-bag") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_render" "bevy_core_pipeline" "png" "tga"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("tga"))) (d #t) (k 2)) (d (n "color-eyre") (r "^0.6") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "1za3lm248r7mg6y9d0az18640m0hiy5i9gmb8q0jym1d2bxpa1ks")))

