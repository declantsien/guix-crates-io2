(define-module (crates-io be vy bevy_log_diagnostic) #:use-module (crates-io))

(define-public crate-bevy_log_diagnostic-0.1.0 (c (n "bevy_log_diagnostic") (v "0.1.0") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0garw2riikbzslhgsxgwr3cpmkswbg753d7m5fbkk8p55dr0i798")))

(define-public crate-bevy_log_diagnostic-0.2.0 (c (n "bevy_log_diagnostic") (v "0.2.0") (d (list (d (n "bevy") (r "^0.3") (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fwrc978a48cxmhxljycv1nblprsv98bp7g5s46b7ljc16ygpp1g")))

(define-public crate-bevy_log_diagnostic-0.3.0 (c (n "bevy_log_diagnostic") (v "0.3.0") (d (list (d (n "bevy") (r "^0.4") (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1gcgkh73p95c0miafr2iamjxi3vpknkvbbgph0pav5g0a02vrp8d")))

