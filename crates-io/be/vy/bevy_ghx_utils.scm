(define-module (crates-io be vy bevy_ghx_utils) #:use-module (crates-io))

(define-public crate-bevy_ghx_utils-0.1.0 (c (n "bevy_ghx_utils") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (k 0)))) (h "0zn6pyrqq3rrldfxi1hxx9bjsir4yzqidi6hy68cnr136060x9hv") (f (quote (("pan-orbit-camera" "bevy/bevy_render") ("default" "pan-orbit-camera"))))))

(define-public crate-bevy_ghx_utils-0.2.0 (c (n "bevy_ghx_utils") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)))) (h "0x5fid1ahgaaxcs02vb2jzy8bm2xflii6fzlmak4phpaamll9vk1") (f (quote (("pan-orbit-camera" "bevy/bevy_render") ("default" "pan-orbit-camera"))))))

(define-public crate-bevy_ghx_utils-0.3.0 (c (n "bevy_ghx_utils") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)))) (h "0saq30hc6if8wrw094lnyfvh7h0ws5d32mx2b1c5ha4clbk6ia7p") (f (quote (("pan-orbit-camera" "bevy/bevy_render") ("default" "pan-orbit-camera"))))))

