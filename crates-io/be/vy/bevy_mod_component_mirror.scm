(define-module (crates-io be vy bevy_mod_component_mirror) #:use-module (crates-io))

(define-public crate-bevy_mod_component_mirror-0.1.0 (c (n "bevy_mod_component_mirror") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1ix30qlnxndvsjmjg0dmm1n24m46kpdcq1vjlrjlbzbyl2i5ss89") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.2.0 (c (n "bevy_mod_component_mirror") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1bja6fr80rnldjv67b4diazy4i57k5csglv6aq5scsq8f8xj6s8v") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.2.1 (c (n "bevy_mod_component_mirror") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "14vpgnqlmmsdna9kqz6k3y6qrhjcwpmzvra3v1r5dccw8nc2mm7c") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.3.0 (c (n "bevy_mod_component_mirror") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "115dbwbs8ysisa5cb9vh49fx3rpsc5nmb4gqh3dcxc7ahm8xaddz") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.4.0 (c (n "bevy_mod_component_mirror") (v "0.4.0") (d (list (d (n "bevy") (r "^0.9.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "17bhgzfy7dby93frc48bz84nf580yzm8khjhqfq7kx7s0p6g9xlr") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.5.0 (c (n "bevy_mod_component_mirror") (v "0.5.0") (d (list (d (n "bevy") (r "^0.9.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1h18i2s996j1bi25ijh0kylms5c8w018bidz8251jp2ghz1v7c9i") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.6.0 (c (n "bevy_mod_component_mirror") (v "0.6.0") (d (list (d (n "bevy") (r "^0.9.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "060i33sgksn7y2nli6j6gg9y3ndq2n2d824jqkkkymj505cz8l2j") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.7.0 (c (n "bevy_mod_component_mirror") (v "0.7.0") (d (list (d (n "bevy") (r "^0.9.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "0s8w2r6gma9png20s41sk45zwcvm41h0z9cbv8q331qcr7mqxmjc") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.8.0 (c (n "bevy_mod_component_mirror") (v "0.8.0") (d (list (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "0blk46cxliikzqdnipif6ydizw6cbprx9264pply6104s6br5pd8") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.9.0 (c (n "bevy_mod_component_mirror") (v "0.9.0") (d (list (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "bevy_rapier3d") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "089wx9598x4yvwha8qc66b3v6wf5364103kkqyqbzy7w79wnab2g") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.10.0 (c (n "bevy_mod_component_mirror") (v "0.10.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_rapier3d") (r "^0.22") (o #t) (d #t) (k 0)))) (h "1jwr1kxc2sdb3h7kkfxaz12c9h9yhx71d8lga5s71hjd4ks9gad8") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

(define-public crate-bevy_mod_component_mirror-0.11.0 (c (n "bevy_mod_component_mirror") (v "0.11.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_rapier3d") (r "^0.23") (o #t) (d #t) (k 0)))) (h "0kcqsa3gmpb4bswfl5rxbp5f1v5j4lzvmlgb16f3hvjl6pslmi53") (f (quote (("rapier" "bevy_rapier3d") ("default" "rapier"))))))

