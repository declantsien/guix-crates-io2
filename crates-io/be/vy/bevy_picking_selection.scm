(define-module (crates-io be vy bevy_picking_selection) #:use-module (crates-io))

(define-public crate-bevy_picking_selection-0.1.0 (c (n "bevy_picking_selection") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_picking_core") (r "^0.1") (d #t) (k 0)))) (h "1x2wpm5gvdk4gvwb9slhlvdxpck3dv9m6i2l9zn0nym5gg1lg5sr")))

(define-public crate-bevy_picking_selection-0.2.0 (c (n "bevy_picking_selection") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_eventlistener") (r "^0.2") (d #t) (k 0)) (d (n "bevy_picking_core") (r "^0.2") (d #t) (k 0)))) (h "0v5ayly5myircjcr9j500krf8lx4q6midmnynj7izxzrzqgis78l")))

(define-public crate-bevy_picking_selection-0.15.0 (c (n "bevy_picking_selection") (v "0.15.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_eventlistener") (r "^0.3") (d #t) (k 0)) (d (n "bevy_picking_core") (r "^0.15") (d #t) (k 0)))) (h "1wlh6gnbc5ks4zyn47lkh6a8zzmwx6saa2wiq999pwlzn1bhx631")))

(define-public crate-bevy_picking_selection-0.16.0 (c (n "bevy_picking_selection") (v "0.16.0") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "bevy_eventlistener") (r "^0.5") (d #t) (k 0)) (d (n "bevy_input") (r "^0.11") (k 0)) (d (n "bevy_picking_core") (r "^0.16") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.11") (k 0)) (d (n "bevy_utils") (r "^0.11") (k 0)))) (h "1svdm33sfm4lf4rjckpnv8id8khkb90qpbqag6hi4wndqp6ywisi")))

(define-public crate-bevy_picking_selection-0.17.0 (c (n "bevy_picking_selection") (v "0.17.0") (d (list (d (n "bevy_app") (r "^0.12") (k 0)) (d (n "bevy_ecs") (r "^0.12") (k 0)) (d (n "bevy_eventlistener") (r "^0.6") (d #t) (k 0)) (d (n "bevy_input") (r "^0.12") (k 0)) (d (n "bevy_picking_core") (r "^0.17") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12") (k 0)) (d (n "bevy_utils") (r "^0.12") (k 0)))) (h "0g40gw1y7az8klzif2ms0kvm0h8dm5imbzvpmw5snlvnjm67hk41")))

(define-public crate-bevy_picking_selection-0.18.0 (c (n "bevy_picking_selection") (v "0.18.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_eventlistener") (r "^0.7") (d #t) (k 0)) (d (n "bevy_input") (r "^0.13") (k 0)) (d (n "bevy_picking_core") (r "^0.18") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.13") (k 0)) (d (n "bevy_utils") (r "^0.13") (k 0)))) (h "0jz2bs6yf2y2bnjj7rihhza793irmvkd261468c8j19qzpp8djgv")))

