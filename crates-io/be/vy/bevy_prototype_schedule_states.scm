(define-module (crates-io be vy bevy_prototype_schedule_states) #:use-module (crates-io))

(define-public crate-bevy_prototype_schedule_states-0.1.0-beta.0 (c (n "bevy_prototype_schedule_states") (v "0.1.0-beta.0") (d (list (d (n "bevy") (r "^0.6") (k 0)))) (h "1l8jvi1fhzlrjn3l2czmhn4c5y2a88zb5hh9cri3wnlpss6iyyir")))

(define-public crate-bevy_prototype_schedule_states-0.1.0-beta.1 (c (n "bevy_prototype_schedule_states") (v "0.1.0-beta.1") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)))) (h "0cjisk91si9b23rm9ix9qik0vwgfibynf43q22f6hss31af3pnyp")))

(define-public crate-bevy_prototype_schedule_states-0.1.0 (c (n "bevy_prototype_schedule_states") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)))) (h "0fc59zy7p5yjgy00hm9m54c3ppwgkvd5985qmgiqccl1w8wa06q9")))

(define-public crate-bevy_prototype_schedule_states-0.1.1 (c (n "bevy_prototype_schedule_states") (v "0.1.1") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)))) (h "1lghf5yzk9469didzyzshfbyh5r3qd0mhyhq1b1j5l3f54gghyia")))

