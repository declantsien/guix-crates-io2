(define-module (crates-io be vy bevy_fly_camera) #:use-module (crates-io))

(define-public crate-bevy_fly_camera-0.1.0 (c (n "bevy_fly_camera") (v "0.1.0") (d (list (d (n "bevy") (r "^0.1.2") (d #t) (k 0)) (d (n "bevy_render") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_winit") (r "^0.1.1") (d #t) (k 0)))) (h "1cvd0rb4b2kqck41kvpz1z9gizkk9gmwf7bmjzkhn6n03pkyk7r7")))

(define-public crate-bevy_fly_camera-0.1.1 (c (n "bevy_fly_camera") (v "0.1.1") (d (list (d (n "bevy") (r "^0.1.2") (d #t) (k 0)) (d (n "bevy_render") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_winit") (r "^0.1.1") (d #t) (k 0)))) (h "0l6qxchzg2kf5pvp7dvg9p9jpfzv081ip9hjmdiw85blw583fl04")))

(define-public crate-bevy_fly_camera-0.2.0 (c (n "bevy_fly_camera") (v "0.2.0") (d (list (d (n "bevy") (r "^0.1.2") (d #t) (k 0)) (d (n "bevy_render") (r "^0.1.0") (d #t) (k 0)))) (h "09rxyjxsp41r30z88r71gk20w47q2qinpqs7c71fcgj99mg5q9ix")))

(define-public crate-bevy_fly_camera-0.3.0 (c (n "bevy_fly_camera") (v "0.3.0") (d (list (d (n "bevy") (r "^0.1.3") (d #t) (k 0)))) (h "1rf416xb6x3njvw9q23r8g750nizlm0w6a0n514m0ddk96xiraz3")))

(define-public crate-bevy_fly_camera-0.4.0 (c (n "bevy_fly_camera") (v "0.4.0") (d (list (d (n "async-executor") (r "=1.1.1") (d #t) (k 0)) (d (n "bevy") (r "^0.2.0") (d #t) (k 0)))) (h "1h1p60h30fkbh0rj49c4lhag9i0hxly20bn87hdz2rl9681zjsdw")))

(define-public crate-bevy_fly_camera-0.4.1 (c (n "bevy_fly_camera") (v "0.4.1") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)))) (h "029x96kdfv806wlzglbrscsf56pdraybdz3i794q6i7ixqd53qrs")))

(define-public crate-bevy_fly_camera-0.5.0 (c (n "bevy_fly_camera") (v "0.5.0") (d (list (d (n "bevy") (r "^0.3.0") (d #t) (k 0)))) (h "10kz52zjmr3pgyjp6ggg3phnwj917bfymii1w5x9cjz9k7805pa3")))

(define-public crate-bevy_fly_camera-0.6.0 (c (n "bevy_fly_camera") (v "0.6.0") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 0)))) (h "0w78vn2z2ix0vj6ilg1qhhmlb3mcf01ng0zl0r7vjzlsy71rgng0")))

(define-public crate-bevy_fly_camera-0.7.0 (c (n "bevy_fly_camera") (v "0.7.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "0flv22ybx687xfh4xb65lk2jn3b46jj4zqvgawsn275131ywawvg")))

(define-public crate-bevy_fly_camera-0.8.0 (c (n "bevy_fly_camera") (v "0.8.0") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)))) (h "0hivaydvsxm4hzjk3bzs6sl9nx9zjp2cs0zsmanpncv20v22rmvx")))

(define-public crate-bevy_fly_camera-0.10.0 (c (n "bevy_fly_camera") (v "0.10.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)))) (h "18mj7nbclw418b637mdlxmc73llhxabp7y3mjsvwg1q0g547a8dn")))

