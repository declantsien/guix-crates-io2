(define-module (crates-io be vy bevy_kot_ecs) #:use-module (crates-io))

(define-public crate-bevy_kot_ecs-0.0.12 (c (n "bevy_kot_ecs") (v "0.0.12") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "0vilssm3i9hm93ai7msx53vl9s2lhglrmfypvd97d8h9snb075gm")))

(define-public crate-bevy_kot_ecs-0.1.0 (c (n "bevy_kot_ecs") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "07qrj2fvkh5akl7yicgsxj1vdx20zsdh9bi2cdwsgp9rs601n58a")))

(define-public crate-bevy_kot_ecs-0.2.0 (c (n "bevy_kot_ecs") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "0i8qrcs7c999j11piflyd5p6wj25vkywr9hk8dpayw7k3fhrc6wc")))

(define-public crate-bevy_kot_ecs-0.3.1 (c (n "bevy_kot_ecs") (v "0.3.1") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_eventlistener_core") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0zqj2d2482ks4axvdfg7bymcfyzh062gp9p090yvfxy1l2zkzpws")))

(define-public crate-bevy_kot_ecs-0.4.0 (c (n "bevy_kot_ecs") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_eventlistener_core") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1xc74cc76yla53rcd0x9k29yyc7m6pq4j3rdis1fbl6vkyc4gwd9")))

(define-public crate-bevy_kot_ecs-0.5.0 (c (n "bevy_kot_ecs") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_eventlistener_core") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0r67rkf6fcq72544ija5s573sz8w3j07wcdi6i3cvihhr1g5j3h7")))

(define-public crate-bevy_kot_ecs-0.6.0 (c (n "bevy_kot_ecs") (v "0.6.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_eventlistener_core") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0awkl76nlr7m4an1m6pi0ahswpimyp1rfr025qrzwv9hiikznpbh")))

(define-public crate-bevy_kot_ecs-0.7.0 (c (n "bevy_kot_ecs") (v "0.7.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_eventlistener_core") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0bmvd6j77bjdpghdxwdsclh4wk6xddv616z5zznsks8768ff1pgp")))

(define-public crate-bevy_kot_ecs-0.8.0 (c (n "bevy_kot_ecs") (v "0.8.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_eventlistener_core") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_utils") (r "^0.8.0") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0mbjzw6n1gnb3dmx0vvpkinhlcnxqf0d2z258p1s68hjfppaj6sj")))

(define-public crate-bevy_kot_ecs-0.9.0 (c (n "bevy_kot_ecs") (v "0.9.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_eventlistener_core") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_utils") (r "^0.9.0") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0kpdc02g47sh4xc78rdas3n203krry04vaihlmhv2xgf2qh4i2i8")))

(define-public crate-bevy_kot_ecs-0.9.1 (c (n "bevy_kot_ecs") (v "0.9.1") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_eventlistener_core") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_utils") (r "^0.9.1") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1pcnasacd47085q3acka5khh6d54cqx68s2mklf5m3khy6smhiaa")))

(define-public crate-bevy_kot_ecs-0.9.2 (c (n "bevy_kot_ecs") (v "0.9.2") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_utils") (r "^0.9.1") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1bvqg1zvvna6hfraz2pqm368wsvwp44w0jdq97yd95ryfy62g5vk")))

(define-public crate-bevy_kot_ecs-0.10.0 (c (n "bevy_kot_ecs") (v "0.10.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_utils") (r "^0.10.0") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0qspmvyp708wm9h19254n5sf9z8l44qcbh8wzi7i6g0m3ry82wch")))

(define-public crate-bevy_kot_ecs-0.10.1 (c (n "bevy_kot_ecs") (v "0.10.1") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_utils") (r "^0.10.1") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1r4pj9ybxfnylsp26jgma4lhsqn04rik476dv0fz03azsnddddfn")))

(define-public crate-bevy_kot_ecs-0.10.2 (c (n "bevy_kot_ecs") (v "0.10.2") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_utils") (r "^0.10.2") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ifvmy7xp87sdsn3ynyyxmyx3y1f7p2x9xv6jrfcjibrixrdkzz1")))

(define-public crate-bevy_kot_ecs-0.10.3 (c (n "bevy_kot_ecs") (v "0.10.3") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_utils") (r "^0.10.3") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "06m036fpz0m0knq72lix1cja0l69vds3vdciv0qz9vq0dcyikpxg")))

(define-public crate-bevy_kot_ecs-0.11.0 (c (n "bevy_kot_ecs") (v "0.11.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_derive") (r "^0.11.0") (k 0)) (d (n "bevy_kot_utils") (r "^0.11.0") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ahx4xmxn4j7kai5q2cg14v1259lgw0xy8ysk7bg6b2n4i0wjqgc")))

