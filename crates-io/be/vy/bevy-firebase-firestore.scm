(define-module (crates-io be vy bevy-firebase-firestore) #:use-module (crates-io))

(define-public crate-bevy-firebase-firestore-0.1.0 (c (n "bevy-firebase-firestore") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.1") (d #t) (k 0)) (d (n "bevy-firebase-auth") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy-tokio-tasks") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_macro_utils") (r "^0.11.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 2)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "0lk40pfcds5nkv17vha77q21ak4lfiwz7bz3xjxv29zxmybikm0y")))

