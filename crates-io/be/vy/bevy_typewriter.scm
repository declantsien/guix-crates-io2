(define-module (crates-io be vy bevy_typewriter) #:use-module (crates-io))

(define-public crate-bevy_typewriter-0.1.0 (c (n "bevy_typewriter") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy") (r "^0.7.0") (d #t) (k 2)))) (h "1z0xd123nhfhnl9q29pzqlx95ny9apqbshasqmhw2mljscr5szrq")))

(define-public crate-bevy_typewriter-0.2.0 (c (n "bevy_typewriter") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 2)))) (h "1vvdjslqcnn4bmyyikbh0j5jmn7ws13l5f1if1vg2aaxnxjh5z4s")))

