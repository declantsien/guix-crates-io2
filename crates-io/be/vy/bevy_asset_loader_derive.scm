(define-module (crates-io be vy bevy_asset_loader_derive) #:use-module (crates-io))

(define-public crate-bevy_asset_loader_derive-0.1.0 (c (n "bevy_asset_loader_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j0v9sv1zm52jrfkmf1959brab1014q0l85ryrrzjna4pdg76mws")))

(define-public crate-bevy_asset_loader_derive-0.2.0 (c (n "bevy_asset_loader_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v65jvzg9b04pw3ayfn2x74mdb2gpy29iz48bycxama2mamcyvp1")))

(define-public crate-bevy_asset_loader_derive-0.2.1 (c (n "bevy_asset_loader_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jpzf07fyyl42712lyydaa1lkglq81yg6n02zn7cvg6ip4gzq7zs")))

(define-public crate-bevy_asset_loader_derive-0.3.0 (c (n "bevy_asset_loader_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gjhwzz36d1vxjrm588rfad7g9hqsvyjwcv6pzxm5r73vjxi3li9")))

(define-public crate-bevy_asset_loader_derive-0.4.0 (c (n "bevy_asset_loader_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wqkxfw94wmlmpxcngn8al19cvwb9hz66dmaw1h44wiy3dpfgp3a")))

(define-public crate-bevy_asset_loader_derive-0.5.0 (c (n "bevy_asset_loader_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ssynqar3z82kajgrywkaddcll0h0msagh46nzfbi0f4dfa4vij4") (f (quote (("render"))))))

(define-public crate-bevy_asset_loader_derive-0.6.0 (c (n "bevy_asset_loader_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qh0ck0ya2qcbhc0acbghxyvk7333z81daf6s11wr4b31acrrqa1") (f (quote (("sprite") ("render"))))))

(define-public crate-bevy_asset_loader_derive-0.7.0 (c (n "bevy_asset_loader_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qqqnh7b1dg31g8y770xc6mwghf492da9rzmmqmz6gdcnqlwn1ja") (f (quote (("render"))))))

(define-public crate-bevy_asset_loader_derive-0.8.0 (c (n "bevy_asset_loader_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xfphk1jsg36plcw3wralzh24bvfp4nygn77zvh2jkrxslpnma23") (f (quote (("render"))))))

(define-public crate-bevy_asset_loader_derive-0.9.0 (c (n "bevy_asset_loader_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sqn5b0qnd9hp1mwksf4xznjzykankjhwp1ssryfbyqnsi0y1jcj") (f (quote (("render"))))))

(define-public crate-bevy_asset_loader_derive-0.10.0 (c (n "bevy_asset_loader_derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wsaqnyibidsxdg6211vrzq1awya1khqwf0i94kc24492rwhnmn5") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.11.0 (c (n "bevy_asset_loader_derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mzxsj56kr4m9y7jg9wz794m5jq343hjnzifj01c5dm2snzhsc0x") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.12.0 (c (n "bevy_asset_loader_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06k43ch5ayzvg719jc2is4i2nqbsp21ahzwrrrbx6xkdllg4f1nh") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.12.1 (c (n "bevy_asset_loader_derive") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vabr07rci6mkrkkq1xkg79j1nwah1r5ha9l14f72gs20rc7a8g7") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.13.0 (c (n "bevy_asset_loader_derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d1gazspx827n3c4ljcgi6c8h11kir6i462nnyynkrhfyzx4426p") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.14.0 (c (n "bevy_asset_loader_derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p2z6dijk7b32adkwnj7p74xvfmcjcwil90slz1anf8w7r1qq2n8") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.15.0 (c (n "bevy_asset_loader_derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fhnp5cwvf65v7i2zqsy7h7fxjbdhhxdd9mr4nbfdyd4c5c2sidc") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.16.0 (c (n "bevy_asset_loader_derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pb1qn8d7z3k8jw00ywq92vkg35rvi1755g4hm5y26ni61pn8899") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.16.1 (c (n "bevy_asset_loader_derive") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "191z95alfzgfa4b46lz6vvi2wmnm40a1z6lnb8v9z2dxqph4v8y3") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.17.0 (c (n "bevy_asset_loader_derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jwg556ndq93d1m1rjbh6jk464vdzrpr776znjf6zzb1mk6p63dz") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.18.0 (c (n "bevy_asset_loader_derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "07f6b2crs8mzy08n840w74zrg6mxc2ki6pyvkbhak9z2qspj0f6l") (f (quote (("3d") ("2d"))))))

(define-public crate-bevy_asset_loader_derive-0.19.0 (c (n "bevy_asset_loader_derive") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "079vcbg0afmy6mvff1wfa1jgp2h9rigixiikv4wir8cdd6yyk12s") (f (quote (("3d") ("2d")))) (r "1.74.0")))

(define-public crate-bevy_asset_loader_derive-0.20.0 (c (n "bevy_asset_loader_derive") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0zwl4cxgbnmx7rh5128n7h3p7wmdwbdi96m25q9lpqyjj4dhznmd") (f (quote (("3d") ("2d")))) (r "1.74.0")))

(define-public crate-bevy_asset_loader_derive-0.20.1 (c (n "bevy_asset_loader_derive") (v "0.20.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "19zysr7y5ggi07rlvxsarganixmmjidsvhvws75pna6mn1iz72ya") (f (quote (("3d") ("2d")))) (r "1.74.0")))

(define-public crate-bevy_asset_loader_derive-0.20.2 (c (n "bevy_asset_loader_derive") (v "0.20.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "18qzzd0mkdjwc4inicfw2m4c4c20mj9wrhpvvg5jq4pq8b0x2q16") (f (quote (("3d") ("2d")))) (r "1.74.0")))

