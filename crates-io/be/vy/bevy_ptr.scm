(define-module (crates-io be vy bevy_ptr) #:use-module (crates-io))

(define-public crate-bevy_ptr-0.0.1 (c (n "bevy_ptr") (v "0.0.1") (h "1bvilra7c1cjm8j4599vb70bpx7gsqaynbj20dp9fppyvazryrri")))

(define-public crate-bevy_ptr-0.8.0 (c (n "bevy_ptr") (v "0.8.0") (h "0fkip2gvlq0450245qbr050pj6fd7mblcy53ha3a20lnx1wmcbfr")))

(define-public crate-bevy_ptr-0.8.1 (c (n "bevy_ptr") (v "0.8.1") (h "029g4gnqbmpq6aj6ynycs410ixd442angdll32zcwhrbb2gc2q4r")))

(define-public crate-bevy_ptr-0.9.0 (c (n "bevy_ptr") (v "0.9.0") (h "0azk7svlsffaj82jipbf260lcnbrmd8h6381w83hr9fdy0x4svy3")))

(define-public crate-bevy_ptr-0.9.1 (c (n "bevy_ptr") (v "0.9.1") (h "1x7gsds6v3aj1ib1vf5jx7rp6d08fzl8vn9lbny4d583amv4zi4f")))

(define-public crate-bevy_ptr-0.10.0 (c (n "bevy_ptr") (v "0.10.0") (h "0z3a4pswq14cmyqzhpmca0664fx7wqklba62j9xr2w230vd28v59")))

(define-public crate-bevy_ptr-0.10.1 (c (n "bevy_ptr") (v "0.10.1") (h "1pm4qycm1czj2yf7c571db9fz1k8gf9smnv7zwxkanjc3m2qhjqc")))

(define-public crate-bevy_ptr-0.11.0 (c (n "bevy_ptr") (v "0.11.0") (h "0hh22rsx247vy27ypn1is3zbx3169zrqic6377sm601sb4vwyky7")))

(define-public crate-bevy_ptr-0.11.1 (c (n "bevy_ptr") (v "0.11.1") (h "0rfzz696mlz3qjqh9s5d1b46ysffzy80frn7frpngn4bzi293swy")))

(define-public crate-bevy_ptr-0.11.2 (c (n "bevy_ptr") (v "0.11.2") (h "1l6jldjkd14vwg202m44qn4swyg00238lhljmg175b0g8bzjsw0m")))

(define-public crate-bevy_ptr-0.11.3 (c (n "bevy_ptr") (v "0.11.3") (h "1ijml7z6c9588yr6dl4200p8ya7m3mf24a308f77svx405j5iivj")))

(define-public crate-bevy_ptr-0.12.0 (c (n "bevy_ptr") (v "0.12.0") (h "1wmdx18phswfap8fin11m9xw05i0d9nsw3z23pvixqkckxkh52ih")))

(define-public crate-bevy_ptr-0.12.1 (c (n "bevy_ptr") (v "0.12.1") (h "1kr3sida5nbflca7n7xn7hcqs2j0y2scrxcfa2bc3kdgiw6c4zn7")))

(define-public crate-bevy_ptr-0.13.0 (c (n "bevy_ptr") (v "0.13.0") (h "0yv7sq6bn36z26wvnz3i0ild3v9fpabafa3g3vz10sz0isla9bw6")))

(define-public crate-bevy_ptr-0.13.1 (c (n "bevy_ptr") (v "0.13.1") (h "14dldzskpi33d3rqsshy8gzjyyblxricpiqch1rjpw0f0223a07a")))

(define-public crate-bevy_ptr-0.13.2 (c (n "bevy_ptr") (v "0.13.2") (h "0s3jk6kbxizn56w0rj67l83khrqjzw0mlfr0fildnhg3ky3f4l40")))

