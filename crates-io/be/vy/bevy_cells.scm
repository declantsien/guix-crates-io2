(define-module (crates-io be vy bevy_cells) #:use-module (crates-io))

(define-public crate-bevy_cells-0.0.1-dev (c (n "bevy_cells") (v "0.0.1-dev") (d (list (d (n "aery") (r "^0.4") (d #t) (k 0)) (d (n "bevy") (r "^0.11.3") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1gsncsqa3cvmwjq5q3pc2z6rvxv1my9a0mwjzzsdj0gdaikwq9v2")))

(define-public crate-bevy_cells-0.1.0 (c (n "bevy_cells") (v "0.1.0") (d (list (d (n "aery") (r "^0.5.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0jgjw5w8dsz9gp3r7138n9f57dfgisvp0nyiv1hnlpp3rrvj1rp5")))

(define-public crate-bevy_cells-0.1.1 (c (n "bevy_cells") (v "0.1.1") (d (list (d (n "aery") (r "^0.5.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "10xx8zlwlf7b9c9z4f3bfq8v4w1658wabk11cpgxila1d006mwvb")))

