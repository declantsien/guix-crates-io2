(define-module (crates-io be vy bevy_color_blindness) #:use-module (crates-io))

(define-public crate-bevy_color_blindness-0.1.0 (c (n "bevy_color_blindness") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)))) (h "0a7hmnlfyazgwy49img6g3v4zq3p7rg39wbp64vbir8f4cm1fvav")))

(define-public crate-bevy_color_blindness-0.2.0 (c (n "bevy_color_blindness") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)))) (h "0sgkx4y1hqpx5habd6i3n4g5xn1n00di7m4n15y2hzg2107ihv41")))

