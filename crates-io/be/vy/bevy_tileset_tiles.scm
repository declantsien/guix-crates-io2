(define-module (crates-io be vy bevy_tileset_tiles) #:use-module (crates-io))

(define-public crate-bevy_tileset_tiles-0.2.0 (c (n "bevy_tileset_tiles") (v "0.2.0") (d (list (d (n "bevy_asset") (r "^0.5") (d #t) (k 0)) (d (n "bevy_render") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0944r5wvihqfz9rz8ai79hm83wmjl25yfhgb3bap7svqh7jfgdv4") (f (quote (("variants") ("default") ("auto-tile" "variants"))))))

(define-public crate-bevy_tileset_tiles-0.3.0 (c (n "bevy_tileset_tiles") (v "0.3.0") (d (list (d (n "bevy_asset") (r "^0.6") (k 0)) (d (n "bevy_render") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0rdxvar7zg6rgk4k3lgg5v785abk12h1g0mcrr2f1ypi8z69dv5k") (f (quote (("variants") ("default") ("auto-tile" "variants"))))))

(define-public crate-bevy_tileset_tiles-0.4.0 (c (n "bevy_tileset_tiles") (v "0.4.0") (d (list (d (n "bevy_asset") (r "^0.7") (k 0)) (d (n "bevy_render") (r "^0.7") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1nbljc027gxii4b8kfwx0nhclfabaq759py0z0qs7vchkf0p2nh8") (f (quote (("variants") ("default") ("auto-tile" "variants"))))))

(define-public crate-bevy_tileset_tiles-0.5.0 (c (n "bevy_tileset_tiles") (v "0.5.0") (d (list (d (n "bevy_asset") (r "^0.8") (k 0)) (d (n "bevy_render") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1j3cy8fjkslkg54ygly4wabsl1zbx89c4frsdksbjmfavp4c7g0c") (f (quote (("variants") ("default") ("auto-tile" "variants"))))))

(define-public crate-bevy_tileset_tiles-0.6.0 (c (n "bevy_tileset_tiles") (v "0.6.0") (d (list (d (n "bevy_asset") (r "^0.9") (k 0)) (d (n "bevy_render") (r "^0.9") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "04dri22idk390rda0pnncs37igip1qdcs9dqasdln82awcwr12li") (f (quote (("variants") ("default") ("auto-tile" "variants"))))))

(define-public crate-bevy_tileset_tiles-0.7.0 (c (n "bevy_tileset_tiles") (v "0.7.0") (d (list (d (n "bevy_asset") (r "^0.10") (k 0)) (d (n "bevy_render") (r "^0.10") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0gaq4mc8bjpx3ag8w7pmxsh6vdd980jpb85b6vahjcz7bcflwa6a") (f (quote (("variants") ("default") ("auto-tile" "variants"))))))

(define-public crate-bevy_tileset_tiles-0.8.0 (c (n "bevy_tileset_tiles") (v "0.8.0") (d (list (d (n "bevy_asset") (r "^0.11") (k 0)) (d (n "bevy_render") (r "^0.11") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1fixwd4iy0g50ysngabjib4qzwammrw5cj5ncq17jf9c8n9jbjsh") (f (quote (("variants") ("default") ("auto-tile" "variants"))))))

