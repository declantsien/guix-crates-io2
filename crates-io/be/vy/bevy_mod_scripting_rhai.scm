(define-module (crates-io be vy bevy_mod_scripting_rhai) #:use-module (crates-io))

(define-public crate-bevy_mod_scripting_rhai-0.2.0 (c (n "bevy_mod_scripting_rhai") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.1") (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.2.0") (d #t) (k 0)) (d (n "rhai") (r "^1.8.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "1ycs6wbxw5k5j61nrdzd8s3gk6rybqs5nsksixxs3kv6c4g1n1pk")))

(define-public crate-bevy_mod_scripting_rhai-0.2.1 (c (n "bevy_mod_scripting_rhai") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9.1") (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.2.1") (d #t) (k 0)) (d (n "rhai") (r "^1.8.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "10pvzyckkc7j9l9yc1j7jsbbc62ib7l1xx7p1a9pm9bcbd7fi5s4")))

(define-public crate-bevy_mod_scripting_rhai-0.2.2 (c (n "bevy_mod_scripting_rhai") (v "0.2.2") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.2.2") (d #t) (k 0)) (d (n "rhai") (r "^1.8.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "0lza81hxcx750rs0kqwmpmm2llkrac1s5w07m7nvlzcjg287g57w")))

(define-public crate-bevy_mod_scripting_rhai-0.3.0 (c (n "bevy_mod_scripting_rhai") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10.1") (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.3.0") (d #t) (k 0)) (d (n "rhai") (r "^1.8.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "1akwwrz274c9ncfmm2gimzy641j9l8xn8dz9vjnslgf3w34rlw7n")))

(define-public crate-bevy_mod_scripting_rhai-0.4.0 (c (n "bevy_mod_scripting_rhai") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.4.0") (d #t) (k 0)) (d (n "rhai") (r "^1.16") (f (quote ("sync"))) (d #t) (k 0)))) (h "1889xdaivf5a4d1pk5l0yn6wv0ayai8qs6qfq727wy7kclwyf1pg")))

(define-public crate-bevy_mod_scripting_rhai-0.5.0 (c (n "bevy_mod_scripting_rhai") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.5.0") (d #t) (k 0)) (d (n "rhai") (r "^1.16") (f (quote ("sync"))) (d #t) (k 0)))) (h "0p5lbg8c68c81s5zwi8b5bj06hbp7rwwn1lfvc2c5ca7mlnch87x")))

(define-public crate-bevy_mod_scripting_rhai-0.6.0 (c (n "bevy_mod_scripting_rhai") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bevy") (r "=0.13.1") (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.6.0") (d #t) (k 0)) (d (n "rhai") (r "^1.16") (f (quote ("sync"))) (d #t) (k 0)))) (h "01jls8lpl0l2an33pc64zcahpz5hj6axg9jab1a5iik0fiajf6f6")))

