(define-module (crates-io be vy bevy_default_font) #:use-module (crates-io))

(define-public crate-bevy_default_font-0.1.0 (c (n "bevy_default_font") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy") (r "^0.9.1") (f (quote ("wayland"))) (d #t) (k 2)) (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "13314gzf36lg15kyds4zn308ndzqafy30qq7p6jcdhib8jnciggx") (y #t)))

