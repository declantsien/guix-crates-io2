(define-module (crates-io be vy bevy_ui_style_builder) #:use-module (crates-io))

(define-public crate-bevy_ui_style_builder-0.1.0 (c (n "bevy_ui_style_builder") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (f (quote ("bevy_ui" "render"))) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "0aziwjcpjkn09r6i9nkcj8n7f6c8232r86fqlfjrdn2w79awfjpk")))

(define-public crate-bevy_ui_style_builder-0.2.0 (c (n "bevy_ui_style_builder") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.1") (f (quote ("bevy_ui" "render"))) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "0z5rywi2kv0x525pi7md58fmchnsiar1jvwjhp15xj3cxafmwh3i")))

(define-public crate-bevy_ui_style_builder-0.2.2 (c (n "bevy_ui_style_builder") (v "0.2.2") (d (list (d (n "bevy") (r "^0.9.1") (f (quote ("bevy_ui" "render"))) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "19n8ffb5y3dz5y2d2ydv9qac16bk1mhm1sgbiwqzxh59xpjvndsd")))

(define-public crate-bevy_ui_style_builder-0.2.3 (c (n "bevy_ui_style_builder") (v "0.2.3") (d (list (d (n "bevy") (r "^0.9.1") (f (quote ("bevy_ui" "render"))) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "0s0d27dvihci6z1kgpzm000dg0j5ryf5hrvyb0bj888vh3dpmjm0")))

