(define-module (crates-io be vy bevy_cleanup) #:use-module (crates-io))

(define-public crate-bevy_cleanup-0.1.0 (c (n "bevy_cleanup") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.2") (k 0)) (d (n "bevy_cleanup_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0qdshvsb0fh3y2yd9mqxgjxsc30jsvxw9ypkqxdiv1lkpsb539an") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:bevy_cleanup_derive"))))))

