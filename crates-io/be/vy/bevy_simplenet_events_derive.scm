(define-module (crates-io be vy bevy_simplenet_events_derive) #:use-module (crates-io))

(define-public crate-bevy_simplenet_events_derive-0.0.1 (c (n "bevy_simplenet_events_derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17hxnfcn07x7hjyp88lwkvv8bj4qffzrlqdywyfc0v03simx940a")))

(define-public crate-bevy_simplenet_events_derive-0.1.0 (c (n "bevy_simplenet_events_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0nkcmfklaywiixs8k3vwl3sqdbdj4akhby42p7adzlpd1fb4vrr9")))

