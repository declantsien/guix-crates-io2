(define-module (crates-io be vy bevy_http) #:use-module (crates-io))

(define-public crate-bevy_http-0.1.0 (c (n "bevy_http") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset"))) (k 0)) (d (n "futures-core") (r "^0.3.29") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)))) (h "0q90mq8n9vdc2ljcvcwds60x49cx71n26j0a1p57wjr7fx6r08xa")))

(define-public crate-bevy_http-0.2.0 (c (n "bevy_http") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset"))) (k 0)) (d (n "futures-core") (r "^0.3.29") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)))) (h "1nxjnsxhv30m1f5js1nfgf6qms9glvkzq0vzm62ai6g3b4vag2y5")))

