(define-module (crates-io be vy bevy_2dviewangle_macro) #:use-module (crates-io))

(define-public crate-bevy_2dviewangle_macro-0.1.0 (c (n "bevy_2dviewangle_macro") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_pbr" "bevy_sprite"))) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0s8fp3k69qpgma67hi6h42b8d4kh8xgpmxb4ywczhq9g19na8f74")))

(define-public crate-bevy_2dviewangle_macro-0.1.1 (c (n "bevy_2dviewangle_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0a3qgj1p6cyr4v05f9pgivzbxgp7yzgr8m7di96rawca96wv2f05")))

(define-public crate-bevy_2dviewangle_macro-0.2.0 (c (n "bevy_2dviewangle_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0b1bmc0206b4zjqkr35fmhvvdmnms52r4x9ycfdv1vlh7m1rwcxx")))

(define-public crate-bevy_2dviewangle_macro-0.2.1 (c (n "bevy_2dviewangle_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0zjd5wai69gs5laqcp89gdg9vyn0z0mh00cjb7vs38ij6hhiklw6")))

(define-public crate-bevy_2dviewangle_macro-0.2.2 (c (n "bevy_2dviewangle_macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "18jiaklap0ddx4fnxp8b73w4shsxhdmhh05wql70arigijyafl6r")))

(define-public crate-bevy_2dviewangle_macro-0.3.0 (c (n "bevy_2dviewangle_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0p2gmyiaylzx24va2z8r9z1fhlkn53vzz0h9x5vms6mv30wqgd16")))

(define-public crate-bevy_2dviewangle_macro-0.4.0 (c (n "bevy_2dviewangle_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0xizchhcrh4zmwk4f859xy1xrxfy46j7vjqkwah17895flxs8ga7")))

