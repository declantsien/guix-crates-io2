(define-module (crates-io be vy bevy_despawn_with) #:use-module (crates-io))

(define-public crate-bevy_despawn_with-0.1.0 (c (n "bevy_despawn_with") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)))) (h "1gl1la4hn5g38nd83cyqbarinqhfrwxrlayc6n1b8c6k29f9n2gd")))

(define-public crate-bevy_despawn_with-0.1.1 (c (n "bevy_despawn_with") (v "0.1.1") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)))) (h "0j6p7gl192frnqd5pppj2sj4xama75kv22fbxcpbi63gwbcms1k6")))

(define-public crate-bevy_despawn_with-0.2.0 (c (n "bevy_despawn_with") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)))) (h "0l12w1zaydq8am8c9bjz2l8m784sqrkvb4279kdkf22bpakzh0cq")))

(define-public crate-bevy_despawn_with-0.2.1 (c (n "bevy_despawn_with") (v "0.2.1") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)))) (h "0l54mkci6yvzcla29blcaxxaih8xarhjfg1vbd5964cxm6cic3sn")))

(define-public crate-bevy_despawn_with-0.3.0 (c (n "bevy_despawn_with") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)))) (h "1f3hsfknafdkxrw06avf1y6hvv366f96wnk03zq5gn059al9nm0h")))

(define-public crate-bevy_despawn_with-0.4.0 (c (n "bevy_despawn_with") (v "0.4.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "14dhf0z15nnc3k2gk51hxi2raa163rlmv9fk86wrsn875nw0pgz8")))

(define-public crate-bevy_despawn_with-0.5.0 (c (n "bevy_despawn_with") (v "0.5.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "14b0arnnqdj1c6i3i04bv298gi6iq0dm3hw31jz8vap1h1v3pkd7")))

(define-public crate-bevy_despawn_with-0.6.0 (c (n "bevy_despawn_with") (v "0.6.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "06241yfn8d1kvwdm0lfihrh124yg2z946mb8n2mll5ra2pa6gifv")))

(define-public crate-bevy_despawn_with-0.7.0 (c (n "bevy_despawn_with") (v "0.7.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "15mg50267dzw23ak13nz0z93l8nwwz9g7x0w852mf353xr36l296") (f (quote (("retain") ("default" "retain"))))))

(define-public crate-bevy_despawn_with-0.8.0 (c (n "bevy_despawn_with") (v "0.8.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "028parrdf8p7lrm3gmd7hj5w4q3l00cavikyxlnddnd44pr723ay") (f (quote (("retain"))))))

(define-public crate-bevy_despawn_with-0.9.0 (c (n "bevy_despawn_with") (v "0.9.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fkmr3jbd1304mbnr2bn96bj0swahw21dg38h39rqx143r8hbv7b") (f (quote (("retain"))))))

(define-public crate-bevy_despawn_with-0.9.1 (c (n "bevy_despawn_with") (v "0.9.1") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "15mcdk5gbkll9b1b1yfrqzq7sykzkn6i43zjbgwzcsfy1fzl45bm") (f (quote (("retain"))))))

(define-public crate-bevy_despawn_with-0.10.0 (c (n "bevy_despawn_with") (v "0.10.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1dz9y5day0cm91q248a7g5xggv0adkwgp6fzigbpdw9my4j899g7") (f (quote (("retain"))))))

(define-public crate-bevy_despawn_with-0.11.0 (c (n "bevy_despawn_with") (v "0.11.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "19w1j8jvs9lmkg6ackx74zmsmn3pjj97p95d08n61cnjq5arzlhk") (f (quote (("retain"))))))

(define-public crate-bevy_despawn_with-0.12.0 (c (n "bevy_despawn_with") (v "0.12.0") (d (list (d (n "bevy") (r "^0.8") (k 0)))) (h "1xhnwhhlps1pn1d8m3vad9bz5kb2rd2ap6x90wd1jw6vyjhskcb2") (f (quote (("retain"))))))

(define-public crate-bevy_despawn_with-0.12.1 (c (n "bevy_despawn_with") (v "0.12.1") (d (list (d (n "bevy") (r "^0.8") (k 0)))) (h "11a0kbdw52ij5xjfz1p0wrlfl0g3rjcawqcx34rmpwwc0hmvaxhs") (f (quote (("retain"))))))

(define-public crate-bevy_despawn_with-0.13.0 (c (n "bevy_despawn_with") (v "0.13.0") (d (list (d (n "bevy") (r "^0.8") (k 0)))) (h "1ansb2k4hgx6cmj6pzaskzd3l2k3brdd8yjwm95ybxm1hbyhn0sg") (f (quote (("retain") ("remove"))))))

(define-public crate-bevy_despawn_with-0.14.0 (c (n "bevy_despawn_with") (v "0.14.0") (d (list (d (n "bevy") (r "^0.9") (k 0)))) (h "0bwb9fcqv0h9dg8sb7gqjcww6pypd1daq6sb21rmws52h92rfgmd")))

(define-public crate-bevy_despawn_with-0.15.0 (c (n "bevy_despawn_with") (v "0.15.0") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "12jg16504kqrd8rc8q43075616n9zh5w84gcpx5mhlvwvdrzkgql")))

