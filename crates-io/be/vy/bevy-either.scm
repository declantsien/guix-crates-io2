(define-module (crates-io be vy bevy-either) #:use-module (crates-io))

(define-public crate-bevy-either-0.1.0 (c (n "bevy-either") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0m4n5w3l53pxdb8q1z2kr08wsic7c9w2x0qphpsbik8gm1pm0x68")))

(define-public crate-bevy-either-0.2.0 (c (n "bevy-either") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1718bsyzvc8adcd452a17ji54286kzd8hj77n6yly2ghxmb9knc4")))

