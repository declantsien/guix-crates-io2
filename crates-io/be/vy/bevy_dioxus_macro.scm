(define-module (crates-io be vy bevy_dioxus_macro) #:use-module (crates-io))

(define-public crate-bevy_dioxus_macro-0.1.1 (c (n "bevy_dioxus_macro") (v "0.1.1") (d (list (d (n "fermi") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1cvsjbaix6036h1zacc60j91qbralsflwybfhbscf2fwll1ghrgj")))

