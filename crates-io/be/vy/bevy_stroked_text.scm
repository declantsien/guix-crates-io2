(define-module (crates-io be vy bevy_stroked_text) #:use-module (crates-io))

(define-public crate-bevy_stroked_text-0.1.0 (c (n "bevy_stroked_text") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite" "bevy_winit" "bevy_core_pipeline" "x11" "default_font"))) (k 2)))) (h "0hm9myjh2kwhx3j7j962ncpz25myryh56barrgdavrba6jm6f4q7")))

(define-public crate-bevy_stroked_text-0.1.1 (c (n "bevy_stroked_text") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite" "bevy_winit" "bevy_core_pipeline" "x11" "default_font"))) (k 2)))) (h "14pilbg0cl2s4n4bgr112hjw0h8va1gcxmf4ifrcg8g5hfqghwg9")))

(define-public crate-bevy_stroked_text-0.2.0 (c (n "bevy_stroked_text") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite" "bevy_winit" "bevy_core_pipeline" "x11" "default_font"))) (k 2)))) (h "1v97nagm3382cf6my55qvap1cia7n2404xdj7074rrkmxz0sp8aj")))

