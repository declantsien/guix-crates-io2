(define-module (crates-io be vy bevy_encase_derive) #:use-module (crates-io))

(define-public crate-bevy_encase_derive-0.8.0 (c (n "bevy_encase_derive") (v "0.8.0") (d (list (d (n "bevy_macro_utils") (r "^0.8.0") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.3.0") (d #t) (k 0)))) (h "1anmkzv1wj2jr1lydm8fv5lp1pnl4sq46s3kga8zrqbcqfnng9vn")))

(define-public crate-bevy_encase_derive-0.8.1 (c (n "bevy_encase_derive") (v "0.8.1") (d (list (d (n "bevy_macro_utils") (r "^0.8.0") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.3.0") (d #t) (k 0)))) (h "1wdd30q10s4qhk7qbjcrbn469hl3s4ni822alryvksf51501kg38")))

(define-public crate-bevy_encase_derive-0.9.0 (c (n "bevy_encase_derive") (v "0.9.0") (d (list (d (n "bevy_macro_utils") (r "^0.9.0") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.4.0") (d #t) (k 0)))) (h "05drqpfzswmil5i5ppk9is7dck7qav2w0f2k4bg3vmihk08p61rs")))

(define-public crate-bevy_encase_derive-0.9.1 (c (n "bevy_encase_derive") (v "0.9.1") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.4.0") (d #t) (k 0)))) (h "093sx95m2npxqp1yfd566c3r4nz5x767m0r9dhpfr4hfsfq6nawn")))

(define-public crate-bevy_encase_derive-0.10.0 (c (n "bevy_encase_derive") (v "0.10.0") (d (list (d (n "bevy_macro_utils") (r "^0.10.0") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.5.0") (d #t) (k 0)))) (h "052gj5a72rz9fq3hnh15fx1z41vk0sfhlk1993rmb2gqs4w4hgbj")))

(define-public crate-bevy_encase_derive-0.10.1 (c (n "bevy_encase_derive") (v "0.10.1") (d (list (d (n "bevy_macro_utils") (r "^0.10.1") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.5.0") (d #t) (k 0)))) (h "13k4ah337if2b16xnrgv8na2fjn7ghkl6abpcdyph8jm3zffp1n0")))

(define-public crate-bevy_encase_derive-0.11.0 (c (n "bevy_encase_derive") (v "0.11.0") (d (list (d (n "bevy_macro_utils") (r "^0.11.0") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.6.1") (d #t) (k 0)))) (h "0mkb9ljzysw6jhkw02m40l12b1fsgdi6q0h9kr8hr2n6pn6q1pvb")))

(define-public crate-bevy_encase_derive-0.11.1 (c (n "bevy_encase_derive") (v "0.11.1") (d (list (d (n "bevy_macro_utils") (r "^0.11.1") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.6.1") (d #t) (k 0)))) (h "0zb6dasxvdcnpcfk7j7xvvm8f7qghhpcgpbyn88w5xq25yq06ql0")))

(define-public crate-bevy_encase_derive-0.11.2 (c (n "bevy_encase_derive") (v "0.11.2") (d (list (d (n "bevy_macro_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.6.1") (d #t) (k 0)))) (h "03hq6bw1yqa8q9jm18n3fb41h3wafq4q9g2ix2vxir2f44yzp3ys")))

(define-public crate-bevy_encase_derive-0.11.3 (c (n "bevy_encase_derive") (v "0.11.3") (d (list (d (n "bevy_macro_utils") (r "^0.11.3") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.6.1") (d #t) (k 0)))) (h "00b001cpycp5x3z5iccx9qgd7lsyr5gvsnzkn3kipjkbmmahzb6h")))

(define-public crate-bevy_encase_derive-0.12.0 (c (n "bevy_encase_derive") (v "0.12.0") (d (list (d (n "bevy_macro_utils") (r "^0.12.0") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.6.1") (d #t) (k 0)))) (h "19xksadr1wgpqy5v0d0v3c035aazsdq990xar9qapqy4c9dgpfb5")))

(define-public crate-bevy_encase_derive-0.12.1 (c (n "bevy_encase_derive") (v "0.12.1") (d (list (d (n "bevy_macro_utils") (r "^0.12.1") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.6.1") (d #t) (k 0)))) (h "14490a5k885136nd23jradx4yg22vhl9bsfh0zzvnglkbrqs6a2k")))

(define-public crate-bevy_encase_derive-0.13.0 (c (n "bevy_encase_derive") (v "0.13.0") (d (list (d (n "bevy_macro_utils") (r "^0.13.0") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.7") (d #t) (k 0)))) (h "0p1yw6h92ap5lggikb86rlyc34jfnd1wrj0pks0icsz748ck0gbc")))

(define-public crate-bevy_encase_derive-0.13.1 (c (n "bevy_encase_derive") (v "0.13.1") (d (list (d (n "bevy_macro_utils") (r "^0.13.1") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.7") (d #t) (k 0)))) (h "1w720chp73yix6vlrqvpypqr24mf89a82hkcfc2q3nzwfc9aqwnz")))

(define-public crate-bevy_encase_derive-0.13.2 (c (n "bevy_encase_derive") (v "0.13.2") (d (list (d (n "bevy_macro_utils") (r "^0.13.2") (d #t) (k 0)) (d (n "encase_derive_impl") (r "^0.7") (d #t) (k 0)))) (h "19w3pkp7bml7xlh94rdcvy7ws16ax7kdfkd86c3z5n92wnjqfw48")))

