(define-module (crates-io be vy bevy_prototype_networking_laminar) #:use-module (crates-io))

(define-public crate-bevy_prototype_networking_laminar-0.1.0 (c (n "bevy_prototype_networking_laminar") (v "0.1.0") (d (list (d (n "bevy") (r "^0.1.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "laminar") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.115") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)) (d (n "smallvec") (r "^1.4.2") (d #t) (k 2)))) (h "0cgarnryf6912hg59vbcnxq3xi1vd75a9qmv53q8lcva01rasxkv")))

