(define-module (crates-io be vy bevy_input_mapper) #:use-module (crates-io))

(define-public crate-bevy_input_mapper-0.0.1 (c (n "bevy_input_mapper") (v "0.0.1") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)))) (h "0f53rwdnk1a9097qb8qrfz9bwffc93hdkpdv47q5lxm9wxbq0432") (f (quote (("bind_macro"))))))

(define-public crate-bevy_input_mapper-0.0.2 (c (n "bevy_input_mapper") (v "0.0.2") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)))) (h "04wq267k9sbifvj2f486hgj1z7rlf8mf6pdhhpnclz263y3sjdwn") (f (quote (("bind_macro"))))))

(define-public crate-bevy_input_mapper-0.0.3 (c (n "bevy_input_mapper") (v "0.0.3") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)))) (h "0x675b6jphpv4fw21s5nxbwms2mfgpimyl2i7kalxk0zyql4p32x") (f (quote (("bind_macro"))))))

