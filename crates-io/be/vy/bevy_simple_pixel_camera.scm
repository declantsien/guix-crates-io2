(define-module (crates-io be vy bevy_simple_pixel_camera) #:use-module (crates-io))

(define-public crate-bevy_simple_pixel_camera-0.1.0 (c (n "bevy_simple_pixel_camera") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("dynamic_linking"))) (d #t) (k 0)))) (h "1n142wc57jg3cvv2jmfv89zj34i59cyw3gsihhlz8ilnlrkzdfnp")))

(define-public crate-bevy_simple_pixel_camera-0.1.1 (c (n "bevy_simple_pixel_camera") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("dynamic_linking"))) (d #t) (k 0)))) (h "1waff0dwirmyzjzxjp7fpl1csp7g5hjgmvnpkayd447ris5vja37")))

