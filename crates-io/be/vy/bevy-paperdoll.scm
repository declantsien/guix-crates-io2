(define-module (crates-io be vy bevy-paperdoll) #:use-module (crates-io))

(define-public crate-bevy-paperdoll-0.1.0 (c (n "bevy-paperdoll") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_ui" "bevy_winit" "default_font" "x11"))) (k 2)) (d (n "paperdoll-tar") (r "^0.1") (d #t) (k 0)))) (h "0f5v75295bi39vl6gyzc1c2piihrig3qp31qqxppymskqm0gljlm")))

(define-public crate-bevy-paperdoll-0.2.0 (c (n "bevy-paperdoll") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_ui" "bevy_winit" "default_font" "x11"))) (k 2)) (d (n "paperdoll-tar") (r "^0.1") (d #t) (k 0)))) (h "19cyhd3qmk50q3lf21nj99d3q40gxd4kd04d7xkv3zwsjjm3y9wh")))

(define-public crate-bevy-paperdoll-0.3.0 (c (n "bevy-paperdoll") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_winit" "default_font" "x11"))) (k 2)) (d (n "paperdoll-tar") (r "^0.1") (d #t) (k 0)))) (h "1zrgdwxwwsi6mmnclca0z0qy3j1ylfcq3a44dfy4pdij8vfnkskz")))

