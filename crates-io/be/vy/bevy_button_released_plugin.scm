(define-module (crates-io be vy bevy_button_released_plugin) #:use-module (crates-io))

(define-public crate-bevy_button_released_plugin-0.1.0 (c (n "bevy_button_released_plugin") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)))) (h "1xkx9c4ijf8izjaikzlsx458hydzk827pb1snkaqvxfys6xzkqbs")))

(define-public crate-bevy_button_released_plugin-0.2.0 (c (n "bevy_button_released_plugin") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "0k745wfwb306dvllih10kc1awka5shx4wl0jq0pr86zngwcgfbqa")))

(define-public crate-bevy_button_released_plugin-0.3.0 (c (n "bevy_button_released_plugin") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "07gf5g37ks182313k0iqwfjm495abizlb5nl9c074il5vlgl5y71")))

(define-public crate-bevy_button_released_plugin-0.3.1 (c (n "bevy_button_released_plugin") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_ui" "bevy_asset" "bevy_text" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)))) (h "0ca5a3f36vj9nr5i1m0rwqrv5fzyq6yqyamk3qswlpspq7kjz9af") (f (quote (("default" "auto_add") ("auto_add"))))))

(define-public crate-bevy_button_released_plugin-0.5.0 (c (n "bevy_button_released_plugin") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_asset" "bevy_text" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "16wg5wsi990w04m7f9bps1mq42yn0gla8favgvqkymmlv8ihiwx7") (f (quote (("default" "auto_add") ("auto_add"))))))

(define-public crate-bevy_button_released_plugin-0.5.1 (c (n "bevy_button_released_plugin") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_asset" "bevy_text" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "13cgh2slxaisc5s21v2xni8lgydpgqrr4ygdc27dg467jhjshbch") (f (quote (("default" "auto_add") ("auto_add"))))))

