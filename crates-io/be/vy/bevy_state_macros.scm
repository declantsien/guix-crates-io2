(define-module (crates-io be vy bevy_state_macros) #:use-module (crates-io))

(define-public crate-bevy_state_macros-0.1.0 (c (n "bevy_state_macros") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "01q5f5bp342l851lsrb4n2ml2ik1xbhkq6zfj43xr3by5pfqs2f4")))

(define-public crate-bevy_state_macros-0.2.0 (c (n "bevy_state_macros") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1zhcdsdkamsv09vw3qlci2d1i5xvqnxj9dg5wbf9qmhm7yl9iwwd")))

(define-public crate-bevy_state_macros-0.3.0 (c (n "bevy_state_macros") (v "0.3.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_state_stack") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "18lhy5n4l310xw4wmmlpnwv0y649m0f645fbq853chzkj7zrwv1x")))

