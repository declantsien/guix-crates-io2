(define-module (crates-io be vy bevy_trait) #:use-module (crates-io))

(define-public crate-bevy_trait-0.0.0 (c (n "bevy_trait") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0miapanslzf3d20wn1np4rv8agz3fg9c1i4p73h9b3fz1fd0sj6s")))

(define-public crate-bevy_trait-0.1.0 (c (n "bevy_trait") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)) (d (n "bevy") (r "^0.10.0") (d #t) (k 2)))) (h "02ligy64mcfnlmwwzazb8hqhf3xk1ak3av00ch38i5a5230ygix3")))

(define-public crate-bevy_trait-0.2.0 (c (n "bevy_trait") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)) (d (n "bevy") (r "^0.10.0") (d #t) (k 2)))) (h "0ww71f21pqy22mvx3avr12b8mqbsggl09zd3v243ppxhhaayiac6")))

