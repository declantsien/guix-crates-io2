(define-module (crates-io be vy bevy_python_ffi) #:use-module (crates-io))

(define-public crate-bevy_python_ffi-0.1.0 (c (n "bevy_python_ffi") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (k 0)))) (h "138z5g1jqp9iflkf4p8krzwgaq1slxp9hgwi8pxz743imdmph3yl")))

(define-public crate-bevy_python_ffi-0.2.0 (c (n "bevy_python_ffi") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "pyo3") (r "^0.21") (f (quote ("extension-module" "macros"))) (d #t) (k 0)))) (h "0ys1iasc2f96vz38wgycwz9q9ldmx21hg4lx76xyb393nq1s46gg")))

