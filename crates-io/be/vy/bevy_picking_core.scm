(define-module (crates-io be vy bevy_picking_core) #:use-module (crates-io))

(define-public crate-bevy_picking_core-0.1.0 (c (n "bevy_picking_core") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_ui" "bevy_asset"))) (k 0)))) (h "1qlv562knnbd8j3shyg2m42z2s0dcrdkf9ywblmiibcm0chsylff")))

(define-public crate-bevy_picking_core-0.2.0 (c (n "bevy_picking_core") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_ui" "bevy_asset"))) (k 0)) (d (n "bevy_eventlistener") (r "^0.2") (d #t) (k 0)))) (h "149v4mw101dga8phmlj5b2gr2vciwbw6dv9w0m2gcfh6w5qvy7sa")))

(define-public crate-bevy_picking_core-0.15.0 (c (n "bevy_picking_core") (v "0.15.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_asset"))) (k 0)) (d (n "bevy_eventlistener") (r "^0.3") (d #t) (k 0)))) (h "1bv5b9np10lg3mr5bw98m2b7zlpyrai70szq2wzwpb3ij0ahzc9p")))

(define-public crate-bevy_picking_core-0.16.0 (c (n "bevy_picking_core") (v "0.16.0") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_derive") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "bevy_eventlistener") (r "^0.5") (d #t) (k 0)) (d (n "bevy_math") (r "^0.11") (k 0)) (d (n "bevy_reflect") (r "^0.11") (k 0)) (d (n "bevy_render") (r "^0.11") (k 0)) (d (n "bevy_utils") (r "^0.11") (k 0)) (d (n "bevy_window") (r "^0.11") (k 0)))) (h "09wvs6c8h65wxxccgh1njy5zq53mrgiyq8vj6x54ry5sq72r90gs")))

(define-public crate-bevy_picking_core-0.17.0 (c (n "bevy_picking_core") (v "0.17.0") (d (list (d (n "bevy_app") (r "^0.12") (k 0)) (d (n "bevy_derive") (r "^0.12") (k 0)) (d (n "bevy_ecs") (r "^0.12") (k 0)) (d (n "bevy_eventlistener") (r "^0.6") (d #t) (k 0)) (d (n "bevy_math") (r "^0.12") (k 0)) (d (n "bevy_reflect") (r "^0.12") (k 0)) (d (n "bevy_render") (r "^0.12") (k 0)) (d (n "bevy_utils") (r "^0.12") (k 0)) (d (n "bevy_window") (r "^0.12") (k 0)))) (h "1clapin3ayp3ibxgz5vzpx5672w9kkzpgzcs4238imh58n64i687")))

(define-public crate-bevy_picking_core-0.18.0 (c (n "bevy_picking_core") (v "0.18.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_derive") (r "^0.13") (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_eventlistener") (r "^0.7") (d #t) (k 0)) (d (n "bevy_math") (r "^0.13") (k 0)) (d (n "bevy_reflect") (r "^0.13") (k 0)) (d (n "bevy_render") (r "^0.13") (k 0)) (d (n "bevy_transform") (r "^0.13") (k 0)) (d (n "bevy_utils") (r "^0.13") (k 0)) (d (n "bevy_window") (r "^0.13") (k 0)))) (h "1qm806wfgk5p0vr36crjjmcacq36pdn7q1xz0v8af245835cvj21")))

