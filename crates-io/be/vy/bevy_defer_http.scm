(define-module (crates-io be vy bevy_defer_http) #:use-module (crates-io))

(define-public crate-bevy_defer_http-0.0.1 (c (n "bevy_defer_http") (v "0.0.1") (d (list (d (n "async-io") (r "^2.3.2") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_defer") (r "^0.4") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.1") (d #t) (k 0)) (d (n "hyper") (r "^1.2.0") (f (quote ("http1" "client"))) (d #t) (k 0)) (d (n "smol-hyper") (r "^0.1.1") (f (quote ("async-io"))) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1m8q4mx33plh0d4gpjx0m4yhv3gppdhxayakwq1lngmc9bmxbwhy")))

(define-public crate-bevy_defer_http-0.0.2 (c (n "bevy_defer_http") (v "0.0.2") (d (list (d (n "async-io") (r "^2.3.2") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_defer") (r "^0.5") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.1") (d #t) (k 0)) (d (n "hyper") (r "^1.2.0") (f (quote ("http1" "client"))) (d #t) (k 0)) (d (n "smol-hyper") (r "^0.1.1") (f (quote ("async-io"))) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0sydi9nwwgi4gm77rrcsaygd84j5f17y8pyx6f2mmdii0072rb29")))

