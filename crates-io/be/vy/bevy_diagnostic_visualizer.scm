(define-module (crates-io be vy bevy_diagnostic_visualizer) #:use-module (crates-io))

(define-public crate-bevy_diagnostic_visualizer-0.1.0 (c (n "bevy_diagnostic_visualizer") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8.1") (k 0)) (d (n "bevy") (r "^0.8.1") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.16.0") (k 0)) (d (n "bevy_egui") (r "^0.16.0") (d #t) (k 2)))) (h "0n4hrk59m9hg7qjx3lnwjmc80ycpxp6wfrx9rq8cb13r4rcw4gd8")))

