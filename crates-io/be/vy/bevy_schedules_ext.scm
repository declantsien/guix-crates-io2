(define-module (crates-io be vy bevy_schedules_ext) #:use-module (crates-io))

(define-public crate-bevy_schedules_ext-0.13.0 (c (n "bevy_schedules_ext") (v "0.13.0") (d (list (d (n "bevy_app") (r "^0.13.0") (o #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.0") (k 0)) (d (n "bevy_utils") (r "^0.13.0") (o #t) (k 0)))) (h "0fwbw9vq5dqk5mps1pyv28b1mhb7b2dimdrsy31labjb2j6i24jb") (f (quote (("nesting_containers" "containers" "nesting") ("nesting") ("default" "app_ext" "nesting")))) (s 2) (e (quote (("containers" "dep:bevy_utils") ("app_ext" "dep:bevy_app"))))))

(define-public crate-bevy_schedules_ext-0.13.1 (c (n "bevy_schedules_ext") (v "0.13.1") (d (list (d (n "bevy_app") (r "^0.13.0") (o #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.0") (k 0)) (d (n "bevy_utils") (r "^0.13.0") (o #t) (k 0)))) (h "0nqvzv2z8f7hkccrk1nlq68z6baw7rb1x3pjq4sz6x75gqpjn1hb") (f (quote (("nesting_containers" "containers" "nesting") ("nesting") ("default" "app_ext" "containers" "nesting" "nesting_containers")))) (s 2) (e (quote (("containers" "dep:bevy_utils") ("app_ext" "dep:bevy_app"))))))

(define-public crate-bevy_schedules_ext-0.13.2 (c (n "bevy_schedules_ext") (v "0.13.2") (d (list (d (n "bevy_app") (r "^0.13.0") (o #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.0") (k 0)) (d (n "bevy_utils") (r "^0.13.0") (o #t) (k 0)))) (h "15fmaw9xfaf0f3vw8h8ygbfjcmm82k2qmx5fapv67fyijvzm79by") (f (quote (("states") ("nesting_containers" "containers" "nesting") ("nesting") ("default" "app_ext" "nesting_containers" "states")))) (s 2) (e (quote (("containers" "dep:bevy_utils") ("app_ext" "dep:bevy_app"))))))

