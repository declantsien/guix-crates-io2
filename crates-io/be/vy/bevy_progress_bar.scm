(define-module (crates-io be vy bevy_progress_bar) #:use-module (crates-io))

(define-public crate-bevy_progress_bar-0.5.0 (c (n "bevy_progress_bar") (v "0.5.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_ninepatch") (r "^0.5") (d #t) (k 0)) (d (n "spirv_headers") (r ">=1.5.0, <1.5.1") (d #t) (k 0)))) (h "1jhijmv9dsprxbgl8i3f5jpy48lpmxqnn282f95zawfdnl25i8iy")))

(define-public crate-bevy_progress_bar-0.9.0 (c (n "bevy_progress_bar") (v "0.9.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render"))) (d #t) (k 2)) (d (n "bevy_ninepatch") (r "^0.9") (d #t) (k 0)) (d (n "spirv_headers") (r ">=1.5.0, <1.5.1") (d #t) (k 0)))) (h "1wdbkxq507nqw8wkd2qlk4qc4aapl5md6bxbsgi3gj3rbnrap70h")))

