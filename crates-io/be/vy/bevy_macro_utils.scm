(define-module (crates-io be vy bevy_macro_utils) #:use-module (crates-io))

(define-public crate-bevy_macro_utils-0.6.0 (c (n "bevy_macro_utils") (v "0.6.0") (d (list (d (n "cargo-manifest") (r "^0.2.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kd7f6jn8cw9pams8xwrjmsa2vxn9q9wkinsij0x01knnsrr1ksp")))

(define-public crate-bevy_macro_utils-0.7.0 (c (n "bevy_macro_utils") (v "0.7.0") (d (list (d (n "cargo-manifest") (r "^0.2.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fxc1mnz8kwz3rx9izqqqd7cfqg4852l2vp531ky6iwmm4rzrpdp")))

(define-public crate-bevy_macro_utils-0.8.0 (c (n "bevy_macro_utils") (v "0.8.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1css629z52ara2v9xagg97x4sx43nfk5n989sb51c7kh7gw1l22d")))

(define-public crate-bevy_macro_utils-0.8.1 (c (n "bevy_macro_utils") (v "0.8.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1f8n3san6a7ic142zlbggip5i2pl9i2iwgf3qvbh50qrwlvm3ys3")))

(define-public crate-bevy_macro_utils-0.9.0 (c (n "bevy_macro_utils") (v "0.9.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "05fbfpd2lvsb8pr8xx7jg08zakw3bw3krpixi31dbiaa8frsfvv5")))

(define-public crate-bevy_macro_utils-0.9.1 (c (n "bevy_macro_utils") (v "0.9.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0xvn9yjdzx3zgamc85499arz5mxvhnpi8x4rns8sdvnyjs8vcaq2")))

(define-public crate-bevy_macro_utils-0.10.0 (c (n "bevy_macro_utils") (v "0.10.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19") (d #t) (k 0)))) (h "1ziga4fvr8mavibfx6ynsxyi6qp39k1dbbzv85b47wcj68va6k7j")))

(define-public crate-bevy_macro_utils-0.10.1 (c (n "bevy_macro_utils") (v "0.10.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19") (d #t) (k 0)))) (h "1nfhh6qdmm17s98hvzd54hj28ld7mx9dybgzzwxxqz29n99ywbsv")))

(define-public crate-bevy_macro_utils-0.11.0 (c (n "bevy_macro_utils") (v "0.11.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19") (d #t) (k 0)))) (h "1kbqqdzwnrql8cgvbvz45cdh1cwkb3rr2s3qwgyljckjapjnhf7x")))

(define-public crate-bevy_macro_utils-0.11.1 (c (n "bevy_macro_utils") (v "0.11.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19") (d #t) (k 0)))) (h "1cfgaw6spiq0syp8qazml6sh42yadq59l00whzc8f9sidalxdqj0")))

(define-public crate-bevy_macro_utils-0.11.2 (c (n "bevy_macro_utils") (v "0.11.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19") (d #t) (k 0)))) (h "131g9as5x8qv2vacna9d5grymd5iaavrnbdkb16n61gy0l14dkfi")))

(define-public crate-bevy_macro_utils-0.11.3 (c (n "bevy_macro_utils") (v "0.11.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19") (d #t) (k 0)))) (h "10dr8rjy2bigksn94n0yv459yd1grpkwsn295n1mfklv926w3p93")))

(define-public crate-bevy_macro_utils-0.12.0 (c (n "bevy_macro_utils") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.20") (d #t) (k 0)))) (h "0vcq887ilr3xrrihh9ib1pa2lfdlib52b1m4v2ygqsfhqs3vlvyg")))

(define-public crate-bevy_macro_utils-0.12.1 (c (n "bevy_macro_utils") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.20") (d #t) (k 0)))) (h "0kgdxx84nysx99n4h2a8ps11mgpyrz169iq640yxgkkddc668rp5")))

(define-public crate-bevy_macro_utils-0.13.0 (c (n "bevy_macro_utils") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21") (f (quote ("parse"))) (k 0)))) (h "0752n6582b0857h0qacdkrx09fhvns85m1x4ala7qzhrbg102i5c")))

(define-public crate-bevy_macro_utils-0.13.1 (c (n "bevy_macro_utils") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21") (f (quote ("parse"))) (k 0)))) (h "0r37gkhgpva9lnvq6rwpzdiqp0z97qjx5vjgk7lix8kpqn7zifrv")))

(define-public crate-bevy_macro_utils-0.13.2 (c (n "bevy_macro_utils") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21") (f (quote ("parse"))) (k 0)))) (h "1iqr85vw8zszi9cgc10rl405srrgv85x37hkcnab4hv2m6c0q9zb")))

