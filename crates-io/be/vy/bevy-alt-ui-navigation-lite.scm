(define-module (crates-io be vy bevy-alt-ui-navigation-lite) #:use-module (crates-io))

(define-public crate-bevy-alt-ui-navigation-lite-0.1.0 (c (n "bevy-alt-ui-navigation-lite") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_ui" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "non-empty-vec") (r "^0.2.2") (k 0)))) (h "0zy5k959ljwz7wrza67jfkdazcrrlx2lmcinmanqmniabwdccjiv") (f (quote (("default" "bevy_reflect") ("bevy_reflect"))))))

