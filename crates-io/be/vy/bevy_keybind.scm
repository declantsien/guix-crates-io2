(define-module (crates-io be vy bevy_keybind) #:use-module (crates-io))

(define-public crate-bevy_keybind-1.0.0 (c (n "bevy_keybind") (v "1.0.0") (d (list (d (n "bevy") (r "^0.7.0") (f (quote ("serialize"))) (d #t) (k 2)) (d (n "bevy_app") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_input") (r "^0.7.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "0wyw5swnfnvcd2f77gp7r444app9f7n36llg6zbfpjj97g4fgxmd")))

