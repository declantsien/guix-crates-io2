(define-module (crates-io be vy bevy_fsl_box_frame) #:use-module (crates-io))

(define-public crate-bevy_fsl_box_frame-0.1.0 (c (n "bevy_fsl_box_frame") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_pbr"))) (k 0)) (d (n "bevy_mod_picking") (r "^0.16") (k 0)) (d (n "bevy_polyline") (r "^0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("glam024"))) (d #t) (k 0)) (d (n "parry3d") (r "^0.13") (d #t) (k 0)))) (h "16vi0jnwl00w2rv16rx0pj1lz2p339xv8wj5az8pvdimk4wfsqks")))

(define-public crate-bevy_fsl_box_frame-0.1.1 (c (n "bevy_fsl_box_frame") (v "0.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_pbr"))) (k 0)) (d (n "bevy_mod_picking") (r "^0.16") (k 0)) (d (n "bevy_polyline") (r "^0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("glam024"))) (d #t) (k 0)) (d (n "parry3d") (r "^0.13") (d #t) (k 0)))) (h "1b5g82g7gq8nimywn166ywz58zx61g7mwhv7inyy1l1baaf0mylh")))

(define-public crate-bevy_fsl_box_frame-0.2.0 (c (n "bevy_fsl_box_frame") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_pbr"))) (k 0)) (d (n "bevy_mod_picking") (r "^0.17") (k 0)) (d (n "bevy_polyline") (r "^0.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("glam024"))) (d #t) (k 0)) (d (n "parry3d") (r "^0.13") (d #t) (k 0)))) (h "0smz5xpx55g6bawyfcbg7sd24dill1bpagfnp597x3p8dc6bjzi6")))

