(define-module (crates-io be vy bevy_touch_camera) #:use-module (crates-io))

(define-public crate-bevy_touch_camera-0.1.0 (c (n "bevy_touch_camera") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.2") (k 0)) (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_core_pipeline"))) (k 2)))) (h "0iq2q0pm8g9z7n2in1ibclmz6daximzl0c946sgsz8yvzmv5k39q") (f (quote (("example" "bevy/bevy_winit" "bevy/x11" "bevy/bevy_sprite" "bevy/bevy_ui" "bevy/png") ("default" "bevy/bevy_core_pipeline"))))))

(define-public crate-bevy_touch_camera-0.1.1 (c (n "bevy_touch_camera") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.2") (k 0)) (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_core_pipeline"))) (k 2)))) (h "0ql1p802nqy8i3ms63144mrjrg9wiky27vq3m90n0674nh4xaj38") (f (quote (("example" "bevy/bevy_winit" "bevy/x11" "bevy/bevy_sprite" "bevy/bevy_ui" "bevy/png") ("default" "bevy/bevy_core_pipeline"))))))

(define-public crate-bevy_touch_camera-0.1.2 (c (n "bevy_touch_camera") (v "0.1.2") (d (list (d (n "bevy") (r "^0.12.1") (k 0)) (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_core_pipeline"))) (k 2)))) (h "0wh2m8pn4nf69m9j85pprscv1qggmk0xpypiwfzz6y6102aqsnc0") (f (quote (("example" "bevy/bevy_winit" "bevy/x11" "bevy/bevy_sprite" "bevy/bevy_ui" "bevy/png") ("default" "bevy/bevy_core_pipeline"))))))

