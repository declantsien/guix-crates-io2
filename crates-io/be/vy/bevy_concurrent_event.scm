(define-module (crates-io be vy bevy_concurrent_event) #:use-module (crates-io))

(define-public crate-bevy_concurrent_event-0.1.0 (c (n "bevy_concurrent_event") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)))) (h "19498li8mryg2ni6yz2pnz1v6w9c4r71xw6pcsld42wavkbcl02g")))

