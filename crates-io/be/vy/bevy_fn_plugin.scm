(define-module (crates-io be vy bevy_fn_plugin) #:use-module (crates-io))

(define-public crate-bevy_fn_plugin-0.1.0 (c (n "bevy_fn_plugin") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "bevy") (r "^0.10") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1hy57rhpjymvyqj1qnmwkapc7qyg7b4757ybf9mzr7dbc51fa4x1") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-bevy_fn_plugin-0.1.1 (c (n "bevy_fn_plugin") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "bevy") (r "^0.10") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0gpb7901nwq41m2q1cvn5l7w8q00x7pfnqkswfjjsrgcjjgklz9k") (f (quote (("default") ("debug" "syn/extra-traits"))))))

(define-public crate-bevy_fn_plugin-0.1.2 (c (n "bevy_fn_plugin") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "bevy") (r "^0.10") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1d8xqarsrjrl326sna41nh14g35amwifv69sj5j25d7fk5rlhwqh") (f (quote (("default") ("debug" "syn/extra-traits"))))))

