(define-module (crates-io be vy bevy_spectator) #:use-module (crates-io))

(define-public crate-bevy_spectator-0.1.0 (c (n "bevy_spectator") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "0l170y6dwzgi2dhcydm7350vxkx42fj94gn59sn7aai59vyhm87k") (f (quote (("init") ("default" "init"))))))

(define-public crate-bevy_spectator-0.1.1 (c (n "bevy_spectator") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "0i7d5iqrjd4yafilp802cb6h6864yy0krnbds9gw2kpv4v4wszvd") (f (quote (("init") ("default" "init"))))))

(define-public crate-bevy_spectator-0.2.0 (c (n "bevy_spectator") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "09i2wa47avj51wc91k38dybf8abz9m6xlj9vv1czpsfggckb3bgj") (f (quote (("init") ("default" "init"))))))

(define-public crate-bevy_spectator-0.3.0 (c (n "bevy_spectator") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "x11"))) (k 2)))) (h "1cbgjqmwhi99ajj06hmigyzb3kfhp0j0h1bs04xhpkrs3r2cwk41") (f (quote (("init") ("default" "init"))))))

(define-public crate-bevy_spectator-0.4.0 (c (n "bevy_spectator") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_sprite" "x11" "ktx2" "zstd" "tonemapping_luts"))) (k 2)))) (h "1wwzzawq3518hhrzx2d105ijgj26phnlvsibrw47wwrf9qjp2qv0") (f (quote (("init") ("default" "init"))))))

(define-public crate-bevy_spectator-0.5.0 (c (n "bevy_spectator") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_sprite" "x11" "ktx2" "zstd" "tonemapping_luts"))) (k 2)))) (h "087k81gfg11bypfq378zvbq5pswm5sdnwapfq01xdyxzmkidif1l") (f (quote (("init") ("default" "init"))))))

