(define-module (crates-io be vy bevy_eventlistener_core) #:use-module (crates-io))

(define-public crate-bevy_eventlistener_core-0.1.0 (c (n "bevy_eventlistener_core") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "0475mcxcks1pbgrfy3sdmgqmcxnbihik5arfxr7kh7zhv6kjqxz4")))

(define-public crate-bevy_eventlistener_core-0.1.1 (c (n "bevy_eventlistener_core") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "004b2rbcai7m133l3hazvfplsgy48p7vpq2gkws9db6rb0qibxiw")))

(define-public crate-bevy_eventlistener_core-0.2.0 (c (n "bevy_eventlistener_core") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "14ygc70cmq4l7v6s3kg9j39rjqrbp41bl04i8p01ck6fzvza330z")))

(define-public crate-bevy_eventlistener_core-0.2.1 (c (n "bevy_eventlistener_core") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "1vijcx3isybs9g93725n4fc0nhack09461a23vn8xcd1vd97nii1")))

(define-public crate-bevy_eventlistener_core-0.2.2 (c (n "bevy_eventlistener_core") (v "0.2.2") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "0b1yj1az2phmpjy8i5w00as4q6hqrryrm6s81l42nyrk1qkx68yg")))

(define-public crate-bevy_eventlistener_core-0.3.0 (c (n "bevy_eventlistener_core") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "1sfzj9maj4rs4ask4rxhfzgq8mdww3rg7gv986i7h65n3ki49lyc")))

(define-public crate-bevy_eventlistener_core-0.4.0 (c (n "bevy_eventlistener_core") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "0zbii3qdvwbq28lsx4v4n6xl67l2rvm3h1zisj8p4m3zhmprvw75")))

(define-public crate-bevy_eventlistener_core-0.4.1 (c (n "bevy_eventlistener_core") (v "0.4.1") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "1gbsvmr8bf24ak9ws1p0ngw5y0p0akzrp361i3lajg99h1hfqych")))

(define-public crate-bevy_eventlistener_core-0.5.0 (c (n "bevy_eventlistener_core") (v "0.5.0") (d (list (d (n "bevy_app") (r "^0.11") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11") (d #t) (k 0)))) (h "1sig50bh8ngyww5fsvdd40dikl1zasr8xg6jgdiacjjsjbg2xprk") (f (quote (("trace") ("default" "trace"))))))

(define-public crate-bevy_eventlistener_core-0.5.1 (c (n "bevy_eventlistener_core") (v "0.5.1") (d (list (d (n "bevy_app") (r "^0.11") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11") (d #t) (k 0)))) (h "00wi2vl4bjflfdjdpkl8dn2wfwd3y4a24lmdgpyp3wlkfqi4pjgl") (f (quote (("trace") ("default" "trace"))))))

(define-public crate-bevy_eventlistener_core-0.6.0 (c (n "bevy_eventlistener_core") (v "0.6.0") (d (list (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12") (d #t) (k 0)))) (h "0hd9sk4vmdq0wjdqzn8fnx4ggwyk7cg7pwfz67bkaxlfg0a7npmf") (f (quote (("trace") ("default" "trace"))))))

(define-public crate-bevy_eventlistener_core-0.6.1 (c (n "bevy_eventlistener_core") (v "0.6.1") (d (list (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12") (d #t) (k 0)))) (h "0sf343biwg77891z80w9k6xifs719yr29jxy4wlpm1gny5znl33r") (f (quote (("trace") ("default" "trace"))))))

(define-public crate-bevy_eventlistener_core-0.6.2 (c (n "bevy_eventlistener_core") (v "0.6.2") (d (list (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12") (d #t) (k 0)))) (h "16022jvb28pq8576dasnz3fkzg9qspp30ffy8k759zk2d2gp2pxk") (f (quote (("trace") ("default" "trace"))))))

