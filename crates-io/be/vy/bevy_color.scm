(define-module (crates-io be vy bevy_color) #:use-module (crates-io))

(define-public crate-bevy_color-0.12.1 (c (n "bevy_color") (v "0.12.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0rd21lv6kssnvjgxspn9d77ppv66znn1psrsd9nbapp53vdlxqyi")))

(define-public crate-bevy_color-0.14.0 (c (n "bevy_color") (v "0.14.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "013jy049fwkbpzrj0s186a1fa7zxlskkwi4f8d0vmm38ql40lhn1") (y #t)))

(define-public crate-bevy_color-0.15.0 (c (n "bevy_color") (v "0.15.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0x9mzsxzkxsdhxr6jxm02bchh0qvz2wsa2r5j5sznqr948gh6jsn") (y #t)))

(define-public crate-bevy_color-0.16.0 (c (n "bevy_color") (v "0.16.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1sgwd9d3jr683p8985nbjcxqvagmyhdiaxgiq9fm5k57b8xlfg7z") (y #t)))

(define-public crate-bevy_color-0.12.2-alpha.1 (c (n "bevy_color") (v "0.12.2-alpha.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0v5fv5lnln0gpra9rdhj7ff1hj751n28v2gkxvn1lr2drniqnh47")))

(define-public crate-bevy_color-0.12.2-alpha.2 (c (n "bevy_color") (v "0.12.2-alpha.2") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0zyllfa52bd18qycwvp397fgsbv5sm65j2z03rcxbhz3bwrdj2l6")))

(define-public crate-bevy_color-0.12.3 (c (n "bevy_color") (v "0.12.3") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0h7y6v7gyglngmcaqqfd8byzphryy53z1586f9qs57x7q2zx93j0")))

