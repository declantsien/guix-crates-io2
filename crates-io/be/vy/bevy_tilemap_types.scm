(define-module (crates-io be vy bevy_tilemap_types) #:use-module (crates-io))

(define-public crate-bevy_tilemap_types-0.1.0 (c (n "bevy_tilemap_types") (v "0.1.0") (d (list (d (n "bevy_math") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_render") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14yd1hcvbm1rbiiwpsny2r2picg9by5z67gq1w5n8svl86d25q0f") (f (quote (("serialize" "serde"))))))

(define-public crate-bevy_tilemap_types-0.1.1 (c (n "bevy_tilemap_types") (v "0.1.1") (d (list (d (n "bevy_math") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_render") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p74rjsdv453x6dkrs0xxlwr5hjvz02h6wvq1mblkbgqz5ca4ngj") (f (quote (("serialize" "serde"))))))

(define-public crate-bevy_tilemap_types-0.4.0 (c (n "bevy_tilemap_types") (v "0.4.0") (d (list (d (n "bevy_math") (r "^0.4") (d #t) (k 0)) (d (n "bevy_render") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pb0zwsqbdm5jm53ih5q9ayg3m4l7ibpm1cgf2y6xxb7fm1kqq65") (f (quote (("serialize" "serde"))))))

