(define-module (crates-io be vy bevy_prototype_input_map) #:use-module (crates-io))

(define-public crate-bevy_prototype_input_map-0.1.0 (c (n "bevy_prototype_input_map") (v "0.1.0") (d (list (d (n "bevy") (r "^0.1.3") (d #t) (k 0)) (d (n "bevy_app") (r "^0.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.1") (d #t) (k 0)) (d (n "bevy_input") (r "^0.1") (d #t) (k 0)))) (h "10ssssypgwyc0hzml0nqsgnswckmhavgvlzz1alp3akpn0xysg7m")))

(define-public crate-bevy_prototype_input_map-0.1.1 (c (n "bevy_prototype_input_map") (v "0.1.1") (d (list (d (n "bevy") (r "^0.1.3") (d #t) (k 0)) (d (n "bevy_app") (r "^0.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.1") (d #t) (k 0)) (d (n "bevy_input") (r "^0.1") (d #t) (k 0)))) (h "1qiy1iarnsbga3dq7imp66jxky46v5dzbqydnyg56vz1rimpwlnq")))

(define-public crate-bevy_prototype_input_map-0.1.2 (c (n "bevy_prototype_input_map") (v "0.1.2") (d (list (d (n "bevy") (r "^0.1.3") (d #t) (k 0)) (d (n "bevy_app") (r "^0.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.1") (d #t) (k 0)) (d (n "bevy_input") (r "^0.1") (d #t) (k 0)))) (h "07qkdpj7rhyy4sn7197pcj0kswkv46zp8a1a8x2w3k4kagiqh805")))

(define-public crate-bevy_prototype_input_map-0.1.3 (c (n "bevy_prototype_input_map") (v "0.1.3") (d (list (d (n "bevy") (r "^0.1.3") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "bevy_app") (r "^0.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.1") (d #t) (k 0)) (d (n "bevy_input") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1nrvxva2iywh0j9zpav7wwjc8syqqd0rbkwazi28x4jg79y19dhc")))

(define-public crate-bevy_prototype_input_map-0.1.4 (c (n "bevy_prototype_input_map") (v "0.1.4") (d (list (d (n "bevy") (r "^0.2") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "bevy_app") (r "^0.2") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.2") (d #t) (k 0)) (d (n "bevy_input") (r "^0.2") (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "11yxnrqzazig1i3lfxg0sdq33hsnwkplaidgp3ii3pgik5q6mvm4")))

(define-public crate-bevy_prototype_input_map-0.1.5 (c (n "bevy_prototype_input_map") (v "0.1.5") (d (list (d (n "bevy") (r "^0.3") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04fsplfwnchk4vvpcmyq3ry0153zdzdy3yx76ifprisnyayiv6wh")))

(define-public crate-bevy_prototype_input_map-0.1.6 (c (n "bevy_prototype_input_map") (v "0.1.6") (d (list (d (n "bevy") (r "^0.3") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "19lhkw3sp5qxq5rdsqq2qi21nmwgv2b7hq036m7fj20bgysc1b8v")))

