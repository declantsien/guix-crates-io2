(define-module (crates-io be vy bevy_shader_mtoon) #:use-module (crates-io))

(define-public crate-bevy_shader_mtoon-0.0.1 (c (n "bevy_shader_mtoon") (v "0.0.1") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)))) (h "1c8672ql5fn5birr9mdhqxnc6wixa1fw8bzbqhm4s1znrc0pyiqa")))

(define-public crate-bevy_shader_mtoon-0.0.2 (c (n "bevy_shader_mtoon") (v "0.0.2") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "0pi3m7hrlzj3dn4hy5ncbz5j1an8dhxadq2jyckpnqxvjrz1ck0r")))

(define-public crate-bevy_shader_mtoon-0.0.3 (c (n "bevy_shader_mtoon") (v "0.0.3") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "1q55da5w97bdm36h4d6hrf5ypwc2d0nlf9xjl5l6wms928nf5k07")))

(define-public crate-bevy_shader_mtoon-0.0.6 (c (n "bevy_shader_mtoon") (v "0.0.6") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "18684il3b60cyl169xq7s8w20zxgp34hhnrqzmij4nxy788zixqp")))

(define-public crate-bevy_shader_mtoon-0.0.7 (c (n "bevy_shader_mtoon") (v "0.0.7") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "1nknyqlvx001p8wisi817iyawayz4lzd4g8gz4nscs39j5xr8zjz")))

(define-public crate-bevy_shader_mtoon-0.0.8 (c (n "bevy_shader_mtoon") (v "0.0.8") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)))) (h "19mhm5br71rm72s0kwjgyxcpi4yyvr51afkpfmlrahf5a6wlsj36")))

(define-public crate-bevy_shader_mtoon-0.0.9 (c (n "bevy_shader_mtoon") (v "0.0.9") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("animation" "bevy_asset" "bevy_scene" "bevy_pbr"))) (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.25.0") (d #t) (k 2)) (d (n "bevy_mod_outline") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_panorbit_camera") (r "^0.16.1") (f (quote ("bevy_egui"))) (d #t) (k 2)) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)))) (h "0hqbg6pmmcj63bamhi6cikz38k1yplcq7qws0bzfvxjy0x3q2f67")))

(define-public crate-bevy_shader_mtoon-0.0.10 (c (n "bevy_shader_mtoon") (v "0.0.10") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("animation" "bevy_asset" "bevy_scene" "bevy_pbr"))) (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.25.0") (d #t) (k 2)) (d (n "bevy_mod_outline") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_panorbit_camera") (r "^0.16.1") (f (quote ("bevy_egui"))) (d #t) (k 2)) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)))) (h "1xq0ai3xb2xsyxx9qw3fxs9adp2ll9p1n6gx6dj873cabqdwbgb1")))

