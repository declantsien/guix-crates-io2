(define-module (crates-io be vy bevy-aabb-instancing-011) #:use-module (crates-io))

(define-public crate-bevy-aabb-instancing-011-0.9.1 (c (n "bevy-aabb-instancing-011") (v "0.9.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.8") (d #t) (k 2)))) (h "1zybd8razv9pc8g851jwp3psm8xs6jys2g93zsn2ddks80hwxp82") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-011-0.9.2 (c (n "bevy-aabb-instancing-011") (v "0.9.2") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.8") (d #t) (k 2)))) (h "1na5q4y50ip0sk3ayslmxjs22wlh5aa3hvz993ikqv9lk698fd7s") (f (quote (("trace" "bevy/trace_chrome"))))))

