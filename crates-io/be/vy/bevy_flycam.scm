(define-module (crates-io be vy bevy_flycam) #:use-module (crates-io))

(define-public crate-bevy_flycam-0.1.0 (c (n "bevy_flycam") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)))) (h "1y64z6m0q5wv8n8p8v2l09ayw4i8flair1ka4crcr63lx4nlqz99")))

(define-public crate-bevy_flycam-0.1.1 (c (n "bevy_flycam") (v "0.1.1") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)))) (h "1kb08dd499wm00idgqspqrlw2582s4bd2s2lhhhnvgcm1xbrsimi")))

(define-public crate-bevy_flycam-0.1.2 (c (n "bevy_flycam") (v "0.1.2") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)))) (h "0j84ibyxiqx9rkhzk4bydxi2yvzp364fal397ydyil1lpslrsxi8")))

(define-public crate-bevy_flycam-0.1.3 (c (n "bevy_flycam") (v "0.1.3") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "05v8213gjv81ppawjxiyhbyxjk8jj8dqw0pm99jgg6j70x6c12vf")))

(define-public crate-bevy_flycam-0.2.0 (c (n "bevy_flycam") (v "0.2.0") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "11m2nzpyr8dwlfmw32flfh08l6lgynzib9fkcq2zkk6bgg8zjga9")))

(define-public crate-bevy_flycam-0.2.1 (c (n "bevy_flycam") (v "0.2.1") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "1q2xikfdii9bg3js1m8kxa473mrn5zvwhrmcjcbqyr1vnqsmxfdy")))

(define-public crate-bevy_flycam-0.3.0 (c (n "bevy_flycam") (v "0.3.0") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "1ncxj21b4yvvl1d9wr909qsbyvkm2sqasdnxrh8p6lz034kkryyw")))

(define-public crate-bevy_flycam-0.4.0 (c (n "bevy_flycam") (v "0.4.0") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "0jmsdr31q22qbc9cf7p8wzdm24sx47wna1j444l5gnsxy7971znv")))

(define-public crate-bevy_flycam-0.5.0 (c (n "bevy_flycam") (v "0.5.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render" "bevy_winit" "bevy_wgpu" "x11" "wayland"))) (k 0)))) (h "1kg2ps62g9gxlbfc16vpwdqpywmsa178vxm61qzvcwis01fa4flw")))

(define-public crate-bevy_flycam-0.5.1 (c (n "bevy_flycam") (v "0.5.1") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.5") (f (quote ("render" "bevy_winit" "bevy_wgpu"))) (k 2)) (d (n "bevy") (r "^0.5") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)))) (h "1dr8dmpy5m05g1i0g1q9g60jfhxkz0bx4akj61afcnpich2p11dz")))

(define-public crate-bevy_flycam-0.6.0 (c (n "bevy_flycam") (v "0.6.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (f (quote ("bevy_winit"))) (k 2)) (d (n "bevy") (r "^0.6") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)))) (h "0wjn6xgvzfr85yf8ynbsd4w8r6pssp1krz55mqslkbj8zg2np7h7")))

(define-public crate-bevy_flycam-0.7.0 (c (n "bevy_flycam") (v "0.7.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("x11" "wayland" "bevy_pbr" "bevy_core_pipeline"))) (k 2)))) (h "0jkv5z06vrbaj6mbmyf2yp7irv5nx9nqk1kkki1wngka4q9avbv2")))

(define-public crate-bevy_flycam-0.8.0 (c (n "bevy_flycam") (v "0.8.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("x11" "wayland" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "0fv2vdbj9j4bvi4lq90r642i1wxq9g4mnl7dixxm21wgw3vxmqha") (y #t)))

(define-public crate-bevy_flycam-0.8.1 (c (n "bevy_flycam") (v "0.8.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("x11" "wayland" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "0x3dp0pfa5v35m6m6q3ka4r3m73y81gb9wng366bl6zyfghl7k7j")))

(define-public crate-bevy_flycam-0.9.0 (c (n "bevy_flycam") (v "0.9.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("x11" "wayland" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "0pwvq3p0a9rrrxf07pa9mvqjfsqsd4s11l5mq80hkaxnagygccm0")))

(define-public crate-bevy_flycam-0.10.0 (c (n "bevy_flycam") (v "0.10.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("x11" "wayland" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "12fkl5hzqxdfs0wmr3fng3kjg4v378a9l13bsxs5j3caf79mds9q")))

(define-public crate-bevy_flycam-0.10.1 (c (n "bevy_flycam") (v "0.10.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("x11" "wayland" "bevy_pbr" "bevy_core_pipeline" "bevy_asset"))) (k 2)))) (h "0ni3zlrjpx9l7hdh0ljn5k05cmvnv4lx7lsfj8yz1f189l7ia95n")))

(define-public crate-bevy_flycam-0.11.0 (c (n "bevy_flycam") (v "0.11.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("x11" "wayland" "bevy_pbr" "bevy_core_pipeline" "bevy_asset" "ktx2" "zstd" "tonemapping_luts"))) (k 2)))) (h "1c7gd18spwalxa9cb32d8h7c36l7l6vsv4x0jxa5j3qygk0hc93p")))

(define-public crate-bevy_flycam-0.12.0 (c (n "bevy_flycam") (v "0.12.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("x11" "wayland" "bevy_pbr" "bevy_core_pipeline" "bevy_asset" "ktx2" "zstd" "tonemapping_luts"))) (k 2)))) (h "1d168dvfab7ingfm6dc79fsc37syrd7z14mgq2zvpb7pl1h8pzxi")))

(define-public crate-bevy_flycam-0.13.0 (c (n "bevy_flycam") (v "0.13.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("x11" "wayland" "bevy_pbr" "bevy_core_pipeline" "bevy_asset" "ktx2" "zstd" "tonemapping_luts"))) (k 2)))) (h "1g9m0vhcmhl28wjvcp99i9hzwqbhkcff1g377mbycddki5dql4dp")))

