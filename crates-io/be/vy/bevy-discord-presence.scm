(define-module (crates-io be vy bevy-discord-presence) #:use-module (crates-io))

(define-public crate-bevy-discord-presence-0.1.0 (c (n "bevy-discord-presence") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "discord-presence") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "15gvpa3wzkx6ji4rz1iz5y5h98jbzl5kqg8aqc0rhp2bwxvifvkc")))

(define-public crate-bevy-discord-presence-0.1.1 (c (n "bevy-discord-presence") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "discord-presence") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "09fpmhwkdw3hh6zp64phkij207905k4143vk7apabz1b7qrwx1lq")))

(define-public crate-bevy-discord-presence-0.2.0 (c (n "bevy-discord-presence") (v "0.2.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "discord-presence") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "03pc4sasywzry18qgnqj00pvj00ny7mfd80yf9v5kx2zcfmqz7c7")))

(define-public crate-bevy-discord-presence-0.2.2 (c (n "bevy-discord-presence") (v "0.2.2") (d (list (d (n "bevy") (r "^0.7.0") (k 0)) (d (n "discord-presence") (r "^0.5.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0xij5d2yg756c4bsfwmzs0zwc7xrfb3rp7vlr9zs8ala6v2rhzln")))

(define-public crate-bevy-discord-presence-0.3.1 (c (n "bevy-discord-presence") (v "0.3.1") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "discord-presence") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "10xw7fq9f9zlcj7iykn58swi780g3dwahi4lzc81kkzp9sygjqvm")))

(define-public crate-bevy-discord-presence-0.3.2 (c (n "bevy-discord-presence") (v "0.3.2") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "discord-presence") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0cqs9jyrrl2bq0nwdyy6jrfwldf5fwy00gx6h26pzrgdgh13aywj")))

(define-public crate-bevy-discord-presence-0.3.3 (c (n "bevy-discord-presence") (v "0.3.3") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "discord-presence") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ydl3kmna4c59hf5ihzx7rjkkvfzqfw1m4wyf2k5pab1km9kxpfh")))

(define-public crate-bevy-discord-presence-0.4.1 (c (n "bevy-discord-presence") (v "0.4.1") (d (list (d (n "bevy") (r ">=0.9") (d #t) (k 0)) (d (n "discord-presence") (r "^0.5") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "06jpk3k19slcg1f4qz0wbf1y2f5pm0nfzxq7p21yns77pkkkjnwh")))

(define-public crate-bevy-discord-presence-0.4.2 (c (n "bevy-discord-presence") (v "0.4.2") (d (list (d (n "bevy") (r ">=0.9") (k 0)) (d (n "discord-presence") (r "^0.5.17") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quork") (r "^0.4.0") (f (quote ("traits"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0xcgpimz88sdxvhc7fi4wmwpvm3vvnsr25jsmxi25a5ih36sa1h6")))

(define-public crate-bevy-discord-presence-0.4.3 (c (n "bevy-discord-presence") (v "0.4.3") (d (list (d (n "bevy") (r ">=0.9") (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "discord-presence") (r "^0.5.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quork") (r "^0.4.0") (f (quote ("traits"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0snsb6b3ryam3fb4ganhxd0vnd0m7xignwias6ah5v0gravq80ws")))

