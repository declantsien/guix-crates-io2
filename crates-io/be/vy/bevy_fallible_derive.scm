(define-module (crates-io be vy bevy_fallible_derive) #:use-module (crates-io))

(define-public crate-bevy_fallible_derive-0.4.0 (c (n "bevy_fallible_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full" "fold" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0pxfz6p59c9zwif3x9va00apwcj4s64g0dfpy92iasgadxajkvci")))

