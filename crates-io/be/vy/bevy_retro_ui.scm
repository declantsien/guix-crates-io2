(define-module (crates-io be vy bevy_retro_ui) #:use-module (crates-io))

(define-public crate-bevy_retro_ui-0.1.0 (c (n "bevy_retro_ui") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy_egui") (r "^0.19") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "peg") (r "^0.8") (d #t) (k 0)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "unicode-linebreak") (r "^0.1") (d #t) (k 0)))) (h "1c99s69li1kawcx158w21gzcgrbzlsxhy3wi70iayhpxhpyvz7bv")))

