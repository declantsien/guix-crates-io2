(define-module (crates-io be vy bevy_auto_update) #:use-module (crates-io))

(define-public crate-bevy_auto_update-0.0.1 (c (n "bevy_auto_update") (v "0.0.1") (h "170mmkr1nbyb2yvrc1hbqc95ky25a0siqvavj2y54kmp8jxj5zq5")))

(define-public crate-bevy_auto_update-0.0.2 (c (n "bevy_auto_update") (v "0.0.2") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "restson") (r "^1.3.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "063dlby9i6a8djl21ijf4zdk025wcyyspyf6awkmqi1rskbsg72c")))

