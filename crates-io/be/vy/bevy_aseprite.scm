(define-module (crates-io be vy bevy_aseprite) #:use-module (crates-io))

(define-public crate-bevy_aseprite-0.6.0 (c (n "bevy_aseprite") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "aseprite-reader2") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.1") (d #t) (k 0)))) (h "1ddpilwx12jfvprgvsphrhzrv4zm6nz1vbjx246rj5i15l9fky15")))

(define-public crate-bevy_aseprite-0.1.0 (c (n "bevy_aseprite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "09v3iyf3ndr9fl79jr7piqr22m1b6x25cyk0jx81pyqmp5rsnqnv")))

(define-public crate-bevy_aseprite-0.1.1 (c (n "bevy_aseprite") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "0w01mby5ibj61v505ikphpr2fvrnh5lc3n0a8bqi92i39rd231zn")))

(define-public crate-bevy_aseprite-0.1.2 (c (n "bevy_aseprite") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "0bsnk9zw5gar5qa0n3v3qqjnhzgwwbvyyd3nsy2cpy85l12jcvrr")))

(define-public crate-bevy_aseprite-0.7.0 (c (n "bevy_aseprite") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "0jwczrza3bgzwlhhysvi8iq7g2bahsccqp0c01r0gkvmw149pv3i")))

(define-public crate-bevy_aseprite-0.9.0 (c (n "bevy_aseprite") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "0brxxrzabnv078442yc7r3nkvy9ixw8mk6bnxzf1p4xxpwd3nbm9")))

(define-public crate-bevy_aseprite-0.9.1 (c (n "bevy_aseprite") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "08wqybsq4nnn43pgmkrbfrzj0gnib12006nl78ddzxy2zs2zpd4m")))

(define-public crate-bevy_aseprite-0.10.0 (c (n "bevy_aseprite") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10.0") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "0rs2lwfa2g7463imdmkrx4vq76wl3jyzn9d9qqz751gnsdhw9lll")))

(define-public crate-bevy_aseprite-0.11.0 (c (n "bevy_aseprite") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.11.0") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.3") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "1m1l557agmyqqbfn5nycy5b89pl7f4rj3hm22vph5rbnvzgq04h0")))

(define-public crate-bevy_aseprite-0.12.0 (c (n "bevy_aseprite") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.3") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "0qigfyf6rxdby405hkzjh33ffdfz111mxxsh43apn9vg6v2p04aj")))

