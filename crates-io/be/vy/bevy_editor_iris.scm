(define-module (crates-io be vy bevy_editor_iris) #:use-module (crates-io))

(define-public crate-bevy_editor_iris-0.1.1 (c (n "bevy_editor_iris") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_editor_iris_common") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_editor_iris_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_editor_iris_editor") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_editor_iris_plugin") (r "^0.1.0") (d #t) (k 0)))) (h "0w2nfshz0iy99knz4l94qg159w9d1mwlz7yv5zg3gj2g4d277zhx")))

