(define-module (crates-io be vy bevy-orbit-controls) #:use-module (crates-io))

(define-public crate-bevy-orbit-controls-1.0.0 (c (n "bevy-orbit-controls") (v "1.0.0") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 0)))) (h "18z5wpc2b856r1qla7gywyaw260z8pchkjbpsdiqxlkd1g4q81ly")))

(define-public crate-bevy-orbit-controls-1.0.1 (c (n "bevy-orbit-controls") (v "1.0.1") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 0)))) (h "0asy3zh609qiqily5bnkpn0hbhk77fg34ybfy4dh7i73zcr3d6jj")))

(define-public crate-bevy-orbit-controls-1.0.2 (c (n "bevy-orbit-controls") (v "1.0.2") (d (list (d (n "bevy") (r "^0.4.0") (d #t) (k 0)))) (h "1phz1765jmx599plqqbf7gr66xlwmry51006fkdv9dy0svk3jd21")))

(define-public crate-bevy-orbit-controls-2.0.0 (c (n "bevy-orbit-controls") (v "2.0.0") (d (list (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 0)))) (h "1a6zp4q9wxjr9zgaw7y6rgh3xa3lfcrmjy77k9m1a9vwbh57g5gc")))

