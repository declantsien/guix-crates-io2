(define-module (crates-io be vy bevy-inspector-egui-derive) #:use-module (crates-io))

(define-public crate-bevy-inspector-egui-derive-0.1.0 (c (n "bevy-inspector-egui-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "158wwhnnc7wzrw17pza1mjra8kxivdy3pk4x4krkdwkbpfavmrv9")))

(define-public crate-bevy-inspector-egui-derive-0.2.0 (c (n "bevy-inspector-egui-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jpyaxpva56wq5zp00rwr35m9byx67j4wf3xc3cndpf7cmnqcqq4")))

(define-public crate-bevy-inspector-egui-derive-0.3.0 (c (n "bevy-inspector-egui-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gapnzb31zfcjs23sm8h1j49k2732fngw4w7sws0p01blq3khdyp")))

(define-public crate-bevy-inspector-egui-derive-0.4.0 (c (n "bevy-inspector-egui-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xvaljwp6xi9dbyahh4b4869ji1x65dj1fasfakvzhh7j5il88sb")))

(define-public crate-bevy-inspector-egui-derive-0.5.0 (c (n "bevy-inspector-egui-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01m6vq7cvfmdfyqkcd4kihrkmijncggba8gb0hj6drl77ifn42ni")))

(define-public crate-bevy-inspector-egui-derive-0.6.0 (c (n "bevy-inspector-egui-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16l92bxp2bxslzhrdq8dschvvn0cldb3a61yp5nwh5mr6ic0zjz1")))

(define-public crate-bevy-inspector-egui-derive-0.6.1 (c (n "bevy-inspector-egui-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06b96yinx1n5vgrpf9wn200gq67afcg6chf9zs6ah7rsc0zrsz2r")))

(define-public crate-bevy-inspector-egui-derive-0.7.1 (c (n "bevy-inspector-egui-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11vhzwnvawn56kqj6gf6kk3a0frx1mcirgk7563plrpincsa4jpf")))

(define-public crate-bevy-inspector-egui-derive-0.10.0 (c (n "bevy-inspector-egui-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bzribp07vxx0sf7gs1d4mxi2r71bbhmzy6jk359kx8m090701y0")))

(define-public crate-bevy-inspector-egui-derive-0.11.0 (c (n "bevy-inspector-egui-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16v2px33a05vwsli2c7pbh2240hzlz80lny0fn1v9g1kcwz2sr9l")))

(define-public crate-bevy-inspector-egui-derive-0.12.0 (c (n "bevy-inspector-egui-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l6h2dcm1whn99g7jwympdk5238ighaxmfdwh43x8939lih65vs8")))

(define-public crate-bevy-inspector-egui-derive-0.13.0 (c (n "bevy-inspector-egui-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11zl3v1xq9xwam7drr2ai92zz2kmz8h81vpjwwm1rhxjzmcsdq1c")))

(define-public crate-bevy-inspector-egui-derive-0.14.0 (c (n "bevy-inspector-egui-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fq8gw7cl085hxsix1cgv98g5r2kpnn7a7jag6l19a4lxlb9adnq")))

(define-public crate-bevy-inspector-egui-derive-0.15.0-pre.0 (c (n "bevy-inspector-egui-derive") (v "0.15.0-pre.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gfax5g6cir029jw0yjhb2qw65x5cdllimjb1y206n3qm41p0km3")))

(define-public crate-bevy-inspector-egui-derive-0.15.0 (c (n "bevy-inspector-egui-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y41pj5iw8h7z8fjx07b5jzmpfjvjxg8fnja63pj2lqiabmhqsic")))

(define-public crate-bevy-inspector-egui-derive-0.16.0-pre.0 (c (n "bevy-inspector-egui-derive") (v "0.16.0-pre.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cqq9n3ancdzdzjy317mvj1p56pzr1akss89m1rmyb1s9d27qd2h")))

(define-public crate-bevy-inspector-egui-derive-0.16.0-pre.1 (c (n "bevy-inspector-egui-derive") (v "0.16.0-pre.1") (d (list (d (n "bevy_math") (r "^0.9") (k 2)) (d (n "bevy_reflect") (r "^0.9") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xqyrdx0gj29q4hqqj5sf3m0fg1czi21yqylsbmg4ybnj5z6gv5f")))

(define-public crate-bevy-inspector-egui-derive-0.16.0-pre.2 (c (n "bevy-inspector-egui-derive") (v "0.16.0-pre.2") (d (list (d (n "bevy_math") (r "^0.9") (k 2)) (d (n "bevy_reflect") (r "^0.9") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gwn1irga9a3qkh8ywdj356dl1lcb1fmijpr1bg2b5pg9c5x06g4")))

(define-public crate-bevy-inspector-egui-derive-0.16.0 (c (n "bevy-inspector-egui-derive") (v "0.16.0") (d (list (d (n "bevy_math") (r "^0.9") (k 2)) (d (n "bevy_reflect") (r "^0.9") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dkvyz7pc792k5y9pxs5rnj36vg545d352bk8k83s4gw0z8qpwmd")))

(define-public crate-bevy-inspector-egui-derive-0.16.1 (c (n "bevy-inspector-egui-derive") (v "0.16.1") (d (list (d (n "bevy_math") (r "^0.9") (k 2)) (d (n "bevy_reflect") (r "^0.9") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yz9rpf4wh52rkxsqc5wjgnlbbylqh289k5ni8af6xz3q9ndn9w9")))

(define-public crate-bevy-inspector-egui-derive-0.17.0 (c (n "bevy-inspector-egui-derive") (v "0.17.0") (d (list (d (n "bevy_ecs") (r "^0.9") (k 2)) (d (n "bevy_math") (r "^0.9") (k 2)) (d (n "bevy_reflect") (r "^0.9") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04w17vpviiid4lqqaav890fm7xvih50rjyryd1vnxdd54mi6x0lp")))

(define-public crate-bevy-inspector-egui-derive-0.18.0 (c (n "bevy-inspector-egui-derive") (v "0.18.0") (d (list (d (n "bevy_ecs") (r "^0.10") (k 2)) (d (n "bevy_math") (r "^0.10") (k 2)) (d (n "bevy_reflect") (r "^0.10") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12zwrcwd82c9wz1z9b5dy214xfxkr4n5iha1fzwjl62jrg6xyizw")))

(define-public crate-bevy-inspector-egui-derive-0.18.1 (c (n "bevy-inspector-egui-derive") (v "0.18.1") (d (list (d (n "bevy_ecs") (r "^0.10") (k 2)) (d (n "bevy_math") (r "^0.10") (k 2)) (d (n "bevy_reflect") (r "^0.10") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0dfap8g445zydsjjgn704mmnrgvkfr8q5niy46mcggmcv89wjmkm")))

(define-public crate-bevy-inspector-egui-derive-0.19.0 (c (n "bevy-inspector-egui-derive") (v "0.19.0") (d (list (d (n "bevy_ecs") (r "^0.11") (k 2)) (d (n "bevy_math") (r "^0.11") (k 2)) (d (n "bevy_reflect") (r "^0.11") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0j3mjid5689c7nng0q6yxla835nx8rfswa31nzlbhq8qc1as9nqf")))

(define-public crate-bevy-inspector-egui-derive-0.20.0 (c (n "bevy-inspector-egui-derive") (v "0.20.0") (d (list (d (n "bevy_ecs") (r "^0.11") (k 2)) (d (n "bevy_math") (r "^0.11") (k 2)) (d (n "bevy_reflect") (r "^0.11") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0v1bn8a4vipdxksx3n7a1j4sjnshfxq1443x9sd8f47s27bshq3s")))

(define-public crate-bevy-inspector-egui-derive-0.21.0 (c (n "bevy-inspector-egui-derive") (v "0.21.0") (d (list (d (n "bevy_ecs") (r "^0.12") (k 2)) (d (n "bevy_math") (r "^0.12") (k 2)) (d (n "bevy_reflect") (r "^0.12") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1fkmgvmx9slmnf47hibfxz7cyjybzxnypw40zzdvalc1z5y0p07c")))

(define-public crate-bevy-inspector-egui-derive-0.22.0 (c (n "bevy-inspector-egui-derive") (v "0.22.0") (d (list (d (n "bevy_ecs") (r "^0.12") (k 2)) (d (n "bevy_math") (r "^0.12") (k 2)) (d (n "bevy_reflect") (r "^0.12") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1c91bknyc48zwvdd9ci7aqj14kbhpjg15hq250dysn39ymwp9isy")))

(define-public crate-bevy-inspector-egui-derive-0.23.0 (c (n "bevy-inspector-egui-derive") (v "0.23.0") (d (list (d (n "bevy_ecs") (r "^0.13") (k 2)) (d (n "bevy_math") (r "^0.13") (k 2)) (d (n "bevy_reflect") (r "^0.13") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0bgfgabyi4174ikg0igkzky46ncl6d2nvqbk0bhj788438b8ii5k")))

(define-public crate-bevy-inspector-egui-derive-0.24.0 (c (n "bevy-inspector-egui-derive") (v "0.24.0") (d (list (d (n "bevy_ecs") (r "^0.13") (k 2)) (d (n "bevy_math") (r "^0.13") (k 2)) (d (n "bevy_reflect") (r "^0.13") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "051b2jaipvrmif0wir1hyjfw3shz6fn2asaslnsj3mn211cr0pcp")))

