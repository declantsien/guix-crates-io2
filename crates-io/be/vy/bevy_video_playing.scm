(define-module (crates-io be vy bevy_video_playing) #:use-module (crates-io))

(define-public crate-bevy_video_playing-0.10.0 (c (n "bevy_video_playing") (v "0.10.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "openh264") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0zcvkm0dr719xks600vshs5brsslxzn9r22aihx8rsvxrgw02g01")))

