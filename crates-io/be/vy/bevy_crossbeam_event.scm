(define-module (crates-io be vy bevy_crossbeam_event) #:use-module (crates-io))

(define-public crate-bevy_crossbeam_event-0.1.0 (c (n "bevy_crossbeam_event") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)))) (h "0xvn5kvvakblljm89blfq88a6hkgfz7cpm6rm3w0xhmrvqq8v5dp")))

(define-public crate-bevy_crossbeam_event-0.2.0 (c (n "bevy_crossbeam_event") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)))) (h "1lj3jn6qlw5ncv4xc8260ycqdcgq8dkb2k01fyd5c5wbgbvpbvsi")))

(define-public crate-bevy_crossbeam_event-0.3.0 (c (n "bevy_crossbeam_event") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)))) (h "0ygjfqlrpiv635wmw47frkngpnzz7xpmnjvxxyvwi5nj6v7rnsch")))

(define-public crate-bevy_crossbeam_event-0.5.0 (c (n "bevy_crossbeam_event") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)))) (h "0g1vwdf6ga9nn2hcrkch6wmikl5889s6zimn8hgf4jx7irmi30zj")))

