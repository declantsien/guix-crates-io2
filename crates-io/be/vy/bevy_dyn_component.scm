(define-module (crates-io be vy bevy_dyn_component) #:use-module (crates-io))

(define-public crate-bevy_dyn_component-0.1.0 (c (n "bevy_dyn_component") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.2") (d #t) (k 2)) (d (n "bevy_app") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ptr") (r "^0.13") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.13") (d #t) (k 0)))) (h "0lx0qhr4smziv803d0k0x1z50kxskybw5vpckg5vswgll11fvhrf")))

(define-public crate-bevy_dyn_component-0.2.0 (c (n "bevy_dyn_component") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13.2") (d #t) (k 2)) (d (n "bevy_app") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ptr") (r "^0.13") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.13") (d #t) (k 0)))) (h "02zmy31lfyjn1xy3048bfvw3nilwz7309a8m5sizq754483cidbx")))

