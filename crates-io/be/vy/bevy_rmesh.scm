(define-module (crates-io be vy bevy_rmesh) #:use-module (crates-io))

(define-public crate-bevy_rmesh-0.1.0 (c (n "bevy_rmesh") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.10.1") (d #t) (k 2)) (d (n "rmesh") (r "^0.3.1") (d #t) (k 0)))) (h "1hv14z2y8mmk16am24z12vk6f54jvm84ljvqxii5y6k468n640qd")))

(define-public crate-bevy_rmesh-0.1.1 (c (n "bevy_rmesh") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.10.1") (d #t) (k 2)) (d (n "rmesh") (r "^0.3.1") (d #t) (k 0)))) (h "199i1sp5awd4hc6ycizdxjyh6hfh8931vqc5ikbcn48i622nsrr0") (y #t)))

(define-public crate-bevy_rmesh-0.1.2 (c (n "bevy_rmesh") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.10.1") (d #t) (k 2)) (d (n "rmesh") (r "^0.3.2") (d #t) (k 0)))) (h "1zcqfdhmicv9gf4hwx802mp31rbqssp0jfkcfjy95agawnhfrf2y")))

(define-public crate-bevy_rmesh-0.1.3 (c (n "bevy_rmesh") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.10.1") (d #t) (k 2)) (d (n "rmesh") (r "^0.3.2") (d #t) (k 0)))) (h "112la30b4y2nqm382zw1n0i8fwxplh0lmqrf9yjr1g99jg7c731w")))

(define-public crate-bevy_rmesh-0.2.0 (c (n "bevy_rmesh") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.11.0") (d #t) (k 2)) (d (n "rmesh") (r "^0.3.2") (d #t) (k 0)))) (h "19gvr4bc1mi28fb8b4yidwm9b0vj4g33df22av4z74z84rp6ksr5")))

(define-public crate-bevy_rmesh-0.3.0 (c (n "bevy_rmesh") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 2)) (d (n "rmesh") (r "^0.3.3") (d #t) (k 0)))) (h "1cjyvaiq7bwwffyfb8qfs6fvjzav3cpfx82mh2rd5z45r63cnkha")))

(define-public crate-bevy_rmesh-0.3.1 (c (n "bevy_rmesh") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "rmesh") (r "^0.3.4") (d #t) (k 0)))) (h "1sq8mv17z6y1llxqlpdq1jnpsyz8m1pl9n7camz95wcymvdjypwl")))

(define-public crate-bevy_rmesh-0.3.2 (c (n "bevy_rmesh") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "rmesh") (r "^0.3.4") (d #t) (k 0)))) (h "07564b35g2zysmr596vxfvzfwa1r31hxklwmp5vgdnj3mnxdq5n0")))

