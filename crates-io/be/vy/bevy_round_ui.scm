(define-module (crates-io be vy bevy_round_ui) #:use-module (crates-io))

(define-public crate-bevy_round_ui-0.1.0 (c (n "bevy_round_ui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "0xmwawii8v8sg6n7pf1lmrxrzrandsarsb8sc65q1zrxlypyq4xb") (f (quote (("default" "autosize") ("autosize"))))))

(define-public crate-bevy_round_ui-0.1.1 (c (n "bevy_round_ui") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "0jdizgr379vazjf1dgw33qi0wxvywx5y6z4ldzjgk8yah13v1fwa") (f (quote (("default" "autosize") ("autosize"))))))

(define-public crate-bevy_round_ui-0.2.0 (c (n "bevy_round_ui") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "0pcfc51x6gcqsadz44rddg0m5bb7d7f35q3vd49s7fa0wvzirk25") (f (quote (("default" "autosize") ("autosize"))))))

(define-public crate-bevy_round_ui-1.0.0 (c (n "bevy_round_ui") (v "1.0.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "1mpnhz616prs1dbyrgma48jiys056vydkl4cfq4ydfjq0kn340pp")))

