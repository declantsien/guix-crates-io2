(define-module (crates-io be vy bevy_mod_index) #:use-module (crates-io))

(define-public crate-bevy_mod_index-0.1.0 (c (n "bevy_mod_index") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0sjaygsay7wfa9n2plqpsvlgqacprbbi99rkwc92fa738712p8wh")))

(define-public crate-bevy_mod_index-0.2.0 (c (n "bevy_mod_index") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1719szi5mfxcgrb3yflaq6rl826m0mvhz3gf01xvzlxb56r59fv1")))

(define-public crate-bevy_mod_index-0.3.0 (c (n "bevy_mod_index") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "16xysswan7kf3jv11a8ak56b4b7jd67sf6alw1mfcriq98gybpbb")))

(define-public crate-bevy_mod_index-0.4.0 (c (n "bevy_mod_index") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ldqnygqyar074i8sra3cyj1z4wn771mddwp2z7lhp2h5h1dzkaf")))

(define-public crate-bevy_mod_index-0.4.1 (c (n "bevy_mod_index") (v "0.4.1") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0qs4dql08wfl6pzpnz9n4lc91901agjhjjp3f4xkkmnq6qnj0xh5") (f (quote (("reflect"))))))

