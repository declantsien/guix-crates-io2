(define-module (crates-io be vy bevy_xpbd_3d_interp) #:use-module (crates-io))

(define-public crate-bevy_xpbd_3d_interp-0.1.0 (c (n "bevy_xpbd_3d_interp") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.3.1") (d #t) (k 0)))) (h "0s05ryd45syy1gxaiifj2ifkg3jk1smik3b84yzybz0g1pgvbybk") (f (quote (("default" "3d") ("3d"))))))

(define-public crate-bevy_xpbd_3d_interp-0.1.1 (c (n "bevy_xpbd_3d_interp") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.3.2") (d #t) (k 0)))) (h "18jginpmfr0bidc5h2hcvxxa5fh85jk5c0lrdh14j75s5kfq0fhq") (f (quote (("default" "3d") ("3d"))))))

(define-public crate-bevy_xpbd_3d_interp-0.1.2 (c (n "bevy_xpbd_3d_interp") (v "0.1.2") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.4") (d #t) (k 0)))) (h "1c2k625dmaypwyy0br4kw0vp8gp3clf7k0hiz4klk0i566a2jz7n") (f (quote (("default" "3d") ("3d"))))))

