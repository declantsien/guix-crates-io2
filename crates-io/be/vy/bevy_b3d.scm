(define-module (crates-io be vy bevy_b3d) #:use-module (crates-io))

(define-public crate-bevy_b3d-0.1.0 (c (n "bevy_b3d") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "b3d") (r "^0.1.6") (d #t) (k 0)) (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0f9prb44f9bx2w30whqqhllc7xn50i3anm92h20s3zhn6hvwnhrj")))

(define-public crate-bevy_b3d-0.1.1 (c (n "bevy_b3d") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "b3d") (r "^0.1.6") (d #t) (k 0)) (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1rffmywx5ymzrjfsy3vf8g4935r1iixck5lz865v30zwy4mgx7k1")))

(define-public crate-bevy_b3d-0.2.0 (c (n "bevy_b3d") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "b3d") (r "^0.1.6") (d #t) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0wvhmzvlm184gskl4xi0n38v9ycny12bsgjaqi212s2c554snlw5")))

(define-public crate-bevy_b3d-0.3.0 (c (n "bevy_b3d") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "b3d") (r "^0.1.7") (d #t) (k 0)) (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "09clj2rl0i5n12d3rck4jfwpyrdpcxrnj2gq623grxifhz8wxhg2")))

(define-public crate-bevy_b3d-0.3.1 (c (n "bevy_b3d") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "b3d") (r "^0.1.7") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0g99xhqg1rc4d0w9fy9a24jik798bhmy8zqdsycdpgbmks2c6v6v")))

(define-public crate-bevy_b3d-0.3.2 (c (n "bevy_b3d") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "b3d") (r "^0.1.8") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_pbr" "bevy_render" "bevy_scene"))) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0caw9277wdc9vz7nyi8fcdhmhn18axnqcnamr5x2ggl9nvxc0lm9")))

