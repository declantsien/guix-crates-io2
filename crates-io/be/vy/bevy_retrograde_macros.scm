(define-module (crates-io be vy bevy_retrograde_macros) #:use-module (crates-io))

(define-public crate-bevy_retrograde_macros-0.0.0 (c (n "bevy_retrograde_macros") (v "0.0.0") (h "0z3ikb5l4m3722qzrh72sys8lzxvb6qllbvw3a6whz9v6c3sc5cg")))

(define-public crate-bevy_retrograde_macros-0.1.0 (c (n "bevy_retrograde_macros") (v "0.1.0") (h "0c34m5az28nj45fixgb3yj16r2fhc415rv7055y2pxsv48fdpqfm")))

(define-public crate-bevy_retrograde_macros-0.2.0 (c (n "bevy_retrograde_macros") (v "0.2.0") (h "08l7d9r7y2c3gsrfsc6k434kqgx3qrjf0g9ar88pyj16yfb5ygya")))

