(define-module (crates-io be vy bevy_audio_stream) #:use-module (crates-io))

(define-public crate-bevy_audio_stream-0.1.0 (c (n "bevy_audio_stream") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_audio") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.13.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)))) (h "0kbm7rhrpf6skqcs2n883c37zjvgy857djx1q49fy56ghrb1yavh") (r "1.76.0")))

