(define-module (crates-io be vy bevy_titan) #:use-module (crates-io))

(define-public crate-bevy_titan-0.1.0 (c (n "bevy_titan") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mjp5h2jyc65hsdw2cbckbl3qwc7lyhcj0jpc94k3f8r2lzhl96h")))

(define-public crate-bevy_titan-0.1.1 (c (n "bevy_titan") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sg5c8myb9magbjnwl1cyd5vk6ygzm0wnpza58zsw9gs1c67ssjy")))

(define-public crate-bevy_titan-0.2.0 (c (n "bevy_titan") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rnd14zxl0gil540lmcyb0ql4irmnsi3grgsf6iki05gn79d3x9f")))

(define-public crate-bevy_titan-0.3.0 (c (n "bevy_titan") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "096ci6hhnrgy503d85ckanyld22d8s7dzxb3vpmxj1pkvsdh169r")))

(define-public crate-bevy_titan-0.4.0 (c (n "bevy_titan") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ygm8b6f5b0hrcp588wgc9m7bkndi0mqhkjrs779saqbn98qwz6i")))

(define-public crate-bevy_titan-0.5.0 (c (n "bevy_titan") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("file_watcher"))) (d #t) (k 2)) (d (n "bevy_asset_loader") (r "^0.18.0") (d #t) (k 2)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "090fm5qyn9g8i3h3wnrcgvkhgq1cd4zns0c8ka3zn98ahn47g615")))

(define-public crate-bevy_titan-0.6.0 (c (n "bevy_titan") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("file_watcher"))) (d #t) (k 2)) (d (n "bevy_asset_loader") (r "^0.20") (d #t) (k 2)) (d (n "glam") (r "^0.24.1") (f (quote ("serde"))) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wgpu-types") (r "^0.19") (f (quote ("replay"))) (k 0)))) (h "0979pdqs6dwkf3710yyf45sc9krxkhyhdfsnc5b7j4q4kh0jrx5q")))

