(define-module (crates-io be vy bevy_defer_picking) #:use-module (crates-io))

(define-public crate-bevy_defer_picking-0.0.1 (c (n "bevy_defer_picking") (v "0.0.1") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bevy_defer") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.2") (d #t) (k 0)) (d (n "bevy_math") (r "^0.13.2") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.18.2") (f (quote ("selection"))) (k 0)) (d (n "bevy_mod_picking") (r "^0.18.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1d06x4jpqj3pshxj5zpih4x2daalx1j8sky3rs1qdc3cavx3ljli")))

