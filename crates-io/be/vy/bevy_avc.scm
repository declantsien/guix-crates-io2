(define-module (crates-io be vy bevy_avc) #:use-module (crates-io))

(define-public crate-bevy_avc-0.9.0 (c (n "bevy_avc") (v "0.9.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "openh264") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0j4hax1cdrh5rhvzcfdx91ysd75375mzvqmlr660ms6sb8jdh8ma") (y #t)))

