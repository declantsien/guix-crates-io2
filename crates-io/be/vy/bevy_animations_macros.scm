(define-module (crates-io be vy bevy_animations_macros) #:use-module (crates-io))

(define-public crate-bevy_animations_macros-0.1.0 (c (n "bevy_animations_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "11l5rj244bwngn0kavanwpvx3yr2qlvcp2za9ci2a577waci7bk9")))

(define-public crate-bevy_animations_macros-0.2.0 (c (n "bevy_animations_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0cy88kf3acb6p2157a7lb222n8gk0fknjwbsksa96s30nxfjml60")))

(define-public crate-bevy_animations_macros-0.3.0 (c (n "bevy_animations_macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0k6gi28dbk8ah6z2wqv1cyjqfjq26npsy3wdwigp4prkxk6xsj6i")))

(define-public crate-bevy_animations_macros-0.4.0 (c (n "bevy_animations_macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0zjjv0d6jlng225ljnbmhjx62n1547g2yz4my3f461dxqj1acv8c")))

