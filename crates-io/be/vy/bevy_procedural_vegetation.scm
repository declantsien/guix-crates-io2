(define-module (crates-io be vy bevy_procedural_vegetation) #:use-module (crates-io))

(define-public crate-bevy_procedural_vegetation-0.1.0 (c (n "bevy_procedural_vegetation") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.1") (f (quote ("bevy_pbr" "bevy_sprite" "tonemapping_luts"))) (k 0)) (d (n "bevy-inspector-egui") (r "^0.23.4") (d #t) (k 0)) (d (n "bevy_procedural_meshes") (r "^0.13.1") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "render-to-texture") (r "^0.13.0") (d #t) (k 0)))) (h "0nll34b0c6j6v1fxcd83n430ybbkw4fh3y4zw9whd5gkahim5fgg") (f (quote (("dynamic" "bevy/dynamic_linking" "bevy/file_watcher") ("default")))) (r "1.76.0")))

