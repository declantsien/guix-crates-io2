(define-module (crates-io be vy bevy_assets_tar_zstd_bundler) #:use-module (crates-io))

(define-public crate-bevy_assets_tar_zstd_bundler-0.1.0 (c (n "bevy_assets_tar_zstd_bundler") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "zstd") (r "^0") (d #t) (k 0)))) (h "0v7xanjxyhw81rfhaah5ysdwkxdav54xvcni2r2pckajzkrf830p")))

