(define-module (crates-io be vy bevy-firebase-auth) #:use-module (crates-io))

(define-public crate-bevy-firebase-auth-0.1.0 (c (n "bevy-firebase-auth") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.1") (d #t) (k 0)) (d (n "bevy-tokio-tasks") (r "^0.11.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "090dw680zs7x4ga1g3v1kx0bj41pvvd84cmlavcnaa8a0dr9003s")))

