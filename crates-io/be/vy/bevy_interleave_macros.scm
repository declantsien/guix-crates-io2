(define-module (crates-io be vy bevy_interleave_macros) #:use-module (crates-io))

(define-public crate-bevy_interleave_macros-0.1.0 (c (n "bevy_interleave_macros") (v "0.1.0") (d (list (d (n "bevy_interleave_interface") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1iv15xbiq1nc9w5hhyhzfirw08wfffv7j52rplpmglbzsnq6nxn7")))

(define-public crate-bevy_interleave_macros-0.1.1 (c (n "bevy_interleave_macros") (v "0.1.1") (d (list (d (n "bevy_interleave_interface") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "10q6fdymik3x30xjm9a7nxcbc16csdm1xxs0ix8hpi8b2f532grl")))

(define-public crate-bevy_interleave_macros-0.2.0 (c (n "bevy_interleave_macros") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy_interleave_interface") (r "^0.1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mgvhv9qia9i98a0bx6cqqpz81bb6d1fvpgznsjj4ic6v2hkq0ng")))

(define-public crate-bevy_interleave_macros-0.2.1 (c (n "bevy_interleave_macros") (v "0.2.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy_interleave_interface") (r "^0.2.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "071s87vv6b8v34f9czk91ip5qi546y6bgcsvmyhzyvagay6b1acy")))

