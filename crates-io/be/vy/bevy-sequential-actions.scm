(define-module (crates-io be vy bevy-sequential-actions) #:use-module (crates-io))

(define-public crate-bevy-sequential-actions-0.1.0 (c (n "bevy-sequential-actions") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.7") (d #t) (k 0)))) (h "0dchpzqafaa41hf5szbhf1m87a76ykvczm8hivc4rinx5czqj0lx")))

(define-public crate-bevy-sequential-actions-0.2.0 (c (n "bevy-sequential-actions") (v "0.2.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.7") (d #t) (k 0)))) (h "1dqk6k9m8kadm6v4q9dknlxp4czisaaiz6hr3bp2azhwxxn50vwf")))

(define-public crate-bevy-sequential-actions-0.3.0 (c (n "bevy-sequential-actions") (v "0.3.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)))) (h "02mqfy1kfw2bm16wsi7aiws85nbh38m0nxprip9h5h2659l1dj1j")))

(define-public crate-bevy-sequential-actions-0.4.0 (c (n "bevy-sequential-actions") (v "0.4.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)))) (h "0dk3ixh21ssbia12vcscssmi9m1zl02dlrlzpzsk97v7ylf4222p")))

(define-public crate-bevy-sequential-actions-0.5.0 (c (n "bevy-sequential-actions") (v "0.5.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)))) (h "1nys61f11zqmq0jxh5sjdssa90ryaq4yb5b2xixfkxf36s9wj156")))

(define-public crate-bevy-sequential-actions-0.6.0 (c (n "bevy-sequential-actions") (v "0.6.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_app") (r "^0.9") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9") (d #t) (k 0)))) (h "183gwvgfbvjmnhxi655075pdy6c0jdmljazi7mryl5nhnik96p5a")))

(define-public crate-bevy-sequential-actions-0.7.0 (c (n "bevy-sequential-actions") (v "0.7.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_app") (r "^0.10") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10") (d #t) (k 0)))) (h "0nrl74s23ap4daj5jvx1v8bxp7h8r12fz06p3rcjzfj8i0w8jfg6")))

(define-public crate-bevy-sequential-actions-0.8.0 (c (n "bevy-sequential-actions") (v "0.8.0") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_derive") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "bevy_utils") (r "^0.11") (k 2)) (d (n "criterion") (r "^0.4") (k 2)) (d (n "downcast-rs") (r "^1.2") (k 0)))) (h "05bldkb48rqq6fmf6sm1dl0djhry6zyxkzlq18j6h9s40likfdnp")))

(define-public crate-bevy-sequential-actions-0.9.0 (c (n "bevy-sequential-actions") (v "0.9.0") (d (list (d (n "bevy_app") (r "^0.12") (k 0)) (d (n "bevy_derive") (r "^0.12") (k 0)) (d (n "bevy_ecs") (r "^0.12") (k 0)) (d (n "bevy_utils") (r "^0.12") (k 2)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "downcast-rs") (r "^1.2") (k 0)))) (h "1f64855vlivgxaxb4jpajcbf83p5bv60lj2nipvsqqjkdsbfnfww")))

(define-public crate-bevy-sequential-actions-0.10.0 (c (n "bevy-sequential-actions") (v "0.10.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_derive") (r "^0.13") (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_log") (r "^0.13") (k 0)) (d (n "bevy_utils") (r "^0.13") (k 2)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "downcast-rs") (r "^1.2") (k 0)))) (h "1xl8zbdw7fbdz69w3wdizp9r2qkyj5s98d59s6v8wpl37wzrb5b6")))

