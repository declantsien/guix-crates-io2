(define-module (crates-io be vy bevy-kira-components) #:use-module (crates-io))

(define-public crate-bevy-kira-components-0.1.0-rc.0 (c (n "bevy-kira-components") (v "0.1.0-rc.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "kira") (r "^0.8.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0arc2ydj5pxlzf6kvdkqrqb4xdqqpb0d92z8rag6ib7w12a1pwf3") (f (quote (("diagnostics") ("default"))))))

(define-public crate-bevy-kira-components-0.1.0 (c (n "bevy-kira-components") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "kira") (r "^0.8.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0li7larpz4np8038m7z0z1qpqqkqjxjc80sl0zhx00lmwhvdcz17") (f (quote (("diagnostics") ("default"))))))

(define-public crate-bevy-kira-components-0.1.1 (c (n "bevy-kira-components") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "kira") (r "^0.8.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0ankvdp8rw0wxgs0g8v51rxr6k2s6pvfyflsygmvszm99k00azbp") (f (quote (("diagnostics") ("default"))))))

