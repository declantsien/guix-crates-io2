(define-module (crates-io be vy bevy_generative) #:use-module (crates-io))

(define-public crate-bevy_generative-0.1.0 (c (n "bevy_generative") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_core_pipeline" "bevy_pbr" "bevy_ui"))) (k 0)) (d (n "colorgrad") (r "^0.6.2") (d #t) (k 0)) (d (n "gltf") (r "^1.3.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "noise") (r "^0.8.2") (d #t) (k 0)) (d (n "rfd") (r "^0.12.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "1aiss848wd0s4ivdzqjk9vygxsmkq3visrljwqhjg8idxylrr7ii")))

(define-public crate-bevy_generative-0.2.0 (c (n "bevy_generative") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_core_pipeline" "bevy_pbr" "bevy_ui"))) (k 0)) (d (n "colorgrad") (r "^0.6.2") (d #t) (k 0)) (d (n "gltf") (r "^1.3.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "noise") (r "^0.8.2") (d #t) (k 0)) (d (n "rfd") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "0bllgl7ycv60anzsrqn7cn9hwl9hq592pi304alyaxv7ddcd3wga")))

