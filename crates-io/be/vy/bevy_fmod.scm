(define-module (crates-io be vy bevy_fmod) #:use-module (crates-io))

(define-public crate-bevy_fmod-0.0.1 (c (n "bevy_fmod") (v "0.0.1") (h "1yiqk1kjyqfqf4lifl1frxyfgdxcnpyl70xp9vw9y78z95pwzs7v") (y #t)))

(define-public crate-bevy_fmod-0.2.0 (c (n "bevy_fmod") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (k 0)) (d (n "bevy") (r "^0.11.2") (d #t) (k 2)) (d (n "bevy_mod_sysfail") (r "^3.0.0") (d #t) (k 0)) (d (n "libfmod") (r "^2.2.607") (d #t) (k 0)))) (h "0c8z9hqfwimkvsahzgdwgls723nppk6wjj76xcirg78gkg2f1cly")))

(define-public crate-bevy_fmod-0.3.0 (c (n "bevy_fmod") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_audio"))) (k 0)) (d (n "bevy") (r "^0.11.2") (d #t) (k 2)) (d (n "bevy_mod_sysfail") (r "^3.0.0") (d #t) (k 0)) (d (n "libfmod") (r "^2.206.2") (d #t) (k 0)) (d (n "smooth-bevy-cameras") (r "^0.9.0") (d #t) (k 2)))) (h "0vjygdpd8z9ih10cb0ciz5ykznh8mj9swykgyihwasc5vyn0hr9f") (f (quote (("live-update") ("default"))))))

(define-public crate-bevy_fmod-0.4.0 (c (n "bevy_fmod") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_audio"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_sysfail") (r "^3.0") (d #t) (k 0)) (d (n "libfmod") (r "~2.206.2") (d #t) (k 0)))) (h "0iizc59bddm39dvvrh5x09zjl7kkl5dr5dmbijzbi9i7kvy5vjsj") (f (quote (("live-update") ("default"))))))

