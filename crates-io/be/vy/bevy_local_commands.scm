(define-module (crates-io be vy bevy_local_commands) #:use-module (crates-io))

(define-public crate-bevy_local_commands-0.1.0 (c (n "bevy_local_commands") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)))) (h "1jqi1b6l64lxhjfl8kz7g7gjs09i0csx8miyxdr3pmcx15kj3yn3")))

(define-public crate-bevy_local_commands-0.1.1 (c (n "bevy_local_commands") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)))) (h "1x2qx8lc81y99c0b9sx2wbzx660z1x9gyh6w8hyj1brywv84jmdz")))

(define-public crate-bevy_local_commands-0.1.2 (c (n "bevy_local_commands") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)))) (h "1mh084vwz4c1gz8ld10bccpldj42f2abixp8isg0agyd10z0ywgx")))

(define-public crate-bevy_local_commands-0.1.3 (c (n "bevy_local_commands") (v "0.1.3") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)))) (h "1na51blsx45jlkjz1ny9r4lmsh05dd478irqk0krlblgpb4mwh10")))

(define-public crate-bevy_local_commands-0.1.4 (c (n "bevy_local_commands") (v "0.1.4") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)))) (h "0xn4chsx22bfydk0qz6g8xnm1smc06yhfgq4yqa7b5dmlc8mybv4")))

(define-public crate-bevy_local_commands-0.2.0 (c (n "bevy_local_commands") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)))) (h "1kglcbywdwvpxx9myrkqj2lpb6565yzn12a0lh92h91jqzvjsl1s")))

(define-public crate-bevy_local_commands-0.2.1 (c (n "bevy_local_commands") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)))) (h "19xp3qx9w1r1l0fz0561iw67yil1cpxr0v5kfag6z9q9g0yk9l6z")))

(define-public crate-bevy_local_commands-0.2.2 (c (n "bevy_local_commands") (v "0.2.2") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "0i6lc3sws0cwj4szxssd73b1rzdx28p26zv4wq0gzjb57f1pvkmh")))

(define-public crate-bevy_local_commands-0.2.3 (c (n "bevy_local_commands") (v "0.2.3") (d (list (d (n "bevy") (r "^0.12") (f (quote ("multi-threaded"))) (k 0)))) (h "0za88673damjlsj3vkxn1fnq8xmpbkzvf22pi6iksdzw1z1jqd5h")))

(define-public crate-bevy_local_commands-0.1.5 (c (n "bevy_local_commands") (v "0.1.5") (d (list (d (n "bevy") (r "^0.11") (f (quote ("multi-threaded"))) (k 0)))) (h "1zvwmxiv4f42y7mnx5gh0dl54m3hs0a084z7sz10vqgw3skanhwp")))

(define-public crate-bevy_local_commands-0.2.4 (c (n "bevy_local_commands") (v "0.2.4") (d (list (d (n "bevy") (r "^0.12") (f (quote ("multi-threaded"))) (k 0)))) (h "0dqfwhf87fhqsnhk2lwizxy7m62lx55ibc6la447x1fax5k914hh")))

(define-public crate-bevy_local_commands-0.3.0 (c (n "bevy_local_commands") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("multi-threaded"))) (k 0)))) (h "1wrjbxmv0vpdv5hj6x23ln4gkm5mfb3f990636hwzf9c70iraibw")))

(define-public crate-bevy_local_commands-0.4.0 (c (n "bevy_local_commands") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("multi-threaded"))) (k 0)))) (h "0wry2xgsa18was8201wi8x0ldkx8zjfzcjmiyg9wmisck5x3v7yz")))

(define-public crate-bevy_local_commands-0.4.1 (c (n "bevy_local_commands") (v "0.4.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("multi-threaded"))) (k 0)))) (h "15hfhqgk48yjf4vv5nynhbfrlnxsvmn8m6yi31xlvpi26msjmz28")))

(define-public crate-bevy_local_commands-0.5.0 (c (n "bevy_local_commands") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("multi-threaded"))) (k 0)))) (h "1kdafab7r7laylrgnb4abgdbpvi3w15y9sbpkxwhk88fpn4i9hbp")))

