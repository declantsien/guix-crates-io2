(define-module (crates-io be vy bevy-fps-counter) #:use-module (crates-io))

(define-public crate-bevy-fps-counter-0.1.0 (c (n "bevy-fps-counter") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "0bchcv8h85wgj13mrqmyh0lvdasbwp3jiv42ydyhhkdybi9hzsm3")))

(define-public crate-bevy-fps-counter-0.2.0 (c (n "bevy-fps-counter") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "0vscsnyimm74vaf8kqwp5m5nigv8nfzpfh5m107ih3s626hi66n2")))

(define-public crate-bevy-fps-counter-0.3.0 (c (n "bevy-fps-counter") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "0x90h3mn9xnnsla6a20fw4gxj9brb6if5a50nir72p8cqlc4g6lr")))

(define-public crate-bevy-fps-counter-0.4.0 (c (n "bevy-fps-counter") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "0mymbz7p2100jy8q6cclbw5q0dz06ih648jkpn8fxq62lf08jcyy")))

