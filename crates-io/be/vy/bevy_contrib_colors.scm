(define-module (crates-io be vy bevy_contrib_colors) #:use-module (crates-io))

(define-public crate-bevy_contrib_colors-0.1.0 (c (n "bevy_contrib_colors") (v "0.1.0") (d (list (d (n "bevy") (r "^0.1.3") (d #t) (k 0)))) (h "10lhdln3z5725n9nlhpsnya7q8mfxd070vikqh2kia571zliinfg")))

(define-public crate-bevy_contrib_colors-0.1.1 (c (n "bevy_contrib_colors") (v "0.1.1") (d (list (d (n "bevy") (r "^0.1.3") (d #t) (k 0)))) (h "13r7yl0gkyz2pirl14r4s2ad8lwy58zd52b84lb05ga5dmdmr54x")))

(define-public crate-bevy_contrib_colors-0.2.0 (c (n "bevy_contrib_colors") (v "0.2.0") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)))) (h "0vk3ig42j5qc9ig7n70f3kfn2dvnp866iqbyjyyjlrzdaivh98x0")))

