(define-module (crates-io be vy bevy_kindly_macros) #:use-module (crates-io))

(define-public crate-bevy_kindly_macros-0.1.0 (c (n "bevy_kindly_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1hswhn38z773di44p5j9n263z0da6ws3vfd1qr8qxxj7zwh3awvm")))

(define-public crate-bevy_kindly_macros-0.1.1 (c (n "bevy_kindly_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1dmv0gy9r2prcxannri844z64wisavlzsn4hsnpfrf43abgxsl8a")))

(define-public crate-bevy_kindly_macros-0.2.0 (c (n "bevy_kindly_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1q0vpr7si5c7nlcgsbz7lk1nf92dry7l770mkysvfr4kdc6dazl6")))

(define-public crate-bevy_kindly_macros-0.2.1 (c (n "bevy_kindly_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "09y3w6nibr1fkyjc9y6jg5rpyhk8sknxlwgg8hvym9hn1k31dnhh")))

