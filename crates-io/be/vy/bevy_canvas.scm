(define-module (crates-io be vy bevy_canvas) #:use-module (crates-io))

(define-public crate-bevy_canvas-0.1.0 (c (n "bevy_canvas") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.5") (d #t) (k 2)) (d (n "tess") (r "^0.17") (d #t) (k 0) (p "lyon_tessellation")))) (h "1ajyr220y7yii399mjhay2a5asjll33m3ha3yxr7n7w3y01fp1r4")))

