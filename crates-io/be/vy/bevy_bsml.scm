(define-module (crates-io be vy bevy_bsml) #:use-module (crates-io))

(define-public crate-bevy_bsml-0.0.1 (c (n "bevy_bsml") (v "0.0.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "0sjl08bz8wwr78sz6pzvbq8kjjz4lrcvynq04mf71akykwnjagdn") (r "1.56")))

(define-public crate-bevy_bsml-0.0.2 (c (n "bevy_bsml") (v "0.0.2") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "0rqzjarcjw8aq7c432zdbvyvh92bl03qccmp7vs58zp9njwzcbmp") (r "1.56")))

(define-public crate-bevy_bsml-0.0.3 (c (n "bevy_bsml") (v "0.0.3") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "replace_ident") (r "^0.1.1") (d #t) (k 0)))) (h "1hcf9xrx50xb5d88hnq5gi9c3shn8vh5jgzrynksan65zb1aavwj") (r "1.56")))

(define-public crate-bevy_bsml-0.0.5 (c (n "bevy_bsml") (v "0.0.5") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "replace_ident") (r "^0.1.1") (d #t) (k 0)))) (h "1bhr744lr4m6igw78ikbb60ry4qywddi9kr3vlsnrhhdla85gclb") (r "1.56")))

(define-public crate-bevy_bsml-0.0.6 (c (n "bevy_bsml") (v "0.0.6") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "replace_ident") (r "^0.1.1") (d #t) (k 0)))) (h "0919kh887rv44k9dl2lwkr4vljbq4p9831ax3ys89263g1zmk2l5") (r "1.56")))

(define-public crate-bevy_bsml-0.0.7 (c (n "bevy_bsml") (v "0.0.7") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "replace_ident") (r "^0.1.1") (d #t) (k 0)))) (h "1wjq33cpywmh8bdh82c7v6kcyfrr68ypvk6f0l9zr7h2qk7nkalr") (r "1.56")))

