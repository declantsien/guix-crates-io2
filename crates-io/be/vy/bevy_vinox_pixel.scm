(define-module (crates-io be vy bevy_vinox_pixel) #:use-module (crates-io))

(define-public crate-bevy_vinox_pixel-0.1.0 (c (n "bevy_vinox_pixel") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "bevy_sprite" "bevy_ui" "png"))) (k 0)) (d (n "bevy") (r "^0.10.1") (d #t) (k 2)))) (h "0w7v93q3fcmbxh8wqz5bgmvywa2cqb1kk2k94b753plpkkrvxaq2")))

