(define-module (crates-io be vy bevy_diagnostic_vertex_count) #:use-module (crates-io))

(define-public crate-bevy_diagnostic_vertex_count-0.1.0 (c (n "bevy_diagnostic_vertex_count") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)))) (h "1kkfzrnrv3g54r4q5h0kapbb116b1i6gqb6v3653n2a5gqw66ma4")))

(define-public crate-bevy_diagnostic_vertex_count-0.2.0 (c (n "bevy_diagnostic_vertex_count") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)))) (h "1pw6li1znzjnppkzn43lccyv7llhq89mnrzkp6bp5sy9ddi5c2zc")))

