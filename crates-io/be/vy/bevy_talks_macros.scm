(define-module (crates-io be vy bevy_talks_macros) #:use-module (crates-io))

(define-public crate-bevy_talks_macros-0.1.0 (c (n "bevy_talks_macros") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (k 2)) (d (n "proc-macro-crate") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1plhshrhnmq3rm0hqnlq2nidngy42ygj0bjfv6rjyasslixzqv6x")))

