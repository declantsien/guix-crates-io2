(define-module (crates-io be vy bevy_sytem_graph) #:use-module (crates-io))

(define-public crate-bevy_sytem_graph-0.1.1 (c (n "bevy_sytem_graph") (v "0.1.1") (d (list (d (n "bevy_ecs") (r "^0.6") (d #t) (k 0)) (d (n "bevy_ecs_macros") (r "^0.6") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.6") (d #t) (k 0)))) (h "1in6qjz5zjd1mq21qxqsff1ps6vlp4xpilcwgmhxcj2wv5948hhy") (y #t)))

