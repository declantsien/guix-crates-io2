(define-module (crates-io be vy bevy_sprite_grid) #:use-module (crates-io))

(define-public crate-bevy_sprite_grid-0.2.0 (c (n "bevy_sprite_grid") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0l1xvd5p3m150ixxzhsadn6z6lyrv93j9ing4isr96m56b0a70gj")))

(define-public crate-bevy_sprite_grid-0.3.0 (c (n "bevy_sprite_grid") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "02jcsd2kjcmgfwzjvmqqgbn94qphngxri2nvf9badjg952pg2pwc")))

(define-public crate-bevy_sprite_grid-0.3.1 (c (n "bevy_sprite_grid") (v "0.3.1") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0rg2pvz37gih4w4s76svz2wl7mlspncmzfmdj92586g9il8nvmgb")))

(define-public crate-bevy_sprite_grid-0.3.11 (c (n "bevy_sprite_grid") (v "0.3.11") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0lxa8m7ynpcqn571am8i6mpr72lyv13cgcjl9avdf26ilgiyxyc6")))

(define-public crate-bevy_sprite_grid-0.4.0 (c (n "bevy_sprite_grid") (v "0.4.0") (d (list (d (n "bevy") (r "^0.7.0") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.7.0") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "1jbam4q4b7xlryk4l4pwlbh6jznsclyfbcy7jvkjwmx7vj13qwf6")))

(define-public crate-bevy_sprite_grid-0.5.0 (c (n "bevy_sprite_grid") (v "0.5.0") (d (list (d (n "bevy") (r "^0.8.0") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0h2dyfdznvbcl8gmn86djax2cc77qc6w5qjr6jmp63af3dvzwkcq")))

