(define-module (crates-io be vy bevy_rng) #:use-module (crates-io))

(define-public crate-bevy_rng-0.1.0 (c (n "bevy_rng") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 0)))) (h "1pag26ndhcrr2ns5mspqfizir1cv9v8lsch52aff38m3bv283q8f")))

(define-public crate-bevy_rng-0.2.0 (c (n "bevy_rng") (v "0.2.0") (d (list (d (n "bevy") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)))) (h "1mkg78i8q4m1r33pn95m9bsj4d4xnxhn9r48a5prk2gjhbvd4j2v")))

(define-public crate-bevy_rng-0.3.0 (c (n "bevy_rng") (v "0.3.0") (d (list (d (n "bevy-nightly") (r "^0.4") (o #t) (k 0) (p "bevy")) (d (n "bevy-stable") (r "^0.4") (o #t) (k 0) (p "bevy")) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)))) (h "118x5cqqwwzyf51vc1bm07hkx0pd18laqi5kn05p0v0rlsx5q703") (f (quote (("default" "bevy-stable"))))))

