(define-module (crates-io be vy bevy_oneshot) #:use-module (crates-io))

(define-public crate-bevy_oneshot-0.1.0 (c (n "bevy_oneshot") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "0sa19543984q5g74q5zd6hzh5c4n3vpkq44ywcj5wcffjs4v8vxp")))

(define-public crate-bevy_oneshot-0.1.1 (c (n "bevy_oneshot") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "0js5qnkg8dcc1gynlmbh48abs4xrmhsgl5lzqka8kxw32rcm9ywa")))

(define-public crate-bevy_oneshot-0.2.0 (c (n "bevy_oneshot") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)))) (h "1i66w6hxmkarpb2a37nd5lc9jfbymq7s7cymhy0jaamvl2bb3w3d")))

