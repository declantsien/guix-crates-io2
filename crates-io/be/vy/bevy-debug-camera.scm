(define-module (crates-io be vy bevy-debug-camera) #:use-module (crates-io))

(define-public crate-bevy-debug-camera-0.1.0 (c (n "bevy-debug-camera") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)))) (h "0dcrb87wkvskyhrlcmv299sadxf19k5n9a81cr0cmwi1pnfpbk1a")))

(define-public crate-bevy-debug-camera-0.1.1 (c (n "bevy-debug-camera") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)))) (h "05y3jcpndlds410i7pmsq13h4ry8ipgg8xds0ka3p3r4dwrhd4f8")))

(define-public crate-bevy-debug-camera-0.2.0 (c (n "bevy-debug-camera") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)))) (h "1vxr7j44phz1rfbrkkxhys7v1fmsw5chf83rcj3qnlammj2r2ywb")))

(define-public crate-bevy-debug-camera-0.3.0 (c (n "bevy-debug-camera") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "1mgigar1h55vn5vdp26xk0bjs4i74dlnjpws5l9spry71bi2d1xs")))

