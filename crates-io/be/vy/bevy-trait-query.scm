(define-module (crates-io be vy bevy-trait-query) #:use-module (crates-io))

(define-public crate-bevy-trait-query-0.0.1 (c (n "bevy-trait-query") (v "0.0.1") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "049h9pw50726bwxgii7g3j8pgjrzpagklawi03ilr7yaxgsgnbi9")))

(define-public crate-bevy-trait-query-0.0.2 (c (n "bevy-trait-query") (v "0.0.2") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1gp62rk0jr92wkmb8rnfxdkqmc15a6visy1yjrpywq0q3jdgh7hc")))

(define-public crate-bevy-trait-query-0.0.3 (c (n "bevy-trait-query") (v "0.0.3") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "098cnd2ad58z0hrkvk033vmrxn0kpbj9z0j4h88bpccq2wz9wqlj")))

(define-public crate-bevy-trait-query-0.1.0 (c (n "bevy-trait-query") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy-trait-query-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1zwxm49v64sx3vaan6mwy3iwwv13rf3349fjky2z9yaczpa160h8")))

(define-public crate-bevy-trait-query-0.1.1 (c (n "bevy-trait-query") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy-trait-query-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0zi1yb0dx1pjk5fdblnspgpsk7846jpgffg5qj6sq17kn1jm3ls1")))

(define-public crate-bevy-trait-query-0.2.0 (c (n "bevy-trait-query") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy-trait-query-impl") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1g11366q76m20bwklywb8pkjqdgp98j3svh0iyhxgy0jnh7k8hs4") (y #t)))

(define-public crate-bevy-trait-query-0.2.1 (c (n "bevy-trait-query") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy-trait-query-impl") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1mzg51yk4xx04x3bp5v98ln15d07qq70n14ml2wp9zfariyic6bp")))

(define-public crate-bevy-trait-query-0.3.0 (c (n "bevy-trait-query") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy-trait-query-impl") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1c5vqinrbszly41jq1d6rgx91wp0fg6kf63qdp3gn9f7cf8smk84")))

(define-public crate-bevy-trait-query-0.4.0 (c (n "bevy-trait-query") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (k 2)) (d (n "bevy-trait-query-impl") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_app") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bevy_core") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09i10xd3xp74v1cp1mx4ppn15jq2vyh5iy86i93yi7an177y7h03") (f (quote (("default" "bevy_app" "bevy_core"))))))

(define-public crate-bevy-trait-query-0.5.0 (c (n "bevy-trait-query") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (k 2)) (d (n "bevy-trait-query-impl") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_app") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_core") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0yda7jvmwmpr6vfhx7lslvf0wk43i515al266m89rdid4q5vwjjj") (f (quote (("default" "bevy_app" "bevy_core")))) (y #t)))

(define-public crate-bevy-trait-query-0.5.1 (c (n "bevy-trait-query") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13") (k 2)) (d (n "bevy-trait-query-impl") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_app") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_core") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "12rdg8hbg4i5fsn567ifvivcjiz20db4bbxks711430kqlpqzym5") (f (quote (("default" "bevy_app" "bevy_core"))))))

