(define-module (crates-io be vy bevy_mod_2d_hierarchy) #:use-module (crates-io))

(define-public crate-bevy_mod_2d_hierarchy-0.1.0 (c (n "bevy_mod_2d_hierarchy") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "13xb15g73hk7iqilbyvwbbdsn2ld4ba5bmx5qvf74kblfzv1i9s2")))

(define-public crate-bevy_mod_2d_hierarchy-0.2.0 (c (n "bevy_mod_2d_hierarchy") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0fy1hmdwfl7qaaj5bgr0npcra6mlgd9isr047hgf6x75dgnccfd5")))

(define-public crate-bevy_mod_2d_hierarchy-0.3.0 (c (n "bevy_mod_2d_hierarchy") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "1s4mb10hfg8bxr25kzhwqfdl4k63v1rmbcfz2dk1y6728pgrvwd4")))

(define-public crate-bevy_mod_2d_hierarchy-0.3.1 (c (n "bevy_mod_2d_hierarchy") (v "0.3.1") (d (list (d (n "bevy") (r "^0.9.1") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "04y1b8zyhpgpnsbbffjv9i20qqd4v2qn3q8lgvmrxvnhczp46521")))

