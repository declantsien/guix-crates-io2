(define-module (crates-io be vy bevy_defer_derive) #:use-module (crates-io))

(define-public crate-bevy_defer_derive-0.1.0 (c (n "bevy_defer_derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0xcnziy2dygsz90dqy9m14y191z47hkhb8rk9lic00q28kp3nbyn")))

