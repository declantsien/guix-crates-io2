(define-module (crates-io be vy bevy_hecs_macros) #:use-module (crates-io))

(define-public crate-bevy_hecs_macros-0.1.0 (c (n "bevy_hecs_macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bxyxvglysdyqs899wxmk0n0719sz636anf4ql45gsl753mci3d6")))

(define-public crate-bevy_hecs_macros-0.1.3 (c (n "bevy_hecs_macros") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05z41zal474wr6cr9k9hbpcqd0q6bk1k5wclmai254cpb7agpg57")))

(define-public crate-bevy_hecs_macros-0.2.0 (c (n "bevy_hecs_macros") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hbg6dvk86j8hj0799mp6as6rg2crvbyz7lws0j4gp4b4fzsqfr1")))

(define-public crate-bevy_hecs_macros-0.2.1 (c (n "bevy_hecs_macros") (v "0.2.1") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fpi09f953h1cs43ns6v9y5jpmsv7ivprpk33s8kgljjssypsx4i")))

(define-public crate-bevy_hecs_macros-0.3.0 (c (n "bevy_hecs_macros") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ah4riaggsnhl2krflqwfzgac2ja2s8rrw32m8hkh623f0xy919s")))

