(define-module (crates-io be vy bevy-egui-kbgp) #:use-module (crates-io))

(define-public crate-bevy-egui-kbgp-0.1.0 (c (n "bevy-egui-kbgp") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6.1") (k 0)) (d (n "bevy") (r "^0.6.1") (f (quote ("bevy_gilrs"))) (k 2)) (d (n "bevy_egui") (r "^0.12") (k 0)))) (h "1plshxbsvyf2i0a0irhrgcsjwvlcsarsqn96ywn4q6lg119m31ic")))

(define-public crate-bevy-egui-kbgp-0.1.1 (c (n "bevy-egui-kbgp") (v "0.1.1") (d (list (d (n "bevy") (r "^0.6.1") (k 0)) (d (n "bevy") (r "^0.6.1") (f (quote ("bevy_gilrs"))) (k 2)) (d (n "bevy_egui") (r "^0.12") (k 0)))) (h "0wll0zg0770nimpdarya0g83qr3bmscx49j0qfqb5qkafvmqxpqf")))

(define-public crate-bevy-egui-kbgp-0.1.2 (c (n "bevy-egui-kbgp") (v "0.1.2") (d (list (d (n "bevy") (r "^0.6.1") (k 0)) (d (n "bevy") (r "^0.6.1") (f (quote ("bevy_gilrs"))) (k 2)) (d (n "bevy_egui") (r "^0.12") (k 0)))) (h "03cmf7jbl5hnpmn8wzqc1675vk4da9jrxcjzw3igxbfs27yv2zyx")))

(define-public crate-bevy-egui-kbgp-0.2.0 (c (n "bevy-egui-kbgp") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6.1") (k 0)) (d (n "bevy") (r "^0.6.1") (f (quote ("bevy_gilrs"))) (k 2)) (d (n "bevy_egui") (r "^0.12") (k 0)))) (h "0cg10agl6afg2fxvxzv5qiv9kmsdph59p84shfm3d4nswx41l25p")))

(define-public crate-bevy-egui-kbgp-0.2.1 (c (n "bevy-egui-kbgp") (v "0.2.1") (d (list (d (n "bevy") (r "^0.6.1") (k 0)) (d (n "bevy") (r "^0.6.1") (f (quote ("bevy_gilrs"))) (k 2)) (d (n "bevy_egui") (r "^0.12") (k 0)))) (h "19w2kg9v1c9c6xnwwsyplfjsiwkk3w0gpfrmrsj3w6y3m45nwhjn")))

(define-public crate-bevy-egui-kbgp-0.3.0 (c (n "bevy-egui-kbgp") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6.1") (k 0)) (d (n "bevy") (r "^0.6.1") (f (quote ("bevy_gilrs"))) (k 2)) (d (n "bevy_egui") (r "^0.12") (k 0)))) (h "0g8fb9cpyais4bvb2k8qfl3grny61x14szy98qvxi0z5ggqqpdcv")))

(define-public crate-bevy-egui-kbgp-0.4.0 (c (n "bevy-egui-kbgp") (v "0.4.0") (d (list (d (n "bevy") (r "^0.6.1") (k 0)) (d (n "bevy") (r "^0.6.1") (f (quote ("bevy_gilrs"))) (k 2)) (d (n "bevy_egui") (r "^0.12") (k 0)))) (h "1sf1r0zfnckgszx8ffqn2mvmva0y1ycp4px75fiqqjr4ppclq3qz")))

(define-public crate-bevy-egui-kbgp-0.5.0 (c (n "bevy-egui-kbgp") (v "0.5.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("bevy_gilrs"))) (k 2)) (d (n "bevy_egui") (r "^0.13") (k 0)))) (h "1pcgd19pyaxabp6n32wnm0zr7msvaxhhh775ggnk53284cmihz24")))

(define-public crate-bevy-egui-kbgp-0.6.0 (c (n "bevy-egui-kbgp") (v "0.6.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.14") (k 0)))) (h "0szpbk2x4q2khm495c4s8ygxskbnvqpb85j4fq44m8b6ydgmsxcm")))

(define-public crate-bevy-egui-kbgp-0.7.0 (c (n "bevy-egui-kbgp") (v "0.7.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.15") (k 0)) (d (n "bevy_egui") (r "^0.15") (f (quote ("default_fonts"))) (k 2)))) (h "003c697bkyrg5mjkf6jpjr0mj2w625jsdzrr6q5jssx9cxw80l8v")))

(define-public crate-bevy-egui-kbgp-0.8.0 (c (n "bevy-egui-kbgp") (v "0.8.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.16") (k 0)) (d (n "bevy_egui") (r "^0.16") (f (quote ("default_fonts"))) (k 2)))) (h "1dwjjjh0zl2s1mgbn2fk36yz8684czfkn3lw37nq140a9myhrmrm")))

(define-public crate-bevy-egui-kbgp-0.9.0 (c (n "bevy-egui-kbgp") (v "0.9.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.17") (k 0)) (d (n "bevy_egui") (r "^0.17") (f (quote ("default_fonts"))) (k 2)))) (h "1kx6vnyfxzlzj399qpfzxqcb82c5x25z8cmcg7zarlcyfzcbrh96")))

(define-public crate-bevy-egui-kbgp-0.10.0 (c (n "bevy-egui-kbgp") (v "0.10.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.18") (k 0)) (d (n "bevy_egui") (r "^0.18") (f (quote ("default_fonts"))) (k 2)))) (h "0wkk529ahjnyxribn7xq0vxvm3kd0crxznmdzldkd69is5qdlj7f")))

(define-public crate-bevy-egui-kbgp-0.11.0 (c (n "bevy-egui-kbgp") (v "0.11.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.19.0") (k 0)) (d (n "bevy_egui") (r "^0.19.0") (f (quote ("default_fonts"))) (k 2)))) (h "1zmbqv6bfyq5772q7i6c48g2mxgb20601vclq7lja1ac1azmw4f7")))

(define-public crate-bevy-egui-kbgp-0.12.0 (c (n "bevy-egui-kbgp") (v "0.12.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.20.0") (k 0)) (d (n "bevy_egui") (r "^0.20.0") (f (quote ("default_fonts"))) (k 2)))) (h "17fzw5mpxnh8snqx83m5jh1l40d9nrhg3j0fqwwjns2xhyncbd0v")))

(define-public crate-bevy-egui-kbgp-0.13.0 (c (n "bevy-egui-kbgp") (v "0.13.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.20") (k 0)) (d (n "bevy_egui") (r "^0.20") (f (quote ("default_fonts"))) (k 2)))) (h "0jqzp9acb3dwk36wx721c623fqj244si4vyg24rp467yfqxpp8hg")))

(define-public crate-bevy-egui-kbgp-0.14.0 (c (n "bevy-egui-kbgp") (v "0.14.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.21") (k 0)) (d (n "bevy_egui") (r "^0.21") (f (quote ("default_fonts"))) (k 2)))) (h "0lif94bbhgrdyimmr7s4bakm43kylpf9fx3xqhrwyxg72y1b9aa6")))

(define-public crate-bevy-egui-kbgp-0.15.0 (c (n "bevy-egui-kbgp") (v "0.15.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.22") (k 0)) (d (n "bevy_egui") (r "^0.22") (f (quote ("default_fonts"))) (k 2)))) (h "0ikh40jdkyb5ylr9yaxq004i980k959ydzzix85xzf0l20m0l8h6")))

(define-public crate-bevy-egui-kbgp-0.16.0 (c (n "bevy-egui-kbgp") (v "0.16.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.23") (k 0)) (d (n "bevy_egui") (r "^0.23") (f (quote ("default_fonts"))) (k 2)))) (h "1w52b25bw6dilh9chfsm4x4x4zq07xlqkcx9b856myby7mfk6pxw")))

(define-public crate-bevy-egui-kbgp-0.17.0 (c (n "bevy-egui-kbgp") (v "0.17.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.25") (k 0)) (d (n "bevy_egui") (r "^0.25") (f (quote ("default_fonts" "render"))) (k 2)))) (h "0j8ci7fzw5w8zp2phfk2msqp7kapzg1ss0fm28gkdsm79lyczw1k")))

(define-public crate-bevy-egui-kbgp-0.18.0 (c (n "bevy-egui-kbgp") (v "0.18.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.26") (k 0)) (d (n "bevy_egui") (r "^0.26") (f (quote ("default_fonts" "render"))) (k 2)))) (h "0rpvp9a666b12arjk2zjysy5i4l07868mzk2yfzmdd1hy2p4xds5")))

(define-public crate-bevy-egui-kbgp-0.19.0 (c (n "bevy-egui-kbgp") (v "0.19.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_gilrs" "x11"))) (k 2)) (d (n "bevy_egui") (r "^0.27") (k 0)) (d (n "bevy_egui") (r "^0.27") (f (quote ("default_fonts" "render"))) (k 2)))) (h "04fy56byx517iqqzh9nafpn8w5v5h0y346rzd096ilpqgdmgi6az")))

