(define-module (crates-io be vy bevy_fps_controller) #:use-module (crates-io))

(define-public crate-bevy_fps_controller-0.1.0-dev (c (n "bevy_fps_controller") (v "0.1.0-dev") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.16.0") (d #t) (k 0)))) (h "13pmyqbdxfm4i9j54lnv9xqfc6dlqgxgqk18rfdjs43gs96w395z")))

(define-public crate-bevy_fps_controller-0.1.1-dev (c (n "bevy_fps_controller") (v "0.1.1-dev") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.16.0") (d #t) (k 0)))) (h "00gvpmq5mn2c77p9nvw9pm9in13cqdyxzj0pg91cajy2d0xqxjri")))

(define-public crate-bevy_fps_controller-0.1.2-dev (c (n "bevy_fps_controller") (v "0.1.2-dev") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.16.0") (d #t) (k 0)))) (h "1c47czag56mbdpxizq4cfgk2yq7nmhc4cisqgh89far1jsxhvvmv")))

(define-public crate-bevy_fps_controller-0.1.3-dev (c (n "bevy_fps_controller") (v "0.1.3-dev") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.17.0") (d #t) (k 0)))) (h "044y7qw4qc90ww8z6k0061lvbw3yhk2vrwhww8k3c41wm0v3awcp")))

(define-public crate-bevy_fps_controller-0.1.4-dev (c (n "bevy_fps_controller") (v "0.1.4-dev") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "1grspik302vcg5wigp184iw57wpv8gzkcn0zk6svf51w54hj12jw")))

(define-public crate-bevy_fps_controller-0.1.5-dev (c (n "bevy_fps_controller") (v "0.1.5-dev") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "0549s89qgzzrwxw36d2j7m49qpv0fpfpc9jkqls0n9gqf0jmrg64")))

(define-public crate-bevy_fps_controller-0.1.6-dev (c (n "bevy_fps_controller") (v "0.1.6-dev") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (d #t) (k 0)))) (h "1qhwdrggzi0w1j5cl4ijvqcqwvin7ws8n7dv22m5bny0a41rdss6")))

(define-public crate-bevy_fps_controller-0.1.7-dev (c (n "bevy_fps_controller") (v "0.1.7-dev") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (d #t) (k 0)))) (h "1ba3i593a4mrr2l0h9l1qqjqdnyj1g2611lhakl66jmviqj64s7q")))

(define-public crate-bevy_fps_controller-0.1.8-dev (c (n "bevy_fps_controller") (v "0.1.8-dev") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (d #t) (k 0)))) (h "0y3h0gvn0jb6mvjj5nsp9y2x2vibw2cfvnsrh5vjhivndhx4f900")))

(define-public crate-bevy_fps_controller-0.2.0-dev (c (n "bevy_fps_controller") (v "0.2.0-dev") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.21.0") (d #t) (k 0)))) (h "0p0wyg1ip70lnrvfsqmr4sd5jq1f52gbzi6188v4sijvkrvc78nc")))

(define-public crate-bevy_fps_controller-0.2.1 (c (n "bevy_fps_controller") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.21.0") (d #t) (k 0)))) (h "0z20k5cqcayipn4mvw7lqm4gxrlw0hvm1xlqzx05l56hsn29wghs")))

(define-public crate-bevy_fps_controller-0.2.2 (c (n "bevy_fps_controller") (v "0.2.2") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.22.0") (d #t) (k 0)))) (h "1zi8p8agj9ir47vkpp3fs706gsrrw3ps7kr18nr92yvqb02pad84")))

(define-public crate-bevy_fps_controller-0.2.3 (c (n "bevy_fps_controller") (v "0.2.3") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.22.0") (d #t) (k 0)))) (h "0a38mp6aa7b3mpnpik5fls34r7a31bnj36pzgg35diyq11m5pgq1")))

(define-public crate-bevy_fps_controller-0.2.4 (c (n "bevy_fps_controller") (v "0.2.4") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.23.0") (d #t) (k 0)))) (h "0kbk0h6pmfci72684x6zgrkhwa517f55r91swjcydcgcgqhnllfx")))

(define-public crate-bevy_fps_controller-0.2.5 (c (n "bevy_fps_controller") (v "0.2.5") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.25.0") (d #t) (k 0)))) (h "19nznwysy3nkax3smpsjcq6xydm8g4yz32vvw8gy6rz0s59bgxdk")))

