(define-module (crates-io be vy bevy_rapier_collider_gen) #:use-module (crates-io))

(define-public crate-bevy_rapier_collider_gen-0.1.0 (c (n "bevy_rapier_collider_gen") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.7.1") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.19.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 2)))) (h "04qxd40lnmxfi1ay12j5fa95lzq0jdzl669yz0zcgcx43nvzg4zq")))

(define-public crate-bevy_rapier_collider_gen-0.1.1 (c (n "bevy_rapier_collider_gen") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.7.1") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.19.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 2)))) (h "0v15vjh8596q8xrmqxwr5658l5dxxh8gfcg5d9my0dfq1g5g32rw")))

(define-public crate-bevy_rapier_collider_gen-0.1.2 (c (n "bevy_rapier_collider_gen") (v "0.1.2") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.8.0") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.21.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.9") (d #t) (k 2)))) (h "0ndq08pj1if7cmwglcrik6gls297gm2bwxhj9y9i8valywy7q4ml")))

(define-public crate-bevy_rapier_collider_gen-0.2.0 (c (n "bevy_rapier_collider_gen") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.9.0") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.22.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.9") (d #t) (k 2)))) (h "0qnb5q9y4yh8ag14siqq520jjbwgcg7kilvfrh2z4al44fmdl87c")))

(define-public crate-bevy_rapier_collider_gen-0.2.1 (c (n "bevy_rapier_collider_gen") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.10.0") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.23.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "09vppbjmgf74hl05wskl4c6s54mxm15h7xiwmxd6q4n4w8qxx4ry")))

(define-public crate-bevy_rapier_collider_gen-0.3.0 (c (n "bevy_rapier_collider_gen") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.10.0") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.23.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "0c1c75mgf6si1scx75129nf2ngfqg1zzq5lr757lvqnqj930dxmp")))

(define-public crate-bevy_rapier_collider_gen-0.3.1 (c (n "bevy_rapier_collider_gen") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.10.0") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.23.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "04mzj5sc5sqbm91zrynndaqsdsd1ymjs0kdzh8yw7d4xdccs9shd")))

(define-public crate-bevy_rapier_collider_gen-0.4.0 (c (n "bevy_rapier_collider_gen") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.11.0") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.25.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "0686c8j5z1cnknfy46dvn2kasy93cw2k1k73ag9mlb5bknzazs9c")))

(define-public crate-bevy_rapier_collider_gen-0.4.2 (c (n "bevy_rapier_collider_gen") (v "0.4.2") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.11.0") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.25.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "0q034q1c9fnfrsrhl5y3pcm9l4385lq1vwswszhbazc2gk26m7sr")))

