(define-module (crates-io be vy bevy-web3) #:use-module (crates-io))

(define-public crate-bevy-web3-0.1.0 (c (n "bevy-web3") (v "0.1.0") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (k 0)) (d (n "web3") (r "^0.19") (f (quote ("wasm" "eip-1193"))) (k 0)))) (h "1mr2n334x8hhf0bscwmy2fwmsxf6r060s2ppcyffb0vx725wfy44")))

