(define-module (crates-io be vy bevy_bundlication_macros) #:use-module (crates-io))

(define-public crate-bevy_bundlication_macros-0.0.1 (c (n "bevy_bundlication_macros") (v "0.0.1") (d (list (d (n "bevy_macro_utils") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0jvi4bl0qjm96663ijiadc76bpkkl6jb7f9s4yjk5brmy4w2xcip")))

(define-public crate-bevy_bundlication_macros-0.0.2 (c (n "bevy_bundlication_macros") (v "0.0.2") (d (list (d (n "bevy_macro_utils") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1dn6p586n2yc6dg5yz0y7ny4n70wim9hqwhr5111x17pp42lc24m")))

(define-public crate-bevy_bundlication_macros-0.1.0 (c (n "bevy_bundlication_macros") (v "0.1.0") (d (list (d (n "bevy_macro_utils") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0a15ddm12bg0ngzb1qzvk2dsd4j7i2qb6zbnkl3xksp290rrgy5k")))

(define-public crate-bevy_bundlication_macros-0.2.0 (c (n "bevy_bundlication_macros") (v "0.2.0") (d (list (d (n "bevy_macro_utils") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "076iml2nawkflxia0pjz30y7v6hs3hj8389zkbga4ayc7dnzyqf3")))

(define-public crate-bevy_bundlication_macros-0.3.0 (c (n "bevy_bundlication_macros") (v "0.3.0") (d (list (d (n "bevy_macro_utils") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1iixa2fwacs1qn06nbmrg4660739pgic7ahcj1h691dr4wpy0zws")))

(define-public crate-bevy_bundlication_macros-0.4.0 (c (n "bevy_bundlication_macros") (v "0.4.0") (d (list (d (n "bevy_macro_utils") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1c2h0rs5cabj0m3bwri16kb4pd2l1nphk0wbpldlxkh51wxfpfir")))

(define-public crate-bevy_bundlication_macros-0.5.0 (c (n "bevy_bundlication_macros") (v "0.5.0") (d (list (d (n "bevy_macro_utils") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1706yk5lq3d2qsbrp2j5bsg4ig2gjza9an5zj0yjc2z89vn3m7vv")))

