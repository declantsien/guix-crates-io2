(define-module (crates-io be vy bevy_ui_material) #:use-module (crates-io))

(define-public crate-bevy_ui_material-0.1.0 (c (n "bevy_ui_material") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.6") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.6") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.6") (d #t) (k 0)) (d (n "bevy_math") (r "^0.6") (d #t) (k 0)) (d (n "bevy_render") (r "^0.6") (d #t) (k 0)) (d (n "bevy_sprite") (r "^0.6") (d #t) (k 0)) (d (n "bevy_transform") (r "^0.6") (d #t) (k 0)) (d (n "bevy_ui") (r "^0.6") (d #t) (k 0)) (d (n "copyless") (r "^0.1") (d #t) (k 0)))) (h "0znxr399sks4a0igcmnz2q9fn43m2hi31zwamhcpiq2apsznk9lr")))

(define-public crate-bevy_ui_material-0.2.0 (c (n "bevy_ui_material") (v "0.2.0") (d (list (d (n "bevy_app") (r "^0.7") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.7") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.7") (d #t) (k 0)) (d (n "bevy_math") (r "^0.7") (d #t) (k 0)) (d (n "bevy_render") (r "^0.7") (d #t) (k 0)) (d (n "bevy_sprite") (r "^0.7") (d #t) (k 0)) (d (n "bevy_transform") (r "^0.7") (d #t) (k 0)) (d (n "bevy_ui") (r "^0.7") (d #t) (k 0)))) (h "0qn927ppbif4q585hmsbgpab109ybcchynp2qg4d9d9vm27g0vg9")))

