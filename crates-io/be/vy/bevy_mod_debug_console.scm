(define-module (crates-io be vy bevy_mod_debug_console) #:use-module (crates-io))

(define-public crate-bevy_mod_debug_console-0.0.1 (c (n "bevy_mod_debug_console") (v "0.0.1") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy") (r "^0.5") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "029dh2cl2z3vm89fpj7ijkxnmlkn095ir16nnckalb0qpy799bxa")))

(define-public crate-bevy_mod_debug_console-0.0.2 (c (n "bevy_mod_debug_console") (v "0.0.2") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy") (r "^0.5") (d #t) (k 2)) (d (n "bevy_console") (r "^0.2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "1gszsh1mv8n9zym0lnf30x38r62i1lqgscxl7hcw7zlpzylb0k38")))

(define-public crate-bevy_mod_debug_console-0.1.0 (c (n "bevy_mod_debug_console") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_console") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "1x980yw1533wd3diihm5syrpjgqd5318yf4f6w60kgk3jx4yaw3g")))

