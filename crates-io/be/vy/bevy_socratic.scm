(define-module (crates-io be vy bevy_socratic) #:use-module (crates-io))

(define-public crate-bevy_socratic-0.0.1 (c (n "bevy_socratic") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "socratic") (r "^0.0.1") (d #t) (k 0)))) (h "1xc3iyl0scav75ga1v18grhb8qkn9k4l8di562412xi6baj5jf0q")))

