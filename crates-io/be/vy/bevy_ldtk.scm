(define-module (crates-io be vy bevy_ldtk) #:use-module (crates-io))

(define-public crate-bevy_ldtk-0.0.1 (c (n "bevy_ldtk") (v "0.0.1") (h "0mrmd6hai1cgy7hqh0sc4syjfsqh8vc19jlvwq4fdb1b7r46mzwm")))

(define-public crate-bevy_ldtk-0.2.0-alpha1 (c (n "bevy_ldtk") (v "0.2.0-alpha1") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4.0") (d #t) (k 0)) (d (n "ldtk") (r "^0.2.0-alpha1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1p3amgr828mb1wf99kis3180a6biwk8ghr2gysmgq33rq197yps2")))

(define-public crate-bevy_ldtk-0.2.0 (c (n "bevy_ldtk") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "11dafs4k0iy9zwp2hjm96a6zqwh07nnx1gcjbdkzaqhk5nbsf45k") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.2.1 (c (n "bevy_ldtk") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1jpfqqv9b2x5clqpyf5b3cdqfh4ycdmxldm4hbkas56ah2p5chjr") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.2.2 (c (n "bevy_ldtk") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "08y17wpqxfhxbd1r788zvanfh41vwn2d253lw0dimz9xm4crjmc9") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.2.3 (c (n "bevy_ldtk") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0wv06s0wfjg7ncg9m4izxfn7jkf4x9mfcydsmp885k8jb1p3240p") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.2.4 (c (n "bevy_ldtk") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1abkmb76vr1xbv2cwq331wwrj67avh80sv5zn0f8kc2yvwld3x8l") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.3.0 (c (n "bevy_ldtk") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "02bcwwz9ddxxz9yvig35is3zs144vgg2zy4b0vw57v1gi8q10plm") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.3.1 (c (n "bevy_ldtk") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1njwfa8am51g2h0c7ixqqfiz93mw8h2gkxhpx1ld7s0qyy79plfj") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.4.0 (c (n "bevy_ldtk") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "13ygag3vhw83hv3zs0wby8bd0zrnb4qprsg34siryrgzc0358025") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.4.1 (c (n "bevy_ldtk") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "18w51akccbha345rdlidfgk709pcxwxpf276db74nwgp4wlss5rv") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.4.2 (c (n "bevy_ldtk") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "ldtk") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1560nvcfc2qp68fc5g2bngs4y0zfa17ihsjc43zl1ahirrrj2sw0") (f (quote (("default") ("bevy-unstable"))))))

(define-public crate-bevy_ldtk-0.5.0 (c (n "bevy_ldtk") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.5") (d #t) (k 2)) (d (n "ldtk") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0vfbiwl20j9hm1pifnadxn16qk5n5991sqvd1akmh4rjr7pxp1rp") (f (quote (("default"))))))

