(define-module (crates-io be vy bevy_outline) #:use-module (crates-io))

(define-public crate-bevy_outline-0.1.0 (c (n "bevy_outline") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("dynamic"))) (d #t) (k 2)) (d (n "bevy_obj") (r "^0.7.0") (d #t) (k 2)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "wgpu-types") (r "^0.12.0") (d #t) (k 0)))) (h "17k1xrz5w4qcwk2yz585l1q3g02w6pnhyf20n3fw1451csjm6mgl")))

