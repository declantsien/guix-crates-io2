(define-module (crates-io be vy bevy-simple-state-machine) #:use-module (crates-io))

(define-public crate-bevy-simple-state-machine-0.1.0 (c (n "bevy-simple-state-machine") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)))) (h "06bzgq45klkhhwskz4rsrvr7r18si84kgl74aprm5lnwgzlf7plg")))

(define-public crate-bevy-simple-state-machine-0.2.0 (c (n "bevy-simple-state-machine") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)))) (h "14rfbjmg5lxdl4sp7brbwm4p1darfbxm9v8filqvw74y6d27w4rr")))

(define-public crate-bevy-simple-state-machine-0.3.0 (c (n "bevy-simple-state-machine") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)))) (h "1mg1ikvwb786a1mn8p2nxj6a2n3fnqvadfms8zinynjrmh2qrz39")))

(define-public crate-bevy-simple-state-machine-0.4.0 (c (n "bevy-simple-state-machine") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)))) (h "0xr8pkhag4nlg9kbh2p9ds36jnqk80qgk26k71ss3n1gbg09i3j5")))

(define-public crate-bevy-simple-state-machine-0.5.0 (c (n "bevy-simple-state-machine") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "1924qkvx8341gy052q2f82y133dsz0547fgr9gab7zlxs3c894vz")))

(define-public crate-bevy-simple-state-machine-0.6.0 (c (n "bevy-simple-state-machine") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)))) (h "1xg61wf4mb6xbj0wrgpqj69w59x6vi0bbgkrc5s71bl0f3ylk3l9")))

