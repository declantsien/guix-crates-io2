(define-module (crates-io be vy bevy_replicon_snap_macros) #:use-module (crates-io))

(define-public crate-bevy_replicon_snap_macros-0.2.0 (c (n "bevy_replicon_snap_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1b1v7dk0p1j1lp4nw76kv5n9nqx7iij1k3mz45v1fmfawfkjqzqv")))

