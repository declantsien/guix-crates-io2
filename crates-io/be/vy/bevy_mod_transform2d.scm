(define-module (crates-io be vy bevy_mod_transform2d) #:use-module (crates-io))

(define-public crate-bevy_mod_transform2d-0.1.0 (c (n "bevy_mod_transform2d") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_rapier2d") (r ">=0.14, <0.16") (o #t) (k 0)))) (h "19pv70c1vqimh3q0sz9pr9jzis2vk6dbxx8px74y3ygf2zn0gl1a")))

(define-public crate-bevy_mod_transform2d-0.1.1 (c (n "bevy_mod_transform2d") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_rapier2d") (r ">=0.14, <0.16") (o #t) (k 0)))) (h "1ssjhjc30yffb29lc9fx6p96gx11d7k2zhxdmw3wyka14wc7h2lx")))

(define-public crate-bevy_mod_transform2d-0.2.0 (c (n "bevy_mod_transform2d") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.16") (o #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.16") (d #t) (k 2)))) (h "1i1308265gc6bp6vrw53g56yp2wd8v6wwkv9a409j3dqah0jmjpr")))

(define-public crate-bevy_mod_transform2d-0.3.0 (c (n "bevy_mod_transform2d") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.19") (o #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.19") (d #t) (k 2)))) (h "13ybl78m753459g6c0yk6l2xi4kdyv03nisggwj8dz282zraqi7j") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-bevy_mod_transform2d-0.4.0 (c (n "bevy_mod_transform2d") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.21") (o #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.21.0") (d #t) (k 2)))) (h "1fpfmdyncbswdf5vj68aqk9k9gbhzk83fcn6issmpd3sicskba5m") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-bevy_mod_transform2d-0.4.1 (c (n "bevy_mod_transform2d") (v "0.4.1") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.21") (o #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.21.0") (d #t) (k 2)))) (h "1w7axjkjiqshpa185g8w0p9yjyrl763lymsyi1nqfm9a15s7nzch") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-bevy_mod_transform2d-0.5.0 (c (n "bevy_mod_transform2d") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.22") (o #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.22") (d #t) (k 2)) (d (n "bevy_xpbd_2d") (r "^0.2") (o #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.2") (d #t) (k 2)))) (h "1yzbkv8azgk8bx9qyidggi0jyhkxsliikzfih3qlrmmlyvbpsnr8") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-bevy_mod_transform2d-0.5.1 (c (n "bevy_mod_transform2d") (v "0.5.1") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.22") (o #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.22") (d #t) (k 2)) (d (n "bevy_xpbd_2d") (r "^0.2") (o #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.2") (d #t) (k 2)))) (h "14fyvgga7vshw62xxlljxv0rw5jk15d5q077927ycqzz36crgwa9") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-bevy_mod_transform2d-0.6.0 (c (n "bevy_mod_transform2d") (v "0.6.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.23") (o #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.23") (d #t) (k 2)) (d (n "bevy_xpbd_2d") (r "^0.3") (o #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.3") (d #t) (k 2)))) (h "117jqhxr6y0ly1qd86ss92fl0rig1vrizpkx8lm3jfgcbf800aqp") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-bevy_mod_transform2d-0.7.0 (c (n "bevy_mod_transform2d") (v "0.7.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_math") (r "^0.13") (f (quote ("approx"))) (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.25") (o #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.25") (d #t) (k 2)) (d (n "bevy_xpbd_2d") (r "^0.4") (o #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.4") (d #t) (k 2)))) (h "10w6w042sig89gjxpjfwqlwhkipn0inp5gkibblyd7wp0j52qjxs") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

