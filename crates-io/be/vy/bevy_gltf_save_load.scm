(define-module (crates-io be vy bevy_gltf_save_load) #:use-module (crates-io))

(define-public crate-bevy_gltf_save_load-0.1.0 (c (n "bevy_gltf_save_load") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "bevy_gltf_blueprints") (r "^0.6") (d #t) (k 0)))) (h "1r3g4pbg64dslpcssxm09m02rrvbqmw9lzn1x8hxm1b4z4clb54f")))

(define-public crate-bevy_gltf_save_load-0.2.0 (c (n "bevy_gltf_save_load") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "bevy_gltf_blueprints") (r "^0.7") (d #t) (k 0)))) (h "0llsyrv2md5vnxaqqm2bdrzrn8j3qgxfx3xgflh493gcbfqwmkbg")))

(define-public crate-bevy_gltf_save_load-0.2.1 (c (n "bevy_gltf_save_load") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "bevy_gltf_blueprints") (r "^0.7") (d #t) (k 0)))) (h "04hjssbiy9j6xcxv5dzbpm3d4jv1a2xscvnvksiq06cpgx4bql8d")))

(define-public crate-bevy_gltf_save_load-0.3.0 (c (n "bevy_gltf_save_load") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "bevy_gltf_blueprints") (r "^0.8") (d #t) (k 0)))) (h "0nl4cip2bbwkflwgyiplbnhl6sph5sgnqr8m2808wbwrv6y04p91")))

(define-public crate-bevy_gltf_save_load-0.4.0 (c (n "bevy_gltf_save_load") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("dynamic_linking"))) (k 2)) (d (n "bevy_gltf_blueprints") (r "^0.9") (d #t) (k 0)))) (h "1car79yrrbi1lhrqzffk680hnwhav05gsg0vgmsxc5s1w20xcscw")))

(define-public crate-bevy_gltf_save_load-0.4.1 (c (n "bevy_gltf_save_load") (v "0.4.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("dynamic_linking"))) (k 2)) (d (n "bevy_gltf_blueprints") (r "^0.10") (d #t) (k 0)))) (h "1kj3q1x8cmvcs8xf62lfwaak1f767wx5y6qdll8yzx3knhm4w70i")))

