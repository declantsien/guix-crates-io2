(define-module (crates-io be vy bevy_wind_waker_shader) #:use-module (crates-io))

(define-public crate-bevy_wind_waker_shader-0.1.0 (c (n "bevy_wind_waker_shader") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("png" "bevy_asset" "bevy_pbr" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("multi-threaded" "file_watcher"))) (d #t) (k 2)))) (h "0dax8by039xaqlk3apj6jrv0aspibis64bz1v3mm18isnj5pq31x")))

(define-public crate-bevy_wind_waker_shader-0.1.1 (c (n "bevy_wind_waker_shader") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("png" "bevy_asset" "bevy_pbr" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("multi-threaded" "file_watcher"))) (d #t) (k 2)))) (h "0cywdr82dyazqpf1l888zndspdmbgpk7jz7wx7h5pvk52gmn5bma")))

(define-public crate-bevy_wind_waker_shader-0.1.2 (c (n "bevy_wind_waker_shader") (v "0.1.2") (d (list (d (n "bevy") (r "^0.13") (f (quote ("png" "bevy_asset" "bevy_pbr" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("multi-threaded" "file_watcher"))) (d #t) (k 2)))) (h "0jk6jhhg1z3k58l8ykrfl3vpcfcg7425s73smd31wyjbdmv36n5g")))

