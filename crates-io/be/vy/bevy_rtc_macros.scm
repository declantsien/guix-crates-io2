(define-module (crates-io be vy bevy_rtc_macros) #:use-module (crates-io))

(define-public crate-bevy_rtc_macros-0.1.0 (c (n "bevy_rtc_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1k9vjh0vnha8rfqkcwvmahr3v9543vj3q5iafy1pvrg4mb5z67wn")))

(define-public crate-bevy_rtc_macros-0.1.1 (c (n "bevy_rtc_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1x0bx9a8w7dwrp67whqkshgiw2acnvmjcznzpygsfslalkqpvbl1")))

(define-public crate-bevy_rtc_macros-0.2.0 (c (n "bevy_rtc_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0cynp0sab7d06swdw9frxkppz623qg5204n7n8l4jynrw8vlfzm4")))

(define-public crate-bevy_rtc_macros-0.3.0 (c (n "bevy_rtc_macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "123pkjdnf8d2l6xfxnzimyg6v7w29m07qc8s6fp86ad2h95vwkid")))

(define-public crate-bevy_rtc_macros-0.3.1 (c (n "bevy_rtc_macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "183261hijgabgd57lyy4mxcm3jlg4aivix2szn3nbhh4z0ix6qkc")))

