(define-module (crates-io be vy bevy-pigeon) #:use-module (crates-io))

(define-public crate-bevy-pigeon-0.3.0 (c (n "bevy-pigeon") (v "0.3.0") (d (list (d (n "bevy") (r "~0.7.0") (k 0)) (d (n "bevy") (r "~0.7.0") (d #t) (k 2)) (d (n "carrier-pigeon") (r "~0.3") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ymxya14w4kap9nbrykcg4fnhxipzdhs8dh3r726b58f6ij769pd") (f (quote (("types" "bevy/render") ("default" "types"))))))

