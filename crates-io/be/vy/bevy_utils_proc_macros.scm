(define-module (crates-io be vy bevy_utils_proc_macros) #:use-module (crates-io))

(define-public crate-bevy_utils_proc_macros-0.10.0 (c (n "bevy_utils_proc_macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0blaai7ll7h0595adhpgfhx1y77syij5pxjvgb2h0s24bi32x932")))

(define-public crate-bevy_utils_proc_macros-0.10.1 (c (n "bevy_utils_proc_macros") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ln0837z7y643gvkjqh93sq5iy8am5ivzzflqwbpkkd55zir42v3")))

(define-public crate-bevy_utils_proc_macros-0.11.0 (c (n "bevy_utils_proc_macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00algpwb6q8amvab8nxkbm4s5wiw7lqir1qrvicszs5hhplg4dwy")))

(define-public crate-bevy_utils_proc_macros-0.11.1 (c (n "bevy_utils_proc_macros") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0zsbjz46mwz7nk8rwhijzp3p5n8z3rq8xp77k1g4b8g9cjq0n90y")))

(define-public crate-bevy_utils_proc_macros-0.11.2 (c (n "bevy_utils_proc_macros") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0h9ansym99fhr26apsd2qlrmqhnj84ilwfcb7rq3q4iiw8lly40d")))

(define-public crate-bevy_utils_proc_macros-0.11.3 (c (n "bevy_utils_proc_macros") (v "0.11.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0myvpnrjrljmnz98yslbckfsjfn8613l94aq3nq6smbgqd1b54ak")))

(define-public crate-bevy_utils_proc_macros-0.12.0 (c (n "bevy_utils_proc_macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03hp5mmfffgbx5qxh6p50fxinwxf9vz6l5qyxn0vd8b1b9rx7pzp")))

(define-public crate-bevy_utils_proc_macros-0.12.1 (c (n "bevy_utils_proc_macros") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1zygpigc7hyzvw0kl0z198pxy9bxhsyr09f1jcdfpf5nab4yrbvs")))

(define-public crate-bevy_utils_proc_macros-0.13.0 (c (n "bevy_utils_proc_macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "16l58635ysbshschnmzxsjc9nx8z409xfb72j1f0z2y0q3lribii")))

(define-public crate-bevy_utils_proc_macros-0.13.1 (c (n "bevy_utils_proc_macros") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "19xdk2l9qjncfk0a6pyyyld5r4bna36n6bb05qd840gdcvs80k01")))

(define-public crate-bevy_utils_proc_macros-0.13.2 (c (n "bevy_utils_proc_macros") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0qsilk4piq7fdspyw5im6j6nk7w28js6036231f3sl1hgxi5iwdy")))

