(define-module (crates-io be vy bevy_ios_iap) #:use-module (crates-io))

(define-public crate-bevy_ios_iap-0.1.0 (c (n "bevy_ios_iap") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_crossbeam_event") (r "^0.5") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_ecs_macros") (r "^0.13") (k 0)) (d (n "bevy_log") (r "^0.13") (k 0)) (d (n "swift-bridge") (r "^0.1") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1") (d #t) (k 1)))) (h "07gvyvxhmmk3c02cvmcbij20yhxwhvylq5f9xczrzhx4133zri64")))

(define-public crate-bevy_ios_iap-0.1.1 (c (n "bevy_ios_iap") (v "0.1.1") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_crossbeam_event") (r "^0.5") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_ecs_macros") (r "^0.13") (k 0)) (d (n "bevy_log") (r "^0.13") (k 0)) (d (n "swift-bridge") (r "^0.1") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1") (d #t) (k 1)))) (h "17c07r2l9gvac2skp77aq0wz20pdn2yhbzmbskj2amh4i5iz4lal")))

(define-public crate-bevy_ios_iap-0.1.2 (c (n "bevy_ios_iap") (v "0.1.2") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_crossbeam_event") (r "^0.5") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_ecs_macros") (r "^0.13") (k 0)) (d (n "bevy_log") (r "^0.13") (k 0)) (d (n "swift-bridge") (r "^0.1") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1") (d #t) (k 1)))) (h "0h724l1hmaqcdcmway77525nykfllm3pq29cjvxpjwfc6mdfg4mn")))

(define-public crate-bevy_ios_iap-0.2.0 (c (n "bevy_ios_iap") (v "0.2.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_crossbeam_event") (r "^0.5") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_ecs_macros") (r "^0.13") (k 0)) (d (n "bevy_log") (r "^0.13") (k 0)) (d (n "swift-bridge") (r "^0.1") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1") (d #t) (k 1)))) (h "089kzkdz6awi630847yg3zz6gsscbp779a1n9vgwxcysnb4wll9d")))

(define-public crate-bevy_ios_iap-0.2.1 (c (n "bevy_ios_iap") (v "0.2.1") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_crossbeam_event") (r "^0.5") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_ecs_macros") (r "^0.13") (k 0)) (d (n "bevy_log") (r "^0.13") (k 0)) (d (n "swift-bridge") (r "^0.1") (d #t) (k 0)) (d (n "swift-bridge-build") (r "^0.1") (d #t) (k 1)))) (h "157g1yfawy7l95ynznah3ykr8608plgwprzc82hjl68w09200b86")))

