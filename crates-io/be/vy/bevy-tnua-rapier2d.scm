(define-module (crates-io be vy bevy-tnua-rapier2d) #:use-module (crates-io))

(define-public crate-bevy-tnua-rapier2d-0.1.0 (c (n "bevy-tnua-rapier2d") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.23") (f (quote ("dim2"))) (k 0)))) (h "134ncqkdhlirfm1a522s9iszf2hf0cdw7chkcsrshkyakrvpdq29")))

(define-public crate-bevy-tnua-rapier2d-0.1.1 (c (n "bevy-tnua-rapier2d") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.23") (f (quote ("dim2"))) (k 0)))) (h "0iid1jarzkabvfw57k7npf9avx545pfw0sgr734gz1iscqjznzwv")))

(define-public crate-bevy-tnua-rapier2d-0.2.0 (c (n "bevy-tnua-rapier2d") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.24") (f (quote ("dim2"))) (k 0)))) (h "04wdfv6jpfdzchcz5gb99ypcxg1lm5s4n1dypv2262mrlnr6zsnz")))

(define-public crate-bevy-tnua-rapier2d-0.3.0 (c (n "bevy-tnua-rapier2d") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.2") (d #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.25") (f (quote ("dim2"))) (k 0)))) (h "1f8hy0ssa9ff2ay883hmlpjlzbg1rj54l5y2gfl5jn4c3xa65gqr")))

(define-public crate-bevy-tnua-rapier2d-0.4.0 (c (n "bevy-tnua-rapier2d") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.25") (f (quote ("dim2"))) (k 0)))) (h "0plppa4621wwi9lyh94zhhp2q02cwrb1wwpc7cxch586wmqf9y83")))

(define-public crate-bevy-tnua-rapier2d-0.5.0 (c (n "bevy-tnua-rapier2d") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.25") (f (quote ("dim2"))) (k 0)))) (h "077zj7jjazaa5nrfj4v32w60jaj3xf2a6x696k97ic2jw4gng3aj")))

(define-public crate-bevy-tnua-rapier2d-0.6.0 (c (n "bevy-tnua-rapier2d") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_rapier2d") (r "^0.26") (f (quote ("dim2"))) (k 0)))) (h "1vcqksnc3g7ah9ykkzmy7nkidk0f72f6kmjmv5vghlxrcarisa3b")))

