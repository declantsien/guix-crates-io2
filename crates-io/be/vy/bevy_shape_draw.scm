(define-module (crates-io be vy bevy_shape_draw) #:use-module (crates-io))

(define-public crate-bevy_shape_draw-0.1.0 (c (n "bevy_shape_draw") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "bevy_input") (r "^0.9") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "bevy_mod_raycast") (r "^0.7") (d #t) (k 0)))) (h "0hjcwghj4n9xrq6r3s5n3bxns1jxx0nci5d95jyz8pwmk5yfqk33")))

(define-public crate-bevy_shape_draw-0.1.1 (c (n "bevy_shape_draw") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "bevy_input") (r "^0.9") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "bevy_mod_raycast") (r "^0.7") (d #t) (k 0)))) (h "0rdg6fw7klirwsrprfg0vlp4fn7rsy6ssy19iwbix6ki1989fc6i")))

(define-public crate-bevy_shape_draw-0.1.2 (c (n "bevy_shape_draw") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_input") (r "^0.9") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "bevy_mod_raycast") (r "^0.7") (d #t) (k 0)))) (h "0vdc13iwzqy7xalziiks44ijp63qbcdsa00fsbjzd8ykmp7y1ija")))

