(define-module (crates-io be vy bevy_gltf_components) #:use-module (crates-io))

(define-public crate-bevy_gltf_components-0.1.0 (c (n "bevy_gltf_components") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.11.2") (d #t) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1jylny06pgpi46wj5bn6nq2xgwx9p0njyk8hypv37s1n2hhqi30n")))

(define-public crate-bevy_gltf_components-0.1.1 (c (n "bevy_gltf_components") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.11.2") (d #t) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "04rgl8r3z8ysyqs0vi5y0v2kdqsambwh81pc68xgbjn9avspc0k8")))

(define-public crate-bevy_gltf_components-0.1.2 (c (n "bevy_gltf_components") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1bi69pzfasmf3jxixqy47r1xl24bv9406vxmwpxmrv3m1cjs0fcy")))

(define-public crate-bevy_gltf_components-0.1.3 (c (n "bevy_gltf_components") (v "0.1.3") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "19nr3qw8rkk2hymmj5lq1qjnr5jv6k3akqn68gkbhd9yzhd9j0an")))

(define-public crate-bevy_gltf_components-0.2.0 (c (n "bevy_gltf_components") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0ync0z3cnzyal6mcxpm1pgxk99ih996ihbg10ij2hcf6i0lmlx6d")))

(define-public crate-bevy_gltf_components-0.2.1 (c (n "bevy_gltf_components") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1d620ic7fmjxm0lcwhvxi0lrwn6f89jmk39rc75444p13i0r0j1z")))

(define-public crate-bevy_gltf_components-0.2.2 (c (n "bevy_gltf_components") (v "0.2.2") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1f8i8x1fbz3x00bw1lv7h21p81qqf9zkq6i50wpl24gqhhklnw4h")))

(define-public crate-bevy_gltf_components-0.3.0 (c (n "bevy_gltf_components") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1ghi69zipsxlbqnvvmwr8ai23hrd59vr50frmg3r1hfwahzcbn5w")))

(define-public crate-bevy_gltf_components-0.3.1 (c (n "bevy_gltf_components") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "18x1zr4n7zkl6m7gj3d8gbg8hsbpk4ydr3pf83nmb3m6ldyvilb3")))

(define-public crate-bevy_gltf_components-0.3.2 (c (n "bevy_gltf_components") (v "0.3.2") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1a9jxlx7vvg1m6mi1w7jkzqx6zq9jn28cqs55czs29cq6qc2r2hz")))

(define-public crate-bevy_gltf_components-0.4.0 (c (n "bevy_gltf_components") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "094r76b2i9fs7jj1wl9vpdx5hnqyfkly2wzszflgfyy37hmhnr95")))

(define-public crate-bevy_gltf_components-0.5.0 (c (n "bevy_gltf_components") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("dynamic_linking"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1v5756qpscqz99963y3dj1d6qc3kzv7nrx05dvvf240ybyn6fvm5")))

(define-public crate-bevy_gltf_components-0.5.1 (c (n "bevy_gltf_components") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_scene" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("dynamic_linking"))) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "123bggd55nwb8579m661849ky594xl2gai1gyh1v04d1w3833bp7")))

