(define-module (crates-io be vy bevy_aseprite_reader) #:use-module (crates-io))

(define-public crate-bevy_aseprite_reader-0.1.0 (c (n "bevy_aseprite_reader") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "0h68gzw4v841shyf7hlia3r6cwc37j10x4d3mcgvw9053nbsj5jv")))

(define-public crate-bevy_aseprite_reader-0.1.1 (c (n "bevy_aseprite_reader") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1yza1xmll99fvihsk4r4pvdl714i7j77d1qgx2j5gxhdn6ds1v5s")))

