(define-module (crates-io be vy bevy_console_derive) #:use-module (crates-io))

(define-public crate-bevy_console_derive-0.3.0 (c (n "bevy_console_derive") (v "0.3.0") (d (list (d (n "better-bae") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w4bx1hyjchmxpnq8kl7pc5fmfrk91dwgcpa8z05m3qbxzxdkx60")))

(define-public crate-bevy_console_derive-0.4.0 (c (n "bevy_console_derive") (v "0.4.0") (d (list (d (n "better-bae") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vj9vwzz77q9bmhhmjl2jw9d0m33kxbpg9idialn9gdjyf2s7qpv")))

(define-public crate-bevy_console_derive-0.5.0 (c (n "bevy_console_derive") (v "0.5.0") (d (list (d (n "better-bae") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07pfky4bsffrlgj6lnj9x17hl696fnmgcrq3yzzk0p3pc8cmxz4v")))

