(define-module (crates-io be vy bevy_mod_billboard) #:use-module (crates-io))

(define-public crate-bevy_mod_billboard-0.1.0 (c (n "bevy_mod_billboard") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_text" "bevy_sprite"))) (k 0)))) (h "0lyvp6hqrkrdpd9blzw3mg18fwxrcbnsf0l85acgplw4ja2s3d2l")))

(define-public crate-bevy_mod_billboard-0.1.1 (c (n "bevy_mod_billboard") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9.1") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_text" "bevy_sprite" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "1f7v91nhalk999hk135jhnw1f6m786iib8ffd3b09krvaajsl9a2")))

(define-public crate-bevy_mod_billboard-0.2.0 (c (n "bevy_mod_billboard") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_text" "bevy_sprite" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.10.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0wk7glzbmmzgad2nvh2wma3ag8blhx2vy8lr2ad9iwdz2dsb0wq0")))

(define-public crate-bevy_mod_billboard-0.2.1 (c (n "bevy_mod_billboard") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_text" "bevy_sprite" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.10.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "02y3xx5746lnd3ws27v7438xd0wz7dm1qnxcivadk4i2ylbjhxni")))

(define-public crate-bevy_mod_billboard-0.3.0 (c (n "bevy_mod_billboard") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_text" "bevy_sprite" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.10.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "1i2xcssmxdfy9fd8xc61q5w1vaayg5qlqk5mr7s2ax150axmxqzi")))

(define-public crate-bevy_mod_billboard-0.4.0 (c (n "bevy_mod_billboard") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_text" "bevy_sprite" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.11.0") (d #t) (k 2)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)))) (h "08dfpycv6bqmg5jjrdpi4apbdj5fi5ys6r84yn3dqx7idxyxv00n")))

(define-public crate-bevy_mod_billboard-0.4.1 (c (n "bevy_mod_billboard") (v "0.4.1") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_text" "bevy_sprite" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.11.0") (d #t) (k 2)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)))) (h "0nz6yj3mwyr9idck7mh35wp7a05zny4mbkzmyslfcgnbazbx4q8l")))

(define-public crate-bevy_mod_billboard-0.5.0 (c (n "bevy_mod_billboard") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_text" "bevy_sprite" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 2)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)))) (h "0flglyhsivsrwp4n245wrwbzp8anx9vq6bf8jsv2lpylg8php6r9")))

(define-public crate-bevy_mod_billboard-0.5.1 (c (n "bevy_mod_billboard") (v "0.5.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_text" "bevy_sprite" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 2)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)))) (h "0kcq7bhfg7vb2nxkg02qrs2wfly1w4vlk6nf70ghxsf9mm4sa7k6")))

