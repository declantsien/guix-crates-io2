(define-module (crates-io be vy bevy_crt) #:use-module (crates-io))

(define-public crate-bevy_crt-0.1.0 (c (n "bevy_crt") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)))) (h "0g19ryj5c9bajgkvq9l7xcs5qs48zqxv3pzk280c3i297mxlsla8")))

(define-public crate-bevy_crt-0.1.1 (c (n "bevy_crt") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)))) (h "0cxbl53sr1gjp1rjykhl00pa45p1rsw9vhdnp1v7702ncy2027jv")))

(define-public crate-bevy_crt-0.1.2 (c (n "bevy_crt") (v "0.1.2") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)))) (h "0i9pq3mxmrk8rlvyi0qpb077ys7xb93vx1gk42s6dj6s61daacfs")))

(define-public crate-bevy_crt-0.1.3 (c (n "bevy_crt") (v "0.1.3") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)))) (h "0akpi7mz9irbfkw29zcmhd2qddz8dqncrqlvs4r5dc4rhnrgiy9g")))

