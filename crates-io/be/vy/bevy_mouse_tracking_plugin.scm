(define-module (crates-io be vy bevy_mouse_tracking_plugin) #:use-module (crates-io))

(define-public crate-bevy_mouse_tracking_plugin-0.1.0 (c (n "bevy_mouse_tracking_plugin") (v "0.1.0") (d (list (d (n "bevy") (r "0.4.*") (f (quote ("render"))) (k 0)))) (h "1v20r9aha3ys2f8s14bzbyry4km8ql3gsqshp9xg849swp3i2y2j")))

(define-public crate-bevy_mouse_tracking_plugin-0.1.1 (c (n "bevy_mouse_tracking_plugin") (v "0.1.1") (d (list (d (n "bevy") (r "0.4.*") (f (quote ("render"))) (k 0)))) (h "05dsw9w0m1wk8sw8b366vcdf9klwambhy1lw7sksz0jjh095pfb7")))

(define-public crate-bevy_mouse_tracking_plugin-0.2.0 (c (n "bevy_mouse_tracking_plugin") (v "0.2.0") (d (list (d (n "bevy") (r "0.6.*") (f (quote ("render"))) (k 0)))) (h "1fmgq3bj3s82901gjr1jsczxcv4vql82apah9vww12kf6ndq7d5c")))

(define-public crate-bevy_mouse_tracking_plugin-0.2.1 (c (n "bevy_mouse_tracking_plugin") (v "0.2.1") (d (list (d (n "bevy") (r "0.7.*") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "0.7.*") (f (quote ("render" "png" "bevy_winit"))) (k 2)))) (h "1mrpz67n2xrwg8xpnyfqxwzn24rqads1kfswjxarpnnqgg58c4si")))

(define-public crate-bevy_mouse_tracking_plugin-0.3.0 (c (n "bevy_mouse_tracking_plugin") (v "0.3.0") (d (list (d (n "bevy") (r "0.8.*") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "0.8.*") (f (quote ("render" "png" "bevy_asset" "bevy_winit"))) (k 2)))) (h "0hf7z1z0nc986i4wcwfi92458jh4n58gyg2bj9hpd8bz9higf883")))

(define-public crate-bevy_mouse_tracking_plugin-0.3.1 (c (n "bevy_mouse_tracking_plugin") (v "0.3.1") (d (list (d (n "bevy") (r "0.8.*") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "0.8.*") (f (quote ("render" "png" "bevy_asset" "bevy_winit"))) (k 2)))) (h "0wm6p3pij058mgm2abnzdkvp9w23b5w3kwqkqi2dj4r1w0dg5vb0")))

(define-public crate-bevy_mouse_tracking_plugin-0.4.0 (c (n "bevy_mouse_tracking_plugin") (v "0.4.0") (d (list (d (n "bevy") (r "0.8.*") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "0.8.*") (f (quote ("render" "png" "bevy_asset" "bevy_winit"))) (k 2)))) (h "1vlm0hdsh2hw46q77xfxrb0xhp295xzcg7ifcb4g9k49c3zlikgd")))

(define-public crate-bevy_mouse_tracking_plugin-0.5.0 (c (n "bevy_mouse_tracking_plugin") (v "0.5.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "png" "bevy_asset" "bevy_winit"))) (k 2)))) (h "1vih15nihr9fxgm8j2hld6amsmw1hbrlgc3rysns83h7zc6b799z")))

(define-public crate-bevy_mouse_tracking_plugin-0.5.1 (c (n "bevy_mouse_tracking_plugin") (v "0.5.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "png" "bevy_asset" "bevy_winit"))) (k 2)))) (h "01ccbk9c58h24y9wd3y5hff22q6prvsqqkgr1dhp5mr3imb6xan4")))

(define-public crate-bevy_mouse_tracking_plugin-0.5.2 (c (n "bevy_mouse_tracking_plugin") (v "0.5.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "png" "bevy_asset" "bevy_winit"))) (k 2)))) (h "1lxks8ngkwyn8h86splapn3l0r7ag3ajaxk6d0ww0h32ywhjm43q")))

(define-public crate-bevy_mouse_tracking_plugin-0.5.3 (c (n "bevy_mouse_tracking_plugin") (v "0.5.3") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "png" "bevy_asset" "bevy_winit"))) (k 2)))) (h "1n055babkmn2fkxnipy6dzh19k0vdwn84zchm3sjrbcrcr1pr79r")))

(define-public crate-bevy_mouse_tracking_plugin-0.6.0 (c (n "bevy_mouse_tracking_plugin") (v "0.6.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite" "bevy_text" "png" "bevy_asset" "bevy_winit" "x11"))) (k 2)))) (h "1hcv7v1kkbdcmabw3jhy1m499v01g4qgsm1cfg9sjqixhmw9kdjp")))

(define-public crate-bevy_mouse_tracking_plugin-0.7.0 (c (n "bevy_mouse_tracking_plugin") (v "0.7.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite" "bevy_text" "png" "bevy_asset" "bevy_winit" "x11"))) (k 2)))) (h "175plalsa45klv469sv76zq9p8svy7alhwdd52v1zf3xkigpb5wy")))

