(define-module (crates-io be vy bevy_bundletree_derive) #:use-module (crates-io))

(define-public crate-bevy_bundletree_derive-0.1.1 (c (n "bevy_bundletree_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qh8i00g1xr1n4vy92l6ckdr2w4viiic0pqmpbk4pp2psb53zr6m")))

