(define-module (crates-io be vy bevy-basic-ui) #:use-module (crates-io))

(define-public crate-bevy-basic-ui-0.3.1 (c (n "bevy-basic-ui") (v "0.3.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "1a5mqsrhwxhv4j07ix97i96kfm8bmw4xmyiyk5nvrrz6v8r6byrd")))

(define-public crate-bevy-basic-ui-0.3.2 (c (n "bevy-basic-ui") (v "0.3.2") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "0w0srxniawrv66yflbkjn10558bf3k80icznkk585yw877m435i8")))

(define-public crate-bevy-basic-ui-0.3.3 (c (n "bevy-basic-ui") (v "0.3.3") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "035znam784h1j0xx8pjzpbj8rxkm7yssgx63sqhhilf9jrwj0iz7")))

(define-public crate-bevy-basic-ui-0.3.4 (c (n "bevy-basic-ui") (v "0.3.4") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "1mrj8v9sma4nik0kckwk6ihqqxwk6027mz8mcz8lgpd1sq8kvx8x")))

(define-public crate-bevy-basic-ui-0.3.5 (c (n "bevy-basic-ui") (v "0.3.5") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_scene" "bevy_winit" "bevy_core_pipeline" "bevy_pbr" "bevy_text" "bevy_ui" "multi-threaded" "png" "vorbis" "x11" "default_font"))) (k 0)))) (h "1w0w9vrvlgz24kz4yslc5nx3g632dc3d6r9hfh4n9vsqsv1p6myx")))

(define-public crate-bevy-basic-ui-0.3.6 (c (n "bevy-basic-ui") (v "0.3.6") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_core_pipeline" "bevy_text" "bevy_ui" "png" "vorbis" "x11" "default_font"))) (k 0)))) (h "0a716yy8vhcn6yh39il4c43xy83fms8iw9wbhxskzrc61g7abqks")))

(define-public crate-bevy-basic-ui-0.3.7 (c (n "bevy-basic-ui") (v "0.3.7") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_core_pipeline" "bevy_text" "bevy_ui" "png" "vorbis" "x11" "default_font"))) (k 0)))) (h "1l02gnc3pqlfaigwjv4nafmf12ligb8cksb4kmfrgjyn2y5z6sz2")))

(define-public crate-bevy-basic-ui-0.3.8 (c (n "bevy-basic-ui") (v "0.3.8") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_core_pipeline" "bevy_text" "bevy_ui" "png" "vorbis" "x11" "default_font"))) (k 0)) (d (n "bevy-ui-dsl") (r "^0.8.0") (d #t) (k 0)))) (h "1rpf8pkrm15p71iy5qszqlmd90zgg35kanipj1bp5b41xqlg3ai0")))

(define-public crate-bevy-basic-ui-0.4.0 (c (n "bevy-basic-ui") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_core_pipeline" "bevy_text" "bevy_ui" "png" "vorbis" "x11" "default_font"))) (k 0)) (d (n "bevy_third_person_camera") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0hl5m6saa9kvpgn7vv4dy8wrrls7sjfks3kfxf144wlhnnw6404n")))

(define-public crate-bevy-basic-ui-0.4.1 (c (n "bevy-basic-ui") (v "0.4.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_core_pipeline" "bevy_text" "bevy_ui" "png" "vorbis" "x11" "default_font"))) (k 0)) (d (n "bevy_third_person_camera") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0f3y6rcxbh6hmllfr66hrg24v6nfa694hn38x8xx753s1f6dpmbn")))

(define-public crate-bevy-basic-ui-0.4.2 (c (n "bevy-basic-ui") (v "0.4.2") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_core_pipeline" "bevy_text" "bevy_ui" "png" "vorbis" "x11"))) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "1fz398halvpirai55q16yrq3jngbi29v1a7yjprvkmrf630c4rgk")))

(define-public crate-bevy-basic-ui-0.4.4 (c (n "bevy-basic-ui") (v "0.4.4") (d (list (d (n "bevy") (r "^0.13.2") (d #t) (k 0)))) (h "0p9xk99gdsbwdmmnyxb32qp7vwyqh0kbxgrkbhm4labclamm5w5x")))

(define-public crate-bevy-basic-ui-0.4.5 (c (n "bevy-basic-ui") (v "0.4.5") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("multi-threaded" "bevy_asset" "bevy_scene" "bevy_winit" "bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_gltf" "bevy_text" "bevy_ui" "png" "x11" "android_shared_stdcxx" "webgl2"))) (k 0)))) (h "1wh06mimzn33d5dn6hpn7aamzji4i0qnyp54cvdljlqp3b89g85j")))

(define-public crate-bevy-basic-ui-0.4.6 (c (n "bevy-basic-ui") (v "0.4.6") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("dynamic_linking" "multi-threaded" "bevy_asset" "bevy_scene" "bevy_winit" "bevy_render" "bevy_pbr" "bevy_core_pipeline" "tonemapping_luts" "bevy_text" "bevy_ui" "png" "x11"))) (k 0)) (d (n "bevy_egui") (r "^0.27.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.18.2") (d #t) (k 0)))) (h "024vnh20d5ksl64fmkgrb2i5v0dzgbhr6gv4qsfp451sby9a5xzq")))

(define-public crate-bevy-basic-ui-0.4.7 (c (n "bevy-basic-ui") (v "0.4.7") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("dynamic_linking" "multi-threaded" "bevy_asset" "bevy_scene" "bevy_winit" "bevy_render" "bevy_pbr" "bevy_core_pipeline" "tonemapping_luts" "bevy_text" "bevy_ui" "asset_processor" "png" "x11"))) (k 0)) (d (n "bevy_common_assets") (r "^0.10.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "bevy_egui") (r "^0.27.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.18.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nixn86kapwgsxyi9d1p9pm36mnrgykhw7bp6m68y473kdlq6pky")))

(define-public crate-bevy-basic-ui-0.4.8 (c (n "bevy-basic-ui") (v "0.4.8") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("dynamic_linking" "multi-threaded" "bevy_asset" "bevy_scene" "bevy_winit" "bevy_render" "bevy_pbr" "bevy_core_pipeline" "tonemapping_luts" "bevy_text" "bevy_ui" "asset_processor" "png" "x11"))) (k 0)) (d (n "bevy_common_assets") (r "^0.10.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "bevy_egui") (r "^0.27.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.18.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wfpcir06g37f6s7x06fb3132xfbn748ilpy4jwmhaxfn3fg7f8m")))

(define-public crate-bevy-basic-ui-0.4.9 (c (n "bevy-basic-ui") (v "0.4.9") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("dynamic_linking" "multi-threaded" "bevy_asset" "bevy_scene" "bevy_winit" "bevy_render" "bevy_pbr" "bevy_core_pipeline" "tonemapping_luts" "bevy_text" "bevy_ui" "asset_processor" "png" "x11"))) (k 0)) (d (n "bevy_common_assets") (r "^0.10.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "bevy_egui") (r "^0.27.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.18.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rqf80094zx5zi5ff03l152sq0w1zgwlxgavxav5cnvq0wpp35za")))

(define-public crate-bevy-basic-ui-0.4.10 (c (n "bevy-basic-ui") (v "0.4.10") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("dynamic_linking" "multi-threaded" "bevy_asset" "bevy_scene" "bevy_winit" "bevy_render" "bevy_pbr" "bevy_core_pipeline" "tonemapping_luts" "bevy_text" "bevy_ui" "asset_processor" "png" "x11"))) (k 0)) (d (n "bevy_common_assets") (r "^0.10.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.18.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lm5aka1jcyms1xzbpjhjw1vd81mjs6kp5w658i1n5qa2sqzipqs")))

(define-public crate-bevy-basic-ui-0.4.12 (c (n "bevy-basic-ui") (v "0.4.12") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("dynamic_linking" "multi-threaded" "bevy_asset" "bevy_scene" "bevy_winit" "bevy_render" "bevy_pbr" "bevy_core_pipeline" "tonemapping_luts" "bevy_text" "bevy_ui" "png" "x11"))) (k 0)) (d (n "bevy_common_assets") (r "^0.10.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.18.2") (f (quote ("backend_bevy_ui" "backend_sprite" "selection"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mkcx5j48gpjxk39mdnwbq7fl5y0gs9fap4x5bcj85spks8khqbl")))

