(define-module (crates-io be vy bevy_2d_screen_space_lightmaps) #:use-module (crates-io))

(define-public crate-bevy_2d_screen_space_lightmaps-0.13.0 (c (n "bevy_2d_screen_space_lightmaps") (v "0.13.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_winit" "bevy_asset" "bevy_sprite" "bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_winit" "bevy_asset" "bevy_sprite" "bevy_ui" "png"))) (k 2)))) (h "0d3a56v0076pylfj2gsxjinwp3dbdkn4wqm9qq20n0rwz6nhz695")))

(define-public crate-bevy_2d_screen_space_lightmaps-0.13.1 (c (n "bevy_2d_screen_space_lightmaps") (v "0.13.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_winit" "bevy_asset" "bevy_sprite" "bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_winit" "bevy_asset" "bevy_sprite" "bevy_ui" "png"))) (k 2)))) (h "1zwmr0243mrga9khgigvyqpzj25pb7sggyw2mwm50nni9dg5bina")))

