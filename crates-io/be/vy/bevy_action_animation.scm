(define-module (crates-io be vy bevy_action_animation) #:use-module (crates-io))

(define-public crate-bevy_action_animation-0.1.0 (c (n "bevy_action_animation") (v "0.1.0") (d (list (d (n "bevy_action") (r "^0.1") (d #t) (k 0)) (d (n "bevy_app") (r "^0.10") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.10") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.10") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.10") (d #t) (k 0)) (d (n "bevy_sprite") (r "^0.10") (d #t) (k 0)) (d (n "bevy_time") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "0gbj4d2r9g4rd8ccmf8xgicf8dib0lvrfkagkxyd84vpiicmv2na")))

