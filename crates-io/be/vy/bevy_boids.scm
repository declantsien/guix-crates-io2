(define-module (crates-io be vy bevy_boids) #:use-module (crates-io))

(define-public crate-bevy_boids-0.1.0 (c (n "bevy_boids") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (k 0)) (d (n "bevy_rapier3d") (r "^0.20.0") (d #t) (k 0)))) (h "0sah0v4jk9xndcn9712w1k7q629js8k8n53l81q4da0mqk5p3jqs") (f (quote (("dev" "bevy/dynamic"))))))

(define-public crate-bevy_boids-0.2.0 (c (n "bevy_boids") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.1") (k 0)))) (h "0q2xgbck45npkz22fxg8kd323jr695gd1jz1s8xgp95ywk2fnf6b") (f (quote (("reflect"))))))

(define-public crate-bevy_boids-0.3.0 (c (n "bevy_boids") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9.1") (k 0)))) (h "1bwwwdkjmm4b8kaagxdmshhsnx2dpnp6y5b913bmf09rmkflxv44") (f (quote (("reflect"))))))

