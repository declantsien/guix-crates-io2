(define-module (crates-io be vy bevy_descendant_collector) #:use-module (crates-io))

(define-public crate-bevy_descendant_collector-0.1.0 (c (n "bevy_descendant_collector") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.23.2") (d #t) (k 2)) (d (n "bevy_asset_loader") (r "^0.20.0") (d #t) (k 2)) (d (n "bevy_descendant_collector_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0m89sjyqka3rvpgzrkgf9c2jawpn629xk31i48xy8b6sara7dck3") (f (quote (("dev" "bevy/dynamic_linking"))))))

(define-public crate-bevy_descendant_collector-0.1.1 (c (n "bevy_descendant_collector") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.24.0") (d #t) (k 2)) (d (n "bevy_asset_loader") (r "^0.20.1") (d #t) (k 2)) (d (n "bevy_descendant_collector_derive") (r "^0.1.1") (d #t) (k 0)))) (h "19lcw2k5pg8b9bib0ybgmw5z6shv4184i2ah4w0rci97m989awzi") (f (quote (("dev" "bevy/dynamic_linking"))))))

