(define-module (crates-io be vy bevy_editor_iris_common) #:use-module (crates-io))

(define-public crate-bevy_editor_iris_common-0.1.0 (c (n "bevy_editor_iris_common") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_editor_iris_derive") (r "^0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "quinn") (r "^0.8.3") (d #t) (k 0)) (d (n "rcgen") (r "^0.9.2") (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (d #t) (k 0)))) (h "1ni7vvjg0y50b4pmnxfzk241m00i5ss9dbs1j9xq8xhvvxb7nr69")))

