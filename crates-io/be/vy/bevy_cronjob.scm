(define-module (crates-io be vy bevy_cronjob) #:use-module (crates-io))

(define-public crate-bevy_cronjob-0.1.0 (c (n "bevy_cronjob") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (k 2)) (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "1ijf84h2jkliiadawxjk0g9lb85a2fdgp673lxccm1ab7p9y9d67")))

(define-public crate-bevy_cronjob-0.1.1 (c (n "bevy_cronjob") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.0") (k 2)) (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "1hyxlf8y5k38s5mrx0ys2jvv3h3342hlidkn99836yy9xj0ml7mc")))

(define-public crate-bevy_cronjob-0.1.2 (c (n "bevy_cronjob") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11.0") (k 2)) (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "0mxdkp0c9hmva1cdryjcdiadrar8ah92yfvh3rmfdhn79674chcb")))

(define-public crate-bevy_cronjob-0.2.0 (c (n "bevy_cronjob") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (k 2)) (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "0wqnp3v093pj7y0fnns2wsb689y08kn9q52wqp84gr8qmzsv7bgi")))

(define-public crate-bevy_cronjob-0.3.0 (c (n "bevy_cronjob") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.0") (k 2)) (d (n "bevy_ecs") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "0wms8i392jrdk5922s6l1694v17zf195v25pi0mrh2kh572hb9dx")))

