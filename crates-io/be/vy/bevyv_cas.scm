(define-module (crates-io be vy bevyv_cas) #:use-module (crates-io))

(define-public crate-bevyv_cas-0.0.1 (c (n "bevyv_cas") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset"))) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "hex") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_scene" "bevy_winit" "jpeg" "x11"))) (k 2)))) (h "1xx4zpar7c69vwa5mcpxny05wm3f5n24c3kwjljfw2jlwg8yyhxj") (y #t)))

