(define-module (crates-io be vy bevy_sparse_grid_2d) #:use-module (crates-io))

(define-public crate-bevy_sparse_grid_2d-0.1.0 (c (n "bevy_sparse_grid_2d") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "0mizb5kz1a9f3iysn97qvzbban950dij2gv59712sz7y582k6r3f")))

(define-public crate-bevy_sparse_grid_2d-0.2.0 (c (n "bevy_sparse_grid_2d") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "13n7y85yivf6s62937xkgkz08mqchdds54nvn688gqcpxy5264a0")))

(define-public crate-bevy_sparse_grid_2d-0.3.0 (c (n "bevy_sparse_grid_2d") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "0nl6lhqd0vnc4abmdkdf0g60mnwl59qh71sdipsmklc6pdkdcals")))

(define-public crate-bevy_sparse_grid_2d-0.3.1 (c (n "bevy_sparse_grid_2d") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "1f1xcypkmy853bb9jzyalyb1nf5ci6xhwqalj82xshxwx8vm4yzk")))

