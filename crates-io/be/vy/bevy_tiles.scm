(define-module (crates-io be vy bevy_tiles) #:use-module (crates-io))

(define-public crate-bevy_tiles-0.0.0-dev1 (c (n "bevy_tiles") (v "0.0.0-dev1") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1i86qa7hx7kadh92s11zrxs27awmp8fc18mn9yxbnclw7kxxclr2")))

(define-public crate-bevy_tiles-0.0.0-dev2 (c (n "bevy_tiles") (v "0.0.0-dev2") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0p3r7s72xi51h2kal3b84aiskb38d3c3129qj57gg5ig660x2f8c")))

(define-public crate-bevy_tiles-0.1.0 (c (n "bevy_tiles") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0bicb76bmlgi9r4v2z47xcx82s37xwbhb54z7shy50ccq81gby7w")))

