(define-module (crates-io be vy bevy_doryen) #:use-module (crates-io))

(define-public crate-bevy_doryen-0.1.0 (c (n "bevy_doryen") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.4.0") (d #t) (k 0)) (d (n "doryen-rs") (r "^1.2.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 2)))) (h "18rsjrbidi99w4676nqbcpzsiirz1b0sx6na7q9b3d2fn77lhpng")))

(define-public crate-bevy_doryen-0.1.1 (c (n "bevy_doryen") (v "0.1.1") (d (list (d (n "bevy_app") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.4.0") (d #t) (k 0)) (d (n "doryen-rs") (r "^1.2.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 2)))) (h "1z3wib7b1ynbdzmcbjqa0q3ygkdsypisx799rhpgchdakm8r2d4r")))

(define-public crate-bevy_doryen-0.2.0 (c (n "bevy_doryen") (v "0.2.0") (d (list (d (n "bevy_app") (r "^0.5") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.5") (d #t) (k 0)) (d (n "doryen-rs") (r "^1.2.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 2)))) (h "1sn92wjk2yfs94najv1kysvisn5k5dq9wz8mw07rd5vjv3qc8n3c")))

(define-public crate-bevy_doryen-0.3.0 (c (n "bevy_doryen") (v "0.3.0") (d (list (d (n "bevy_app") (r "^0.11") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11") (d #t) (k 0)) (d (n "doryen-rs") (r "^1.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 2)))) (h "0zz79bpbswszjrh7kblxqql705j011akh5h16ji7yyby5d00wdip")))

(define-public crate-bevy_doryen-0.4.0 (c (n "bevy_doryen") (v "0.4.0") (d (list (d (n "bevy_app") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12.1") (d #t) (k 0)) (d (n "doryen-fov") (r "^0.1") (d #t) (k 2)) (d (n "doryen-rs") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 2)))) (h "0yyg5m0vb6shhhq5763rah5vbhgik2h81yjw0dj661nxjkjbwbbx") (y #t)))

(define-public crate-bevy_doryen-0.4.1 (c (n "bevy_doryen") (v "0.4.1") (d (list (d (n "bevy_app") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12.1") (d #t) (k 0)) (d (n "doryen-fov") (r "^0.1") (d #t) (k 2)) (d (n "doryen-rs") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 2)))) (h "03l6z2lvff9w4cy0lziri9ql4d5irswiq64rkmi1radkkcwchxxl")))

(define-public crate-bevy_doryen-0.5.0 (c (n "bevy_doryen") (v "0.5.0") (d (list (d (n "bevy_app") (r "^0.13") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.13") (d #t) (k 0)) (d (n "doryen-fov") (r "^0.1") (d #t) (k 2)) (d (n "doryen-rs") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 2)))) (h "096rf48n3x4x9d5d3zz9vnrkp7xc859fk7r0inj1l6wxicpvk43y")))

