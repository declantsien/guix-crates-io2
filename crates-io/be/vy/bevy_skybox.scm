(define-module (crates-io be vy bevy_skybox) #:use-module (crates-io))

(define-public crate-bevy_skybox-0.1.0 (c (n "bevy_skybox") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "bevy_fly_camera") (r "^0.6") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1hgnw1v05pf7axmza79q7kk72w980z411fipcfybgsyxh13n1whd")))

(define-public crate-bevy_skybox-0.2.0 (c (n "bevy_skybox") (v "0.2.0") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "bevy_fly_camera") (r "^0.6") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0y1cwnrj96axaq1blpnp8zrf0lzm7k40jpvprhsclclsy6ql5dwf")))

(define-public crate-bevy_skybox-0.3.0 (c (n "bevy_skybox") (v "0.3.0") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "bevy_fly_camera") (r "^0.6") (d #t) (k 2)) (d (n "bevy_log") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "12d358yk3mhxkcl2f4sh6dypw0sjj9yh4cwli5r8a9cjq3way9fm")))

(define-public crate-bevy_skybox-0.4.0 (c (n "bevy_skybox") (v "0.4.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fly_camera") (r "^0.7") (d #t) (k 2)) (d (n "bevy_log") (r "^0.5") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "02qg5xzx7diysr8yrfiaznf4f2l7dyrdvadkhhfb7dzyyms8mhhw")))

(define-public crate-bevy_skybox-0.5.0 (c (n "bevy_skybox") (v "0.5.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_fly_camera") (r "^0.7") (d #t) (k 2)) (d (n "bevy_log") (r "^0.5") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1pslzgd2zw9cmd1i7zd31k32nqv8hq3332b7z76nj0npxk21ry4m")))

