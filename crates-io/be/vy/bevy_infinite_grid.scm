(define-module (crates-io be vy bevy_infinite_grid) #:use-module (crates-io))

(define-public crate-bevy_infinite_grid-0.1.0 (c (n "bevy_infinite_grid") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.7") (d #t) (k 2)))) (h "01fahbj24amkyckmv97zi48cal42q0pvm051zqbnxdf1jh580ijh")))

(define-public crate-bevy_infinite_grid-0.2.0 (c (n "bevy_infinite_grid") (v "0.2.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.7") (d #t) (k 2)))) (h "0gc4f06k49alqwg79fh4x3phn0lgg4akirarj4d6d6sxpyccb92l")))

(define-public crate-bevy_infinite_grid-0.2.1 (c (n "bevy_infinite_grid") (v "0.2.1") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.7") (d #t) (k 2)))) (h "0d0z1pwjnxmcv4fgvxpp5y4f7jncmhrb0sw78im428lav6xk5iak")))

(define-public crate-bevy_infinite_grid-0.3.0 (c (n "bevy_infinite_grid") (v "0.3.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.7") (d #t) (k 2)))) (h "0v93ivqnrrihvyi6p3jxz1gkxmwjkr17k12mhvk6rwy7cnmhka79")))

(define-public crate-bevy_infinite_grid-0.3.1 (c (n "bevy_infinite_grid") (v "0.3.1") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.7") (d #t) (k 2)))) (h "1cr8c7cg86dvqpy8i3sl6nxzvrnfpsyanzdj66f498ac840k45yx")))

(define-public crate-bevy_infinite_grid-0.4.0 (c (n "bevy_infinite_grid") (v "0.4.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.7") (d #t) (k 2)))) (h "1hwlqbxsc63fzxhdwcqbcr7c6iihwg2s10k13rjy752czzg28alf")))

(define-public crate-bevy_infinite_grid-0.5.0 (c (n "bevy_infinite_grid") (v "0.5.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.8") (d #t) (k 2)))) (h "0qjm9dfk0bqnrgsw46x2rd4lh38rnvnjr6q7d8gbvkfng1k3q5qh")))

(define-public crate-bevy_infinite_grid-0.5.1 (c (n "bevy_infinite_grid") (v "0.5.1") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.8") (d #t) (k 2)))) (h "0kwvj5ichcbmbr1mzggdvp76vvj00qmblv440c0ycc4q9gcv2dwm")))

(define-public crate-bevy_infinite_grid-0.5.2 (c (n "bevy_infinite_grid") (v "0.5.2") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.8") (d #t) (k 2)))) (h "18lkiivbch8j86lifyn3vdgrvhdknhfgz1y7mrng0mwqqhvr90lw")))

(define-public crate-bevy_infinite_grid-0.6.0 (c (n "bevy_infinite_grid") (v "0.6.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_winit" "x11"))) (k 2)) (d (n "bevy_flycam") (r "^0.9") (d #t) (k 2)))) (h "1myy3lrfpdrda9s7w900bmjbi39ryfsg6dd5fsaiydaibwncijzx")))

(define-public crate-bevy_infinite_grid-0.7.0 (c (n "bevy_infinite_grid") (v "0.7.0") (d (list (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_winit" "x11"))) (k 2)) (d (n "bevy_flycam") (r "^0.10.0") (d #t) (k 2)))) (h "0mz4i6l77h625wvk6lv4vvr6l48hicjkfv3czvmd4c6pbqc9kx5l")))

(define-public crate-bevy_infinite_grid-0.8.0 (c (n "bevy_infinite_grid") (v "0.8.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_winit" "x11" "tonemapping_luts" "ktx2" "zstd"))) (k 2)))) (h "1xjbnjhw049bjcq77ra1p2ag4k1simfpa82mc00z6gi7vkmz4yhg")))

(define-public crate-bevy_infinite_grid-0.8.1 (c (n "bevy_infinite_grid") (v "0.8.1") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_winit" "x11" "tonemapping_luts" "ktx2" "zstd"))) (k 2)))) (h "0l22rqqj6cdwhdkwmrssfqfs9vw5cp5q2p6f7dy84kx5ipspkywa")))

(define-public crate-bevy_infinite_grid-0.9.0 (c (n "bevy_infinite_grid") (v "0.9.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_winit" "x11" "tonemapping_luts" "ktx2" "zstd"))) (k 2)))) (h "0qpz1g43ls3bdgljk2fsa13csxb965mk45js4jqlaz07d4dxjm1d")))

(define-public crate-bevy_infinite_grid-0.10.0 (c (n "bevy_infinite_grid") (v "0.10.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_winit" "x11" "tonemapping_luts" "ktx2" "zstd"))) (k 2)))) (h "1n7jdvf3h0kpxlk1m2l5xw4n2flpxw56c3c8h33jia5d7h38j7jc") (y #t)))

(define-public crate-bevy_infinite_grid-0.11.0 (c (n "bevy_infinite_grid") (v "0.11.0") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_winit" "x11" "tonemapping_luts" "ktx2" "zstd"))) (k 2)))) (h "05c6v7246273s2hj1kkbnk0ggp8fmjbrm5ijapclv7p2gjhn51k3")))

(define-public crate-bevy_infinite_grid-0.12.0 (c (n "bevy_infinite_grid") (v "0.12.0") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_winit" "x11" "tonemapping_luts" "ktx2" "zstd"))) (k 2)))) (h "14ni0cpr5paa6aq7wrr8n60ici4yrnbvkfz3zky300q67f6rn70m")))

(define-public crate-bevy_infinite_grid-0.10.1 (c (n "bevy_infinite_grid") (v "0.10.1") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_winit" "x11" "tonemapping_luts" "ktx2" "zstd"))) (k 2)))) (h "0nmhrkz88ssa9q2959r4mbnz3rpshvvkl9979nsvgyzk0b7r49zh")))

