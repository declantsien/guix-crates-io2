(define-module (crates-io be vy bevy-notify) #:use-module (crates-io))

(define-public crate-bevy-notify-0.1.0 (c (n "bevy-notify") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.16") (d #t) (k 0)) (d (n "egui-notify") (r "^0.4") (d #t) (k 0)))) (h "1lkgf8p59138g07ry42y3741dch7hgn27rkjrm559qpd24yk8paf")))

(define-public crate-bevy-notify-0.2.0 (c (n "bevy-notify") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.17") (d #t) (k 0)) (d (n "egui-notify") (r "^0.4") (d #t) (k 0)))) (h "11mp9722mc15gcqgg0r5vy6106d7l28lv7n9y1xvnrzayxbgf0yd")))

