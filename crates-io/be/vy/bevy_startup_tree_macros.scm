(define-module (crates-io be vy bevy_startup_tree_macros) #:use-module (crates-io))

(define-public crate-bevy_startup_tree_macros-0.1.0 (c (n "bevy_startup_tree_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nygpjpwzap7sr0vz50ibfm0kqn3nl1vx7ai4xm9zq7qz6rcq8k9") (y #t)))

(define-public crate-bevy_startup_tree_macros-0.1.2 (c (n "bevy_startup_tree_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h30qnzxr5nms5fbq7c146lra8p81lk5xp0pay966y6pgl35k4s0")))

(define-public crate-bevy_startup_tree_macros-0.1.3 (c (n "bevy_startup_tree_macros") (v "0.1.3") (d (list (d (n "bevy_startup_tree_macros_core") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mmijnckif357qc2qjg4qmgs433k5jrq4cr85cqj4kl027nngdk9")))

(define-public crate-bevy_startup_tree_macros-0.2.0 (c (n "bevy_startup_tree_macros") (v "0.2.0") (d (list (d (n "bevy_startup_tree_macros_core") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "047m7cilwwyapb4ifj1n6z4j9bc5z6jxc62r02c9kzpmrkpsc2ac")))

(define-public crate-bevy_startup_tree_macros-0.3.0 (c (n "bevy_startup_tree_macros") (v "0.3.0") (d (list (d (n "bevy_startup_tree_macros_core") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "01qqi8khkck22zj2vkcrzyl6vckzxjlpzqgskldq82lxwddpcllx")))

(define-public crate-bevy_startup_tree_macros-0.4.0 (c (n "bevy_startup_tree_macros") (v "0.4.0") (d (list (d (n "bevy_startup_tree_macros_core") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1isfr1hy9yixfdfnqx983ijrj684msba3bb9pbql5j5bc9wgfm7j")))

(define-public crate-bevy_startup_tree_macros-0.4.1 (c (n "bevy_startup_tree_macros") (v "0.4.1") (d (list (d (n "bevy_startup_tree_macros_core") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0bh3r7cihrcahs922z3an43a3sg5h468aazmhfmvv7b1v5gz9vs2")))

