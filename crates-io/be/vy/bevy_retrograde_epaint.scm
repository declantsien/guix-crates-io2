(define-module (crates-io be vy bevy_retrograde_epaint) #:use-module (crates-io))

(define-public crate-bevy_retrograde_epaint-0.0.0 (c (n "bevy_retrograde_epaint") (v "0.0.0") (h "104gf2gl1qmr824l27r91hj5gvwfy7qjvbc3rffff82qxslqacar")))

(define-public crate-bevy_retrograde_epaint-0.2.0 (c (n "bevy_retrograde_epaint") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("bevy_winit"))) (k 0)) (d (n "bevy_retrograde_core") (r "^0.2") (d #t) (k 0)) (d (n "bevy_retrograde_macros") (r "^0.2") (d #t) (k 0)) (d (n "epaint") (r "^0.13.0") (d #t) (k 0)))) (h "0a2j17qq8xxfycqqs11j44kf0pai1s8gvxxs5qy7wr8i96f5m3al")))

