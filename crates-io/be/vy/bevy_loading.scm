(define-module (crates-io be vy bevy_loading) #:use-module (crates-io))

(define-public crate-bevy_loading-0.1.0 (c (n "bevy_loading") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (d #t) (k 2)))) (h "1gwx42gfvcxcc4bzphwlpnampf9zhhxyk33l5b31m29az4r4w73q")))

(define-public crate-bevy_loading-0.1.1 (c (n "bevy_loading") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (d #t) (k 2)))) (h "041fpz42i3qishckddyn93v86jy0k396rxcx09aqrcy22rgfh62z")))

(define-public crate-bevy_loading-0.1.2 (c (n "bevy_loading") (v "0.1.2") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (d #t) (k 2)))) (h "1zxf27yqvracfbp4sgfsgdiwm28yh1dhmdrrlh4kv6icm7gy4hhn")))

(define-public crate-bevy_loading-0.2.0 (c (n "bevy_loading") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy") (r "^0.6.0") (d #t) (k 2)))) (h "13pp0z3rwbid7dv5yr93i6npwlmrzplk2hlaqpc61c8qwzfi08ls")))

(define-public crate-bevy_loading-0.3.0 (c (n "bevy_loading") (v "0.3.0") (h "1apclbbcc7xx2ij0q3k2k4c1jwxffwf8d3mq053yszq7akk6597v")))

