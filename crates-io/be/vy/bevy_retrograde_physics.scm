(define-module (crates-io be vy bevy_retrograde_physics) #:use-module (crates-io))

(define-public crate-bevy_retrograde_physics-0.0.0 (c (n "bevy_retrograde_physics") (v "0.0.0") (h "0ih075dcvm6940myyw9x2vvgsyyzmpavws4d2l1kngsr7ypld307")))

(define-public crate-bevy_retrograde_physics-0.2.0 (c (n "bevy_retrograde_physics") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy_retrograde_core") (r "^0.2") (d #t) (k 0)) (d (n "density-mesh-core") (r "^1.5.0") (d #t) (k 0)) (d (n "density-mesh-image") (r "^1.5.0") (d #t) (k 0)) (d (n "heron") (r "^0.10.1") (f (quote ("2d"))) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "17fgkq2fikwq7c1ac3a27fadha58f29dkf9y0hxdndzzr4fi0mfc") (f (quote (("default"))))))

(define-public crate-bevy_retrograde_physics-0.2.1 (c (n "bevy_retrograde_physics") (v "0.2.1") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy_retrograde_core") (r "^0.2") (d #t) (k 0)) (d (n "density-mesh-core") (r "^1.5.0") (d #t) (k 0)) (d (n "density-mesh-image") (r "^1.5.0") (d #t) (k 0)) (d (n "heron") (r "^0.11.0") (f (quote ("2d"))) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "13g7x2iglbwga5wxrvzm0is5njfafm6xic30l7n14fydl7x263z1") (f (quote (("default"))))))

