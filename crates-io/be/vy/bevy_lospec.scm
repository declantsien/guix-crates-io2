(define-module (crates-io be vy bevy_lospec) #:use-module (crates-io))

(define-public crate-bevy_lospec-0.1.0 (c (n "bevy_lospec") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wnrlgkqsjwnwi1ifjdnba5333v2i844f0abf1qjphknfx0zxgfr")))

(define-public crate-bevy_lospec-0.1.1 (c (n "bevy_lospec") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "167imcyyxvs3cn1r4sizpww2bgh1283gf4bpmcddh7hg34zlp7c6")))

(define-public crate-bevy_lospec-0.2.0 (c (n "bevy_lospec") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09bsswh3akni1asad6jg9k10nb1yymjg6p819xb2pfwr4skxjsw4")))

(define-public crate-bevy_lospec-0.3.0 (c (n "bevy_lospec") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j8qah89n4fy21b6nw39nyvxawkqi60mxvawhwmwix8vsqkyz47s")))

(define-public crate-bevy_lospec-0.4.0 (c (n "bevy_lospec") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_asset"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "047vls6v19hfwvfih5kq5ik1y44zp4dhgl8jjn3wa3dpk5bs2d0y")))

(define-public crate-bevy_lospec-0.5.0 (c (n "bevy_lospec") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_asset"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xpd44hnb7nnffjs68vrgipgrv319ipcayrakhvvsqbn005x25k5")))

(define-public crate-bevy_lospec-0.6.0 (c (n "bevy_lospec") (v "0.6.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_sprite" "bevy_winit" "x11" "webgl2"))) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03f40pghvcyipq04jsr3f0r7sr1ybvpc23r8gnihc99cz9y3gqw3")))

(define-public crate-bevy_lospec-0.7.0 (c (n "bevy_lospec") (v "0.7.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_sprite" "bevy_winit" "x11" "webgl2"))) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ykf3igvx0pawa5w5s62sa0npx992jcax71aqgmk05ach9nr2cf8")))

