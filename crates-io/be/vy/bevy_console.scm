(define-module (crates-io be vy bevy_console) #:use-module (crates-io))

(define-public crate-bevy_console-0.0.1 (c (n "bevy_console") (v "0.0.1") (h "1h3hl0x1f40lqjgj9n67f4dqw53hvyf0qg1dm387ssrlxw1d1d0r") (y #t)))

(define-public crate-bevy_console-0.1.0 (c (n "bevy_console") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.4.0") (d #t) (k 0)))) (h "129yxkmjr39dn530pfs52a329jb2qpkbl5mjicaw6j8512hhnwxq")))

(define-public crate-bevy_console-0.1.1 (c (n "bevy_console") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.4.0") (d #t) (k 0)))) (h "02msra0kmi7cr2xa47fisi5y5p3ds4fb083dl7q8c4zqrm54yc79")))

(define-public crate-bevy_console-0.2.0 (c (n "bevy_console") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy_egui") (r "^0.4.0") (d #t) (k 0)))) (h "0ncrckmlr987ng9w6prh3x647gaj4rh41ny31g58cg2znsc4x2i3") (f (quote (("examples" "bevy/bevy_gltf" "bevy/bevy_winit" "bevy/render" "bevy/png" "bevy/bevy_wgpu"))))))

(define-public crate-bevy_console-0.2.1 (c (n "bevy_console") (v "0.2.1") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy_egui") (r "^0.5") (d #t) (k 0)))) (h "01ic4z91av3nmrqif65wip3k57d667bjsz7p8shsqcj5i9z2zc5b") (f (quote (("examples" "bevy/bevy_gltf" "bevy/bevy_winit" "bevy/render" "bevy/png" "bevy/bevy_wgpu"))))))

(define-public crate-bevy_console-0.2.2 (c (n "bevy_console") (v "0.2.2") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy_egui") (r "^0.7.0") (d #t) (k 0)))) (h "0mj1wxdqf05fq7lkpyc5253wix92w0ph8l0cr85604w7rpj5mjn1") (f (quote (("examples" "bevy/bevy_gltf" "bevy/bevy_winit" "bevy/render" "bevy/png" "bevy/bevy_wgpu"))))))

(define-public crate-bevy_console-0.2.3 (c (n "bevy_console") (v "0.2.3") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy_egui") (r "^0") (d #t) (k 0)))) (h "1h6pc6w0yv1ixvks1iy633s47n3rivzyv1qj9q2836i1a9cpqq8m") (f (quote (("examples" "bevy/bevy_gltf" "bevy/bevy_winit" "bevy/render" "bevy/png" "bevy/bevy_wgpu"))))))

(define-public crate-bevy_console-0.2.4 (c (n "bevy_console") (v "0.2.4") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy_egui") (r "^0") (d #t) (k 0)))) (h "11aa4zhf8pvqy162c24nfnf9yl7pk51csppypqs5k6kvi526h7a8") (f (quote (("examples" "bevy/bevy_gltf" "bevy/bevy_winit" "bevy/render" "bevy/png"))))))

(define-public crate-bevy_console-0.2.5 (c (n "bevy_console") (v "0.2.5") (d (list (d (n "bevy") (r "^0.6.1") (k 0)) (d (n "bevy_egui") (r "^0") (d #t) (k 0)))) (h "0z7nfrrx22wwm161m96954xs9ni7lg1197zdghf3hn6yl5cl64ch") (f (quote (("examples" "bevy/bevy_gltf" "bevy/bevy_winit" "bevy/render" "bevy/png"))))))

(define-public crate-bevy_console-0.3.0 (c (n "bevy_console") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_console_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.11") (d #t) (k 0)))) (h "0g47pwspv2hvz4cfikw9d29b4v74xiaysxfn7zbibs0yyzd4jinx")))

(define-public crate-bevy_console-0.3.1 (c (n "bevy_console") (v "0.3.1") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "bevy") (r "^0.7.0") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_console_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.13") (d #t) (k 0)))) (h "0x6wlkdhdx944gjyd45l5d30fj7m5nvwziavdxfprd6p3lpganb5")))

(define-public crate-bevy_console-0.4.0 (c (n "bevy_console") (v "0.4.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_console_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.15.0") (d #t) (k 0)))) (h "155s99grk9havciy8jq80v4dvrg22wbkwigbmj78jjzqnlnslcaw")))

(define-public crate-bevy_console-0.5.0 (c (n "bevy_console") (v "0.5.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_console_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.17") (d #t) (k 0)))) (h "115f0hz1m8lhg5hkwy3lnvj26asz9vq6ang248k45qgyjjn4rp79")))

(define-public crate-bevy_console-0.6.0 (c (n "bevy_console") (v "0.6.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.17") (d #t) (k 0)) (d (n "clap") (r "=4.0.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q43i073xirvmhryifz2d4rd4znvrqg86sqvpywilpf9bkcncci7")))

(define-public crate-bevy_console-0.7.0 (c (n "bevy_console") (v "0.7.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.20.1") (d #t) (k 0)) (d (n "clap") (r "=4.1.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "11zfkv1aypm7pdkkij2d1b2lg1iyyv4xszb1haf6n1xgqwr8d48n")))

(define-public crate-bevy_console-0.8.0 (c (n "bevy_console") (v "0.8.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.21") (d #t) (k 0)) (d (n "clap") (r "=4.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shlex") (r "^1.1") (d #t) (k 0)))) (h "131dpvzans42jjkbcxjhgfxrhcj4i5xn7v6sch73vvpk6dvcz38j")))

(define-public crate-bevy_console-0.9.0 (c (n "bevy_console") (v "0.9.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.22") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shlex") (r "^1.1") (d #t) (k 0)))) (h "0lqsbs9qcwbwr0dslcrkmyrwxhvsb0iz28n743f5a3znc81n72mz")))

(define-public crate-bevy_console-0.10.0 (c (n "bevy_console") (v "0.10.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.23") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shlex") (r "^1.1") (d #t) (k 0)))) (h "081rdg5xa73j8yr8w5y2z433x9pwia5p6nq12r6sy02hnzify4vy")))

(define-public crate-bevy_console-0.10.1 (c (n "bevy_console") (v "0.10.1") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.24") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shlex") (r "^1.1") (d #t) (k 0)))) (h "16ad5d89pxhigllbnimrg9yczqsvfdr2sjxa0c7p6vgkiaghrb8k")))

(define-public crate-bevy_console-0.11.1 (c (n "bevy_console") (v "0.11.1") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_console_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.25") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shlex") (r "^1.3") (d #t) (k 0)))) (h "1zl2n1h8jk2xn2hyrlhn2zdx88hx4rwy7n4n0241ikjjdcvdf4gn")))

