(define-module (crates-io be vy bevy_retro_core) #:use-module (crates-io))

(define-public crate-bevy_retro_core-0.1.0 (c (n "bevy_retro_core") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1c0hfvd00pcsf9hmp0idv7h3fhxm19aaci2ibync3ljm9mbsvvpz")))

