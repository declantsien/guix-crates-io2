(define-module (crates-io be vy bevy_proto_derive) #:use-module (crates-io))

(define-public crate-bevy_proto_derive-0.1.0 (c (n "bevy_proto_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1014c7c2whfhkm32xy89wlmnysw9cx0a0wvdaqxl1xl3l9q5iifh")))

(define-public crate-bevy_proto_derive-0.2.0 (c (n "bevy_proto_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xakvzpckv3a74czjcmr1m9spvh3w1z82mx0kjpkskq3i6s8khyv")))

(define-public crate-bevy_proto_derive-0.3.0 (c (n "bevy_proto_derive") (v "0.3.0") (d (list (d (n "optional-error") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05m0pcli4dk8p0r2vi9xk9chnq2n0gbrr75qvdzqifl4q2dz80ab") (f (quote (("default" "assertions") ("assertions")))) (y #t)))

(define-public crate-bevy_proto_derive-0.3.1 (c (n "bevy_proto_derive") (v "0.3.1") (d (list (d (n "optional-error") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ilhn06pqqzhhpz8sw34r89flscbl82wji202qqwhf91fhqr3vy8") (f (quote (("default" "assertions") ("assertions"))))))

(define-public crate-bevy_proto_derive-0.4.0 (c (n "bevy_proto_derive") (v "0.4.0") (d (list (d (n "optional-error") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "to_phantom") (r "^0.1") (d #t) (k 0)))) (h "0hj1f14znid2higwzkq0z8wlrcc0arva83cq1piw2xx0lwr8hv8p") (f (quote (("default" "assertions") ("assertions"))))))

(define-public crate-bevy_proto_derive-0.5.0 (c (n "bevy_proto_derive") (v "0.5.0") (d (list (d (n "optional-error") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "to_phantom") (r "^0.1") (d #t) (k 0)))) (h "0bwka5i40d5fwpw8rv1f80p051rfz5n30hfjb5rdiq3qqps2mdmn") (f (quote (("default" "assertions") ("assertions"))))))

(define-public crate-bevy_proto_derive-0.6.0 (c (n "bevy_proto_derive") (v "0.6.0") (d (list (d (n "optional-error") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "to_phantom") (r "^0.1") (d #t) (k 0)))) (h "10gyzj5avcschyv9xj29a6yl2i248gdk6cwqysbjzqd2m90xmwhy") (f (quote (("default" "assertions") ("assertions"))))))

