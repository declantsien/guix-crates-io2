(define-module (crates-io be vy bevy_replicon_quinnet) #:use-module (crates-io))

(define-public crate-bevy_replicon_quinnet-0.1.0 (c (n "bevy_replicon_quinnet") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_text" "bevy_ui" "bevy_gizmos" "x11" "default_font"))) (k 2)) (d (n "bevy_quinnet") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_replicon") (r "^0.24") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "09qdw7n7hriss7m63808snwcm937k4cgbyiij6ccyq1l5j96c60f")))

(define-public crate-bevy_replicon_quinnet-0.2.0 (c (n "bevy_replicon_quinnet") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_text" "bevy_ui" "bevy_gizmos" "x11" "default_font"))) (k 2)) (d (n "bevy_quinnet") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_replicon") (r "^0.25") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "0zik0qwi00b1ggw8k5l4v6xgnqbijy4w116jgjlps6qy9qrh6yim")))

(define-public crate-bevy_replicon_quinnet-0.3.0 (c (n "bevy_replicon_quinnet") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_text" "bevy_ui" "bevy_gizmos" "x11" "default_font"))) (k 2)) (d (n "bevy_quinnet") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_replicon") (r "^0.26") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "06khnq2iid9zylwkci5pz3b1x2dn1z9p1afxlfcfliackbhmqh68")))

