(define-module (crates-io be vy bevy_retrograde_text) #:use-module (crates-io))

(define-public crate-bevy_retrograde_text-0.0.0 (c (n "bevy_retrograde_text") (v "0.0.0") (h "1hgw4czf4687nyqfbphnyjigpky0abjgq48h3zrm4c0kkyqgbqh1")))

(define-public crate-bevy_retrograde_text-0.1.0 (c (n "bevy_retrograde_text") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy_retrograde_core") (r "^0.1") (d #t) (k 0)) (d (n "bevy_retrograde_macros") (r "^0.1") (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-linebreak") (r "^0.1.1") (d #t) (k 0)))) (h "1mxhwm5qg1l2l0d2s27fgc6y56wr5ihna15vlk5hd9bk4zza4d96")))

(define-public crate-bevy_retrograde_text-0.2.0 (c (n "bevy_retrograde_text") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy_retrograde_core") (r "^0.2") (d #t) (k 0)) (d (n "bevy_retrograde_macros") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-linebreak") (r "^0.1.1") (d #t) (k 0)))) (h "1m9r8fmm90xmv8p9a1byyvkh182i5knlqkd94f28s53axcw6w1i2")))

