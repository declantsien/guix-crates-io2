(define-module (crates-io be vy bevy_text_animation) #:use-module (crates-io))

(define-public crate-bevy_text_animation-0.1.0 (c (n "bevy_text_animation") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "06b438f9csskq99k3icb4ssvm2hf959lnkd0bzbmf1xr5pic4kzy") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.1 (c (n "bevy_text_animation") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "0ks6ymjp1w4sf4lq58qzhpvm8wdgz01dx6hasw5arnk8ik8mvws6") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.2 (c (n "bevy_text_animation") (v "0.1.2") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "1yc0n87w5rwhg615k6664ajbiydazrxm3bsy19jxca5za2bq4j8w") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.3 (c (n "bevy_text_animation") (v "0.1.3") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "1wiqads24kmwvd8q3k1w3jl4d77wlgbylrndb9mgl2hh5nm6j7l4") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.4 (c (n "bevy_text_animation") (v "0.1.4") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "0fc8bxxq9j9xyyk18ss4wj5kdbszi4jc816phszv6d718xwfaw5d") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.5 (c (n "bevy_text_animation") (v "0.1.5") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "1g2yr0zki59jbnhqd26rnmrcb1x313ayh5fjyd7348bj8lj77k7r") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.6 (c (n "bevy_text_animation") (v "0.1.6") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "18iraaczwxib232p3wsy8x786d19zlrvs0gq9a7g49wfl2d2rl52") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.7 (c (n "bevy_text_animation") (v "0.1.7") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "1d8r8rdc5abkpl6kqc8prmssxwbxqf50l9v51i34mf9n88q48cqc") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.8 (c (n "bevy_text_animation") (v "0.1.8") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "0gp5n3r72x1p94k6shg8mpv8cavr40nzxccdamndp8yq4xdlg2cp") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.9 (c (n "bevy_text_animation") (v "0.1.9") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "0fw698q50cicg5p2nbfzp9hy1c0aajp3prqfgcfpjrsan9rf1f25") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.10 (c (n "bevy_text_animation") (v "0.1.10") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "1lzsj50y15givjpbvs2p5b2xbcdnhq68rjiibsbaq1blygg7g1bc") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.11 (c (n "bevy_text_animation") (v "0.1.11") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "17mnp0jjbv24fpf7cj1gzpriw0sizwm8dwvajz7yqnnnlslpwgks") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.12 (c (n "bevy_text_animation") (v "0.1.12") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "1nsmlw4dbxbh1ik7x44sdxhd5wgjxf2q3awlbm9qyzy71gjhsg6r") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.13 (c (n "bevy_text_animation") (v "0.1.13") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "1v4hqwcgsjl66pgv05n5197f1jyc2lvdblwb9sza9j81rjxdnanf") (f (quote (("default"))))))

(define-public crate-bevy_text_animation-0.1.14 (c (n "bevy_text_animation") (v "0.1.14") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_winit" "bevy_render" "bevy_text" "bevy_ui" "default_font"))) (k 2)) (d (n "utf8_slice") (r "^1.0.0") (d #t) (k 0)))) (h "0yfjnnw3c3np9zg6n2b8nass2vcfrn9yjg9qr7bmvd3qw0ww7sj7") (f (quote (("default"))))))

