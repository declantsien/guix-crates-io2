(define-module (crates-io be vy bevy_erm_derive) #:use-module (crates-io))

(define-public crate-bevy_erm_derive-0.0.1 (c (n "bevy_erm_derive") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.78") (d #t) (k 0)) (d (n "bevy_erm_core") (r "^0.0.1") (d #t) (k 0)) (d (n "casey") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "0y7w2brk908k1xsz443n5x90sabg8ycp76z6kqb9sz7d9i7a059y")))

