(define-module (crates-io be vy bevy_pipelines_ready) #:use-module (crates-io))

(define-public crate-bevy_pipelines_ready-0.1.0 (c (n "bevy_pipelines_ready") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)))) (h "1s3660kbr2q5hb2n5b7pphnisqdcg8nl28v0ib00vhbaia9mjxwr")))

(define-public crate-bevy_pipelines_ready-0.1.1 (c (n "bevy_pipelines_ready") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)))) (h "1yivk2kxpyy94f874b4kwn110lvz0xg6psygqrrp9fyhjrsx9pqs")))

(define-public crate-bevy_pipelines_ready-0.2.0 (c (n "bevy_pipelines_ready") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("android_shared_stdcxx" "bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_ui" "bevy_winit" "default_font" "hdr" "multi-threaded" "tonemapping_luts" "ktx2" "zstd" "x11"))) (k 2)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)))) (h "0n938vizjz9c0bh32c5qxrgnmk7h53n2pxazwmfg5yrkbv8jdzll")))

(define-public crate-bevy_pipelines_ready-0.3.0 (c (n "bevy_pipelines_ready") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("android_shared_stdcxx" "bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_ui" "bevy_winit" "default_font" "hdr" "multi-threaded" "tonemapping_luts" "ktx2" "zstd" "x11"))) (k 2)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)))) (h "0fwagkn567h0hqx91ykbkcf8zdq5yfb9hbvhsla8hbfvbfn9vdd4")))

