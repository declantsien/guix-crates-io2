(define-module (crates-io be vy bevy_cell) #:use-module (crates-io))

(define-public crate-bevy_cell-0.11.0 (c (n "bevy_cell") (v "0.11.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "intuple") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "type_cell") (r "^0.1.0") (d #t) (k 0)))) (h "195nz9xndniccg6wyqnggsfvgqcinpj7iw8cfa69p2gzgzjzp3vi") (y #t)))

(define-public crate-bevy_cell-0.11.1 (c (n "bevy_cell") (v "0.11.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "intuple") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "type_cell") (r "^0.1.0") (d #t) (k 0)))) (h "0qn5kg8a2zi31ai6aky12va8ybfld3ja2s7k059yzf08xymr000b")))

(define-public crate-bevy_cell-0.12.0 (c (n "bevy_cell") (v "0.12.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "intuple") (r "^0.1") (d #t) (k 0)) (d (n "type_cell") (r "^0.2") (d #t) (k 0)))) (h "1crgmm2ip76d1vspyb1di4r8g72qds7nzdy4hfjv56jqp0vpmcq0")))

(define-public crate-bevy_cell-0.13.0 (c (n "bevy_cell") (v "0.13.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "type_cell") (r "^0.3") (d #t) (k 0)))) (h "00310amr04imxjca2cq9aa1s1v0gyzb3xz45vvl57xkhjz82n9mg")))

(define-public crate-bevy_cell-0.13.1 (c (n "bevy_cell") (v "0.13.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "type_cell") (r "^0.3") (d #t) (k 0)))) (h "1k5937r2ci1a657akwczybq2wcjlwrcmpkxlf9fjh9mffbjpn2i2")))

(define-public crate-bevy_cell-0.13.2 (c (n "bevy_cell") (v "0.13.2") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "type_cell") (r "^0.3") (d #t) (k 0)))) (h "06jr87h4m5zlh14mvhrvrirfl39lxb0kvcw87x9knzz86wrkxi5p")))

(define-public crate-bevy_cell-0.13.3 (c (n "bevy_cell") (v "0.13.3") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "type_cell") (r "^0.3") (d #t) (k 0)))) (h "178n5vrdn3mfidi76fziih3h2wyp21sh69gdwbmc6pqncx6v7nfw")))

