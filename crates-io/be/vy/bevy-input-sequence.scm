(define-module (crates-io be vy bevy-input-sequence) #:use-module (crates-io))

(define-public crate-bevy-input-sequence-0.1.0 (c (n "bevy-input-sequence") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)))) (h "0p67c3kw4ngj10wphnxkkxnd5fn016g6f1gm59ygb2b534ri0hhr")))

(define-public crate-bevy-input-sequence-0.2.0 (c (n "bevy-input-sequence") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.1") (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 2)) (d (n "keyseq") (r "^0.1.1") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "trie-rs") (r "^0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1r5gqh1rcfr442c4kdfkzbqighk6cm3zkjzzv0yvv8kpnw26s2a1")))

(define-public crate-bevy-input-sequence-0.3.0 (c (n "bevy-input-sequence") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "keyseq") (r "^0.2.2") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "trie-rs") (r "^0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1y75yh6n0y6v4r8cgykxixdfm9qqdlx295bkch31ma3j8pjpxxbi")))

(define-public crate-bevy-input-sequence-0.4.0 (c (n "bevy-input-sequence") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "keyseq") (r "^0.2.3") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "trie-rs") (r "^0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0026ja5wchv5h2b94h5ch9xhza3rzgvk656c0v1vyrwzqbdn38r0")))

