(define-module (crates-io be vy bevy_ui_styled_macros) #:use-module (crates-io))

(define-public crate-bevy_ui_styled_macros-0.2.0 (c (n "bevy_ui_styled_macros") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_ui" "render"))) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wqqhl3m7a7b971x8snpcb7n5m4agg67lwlax6ll9n69v7svr9va")))

(define-public crate-bevy_ui_styled_macros-0.3.0 (c (n "bevy_ui_styled_macros") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vwzly703qzqshwryfkk9ps3bh53kxlxygfkmnagh28lnmxnsds0")))

(define-public crate-bevy_ui_styled_macros-0.4.0 (c (n "bevy_ui_styled_macros") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wc2sqza0gj84rrj3mh46q4qzzl292qx4k70734l73k17xhvnnl9")))

