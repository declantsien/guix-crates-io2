(define-module (crates-io be vy bevy_buttplug) #:use-module (crates-io))

(define-public crate-bevy_buttplug-0.1.0 (c (n "bevy_buttplug") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "bevy-tokio-tasks") (r "^0.9.2") (d #t) (k 0)) (d (n "buttplug") (r "^7.0") (d #t) (k 0)))) (h "187j50cp2r5ppsky2l8km6603lx9c2q3z8x706af0ikcp9vncq1f")))

