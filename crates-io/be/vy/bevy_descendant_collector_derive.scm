(define-module (crates-io be vy bevy_descendant_collector_derive) #:use-module (crates-io))

(define-public crate-bevy_descendant_collector_derive-0.1.0 (c (n "bevy_descendant_collector_derive") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0qwgnhrg50vkmkmph86awjp7a5xybmckaqkb5gp0n49rqyp90pk4")))

(define-public crate-bevy_descendant_collector_derive-0.1.1 (c (n "bevy_descendant_collector_derive") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0xhsxxymh0hs9kf060281rs7nq2z7qpi0aj79bd6dvji87bjkwyy")))

