(define-module (crates-io be vy bevy_retrograde_audio) #:use-module (crates-io))

(define-public crate-bevy_retrograde_audio-0.0.0 (c (n "bevy_retrograde_audio") (v "0.0.0") (h "16jkrm3s36v4qjgaarycsmklhqvzig2483qmwz54zshkmmaydkpj")))

(define-public crate-bevy_retrograde_audio-0.1.0 (c (n "bevy_retrograde_audio") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.5") (k 0)) (d (n "kira") (r "^0.5.2") (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0xl4hg6d7r78c025sb877rl95qpqbj2dv2k22y2y2jflrjj0vhi4") (f (quote (("wav" "kira/wav") ("ogg" "kira/ogg") ("mp3" "kira/mp3") ("flac" "kira/flac") ("default" "flac" "ogg" "wav"))))))

(define-public crate-bevy_retrograde_audio-0.2.0 (c (n "bevy_retrograde_audio") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.5") (k 0)) (d (n "kira") (r "^0.5.2") (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rlyxj0h6w3pw0fwn5b8yiqwqfg954rpf43nxh9x9c2y99jizl54") (f (quote (("wav" "kira/wav") ("ogg" "kira/ogg") ("mp3" "kira/mp3") ("flac" "kira/flac") ("default" "flac" "ogg" "wav"))))))

