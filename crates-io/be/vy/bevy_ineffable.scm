(define-module (crates-io be vy bevy_ineffable) #:use-module (crates-io))

(define-public crate-bevy_ineffable-0.1.0 (c (n "bevy_ineffable") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset" "serialize"))) (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 2)) (d (n "bevy_ineffable_macros") (r "=0.1.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "019559gpia2md2k44m1y64biq41gykxhzdb9gpzkd2lmvs1p7b57")))

(define-public crate-bevy_ineffable-0.2.0 (c (n "bevy_ineffable") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset" "serialize"))) (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 2)) (d (n "bevy_ineffable_macros") (r "=0.2.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k1l8rwlq6b7rnm8wxm127rbjfwsmw1n2nigvx9s668kqvjyzq8i")))

(define-public crate-bevy_ineffable-0.3.0 (c (n "bevy_ineffable") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset" "serialize"))) (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 2)) (d (n "bevy_ineffable_macros") (r "=0.3.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v75i2vz2rkmmyjqwc5caz1wsvz3dmnxrlrn72nhyvpg81yrrdc4")))

(define-public crate-bevy_ineffable-0.4.0 (c (n "bevy_ineffable") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "serialize"))) (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bevy_ineffable_macros") (r "=0.4.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "02glbi55ljaj3pk9wssyqz7al2j3scpbsv55p118iy208q0j8ms2")))

(define-public crate-bevy_ineffable-0.5.0 (c (n "bevy_ineffable") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "serialize"))) (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bevy_ineffable_macros") (r "=0.5.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "01kb2g63sp9gwylj2wfy1rnv6wykpaikyr28vpdir2idxnqfpf0i")))

