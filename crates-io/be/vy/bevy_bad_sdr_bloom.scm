(define-module (crates-io be vy bevy_bad_sdr_bloom) #:use-module (crates-io))

(define-public crate-bevy_bad_sdr_bloom-0.1.0 (c (n "bevy_bad_sdr_bloom") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "0hf84mci8kfv8lg62x2cc4dmaxziazjv6s5mrmdi9afvjkbq70vn")))

(define-public crate-bevy_bad_sdr_bloom-0.2.0 (c (n "bevy_bad_sdr_bloom") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "016f8z11klircgi3q0gkxlyyap0p06wygx6dqazx5jgl6c33cjac")))

