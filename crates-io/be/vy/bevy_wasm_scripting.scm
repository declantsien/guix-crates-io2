(define-module (crates-io be vy bevy_wasm_scripting) #:use-module (crates-io))

(define-public crate-bevy_wasm_scripting-0.1.0 (c (n "bevy_wasm_scripting") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "wasmer") (r "^3.0") (d #t) (k 0)) (d (n "wasmer-compiler-cranelift") (r "^3.0") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "0bddcz2cdnd9slvchkm3whna0gb0wbx8bi751ndqqdygjmdsjs1q")))

(define-public crate-bevy_wasm_scripting-0.2.0 (c (n "bevy_wasm_scripting") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "wasmer") (r "^3") (f (quote ("wat" "std"))) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "0jm5c06lb14ywld00yiw2f201la2y10a5xrjb2pvkcdqm1ivswv8") (f (quote (("non-js" "wasmer/sys-default") ("js" "wasmer/js-default") ("default" "non-js"))))))

