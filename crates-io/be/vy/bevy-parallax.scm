(define-module (crates-io be vy bevy-parallax) #:use-module (crates-io))

(define-public crate-bevy-parallax-0.1.0 (c (n "bevy-parallax") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.8.2") (d #t) (k 2)) (d (n "ron") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "0gg7580xkpyndw7a6ycjkhc3jnr2yn9qbpbysfh6qw82k6jjhxlz")))

(define-public crate-bevy-parallax-0.1.1 (c (n "bevy-parallax") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.10.0") (d #t) (k 2)) (d (n "ron") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "1ksmq2al7hmfsyyf6z6f2lpncpmd774rbmasdfsdl72pnaqk797a")))

(define-public crate-bevy-parallax-0.1.2 (c (n "bevy-parallax") (v "0.1.2") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.10.0") (d #t) (k 2)) (d (n "ron") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "0p54szryjbcgimh6i9fcrbx75bla4inbfzdbqvd540p1afyfpi5b")))

(define-public crate-bevy-parallax-0.2.0 (c (n "bevy-parallax") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.8.0") (f (quote ("x11" "png"))) (k 2)) (d (n "ron") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "18qjz1dwgpm2ia0bs3zlbndni5414pf3gfjynr1m6cd00aw910l4")))

(define-public crate-bevy-parallax-0.3.0 (c (n "bevy-parallax") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.9.0") (f (quote ("x11" "png"))) (k 2)) (d (n "ron") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "1q00w8bj7174m00iwwcy6njwvbxmfrwhqa4jknyw683gjx1zygzl")))

(define-public crate-bevy-parallax-0.4.0 (c (n "bevy-parallax") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.10.0") (f (quote ("x11" "png"))) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "1z629g67wa8nasaj35znc48waj5msxjkhjaqv0hp23q2w8gps9sm")))

(define-public crate-bevy-parallax-0.5.0 (c (n "bevy-parallax") (v "0.5.0") (d (list (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset" "bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.10.0") (f (quote ("x11" "png"))) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "082avfp1vyi3va7rm7imxkw9gqhn8f20qq42z5hhsg3b83mmn0hj")))

(define-public crate-bevy-parallax-0.6.0 (c (n "bevy-parallax") (v "0.6.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("x11" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.180") (d #t) (k 0)))) (h "02jgdnmzd63qiyh1q3wv80wpjl2qqw54wnr1ifpiibgy0j7gwmnk")))

(define-public crate-bevy-parallax-0.6.1 (c (n "bevy-parallax") (v "0.6.1") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("x11" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.180") (d #t) (k 0)))) (h "1jakkknjqpn1q58m9jc8bd2qai2dwiy7r709fbiyw43by39d8hsr")))

(define-public crate-bevy-parallax-0.7.0 (c (n "bevy-parallax") (v "0.7.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("x11" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.21.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.180") (d #t) (k 0)))) (h "0hwff1mrhn4zkkil29r996kw9zh6mld44kmpvmx7vwskv110dl97")))

(define-public crate-bevy-parallax-0.8.0 (c (n "bevy-parallax") (v "0.8.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("x11" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.23.0") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.180") (d #t) (k 0)))) (h "1kfnqzdcxn9fss4s2x0mvyckbpv6n2igr20ci7hnaa8msjvxia28")))

(define-public crate-bevy-parallax-0.9.0 (c (n "bevy-parallax") (v "0.9.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("x11" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.24") (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.180") (d #t) (k 0)))) (h "1ijmkijbjik7fchq57mdkzipvg450alqvyxqxihx1dpnwr9fmx8p") (s 2) (e (quote (("bevy-inspector-egui" "dep:bevy-inspector-egui"))))))

