(define-module (crates-io be vy bevy_derive_commands) #:use-module (crates-io))

(define-public crate-bevy_derive_commands-0.1.0 (c (n "bevy_derive_commands") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1144860c8b1df66vz7mdr6avk3mn8vqxd7xwx0ldvgn374fl80yz") (f (quote (("default"))))))

(define-public crate-bevy_derive_commands-0.1.1 (c (n "bevy_derive_commands") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0f1vz2dyhr9287klmv99vwc5ad3v96s1yj7dby96jx50qp9891xy") (f (quote (("default"))))))

(define-public crate-bevy_derive_commands-0.1.2 (c (n "bevy_derive_commands") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0ry30f053qz4gkzwrhybs3lbzrlbxz8s46kic7q1jamf3ifa7c8y") (f (quote (("default"))))))

