(define-module (crates-io be vy bevy_fpc_sprint) #:use-module (crates-io))

(define-public crate-bevy_fpc_sprint-0.1.0 (c (n "bevy_fpc_sprint") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "bevy_fpc_core") (r "^0.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.21") (f (quote ("simd-stable" "parallel"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1yl3s76g2azvy6ps80ahx21p30z1hiw07vsdnsq9jjq4y2l1s0rr")))

(define-public crate-bevy_fpc_sprint-0.1.1 (c (n "bevy_fpc_sprint") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_winit" "bevy_core_pipeline"))) (k 0)) (d (n "bevy_fpc_core") (r "^0.1.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.22") (f (quote ("dim3" "async-collider"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09jhccn9bgw9qp93qg2vaxwv418kzjqrpyffd92zis3kfsd4hcsx")))

(define-public crate-bevy_fpc_sprint-0.1.2 (c (n "bevy_fpc_sprint") (v "0.1.2") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline"))) (k 0)) (d (n "bevy_fpc_core") (r "^0.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.23") (f (quote ("dim3" "async-collider"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0cf8dxvb6nl70d0azfvn37q81n09znbbv777yzpki2jsfjyf611m")))

(define-public crate-bevy_fpc_sprint-0.1.3 (c (n "bevy_fpc_sprint") (v "0.1.3") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_core_pipeline"))) (k 0)) (d (n "bevy_fpc_core") (r "^0.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.25") (f (quote ("dim3" "async-collider"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0gd7v139rzhqlg8cngym3khlyig4ghma75vgclg9x7cisxqqvff7")))

