(define-module (crates-io be vy bevy_mod_static_inventory) #:use-module (crates-io))

(define-public crate-bevy_mod_static_inventory-0.1.0 (c (n "bevy_mod_static_inventory") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.13") (d #t) (k 0)) (d (n "dyn-eq") (r "^0.1.3") (d #t) (k 0)) (d (n "intertrait") (r "^0.2.2") (d #t) (k 0)))) (h "1xdyxq23v98yxl76plk98c9gqli1dwzykgdmpajcn15i1illbgsv")))

