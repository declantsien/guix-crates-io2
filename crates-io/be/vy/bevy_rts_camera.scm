(define-module (crates-io be vy bevy_rts_camera) #:use-module (crates-io))

(define-public crate-bevy_rts_camera-0.1.0 (c (n "bevy_rts_camera") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)))) (h "08gpbd574wc53g38z6rqbvsmfldf4c75339jfnj60ylp2dzdi7r2")))

(define-public crate-bevy_rts_camera-0.1.1 (c (n "bevy_rts_camera") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)))) (h "1xcjlnjl6739c9yzbl4317yphnyv5zf7a59nmfnnij9kq6mnfn88")))

(define-public crate-bevy_rts_camera-0.2.0 (c (n "bevy_rts_camera") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13") (d #t) (k 2)))) (h "0wpx8lflq6743gjpzbcf8xl41k3v5q3aqfk113pfh2yhjavhbhbl")))

(define-public crate-bevy_rts_camera-0.3.0 (c (n "bevy_rts_camera") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13") (d #t) (k 2)))) (h "05qfqx1rv2axfm67ym6xf9q5als04zknm55k2yb34i0ph7svkbwk")))

(define-public crate-bevy_rts_camera-0.4.0 (c (n "bevy_rts_camera") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13") (d #t) (k 2)))) (h "1k8fq9ksxvmfppd5nm7z2jbl8kzd6ja0pxi73yp1w4r5sm5s4lhv")))

(define-public crate-bevy_rts_camera-0.4.1 (c (n "bevy_rts_camera") (v "0.4.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13") (d #t) (k 2)))) (h "0cqhdrjfa3g6r2xnnf0kdi1y6gws2192mc9xisc0hrd82567iqfp")))

(define-public crate-bevy_rts_camera-0.4.2 (c (n "bevy_rts_camera") (v "0.4.2") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13") (d #t) (k 2)))) (h "1d2bc2xbckc58lkkd9pmsl3nr53h7y97m4jjs9hj50amz9cqybj2")))

(define-public crate-bevy_rts_camera-0.4.3 (c (n "bevy_rts_camera") (v "0.4.3") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13") (d #t) (k 2)))) (h "1vvksv3cfajjrsy87il90bssdw9a6f1ds5qbd14z3gcxdnhimnzf")))

(define-public crate-bevy_rts_camera-0.5.0 (c (n "bevy_rts_camera") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)))) (h "16vswq80mnvfiggzf3lsf8vzzmbyqj9q1gixk0fxqfqw24jw00g2")))

(define-public crate-bevy_rts_camera-0.5.1 (c (n "bevy_rts_camera") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)))) (h "08brcxl865y7nxfckx40vsgyzjsbmw0bwqvrk3nlp3s1rpsib0aa")))

(define-public crate-bevy_rts_camera-0.6.0 (c (n "bevy_rts_camera") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)))) (h "05fgjxbphsrx46wzgbs9kb48ir189w0a978gjjwi59dkrv32zfcs")))

(define-public crate-bevy_rts_camera-0.7.0 (c (n "bevy_rts_camera") (v "0.7.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)))) (h "0gwhm2qdj19fxlvhp9zbb30nhfiibkixyf5j72payay9df8hx0vl")))

(define-public crate-bevy_rts_camera-0.7.1 (c (n "bevy_rts_camera") (v "0.7.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_mod_raycast") (r "^0.17") (d #t) (k 0)))) (h "1b1yd0bs7bg7dnwklxg5rymxm2zpx84ly0726amz31d9qi8w3xhf")))

