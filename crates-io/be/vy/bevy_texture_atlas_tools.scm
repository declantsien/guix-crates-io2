(define-module (crates-io be vy bevy_texture_atlas_tools) #:use-module (crates-io))

(define-public crate-bevy_texture_atlas_tools-0.3.0 (c (n "bevy_texture_atlas_tools") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.4") (d #t) (k 0)) (d (n "bevy") (r "^0.6.0") (d #t) (k 0)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)))) (h "1149xw01zvlg687v75822xakszbiix76693lfq96rd7642xnchcd")))

(define-public crate-bevy_texture_atlas_tools-0.4.0 (c (n "bevy_texture_atlas_tools") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.4") (d #t) (k 0)) (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)))) (h "1anbz8mpwxjx44rcgdbjkvmsx8m5d5v7ml94i22q7b37ws98zb6v")))

(define-public crate-bevy_texture_atlas_tools-0.5.0 (c (n "bevy_texture_atlas_tools") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.4") (d #t) (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 2)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)))) (h "0a6bfdwi18h5x3ljm7p1cxv6nsb4pax6ch3h8ppqqx9hjyf6yz4k")))

(define-public crate-bevy_texture_atlas_tools-0.5.1 (c (n "bevy_texture_atlas_tools") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.4") (d #t) (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 2)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)))) (h "148qq1ncv4qgnhbv19mdf523pi1dd3c822jx36swcdzyqfm7gkn6")))

(define-public crate-bevy_texture_atlas_tools-0.6.0 (c (n "bevy_texture_atlas_tools") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.4") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)))) (h "1bf27knazv3jaw8rfpyk1g9pfb31bqxcrpw84w7vwsnrfjc8sghr")))

(define-public crate-bevy_texture_atlas_tools-0.6.1 (c (n "bevy_texture_atlas_tools") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.4") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)))) (h "150dy2m54rzcpqshyfcglfq8cfr2028f2ij2h6habk75apaihaxf")))

