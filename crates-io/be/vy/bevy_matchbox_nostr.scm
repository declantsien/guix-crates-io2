(define-module (crates-io be vy bevy_matchbox_nostr) #:use-module (crates-io))

(define-public crate-bevy_matchbox_nostr-0.6.0 (c (n "bevy_matchbox_nostr") (v "0.6.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "matchbox_socket_nostr") (r "^0.6") (d #t) (k 0)))) (h "1hx41pmqbpscjpzyn85caavysyykw2yxs31brr55fka1w80p4kri") (f (quote (("ggrs" "matchbox_socket_nostr/ggrs"))))))

(define-public crate-bevy_matchbox_nostr-0.6.1 (c (n "bevy_matchbox_nostr") (v "0.6.1") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "matchbox_socket_nostr") (r "^0.6.2") (d #t) (k 0)))) (h "1v02bbmlralgs5bfx6b255ydvvnv5fi3iagr8plvp07wfl65mzd8") (f (quote (("ggrs" "matchbox_socket_nostr/ggrs"))))))

(define-public crate-bevy_matchbox_nostr-0.6.3 (c (n "bevy_matchbox_nostr") (v "0.6.3") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "matchbox_socket_nostr") (r "^0.6.2") (d #t) (k 0)) (d (n "nostr") (r "^0.21.0") (d #t) (k 0)))) (h "1iwr2b0s99zwf5qxnyik5sivc54i0knirpgnjsh3zbyk7hnc6i0q") (f (quote (("ggrs" "matchbox_socket_nostr/ggrs"))))))

(define-public crate-bevy_matchbox_nostr-0.6.4 (c (n "bevy_matchbox_nostr") (v "0.6.4") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "matchbox_socket_nostr") (r "^0.6.2") (d #t) (k 0)) (d (n "nostr") (r "^0.21.0") (d #t) (k 0)))) (h "0zafrn81h54z73n61kdi8zpbwfryfl2zr6m1b496p2lsrkdzgp8f") (f (quote (("ggrs" "matchbox_socket_nostr/ggrs"))))))

