(define-module (crates-io be vy bevy_derive) #:use-module (crates-io))

(define-public crate-bevy_derive-0.1.0 (c (n "bevy_derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "165ih7njwz9dz97ddjv9fx250yzj91kgnbb1d2y4yll85x85q9zh")))

(define-public crate-bevy_derive-0.1.3 (c (n "bevy_derive") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "063x95g7fxvginir6fxhk3kzgb16wf5rglyw9yny9dwdmkgw8xlw")))

(define-public crate-bevy_derive-0.2.0 (c (n "bevy_derive") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f9z7zjfjq00s10673q2wkzd46gczyn94gw8gimibagkva2bfiz8")))

(define-public crate-bevy_derive-0.2.1 (c (n "bevy_derive") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p837kikiz105fp1ca8z51ymk7f17qzd8xpwq4ig4b3q2p3c20ah")))

(define-public crate-bevy_derive-0.3.0 (c (n "bevy_derive") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "18702xf5swrcahdynmi3rzpfrjqdrqmziyvhmkdnc71g7ggdh64w")))

(define-public crate-bevy_derive-0.4.0 (c (n "bevy_derive") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z9ld0gby5xrajwrvrqq2icn08zhmzqbvjpk1b7m6fc3bcvpvqcn")))

(define-public crate-bevy_derive-0.5.0 (c (n "bevy_derive") (v "0.5.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0151ppbpk31j06srk2ha5mmcw48671v49j3gnac4kysw69nx0vxx")))

(define-public crate-bevy_derive-0.6.0 (c (n "bevy_derive") (v "0.6.0") (d (list (d (n "bevy_macro_utils") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nayk5jw5csr0hnn8w0am0782hj0zms9vf4xk27lx2qyy3gw13ci")))

(define-public crate-bevy_derive-0.7.0 (c (n "bevy_derive") (v "0.7.0") (d (list (d (n "bevy_macro_utils") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "056pz20s9pavlvdki3c54akrhzl5pk0b76mzm4l1vwqmshpdzgba")))

(define-public crate-bevy_derive-0.8.0 (c (n "bevy_derive") (v "0.8.0") (d (list (d (n "bevy_macro_utils") (r "^0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ywzaw15amixr8hl53dxyy30hgfvcgj7p4cpab9y1iqzdmwg1f54")))

(define-public crate-bevy_derive-0.8.1 (c (n "bevy_derive") (v "0.8.1") (d (list (d (n "bevy_macro_utils") (r "^0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ya5dljsqw7rbnf2awcmfwlsjpdks3n61n2w2rn7nvdy3d1laqqf")))

(define-public crate-bevy_derive-0.9.0 (c (n "bevy_derive") (v "0.9.0") (d (list (d (n "bevy_macro_utils") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19d2l3vx96kz0y6hpda5vlg3b7pwmq5sji0wjy7468w4zz950bm1")))

(define-public crate-bevy_derive-0.9.1 (c (n "bevy_derive") (v "0.9.1") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19l209nxp089qn8v69w2kvqwj83l5r57drh8zp357hs1ip2p7bvv")))

(define-public crate-bevy_derive-0.10.0 (c (n "bevy_derive") (v "0.10.0") (d (list (d (n "bevy_macro_utils") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05xn6j3jyvn8r10az0hnrjs3sgb4yd3lb7xc7xxdrx0vq00igwfd")))

(define-public crate-bevy_derive-0.10.1 (c (n "bevy_derive") (v "0.10.1") (d (list (d (n "bevy_macro_utils") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14hrk9k3synr1smanmivm9pkidj8pd1r13z1hrz2nssamgasvw6z")))

(define-public crate-bevy_derive-0.11.0 (c (n "bevy_derive") (v "0.11.0") (d (list (d (n "bevy_macro_utils") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08rjmbyxikg68b6x6b3m63lmfplpw9vxdpx0ddsq3xvxn53p6iz1")))

(define-public crate-bevy_derive-0.11.1 (c (n "bevy_derive") (v "0.11.1") (d (list (d (n "bevy_macro_utils") (r "^0.11.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lp3hdwq2r671b1di1npkgsna3zinympfij88n1hs43srg0c2sxg")))

(define-public crate-bevy_derive-0.11.2 (c (n "bevy_derive") (v "0.11.2") (d (list (d (n "bevy_macro_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1612sp3qiiwih4avxcd7fmbni4m1zbf0dbcfggyx22jdbyc7ik65")))

(define-public crate-bevy_derive-0.11.3 (c (n "bevy_derive") (v "0.11.3") (d (list (d (n "bevy_macro_utils") (r "^0.11.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bmn07ayf87h0rarhwy74whs71x90cly0ms1kqck0558hhklwkm4")))

(define-public crate-bevy_derive-0.12.0 (c (n "bevy_derive") (v "0.12.0") (d (list (d (n "bevy_macro_utils") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mn5xpk3ny4r51ry4xsvcssywccdy5gnylzxv4j1lap1kcjl1gr4")))

(define-public crate-bevy_derive-0.12.1 (c (n "bevy_derive") (v "0.12.1") (d (list (d (n "bevy_macro_utils") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0075a4dav1x7qfmvgdm6hkjbdmhzdijmm92276x5hb26a21k317l")))

(define-public crate-bevy_derive-0.13.0 (c (n "bevy_derive") (v "0.13.0") (d (list (d (n "bevy_macro_utils") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19lbmqlpyfiqw579w4ryfgrxra76xfqypgpisy2m21bq8siy52h2")))

(define-public crate-bevy_derive-0.13.1 (c (n "bevy_derive") (v "0.13.1") (d (list (d (n "bevy_macro_utils") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d5sh1ss70xjpsxyrgbl2d4dxizsaagshs7582kj1qjls4ipbrvx")))

(define-public crate-bevy_derive-0.13.2 (c (n "bevy_derive") (v "0.13.2") (d (list (d (n "bevy_macro_utils") (r "^0.13.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "006x0yadbwm8z63zq4w77ka2w9avzf18ndivlvbf54gk8f1izq7h")))

