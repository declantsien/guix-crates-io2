(define-module (crates-io be vy bevy_video) #:use-module (crates-io))

(define-public crate-bevy_video-0.9.0 (c (n "bevy_video") (v "0.9.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "openh264") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0zi40b3ha81fcc0286g4yw3zmn9qxx0ck0f2rmqh9f6bk43l923m")))

(define-public crate-bevy_video-0.9.1 (c (n "bevy_video") (v "0.9.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "openh264") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0zcfzfqjcia4zzd8frl6x7aa684fy466if0glbs7chq0yfrgk84w")))

