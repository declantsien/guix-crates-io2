(define-module (crates-io be vy bevy_crossterm) #:use-module (crates-io))

(define-public crate-bevy_crossterm-0.4.0 (c (n "bevy_crossterm") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.4") (k 0)) (d (n "bevy") (r "^0.4") (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "broccoli") (r "^0.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.13.1") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 0)))) (h "1incm83spd0n39a8s1nyxs52nswi8gxyn5pnv7blzngywzb4rcf5")))

