(define-module (crates-io be vy bevy_roll_safe) #:use-module (crates-io))

(define-public crate-bevy_roll_safe-0.1.0 (c (n "bevy_roll_safe") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_ggrs") (r "^0.14") (o #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)))) (h "0bs8zhyixkqrc0ja6zzl8m7qr47137xv48iidjl3c62nl4ywcc3g") (f (quote (("math_determinism" "glam/libm") ("default" "bevy_ggrs" "math_determinism")))) (s 2) (e (quote (("bevy_ggrs" "dep:bevy_ggrs"))))))

(define-public crate-bevy_roll_safe-0.2.0 (c (n "bevy_roll_safe") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy_ggrs") (r "^0.15") (o #t) (k 0)) (d (n "bevy_math") (r "^0.13") (d #t) (k 0)))) (h "1c82w3kgc32j66wdi96kbz54gmnx0v2prkaa22wqpggksks32nhx") (f (quote (("math_determinism" "bevy_math/libm") ("default" "bevy_ggrs" "math_determinism")))) (s 2) (e (quote (("bevy_ggrs" "dep:bevy_ggrs"))))))

