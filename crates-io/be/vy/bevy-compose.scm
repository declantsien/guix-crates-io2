(define-module (crates-io be vy bevy-compose) #:use-module (crates-io))

(define-public crate-bevy-compose-0.1.0 (c (n "bevy-compose") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.2") (d #t) (k 0)))) (h "0k6r6mvl55mi04vbzx2a131jirgkgm7jah2qnyd4vafbv9z4bjm8")))

(define-public crate-bevy-compose-0.2.0-alpha.1 (c (n "bevy-compose") (v "0.2.0-alpha.1") (d (list (d (n "bevy") (r "^0.13.2") (d #t) (k 0)))) (h "0r894kmhaa0rwffr8krsj0z9vn154822nj48srkzil4ldv7p79kl")))

(define-public crate-bevy-compose-0.2.0-alpha.2 (c (n "bevy-compose") (v "0.2.0-alpha.2") (d (list (d (n "bevy") (r "^0.13.2") (d #t) (k 0)))) (h "07cd4ck9n24f8if79vvsw5g8c5sxqaxwnhjy9ain9sl13d6zakrx")))

