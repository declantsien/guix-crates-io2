(define-module (crates-io be vy bevy_asset_macros) #:use-module (crates-io))

(define-public crate-bevy_asset_macros-0.12.0 (c (n "bevy_asset_macros") (v "0.12.0") (d (list (d (n "bevy_macro_utils") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0fhy2yvndb0hgk319lc3l36isl14n9y4d6h5y6y8mz4h7qclachv")))

(define-public crate-bevy_asset_macros-0.12.1 (c (n "bevy_asset_macros") (v "0.12.1") (d (list (d (n "bevy_macro_utils") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1305h0gdv7ws932brwzm1awf1yyb18d1xkdm214mwq7cwjxvjj1z")))

(define-public crate-bevy_asset_macros-0.13.0 (c (n "bevy_asset_macros") (v "0.13.0") (d (list (d (n "bevy_macro_utils") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ylj4w0w1mqz9zw505wcn96zaxdzs64df2ldbb27h4r5r2md30nb")))

(define-public crate-bevy_asset_macros-0.13.1 (c (n "bevy_asset_macros") (v "0.13.1") (d (list (d (n "bevy_macro_utils") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0j4mhbarmzn48p63341z75nw85nimryhxnkanx04fwz7a5b8ak38")))

(define-public crate-bevy_asset_macros-0.13.2 (c (n "bevy_asset_macros") (v "0.13.2") (d (list (d (n "bevy_macro_utils") (r "^0.13.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mxzsh66yxm3pbbx4w2s4pf7ky42pgylh09n2pc1i11n11clf5v6")))

