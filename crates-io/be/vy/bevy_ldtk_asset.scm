(define-module (crates-io be vy bevy_ldtk_asset) #:use-module (crates-io))

(define-public crate-bevy_ldtk_asset-0.1.0 (c (n "bevy_ldtk_asset") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.22") (d #t) (k 2)) (d (n "bevy_mod_aseprite") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "021fybyb4nrmqjcb13qdhfkygnjvivy6smnj532n30vccpla8dk0")))

(define-public crate-bevy_ldtk_asset-0.2.0 (c (n "bevy_ldtk_asset") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.22") (d #t) (k 2)) (d (n "bevy_mod_aseprite") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jlz6cf02kr1wddllyzvn3gqry3mrn0zbcgs806p5igkb23hxcm0")))

