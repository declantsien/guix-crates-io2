(define-module (crates-io be vy bevy_lunex) #:use-module (crates-io))

(define-public crate-bevy_lunex-0.0.1 (c (n "bevy_lunex") (v "0.0.1") (d (list (d (n "bevy_lunex_core") (r "^0.0.1") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.1") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.1") (d #t) (k 0)))) (h "085iywnpx2caw8c3p49caf1fv4mlm2qjaysfvyqcqrrvd9zc03w7")))

(define-public crate-bevy_lunex-0.0.2 (c (n "bevy_lunex") (v "0.0.2") (d (list (d (n "bevy_lunex_core") (r "^0.0.2") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.2") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.2") (d #t) (k 0)))) (h "0ra4fhymlb9mj6hzzhj0ik35d9h3s7n7pvhfzgwv0pfvlxp87zb7")))

(define-public crate-bevy_lunex-0.0.3 (c (n "bevy_lunex") (v "0.0.3") (d (list (d (n "bevy_lunex_core") (r "^0.0.2") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.3") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.3") (d #t) (k 0)))) (h "0nl1yw2l073s68apcpfls470vvvg5q6z02bw0ywd3miww3qvdd90")))

(define-public crate-bevy_lunex-0.0.4 (c (n "bevy_lunex") (v "0.0.4") (d (list (d (n "bevy_lunex_core") (r "^0.0.4") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.4") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.4") (d #t) (k 0)))) (h "0l39h5iz177sw71kf5ql1zq8r21w8wkv8s15iwzl5ihzhgky1y16")))

(define-public crate-bevy_lunex-0.0.5 (c (n "bevy_lunex") (v "0.0.5") (d (list (d (n "bevy_lunex_core") (r "^0.0.5") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.5") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.5") (d #t) (k 0)))) (h "1lz7ccd8wapd6368lfq9jwg1figb2mb8raj9s7gxb62bybxbfqr2")))

(define-public crate-bevy_lunex-0.0.6 (c (n "bevy_lunex") (v "0.0.6") (d (list (d (n "bevy_lunex_core") (r "^0.0.6") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.6") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.6") (d #t) (k 0)))) (h "0c2jvldjbla0bgka18xw6crpxbas8681j55jypn08gyay8wyc61a")))

(define-public crate-bevy_lunex-0.0.7 (c (n "bevy_lunex") (v "0.0.7") (d (list (d (n "bevy_lunex_core") (r "^0.0.7") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.7") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.7") (d #t) (k 0)))) (h "1kgyx473h83lr2p2rk2ijjz21vzz2mvwbwbvjb86g4jypkmhqbyx")))

(define-public crate-bevy_lunex-0.0.8 (c (n "bevy_lunex") (v "0.0.8") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "bevy_lunex_core") (r "^0.0.8") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.8") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.8") (d #t) (k 0)))) (h "0pkxd3anjbf0mjbld7yzlagd6zaxbk6l6lfzk0yq2pjny2y98vbk")))

(define-public crate-bevy_lunex-0.0.9 (c (n "bevy_lunex") (v "0.0.9") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos" "dynamic_linking"))) (d #t) (k 2)) (d (n "bevy_lunex_core") (r "^0.0.9") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.9") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.9") (d #t) (k 0)))) (h "1g41vw6ar83b4cb0fcyr7bj8x6rxnaxlclk36z51vg95bsnpdvpx")))

(define-public crate-bevy_lunex-0.0.10 (c (n "bevy_lunex") (v "0.0.10") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos" "dynamic_linking"))) (d #t) (k 2)) (d (n "bevy_lunex_core") (r "^0.0.10") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.10") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.10") (d #t) (k 0)) (d (n "mathio") (r "^0.2.5") (f (quote ("bevy"))) (d #t) (k 0)))) (h "1vz87hk49a72h7x4nz0jmxn6cgrssx0g672x3nb45ybjw189bdq0")))

(define-public crate-bevy_lunex-0.0.11 (c (n "bevy_lunex") (v "0.0.11") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos" "dynamic_linking"))) (d #t) (k 2)) (d (n "bevy_lunex_core") (r "^0.0.11") (d #t) (k 0)) (d (n "bevy_lunex_ui") (r "^0.0.11") (d #t) (k 0)) (d (n "bevy_lunex_utility") (r "^0.0.11") (d #t) (k 0)) (d (n "mathio") (r "^0.2.5") (f (quote ("bevy"))) (d #t) (k 0)))) (h "0354f1qjncjicryxcsm0vq4mj3rwzpciv2n8ix6ggjb1pb2mljaa")))

(define-public crate-bevy_lunex-0.1.0-alpha (c (n "bevy_lunex") (v "0.1.0-alpha") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_pbr" "bevy_sprite" "bevy_text" "multi-threaded" "bevy_gizmos"))) (k 0)) (d (n "bevy_mod_picking") (r "^0.18.2") (o #t) (k 0)) (d (n "colored") (r "^2.1") (d #t) (k 0)) (d (n "lunex_engine") (r "^0.1.0-alpha") (f (quote ("bevy"))) (d #t) (k 0)))) (h "1ngkhmabfjf5icq0z7khlk3y2vsq93gxvd4z0pcvxa1lbpsd4naj") (f (quote (("picking" "bevy_mod_picking") ("debug"))))))

