(define-module (crates-io be vy bevy_ui_animation) #:use-module (crates-io))

(define-public crate-bevy_ui_animation-0.0.0 (c (n "bevy_ui_animation") (v "0.0.0") (d (list (d (n "bevy") (r "^0.6.1") (k 0)))) (h "0cnr1hli00f97a72wabb957dnpzqak4qprnbrj0avishkdw4d751")))

(define-public crate-bevy_ui_animation-1.0.0 (c (n "bevy_ui_animation") (v "1.0.0") (d (list (d (n "bevy") (r "^0.6.1") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite" "bevy_text" "bevy_ui"))) (k 0)))) (h "0yb2jlvls6gcnwdgb8dxr1c7r19py5pjhf9zvsdcz959cbpgqpwq")))

