(define-module (crates-io be vy bevy_video_glitch) #:use-module (crates-io))

(define-public crate-bevy_video_glitch-0.1.0 (c (n "bevy_video_glitch") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "05v4k42d1fydni1z1k9mshmqrqx0b2vi6smyph8dnn9jm429nix1")))

(define-public crate-bevy_video_glitch-0.1.1 (c (n "bevy_video_glitch") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "1jjp00almhl2z6nhysz6pg2q85glc51w9gs5h46grkv3a804n2jw")))

