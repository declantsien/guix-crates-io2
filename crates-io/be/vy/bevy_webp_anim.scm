(define-module (crates-io be vy bevy_webp_anim) #:use-module (crates-io))

(define-public crate-bevy_webp_anim-0.1.0 (c (n "bevy_webp_anim") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt" "sync" "rt-multi-thread"))) (d #t) (k 0)))) (h "05xpn04i4z1xsgbxp6mqfcq9jiykwdcbz42n7fqlsrr1rhdsh5xl")))

(define-public crate-bevy_webp_anim-0.2.0 (c (n "bevy_webp_anim") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt" "sync" "rt-multi-thread"))) (d #t) (k 0)))) (h "1q8fq08hbdhqzyk1crjfzqmhbr4vzj8mxwycrhadgjm8c4y8f4ba")))

(define-public crate-bevy_webp_anim-0.3.0 (c (n "bevy_webp_anim") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt" "sync" "rt-multi-thread"))) (d #t) (k 0)))) (h "0322zmbk3ka7j8v2wqgcvw55gcgsf54asg1cxjm3in0nmjljalpa")))

(define-public crate-bevy_webp_anim-0.3.1 (c (n "bevy_webp_anim") (v "0.3.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt" "sync" "rt-multi-thread"))) (d #t) (k 0)))) (h "1yn1kzzy0h8q85zgjgs2g14nk6m6s49b8cpvhapgi7x9skwnr4ip")))

