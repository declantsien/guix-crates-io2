(define-module (crates-io be vy bevy_prototype_inline_assets) #:use-module (crates-io))

(define-public crate-bevy_prototype_inline_assets-0.1.0 (c (n "bevy_prototype_inline_assets") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "11bxhmdbrya93mar1wdck0p3bknq7c4zw4wb4km1w0b3bwaqh5mq")))

(define-public crate-bevy_prototype_inline_assets-0.1.1 (c (n "bevy_prototype_inline_assets") (v "0.1.1") (d (list (d (n "bevy_app") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "0a5l059mqqp4258p0za691m2axqxsvcm5kzcqs1c8nxpv15k8df1")))

