(define-module (crates-io be vy bevy_attr) #:use-module (crates-io))

(define-public crate-bevy_attr-0.1.0-dev (c (n "bevy_attr") (v "0.1.0-dev") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy-trait-query") (r "^0.1") (d #t) (k 0)))) (h "1acqhzsrcp34h6xj7xqq733yzmxgvmdmcb8wl9sj3663zi07f2sz") (y #t)))

(define-public crate-bevy_attr-0.1.0 (c (n "bevy_attr") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy-trait-query") (r "^0.1") (d #t) (k 0)))) (h "1sms4z10n4f06ka18i44safgssnk59x1xcrr64ch67d76jabjm3j")))

