(define-module (crates-io be vy bevy_ecs_ldtk_macros) #:use-module (crates-io))

(define-public crate-bevy_ecs_ldtk_macros-0.1.0 (c (n "bevy_ecs_ldtk_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12jwmqzhfkagyvmzl36k94xxrkka60ykm4dprxg9j08zzyg2hqi1")))

(define-public crate-bevy_ecs_ldtk_macros-0.2.0 (c (n "bevy_ecs_ldtk_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g7gkalag3k7l0q43qyi9dkjds2q7q317z6zp92xlpxmv26fdb5f")))

(define-public crate-bevy_ecs_ldtk_macros-0.3.0 (c (n "bevy_ecs_ldtk_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1skax7ns72cf3d5yh4763ipia7vzr189awa5hxwc7b2k0v8jrnq5")))

(define-public crate-bevy_ecs_ldtk_macros-0.4.0 (c (n "bevy_ecs_ldtk_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01bvylc1mskpi3lj1bbzj2869cs2g3kavvpn49a51g4vakb455m0")))

(define-public crate-bevy_ecs_ldtk_macros-0.5.0 (c (n "bevy_ecs_ldtk_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "020rxwi3rywxl62i0xazsxrrdig1s75506zz6myc5lr8bh9cdlbx")))

(define-public crate-bevy_ecs_ldtk_macros-0.6.0 (c (n "bevy_ecs_ldtk_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lq45h4fl1mzwl1nxjd9mgjpg2c989xbx2c12w2l2ljc5fnfqddi")))

(define-public crate-bevy_ecs_ldtk_macros-0.7.0 (c (n "bevy_ecs_ldtk_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0avr180cyisj04fpkhypc3cmzd9z2qxkcqldnb2hdzy417ggr52y")))

(define-public crate-bevy_ecs_ldtk_macros-0.8.0 (c (n "bevy_ecs_ldtk_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17rpg6j31d4ppvmirxvdjcps31nm20jsgwplh3gqd5avp8xfnkdq")))

(define-public crate-bevy_ecs_ldtk_macros-0.9.0 (c (n "bevy_ecs_ldtk_macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wxqwd601fqqx0r26waiz81i71r2g03hnp02fxicf44ilydfk9b4")))

