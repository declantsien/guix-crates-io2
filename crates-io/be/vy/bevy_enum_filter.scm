(define-module (crates-io be vy bevy_enum_filter) #:use-module (crates-io))

(define-public crate-bevy_enum_filter-0.1.0 (c (n "bevy_enum_filter") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_app") (r "^0.8") (k 0)) (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "bevy_enum_filter_derive") (r "^0.1.0") (d #t) (k 0)))) (h "00835qvazrljnhg4cacdq528m7xsyqrqsjcqwyjpcjbm05wx9yci")))

(define-public crate-bevy_enum_filter-0.1.1 (c (n "bevy_enum_filter") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_app") (r "^0.8") (k 0)) (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "bevy_enum_filter_derive") (r "^0.1.0") (d #t) (k 0)))) (h "05n9ph6db0viaagdb9fg6fh28gzw83s4yb4zfg4flwbc7hz96a4z")))

(define-public crate-bevy_enum_filter-0.2.0 (c (n "bevy_enum_filter") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "bevy_enum_filter_derive") (r "^0.1.0") (d #t) (k 0)))) (h "02lqfchvlmahm2nfi9rhfy15pkbldq7274ix56ms5ld71wxvwpjb")))

(define-public crate-bevy_enum_filter-0.3.0 (c (n "bevy_enum_filter") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12") (k 0)) (d (n "bevy_ecs") (r "^0.12") (k 0)) (d (n "bevy_enum_filter_derive") (r "^0.1.0") (d #t) (k 0)))) (h "14agb9qjp6pji8izb0f54i8ic2xvr4zngzw3nwzlh370gxg8y428")))

