(define-module (crates-io be vy bevy_text_mode) #:use-module (crates-io))

(define-public crate-bevy_text_mode-0.1.0 (c (n "bevy_text_mode") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)))) (h "1x2vhxsqkjn6fkgppfrsd0k4nkhl216g3d4b1q7jxbjkhwdnf0hp")))

(define-public crate-bevy_text_mode-0.1.1 (c (n "bevy_text_mode") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)))) (h "0fnsmk8p30k1sap5s7hcs3ddj68bgy2984zd134i4as2xpj09gg8")))

(define-public crate-bevy_text_mode-0.2.0 (c (n "bevy_text_mode") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)))) (h "0w2ypfjxnzfav8hpp8mnayr6may6mfw2kwigc1b17kbmipvy0xpx")))

(define-public crate-bevy_text_mode-0.3.0 (c (n "bevy_text_mode") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_sprite") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)))) (h "178sbpkd8dwvsql8d7m70kw617rfkqgmmwdgqszbwp6yk8xn1jsz")))

