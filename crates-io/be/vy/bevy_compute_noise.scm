(define-module (crates-io be vy bevy_compute_noise) #:use-module (crates-io))

(define-public crate-bevy_compute_noise-0.1.0 (c (n "bevy_compute_noise") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_winit" "x11" "tonemapping_luts" "bevy_pbr" "bevy_sprite"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.24.0") (d #t) (k 0)) (d (n "bevy_flycam") (r "^0.13.0") (d #t) (k 2)) (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0i60mwmj5jw3zjdv8k09lczzi9am8b3p4bl1v47aphk5m8jnwm6l")))

