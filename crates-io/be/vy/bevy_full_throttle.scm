(define-module (crates-io be vy bevy_full_throttle) #:use-module (crates-io))

(define-public crate-bevy_full_throttle-0.1.0 (c (n "bevy_full_throttle") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "windows") (r "^0.43") (f (quote ("Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14wngsdsczyqp1zdgh11yyhbcqq0kjcy0rgxzvv0ldpg56r72waw")))

(define-public crate-bevy_full_throttle-0.1.1 (c (n "bevy_full_throttle") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nkz13rlx8i9py1jmyybl7bdhsqj4gaghm43hsk98vzhdsxjz43h")))

(define-public crate-bevy_full_throttle-0.1.2 (c (n "bevy_full_throttle") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "ctrlc") (r "^3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0v035qzhjqaxp2pqii8y7d3byc80crl7v5d7v1qxhicdk8396wfy")))

(define-public crate-bevy_full_throttle-0.1.3 (c (n "bevy_full_throttle") (v "0.1.3") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05cizwg6rq8c7lspbclhwkvmp0xjwjbxiksjsfcmx9vqfmyimim1")))

(define-public crate-bevy_full_throttle-0.2.0 (c (n "bevy_full_throttle") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w5058yq5531npsgnqych052ajfnw7bpn51g1h2z97fdsn9b8x2w")))

(define-public crate-bevy_full_throttle-0.2.1 (c (n "bevy_full_throttle") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "windows") (r "^0.46") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0s3rmfjbn5wrklaj6z9xwf6if9r7hwbv92jviaip5qknb78cyb26")))

(define-public crate-bevy_full_throttle-0.2.2 (c (n "bevy_full_throttle") (v "0.2.2") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1s49nnyz1b4n26837md92alzkv9ljy916f3nywp2lf33nb5js2kn")))

(define-public crate-bevy_full_throttle-0.3.0 (c (n "bevy_full_throttle") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0d2ma2rq8w5i5rxjq4mhw0ma623d3y8687fjvhwac683xhdy3kax")))

(define-public crate-bevy_full_throttle-0.3.1 (c (n "bevy_full_throttle") (v "0.3.1") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q7b2ardqamf9mdhy2ldr7r4bwac3d7dvhdyv87vd1xmbz0s41ip")))

(define-public crate-bevy_full_throttle-0.4.0 (c (n "bevy_full_throttle") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "08v9l9qwg4ikvhp6nnps4jp2pdqs90gx8v7kr2jcxmmp0jrllcl1")))

(define-public crate-bevy_full_throttle-0.5.0 (c (n "bevy_full_throttle") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xz8wnjpzlzv4lxgdg0x27972ilis495vps7ryb44810riglj01z")))

(define-public crate-bevy_full_throttle-0.5.1 (c (n "bevy_full_throttle") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "windows") (r "^0.56") (f (quote ("Win32_Foundation" "Win32_System_Power" "Win32_System_Registry" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0085iv9k857a4dfzfy91xyg788gw3h0ky7657w39yn3mzrr64yvd")))

