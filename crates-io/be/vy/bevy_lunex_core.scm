(define-module (crates-io be vy bevy_lunex_core) #:use-module (crates-io))

(define-public crate-bevy_lunex_core-0.0.1 (c (n "bevy_lunex_core") (v "0.0.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "15fz9pglpdai76crsykr8bnvw9ai9wkzhcvnnhh47xlvs6qzlkh8")))

(define-public crate-bevy_lunex_core-0.0.2 (c (n "bevy_lunex_core") (v "0.0.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1afs39ll9cyn975zp3baym0xmrn5jnd0031425g6xh4y7bn8yxqk")))

(define-public crate-bevy_lunex_core-0.0.4 (c (n "bevy_lunex_core") (v "0.0.4") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1rk27nzdgyjx1qslm2hfzzj7rfsdlk8qdbnhn71l7k2k3yxfyfz5")))

(define-public crate-bevy_lunex_core-0.0.5 (c (n "bevy_lunex_core") (v "0.0.5") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0rird4nzrqx9f2jx0by6malmq71nr1vixm75hzr15p8m16d8fy95")))

(define-public crate-bevy_lunex_core-0.0.6 (c (n "bevy_lunex_core") (v "0.0.6") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0d0nw90fa217x5dzplrw89fkdj5yvsdhbi8jwcl8r806nj3q64js")))

(define-public crate-bevy_lunex_core-0.0.7 (c (n "bevy_lunex_core") (v "0.0.7") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pathio") (r "^0.2.1") (f (quote ("bevy"))) (d #t) (k 0)))) (h "0mrxfdx1gwjffszc1krn3ybhg7zbc4qvjih28n0x5y44w9z2mczh")))

(define-public crate-bevy_lunex_core-0.0.8 (c (n "bevy_lunex_core") (v "0.0.8") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos"))) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "pathio") (r "^0.2.3") (f (quote ("bevy"))) (d #t) (k 0)))) (h "1408kiy088jyif6ih6yzf0wlyjc53fgrqkaz8nfps8m75vxr93sq")))

(define-public crate-bevy_lunex_core-0.0.9 (c (n "bevy_lunex_core") (v "0.0.9") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos"))) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "pathio") (r "^0.2.3") (f (quote ("bevy"))) (d #t) (k 0)))) (h "0w8xn7z45lvw59gxhdlnz26097xzk4kfvjw6ddwjw634sgv0hrb1")))

(define-public crate-bevy_lunex_core-0.0.10 (c (n "bevy_lunex_core") (v "0.0.10") (d (list (d (n "ahash") (r "^0.8.6") (d #t) (k 0)) (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos"))) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "mathio") (r "^0.2.5") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "pathio") (r "^0.2.3") (f (quote ("bevy"))) (d #t) (k 0)))) (h "0d5572q5v3dqnp5a3k5x5g9q640qcsx0bg4qy9fgcpvgg2mz0xqh")))

(define-public crate-bevy_lunex_core-0.0.11 (c (n "bevy_lunex_core") (v "0.0.11") (d (list (d (n "ahash") (r "^0.8.6") (d #t) (k 0)) (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos"))) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "mathio") (r "^0.2.5") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "pathio") (r "^0.2.3") (f (quote ("bevy"))) (d #t) (k 0)))) (h "0mvhjzwa1wbdchjry1i28gcq035ac1p60agr3flx9xvnjp1mv95r")))

