(define-module (crates-io be vy bevy-trait-resource-macro) #:use-module (crates-io))

(define-public crate-bevy-trait-resource-macro-0.1.0 (c (n "bevy-trait-resource-macro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0jl8cllydbk5qjrq86kzzkvzva7vm761cq2ysaw9zrfnm64xkb45")))

(define-public crate-bevy-trait-resource-macro-0.1.1 (c (n "bevy-trait-resource-macro") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "07l9jggwrrga4przk2rm1dk4psw5wh9mxa95dh41n18shf4lkh62")))

