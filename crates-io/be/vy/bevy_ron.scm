(define-module (crates-io be vy bevy_ron) #:use-module (crates-io))

(define-public crate-bevy_ron-0.1.0 (c (n "bevy_ron") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1my58iwhsvcs6rzxafjh7psz83pbpdx187n6mpc0vnplvk1girbj")))

(define-public crate-bevy_ron-0.1.3 (c (n "bevy_ron") (v "0.1.3") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "13684qsp13rfzv215mp3ml9kbxf4ck3qbrcpwxadccgspl8bs1qn")))

