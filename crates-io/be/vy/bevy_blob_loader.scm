(define-module (crates-io be vy bevy_blob_loader) #:use-module (crates-io))

(define-public crate-bevy_blob_loader-0.0.1 (c (n "bevy_blob_loader") (v "0.0.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.66") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "0carrbjx8p1ci6l6wkgdfaih1vpsabjrcpfmmracr515i01hy498")))

(define-public crate-bevy_blob_loader-0.0.2 (c (n "bevy_blob_loader") (v "0.0.2") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.66") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.39") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("Request" "Window" "Response"))) (d #t) (k 0)))) (h "0bk03vsmrdv5a0m2w2n1wrmdfc23avrikgd5ydwq3bdm5q7h6hqd")))

(define-public crate-bevy_blob_loader-0.0.3 (c (n "bevy_blob_loader") (v "0.0.3") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.66") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.39") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("Request" "Window" "Response"))) (d #t) (k 0)))) (h "05zh52r6wqvh306a5iikc1dqhyns0gm2cf18b5sggqq61knlnrxz")))

(define-public crate-bevy_blob_loader-0.0.4 (c (n "bevy_blob_loader") (v "0.0.4") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.66") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.39") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("Request" "Window" "Response"))) (d #t) (k 0)))) (h "0yar5v6rmr6zvbx9ycsqcw7ny9dnp3hhinqsz97ik0lfdav1y1ni")))

