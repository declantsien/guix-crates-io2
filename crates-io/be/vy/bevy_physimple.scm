(define-module (crates-io be vy bevy_physimple) #:use-module (crates-io))

(define-public crate-bevy_physimple-0.1.0 (c (n "bevy_physimple") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1wmnsv0i0q9sbwxcw6c5w9frdg844lxrhq45fp7hal23gwcq7dy1")))

(define-public crate-bevy_physimple-0.2.0 (c (n "bevy_physimple") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1bi5q1mi56gb19sjly3m447wjxpkv3l4dmn54b68vp9c55cr2iif")))

(define-public crate-bevy_physimple-0.3.0 (c (n "bevy_physimple") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "021mkx7bzwnnww4a46xlihzcsxlqz05arnslns5y0h78w0mb47gn")))

(define-public crate-bevy_physimple-0.4.0 (c (n "bevy_physimple") (v "0.4.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "152kwxzhcjb2xn4ljwiq5bbv4wal22xw2lkrw96p9smai2bwhq4v")))

(define-public crate-bevy_physimple-0.5.0 (c (n "bevy_physimple") (v "0.5.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0bcy1qyy4dbrjy63s6bx47sb6g7ccjmhs7sv94cxl4xmrz1sy3hb")))

