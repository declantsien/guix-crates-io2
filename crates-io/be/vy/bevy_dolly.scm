(define-module (crates-io be vy bevy_dolly) #:use-module (crates-io))

(define-public crate-bevy_dolly-0.0.1 (c (n "bevy_dolly") (v "0.0.1") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_render" "bevy_asset" "bevy_pbr"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_core_pipeline" "bevy_asset" "bevy_scene" "bevy_pbr" "bevy_winit" "bevy_gltf" "bevy_sprite" "png" "ktx2" "zstd" "tonemapping_luts"))) (k 2)) (d (n "bevy") (r "^0.11.0") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "bevy_math") (r "^0.11") (d #t) (k 0)) (d (n "bevy_transform") (r "^0.11") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.10") (d #t) (k 2)))) (h "01rv7s39i12apx8vvai10krrh0w6y4934k39qwfdpgvm1dndyi65") (f (quote (("drivers") ("default" "drivers" "helpers")))) (s 2) (e (quote (("helpers" "dep:leafwing-input-manager"))))))

(define-public crate-bevy_dolly-0.0.2 (c (n "bevy_dolly") (v "0.0.2") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_asset" "bevy_pbr"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline" "bevy_asset" "bevy_scene" "bevy_pbr" "bevy_winit" "bevy_gltf" "bevy_sprite" "png" "ktx2" "zstd" "tonemapping_luts" "bevy_gizmos"))) (k 2)) (d (n "bevy") (r "^0.12") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "bevy_math") (r "^0.12") (d #t) (k 0)) (d (n "bevy_transform") (r "^0.12") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.11.2") (d #t) (k 2)))) (h "046xjvskkl6a3bz7zv6a3va646iwagbgzmijzv0flmbavnmvzmk4") (f (quote (("drivers") ("default" "drivers" "helpers")))) (s 2) (e (quote (("helpers" "dep:leafwing-input-manager"))))))

(define-public crate-bevy_dolly-0.0.3 (c (n "bevy_dolly") (v "0.0.3") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_core_pipeline" "bevy_asset" "bevy_scene" "bevy_pbr" "bevy_winit" "bevy_gltf" "bevy_sprite" "png" "ktx2" "zstd" "tonemapping_luts" "bevy_gizmos"))) (k 2)) (d (n "bevy") (r "^0.13") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "bevy_math") (r "^0.13") (d #t) (k 0)) (d (n "bevy_transform") (r "^0.13") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13") (d #t) (k 2)))) (h "1f0p1xchp8ydpn8b17lr6p2nka96dmwawgsq1svdqax9pcp98q3k") (f (quote (("drivers") ("default" "drivers" "helpers")))) (s 2) (e (quote (("helpers" "dep:leafwing-input-manager" "bevy/bevy_pbr"))))))

