(define-module (crates-io be vy bevy_mod_spritesheet) #:use-module (crates-io))

(define-public crate-bevy_mod_spritesheet-0.1.0 (c (n "bevy_mod_spritesheet") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.1") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "1rfi0kji74g2cnd1xi72s47rsyyb50m46blgl90rv11p8lxbvfvc")))

(define-public crate-bevy_mod_spritesheet-0.2.0 (c (n "bevy_mod_spritesheet") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13.1") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.23.4") (d #t) (k 2)) (d (n "bevy_web_asset") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ich4wfdwy031d1lijnpvlmyjbyhp6danjlqws83kf1w44sq2avg") (f (quote (("json-hash" "serde_json") ("json-array" "serde_json") ("default" "json-array" "json-hash"))))))

