(define-module (crates-io be vy bevy_fnplugins) #:use-module (crates-io))

(define-public crate-bevy_fnplugins-0.1.0 (c (n "bevy_fnplugins") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "0bfxjlf8064r7pz3vck5mh8h78jiq9f11lbwkhr8p88ca2p7ah5y")))

(define-public crate-bevy_fnplugins-0.1.1 (c (n "bevy_fnplugins") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "0ljvwh1kryyhm055kp1ybi6b1s8qlljsp7w8rsp2fgd7vc6j5rvw")))

