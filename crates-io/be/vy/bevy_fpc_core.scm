(define-module (crates-io be vy bevy_fpc_core) #:use-module (crates-io))

(define-public crate-bevy_fpc_core-0.1.0 (c (n "bevy_fpc_core") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.21") (f (quote ("simd-stable" "parallel"))) (d #t) (k 0)))) (h "1jfdb5226wwgq1qrpvk99v17ynalb93z2r86afsxrp5mq3a9xbs3")))

(define-public crate-bevy_fpc_core-0.1.1 (c (n "bevy_fpc_core") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_winit" "bevy_core_pipeline"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.22") (f (quote ("dim3" "async-collider"))) (k 0)))) (h "1j80bi4kf43jwr4k9x4dfzck81az5v44zxr53pmi03j19hbnmvlr")))

(define-public crate-bevy_fpc_core-0.1.2 (c (n "bevy_fpc_core") (v "0.1.2") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.23") (f (quote ("dim3" "async-collider"))) (k 0)))) (h "0yvfp83c5jr3q1dvjkb9nswq5s4wbxs0zja4nr9rd5abwm867kaq")))

(define-public crate-bevy_fpc_core-0.1.3 (c (n "bevy_fpc_core") (v "0.1.3") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_core_pipeline"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.25") (f (quote ("dim3" "async-collider"))) (k 0)))) (h "1wi5hd1ia3i0lk5fjbbb8y5ggmr1bxf3swjscf7xzsmbzx8lw7ya")))

