(define-module (crates-io be vy bevy_shader_utils) #:use-module (crates-io))

(define-public crate-bevy_shader_utils-0.1.0 (c (n "bevy_shader_utils") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)))) (h "1an1kdzs77sgmmf27sgpldxn4rq4qmy1imxbkwwlibv764yvqysm")))

(define-public crate-bevy_shader_utils-0.2.0 (c (n "bevy_shader_utils") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)))) (h "1hi3pjwn21si5mi1dyh2m0pdwp56lpqlfsc8zd55rxp2zjcjj3ck")))

(define-public crate-bevy_shader_utils-0.3.0 (c (n "bevy_shader_utils") (v "0.3.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)))) (h "0k74lchji4wnlxdac3j9kqk9y48kljrlldvkr4qh48wsqq6ppxay")))

(define-public crate-bevy_shader_utils-0.5.0 (c (n "bevy_shader_utils") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)))) (h "182mq5m8mipjhj2vmwlrbfwy33940qkc9k8kzw7jbb1ric0kh3sd")))

(define-public crate-bevy_shader_utils-0.5.1 (c (n "bevy_shader_utils") (v "0.5.1") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)))) (h "1xxmbx58jnkqp7b5xwxz6cjsfdj9wacar0dysrvf85igihikzlzf")))

(define-public crate-bevy_shader_utils-0.5.2 (c (n "bevy_shader_utils") (v "0.5.2") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19.0") (d #t) (k 2)))) (h "11gb6v76mib0c42b73y96y89p164x7rdq3y2qvmlbv3l9vcba60b")))

(define-public crate-bevy_shader_utils-0.7.0 (c (n "bevy_shader_utils") (v "0.7.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.23.4") (d #t) (k 2)))) (h "0x4lywp3ddq10z13fc9hc3bws596lz8iyifb9w8lwyxjvs52zb6j")))

