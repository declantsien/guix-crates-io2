(define-module (crates-io be vy bevy_retrograde_ui) #:use-module (crates-io))

(define-public crate-bevy_retrograde_ui-0.0.0 (c (n "bevy_retrograde_ui") (v "0.0.0") (h "0m3m7axj6mbv4dbajbhb94i860zljm1kk3hldf13v80cdxnvf5ay")))

(define-public crate-bevy_retrograde_ui-0.1.0 (c (n "bevy_retrograde_ui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("bevy_winit"))) (k 0)) (d (n "bevy_retrograde_core") (r "^0.1") (d #t) (k 0)) (d (n "bevy_retrograde_macros") (r "^0.1") (d #t) (k 0)) (d (n "bevy_retrograde_text") (r "^0.1") (d #t) (k 0)) (d (n "raui") (r "^0.37.1") (f (quote ("material" "tesselate"))) (d #t) (k 0)))) (h "0nglvdq9dfi6m98v1kdfr43pbj9zy3pd4vbc8fpp4nn44cwwlyc5")))

(define-public crate-bevy_retrograde_ui-0.2.0 (c (n "bevy_retrograde_ui") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("bevy_winit"))) (k 0)) (d (n "bevy_retrograde_core") (r "^0.2") (d #t) (k 0)) (d (n "bevy_retrograde_macros") (r "^0.2") (d #t) (k 0)) (d (n "bevy_retrograde_text") (r "^0.2") (d #t) (k 0)) (d (n "raui") (r "^0.37.1") (f (quote ("material" "tesselate"))) (d #t) (k 0)))) (h "1zdmkm9mp0w878n70g7i838id60fnkcm6myl5pmrzw6j3fibc1bq")))

