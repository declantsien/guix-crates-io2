(define-module (crates-io be vy bevy_missing_texture) #:use-module (crates-io))

(define-public crate-bevy_missing_texture-0.1.0 (c (n "bevy_missing_texture") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11.0") (d #t) (k 2)))) (h "0grl86w3ay47c0g2qwc6icr0890dc6s9g21gzq07dyrr928i9ymp")))

(define-public crate-bevy_missing_texture-0.1.1 (c (n "bevy_missing_texture") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11.0") (d #t) (k 2)))) (h "1yw6g56j1zj4jn286132vqi4ypp5ha14f2kzba2izg2hlqb08pnn")))

(define-public crate-bevy_missing_texture-0.1.2 (c (n "bevy_missing_texture") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11.0") (d #t) (k 2)))) (h "1jza32vjs5nrc61ji48w1k3p2r2wlwisp34nycrg9324mmifjwri")))

