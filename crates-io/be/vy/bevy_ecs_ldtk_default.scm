(define-module (crates-io be vy bevy_ecs_ldtk_default) #:use-module (crates-io))

(define-public crate-bevy_ecs_ldtk_default-0.1.0 (c (n "bevy_ecs_ldtk_default") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1mc26nqiz36597wbb71qmjsm7wz8kmxrpbxfv4l7mxshi106c6ai") (y #t)))

(define-public crate-bevy_ecs_ldtk_default-0.1.1 (c (n "bevy_ecs_ldtk_default") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1kkwgkk6cnxpb5m4n8rfp9g059bha7fwrg3zx3md3z5nkifi73lm") (y #t)))

