(define-module (crates-io be vy bevy_eventlistener_derive) #:use-module (crates-io))

(define-public crate-bevy_eventlistener_derive-0.1.0 (c (n "bevy_eventlistener_derive") (v "0.1.0") (d (list (d (n "bevy_eventlistener_core") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1b68v0z5a4n1m926hb6drxwv5h723glblgsafnhcsl15h04bagvc")))

(define-public crate-bevy_eventlistener_derive-0.1.1 (c (n "bevy_eventlistener_derive") (v "0.1.1") (d (list (d (n "bevy_eventlistener_core") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0p4ghsxs5gz7az2ml6siracm56ympdrvgdm2kksam6qs2ixk4g4b")))

(define-public crate-bevy_eventlistener_derive-0.2.0 (c (n "bevy_eventlistener_derive") (v "0.2.0") (d (list (d (n "bevy_eventlistener_core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ym5xp0j2q1dh8jlxmfhb2xj0i9nfhzc5gjgky4lnzmi7rig6bfp")))

(define-public crate-bevy_eventlistener_derive-0.2.1 (c (n "bevy_eventlistener_derive") (v "0.2.1") (d (list (d (n "bevy_eventlistener_core") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "00xprwl9prl2iif6vcv6md6hqrk0nkabzsz9yaysg809j5vc924w")))

(define-public crate-bevy_eventlistener_derive-0.2.2 (c (n "bevy_eventlistener_derive") (v "0.2.2") (d (list (d (n "bevy_eventlistener_core") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0n42q4xbfsbxwpla7g3i2mj9qn7jhm23my3lq7n53cd19agd6csr")))

(define-public crate-bevy_eventlistener_derive-0.3.0 (c (n "bevy_eventlistener_derive") (v "0.3.0") (d (list (d (n "bevy_eventlistener_core") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0zwkx2shs2qvqjpg4dgjrygj6k52lim24m62k2qix1ia5w410wa0")))

(define-public crate-bevy_eventlistener_derive-0.4.0 (c (n "bevy_eventlistener_derive") (v "0.4.0") (d (list (d (n "bevy_eventlistener_core") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0kfqa7g0rzcnapcbir660s2rn9z0l2d03rccwjvrzjafyvvi6lw4")))

(define-public crate-bevy_eventlistener_derive-0.4.1 (c (n "bevy_eventlistener_derive") (v "0.4.1") (d (list (d (n "bevy_eventlistener_core") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1cng7xk6y1i26f2qg648sch6cnv8r15m86vh3arkdwblcf1ih4y1")))

(define-public crate-bevy_eventlistener_derive-0.5.0 (c (n "bevy_eventlistener_derive") (v "0.5.0") (d (list (d (n "bevy_eventlistener_core") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0hdddsgadr5dp0gq45xpg3mx1l0lkvw81ll8bx0lhdlk679hgqjg")))

(define-public crate-bevy_eventlistener_derive-0.5.1 (c (n "bevy_eventlistener_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "05m6lrzy4c5kla3pvncldyyavx9r3a6rax6xmv4q7m0l5rlzr21v")))

(define-public crate-bevy_eventlistener_derive-0.6.0 (c (n "bevy_eventlistener_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0gdpxsb24vz3rjfcqdyvs2s1m4k03wjw6c2iq44yix76ilw3z8f6")))

(define-public crate-bevy_eventlistener_derive-0.6.1 (c (n "bevy_eventlistener_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "020pywhyn75samsvhzxg5qcl7fmamaxrx1xsx7w5w58k0yq504cv")))

(define-public crate-bevy_eventlistener_derive-0.6.2 (c (n "bevy_eventlistener_derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1cii1sx2makd53879bgylks6a5zk0fkp86z75zfz4gm8di8ping2")))

(define-public crate-bevy_eventlistener_derive-0.7.0 (c (n "bevy_eventlistener_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1aq009ylxzs00rrs5adrhh41l73ijkr1bvq78p5dg98279rvwags")))

