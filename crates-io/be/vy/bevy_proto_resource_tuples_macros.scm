(define-module (crates-io be vy bevy_proto_resource_tuples_macros) #:use-module (crates-io))

(define-public crate-bevy_proto_resource_tuples_macros-0.1.0 (c (n "bevy_proto_resource_tuples_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y0sjya3zcjp2k6fw5xqaq9a20n20a937zr7g3jlsxj7yfzarr23")))

