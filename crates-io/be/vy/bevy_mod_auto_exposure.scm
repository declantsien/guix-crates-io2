(define-module (crates-io be vy bevy_mod_auto_exposure) #:use-module (crates-io))

(define-public crate-bevy_mod_auto_exposure-0.1.0 (c (n "bevy_mod_auto_exposure") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "05f4gsjfj678fnwqy60n4462q6bis7c3b4kl234gddf84kd7fch9")))

(define-public crate-bevy_mod_auto_exposure-0.1.1 (c (n "bevy_mod_auto_exposure") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "0wl0fhmrdymj6z5w39iry45m1fmhlmvlyfkb5sbl15mcw8028wm4") (y #t)))

(define-public crate-bevy_mod_auto_exposure-0.1.2 (c (n "bevy_mod_auto_exposure") (v "0.1.2") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "1kzdzq3mma3x21gssy2mzarjjhmrjxbj87ds3ycmqry4jawyhxz1") (y #t)))

(define-public crate-bevy_mod_auto_exposure-0.1.3 (c (n "bevy_mod_auto_exposure") (v "0.1.3") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "0vm1c1cw07fxjvpxc908k6mrg720xg0mi4d8a04cz2423b3svnm2")))

(define-public crate-bevy_mod_auto_exposure-0.2.0 (c (n "bevy_mod_auto_exposure") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "18qfb5g9dl9azzjx6bccmcqh4y56ph8jxlsc6j1avjz8685vyi1h")))

