(define-module (crates-io be vy bevy_openai) #:use-module (crates-io))

(define-public crate-bevy_openai-0.1.0 (c (n "bevy_openai") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "openai-api-rs") (r "^4.0.4") (d #t) (k 0)))) (h "0v6zdnm4ynpqcwx8aljv88jzjda270nqw854i17wqyz7y47a574q")))

(define-public crate-bevy_openai-0.1.1 (c (n "bevy_openai") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "openai-api-rs") (r "^4.0.4") (d #t) (k 0)))) (h "0jq1sxp02k361qc884j1szcj0kns1794qprj6frxr5rin9118s21")))

