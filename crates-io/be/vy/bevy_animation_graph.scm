(define-module (crates-io be vy bevy_animation_graph) #:use-module (crates-io))

(define-public crate-bevy_animation_graph-0.1.0-alpha.4 (c (n "bevy_animation_graph") (v "0.1.0-alpha.4") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0dbyl7l92frf14jcqw1al3c46dchqj5z4fnrjc6zp5lhjjph8fk8")))

(define-public crate-bevy_animation_graph-0.1.0 (c (n "bevy_animation_graph") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1qxy9b8vkkp4xlpsqr9fimf2rji2amzs1ayv1zk0x87y5hl0lqaj")))

(define-public crate-bevy_animation_graph-0.2.0 (c (n "bevy_animation_graph") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1kpn6mcd2sb24jc6q6syap5xsziwnli13z2p2knw188c20630rx3")))

(define-public crate-bevy_animation_graph-0.3.0 (c (n "bevy_animation_graph") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1f5w1j2h0vnfwcm0bfr8szckj75s878h10d2j6rczbn423s7rlgv")))

