(define-module (crates-io be vy bevy_light_2d) #:use-module (crates-io))

(define-public crate-bevy_light_2d-0.1.0 (c (n "bevy_light_2d") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_winit" "bevy_sprite" "x11"))) (k 0)))) (h "1sknngadq66dr0s19zbyiidalg51g137w2n1nbxacrpxpiyx79aa")))

(define-public crate-bevy_light_2d-0.1.1 (c (n "bevy_light_2d") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_winit" "bevy_sprite" "png" "x11"))) (k 0)))) (h "036q85l40hx65qgp6hxmfivmf9c1v168lzh30s2wzm478zjqvrkg")))

(define-public crate-bevy_light_2d-0.1.2 (c (n "bevy_light_2d") (v "0.1.2") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_winit" "bevy_sprite" "png" "x11"))) (k 0)))) (h "07iqf3b9kpvynlnp9cx9gahqjv4lshz4h1hk7fs0phn7q025py60")))

