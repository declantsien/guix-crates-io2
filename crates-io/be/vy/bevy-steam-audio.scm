(define-module (crates-io be vy bevy-steam-audio) #:use-module (crates-io))

(define-public crate-bevy-steam-audio-0.1.0 (c (n "bevy-steam-audio") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6.1") (d #t) (k 0)) (d (n "steam-audio") (r "^0.3") (d #t) (k 0)))) (h "1yc8szlfnyx7zbw9j3v8bxsivj8xpski3jp2a82l7bkx956pdll2")))

