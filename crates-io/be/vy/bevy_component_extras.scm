(define-module (crates-io be vy bevy_component_extras) #:use-module (crates-io))

(define-public crate-bevy_component_extras-0.1.0 (c (n "bevy_component_extras") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy_mod_raycast") (r "^0.13.1") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "15mh07zx6w9gfns2cnxj6m1kpp647wg7xjgxj3r2j5x74isl4dzs")))

(define-public crate-bevy_component_extras-0.1.1 (c (n "bevy_component_extras") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_core_pipeline"))) (k 0)) (d (n "bevy_mod_raycast") (r "^0.13.1") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "0w249jm9ryf8cwk83fb9wjlkbjc8rmdyp6pj6pr936alc24mpr5g")))

(define-public crate-bevy_component_extras-0.1.2 (c (n "bevy_component_extras") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_core_pipeline"))) (k 0)) (d (n "bevy_mod_raycast") (r "^0.13.1") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "0z98vy9nvirg7k3rk69705b2alnfic97y1455rna0ylzxq485g0s")))

(define-public crate-bevy_component_extras-0.1.3 (c (n "bevy_component_extras") (v "0.1.3") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline"))) (k 0)) (d (n "bevy_mod_raycast") (r "^0.13") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)))) (h "0z69c8pzmvr3xjyj5bx58c2xzg6rxml5wd7hqcsfnl94cy67lpar")))

(define-public crate-bevy_component_extras-0.1.4 (c (n "bevy_component_extras") (v "0.1.4") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline"))) (k 0)) (d (n "bevy_mod_raycast") (r "^0.13") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)))) (h "14bxbfgi79i9bcc87g6kdnxz3w2gdz4qj0r9p8f109dcamd1av52")))

(define-public crate-bevy_component_extras-0.1.5 (c (n "bevy_component_extras") (v "0.1.5") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline"))) (k 0)) (d (n "bevy_mod_raycast") (r "^0.16") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)))) (h "0hhbm8ghklwl77qi4k2z3swwyzm0dqkr6d91j45mgydiw5pzx2w1")))

