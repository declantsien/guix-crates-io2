(define-module (crates-io be vy bevy_assets_tar_zstd) #:use-module (crates-io))

(define-public crate-bevy_assets_tar_zstd-0.1.0 (c (n "bevy_assets_tar_zstd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "bevy") (r "^0.9.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "zstd") (r "^0") (d #t) (k 0)))) (h "07yc9nf0bqiapslpgpp4xkhplnnbkr9da92kzab22mmbmkl0k350")))

