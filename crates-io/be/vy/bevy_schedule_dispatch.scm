(define-module (crates-io be vy bevy_schedule_dispatch) #:use-module (crates-io))

(define-public crate-bevy_schedule_dispatch-0.1.0 (c (n "bevy_schedule_dispatch") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.11") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "retour") (r "^0.3") (f (quote ("static-detour"))) (d #t) (k 2)))) (h "0d9ndj9ncbfz5l80nknp4s0la3ygmdi5jjbcxhbqxda9r5cxl6y8")))

(define-public crate-bevy_schedule_dispatch-0.1.1 (c (n "bevy_schedule_dispatch") (v "0.1.1") (d (list (d (n "bevy_app") (r "^0.11") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "retour") (r "^0.3") (f (quote ("static-detour"))) (d #t) (k 2)))) (h "13gdyq64d9xvam1mi11q6nil01p332mlzl9k9nr10hwji4421b99")))

(define-public crate-bevy_schedule_dispatch-0.1.2 (c (n "bevy_schedule_dispatch") (v "0.1.2") (d (list (d (n "bevy_app") (r "^0.11") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "retour") (r "^0.3") (f (quote ("static-detour"))) (d #t) (k 2)) (d (n "tracing-mutex") (r "^0.3") (d #t) (k 0)))) (h "0b3kic8hpm952iyj9kz9als3gjwgwv62637lqv9wc8377fpk32zp")))

