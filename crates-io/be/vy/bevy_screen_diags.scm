(define-module (crates-io be vy bevy_screen_diags) #:use-module (crates-io))

(define-public crate-bevy_screen_diags-0.1.0 (c (n "bevy_screen_diags") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)))) (h "1v6qgk66cnf4gjszh18c09k6f6nmdss3v962gc9518svfjsvl966")))

(define-public crate-bevy_screen_diags-0.2.0 (c (n "bevy_screen_diags") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)))) (h "10f66ya0cvfc7y6h6zvn7q1k3l1xl38mfd6qysq3kpwnh64ym4yb")))

(define-public crate-bevy_screen_diags-0.3.0 (c (n "bevy_screen_diags") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("render" "bevy_winit" "x11"))) (k 0)))) (h "0npd5vy5qfzpgrg6mn9ks3dknd77ihn2g3yv0qa13dz5dnzpxh00")))

(define-public crate-bevy_screen_diags-0.4.0 (c (n "bevy_screen_diags") (v "0.4.0") (d (list (d (n "bevy") (r "^0.7.0") (f (quote ("render" "bevy_winit" "x11"))) (k 0)))) (h "1f7qw487gkqgk2qr7k7sbsfygh1p39vflssh8wkqjvn8l1mvdlh0")))

(define-public crate-bevy_screen_diags-0.6.0 (c (n "bevy_screen_diags") (v "0.6.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)))) (h "1lmp2j4akcg4rrcg0238rjxsscn8nyycz65ywwql19mml7x4dwg3")))

