(define-module (crates-io be vy bevy_utilitarian) #:use-module (crates-io))

(define-public crate-bevy_utilitarian-0.1.0 (c (n "bevy_utilitarian") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_render"))) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "142r0xfw5a6hakczvlkfn3qzcyaki9cgg2p1xbz7c8w1sy4h1h8m")))

(define-public crate-bevy_utilitarian-0.2.0 (c (n "bevy_utilitarian") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_render"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vfz19pslqycchi3z4j765x519wmagc4vccpzyi0318n0kjkmf83")))

(define-public crate-bevy_utilitarian-0.3.0 (c (n "bevy_utilitarian") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_render"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pai5bjcpjyryx2f27ps5kiygcrzxws73gdg7ry2g9ci4fwj5jhr")))

(define-public crate-bevy_utilitarian-0.4.0 (c (n "bevy_utilitarian") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mk2yd0hv3rn0x4x9fha59pg9ppwx0ck3lsc3v9m4w3k5g6j0jkj")))

