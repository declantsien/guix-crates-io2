(define-module (crates-io be vy bevy_spawnable) #:use-module (crates-io))

(define-public crate-bevy_spawnable-0.1.0 (c (n "bevy_spawnable") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "0nmyr8rllsnpqjpssfbmmd6v9dyjj70fkm6cvpqj6hn8m4d0d05s")))

(define-public crate-bevy_spawnable-0.1.1 (c (n "bevy_spawnable") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "02mpz88v760ga4cl343vr6rbzc94skxamf7l755hr00495mi9bj6")))

(define-public crate-bevy_spawnable-0.1.2 (c (n "bevy_spawnable") (v "0.1.2") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "0g0qdvav7gdvbmibhjv2w0m8icc0dnjpz9hxz0qp6cg79ch55kqk")))

(define-public crate-bevy_spawnable-0.1.3 (c (n "bevy_spawnable") (v "0.1.3") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "1n428kxbhi216jfjd5b9dh05m6wwzyfphy63yv2igac1hilpxwkg")))

