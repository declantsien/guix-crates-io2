(define-module (crates-io be vy bevy_h264) #:use-module (crates-io))

(define-public crate-bevy_h264-0.1.0 (c (n "bevy_h264") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.13") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.13") (d #t) (k 0)) (d (n "bevy_render") (r "^0.13") (d #t) (k 0)) (d (n "bevy_time") (r "^0.13") (d #t) (k 0)) (d (n "openh264") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0psfkfxmmvfjjy8nh1m3qzrjyc4fwkd6479pj3l15gfj9w1a5ggb")))

