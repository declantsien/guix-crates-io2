(define-module (crates-io be vy bevy_mod_gizmos) #:use-module (crates-io))

(define-public crate-bevy_mod_gizmos-0.1.0 (c (n "bevy_mod_gizmos") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)) (d (n "bevy_mod_picking") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1qsb7dwrza822k755x0s76ay6rr4yyfnc169xkn7jil9m0j2n22a")))

(define-public crate-bevy_mod_gizmos-0.1.1 (c (n "bevy_mod_gizmos") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)) (d (n "bevy_mod_picking") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ckhiqqkfvvai3xbhpkkia47pwacg7pzfp0pxp62x6iyvqwpnv9l")))

(define-public crate-bevy_mod_gizmos-0.2.0 (c (n "bevy_mod_gizmos") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0brw9g2k686gw19w23p792w3wc616xy6a999n12kyn7nyj7w6399")))

(define-public crate-bevy_mod_gizmos-0.3.0 (c (n "bevy_mod_gizmos") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1j8x5ysvjsdqyzf49qzv4753fs9rhr1xhjzfgbqx8m7plgz8ydwg")))

(define-public crate-bevy_mod_gizmos-0.3.1 (c (n "bevy_mod_gizmos") (v "0.3.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "02lw116iq0b4ixx8lqlwy8fwfjja6r6hb0fmday6wgp519fs4601")))

(define-public crate-bevy_mod_gizmos-0.4.0 (c (n "bevy_mod_gizmos") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)))) (h "04jdgqsnq1v8ng7iwgb620pz0kfl8ip9k8qkq00l6d7279m4fn0m")))

