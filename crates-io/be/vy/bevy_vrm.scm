(define-module (crates-io be vy bevy_vrm) #:use-module (crates-io))

(define-public crate-bevy_vrm-0.0.1 (c (n "bevy_vrm") (v "0.0.1") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)))) (h "0qmb1cwfcvlfysyqwjy41fv266r86qk2cj6y9ml5fjz32imf7rap")))

(define-public crate-bevy_vrm-0.0.2 (c (n "bevy_vrm") (v "0.0.2") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "goth-gltf") (r "^0.1.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.35") (d #t) (k 0)))) (h "1dpc5qnmj2fj9x8bm5nc93byr59ffppa7pyj3rnkcb3b3rl2lnfn")))

(define-public crate-bevy_vrm-0.0.3 (c (n "bevy_vrm") (v "0.0.3") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "bevy_shader_mtoon") (r "^0.0.1") (d #t) (k 0)) (d (n "goth-gltf") (r "^0.1.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.35") (d #t) (k 0)))) (h "1k4cm6hfnmdk6prmi2cqhvarc9j5nvca86k9ccfcc2rqdd64x191")))

(define-public crate-bevy_vrm-0.0.4 (c (n "bevy_vrm") (v "0.0.4") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "bevy_shader_mtoon") (r "^0.0.1") (d #t) (k 0)) (d (n "goth-gltf") (r "^0.1.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.35") (d #t) (k 0)))) (h "1wphlqnnj8cnc1ym6dx7zbqh6wsqar9l67xq3k0wmdskl4f3x57y")))

(define-public crate-bevy_vrm-0.0.5 (c (n "bevy_vrm") (v "0.0.5") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_shader_mtoon") (r "^0.0.3") (d #t) (k 0)) (d (n "goth-gltf") (r "^0.1.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.35") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13q5bfy2fsapfx4mmvfagni3gmnf509362y8wa0y99lq54ggpbaq")))

(define-public crate-bevy_vrm-0.0.6 (c (n "bevy_vrm") (v "0.0.6") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_shader_mtoon") (r "^0.0.6") (d #t) (k 0)) (d (n "goth-gltf") (r "^0.1.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.35") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i61c366lqfra5r3rpi4dp2ih2511588l3lsl3jc79x5yl9zr369")))

(define-public crate-bevy_vrm-0.0.7 (c (n "bevy_vrm") (v "0.0.7") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_shader_mtoon") (r "^0.0.7") (d #t) (k 0)) (d (n "goth-gltf") (r "^0.1.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.35") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "181zfdab1isxh991pddijd4zyjgjas0qjsb2ymvrcsbysi6q045w")))

(define-public crate-bevy_vrm-0.0.8 (c (n "bevy_vrm") (v "0.0.8") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_shader_mtoon") (r "^0.0.8") (d #t) (k 0)) (d (n "goth-gltf") (r "^0.1.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.35") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zvkldd47ql846bs20crr0n9m96px7fpqpv63fvpkyvrdhjnbjkd")))

(define-public crate-bevy_vrm-0.0.9 (c (n "bevy_vrm") (v "0.0.9") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("animation" "bevy_asset" "bevy_scene" "bevy_pbr"))) (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.25.0") (d #t) (k 2)) (d (n "bevy_gltf_kun") (r "^0.0.12") (f (quote ("import"))) (k 0)) (d (n "bevy_panorbit_camera") (r "^0.16.1") (f (quote ("bevy_egui"))) (d #t) (k 2)) (d (n "bevy_shader_mtoon") (r "^0.0.9") (d #t) (k 0)) (d (n "gltf_kun") (r "^0.0.12") (k 0)) (d (n "gltf_kun_vrm") (r "^0.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_vrm") (r "^0.0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0d172fmb372sj94ddcbs30lrfhx76rlvdh12ik4af2yrgycy62ki")))

(define-public crate-bevy_vrm-0.0.10 (c (n "bevy_vrm") (v "0.0.10") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("animation" "bevy_asset" "bevy_scene" "bevy_pbr"))) (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.25.0") (d #t) (k 2)) (d (n "bevy_gltf_kun") (r "^0.0.12") (f (quote ("import"))) (k 0)) (d (n "bevy_panorbit_camera") (r "^0.16.1") (f (quote ("bevy_egui"))) (d #t) (k 2)) (d (n "bevy_shader_mtoon") (r "^0.0.10") (d #t) (k 0)) (d (n "gltf_kun") (r "^0.0.12") (k 0)) (d (n "gltf_kun_vrm") (r "^0.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_vrm") (r "^0.0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0bdm6dzas4cj40aw44k3n9nm519rqb4s47g9zz5rpz21rabbacms")))

