(define-module (crates-io be vy bevy_wasm_shared) #:use-module (crates-io))

(define-public crate-bevy_wasm_shared-0.9.5 (c (n "bevy_wasm_shared") (v "0.9.5") (h "0kffmdw5jzlfz73ayg9vx204n7zcb6s8qm5mg9hwg03k4sgcr6pr")))

(define-public crate-bevy_wasm_shared-0.9.6 (c (n "bevy_wasm_shared") (v "0.9.6") (h "0nymzjv08yhsxs14303s1hbf853kkyl4fc0x0aicyr7ccmws24sl")))

(define-public crate-bevy_wasm_shared-0.9.7 (c (n "bevy_wasm_shared") (v "0.9.7") (h "1kxw3206g60m09qrx648vfzbnlq98byzla18vbznf3j36kbd0kvz")))

(define-public crate-bevy_wasm_shared-0.9.8 (c (n "bevy_wasm_shared") (v "0.9.8") (h "1l8pa7fq6hfqyk71ibff5vxzhqw70fqh26nhslb9276mvm3cz4qn")))

(define-public crate-bevy_wasm_shared-0.9.9 (c (n "bevy_wasm_shared") (v "0.9.9") (h "0i14bpgy2bim1wr61fnwmy9si074mcg8hk31gqrxbd3svclgmchq")))

(define-public crate-bevy_wasm_shared-0.10.0 (c (n "bevy_wasm_shared") (v "0.10.0") (h "0vkfcajjb7yvh08gaabb3mx66iwawrmjdcpzac7ld789w8744d23")))

(define-public crate-bevy_wasm_shared-0.10.1 (c (n "bevy_wasm_shared") (v "0.10.1") (h "1ycnlfam76nphm6ndyvm8wrpzpvp9la3qs1sza3w4y81g39yiw4d")))

