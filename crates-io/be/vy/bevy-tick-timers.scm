(define-module (crates-io be vy bevy-tick-timers) #:use-module (crates-io))

(define-public crate-bevy-tick-timers-0.1.0 (c (n "bevy-tick-timers") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)))) (h "17fk7bg4wwk6hycnxnca13x4dacql7ndyq01mzgl7hkl8cg20nv3") (y #t)))

(define-public crate-bevy-tick-timers-0.1.1 (c (n "bevy-tick-timers") (v "0.1.1") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)))) (h "153bik918brdz095j90klmxyd2v9by4g457q530z0sja030dv8v5") (y #t)))

(define-public crate-bevy-tick-timers-0.2.0 (c (n "bevy-tick-timers") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)))) (h "0c43zvln5r7gylw1n9j1b3px1za6zhyfdz5xgrrpbkr80rnm6g56") (y #t)))

(define-public crate-bevy-tick-timers-0.3.0 (c (n "bevy-tick-timers") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)))) (h "1fwhs5xafi85zp104ffxn0dd38fwl6gjvgymkcjxyfh26fmsj7lb")))

