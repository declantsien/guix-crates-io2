(define-module (crates-io be vy bevy_entitiles_derive) #:use-module (crates-io))

(define-public crate-bevy_entitiles_derive-0.1.0 (c (n "bevy_entitiles_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (d #t) (k 0)))) (h "1madg9xn9w7g98rayis56h7kzhrnzgq2yjk92k4xvpkijn2pcxgh")))

(define-public crate-bevy_entitiles_derive-0.1.1 (c (n "bevy_entitiles_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (d #t) (k 0)))) (h "1ymkxqg5bmy1dsc257kkjv99p0gg948czyzigv7hv25h0dnz25qv")))

(define-public crate-bevy_entitiles_derive-0.2.0 (c (n "bevy_entitiles_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0k29gmpxikml4m2i3mqmkzzg1ircc20lj0pdjrc5fzyk32595crm")))

(define-public crate-bevy_entitiles_derive-0.3.0 (c (n "bevy_entitiles_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0c17g40rh1y5lxn3g430qbpxqyf4yqqbbfa6vh07qx9ljrqrb8f4")))

(define-public crate-bevy_entitiles_derive-0.4.0 (c (n "bevy_entitiles_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1qp3c92gpf9s4m6zj1hhghn4fgqcknqbppm15issshw45hgzdgxi")))

