(define-module (crates-io be vy bevy_match3) #:use-module (crates-io))

(define-public crate-bevy_match3-0.0.1 (c (n "bevy_match3") (v "0.0.1") (d (list (d (n "bevy") (r "^0.6.1") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yivi9s7vzh5nnxafjap66g5wii39zyiwi4kgwqjgrm8p6m7f8wl")))

(define-public crate-bevy_match3-0.0.2 (c (n "bevy_match3") (v "0.0.2") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ycbzg6m03zlzfrwfplaxgxwksywallzrs5h806w4c4dybhqhh6f")))

(define-public crate-bevy_match3-0.1.0 (c (n "bevy_match3") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qw9i72523b3xfghb4fsh1920plzin4nig3pkw3ic13cmsi7yqrk")))

(define-public crate-bevy_match3-0.2.0 (c (n "bevy_match3") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pkxgf4zn2xfz4wpzadw0b9s7j4a1753y7dhm3k40fk6q5cb9g80")))

(define-public crate-bevy_match3-0.3.0 (c (n "bevy_match3") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18r4507clqmnwss9xch2h47yn9dw8qaah8n5sk0cxdwj0i47dyrn")))

(define-public crate-bevy_match3-0.4.0 (c (n "bevy_match3") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z7rzsklacd21bnscyamv9j4l7w73vhfg80vd56974ng6n32j0hc")))

