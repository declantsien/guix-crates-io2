(define-module (crates-io be vy bevy_xml) #:use-module (crates-io))

(define-public crate-bevy_xml-0.1.0 (c (n "bevy_xml") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1f20hzykns133d7gqigl7g6d660lndhqmm41w9by5xb3v1bi0kg8")))

(define-public crate-bevy_xml-0.2.0 (c (n "bevy_xml") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_sprite" "bevy_scene" "bevy_core_pipeline" "bevy_pbr" "bevy_render"))) (k 0)) (d (n "bevy_sprite3d") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1di5qb81pk4ainasgy6ky9j1b1y2pal3f7fkg7r3miixjwgjpc1g")))

