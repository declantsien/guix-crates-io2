(define-module (crates-io be vy bevy_health_system) #:use-module (crates-io))

(define-public crate-bevy_health_system-0.0.1 (c (n "bevy_health_system") (v "0.0.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "1j30pvdp7661hpb6k6yvr13igvzy8pj5z2gi9sc4j10pdmhrbrxm")))

(define-public crate-bevy_health_system-0.1.1 (c (n "bevy_health_system") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "1lxl2507m1v47h1d3c5j9jpsf298sppfljjyysx0pm3m0cpypm7v")))

(define-public crate-bevy_health_system-0.2.0 (c (n "bevy_health_system") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)))) (h "17445fk4iv688dmxnlzg2dc0hhcraism8xzrjb51v6d9xnax05aa")))

