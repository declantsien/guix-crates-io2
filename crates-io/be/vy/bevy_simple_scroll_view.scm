(define-module (crates-io be vy bevy_simple_scroll_view) #:use-module (crates-io))

(define-public crate-bevy_simple_scroll_view-0.1.0 (c (n "bevy_simple_scroll_view") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "1kqrak0ihfz4n8dp6mds7k29dyjvphmcpksamngrqryd946drkqx")))

