(define-module (crates-io be vy bevyinit) #:use-module (crates-io))

(define-public crate-bevyinit-0.1.0 (c (n "bevyinit") (v "0.1.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s7laklccamq0698ryf0wzgxaxg3f4djmnk87190v9cy5brd5j7k")))

(define-public crate-bevyinit-0.2.0 (c (n "bevyinit") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.18") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0z103ajs61fyxnkqh4had4v1kilgw1ii7fcglyksvqbdqy74krlr")))

