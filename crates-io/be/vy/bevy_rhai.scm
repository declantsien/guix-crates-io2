(define-module (crates-io be vy bevy_rhai) #:use-module (crates-io))

(define-public crate-bevy_rhai-0.1.0 (c (n "bevy_rhai") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.6") (k 0)) (d (n "bevy") (r "^0.6") (f (quote ("dynamic"))) (k 2)) (d (n "rhai") (r "^1.6") (f (quote ("sync"))) (d #t) (k 0)))) (h "14m4r3nwfpdx4zzmjy3lfmm71whsry32p6xypgg8lrxn3cymvyir")))

(define-public crate-bevy_rhai-0.2.0 (c (n "bevy_rhai") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("dynamic"))) (k 2)) (d (n "bevy_app") (r "^0.7") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.7") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.7") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.7") (d #t) (k 0)) (d (n "rhai") (r "^1.6") (f (quote ("sync"))) (d #t) (k 0)))) (h "0w2wpki3gn6fjg1vn5pqlx0lcgkfa5di4w7pxjkszw86hqps735j")))

(define-public crate-bevy_rhai-0.3.0 (c (n "bevy_rhai") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("dynamic"))) (d #t) (k 2)) (d (n "bevy_app") (r "^0.7") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.7") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.7") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.7") (d #t) (k 0)) (d (n "rhai") (r "^1.6") (f (quote ("sync"))) (d #t) (k 0)))) (h "0q5a2gipacdb1zq1jd9xrx2pyqz5qa92nl4g4v386yllhdbz6cj6")))

