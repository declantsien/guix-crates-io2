(define-module (crates-io be vy bevy_simple_2d_outline) #:use-module (crates-io))

(define-public crate-bevy_simple_2d_outline-0.1.0 (c (n "bevy_simple_2d_outline") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.0") (f (quote ("bevy_core_pipeline" "bevy_winit" "bevy_sprite" "bevy_render" "bevy_asset" "bevy_pbr" "png"))) (k 0)))) (h "1xhj14cnb282dvblbr9m7pqygs91msbvr093ziannlhyz6m18bbr")))

(define-public crate-bevy_simple_2d_outline-0.1.1 (c (n "bevy_simple_2d_outline") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9.0") (f (quote ("bevy_core_pipeline" "bevy_winit" "bevy_sprite" "bevy_render" "bevy_asset" "bevy_pbr" "png" "x11"))) (k 0)))) (h "0q3kfjwcfjl133h2afwvvsbhr84ma6ma1y4z0bbfb2w5iiyjlaah")))

