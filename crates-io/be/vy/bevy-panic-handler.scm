(define-module (crates-io be vy bevy-panic-handler) #:use-module (crates-io))

(define-public crate-bevy-panic-handler-0.1.0 (c (n "bevy-panic-handler") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)) (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)))) (h "1zm4r9vz3rybyv41g6kpy6ghv5m3vr0swfp1c6r2zq1cvnphmayx")))

(define-public crate-bevy-panic-handler-1.0.0 (c (n "bevy-panic-handler") (v "1.0.0") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)) (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)))) (h "0ydh4d4zzda3m2m3mc8xa48blhk6bip651rhf4lynrcvzs1ibx1m")))

(define-public crate-bevy-panic-handler-1.0.1 (c (n "bevy-panic-handler") (v "1.0.1") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)) (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)))) (h "0vdfpmzfbramwm3a3v5yvqhxgsczz7rmjf7v2cas8phzf5fl1mx4")))

(define-public crate-bevy-panic-handler-1.0.2 (c (n "bevy-panic-handler") (v "1.0.2") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)) (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)))) (h "1j4ds8gxr068r61hvcgybzazc25fx0wlv4cv6lk5ysbjlc9hg2js")))

(define-public crate-bevy-panic-handler-1.1.0 (c (n "bevy-panic-handler") (v "1.1.0") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)) (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)))) (h "1jn2pzqmxcs1x7gj2qqwn9flzph2mlznar13hlbaxj6mrqg3d55s")))

(define-public crate-bevy-panic-handler-2.0.0 (c (n "bevy-panic-handler") (v "2.0.0") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)) (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)))) (h "0ms9wayljbhgw7x84jfbymdab97z61fd8yk5h9k6cb7gwfwxyq8a")))

(define-public crate-bevy-panic-handler-2.0.1 (c (n "bevy-panic-handler") (v "2.0.1") (d (list (d (n "bevy") (r "^0.11.0") (k 0)) (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)))) (h "0kvjm3igni73bz1r981bpxh76g739b9pqss8zd72az2mkqagch3m")))

(define-public crate-bevy-panic-handler-2.1.0 (c (n "bevy-panic-handler") (v "2.1.0") (d (list (d (n "bevy") (r "^0.12.0") (k 0)) (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)))) (h "1hs909qbg2hfpwgddmgdvlgz0fynm0i2h7y34i2d16n1s7mw9b62")))

(define-public crate-bevy-panic-handler-2.2.0 (c (n "bevy-panic-handler") (v "2.2.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)))) (h "118fjcmq4df2yh759vi92f8rr4i72wa7wmcnmsbnd69v80kkdgs7")))

