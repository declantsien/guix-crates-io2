(define-module (crates-io be vy bevy_bundletree) #:use-module (crates-io))

(define-public crate-bevy_bundletree-0.1.0 (c (n "bevy_bundletree") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_bundletree_derive") (r "^0.1") (d #t) (k 0)))) (h "1rfwv9qm0i7nf0dg2h6xivgfam5g4lppr04ib411d9ylaii45a5b")))

(define-public crate-bevy_bundletree-0.1.1 (c (n "bevy_bundletree") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_bundletree_derive") (r "^0.1") (d #t) (k 0)))) (h "0n4s4whnawrjqwqq64jcwawijbbabks42zab4ip4qsvb6jkb4kdj")))

