(define-module (crates-io be vy bevy_windows_param) #:use-module (crates-io))

(define-public crate-bevy_windows_param-0.1.0 (c (n "bevy_windows_param") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)))) (h "1r5s9maklpnm84aakx9ds11sw5yddhgwisqf5606ij1z9d0xys2k")))

(define-public crate-bevy_windows_param-0.1.1 (c (n "bevy_windows_param") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_ui" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "0m13l3fprg34kfcm6122ddxc6rvg6dm0ai8vhhmnzamr4gc2hyz5")))

