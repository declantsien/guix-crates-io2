(define-module (crates-io be vy bevy-trait-resource) #:use-module (crates-io))

(define-public crate-bevy-trait-resource-0.1.0 (c (n "bevy-trait-resource") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "bevy-trait-resource-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0jf0r3kw6cswcfkvg2mf7iiw9mssvfk3aq9ckfq45jp5rfb4cn3r")))

(define-public crate-bevy-trait-resource-0.1.1 (c (n "bevy-trait-resource") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "bevy-trait-resource-macro") (r "^0.1.0") (d #t) (k 0)))) (h "11xgkz6p60sav7ybhy1n02057hwgkxkcgq0jgimxc8f58z7gdw11")))

(define-public crate-bevy-trait-resource-0.2.0 (c (n "bevy-trait-resource") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.1") (k 0)) (d (n "bevy-trait-resource-macro") (r "^0.1.1") (d #t) (k 0)))) (h "14x4flp9klam5xy197zaq5vmdqxk9dy3cz10a6s7304anmp1i7br")))

(define-public crate-bevy-trait-resource-0.2.1 (c (n "bevy-trait-resource") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12.1") (k 0)) (d (n "bevy-trait-resource-macro") (r "^0.1.1") (d #t) (k 0)))) (h "02gnqq7cmp57d2swd2yjpzbrj8wmdifsbnx4sii0aq3a216dp8ay")))

