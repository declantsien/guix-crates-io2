(define-module (crates-io be vy bevy_lunex_utility) #:use-module (crates-io))

(define-public crate-bevy_lunex_utility-0.0.1 (c (n "bevy_lunex_utility") (v "0.0.1") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "04w0dab6gyckvsmc0562lcckssr808zx82nzii37qv3dgca5cgid")))

(define-public crate-bevy_lunex_utility-0.0.2 (c (n "bevy_lunex_utility") (v "0.0.2") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1msw4vhdyp2ydq3dqzq1r21qdln701g1g773w8j272hvf5bv168x")))

(define-public crate-bevy_lunex_utility-0.0.3 (c (n "bevy_lunex_utility") (v "0.0.3") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "17rcq1kfdb5hjx7nsvyqlmww3rw2vhf0q04yghkpk4g3bsbi6kq0")))

(define-public crate-bevy_lunex_utility-0.0.4 (c (n "bevy_lunex_utility") (v "0.0.4") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1614j6zgbvxrwmbbsjdg13z0lw7zdiiiparm59rzyp859qww65ca")))

(define-public crate-bevy_lunex_utility-0.0.5 (c (n "bevy_lunex_utility") (v "0.0.5") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.5") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1yls5k9scxxjwg3mhmcn5r20mb7kgqaih9j5v897ch3lxv6mqwlv")))

(define-public crate-bevy_lunex_utility-0.0.6 (c (n "bevy_lunex_utility") (v "0.0.6") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.6") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "05mnigfklkhvcksqp86hc4wxjsszm034hpanrs3a2i812s88khs1")))

(define-public crate-bevy_lunex_utility-0.0.7 (c (n "bevy_lunex_utility") (v "0.0.7") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_sprite" "bevy_render" "bevy_text"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0f3gc5kmhn07g8q8api0bfz7s8sak5dh2qhfqnd8g4c7zk4pb7ai")))

(define-public crate-bevy_lunex_utility-0.0.8 (c (n "bevy_lunex_utility") (v "0.0.8") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.8") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "086lzmlm47mzyjg5jmvrq8x7rcwrdy8jvj7dsdyaiizv9xhzca75")))

(define-public crate-bevy_lunex_utility-0.0.9 (c (n "bevy_lunex_utility") (v "0.0.9") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.9") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1ddnq4107kdr9d7rwn1csdmr6p5gb170s8gvkmxx9324aiv3dny1")))

(define-public crate-bevy_lunex_utility-0.0.10 (c (n "bevy_lunex_utility") (v "0.0.10") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.10") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1zmqkdk7ywb1wr12ykhaxbbk750x6a67hfld1z1k7jjjq86g1ziw")))

(define-public crate-bevy_lunex_utility-0.0.11 (c (n "bevy_lunex_utility") (v "0.0.11") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_sprite" "bevy_render" "bevy_text" "bevy_gizmos"))) (k 0)) (d (n "bevy_lunex_core") (r "^0.0.11") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0ixp4yxsrjl697i2i0fiy0wk3bjlipd49x2d0wp6370l4fp111aa")))

