(define-module (crates-io be vy bevy_picking_ui) #:use-module (crates-io))

(define-public crate-bevy_picking_ui-0.1.0 (c (n "bevy_picking_ui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy_picking_core") (r "^0.1") (d #t) (k 0)))) (h "0aqixnsha6mylhgc37w6p7l8avl17y0nd0bgimi2hs6i4b00xibz")))

(define-public crate-bevy_picking_ui-0.2.0 (c (n "bevy_picking_ui") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy_picking_core") (r "^0.2") (d #t) (k 0)))) (h "1v3jb4av32npr16z0pdxpfdy1ls884rxjz19lzyxk14dzdjpv7xm")))

(define-public crate-bevy_picking_ui-0.15.0 (c (n "bevy_picking_ui") (v "0.15.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_ui"))) (k 0)) (d (n "bevy_picking_core") (r "^0.15") (d #t) (k 0)))) (h "12zfw56z0xr4vg01jifnmcayndqdzd66m9rxjfnxz89r75rafcqm")))

(define-public crate-bevy_picking_ui-0.16.0 (c (n "bevy_picking_ui") (v "0.16.0") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "bevy_math") (r "^0.11") (k 0)) (d (n "bevy_picking_core") (r "^0.16") (d #t) (k 0)) (d (n "bevy_render") (r "^0.11") (k 0)) (d (n "bevy_transform") (r "^0.11") (k 0)) (d (n "bevy_ui") (r "^0.11") (k 0)) (d (n "bevy_window") (r "^0.11") (k 0)))) (h "14wkh843323p76idwbbacv2mm66a6np4q4cc1ggfsmykd32zfg89")))

(define-public crate-bevy_picking_ui-0.17.0 (c (n "bevy_picking_ui") (v "0.17.0") (d (list (d (n "bevy_app") (r "^0.12") (k 0)) (d (n "bevy_ecs") (r "^0.12") (k 0)) (d (n "bevy_math") (r "^0.12") (k 0)) (d (n "bevy_picking_core") (r "^0.17") (d #t) (k 0)) (d (n "bevy_render") (r "^0.12") (k 0)) (d (n "bevy_transform") (r "^0.12") (k 0)) (d (n "bevy_ui") (r "^0.12") (k 0)) (d (n "bevy_window") (r "^0.12") (k 0)))) (h "0aw775v6p4csknfb6kr7nval0m2hjasqbpn4kjpiy6q2057kc850")))

(define-public crate-bevy_picking_ui-0.18.0 (c (n "bevy_picking_ui") (v "0.18.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_hierarchy") (r "^0.13") (k 0)) (d (n "bevy_math") (r "^0.13") (k 0)) (d (n "bevy_picking_core") (r "^0.18") (d #t) (k 0)) (d (n "bevy_render") (r "^0.13") (k 0)) (d (n "bevy_transform") (r "^0.13") (k 0)) (d (n "bevy_ui") (r "^0.13") (k 0)) (d (n "bevy_utils") (r "^0.13") (k 0)) (d (n "bevy_window") (r "^0.13") (k 0)))) (h "0yafsqbrjfm4nql0155a3f8kfmng851rip4aydd120rjwd06vbc5")))

