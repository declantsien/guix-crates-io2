(define-module (crates-io be vy bevy_simple_tilemap) #:use-module (crates-io))

(define-public crate-bevy_simple_tilemap-0.1.0 (c (n "bevy_simple_tilemap") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "0ng9qyg0jvspa51g4zzcb5d03fv37m51jhh7c0v58ahb2iisdnma")))

(define-public crate-bevy_simple_tilemap-0.2.0 (c (n "bevy_simple_tilemap") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "01s7vd4rbilglw3lbvrhj944hpnsy89p7gyc9rksg4v38vc4r061")))

(define-public crate-bevy_simple_tilemap-0.3.0 (c (n "bevy_simple_tilemap") (v "0.3.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "0ykabd0yjcjfivxirwhcp8sd81rm26agw8k9zvzw2hrhkvlgj957")))

(define-public crate-bevy_simple_tilemap-0.3.1 (c (n "bevy_simple_tilemap") (v "0.3.1") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "198akj7s2401pw7rrwhrl5m86dwf4z329jj2j2ipdm9hcgzflhag")))

(define-public crate-bevy_simple_tilemap-0.4.0 (c (n "bevy_simple_tilemap") (v "0.4.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "09dv19rmcpw6bcqb8xanyn5p90jvywixvd8rffp3pr2fsd3jzxd9")))

(define-public crate-bevy_simple_tilemap-0.5.0 (c (n "bevy_simple_tilemap") (v "0.5.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "0pnrqwmfs997nhgivcc6bh8hm8sz1c4609m0416737pldy0zpb28")))

(define-public crate-bevy_simple_tilemap-0.5.1 (c (n "bevy_simple_tilemap") (v "0.5.1") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "0gpzyf7crq6giknxrc9yknhyawraz25jbsvv09k9nsxbdx0z093b")))

(define-public crate-bevy_simple_tilemap-0.6.0 (c (n "bevy_simple_tilemap") (v "0.6.0") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1qcr8vrpjmy184hmq8cyba2xxvykw23ii3791vg6v6638l34faxp")))

(define-public crate-bevy_simple_tilemap-0.6.1 (c (n "bevy_simple_tilemap") (v "0.6.1") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1i6bmnwhpk8hrsbf4gkwgm0x43k7xj5kp42gzn339m6nyws52k0q") (y #t)))

(define-public crate-bevy_simple_tilemap-0.6.2 (c (n "bevy_simple_tilemap") (v "0.6.2") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1ry40j4rvj0zlgaqi3zj5d1wjsi4m1ryywxsxnz318s02015p58m")))

(define-public crate-bevy_simple_tilemap-0.7.0 (c (n "bevy_simple_tilemap") (v "0.7.0") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1jzgmpr3m4zmriycrxhghp2yngg1k3z8knzpwllqcqmjrvdrsf9n")))

(define-public crate-bevy_simple_tilemap-0.8.0 (c (n "bevy_simple_tilemap") (v "0.8.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.7.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.9.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0birqjysld8slcz92hjcxq0ybnkfvm7ily9n1ixy3qx3asp5k4h9")))

(define-public crate-bevy_simple_tilemap-0.8.1 (c (n "bevy_simple_tilemap") (v "0.8.1") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.7.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.9.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "17x95qlhzj67qqfijx5hjzfdnr0a6cdvw1zsqwl1wi5365kljl7c")))

(define-public crate-bevy_simple_tilemap-0.9.0 (c (n "bevy_simple_tilemap") (v "0.9.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.9.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0162my9dq7nyn314p2ni9fka003fx822dp8575snw5vw6alni5vd")))

(define-public crate-bevy_simple_tilemap-0.10.0 (c (n "bevy_simple_tilemap") (v "0.10.0") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1bx89svy6fdzlv4kzhqasq234cnmw16mrk68hxj690qsqgxl17wz")))

(define-public crate-bevy_simple_tilemap-0.10.1 (c (n "bevy_simple_tilemap") (v "0.10.1") (d (list (d (n "bevy") (r "^0.9.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9.0") (f (quote ("x11" "png"))) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0d0f06rdawyzr4nbkv6jl9w7gm2q5vd7sx839vfp3jcp57vkxnly")))

(define-public crate-bevy_simple_tilemap-0.11.0 (c (n "bevy_simple_tilemap") (v "0.11.0") (d (list (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10.0") (f (quote ("x11" "png"))) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1rsqca0vgwzv4fssr7qw2vfwn0sxkn8q1gibdzkcg9k0fkz0y2d9")))

(define-public crate-bevy_simple_tilemap-0.12.0 (c (n "bevy_simple_tilemap") (v "0.12.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("x11" "png"))) (k 2)) (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1j6wxvvc68hlxbk24cgikp6wqbn91qyqwad8liyzpgfx0wz2i4xl")))

(define-public crate-bevy_simple_tilemap-0.13.0 (c (n "bevy_simple_tilemap") (v "0.13.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "bevy_sprite" "multi-threaded"))) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("x11" "png"))) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0r0bchkwvdyg0mnf9lgkyawi60nlvzifxvfib3p5ww8rlm1cxyn9")))

(define-public crate-bevy_simple_tilemap-0.14.0 (c (n "bevy_simple_tilemap") (v "0.14.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "bevy_sprite" "multi-threaded"))) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("x11" "png"))) (k 2)) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0dsla59mixa9gqqmbk90jwr1lscbrpayd3fc9i256f8rg1lb72ak")))

