(define-module (crates-io be vy bevy_web_file_drop) #:use-module (crates-io))

(define-public crate-bevy_web_file_drop-0.0.1 (c (n "bevy_web_file_drop") (v "0.0.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_blob_loader") (r "^0.0.3") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.66") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "0djn0dac12jymczlmpbljyx9fla8srks0nhhczqhk50aw8yi4pw4")))

(define-public crate-bevy_web_file_drop-0.0.2 (c (n "bevy_web_file_drop") (v "0.0.2") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_blob_loader") (r "^0.0.3") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.66") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)))) (h "06sdhbgy4f5vpsqfsbaqy2naa5y1iq0mbwx0ribi6c5jqas2k55r")))

(define-public crate-bevy_web_file_drop-0.0.3 (c (n "bevy_web_file_drop") (v "0.0.3") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_blob_loader") (r "^0.0.4") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.66") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)))) (h "0kjqzpyw569sj36vii6cd97jwpfibk2ji05nqg3siay6grmah89z")))

