(define-module (crates-io be vy bevy-tnua-xpbd3d) #:use-module (crates-io))

(define-public crate-bevy-tnua-xpbd3d-0.1.0 (c (n "bevy-tnua-xpbd3d") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.3") (d #t) (k 0)))) (h "1yrrcprm210qlnza22nmrgd72zdzhk96fradsj1dwmnma7j3slil")))

(define-public crate-bevy-tnua-xpbd3d-0.1.1 (c (n "bevy-tnua-xpbd3d") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.3") (d #t) (k 0)))) (h "0yqflkg9qbiwl4kfaspnc56xhiyxfcnrbfvk1vh4wnm4kpssd2xg")))

(define-public crate-bevy-tnua-xpbd3d-0.2.0 (c (n "bevy-tnua-xpbd3d") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.2") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.4") (d #t) (k 0)))) (h "0ij9hblzfwwwyag4vniqj5x4srl1a271jvzd8gq2pl50ii3kbjx3")))

(define-public crate-bevy-tnua-xpbd3d-0.3.0 (c (n "bevy-tnua-xpbd3d") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.4") (f (quote ("3d" "debug-plugin" "parallel" "async-collider"))) (k 0)))) (h "126s44xcbjbj44xgfv1ya1zw370hqwlvlsys5g9x5329k65rk8z2") (f (quote (("f64" "bevy_xpbd_3d/parry-f64" "bevy-tnua-physics-integration-layer/f64") ("default" "bevy_xpbd_3d/parry-f32"))))))

(define-public crate-bevy-tnua-xpbd3d-0.4.0 (c (n "bevy-tnua-xpbd3d") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.4") (f (quote ("3d" "debug-plugin" "parallel" "async-collider"))) (k 0)))) (h "13rsfz9cii738wl48jkl1zjwsmlgpq5s6x0si34qw8wy53ivzi3p") (f (quote (("f64" "bevy_xpbd_3d/parry-f64" "bevy-tnua-physics-integration-layer/f64") ("default" "bevy_xpbd_3d/parry-f32"))))))

