(define-module (crates-io be vy bevy_gizmos_macros) #:use-module (crates-io))

(define-public crate-bevy_gizmos_macros-0.13.0 (c (n "bevy_gizmos_macros") (v "0.13.0") (d (list (d (n "bevy_macro_utils") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0vfjyqj2z8x5bhjch66g2w1zph1xnsics8akhx6nx2jknjw9x51a")))

(define-public crate-bevy_gizmos_macros-0.13.1 (c (n "bevy_gizmos_macros") (v "0.13.1") (d (list (d (n "bevy_macro_utils") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1j80iv989cbd965zi2rkl6c9whx3p8fmy77hs6haamj2fsz8krm3")))

(define-public crate-bevy_gizmos_macros-0.13.2 (c (n "bevy_gizmos_macros") (v "0.13.2") (d (list (d (n "bevy_macro_utils") (r "^0.13.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17v1n5k8pvb3p70rcd5dhq4889i805x7wllk69fald6dv1sazp5b")))

