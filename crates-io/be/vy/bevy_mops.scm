(define-module (crates-io be vy bevy_mops) #:use-module (crates-io))

(define-public crate-bevy_mops-0.1.0 (c (n "bevy_mops") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.2") (d #t) (k 0)) (d (n "bevy_panorbit_camera") (r "^0.17.0") (d #t) (k 2)))) (h "0023rnk8anbg97bscyy8zvd9aqvz360ia6adyqx0pph5pg6fi67q")))

(define-public crate-bevy_mops-0.1.1 (c (n "bevy_mops") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_panorbit_camera") (r "^0.17.0") (d #t) (k 2)))) (h "1kvxhkqhjva67zzbqn06hk6424i93w33gbmy9r5n4ag9pa8f0xac")))

