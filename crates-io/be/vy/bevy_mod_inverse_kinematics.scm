(define-module (crates-io be vy bevy_mod_inverse_kinematics) #:use-module (crates-io))

(define-public crate-bevy_mod_inverse_kinematics-0.1.0 (c (n "bevy_mod_inverse_kinematics") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)) (d (n "bevy_prototype_debug_lines") (r "^0.8.1") (f (quote ("3d"))) (o #t) (d #t) (k 0)))) (h "18dqriqjv5an72rrgyqj3c4llsagyfi626cycwb0i4hjip9lmhfa") (f (quote (("debug_lines" "bevy_prototype_debug_lines"))))))

(define-public crate-bevy_mod_inverse_kinematics-0.1.1 (c (n "bevy_mod_inverse_kinematics") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)) (d (n "bevy_prototype_debug_lines") (r "^0.8.1") (f (quote ("3d"))) (o #t) (d #t) (k 0)))) (h "1afsq4yw3nvnz86ibp1hiid1w2qfd1d9qznawrnglg918rhym619") (f (quote (("debug_lines" "bevy_prototype_debug_lines"))))))

(define-public crate-bevy_mod_inverse_kinematics-0.2.0 (c (n "bevy_mod_inverse_kinematics") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)))) (h "1cw6989dbpqw35v152sqfv0sgcsvzzfvv6fjfyka9kx9i1dxliwh")))

(define-public crate-bevy_mod_inverse_kinematics-0.3.0 (c (n "bevy_mod_inverse_kinematics") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)))) (h "17n5pz02gf8ib6wq9za9700287ixrjay6p86kvbk3hdkh3m6vlkv")))

(define-public crate-bevy_mod_inverse_kinematics-0.4.0 (c (n "bevy_mod_inverse_kinematics") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "0l8sgjadxj5xikdfafybr8fxqwlpp7zdjf1y69aj1x4y9h6hkqy1")))

(define-public crate-bevy_mod_inverse_kinematics-0.5.0 (c (n "bevy_mod_inverse_kinematics") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "00pvj2dmla12vnvyswij0c7bpsy1hz7s5kf677fzc38vln5d2xpd")))

(define-public crate-bevy_mod_inverse_kinematics-0.6.0 (c (n "bevy_mod_inverse_kinematics") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "02ygzbn3wkzs4gshlgz65c36ian06dvbha4i14dmgjb7cy476cln")))

