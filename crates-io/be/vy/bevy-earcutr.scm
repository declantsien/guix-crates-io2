(define-module (crates-io be vy bevy-earcutr) #:use-module (crates-io))

(define-public crate-bevy-earcutr-0.1.0 (c (n "bevy-earcutr") (v "0.1.0") (d (list (d (n "bevy_render") (r "^0.4") (d #t) (k 0)) (d (n "earcutr") (r "^0.1") (d #t) (k 0)))) (h "0abx19q5swyxn0zija5jyhi5zq7flr14ns7cppn2pbh80kq7lg99")))

(define-public crate-bevy-earcutr-0.2.0 (c (n "bevy-earcutr") (v "0.2.0") (d (list (d (n "bevy_render") (r "^0.5") (d #t) (k 0)) (d (n "earcutr") (r "^0.1") (d #t) (k 0)))) (h "0gd64f5zh7x1qck88795pzfbvy5dm7qr3n9qn14w52lnfd0gzkvh")))

(define-public crate-bevy-earcutr-0.3.0 (c (n "bevy-earcutr") (v "0.3.0") (d (list (d (n "bevy_render") (r "^0.6") (d #t) (k 0)) (d (n "earcutr") (r "^0.2") (d #t) (k 0)))) (h "1dgc9ca1r7xa053bc8l1jgsz89pk48xdb1rdhfzzjvjg089592nm")))

(define-public crate-bevy-earcutr-0.3.1 (c (n "bevy-earcutr") (v "0.3.1") (d (list (d (n "bevy_render") (r "^0.6") (d #t) (k 0)) (d (n "earcutr") (r "^0.2") (d #t) (k 0)))) (h "0h14sjgrk6yzccbr550k8r5vd7b28h4n213dxwz84n18cxyz57km")))

(define-public crate-bevy-earcutr-0.4.0 (c (n "bevy-earcutr") (v "0.4.0") (d (list (d (n "bevy_render") (r "^0.6") (k 0)) (d (n "earcutr") (r "^0.2") (d #t) (k 0)))) (h "1cfz3i5bx6bwjc7x39sz91yxcrc6csfjv807q746xwwhil1ray71")))

(define-public crate-bevy-earcutr-0.4.1 (c (n "bevy-earcutr") (v "0.4.1") (d (list (d (n "bevy_render") (r "^0.6") (k 0)) (d (n "earcutr") (r "^0.2") (d #t) (k 0)))) (h "1nrjwpy3gfgkwfw3ra38zpcp181nlvqdmddw482v1i59ki5smlzz")))

(define-public crate-bevy-earcutr-0.5.0 (c (n "bevy-earcutr") (v "0.5.0") (d (list (d (n "bevy_render") (r "^0.7") (k 0)) (d (n "earcutr") (r "^0.2") (d #t) (k 0)))) (h "08fzbc3xaa7j6x66nm3yrkyzal6kx9dhl8ign6b3n970qg3rdh7x")))

(define-public crate-bevy-earcutr-0.6.0 (c (n "bevy-earcutr") (v "0.6.0") (d (list (d (n "bevy_render") (r "^0.8") (k 0)) (d (n "earcutr") (r "^0.2") (d #t) (k 0)))) (h "07l6b2j7qdyj1c8nlimwrbfq5fj6fx377hjx8hzb7n0i0f4y31qb")))

(define-public crate-bevy-earcutr-0.7.0 (c (n "bevy-earcutr") (v "0.7.0") (d (list (d (n "bevy_render") (r "^0.9") (k 0)) (d (n "earcutr") (r "^0.2") (d #t) (k 0)))) (h "0v46jq8njrk4ldkd5h8hi8f1qg065jyviac15jx2liq5bsqn0r6l")))

(define-public crate-bevy-earcutr-0.8.0 (c (n "bevy-earcutr") (v "0.8.0") (d (list (d (n "bevy_render") (r "^0.9") (k 0)) (d (n "earcutr") (r "^0.4") (d #t) (k 0)))) (h "08h9l6r87h9nnf512zf5fr7k5h8gdnamkv6yhcvriiilj2z5j6c9")))

(define-public crate-bevy-earcutr-0.9.0 (c (n "bevy-earcutr") (v "0.9.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (k 0)) (d (n "earcutr") (r "^0.4") (d #t) (k 0)))) (h "1iwmmk534d94m3ifylgjmq2qzd7d08v1kmz44v9wp32zz9pl8iwg")))

(define-public crate-bevy-earcutr-0.10.0 (c (n "bevy-earcutr") (v "0.10.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render"))) (k 0)) (d (n "earcutr") (r "^0.4") (d #t) (k 0)))) (h "0vx4k2vkp6hrd15kclz0ggmpb89izbb3fr3c47dn99yfk2l1zq12")))

(define-public crate-bevy-earcutr-0.11.0 (c (n "bevy-earcutr") (v "0.11.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render"))) (k 0)) (d (n "earcutr") (r "^0.4") (d #t) (k 0)))) (h "1432q4w2zfyiyimi97k18n14vsf1i1dvajj91x31rz6m9izyb9rb")))

(define-public crate-bevy-earcutr-0.12.0 (c (n "bevy-earcutr") (v "0.12.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "earcutr") (r "^0.4") (d #t) (k 0)))) (h "0v43fn9bj82a1bp96515b1iaw1hpr6anh7f22iv9zscda1bwvxqv")))

