(define-module (crates-io be vy bevy_serial) #:use-module (crates-io))

(define-public crate-bevy_serial-0.1.0 (c (n "bevy_serial") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "mio") (r "^0.7") (d #t) (k 0)) (d (n "mio-serial") (r "^5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "serialport") (r "^4.0") (d #t) (k 0)))) (h "0zpmygnpj0dx2r4b07hv5zjqqx3ick7ccbnpm459hhgnh1kdjhaf")))

(define-public crate-bevy_serial-0.2.0 (c (n "bevy_serial") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "mio") (r "^0.7") (d #t) (k 0)) (d (n "mio-serial") (r "^5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "serialport") (r "^4.0") (d #t) (k 0)))) (h "08njwag0mzcsb75z2dngbvhnhzyw7vpizxzyg5js8lqvmw78wlln")))

(define-public crate-bevy_serial-0.2.1 (c (n "bevy_serial") (v "0.2.1") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "mio") (r "^0.7") (d #t) (k 0)) (d (n "mio-serial") (r "^5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "serialport") (r "^4.0") (d #t) (k 0)))) (h "1l22m237g8h01lnv6xinz6qzc4lh01w17y53gxzb6lhx3ld8fdjb")))

(define-public crate-bevy_serial-0.3.0 (c (n "bevy_serial") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "mio") (r "^0.8") (d #t) (k 0)) (d (n "mio-serial") (r "^5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "serialport") (r "^4.2") (d #t) (k 0)))) (h "02wwryb0c276siiapjxq1pn65jj850x3v00knx5vzjnjzmj2s3ay")))

(define-public crate-bevy_serial-0.4.0 (c (n "bevy_serial") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "mio") (r "^0.8") (d #t) (k 0)) (d (n "mio-serial") (r "^5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "serialport") (r "^4.2") (d #t) (k 0)))) (h "0d6w08lkvwivr9lsijhzs58krwi9nw0awmg89m0jsw0sgpxyhs71")))

(define-public crate-bevy_serial-0.5.0 (c (n "bevy_serial") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "mio") (r "^0.8") (d #t) (k 0)) (d (n "mio-serial") (r "^5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "serialport") (r "^4.2") (d #t) (k 0)))) (h "15kqlviy0mi4gk0mqajr5jzk04x2lqjdz3cavbfbkb551x2cfkkf")))

