(define-module (crates-io be vy bevy_ecs_markers_macros) #:use-module (crates-io))

(define-public crate-bevy_ecs_markers_macros-0.1.0 (c (n "bevy_ecs_markers_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1g8aks2agr3lnnh1g45qjagfwhnxgijc1ia65fqp2v259hcczgx8")))

(define-public crate-bevy_ecs_markers_macros-0.1.1 (c (n "bevy_ecs_markers_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1wsl3rj1nf5dww2g2wlr0r7z2mp579zzgrpnb0rb1bclz6yh7vnq")))

(define-public crate-bevy_ecs_markers_macros-0.1.2 (c (n "bevy_ecs_markers_macros") (v "0.1.2") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "08r498q2h3z0f7j6n2kngg0ad49jasvdr4q7ljvyzn717xcnxj1g")))

(define-public crate-bevy_ecs_markers_macros-0.1.3 (c (n "bevy_ecs_markers_macros") (v "0.1.3") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1p02ag2ci6iv18qj5k7kgjlx4alzzns08n9lqhs5a50fylxk4zzp")))

(define-public crate-bevy_ecs_markers_macros-0.1.4 (c (n "bevy_ecs_markers_macros") (v "0.1.4") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1m9y1ix75xah61n6fnw1vsm761vpzzr6ic23c3qg65904bqpjhqn")))

(define-public crate-bevy_ecs_markers_macros-1.0.0 (c (n "bevy_ecs_markers_macros") (v "1.0.0") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0zbzxmhxijg518ylf3qy2d4ivvm6yh8l9k8izyq63sb9d7yaz2db")))

(define-public crate-bevy_ecs_markers_macros-1.0.1 (c (n "bevy_ecs_markers_macros") (v "1.0.1") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1bxfdb0z374hl3ymvnlbr07ji2wzqb3094960wmc41ypkidbimm3")))

(define-public crate-bevy_ecs_markers_macros-1.0.2 (c (n "bevy_ecs_markers_macros") (v "1.0.2") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0mmk7x06af7fhawjxyd3a8rz458q021qcx1d0lqikv30gdf49f9l")))

(define-public crate-bevy_ecs_markers_macros-1.0.3 (c (n "bevy_ecs_markers_macros") (v "1.0.3") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1n66hi5dzkbq7pvs2iqasyrij17m8bm499c4gazcymw4a3wdyy09")))

(define-public crate-bevy_ecs_markers_macros-1.0.4 (c (n "bevy_ecs_markers_macros") (v "1.0.4") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1g34r2zdawv9xy5v6pgalgvlf1dacgha5rbvsk942rb7a8n99rh2")))

(define-public crate-bevy_ecs_markers_macros-2.0.0 (c (n "bevy_ecs_markers_macros") (v "2.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1ar29d9i3mj602z3id9kmdv4ng9ycrwwxjq405xqkjisp3viwxfb")))

