(define-module (crates-io be vy bevy_verlet) #:use-module (crates-io))

(define-public crate-bevy_verlet-0.1.0 (c (n "bevy_verlet") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy") (r "^0.5") (f (quote ("render" "bevy_winit" "png" "x11" "bevy_wgpu"))) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1pfxkb9fc7gcns3by6lw746ybzwgvwxfbnc31zgax4yz4y2g43rb") (f (quote (("default") ("debug" "bevy_prototype_debug_lines"))))))

(define-public crate-bevy_verlet-0.1.1 (c (n "bevy_verlet") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy") (r "^0.5") (f (quote ("render" "bevy_winit" "png" "x11" "bevy_wgpu"))) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0kwnm6wga233j8pkchmynza5aws1gbszl6jfkfzazl2vwgv011c2") (f (quote (("default") ("debug" "bevy_prototype_debug_lines"))))))

(define-public crate-bevy_verlet-0.2.0 (c (n "bevy_verlet") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "bevy") (r "^0.6") (f (quote ("render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0p6j2nlglrqrkb0z1kb6wpgdpgsjaxmwfbqgriiqfx721l34gwpw") (f (quote (("default") ("debug" "bevy_prototype_debug_lines"))))))

(define-public crate-bevy_verlet-0.3.0 (c (n "bevy_verlet") (v "0.3.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1rhn44hpkx4pipxjvzvk716zr44gl9b2ndb27xmpyqhvsyk11bsq") (f (quote (("default") ("debug" "bevy_prototype_debug_lines"))))))

(define-public crate-bevy_verlet-0.4.0 (c (n "bevy_verlet") (v "0.4.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0k48lpfqcfp48q4m3zkbsjmgwwz8k0zbbjkxc9ryq3xdzcbssiz3") (f (quote (("default") ("debug" "bevy_prototype_debug_lines"))))))

(define-public crate-bevy_verlet-0.5.0 (c (n "bevy_verlet") (v "0.5.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.9") (o #t) (d #t) (k 0)))) (h "143nbghfhvl6121pxw6ifmj3nlpg191dn79w1sr43i8h0vxdcy5l") (f (quote (("default") ("debug" "bevy_prototype_debug_lines"))))))

(define-public crate-bevy_verlet-0.6.0 (c (n "bevy_verlet") (v "0.6.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_winit" "x11" "bevy_core_pipeline" "bevy_sprite" "bevy_pbr"))) (k 2)))) (h "0by6j4pqzc5yfph6mgbrxdz9kd9x007mscdrf2n5855nm1azxa76") (f (quote (("default") ("debug" "bevy/bevy_gizmos" "bevy/bevy_render"))))))

(define-public crate-bevy_verlet-0.6.1 (c (n "bevy_verlet") (v "0.6.1") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_winit" "x11" "bevy_core_pipeline" "bevy_sprite" "bevy_pbr"))) (k 2)))) (h "1502vankzd81dhcnmjrx4mn84mmx4h4k5bc0rjpnnmgrhx5vmznz") (f (quote (("default") ("debug" "bevy/bevy_gizmos" "bevy/bevy_render"))))))

(define-public crate-bevy_verlet-0.7.0 (c (n "bevy_verlet") (v "0.7.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_winit" "x11" "bevy_core_pipeline" "bevy_sprite" "bevy_pbr"))) (k 2)))) (h "1fpjgdf54hv70534ikidiq81g6m1iw51nfkjdy6ynvk2bfvaacjn") (f (quote (("default") ("debug" "bevy/bevy_gizmos" "bevy/bevy_render"))))))

(define-public crate-bevy_verlet-0.8.0 (c (n "bevy_verlet") (v "0.8.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_winit" "bevy_core_pipeline" "bevy_sprite" "bevy_pbr" "x11" "multi-threaded" "tonemapping_luts" "dynamic_linking"))) (k 2)))) (h "02nqb9kqk6y1x6nqa42ypca6nnakhh73qbpm25b9s0zfvpkzwzis") (f (quote (("default") ("debug" "bevy/bevy_gizmos" "bevy/bevy_render"))))))

