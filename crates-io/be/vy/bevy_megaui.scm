(define-module (crates-io be vy bevy_megaui) #:use-module (crates-io))

(define-public crate-bevy_megaui-0.1.0 (c (n "bevy_megaui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)) (d (n "megaui") (r "^0.2.14") (d #t) (k 0)))) (h "1nyb6160a48hvqichkrdkp08qm9xr7znsrs1ypqaya3zmv5zlg3n")))

(define-public crate-bevy_megaui-0.1.1 (c (n "bevy_megaui") (v "0.1.1") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)) (d (n "megaui") (r "^0.2.15") (d #t) (k 0)))) (h "12r551daz1rjws8iz5i22bv2rncbr5i0i1c4s8vkq6rxil4hxd7a")))

(define-public crate-bevy_megaui-0.1.2 (c (n "bevy_megaui") (v "0.1.2") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)) (d (n "megaui") (r "^0.2.16") (d #t) (k 0)))) (h "14zas79jfnms7vzslnfrrwli8gb57di7fczpwbz94g02khwk32hn")))

(define-public crate-bevy_megaui-0.1.3 (c (n "bevy_megaui") (v "0.1.3") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)) (d (n "megaui") (r "^0.2.16") (d #t) (k 0)))) (h "18p681wins7fv26jv3brq0060y9q4z6p87rq2w0kyx0qf4w0wkss")))

(define-public crate-bevy_megaui-0.1.4 (c (n "bevy_megaui") (v "0.1.4") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)) (d (n "megaui") (r "^0.2.16") (d #t) (k 0)))) (h "0fzxzq3hsr182ffkj2r0qayxjgs7ni7d1a44m2v7r6a6i501srhk")))

