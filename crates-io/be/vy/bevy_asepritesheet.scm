(define-module (crates-io be vy bevy_asepritesheet) #:use-module (crates-io))

(define-public crate-bevy_asepritesheet-0.2.0 (c (n "bevy_asepritesheet") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08lba11365qql3y0h0lpxr8wnp7zlgqw5brnnwflm5xijpw4z5jd")))

(define-public crate-bevy_asepritesheet-0.3.0 (c (n "bevy_asepritesheet") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17lfa11yrwyp4rq6vbazjb8iwndwymwxpbpxpmzb4ib1945xw6nc")))

(define-public crate-bevy_asepritesheet-0.4.0 (c (n "bevy_asepritesheet") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gdmb1n8syplxajlk0kvg6qiqbfiq9dzwkqrwylrhz575zdkyqkm")))

(define-public crate-bevy_asepritesheet-0.4.1 (c (n "bevy_asepritesheet") (v "0.4.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1k5j42hmnp5f4bagv50nva6v5bcpsrim2h3g6g0g5zlaz681zqf1")))

(define-public crate-bevy_asepritesheet-0.4.2 (c (n "bevy_asepritesheet") (v "0.4.2") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "144wh5rqsd778kap19mqj0q48gq0lfn0pb3n4aah9gdzm5m804vw")))

(define-public crate-bevy_asepritesheet-0.5.0 (c (n "bevy_asepritesheet") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16f297z6jw6c0pjnsrh3zxs4a74rvf19gxaw03dr2s1is6wzfawn")))

(define-public crate-bevy_asepritesheet-0.5.1 (c (n "bevy_asepritesheet") (v "0.5.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ff9zhkpwg4ppy1wlkr8igy620ys7qkyzhjlbq5zhz5pr8pdx321")))

(define-public crate-bevy_asepritesheet-0.5.2 (c (n "bevy_asepritesheet") (v "0.5.2") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1h5778mfrdlnxdndrl6545b0dbv8x6gcwkwl9np3n1saf4qq46x9")))

(define-public crate-bevy_asepritesheet-0.6.0 (c (n "bevy_asepritesheet") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09f0z9b6z7z84vgvrsl1ks6ys16ahzs10j5b4kdcy9cnjlgzak3f")))

