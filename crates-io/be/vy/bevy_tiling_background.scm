(define-module (crates-io be vy bevy_tiling_background) #:use-module (crates-io))

(define-public crate-bevy_tiling_background-0.0.9 (c (n "bevy_tiling_background") (v "0.0.9") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render" "bevy_winit" "png"))) (k 2)))) (h "1zymac1j6l0r14vd14cxqa4p39pjysnwaj8rsvphb5lz4jibr026") (y #t)))

(define-public crate-bevy_tiling_background-0.9.1 (c (n "bevy_tiling_background") (v "0.9.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render" "bevy_winit" "png"))) (k 2)))) (h "04jvn63yswjnb3fs2d9nawvazn6nznrlap3p77hbyzc7mrxh9xq2")))

(define-public crate-bevy_tiling_background-0.9.2 (c (n "bevy_tiling_background") (v "0.9.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render" "bevy_winit" "png"))) (k 2)))) (h "19q7x8shdmcjc2l8wx4wbz9hfbhaz7p5fx1nrw6xsvndrx2pp12i")))

(define-public crate-bevy_tiling_background-0.10.0 (c (n "bevy_tiling_background") (v "0.10.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_render" "bevy_core_pipeline" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_render" "bevy_core_pipeline" "bevy_sprite" "bevy_winit" "bevy_text" "bevy_ui" "png"))) (k 2)))) (h "1jfchydj98paax6p4v395ajnz584aqxvbqkvmljnkigy0x4ggyj0")))

