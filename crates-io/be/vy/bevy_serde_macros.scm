(define-module (crates-io be vy bevy_serde_macros) #:use-module (crates-io))

(define-public crate-bevy_serde_macros-0.1.0 (c (n "bevy_serde_macros") (v "0.1.0") (d (list (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1qa7a9xybfrsw0l2zdcag9kgdcqapgj5cdbbzbpmaiafxidc9vqm")))

(define-public crate-bevy_serde_macros-0.2.0 (c (n "bevy_serde_macros") (v "0.2.0") (d (list (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1l20vb76xbzr7dhdkpkmdgyg187dg4vv1d8f94f7n392zxw9a27y")))

(define-public crate-bevy_serde_macros-0.2.1 (c (n "bevy_serde_macros") (v "0.2.1") (d (list (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "072vpfvh3gnkf8aqfl176aqkpbkd8sps3w0bbd7spdgqpyjba3qa")))

(define-public crate-bevy_serde_macros-0.2.2 (c (n "bevy_serde_macros") (v "0.2.2") (d (list (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "07g5q05lc982xw4lqbmhvnqjzf1npd3yq5iylf9va7rnyq2x9829")))

