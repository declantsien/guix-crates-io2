(define-module (crates-io be vy bevy_hexasphere) #:use-module (crates-io))

(define-public crate-bevy_hexasphere-0.1.0 (c (n "bevy_hexasphere") (v "0.1.0") (d (list (d (n "glam") (r "^0.13.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "01x1plifbm7rfxgag5ys6cfjwcz4rsi10vjds1j1wks3s2kpng7a")))

(define-public crate-bevy_hexasphere-0.1.1 (c (n "bevy_hexasphere") (v "0.1.1") (d (list (d (n "glam") (r "^0.13.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0gbcfs38dg8bc4x9hygm325c8yx1lym0nkynxmyg89jp6jw82lks")))

(define-public crate-bevy_hexasphere-0.1.2 (c (n "bevy_hexasphere") (v "0.1.2") (d (list (d (n "glam") (r "^0.13.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1svxppryqfl55sidly914rh342yvn6i3mmm7k7lhjv02k9gs9xlf")))

