(define-module (crates-io be vy bevy_ui_3d) #:use-module (crates-io))

(define-public crate-bevy_ui_3d-0.1.0 (c (n "bevy_ui_3d") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.22.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.1") (d #t) (k 0)))) (h "0x0zn7xh1i8vcadi584bjylyxisw8va32r4d4qwc198kd20i0g0l")))

(define-public crate-bevy_ui_3d-0.1.1 (c (n "bevy_ui_3d") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.22.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.1") (d #t) (k 0)))) (h "0xp3x0412ns4v16yhrbvp2yj6r4lszc542z7lk9sy5n3s9jwvs1k")))

(define-public crate-bevy_ui_3d-0.1.2 (c (n "bevy_ui_3d") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.22.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.1") (d #t) (k 0)))) (h "0pzyalgsg2zs2030v398hxp63f8wgks1lpaki60x2f27xsv7axmy")))

(define-public crate-bevy_ui_3d-0.2.0 (c (n "bevy_ui_3d") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.23.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.1") (d #t) (k 0)))) (h "0gv1zwy3mik52yamyf6r4jw60akajym2jagghsbh83dr7037gz73")))

