(define-module (crates-io be vy bevy_prototype_lyon) #:use-module (crates-io))

(define-public crate-bevy_prototype_lyon-0.1.1 (c (n "bevy_prototype_lyon") (v "0.1.1") (d (list (d (n "bevy") (r "^0.1.3") (d #t) (k 0)) (d (n "lyon") (r "^0.16.0") (d #t) (k 0)))) (h "1sj5zbxd065q08f2vx1ys88nlw7ayrdd5v9g9dhy4mzshxmxsw4r")))

(define-public crate-bevy_prototype_lyon-0.1.2 (c (n "bevy_prototype_lyon") (v "0.1.2") (d (list (d (n "bevy") (r "^0.2.0") (d #t) (k 0)) (d (n "lyon") (r "^0.16.0") (d #t) (k 0)))) (h "0wss7fsfdqxs4ml90s29smbsk2jcc4kx3p6l42aisgfk6dn07pgy")))

(define-public crate-bevy_prototype_lyon-0.1.3 (c (n "bevy_prototype_lyon") (v "0.1.3") (d (list (d (n "bevy") (r "^0.3.0") (d #t) (k 0)) (d (n "lyon") (r "^0.16.0") (d #t) (k 0)))) (h "1ssqrhlz00wd7zx68vpqimx8mgnryl2a96kx3g5y2wbwdfq9jah7")))

(define-public crate-bevy_prototype_lyon-0.1.4 (c (n "bevy_prototype_lyon") (v "0.1.4") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 2)) (d (n "lyon") (r "^0.16") (d #t) (k 0)))) (h "13wygfgck8w9pbngdwg1v77qdsn2a15ri31ws7biiyh0bpv0m9cj")))

(define-public crate-bevy_prototype_lyon-0.1.5 (c (n "bevy_prototype_lyon") (v "0.1.5") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)))) (h "1n80mql2g9vs9z24fqvy26ccy7jmqwgfj1idsjb4r4gis0wnmmqx")))

(define-public crate-bevy_prototype_lyon-0.2.0 (c (n "bevy_prototype_lyon") (v "0.2.0") (d (list (d (n "bevy") (r "^0.4") (k 0)) (d (n "bevy") (r "^0.4") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.1") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.7") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1y64l1pmzrr5f4kz9b8vy1ij66iizlxnx7cm51ciliz35p4pvmjz")))

(define-public crate-bevy_prototype_lyon-0.3.0 (c (n "bevy_prototype_lyon") (v "0.3.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.5") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5") (d #t) (k 0)))) (h "1ap355pbabz05ip19sdh8hrn552a08nhgwxz1hmjkh840xlv7xsk")))

(define-public crate-bevy_prototype_lyon-0.3.1 (c (n "bevy_prototype_lyon") (v "0.3.1") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.5") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5") (d #t) (k 0)))) (h "0bhsk6xkbzisd65aajfl2kzlphj6qk3gwz1qw5cdnca0fmdzbdqv")))

(define-public crate-bevy_prototype_lyon-0.4.0 (c (n "bevy_prototype_lyon") (v "0.4.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5") (d #t) (k 0)))) (h "15b34n53h5yg1qadq9hy8x0pajqq8ww211sk4rj509xaym3yb8zl")))

(define-public crate-bevy_prototype_lyon-0.5.0 (c (n "bevy_prototype_lyon") (v "0.5.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("bevy_sprite" "bevy_render" "bevy_core_pipeline"))) (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("x11"))) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5") (d #t) (k 0)))) (h "0rndvfzh0n8v62g8caldmdvh14igrhy49647zis8h384pak05ad0")))

(define-public crate-bevy_prototype_lyon-0.6.0 (c (n "bevy_prototype_lyon") (v "0.6.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_sprite" "bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("x11" "bevy_asset"))) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5") (d #t) (k 0)))) (h "1l3qbfx3gp39h40mlfmvbrpcfig8fp46nzipv5713ixsziglzr7r")))

(define-public crate-bevy_prototype_lyon-0.7.0 (c (n "bevy_prototype_lyon") (v "0.7.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_sprite" "bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("x11" "bevy_asset"))) (k 2)) (d (n "lyon_tessellation") (r "^1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.8") (d #t) (k 0)))) (h "1sqzhbxdq01gvrpryh3jqq0rlhdyj8mjgjqib9k46ajii9lj8yrs")))

(define-public crate-bevy_prototype_lyon-0.7.1 (c (n "bevy_prototype_lyon") (v "0.7.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_sprite" "bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("x11" "bevy_asset"))) (k 2)) (d (n "lyon_tessellation") (r "^1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.8") (d #t) (k 0)))) (h "00smjh0kn799y7daccdjjramzq3pg11n6j46j0d4l9mdssd461dz")))

(define-public crate-bevy_prototype_lyon-0.7.2 (c (n "bevy_prototype_lyon") (v "0.7.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_sprite" "bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("x11" "bevy_asset"))) (k 2)) (d (n "lyon_tessellation") (r "^1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.8") (d #t) (k 0)))) (h "0hi4vcxirklfchh7zkr8dxpidwpg02c6dkzrmmf0swna23zkl1hc")))

(define-public crate-bevy_prototype_lyon-0.8.0 (c (n "bevy_prototype_lyon") (v "0.8.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_sprite" "bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("x11" "bevy_asset"))) (k 2)) (d (n "lyon_algorithms") (r "^1") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.8") (d #t) (k 0)))) (h "17rxkc3g4vzpmkniappbfkf2qwrhglad41ndps6pvxnyik3rp7vr")))

(define-public crate-bevy_prototype_lyon-0.9.0 (c (n "bevy_prototype_lyon") (v "0.9.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_sprite" "bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("x11" "bevy_asset"))) (k 2)) (d (n "lyon_algorithms") (r "^1") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.8") (d #t) (k 0)))) (h "023kyyz0crpxbxmzq0ani9aspw7fxy63ifklyz2mvq7dr8b7qd4y")))

(define-public crate-bevy_prototype_lyon-0.10.0 (c (n "bevy_prototype_lyon") (v "0.10.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_sprite" "bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("x11" "bevy_asset"))) (k 2)) (d (n "lyon_algorithms") (r "^1") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.12") (d #t) (k 0)))) (h "1z3h4gkzcc748x3s8f5c0q6ymvjlq4qp6wk8f000j0fypgaidj8z")))

(define-public crate-bevy_prototype_lyon-0.11.0 (c (n "bevy_prototype_lyon") (v "0.11.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_sprite" "bevy_render" "bevy_core_pipeline" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("x11" "bevy_asset"))) (k 2)) (d (n "lyon_algorithms") (r "^1") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.12") (d #t) (k 0)))) (h "13l7wyb97q997bkcmr5qazv9g7g5ccrhgka0zpxhflb8n1dcfvp9")))

