(define-module (crates-io be vy bevy_mod_aseprite) #:use-module (crates-io))

(define-public crate-bevy_mod_aseprite-0.1.0 (c (n "bevy_mod_aseprite") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "1v20r0ffj1505i0ivl3rpw623a8wa83lr2x2y22q26kzn6598ala")))

(define-public crate-bevy_mod_aseprite-0.2.0 (c (n "bevy_mod_aseprite") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "09dlmli3ijjnrc3sz27gxmipvs43msf7hlvmkhnv5pjcyc088hla")))

(define-public crate-bevy_mod_aseprite-0.3.0 (c (n "bevy_mod_aseprite") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "12788b8wchjr2ghgdqhxxn562lm2f5krdkivfh570y82flswss7d")))

(define-public crate-bevy_mod_aseprite-0.4.0 (c (n "bevy_mod_aseprite") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.2") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "122476siyppmj1fwlizdxsrqilvss9mdqpz9hj886c72la3r9vvg")))

(define-public crate-bevy_mod_aseprite-0.4.1 (c (n "bevy_mod_aseprite") (v "0.4.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.3") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "052pjh8wi7fsw7b2w6277hg1h2cyz746axvnlvllp794dsfx74kj")))

(define-public crate-bevy_mod_aseprite-0.5.0 (c (n "bevy_mod_aseprite") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.3") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "13v6y37ifvc1rc8qk8m9cbhm04y5jhk473fvnaac75kjinmdqxsn")))

(define-public crate-bevy_mod_aseprite-0.6.0 (c (n "bevy_mod_aseprite") (v "0.6.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.3") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "1zwk2ca7psvw14mndk53jj1z31v2jf7r9q6ndqkv2xlvsddyd3d1")))

(define-public crate-bevy_mod_aseprite-0.7.0 (c (n "bevy_mod_aseprite") (v "0.7.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.3") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "1ls6a1nfrrrrigi2kh1f88k2z4887nj9nyhs9f81vqijk2pzjvd1")))

(define-public crate-bevy_mod_aseprite-0.7.1 (c (n "bevy_mod_aseprite") (v "0.7.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.3") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "1crv6cb34hd9p90zb144pq1wsmf3yng4xvglwci9gfx3fpx8ngsr")))

(define-public crate-bevy_mod_aseprite-0.7.2 (c (n "bevy_mod_aseprite") (v "0.7.2") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_aseprite_derive") (r "^0.3") (d #t) (k 0)) (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)))) (h "0la0yvc9x711b6kxchripzba6mhwak4v4q5fnivdrrgi4ss2gn59")))

