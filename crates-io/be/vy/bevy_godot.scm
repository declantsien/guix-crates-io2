(define-module (crates-io be vy bevy_godot) #:use-module (crates-io))

(define-public crate-bevy_godot-0.1.0 (c (n "bevy_godot") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "18d2wzi0mb4bj78bqcw5wpjzy9frp0mmzws5qyqzjl4g6j67m1bq")))

