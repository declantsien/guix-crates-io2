(define-module (crates-io be vy bevy_vach_assets) #:use-module (crates-io))

(define-public crate-bevy_vach_assets-0.1.0 (c (n "bevy_vach_assets") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset"))) (k 0)) (d (n "futures-lite") (r "^2.1.0") (f (quote ("std"))) (k 0)) (d (n "vach") (r "^0.5.0") (f (quote ("compression" "crypto"))) (d #t) (k 0)))) (h "172n58a10x813p8mzgdrhi93fkmb5z2n2h4lpbdps6vjnlw6dbds")))

(define-public crate-bevy_vach_assets-0.1.1 (c (n "bevy_vach_assets") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset"))) (k 0)) (d (n "futures-lite") (r "^2.1.0") (f (quote ("std"))) (k 0)) (d (n "vach") (r "^0.5.0") (f (quote ("compression" "crypto"))) (d #t) (k 0)))) (h "1xz685vjp3clfhqlrrm4l9hgjx9bi90xwrm9w0wfsfl4wc0s9ndh")))

(define-public crate-bevy_vach_assets-0.1.2 (c (n "bevy_vach_assets") (v "0.1.2") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset"))) (k 0)) (d (n "futures-lite") (r "^2.1.0") (f (quote ("std"))) (k 0)) (d (n "vach") (r "^0.5.0") (f (quote ("compression" "crypto"))) (d #t) (k 0)))) (h "0xrmm79malspjn0vivvq68wcfk9jrgb1y97fzh3ymisp43sl53pc")))

(define-public crate-bevy_vach_assets-0.1.3 (c (n "bevy_vach_assets") (v "0.1.3") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset"))) (k 0)) (d (n "futures-lite") (r "^2.1.0") (f (quote ("std"))) (k 0)) (d (n "vach") (r "^0.5.0") (f (quote ("compression" "crypto"))) (d #t) (k 0)))) (h "03wjf16vxaccjws0nfaf6vnhnrc25saw0zrz83l5r68vcly82zmr")))

(define-public crate-bevy_vach_assets-0.1.4 (c (n "bevy_vach_assets") (v "0.1.4") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset"))) (k 0)) (d (n "futures-lite") (r "^2.1.0") (f (quote ("std"))) (k 0)) (d (n "vach") (r "^0.5.0") (f (quote ("compression" "crypto"))) (d #t) (k 0)))) (h "1xqckkq12mb53y1iy3jfndvmq63w6n12vnpv4jkmppqjpfq100zg")))

(define-public crate-bevy_vach_assets-0.1.5 (c (n "bevy_vach_assets") (v "0.1.5") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset"))) (k 0)) (d (n "futures-lite") (r "^2.1.0") (f (quote ("std"))) (k 0)) (d (n "vach") (r "^0.5.0") (f (quote ("compression" "crypto"))) (d #t) (k 0)))) (h "1n08a9wqq0kl2bmhhxlbw0y25a17zz2n5zjqg3ilsgcgdsc22zi1")))

