(define-module (crates-io be vy bevy_easy_localize) #:use-module (crates-io))

(define-public crate-bevy_easy_localize-0.1.0 (c (n "bevy_easy_localize") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1pkyby0a2hd81mfvsvqrra1jadz9600ad9yqrxl3f901rc098ssc")))

(define-public crate-bevy_easy_localize-0.1.1 (c (n "bevy_easy_localize") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "filesystem_watcher" "bevy_winit" "x11"))) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ca8bb8nqssfig2w6ipmvdpbv38hmkjvbsqkv76ixp7pddpgw2gs")))

(define-public crate-bevy_easy_localize-0.1.2 (c (n "bevy_easy_localize") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "047ic4r5i4a13wk8y51891pllpjn7yxr0lqq1smlgpfbn0n6dgj9")))

(define-public crate-bevy_easy_localize-0.1.3 (c (n "bevy_easy_localize") (v "0.1.3") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)))) (h "1cn0rd3j1cpsxbrr3g5461xvspk0fwjsp6n551x5b3w340sqk4r2")))

(define-public crate-bevy_easy_localize-0.2.0 (c (n "bevy_easy_localize") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)))) (h "1zqjk0gpnric16kwf6yhyfzygkw676rfn56am3dawwvp94qscvva")))

(define-public crate-bevy_easy_localize-0.2.1 (c (n "bevy_easy_localize") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "quick-csv") (r "^0.1") (d #t) (k 0)))) (h "19jcc9x76rlfd9a1iq0r3vfd1ajgjvj8ad4ygpgwr8d2s27wwin3")))

(define-public crate-bevy_easy_localize-0.3.0 (c (n "bevy_easy_localize") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "0c2yr04qqpqcw45if240pw1dihlrlp4yjhiyslv8kiwai0qxgmnr")))

(define-public crate-bevy_easy_localize-0.4.0 (c (n "bevy_easy_localize") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_text" "file_watcher"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "16m2wnjnbnk8wrvldppm0nvn9c2v1b1r9bp123cwlzqly9y2rvqy")))

(define-public crate-bevy_easy_localize-0.5.0 (c (n "bevy_easy_localize") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_text" "file_watcher" "multi-threaded"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "1svya7v9lswkjj73a5imq8mkwmqgl7kj6ga1nn7ykfpw8hm8dj5k")))

