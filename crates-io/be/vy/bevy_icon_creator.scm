(define-module (crates-io be vy bevy_icon_creator) #:use-module (crates-io))

(define-public crate-bevy_icon_creator-0.1.0 (c (n "bevy_icon_creator") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)))) (h "1kl53zgjndkh7b2jfshf1q384zwjpc5i3fvhdsd6nyqgs7si67mz")))

(define-public crate-bevy_icon_creator-0.1.1 (c (n "bevy_icon_creator") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)))) (h "0ry47n1pan76y3qv6az189cwhpx3wi475f6sbc907q5qwpjhjd50")))

(define-public crate-bevy_icon_creator-0.1.2 (c (n "bevy_icon_creator") (v "0.1.2") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)))) (h "05nq8gcbfaw6vzfi0rjwgcjqkgb067rar95kcfhjm8al0g0wq6dj")))

(define-public crate-bevy_icon_creator-0.1.3 (c (n "bevy_icon_creator") (v "0.1.3") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)))) (h "0l9fc2qij29idx1rgbn4ia66f3nnmzjai0f5mggg06xmmfn3vf8v")))

(define-public crate-bevy_icon_creator-0.1.4 (c (n "bevy_icon_creator") (v "0.1.4") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)))) (h "16cpg3kdfaapywih9k0vjpj02sqavm51nwrksrf2p976kivh0j0h")))

(define-public crate-bevy_icon_creator-0.1.5 (c (n "bevy_icon_creator") (v "0.1.5") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)))) (h "1dwpxihm1iy22va5w41p0rcqmlilmm4xvvd5z1j3fg1nj96kv8b8")))

(define-public crate-bevy_icon_creator-0.1.6 (c (n "bevy_icon_creator") (v "0.1.6") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)))) (h "01cdnglqj4202qbknq6bzd80fy6j00pjwz51p7ppvwy4qw91dnzb")))

(define-public crate-bevy_icon_creator-0.1.7 (c (n "bevy_icon_creator") (v "0.1.7") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)))) (h "0g4i84h35127bikpxkfsc1wbblxfv6s9w4pf5709yrbp6k6a4cki")))

