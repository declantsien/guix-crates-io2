(define-module (crates-io be vy bevy_clap) #:use-module (crates-io))

(define-public crate-bevy_clap-0.1.0 (c (n "bevy_clap") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "clap") (r "^3.0.14") (d #t) (k 0)))) (h "1yxdzla8bm4siaxiyjay4jx8n6dyqq7s44r0x5qr7mx8n1njfknd") (f (quote (("derive" "clap/derive"))))))

