(define-module (crates-io be vy bevy_yarnspinner_demo) #:use-module (crates-io))

(define-public crate-bevy_yarnspinner_demo-0.1.0 (c (n "bevy_yarnspinner_demo") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_editor_pls") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "bevy_sprite3d") (r "^2.7") (d #t) (k 0)) (d (n "bevy_yarnspinner") (r "^0.1") (d #t) (k 0)) (d (n "bevy_yarnspinner_example_dialogue_view") (r "^0.1") (d #t) (k 0)))) (h "16s1a9f8rhd265qmz0q1ca3q01rwdq36vlbfh3w80xdm4frl3pac") (f (quote (("default")))) (s 2) (e (quote (("editor" "dep:bevy_editor_pls"))))))

