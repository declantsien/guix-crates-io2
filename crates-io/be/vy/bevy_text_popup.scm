(define-module (crates-io be vy bevy_text_popup) #:use-module (crates-io))

(define-public crate-bevy_text_popup-0.1.0 (c (n "bevy_text_popup") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "08hpr13az2psac9rak88msfw7akpmc0wrp7f48f912nyzgnc3xd5")))

(define-public crate-bevy_text_popup-0.2.0 (c (n "bevy_text_popup") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "0nibj11nqykqk7c9hbzn13qxyqrx67q4r42jjv7bllbwzc66vkhq")))

(define-public crate-bevy_text_popup-0.3.0 (c (n "bevy_text_popup") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)))) (h "15cyx5jqqr3cbbzkfwchln8234rb72dg8vmf7c8x2ynwk2bnqmis")))

