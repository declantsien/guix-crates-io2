(define-module (crates-io be vy bevy_enum_filter_derive) #:use-module (crates-io))

(define-public crate-bevy_enum_filter_derive-0.1.0 (c (n "bevy_enum_filter_derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05xafh60i3j4nvp7vqg7jpbdimjy6v0lvz0y3xz0c5hdxgh03hag")))

