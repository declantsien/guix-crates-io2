(define-module (crates-io be vy bevy_remote_asset) #:use-module (crates-io))

(define-public crate-bevy_remote_asset-0.1.0 (c (n "bevy_remote_asset") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_sprite" "png" "x11"))) (k 2)) (d (n "ehttp") (r "^0.3.0") (f (quote ("native-async"))) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)))) (h "1xzisszxddx93vsyksj318r77ilzm3wh239hp2n369ad934a218y")))

(define-public crate-bevy_remote_asset-0.1.1 (c (n "bevy_remote_asset") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_sprite" "png" "x11"))) (k 2)) (d (n "ehttp") (r "^0.3.0") (f (quote ("native-async"))) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)))) (h "1xarff612lbs4n3h9vd35xjz9pml3knsm2hdy5pz66f07awh3rdd")))

