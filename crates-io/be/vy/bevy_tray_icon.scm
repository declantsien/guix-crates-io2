(define-module (crates-io be vy bevy_tray_icon) #:use-module (crates-io))

(define-public crate-bevy_tray_icon-0.1.0 (c (n "bevy_tray_icon") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "tray-icon") (r "^0.14.0") (d #t) (k 0)))) (h "1p82zvli5iwgja781ay8szka65ghj8c11bk8vi97253l63wwvy6p")))

(define-public crate-bevy_tray_icon-0.1.1 (c (n "bevy_tray_icon") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "tray-icon") (r "^0.14.0") (d #t) (k 0)))) (h "171yn0ak6b7vzp8gpb64qr89vc83rl2721na5jpk0j4320vmmm6j")))

