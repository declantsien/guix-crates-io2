(define-module (crates-io be vy bevy_mod_wanderlust) #:use-module (crates-io))

(define-public crate-bevy_mod_wanderlust-0.1.0 (c (n "bevy_mod_wanderlust") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.15.0") (f (quote ("debug-render"))) (d #t) (k 0)))) (h "1yh8ai539zsh0z6kv9br7dk024bfnx8d909yalqxhh5ck037441y")))

(define-public crate-bevy_mod_wanderlust-0.1.1 (c (n "bevy_mod_wanderlust") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.15.0") (f (quote ("debug-render"))) (d #t) (k 0)))) (h "1k7za9mhfpp14gvr2gkf3i50vv7347imwqlrxmq42r4mbabv55rm")))

(define-public crate-bevy_mod_wanderlust-0.1.2 (c (n "bevy_mod_wanderlust") (v "0.1.2") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.15.0") (f (quote ("debug-render"))) (d #t) (k 0)))) (h "08w6mi2nri7widdcwmn4m5ch85pa8vgsjrjkb95nmbpi11778f6g")))

(define-public crate-bevy_mod_wanderlust-0.1.3 (c (n "bevy_mod_wanderlust") (v "0.1.3") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.15.0") (f (quote ("debug-render"))) (d #t) (k 0)))) (h "1vhzq9b06r77ljkpym38q2120i01wij7pgmncqwybd96bxkv5nlq")))

(define-public crate-bevy_mod_wanderlust-0.1.4 (c (n "bevy_mod_wanderlust") (v "0.1.4") (d (list (d (n "bevy") (r "^0.7") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.15.0") (f (quote ("dim3"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.15.0") (f (quote ("debug-render"))) (d #t) (k 2)))) (h "1yxlmwyx47fvhbkkwkkadzx2syaxf8qg99yranvxl8gxbvfrdr5g")))

(define-public crate-bevy_mod_wanderlust-0.1.5 (c (n "bevy_mod_wanderlust") (v "0.1.5") (d (list (d (n "bevy") (r "^0.7") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.15.0") (f (quote ("dim3"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.15.0") (f (quote ("debug-render"))) (d #t) (k 2)))) (h "072ms37xd2d74g72krlzkk5za8rj4xmyrgqpqf6zxksh5axd6cf9")))

(define-public crate-bevy_mod_wanderlust-0.2.0 (c (n "bevy_mod_wanderlust") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_rapier3d") (r "^0.16.0") (f (quote ("dim3"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.16.0") (f (quote ("debug-render"))) (d #t) (k 2)))) (h "01pxhgd2khbp81f3if565x6xk93044100cssvlg8vrwfcx0189k5")))

(define-public crate-bevy_mod_wanderlust-0.2.1 (c (n "bevy_mod_wanderlust") (v "0.2.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_rapier3d") (r "^0.16") (f (quote ("dim3"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.16") (f (quote ("debug-render"))) (d #t) (k 2)))) (h "1yrvnc5j3zcyyjmvvgcbma2b4viczm04s049sbycdmzhdsd8wa9x")))

(define-public crate-bevy_mod_wanderlust-0.2.2 (c (n "bevy_mod_wanderlust") (v "0.2.2") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_rapier3d") (r "^0.16") (f (quote ("dim3"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.16") (f (quote ("debug-render"))) (d #t) (k 2)))) (h "0jhsq4aj7vs1vsyvsrfsay99crszda48s617lrrmi5afcp53bw55")))

(define-public crate-bevy_mod_wanderlust-0.3.0 (c (n "bevy_mod_wanderlust") (v "0.3.0") (d (list (d (n "aether_spyglass") (r "^0.1") (d #t) (k 2)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.10") (f (quote ("3d"))) (o #t) (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.21") (f (quote ("async-collider" "dim3"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.21") (f (quote ("debug-render"))) (d #t) (k 2)))) (h "01i883ckxz71lldn97zwhlxcm4m45gfk1n9sxzassix2q2ibzn28") (f (quote (("debug_lines" "bevy_prototype_debug_lines"))))))

(define-public crate-bevy_mod_wanderlust-0.4.0 (c (n "bevy_mod_wanderlust") (v "0.4.0") (d (list (d (n "aether_spyglass") (r "^0.2") (d #t) (k 2)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_gizmos"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_rapier3d") (r "^0.22") (f (quote ("async-collider" "dim3"))) (k 0)) (d (n "bevy_rapier3d") (r "^0.22") (f (quote ("debug-render"))) (d #t) (k 2)))) (h "1wkclp177w8213clk6a84c5jjpq2ngmbgh8fff455xkd3lm7nijm") (f (quote (("default") ("debug_lines"))))))

