(define-module (crates-io be vy bevy_quickmenu) #:use-module (crates-io))

(define-public crate-bevy_quickmenu-0.1.0 (c (n "bevy_quickmenu") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9.0") (d #t) (k 2)))) (h "0ajkmvnnw63drvmxbm6iz7j3mcl62qy6gyzygxa0siqsl2rhsi9h")))

(define-public crate-bevy_quickmenu-0.1.1 (c (n "bevy_quickmenu") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9.0") (f (quote ("bevy_ui" "bevy_render" "bevy_asset" "png" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.9.0") (d #t) (k 2)))) (h "0kxjqmisnvg93psmij98a6sqawfka7riy55v38as48yr0v65sacx")))

(define-public crate-bevy_quickmenu-0.1.5 (c (n "bevy_quickmenu") (v "0.1.5") (d (list (d (n "bevy") (r "^0.9.0") (f (quote ("bevy_ui" "bevy_render" "bevy_asset" "png" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.9.0") (d #t) (k 2)))) (h "00ya51gazr9k7gc2rh4azmgi61cy6rjhn8k1ip0w8f5i8y25gq41")))

(define-public crate-bevy_quickmenu-0.1.6 (c (n "bevy_quickmenu") (v "0.1.6") (d (list (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_ui" "bevy_render" "bevy_asset" "png" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "1mma43ivdvn83svy1c52pz9mcsn4lyr5cri57fv7xhcgrjiw0yf3")))

(define-public crate-bevy_quickmenu-0.2.0 (c (n "bevy_quickmenu") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_ui" "bevy_render" "bevy_asset" "png" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "1ma28qyiqh4kx90qqxmk70c8fkp4d22sr8hhq73l79jp4zirb0bn")))

