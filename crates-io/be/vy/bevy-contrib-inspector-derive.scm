(define-module (crates-io be vy bevy-contrib-inspector-derive) #:use-module (crates-io))

(define-public crate-bevy-contrib-inspector-derive-0.1.0 (c (n "bevy-contrib-inspector-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v1n4qy6rwjjjljq47rgy6fcby583hpr28pfiaygmfzyapy4xwrx")))

(define-public crate-bevy-contrib-inspector-derive-0.2.0 (c (n "bevy-contrib-inspector-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pn4nw2wn6kazn0njysyldrqrh4bwypfcql35kzbfp1qgpyrccm1")))

(define-public crate-bevy-contrib-inspector-derive-0.3.0 (c (n "bevy-contrib-inspector-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17iwdmx7dcvc4hqhgjs2p8i5zh2k71k9g01b3329r2kdbq6hhkh3")))

(define-public crate-bevy-contrib-inspector-derive-0.4.0 (c (n "bevy-contrib-inspector-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05z0h22z7lxxdfr35dc7723f647immhjjzj2ck6yklkwzxgmrrqm")))

(define-public crate-bevy-contrib-inspector-derive-0.5.0 (c (n "bevy-contrib-inspector-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j7kmj8wnd7d1a8gfb62ql87zszgmzzvywqs7kjiv17lawy443lj")))

