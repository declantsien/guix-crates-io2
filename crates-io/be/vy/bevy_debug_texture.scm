(define-module (crates-io be vy bevy_debug_texture) #:use-module (crates-io))

(define-public crate-bevy_debug_texture-0.1.0 (c (n "bevy_debug_texture") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "180kz89a5j3krmq1gqhrid9l49l8dkq68rk8rhnrgwh7zhxwgqqs")))

(define-public crate-bevy_debug_texture-0.2.0 (c (n "bevy_debug_texture") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "122wy4ajg0sgd40xd9c81h49aghbfpsbg1r6zjzibqa3n37hgl80")))

