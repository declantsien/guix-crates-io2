(define-module (crates-io be vy bevy-scene-hook) #:use-module (crates-io))

(define-public crate-bevy-scene-hook-1.0.0 (c (n "bevy-scene-hook") (v "1.0.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)))) (h "10zkwqj28kn8c09s3gwflx1v519xhzkbqprzaca01kpj1q2a21av")))

(define-public crate-bevy-scene-hook-1.1.0 (c (n "bevy-scene-hook") (v "1.1.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)))) (h "0dl0dax0lkjqrgbwmahngl16gk21yhcvfjasp15k7cnrynll466m")))

(define-public crate-bevy-scene-hook-1.2.0 (c (n "bevy-scene-hook") (v "1.2.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)))) (h "1v7jqinm9x4ykbgigg7ams8xb7h70idmsdd7x5fihd1cvcsn3dvv")))

(define-public crate-bevy-scene-hook-2.0.0 (c (n "bevy-scene-hook") (v "2.0.0") (d (list (d (n "bevy") (r "^0.7.0") (k 0)))) (h "03crci3lwba08br8iwngjcd0sw5hjj6fycxajpxn96brldw4r0c2")))

(define-public crate-bevy-scene-hook-3.0.0 (c (n "bevy-scene-hook") (v "3.0.0") (d (list (d (n "bevy") (r "^0.7.0") (k 0)))) (h "1avmj5m7z4qdw34pfz84436pwg34ii7yycim9yddacfn4nmvs6y6")))

(define-public crate-bevy-scene-hook-3.1.0 (c (n "bevy-scene-hook") (v "3.1.0") (d (list (d (n "bevy") (r "^0.7.0") (k 0)))) (h "15b7y1hhm8s68pj2n0waki68wawbfy6k07aqba0h4kyd2lxvcs37")))

(define-public crate-bevy-scene-hook-4.0.0 (c (n "bevy-scene-hook") (v "4.0.0") (d (list (d (n "bevy") (r "^0.8.0") (f (quote ("bevy_scene"))) (k 0)))) (h "08xn7bnb2a0cgarzfjygdcaqv542ifyf0a6hn9r6r6w4j3mndjay")))

(define-public crate-bevy-scene-hook-4.1.0 (c (n "bevy-scene-hook") (v "4.1.0") (d (list (d (n "bevy") (r "^0.8.0") (f (quote ("bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.8.0") (f (quote ("bevy_scene" "bevy_asset"))) (k 2)))) (h "00i5039bydc62xpa295pir6xvqb0184hcdh6l57569s2rp31ijz7")))

(define-public crate-bevy-scene-hook-5.1.0 (c (n "bevy-scene-hook") (v "5.1.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_scene" "bevy_asset"))) (k 2)))) (h "0l5pjyf3m6bqjgj00giwp2izxk5x0wbwnilj9bp2b9bamqi8h99p")))

(define-public crate-bevy-scene-hook-5.1.1 (c (n "bevy-scene-hook") (v "5.1.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_scene" "bevy_asset"))) (k 2)))) (h "1440bdj87lfy8bsj3k6nk4d6cdjb1xpympmfcqd5q69wiv3k4bwi")))

(define-public crate-bevy-scene-hook-5.1.2 (c (n "bevy-scene-hook") (v "5.1.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_scene"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_scene" "bevy_asset"))) (k 2)))) (h "1b2a31ymnjpdlgiq0r369azmq52kig5im4faypfr5akhq5ckvgkn")))

(define-public crate-bevy-scene-hook-5.2.0 (c (n "bevy-scene-hook") (v "5.2.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_scene" "bevy_asset"))) (k 0)))) (h "0r5clnzgw0ncnhn2q4la0ckvlln8fdlh22v71za6wj06h23mh644")))

(define-public crate-bevy-scene-hook-6.0.0 (c (n "bevy-scene-hook") (v "6.0.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_scene" "bevy_asset"))) (k 0)))) (h "1ajbvxa7sh6syxq54ygii63h9v9l5wqn071pcwai85vrybsk7a9f")))

(define-public crate-bevy-scene-hook-7.0.0 (c (n "bevy-scene-hook") (v "7.0.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_scene" "bevy_asset"))) (k 0)))) (h "0k71mg0kdb6n4dl3rrvp70xb4blnjnnlk3qd8jaldfi3cl5c34bs")))

(define-public crate-bevy-scene-hook-8.0.0 (c (n "bevy-scene-hook") (v "8.0.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_scene" "bevy_asset"))) (k 0)))) (h "06ncwfhzk0ykiy4zi1vjb0144dq10jmf9fwskz8drrnw3bgzaz6m")))

(define-public crate-bevy-scene-hook-9.0.0 (c (n "bevy-scene-hook") (v "9.0.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_scene" "bevy_asset"))) (k 0)))) (h "0zbdj9zianibqgnldr4qwkvmah56ah56b111x1r6ws3qhfak7r43")))

(define-public crate-bevy-scene-hook-10.0.0 (c (n "bevy-scene-hook") (v "10.0.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_scene" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "1mh0qjr0wrq1mckq28kx2rk5w1y84xz2wkyli0wpvyrij2fs7788")))

