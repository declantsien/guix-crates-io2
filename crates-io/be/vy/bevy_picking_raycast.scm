(define-module (crates-io be vy bevy_picking_raycast) #:use-module (crates-io))

(define-public crate-bevy_picking_raycast-0.1.0 (c (n "bevy_picking_raycast") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_mod_raycast") (r "^0.8") (d #t) (k 0)) (d (n "bevy_picking_core") (r "^0.1") (d #t) (k 0)))) (h "18qfqsqik58hlbjczpx0jhs2yd07a2vcf3nnck42v0dkg2gs5z4k")))

(define-public crate-bevy_picking_raycast-0.2.0 (c (n "bevy_picking_raycast") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_mod_raycast") (r "^0.8") (d #t) (k 0)) (d (n "bevy_picking_core") (r "^0.2") (d #t) (k 0)))) (h "1mplnlhviqyw6yvd5c14k5n3jf0pbmzfxlnmv1abdb6835xycyf6")))

(define-public crate-bevy_picking_raycast-0.15.0 (c (n "bevy_picking_raycast") (v "0.15.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_mod_raycast") (r "^0.12") (d #t) (k 0)) (d (n "bevy_picking_core") (r "^0.15") (d #t) (k 0)))) (h "0g0sa1b2kygc2h72mbc3v7fx4xfadh35plzl2vh4hfhsxsdbllvz")))

(define-public crate-bevy_picking_raycast-0.16.0 (c (n "bevy_picking_raycast") (v "0.16.0") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "bevy_mod_raycast") (r "^0.15") (d #t) (k 0)) (d (n "bevy_picking_core") (r "^0.16") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.11") (k 0)) (d (n "bevy_render") (r "^0.11") (k 0)) (d (n "bevy_transform") (r "^0.11") (k 0)) (d (n "bevy_window") (r "^0.11") (k 0)))) (h "1w27c7dfawvsmvjq29iva9vfk7z8lis0fm9fplr6a1gmgwii5bq5")))

(define-public crate-bevy_picking_raycast-0.17.0 (c (n "bevy_picking_raycast") (v "0.17.0") (d (list (d (n "bevy_app") (r "^0.12") (k 0)) (d (n "bevy_ecs") (r "^0.12") (k 0)) (d (n "bevy_mod_raycast") (r "^0.16") (d #t) (k 0)) (d (n "bevy_picking_core") (r "^0.17") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12") (k 0)) (d (n "bevy_render") (r "^0.12") (k 0)) (d (n "bevy_transform") (r "^0.12") (k 0)) (d (n "bevy_window") (r "^0.12") (k 0)))) (h "1ar5dg9ryzai3yav3m82s59a1yiczdphadagvl7rqv8y1917caqa")))

(define-public crate-bevy_picking_raycast-0.18.0 (c (n "bevy_picking_raycast") (v "0.18.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_mod_raycast") (r "^0.17.0") (d #t) (k 0)) (d (n "bevy_picking_core") (r "^0.18") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.13") (k 0)) (d (n "bevy_render") (r "^0.13") (k 0)) (d (n "bevy_transform") (r "^0.13") (k 0)) (d (n "bevy_window") (r "^0.13") (k 0)))) (h "0k8ndjlcpyl99hkrv5hxq2jqb5ln9w89qr0nl8kk7sig82ix0xr6")))

