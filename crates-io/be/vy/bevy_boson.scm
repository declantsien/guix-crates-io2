(define-module (crates-io be vy bevy_boson) #:use-module (crates-io))

(define-public crate-bevy_boson-0.1.0 (c (n "bevy_boson") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_winit"))) (k 0)) (d (n "boson") (r "^0.13.6") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "0pbz885i53mh8kn2mh7j3a1zqc4djc7wc3v8pbzjdjrri5f00wjb")))

