(define-module (crates-io be vy bevy_blender_utils) #:use-module (crates-io))

(define-public crate-bevy_blender_utils-0.1.0 (c (n "bevy_blender_utils") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "bevy") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.154") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "01vbvab6zqgn78rk9m5zwcgajgaldk3xaa59860vrb4khjyhsrv3")))

(define-public crate-bevy_blender_utils-0.2.0 (c (n "bevy_blender_utils") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy") (r "^0.11.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.154") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "00yxa2cnn6n6wmpr2jqiv8dd2d9min1q0ywi5zrl51lqvi2chwnr")))

