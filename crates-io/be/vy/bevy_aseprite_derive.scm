(define-module (crates-io be vy bevy_aseprite_derive) #:use-module (crates-io))

(define-public crate-bevy_aseprite_derive-0.1.0 (c (n "bevy_aseprite_derive") (v "0.1.0") (d (list (d (n "aseprite-reader2") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0axs86zw096yymblrq0wm5n7xcijn0v4skmqxsfgf60ma2yjixdp")))

(define-public crate-bevy_aseprite_derive-0.2.0 (c (n "bevy_aseprite_derive") (v "0.2.0") (d (list (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j1gpcqc43qiw1szbzvb4j871c58m2nrp2x5sn56hn13grrbsczp")))

(define-public crate-bevy_aseprite_derive-0.3.0 (c (n "bevy_aseprite_derive") (v "0.3.0") (d (list (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1r0w4z9qp82xam9nk8ldf0hj16jqx85l3aakb8fyn45cfzqyv5sc")))

(define-public crate-bevy_aseprite_derive-0.3.1 (c (n "bevy_aseprite_derive") (v "0.3.1") (d (list (d (n "bevy_aseprite_reader") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1d5xdgzyly0k3rkas48k67yinkfdr1hjmrfx8w6i3y9yadm8fbhr")))

