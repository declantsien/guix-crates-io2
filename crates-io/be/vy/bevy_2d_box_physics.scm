(define-module (crates-io be vy bevy_2d_box_physics) #:use-module (crates-io))

(define-public crate-bevy_2d_box_physics-0.1.0 (c (n "bevy_2d_box_physics") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "bevy_prototype_debug_lines") (r "^0.9") (d #t) (k 0)))) (h "1dba05famqscsdl434smsjlx9x2nr3hpb5s0wx1ncsa5kik7dk1x")))

(define-public crate-bevy_2d_box_physics-0.1.1 (c (n "bevy_2d_box_physics") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "bevy_prototype_debug_lines") (r "^0.9") (d #t) (k 0)))) (h "0mzd686v2pd0qpi0hzxxd9mpirj8w3jiys3qjivdbfw3jwmqp903")))

