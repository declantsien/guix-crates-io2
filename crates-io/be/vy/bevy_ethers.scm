(define-module (crates-io be vy bevy_ethers) #:use-module (crates-io))

(define-public crate-bevy_ethers-0.1.0 (c (n "bevy_ethers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ethers") (r "^1.0") (f (quote ("ws" "openssl"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1ys0rx8llm65mg51cqk8lwrifwxlq0khigw3w5q6zmx33s8gl02y")))

