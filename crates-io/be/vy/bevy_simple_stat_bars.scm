(define-module (crates-io be vy bevy_simple_stat_bars) #:use-module (crates-io))

(define-public crate-bevy_simple_stat_bars-0.1.0 (c (n "bevy_simple_stat_bars") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "16qbk3rxyi53dkcqmh8gsywdkgj67crixf0m7hmp61baprcf6lbp")))

(define-public crate-bevy_simple_stat_bars-0.2.0 (c (n "bevy_simple_stat_bars") (v "0.2.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0jczrai1y22nnz9bsh6x4kgxqyqj20yv2pjd5wll636ni8sy7lqf")))

(define-public crate-bevy_simple_stat_bars-0.3.0 (c (n "bevy_simple_stat_bars") (v "0.3.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "16rn8k3jv4hv3rzggb3xhqfffhc8j9lp9cbg6f7szg41hhik1wym")))

