(define-module (crates-io be vy bevy_mod_imgui) #:use-module (crates-io))

(define-public crate-bevy_mod_imgui-0.1.0 (c (n "bevy_mod_imgui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.23.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.16.0") (d #t) (k 0)))) (h "00dpdfycl99mj2crzz0y2r2cvkq1r40wnd6i2l554i5p4g076f80") (r "1.70.0")))

(define-public crate-bevy_mod_imgui-0.2.0 (c (n "bevy_mod_imgui") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.24.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.17.1") (d #t) (k 0)))) (h "0mpcsfi57h7z6xz0rlp5v5wb7606pwh01q7q9a679xfx7r0axrml") (r "1.70.0")))

(define-public crate-bevy_mod_imgui-0.2.1 (c (n "bevy_mod_imgui") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.24.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.17.1") (d #t) (k 0)))) (h "0sgqb268jlg3bahh0zziyp12m319raaivm80h6ggixr0v1gawl12") (r "1.70.0")))

(define-public crate-bevy_mod_imgui-0.1.1 (c (n "bevy_mod_imgui") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.23.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.16.0") (d #t) (k 0)))) (h "16kaci3jazzh6njnnwmaixdhyy8fnhjxjyanswnfqspbkx66qg2r") (r "1.70.0")))

(define-public crate-bevy_mod_imgui-0.3.0 (c (n "bevy_mod_imgui") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_core_pipeline" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_core_pipeline" "bevy_pbr" "bevy_render"))) (k 2)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)))) (h "1963z5pk0dl36898ihnk8py67v5g0kii3fxz33w20pqq4jd8ll6d") (r "1.77.0")))

