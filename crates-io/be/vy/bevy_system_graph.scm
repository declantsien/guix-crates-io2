(define-module (crates-io be vy bevy_system_graph) #:use-module (crates-io))

(define-public crate-bevy_system_graph-0.1.0 (c (n "bevy_system_graph") (v "0.1.0") (d (list (d (n "bevy_ecs") (r "^0.6") (d #t) (k 0)) (d (n "bevy_ecs_macros") (r "^0.6") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.6") (d #t) (k 0)))) (h "0frnrgpv7hmy1zdf25fwrjq04j2nzhqd84l2za99jirl0kpns2r3")))

(define-public crate-bevy_system_graph-0.1.1 (c (n "bevy_system_graph") (v "0.1.1") (d (list (d (n "bevy_ecs") (r "^0.6") (d #t) (k 0)) (d (n "bevy_ecs_macros") (r "^0.6") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.6") (d #t) (k 0)))) (h "0y4kwng4pkfkwa29dpp7yk1m7bqsv28b3xlfid0jfpd5bylmir1m")))

(define-public crate-bevy_system_graph-0.2.0 (c (n "bevy_system_graph") (v "0.2.0") (d (list (d (n "bevy_ecs") (r "^0.7") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.7") (d #t) (k 0)))) (h "0j7i3r7ahkgka69h7lynl8xb88wm05diicx7ivh215yx7lv4myn1")))

(define-public crate-bevy_system_graph-0.3.0 (c (n "bevy_system_graph") (v "0.3.0") (d (list (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.8") (d #t) (k 0)))) (h "0pybirjc7b6i3h9knj3641sfpzxwk5i0cqk3ik1qnikxvh321601")))

(define-public crate-bevy_system_graph-0.4.0 (c (n "bevy_system_graph") (v "0.4.0") (d (list (d (n "bevy_ecs") (r "^0.9") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.9") (d #t) (k 0)))) (h "1m5rimj543s29myf2km5pv4kcb7g42w24hfpgi24awfw6m8ff1ay")))

