(define-module (crates-io be vy bevy_a11y) #:use-module (crates-io))

(define-public crate-bevy_a11y-0.0.1 (c (n "bevy_a11y") (v "0.0.1") (h "1z0q6xfvjb5m14zpn8cza9k5a0mhshzlzrchyp0i8b40py7qgsrl")))

(define-public crate-bevy_a11y-0.10.0 (c (n "bevy_a11y") (v "0.10.0") (d (list (d (n "accesskit") (r "^0.10") (d #t) (k 0)) (d (n "bevy_app") (r "^0.10.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.10.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10.0") (d #t) (k 0)))) (h "09ychhzgp4fxb59izbzdnsfm0wy60534x98s4xl9s6wp0k7ja2x1")))

(define-public crate-bevy_a11y-0.10.1 (c (n "bevy_a11y") (v "0.10.1") (d (list (d (n "accesskit") (r "^0.10") (d #t) (k 0)) (d (n "bevy_app") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10.1") (d #t) (k 0)))) (h "0saizqgbh3n9mnxls9y4in1sak50h83z8izbjvasbhfsyxil0z03")))

(define-public crate-bevy_a11y-0.11.0 (c (n "bevy_a11y") (v "0.11.0") (d (list (d (n "accesskit") (r "^0.11") (d #t) (k 0)) (d (n "bevy_app") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)))) (h "0xk0hn6kaddrvi2wj66ihydc35b089mzg873m08byqnqv4vz8n7p")))

(define-public crate-bevy_a11y-0.11.1 (c (n "bevy_a11y") (v "0.11.1") (d (list (d (n "accesskit") (r "^0.11") (d #t) (k 0)) (d (n "bevy_app") (r "^0.11.1") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.11.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11.1") (d #t) (k 0)))) (h "09biyrgvj042s43iqqwipvrlip292p9cz5gs6mkhmhqa08a26k7a")))

(define-public crate-bevy_a11y-0.11.2 (c (n "bevy_a11y") (v "0.11.2") (d (list (d (n "accesskit") (r "^0.11") (d #t) (k 0)) (d (n "bevy_app") (r "^0.11.2") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.11.2") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11.2") (d #t) (k 0)))) (h "0kj2im1zpn467q0z75dbz6xfm1jk9y23gsfmqpsvbbpg7isxb1rx")))

(define-public crate-bevy_a11y-0.11.3 (c (n "bevy_a11y") (v "0.11.3") (d (list (d (n "accesskit") (r "^0.11") (d #t) (k 0)) (d (n "bevy_app") (r "^0.11.3") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.11.3") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11.3") (d #t) (k 0)))) (h "0hifgvs3y3z6c7wz8va2adijk4pfa8ngl9bd3yaralvwlwsrwb0k")))

(define-public crate-bevy_a11y-0.12.0 (c (n "bevy_a11y") (v "0.12.0") (d (list (d (n "accesskit") (r "^0.12") (d #t) (k 0)) (d (n "bevy_app") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)))) (h "14knpjw3lhvg0qprqf3rc476qbfq5mcn8z8g81m0bx9lawp826r7")))

(define-public crate-bevy_a11y-0.12.1 (c (n "bevy_a11y") (v "0.12.1") (d (list (d (n "accesskit") (r "^0.12") (d #t) (k 0)) (d (n "bevy_app") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.1") (d #t) (k 0)))) (h "1w4lk840s9dsf73w8fbmg2k9xp0dzsg2ka6kcdjlyqrjr6404238")))

(define-public crate-bevy_a11y-0.13.0 (c (n "bevy_a11y") (v "0.13.0") (d (list (d (n "accesskit") (r "^0.12") (d #t) (k 0)) (d (n "bevy_app") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.0") (d #t) (k 0)))) (h "11z2r6xq65s334jqjhfqbw1slk3s30fnzc9lkczhg96ws3b0ry2v")))

(define-public crate-bevy_a11y-0.13.1 (c (n "bevy_a11y") (v "0.13.1") (d (list (d (n "accesskit") (r "^0.12") (d #t) (k 0)) (d (n "bevy_app") (r "^0.13.1") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.13.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.1") (d #t) (k 0)))) (h "1cfq5qi17hnvs719x919528cx9fcikbzp8l9bnns1m04194dp4k1")))

(define-public crate-bevy_a11y-0.13.2 (c (n "bevy_a11y") (v "0.13.2") (d (list (d (n "accesskit") (r "^0.12") (d #t) (k 0)) (d (n "bevy_app") (r "^0.13.2") (d #t) (k 0)) (d (n "bevy_derive") (r "^0.13.2") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.2") (d #t) (k 0)))) (h "06k2ccxnw709s8hvq0znzvl94npbhd08616s9rm82p3zbxwz53nd")))

