(define-module (crates-io be vy bevy_blackout) #:use-module (crates-io))

(define-public crate-bevy_blackout-0.1.0 (c (n "bevy_blackout") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.12") (d #t) (k 0)) (d (n "bevy_pbr") (r "^0.12") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12") (d #t) (k 0)) (d (n "bevy_render") (r "^0.12") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "13z5v0z7zyfzbf8hsl5ws76yvx7ga282nwvc0lrwphl2xb7w3qwp")))

