(define-module (crates-io be vy bevy_ecs_markers) #:use-module (crates-io))

(define-public crate-bevy_ecs_markers-0.1.0 (c (n "bevy_ecs_markers") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)))) (h "0cnsnqb3gl38m6x00dfw2k496lsacp3k0wvgl04dqvw97jxbhs92")))

(define-public crate-bevy_ecs_markers-0.1.1 (c (n "bevy_ecs_markers") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)))) (h "0vd74d7p6i2pqdbsbmv240g84kpsvdrqbjmfxmm4s2935zwhfwk8")))

(define-public crate-bevy_ecs_markers-0.1.2 (c (n "bevy_ecs_markers") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)))) (h "03vvl203cc09qlnlxxvzsa4h0p7lpzkj1bplzp9kqazw7258730n")))

(define-public crate-bevy_ecs_markers-0.1.3 (c (n "bevy_ecs_markers") (v "0.1.3") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^0.1.3") (d #t) (k 0)))) (h "0jsv1649hlr3hss8h7fn0iq5anjmp798zlvcd64vxlq5p2ns2dh4")))

(define-public crate-bevy_ecs_markers-0.1.4 (c (n "bevy_ecs_markers") (v "0.1.4") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "18fnw8ibkndm66gw7biskcpazw43nl9yqwkzr7k7sr20sd96wcci") (f (quote (("default" "proc")))) (s 2) (e (quote (("proc" "dep:bevy_ecs_markers_macros"))))))

(define-public crate-bevy_ecs_markers-1.0.0 (c (n "bevy_ecs_markers") (v "1.0.0") (d (list (d (n "bevy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0dffflc09b7s08n18a0wgrm9n8i4a4llk1m4h5pb36k0vf44ns15") (f (quote (("default" "proc")))) (s 2) (e (quote (("proc" "dep:bevy_ecs_markers_macros") ("full_bevy" "dep:bevy"))))))

(define-public crate-bevy_ecs_markers-1.0.1 (c (n "bevy_ecs_markers") (v "1.0.1") (d (list (d (n "bevy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0mydsgwskchdzmz30is4b92hmddq1w9gsgya3mbja38f3cwgycin") (f (quote (("default" "proc")))) (s 2) (e (quote (("proc" "dep:bevy_ecs_markers_macros") ("full_bevy" "dep:bevy"))))))

(define-public crate-bevy_ecs_markers-1.0.2 (c (n "bevy_ecs_markers") (v "1.0.2") (d (list (d (n "bevy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "0h99wd94im1wpxv6pyxk0lzcxsgxchvri6fy7ha813lslp2sm13q") (f (quote (("default" "proc")))) (s 2) (e (quote (("proc" "dep:bevy_ecs_markers_macros") ("full_bevy" "dep:bevy"))))))

(define-public crate-bevy_ecs_markers-1.0.3 (c (n "bevy_ecs_markers") (v "1.0.3") (d (list (d (n "bevy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "1hcm5ybbzfynswap111ijiw4cq17gvnq66hqxa1p7p4shii5lpp8") (f (quote (("default" "proc")))) (s 2) (e (quote (("proc" "dep:bevy_ecs_markers_macros") ("full_bevy" "dep:bevy"))))))

(define-public crate-bevy_ecs_markers-1.0.4 (c (n "bevy_ecs_markers") (v "1.0.4") (d (list (d (n "bevy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^1.0.4") (o #t) (d #t) (k 0)))) (h "1ms7z5mxw2rlfprvznq6v4jv3w3xim2awk41wrxbrs6id546jn9d") (f (quote (("default" "proc")))) (s 2) (e (quote (("proc" "dep:bevy_ecs_markers_macros") ("full_bevy" "dep:bevy"))))))

(define-public crate-bevy_ecs_markers-2.0.0 (c (n "bevy_ecs_markers") (v "2.0.0") (d (list (d (n "bevy") (r "^0.13.0") (o #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_ecs_markers_macros") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1jsvd9ywx5824794rsyqdgb7vyv5345620gf7zbnxbly1aazs78c") (f (quote (("default" "proc" "bevy_app")))) (s 2) (e (quote (("proc" "dep:bevy_ecs_markers_macros") ("bevy_app" "dep:bevy"))))))

