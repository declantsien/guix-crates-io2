(define-module (crates-io be vy bevy_mod_yarn) #:use-module (crates-io))

(define-public crate-bevy_mod_yarn-0.1.0 (c (n "bevy_mod_yarn") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "chapter") (r "^0.1.0") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "0sqrwmiz5ycbb4lrk15l62dlb841j8gj9byd209wpd5g18m7gpyk") (f (quote (("input-handlers") ("default" "input-handlers"))))))

