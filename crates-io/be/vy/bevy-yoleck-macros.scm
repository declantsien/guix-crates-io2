(define-module (crates-io be vy bevy-yoleck-macros) #:use-module (crates-io))

(define-public crate-bevy-yoleck-macros-0.9.0 (c (n "bevy-yoleck-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0z0s7pzw4y6w4c0cscnnq0vqv029kn3fzc543k1vspcr23sdxqr2")))

(define-public crate-bevy-yoleck-macros-0.9.1 (c (n "bevy-yoleck-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "11nm0yprq7050whs5xjpdxvq95xynnbm6sx8yqm1gy09c8wj3mwf")))

