(define-module (crates-io be vy bevy_stat_bars) #:use-module (crates-io))

(define-public crate-bevy_stat_bars-0.1.0 (c (n "bevy_stat_bars") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "1l1hgyx218k10qjr4q5y3p5j2vdqyl9jfmq74sc3w70g45lk969p")))

(define-public crate-bevy_stat_bars-0.1.1 (c (n "bevy_stat_bars") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0nkxq8qs6milzzk3kvyyp6m6f97v278j5c78g7rm3wraw8k58a6m")))

(define-public crate-bevy_stat_bars-0.1.2 (c (n "bevy_stat_bars") (v "0.1.2") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0rmdfi033bz51q652y9q9rnf8pffny1syvfh1lvlydglsc9c56z8")))

(define-public crate-bevy_stat_bars-0.2.0 (c (n "bevy_stat_bars") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "16nvhhgv27z3c2r6v4agjc8rhs3zx1drsc2klf3xmj20nzwhxh60")))

(define-public crate-bevy_stat_bars-0.3.0 (c (n "bevy_stat_bars") (v "0.3.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.13.0") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "1z5abf1hb2aakvmig6r5ic922rkn1a42kqcshp5rdp5wpyxr4chi")))

(define-public crate-bevy_stat_bars-0.3.1 (c (n "bevy_stat_bars") (v "0.3.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.13.0") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 2)))) (h "0b327bkca6yyq36pyrbcs8h9fpclqcr28zhzigr4q5bdzg40a6y2")))

