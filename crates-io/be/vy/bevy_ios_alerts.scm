(define-module (crates-io be vy bevy_ios_alerts) #:use-module (crates-io))

(define-public crate-bevy_ios_alerts-0.1.0 (c (n "bevy_ios_alerts") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy_crossbeam_event") (r "^0.5") (d #t) (k 0)))) (h "1fakrqlqz5wmfw3v03zbh1hz0b9pj9p21l6aghqfa6z67l5wxi2h")))

(define-public crate-bevy_ios_alerts-0.1.1 (c (n "bevy_ios_alerts") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy_crossbeam_event") (r "^0.5") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "01p0hfz4svvvkkncvqib5j38vybqzgjy6v2kbikg8882lp1yc6qm")))

(define-public crate-bevy_ios_alerts-0.1.2 (c (n "bevy_ios_alerts") (v "0.1.2") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy_crossbeam_event") (r "^0.5") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "0yrv7g4y7f5yzhviwr7k9qpw9l8phbmkd9rm3x3dj6fqkhaw97ad")))

(define-public crate-bevy_ios_alerts-0.1.3 (c (n "bevy_ios_alerts") (v "0.1.3") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy_crossbeam_event") (r "^0.5") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "0xi18wbrr72i0r15hkgb6i3jc1zzc8hxq2q3l4nrk218slkx5xai")))

