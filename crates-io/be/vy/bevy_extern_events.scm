(define-module (crates-io be vy bevy_extern_events) #:use-module (crates-io))

(define-public crate-bevy_extern_events-0.1.0 (c (n "bevy_extern_events") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit" "bevy_core_pipeline"))) (k 0)) (d (n "generic-global-variables") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0j6ppmyz1cfbq02q35lqbh7pvaywv870csnd4ssv9cgnq2r4lzsi")))

(define-public crate-bevy_extern_events-0.1.1 (c (n "bevy_extern_events") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit" "bevy_core_pipeline"))) (k 0)) (d (n "generic-global-variables") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0wqb6471m6qr9fdwk07jwj00m5g0yd55xn90vziw8qmxvlags2ib")))

(define-public crate-bevy_extern_events-0.1.2 (c (n "bevy_extern_events") (v "0.1.2") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit" "bevy_core_pipeline"))) (k 0)) (d (n "generic-global-variables") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1w6bxbynfy8fh8h44izddslwqbnkf4l6pwan2cvifscjnal090i8")))

(define-public crate-bevy_extern_events-0.2.0 (c (n "bevy_extern_events") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit" "bevy_core_pipeline"))) (k 0)) (d (n "generic-global-variables") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0sdikbm2l8afgsfvkaqf37r47gbpmykbmxxi8n6yh0m00shbl7c7")))

(define-public crate-bevy_extern_events-0.2.1 (c (n "bevy_extern_events") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit" "bevy_core_pipeline"))) (k 0)) (d (n "generic-global-variables") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1jc928c95dman4jj2nziw2dxpwdmydyd8biwf6dc1659cychbjxi")))

(define-public crate-bevy_extern_events-0.2.2 (c (n "bevy_extern_events") (v "0.2.2") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "generic-global-variables") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1fndj8lp7mma4sj7b7h5sqhx7cnq8a1nxy5hl6bmf2zmphmjv5a1")))

(define-public crate-bevy_extern_events-0.2.3 (c (n "bevy_extern_events") (v "0.2.3") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "generic-global-variables") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1v0y8pxar2gxs37a4s7ba578wsk6i3wdxr09c9572rgjqyaibl46")))

(define-public crate-bevy_extern_events-0.3.0 (c (n "bevy_extern_events") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "generic-global-variables") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "parking_lot") (r "^0.11") (f (quote ("wasm-bindgen"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0iwa9iyy8hjz9kdxnj07kjyl9zcgp72j9lpaxz7kcf0v32s0zgk9")))

