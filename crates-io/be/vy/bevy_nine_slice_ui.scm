(define-module (crates-io be vy bevy_nine_slice_ui) #:use-module (crates-io))

(define-public crate-bevy_nine_slice_ui-0.1.0 (c (n "bevy_nine_slice_ui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "1m85vgbrn5hvnpwqfp9iw46vsb29zlq0zplxbdnjfrs47q9vds74")))

(define-public crate-bevy_nine_slice_ui-0.1.1 (c (n "bevy_nine_slice_ui") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "14qqy6g2aj8rbp4czadzxa08xhvv69rdkqyppd3jgbhqr6rs13m2")))

(define-public crate-bevy_nine_slice_ui-0.2.0 (c (n "bevy_nine_slice_ui") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "0y866rzbd9r1v13iaca96hclr8h9f50k1632a855lam42pgm5yly")))

(define-public crate-bevy_nine_slice_ui-0.3.0 (c (n "bevy_nine_slice_ui") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "0dfwx4msix9y9f6lqlm8nx5ivmjp7jpsn4nwsmrr18by2kdfclc8")))

(define-public crate-bevy_nine_slice_ui-0.3.1 (c (n "bevy_nine_slice_ui") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "1bf7s8gdc856ki031sgzigxdg7j8zr4dcbch15z3zbpgik0k7xpj")))

(define-public crate-bevy_nine_slice_ui-0.4.0 (c (n "bevy_nine_slice_ui") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "04x88jx7gax0wwvghwpk2m7ga94kx7vygm5vnzkycg402i44xv1v")))

(define-public crate-bevy_nine_slice_ui-0.5.0 (c (n "bevy_nine_slice_ui") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_render" "bevy_ui" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 2)))) (h "1sylvf3zq7vbv18njlllvqr0kdxlyy2m9mqph0p0x59jsqmydqfk")))

(define-public crate-bevy_nine_slice_ui-0.6.0 (c (n "bevy_nine_slice_ui") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render" "bevy_ui" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "1igv602x02g35lgaj5b27kqxvy3d77apxnc8vpf0613djd3qw8jp")))

