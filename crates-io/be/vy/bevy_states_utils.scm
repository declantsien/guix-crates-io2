(define-module (crates-io be vy bevy_states_utils) #:use-module (crates-io))

(define-public crate-bevy_states_utils-0.1.0 (c (n "bevy_states_utils") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (k 0)))) (h "07xv0hjv01q4v36wdpzqak1f4wjdz7w2gg6z39lr48k314c5x1ra") (f (quote (("nested") ("gc") ("default" "gc" "nested"))))))

(define-public crate-bevy_states_utils-0.2.0 (c (n "bevy_states_utils") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (k 0)))) (h "12aa87iiphv27v47jiffkiqbqiq04236r6cgf369jmsv4hipxzl9") (f (quote (("nested") ("gc") ("default" "gc" "nested"))))))

(define-public crate-bevy_states_utils-0.3.0 (c (n "bevy_states_utils") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (k 0)))) (h "0mvs0vgrb6i95s3ysn5ki1mrz24g3l68jkims97l99jlpv7c65xb") (f (quote (("nested") ("gc") ("default" "gc" "nested"))))))

