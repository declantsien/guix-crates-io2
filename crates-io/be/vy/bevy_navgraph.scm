(define-module (crates-io be vy bevy_navgraph) #:use-module (crates-io))

(define-public crate-bevy_navgraph-0.0.1 (c (n "bevy_navgraph") (v "0.0.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02rdz0psyr9n440gbb2n546zf6l4q0g07m8qypamwiy0cilmvgmw") (y #t)))

