(define-module (crates-io be vy bevy_svg_map) #:use-module (crates-io))

(define-public crate-bevy_svg_map-0.1.0 (c (n "bevy_svg_map") (v "0.1.0") (d (list (d (n "bevy") (r "^0.2.1") (f (quote ("render"))) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5.0") (d #t) (k 0)))) (h "0vvqfgk3mgdd6sx8ffy7mp1zbivhjm6abvrigsi8xjxi1qh72zm4")))

(define-public crate-bevy_svg_map-0.1.1 (c (n "bevy_svg_map") (v "0.1.1") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)) (d (n "euclid") (r "^0.22.1") (d #t) (k 0)) (d (n "lyon") (r "^0.17.1") (f (quote ("svg"))) (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5.0") (d #t) (k 0)))) (h "09pir81913ga9d424x7rdwad571x1m5dan8xmid96id9b7iqbqdq")))

(define-public crate-bevy_svg_map-0.2.0 (c (n "bevy_svg_map") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.5") (d #t) (k 2)) (d (n "euclid") (r "^0.22.1") (d #t) (k 0)) (d (n "lyon") (r "^0.17.1") (f (quote ("svg"))) (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.0") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5.0") (d #t) (k 0)))) (h "0k312f5l48q56y8k7axf8bbwxv7jf4hcb13i5dg5ji72sf628s5i")))

