(define-module (crates-io be vy bevy_screenplay) #:use-module (crates-io))

(define-public crate-bevy_screenplay-0.2.0 (c (n "bevy_screenplay") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04y4wfww144qgbkss2x2bj0pg7wz8sqf70w7aixqyqc91ij9akj6") (y #t)))

