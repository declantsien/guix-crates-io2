(define-module (crates-io be vy bevy-undo2) #:use-module (crates-io))

(define-public crate-bevy-undo2-0.0.1 (c (n "bevy-undo2") (v "0.0.1") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)))) (h "10f3qrr2vykx2xvc7g43i8g1nhzznmq5rxv9cb41r41d3d43rzpg") (f (quote (("default" "callback_event") ("callback_event"))))))

(define-public crate-bevy-undo2-0.0.2 (c (n "bevy-undo2") (v "0.0.2") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)))) (h "1006bhgfimzmxg2gwzgrl9vr4g1hv3f6zdllhyxla18njy8bvpzn") (f (quote (("default" "callback_event") ("callback_event"))))))

(define-public crate-bevy-undo2-0.0.3 (c (n "bevy-undo2") (v "0.0.3") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)))) (h "1gak4jcc547hra32nfsvd70vf5jvqff5ml00bfhivs4i5p04wwvz") (f (quote (("default" "callback_event") ("callback_event"))))))

(define-public crate-bevy-undo2-0.1.0 (c (n "bevy-undo2") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)))) (h "1vhnys56irl5994grhi1wzp5hrxgkgk4mzkk62b4qg3297z9ciz3") (f (quote (("default" "callback_event") ("callback_event"))))))

