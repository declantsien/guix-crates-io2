(define-module (crates-io be vy bevy_state_stack) #:use-module (crates-io))

(define-public crate-bevy_state_stack-0.1.0 (c (n "bevy_state_stack") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)))) (h "1hvrzi0f0bp8j18bj92d4zcvij9j8n0hxd8lpha8fngqwh3y75lk")))

(define-public crate-bevy_state_stack-0.1.1 (c (n "bevy_state_stack") (v "0.1.1") (d (list (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)))) (h "0iair94z22cizi5akdwhgnc4p48s3a1kwhhzgqjf18widxfji2ya") (y #t)))

(define-public crate-bevy_state_stack-0.2.0 (c (n "bevy_state_stack") (v "0.2.0") (d (list (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)))) (h "190hclf6jkwqab9v6wjqazrmhhbwmbvahz5g6rg0cpjq1aba9y0g") (y #t)))

(define-public crate-bevy_state_stack-0.2.1 (c (n "bevy_state_stack") (v "0.2.1") (d (list (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)))) (h "0q6r86kbrin7ab52vzzn3jc1a0x1w6qwanbzg0j7952k70dmsi10")))

