(define-module (crates-io be vy bevy_embedded_assets) #:use-module (crates-io))

(define-public crate-bevy_embedded_assets-0.1.0 (c (n "bevy_embedded_assets") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "1x4h13a7rvs8xhayppz3f802xqhzlnlagsbbz6f1xdjzzdijgi6y")))

(define-public crate-bevy_embedded_assets-0.1.1 (c (n "bevy_embedded_assets") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "0sgbdpk371w9bcw0a9nr2x6hcar1krrjvlx4jnnprs15hswsf7j0")))

(define-public crate-bevy_embedded_assets-0.1.2 (c (n "bevy_embedded_assets") (v "0.1.2") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "1j27vwlg9rza1z90qdcs861wn6v1ah2dva7yy2dklhvmybfisavr")))

(define-public crate-bevy_embedded_assets-0.1.3 (c (n "bevy_embedded_assets") (v "0.1.3") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "0k8av7a38akc3aph1ca26mnn484fbaj7685m2sx734xqjahl50y7")))

(define-public crate-bevy_embedded_assets-0.2.0 (c (n "bevy_embedded_assets") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "1gvy451x6hy5sfrzw81ks0ycf40h865rip2aawq57anhjgbgypg6")))

(define-public crate-bevy_embedded_assets-0.2.1 (c (n "bevy_embedded_assets") (v "0.2.1") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "188sgg90ii7mg9d7afw7is5y7v8vkdh5fl4ffn228ws6q75a5cp8")))

(define-public crate-bevy_embedded_assets-0.3.0 (c (n "bevy_embedded_assets") (v "0.3.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "0s1shsychmkzn220p272m7jvxk6k9p0nkjmdn77ms48gac9xx5n2")))

(define-public crate-bevy_embedded_assets-0.4.0 (c (n "bevy_embedded_assets") (v "0.4.0") (d (list (d (n "bevy") (r "^0.8.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "1np323pb04vlsrahjqdw2494qcihib2ldjlvh8cwy6a1h397iqcw")))

(define-public crate-bevy_embedded_assets-0.6.0 (c (n "bevy_embedded_assets") (v "0.6.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "1ihzchgpql8rysramgia2pjj8v6x9sizw4mg5hlw1jfdg5z7b9qy")))

(define-public crate-bevy_embedded_assets-0.6.1 (c (n "bevy_embedded_assets") (v "0.6.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)))) (h "0scy0l1ha63nym6fjhjlah9lwvf713wncvlsw90yva0231by6xig")))

(define-public crate-bevy_embedded_assets-0.6.2 (c (n "bevy_embedded_assets") (v "0.6.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "03gh63kmd1nqxa5xcpja0ikr2vf0b9hmksi0nhxf3z3f0jz14fbm")))

(define-public crate-bevy_embedded_assets-0.7.0 (c (n "bevy_embedded_assets") (v "0.7.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0lcl6wm8x58w46md4vm8nnfnj7wgfkq0qmpgzmn1jly5cz7lw79a")))

(define-public crate-bevy_embedded_assets-0.8.0 (c (n "bevy_embedded_assets") (v "0.8.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "17szx0ir07hpg44s2r8jdgprvkz5rcazy2zswr245pyjlxs92hs9")))

(define-public crate-bevy_embedded_assets-0.9.0 (c (n "bevy_embedded_assets") (v "0.9.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0kdjhxpiiz60hk0ic973r5avsmxp7wfqigc7rh8195mmwndw9a9k") (f (quote (("default-source" "futures-io" "futures-lite") ("default" "default-source"))))))

(define-public crate-bevy_embedded_assets-0.9.1 (c (n "bevy_embedded_assets") (v "0.9.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "18c61vlb64h918glb53q42q27g10sxgqy9kif0cz4rfndmqs5fnw") (f (quote (("default-source" "futures-io" "futures-lite") ("default" "default-source"))))))

(define-public crate-bevy_embedded_assets-0.10.0 (c (n "bevy_embedded_assets") (v "0.10.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0wld2n3hc0n4chdvk7fcln271k80njxa4m4xf66yjq92y7za0afj") (f (quote (("default-source" "futures-io" "futures-lite") ("default" "default-source"))))))

(define-public crate-bevy_embedded_assets-0.10.1 (c (n "bevy_embedded_assets") (v "0.10.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "03xlx77pkx7dj7l3aracaxgxdps1ld5rgmbsw59n957ngp4rvmmz") (f (quote (("default-source" "futures-io" "futures-lite") ("default" "default-source"))))))

(define-public crate-bevy_embedded_assets-0.10.2 (c (n "bevy_embedded_assets") (v "0.10.2") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset"))) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "futures-io") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1g8zjyrspnm2nj3l0clg1kdxyl3xfmb0gbhmjj15j35xvky0njra") (f (quote (("default-source" "futures-io" "futures-lite") ("default" "default-source"))))))

