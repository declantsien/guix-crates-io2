(define-module (crates-io be vy bevy_consumable_event) #:use-module (crates-io))

(define-public crate-bevy_consumable_event-0.1.0 (c (n "bevy_consumable_event") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (k 2)) (d (n "bevy_app") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.1") (k 0)))) (h "1b534x328b5y8a9d708gnyhla76j0bzpzw49gsxr6wwsjl4qprz7") (f (quote (("default" "bevy_app"))))))

(define-public crate-bevy_consumable_event-0.2.0 (c (n "bevy_consumable_event") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.1") (k 2)) (d (n "bevy_app") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.1") (k 0)))) (h "12x051ii0cd6qd0808ngza0yc43q4ygfmx52inhx4wknsyjlixh5") (f (quote (("default" "bevy_app"))))))

(define-public crate-bevy_consumable_event-0.3.0 (c (n "bevy_consumable_event") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.0") (k 2)) (d (n "bevy_app") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.0") (k 0)))) (h "0b5rp5g4zv2hp00driynj9rk08p0ynfzw0y8g6g02pbn9jg1indc") (f (quote (("default" "bevy_app"))))))

