(define-module (crates-io be vy bevy_ui_borders) #:use-module (crates-io))

(define-public crate-bevy_ui_borders-0.1.0 (c (n "bevy_ui_borders") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "1cyznm3cn1qy6qzqv1n29zysz4dr010kyn6rvjr14qxvxp205f1l")))

(define-public crate-bevy_ui_borders-0.1.1 (c (n "bevy_ui_borders") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "011z8ab1nnhjjfsxirgc76a7hgj81m3m0pr4vd9px84ykai6bwr5")))

(define-public crate-bevy_ui_borders-0.2.0 (c (n "bevy_ui_borders") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10.0") (f (quote ("bevy_ui" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10.0") (d #t) (k 2)))) (h "1h543x0j58h0hxaxm2pazck90lrj6rnb8psdah3w9dws67zmcpg2")))

(define-public crate-bevy_ui_borders-0.3.0 (c (n "bevy_ui_borders") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_ui" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "1wkdv4fsk152drygn83i20lnc3282k00gs02akhz1295sl3c2834")))

