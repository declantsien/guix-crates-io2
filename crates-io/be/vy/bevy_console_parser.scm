(define-module (crates-io be vy bevy_console_parser) #:use-module (crates-io))

(define-public crate-bevy_console_parser-0.3.0 (c (n "bevy_console_parser") (v "0.3.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)))) (h "082lq0lj5kvxd8mn4wdap73q307fv23kaz70vfpddqwzdbr5vgqf")))

(define-public crate-bevy_console_parser-0.4.0 (c (n "bevy_console_parser") (v "0.4.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)))) (h "1jkalrgykpbh60gf2vs3bhflyzjn8mfhx978ydy8ddva9y6pzx5a")))

