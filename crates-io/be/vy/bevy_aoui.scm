(define-module (crates-io be vy bevy_aoui) #:use-module (crates-io))

(define-public crate-bevy_aoui-0.1.0 (c (n "bevy_aoui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_sprite" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.23") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0dcc28mi23fz1680z9xf93lm66s3imrhmj2v4zp5x9xa68v3vvww") (f (quote (("default" "bundles" "serde") ("bundles"))))))

(define-public crate-bevy_aoui-0.2.0 (c (n "bevy_aoui") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_sprite" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.23") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1qjrdikdisjxfmybxm0149pfb39hrbgi9p2xwbghc7hph98qdv91") (f (quote (("default" "bundles" "serde") ("bundles"))))))

(define-public crate-bevy_aoui-0.2.1 (c (n "bevy_aoui") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_sprite" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.23") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0vr0ixsnbcysjmp0i7qym18fxnp6mn54ldgbzjlir53pjim836a4") (f (quote (("default" "bundles" "serde") ("bundles"))))))

