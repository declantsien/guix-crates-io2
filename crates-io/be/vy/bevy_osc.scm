(define-module (crates-io be vy bevy_osc) #:use-module (crates-io))

(define-public crate-bevy_osc-0.1.0 (c (n "bevy_osc") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy") (r "^0.5") (f (quote ("render" "bevy_winit" "bevy_wgpu" "bevy_gltf"))) (k 2)) (d (n "bevy") (r "^0.5") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "nannou_osc") (r "^0.16.0") (d #t) (k 0)))) (h "0nrkqyqv1b4cw1v0gzii59i75kpndla5j7b3670akizhd941imjq")))

(define-public crate-bevy_osc-0.2.0 (c (n "bevy_osc") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "bevy") (r "^0.6") (f (quote ("render" "bevy_winit"))) (k 2)) (d (n "bevy") (r "^0.6") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "nannou_osc") (r "^0.18.0") (d #t) (k 0)))) (h "10yfhpcadff7zbpmx6k7l5xv6xacwdgs1yld0dkf6s1w5klamd9f")))

(define-public crate-bevy_osc-0.3.0 (c (n "bevy_osc") (v "0.3.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("render" "bevy_winit"))) (k 2)) (d (n "bevy") (r "^0.7") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "nannou_osc") (r "^0.18.0") (d #t) (k 0)))) (h "0zc84bkzpq0cvqy819qvdadp5ilhxhb163kpjhf87c8g9s2wm1p6")))

(define-public crate-bevy_osc-0.4.0 (c (n "bevy_osc") (v "0.4.0") (d (list (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "bevy") (r "^0.8.0") (f (quote ("bevy_asset" "render" "bevy_winit"))) (k 2)) (d (n "bevy") (r "^0.8.0") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "nannou_osc") (r "^0.18.0") (d #t) (k 0)))) (h "0hc5yxv0ybqdnxcch9m9l1d6kyxf197irgi6ml481ppwmh156ghp")))

(define-public crate-bevy_osc-0.5.0 (c (n "bevy_osc") (v "0.5.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render" "bevy_winit"))) (k 2)) (d (n "bevy") (r "^0.9") (f (quote ("x11" "wayland"))) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "nannou_osc") (r "^0.18.0") (d #t) (k 0)))) (h "1a95gahjpy9wa2nwjz3xfsrqfp4wz7fz1h2wkizzb9mzs8x1nyzy")))

