(define-module (crates-io be vy bevy_starfield) #:use-module (crates-io))

(define-public crate-bevy_starfield-0.1.0 (c (n "bevy_starfield") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)))) (h "0x11sr4w61bfngjidb0c17ck01yfx8b69jng4dlpzi22l3za0k96")))

(define-public crate-bevy_starfield-0.1.1 (c (n "bevy_starfield") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)))) (h "1f5ks4mvp25pbys1w6d7bdgxkwf87d57l45bqrlf3pgsm2cdhgp9")))

