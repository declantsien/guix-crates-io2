(define-module (crates-io be vy bevy_image_config) #:use-module (crates-io))

(define-public crate-bevy_image_config-0.1.0 (c (n "bevy_image_config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (d #t) (k 0)) (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)))) (h "0z30c7hc8vmncx6w5vzriivxzn5sffp6kck3s8p7gd849zga8n9y")))

(define-public crate-bevy_image_config-0.2.0 (c (n "bevy_image_config") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (d #t) (k 0)) (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)))) (h "1ppxvvhclwgz9q8na896yklpgfnrrvx9sgqy3h464q1avyjav0kn")))

