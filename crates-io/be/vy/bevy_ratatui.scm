(define-module (crates-io be vy bevy_ratatui) #:use-module (crates-io))

(define-public crate-bevy_ratatui-0.1.0 (c (n "bevy_ratatui") (v "0.1.0") (h "0jw9ca7bmi4y25v4vcwxm1ycvga7hp6g3kqjcrh1az23590gbsb7")))

(define-public crate-bevy_ratatui-0.1.1 (c (n "bevy_ratatui") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "0xav26kdfc0r67n6i9ya4z85zjq0vb1pa01zn9h10qasiag257kc")))

(define-public crate-bevy_ratatui-0.1.2 (c (n "bevy_ratatui") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "1pfb7s1kkrd2y6hq87qa0fh7a8m6y3p9g8sglk4zs9hx26lv3mzr")))

(define-public crate-bevy_ratatui-0.1.3 (c (n "bevy_ratatui") (v "0.1.3") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "1vpznrg8myis2z95kw3is4dqk7ga7hkpprpvycmixffg24r7wjxk")))

(define-public crate-bevy_ratatui-0.1.4 (c (n "bevy_ratatui") (v "0.1.4") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "1mcwd4x2nxh13jfybspw4xz6yj9ba8ypvni1rw17kwg929cwakmg")))

(define-public crate-bevy_ratatui-0.1.5 (c (n "bevy_ratatui") (v "0.1.5") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "1p64mk8wwl07nwfiwq74p3znj8hbwfdswwl0id7qw8bbvly4zxm4")))

(define-public crate-bevy_ratatui-0.1.6 (c (n "bevy_ratatui") (v "0.1.6") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "1ada9bjk6blxikmg2gvyv1ydswj6nqnbcn2hgyy15gh221g8hhz6")))

(define-public crate-bevy_ratatui-0.1.7 (c (n "bevy_ratatui") (v "0.1.7") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "10bx449q4igg1arz7s0ag3lwimx2jarc66gzn89dgmjfbxxdcnmx")))

(define-public crate-bevy_ratatui-0.1.8 (c (n "bevy_ratatui") (v "0.1.8") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "05myzb17hfx8f1wpw6vvw85bqymy2fp4hmxkhrkr0mhfdfl6i7q6")))

(define-public crate-bevy_ratatui-0.1.9 (c (n "bevy_ratatui") (v "0.1.9") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.1") (f (quote ("bevy_winit" "bevy_ui"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "1x9pfbiczdmm5ycgmn5180zxb9h8dbjwrqcv6n6iyjgp9jsv65za")))

(define-public crate-bevy_ratatui-0.2.0 (c (n "bevy_ratatui") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "0phha9qds1qlg3ykc1iyd2ny70hhmy61406q1gvsh8dwxzsbklpj")))

(define-public crate-bevy_ratatui-0.2.1 (c (n "bevy_ratatui") (v "0.2.1") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (f (quote ("animation" "bevy_asset" "bevy_scene" "bevy_winit" "bevy_core_pipeline" "bevy_render" "bevy_sprite" "bevy_text" "bevy_ui" "multi-threaded" "png" "bevy_gizmos" "default_font"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "0rih1csh06iwiq3dgx5fdkpjc9iaib0zn8zyp4yh2lwnw9x0hxrn")))

(define-public crate-bevy_ratatui-0.2.2 (c (n "bevy_ratatui") (v "0.2.2") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_scene" "bevy_winit" "bevy_core_pipeline" "bevy_render" "bevy_sprite" "bevy_text" "bevy_ui" "multi-threaded" "png" "bevy_gizmos" "x11" "default_font"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "1jkwzli439lmy5c831pv50ri0q4pf3ari7ml1vaafhcy7f5q24ws")))

(define-public crate-bevy_ratatui-9.3.2 (c (n "bevy_ratatui") (v "9.3.2") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_scene" "bevy_winit" "bevy_core_pipeline" "bevy_render" "bevy_sprite" "bevy_text" "bevy_ui" "multi-threaded" "png" "bevy_gizmos" "x11" "default_font"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "0mbq39bg9x4kfpwq1drwdy6ka65jsdi2341h500q1lh18lcz4ndy")))

