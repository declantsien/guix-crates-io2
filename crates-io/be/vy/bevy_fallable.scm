(define-module (crates-io be vy bevy_fallable) #:use-module (crates-io))

(define-public crate-bevy_fallable-0.3.0 (c (n "bevy_fallable") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 2)) (d (n "bevy_app") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_fallable_derive") (r "^0.3.0") (d #t) (k 0)))) (h "0s56rlmdxb2v8g5ls9x5ll5qhlyzvjkhdgncz5xsmhpk0fb2ymhh") (y #t)))

