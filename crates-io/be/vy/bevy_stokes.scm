(define-module (crates-io be vy bevy_stokes) #:use-module (crates-io))

(define-public crate-bevy_stokes-0.1.0-alpha.0 (c (n "bevy_stokes") (v "0.1.0-alpha.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)) (d (n "laminar") (r "^0.5") (d #t) (k 0)))) (h "1754icijnb9p7vzqsbsdm5l4ppbjd4harq1lscfgs6wzwsi7if73")))

(define-public crate-bevy_stokes-0.1.0-alpha.1 (c (n "bevy_stokes") (v "0.1.0-alpha.1") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)) (d (n "laminar") (r "^0.5") (d #t) (k 0)))) (h "0lh06y6m00wx7j7ivnx2fhm6fdfs5rcpxdlnrpd0rvppkd1c8fma")))

(define-public crate-bevy_stokes-0.1.0-alpha.2 (c (n "bevy_stokes") (v "0.1.0-alpha.2") (d (list (d (n "bevy") (r "^0.6.1") (d #t) (k 0)) (d (n "laminar") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "005mavbkcjr38v96y9cqqsl9347xbzch2a6mz2jj9fc2zjcahf2v")))

