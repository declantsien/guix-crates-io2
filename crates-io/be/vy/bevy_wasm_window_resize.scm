(define-module (crates-io be vy bevy_wasm_window_resize) #:use-module (crates-io))

(define-public crate-bevy_wasm_window_resize-0.1.0 (c (n "bevy_wasm_window_resize") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)))) (h "0njz93z9pmy0hcn9bwybijbppvb0awjhcqbhp7lgl03rx3ir23j4")))

(define-public crate-bevy_wasm_window_resize-0.1.1 (c (n "bevy_wasm_window_resize") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1wwffzc2yhihblzfa5bhxvx9apjckjwx04mh3pwhfbvpz5aazqab")))

(define-public crate-bevy_wasm_window_resize-0.2.0 (c (n "bevy_wasm_window_resize") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1mzvaalldkcs9gz6pjd4jxsylzy7imdyd0hx9ylawhnfa49cwrsh")))

(define-public crate-bevy_wasm_window_resize-0.2.1 (c (n "bevy_wasm_window_resize") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit"))) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1rfb76040nlyvizvahhbjs44bjh4q0vaym3g0ydrd2x0y4aam6xa")))

(define-public crate-bevy_wasm_window_resize-0.3.0 (c (n "bevy_wasm_window_resize") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_winit"))) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0nri5d9q6dzfbgs5igkwpr6nv6bfyvpqvkamqqyxfgr6ca510vkj")))

