(define-module (crates-io be vy bevy_atomic_save) #:use-module (crates-io))

(define-public crate-bevy_atomic_save-0.1.0 (c (n "bevy_atomic_save") (v "0.1.0") (d (list (d (n "bevy") (r "0.9.*") (d #t) (k 0)) (d (n "ron") (r "0.8.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)))) (h "1wj6lzlr0rnxrlnc2vmfab3w922skbhpg61a9q6w75880xqyfiba")))

(define-public crate-bevy_atomic_save-0.1.1 (c (n "bevy_atomic_save") (v "0.1.1") (d (list (d (n "bevy") (r "0.9.*") (d #t) (k 0)) (d (n "ron") (r "0.8.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)))) (h "1hpc0q1mynsl9wy0p3x3v03ikzf62b9rhwjq09f5scydbjyi4naf")))

(define-public crate-bevy_atomic_save-0.2.0 (c (n "bevy_atomic_save") (v "0.2.0") (d (list (d (n "bevy") (r "0.9.*") (d #t) (k 0)) (d (n "ron") (r "0.8.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)))) (h "1xw7jg0i8n1bjnsvsm4ihq4lnv9dwqgznlr4n8splzwmp84x5269")))

(define-public crate-bevy_atomic_save-0.2.1 (c (n "bevy_atomic_save") (v "0.2.1") (d (list (d (n "bevy") (r "0.9.*") (d #t) (k 0)) (d (n "ron") (r "0.8.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)))) (h "1kasglmjds48ds3kb0pz7f1wnq6qc7vdac2a4kkx1iswrxyvrfwh")))

(define-public crate-bevy_atomic_save-0.2.2 (c (n "bevy_atomic_save") (v "0.2.2") (d (list (d (n "bevy") (r "0.9.*") (d #t) (k 0)) (d (n "ron") (r "0.8.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)))) (h "051z8yaqpgg4iwqi8wi0yymjxpmpjkwp1gjyazkbgwamzybmssnf")))

