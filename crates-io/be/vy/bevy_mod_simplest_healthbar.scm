(define-module (crates-io be vy bevy_mod_simplest_healthbar) #:use-module (crates-io))

(define-public crate-bevy_mod_simplest_healthbar-0.1.0 (c (n "bevy_mod_simplest_healthbar") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_text" "bevy_ui" "bevy_core_pipeline" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "0vmngq3hyag6skz4cy830pai1is9wqinfxmln471aj3khvmxsqgw")))

