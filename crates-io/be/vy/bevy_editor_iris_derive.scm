(define-module (crates-io be vy bevy_editor_iris_derive) #:use-module (crates-io))

(define-public crate-bevy_editor_iris_derive-0.1.0 (c (n "bevy_editor_iris_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "09k8q9c6fpxgzdmyvq0kkfi2b1mbfqqgm6qz6p25j793nxnkq991")))

