(define-module (crates-io be vy bevy_picking_sprite) #:use-module (crates-io))

(define-public crate-bevy_picking_sprite-0.1.0 (c (n "bevy_picking_sprite") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_sprite" "bevy_ui" "bevy_render"))) (k 0)) (d (n "bevy_picking_core") (r "^0.1") (d #t) (k 0)))) (h "0z0yids54xihyjawa4lg210mx70x8a0jsjilh1j04kcr3r81mj3w")))

(define-public crate-bevy_picking_sprite-0.2.0 (c (n "bevy_picking_sprite") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_sprite" "bevy_ui" "bevy_render"))) (k 0)) (d (n "bevy_picking_core") (r "^0.2") (d #t) (k 0)))) (h "1ika7mf04srw2f6ddmbxy630jl15xfw0wvgk39m9smgvzncymh4f")))

(define-public crate-bevy_picking_sprite-0.15.0 (c (n "bevy_picking_sprite") (v "0.15.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_sprite" "bevy_render"))) (k 0)) (d (n "bevy_picking_core") (r "^0.15") (d #t) (k 0)))) (h "1idrlfk00g3ps5djp86mvmjdr07kw3287hp0h1rcfsi915g5b6qp")))

(define-public crate-bevy_picking_sprite-0.16.0 (c (n "bevy_picking_sprite") (v "0.16.0") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_asset") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "bevy_math") (r "^0.11") (k 0)) (d (n "bevy_picking_core") (r "^0.16") (d #t) (k 0)) (d (n "bevy_render") (r "^0.11") (k 0)) (d (n "bevy_sprite") (r "^0.11") (k 0)) (d (n "bevy_transform") (r "^0.11") (k 0)) (d (n "bevy_window") (r "^0.11") (k 0)))) (h "1x24xmxchcr1zqr0ag37lz87l3akq85wkxvlx6y7h6y71xy7wy62")))

(define-public crate-bevy_picking_sprite-0.17.0 (c (n "bevy_picking_sprite") (v "0.17.0") (d (list (d (n "bevy_app") (r "^0.12") (k 0)) (d (n "bevy_asset") (r "^0.12") (k 0)) (d (n "bevy_ecs") (r "^0.12") (k 0)) (d (n "bevy_math") (r "^0.12") (k 0)) (d (n "bevy_picking_core") (r "^0.17") (d #t) (k 0)) (d (n "bevy_render") (r "^0.12") (k 0)) (d (n "bevy_sprite") (r "^0.12") (k 0)) (d (n "bevy_transform") (r "^0.12") (k 0)) (d (n "bevy_window") (r "^0.12") (k 0)))) (h "0qw6r1295x9qbq1kg2p6dx4dgqlnhwgs1n0fgy496vxhf8qipznf")))

(define-public crate-bevy_picking_sprite-0.18.0 (c (n "bevy_picking_sprite") (v "0.18.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_asset") (r "^0.13") (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_math") (r "^0.13") (k 0)) (d (n "bevy_picking_core") (r "^0.18") (d #t) (k 0)) (d (n "bevy_render") (r "^0.13") (k 0)) (d (n "bevy_sprite") (r "^0.13") (k 0)) (d (n "bevy_transform") (r "^0.13") (k 0)) (d (n "bevy_window") (r "^0.13") (k 0)))) (h "0xfzb4a8jxq3hr8xicwvgr4a1c0zyai3mv5gsapgbiz84s831ibs")))

