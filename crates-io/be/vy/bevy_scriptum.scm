(define-module (crates-io be vy bevy_scriptum) #:use-module (crates-io))

(define-public crate-bevy_scriptum-0.1.0 (c (n "bevy_scriptum") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_asset"))) (k 0)) (d (n "rhai") (r "^1.14.0") (f (quote ("sync" "internals" "unchecked"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "112i486cadp6y5b83499waf9s4mxq7bga30fbvk6qkb34bifram1")))

(define-public crate-bevy_scriptum-0.2.0 (c (n "bevy_scriptum") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "rhai") (r "^1.14.0") (f (quote ("sync" "internals" "unchecked"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0frn2vc8zgk3sljh6ay246si3wbb9x1aplmpgk9zpnmww57f6r3b")))

(define-public crate-bevy_scriptum-0.2.1 (c (n "bevy_scriptum") (v "0.2.1") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "rhai") (r "^1.14.0") (f (quote ("sync" "internals" "unchecked"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1kvlflkhmh6dvjfc8j2v2zyhnm04a2v889nl8gdpl83gjqfliri2")))

(define-public crate-bevy_scriptum-0.2.2 (c (n "bevy_scriptum") (v "0.2.2") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "rhai") (r "^1.14.0") (f (quote ("sync" "internals" "unchecked"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1gs466slb1q2vmvrn6p8ykvi8484zv164v12xgr0q68n7a4hjyw9")))

(define-public crate-bevy_scriptum-0.3.0 (c (n "bevy_scriptum") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "rhai") (r "^1.14.0") (f (quote ("sync" "internals" "unchecked"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0q7wfdkd2klb75xp1ihlqd2n25s8lxmbq5hcmy4jk6hnlriz5hsg")))

(define-public crate-bevy_scriptum-0.4.0 (c (n "bevy_scriptum") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "rhai") (r "^1.14.0") (f (quote ("sync" "internals" "unchecked"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1i2h3dfcz80c9w0h5qw1zmz1dj4vq7vzlxbbb528l80mw62sqc0p")))

