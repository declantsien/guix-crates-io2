(define-module (crates-io be vy bevy_mod_check_filter_macros) #:use-module (crates-io))

(define-public crate-bevy_mod_check_filter_macros-0.2.0 (c (n "bevy_mod_check_filter_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "07h7xvc5k01jaapxvsy3xzx15c56bi936qlax1d0d0f8ack5xg74")))

(define-public crate-bevy_mod_check_filter_macros-0.2.1 (c (n "bevy_mod_check_filter_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1144fh7xjaq4cm4fp847lq4w4m0la377iz58vix348pyp9k8ql46")))

(define-public crate-bevy_mod_check_filter_macros-0.3.0 (c (n "bevy_mod_check_filter_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1rzx5xlczr50zp4xp2kbl6idxjr9d7ywqdvl6a4wwc2817imdjnv")))

