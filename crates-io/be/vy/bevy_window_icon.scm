(define-module (crates-io be vy bevy_window_icon) #:use-module (crates-io))

(define-public crate-bevy_window_icon-0.1.0 (c (n "bevy_window_icon") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "0dnawfmjrhmqdbhzw6v3p0v90j1p6pv2fmlnx9fkdv9siqygnk7l")))

(define-public crate-bevy_window_icon-0.2.0 (c (n "bevy_window_icon") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "0ldzi5f7gn9135jm02pp767vv9dwy5h9rban9ix161s2bkq1qaq6")))

