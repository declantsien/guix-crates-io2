(define-module (crates-io be vy bevy_query_ext) #:use-module (crates-io))

(define-public crate-bevy_query_ext-0.1.0 (c (n "bevy_query_ext") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "paste") (r "^1.0.14") (o #t) (d #t) (k 0)))) (h "0kxhb7jf2dalwhv2mnnq7rziqp8mrnfg4ndai450dpa8aab2vhav") (s 2) (e (quote (("all_docs" "dep:paste"))))))

(define-public crate-bevy_query_ext-0.1.1 (c (n "bevy_query_ext") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "paste") (r "^1.0.14") (o #t) (d #t) (k 0)))) (h "0mlyg7x6pvqps4hyslzpar8489g8n89wp5570crymjby4qz5vlh2") (s 2) (e (quote (("all_docs" "dep:paste"))))))

(define-public crate-bevy_query_ext-0.1.2 (c (n "bevy_query_ext") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "paste") (r "^1.0.14") (o #t) (d #t) (k 0)))) (h "1r5i42c3nwxc0wxiparaqj0ky94pxs086z6chv0r7zldpdwnkyc3") (s 2) (e (quote (("all_docs" "dep:paste"))))))

(define-public crate-bevy_query_ext-0.2.0 (c (n "bevy_query_ext") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "paste") (r "^1.0.14") (o #t) (d #t) (k 0)))) (h "0kqrhcpb8wv477zcshviqm44gmfisj5i46f4m0nrdsc9lwdz77v1") (s 2) (e (quote (("all_docs" "dep:paste"))))))

(define-public crate-bevy_query_ext-0.3.0 (c (n "bevy_query_ext") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "paste") (r "^1.0.14") (o #t) (d #t) (k 0)))) (h "07zn138qf9cjxfd2934c7daxm2pw1gcv3afdxylgbn8ljfjlm41n") (s 2) (e (quote (("all_docs" "dep:paste"))))))

