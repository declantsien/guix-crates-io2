(define-module (crates-io be vy bevy_sprite3d) #:use-module (crates-io))

(define-public crate-bevy_sprite3d-0.1.0 (c (n "bevy_sprite3d") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)))) (h "0gkqvagj3acpwbx954qpzi314f064ibai23hpwgym8fzz2hywrm0")))

(define-public crate-bevy_sprite3d-1.0.0 (c (n "bevy_sprite3d") (v "1.0.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)))) (h "1d8i7g1naqdq985rm92k57szvczh0448a6grvq7f4l9l0s2wr5bm")))

(define-public crate-bevy_sprite3d-1.1.0 (c (n "bevy_sprite3d") (v "1.1.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)))) (h "1w631vxk6m3cvhg16hjbgn9dbi49pl7brg5z6vmgn2m14j316avf")))

(define-public crate-bevy_sprite3d-2.0.0 (c (n "bevy_sprite3d") (v "2.0.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)) (d (n "bevy_asset_loader") (r "^0.12.1") (f (quote ("2d"))) (d #t) (k 2)))) (h "0pw3wwn1xaihh7vhiyxma7pqjvly9ipfab8pn7akz2cn69br83a2")))

(define-public crate-bevy_sprite3d-2.1.0 (c (n "bevy_sprite3d") (v "2.1.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "bevy_asset_loader") (r "^0.14") (f (quote ("2d"))) (d #t) (k 2)))) (h "00hc6fbkp1bqfrh54nkkh2y41igbwimwdk1wkmi94wdibsbcpzm9")))

(define-public crate-bevy_sprite3d-2.2.0 (c (n "bevy_sprite3d") (v "2.2.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "bevy_asset_loader") (r "^0.14") (f (quote ("2d"))) (d #t) (k 2)))) (h "1an8ma2gs67d82pgv43la31rmrd64lwfhczhsjycd2s1zxcpg5g6")))

(define-public crate-bevy_sprite3d-2.3.0 (c (n "bevy_sprite3d") (v "2.3.0") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0zz0ibcb0zapw48i9j1vdi5nxvs5nhx6qjvy37jyx28x45sxa3d1")))

(define-public crate-bevy_sprite3d-2.3.1 (c (n "bevy_sprite3d") (v "2.3.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "16yz9fkwjhmhzzc87cryf05vdc9nrsiwj7p2p71h77bjnn2wks38")))

(define-public crate-bevy_sprite3d-2.4.0 (c (n "bevy_sprite3d") (v "2.4.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0a6f4179gfqa2yk5scysdw8bvxp576wyyz7mg1ph08hhpvakv1rz")))

(define-public crate-bevy_sprite3d-2.5.0 (c (n "bevy_sprite3d") (v "2.5.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0dk5b14hq6rzw2z688a1qb2a0kgsn6xsrpfkw648nvwk443nlxqi")))

(define-public crate-bevy_sprite3d-2.6.0 (c (n "bevy_sprite3d") (v "2.6.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_pbr" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1r9qc8c4wd8hh6ikypk7sd5v7rv6s14zizjcaxlv61mlfs8s80q4")))

(define-public crate-bevy_sprite3d-2.7.0 (c (n "bevy_sprite3d") (v "2.7.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_pbr" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "14lvc9dl39ml9ck55iyysdaq28446kq2w7i2j4xh00jw9nn80fdf")))

(define-public crate-bevy_sprite3d-2.8.0 (c (n "bevy_sprite3d") (v "2.8.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_pbr" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1zf3lyw1nz4l1y83n3nmny770kllm97vhi6ffrgrqf9nmabgvna2")))

