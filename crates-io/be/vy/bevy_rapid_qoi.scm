(define-module (crates-io be vy bevy_rapid_qoi) #:use-module (crates-io))

(define-public crate-bevy_rapid_qoi-0.1.0 (c (n "bevy_rapid_qoi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "~0.8") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "~0.8") (d #t) (k 2)) (d (n "qoi") (r "^0.4") (d #t) (k 0)))) (h "11dgg1zkf2jdwf3zrm3741qmxbwnpkhmn64xsbdcs4kwxwv1s8iw")))

(define-public crate-bevy_rapid_qoi-1.0.0 (c (n "bevy_rapid_qoi") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "~0.8") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "~0.8") (d #t) (k 2)) (d (n "rapid-qoi") (r "^0.6.1") (d #t) (k 0)))) (h "1ixg1msgf4qk9kpm4inqhhgq65v5r9wfr4cwrqxl5xm9adci73hw")))

