(define-module (crates-io be vy bevy_assetio_zip_bundler) #:use-module (crates-io))

(define-public crate-bevy_assetio_zip_bundler-0.1.0 (c (n "bevy_assetio_zip_bundler") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)) (d (n "xorio") (r "^0.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "1whlag6mcm0w66la2jly8jlwgl7mq12vf7dkksiidi75rv981wzk") (f (quote (("default" "bundle-crate-assets") ("bundle-crate-assets" "serde" "toml"))))))

