(define-module (crates-io be vy bevy_retrograde_ldtk) #:use-module (crates-io))

(define-public crate-bevy_retrograde_ldtk-0.0.0 (c (n "bevy_retrograde_ldtk") (v "0.0.0") (h "00xkrb6ms46pcsalqmg8246h2xb1cw1h6gg94rlhpk523ppxpcy0")))

(define-public crate-bevy_retrograde_ldtk-0.1.0 (c (n "bevy_retrograde_ldtk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy_retrograde_core") (r "^0.1") (d #t) (k 0)) (d (n "ldtk") (r "^0.4") (f (quote ("ldtk-v0-9-3"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1nqx7rssslvzg540cmyxzmm3d7b51lbpish1kcmib8l7mp6bzrfk") (f (quote (("default"))))))

(define-public crate-bevy_retrograde_ldtk-0.2.0 (c (n "bevy_retrograde_ldtk") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "bevy") (r "^0.5") (k 0)) (d (n "bevy_retrograde_core") (r "^0.2") (d #t) (k 0)) (d (n "ldtk") (r "^0.4") (f (quote ("ldtk-v0-9-3"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "06y47rqp21bn37sigvl24wj941y6bwl2aglglqvy30pxddlay8sm") (f (quote (("default"))))))

