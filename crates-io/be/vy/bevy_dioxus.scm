(define-module (crates-io be vy bevy_dioxus) #:use-module (crates-io))

(define-public crate-bevy_dioxus-0.1.0 (c (n "bevy_dioxus") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "bevy_dioxus_desktop") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "dioxus") (r "^0.2") (d #t) (k 2)) (d (n "leafwing-input-manager") (r "^0.3") (k 2)))) (h "1alcv87470y6slxdp8qk3sm38whzprwcsm42322sq75jn3sc2i3w") (f (quote (("desktop" "bevy_dioxus_desktop") ("default" "desktop"))))))

(define-public crate-bevy_dioxus-0.1.1 (c (n "bevy_dioxus") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "bevy_dioxus_core") (r "^0.1") (d #t) (k 0)) (d (n "bevy_dioxus_desktop") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bevy_dioxus_macro") (r "^0.1") (d #t) (k 0)) (d (n "dioxus") (r "^0.2") (f (quote ("fermi"))) (d #t) (k 2)) (d (n "leafwing-input-manager") (r "^0.3") (k 2)))) (h "0jx76d312v1r8d6svvs6c7y8b2009nq6yprk0nna1v0kmc6ya6q7") (f (quote (("desktop" "bevy_dioxus_desktop") ("default" "desktop"))))))

