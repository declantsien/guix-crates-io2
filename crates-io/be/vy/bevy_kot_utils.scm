(define-module (crates-io be vy bevy_kot_utils) #:use-module (crates-io))

(define-public crate-bevy_kot_utils-0.0.12 (c (n "bevy_kot_utils") (v "0.0.12") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "10fm29d2kjd6q6ya53lrrcnsa046ca61yjrs1jjl7xxszs3sy4sp")))

(define-public crate-bevy_kot_utils-0.1.0 (c (n "bevy_kot_utils") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "0aa1kpaz9pc5fxzfpkcc7zxccssbvgdsd7wvfqql7yzq1rwxv3xy")))

(define-public crate-bevy_kot_utils-0.2.0 (c (n "bevy_kot_utils") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "0fjagifbwfcszg15qr2ghyjx1zbi0sfxix039i5z291x25ja4kbw")))

(define-public crate-bevy_kot_utils-0.3.1 (c (n "bevy_kot_utils") (v "0.3.1") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "097q2c22hss76hn9k5gga8rhgvvpz2wln90awc8r9qr4ylixkvy4")))

(define-public crate-bevy_kot_utils-0.4.0 (c (n "bevy_kot_utils") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "0szfh6v2cw2ymv26kb2izjg967yn4qizmmgnird1s7d4yppx1gnx")))

(define-public crate-bevy_kot_utils-0.5.0 (c (n "bevy_kot_utils") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "0q8fm2chv9lqxdbnd2253xcmgi1i78jmd2awq67azwhsi8vhx844")))

(define-public crate-bevy_kot_utils-0.6.0 (c (n "bevy_kot_utils") (v "0.6.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "0ckk45lg4wj8cgwv03sm7a5yy9ll6iyr0nnxg0jbc2a8828kxyba")))

(define-public crate-bevy_kot_utils-0.7.0 (c (n "bevy_kot_utils") (v "0.7.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "0z6gdv46dhd4jr6fbbrmvlpjdv8xnn07g8k2g3ywyfazzxdh0026")))

(define-public crate-bevy_kot_utils-0.8.0 (c (n "bevy_kot_utils") (v "0.8.0") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "1wsq9vwqvhihnw5xjjp3flfqhmav6hxrrr5hik8vl7mvlr73vv9d")))

(define-public crate-bevy_kot_utils-0.9.0 (c (n "bevy_kot_utils") (v "0.9.0") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "0y91innhqrg2r06434ci1f17vgah2zjcwp247q8dz1vld3q8grjr")))

(define-public crate-bevy_kot_utils-0.9.1 (c (n "bevy_kot_utils") (v "0.9.1") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "1qzr88qxybk1wa69rsq99n4s6dx1qvngn0fimlr5fb9rlpfarri3")))

(define-public crate-bevy_kot_utils-0.9.2 (c (n "bevy_kot_utils") (v "0.9.2") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "08lfgyd6afmjng2dbghk97q8cjn06m6k561gxlhb79yl3ycz17mv")))

(define-public crate-bevy_kot_utils-0.10.0 (c (n "bevy_kot_utils") (v "0.10.0") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "166qnq16vfhfljhzrb4zf3g08gwf36iampxijrym49mafwvk3dgv")))

(define-public crate-bevy_kot_utils-0.10.1 (c (n "bevy_kot_utils") (v "0.10.1") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "0n6li3zxq62zv1hmwr8h1bmw6ap67ph3dgb0vin4z246z19vs1j9")))

(define-public crate-bevy_kot_utils-0.10.2 (c (n "bevy_kot_utils") (v "0.10.2") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "0njz006b9d8xmsbdpmdqbqqv8km50wd3h2fh7lc4za4w8vhrvbqa")))

(define-public crate-bevy_kot_utils-0.10.3 (c (n "bevy_kot_utils") (v "0.10.3") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "17gm6ja3zcakcps4lfkzpvh9ipwqyjbpxsk8jm0mg2x49ykhvz33")))

(define-public crate-bevy_kot_utils-0.11.0 (c (n "bevy_kot_utils") (v "0.11.0") (d (list (d (n "async-channel") (r "^2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "030v9k19dwizvssbj0yglqmmx213sx399zzn7jr686cqadvbd06c")))

