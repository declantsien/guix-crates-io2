(define-module (crates-io be vy bevy_global_input) #:use-module (crates-io))

(define-public crate-bevy_global_input-0.1.0 (c (n "bevy_global_input") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "mouce") (r "^0.2.41") (d #t) (k 0)))) (h "09zhdmmia299mjmq6w9m6bjsc3x025rwqfj1g2k1c0x8lvv4r5xn") (f (quote (("default"))))))

(define-public crate-bevy_global_input-0.2.0 (c (n "bevy_global_input") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "mouce") (r "^0.2.41") (d #t) (k 0)))) (h "1h2k99kxx0fz8f1wsj5piwi28v0bix0cplccx9mrgc8ndgh7hyxr") (f (quote (("default"))))))

(define-public crate-bevy_global_input-0.2.1 (c (n "bevy_global_input") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "mouce") (r "^0.2.41") (d #t) (k 0)))) (h "0a1ll4i4gbpw2j8ziv92qkd8kch5q3gw67sf5k3khjp03ypjv7hj") (f (quote (("default"))))))

(define-public crate-bevy_global_input-0.3.0 (c (n "bevy_global_input") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "mouce") (r "^0.2.41") (d #t) (k 0)))) (h "04n1afqi0zl95mhhqw0vqqa1dyrrg1aawyhhpd7w89549s9fvc37") (f (quote (("default"))))))

(define-public crate-bevy_global_input-0.4.0 (c (n "bevy_global_input") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "mouce") (r "^0.2.43") (d #t) (k 0)))) (h "0zb0l18zf2pd07gb5y3wwzhl5hjz9143slfabxzclk1a9az2v8r8") (f (quote (("default"))))))

