(define-module (crates-io be vy bevy_mod_ui_sprite) #:use-module (crates-io))

(define-public crate-bevy_mod_ui_sprite-0.1.0 (c (n "bevy_mod_ui_sprite") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1717z2vxibgnaz84cpl72hkvldyrsxv6lvw7zrpkhj9bnpg4ghvr")))

(define-public crate-bevy_mod_ui_sprite-0.1.1 (c (n "bevy_mod_ui_sprite") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1rj06xq3lgzvzqb2y8jnaf5vclj3insy0qb8shjz18sbf0jzdkwq")))

(define-public crate-bevy_mod_ui_sprite-0.1.2 (c (n "bevy_mod_ui_sprite") (v "0.1.2") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1wf5gwprgs7bahh4fprnngn3fv9z7a8qw2q3w9kz7gwyjvb0lz0x")))

(define-public crate-bevy_mod_ui_sprite-0.2.0 (c (n "bevy_mod_ui_sprite") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1nv42niwhacz2xv18p3zvvji35jayaygml54f7ygs9nk33ncl7y0")))

(define-public crate-bevy_mod_ui_sprite-0.2.1 (c (n "bevy_mod_ui_sprite") (v "0.2.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "14v4gnly96ikxlllnwm3b3axnb5mhmlwqqpbifv72d1p52z2dyr9")))

