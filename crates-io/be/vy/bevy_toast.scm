(define-module (crates-io be vy bevy_toast) #:use-module (crates-io))

(define-public crate-bevy_toast-0.1.0 (c (n "bevy_toast") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6.1") (f (quote ("png"))) (k 0)) (d (n "bevy-inspector-egui") (r "^0.8.2") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.3.3") (d #t) (k 0)))) (h "0r4wpj5fj8r6rscd0x39xgn8zf9yi948ijw5yqjcd5lhjjb4wxwf")))

(define-public crate-bevy_toast-0.1.1 (c (n "bevy_toast") (v "0.1.1") (d (list (d (n "bevy") (r "^0.6.1") (f (quote ("png"))) (k 0)) (d (n "bevy-inspector-egui") (r "^0.8.2") (d #t) (k 2)) (d (n "bevy_tweening") (r "^0.3.3") (d #t) (k 0)))) (h "0ykas9zm0pmz9rl0hfc735qc6rcmidb0q6lnq8kgz28cfa4l85m1")))

