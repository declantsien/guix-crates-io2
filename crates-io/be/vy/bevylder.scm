(define-module (crates-io be vy bevylder) #:use-module (crates-io))

(define-public crate-bevylder-0.1.0 (c (n "bevylder") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (f (quote ("render"))) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.4.0") (d #t) (k 2)))) (h "01gjq3qj10dvwjs0paysvxn78mc3i9sbcgi42fqv6wqhqprni5f0")))

