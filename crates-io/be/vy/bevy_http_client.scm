(define-module (crates-io be vy bevy_http_client) #:use-module (crates-io))

(define-public crate-bevy_http_client-0.1.0 (c (n "bevy_http_client") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (k 0)) (d (n "ehttp") (r "^0.3.0") (f (quote ("native-async"))) (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)))) (h "158x0py0mq2mr9m8m9yy9j5x9wvwadqmllbiw7nda7d97zxx4s0v")))

(define-public crate-bevy_http_client-0.2.0 (c (n "bevy_http_client") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("multi-threaded"))) (k 0)) (d (n "ehttp") (r "^0.3.0") (f (quote ("native-async"))) (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)))) (h "09v0vkab8plyn9q4qfnfkw6wmldajavd6jkdiy5wzpixhxjmcqyd")))

(define-public crate-bevy_http_client-0.3.0 (c (n "bevy_http_client") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("multi-threaded"))) (k 0)) (d (n "ehttp") (r "^0.4.0") (f (quote ("native-async"))) (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cijk8hwnnv16cw45s697w98mfsy18bkr2nayw7fijfpzmz4r65q")))

(define-public crate-bevy_http_client-0.4.0 (c (n "bevy_http_client") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("multi-threaded"))) (k 0)) (d (n "ehttp") (r "^0.5.0") (f (quote ("native-async"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dwfknfx5h4fywd0ni3d8aziqphhkviaaj8bq93jg4f95hh4kdq6")))

(define-public crate-bevy_http_client-0.5.0 (c (n "bevy_http_client") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("multi-threaded"))) (k 0)) (d (n "ehttp") (r "^0.5.0") (f (quote ("native-async" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y2b6apil05ypav7w2sqg7qq44z1jnj98ympz70jy4haksgjb8f2")))

(define-public crate-bevy_http_client-0.5.1 (c (n "bevy_http_client") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("multi-threaded"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "ehttp") (r "^0.5.0") (f (quote ("native-async" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dybydk07bqwxq3d8rphaqr62ryff84kzwbjs40dq4xasa1zhygp")))

(define-public crate-bevy_http_client-0.5.2 (c (n "bevy_http_client") (v "0.5.2") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("multi-threaded"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "ehttp") (r "^0.5.0") (f (quote ("native-async" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r2h80ri8qvw57011cxlkmgbr7bjq9sxwwv1bh2mdqdcli8ln0lc")))

