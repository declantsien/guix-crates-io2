(define-module (crates-io be vy bevy-settings) #:use-module (crates-io))

(define-public crate-bevy-settings-0.0.1 (c (n "bevy-settings") (v "0.0.1") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy_app") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "12ry57y8gr7x6znyki8xfrxvcy2sdrw250042m49i804nw0yiapd")))

(define-public crate-bevy-settings-0.0.2 (c (n "bevy-settings") (v "0.0.2") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy_app") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "0m5adwvsjwpbxwqqy2dxswadqrgnaq7j4jp94bmlsvgs08f61bch")))

(define-public crate-bevy-settings-0.1.0 (c (n "bevy-settings") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy_app") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.7.1") (d #t) (k 0)))) (h "0yiv7dfkcp8bx7xrdgbizmis60xdjzpbrawrwdlmp4akjlz9xi08")))

(define-public crate-bevy-settings-0.2.0 (c (n "bevy-settings") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.10.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.7.1") (d #t) (k 0)))) (h "1s0k6lr79vcaaaihjn19swbgz6r19drjvnzsbrfajxaazbvknzl1")))

(define-public crate-bevy-settings-0.3.0 (c (n "bevy-settings") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "14wmqknxz9g43viwh1zd5srqgkrn6mv7vyz1awi54lqh3wbz68mk")))

(define-public crate-bevy-settings-0.3.1 (c (n "bevy-settings") (v "0.3.1") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1vnb44hc6wv1izh1rqmqanvz0cbgvf5c4j1lwqa89ki5mm2l8jmq")))

(define-public crate-bevy-settings-0.4.0 (c (n "bevy-settings") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1qmyk6ywp8f6vmvrggh06ic7pkqnfvwy3mhk4ilxmd2aizgg95jw")))

(define-public crate-bevy-settings-0.5.0 (c (n "bevy-settings") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.0") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0sbyv2a5chz4fliza6yap0xpb8nppy2adp1wpbcsabzmi0v3ay6c")))

(define-public crate-bevy-settings-0.5.1 (c (n "bevy-settings") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_app") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "bevy_log") (r "^0.13") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0kz7hz3r2ki4z1qv8r7dx6z896g2lx9wcmm6qiilqipj1wcyl884")))

