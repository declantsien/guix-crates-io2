(define-module (crates-io be vy bevy_kot_misc) #:use-module (crates-io))

(define-public crate-bevy_kot_misc-0.10.0 (c (n "bevy_kot_misc") (v "0.10.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_derive") (r "^0.10.0") (k 0)) (d (n "bevy_kot_ecs") (r "^0.10.0") (k 0)))) (h "14zili77rqxfk6pigq1xngcrq9zcmyr73m9l4xar7cv252bf5yws")))

(define-public crate-bevy_kot_misc-0.10.1 (c (n "bevy_kot_misc") (v "0.10.1") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_derive") (r "^0.10.1") (k 0)) (d (n "bevy_kot_ecs") (r "^0.10.1") (k 0)))) (h "0pl49b1wx3y5cs8923m0n2505jfdxd7ra0b5vxfgrakflvx9s7vh")))

(define-public crate-bevy_kot_misc-0.10.2 (c (n "bevy_kot_misc") (v "0.10.2") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_derive") (r "^0.10.2") (k 0)) (d (n "bevy_kot_ecs") (r "^0.10.2") (k 0)))) (h "18dbp1zkafg3b9nmjz8d7b37ydpbvnxs50rcj48xmfxvan05hl6b")))

(define-public crate-bevy_kot_misc-0.10.3 (c (n "bevy_kot_misc") (v "0.10.3") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_derive") (r "^0.10.3") (k 0)) (d (n "bevy_kot_ecs") (r "^0.10.3") (k 0)))) (h "1gfsw038wjaqx75ic2mincx68illhkscy3dbp41hx198rnqwnqd5")))

(define-public crate-bevy_kot_misc-0.11.0 (c (n "bevy_kot_misc") (v "0.11.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy_fn_plugin") (r "^0.1") (d #t) (k 0)) (d (n "bevy_kot_derive") (r "^0.11.0") (k 0)) (d (n "bevy_kot_ecs") (r "^0.11.0") (k 0)))) (h "09hpvlpbrsxf1ciqzwbgciaypixr4qy1czja9mbld54ykasz1cp0")))

