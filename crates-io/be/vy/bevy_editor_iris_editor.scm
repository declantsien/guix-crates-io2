(define-module (crates-io be vy bevy_editor_iris_editor) #:use-module (crates-io))

(define-public crate-bevy_editor_iris_editor-0.1.0 (c (n "bevy_editor_iris_editor") (v "0.1.0") (d (list (d (n "bevy_editor_iris_common") (r "^0.1") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.14.0") (d #t) (k 0)))) (h "1x5wg5ql5ls6hq7d2ckdnhkjxlmp7v7yz7xcni4hb5jypnkjnbvl")))

