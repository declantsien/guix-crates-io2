(define-module (crates-io be vy bevy_mod_rounded_box) #:use-module (crates-io))

(define-public crate-bevy_mod_rounded_box-0.1.0 (c (n "bevy_mod_rounded_box") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.7") (f (quote ("bevy_winit" "x11"))) (k 2)))) (h "050pfvbx7bgslxbj9zd4xdzsz9vgjqkxszqqb82x4i4lzvc7hrga")))

(define-public crate-bevy_mod_rounded_box-0.2.0 (c (n "bevy_mod_rounded_box") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_winit" "x11" "png"))) (k 2)))) (h "0wnz4clm96g0zghxaqch4sqsg3mwkp0q5m6104jdn49w61zhs63v") (f (quote (("uvf") ("default" "uvf"))))))

(define-public crate-bevy_mod_rounded_box-0.3.0 (c (n "bevy_mod_rounded_box") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_winit" "x11" "png"))) (k 2)))) (h "01m2kyyjqn973p9v0aq011nkcyiskrkhlh6gzjkwpz5zpvmq6x4z") (f (quote (("uvf") ("default" "uvf"))))))

(define-public crate-bevy_mod_rounded_box-0.4.0 (c (n "bevy_mod_rounded_box") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_core_pipeline" "bevy_pbr" "bevy_winit" "x11" "png"))) (k 2)))) (h "0yvlahh9kmwnvlkhj3q9475gis9v0nml0pwz8qvvfw6r3zvy7wwl") (f (quote (("uvf") ("default" "uvf"))))))

(define-public crate-bevy_mod_rounded_box-0.5.0 (c (n "bevy_mod_rounded_box") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_pbr" "bevy_winit" "tonemapping_luts" "x11" "png" "ktx2" "zstd"))) (k 2)))) (h "0q6ks2jw13l5si71x5f74748j7ybhcqqs8fspd35ss5qg0a4xix2") (f (quote (("uvf") ("default" "uvf"))))))

(define-public crate-bevy_mod_rounded_box-0.6.0 (c (n "bevy_mod_rounded_box") (v "0.6.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline" "bevy_pbr" "bevy_winit" "tonemapping_luts" "x11" "png" "ktx2" "zstd"))) (k 2)))) (h "1krn42ff8baz7lg9ikgmhk0idq8jy9vqi1af1n275lbi5bdbzwn4") (f (quote (("uvf") ("default" "uvf"))))))

(define-public crate-bevy_mod_rounded_box-0.7.0 (c (n "bevy_mod_rounded_box") (v "0.7.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_core_pipeline" "bevy_pbr" "bevy_winit" "tonemapping_luts" "x11" "png" "ktx2" "zstd"))) (k 2)))) (h "0w54cpw0l81r0ixycn3sbi66h1r1i9241ppfbb9vr8xzyhf0zlc7") (f (quote (("uvf") ("default" "uvf"))))))

