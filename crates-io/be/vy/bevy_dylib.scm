(define-module (crates-io be vy bevy_dylib) #:use-module (crates-io))

(define-public crate-bevy_dylib-0.4.0 (c (n "bevy_dylib") (v "0.4.0") (d (list (d (n "bevy_internal") (r "^0.4.0") (k 0)))) (h "058z8pxr4pq873x4jx9kg7p4jd1q9wmfarhv0hh0aqrd23kwv9kc")))

(define-public crate-bevy_dylib-0.5.0 (c (n "bevy_dylib") (v "0.5.0") (d (list (d (n "bevy_internal") (r "^0.5.0") (k 0)))) (h "11ab2d11hcxnd0fhizbq4sahnk39qpw7ncq7k99nj2a70dy2r9pg")))

(define-public crate-bevy_dylib-0.6.0 (c (n "bevy_dylib") (v "0.6.0") (d (list (d (n "bevy_internal") (r "^0.6.0") (k 0)))) (h "1lszsrjmmw9gy34afvw2mv16zxm3fhdhgh9qjv6m7bv99g8zpr1p")))

(define-public crate-bevy_dylib-0.7.0 (c (n "bevy_dylib") (v "0.7.0") (d (list (d (n "bevy_internal") (r "^0.7.0") (k 0)))) (h "11p7sss5dahy6x21f08p4f50bmh5njlwg3dk9kckqx3qlpkjrw3b")))

(define-public crate-bevy_dylib-0.8.0 (c (n "bevy_dylib") (v "0.8.0") (d (list (d (n "bevy_internal") (r "^0.8.0") (k 0)))) (h "1gpqdvkkq3pd2wg85n601r19agbz7y05dnqa9cqy9f55jfdamxdx")))

(define-public crate-bevy_dylib-0.8.1 (c (n "bevy_dylib") (v "0.8.1") (d (list (d (n "bevy_internal") (r "^0.8.0") (k 0)))) (h "0azkf8lm7m4kfklnnkphw2hkcv7m3si24cpd5nmkhy035dhw56lv")))

(define-public crate-bevy_dylib-0.9.0 (c (n "bevy_dylib") (v "0.9.0") (d (list (d (n "bevy_internal") (r "^0.9.0") (k 0)))) (h "1aff8l5hgmg3dd3ggdi8wf47cmz0fgbrxm4a0fknxhlzfa8wqlhd")))

(define-public crate-bevy_dylib-0.9.1 (c (n "bevy_dylib") (v "0.9.1") (d (list (d (n "bevy_internal") (r "^0.9.1") (k 0)))) (h "0dppvqyzxzkycxh85ammz1idipvxsrfx29v3b43d3gg3aqykq6ax")))

(define-public crate-bevy_dylib-0.10.0 (c (n "bevy_dylib") (v "0.10.0") (d (list (d (n "bevy_internal") (r "^0.10.0") (k 0)))) (h "00blc38qj19z4zzvvw6i517x88anp5nh6z2ahc5q0rg9fc9wk792")))

(define-public crate-bevy_dylib-0.10.1 (c (n "bevy_dylib") (v "0.10.1") (d (list (d (n "bevy_internal") (r "^0.10.1") (k 0)))) (h "14wiqxfd6bjqh2ashx9x83kc47w8rn7jdf88mfg7yn71w4swfmjj")))

(define-public crate-bevy_dylib-0.11.0 (c (n "bevy_dylib") (v "0.11.0") (d (list (d (n "bevy_internal") (r "^0.11.0") (k 0)))) (h "17rz7fvhmzjnizgjz2zrh596bjvi4cj56xjmikv7sd3xx7m89m3s")))

(define-public crate-bevy_dylib-0.11.1 (c (n "bevy_dylib") (v "0.11.1") (d (list (d (n "bevy_internal") (r "^0.11.1") (k 0)))) (h "1818llvvdzykpvnjgjlj1696v6aafzlkz5laihcyncf0qycc2s56")))

(define-public crate-bevy_dylib-0.11.2 (c (n "bevy_dylib") (v "0.11.2") (d (list (d (n "bevy_internal") (r "^0.11.2") (k 0)))) (h "16rd1wbvq6fn57x02fx9g1d253q7ijwv5x1v5gh0lcnmygld21r1")))

(define-public crate-bevy_dylib-0.11.3 (c (n "bevy_dylib") (v "0.11.3") (d (list (d (n "bevy_internal") (r "^0.11.3") (k 0)))) (h "0g46a6f8jpq1pb10j4k68q1lq71zhz2qig7q6yfir3h263w13si2")))

(define-public crate-bevy_dylib-0.12.0 (c (n "bevy_dylib") (v "0.12.0") (d (list (d (n "bevy_internal") (r "^0.12.0") (k 0)))) (h "0nnwxqyqkrina9xf4dzzc8zgbs48mcfka2j9lxikai814j0q0sbn")))

(define-public crate-bevy_dylib-0.12.1 (c (n "bevy_dylib") (v "0.12.1") (d (list (d (n "bevy_internal") (r "^0.12.1") (k 0)))) (h "0g45m5lyxbk9nmadlb2a35msj7m6zlr8pk33kj6wfds8xc0r1fa5")))

(define-public crate-bevy_dylib-0.13.0 (c (n "bevy_dylib") (v "0.13.0") (d (list (d (n "bevy_internal") (r "^0.13.0") (k 0)))) (h "0721r7xrm9iyk5aq6l471nsbpwra5lnnycg58kwshkbs1mpvgcy3")))

(define-public crate-bevy_dylib-0.13.1 (c (n "bevy_dylib") (v "0.13.1") (d (list (d (n "bevy_internal") (r "^0.13.1") (k 0)))) (h "0ybiplq0s0i90v3fhzk90wvg2f9ig7niv31xxfkkajzaq8kprrd2")))

(define-public crate-bevy_dylib-0.13.2 (c (n "bevy_dylib") (v "0.13.2") (d (list (d (n "bevy_internal") (r "^0.13.2") (k 0)))) (h "1k306s83716znr6sm3xbhz70q9k2lvn8rqa9df1ijz7kp3ijca4j")))

