(define-module (crates-io be vy bevy_discovery) #:use-module (crates-io))

(define-public crate-bevy_discovery-0.1.0 (c (n "bevy_discovery") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18sv84cnrcf0mlk0dr98zwvb14s9lc4qd57q2il1d359212jjqzp")))

