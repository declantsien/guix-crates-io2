(define-module (crates-io be vy bevy_mod_ui_texture_atlas_image) #:use-module (crates-io))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.0 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1v981fik4gdq7qywygr7bmsnw3nn67mgm9nvsl1nnmgav17gxm7q")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.1 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1f779v12ad1vhv4wxxlx3rc9a9vcz8sslrsfxyj0ji3fhqa1cdiz")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.2 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.2") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0z1hcjxv0bshvklvpgqxqyhzvr4yg7lgilzldwr6gcqxnlnmgq6z")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.3 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.3") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1y653a166ffzwdlq4b2wx9yidmlv2q44gxi5by90mlb8fzm0i58c")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.4 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.4") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "10c7fsl220mrpjnhrwvvl8zki38f80rxscykikpvmk7jqzycs5bh")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.5 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.5") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1ckcsj7d9kgzhzr23q6ixbqbgz7zar60zxjraqqb6spnna9plybr")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.6 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.6") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "06d9m7nyha354wc1s0rl4gjhl1d9ic6lbhy1drd1y6q12b3b0lz8")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.7 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.7") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1zg2qlv6i1fwrk3hn07jyliwh2qakfp9gygzavn1w540xq20wcn5")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.8 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.8") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0l9vr9j0bd6hh0a01aldrgxy05n3lp85mz68ggg4cfq8c2qv2w14")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.9 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.9") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "05sb4w4jfhl7m9p7y74p0lzrks9arplc2ww9ki2jnfyirsfyc6gw")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.1.10 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.1.10") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0nsmq63dw3ga11pam24s0902gnwnf3wxv4d9hh1rhqn877lx2plb")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.2.0 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "1l3qz671rjlrlfrdgnfpa0kzpqw171n3xplj8qkr56b3pbjzvccv")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.2.1 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "1a3fn6kdk533mkn5fdd105wjsd2p2czy2lkd526zxcgjcqc4q6jv")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.2.2 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.2.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "0k6zkj5nv237ck5cqyfksxkp5vmfi41kjpj6q0jj469qs71mf25m")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.2.3 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.2.3") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "1a9dx8nm1nwari92bxswwr7z4a30syc3jyn2f86j5vfncfm0lz25")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.2.4 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.2.4") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "0yw95xnflnyb5z37f2pmdk73s2br85srv7rvqmsdpbkl7bs4ps4j")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.3.0 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_render" "bevy_ui" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "0snpcz9hs64n83jpcd5p8zx7kvds1glwfi6cb8fswqm5dmf9xham")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.4.0 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_render" "bevy_ui" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "01bc9y7nz9wxidjvw1mn5xbk5i5qjwr16knb109l3a5vv85yyhx1")))

(define-public crate-bevy_mod_ui_texture_atlas_image-0.4.1 (c (n "bevy_mod_ui_texture_atlas_image") (v "0.4.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_render" "bevy_ui" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "1mhj5k974cqasxssaciwgsavrshyd0b2ky6zjymghd9g8dllfk9a")))

