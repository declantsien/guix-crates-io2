(define-module (crates-io be vy bevy-mutate-image) #:use-module (crates-io))

(define-public crate-bevy-mutate-image-0.1.0 (c (n "bevy-mutate-image") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "05cpsvj8y9q3i58vc1y7skbph02nwbl544y7pqal11g7x1492mph") (f (quote (("default"))))))

