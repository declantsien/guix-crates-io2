(define-module (crates-io be vy bevy_manim) #:use-module (crates-io))

(define-public crate-bevy_manim-0.1.0 (c (n "bevy_manim") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "lyon") (r "^0.17.5") (d #t) (k 0)))) (h "0mdwhib63d8g1gg5032nqc9i94578yfymm2f2sf28ra5z1dpimzl")))

