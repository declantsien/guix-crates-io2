(define-module (crates-io be vy bevy_proto_resource_tuples) #:use-module (crates-io))

(define-public crate-bevy_proto_resource_tuples-0.1.0 (c (n "bevy_proto_resource_tuples") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_app") (r "^0.10") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10") (d #t) (k 0)) (d (n "bevy_proto_resource_tuples_macros") (r "^0.1") (d #t) (k 0)))) (h "0bh3vm0b4bg63z86l7i4f03d8jw9lni9ifcv0jzblpj5j6i39kl7")))

