(define-module (crates-io be vy bevy_asset_packer) #:use-module (crates-io))

(define-public crate-bevy_asset_packer-0.1.0 (c (n "bevy_asset_packer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset"))) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.12") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1cd4vwarqpgr3gyzjp8v04yij7ggzvfbl9m703fkkjayvmy0s2gp")))

(define-public crate-bevy_asset_packer-0.2.0 (c (n "bevy_asset_packer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset"))) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.12") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0wzkiq0fbdcf1alijvdmgh11kbhjr4p1j8craf591p4b8i3cid3h")))

(define-public crate-bevy_asset_packer-0.3.0 (c (n "bevy_asset_packer") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset"))) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.12") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (f (quote ("std" "simd"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0k83lars9zyx4zv127a3jjqa39h1z1hwgimxv2f6ny6jss004gpg")))

(define-public crate-bevy_asset_packer-0.4.0 (c (n "bevy_asset_packer") (v "0.4.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "belt-ctr") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset"))) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (f (quote ("std" "simd"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1vry9caigkjvcxhgk7yx5nxwb0mxbci66d3qkai8i57mm2qmg3ff")))

