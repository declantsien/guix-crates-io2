(define-module (crates-io be vy bevy-tnua-xpbd2d) #:use-module (crates-io))

(define-public crate-bevy-tnua-xpbd2d-0.1.0 (c (n "bevy-tnua-xpbd2d") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.3") (d #t) (k 0)))) (h "05a9hypcadhw2k7ggcwr4lyiz5jldspfqiwcvg0c6qaij7qi6h5w")))

(define-public crate-bevy-tnua-xpbd2d-0.1.1 (c (n "bevy-tnua-xpbd2d") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.3") (d #t) (k 0)))) (h "0kpr6vqap2d5di6bv0b3s3cxcbwzp3zva1fb3gsca4p9ysapwzzd")))

(define-public crate-bevy-tnua-xpbd2d-0.2.0 (c (n "bevy-tnua-xpbd2d") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.2") (d #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.4") (d #t) (k 0)))) (h "1vpnpnsc851zg02d4nyk3gvfdig56mgvfhnn1ick3vdh4fj6z3vh")))

(define-public crate-bevy-tnua-xpbd2d-0.3.0 (c (n "bevy-tnua-xpbd2d") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.4") (f (quote ("2d" "debug-plugin" "parallel"))) (k 0)))) (h "0945jh63xirm4s3jhgxzr7ls4hlni6nhx5yg32qkni9g5a528kx5") (f (quote (("f64" "bevy_xpbd_2d/parry-f64" "bevy-tnua-physics-integration-layer/f64") ("default" "bevy_xpbd_2d/parry-f32"))))))

(define-public crate-bevy-tnua-xpbd2d-0.4.0 (c (n "bevy-tnua-xpbd2d") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.4") (f (quote ("2d" "debug-plugin" "parallel"))) (k 0)))) (h "1v0iiahaihjlkdizc00z0dc5zcg2db7z208aci00pgn71p5xbi5r") (f (quote (("f64" "bevy_xpbd_2d/parry-f64" "bevy-tnua-physics-integration-layer/f64") ("default" "bevy_xpbd_2d/parry-f32"))))))

