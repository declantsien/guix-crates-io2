(define-module (crates-io be vy bevy_plot) #:use-module (crates-io))

(define-public crate-bevy_plot-0.1.0 (c (n "bevy_plot") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0hdf898pl7ymmhin2hnixvsiji2s8j81m7pdwj8bzrc492cl71w8") (y #t)))

(define-public crate-bevy_plot-0.1.1 (c (n "bevy_plot") (v "0.1.1") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0qzmc1mx4inwms6b46xak01r1im5mz1935r7w2ckmamc3jr43dw3") (y #t)))

(define-public crate-bevy_plot-0.1.2 (c (n "bevy_plot") (v "0.1.2") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1bazq13i17jygkq8n2fa075dnc8jpzvnb2j7z6823v55k2aa0f15") (y #t)))

(define-public crate-bevy_plot-0.1.3 (c (n "bevy_plot") (v "0.1.3") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("render" "x11"))) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0n6r4ac9lfgd4pzkj1718hfnmhzqrky4ikmzqnp2ppr7agg3ldqa") (f (quote (("unstable") ("default"))))))

(define-public crate-bevy_plot-0.1.4 (c (n "bevy_plot") (v "0.1.4") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("render" "x11"))) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "00cj2hww6hgllxz4chykrl3xk7d5vp63mx4xwbk7n3mxa688m2hr") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-bevy_plot-0.1.5 (c (n "bevy_plot") (v "0.1.5") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("render" "x11"))) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1grrymhy5g3brxsnqzy7vgmbp39ikm6hkdqzgvhwv0v88l4sjfjm") (f (quote (("unstable") ("default"))))))

