(define-module (crates-io be vy bevy_event_set) #:use-module (crates-io))

(define-public crate-bevy_event_set-0.1.0 (c (n "bevy_event_set") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (k 0)))) (h "1vir6aivcn6myq75hf79q5rl05d4ax86fldixdgs6l6kmagl0drn")))

(define-public crate-bevy_event_set-0.1.1 (c (n "bevy_event_set") (v "0.1.1") (d (list (d (n "bevy") (r "^0.4") (k 0)))) (h "10dwwqcfx7vcy6q0lam2i4sz2jfa4vzqf5d3s8rchjidzkvy04w8")))

(define-public crate-bevy_event_set-0.2.0 (c (n "bevy_event_set") (v "0.2.0") (d (list (d (n "bevy") (r "^0.4") (k 0)))) (h "17ygfk15f3g1gpy6c1sjfqmnxgih2jhldlrqg8122gpvwjc15v1f")))

