(define-module (crates-io be vy bevy_kot_derive) #:use-module (crates-io))

(define-public crate-bevy_kot_derive-0.0.9 (c (n "bevy_kot_derive") (v "0.0.9") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1fsjrq72g9kydy95l6kp8bjaqxypadkcgklbj2jnydyqfipqfc8r")))

(define-public crate-bevy_kot_derive-0.0.10 (c (n "bevy_kot_derive") (v "0.0.10") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0h1mcybhh1dg0cvbpqjhlyqypv3dwb2lbhd8b9s9ip2mksbc5jmv")))

(define-public crate-bevy_kot_derive-0.0.11 (c (n "bevy_kot_derive") (v "0.0.11") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1rph5hzxffkip3w4hvp1h451abscxjv0x76b99avm7whv90f7pgf")))

(define-public crate-bevy_kot_derive-0.0.12 (c (n "bevy_kot_derive") (v "0.0.12") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1lss02dc7g62q8072v0m3q2hv75ghj29zc2gb538xhh0bldp5myd")))

(define-public crate-bevy_kot_derive-0.1.0 (c (n "bevy_kot_derive") (v "0.1.0") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1jjh7as9if9nnjh2ih6sj6bw71fpjshhn45w0zbi5bca5iwd182l")))

(define-public crate-bevy_kot_derive-0.2.0 (c (n "bevy_kot_derive") (v "0.2.0") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1g4zxv76lszf49ryv6dsz69x9vk7j3y1irpwhsmbm66rq2vm976i")))

(define-public crate-bevy_kot_derive-0.3.1 (c (n "bevy_kot_derive") (v "0.3.1") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12hzhbrn8z4klc1pylvl48c24g77358dsn3n64clj72xcsh4v0ly")))

(define-public crate-bevy_kot_derive-0.4.0 (c (n "bevy_kot_derive") (v "0.4.0") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ilnh8q2rgfcxc021iy418aji95v8ffx0y8a7y2cza0amwdr4aj5")))

(define-public crate-bevy_kot_derive-0.5.0 (c (n "bevy_kot_derive") (v "0.5.0") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0pqh5xjahdb3wfxwxr546767kbdv5g4l3kqllvfdqvaffk4r6vng")))

(define-public crate-bevy_kot_derive-0.6.0 (c (n "bevy_kot_derive") (v "0.6.0") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1n3smq6p3nwyb35sdzwddai0sqm89301ksi1lm4cnbpb3k4cqzan")))

(define-public crate-bevy_kot_derive-0.7.0 (c (n "bevy_kot_derive") (v "0.7.0") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1brzcdwci6cvx8ahwx9k3492dw5h3mvx319b83jh8sy9sbqx6b2a")))

(define-public crate-bevy_kot_derive-0.8.0 (c (n "bevy_kot_derive") (v "0.8.0") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1bca3lf67g8z7w80bikfk5h9c3yixlfkw0fm3q6axkkxqiwdassw")))

(define-public crate-bevy_kot_derive-0.9.0 (c (n "bevy_kot_derive") (v "0.9.0") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1n27f3jkgdig0fdwdjmfpj9k85wqxv9bxa7s6ibrj7vbn6dgflsj")))

(define-public crate-bevy_kot_derive-0.9.1 (c (n "bevy_kot_derive") (v "0.9.1") (d (list (d (n "bevy_macro_utils") (r "^0.11") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1c9xxf2p9cw5aaa4b72iiqcz0vlxql5lih77cgyhj5f89m6br57w")))

(define-public crate-bevy_kot_derive-0.9.2 (c (n "bevy_kot_derive") (v "0.9.2") (d (list (d (n "bevy_macro_utils") (r "^0.12") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0wd68sr6pkmc3c585bhm5sdvw53aixr3pjcyca72k4cl8iil5wgn")))

(define-public crate-bevy_kot_derive-0.10.0 (c (n "bevy_kot_derive") (v "0.10.0") (d (list (d (n "bevy_macro_utils") (r "^0.12") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1w41xs1x77zd2k5ri8vd57snj35r4ld49g93qb29z6phyikjhcvx")))

(define-public crate-bevy_kot_derive-0.10.1 (c (n "bevy_kot_derive") (v "0.10.1") (d (list (d (n "bevy_macro_utils") (r "^0.12") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1zbwdinl8c2qzl8cr9z26br3kiawxr7x3r9g423jb5980l3056al")))

(define-public crate-bevy_kot_derive-0.10.2 (c (n "bevy_kot_derive") (v "0.10.2") (d (list (d (n "bevy_macro_utils") (r "^0.12") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0yc0xczdw12ryfbyha3780pv5v94gskab68dqdby120g3a4x5zya")))

(define-public crate-bevy_kot_derive-0.10.3 (c (n "bevy_kot_derive") (v "0.10.3") (d (list (d (n "bevy_macro_utils") (r "^0.12") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "183wwj4xykyg4y8ns36cpr2n95snx899xb71na233lb9ahgmhadf")))

(define-public crate-bevy_kot_derive-0.11.0 (c (n "bevy_kot_derive") (v "0.11.0") (d (list (d (n "bevy_macro_utils") (r "^0.12") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05mvd7hf2mkp3gcx9jcsnrl9vb8ns8jxdwg0v34zgyhv33n3i2wc")))

