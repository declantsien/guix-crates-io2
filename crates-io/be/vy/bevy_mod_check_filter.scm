(define-module (crates-io be vy bevy_mod_check_filter) #:use-module (crates-io))

(define-public crate-bevy_mod_check_filter-0.1.0 (c (n "bevy_mod_check_filter") (v "0.1.0") (d (list (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ptr") (r "^0.8") (d #t) (k 0)))) (h "16ia5x3knpsxa4dh213g2nni4za3l2wxdnjflmv7sqc0hqc8gx3d")))

(define-public crate-bevy_mod_check_filter-0.1.1 (c (n "bevy_mod_check_filter") (v "0.1.1") (d (list (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ptr") (r "^0.8") (d #t) (k 0)))) (h "1qxk7xz12hwwj059ljx1bls8cvrjjl1pvq5vv3vx19hncqj6rn2g")))

(define-public crate-bevy_mod_check_filter-0.1.2 (c (n "bevy_mod_check_filter") (v "0.1.2") (d (list (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ptr") (r "^0.8") (d #t) (k 0)))) (h "0sf1znnmsvylzl9pklppx6wnlpa0pxyn4k2xi1kxcdpzv8s618ya")))

(define-public crate-bevy_mod_check_filter-0.2.0 (c (n "bevy_mod_check_filter") (v "0.2.0") (d (list (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_mod_check_filter_macros") (r "^0.2") (d #t) (k 0)) (d (n "bevy_ptr") (r "^0.8") (d #t) (k 0)))) (h "1jwiw4lzvi5nl58a7x6xvvrgs8vrcqlvshc1cih0lyy5cw6z0p8m")))

(define-public crate-bevy_mod_check_filter-0.2.1 (c (n "bevy_mod_check_filter") (v "0.2.1") (d (list (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_mod_check_filter_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "bevy_ptr") (r "^0.8") (d #t) (k 0)))) (h "1lhj03pik98f8djl6rkaldx60r07shx1pkpymkix6bkrwpd224bd")))

(define-public crate-bevy_mod_check_filter-0.3.0 (c (n "bevy_mod_check_filter") (v "0.3.0") (d (list (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_mod_check_filter_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "bevy_ptr") (r "^0.8") (d #t) (k 0)))) (h "1hq838ysa0y8fnihx254wly5fwkpqfgidjkamqd8r3v0imr3gwdy")))

