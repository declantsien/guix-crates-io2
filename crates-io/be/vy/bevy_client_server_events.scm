(define-module (crates-io be vy bevy_client_server_events) #:use-module (crates-io))

(define-public crate-bevy_client_server_events-0.1.0 (c (n "bevy_client_server_events") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.9") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06sw1rkj8zgdvdways7js4qf0xrc93spglgdsn02xf5y0f7rc788")))

(define-public crate-bevy_client_server_events-0.2.0 (c (n "bevy_client_server_events") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.9") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "12qjnk0j28kdlzilfy0bpw9fr69w0j672bihjpm37pdczini1gv8")))

(define-public crate-bevy_client_server_events-0.3.0 (c (n "bevy_client_server_events") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.9") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1hrcn2n8p8jndbcqvjfky11crcl8p21khvsaas0jwk4vnp61qal5")))

(define-public crate-bevy_client_server_events-0.4.0 (c (n "bevy_client_server_events") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.9") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10ax5m3cwiz9qi0rdyspq8h54ck9p5shswxl8464kdqcgmz4wh0m")))

(define-public crate-bevy_client_server_events-0.4.1 (c (n "bevy_client_server_events") (v "0.4.1") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.9") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)))) (h "1jlkcm3x2w9l4j10vc28yaga0hy2k6i8l6sb0h5q539qgmb2dzy0")))

(define-public crate-bevy_client_server_events-0.5.0 (c (n "bevy_client_server_events") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.9") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)))) (h "1q38vd4yns00h545ij5ilc5bscvccwkbz28i1qdnxn0hh3x8gnw6")))

(define-public crate-bevy_client_server_events-0.5.1 (c (n "bevy_client_server_events") (v "0.5.1") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.9") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)))) (h "1n3xs9r9rsy3jrhz85lwz9gpqgv98y5mgzfidvypna00kli6vhg4")))

(define-public crate-bevy_client_server_events-0.5.2 (c (n "bevy_client_server_events") (v "0.5.2") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.9") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)))) (h "0cvaiwg7mdq9m43kn8awc4mpdw7i6v413jaq3cs2cv7wi8ays353")))

(define-public crate-bevy_client_server_events-0.5.3 (c (n "bevy_client_server_events") (v "0.5.3") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.9") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "renet") (r "^0.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1w28a3kr5a4cilfasrb7pi6qmja7cwkzfzddpmkzqx5jw8f0gvma")))

(define-public crate-bevy_client_server_events-0.6.0 (c (n "bevy_client_server_events") (v "0.6.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_renet") (r "^0.0.10") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "renet") (r "^0.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1f054qlbq9mykcm7ajaxg4xrjw0pqzslzzi32bf8m105lra98x7x")))

