(define-module (crates-io be vy bevy_picking_rapier) #:use-module (crates-io))

(define-public crate-bevy_picking_rapier-0.1.0 (c (n "bevy_picking_rapier") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_picking_core") (r "^0.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.21") (d #t) (k 0)))) (h "0sdm09cgd2195kbzj5dpw30fciyysyhkrx864yjsk7v19blvwif2")))

(define-public crate-bevy_picking_rapier-0.2.0 (c (n "bevy_picking_rapier") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_picking_core") (r "^0.2") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.21") (d #t) (k 0)))) (h "0ahcqnw6adx2mq77mw862c9xlg7wny7rssghyxckcyrfg2hqhdqw")))

(define-public crate-bevy_picking_rapier-0.15.0 (c (n "bevy_picking_rapier") (v "0.15.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy_picking_core") (r "^0.15") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.22") (d #t) (k 0)))) (h "1xb68djvhwycrgk5glqfmmxn6r4v7s973n288fd2sgzv3zhxkln9")))

(define-public crate-bevy_picking_rapier-0.16.0 (c (n "bevy_picking_rapier") (v "0.16.0") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "bevy_math") (r "^0.11") (k 0)) (d (n "bevy_picking_core") (r "^0.16") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.22") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.11") (k 0)) (d (n "bevy_render") (r "^0.11") (k 0)) (d (n "bevy_transform") (r "^0.11") (k 0)) (d (n "bevy_utils") (r "^0.11") (k 0)) (d (n "bevy_window") (r "^0.11") (k 0)))) (h "0skf65kkw1qvv2gig21fia5b8fywglfda4a4x6cf8kpiviacy542")))

(define-public crate-bevy_picking_rapier-0.17.0 (c (n "bevy_picking_rapier") (v "0.17.0") (d (list (d (n "bevy_app") (r "^0.12") (k 0)) (d (n "bevy_ecs") (r "^0.12") (k 0)) (d (n "bevy_math") (r "^0.12") (k 0)) (d (n "bevy_picking_core") (r "^0.17") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.23") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12") (k 0)) (d (n "bevy_render") (r "^0.12") (k 0)) (d (n "bevy_transform") (r "^0.12") (k 0)) (d (n "bevy_utils") (r "^0.12") (k 0)) (d (n "bevy_window") (r "^0.12") (k 0)))) (h "0sq1gcmfsvhxpv2m3rwb3ylz7i6vmq6ilyw63d9017jvsb9m4xfw")))

(define-public crate-bevy_picking_rapier-0.18.0 (c (n "bevy_picking_rapier") (v "0.18.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_picking_core") (r "^0.18") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.25") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.13") (k 0)) (d (n "bevy_render") (r "^0.13") (k 0)) (d (n "bevy_transform") (r "^0.13") (k 0)) (d (n "bevy_window") (r "^0.13") (k 0)))) (h "1dgjgyvvn3x7jgjqbr2fchqb5hlga6hcm7rw25mhl241wnln8f34")))

