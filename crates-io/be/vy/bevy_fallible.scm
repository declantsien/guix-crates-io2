(define-module (crates-io be vy bevy_fallible) #:use-module (crates-io))

(define-public crate-bevy_fallible-0.4.0 (c (n "bevy_fallible") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 2)) (d (n "bevy_app") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_fallible_derive") (r "^0.4.0") (d #t) (k 0)))) (h "18ivicgw9xhp226jlqk6v0zn83sipddfjd96l73m0zns5z6cryi0")))

