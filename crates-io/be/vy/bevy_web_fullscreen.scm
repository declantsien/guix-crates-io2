(define-module (crates-io be vy bevy_web_fullscreen) #:use-module (crates-io))

(define-public crate-bevy_web_fullscreen-0.2.1 (c (n "bevy_web_fullscreen") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Element" "Document" "Window"))) (d #t) (k 0)))) (h "05msdrwy2ga5p88cydqmpb2xpx2dbqfhrz0ids10nv3ldxn4wzj7")))

