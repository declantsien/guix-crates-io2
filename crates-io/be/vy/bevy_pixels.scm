(define-module (crates-io be vy bevy_pixels) #:use-module (crates-io))

(define-public crate-bevy_pixels-0.1.0 (c (n "bevy_pixels") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0psxj714g55bsnbjxd1lk0pzrxxy464bpji6072a55rasyg9gm96")))

(define-public crate-bevy_pixels-0.1.1 (c (n "bevy_pixels") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1zn0pcv4kkc2nsakyryg18kx4dj3bq8s0qbg55lcl18isqd0x0qk")))

(define-public crate-bevy_pixels-0.2.0 (c (n "bevy_pixels") (v "0.2.0") (d (list (d (n "bevy_app") (r "^0.5") (d #t) (k 0)) (d (n "bevy_diagnostic") (r "^0.5") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.5") (d #t) (k 0)) (d (n "bevy_internal") (r "^0.5") (f (quote ("bevy_winit"))) (d #t) (k 2)) (d (n "bevy_window") (r "^0.5") (d #t) (k 0)) (d (n "bevy_winit") (r "^0.5") (d #t) (k 0)) (d (n "pixels") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1yfniplkcdv22r3hp89kkd1jinkgr1f1fwbzcarb3km4y429fvmi")))

(define-public crate-bevy_pixels-0.3.0 (c (n "bevy_pixels") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1pzvmyana1316l0wii4npznjm47mizfz3kdzmpj5lxn7h78xggwv")))

(define-public crate-bevy_pixels-0.4.0 (c (n "bevy_pixels") (v "0.4.0") (d (list (d (n "bevy") (r "^0.6.1") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "03j6bbdyfzvfd21fg2qxxr94vkf9rmavc87a5l5vjqfiakh2fhwb") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("default" "x11"))))))

(define-public crate-bevy_pixels-0.5.0 (c (n "bevy_pixels") (v "0.5.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "103mzhb9zd5h6gd8cby3mr94gixrivknzkwq442qqa79qyp5dnab") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("default" "x11"))))))

(define-public crate-bevy_pixels-0.6.0 (c (n "bevy_pixels") (v "0.6.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.10") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "wgpu") (r "^0.13") (f (quote ("webgl"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "06vac0h3ywkqh37lrpnk99j79lyhz1rk853484ijaic6dxa7vkyl") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("default" "x11"))))))

(define-public crate-bevy_pixels-0.7.0 (c (n "bevy_pixels") (v "0.7.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.10") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "wgpu") (r "^0.13") (f (quote ("webgl"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "04z4akx5cr19hxxa8cq2vjarxwgcnyxlayvcrhq9dzc5633c4sva") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("default" "x11"))))))

(define-public crate-bevy_pixels-0.8.0 (c (n "bevy_pixels") (v "0.8.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.11") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1sgh0hn0g3zirs7f2a287b2rcnia7wmms11h0d3bw37whp77h5jz") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("default" "x11"))))))

(define-public crate-bevy_pixels-0.9.0 (c (n "bevy_pixels") (v "0.9.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.12") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "12wzr77j4bzpk2iccwdf7ylp0kdkg9k2ljf76694pfnnjxxfbkgz") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("default" "x11"))))))

(define-public crate-bevy_pixels-0.10.0 (c (n "bevy_pixels") (v "0.10.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.12") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1byyyh78kv4hm32y311blv4lgqgsf6j26ykfyw7nm9i6dscaz2h0") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("render") ("default" "render" "x11"))))))

(define-public crate-bevy_pixels-0.11.0 (c (n "bevy_pixels") (v "0.11.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.13") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "139yjj3si8wcgg2d8nqwz0qjrrylhkp5n0qkvvgdwl771wlk13c6") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("render") ("default" "render" "x11"))))))

(define-public crate-bevy_pixels-0.12.0 (c (n "bevy_pixels") (v "0.12.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.13") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1ag6j0knh4n0n41c5bijhmc87wzim7ibx9n09x2g0x0xqfa6gl01") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("render") ("default" "render" "x11"))))))

(define-public crate-bevy_pixels-0.13.0 (c (n "bevy_pixels") (v "0.13.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_winit"))) (k 0)) (d (n "pixels") (r "^0.13") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.29") (f (quote ("rwh_05"))) (d #t) (k 0)))) (h "047jdjj6bn17dh6qyz09xh78q242ndzxicrhry648zjn8kh59m72") (f (quote (("x11" "bevy/x11") ("wayland" "bevy/wayland") ("render") ("default" "render" "x11"))))))

