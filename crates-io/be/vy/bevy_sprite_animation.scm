(define-module (crates-io be vy bevy_sprite_animation) #:use-module (crates-io))

(define-public crate-bevy_sprite_animation-0.4.0 (c (n "bevy_sprite_animation") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "opener") (r ">=0.6") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (d #t) (k 0)))) (h "0p3va6zkr0h3b8pp2qwhfwlp3mx6mp6yc76rvg9nzhq6qpr0rh06") (f (quote (("serialize" "ron" "serde") ("dot" "opener") ("default" "serialize" "dot"))))))

