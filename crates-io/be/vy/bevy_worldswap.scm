(define-module (crates-io be vy bevy_worldswap) #:use-module (crates-io))

(define-public crate-bevy_worldswap-0.0.1 (c (n "bevy_worldswap") (v "0.0.1") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.27") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "04i534lvfj0w500dzn5kj3asfi7vq30d8hwj1kh8rrjhavynmvv4")))

