(define-module (crates-io be vy bevy_ui_styled) #:use-module (crates-io))

(define-public crate-bevy_ui_styled-0.1.0 (c (n "bevy_ui_styled") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_ui" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "12n74g9bmc0vcpz81hbqc54kva50caai3yyrrpjbzr34lahfxbfc")))

(define-public crate-bevy_ui_styled-0.2.0 (c (n "bevy_ui_styled") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_ui" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_ui_styled_macros") (r "^0.2.0") (d #t) (k 0)))) (h "02ryi08clw5yxmmmgv2cq707mhbn8mripgk1mc4jyxs77r5v1jhx")))

(define-public crate-bevy_ui_styled-0.3.0 (c (n "bevy_ui_styled") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_ui_styled_macros") (r "^0.3.0") (d #t) (k 0)))) (h "1iyqjhardl601ksw3w5s090gsns5f7pb24vdwbxd6fbcmxp7mh6s")))

(define-public crate-bevy_ui_styled-0.4.0 (c (n "bevy_ui_styled") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_ui_styled_macros") (r "^0.4.0") (d #t) (k 0)))) (h "03fk3rc9cxncxwmzfapnaxgspq2kj5ld057mpbb0gnnx6khps1kr")))

