(define-module (crates-io be vy bevy_slippy_tiles) #:use-module (crates-io))

(define-public crate-bevy_slippy_tiles-0.1.0 (c (n "bevy_slippy_tiles") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1i9dxklmfli06asy1cr6bb9d0cc7rpl2laz5d8cw06xl05fgy1zi")))

(define-public crate-bevy_slippy_tiles-0.1.1 (c (n "bevy_slippy_tiles") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1jl0q3bc2maljgz1hs5zkqwwirhbm2p38fa4qqy236bmczm62728")))

(define-public crate-bevy_slippy_tiles-0.1.2 (c (n "bevy_slippy_tiles") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1rf9vqf7idlhi1q5gsbq283sa114b3vjagmq9z3z63q31mmvjw2y")))

(define-public crate-bevy_slippy_tiles-0.1.3 (c (n "bevy_slippy_tiles") (v "0.1.3") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0xa0wl187aqg7bpllr975ch1ixna786hx3kv27l482rhy0pdgjjv")))

(define-public crate-bevy_slippy_tiles-0.2.0 (c (n "bevy_slippy_tiles") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "14ll7m07rskgsirkybf6v7lhzqjw4kkshsfjacj36gxljr7g92qv")))

(define-public crate-bevy_slippy_tiles-0.2.1 (c (n "bevy_slippy_tiles") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0wwx9q4lnkysfnwy0vxzs8k1hh1bslrk6203firg0lhmvgqjskv4")))

(define-public crate-bevy_slippy_tiles-0.3.0 (c (n "bevy_slippy_tiles") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1kvfvpr6y5hb2zg4nyi7ynph9ip46h87fhx491lq15a7kcma1v29")))

(define-public crate-bevy_slippy_tiles-0.4.0 (c (n "bevy_slippy_tiles") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "081zqih18w7gf12faws70nqpwhk388clh84kqwnla9c8qszz3mlk")))

(define-public crate-bevy_slippy_tiles-0.3.1 (c (n "bevy_slippy_tiles") (v "0.3.1") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "05d7869w84vxgdh974v89h7xrvpx305cna2xrqjiick0q71p9dfs")))

(define-public crate-bevy_slippy_tiles-0.3.2 (c (n "bevy_slippy_tiles") (v "0.3.2") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1405hni8fvyl9i7xkmzld6ic2541243qyxp9rs3774d8iz500vmf")))

(define-public crate-bevy_slippy_tiles-0.3.3 (c (n "bevy_slippy_tiles") (v "0.3.3") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "googleprojection") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0rsl86fgr1y09wszw9ka8c4598cc4c9jd52d7y4fq7yv49wfm2zj")))

(define-public crate-bevy_slippy_tiles-0.4.1 (c (n "bevy_slippy_tiles") (v "0.4.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "googleprojection") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1pld8qwvapikrf5kk9w74kmwxixngbx017lxis9h8mfsww486q05")))

(define-public crate-bevy_slippy_tiles-0.4.2 (c (n "bevy_slippy_tiles") (v "0.4.2") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "googleprojection") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1wf555s69jcx66g0mpwp4f25mdj9gqvg3xa7mx9mphj0hkdywk0s")))

(define-public crate-bevy_slippy_tiles-0.3.4 (c (n "bevy_slippy_tiles") (v "0.3.4") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "googleprojection") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "028iri49rxhwcnd3k3cs0s9wfj3bk4gliqn7hvbymkkxszzksng9")))

(define-public crate-bevy_slippy_tiles-0.3.5 (c (n "bevy_slippy_tiles") (v "0.3.5") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "googleprojection") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0r0sy3n28qqgzxf9yi8q7k1gvw65kl4346cnli0ajx3rlg4abx7f")))

(define-public crate-bevy_slippy_tiles-0.4.3 (c (n "bevy_slippy_tiles") (v "0.4.3") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13") (d #t) (k 0)) (d (n "googleprojection") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0sfbfic6d58h9q6ikm33azvqwxc8l3nb2il5cvi0y9hj01zhhj0g")))

(define-public crate-bevy_slippy_tiles-0.5.0 (c (n "bevy_slippy_tiles") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "googleprojection") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0i6pfr7jg7zzhcy838w8fca6pg6j7lkkp5fgv6k59xwvdr2zmr1b")))

(define-public crate-bevy_slippy_tiles-0.5.1 (c (n "bevy_slippy_tiles") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "ehttp") (r "^0.5") (f (quote ("native-async"))) (d #t) (k 0)) (d (n "googleprojection") (r "^1.2") (d #t) (k 0)))) (h "041bdfdwnsgmdxszcxfyvqzm7rvqh9yh930cqn4r86hqx6hfw40l")))

