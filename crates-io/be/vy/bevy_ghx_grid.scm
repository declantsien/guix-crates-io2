(define-module (crates-io be vy bevy_ghx_grid) #:use-module (crates-io))

(define-public crate-bevy_ghx_grid-0.1.0 (c (n "bevy_ghx_grid") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (o #t) (k 0)) (d (n "ghx_grid") (r "^0.1.0") (f (quote ("bevy"))) (d #t) (k 0)))) (h "1pwnj61pymv2kx3r897nvwyvvhkr6mlwqb3kxivddsvafr2zjikz") (f (quote (("reflect" "ghx_grid/reflect") ("default" "reflect" "debug-plugin") ("debug-plugin" "bevy/bevy_gizmos" "bevy/bevy_render"))))))

(define-public crate-bevy_ghx_grid-0.2.0 (c (n "bevy_ghx_grid") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13.0") (o #t) (k 0)) (d (n "ghx_grid") (r "^0.2.0") (f (quote ("bevy"))) (d #t) (k 0)))) (h "1sjqgv1003vw2q8mryhzqnq6vh2gzbpv7a7j5q02kmvy197xrmjl") (f (quote (("reflect" "ghx_grid/reflect") ("default" "reflect" "debug-plugin") ("debug-plugin" "bevy/bevy_gizmos" "bevy/bevy_render"))))))

(define-public crate-bevy_ghx_grid-0.2.1 (c (n "bevy_ghx_grid") (v "0.2.1") (d (list (d (n "bevy") (r "^0.13.0") (o #t) (k 0)) (d (n "ghx_grid") (r "^0.2.0") (f (quote ("bevy"))) (d #t) (k 0)))) (h "0n3afyqv3rfmww0qkckyqdsplarpjimcivkrjrxkl3r5d0zhp29v") (f (quote (("reflect" "ghx_grid/reflect") ("default" "reflect" "debug-plugin") ("debug-plugin" "bevy/bevy_gizmos" "bevy/bevy_render"))))))

