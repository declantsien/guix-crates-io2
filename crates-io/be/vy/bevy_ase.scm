(define-module (crates-io be vy bevy_ase) #:use-module (crates-io))

(define-public crate-bevy_ase-0.6.0 (c (n "bevy_ase") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "asefile") (r "^0.3.6") (d #t) (k 0)) (d (n "benimator") (r "^4.1") (o #t) (d #t) (k 0)) (d (n "bevy") (r "^0.11.2") (d #t) (k 0)))) (h "1zlw0qkaqzhg47scli8ycrry2ks29yzlnn46ryzm05zdcpphg2bk")))

