(define-module (crates-io be vy bevy-pixel-map) #:use-module (crates-io))

(define-public crate-bevy-pixel-map-0.1.0 (c (n "bevy-pixel-map") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)))) (h "1mkmbb59j62gh2996ah0qrc3hvac6q29z3vdqc2lfi3n5yh6zxw6")))

(define-public crate-bevy-pixel-map-0.2.0 (c (n "bevy-pixel-map") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)))) (h "1qgd25hkhhvi5scgj1qiq187h0ybhdrwa8mjvfl1jnhwg04qgwpq")))

(define-public crate-bevy-pixel-map-0.2.1 (c (n "bevy-pixel-map") (v "0.2.1") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)))) (h "03h2bvpmvvmf3c3ca6ks4cgqp5r1yqp9zyr77nyj47a17jkbxkv4")))

(define-public crate-bevy-pixel-map-0.2.2 (c (n "bevy-pixel-map") (v "0.2.2") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "113lpggal5c4n867askdw11zfjqbrd1l0yg2nd119fn811h0jk6k")))

