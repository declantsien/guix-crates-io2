(define-module (crates-io be vy bevy_arcade_car_controller) #:use-module (crates-io))

(define-public crate-bevy_arcade_car_controller-0.1.0 (c (n "bevy_arcade_car_controller") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "heron") (r "^3.1") (f (quote ("3d"))) (d #t) (k 0)))) (h "001h73422yjd9r6fxh9ksz9r1cxnxkdknip9b39vrz2rbrysp8dq")))

