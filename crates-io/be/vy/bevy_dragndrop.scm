(define-module (crates-io be vy bevy_dragndrop) #:use-module (crates-io))

(define-public crate-bevy_dragndrop-0.1.0 (c (n "bevy_dragndrop") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_asset" "bevy_render" "bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1fzzphy554axzijh4330f4xki41axd059lc615cxnyh835spj2d6")))

(define-public crate-bevy_dragndrop-0.1.1 (c (n "bevy_dragndrop") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_asset" "bevy_render" "bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "11h038g1ck6knlxmdj5s6bd7d4ar7q4km2j6540i8vd5swcc5drx")))

(define-public crate-bevy_dragndrop-0.2.0 (c (n "bevy_dragndrop") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_asset" "bevy_render" "bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("dynamic_linking"))) (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1c5xbagw04pb5iwraxsvlmlv81826gz9jm7jny5izizrp3r3s1iq")))

