(define-module (crates-io be vy bevy-aabb-instancing) #:use-module (crates-io))

(define-public crate-bevy-aabb-instancing-0.1.0 (c (n "bevy-aabb-instancing") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render" "x11"))) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "0k4j01l2ibsi1mq4aw5sjm4n9lz0p1y5mxrxm3a3qgifppql4k22")))

(define-public crate-bevy-aabb-instancing-0.1.1 (c (n "bevy-aabb-instancing") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render" "x11"))) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "1agz7pk39hddgndabhq7a220a5acgif0137mgix78fx5ljm1sj5y")))

(define-public crate-bevy-aabb-instancing-0.1.2 (c (n "bevy-aabb-instancing") (v "0.1.2") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render" "x11"))) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "0xmir2ml4w1k2nzxkk8f15igghlnf8024badj9izp5gbk395cg13")))

(define-public crate-bevy-aabb-instancing-0.1.3 (c (n "bevy-aabb-instancing") (v "0.1.3") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render" "x11"))) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "17i037sj2bxzdbslla451h0an9m1dphaabkbw9cap2yabpi4blc6")))

(define-public crate-bevy-aabb-instancing-0.2.0 (c (n "bevy-aabb-instancing") (v "0.2.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render" "x11"))) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "0zgnzfpfgvq0sdi003067rzmlncirz0asnqx7nbhr82z99dhypr5")))

(define-public crate-bevy-aabb-instancing-0.2.1 (c (n "bevy-aabb-instancing") (v "0.2.1") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render" "x11"))) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)))) (h "1pjcvfjs40fvkal74mym3dcfvdll6aijgj4wgzqk0swc9pl74mbq")))

(define-public crate-bevy-aabb-instancing-0.3.0 (c (n "bevy-aabb-instancing") (v "0.3.0") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pvdh7avi7znc3wl7f96nsjjsmwsc2hl0cl9gmlsqp1pp6gs924v") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.4.0 (c (n "bevy-aabb-instancing") (v "0.4.0") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "14d9br3p4hw388knn0744gi4281fc2aalnarnbm1nsa12cky4lkh") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.4.1 (c (n "bevy-aabb-instancing") (v "0.4.1") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1xxdi4cbvd5x693a8h25fq7389ivxsamrbl51x7p7ssdfbkh8gs4") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.4.2 (c (n "bevy-aabb-instancing") (v "0.4.2") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0jm7mpxr1n20f1634mvm6fk1qvmxyahwlxpm9nfwzv8xayjss9gm") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.5.0 (c (n "bevy-aabb-instancing") (v "0.5.0") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1rhhn8a2mh5n5p43gkqisw3sh74nyj0sxf4cnq3ikli8g75ngyrw") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.5.1 (c (n "bevy-aabb-instancing") (v "0.5.1") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "07k55b94462scbprqirbyfjpvmsy8dmaz4ipyd01z1vs16wdgnai") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.5.2 (c (n "bevy-aabb-instancing") (v "0.5.2") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qrczj69n185gzivc80l6j5klyr51cnl7qll3f50aqcz51zw1rb6") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.5.3 (c (n "bevy-aabb-instancing") (v "0.5.3") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0d5b8q0kfim9ziis279v6s2cw00qflbib4g40yj8rxnqmgjj9ksw") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.6.0 (c (n "bevy-aabb-instancing") (v "0.6.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ldr4gbybncrgwswlvl9pgfn2fflg6lb6yxcpghzckdg7vxv74v9") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.6.1 (c (n "bevy-aabb-instancing") (v "0.6.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "061p2jwz6qm0g4qjh58z9a9pfd1kw7karmr9p2yi7m0d4j8yls8w") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.6.2 (c (n "bevy-aabb-instancing") (v "0.6.2") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1fg2ldkgxr5n2sjwlsj7q25l4lvf9b8ggimfa4f0w31s7qvzzig5") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.7.0 (c (n "bevy-aabb-instancing") (v "0.7.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "01xgfwr7hl9qnm3vxi37hhg3kjydidkrzqvv94r5zdrn3684b00c") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.8.0 (c (n "bevy-aabb-instancing") (v "0.8.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1y61vxlhbbp6lagqfpqra708526v58bwr2lz33gz6lyn1ywz4i2c") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.9.0 (c (n "bevy-aabb-instancing") (v "0.9.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.8") (d #t) (k 2)))) (h "0ir352zwscp1qgb9m2i1knng5scis8xzg15901cbm7fhk24c8psa") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.10.0 (c (n "bevy-aabb-instancing") (v "0.10.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.9") (d #t) (k 2)))) (h "0b2b1p42hs57425khvwwx13iw3dg3xw1jpfj20icbrnvzywyhgmn") (f (quote (("trace" "bevy/trace_chrome"))))))

(define-public crate-bevy-aabb-instancing-0.11.0 (c (n "bevy-aabb-instancing") (v "0.11.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "x11"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.10") (d #t) (k 2)))) (h "0a4kndm365knwd7ypzzcci7fm3m2ww7jxgrcwbf0bl4y83z1hx4d") (f (quote (("trace" "bevy/trace_chrome"))))))

