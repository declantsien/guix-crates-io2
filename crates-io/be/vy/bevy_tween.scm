(define-module (crates-io be vy bevy_tween) #:use-module (crates-io))

(define-public crate-bevy_tween-0.1.0 (c (n "bevy_tween") (v "0.1.0") (h "161a87zcd6x8r8kjmxdn4x09li9yg50q5br9cqhmrprjrsvgf7k6") (y #t)))

(define-public crate-bevy_tween-0.1.1 (c (n "bevy_tween") (v "0.1.1") (h "0my5g4h762wnngb69z87b62d66jm7s19g4j0srfwk5ihgrwilcgv") (y #t)))

(define-public crate-bevy_tween-0.1.2 (c (n "bevy_tween") (v "0.1.2") (h "03kf04kd8vkb30f2nsrkrhyaw443y9v7hh4y14d105bbdw2dhv3f") (y #t)))

(define-public crate-bevy_tween-0.1.3 (c (n "bevy_tween") (v "0.1.3") (h "043dz440xp4z9vlgwn5gl8m7l4v97wvhigil9nvwdh4mr2xhgqx0") (y #t)))

(define-public crate-bevy_tween-0.2.0 (c (n "bevy_tween") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_winit" "bevy_asset" "bevy_render" "bevy_sprite" "tonemapping_luts" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kjlhsm9xwd2nxb15aiql7g6kxj6qdfvy1y4ylha9lgvgnf04c87") (f (quote (("span_tween") ("default" "span_tween" "bevy_asset" "bevy_render" "bevy_sprite") ("bevy_sprite" "bevy/bevy_sprite" "bevy_render") ("bevy_render" "bevy/bevy_render") ("bevy_asset" "bevy/bevy_asset"))))))

(define-public crate-bevy_tween-0.3.0 (c (n "bevy_tween") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite" "tonemapping_luts" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vl9fd2sqvfpbk788h5azqrka9i0y0x48p90yi0mf575bn5mcmq0") (f (quote (("span_tween") ("default" "span_tween" "bevy_asset" "bevy_render" "bevy_sprite") ("bevy_sprite" "bevy/bevy_sprite" "bevy_render") ("bevy_render" "bevy/bevy_render") ("bevy_asset" "bevy/bevy_asset"))))))

(define-public crate-bevy_tween-0.3.1 (c (n "bevy_tween") (v "0.3.1") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite" "tonemapping_luts" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1my05k33mdxwxsbd7ykbkxfhvwcxx2c4dc23nzgzplma4fyqx927") (f (quote (("span_tween") ("default" "span_tween" "bevy_asset" "bevy_render" "bevy_sprite") ("bevy_sprite" "bevy/bevy_sprite" "bevy_render") ("bevy_render" "bevy/bevy_render") ("bevy_asset" "bevy/bevy_asset"))))))

(define-public crate-bevy_tween-0.4.0 (c (n "bevy_tween") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_render" "bevy_sprite" "tonemapping_luts" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nlnyr4s7f31f82xkydpwq2j5r1az03r19saav87cj0i0l5kxrq2") (f (quote (("span_tween") ("default" "span_tween" "bevy_asset" "bevy_render" "bevy_sprite") ("bevy_sprite" "bevy/bevy_sprite" "bevy_render") ("bevy_render" "bevy/bevy_render") ("bevy_asset" "bevy/bevy_asset"))))))

