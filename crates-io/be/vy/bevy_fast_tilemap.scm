(define-module (crates-io be vy bevy_fast_tilemap) #:use-module (crates-io))

(define-public crate-bevy_fast_tilemap-0.1.0 (c (n "bevy_fast_tilemap") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 0)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "020sx6kg7fc54gxgdn9s0bhi5ljh82dgrhx0asmbskddwa1pjjbl")))

(define-public crate-bevy_fast_tilemap-0.2.0 (c (n "bevy_fast_tilemap") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 0)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "1cp3270hr430d1fb1qsc0w2rrl4q2zx9c491lylqdgriha5ll49x")))

(define-public crate-bevy_fast_tilemap-0.3.0 (c (n "bevy_fast_tilemap") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "1074frfh156r3cmmh295f9cmm2agqv1cwqfa14rv4mbhv6k9bryz")))

(define-public crate-bevy_fast_tilemap-0.4.0 (c (n "bevy_fast_tilemap") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "1fyx2yc39dmfwnqzbn295j05i4nvwg3k5kbhmbyp6vwhp6a0mf02")))

(define-public crate-bevy_fast_tilemap-0.4.1 (c (n "bevy_fast_tilemap") (v "0.4.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "1057gndvlx7ngp0pm0gr1qklg45rhsn5a00k6pgk4z68700g62kq")))

(define-public crate-bevy_fast_tilemap-0.4.2 (c (n "bevy_fast_tilemap") (v "0.4.2") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0dm6f3ygb4qgl9iqfs8mjfp0lys0d8kfp9gb4ds190na85cpqydb")))

(define-public crate-bevy_fast_tilemap-0.5.0 (c (n "bevy_fast_tilemap") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset" "bevy_winit" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.19.0") (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0axlg8kyw30zd46br4j8v9f8j6ly6kpqqli3mv11mnylbnsixd8k")))

(define-public crate-bevy_fast_tilemap-0.5.1 (c (n "bevy_fast_tilemap") (v "0.5.1") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_asset" "bevy_winit" "png"))) (k 2)) (d (n "bevy-inspector-egui") (r "^0.19.0") (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "042ipyp6m7gqpgwg7540n1jjdbn9kjsn6cdrz9v3h1mzy77p2dka")))

(define-public crate-bevy_fast_tilemap-0.6.0 (c (n "bevy_fast_tilemap") (v "0.6.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.22") (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "10kfa4jf41slwbaqznc2h1bp01734gri11gfylk50arz5vqsp7qz")))

(define-public crate-bevy_fast_tilemap-0.7.0 (c (n "bevy_fast_tilemap") (v "0.7.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r ">=0.22") (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0nyxpdkkh4bzhrx3l0nqzi0jiyv79w6hzm4jifyjcvajdg2llxv7")))

(define-public crate-bevy_fast_tilemap-0.7.1 (c (n "bevy_fast_tilemap") (v "0.7.1") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r ">=0.22") (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "1nw0f3nxl6l2yk4d4p11qiqzhawkycan6fl734hgi16cnk1z27i2")))

(define-public crate-bevy_fast_tilemap-0.7.2 (c (n "bevy_fast_tilemap") (v "0.7.2") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r ">=0.22") (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0sbf4awsq0amvmg6yjibwdbi313sm8qc8ki2v0gyc79x76yqxyjf")))

(define-public crate-bevy_fast_tilemap-0.7.3 (c (n "bevy_fast_tilemap") (v "0.7.3") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r ">=0.22") (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0rr6alyiq71q19030pz1wcbw002xncjpbl07sfwagla9yf7a7m36")))

(define-public crate-bevy_fast_tilemap-0.7.4 (c (n "bevy_fast_tilemap") (v "0.7.4") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r ">=0.22") (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0gqb1n8hmkb4l9ziwhfw8nsacq2q16b4hd56hx2q9wvbl3zdr4hh")))

