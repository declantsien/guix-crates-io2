(define-module (crates-io be vy bevy_cameraman) #:use-module (crates-io))

(define-public crate-bevy_cameraman-0.1.0 (c (n "bevy_cameraman") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "0fqp12fsvfczrpd585kwjy4k5xsfn4wswz337cdip0y7gxbjx3ka")))

(define-public crate-bevy_cameraman-0.1.1 (c (n "bevy_cameraman") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)))) (h "0ih1mpyk2s20hayan63a6lrf9gr3nv43pwj8r1zcbw3l8m3svrix") (r "1.73.0")))

