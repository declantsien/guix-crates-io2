(define-module (crates-io be vy bevy-snapolation) #:use-module (crates-io))

(define-public crate-bevy-snapolation-0.1.0 (c (n "bevy-snapolation") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s0ds5hisl5l2dmj0bvby0w270ribyp25vry2sqd3vihqyib90nn")))

(define-public crate-bevy-snapolation-0.1.1 (c (n "bevy-snapolation") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d18y1ksy9sf44v8jl42zxnad4xmmyrag8mqafg0zd5snvg8yg0f")))

(define-public crate-bevy-snapolation-0.2.0 (c (n "bevy-snapolation") (v "0.2.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zkzfinn5cnz8rzwj7gn49wb2z9vzgy6h5qralgzxzah1hn5n7zn")))

