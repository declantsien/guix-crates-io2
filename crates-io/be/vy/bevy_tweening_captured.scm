(define-module (crates-io be vy bevy_tweening_captured) #:use-module (crates-io))

(define-public crate-bevy_tweening_captured-0.10.0 (c (n "bevy_tweening_captured") (v "0.10.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy-inspector-egui") (r "^0.23") (d #t) (k 2)) (d (n "interpolation") (r "^0.3") (d #t) (k 0)))) (h "1rrs19vdnpp21sp8jlg2fjn7ym9rz65vr1j9ilkb9lvh67prp9h1") (f (quote (("default" "bevy_sprite" "bevy_ui" "bevy_asset" "bevy_text") ("bevy_ui" "bevy/bevy_ui" "bevy/bevy_render") ("bevy_text" "bevy/bevy_text" "bevy/bevy_render" "bevy/bevy_sprite") ("bevy_sprite" "bevy/bevy_sprite" "bevy/bevy_render") ("bevy_asset" "bevy/bevy_asset"))))))

