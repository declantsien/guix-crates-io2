(define-module (crates-io be vy bevy_retro_physics) #:use-module (crates-io))

(define-public crate-bevy_retro_physics-0.1.0 (c (n "bevy_retro_physics") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy_rapier2d") (r "^0.20") (d #t) (k 0)) (d (n "density-mesh-core") (r "^1.5") (d #t) (k 0)) (d (n "density-mesh-image") (r "^1.5") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1apw4hnpha9jgn3n53y9pxd89x5vh39385155wjbrkgwzj47x349") (f (quote (("simd-stable" "bevy_rapier2d/simd-stable") ("simd-nightly" "bevy_rapier2d/simd-nightly") ("default" "simd-stable") ("debug" "bevy_rapier2d/debug-render-2d"))))))

