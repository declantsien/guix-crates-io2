(define-module (crates-io be vy bevy_background_compute) #:use-module (crates-io))

(define-public crate-bevy_background_compute-0.1.0 (c (n "bevy_background_compute") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.4.2") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (k 2)) (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.8") (d #t) (k 0)))) (h "0nxcgkzx8g9ij7rxq7nvxwyq23cj53by8xrv2l75gi330vk4bvcb")))

(define-public crate-bevy_background_compute-0.1.1 (c (n "bevy_background_compute") (v "0.1.1") (d (list (d (n "async-channel") (r "^1.4.2") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (k 2)) (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.8") (d #t) (k 0)))) (h "06a1f92gc8pj6wb0czsavzajc58sd92xr5s1c7sz0l9csrmw7f3b")))

(define-public crate-bevy_background_compute-0.2.0 (c (n "bevy_background_compute") (v "0.2.0") (d (list (d (n "async-channel") (r "^1.7.1") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (k 2)) (d (n "bevy_app") (r "^0.9") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.9") (d #t) (k 0)))) (h "1spdwq9np5vwzajswcaky8y4dxzzfafkzknqcwzb7n5ph61m8h25")))

(define-public crate-bevy_background_compute-0.2.1 (c (n "bevy_background_compute") (v "0.2.1") (d (list (d (n "async-channel") (r "^1.7.1") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (k 2)) (d (n "bevy_app") (r "^0.9") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.9") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.9") (d #t) (k 0)))) (h "1aj4zxpszy8rng23i4kn9rcqr5c4y51mcywziik77sqzwh1wy4ar")))

(define-public crate-bevy_background_compute-0.3.0 (c (n "bevy_background_compute") (v "0.3.0") (d (list (d (n "async-channel") (r "^1.7.1") (d #t) (k 0)) (d (n "bevy") (r "^0.10") (k 2)) (d (n "bevy_app") (r "^0.10") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.10") (d #t) (k 0)))) (h "0r8z4r30cz2h4n9g7qmgpf081k55cihjj3fq0a7i47w31z4w21g4")))

(define-public crate-bevy_background_compute-0.4.0 (c (n "bevy_background_compute") (v "0.4.0") (d (list (d (n "async-channel") (r "^1.7.1") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (k 2)) (d (n "bevy_app") (r "^0.11") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.11") (d #t) (k 0)))) (h "1ga9gfygmxfs65dw8y9vkwgsn91b1ymnq4lkb8ar73w9nxsfwhly")))

(define-public crate-bevy_background_compute-0.4.1 (c (n "bevy_background_compute") (v "0.4.1") (d (list (d (n "async-channel") (r "^1.7.1") (d #t) (k 0)) (d (n "bevy") (r "^0.11") (k 2)) (d (n "bevy_app") (r "^0.11") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.11") (d #t) (k 0)))) (h "148gh0jbdsrj56293p9b97295l4pyayp6gd60apw7wdiq6ald0jl")))

(define-public crate-bevy_background_compute-0.5.0 (c (n "bevy_background_compute") (v "0.5.0") (d (list (d (n "async-channel") (r "^1.7.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (k 2)) (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.12") (f (quote ("multi-threaded"))) (d #t) (k 0)))) (h "0kjkvficzazzr6y2f84859r6zxn2b6l6v6zail1c708617k2k94i")))

