(define-module (crates-io be vy bevy_aegis) #:use-module (crates-io))

(define-public crate-bevy_aegis-0.1.0 (c (n "bevy_aegis") (v "0.1.0") (h "00x72v5daxbqldmqrg5sc4dc5vmpm2a52459l2r2i7dsxs6w4fa9") (y #t)))

(define-public crate-bevy_aegis-0.0.1 (c (n "bevy_aegis") (v "0.0.1") (h "0y0h6bsyl2qa2mfkk5nsihwh1v8hqygcbwpvfwy5hl4rcr0glb0q")))

