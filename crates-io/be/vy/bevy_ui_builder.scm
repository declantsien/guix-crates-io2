(define-module (crates-io be vy bevy_ui_builder) #:use-module (crates-io))

(define-public crate-bevy_ui_builder-0.1.0 (c (n "bevy_ui_builder") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (f (quote ("bevy_ui" "bevy_text" "bevy_asset" "bevy_render"))) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9.1") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.17.0") (d #t) (k 2)))) (h "1v9xyhxifwzz9clwsadnwczbafqggkd2apxfpjni46j95asy5d8c")))

