(define-module (crates-io be vy bevy_mod_scripting_rune) #:use-module (crates-io))

(define-public crate-bevy_mod_scripting_rune-0.4.0 (c (n "bevy_mod_scripting_rune") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.4.0") (d #t) (k 0)) (d (n "rune") (r "^0.13.1") (d #t) (k 0)) (d (n "rune-modules") (r "^0.13.1") (d #t) (k 0)))) (h "0amq8yjnmvp1bpxzp5ljl7s2yrfwcfm60a169pvn38fsqw2rgz81")))

(define-public crate-bevy_mod_scripting_rune-0.5.0 (c (n "bevy_mod_scripting_rune") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.5.0") (d #t) (k 0)) (d (n "rune") (r "^0.13.1") (d #t) (k 0)) (d (n "rune-modules") (r "^0.13.1") (d #t) (k 0)))) (h "0b85qlwhrqh0syysiahnrp0d0gq9zpa5732rjrn9psnabyjapnb7")))

(define-public crate-bevy_mod_scripting_rune-0.6.0 (c (n "bevy_mod_scripting_rune") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bevy") (r "=0.13.1") (k 0)) (d (n "bevy_mod_scripting_core") (r "^0.6.0") (d #t) (k 0)) (d (n "rune") (r "^0.13.1") (d #t) (k 0)) (d (n "rune-modules") (r "^0.13.1") (d #t) (k 0)))) (h "0xa1k7lhgr3sw12khyncvw5rda2jmgk639y15xmrgm34zb4xw6hl")))

