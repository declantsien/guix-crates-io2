(define-module (crates-io be vy bevy_radial_bar) #:use-module (crates-io))

(define-public crate-bevy_radial_bar-0.1.0 (c (n "bevy_radial_bar") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "09swcmja1zsr3ji85ifq0ninfj91my8pkzkj4zac9rs06rz6f4i0") (y #t)))

(define-public crate-bevy_radial_bar-0.1.1 (c (n "bevy_radial_bar") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)))) (h "1c82nrrld9vck1l8mglf8xl3s15qqdyqz03sv8gbx13jvphwk3v8")))

