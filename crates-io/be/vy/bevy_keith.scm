(define-module (crates-io be vy bevy_keith) #:use-module (crates-io))

(define-public crate-bevy_keith-0.0.1 (c (n "bevy_keith") (v "0.0.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "render"))) (k 0)) (d (n "bevy-inspector-egui") (r "^0.12") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0h3qqkx1rl7rvp0wmwrf5nrgf2919hd71r2piynxkabrgpii2cf5")))

(define-public crate-bevy_keith-0.0.2 (c (n "bevy_keith") (v "0.0.2") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "bevy_core_pipeline" "render"))) (k 0)) (d (n "bevy-inspector-egui") (r "^0.12") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0r5d7bmmzrxf31k9lq3jjlc4wcc14azifl5dp536zadkwv348paq")))

