(define-module (crates-io be vy bevy_app_compute) #:use-module (crates-io))

(define-public crate-bevy_app_compute-0.10.0 (c (n "bevy_app_compute") (v "0.10.0") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.15.1") (d #t) (k 0)))) (h "0lacphrl85kqjvwr3d1i8nnp4n0mys8x2mwvg1hazr6558p4szs3") (y #t)))

(define-public crate-bevy_app_compute-0.10.1 (c (n "bevy_app_compute") (v "0.10.1") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.15.1") (d #t) (k 0)))) (h "16n9zpb0nr6lfgcdf2hdrk62lh29mr8pcg1v36l805a5jmjyklnd")))

(define-public crate-bevy_app_compute-0.10.2 (c (n "bevy_app_compute") (v "0.10.2") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.15.1") (d #t) (k 0)))) (h "1jz69g6x4ag5lvynbmq5vsamxq1373jkm2iwbcmva0b0f2qif1fv")))

(define-public crate-bevy_app_compute-0.10.3 (c (n "bevy_app_compute") (v "0.10.3") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "wgpu") (r "^0.15.1") (d #t) (k 0)))) (h "1yignl5nndzr6w9fgfmbl09fzm6f3rf39gm95knaqja2fia507di")))

