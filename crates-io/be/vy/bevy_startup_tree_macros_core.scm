(define-module (crates-io be vy bevy_startup_tree_macros_core) #:use-module (crates-io))

(define-public crate-bevy_startup_tree_macros_core-0.1.3 (c (n "bevy_startup_tree_macros_core") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "158szpy8awvyppyk2lyv8a7275299al86d2q8qwm6rls6j7i22wk")))

(define-public crate-bevy_startup_tree_macros_core-0.2.0 (c (n "bevy_startup_tree_macros_core") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14c70c9nsbz1xcdjawk5a2k4xl6a2fx7j04kgq4cai9d3m3pxvww")))

(define-public crate-bevy_startup_tree_macros_core-0.3.0 (c (n "bevy_startup_tree_macros_core") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14bn7qk0xrzwv6hwi5sik7a6s22bfh730xjrfzzfsl1yq4q28krf")))

(define-public crate-bevy_startup_tree_macros_core-0.4.0 (c (n "bevy_startup_tree_macros_core") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0q0071w6jcq4qgm348bqcqadzm61da64dslcyy29s84362xcd7r5")))

(define-public crate-bevy_startup_tree_macros_core-0.4.1 (c (n "bevy_startup_tree_macros_core") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1r62b8mi24n4719c00sy7wn5q0x4xhbs0mis2qdipm5i07d64f61")))

