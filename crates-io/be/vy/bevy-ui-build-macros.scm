(define-module (crates-io be vy bevy-ui-build-macros) #:use-module (crates-io))

(define-public crate-bevy-ui-build-macros-0.1.0 (c (n "bevy-ui-build-macros") (v "0.1.0") (h "1ppmwm2v6rn0psh94hvjdasgrsrsjg1xcm8gi7m9065jk6lxap1q")))

(define-public crate-bevy-ui-build-macros-0.2.0 (c (n "bevy-ui-build-macros") (v "0.2.0") (h "0pl4jzg3br34bs5h8fl2ncanj7g9piqb1jg4ap964aalb0l5s4lj")))

(define-public crate-bevy-ui-build-macros-0.2.1 (c (n "bevy-ui-build-macros") (v "0.2.1") (h "1nqaxi0rfacx3lda3p852arzgh3804250wgqchfxd9d86wd1m36s")))

(define-public crate-bevy-ui-build-macros-0.3.0 (c (n "bevy-ui-build-macros") (v "0.3.0") (h "00z44djb10d9lc53p86rcl11ph5wajnpfj3lm0x0nl5nsrbrsb5g")))

(define-public crate-bevy-ui-build-macros-0.4.0 (c (n "bevy-ui-build-macros") (v "0.4.0") (h "1nbvzydmghqdwvl9m8vzrvvlj6qjwl43mvczn18hg2yzy0sx2vpy")))

(define-public crate-bevy-ui-build-macros-0.5.0 (c (n "bevy-ui-build-macros") (v "0.5.0") (h "1n8nxj71scnz9hfrcfszmlgi9pyjp7xcdz05adql874qpx0a9ajl")))

(define-public crate-bevy-ui-build-macros-0.6.0 (c (n "bevy-ui-build-macros") (v "0.6.0") (h "13wm5zx5nq6lkvzxn6bspl03bh76k7mpn2w2d5liifjiy7r35kc1")))

(define-public crate-bevy-ui-build-macros-0.6.1 (c (n "bevy-ui-build-macros") (v "0.6.1") (h "1wifs59pci1nax95g11yc5xjl0nw40iirl373asar01l918rlwnw")))

