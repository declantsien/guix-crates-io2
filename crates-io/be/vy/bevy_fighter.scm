(define-module (crates-io be vy bevy_fighter) #:use-module (crates-io))

(define-public crate-bevy_fighter-0.1.0 (c (n "bevy_fighter") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)) (d (n "bevy_backroll") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lerp") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nj9cc0ga613qlpmax7vv9ywysscwnbnr6fab1s76h59x0yfrqns")))

(define-public crate-bevy_fighter-0.1.1 (c (n "bevy_fighter") (v "0.1.1") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)) (d (n "bevy_backroll") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lerp") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nacm6n8flax87cmapa5pgsdhrfr2agrgi6aqwmhlp33sjb63l4g")))

(define-public crate-bevy_fighter-0.1.11 (c (n "bevy_fighter") (v "0.1.11") (d (list (d (n "bevy") (r "^0.6.0") (d #t) (k 0)) (d (n "bevy_backroll") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lerp") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05dsdd19m3bb9x147w4f2af5xwxgf5yg57mbvrdr1xjc5cnv0y27")))

