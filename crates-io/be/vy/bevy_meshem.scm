(define-module (crates-io be vy bevy_meshem) #:use-module (crates-io))

(define-public crate-bevy_meshem-0.1.0 (c (n "bevy_meshem") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)))) (h "195p0f0rddf6vngkrjbh7byj13ix8r20b4dk5spqv95r9imrizw7")))

(define-public crate-bevy_meshem-0.1.1 (c (n "bevy_meshem") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)))) (h "0y9i9s0w7mypiqvpxhiy9gn0lahnw7ip8z1id3jniz8y162r4k3f")))

(define-public crate-bevy_meshem-0.2.0 (c (n "bevy_meshem") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1sgnj0szczawn16b2rrhqapv8q7qrmg0s80npvvphc7a3ddzh0gi")))

(define-public crate-bevy_meshem-0.2.1 (c (n "bevy_meshem") (v "0.2.1") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w4iabg69lgqxccp496hscyd6i1w52xnhwng91rx9n6d3xwrm750")))

(define-public crate-bevy_meshem-0.2.2 (c (n "bevy_meshem") (v "0.2.2") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02yr4x0jp5wf42rjcm0xr9xhpk4g8jiwj1ky27hgy8pzjrqi7v1n")))

(define-public crate-bevy_meshem-0.2.3 (c (n "bevy_meshem") (v "0.2.3") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0k1zz1sx2zir56lyz8siq3qmfsagbq6ib6inx37h3i1361wx3xyw")))

(define-public crate-bevy_meshem-0.2.4 (c (n "bevy_meshem") (v "0.2.4") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fw7aydk6rsf3s37ix16q9jg8qixfws89s625qwvyj65ib3pwmpp")))

(define-public crate-bevy_meshem-0.2.5 (c (n "bevy_meshem") (v "0.2.5") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yn8lw9bkwgw42xwgjrzxrfxdkfbhhj8srgpsycagyj720k5dmvc")))

(define-public crate-bevy_meshem-0.3.0 (c (n "bevy_meshem") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18fc60j0d7k4xv169f489s4s7j4gmfl1xaf4jbirkl0ncz60lndr")))

(define-public crate-bevy_meshem-0.4.0 (c (n "bevy_meshem") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0siy1xmpysk499bg54i1yavi139dh7k6q20xxij79qv8922lxja2")))

