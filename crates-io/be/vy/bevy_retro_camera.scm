(define-module (crates-io be vy bevy_retro_camera) #:use-module (crates-io))

(define-public crate-bevy_retro_camera-0.1.0 (c (n "bevy_retro_camera") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_render" "bevy_core_pipeline"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0aswhm5gjhcvyzxrc01wd0mghkhyc20qf6fh3a12ccaw44vlml11")))

(define-public crate-bevy_retro_camera-1.0.0 (c (n "bevy_retro_camera") (v "1.0.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_render" "bevy_core_pipeline"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0c2rf9fqc5bvkyfzvyv8dh5hrj514z3cxcy4f3ny133l5d2g0z1h")))

(define-public crate-bevy_retro_camera-1.0.1 (c (n "bevy_retro_camera") (v "1.0.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_render" "bevy_core_pipeline"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0gfwibw3rvsfh90x6i18diwlgk03xxbwahn9b98l910ck8zs6h2m")))

(define-public crate-bevy_retro_camera-1.1.0 (c (n "bevy_retro_camera") (v "1.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_render" "bevy_core_pipeline"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0xrzaaagj4c038fidkkjyg35q3j6x4sbxb2nlggyyi07fwg42jfw")))

