(define-module (crates-io be vy bevy_key_rotation) #:use-module (crates-io))

(define-public crate-bevy_key_rotation-0.1.0 (c (n "bevy_key_rotation") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy_async_task") (r "^0.1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.12") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "web-time") (r "^1.1.0") (d #t) (k 0)))) (h "0vkgcsh41p7av65crginyr5k1l8fclz5n7dcqm9p4izv7rj1hzvd")))

