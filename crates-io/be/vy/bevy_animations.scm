(define-module (crates-io be vy bevy_animations) #:use-module (crates-io))

(define-public crate-bevy_animations-0.1.0 (c (n "bevy_animations") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "05bw0ymylgyb2vlfvv240d9fd1yq4rkdc6nx3l9b587r0wp9fjnd")))

(define-public crate-bevy_animations-0.1.1 (c (n "bevy_animations") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "18ymngpcdnvjwz9rzbi3pcxyg2p88n8z5pc73dv8q7gh5jv4s9zl")))

(define-public crate-bevy_animations-0.1.2 (c (n "bevy_animations") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "0g91f457lqwsv5w2448xr9ln7sm1fan4sam7xnxjqbpin8mxk41v")))

(define-public crate-bevy_animations-0.1.3 (c (n "bevy_animations") (v "0.1.3") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "1kwz8r2xzfc44g76459r26slswwfz0n9f1x40yihjjhxfz21l7db")))

(define-public crate-bevy_animations-0.1.4 (c (n "bevy_animations") (v "0.1.4") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "09rdy11mlmq76ng07qgzda1csd0ahj0d7ghjizw7vkpa96lxrvls")))

(define-public crate-bevy_animations-0.1.5 (c (n "bevy_animations") (v "0.1.5") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "1qvimpbxcb76fmrg5j8cwyzfa87y1iwxw70lisipkyqfb40ffisd")))

(define-public crate-bevy_animations-0.1.6 (c (n "bevy_animations") (v "0.1.6") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "19bisx6cz7aa9rv15kksa5gqq1yfj10nxr3cxfcj5skx15sk7cwq")))

(define-public crate-bevy_animations-0.1.7 (c (n "bevy_animations") (v "0.1.7") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "135g7xyb1jszdl5jak51zj6aadz2ny8jbmknl5jx5j5yi10vdjh1")))

(define-public crate-bevy_animations-0.1.8 (c (n "bevy_animations") (v "0.1.8") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "0jk51vn14y8y8axagnjsf3f2s3594i25l2dagald1ylcidhcm3lq")))

(define-public crate-bevy_animations-0.1.9 (c (n "bevy_animations") (v "0.1.9") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "0jf8ws78z3vcmqkkc1szxalxglmrn42y0w5bpa49h50pyf3bn0v4")))

(define-public crate-bevy_animations-0.2.0 (c (n "bevy_animations") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "1b1ly7mky9acsvwcw449n4gf4h7ibjxgr00bdlmc220pmiy0pl7z")))

(define-public crate-bevy_animations-0.2.1 (c (n "bevy_animations") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "031nbgbb1r3h3rwndqdqlfv0i3rs9rylq782gm5234gx3mxxb1ii")))

(define-public crate-bevy_animations-0.2.2 (c (n "bevy_animations") (v "0.2.2") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "1bvxg2zmz861385krmfcljvm34dir09dmkw8zn2yzgspjgzn2b3m")))

(define-public crate-bevy_animations-0.2.3 (c (n "bevy_animations") (v "0.2.3") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "0mqiam73azbvrvvk7higyya5hh1g7xpf81l8m67367nascs1g8mc")))

(define-public crate-bevy_animations-0.3.0 (c (n "bevy_animations") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "1ghdq3vpa3zj361hjqidl370l409i17ja479jdawqpcw5sqhsrns")))

(define-public crate-bevy_animations-0.3.1 (c (n "bevy_animations") (v "0.3.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_animations_macros") (r "^0.4.0") (d #t) (k 0)))) (h "1n5598qavc91aifr0pba1ac4lil34d2hpccqf7h3vfnm60vi68c4")))

(define-public crate-bevy_animations-0.3.2 (c (n "bevy_animations") (v "0.3.2") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "10368gaxrp50w179yi473s2lzdlq5d7mgg543fa3fr5v6p5ahqpm")))

(define-public crate-bevy_animations-0.4.2 (c (n "bevy_animations") (v "0.4.2") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)))) (h "035372xhbgw5w37w403gfllgli5v3k7m0yin2qc7vs75dv3z7y9y")))

(define-public crate-bevy_animations-0.5.0 (c (n "bevy_animations") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "0pcshp5yj881yzqpg9px4s0vffrhfy00n6m2bib2w442ac5haaz6")))

(define-public crate-bevy_animations-0.3.3 (c (n "bevy_animations") (v "0.3.3") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "1rldmwxvpwpngl79p5vqlbfmrbx19jdmnwmf0fmx0i8yda0nhlbm")))

(define-public crate-bevy_animations-0.3.4 (c (n "bevy_animations") (v "0.3.4") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "1p3x4hih11lx8044ws9vvrcx1vqldyv9rfvs6y8qfff2wr764lw2")))

(define-public crate-bevy_animations-0.4.3 (c (n "bevy_animations") (v "0.4.3") (d (list (d (n "bevy") (r "^0.11.1") (d #t) (k 0)))) (h "1q7zdz6qlj96iqvdg1cldykgb455iryy9v43nz5hnx19yphda6ml")))

(define-public crate-bevy_animations-0.5.1 (c (n "bevy_animations") (v "0.5.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "0r2wqglnnq2g26mf1licr6r6bp2yswf6njgf65r1rs437yn5253z")))

(define-public crate-bevy_animations-0.5.2 (c (n "bevy_animations") (v "0.5.2") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "1g4lsqs03i4bmr85ia4wibp3fjg8firijr16pr80gviwa9vbi1r9")))

(define-public crate-bevy_animations-0.4.4 (c (n "bevy_animations") (v "0.4.4") (d (list (d (n "bevy") (r "^0.11.1") (d #t) (k 0)))) (h "0qmx5mav3s566k8vdi92kbgc91y53npj9a5m5hvak26673fr38i8")))

(define-public crate-bevy_animations-0.3.5 (c (n "bevy_animations") (v "0.3.5") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "1ngcj9izd5ja8m026rj2v7hy4qb16kfi72dxfjmkzbl63x42g58p")))

(define-public crate-bevy_animations-0.3.51 (c (n "bevy_animations") (v "0.3.51") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "045m397i18n4iqklhsjmwsvp3yq96i2jshhyv2aj355s3qvl7kfl")))

(define-public crate-bevy_animations-0.4.41 (c (n "bevy_animations") (v "0.4.41") (d (list (d (n "bevy") (r "^0.11.1") (d #t) (k 0)))) (h "0z049rdjlifvd9il4s1pd6137xdnxdnc1z73fsrgf0qb4gh8mxnk")))

(define-public crate-bevy_animations-0.5.21 (c (n "bevy_animations") (v "0.5.21") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "1qqrbrg0aqrs1a91hzxpg2h28sajj7y8jsplffimc9rdm9clwmr4")))

(define-public crate-bevy_animations-0.3.53 (c (n "bevy_animations") (v "0.3.53") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "059dpqfmgg2mvgq50mnhd6gcr8lcml0702wpsb12wz69b652662s")))

(define-public crate-bevy_animations-0.4.53 (c (n "bevy_animations") (v "0.4.53") (d (list (d (n "bevy") (r "^0.11.1") (d #t) (k 0)))) (h "1msb9nrp3xs5jd34iz34yr31qirfpqlfzvf8hr42733d3y7szgbp")))

(define-public crate-bevy_animations-0.5.53 (c (n "bevy_animations") (v "0.5.53") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "113wliwzpp1x5ipnrjdrih8b6nq3l8sh7kc4pzl50wjdk98qiqbb")))

(define-public crate-bevy_animations-0.3.6 (c (n "bevy_animations") (v "0.3.6") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "1f3fqr1p0h79bg87c5k8hcbawsw3zl23wdfzif4skhhisijq6rxd")))

(define-public crate-bevy_animations-0.4.6 (c (n "bevy_animations") (v "0.4.6") (d (list (d (n "bevy") (r "^0.11.1") (d #t) (k 0)))) (h "0jrv477qfa3y9navwmrnz3afi9w9ng9yrb9rx20m6g9ya9d6277r")))

(define-public crate-bevy_animations-0.5.6 (c (n "bevy_animations") (v "0.5.6") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "02id4n4l8kqgc5zp324nml76vn5h6bw60zzn7jig33cps8g9a6y1")))

(define-public crate-bevy_animations-0.3.61 (c (n "bevy_animations") (v "0.3.61") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)))) (h "0y1b6djyc1q6kd6c2kjl3b5vd717c582j3fdzlyd0df78iyk62xc")))

(define-public crate-bevy_animations-0.4.61 (c (n "bevy_animations") (v "0.4.61") (d (list (d (n "bevy") (r "^0.11.1") (d #t) (k 0)))) (h "1xcmgyn1xc6nnd0hf9pigks72213vrz3hcv7d5wr273gc6rfysxh")))

(define-public crate-bevy_animations-0.5.61 (c (n "bevy_animations") (v "0.5.61") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)))) (h "0x4wqqswjvskvpnsx76i0sy7mnvmg90gc08bhyckyq6mq53bznpg")))

(define-public crate-bevy_animations-0.6.0 (c (n "bevy_animations") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "15awizj09d0yfv12wblb1f3ahsx1lsnkp4r1lq95sms2fyx9kphl")))

(define-public crate-bevy_animations-0.5.62 (c (n "bevy_animations") (v "0.5.62") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "01gmkhv4iz7cj6mdar8fzqypc1awsbk3vr093h86i41vd6hfsm2c")))

