(define-module (crates-io be vy bevy_metamask) #:use-module (crates-io))

(define-public crate-bevy_metamask-0.1.0 (c (n "bevy_metamask") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.34") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("Window"))) (d #t) (k 0)))) (h "18nj5gdbdy92dpcbpwyw1j8bnv45zifh5pgi6sy3dxd8h6r3aqdb")))

