(define-module (crates-io be vy bevy_simple_text_input) #:use-module (crates-io))

(define-public crate-bevy_simple_text_input-0.1.0 (c (n "bevy_simple_text_input") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.2") (d #t) (k 0)))) (h "1gbfprnfwjaha1ql4dilrbibbcwlnl8lg7mvjkq6a9q2x7vp8ibk") (y #t)))

(define-public crate-bevy_simple_text_input-0.1.1 (c (n "bevy_simple_text_input") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "1wlj4w5vp4dvc34rby1rvh98ilh1c7yp0qqy3wkfkfax8s50clv1")))

(define-public crate-bevy_simple_text_input-0.1.2 (c (n "bevy_simple_text_input") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "1c1z4v8qhrxb7hd1p12wj2jdsd5myff4ls6495vkv50p0vif6h81")))

(define-public crate-bevy_simple_text_input-0.2.0 (c (n "bevy_simple_text_input") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "1hzin4nqnma64ikxyfhg4k42ijvqx0ndp0s6y0xgjx4m0n83gf4l")))

(define-public crate-bevy_simple_text_input-0.3.0 (c (n "bevy_simple_text_input") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)))) (h "0d1w9s820bqq0dx5z204rpdh1z5040cphw4131llx0wl6y302bxd") (y #t)))

(define-public crate-bevy_simple_text_input-0.3.1 (c (n "bevy_simple_text_input") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)))) (h "1nxwnnb7p1pqfv3pff9nnifxjhc3qapvm08488052wzbfr4fxvnf")))

(define-public crate-bevy_simple_text_input-0.4.0 (c (n "bevy_simple_text_input") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "0jr1rc62ax83jvaf9lflymwz3868pv1qbsin6a6zsf7fm55ssywg")))

(define-public crate-bevy_simple_text_input-0.5.0 (c (n "bevy_simple_text_input") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "1q1adfykhgx1pzd3nk4mkvqqdw07glypiqkgljp96fsvnjj7z1bi")))

(define-public crate-bevy_simple_text_input-0.5.1 (c (n "bevy_simple_text_input") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "0xmm8m3n68krpysnsvf72akvik79dx3n8p2npyq79md71p64qv8p")))

(define-public crate-bevy_simple_text_input-0.6.0 (c (n "bevy_simple_text_input") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "0m8irf95fxija7r5lxz49gffmxafwky59ymcrfy0pb82jk9q4rqa")))

(define-public crate-bevy_simple_text_input-0.6.1 (c (n "bevy_simple_text_input") (v "0.6.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "1yxddibi572armhar6vqxiyjvd93py73mmkmb55cw3qnmkqxf6lv")))

(define-public crate-bevy_simple_text_input-0.7.0 (c (n "bevy_simple_text_input") (v "0.7.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_asset" "bevy_text"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "0a8nkljjzn7m15v269cxmb8aqcwmsdg4kmdsd5dsmi18xp2r4zxv")))

