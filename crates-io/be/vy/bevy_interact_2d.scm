(define-module (crates-io be vy bevy_interact_2d) #:use-module (crates-io))

(define-public crate-bevy_interact_2d-0.5.0 (c (n "bevy_interact_2d") (v "0.5.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.3") (d #t) (k 0)))) (h "1shn3ikwp0mals3kv9dnw5swj4izsjpywa99rb0d4h522c7i1mnq")))

(define-public crate-bevy_interact_2d-0.5.1 (c (n "bevy_interact_2d") (v "0.5.1") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.3") (d #t) (k 0)) (d (n "spirv_headers") (r ">=1.5.0, <1.5.1") (d #t) (k 0)))) (h "1pn54l6201vhqmgfww5jvjakrwhvzjhwa3m6j08zk84kzn6h073k")))

(define-public crate-bevy_interact_2d-0.5.2 (c (n "bevy_interact_2d") (v "0.5.2") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "spirv_headers") (r ">=1.5.0, <1.5.1") (d #t) (k 0)))) (h "0zw2jihk3dkijnf7xzcdn68m2qhga8gfk9fg6sw5iad580dz8kik")))

(define-public crate-bevy_interact_2d-0.5.3 (c (n "bevy_interact_2d") (v "0.5.3") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0h9gaghc4gg4l9hb55l6459gl7zzxn7vn4lcicb3fnwwn2pcf105")))

(define-public crate-bevy_interact_2d-0.9.0 (c (n "bevy_interact_2d") (v "0.9.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_prototype_lyon") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "18mgln0f49w7hjw2hvms3jpy9kkm9dggslygn5khj72zq9115m0a") (f (quote (("debug" "bevy_prototype_lyon"))))))

