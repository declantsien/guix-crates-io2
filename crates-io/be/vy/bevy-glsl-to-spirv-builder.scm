(define-module (crates-io be vy bevy-glsl-to-spirv-builder) #:use-module (crates-io))

(define-public crate-bevy-glsl-to-spirv-builder-0.0.1 (c (n "bevy-glsl-to-spirv-builder") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.66") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (o #t) (d #t) (k 1)))) (h "1yg5zi08mvvk8763wib202s00ls6rjkfj0i00j2jjwabn41xpdq2") (f (quote (("default") ("build-from-source" "cmake" "cc"))))))

