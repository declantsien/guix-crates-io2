(define-module (crates-io be vy bevy_window_title_diagnostics) #:use-module (crates-io))

(define-public crate-bevy_window_title_diagnostics-0.1.0 (c (n "bevy_window_title_diagnostics") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)))) (h "0fmv3b54qhn01xjcm6726qa1vfj87m9yd60yr7yyqbv6nn37fpfs")))

(define-public crate-bevy_window_title_diagnostics-0.1.1 (c (n "bevy_window_title_diagnostics") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)))) (h "01yaxnbv30yjpj131l65y8srrkfjpyqa0dd3708l2isk7fakpk19")))

(define-public crate-bevy_window_title_diagnostics-0.2.0 (c (n "bevy_window_title_diagnostics") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)))) (h "0jhs460qfbs3ij6a44nri9fjxhja75imkb2dma5q243kdvab0ymd")))

(define-public crate-bevy_window_title_diagnostics-0.3.0 (c (n "bevy_window_title_diagnostics") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)))) (h "0p8qwp29byr9w0pn55y764pw81b5rn8mcqkz0f9359bszfanhnpd")))

(define-public crate-bevy_window_title_diagnostics-0.4.0 (c (n "bevy_window_title_diagnostics") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)))) (h "1szz8hlffm15qnli2wl5fzdwra92ndqfw0zgcb72b7h1qsd64bwa")))

(define-public crate-bevy_window_title_diagnostics-0.5.0 (c (n "bevy_window_title_diagnostics") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "09ffj77k69d4vmp60khv4bzbcxv7asivqlg6ix009s6nxkr8fim6")))

(define-public crate-bevy_window_title_diagnostics-0.6.0 (c (n "bevy_window_title_diagnostics") (v "0.6.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "1n2ipqxss952z8l8m4bjdkpllfmii6ya86lc46i2bxj1zpn555v0")))

(define-public crate-bevy_window_title_diagnostics-0.12.0 (c (n "bevy_window_title_diagnostics") (v "0.12.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "0w6j79nlgmrahpskdgk6lak9phd958qzapj09kp80bgg8nzia37p")))

(define-public crate-bevy_window_title_diagnostics-0.13.0 (c (n "bevy_window_title_diagnostics") (v "0.13.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "0nlr38509fdrv7l9m6zgkrm888ddsq5bxdwz024ffqfq684080i5")))

(define-public crate-bevy_window_title_diagnostics-0.13.1 (c (n "bevy_window_title_diagnostics") (v "0.13.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "1ym9czapalg1nchmw43zjwy9gji9whxyqxiqp15y9bsv95a4w27f")))

