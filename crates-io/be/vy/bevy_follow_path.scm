(define-module (crates-io be vy bevy_follow_path) #:use-module (crates-io))

(define-public crate-bevy_follow_path-0.1.0 (c (n "bevy_follow_path") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "euclid") (r "^0.22.7") (d #t) (k 0)) (d (n "lyon_geom") (r "^1.0.1") (d #t) (k 0)))) (h "0sbgalffsnd9a7irv8iwvivxxspxb0pdr5x5c5lbiff1fsjhsmgb") (s 2) (e (quote (("debug_draw" "dep:bevy_prototype_lyon"))))))

(define-public crate-bevy_follow_path-0.2.0 (c (n "bevy_follow_path") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bezier-nd") (r "^0.1.4") (d #t) (k 0)) (d (n "euclid") (r "^0.22.7") (d #t) (k 0)) (d (n "geo-nd") (r "^0.1.3") (d #t) (k 0)))) (h "013f2d5s0a5qjafcb78cy7zhnzhif3m7919n64294pirya4f6x6h") (f (quote (("debug_draw"))))))

(define-public crate-bevy_follow_path-0.2.1 (c (n "bevy_follow_path") (v "0.2.1") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bezier-nd") (r "^0.1.4") (d #t) (k 0)) (d (n "euclid") (r "^0.22.7") (d #t) (k 0)) (d (n "geo-nd") (r "^0.1.3") (d #t) (k 0)))) (h "0cxp8bqzvsqnjgwm5yk3z95mivy3alkbi4qgrigkl82wjwxwgnmb") (f (quote (("debug_draw"))))))

(define-public crate-bevy_follow_path-0.3.0 (c (n "bevy_follow_path") (v "0.3.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bezier-nd") (r "^0.1.4") (d #t) (k 0)) (d (n "euclid") (r "^0.22.7") (d #t) (k 0)) (d (n "geo-nd") (r "^0.1.3") (d #t) (k 0)))) (h "1fixln24rl4q9k5518z6dbvyrixgd5plyv768a4w4p6bg23d413p") (f (quote (("debug_draw"))))))

