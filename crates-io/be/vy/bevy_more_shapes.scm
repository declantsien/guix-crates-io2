(define-module (crates-io be vy bevy_more_shapes) #:use-module (crates-io))

(define-public crate-bevy_more_shapes-0.1.0 (c (n "bevy_more_shapes") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (d #t) (k 0)) (d (n "bevy") (r "^0.6") (d #t) (k 2)) (d (n "bevy_flycam") (r "^0.6.0") (d #t) (k 2)))) (h "1zssmsg730z654cr680s9pziax2i4h9gnmbgbrc34vl7d0ndyxbx")))

(define-public crate-bevy_more_shapes-0.2.0 (c (n "bevy_more_shapes") (v "0.2.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (d #t) (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.3") (d #t) (k 2)))) (h "1mk7xxl7lp1v6l7c48zx1018qd4ichmr2bw6vrpgp9m51jcniicb")))

(define-public crate-bevy_more_shapes-0.2.1 (c (n "bevy_more_shapes") (v "0.2.1") (d (list (d (n "bevy") (r "^0.7") (f (quote ("render"))) (d #t) (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.3") (d #t) (k 2)) (d (n "triangulate") (r "^0.1.0") (d #t) (k 0)))) (h "0vmfr0jl7vrlyqrfmyhkv26g3nxlm3yjkjpy8lh1hhc6w9imhrz2")))

(define-public crate-bevy_more_shapes-0.3.0 (c (n "bevy_more_shapes") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("render"))) (d #t) (k 0)) (d (n "triangulate") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.6") (d #t) (k 2)))) (h "03raz6ydgypy8niqj3wl6p2g4zzyncv8m501w7a3ssp7qxg9z3qj")))

(define-public crate-bevy_more_shapes-0.4.0 (c (n "bevy_more_shapes") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (d #t) (k 0)) (d (n "triangulate") (r "^0.2.0") (d #t) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.8") (d #t) (k 2)))) (h "0cvc77w4vrnxiz53psmk0c5x36kb2fj6pplmpmvkzlayal6asl4l")))

(define-public crate-bevy_more_shapes-0.5.0 (c (n "bevy_more_shapes") (v "0.5.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render"))) (d #t) (k 0)) (d (n "triangulate") (r "^0.2.0") (d #t) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_normal_material") (r "^0.2.1") (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.8") (d #t) (k 2)))) (h "1lbnxmg3gzbjmy0pk9pb185cgyh0px3ikgwihmp9vswfr9lsmydk")))

