(define-module (crates-io be vy bevy_screen_diagnostics) #:use-module (crates-io))

(define-public crate-bevy_screen_diagnostics-0.1.0 (c (n "bevy_screen_diagnostics") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_text" "bevy_ui" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "0gsivx6sf497fk4hl2drjkh9wrvygpykyla63lalnsz23nrsch3q")))

(define-public crate-bevy_screen_diagnostics-0.1.1 (c (n "bevy_screen_diagnostics") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_text" "bevy_ui" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "0sj5qqz17dfmskkm3kq0zij905si47vwjvsps0fldsz756mxdx5y")))

(define-public crate-bevy_screen_diagnostics-0.2.0 (c (n "bevy_screen_diagnostics") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_text" "bevy_ui" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "1hq0zqlw83d8rrapxffl0xsxavvzlkmw3ryyxc99f4b5qh6h2yhf")))

(define-public crate-bevy_screen_diagnostics-0.2.1 (c (n "bevy_screen_diagnostics") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_text" "bevy_ui" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "1sd39r0idv0jr0792whsivpiwd6laqhbv556vk7c5yijyx6iz6ys")))

(define-public crate-bevy_screen_diagnostics-0.2.2 (c (n "bevy_screen_diagnostics") (v "0.2.2") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_text" "bevy_ui" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "0d87l6m6r10g5i7npqc1h91qmkbfym6qk1rs4lc2mygh764fs6cb") (f (quote (("default" "builtin-font") ("builtin-font"))))))

(define-public crate-bevy_screen_diagnostics-0.2.3 (c (n "bevy_screen_diagnostics") (v "0.2.3") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_text" "bevy_ui" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "0gz60kr99k18l8dbhgxb6dvq3chzlbb80xbkgagd4nigq4ncv8fc") (f (quote (("default" "builtin-font") ("builtin-font"))))))

(define-public crate-bevy_screen_diagnostics-0.3.0 (c (n "bevy_screen_diagnostics") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_text" "bevy_ui" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "126j0g0dy00i9i7j9x1ckl8ljb4x2jffkfbpsvyd4mnfv5413x80") (f (quote (("default" "builtin-font") ("builtin-font"))))))

(define-public crate-bevy_screen_diagnostics-0.4.0 (c (n "bevy_screen_diagnostics") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_text" "bevy_ui" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)))) (h "1d631686kh2xm7ax4z0n9kg4473ajm4wzh0szspnbb8lkv67ggwb") (f (quote (("default" "builtin-font") ("builtin-font" "bevy/default_font"))))))

(define-public crate-bevy_screen_diagnostics-0.5.0 (c (n "bevy_screen_diagnostics") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_text" "bevy_ui" "bevy_asset" "bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)))) (h "13baj9mniqa6ir1pr2x8zlxn3sy2f4kf5sgz7gq1rca33175kizf") (f (quote (("default" "builtin-font") ("builtin-font" "bevy/default_font"))))))

