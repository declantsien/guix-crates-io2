(define-module (crates-io be vy bevy_proto_typetag_derive) #:use-module (crates-io))

(define-public crate-bevy_proto_typetag_derive-0.2.0 (c (n "bevy_proto_typetag_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15hj8mkh2dgh1dxccwky2hh1a0dgcjdibkw7linygvzxzzvx37f4")))

