(define-module (crates-io be vy bevy_fuzz) #:use-module (crates-io))

(define-public crate-bevy_fuzz-0.0.1 (c (n "bevy_fuzz") (v "0.0.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "bevy") (r "^0.6") (k 0)))) (h "1clwvnp3w8c7h0m7yic13xq4jxnr06wgahy51m526lwphnjj7mm0")))

(define-public crate-bevy_fuzz-0.0.2 (c (n "bevy_fuzz") (v "0.0.2") (d (list (d (n "bevy") (r "^0.6") (f (quote ("serialize"))) (k 0)) (d (n "postcard") (r "^0.7.3") (f (quote ("use-std" "alloc"))) (d #t) (k 0)) (d (n "postcard-cobs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d1khynnr998rg4986gvfyp9fm2ac8wcgm2s78psbfwg45n5nwvb")))

