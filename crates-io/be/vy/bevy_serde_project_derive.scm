(define-module (crates-io be vy bevy_serde_project_derive) #:use-module (crates-io))

(define-public crate-bevy_serde_project_derive-0.1.0 (c (n "bevy_serde_project_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "13w3h5a0m1cn5a4q9zwgv5kh2wmpdp345fl83bjd5la9snckrk0x")))

(define-public crate-bevy_serde_project_derive-0.2.0 (c (n "bevy_serde_project_derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "1kyg9vragkxc9j9lnp3rm4bc5llgmcf82wrdlsx4qv89rmy7i62l")))

(define-public crate-bevy_serde_project_derive-0.2.1 (c (n "bevy_serde_project_derive") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "1n8ypq76r55wcz0ddk7dpp10k0af9n3hpblg2n1x3rpfwl6zk90n")))

