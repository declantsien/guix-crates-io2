(define-module (crates-io be vy bevy_ios_impact) #:use-module (crates-io))

(define-public crate-bevy_ios_impact-0.1.0 (c (n "bevy_ios_impact") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "ios_impact") (r "^0.1") (d #t) (k 0)) (d (n "objc2") (r "^0.4.1") (d #t) (k 0)))) (h "15z72hb4wxx4r6zmb4ixr15kaz7dq44fwld3kc28l90hd73a298a")))

(define-public crate-bevy_ios_impact-0.1.1 (c (n "bevy_ios_impact") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "ios_impact") (r "^0.1") (d #t) (k 0)) (d (n "objc2") (r "^0.4.1") (d #t) (k 0)))) (h "0nw7ns79kg1rpm6r35i546r9dx3nl49m8ci4fg1483vdqz9kqxin")))

(define-public crate-bevy_ios_impact-0.1.2 (c (n "bevy_ios_impact") (v "0.1.2") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "ios_impact") (r "^0.1") (d #t) (t "cfg(target_os = \"ios\")") (k 0)) (d (n "objc2") (r "^0.4.1") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "068hsdpbznyg2hz0r7wwl1jkca2plnykq974dzfk7nsn43scmlfw")))

(define-public crate-bevy_ios_impact-0.1.3 (c (n "bevy_ios_impact") (v "0.1.3") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "ios_impact") (r "^0.1") (d #t) (t "cfg(target_os = \"ios\")") (k 0)) (d (n "objc2") (r "^0.4.1") (d #t) (t "cfg(target_os = \"ios\")") (k 0)))) (h "140v4bhmpm2p58y97rc93l0hqv9gdhgz1a3zpbl2shcabpl97grv")))

