(define-module (crates-io be vy bevy_ui_string_parser) #:use-module (crates-io))

(define-public crate-bevy_ui_string_parser-0.1.0 (c (n "bevy_ui_string_parser") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "1rmqqrja7h5hpqfhv6f0jlan6wyv5az469lwnl36ba2mfxdfm454") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bevy_ui_string_parser-0.1.1 (c (n "bevy_ui_string_parser") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "1m4m3b3nxiv2p15qc1vc1ya3kvcpgclagl6p4jv1h1spkpq3laa3") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bevy_ui_string_parser-0.1.2 (c (n "bevy_ui_string_parser") (v "0.1.2") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "1k3jjiyqp8a9ysmk5nlfql9ym9zlbf1jwnjj8qvkby9n3kb4ys9i") (s 2) (e (quote (("serde" "dep:serde"))))))

