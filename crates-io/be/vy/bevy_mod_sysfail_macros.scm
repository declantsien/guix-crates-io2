(define-module (crates-io be vy bevy_mod_sysfail_macros) #:use-module (crates-io))

(define-public crate-bevy_mod_sysfail_macros-0.1.0 (c (n "bevy_mod_sysfail_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zxwips9gdh6zn5zk9gf7n8igxvxjpr4hkw7hf273vrdf41y5qnl")))

(define-public crate-bevy_mod_sysfail_macros-1.0.0 (c (n "bevy_mod_sysfail_macros") (v "1.0.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "023hay4v7k02k28ymyy0prl84ihpky0hqbpz10b8kpjn6zgyshsk")))

(define-public crate-bevy_mod_sysfail_macros-1.1.0 (c (n "bevy_mod_sysfail_macros") (v "1.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dvmcwa9aw619bjavc9z9204j5ij876r8jjvml9xfk7sw0yyvl3i")))

(define-public crate-bevy_mod_sysfail_macros-2.0.0 (c (n "bevy_mod_sysfail_macros") (v "2.0.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wrahjaxnh9jx7k7vxcfybb0xnlk28qmxi53xlq85frjsdnilh1f")))

(define-public crate-bevy_mod_sysfail_macros-3.0.0 (c (n "bevy_mod_sysfail_macros") (v "3.0.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b78nwcqs3832m2wn0bhdakjkw635l3qqyhy9qdb6d3scm548qbq")))

(define-public crate-bevy_mod_sysfail_macros-4.2.0 (c (n "bevy_mod_sysfail_macros") (v "4.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "proc-macro" "parsing" "clone-impls"))) (d #t) (k 0)))) (h "0dzxpx7dyiwynqqqnxyhsvm5q6s15h8qkq74gcwwwd9ppiyf5ykk")))

(define-public crate-bevy_mod_sysfail_macros-5.0.0 (c (n "bevy_mod_sysfail_macros") (v "5.0.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "proc-macro" "parsing" "clone-impls"))) (d #t) (k 0)))) (h "1n1lixcgmw1ldsm1sl05p5s2psm9py5fqp2pd5j12xfc3ykfnpar")))

