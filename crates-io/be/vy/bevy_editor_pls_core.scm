(define-module (crates-io be vy bevy_editor_pls_core) #:use-module (crates-io))

(define-public crate-bevy_editor_pls_core-0.1.0 (c (n "bevy_editor_pls_core") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy-inspector-egui") (r "^0.12") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "0whl0mhq6w85azkg023kjgp4ijnjv2c09n8r3zs2annq3mll3zgd") (f (quote (("viewport"))))))

(define-public crate-bevy_editor_pls_core-0.1.1 (c (n "bevy_editor_pls_core") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy-inspector-egui") (r "^0.12") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "1d7fl7lr1xdfs2qs0gwbl0axhwwr283gd2vn29nfmiy8nx9anvfc") (f (quote (("viewport"))))))

(define-public crate-bevy_editor_pls_core-0.2.0 (c (n "bevy_editor_pls_core") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy-inspector-egui") (r "^0.14") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "0hgxgisi2nr5sjhsammik3iiw8hv8qvix5nq3h98skzx4rkqw2ns") (f (quote (("viewport"))))))

(define-public crate-bevy_editor_pls_core-0.3.0 (c (n "bevy_editor_pls_core") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (f (quote ("bevy_pbr"))) (k 0)) (d (n "egui_dock") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "06bijy79xr8nwi4cwx1krwg5p9nbd2ad9i1v0an1s6d8nyy1vfwy")))

(define-public crate-bevy_editor_pls_core-0.3.1 (c (n "bevy_editor_pls_core") (v "0.3.1") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (d #t) (k 0)) (d (n "egui_dock") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "19im0xncp8h1zvy5ip667fq7km2gm6v5x3mggyj6hxdw4ajm1hmj")))

(define-public crate-bevy_editor_pls_core-0.4.0 (c (n "bevy_editor_pls_core") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (d #t) (k 0)) (d (n "egui_dock") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "00708hnglhv0lca9s7fixj7swcsgjzd09sb9hmka0ga1mw52jq5l")))

(define-public crate-bevy_editor_pls_core-0.5.0 (c (n "bevy_editor_pls_core") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy-inspector-egui") (r "^0.20") (d #t) (k 0)) (d (n "egui_dock") (r "^0.8") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)))) (h "0m5c44sfsy81iyq6p1f6bvqhi66k23v71zrysk79q1r8q8ay63n2")))

(define-public crate-bevy_editor_pls_core-0.6.0 (c (n "bevy_editor_pls_core") (v "0.6.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy-inspector-egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_dock") (r "^0.8") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)))) (h "17436h1ffig9vfdw14ar3j1drwggx0xapc0zdabpsgb5vqqv7fmr")))

(define-public crate-bevy_editor_pls_core-0.7.0 (c (n "bevy_editor_pls_core") (v "0.7.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy-inspector-egui") (r "^0.22.0") (d #t) (k 0)) (d (n "egui_dock") (r "^0.9") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)))) (h "0p5b0v3f334dbygd5mi55znr6l4xzb92wcifvqdvrr35hfwvj7ws")))

(define-public crate-bevy_editor_pls_core-0.8.0 (c (n "bevy_editor_pls_core") (v "0.8.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy-inspector-egui") (r "^0.23.0") (d #t) (k 0)) (d (n "egui_dock") (r "^0.11") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)))) (h "0bl2avn7xqvlwn2psmklrffgi47804x8r5b56i30kh18xc0aslb5")))

(define-public crate-bevy_editor_pls_core-0.8.1 (c (n "bevy_editor_pls_core") (v "0.8.1") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy-inspector-egui") (r "^0.23.0") (d #t) (k 0)) (d (n "egui_dock") (r "^0.11") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)))) (h "1x71966rxn3kwi8gfg5bk055g01jlwk18kzgpbmfjf26434rvpl4")))

