(define-module (crates-io be vy bevy-events-docs-creator) #:use-module (crates-io))

(define-public crate-bevy-events-docs-creator-0.1.0 (c (n "bevy-events-docs-creator") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "markdown") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1r1hnjvyd327x9i6vlynfs52gn6d8gqi7kyagzvfr7qxp07j1jkn")))

