(define-module (crates-io be vy bevy-glsl-to-spirv) #:use-module (crates-io))

(define-public crate-bevy-glsl-to-spirv-0.1.7 (c (n "bevy-glsl-to-spirv") (v "0.1.7") (d (list (d (n "cmake") (r "^0.1.27") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "18y3xwq3ajgnmis33hd2k8m0lqxkl9qrcvc9mz6nq057v98b2djz")))

(define-public crate-bevy-glsl-to-spirv-0.2.0 (c (n "bevy-glsl-to-spirv") (v "0.2.0") (h "10jz7appsxgxc0j089jd55axdkfwvhn5cnxpc9xhy036481pxcyh")))

(define-public crate-bevy-glsl-to-spirv-0.2.1 (c (n "bevy-glsl-to-spirv") (v "0.2.1") (h "0qay9vl9ghffra8rgrr7ydyxv02yf31r55vr405cbhxfy1c2ypqd")))

