(define-module (crates-io be vy bevy_ineffable_macros) #:use-module (crates-io))

(define-public crate-bevy_ineffable_macros-0.1.0 (c (n "bevy_ineffable_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "09mnlnr1hq1gi6p5cpqbwypmc95w04s1wjrrdakjwmhqp2r2kb15")))

(define-public crate-bevy_ineffable_macros-0.2.0 (c (n "bevy_ineffable_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0zcp12xf02dgnz1qfd3jkx8vy72hbsdwk2k1cwglp1bja1ranbjq")))

(define-public crate-bevy_ineffable_macros-0.3.0 (c (n "bevy_ineffable_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0khahm0zjscphg7fs5fqdbdz75d04a2wjyd062zv0dwaa3zg03ik")))

(define-public crate-bevy_ineffable_macros-0.4.0 (c (n "bevy_ineffable_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "1a6s96dzsrf5zy0dkfhbp2z92s1kw4islq86ya0ck0zjkpy47ys1")))

(define-public crate-bevy_ineffable_macros-0.5.0 (c (n "bevy_ineffable_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "1zfif4q47ar6xq4mqyh7hcnqzklw8b3g199g98hkphr6n51g1q3s")))

