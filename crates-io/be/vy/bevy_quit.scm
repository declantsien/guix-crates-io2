(define-module (crates-io be vy bevy_quit) #:use-module (crates-io))

(define-public crate-bevy_quit-0.1.0 (c (n "bevy_quit") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "09276wfm3f4m3drzqxd1vzic14h7r64zxqd4r7f3cfw9sswva38b")))

(define-public crate-bevy_quit-0.1.1 (c (n "bevy_quit") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)))) (h "0n9lphh8nihqbzfv69vzj0l6hixxfqgkwq62cilw9br027ji7mc7")))

