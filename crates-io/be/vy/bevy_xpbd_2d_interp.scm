(define-module (crates-io be vy bevy_xpbd_2d_interp) #:use-module (crates-io))

(define-public crate-bevy_xpbd_2d_interp-0.1.0 (c (n "bevy_xpbd_2d_interp") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.3.1") (d #t) (k 0)))) (h "10d8rpjnx33ysw5np4x4lbsb36zavblj7x4455ix5fhz2vss0hha") (f (quote (("default" "2d") ("2d"))))))

(define-public crate-bevy_xpbd_2d_interp-0.1.1 (c (n "bevy_xpbd_2d_interp") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.3.2") (d #t) (k 0)))) (h "0jryrjvss9vhd3g2hhv7ihnqcc2bz7r9b2rwrwzn4p1sjw3hk6rc") (f (quote (("default" "2d") ("2d"))))))

(define-public crate-bevy_xpbd_2d_interp-0.1.2 (c (n "bevy_xpbd_2d_interp") (v "0.1.2") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy_xpbd_2d") (r "^0.4") (d #t) (k 0)))) (h "1110ajhb4a60l1ha1ksxx5kagjlyk46yn7m0g7sjxsqgn0490i94") (f (quote (("default" "2d") ("2d"))))))

