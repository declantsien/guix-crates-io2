(define-module (crates-io be vy bevy_skybox_cubemap) #:use-module (crates-io))

(define-public crate-bevy_skybox_cubemap-0.1.0 (c (n "bevy_skybox_cubemap") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 0)) (d (n "bevy") (r "^0.5.0") (d #t) (k 2)))) (h "1rygbnlrr34s8x0ih13c7wfpjx0xpzfjd3skiz3pg8lb943crzqa")))

