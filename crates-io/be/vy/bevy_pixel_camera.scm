(define-module (crates-io be vy bevy_pixel_camera) #:use-module (crates-io))

(define-public crate-bevy_pixel_camera-0.1.0 (c (n "bevy_pixel_camera") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)))) (h "10iaadj9nhhzci9qa9qlibn4iizl6gr91ny7bbl9gbnfa0pld4c7")))

(define-public crate-bevy_pixel_camera-0.1.1 (c (n "bevy_pixel_camera") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)))) (h "01pwjjg0376qv6lvb4kv1x7xbpjsqks2x31vwvckhwnx1yhb89a8")))

(define-public crate-bevy_pixel_camera-0.1.2 (c (n "bevy_pixel_camera") (v "0.1.2") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)))) (h "0yz30qq0m6073947v12dhx4wn2d62xn3mfl7a4bnspfhfzsb1lc2")))

(define-public crate-bevy_pixel_camera-0.1.3 (c (n "bevy_pixel_camera") (v "0.1.3") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)))) (h "13jwlbmyj6kvlm9akdsph6swbmbq6mdh631596s9bczajk6r6xf5")))

(define-public crate-bevy_pixel_camera-0.2.0 (c (n "bevy_pixel_camera") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)))) (h "0sdsvkxrzlnr4p2bfl1fhdksrdyzb94wn8vdlvbmlb84gyv55l7s")))

(define-public crate-bevy_pixel_camera-0.2.1 (c (n "bevy_pixel_camera") (v "0.2.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_winit" "bevy_asset" "png" "x11"))) (k 2)))) (h "0hikvhzpjjr08nzf25g5vyzxsfw5kbsp2d0gav4ar0qr07i8kk42")))

(define-public crate-bevy_pixel_camera-0.3.0 (c (n "bevy_pixel_camera") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_winit" "bevy_asset" "png" "x11"))) (k 2)))) (h "11ca0frg76kjrih7pxhasvr0pf13hc5jgjif1bn2dga144c8i5wh")))

(define-public crate-bevy_pixel_camera-0.4.0 (c (n "bevy_pixel_camera") (v "0.4.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_winit" "bevy_asset" "png" "x11"))) (k 2)))) (h "1wh1c86pmscyvlbfpy2jkgwbz3h988gbcbb289i9cd0c2byn0ini")))

(define-public crate-bevy_pixel_camera-0.4.1 (c (n "bevy_pixel_camera") (v "0.4.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_winit" "bevy_asset" "png" "x11"))) (k 2)))) (h "0isvymf0gfy89y49z66cfyhcf87jz1rbiipsxx4dbymwy0w7dwyk")))

(define-public crate-bevy_pixel_camera-0.5.0 (c (n "bevy_pixel_camera") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_winit" "bevy_asset" "png" "x11"))) (k 2)))) (h "0r8chscy6n2jsphcqa5fwrmla1nyj6s258zcccq5vj0zb5ibgs24")))

(define-public crate-bevy_pixel_camera-0.5.1 (c (n "bevy_pixel_camera") (v "0.5.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_winit" "bevy_asset" "png" "x11"))) (k 2)))) (h "1iq91xfyp42fwa58x1ppblnlpmdprhbq1z6amjb7zh4dd7cj4qrg")))

(define-public crate-bevy_pixel_camera-0.12.0 (c (n "bevy_pixel_camera") (v "0.12.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit" "bevy_asset" "png" "multi-threaded" "x11"))) (k 2)))) (h "0j5rzbsl4cjhvhicmzw4vf34j14mn93rk8vgzdhgj8wm6m7az7gz")))

(define-public crate-bevy_pixel_camera-0.12.1 (c (n "bevy_pixel_camera") (v "0.12.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit" "bevy_asset" "png" "multi-threaded" "x11"))) (k 2)))) (h "1ajynzswmqshh2332gng4mjmmcaz24kzw8giwrfk0bzijrr05jkd")))

(define-public crate-bevy_pixel_camera-0.13.0 (c (n "bevy_pixel_camera") (v "0.13.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_winit" "bevy_asset" "png" "multi-threaded" "x11"))) (k 2)))) (h "150pk41n3agq5y1s56r52bphkymjmf4mcym0bmxag4l59b46chbv")))

