(define-module (crates-io be vy bevy_toon_shader) #:use-module (crates-io))

(define-public crate-bevy_toon_shader-0.1.0 (c (n "bevy_toon_shader") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_pbr" "bevy_render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.20.2") (d #t) (k 2)))) (h "1cl4cl0lz7z1nws69f2x2xw8l2j16xnvnvlg4jm42hpx8f6sy553")))

(define-public crate-bevy_toon_shader-0.2.0 (c (n "bevy_toon_shader") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_pbr" "bevy_render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.21.0") (d #t) (k 2)))) (h "11jc9sjld2np505spwwkd45vqgaww652swfi76yxzaaip87n47kb")))

(define-public crate-bevy_toon_shader-0.3.0 (c (n "bevy_toon_shader") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_pbr" "bevy_render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.23.0") (d #t) (k 2)))) (h "1rny0ngi4w13fsgblkv5q3lfvy41lhsnpl8my82c1p1bv76fzr8n")))

