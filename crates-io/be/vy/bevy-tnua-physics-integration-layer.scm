(define-module (crates-io be vy bevy-tnua-physics-integration-layer) #:use-module (crates-io))

(define-public crate-bevy-tnua-physics-integration-layer-0.1.0 (c (n "bevy-tnua-physics-integration-layer") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (k 0)))) (h "0y0n14wndki9lcjyjfns02dy6zgk163kzmawifbnfm98pd7b5gs2")))

(define-public crate-bevy-tnua-physics-integration-layer-0.1.1 (c (n "bevy-tnua-physics-integration-layer") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (k 0)))) (h "09h7vfi6p0y1blrnvdbhz6drfr993zpk2njvqwfnsg69j67kqf1h")))

(define-public crate-bevy-tnua-physics-integration-layer-0.2.0 (c (n "bevy-tnua-physics-integration-layer") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (k 0)))) (h "0sglglyy311kskdmzy1zw0czk057zi6n6l99z32v6sgpx5gc45i1")))

(define-public crate-bevy-tnua-physics-integration-layer-0.3.0 (c (n "bevy-tnua-physics-integration-layer") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (k 0)))) (h "0s6z5lz4w6cx2k3mcwf629nxpq9vf38221cjvqqishd54ixhj91r") (f (quote (("f64"))))))

