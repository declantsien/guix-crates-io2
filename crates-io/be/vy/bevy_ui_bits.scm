(define-module (crates-io be vy bevy_ui_bits) #:use-module (crates-io))

(define-public crate-bevy_ui_bits-0.1.0 (c (n "bevy_ui_bits") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (f (quote ("bevy_ui" "bevy_render" "bevy_asset" "bevy_text"))) (k 0)))) (h "1hj34mmgly7rz3hbplfq54dji2ysc4qp45gk5k9z6br1al9ih5ih")))

(define-public crate-bevy_ui_bits-0.2.0 (c (n "bevy_ui_bits") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_ui" "bevy_render" "bevy_asset" "bevy_text"))) (k 0)))) (h "18gvqs3l6y8mrcr369v0dx4bsikfhy85z50hnrgcvhlsdkapxh5h")))

(define-public crate-bevy_ui_bits-0.3.0 (c (n "bevy_ui_bits") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_ui" "bevy_render" "bevy_asset" "bevy_text"))) (k 0)))) (h "053r2kkz9lxw7l1b9784j9szq54cgy9n0fvqazqqy3r0vvxj7zy8")))

