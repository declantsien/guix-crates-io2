(define-module (crates-io be vy bevy_mod_bounding) #:use-module (crates-io))

(define-public crate-bevy_mod_bounding-0.1.0 (c (n "bevy_mod_bounding") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)))) (h "0bhx351ag37gr7g1yb5p4k9z8psjchvvpmw0h3ijhngs2fjnl4k2") (f (quote (("ex" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_mod_bounding-0.1.1 (c (n "bevy_mod_bounding") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)))) (h "096vkn0l5xg0giifs4271jlfmxy1pnzvc84m11bn1mkcr3z320ah") (f (quote (("ex" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

