(define-module (crates-io be vy bevycheck) #:use-module (crates-io))

(define-public crate-bevycheck-0.1.0 (c (n "bevycheck") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "13v0zysvnq32lhigjfp4mp91aki3al8wsll3fyl7g7bi4p8f027d")))

(define-public crate-bevycheck-0.1.1 (c (n "bevycheck") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cn4pwmz95dh21z44bs27fxihgnnal216p1c0ysvj944iq6bgw2s")))

(define-public crate-bevycheck-0.1.2 (c (n "bevycheck") (v "0.1.2") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1d02bihvp96fglaayfha3fx35jrycjfzv9mn12ijl7rm25q023i7")))

(define-public crate-bevycheck-0.1.3 (c (n "bevycheck") (v "0.1.3") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0i5akdqmc8j759lbacj193rb4vrm6ywyrh3abc4p3mgzrq0cdxgf")))

(define-public crate-bevycheck-0.1.4 (c (n "bevycheck") (v "0.1.4") (d (list (d (n "bevy") (r "^0.5") (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1jjqyzkbah8gcb63lrnhgdnrlp5j8z3ydxl5s6dp0wjd30k62pha")))

(define-public crate-bevycheck-0.2.0 (c (n "bevycheck") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1g0xvv8r2swb7gflbffbhvbayrqslfmz6l6m8h2x387bjaap3fvh")))

(define-public crate-bevycheck-0.3.0 (c (n "bevycheck") (v "0.3.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zmccbx8x66mg3grjzdpwrhpzr1qhls8vczq5xb86j8f1a9hvjx4")))

(define-public crate-bevycheck-0.4.0 (c (n "bevycheck") (v "0.4.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "11yx5wpfp03ddgyzgsmwddhdg1r0m91jw3z8iy9vgqkyb1yb351d")))

(define-public crate-bevycheck-0.5.2 (c (n "bevycheck") (v "0.5.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "proc-macro" "printing"))) (k 0)) (d (n "bevy") (r "^0.10") (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1bw99qvyvjfim10mraqxhzwx6x9qsgbmp0sfhbxgv3m05gkg9vrm")))

