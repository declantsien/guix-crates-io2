(define-module (crates-io be vy bevy_aseprite_ultra) #:use-module (crates-io))

(define-public crate-bevy_aseprite_ultra-0.1.0 (c (n "bevy_aseprite_ultra") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "aseprite-loader") (r "^0.2.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_sprite" "bevy_asset" "bevy_ui"))) (k 0)))) (h "02xyd0xns1xgcbpjnkbkmhjig1bcvl62vg7ly4jng42snlkmnhrk") (f (quote (("dev" "bevy/multi-threaded" "bevy/wayland" "bevy/file_watcher"))))))

(define-public crate-bevy_aseprite_ultra-0.1.1 (c (n "bevy_aseprite_ultra") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "aseprite-loader") (r "^0.2.0") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_sprite" "bevy_asset" "bevy_ui"))) (k 0)))) (h "179zp5jsdrlawcsc3vhacfim1z6q6nwcls1g4rcyzsrcw5fr4n4b") (f (quote (("dev" "bevy/multi-threaded" "bevy/wayland" "bevy/file_watcher"))))))

