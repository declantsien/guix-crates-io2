(define-module (crates-io be vy bevy_spinal) #:use-module (crates-io))

(define-public crate-bevy_spinal-0.0.1 (c (n "bevy_spinal") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.15.0") (d #t) (k 2)) (d (n "bevy_prototype_lyon") (r "^0.6.0") (d #t) (k 0)) (d (n "spinal") (r "^0.0.1") (d #t) (k 0)))) (h "1cs2hr77kmjsb98rnimcfxnqnam4fmh1hwjy26bcbqj6ygkcwyif")))

