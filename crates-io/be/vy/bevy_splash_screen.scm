(define-module (crates-io be vy bevy_splash_screen) #:use-module (crates-io))

(define-public crate-bevy_splash_screen-0.2.0 (c (n "bevy_splash_screen") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.7.0") (d #t) (k 0)))) (h "1pha3c8mk773y3hx3lbim6zl3v3b5hd6fdrghin80a34bhpna54k")))

(define-public crate-bevy_splash_screen-0.3.0 (c (n "bevy_splash_screen") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.7.0") (d #t) (k 0)))) (h "1g54jkz5qp4ipyi0brm7aff68ykm6ga6yqdd6g1r6dff4lgfjgxa")))

(define-public crate-bevy_splash_screen-0.4.3 (c (n "bevy_splash_screen") (v "0.4.3") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8") (d #t) (k 0)))) (h "1s1lx08fvwx66wzisvc1c2ijav5scjf4l8knm64yf8jzz14921as")))

(define-public crate-bevy_splash_screen-0.4.4 (c (n "bevy_splash_screen") (v "0.4.4") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy_tweening") (r "^0.8") (d #t) (k 0)))) (h "1nx951yc7hfc8p1yl2hz9jad8ci5mm6idhy7225vkm78jg46kgs5")))

(define-public crate-bevy_splash_screen-0.5.0 (c (n "bevy_splash_screen") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("multi-threaded" "bevy_asset" "bevy_winit" "bevy_render" "bevy_sprite" "png"))) (k 2)) (d (n "bevy_tweening") (r "^0.9") (d #t) (k 0)))) (h "0p7mxw0nraxwwfw4arfxng26hk0x66w0aadh07rj39rgq8h3b1y6") (f (quote (("dev" "bevy/bevy_asset" "bevy/bevy_scene" "bevy/bevy_winit" "bevy/bevy_core_pipeline" "bevy/bevy_render" "bevy/bevy_sprite" "bevy/bevy_text" "bevy/bevy_ui" "bevy/multi-threaded" "bevy/png" "bevy/ktx2" "bevy/x11" "bevy/bevy_gizmos" "bevy/default_font") ("default"))))))

(define-public crate-bevy_splash_screen-0.6.0 (c (n "bevy_splash_screen") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("multi-threaded" "bevy_asset" "bevy_winit" "bevy_render" "bevy_sprite" "png"))) (k 2)) (d (n "bevy_tweening") (r "^0.10") (d #t) (k 0)))) (h "0bap2a0gzgbv9xz9dvjk43h0n8naly7v3g7ggl27sx32nsh86jc6") (f (quote (("dev" "bevy/bevy_asset" "bevy/bevy_scene" "bevy/bevy_winit" "bevy/bevy_core_pipeline" "bevy/bevy_render" "bevy/bevy_sprite" "bevy/bevy_text" "bevy/bevy_ui" "bevy/multi-threaded" "bevy/png" "bevy/ktx2" "bevy/x11" "bevy/bevy_gizmos" "bevy/default_font") ("default"))))))

