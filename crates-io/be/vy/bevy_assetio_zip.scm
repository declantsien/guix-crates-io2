(define-module (crates-io be vy bevy_assetio_zip) #:use-module (crates-io))

(define-public crate-bevy_assetio_zip-0.1.0 (c (n "bevy_assetio_zip") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (d #t) (k 0)) (d (n "bevy_assetio_zip_bundler") (r "^0.1.0") (d #t) (k 1)) (d (n "xorio") (r "^0.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "1qffi3zizydhb8gir62p607jbccpg5g0999krwmvpsbj9zfqx639") (f (quote (("default") ("bevy-unstable"))))))

