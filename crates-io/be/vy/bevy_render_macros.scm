(define-module (crates-io be vy bevy_render_macros) #:use-module (crates-io))

(define-public crate-bevy_render_macros-0.8.0 (c (n "bevy_render_macros") (v "0.8.0") (d (list (d (n "bevy_macro_utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19c84vzyawinpawhkfqb0m9zhh7amb2mcf7lch6c46d83yjvgl77")))

(define-public crate-bevy_render_macros-0.8.1 (c (n "bevy_render_macros") (v "0.8.1") (d (list (d (n "bevy_macro_utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02s359yrb43z8lx9ijnpmz66izm5in0p0din7s1hhn2xkvf0ms8v")))

(define-public crate-bevy_render_macros-0.9.0 (c (n "bevy_render_macros") (v "0.9.0") (d (list (d (n "bevy_macro_utils") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11n1bkamsqh31rjb9g1shprfkqlckfhj9lxyf67a0hw7nd7v97g2")))

(define-public crate-bevy_render_macros-0.9.1 (c (n "bevy_render_macros") (v "0.9.1") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wmmxng1n7pah9kwcwmy7b2r2336y9f75q93lnz0bb3nfxlsxb5p")))

(define-public crate-bevy_render_macros-0.10.0 (c (n "bevy_render_macros") (v "0.10.0") (d (list (d (n "bevy_macro_utils") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a0k7qma4q1zh00bsj9c6ym9r6wpr422hmvmmg45m723707is2sp")))

(define-public crate-bevy_render_macros-0.10.1 (c (n "bevy_render_macros") (v "0.10.1") (d (list (d (n "bevy_macro_utils") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g6n0h0wbph14x5nlqwrpg6nx2qy6g7a9n6znflfdikpjm6qqbv5")))

(define-public crate-bevy_render_macros-0.11.0 (c (n "bevy_render_macros") (v "0.11.0") (d (list (d (n "bevy_macro_utils") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "02cc6iqws9ddz4pi81nfpj365lydd9rmg49w3rg4yr36z4vxki07")))

(define-public crate-bevy_render_macros-0.11.1 (c (n "bevy_render_macros") (v "0.11.1") (d (list (d (n "bevy_macro_utils") (r "^0.11.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ql76hin5ffy90fdqqi4awvf3c51jhzac184nnkj7q41msvi6kzw")))

(define-public crate-bevy_render_macros-0.11.2 (c (n "bevy_render_macros") (v "0.11.2") (d (list (d (n "bevy_macro_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0d0wskx6rbvhhwd3y2wghijj2r8b12frb6yfhm5hsp98ar0yp0z2")))

(define-public crate-bevy_render_macros-0.11.3 (c (n "bevy_render_macros") (v "0.11.3") (d (list (d (n "bevy_macro_utils") (r "^0.11.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1j9n9jzyxxzqs1n3xzknrddwdkhhkf3ayigv68z3cwxc19s8rl0b")))

(define-public crate-bevy_render_macros-0.12.0 (c (n "bevy_render_macros") (v "0.12.0") (d (list (d (n "bevy_macro_utils") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1rspjs70k8svpdk1215k94dwgsicbjlrmz30jir4mydz18yhrc1m")))

(define-public crate-bevy_render_macros-0.12.1 (c (n "bevy_render_macros") (v "0.12.1") (d (list (d (n "bevy_macro_utils") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1skbh9smyhdkbnb70ik20rahylsmh6qylk0cxkmbwzqybby6pn34")))

(define-public crate-bevy_render_macros-0.13.0 (c (n "bevy_render_macros") (v "0.13.0") (d (list (d (n "bevy_macro_utils") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ak64zqb908i1fxhicad7ixnz4igzrfhbhz27gvbfnrpa2dxk9ja")))

(define-public crate-bevy_render_macros-0.13.1 (c (n "bevy_render_macros") (v "0.13.1") (d (list (d (n "bevy_macro_utils") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0lah8rikvdqxg4qzh00w215ffzr4s2nzrwdfl1w5rdd1wy4immpz")))

(define-public crate-bevy_render_macros-0.13.2 (c (n "bevy_render_macros") (v "0.13.2") (d (list (d (n "bevy_macro_utils") (r "^0.13.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "047jljc6dv6qsk8096dr29lih02kzcgkrm5p5643fvkqdr08h2vj")))

