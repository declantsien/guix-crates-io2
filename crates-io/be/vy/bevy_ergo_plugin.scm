(define-module (crates-io be vy bevy_ergo_plugin) #:use-module (crates-io))

(define-public crate-bevy_ergo_plugin-0.1.0 (c (n "bevy_ergo_plugin") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "13779ikhc4hy8i22i95iyw789s1zx5ndzfby64fawmd3hga6p5yc")))

(define-public crate-bevy_ergo_plugin-0.1.1 (c (n "bevy_ergo_plugin") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0fn84l07pxclkza97g062f8kqnk4lhxmlhamhidlnigrv9q6bilx")))

(define-public crate-bevy_ergo_plugin-0.2.0 (c (n "bevy_ergo_plugin") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "109w4s9jj35pmyy0c872vwxj3qd4k3yan3dkw7cj7wbcgm868bfv")))

(define-public crate-bevy_ergo_plugin-0.2.1 (c (n "bevy_ergo_plugin") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0jvdqwi60jri5rvmjvl1qpkys6fb635avw7f62wcasm3grxkvvgj")))

