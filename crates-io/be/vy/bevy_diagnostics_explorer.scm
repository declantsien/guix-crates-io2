(define-module (crates-io be vy bevy_diagnostics_explorer) #:use-module (crates-io))

(define-public crate-bevy_diagnostics_explorer-0.1.0 (c (n "bevy_diagnostics_explorer") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "bevy_app") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.10.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0iajhb2mj6lv9wh6n52cy9zaycg6av1qxhmjjws8vqjcagax7hvk")))

