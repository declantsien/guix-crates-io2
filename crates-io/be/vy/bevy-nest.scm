(define-module (crates-io be vy bevy-nest) #:use-module (crates-io))

(define-public crate-bevy-nest-0.1.0 (c (n "bevy-nest") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("io-util" "net" "rt-multi-thread"))) (d #t) (k 0)))) (h "0z0dq0j8fwkl185zcp7pfgq2vxxrqfaa117hrsvcshp0k477gpr2")))

(define-public crate-bevy-nest-0.1.1 (c (n "bevy-nest") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("io-util" "net" "rt-multi-thread"))) (d #t) (k 0)))) (h "0rcmi6nf12bbk797igghv5dzaad2wghfbr7b7c2qdr6i5rwk47wa")))

(define-public crate-bevy-nest-0.2.0 (c (n "bevy-nest") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "dashmap") (r "^5.4") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("io-util" "net" "rt-multi-thread"))) (d #t) (k 0)))) (h "1n3gz7yl016nhx6icwfv516h70j465scamqlfmzgzrbi6glibslj")))

(define-public crate-bevy-nest-0.3.0 (c (n "bevy-nest") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("io-util" "net" "rt-multi-thread"))) (d #t) (k 0)))) (h "0rc3q3an9lqa8z1qdvhc6qfgbvrdcjkm7y9la4qjda1krhryb6s9")))

