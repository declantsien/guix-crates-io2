(define-module (crates-io be vy bevy-undo) #:use-module (crates-io))

(define-public crate-bevy-undo-0.1.0 (c (n "bevy-undo") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0q8v9rjajsj763jchfa5lpihmrk9rdic812isx0qxykhmkghawn9") (f (quote (("tween" "bevy_tweening") ("default"))))))

(define-public crate-bevy-undo-0.1.1 (c (n "bevy-undo") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "0czm0c9mj9i2hqhgpj79mbnsk2m4pm77isx77z1hy0r1iczdjwvr") (f (quote (("tween" "bevy_tweening") ("default"))))))

(define-public crate-bevy-undo-0.1.2 (c (n "bevy-undo") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "bevy_tweening") (r "^0.8.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1vx0brw4m110mfcsb9yqrl1adj4rvq4rf7dw6bhvnqbrdbawpsjr") (f (quote (("tween" "bevy_tweening") ("default"))))))

(define-public crate-bevy-undo-0.1.3 (c (n "bevy-undo") (v "0.1.3") (h "1qgjnkbdkwz9n5xhhwxifgp6g4i2cf1hw1ng19i58p19vilxdnxs")))

