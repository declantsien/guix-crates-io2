(define-module (crates-io be vy bevy_burn) #:use-module (crates-io))

(define-public crate-bevy_burn-0.1.0 (c (n "bevy_burn") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_core_pipeline" "bevy_render" "bevy_winit"))) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Document" "Element" "HtmlElement" "Location" "Node" "Window"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.17.1") (d #t) (k 0)))) (h "01z50pc9dq9a3hw2mgjyxp1annh19xnxvvdda2fr7fizi7396c2g")))

