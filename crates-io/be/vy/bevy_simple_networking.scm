(define-module (crates-io be vy bevy_simple_networking) #:use-module (crates-io))

(define-public crate-bevy_simple_networking-0.1.0 (c (n "bevy_simple_networking") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "09dhdyb1x8isv97qhbbip896iincn7zwvfa15042z0d7y1jia544")))

(define-public crate-bevy_simple_networking-0.1.1 (c (n "bevy_simple_networking") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "07yzkwl0y2g1fcpjmwjz45kq3mghzwb1lkm313vlhzgw0g31iy9l")))

(define-public crate-bevy_simple_networking-0.1.2 (c (n "bevy_simple_networking") (v "0.1.2") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "08dgzz5m2dp1bypnmqpsag07r7kjzvxjz2hvrzp72bnks7wwcij9")))

(define-public crate-bevy_simple_networking-0.2.0 (c (n "bevy_simple_networking") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "1pb0j82b1snczrircngv7h2y0rx22xp1d71j0hvpz9p5gf6v2cl5")))

(define-public crate-bevy_simple_networking-0.3.0 (c (n "bevy_simple_networking") (v "0.3.0") (d (list (d (n "bevy") (r "^0.6") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "13i9j9pi4kpz9w07ar5qz0wcwyyfmxiwf9za5d4372dly2sfvgbj")))

