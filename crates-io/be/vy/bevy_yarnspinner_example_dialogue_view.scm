(define-module (crates-io be vy bevy_yarnspinner_example_dialogue_view) #:use-module (crates-io))

(define-public crate-bevy_yarnspinner_example_dialogue_view-0.1.0 (c (n "bevy_yarnspinner_example_dialogue_view") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_ui" "bevy_text" "bevy_render" "png" "bevy_asset"))) (k 0)) (d (n "bevy_yarnspinner") (r "^0.1") (d #t) (k 0)) (d (n "seldom_fn_plugin") (r "^0.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "16rhzjavvyqkw09zp1zqj0dla3kwwhglpd2cw9y1by3xjnk2lvag")))

(define-public crate-bevy_yarnspinner_example_dialogue_view-0.2.0 (c (n "bevy_yarnspinner_example_dialogue_view") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_text" "bevy_render" "png" "bevy_asset"))) (k 0)) (d (n "bevy_yarnspinner") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1xyh66zf51wh6ycwgjb06x7ws74gnjc2fqglyy1r8xn70ngyrw7w")))

(define-public crate-bevy_yarnspinner_example_dialogue_view-0.2.1 (c (n "bevy_yarnspinner_example_dialogue_view") (v "0.2.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_ui" "bevy_text" "bevy_render" "png" "bevy_asset"))) (k 0)) (d (n "bevy_yarnspinner") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "10bmqv0n10wzgx5ypg2mjxjq75yk21rk13q46hsl20vff65h9vvr")))

