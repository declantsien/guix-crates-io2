(define-module (crates-io be vy bevy_cursor) #:use-module (crates-io))

(define-public crate-bevy_cursor-0.1.0 (c (n "bevy_cursor") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_ui" "bevy_winit" "default_font" "png" "x11"))) (k 2)) (d (n "bevy_pancam") (r "^0.9.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (f (quote ("union"))) (d #t) (k 0)))) (h "17q76g4myqb3ffwm75kgbzsk9qnxlv98r4wwi5jmxczf76dicsj8") (f (quote (("default" "2d") ("3d") ("2d"))))))

(define-public crate-bevy_cursor-0.1.1 (c (n "bevy_cursor") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.11.0") (f (quote ("bevy_ui" "bevy_winit" "default_font" "png" "x11"))) (k 2)) (d (n "bevy_pancam") (r "^0.9.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (f (quote ("union"))) (d #t) (k 0)))) (h "1yva5530lsab5ykyf7d9x0ss49k0v7j3frnqgxvvyvczzn8in8d8") (f (quote (("default" "2d") ("3d") ("2d"))))))

(define-public crate-bevy_cursor-0.2.0 (c (n "bevy_cursor") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("bevy_ui" "bevy_winit" "default_font" "png" "x11"))) (k 2)) (d (n "smallvec") (r "^1.11.0") (f (quote ("union"))) (d #t) (k 0)))) (h "0fmixh9s7drqv4y5clsih9z133wzdi188kmp4l3h8588jhdfnaq7") (f (quote (("default" "2d") ("3d") ("2d"))))))

(define-public crate-bevy_cursor-0.3.0 (c (n "bevy_cursor") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_ui" "bevy_winit" "default_font" "png" "x11"))) (k 2)) (d (n "smallvec") (r "^1.11.0") (f (quote ("union"))) (d #t) (k 0)))) (h "11g2r9l489y8frakg7rvnkr7jzjrc268spn2s14wj0f41vdsc9kh") (f (quote (("3d") ("2d"))))))

