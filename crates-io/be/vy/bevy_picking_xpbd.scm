(define-module (crates-io be vy bevy_picking_xpbd) #:use-module (crates-io))

(define-public crate-bevy_picking_xpbd-0.18.0 (c (n "bevy_picking_xpbd") (v "0.18.0") (d (list (d (n "bevy_app") (r "^0.13") (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_math") (r "^0.13") (k 0)) (d (n "bevy_picking_core") (r "^0.18") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.13") (k 0)) (d (n "bevy_render") (r "^0.13") (k 0)) (d (n "bevy_transform") (r "^0.13") (k 0)) (d (n "bevy_utils") (r "^0.13") (k 0)) (d (n "bevy_window") (r "^0.13") (k 0)) (d (n "bevy_xpbd_3d") (r "^0.4") (d #t) (k 0)))) (h "0q2iwib0pfnkqvkz0xq74jp7r3jxvjphn3k5dzn7nc3yc6160530")))

