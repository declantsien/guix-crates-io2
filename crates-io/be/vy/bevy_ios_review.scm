(define-module (crates-io be vy bevy_ios_review) #:use-module (crates-io))

(define-public crate-bevy_ios_review-0.1.0 (c (n "bevy_ios_review") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_winit"))) (k 0)) (d (n "winit") (r "^0.29") (f (quote ("rwh_06"))) (k 0)))) (h "0qxixlk6pp0dqp0i8rp5mcj58h4hf3s8qs0qhg05m5ibglmqxx5x")))

(define-public crate-bevy_ios_review-0.1.1 (c (n "bevy_ios_review") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_winit"))) (k 0)) (d (n "winit") (r "^0.29") (f (quote ("rwh_06"))) (k 0)))) (h "0lljbdxdfdw1qsk96mcj7byfxdlb2yr26lhdsabi03lhq4pybw4y")))

