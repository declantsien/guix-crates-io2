(define-module (crates-io be vy bevy_camera_shake) #:use-module (crates-io))

(define-public crate-bevy_camera_shake-0.1.0 (c (n "bevy_camera_shake") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8.1") (d #t) (k 0)) (d (n "noise") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0zxdnhq61sxw4lrhkhzrnz8n81qsnn2hfdnlc8ayzjwxydya70pr")))

(define-public crate-bevy_camera_shake-1.0.0 (c (n "bevy_camera_shake") (v "1.0.0") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 0)) (d (n "noise") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1zjhz920wfhfc0k2jib7hbqx9yzdcjdbsyx1rlply2w84176kk3f")))

(define-public crate-bevy_camera_shake-2.0.0 (c (n "bevy_camera_shake") (v "2.0.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "noise") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0g48by7xkiaircplmanv2l843xdch8yp5vq7c1czm8rdmq3j61in")))

(define-public crate-bevy_camera_shake-3.0.0 (c (n "bevy_camera_shake") (v "3.0.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "noise") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03lcrn9nlplc88wmscxk3bp50pswp7sna3p72swn160r7svby9ja")))

