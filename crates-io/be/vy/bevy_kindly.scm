(define-module (crates-io be vy bevy_kindly) #:use-module (crates-io))

(define-public crate-bevy_kindly-0.1.0 (c (n "bevy_kindly") (v "0.1.0") (d (list (d (n "bevy_ecs") (r "0.8.*") (d #t) (k 0)))) (h "0544a53x1spjc9qiv55ngik4zyi1wpkyysaflfdfbskf2f0nhgay")))

(define-public crate-bevy_kindly-0.2.0 (c (n "bevy_kindly") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.8.*") (d #t) (k 0)) (d (n "bevy_kindly_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1jj1a0klrdxfvi2g3cm4r07nis771vz86d02n0lkplvw2b3f8qwv")))

(define-public crate-bevy_kindly-0.2.1 (c (n "bevy_kindly") (v "0.2.1") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.8.*") (d #t) (k 0)) (d (n "bevy_kindly_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1mn4aarjqxybh3gkpa7srh2grgxkj4njn3fwijqhg5s1r8d5a996")))

(define-public crate-bevy_kindly-0.3.0 (c (n "bevy_kindly") (v "0.3.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.8.*") (d #t) (k 0)) (d (n "bevy_kindly_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0hc1sgd3hh18na7kj7zkpqqk4ivqiqja1pa43gr0cxf3ppcgc4jl")))

(define-public crate-bevy_kindly-0.3.1 (c (n "bevy_kindly") (v "0.3.1") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.8.*") (d #t) (k 0)) (d (n "bevy_kindly_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1nrf94kh5gwwk9g8jfxyhfr096l0x521xq7ayg4p7r804x7y7bf9")))

(define-public crate-bevy_kindly-0.3.2 (c (n "bevy_kindly") (v "0.3.2") (d (list (d (n "bevy") (r "0.10.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.10.*") (d #t) (k 0)) (d (n "bevy_kindly_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0avs480ln1n2ciarin66mv2849ppqsjmbpjyq8bmrc2s67x6xqzv")))

(define-public crate-bevy_kindly-0.3.3 (c (n "bevy_kindly") (v "0.3.3") (d (list (d (n "bevy") (r "0.11.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.11.*") (d #t) (k 0)) (d (n "bevy_kindly_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1bv1y9pmlkvi6iy2x5pr6ircg0kzldhv8v0fwi4cmc6kv4cvyarp")))

