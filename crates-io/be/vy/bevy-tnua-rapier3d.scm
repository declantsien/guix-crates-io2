(define-module (crates-io be vy bevy-tnua-rapier3d) #:use-module (crates-io))

(define-public crate-bevy-tnua-rapier3d-0.1.0 (c (n "bevy-tnua-rapier3d") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.23") (f (quote ("dim3"))) (k 0)))) (h "10slgqswyjs35532z385nq2rjw1c1x29q3wb2g3d379m67qvn4xp")))

(define-public crate-bevy-tnua-rapier3d-0.1.1 (c (n "bevy-tnua-rapier3d") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.23") (f (quote ("dim3"))) (k 0)))) (h "0zswmf993fbwh6hzmq3vy4zrjd19vkzsyij2h2pxv6ky1h97qs92")))

(define-public crate-bevy-tnua-rapier3d-0.2.0 (c (n "bevy-tnua-rapier3d") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.24") (f (quote ("dim3"))) (k 0)))) (h "05va7ipci3bi6sa14pqj729d2mdy0y57gyn61hiskj588nkzb2fn")))

(define-public crate-bevy-tnua-rapier3d-0.3.0 (c (n "bevy-tnua-rapier3d") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.2") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.25") (f (quote ("dim3"))) (k 0)))) (h "10n3qg16bvnf0lncfqgv3h9kmcdijmbb2wyapp4bms2zqpcsz7p8")))

(define-public crate-bevy-tnua-rapier3d-0.4.0 (c (n "bevy-tnua-rapier3d") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.25") (f (quote ("dim3"))) (k 0)))) (h "064jygvahynkpkspfja6v7rk0vy8rb8w4rdjkp3imgpwhwdmnvhy")))

(define-public crate-bevy-tnua-rapier3d-0.5.0 (c (n "bevy-tnua-rapier3d") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.25") (f (quote ("dim3"))) (k 0)))) (h "0yngacry4sccavfj4x4rlmrqp4i1ly6wf4cl5vqp0fqhdg9i8dg4")))

(define-public crate-bevy-tnua-rapier3d-0.6.0 (c (n "bevy-tnua-rapier3d") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)) (d (n "bevy-tnua-physics-integration-layer") (r "^0.3") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.26") (f (quote ("dim3"))) (k 0)))) (h "1y1r973niw7if8yy3ffhfyz5c5qn234anzfgf0mivyyhmdq01as1")))

