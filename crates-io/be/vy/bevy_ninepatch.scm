(define-module (crates-io be vy bevy_ninepatch) #:use-module (crates-io))

(define-public crate-bevy_ninepatch-0.1.0 (c (n "bevy_ninepatch") (v "0.1.0") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)))) (h "13gk3jsiv7p03k48qfk2fb1aqaj44kbz204cvlb6v2k5bkv4q2yr")))

(define-public crate-bevy_ninepatch-0.1.1 (c (n "bevy_ninepatch") (v "0.1.1") (d (list (d (n "bevy") (r "^0.2.1") (d #t) (k 0)))) (h "0mlxka1fy675s3hxarl3xq414wlz4b37k1ma49jn8nrx5zv3gqmp")))

(define-public crate-bevy_ninepatch-0.1.2 (c (n "bevy_ninepatch") (v "0.1.2") (d (list (d (n "bevy") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0c5dzdb008kyib0ylgfmafcjygpssqh5mksw26pivlff71pmj7vk")))

(define-public crate-bevy_ninepatch-0.1.3 (c (n "bevy_ninepatch") (v "0.1.3") (d (list (d (n "bevy") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0w4bz8xwq8zmvyj7g1k4258xmp6l6yw89hiinrfrm9il77r67ryz")))

(define-public crate-bevy_ninepatch-0.3.0 (c (n "bevy_ninepatch") (v "0.3.0") (d (list (d (n "bevy") (r "^0.3") (f (quote ("render"))) (k 0)))) (h "0p27n8s9rz3k4r2lqbgjsh9c7fv6afbbrx0agxm4qkw7y50qpaiv")))

(define-public crate-bevy_ninepatch-0.4.0 (c (n "bevy_ninepatch") (v "0.4.0") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "199hfyqklrjgfzl37r7lfk6bb76613c8iymvb8k1izhb6g1cqfxb")))

(define-public crate-bevy_ninepatch-0.5.0 (c (n "bevy_ninepatch") (v "0.5.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)))) (h "1rsy5g3wl0sbq1y50vr7jbh9c1nbrgwhahavxh8axykmkzv0dsdx")))

(define-public crate-bevy_ninepatch-0.6.0 (c (n "bevy_ninepatch") (v "0.6.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("bevy_ui" "bevy_render" "bevy_sprite"))) (k 0)))) (h "1jfrhvdawv15ynw9ahv92pjnk77lbsp8jar6khv6ghgnl37wbm4b")))

(define-public crate-bevy_ninepatch-0.7.0 (c (n "bevy_ninepatch") (v "0.7.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("bevy_ui" "bevy_render" "bevy_sprite"))) (k 0)))) (h "1g84yrjadpzl5bvvvafmjhklp8fhhl98vkj935129hnmc8i2wqcy") (f (quote (("examples" "bevy/bevy_winit" "bevy/render" "bevy/png"))))))

(define-public crate-bevy_ninepatch-0.8.0 (c (n "bevy_ninepatch") (v "0.8.0") (d (list (d (n "bevy") (r "^0.8.0") (f (quote ("bevy_ui" "bevy_render" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "1lkxa78w53kal50yrclbqfc5fsv4vw9knvn9mmkagka8zrd3askd") (f (quote (("examples" "bevy/bevy_winit" "bevy/render" "bevy/png"))))))

(define-public crate-bevy_ninepatch-0.9.0 (c (n "bevy_ninepatch") (v "0.9.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_ui" "bevy_render" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "18w1vczff22jrgy7d3z3azah0pwnggm4mxkjw0g8psji28nawawv") (f (quote (("examples" "bevy/bevy_winit" "bevy/render" "bevy/png"))))))

(define-public crate-bevy_ninepatch-0.9.1 (c (n "bevy_ninepatch") (v "0.9.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_ui" "bevy_render" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "03pgbdm6p80m0g9q02pd7rwwq440bm44k1w0zgaa7156r40p2rpd") (f (quote (("examples" "bevy/bevy_winit" "bevy/render" "bevy/png"))))))

(define-public crate-bevy_ninepatch-0.10.0 (c (n "bevy_ninepatch") (v "0.10.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_ui" "bevy_render" "bevy_sprite" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_text" "bevy_ui" "bevy_render" "bevy_sprite" "bevy_asset"))) (k 2)))) (h "0vhfklx6mva7w8rh7rklbkk505rgzk3fw0k6fzb4fq54j8f47xqf") (f (quote (("examples" "bevy/bevy_winit" "bevy/png" "bevy/bevy_core_pipeline" "bevy/bevy_text"))))))

