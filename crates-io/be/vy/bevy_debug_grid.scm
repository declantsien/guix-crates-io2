(define-module (crates-io be vy bevy_debug_grid) #:use-module (crates-io))

(define-public crate-bevy_debug_grid-0.1.0 (c (n "bevy_debug_grid") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_spectator") (r "^0.2.0") (d #t) (k 2)))) (h "1afzr01x4j2gjc5xzhirlrc71jifwcs3cnjw2n77gpdkqzrbgcgl")))

(define-public crate-bevy_debug_grid-0.1.1 (c (n "bevy_debug_grid") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy_spectator") (r "^0.2.0") (d #t) (k 2)))) (h "1960l79klicc1dmqbx0amrnqqlg4w4q8kq9jd9fs8aslxs6ay1bn")))

(define-public crate-bevy_debug_grid-0.2.0 (c (n "bevy_debug_grid") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_spectator") (r "^0.3.0") (d #t) (k 2)))) (h "0wwksn5p51l7560ncjzszxx5x5ixwy2q0dv6rsgi6aablqyfkzp7")))

(define-public crate-bevy_debug_grid-0.2.1 (c (n "bevy_debug_grid") (v "0.2.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_spectator") (r "^0.3.0") (d #t) (k 2)))) (h "1cnj7yl6dyqzar6582ln03ny36apz2kr8dvzmjvcjw7zfwb8mnvh")))

(define-public crate-bevy_debug_grid-0.3.0 (c (n "bevy_debug_grid") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_spectator") (r "^0.4") (d #t) (k 2)))) (h "0glzsyi4cw0i103kby64n92hf63cxfhpdm09a7p0v6hv417ym24a")))

(define-public crate-bevy_debug_grid-0.4.0 (c (n "bevy_debug_grid") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_spectator") (r "^0.4") (d #t) (k 2)))) (h "0729r2qwanzmwpb42y6nb05dw1fiwzazsk7ia2jkbksdxn9w2f28")))

(define-public crate-bevy_debug_grid-0.4.1 (c (n "bevy_debug_grid") (v "0.4.1") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_spectator") (r "^0.4") (d #t) (k 2)))) (h "1mmanhl9mlbws4z811r7sd4qjnw70xd90nf1gh7b7v67mywzz09d")))

(define-public crate-bevy_debug_grid-0.5.0 (c (n "bevy_debug_grid") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_pbr" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_spectator") (r "^0.5") (d #t) (k 2)))) (h "1kg5fbpr8dkg6mc0dkv2n84zhlx9djky3vcc8lgqq8jkrgza93jc")))

