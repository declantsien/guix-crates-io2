(define-module (crates-io be vy bevy_fixed_sprites) #:use-module (crates-io))

(define-public crate-bevy_fixed_sprites-0.1.0 (c (n "bevy_fixed_sprites") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0avrzbhxbm6inrzz590pyym9bbpr2w1mjwvqxakia7r7d2cygpyi")))

(define-public crate-bevy_fixed_sprites-0.1.1 (c (n "bevy_fixed_sprites") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "1pxlsmpabii243hmfd3n4ik9dq47v0rgn6rsa73lvipbzaa0v705")))

(define-public crate-bevy_fixed_sprites-0.1.2 (c (n "bevy_fixed_sprites") (v "0.1.2") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "0r8sm19x350z7ja5r5r872qlxlk3hdimwam6g3yyldc62p7waxb7")))

(define-public crate-bevy_fixed_sprites-0.1.3 (c (n "bevy_fixed_sprites") (v "0.1.3") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "1bhsl6j4fagm916v8m951wzmzbss1df0ybw2q9lwk42vdwjqx9hk")))

(define-public crate-bevy_fixed_sprites-0.1.4 (c (n "bevy_fixed_sprites") (v "0.1.4") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "1w1r2nyip7pd94mj3lfidpbfbk19lrhli3pjy0vhlqndfmn6amg1")))

(define-public crate-bevy_fixed_sprites-0.1.5 (c (n "bevy_fixed_sprites") (v "0.1.5") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "1qcr83nfm6p93hcrbny6r8faprnib3iwrazixv8181576k4sp90r")))

(define-public crate-bevy_fixed_sprites-0.2.0 (c (n "bevy_fixed_sprites") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("render" "bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)))) (h "1g72gnlfkhh9s2b718pazs2ahy5f66b467n1fbk2x53kwsmswxp4")))

