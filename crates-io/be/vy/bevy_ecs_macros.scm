(define-module (crates-io be vy bevy_ecs_macros) #:use-module (crates-io))

(define-public crate-bevy_ecs_macros-0.4.0 (c (n "bevy_ecs_macros") (v "0.4.0") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16vmzacmgigspfy8jj6xid5cgq41m6917785vlcz7kk5vn98wqlj")))

(define-public crate-bevy_ecs_macros-0.5.0 (c (n "bevy_ecs_macros") (v "0.5.0") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v5aggsdfqy6mg5x09dp52dd94c8bjmc2vn1d2vhg106jrl3yck5")))

(define-public crate-bevy_ecs_macros-0.6.0 (c (n "bevy_ecs_macros") (v "0.6.0") (d (list (d (n "bevy_macro_utils") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1abasyr5k3mcgdg40fjvcw7l39nzal1fwl9jmg4wyigandjfdsf7")))

(define-public crate-bevy_ecs_macros-0.7.0 (c (n "bevy_ecs_macros") (v "0.7.0") (d (list (d (n "bevy_macro_utils") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ihj88q6cynd31l6da4vl9i9qkqk13y62ba9fh8dh2s9j6j272bi")))

(define-public crate-bevy_ecs_macros-0.8.0 (c (n "bevy_ecs_macros") (v "0.8.0") (d (list (d (n "bevy_macro_utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "102n3z330jgwgj0j73n3bbpy0zq05xjij2hdjd05swpqfwkg3wm5")))

(define-public crate-bevy_ecs_macros-0.8.1 (c (n "bevy_ecs_macros") (v "0.8.1") (d (list (d (n "bevy_macro_utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kqzlxnmrads1is7acpmy6pb836v19l33rz8fk6brzp896gc6l6c")))

(define-public crate-bevy_ecs_macros-0.9.0 (c (n "bevy_ecs_macros") (v "0.9.0") (d (list (d (n "bevy_macro_utils") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p7fabxcwd14lzgshi81vc6x81dx399s5y70fx65bf9szgkygf0h")))

(define-public crate-bevy_ecs_macros-0.9.1 (c (n "bevy_ecs_macros") (v "0.9.1") (d (list (d (n "bevy_macro_utils") (r "^0.9.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kj24ymsyr671sin4i2jz6c9ysd50yxhblpjfjnq3dpf71ad8ny1")))

(define-public crate-bevy_ecs_macros-0.10.0 (c (n "bevy_ecs_macros") (v "0.10.0") (d (list (d (n "bevy_macro_utils") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c5fzp67a84k4llii5qj6wk69v13h8k0rwx40vhbscwm65brgrxi")))

(define-public crate-bevy_ecs_macros-0.10.1 (c (n "bevy_ecs_macros") (v "0.10.1") (d (list (d (n "bevy_macro_utils") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y3dz7h5vql04wxw6hap924aqi3vclx4nq9k33xk0yyvl0kd2zx9")))

(define-public crate-bevy_ecs_macros-0.11.0 (c (n "bevy_ecs_macros") (v "0.11.0") (d (list (d (n "bevy_macro_utils") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "149rzzqdclhqy95jwiw4svwwdmbn1a3b806kvmjf93yvmzygfpx1")))

(define-public crate-bevy_ecs_macros-0.11.1 (c (n "bevy_ecs_macros") (v "0.11.1") (d (list (d (n "bevy_macro_utils") (r "^0.11.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "126rs75lysnid1d73wvr0gw5i07fwx95rjq3adskfmjj7mdsb9sq")))

(define-public crate-bevy_ecs_macros-0.11.2 (c (n "bevy_ecs_macros") (v "0.11.2") (d (list (d (n "bevy_macro_utils") (r "^0.11.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0fvcqqx2rzcvrj2b218r2l4f5416dfw0xzayhbzvcga3ckc0yfz1")))

(define-public crate-bevy_ecs_macros-0.11.3 (c (n "bevy_ecs_macros") (v "0.11.3") (d (list (d (n "bevy_macro_utils") (r "^0.11.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "107srpy833ybrakj3vij7lfrrczndkmly49zxq451383pv1sjmvi")))

(define-public crate-bevy_ecs_macros-0.12.0 (c (n "bevy_ecs_macros") (v "0.12.0") (d (list (d (n "bevy_macro_utils") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05pcr4x8y3bn1hly2gablf3v8zj5a5ily1qmvy7ay3adgjvc4hpn")))

(define-public crate-bevy_ecs_macros-0.12.1 (c (n "bevy_ecs_macros") (v "0.12.1") (d (list (d (n "bevy_macro_utils") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1fpiwg20md6nn8nagskii84sf8fjsxffgznrvifg4ngii6j3m158")))

(define-public crate-bevy_ecs_macros-0.13.0 (c (n "bevy_ecs_macros") (v "0.13.0") (d (list (d (n "bevy_macro_utils") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0yqvq5clr841q8qpphx0ni0j4vpaqb77bpr9wy81jpvwbnvf8g4s")))

(define-public crate-bevy_ecs_macros-0.13.1 (c (n "bevy_ecs_macros") (v "0.13.1") (d (list (d (n "bevy_macro_utils") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1bykpxmpnavlqsk11x74dvdcpls5hpdp0ya9hdcyz37h01ivpnsm")))

(define-public crate-bevy_ecs_macros-0.13.2 (c (n "bevy_ecs_macros") (v "0.13.2") (d (list (d (n "bevy_macro_utils") (r "^0.13.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0nrbfc26l70248y0ylg966ga1qkiq2imxd93yps5h3j1qc352yw0")))

