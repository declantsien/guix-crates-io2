(define-module (crates-io be vy bevy_c3d) #:use-module (crates-io))

(define-public crate-bevy_c3d-0.11.0 (c (n "bevy_c3d") (v "0.11.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "c3dio") (r "^0.1") (d #t) (k 0)))) (h "1g2ppws80xcbg6skb3m9hc3gwwx72mm6ghxxvklz5qijn9hvbk21") (y #t)))

(define-public crate-bevy_c3d-0.11.1 (c (n "bevy_c3d") (v "0.11.1") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "c3dio") (r "^0.2") (d #t) (k 0)))) (h "1sc82h4nd6y4h73dm5msjcjqjqwmrfwccnzs7hr2vwvw0lbpdzy1")))

(define-public crate-bevy_c3d-0.11.2 (c (n "bevy_c3d") (v "0.11.2") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "c3dio") (r "^0.2") (d #t) (k 0)))) (h "08ns6yd5b5v7db71xsxagdhp8yxyiwjkcqlbd2f359g3nw7nsjim")))

(define-public crate-bevy_c3d-0.11.3 (c (n "bevy_c3d") (v "0.11.3") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "c3dio") (r "^0.3") (d #t) (k 0)))) (h "1phq96j3gswflnl4fax6sxcgz10m48xllpjcz1249ijy4qmn95j0")))

(define-public crate-bevy_c3d-0.11.4 (c (n "bevy_c3d") (v "0.11.4") (d (list (d (n "bevy") (r "=0.11") (d #t) (k 0)) (d (n "c3dio") (r "=0.4.0") (d #t) (k 0)))) (h "1jw1xx5hcimzs1fnf5m9j7nzxpsmm5b09ck5yfzkrdp4mz2dzyah")))

(define-public crate-bevy_c3d-0.11.5 (c (n "bevy_c3d") (v "0.11.5") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_app") (r "^0.11") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.11") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11") (d #t) (k 0)) (d (n "bevy_pbr") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.11") (d #t) (k 0)) (d (n "bevy_render") (r "^0.11") (d #t) (k 0)) (d (n "bevy_scene") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "bevy_utils") (r "^0.11") (d #t) (k 0)) (d (n "c3dio") (r "=0.5.0") (d #t) (k 0)))) (h "04clmi8z65xgi9d1dlg925rksz0fnxjggqbx8bivy29qky57qmx3")))

(define-public crate-bevy_c3d-0.12.0 (c (n "bevy_c3d") (v "0.12.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "bevy_pbr") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12") (d #t) (k 0)) (d (n "bevy_render") (r "^0.12") (d #t) (k 0)) (d (n "bevy_scene") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12") (d #t) (k 0)) (d (n "c3dio") (r "=0.5") (d #t) (k 0)))) (h "1nggxzi2ph5m1jw9xq0s9a3cjz64431m19vincarip9my6l0s5mq")))

(define-public crate-bevy_c3d-0.12.1 (c (n "bevy_c3d") (v "0.12.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "bevy_pbr") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12") (d #t) (k 0)) (d (n "bevy_render") (r "^0.12") (d #t) (k 0)) (d (n "bevy_scene") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12") (d #t) (k 0)) (d (n "c3dio") (r "^0.6.1") (d #t) (k 0)))) (h "18zsvvabnannjj33w4sd3h1189r19djzc4wbh2xjw2mwwsqlwvgy")))

(define-public crate-bevy_c3d-0.12.2 (c (n "bevy_c3d") (v "0.12.2") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.12") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12") (d #t) (k 0)) (d (n "bevy_pbr") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12") (d #t) (k 0)) (d (n "bevy_render") (r "^0.12") (d #t) (k 0)) (d (n "bevy_scene") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bevy_utils") (r "^0.12") (d #t) (k 0)) (d (n "c3dio") (r "^0.7.0") (d #t) (k 0)))) (h "0kw8dp52b8nswk44m1qjasr64rq9l1q07w24fd46qlc1xscgwsi3")))

(define-public crate-bevy_c3d-0.13.0 (c (n "bevy_c3d") (v "0.13.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_app") (r "^0.13") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "bevy_pbr") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.13") (d #t) (k 0)) (d (n "bevy_render") (r "^0.13") (d #t) (k 0)) (d (n "bevy_scene") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_utils") (r "^0.13") (d #t) (k 0)) (d (n "c3dio") (r "^0.7.0") (d #t) (k 0)))) (h "0jlhcqswp9vp6mcjna429bhbhd05ls5iswb0jj3gzphkylnc0sh0")))

(define-public crate-bevy_c3d-0.13.1 (c (n "bevy_c3d") (v "0.13.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy_app") (r "^0.13") (d #t) (k 0)) (d (n "bevy_asset") (r "^0.13") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "bevy_pbr") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.13") (d #t) (k 0)) (d (n "bevy_render") (r "^0.13") (d #t) (k 0)) (d (n "bevy_scene") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bevy_utils") (r "^0.13") (d #t) (k 0)) (d (n "c3dio") (r "^0.8") (d #t) (k 0)))) (h "1zn85abxz9g6mm4238wxjzji705d8q5ifc4c72p3j2irqrgy6fhb")))

