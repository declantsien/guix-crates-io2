(define-module (crates-io be vy bevy_fallable_derive) #:use-module (crates-io))

(define-public crate-bevy_fallable_derive-0.3.0 (c (n "bevy_fallable_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full" "fold" "visit" "extra-traits"))) (d #t) (k 0)))) (h "10s7giray3fzkgc2l9kvyd23wcgxzr9kgj86kl792bz4sr78sj5p") (y #t)))

(define-public crate-bevy_fallable_derive-0.3.1 (c (n "bevy_fallable_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full" "fold" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0kk2jfih45626fqlnmbj2cp4nqqxk5y93b7skdgvdxz4rx7qpjjq") (y #t)))

(define-public crate-bevy_fallable_derive-0.3.2 (c (n "bevy_fallable_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full" "fold" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0rjlm36l6r1jzi3wsz95n850jcfdhvn1yjmgrfzx3ybdq5rdbv76") (y #t)))

