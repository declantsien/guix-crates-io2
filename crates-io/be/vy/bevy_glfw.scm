(define-module (crates-io be vy bevy_glfw) #:use-module (crates-io))

(define-public crate-bevy_glfw-0.0.0 (c (n "bevy_glfw") (v "0.0.0") (h "1mr3gv5s8pglw21i4cmlfmapjbzq06jn78pmb50fyfw5xmi33sdq")))

(define-public crate-bevy_glfw-0.1.0 (c (n "bevy_glfw") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 2)) (d (n "glfw-bindgen") (r "^0.1") (f (quote ("wayland"))) (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "raw-window-handle") (r "^0.4") (d #t) (k 0)))) (h "10xfwilgb2xdpp4446f7qiiqxbb1zaiq267nl209dxp5jc9bv2iy")))

(define-public crate-bevy_glfw-0.1.1 (c (n "bevy_glfw") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 2)) (d (n "glfw-bindgen") (r "^0.1") (f (quote ("wayland"))) (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "raw-window-handle") (r "^0.4") (d #t) (k 0)))) (h "1x1an7mzvqypsm9d3ywg6k1sjmcwwhgkv4960gmabm61si0qdms8")))

