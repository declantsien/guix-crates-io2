(define-module (crates-io be vy bevy_ss_anim) #:use-module (crates-io))

(define-public crate-bevy_ss_anim-0.0.1 (c (n "bevy_ss_anim") (v "0.0.1") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "148frbyxzpgglg421i8fh0kldw281mb063xr4zyxqk6a0m3m1hqh")))

(define-public crate-bevy_ss_anim-0.0.2 (c (n "bevy_ss_anim") (v "0.0.2") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1lwz7dmk7a2izg1jj0qh5pbphnz5bn4db8lz38i36x15yqv5qim3")))

(define-public crate-bevy_ss_anim-0.0.3 (c (n "bevy_ss_anim") (v "0.0.3") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0yzzqbj3bafajxarzp9s6988fwwawd5g816zpnlgrh7zjgib1xf6")))

(define-public crate-bevy_ss_anim-0.0.4 (c (n "bevy_ss_anim") (v "0.0.4") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1bmrkkvry3hh1fd33ik6rz18hkij27q0spys6899diyb0snps7kn")))

(define-public crate-bevy_ss_anim-0.0.5 (c (n "bevy_ss_anim") (v "0.0.5") (d (list (d (n "bevy") (r "^0.11.2") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0639knfp93b5lj9832syq1mylb022f2gxikja35x1p1brla0nqzm")))

(define-public crate-bevy_ss_anim-0.1.0 (c (n "bevy_ss_anim") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1vb8hwqnqnjx9na5aq9175ikk64m6fij3bnygmw53yzf6dh84yzq")))

(define-public crate-bevy_ss_anim-0.1.1 (c (n "bevy_ss_anim") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1ax0f1fl0lqfr7wy8dkdkds2yk1zfcsv6vv4fwjwi2b3dbw4lr9h")))

(define-public crate-bevy_ss_anim-0.1.2 (c (n "bevy_ss_anim") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0h5x2nfdpz3damyqdx3yjvj6l2hzraaf6wd2kcifdklrrnhs4r88")))

(define-public crate-bevy_ss_anim-0.2.0 (c (n "bevy_ss_anim") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0dldh7phd94rl3famxrqvzba2jqky1lhb726l40a4bamx7w31x1w")))

(define-public crate-bevy_ss_anim-0.2.1 (c (n "bevy_ss_anim") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0s9zc1c8ds2afbw1lb5xis5jkfsvpcl9bsg3h0lix38i3yxg625x")))

(define-public crate-bevy_ss_anim-0.2.2 (c (n "bevy_ss_anim") (v "0.2.2") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "bevy_sprite"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1pc36q5rv06zxif7pvw3iwlyqg34xpgrfaa3h5spd2mb8v05k9ij")))

