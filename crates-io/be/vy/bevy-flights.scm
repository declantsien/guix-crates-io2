(define-module (crates-io be vy bevy-flights) #:use-module (crates-io))

(define-public crate-bevy-flights-0.1.0 (c (n "bevy-flights") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.10") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10") (d #t) (k 0)) (d (n "bevy_math") (r "^0.10") (d #t) (k 0)) (d (n "bevy_time") (r "^0.10") (d #t) (k 0)) (d (n "bevy_transform") (r "^0.10") (d #t) (k 0)))) (h "0655na8dzz239bsdi5jwb0xmmwxmdp9j0k9x3ivqxjqgyfmksfds")))

(define-public crate-bevy-flights-0.1.1 (c (n "bevy-flights") (v "0.1.1") (d (list (d (n "bevy_app") (r "^0.10") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10") (d #t) (k 0)) (d (n "bevy_math") (r "^0.10") (d #t) (k 0)) (d (n "bevy_time") (r "^0.10") (d #t) (k 0)) (d (n "bevy_transform") (r "^0.10") (d #t) (k 0)))) (h "1ldgcm8q3qjrpcfclxw0xcyd5pilvmz8azgmfwjhmkkkg95h5p5y") (y #t)))

(define-public crate-bevy-flights-0.1.2 (c (n "bevy-flights") (v "0.1.2") (d (list (d (n "bevy_app") (r "^0.10") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.10") (d #t) (k 0)) (d (n "bevy_math") (r "^0.10") (d #t) (k 0)) (d (n "bevy_time") (r "^0.10") (d #t) (k 0)) (d (n "bevy_transform") (r "^0.10") (d #t) (k 0)))) (h "0mcd8m7x5dcm3fsgbb4a03q1g1s7qzkhk1gjlm7f83vlalfg8d8w")))

