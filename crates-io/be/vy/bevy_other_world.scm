(define-module (crates-io be vy bevy_other_world) #:use-module (crates-io))

(define-public crate-bevy_other_world-0.1.0-alpha (c (n "bevy_other_world") (v "0.1.0-alpha") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)))) (h "02abb10w0w9p0whqfyw4ca4hqn4s0isc0a20r0jranh63f3cnvjj")))

(define-public crate-bevy_other_world-0.1.1-alpha (c (n "bevy_other_world") (v "0.1.1-alpha") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)))) (h "0vidrd4ws8s4yz31d8z4ypn942nr6dshfzza8rvlcjg7537s1wh7")))

(define-public crate-bevy_other_world-0.1.2-alpha (c (n "bevy_other_world") (v "0.1.2-alpha") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)))) (h "0kdk22h4b20kxyx7h8s5708w8lcgvhd4qjf60wmzpnjqqvn7hbjr")))

(define-public crate-bevy_other_world-0.1.3-alpha (c (n "bevy_other_world") (v "0.1.3-alpha") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)))) (h "1dckcnjgj7gbqs1dbpmyvqbhksa6fjg15habzs3gkw8vdyz7b5r1")))

