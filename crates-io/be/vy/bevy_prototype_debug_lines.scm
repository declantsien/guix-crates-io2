(define-module (crates-io be vy bevy_prototype_debug_lines) #:use-module (crates-io))

(define-public crate-bevy_prototype_debug_lines-0.1.0-alpha (c (n "bevy_prototype_debug_lines") (v "0.1.0-alpha") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "0qm4alhx46kl1gvq9h0482d36mqgwc2ywgwl41arwfyvgpag97z8") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.1.1-alpha (c (n "bevy_prototype_debug_lines") (v "0.1.1-alpha") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "1gphbrpdqr3dagvm2qwk6xvp3k75bqn1mnd5qcpmmykgdpv8la8k") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.1.2 (c (n "bevy_prototype_debug_lines") (v "0.1.2") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "0gkx2hrv465kh52p1cwb7l5vfw8zy1m67yh7sl7nnmr2l8h37d1q") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.1.3 (c (n "bevy_prototype_debug_lines") (v "0.1.3") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "07b6zf34wnxgdz5ilssa6lgr9274a7l1pay6fwgcghhlxnlgnawh") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.2.0 (c (n "bevy_prototype_debug_lines") (v "0.2.0") (d (list (d (n "bevy") (r "^0.4") (f (quote ("render"))) (k 0)))) (h "1nphymzcx87m5cbrqjc32lzz7vvmgzlgmk4fyh9aqmmmi2wgnvjz") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.2.2 (c (n "bevy_prototype_debug_lines") (v "0.2.2") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)))) (h "17ywzs0hnzxb8a29w47dlq6zsswrckkhwr36da70xa24hy0z2nfw") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.3.0 (c (n "bevy_prototype_debug_lines") (v "0.3.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)))) (h "0w1ssnaz8h3r3jfl2igf856ps66745v7lh4chdsgxx09658ng4rb") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11")))) (y #t)))

(define-public crate-bevy_prototype_debug_lines-0.3.1 (c (n "bevy_prototype_debug_lines") (v "0.3.1") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)))) (h "0qqzpir2f6511iwipnzfa1a7lj7iiq1fyh31byza6blxj9jigp9q") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.3.2 (c (n "bevy_prototype_debug_lines") (v "0.3.2") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)))) (h "1qlam1apc9pm324xyhkf2h94shzs8w3px2rmaqg3i44zixx0w8dk") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.3.3 (c (n "bevy_prototype_debug_lines") (v "0.3.3") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)))) (h "00iibfgr91ffx6fa04y0lw6rpcfk3vab0nsjnb3zvjshm17x1rbh") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.3.4 (c (n "bevy_prototype_debug_lines") (v "0.3.4") (d (list (d (n "bevy") (r "^0.5") (f (quote ("render"))) (k 0)))) (h "0zi4074p7c89qdyxlanmaylx40fq88jp7lss0j8lklk58mcgzk5b") (f (quote (("example_deps" "bevy/bevy_wgpu" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11"))))))

(define-public crate-bevy_prototype_debug_lines-0.6.0 (c (n "bevy_prototype_debug_lines") (v "0.6.0") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)))) (h "1bjm9rr5x7zcrp6qixnrgjrcbfanhlf90y0hm9lrbb0jmqrlvc3j") (f (quote (("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.6.1 (c (n "bevy_prototype_debug_lines") (v "0.6.1") (d (list (d (n "bevy") (r "^0.6") (f (quote ("render"))) (k 0)))) (h "0nabpf48yfa3jal7k70m3qlw2i9rdrz75qfbk02sbaq95pffkwi5") (f (quote (("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.7.0 (c (n "bevy_prototype_debug_lines") (v "0.7.0") (d (list (d (n "bevy") (r "^0.7") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite"))) (k 0)))) (h "0pn0019k289xd5i6jnjixn3ln14pr6jv0rj96k0dx26qfjx14sl7") (f (quote (("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.7.1 (c (n "bevy_prototype_debug_lines") (v "0.7.1") (d (list (d (n "bevy") (r "^0.7") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite"))) (k 0)))) (h "0116j4zd36v8icbasmpwvakzn73d4d8chfv948qvr5bmmh6s4nd4") (f (quote (("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.7.2 (c (n "bevy_prototype_debug_lines") (v "0.7.2") (d (list (d (n "bevy") (r "^0.7") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite"))) (k 0)))) (h "1nznr3gbligwspdqm5lj943h7zcsbnc7lghaqqjf3mvhkwg888v3") (f (quote (("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/png" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.8.0 (c (n "bevy_prototype_debug_lines") (v "0.8.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "1bq1jl389kfgx8kvbkqnp62y1rw43pjcq7db44zlc2ligmb8cd2q") (f (quote (("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/png" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.8.1 (c (n "bevy_prototype_debug_lines") (v "0.8.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "1858vydypjyxf3agv1z0sz3mvdvy8cdx70h9r07n3zvbfghjy9sy") (f (quote (("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/png" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.9.0 (c (n "bevy_prototype_debug_lines") (v "0.9.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "15m0rixaw1ll9998xkglza8v1lwffnv7si1vx33znwgrbczr77kd") (f (quote (("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/png" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.10.0 (c (n "bevy_prototype_debug_lines") (v "0.10.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "1zw7r8lr1lka02nibfmxx0kfs4m2bab57g7jb6pv2fghpavm7f9k") (f (quote (("shapes") ("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/png" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("default" "shapes") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.10.1 (c (n "bevy_prototype_debug_lines") (v "0.10.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "1lng3lgrb4a3ydbjiizcbh3fx7l7n2jr5gf6ilxlgfyarf37jh6q") (f (quote (("shapes") ("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/png" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("default" "shapes") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.10.2 (c (n "bevy_prototype_debug_lines") (v "0.10.2") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "0m2aij4mms6s6arsdf5yb8vvqa39j6bj0w8m0kikih02xwxd1cvr") (f (quote (("shapes") ("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/png" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("default" "shapes") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.11.0 (c (n "bevy_prototype_debug_lines") (v "0.11.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "1jafhmw8g5hx65f1sk65qrcpsclfx3b1n2sb744hv66kxr77a5lv") (f (quote (("shapes") ("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/png" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "3d") ("default" "shapes") ("3d"))))))

(define-public crate-bevy_prototype_debug_lines-0.11.1 (c (n "bevy_prototype_debug_lines") (v "0.11.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_pbr" "bevy_sprite" "bevy_asset"))) (k 0)))) (h "0fdi0bk19nzyqx1286rlay5wpm2ywajfz6sn0gk7q6pfbniqa5xv") (f (quote (("shapes") ("example_deps_2d" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/png" "bevy/x11") ("example_deps" "bevy/bevy_winit" "bevy/bevy_gltf" "bevy/x11" "bevy/tonemapping_luts" "bevy/ktx2" "bevy/zstd" "3d") ("default" "shapes") ("3d"))))))

