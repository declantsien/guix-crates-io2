(define-module (crates-io be vy bevy-single-variable-function-mesh) #:use-module (crates-io))

(define-public crate-bevy-single-variable-function-mesh-0.2.1 (c (n "bevy-single-variable-function-mesh") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)))) (h "02bz95d1ksbvjviwaxsz5635gncv70sb4pzzy516p48m99qp4y16")))

(define-public crate-bevy-single-variable-function-mesh-0.2.2 (c (n "bevy-single-variable-function-mesh") (v "0.2.2") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)))) (h "05xriivqmpd5xkn77vknsc18gj2hbs1c88ng09i4kqfjljws2m21")))

(define-public crate-bevy-single-variable-function-mesh-0.2.3 (c (n "bevy-single-variable-function-mesh") (v "0.2.3") (d (list (d (n "bevy") (r "^0.9") (d #t) (k 0)))) (h "19dm8xa0a6djz5a8l5hk501df055gw5j9ddamkbajd1pbwv6qhz5")))

(define-public crate-bevy-single-variable-function-mesh-0.2.4 (c (n "bevy-single-variable-function-mesh") (v "0.2.4") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 0)))) (h "16icf0xgyshq6n3ymwglhm67r20ca9ix6fvnlfrfn8hs0j1d2ra2")))

(define-public crate-bevy-single-variable-function-mesh-0.2.5 (c (n "bevy-single-variable-function-mesh") (v "0.2.5") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "1aaka60fb2baamsjwvx13p2syk1sl28nq4h56pk2aq8rspr0ain4")))

(define-public crate-bevy-single-variable-function-mesh-0.11.0 (c (n "bevy-single-variable-function-mesh") (v "0.11.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "0qhdsmp7wyf5acii8sldhbx1q748da1hd2akvv8qnakz40dash0q")))

(define-public crate-bevy-single-variable-function-mesh-0.12.0 (c (n "bevy-single-variable-function-mesh") (v "0.12.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render"))) (k 0)))) (h "1gvcvf01831i2y3aqys6g9n5iss2bbqnz8pf8smkpnvrzy0lb2mk")))

(define-public crate-bevy-single-variable-function-mesh-0.13.0 (c (n "bevy-single-variable-function-mesh") (v "0.13.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render"))) (k 0)))) (h "1x7p98xl2863d6hin57b7lnykzz4b7vi0vgvnxsja84saxk182ii")))

