(define-module (crates-io be vy bevy-trait-query-impl) #:use-module (crates-io))

(define-public crate-bevy-trait-query-impl-0.1.0 (c (n "bevy-trait-query-impl") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11856asgiwfymp3fm2d0y3vxcvxzw4raagr4slfknxbqcmrc9fbn")))

(define-public crate-bevy-trait-query-impl-0.1.1 (c (n "bevy-trait-query-impl") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cy2vdvm73l5h8w70cx9llnqa2cy8dbdq4bj3mkwpdvf13psvzza")))

(define-public crate-bevy-trait-query-impl-0.2.0 (c (n "bevy-trait-query-impl") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11i9j40iclr6k2n89n19g70wyk8piimwcwz7f6hn2691g7r9lh4l")))

(define-public crate-bevy-trait-query-impl-0.3.0 (c (n "bevy-trait-query-impl") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zqv6pvkvaq8180ihgs57rs1yzpdppbinz9svk90f8wkirz4qsax")))

(define-public crate-bevy-trait-query-impl-0.4.0 (c (n "bevy-trait-query-impl") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0j2pai7s10dn3lii52zg617a3d2cx0vamki2nswgwm28kcz5v7aa")))

(define-public crate-bevy-trait-query-impl-0.5.1 (c (n "bevy-trait-query-impl") (v "0.5.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19lxlhzy7mjnamavn297fpk8k5gsyv7wdj55ziqafxz6p7ljbc8i")))

