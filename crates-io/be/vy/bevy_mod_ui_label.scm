(define-module (crates-io be vy bevy_mod_ui_label) #:use-module (crates-io))

(define-public crate-bevy_mod_ui_label-0.1.0 (c (n "bevy_mod_ui_label") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "068fb0scx5li99c3fjg5m93sghg73hmdzyk31aaahig690mab1ry") (y #t)))

(define-public crate-bevy_mod_ui_label-0.2.0 (c (n "bevy_mod_ui_label") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0bsipd2j7r47hilw31cxvx4i4k6xaz1fp01f1rqqwx23684n6ydl") (y #t)))

(define-public crate-bevy_mod_ui_label-0.2.1 (c (n "bevy_mod_ui_label") (v "0.2.1") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "16fshg5ss7zz4az4ib1xn1avfird3sfhgf22nn4nwv3llnwv8rnn") (y #t)))

(define-public crate-bevy_mod_ui_label-0.2.2 (c (n "bevy_mod_ui_label") (v "0.2.2") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1i8ylcxf5lfkq7wcz63d65x363vjcqksyxx3p2fpgqwv6jph9fm4") (y #t)))

(define-public crate-bevy_mod_ui_label-0.2.3 (c (n "bevy_mod_ui_label") (v "0.2.3") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1808wkgixmagrpxc4gwnn6zsbn9ihw8y0b5f7mvfpzr56kg5ydsn") (y #t)))

(define-public crate-bevy_mod_ui_label-0.2.4 (c (n "bevy_mod_ui_label") (v "0.2.4") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "1a1hqpaxzp5zyc13j8kayl5illkjlsxkbyv3r2l0z0zljgx66w4d") (y #t)))

(define-public crate-bevy_mod_ui_label-0.2.5 (c (n "bevy_mod_ui_label") (v "0.2.5") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "096bg0ah515q7dggdb02hsb9jmim8fp7dcajdmjjwpywgcn2d3v3") (y #t)))

(define-public crate-bevy_mod_ui_label-0.2.6 (c (n "bevy_mod_ui_label") (v "0.2.6") (d (list (d (n "bevy") (r "^0.8") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "19lh0kffy2sn8f3w7v9m6s3am6hxh87d6rhdpw1khnksxizcdgyx") (y #t)))

