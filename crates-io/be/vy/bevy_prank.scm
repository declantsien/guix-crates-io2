(define-module (crates-io be vy bevy_prank) #:use-module (crates-io))

(define-public crate-bevy_prank-0.1.0 (c (n "bevy_prank") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "0zy3jh73xyqf7piwz2wqy136f2mnrmgnmjkxhabhr7ajy2c6br6c")))

(define-public crate-bevy_prank-0.1.1 (c (n "bevy_prank") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "0sh9wwxlvxkhjy4qjsf1r1rily7p8ask6wq46n5xb1j8ay3vgnby")))

(define-public crate-bevy_prank-0.1.2 (c (n "bevy_prank") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "1jv9lg0dp78908slb4pwsmcgslw1k2bjxag2kqlr1m00j9zms2ad")))

(define-public crate-bevy_prank-0.2.0 (c (n "bevy_prank") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "1xf6lqgsnxanp0j01nf3i4kpz8pq81pnwfiy761zk5d414c467gf")))

(define-public crate-bevy_prank-0.3.0 (c (n "bevy_prank") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "0va0ph569wnadm1n4cwib8d8alfv9kizanqa31wi8i18zpjqx88l")))

(define-public crate-bevy_prank-0.4.0 (c (n "bevy_prank") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "0grhmzspnlyncz6m6ril620db7za5cfsdazhqrz9isc8r8l8m6fh")))

(define-public crate-bevy_prank-0.4.1 (c (n "bevy_prank") (v "0.4.1") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "0j9di1qpf8ds8w1qdv5iwz8bki28w4k3f59rqkap9y3bm2jkazxs")))

(define-public crate-bevy_prank-0.5.0 (c (n "bevy_prank") (v "0.5.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "11pgnly2xibqx2jvq45bbm1cpjfdfiwl7wahrvd8c1j67d4i7bws")))

(define-public crate-bevy_prank-0.6.0 (c (n "bevy_prank") (v "0.6.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_ui" "default_font"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "15dfwrrg34z340ykqqamj279f1dvf75zic2qx094f47hcninwx9f")))

(define-public crate-bevy_prank-0.7.0 (c (n "bevy_prank") (v "0.7.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_ui" "default_font"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "17f022g431mpnbhmvczxfnsiwwhwwgq7y646lr57mbkzx1zadvv9")))

(define-public crate-bevy_prank-0.7.1 (c (n "bevy_prank") (v "0.7.1") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_ui" "default_font"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "0hpr3v307zglsgzf699izg7xkscfvsxrbrh4p241k4l2z2hvhvsb")))

(define-public crate-bevy_prank-0.8.0 (c (n "bevy_prank") (v "0.8.0") (d (list (d (n "bevy") (r "^0.11") (f (quote ("bevy_core_pipeline" "bevy_gizmos" "bevy_pbr" "bevy_ui" "default_font"))) (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "178sxn118ib96vsblil6bmljm4mr764dk0mldkxwnaig17j5xk53")))

