(define-module (crates-io be vy bevy_ui_exact_image) #:use-module (crates-io))

(define-public crate-bevy_ui_exact_image-0.1.0 (c (n "bevy_ui_exact_image") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "1gv7jrin6ljbcsgkjw9syw4kj8qaxmvi6x44dgd6j1n7a84r6lbg")))

(define-public crate-bevy_ui_exact_image-0.1.1 (c (n "bevy_ui_exact_image") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "0c6dy2b34794gz29q8y2l18lnykwm4k2xgprmlj0krxchb4kr8fv")))

(define-public crate-bevy_ui_exact_image-0.2.0 (c (n "bevy_ui_exact_image") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "1w1ahgk1d1k7kby61ancz3d4cm6hmi7wfsnykzimziq2zvjp03z7")))

(define-public crate-bevy_ui_exact_image-0.2.1 (c (n "bevy_ui_exact_image") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9") (f (quote ("bevy_asset" "render"))) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "0b6iqsd4qxvpvixvwmy3j6xfwh8icqr3q5lmibqj9hsscbxnsjn5")))

