(define-module (crates-io be vy bevy_state_tool) #:use-module (crates-io))

(define-public crate-bevy_state_tool-0.1.0 (c (n "bevy_state_tool") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b4hgnqkhj4aah1bl8zirb0rnikcz34rn0gjwpsm7amr1x22jbkx") (y #t)))

(define-public crate-bevy_state_tool-0.1.1 (c (n "bevy_state_tool") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q0dr75p6wccq1ai1dqx4gwa6rx8prxch5r6z8ama8g37i77vpfj")))

(define-public crate-bevy_state_tool-0.2.0 (c (n "bevy_state_tool") (v "0.2.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07ccp9vfr24hnpsrwp05x1wgkpj9bw3kbgjb0ybw49bj81hqhqwj")))

(define-public crate-bevy_state_tool-0.2.1 (c (n "bevy_state_tool") (v "0.2.1") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12z9g99cz4z384x14z81kazk5fnjclrpnqim95hhlbikmbrpgwsh")))

