(define-module (crates-io be vy bevy-wasm-tasks) #:use-module (crates-io))

(define-public crate-bevy-wasm-tasks-0.11.0 (c (n "bevy-wasm-tasks") (v "0.11.0") (d (list (d (n "bevy_app") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.37") (d #t) (k 0)))) (h "06p7qiqn93bwi730h2766imfv05fcn11iwxs75fm16fqdhhdp2ic")))

(define-public crate-bevy-wasm-tasks-0.11.1 (c (n "bevy-wasm-tasks") (v "0.11.1") (d (list (d (n "bevy_app") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("channel"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.37") (d #t) (k 0)))) (h "02mqzakd215m9q94j06wznh0rnssqxzg3ls382837sygj0ffm9in")))

(define-public crate-bevy-wasm-tasks-0.13.0 (c (n "bevy-wasm-tasks") (v "0.13.0") (d (list (d (n "bevy_app") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.13.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("channel"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.41") (d #t) (k 0)))) (h "1ci76w6ny1is79668g4igsg507rnh3pqx4y8k7qdxm9cvq19a5cc")))

