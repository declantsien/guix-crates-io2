(define-module (crates-io be vy bevy_math) #:use-module (crates-io))

(define-public crate-bevy_math-0.1.0 (c (n "bevy_math") (v "0.1.0") (d (list (d (n "glam") (r "^0.8.7") (f (quote ("serde"))) (d #t) (k 0)))) (h "1wb0qpkhrmzr419w5mr90pjkjin522zi710b187a3gv2k2y1psic")))

(define-public crate-bevy_math-0.1.3 (c (n "bevy_math") (v "0.1.3") (d (list (d (n "glam") (r "^0.9.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ni1n8kiv3vi7ml6k9w60blbj3r4i5j60k5kdlp6n96yqaba5mdl")))

(define-public crate-bevy_math-0.2.0 (c (n "bevy_math") (v "0.2.0") (d (list (d (n "glam") (r "^0.9.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0h6zmnz6q7qz8gkinkqg2m5j7jksm2543q7smb8yq4a35bnjv0yp")))

(define-public crate-bevy_math-0.2.1 (c (n "bevy_math") (v "0.2.1") (d (list (d (n "glam") (r "^0.9.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1qw55fxrll2ha129q2gr1sfy4rp83dqc7287ky1986sqimdp51fl")))

(define-public crate-bevy_math-0.3.0 (c (n "bevy_math") (v "0.3.0") (d (list (d (n "glam") (r "^0.9.5") (f (quote ("serde"))) (d #t) (k 0)))) (h "1lvg20db7fajnliivg2n0w186rim6c779r3ag65gd7li4r9q9zvi")))

(define-public crate-bevy_math-0.4.0 (c (n "bevy_math") (v "0.4.0") (d (list (d (n "bevy_reflect") (r "^0.4.0") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "glam") (r "^0.11.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0dcpv8kf72y273xgcisr9dkb2440jz9w6ww8xgri7jckqdnhdrnf")))

(define-public crate-bevy_math-0.5.0 (c (n "bevy_math") (v "0.5.0") (d (list (d (n "bevy_reflect") (r "^0.5.0") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "glam") (r "^0.13.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1k77fkbf2hl2d9lbrr4283li16ia7417qz6xc95mavy9rmwshdmv")))

(define-public crate-bevy_math-0.6.0 (c (n "bevy_math") (v "0.6.0") (d (list (d (n "bevy_reflect") (r "^0.6.0") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "glam") (r "^0.20.0") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)))) (h "04k1nsaf9x0vk8myg2fhih208407jisq3jjx7vzqy2pq5szrw3vb")))

(define-public crate-bevy_math-0.7.0 (c (n "bevy_math") (v "0.7.0") (d (list (d (n "bevy_reflect") (r "^0.7.0") (f (quote ("bevy"))) (d #t) (k 0)) (d (n "glam") (r "^0.20.0") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)))) (h "1wi3nj24s7nqqgzvp4c58cv786i7y44m5xgsmyxmiwhgyzq8sa10")))

(define-public crate-bevy_math-0.8.0 (c (n "bevy_math") (v "0.8.0") (d (list (d (n "glam") (r "^0.21") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)))) (h "1hmzzbmpf3mcv69nd301g8x0i96j9vn9hvkca2md622pdkl98lyv")))

(define-public crate-bevy_math-0.8.1 (c (n "bevy_math") (v "0.8.1") (d (list (d (n "glam") (r "^0.21") (f (quote ("serde" "bytemuck"))) (d #t) (k 0)))) (h "1lhl07m5cnm4acwvlzm6cxg4qddk640mcgcb85psjqg8qy82n7sk")))

(define-public crate-bevy_math-0.9.0 (c (n "bevy_math") (v "0.9.0") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jkxmjzhsdszygk6aw651wp5clljbw5am1rlirw9b5jzb9bmjr5j") (f (quote (("mint" "glam/mint")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.9.1 (c (n "bevy_math") (v "0.9.1") (d (list (d (n "glam") (r "^0.22") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1brf0snc76b197a8iwaivx3jpc5khmr8mvv2j3nhdj36nxxcfd6l") (f (quote (("mint" "glam/mint")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.10.0 (c (n "bevy_math") (v "0.10.0") (d (list (d (n "glam") (r "^0.23") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "159fgv3w5m0fdnj7153w5rhy53wjj3b2wbv2msrjvaf059nf8iay") (f (quote (("mint" "glam/mint")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.10.1 (c (n "bevy_math") (v "0.10.1") (d (list (d (n "glam") (r "^0.23") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0an46rqiqbvlab68sivspzwlqhb2w4cyfcmh1nclgsbgs04i2sns") (f (quote (("mint" "glam/mint")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.11.0 (c (n "bevy_math") (v "0.11.0") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nxgssnhh15byb0339z6lk3r6fb1kv21sy1ck7nqs2zyk1jqq215") (f (quote (("mint" "glam/mint") ("glam_assert" "glam/glam-assert")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.11.1 (c (n "bevy_math") (v "0.11.1") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cbsi9flzw9c5wlqnai39yibl04jmm09niw8jfwiblv1imh7nyd4") (f (quote (("mint" "glam/mint") ("glam_assert" "glam/glam-assert")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.11.2 (c (n "bevy_math") (v "0.11.2") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xdsiwjyfvjvzvslddx3w2cig1zhpwnhqcmid0bhaj599b22wzr6") (f (quote (("mint" "glam/mint") ("glam_assert" "glam/glam-assert")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.11.3 (c (n "bevy_math") (v "0.11.3") (d (list (d (n "glam") (r "^0.24") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0js71a72law7xz7pnn6k4d2ajagy097lzcasnk26syddzs0nla3q") (f (quote (("mint" "glam/mint") ("glam_assert" "glam/glam-assert")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.12.0 (c (n "bevy_math") (v "0.12.0") (d (list (d (n "glam") (r "^0.24.1") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1glndi2rscwfy3sgxza33jb0i8wz05m86fscf0h9qhhzi9an89vm") (f (quote (("mint" "glam/mint") ("glam_assert" "glam/glam-assert") ("debug_glam_assert" "glam/debug-glam-assert")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.12.1 (c (n "bevy_math") (v "0.12.1") (d (list (d (n "glam") (r "^0.24.1") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v7ldhxv9f1f7q1v8bdhsx211c5125bqxy3q059rr4w3cyvw5paq") (f (quote (("mint" "glam/mint") ("glam_assert" "glam/glam-assert") ("debug_glam_assert" "glam/debug-glam-assert")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde"))))))

(define-public crate-bevy_math-0.13.0 (c (n "bevy_math") (v "0.13.0") (d (list (d (n "approx") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "glam") (r "^0.25") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xmgzhylr76sz520pwki63dk1h7s7vihh2q4cmdrdlx6i8djncbg") (f (quote (("mint" "glam/mint") ("libm" "glam/libm") ("glam_assert" "glam/glam-assert") ("debug_glam_assert" "glam/debug-glam-assert")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde") ("approx" "dep:approx" "glam/approx"))))))

(define-public crate-bevy_math-0.13.1 (c (n "bevy_math") (v "0.13.1") (d (list (d (n "approx") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "glam") (r "^0.25") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s7rfaairpnr42yi13dbxgpvmjw51g2l1cfy76nzkdd06qgp4c4x") (f (quote (("mint" "glam/mint") ("libm" "glam/libm") ("glam_assert" "glam/glam-assert") ("debug_glam_assert" "glam/debug-glam-assert")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde") ("approx" "dep:approx" "glam/approx"))))))

(define-public crate-bevy_math-0.13.2 (c (n "bevy_math") (v "0.13.2") (d (list (d (n "approx") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "glam") (r "^0.25") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z430m49f5r7hb4pcs9g75f30v3g52xc0mi2fyx90bdqzwkalvgh") (f (quote (("mint" "glam/mint") ("libm" "glam/libm") ("glam_assert" "glam/glam-assert") ("debug_glam_assert" "glam/debug-glam-assert")))) (s 2) (e (quote (("serialize" "dep:serde" "glam/serde") ("approx" "dep:approx" "glam/approx"))))))

