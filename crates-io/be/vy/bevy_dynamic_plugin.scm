(define-module (crates-io be vy bevy_dynamic_plugin) #:use-module (crates-io))

(define-public crate-bevy_dynamic_plugin-0.3.0 (c (n "bevy_dynamic_plugin") (v "0.3.0") (d (list (d (n "bevy_app") (r "^0.3.0") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)))) (h "0y2s3a4cwlrr60rsv2k2fkqa8any1wly3wgjnq8qxqz1gfi74mnc")))

(define-public crate-bevy_dynamic_plugin-0.4.0 (c (n "bevy_dynamic_plugin") (v "0.4.0") (d (list (d (n "bevy_app") (r "^0.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)))) (h "1g3av6ml7vwcdv45gfa30fzbqwjmy12pc3zdkcz7g0336cbamf87")))

(define-public crate-bevy_dynamic_plugin-0.5.0 (c (n "bevy_dynamic_plugin") (v "0.5.0") (d (list (d (n "bevy_app") (r "^0.5.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1v8520w97c1i2sli60ckza9yydfqrw18mdxjv2fyi3lkzsz1jcix")))

(define-public crate-bevy_dynamic_plugin-0.6.0 (c (n "bevy_dynamic_plugin") (v "0.6.0") (d (list (d (n "bevy_app") (r "^0.6.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1v9p9s6hb6g807qd8h9sv50mnnv841arb7x8xxyyals5f17xy060")))

(define-public crate-bevy_dynamic_plugin-0.7.0 (c (n "bevy_dynamic_plugin") (v "0.7.0") (d (list (d (n "bevy_app") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0xg8fp3fylrwb3680l635cya5whp05md971n4949znq93f9b0883")))

(define-public crate-bevy_dynamic_plugin-0.8.0 (c (n "bevy_dynamic_plugin") (v "0.8.0") (d (list (d (n "bevy_app") (r "^0.8.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "09cw4a7iix0xhq2nmjxgk1cxlcg0fqr6s4f1kh1zjs4j2nbb5vbr")))

(define-public crate-bevy_dynamic_plugin-0.8.1 (c (n "bevy_dynamic_plugin") (v "0.8.1") (d (list (d (n "bevy_app") (r "^0.8.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1d4ccaw0bmxircvp41ni3cpl76naxy00041gvy0bp4bgkiapsjiv")))

(define-public crate-bevy_dynamic_plugin-0.9.0 (c (n "bevy_dynamic_plugin") (v "0.9.0") (d (list (d (n "bevy_app") (r "^0.9.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jvhcf3b7j9lzmx9h15zids5kwxvvyyx0ghcrlds0lln3azy6j68")))

(define-public crate-bevy_dynamic_plugin-0.9.1 (c (n "bevy_dynamic_plugin") (v "0.9.1") (d (list (d (n "bevy_app") (r "^0.9.1") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rw1ggh4di5gaskmdd4w09dpjzbl9rrnavd9z6xabi35xsczikjv")))

(define-public crate-bevy_dynamic_plugin-0.10.0 (c (n "bevy_dynamic_plugin") (v "0.10.0") (d (list (d (n "bevy_app") (r "^0.10.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12kg5a0kp1clk3hhm7ghi4f6pxnr0zvxd0jk1b87p77cc5v1j4k6")))

(define-public crate-bevy_dynamic_plugin-0.10.1 (c (n "bevy_dynamic_plugin") (v "0.10.1") (d (list (d (n "bevy_app") (r "^0.10.1") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14bgdqz598a9mikqcw7wv3hi17zirbhsb5c540h6m3d9gnhlpfff")))

(define-public crate-bevy_dynamic_plugin-0.11.0 (c (n "bevy_dynamic_plugin") (v "0.11.0") (d (list (d (n "bevy_app") (r "^0.11.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a16xbh16x5khcb1gbym572233l1mp22y2ndl85gzs037lqwr1pl")))

(define-public crate-bevy_dynamic_plugin-0.11.1 (c (n "bevy_dynamic_plugin") (v "0.11.1") (d (list (d (n "bevy_app") (r "^0.11.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dar6zig3qykxyjj8yxpjs854y995jcih4wnrlb6qgq5y9r9xzmi")))

(define-public crate-bevy_dynamic_plugin-0.11.2 (c (n "bevy_dynamic_plugin") (v "0.11.2") (d (list (d (n "bevy_app") (r "^0.11.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0657vsgqj4vjb96shm5x7xvy9v9pf1z0l8m2cp9vj2sc772739mw")))

(define-public crate-bevy_dynamic_plugin-0.11.3 (c (n "bevy_dynamic_plugin") (v "0.11.3") (d (list (d (n "bevy_app") (r "^0.11.3") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11ma7cbizc52pbbpzls7s35zqxa5a24jvzq6sqkb5qprl36wmfhg")))

(define-public crate-bevy_dynamic_plugin-0.12.0 (c (n "bevy_dynamic_plugin") (v "0.12.0") (d (list (d (n "bevy_app") (r "^0.12.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09izj2jlkcar9jjbprb2yn1fd7i9243lsnr565jinhf7kgv19njy")))

(define-public crate-bevy_dynamic_plugin-0.12.1 (c (n "bevy_dynamic_plugin") (v "0.12.1") (d (list (d (n "bevy_app") (r "^0.12.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hlf0wa9km1rpcz6g997ph2hak6b8axv7f8zfr10vk65mmpknqbr")))

(define-public crate-bevy_dynamic_plugin-0.13.0 (c (n "bevy_dynamic_plugin") (v "0.13.0") (d (list (d (n "bevy_app") (r "^0.13.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rczskz2qds6pkr74fvh8ys7j4lbq59yrvnvmh6rcycq67ws3kif")))

(define-public crate-bevy_dynamic_plugin-0.13.1 (c (n "bevy_dynamic_plugin") (v "0.13.1") (d (list (d (n "bevy_app") (r "^0.13.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bp6vzqyfzi87wzrbqfv4k9l925nhw4idcmr9kp74xz9zfd8acys")))

(define-public crate-bevy_dynamic_plugin-0.13.2 (c (n "bevy_dynamic_plugin") (v "0.13.2") (d (list (d (n "bevy_app") (r "^0.13.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15d0b2929d44ry4h97dwzdvfj1lqpkjjplvi022nqs55y4lp7lbr")))

