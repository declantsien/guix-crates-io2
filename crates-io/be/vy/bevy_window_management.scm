(define-module (crates-io be vy bevy_window_management) #:use-module (crates-io))

(define-public crate-bevy_window_management-0.1.0 (c (n "bevy_window_management") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "winit") (r "^0.28.7") (d #t) (k 0)) (d (n "winsafe") (r "^0.0.19") (f (quote ("shell"))) (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "0b7wv4fk8iv1g65y0l5vma0k09s7rsmfzrfgcl4gbz3nglwpm4bn") (f (quote (("default" "taskbar")))) (y #t) (s 2) (e (quote (("taskbar" "dep:winsafe"))))))

(define-public crate-bevy_window_management-0.1.1 (c (n "bevy_window_management") (v "0.1.1") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "winit") (r "^0.28.7") (d #t) (k 0)) (d (n "winsafe") (r "^0.0.19") (f (quote ("shell"))) (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "18lw3dm4zjxschkw1z7ficc60vaqjcq2yniz3v43cm3yp6yw561z") (f (quote (("default" "taskbar")))) (y #t) (s 2) (e (quote (("taskbar" "dep:winsafe"))))))

(define-public crate-bevy_window_management-0.1.2 (c (n "bevy_window_management") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11.3") (d #t) (k 0)) (d (n "winit") (r "^0.28.7") (d #t) (k 0)) (d (n "winsafe") (r "^0.0.19") (f (quote ("shell"))) (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "1bq95jw03y0pg6y1gv11cv45zjm4hk6p577b6ih955vrr51mbpls") (f (quote (("default" "taskbar")))) (s 2) (e (quote (("taskbar" "dep:winsafe"))))))

