(define-module (crates-io be e6 bee64) #:use-module (crates-io))

(define-public crate-bee64-1.0.0 (c (n "bee64") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)))) (h "07q3aj82svz59z20fqawb9fm8nnm1i5n2rqivmbjjilmd1lxcpkc") (r "1.48.0")))

