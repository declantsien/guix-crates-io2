(define-module (crates-io be bo bebop-lang) #:use-module (crates-io))

(define-public crate-bebop-lang-0.1.0 (c (n "bebop-lang") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nombytes") (r "^0.1.1") (d #t) (k 0)))) (h "0kf5f76y40av0w824ncdb17l6bvm80g9zshngmcg2v7mf9s3mcjl")))

(define-public crate-bebop-lang-0.1.1 (c (n "bebop-lang") (v "0.1.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nombytes") (r "^0.1.1") (d #t) (k 0)))) (h "026mlgmzyvkyxfzpcam6bmf2pkgvkwk0a940rixnfd4gyghfbj9s")))

(define-public crate-bebop-lang-0.1.2 (c (n "bebop-lang") (v "0.1.2") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nombytes") (r "^0.1.1") (d #t) (k 0)))) (h "11834r64lxzni1grvqfdd9v67yfpmjaj87flm3gprdi147cdc78l")))

(define-public crate-bebop-lang-0.1.3 (c (n "bebop-lang") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1cggawby5chawld50vijql4xvk44bn9gfl71bq0f85z1w5xqzx09")))

(define-public crate-bebop-lang-0.1.4 (c (n "bebop-lang") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0jfi4wb4wfyixnm6cqqm0dv2pg7jnhqzpm0fch163knsai270ffx")))

(define-public crate-bebop-lang-0.1.5 (c (n "bebop-lang") (v "0.1.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "09rwwwqfp5h2db3lby08ww1srfbrkpwgwb28l1sjga9ynmsx506h")))

(define-public crate-bebop-lang-0.1.6 (c (n "bebop-lang") (v "0.1.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0pmdzgpk4wbnp3pz6k3hphm4d8qj8mqbvj7080q0xzdmfrvwhk72")))

(define-public crate-bebop-lang-0.1.7 (c (n "bebop-lang") (v "0.1.7") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1bjrc0nr339sbxm8gr038jz20lg8mz8a0c40nir6r2n1dx89a53i")))

(define-public crate-bebop-lang-0.1.8 (c (n "bebop-lang") (v "0.1.8") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "0bcpg6vgmzpian0fdm3s2rqkqnhmpph8f4bq9g133c2yxh42vh3z")))

(define-public crate-bebop-lang-0.1.9 (c (n "bebop-lang") (v "0.1.9") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1nvgrqdizrk7pw55mah0an2g2r23aj6qrv23z4w1gjj6g3rq559m")))

(define-public crate-bebop-lang-0.1.10 (c (n "bebop-lang") (v "0.1.10") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "121l4rdp9nmxa9d0k980ds925hf4ak6vr1596dp9wldl1khkf4aw")))

(define-public crate-bebop-lang-0.1.11 (c (n "bebop-lang") (v "0.1.11") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1j3mx9ihj1v0xdk0zy5pknim8h9pi13i8p00b9316cmsga69kxix")))

(define-public crate-bebop-lang-0.1.12 (c (n "bebop-lang") (v "0.1.12") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "tiny-rng") (r "^0.2.0") (d #t) (k 0)))) (h "100805zxbcagq32f6iiczqwvlfdkixbj3rccnwf69ykfjddwhiy3")))

(define-public crate-bebop-lang-0.1.13 (c (n "bebop-lang") (v "0.1.13") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "15s9v4fphnjyasizdcjsw7rf28am46375gclj8ib26czjj78siyw")))

(define-public crate-bebop-lang-0.1.14 (c (n "bebop-lang") (v "0.1.14") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "0jcfbxakrzin9l8w68fqfr9r8964jcwkwwpvdj0vd1rwcbbwvq1m")))

(define-public crate-bebop-lang-0.1.15 (c (n "bebop-lang") (v "0.1.15") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1vk7qvzixqwk5cg6gsvwibnjksb32lfsc73c8lxkicmw76sma8a3")))

(define-public crate-bebop-lang-0.1.16 (c (n "bebop-lang") (v "0.1.16") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "11817rcxaa7qilamlim4qn49d3n1qkp0snkr812xl11jmj0qyk7k")))

(define-public crate-bebop-lang-0.1.17 (c (n "bebop-lang") (v "0.1.17") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1bpcb6vyw40mi75d6cn7a2fkk1sb0a1mgj0dnfm1mmwm5c51l6zg")))

