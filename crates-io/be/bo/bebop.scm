(define-module (crates-io be bo bebop) #:use-module (crates-io))

(define-public crate-bebop-0.1.0 (c (n "bebop") (v "0.1.0") (h "1a2hv1bbq6y9sp1qz2vhby3svnkxqgj8vrwzrwdmh8h11cjf4z42")))

(define-public crate-bebop-2.3.0-alpha (c (n "bebop") (v "2.3.0-alpha") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "02rv9ldizxqp7qzi43d541gmvkahq337znais3d4wr4m7iw1h43c") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.3.0-alpha.2 (c (n "bebop") (v "2.3.0-alpha.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0z1p4c6722fplsrilfph3smcg9pmsv5pq3aqklnb299fqdzdapia") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.3.0-alpha.3 (c (n "bebop") (v "2.3.0-alpha.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "197x6g9sbpyhjd3aishf6s0psc8mfklz1jvzgzk21h644y6mdfkn") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.3.0 (c (n "bebop") (v "2.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "18nr0af7p26mjijlrim0a97qhn4ldcqb5rw0sbz040d72z5zziyy") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.3.1 (c (n "bebop") (v "2.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "18hk2d05k25sim1lbi2q8fgbr2jb78vjl7g1n3s6kai55ra97y5c") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.0 (c (n "bebop") (v "2.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "07xdnaj0dxfinn3h8nymj4xzr48jl9lk4ajkj4nja26kmxkqk3h0") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.1 (c (n "bebop") (v "2.4.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1wqy2wfm8hwacdidwyk00rfzgw6wan9khq4hdh3i0qvm81slaywv") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.2 (c (n "bebop") (v "2.4.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1cr0pplcgy1y14f6mfa0y8l89ijbklazd22da4yj4wn3piahrpvi") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.3 (c (n "bebop") (v "2.4.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1d3jbg1a702q1rwlad9lxjn6qdr5pc3pm5b2qz7sk7mlnx6vvfj2") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.4 (c (n "bebop") (v "2.4.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "09xbvqcaivwqyzxwxnsz6fz2m4c4d62l1cjd4aqvijr5mp1g2igf") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.5 (c (n "bebop") (v "2.4.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0jmcliyfywyvdzgbljjdb9hgxc0430mv2pdi1zfi5z9wd1cvmag5") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.6 (c (n "bebop") (v "2.4.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0kw20kmhj8a6xcraa77is098mwwjnnjs71nh938n7yjma2zqzfl0") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.7 (c (n "bebop") (v "2.4.7") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "169l4p4vjwrksssy46hslrk4s4fdpxh8za2p2nw91c084bfawyph") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.8 (c (n "bebop") (v "2.4.8") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1i06p97l19f4836j99jbas2jnjxq5fsrk17hkf6k8ladrdiiibdh") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.9 (c (n "bebop") (v "2.4.9") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0nzr4zyskswd0czhwmaay1xqr150bfjlhp22xql7cjh9xplymm46") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.4.10 (c (n "bebop") (v "2.4.10") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1kd427d5508mj1c9p66gc6y3qs2y306wzc3nmyxc6fmcyyvzcnil") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.5.0 (c (n "bebop") (v "2.5.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0lpvvh47qx4hl88f9wrd880dip0pa8ix1pwqg5ng7dkal8pl9a2h") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.5.1 (c (n "bebop") (v "2.5.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1r2rwxcmdpa6f5bznghf3q4579clnrm8bw4x0zip5mn14s4mi2lm") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.5.2 (c (n "bebop") (v "2.5.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "00y8bcdnjl9s7p9nk4am9wb9rhap95b745rjq9768ch6pjfh7gl2") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.5.3 (c (n "bebop") (v "2.5.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1lz9w0vrmj3zc2ym0w5hwhndryah5m268gjq629m5rmp8c0bmrv1") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.5.4 (c (n "bebop") (v "2.5.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1jzyarvh941rmnpzrsdjv87mnykdh40xjhxrzryy55wb37kwcazk") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.6.0 (c (n "bebop") (v "2.6.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1hv2yin1jnwdm82f86c6dbybrya0xba64a30hn6fwz5s2dwb7x0y") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.6.1 (c (n "bebop") (v "2.6.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1a6kzgcy6mc97ar9d95lwjydflg7zcwr8rwc08nbmipkhp027jmn") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.6.2 (c (n "bebop") (v "2.6.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0hrxc20b6v81n6ffq58iz6lqqd2maiv9l6mia95fdzhwhhi6qc1i") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.6.3 (c (n "bebop") (v "2.6.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1bi8piha8zr28cybqrm9fkw4hr63gps5jsmddhjhrnnw43iacyr3") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.6.4 (c (n "bebop") (v "2.6.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0vkg7f2sfvxbprbk5cl7zwpy69bvrsl767h12nwvjkj203f3p91k") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.6.5 (c (n "bebop") (v "2.6.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0nbscvz6r5axp01pn155p8sfqfw03qwzvml3ns838xgbf06gk4la") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.6.6 (c (n "bebop") (v "2.6.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0n5mr5386wcii247imx3hf26p42fsxyayp1pb4n1flv17g53vj3a") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.6.7 (c (n "bebop") (v "2.6.7") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "02rbm4j45y574yzfcbirn8gw3iq67kiijj60pb694wxxylg3dpk4") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.7.0 (c (n "bebop") (v "2.7.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1r3bdq5amd102139g620bhg0338zbj7qdq4ls39rpf0jjnhz9a61") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.7.1 (c (n "bebop") (v "2.7.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0xv697mk8mwz4wvpksd6cbfasf38g65fx88d7xmcw3h75dzv0mfv") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.7.2 (c (n "bebop") (v "2.7.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "00c6hc4i4ng30rg7cr6h0gkg55vafghzx3s69kv4wsmrpmsq7gsq") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.7.3 (c (n "bebop") (v "2.7.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1z3y6ywnkani1d0d774gvj72xm35gzc4ayvqyi4bf6iwknbgfdn1") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.7.4 (c (n "bebop") (v "2.7.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1c2nqjwnmp290mfv7wk5qzdjwpmfa6yl4l9jhg4jsx6i106p3b5q") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.8.0 (c (n "bebop") (v "2.8.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0knz1wr25n9ymly0yj29pjgnc7j7xfnnamc83yq3qryi74arl8s0") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.8.1 (c (n "bebop") (v "2.8.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1381kqvmf652c5kib8gqq6d69jpsd2jmf0cb9xnhzxr065xlxn25") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.8.2 (c (n "bebop") (v "2.8.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1rgajb64d601gimdgzknc9jqljv4z2g6r6nmw9409lp5mz1p6s18") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.8.3 (c (n "bebop") (v "2.8.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0gq1zkhy3478hf90rp0kr4ffwh6nm4m7prr2hwvafz0a9p806d4b") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.8.4 (c (n "bebop") (v "2.8.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0b5cd7rs1l2vcbdphhsg7rdvqcihw4v1302fimklxl2df077x4cc") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.8.5 (c (n "bebop") (v "2.8.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0f0s2iv5rr53v9d7c4126z4c1hn026sc408vlwwwr4pf62217r8i") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.8.6 (c (n "bebop") (v "2.8.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "15gdna0aj53s8pb97ap549wxs8dbpg2kjshd1rhqkmqn202bakb7") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-2.8.7 (c (n "bebop") (v "2.8.7") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0f24h3bv65ipwcjwmf555vyw7h1m92ccnhibiypr2i0vyxjpnljx") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.0-rc.1 (c (n "bebop") (v "3.0.0-rc.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1lj2y7pqvl8fhbiv05ib8k5a41m4llpljn9ldia6k8vqh19p5q1a") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.0-rc.2 (c (n "bebop") (v "3.0.0-rc.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0dkhly5ipdhgvwqzl58aby4aircqji9pb6026j3wq0cmx8f1g5c5") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.0-rc.3 (c (n "bebop") (v "3.0.0-rc.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1chb7psf3jqrp8ikq1pyf51h1hgxgmr1mbv0jga4d7zxk7pmx0p5") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.1-beta.1 (c (n "bebop") (v "3.0.1-beta.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0f83lrkmvvm43qplvkbnaivvsravp0j6g5dqg2j1njp6caqbk3vf") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.2-beta.1 (c (n "bebop") (v "3.0.2-beta.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1nfwrv19392y8af2wfiki1vz00w7w3ikv7dsrqvrycv8qk5xmk8s") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.3-beta.1 (c (n "bebop") (v "3.0.3-beta.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "19vpqgffkvkgrclnskhzgy94xjls3ldsigcwf2lp858x5n5ssapj") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.3-beta.2 (c (n "bebop") (v "3.0.3-beta.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "093zhk106ys72s6gal0ypi5ylv6gpysdz9h7y88z5parjb9mp0pa") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.3-beta.3 (c (n "bebop") (v "3.0.3-beta.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0znxfiw4gy5zdb3m2nsafanwvqlldr376b3ji9mijdq4c4wkpfhr") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.4 (c (n "bebop") (v "3.0.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1dqh4j1mam9kvsjwvmpminzrcn5a3bl8i8zi6s1667ycnb0smk0i") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.5 (c (n "bebop") (v "3.0.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "01iwsfzfsx0a3fc6j3gc8hqn652mps8pmg10rrqhmy7283r3pnrq") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.6 (c (n "bebop") (v "3.0.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "0vwyzr6lh4jww5ikfnw83ya8jw9gv81pi72i34lyapaqacqdyzrg") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.7 (c (n "bebop") (v "3.0.7") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1nmxbc7css31smrl7sxck2z0l3lgpmillzl49lcd224cn2lcsylq") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.8 (c (n "bebop") (v "3.0.8") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1m9rxbd7161da8dvmgqyqkl596nkcn2mcjl1s086027bysgchrsf") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

(define-public crate-bebop-3.0.9 (c (n "bebop") (v "3.0.9") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1y42xz4d41m5s07rbpdzlmnc68x4ikha59b92qc9bafb31ql3bw9") (f (quote (("unchecked") ("sorted_maps" "itertools"))))))

