(define-module (crates-io be rn bern-conf) #:use-module (crates-io))

(define-public crate-bern-conf-0.1.0 (c (n "bern-conf") (v "0.1.0") (d (list (d (n "bern-conf-type") (r "^0.1.0") (d #t) (k 0)))) (h "03zm3fzadhv3d28j4608ygm5w71ibmvndf4y665kw144lpvfk7pw")))

(define-public crate-bern-conf-0.1.1 (c (n "bern-conf") (v "0.1.1") (d (list (d (n "bern-conf-type") (r "^0.1") (d #t) (k 0)))) (h "0c36isir9wvmhy1c0mvlpkiq9ipq0s15pjwj86nsgiwyikbpfsz2")))

(define-public crate-bern-conf-0.2.0 (c (n "bern-conf") (v "0.2.0") (d (list (d (n "bern-conf-type") (r "^0.2") (d #t) (k 0)) (d (n "bern-units") (r "^0.1") (d #t) (k 0)))) (h "0a6pdgfscpgrwpvl7w35hsmzs7hbxdwqjbhwc5n0z7bl021h75lf")))

