(define-module (crates-io be rn bern-test-macros) #:use-module (crates-io))

(define-public crate-bern-test-macros-0.1.0 (c (n "bern-test-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1awinc0li2prv2ky1lrw2lyydrmczlg0qzzp7c0dc8y9ga13kj6h")))

