(define-module (crates-io be rn bern-test) #:use-module (crates-io))

(define-public crate-bern-test-0.1.0 (c (n "bern-test") (v "0.1.0") (d (list (d (n "bern-test-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "066a2jpl59fr48yx7sfl9n6fr5mp4vb4bj0w8gbaxzn974y5an7m") (f (quote (("serial") ("rtt" "rtt-target") ("default" "serial" "autorun" "colored") ("colored") ("autorun"))))))

