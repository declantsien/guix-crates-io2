(define-module (crates-io be rn bern-conf-type) #:use-module (crates-io))

(define-public crate-bern-conf-type-0.1.0 (c (n "bern-conf-type") (v "0.1.0") (d (list (d (n "bern-arch") (r "^0.2.0") (d #t) (k 0)))) (h "1jdzcprjm6gd6p7imx5ba09qkhs6rs55gkx7g9hm7dripvb5xhxh")))

(define-public crate-bern-conf-type-0.1.1 (c (n "bern-conf-type") (v "0.1.1") (d (list (d (n "bern-arch") (r "^0.2") (d #t) (k 0)))) (h "07vj66m7b6al6yhnzh9wmylcjwsfy1sqzchb74xl8bhy0izfkqzm")))

(define-public crate-bern-conf-type-0.2.0 (c (n "bern-conf-type") (v "0.2.0") (d (list (d (n "bern-arch") (r "^0.3") (d #t) (k 0)) (d (n "bern-units") (r "^0.1") (d #t) (k 0)))) (h "1zfbx26hps7v0m4rysdrg3yhgia7d1bpljd3g1q3fd5a1y51l74v")))

