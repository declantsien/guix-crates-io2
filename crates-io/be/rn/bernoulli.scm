(define-module (crates-io be rn bernoulli) #:use-module (crates-io))

(define-public crate-bernoulli-0.1.0 (c (n "bernoulli") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "11dyk455z7cw9kjrzdlw5y7rp3l5d5y1bjivrbhakly9nz2qkv4j")))

