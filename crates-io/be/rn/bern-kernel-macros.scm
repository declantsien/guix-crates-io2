(define-module (crates-io be rn bern-kernel-macros) #:use-module (crates-io))

(define-public crate-bern-kernel-macros-0.2.0 (c (n "bern-kernel-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0svpwf089ms28q12kr3a63is918gx3g8p60m65nq7w5kwpbwr9va")))

(define-public crate-bern-kernel-macros-0.3.0 (c (n "bern-kernel-macros") (v "0.3.0") (d (list (d (n "bern-conf") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0xnh0vdv01535pg8xiar7s8f36lcmagf69a465dd0xriqmmrzqdw")))

