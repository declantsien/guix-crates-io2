(define-module (crates-io be nv benvolio) #:use-module (crates-io))

(define-public crate-benvolio-0.1.0 (c (n "benvolio") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "1s72jilynm53x5y4ljqb1awd06mjabdix73s89m5kny3nyird6vi")))

