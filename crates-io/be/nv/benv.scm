(define-module (crates-io be nv benv) #:use-module (crates-io))

(define-public crate-benv-0.1.0 (c (n "benv") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ipk34dffhvc9hzr6hf45q79ai4y3m27ppv7j03rjxmziaprxg2b")))

(define-public crate-benv-0.1.1 (c (n "benv") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1v9b2lw6wgsa651na02yq939dhlq18fyza3ksby0f94m8ym6wv7m")))

(define-public crate-benv-0.1.2 (c (n "benv") (v "0.1.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0vccxgffd2z7wg1mkbcdajy38fd2qznavz0ibc396kbjgjdm21a8")))

(define-public crate-benv-0.1.3 (c (n "benv") (v "0.1.3") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0pm4j2pcq067si7m2rsn19iyjda5n4z3vigpx0024wa8vkk2id33")))

