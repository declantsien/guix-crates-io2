(define-module (crates-io be ac beach) #:use-module (crates-io))

(define-public crate-beach-0.0.0 (c (n "beach") (v "0.0.0") (h "00sh5ynawrm397qpm5n6ykyzdbz23xkkifnkcpnkj1n9ljf9crg1")))

(define-public crate-beach-0.1.0 (c (n "beach") (v "0.1.0") (h "0ydh8vr5d3k3lw9cp60i6hmy1yb8s8fp6wvbjgrm3nzrrba1v8b3")))

(define-public crate-beach-0.2.0 (c (n "beach") (v "0.2.0") (h "185pwzkcnsx00pz4mfrnfd2d5nacjfj4gww4nzs2h7b2fkc2v1j1")))

(define-public crate-beach-0.2.1 (c (n "beach") (v "0.2.1") (h "0m5xj9yr6sr2ag80chwfil0jppaj0m7dwp5fmvhay8yjkhypyhd2")))

