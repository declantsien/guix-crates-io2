(define-module (crates-io be ac beach_map) #:use-module (crates-io))

(define-public crate-beach_map-0.1.0 (c (n "beach_map") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "1ahhw1q49q0s7x0wgflm2k8jxiay40xdczkabr49sffb12s2s497") (f (quote (("parallel" "rayon"))))))

(define-public crate-beach_map-0.1.1 (c (n "beach_map") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "161bbskdv1yp3lc41yw87245cd22m17ygi7aq5y9cw8gdnp8s4xi") (f (quote (("parallel" "rayon"))))))

(define-public crate-beach_map-0.1.2 (c (n "beach_map") (v "0.1.2") (d (list (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "0m95fdz5sgb7767ikahq86xnq8b7f946k4lphnbcxrq13y49ilwz") (f (quote (("parallel" "rayon"))))))

(define-public crate-beach_map-0.2.1 (c (n "beach_map") (v "0.2.1") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0gcz2brv286hb021ldgb2hz027s70d3sf5xji51s00xrx06h6n28") (f (quote (("parallel" "rayon"))))))

