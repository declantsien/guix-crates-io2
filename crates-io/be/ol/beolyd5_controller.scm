(define-module (crates-io be ol beolyd5_controller) #:use-module (crates-io))

(define-public crate-beolyd5_controller-0.1.0 (c (n "beolyd5_controller") (v "0.1.0") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)))) (h "0bi3dpww5dz60vpnjyh6a5xrldg4sprdlsh50yqnxg7sq6lp5j10") (y #t)))

(define-public crate-beolyd5_controller-0.1.1 (c (n "beolyd5_controller") (v "0.1.1") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)))) (h "14m8h2blfbv0g690lzrgz8x907zv7spk5dwmdfgw6lbw6cyhgz4i") (y #t)))

(define-public crate-beolyd5_controller-0.2.0 (c (n "beolyd5_controller") (v "0.2.0") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)))) (h "1c9qcf7wkdi5gs23mnc63dasslfxg477mbdk74cqbhbzq78h7b3j") (y #t)))

(define-public crate-beolyd5_controller-0.3.0 (c (n "beolyd5_controller") (v "0.3.0") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)))) (h "0lvfwkx8whgjylz7mdhnlkjjr06dvgqc4jz45qqnal9il2476rc6") (y #t)))

(define-public crate-beolyd5_controller-0.4.0 (c (n "beolyd5_controller") (v "0.4.0") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zwyws6m32y8ajzz6rn0dw3gmz108yjql5alqrag1q57y05w4fx8") (y #t)))

(define-public crate-beolyd5_controller-1.0.0 (c (n "beolyd5_controller") (v "1.0.0") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yz7znpm036hn8x7d60rlnwf94p12m70bcl0aa90absyl08ryzil") (y #t)))

(define-public crate-beolyd5_controller-1.0.1 (c (n "beolyd5_controller") (v "1.0.1") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vg9xy2y3isjh6d6vgbpfjh5n99vjj4bbfg08hfxnjr1zbyx8dnb")))

(define-public crate-beolyd5_controller-1.0.2 (c (n "beolyd5_controller") (v "1.0.2") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sshykvgibfc6qmcdgwmwzrzrvvvk0zjf4bj2d061cxvg48krdlr")))

