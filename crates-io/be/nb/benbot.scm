(define-module (crates-io be nb benbot) #:use-module (crates-io))

(define-public crate-benbot-0.1.0 (c (n "benbot") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1dj6b7rj3n9glgpx53g3xs5nisi99n97090h6y5iv12ca5m45wf0")))

