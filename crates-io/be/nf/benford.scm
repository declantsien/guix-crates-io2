(define-module (crates-io be nf benford) #:use-module (crates-io))

(define-public crate-benford-0.1.0 (c (n "benford") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qa9lypj1mkzygmkahnwkkin2jmm8n9xjx3rjd1f75jw1753160s")))

(define-public crate-benford-0.1.1 (c (n "benford") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19a547g3avsx92chqg5xsjxp7lfbbsy7pqf6ymzp2hls90w4j6yk")))

(define-public crate-benford-0.1.2 (c (n "benford") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help"))) (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "082piv8h3kz9xnmxjnfi9l10hj65flk5rvq6nhn46hxn492g394x")))

