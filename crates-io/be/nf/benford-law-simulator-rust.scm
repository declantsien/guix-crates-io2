(define-module (crates-io be nf benford-law-simulator-rust) #:use-module (crates-io))

(define-public crate-benford-law-simulator-rust-0.1.0 (c (n "benford-law-simulator-rust") (v "0.1.0") (d (list (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "textplots") (r ">=0.5.3, <0.6.0") (d #t) (k 0)))) (h "1jvjxmzg7x9p1f09rh1p878yy1lhayzn0czbw8x37mfgrsjami1j")))

(define-public crate-benford-law-simulator-rust-0.1.1 (c (n "benford-law-simulator-rust") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textplots") (r "^0.8") (d #t) (k 0)))) (h "0kxn2axva7jgm8v3xj4r968bfq9qqjc4f7qx6zz4s7g0asbx1k6s")))

