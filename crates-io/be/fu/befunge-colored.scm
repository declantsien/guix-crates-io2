(define-module (crates-io be fu befunge-colored) #:use-module (crates-io))

(define-public crate-befunge-colored-0.1.0 (c (n "befunge-colored") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "06vrify4d4c40mx0cf76b97v0915wj2dkfgp1h4s4ybbks9d1c9j")))

(define-public crate-befunge-colored-0.1.1 (c (n "befunge-colored") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "124ml9xjbw5crz9xh1grf557jqxjywd5m77w88zc4mjfg0v0ggas")))

(define-public crate-befunge-colored-0.1.2 (c (n "befunge-colored") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0bb0ky5nkgpakbf7n1b4dvnk2xcd3y4xbwrzb4q8vl9k1qcv0vc0")))

