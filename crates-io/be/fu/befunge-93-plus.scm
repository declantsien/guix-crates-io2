(define-module (crates-io be fu befunge-93-plus) #:use-module (crates-io))

(define-public crate-befunge-93-plus-1.0.1 (c (n "befunge-93-plus") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0zclwyzmh1n9qrxp9zpz5pq16f3vyg4qqgpwm8pjw3hanm9s8fwr")))

(define-public crate-befunge-93-plus-1.0.2 (c (n "befunge-93-plus") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0i72pq9622f4jqgvfbi9f14nxlgmjv0fzzgd10bjsmnfavf658d3")))

(define-public crate-befunge-93-plus-1.0.3 (c (n "befunge-93-plus") (v "1.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0li8avyf8xi3zp8gwrhfj5ldcdd7l2n86y847vzlsvj4cqi5cywj")))

(define-public crate-befunge-93-plus-1.0.4 (c (n "befunge-93-plus") (v "1.0.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0wbb5rni0mkq4xdk4sbwdfxac53vfr9sra4rbgbfkifin0i30m12")))

(define-public crate-befunge-93-plus-1.0.5 (c (n "befunge-93-plus") (v "1.0.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1k3vif6dsi4zck06ci43ycg7dks4jcmp0zrwpfp2j7kdvdlfcnyc")))

(define-public crate-befunge-93-plus-1.0.6 (c (n "befunge-93-plus") (v "1.0.6") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0ws6qni2k92xmv3pbgfywaa0ma9d9i1mwqbfl2c7x3zsi65dkl57")))

(define-public crate-befunge-93-plus-1.0.7 (c (n "befunge-93-plus") (v "1.0.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1w7dklr3n0vzf4y89k5anivk0xbc1nywa731fdbm4yjllrib6n1m")))

