(define-module (crates-io be vi bevis) #:use-module (crates-io))

(define-public crate-bevis-0.1.0 (c (n "bevis") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1xzrnhzf9a8rzb2k8lf7cyr4r3fvg16rwlqc3c7n176dkzn9fwmx") (f (quote (("trace") ("safe"))))))

(define-public crate-bevis-0.1.1 (c (n "bevis") (v "0.1.1") (d (list (d (n "bevis-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1q35428ghq6lc4p5dg6yf59zlj0mfxrmp0ys9ixf022a9fmvbi1b") (f (quote (("trace") ("safe") ("derive" "bevis-derive"))))))

