(define-module (crates-io be vi bevier) #:use-module (crates-io))

(define-public crate-bevier-0.1.0 (c (n "bevier") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qww0lw4kdicvk9f70ih4psfmdzdc6ggbm7ylcv1iqjx78jb9l04")))

(define-public crate-bevier-0.1.1 (c (n "bevier") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "01rbz4in9n2p2z05x6fy4dfrz6f139khxw2hdki9152nnqfi45gg")))

(define-public crate-bevier-0.1.2 (c (n "bevier") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nn75ccsdwxhzs2bpdzw7839p5l00w8hc7gzmjks2xa9798q3i95")))

(define-public crate-bevier-0.1.3 (c (n "bevier") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wx6ab2x1vqhfcdjlb3lfilc8m5w44m9sdfq8grxfnvkcxrshgj5")))

(define-public crate-bevier-0.1.4 (c (n "bevier") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lr8dbv8a6lr5hi65bcy0idvmjckqjya4csbhkbmpfnvs5w7i596")))

(define-public crate-bevier-1.0.0 (c (n "bevier") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1wzmqii1rdy2f6qpqgw78fq4m4hjbx1vdi771bsybjix9j2n0xqf")))

(define-public crate-bevier-1.1.0 (c (n "bevier") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "09ax829xaqd6b32ai5qbk8sw6vvwd3v81v3lnz6rk6z6j45p025m") (y #t)))

(define-public crate-bevier-1.1.1 (c (n "bevier") (v "1.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0pdrwafybsqg8kdr3522c5vw720jkika7f09bdrsf5rk44d3cazv")))

