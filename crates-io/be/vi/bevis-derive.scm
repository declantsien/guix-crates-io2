(define-module (crates-io be vi bevis-derive) #:use-module (crates-io))

(define-public crate-bevis-derive-0.1.0 (c (n "bevis-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lawh56lkc3ld5b7xd408difyy07rn7s3x1zjfc23c4pnnwir0zi")))

