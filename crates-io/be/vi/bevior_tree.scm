(define-module (crates-io be vi bevior_tree) #:use-module (crates-io))

(define-public crate-bevior_tree-0.1.0 (c (n "bevior_tree") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "genawaiter") (r "^0.99") (d #t) (k 0)))) (h "0lilig7mq0b1gfdys1ihrf7dn9bnmbwd4ivz8giyvi1y58fyf1jl")))

(define-public crate-bevior_tree-0.2.0 (c (n "bevior_tree") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "genawaiter") (r "^0.99") (d #t) (k 0)) (d (n "ordered-float") (r "^3.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "13f43asdmvik7nv72yxsg2g5ix9wywyw59c7hf5wskznycna0c46") (f (quote (("default" "random")))) (s 2) (e (quote (("random" "dep:rand"))))))

(define-public crate-bevior_tree-0.3.0 (c (n "bevior_tree") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "genawaiter") (r "^0.99") (d #t) (k 0)) (d (n "ordered-float") (r "^4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "11cskgrjgz4fmbp68vn4f587ai8n9zig6g8385zlb0icnms4axx0") (f (quote (("default" "random")))) (s 2) (e (quote (("random" "dep:rand"))))))

(define-public crate-bevior_tree-0.4.0 (c (n "bevior_tree") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "genawaiter") (r "^0.99") (d #t) (k 0)) (d (n "ordered-float") (r "^4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "05idnbm5am4i6m4g0k7zg9icfhvwzn6zwgq6iz011ax8vj1x1s3q") (f (quote (("default" "random")))) (s 2) (e (quote (("random" "dep:rand"))))))

(define-public crate-bevior_tree-0.5.0 (c (n "bevior_tree") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "derive-nodestate") (r "^0.5") (d #t) (k 0)) (d (n "macro-delegatenode") (r "^0.5") (d #t) (k 0)) (d (n "macro-withstate") (r "^0.5") (d #t) (k 0)) (d (n "ordered-float") (r "^4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "153if0gm6qf2bmiqwkqwhf7fx72czq6vh75b280122z0vma583nj") (f (quote (("default" "random")))) (s 2) (e (quote (("random" "dep:rand"))))))

