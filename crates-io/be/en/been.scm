(define-module (crates-io be en been) #:use-module (crates-io))

(define-public crate-been-0.1.0 (c (n "been") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "termtree") (r "^0.4.0") (d #t) (k 0)))) (h "0hv3vs3mazih42vc24mr5jvxrbvv4xihxc8j4wv9kz0amvsi1iz8")))

