(define-module (crates-io be br bebrohide) #:use-module (crates-io))

(define-public crate-bebrohide-0.1.0 (c (n "bebrohide") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "argon2") (r "^0.5.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "15nia5lc818dm28h44xkazsmh7z40crjd20zrrqxz2xax06mbk9m") (y #t)))

(define-public crate-bebrohide-0.1.1 (c (n "bebrohide") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "argon2") (r "^0.5.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y8byh7pg4dlbrkqckz1xlwwyg29bkyni6byw0n7ndhs3pfy7j6k") (y #t)))

(define-public crate-bebrohide-0.1.2 (c (n "bebrohide") (v "0.1.2") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argon2") (r "^0.5.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "19mw2280cafngzxblfn57ky312175xp121rq5v1agc4zbiqk2b7m") (y #t)))

(define-public crate-bebrohide-0.1.3 (c (n "bebrohide") (v "0.1.3") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argon2") (r "^0.5.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kzx3gd0whdl31vz8c7qhk7v8xrs2bvwfpmb7y4vchzg4ppqizcb")))

