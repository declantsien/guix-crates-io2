(define-module (crates-io be ef beef) #:use-module (crates-io))

(define-public crate-beef-0.1.0 (c (n "beef") (v "0.1.0") (h "0qr10fc8zdynki5igiwq1jsgzx01lzpwxxazhlmwc69g2f2rrhwi")))

(define-public crate-beef-0.1.1 (c (n "beef") (v "0.1.1") (h "0cgrq693n9h1czwz4rrbrjs1vnhd8yj36vsjrcim6bykayhkygkp")))

(define-public crate-beef-0.1.2 (c (n "beef") (v "0.1.2") (h "0b9l1zh9929ay28p4z7cddxsm078qg692lbjlfsyp5l1h56p475a")))

(define-public crate-beef-0.1.3 (c (n "beef") (v "0.1.3") (h "03zfdihl849gbixcp1bryz9jid5p8lmx13acp7l7ygg45zfa819c")))

(define-public crate-beef-0.1.4 (c (n "beef") (v "0.1.4") (h "111h16xqb6sa0q94ibhb6x8yqz1rx59w9kxmvviwdp1r98l3a8s5") (f (quote (("default") ("const_fn"))))))

(define-public crate-beef-0.1.5 (c (n "beef") (v "0.1.5") (h "1q0p87i7m5lybbdw9539rk4433azs68z6qc7khn536ipxs74613i") (f (quote (("default") ("const_fn"))))))

(define-public crate-beef-0.2.0 (c (n "beef") (v "0.2.0") (h "0ps3p6s714pv60yh2p5la76pq97yd9xizb59zgd0iqd7x0vwsl57") (f (quote (("default") ("const_fn"))))))

(define-public crate-beef-0.2.1 (c (n "beef") (v "0.2.1") (h "19zgach5paxlxnjb30542hhcp7439nwql38rrdd7z5gj7r8xbmy3") (f (quote (("default") ("const_fn"))))))

(define-public crate-beef-0.3.0 (c (n "beef") (v "0.3.0") (h "0pbxpwy5054jdrrfv8lcr4kaln3jd3arlr6x7vr7s5rsf8cilnry") (f (quote (("default") ("const_fn"))))))

(define-public crate-beef-0.4.0 (c (n "beef") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1qjcn620y1j66jhmil898v243bvi4nvx08wl2cmiq3vq1hw31y39") (f (quote (("impl_serde" "serde") ("default") ("const_fn"))))))

(define-public crate-beef-0.4.1 (c (n "beef") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05jidgqjbvqabww70hfl4q1849n0jsb0mjrks755z03vygnjzwa0") (f (quote (("impl_serde" "serde") ("default") ("const_fn"))))))

(define-public crate-beef-0.4.2 (c (n "beef") (v "0.4.2") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zvfkhhqcrdv4j4hg9s0b8bj26ckf6rgv9j2vsyikgvnd965sv4d") (f (quote (("impl_serde" "serde") ("default") ("const_fn"))))))

(define-public crate-beef-0.4.3 (c (n "beef") (v "0.4.3") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1yh64ixd4a26j37lcvggybhz4jyp2ddw8risjn5yb68qrd9j8knh") (f (quote (("impl_serde" "serde") ("default") ("const_fn"))))))

(define-public crate-beef-0.4.4 (c (n "beef") (v "0.4.4") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hva1rmbx2a54q4ncs8i5lbr26669wyvnya1sh3x22r0cxm64jj7") (f (quote (("impl_serde" "serde") ("default") ("const_fn"))))))

(define-public crate-beef-0.5.0 (c (n "beef") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02blba0j192l0374kqwn8rjsc8aifj43xi26v142ijpjim1f4dk7") (f (quote (("impl_serde" "serde") ("default") ("const_fn"))))))

(define-public crate-beef-0.5.1 (c (n "beef") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dp7hx3qja3y32z0w4mdjjfvdl9m6am0i38mxjhjjrr4a2ym9mdy") (f (quote (("impl_serde" "serde") ("default") ("const_fn"))))))

(define-public crate-beef-0.5.2 (c (n "beef") (v "0.5.2") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1c95lbnhld96iwwbyh5kzykbpysq0fnjfhwxa1mhap5qxgrl30is") (f (quote (("impl_serde" "serde") ("default") ("const_fn"))))))

