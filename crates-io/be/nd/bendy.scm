(define-module (crates-io be nd bendy) #:use-module (crates-io))

(define-public crate-bendy-0.0.0 (c (n "bendy") (v "0.0.0") (h "0726ni9dyfybv6apawdyxwzxbas1x0pmz5l26l5fd8as5d2aqhzm") (y #t)))

(define-public crate-bendy-0.1.0 (c (n "bendy") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "0z88dq11m70l0v609ia6qvbka6h13sxw7sy23fnmhzv9fmbwjpzp")))

(define-public crate-bendy-0.1.1 (c (n "bendy") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "003pn090l1f96lb9xd90xcql0fgyszfrfvsbg5kf9iyilgqpv0za")))

(define-public crate-bendy-0.1.2 (c (n "bendy") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "01gkqkhwlrbw05461l315dr6gbd9f3ba0z8gdr3qwyjg43xvi0hq")))

(define-public crate-bendy-0.2.0 (c (n "bendy") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "05hy7v1byq4cwvn6m7lc0s5ni414vqnjpzfczq93llx1bmz5wdh7")))

(define-public crate-bendy-0.2.1 (c (n "bendy") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "0s5y9b6yr57jgqmb8336zpc5yflah9rgmcxx25m4qqmxzvka4zgr") (f (quote (("std" "failure/std") ("default" "std"))))))

(define-public crate-bendy-0.2.2 (c (n "bendy") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)))) (h "06mqw398hifxlvscwh15rb49vf8mram1bgx5zhdx36s8dfa9mwx2") (f (quote (("std" "failure/std") ("default" "std"))))))

(define-public crate-bendy-0.3.0 (c (n "bendy") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_bytes") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "11lashdasq6vslslviipn5jz6ymyk9jjkz58xs72k12wnl698nfs") (f (quote (("std" "failure/std") ("serde" "serde_" "serde_bytes") ("default" "std"))))))

(define-public crate-bendy-0.3.1 (c (n "bendy") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_bytes") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "08d6wl3d5m7vc1lzbl09aqrri4hi16s59pw02cdsdm89nw1bvlqn") (f (quote (("std" "failure/std") ("serde" "serde_" "serde_bytes") ("default" "std"))))))

(define-public crate-bendy-0.3.2 (c (n "bendy") (v "0.3.2") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_bytes") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0wgyr221iml8vp0cvwbqjj5y42kdzz8iqyzncf063sdalijyhd8z") (f (quote (("std" "failure/std") ("serde" "serde_" "serde_bytes") ("default" "std"))))))

(define-public crate-bendy-0.3.3 (c (n "bendy") (v "0.3.3") (d (list (d (n "failure") (r "^0.1.3") (f (quote ("derive"))) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_bytes") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0n566lxb7zlgiwiff9i8i1h4zxkv4jqxlizk67jj3j5yr02f8cw1") (f (quote (("std" "failure/std") ("serde" "serde_" "serde_bytes") ("default" "std"))))))

(define-public crate-bendy-0.4.0-beta.1 (c (n "bendy") (v "0.4.0-beta.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_bytes") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "snafu") (r "^0.6.9") (k 0)))) (h "0dcdqm4nn79p1d9cdn0j76ywhpx7vnar10laf1qza1h51f4wi0cf") (f (quote (("std" "snafu/std") ("serde" "serde_" "serde_bytes") ("default" "std"))))))

(define-public crate-bendy-0.4.0-beta.2 (c (n "bendy") (v "0.4.0-beta.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_bytes") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7.1") (k 0)))) (h "1cv7icvnr4v1a5f4iyhvlb1jah9j191bj0crzm7y0ls9c7wa6xra") (f (quote (("std" "snafu/std") ("serde" "serde_" "serde_bytes") ("default" "std"))))))

