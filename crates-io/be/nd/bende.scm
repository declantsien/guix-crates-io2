(define-module (crates-io be nd bende) #:use-module (crates-io))

(define-public crate-bende-0.1.0 (c (n "bende") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1gbwq87ww1hqiyal3zwshv9vbqwpmh1z7iskfjgaxpfj6g7k83cg")))

(define-public crate-bende-0.2.1 (c (n "bende") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "0mwmfdab29sfpsp70357z8jhlql3radr6dy3bvl64v65yvr61xir")))

(define-public crate-bende-0.3.0 (c (n "bende") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "0ssbdjzl2lpwqsvz2rk7c14rfbwsrjm2cdcf9dj6b3xdj7mlg73d")))

(define-public crate-bende-0.4.0 (c (n "bende") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1ciq8qf78hsz6lipdqqxl0di1l6qaq2gb0v15aqiy9mlkr9ridhb")))

(define-public crate-bende-0.5.0 (c (n "bende") (v "0.5.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1mwsq27cmkl3nywh9zg68z9i26zvmd633bx696vysdk8572djlbx")))

(define-public crate-bende-0.5.1 (c (n "bende") (v "0.5.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "05dlq8f7rc9h6782mn2z8nf9l6vydsch9whnpf354k3r4mqzw7ib")))

(define-public crate-bende-0.5.2 (c (n "bende") (v "0.5.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1d9gpwqnmamhnvi0j8cxx2jhj26la61cdqpg2mf3rpaswydllqf6")))

(define-public crate-bende-0.5.3 (c (n "bende") (v "0.5.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1fcbc3xyfjrnc3dp6vvlwn6r85d06iph6x2wcqfs0a1c1b51m0jd")))

(define-public crate-bende-0.5.4 (c (n "bende") (v "0.5.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "04gvs2bgpmnz1afc0mq6fdsk9irrv36ksj9dbmwcm2h3zx0ghrqf")))

