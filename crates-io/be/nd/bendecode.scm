(define-module (crates-io be nd bendecode) #:use-module (crates-io))

(define-public crate-bendecode-0.1.0 (c (n "bendecode") (v "0.1.0") (h "1lhm777mchg2kh8zwvwd4r0i2fkqxiyxpgil427gjvlz75vim5fy") (y #t)))

(define-public crate-bendecode-0.1.1 (c (n "bendecode") (v "0.1.1") (h "1qfdk663b8133136kgfnrilappahmsrpw5af8b0n32v5rhka7kmf") (y #t)))

(define-public crate-bendecode-0.1.2 (c (n "bendecode") (v "0.1.2") (h "1h1q9gacdjkjhnzr6fnlwbprqbgsvfv4f38jq8kz0xcdwgf3shr9") (y #t)))

(define-public crate-bendecode-0.1.3 (c (n "bendecode") (v "0.1.3") (h "0ghrnp52rlb7gjpr8aggkbp6zllknjn3a2yrbnb6f58ajp2d5mz9")))

