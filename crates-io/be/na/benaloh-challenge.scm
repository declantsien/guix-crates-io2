(define-module (crates-io be na benaloh-challenge) #:use-module (crates-io))

(define-public crate-benaloh-challenge-0.1.0 (c (n "benaloh-challenge") (v "0.1.0") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.1.2") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "^0.5.2") (d #t) (k 0)))) (h "1y4dvz6r34ws8wqq8g6vpphs3qbjlhihkvvzaikn8kv2b7ncdzx8")))

(define-public crate-benaloh-challenge-0.6.0 (c (n "benaloh-challenge") (v "0.6.0") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.4.0") (d #t) (k 0)) (d (n "rsa") (r "^0.1.3") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "^0.6.0") (d #t) (k 0)))) (h "0wf16f6a63wff0cbn9f1qarkgxlxh4n0gg0bqb3i7236yvq9wqg3")))

(define-public crate-benaloh-challenge-0.7.0 (c (n "benaloh-challenge") (v "0.7.0") (d (list (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "rsa") (r "^0.3.0") (d #t) (k 2)) (d (n "sha2") (r "^0.9.0") (d #t) (k 2)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0ndq9ccmdr7nn76jyg3qcmh36cj9sbrj9ijqrw7602bm9sfk9wks")))

(define-public crate-benaloh-challenge-0.7.1 (c (n "benaloh-challenge") (v "0.7.1") (d (list (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "rsa") (r "^0.3.0") (d #t) (k 2)) (d (n "sha2") (r "^0.9.1") (d #t) (k 2)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "1l565c1ic1sji3d03wx6sccjwk6mrnvxlcl8qjplls1bmvjyxz29")))

(define-public crate-benaloh-challenge-0.8.0 (c (n "benaloh-challenge") (v "0.8.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (d #t) (k 0)) (d (n "rsa") (r "^0.7") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 2)))) (h "03dg6dkqi0wdvryq1idfw4iavczzrksssxyviib3k6g9f8bj28l7")))

