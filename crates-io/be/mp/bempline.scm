(define-module (crates-io be mp bempline) #:use-module (crates-io))

(define-public crate-bempline-0.1.0 (c (n "bempline") (v "0.1.0") (h "1n0hn14i2h8k5hffbd43pjljk7rb1ci42yxvj365bpmb41mszy78")))

(define-public crate-bempline-0.2.0 (c (n "bempline") (v "0.2.0") (h "1rzkc0hg0yb4wh1cs048ww46agc0d1lnk3zc8cw9f5fdikalywqp")))

(define-public crate-bempline-1.0.0 (c (n "bempline") (v "1.0.0") (h "07x3985bsa3yiz1y734z3dhaqyw6vsn1izgzkkyhybwvvk5a46zz") (y #t)))

(define-public crate-bempline-2.0.0 (c (n "bempline") (v "2.0.0") (h "0ri3rpk2ivgdz1wdbpzy74g6w3jc7hp63gc4k13na57njsnz2xpl") (y #t)))

(define-public crate-bempline-0.3.0 (c (n "bempline") (v "0.3.0") (h "0v7kk7fkpc0a8zzfxy62bz0g7h1jswg6n5jjp6hcd2kmp0cf240z")))

(define-public crate-bempline-0.4.0 (c (n "bempline") (v "0.4.0") (h "1a9rnl9jyp8pg75y6w5ifvy88rg7lkicnq0x57if811l1p7i83cn")))

(define-public crate-bempline-0.4.1 (c (n "bempline") (v "0.4.1") (h "10izyvlmi6fpyhbyn3igmxxjb49x75wql253rcygnhmkd578yd3p")))

(define-public crate-bempline-0.5.0 (c (n "bempline") (v "0.5.0") (h "1h2ycbqqbpkgcagy2i5k6972ydpl2ajpc7k4aj1hls9cqrw57gdj")))

(define-public crate-bempline-0.6.0 (c (n "bempline") (v "0.6.0") (h "0f5mh56w60ldn5fbiw714axv51xknqqdx4zhlv576pvhlhrnnyi8")))

(define-public crate-bempline-0.6.1 (c (n "bempline") (v "0.6.1") (h "03bypayghlcdmpd3fd1cxkfwa7b739yb86hdjczh804aj5n1wr6y")))

(define-public crate-bempline-0.7.0 (c (n "bempline") (v "0.7.0") (h "1074dj38cf648ygqa7hl4l6jj2b5yqa008wmwjhd2v5pr8s1rl09")))

(define-public crate-bempline-0.8.0 (c (n "bempline") (v "0.8.0") (h "1y76gw94ij6zswpv2izk9f4bkvxkssdsgyk4jr3db6i3n6r71w8w")))

(define-public crate-bempline-0.8.1 (c (n "bempline") (v "0.8.1") (h "0d1k8kklq6gvcx1c5l833m1yff29ypfiwlymgidr6xbb7ys506hs")))

