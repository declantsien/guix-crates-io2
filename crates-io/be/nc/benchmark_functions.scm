(define-module (crates-io be nc benchmark_functions) #:use-module (crates-io))

(define-public crate-benchmark_functions-0.1.0 (c (n "benchmark_functions") (v "0.1.0") (h "11z6g24p2lpdb4jg9n7qngzl928pxmv5lyarmf6gz3rw4qbcv4hq") (y #t)))

(define-public crate-benchmark_functions-0.1.1 (c (n "benchmark_functions") (v "0.1.1") (h "04z1l4gc11fwlk85yb8i40q453ak32h7n5wvvairrzn97zgcysmg") (y #t)))

(define-public crate-benchmark_functions-0.1.2 (c (n "benchmark_functions") (v "0.1.2") (h "0fmpydvqdb3hq1xjbrhxgdz982061njyxy3s6j19hl778d1cw50i") (y #t)))

(define-public crate-benchmark_functions-0.1.3 (c (n "benchmark_functions") (v "0.1.3") (h "19q672jy3901lgp8r0snpl314whyariamc6548gihysp05cqq614") (y #t)))

(define-public crate-benchmark_functions-0.1.4 (c (n "benchmark_functions") (v "0.1.4") (h "06cljmmpgl8bz5jdjk43ylgk9h2q1hyn9zbb4nlq1sk8nnljy8i1") (y #t)))

(define-public crate-benchmark_functions-0.1.5 (c (n "benchmark_functions") (v "0.1.5") (h "1m2p9q80cg4idx0f07afjv7gwvb8fh0b08116vlav9gfyym8chp6") (y #t)))

