(define-module (crates-io be nc bencode) #:use-module (crates-io))

(define-public crate-bencode-0.1.0 (c (n "bencode") (v "0.1.0") (h "0jmkhq3gmyl4ky1wgaxz8mb4xab6g9h1b91sbhkl6w6l46bs4048")))

(define-public crate-bencode-0.1.1 (c (n "bencode") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "~0.1.0") (d #t) (k 0)))) (h "1y8rc8cz5n9nx4rzlgfpnsnwzsh5v23l9ff9ibbxqp6i196wbwnj")))

(define-public crate-bencode-0.1.3 (c (n "bencode") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "0qqa4yi2av4al5qi8ldb0p0b3v39l3h50bmkvxymbh47jp1if4p9")))

(define-public crate-bencode-0.1.5 (c (n "bencode") (v "0.1.5") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "07wrlsbhsv0phk0vr550653mn0axxagyl5i4b9w3611n3p87hxk1")))

(define-public crate-bencode-0.1.6 (c (n "bencode") (v "0.1.6") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "0280dhdb5j94cdcywc8nx2qv0m68xdf8wzsk1xh0fqpk80gznkga")))

(define-public crate-bencode-0.1.7 (c (n "bencode") (v "0.1.7") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "0r7ldb9bff1xmd8ri7nykvjk3zf1g27x59cq9bcqvwvbs8ywsywi")))

(define-public crate-bencode-0.1.8 (c (n "bencode") (v "0.1.8") (d (list (d (n "rustc-serialize") (r "~0.2.0") (d #t) (k 0)))) (h "0ypvay94qkk6nr89ngwc52ahr5s953b3g2ay20f22syjn7kxdahn")))

(define-public crate-bencode-0.1.9 (c (n "bencode") (v "0.1.9") (d (list (d (n "rustc-serialize") (r "~0.3.0") (d #t) (k 0)))) (h "052nq6wwvl9wmx5aa7zb038bh4vpxgbs5kvdqfaihf7dfdm45j0k")))

(define-public crate-bencode-0.1.10 (c (n "bencode") (v "0.1.10") (d (list (d (n "rustc-serialize") (r "~0.3.0") (d #t) (k 0)))) (h "09jrsxiighsbxpbkdm9wgmjikc04sjd7qayq7l4kjsdhr0h0m1sf")))

(define-public crate-bencode-0.1.11 (c (n "bencode") (v "0.1.11") (d (list (d (n "rustc-serialize") (r "~0.3.0") (d #t) (k 0)))) (h "16lxjc5yfd005yfnvad0gdc1xfdp8vk4nfkwdj9bz4ijzbadp7y9")))

(define-public crate-bencode-0.1.12 (c (n "bencode") (v "0.1.12") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.0") (d #t) (k 0)))) (h "1ppa88vmk24klp6wx3scn27h2zn886b2v534j5ilbwzwyqf4hcc4") (f (quote (("nightly"))))))

(define-public crate-bencode-0.1.13 (c (n "bencode") (v "0.1.13") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "09kbxmb8171dd8j9q1gidn5lxwafyv7pfqzvblxhb1z082yn7r0d") (f (quote (("nightly"))))))

(define-public crate-bencode-0.1.14 (c (n "bencode") (v "0.1.14") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1mq8kkz8bc79nmzq6n71hk83na4jc19cfn2v9y5qhs9xsng8hczn") (f (quote (("nightly"))))))

(define-public crate-bencode-0.1.15 (c (n "bencode") (v "0.1.15") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "12zvr2yy3hq4hlqblxadrdvw1bd7h5cqxjwv3r54qmgprqvv9dsj") (f (quote (("nightly"))))))

(define-public crate-bencode-0.1.16 (c (n "bencode") (v "0.1.16") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jk0fp6f9qqa4j20rxy4mla53c2zpax1gxrzz2k7y3in8bwrxf3h") (f (quote (("nightly"))))))

