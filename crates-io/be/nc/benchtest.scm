(define-module (crates-io be nc benchtest) #:use-module (crates-io))

(define-public crate-benchtest-0.1.0 (c (n "benchtest") (v "0.1.0") (h "082zcijbi1rxnzym8w2hrgwrg2sw0hsv8icj0sibgsfczrr7r4gq")))

(define-public crate-benchtest-0.1.1 (c (n "benchtest") (v "0.1.1") (h "0yl7gqx4kk32rk7w0xdnprsa8h8j5h3yab4r374df2aaxb6wd5ic")))

