(define-module (crates-io be nc bench_rust) #:use-module (crates-io))

(define-public crate-bench_rust-0.1.0 (c (n "bench_rust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1qc74vyhy3szq1jgkns7p6a9542ya6biq6kgn7sqkxyxzdfsz7mx")))

(define-public crate-bench_rust-0.1.1 (c (n "bench_rust") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "00y90l3sjm4rb2if8kazlp5w2v0nccj5l20asds9snry6nnjw8k3")))

(define-public crate-bench_rust-0.1.2 (c (n "bench_rust") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0qjad4jygdqv11bpf24lddvbdk46ryrzvhr210dlyhcs9i1mw8ms")))

(define-public crate-bench_rust-0.1.3 (c (n "bench_rust") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1i2br5j6b1pj7d4vjgdbi3i5si0zqin1vqmdml3vx45pnbd8q541")))

(define-public crate-bench_rust-0.1.4 (c (n "bench_rust") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0wldfy6xrwf9dc06yfb14j3y54yi264c7diq4s5mwn7a996zcn74")))

