(define-module (crates-io be nc bencodex-rs) #:use-module (crates-io))

(define-public crate-bencodex-rs-0.0.1 (c (n "bencodex-rs") (v "0.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.4") (d #t) (k 0)))) (h "11yhg562f5zla3c9n5lcsdpdqj2flwipxwxkbdgv47fb9qk6k0xk")))

(define-public crate-bencodex-rs-0.1.0 (c (n "bencodex-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.4") (d #t) (k 0)))) (h "0fhs9ywxn7s0j5riab2jifkdvzkhi2dhf9nhb3h180z37wsiihxb")))

(define-public crate-bencodex-rs-0.2.0 (c (n "bencodex-rs") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.4") (d #t) (k 0)))) (h "14z8xs2j7c5fw703cxzlcjal52rf6x5ji2f8x3gm95fqf6kw16z9")))

(define-public crate-bencodex-rs-0.3.0 (c (n "bencodex-rs") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.4") (d #t) (k 0)))) (h "0w89afj9hpcqxqcpgv125q5dpgrm8ddxr2nhs6k4r6whshyzy48d")))

(define-public crate-bencodex-rs-0.3.1 (c (n "bencodex-rs") (v "0.3.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.4") (d #t) (k 2)))) (h "10gy56hl91xarsba26fsjnd4rgj3ajsxlc1n45zfslmjvwi5z42f")))

