(define-module (crates-io be nc bencher) #:use-module (crates-io))

(define-public crate-bencher-0.1.0 (c (n "bencher") (v "0.1.0") (h "0xlwnighcwvy20dsniqmbvk1ma80pa14wmqkmcdcmch8vghd6wgv")))

(define-public crate-bencher-0.1.1 (c (n "bencher") (v "0.1.1") (h "0k1qicscmll8qlsj2qzk5fxl9f4c4lzqfdsc7wixx1kwdb3brkpq")))

(define-public crate-bencher-0.1.2 (c (n "bencher") (v "0.1.2") (h "18pplwm7dcwf7z8ff42715abicp0h1rp9v0ark1gha4r71592f0r")))

(define-public crate-bencher-0.1.3 (c (n "bencher") (v "0.1.3") (h "0ad52vyvv7qjfb66xnvi9zxfzg5vqzqgw0j1bfjy8si6p8fxp1qn")))

(define-public crate-bencher-0.1.4 (c (n "bencher") (v "0.1.4") (h "14n9ssbhwxi9yrbn82psliw86pjhh1dcmr9yjd0b5lqda08v6s0n")))

(define-public crate-bencher-0.1.5 (c (n "bencher") (v "0.1.5") (h "1x8p2xblgqssay8cdykp5pkfc0np0jk5bs5cx4f5av097aav9zbx")))

