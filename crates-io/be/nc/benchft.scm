(define-module (crates-io be nc benchft) #:use-module (crates-io))

(define-public crate-benchft-0.1.0 (c (n "benchft") (v "0.1.0") (h "0ydv15hf76j62dvp1lmk9b8y8wwdffbin638pkxdf6v04jjx1dzj") (y #t)))

(define-public crate-benchft-0.0.0-reserved (c (n "benchft") (v "0.0.0-reserved") (h "0nsxi1iz7nshg26mghd4a0v0rgj73whv94zcj50p07ngqp0karkd")))

