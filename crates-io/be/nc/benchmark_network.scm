(define-module (crates-io be nc benchmark_network) #:use-module (crates-io))

(define-public crate-benchmark_network-0.1.0 (c (n "benchmark_network") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0x2gkkpk138wwza3hkidlav0filzph2pmg0qnz29bwmarvv22yig") (y #t)))

(define-public crate-benchmark_network-0.1.1 (c (n "benchmark_network") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "154w4x6j7rg2j4817vf9r38hg8fmf5r8izizbw9xp7s2wqzvgj8h")))

