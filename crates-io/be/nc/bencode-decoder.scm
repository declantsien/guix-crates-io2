(define-module (crates-io be nc bencode-decoder) #:use-module (crates-io))

(define-public crate-bencode-decoder-0.0.2 (c (n "bencode-decoder") (v "0.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0idibw0xaa00rqkgjcq14xrrmr0hsdiqgy51lipwhgqnvdd0drfq")))

(define-public crate-bencode-decoder-0.0.3 (c (n "bencode-decoder") (v "0.0.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0cm1dimz607prdcckhdmlpj5lzvy30ba4mvldx4bh0684h8n33sz")))

