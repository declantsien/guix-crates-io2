(define-module (crates-io be nc bench_timer) #:use-module (crates-io))

(define-public crate-bench_timer-0.1.0 (c (n "bench_timer") (v "0.1.0") (d (list (d (n "stderr") (r "^0.8.0") (d #t) (k 0)))) (h "0f9h2fil023s01i8919vlmmnl9q3pv7iq9x717bz6pvbafv9mcha")))

(define-public crate-bench_timer-0.1.1 (c (n "bench_timer") (v "0.1.1") (d (list (d (n "stderr") (r "^0.8.0") (d #t) (k 0)))) (h "1r63h4qbi1pn9w13l5j2zy2ahlpixbl80gynyci2sp2i1kq9v76f")))

