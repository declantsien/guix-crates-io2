(define-module (crates-io be nc benchpmc) #:use-module (crates-io))

(define-public crate-benchpmc-1.0.0 (c (n "benchpmc") (v "1.0.0") (d (list (d (n "ansi_term") (r "0.11.*") (d #t) (k 0)) (d (n "clap") (r "2.31.*") (d #t) (k 0)) (d (n "nix") (r "0.10.*") (d #t) (k 0)) (d (n "pmc-rs") (r "^0.1.0") (d #t) (t "cfg(target_os = \"freebsd\")") (k 0)) (d (n "separator") (r "0.3.*") (d #t) (k 0)))) (h "0glxlrldwz9syragcq46yzg2m3nz6cnds13bihrzich0znc9xpzc")))

