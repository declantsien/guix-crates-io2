(define-module (crates-io be nc benchify) #:use-module (crates-io))

(define-public crate-benchify-0.4.5 (c (n "benchify") (v "0.4.5") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1yavg14zv3kcf8x06rih4hqlc6xh581x8s4hls6yn8ay1rrzkzjy")))

