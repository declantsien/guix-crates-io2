(define-module (crates-io be nc benchmark-counters) #:use-module (crates-io))

(define-public crate-benchmark-counters-0.1.0 (c (n "benchmark-counters") (v "0.1.0") (h "1d9y88aivj7rpj64pcq1bziqazwz14pmapldqisirkw16q91m52y")))

(define-public crate-benchmark-counters-0.2.0 (c (n "benchmark-counters") (v "0.2.0") (h "16s1y8i22xdbdm3ysd7kpar4m7yv9sz3z4bvh5833554nwv6808d")))

