(define-module (crates-io be nc benchling) #:use-module (crates-io))

(define-public crate-benchling-1.0.0 (c (n "benchling") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0zbvgrlkazxafbaxc99z7966gyz9j6g6vx3azxzr6ghxhimg0rsk")))

(define-public crate-benchling-2.0.0 (c (n "benchling") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "012q0b5n8wgy96h016jmix41bvnb0fj2jw6bk2xfk638nzda4s3i")))

(define-public crate-benchling-3.0.0 (c (n "benchling") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "10vxd02305zzgq2gw21c4dnj1nqyrch1bx8i5dajxahrqh6dmkj4")))

(define-public crate-benchling-4.0.0 (c (n "benchling") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1jp8vl68fprbm98ja5y6ac4zdax4d5iwqcjmjbwjlhjj8wsz8yzk")))

(define-public crate-benchling-5.0.0 (c (n "benchling") (v "5.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1zmfnn7fjw7pf3dw1w89ri02fpmwha05shkab6a5gqqpngkk7x18")))

