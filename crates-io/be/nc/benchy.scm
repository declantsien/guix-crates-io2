(define-module (crates-io be nc benchy) #:use-module (crates-io))

(define-public crate-benchy-0.1.0 (c (n "benchy") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "test-env-log") (r "^0.1.0") (d #t) (k 2)))) (h "1n5v9cwp9a2d5741ijhly5qiz34h18vyald2cymjyyhl2dq7mqms") (y #t)))

(define-public crate-benchy-0.1.1 (c (n "benchy") (v "0.1.1") (d (list (d (n "benchy-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "memory-stats") (r "^1.1.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("process"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wq9sva97gmw2jgn1lqsn12019gs9n6qivnalmvymrcdigrlsax0")))

