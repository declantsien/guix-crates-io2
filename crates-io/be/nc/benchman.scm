(define-module (crates-io be nc benchman) #:use-module (crates-io))

(define-public crate-benchman-0.1.0 (c (n "benchman") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "03ipza771679xgcbl2xjzrl886ywfbbfpsfsr74s6pfpghi8c0lg")))

(define-public crate-benchman-0.1.1 (c (n "benchman") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1klsymd5qj9amlrz1zlfzkrpmpdmijazx1flrmc7jf541b0p90lg")))

(define-public crate-benchman-0.2.0 (c (n "benchman") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0s84s4wj55nl9q2vj09872yg9fzh6m5qj28qs3yiqf9rymzfy770")))

(define-public crate-benchman-0.2.1 (c (n "benchman") (v "0.2.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "13y8y48s19v2p0l2vcb1bda9n0830901zpk67bf7xvzmq4b0grgn")))

(define-public crate-benchman-0.2.2 (c (n "benchman") (v "0.2.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "03646raa5i3c7n28gh5ka05bgwmqzlknl2qfknbfs0jxfqw6gm0l")))

(define-public crate-benchman-0.2.3 (c (n "benchman") (v "0.2.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "00zdbg9i21nkvs5cw1j541g5nl0lpsxi0ccdhqhiys2dxgs2h41r")))

(define-public crate-benchman-0.2.4 (c (n "benchman") (v "0.2.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "06zzsy3jf93jlm731ql4hf5qjmrywfvn745pcijw30a13n7hi9sg")))

(define-public crate-benchman-0.2.5 (c (n "benchman") (v "0.2.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "0rqbdz1qnfibzndx4cbvxia1dz6kfxxb28anywh0jvg44lfwyng0")))

(define-public crate-benchman-0.2.6 (c (n "benchman") (v "0.2.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)))) (h "0gygnzwkwapv8b6vrbn1nyyr3sx6ggagcakmm85i05p7mqagiwav")))

