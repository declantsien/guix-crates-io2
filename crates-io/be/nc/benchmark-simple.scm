(define-module (crates-io be nc benchmark-simple) #:use-module (crates-io))

(define-public crate-benchmark-simple-0.1.0 (c (n "benchmark-simple") (v "0.1.0") (d (list (d (n "precision") (r "^0.1.12") (d #t) (k 0)))) (h "18nr6gv2vim24xwmasjrz69pvwijwv07ryic8xlwrj741y8k8vmi")))

(define-public crate-benchmark-simple-0.1.1 (c (n "benchmark-simple") (v "0.1.1") (d (list (d (n "precision") (r "^0.1.12") (d #t) (k 0)))) (h "0ni323j98sfb5bqbpcaxkarwwfsnvk37x6fhkc9py0ms9a3d0gvb")))

(define-public crate-benchmark-simple-0.1.2 (c (n "benchmark-simple") (v "0.1.2") (d (list (d (n "precision") (r "^0.1.12") (d #t) (k 0)))) (h "1cak03xp8bis5xv0h9gnkza206sbnv1rwqcz54fjdf1jp6bbrhw8")))

(define-public crate-benchmark-simple-0.1.3 (c (n "benchmark-simple") (v "0.1.3") (d (list (d (n "precision") (r "^0.1.12") (d #t) (k 0)))) (h "09lz2rckb4ddcfl0lrxzvlhlmbfqg3wvlnvqfj71fhazjcbq05s2")))

(define-public crate-benchmark-simple-0.1.4 (c (n "benchmark-simple") (v "0.1.4") (d (list (d (n "precision") (r "^0.1.12") (d #t) (k 0)))) (h "086wqini93a0y77jf92g6bg1xyqaxnlim7xfh1yghbv8znbpz6fg")))

(define-public crate-benchmark-simple-0.1.5 (c (n "benchmark-simple") (v "0.1.5") (d (list (d (n "precision") (r "^0.1.12") (d #t) (k 0)))) (h "01ihhanb9n74z984kyi0x27zfr6fj59d57xi6ybxra0j64vipca4")))

(define-public crate-benchmark-simple-0.1.6 (c (n "benchmark-simple") (v "0.1.6") (d (list (d (n "precision") (r "^0.1.12") (d #t) (k 0)))) (h "0s2pr1xnhc6h3vrrz4aw2rsfxxiknw8jpds0lmmj56mhz276x2jj")))

(define-public crate-benchmark-simple-0.1.7 (c (n "benchmark-simple") (v "0.1.7") (d (list (d (n "precision") (r "^0.1.12") (d #t) (k 0)))) (h "0fp4nj5c940lzma1bwv2qhyxn0pkg1677npinhvgkb5mj3wpx2ph")))

(define-public crate-benchmark-simple-0.1.8 (c (n "benchmark-simple") (v "0.1.8") (d (list (d (n "precision") (r "^0.1.16") (d #t) (k 0)))) (h "1i52dshns1fl8n0xa1gcx1ya3lwrjabc36ayqcb2gyibx10dlypq")))

(define-public crate-benchmark-simple-0.1.9 (c (n "benchmark-simple") (v "0.1.9") (d (list (d (n "precision") (r "^0.1.16") (d #t) (k 0)))) (h "1ipyl4krix461948qwpk5a5l2wr83zf5pwymlzslkv9x4gbrgccb")))

