(define-module (crates-io be nc benchpress) #:use-module (crates-io))

(define-public crate-benchpress-0.1.0 (c (n "benchpress") (v "0.1.0") (d (list (d (n "astrolib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0b6lrz1v6lw4jsm6nqj3jj1smxzn6wbffdn0xir2fmpw9m6khdmc")))

(define-public crate-benchpress-0.1.1 (c (n "benchpress") (v "0.1.1") (d (list (d (n "astrolib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1g1lirvrqc5ywhziqdk7lfkdkkvqhd4c5vhbkg0a59k4x1s8qmkx")))

(define-public crate-benchpress-0.1.2 (c (n "benchpress") (v "0.1.2") (d (list (d (n "astrolib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "04ax6yvnsfy35lg34q90673l24yzfci7f29y0bf7cjp5c13hbb3v")))

(define-public crate-benchpress-0.1.3 (c (n "benchpress") (v "0.1.3") (d (list (d (n "astrolib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1hbgk8sfhw2fv7g3cyingg81f6vqm7jx1grkp90riiyjn6axygpg")))

