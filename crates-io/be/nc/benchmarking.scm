(define-module (crates-io be nc benchmarking) #:use-module (crates-io))

(define-public crate-benchmarking-0.1.0 (c (n "benchmarking") (v "0.1.0") (h "065ms3rwsxgh4zpp8ix9qw8wpcv9nz7kds0yfi7wmzip92zsj9j7") (y #t)))

(define-public crate-benchmarking-0.1.1 (c (n "benchmarking") (v "0.1.1") (h "0akaz2bb3zxgyns3vh7h5wc3b5qnypsv1v0ahqj16jvp8flippwi") (y #t)))

(define-public crate-benchmarking-0.1.2 (c (n "benchmarking") (v "0.1.2") (h "10jbvsraycr1jdlh7i42pvqm1xhgn2w06751jq8z9hc6xgg1922g") (y #t)))

(define-public crate-benchmarking-0.2.0 (c (n "benchmarking") (v "0.2.0") (h "1b0vsk02niff7k0fssmv103ql1kwwvnl3in7na494hikaydbcbd8") (y #t)))

(define-public crate-benchmarking-0.2.1 (c (n "benchmarking") (v "0.2.1") (h "0igldxd3n3qjljvff34iqh7rbvhqii7hsks1dyvi5x9xv88nxd2w") (y #t)))

(define-public crate-benchmarking-0.2.2 (c (n "benchmarking") (v "0.2.2") (h "1qcf626i5z9r8w0yxil4d926ml8y9j84ahrddbgf59cinv2cgfmg") (y #t)))

(define-public crate-benchmarking-0.2.3 (c (n "benchmarking") (v "0.2.3") (h "198py6gs24qhqdngbn81wm3q4fxwrhkwsh0pzvp12ym8npzb9f1y")))

(define-public crate-benchmarking-0.3.0 (c (n "benchmarking") (v "0.3.0") (h "0njj5fn6jsz3hj4gfbp2qh7sk3awj0r88pzh8cnrdf0xzgp03nxm") (y #t)))

(define-public crate-benchmarking-0.3.1 (c (n "benchmarking") (v "0.3.1") (h "1l1nvvlkb064bqrghnh6axlppldj4sdwc9mmrfrhvcy2w5sakf4n") (y #t)))

(define-public crate-benchmarking-0.3.2 (c (n "benchmarking") (v "0.3.2") (h "054sszqkgics2m5qqphb33ffs7yd0p4yy641fpvj91w6jim4mfxn")))

(define-public crate-benchmarking-0.4.0 (c (n "benchmarking") (v "0.4.0") (h "0bggnr0n6nbjgghxjzyqw7h1a1gs57y54x98qir5c1f1nfzmmchj") (y #t)))

(define-public crate-benchmarking-0.4.1 (c (n "benchmarking") (v "0.4.1") (h "0bg6gcrc7735ivcdm4lm98kra1p7my8gz9vzmxf4drh68bj2wmxb") (y #t)))

(define-public crate-benchmarking-0.4.2 (c (n "benchmarking") (v "0.4.2") (h "0ar9lvsscg8q10va1xw1k8701v9gskknyi0cvgfr0hxz1wjivg2r")))

(define-public crate-benchmarking-0.4.3 (c (n "benchmarking") (v "0.4.3") (h "1saxnyy42wa938saq2slv0pn3n9v6l2jmz9pfi145hax5s3p20ag")))

(define-public crate-benchmarking-0.4.4 (c (n "benchmarking") (v "0.4.4") (h "0lpwaw60q4qfhazy8ilgh8g7k44705lfr9wsq1a9lz7m7gawfzvb")))

(define-public crate-benchmarking-0.4.5 (c (n "benchmarking") (v "0.4.5") (h "1q0i860b8k0d4d7j8d2qp2s6ixq0b35g5mljsxrgvpl320ksl7n6")))

(define-public crate-benchmarking-0.4.6 (c (n "benchmarking") (v "0.4.6") (h "03kyhircsf9lllqp5rhsr9c2wz52fiybqcm7nvpary7xc3w7n9yf")))

(define-public crate-benchmarking-0.4.7 (c (n "benchmarking") (v "0.4.7") (h "187ms90d5lb4a8li090h4syc4j6pj9crb96ixfs0my96gws6l9j8")))

(define-public crate-benchmarking-0.4.8 (c (n "benchmarking") (v "0.4.8") (h "053ayr5q3yiqad6vbmqx1lsaglf3xrijdh276sy8mg09vv57pfq5")))

(define-public crate-benchmarking-0.4.9 (c (n "benchmarking") (v "0.4.9") (h "1wam3nbs28wbsi8kmr20cxb344v4svi0d3hmw92rhqbiwycmxqr5")))

(define-public crate-benchmarking-0.4.10 (c (n "benchmarking") (v "0.4.10") (h "03m92bjh111q6gyrfqxx3m89f96fgqc840x5kj0zjnx8v6aa6lpa")))

(define-public crate-benchmarking-0.4.11 (c (n "benchmarking") (v "0.4.11") (h "1mmsf6shdxbl6vs3h83dsac5wkmna4jf12k0b801q5jry2g44drr")))

(define-public crate-benchmarking-0.4.12 (c (n "benchmarking") (v "0.4.12") (h "1fs8kbnrnsfwy09mcyd2qmss7a27pwb2ssc14q5v69rbww12b11j")))

(define-public crate-benchmarking-0.4.13 (c (n "benchmarking") (v "0.4.13") (h "0jpy0fgh1wmqqa8i2psp4aafpm6wr5ipgzh784z6mfirwsfmlcvc") (r "1.56")))

