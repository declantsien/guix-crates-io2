(define-module (crates-io be nc benchft-minbft) #:use-module (crates-io))

(define-public crate-benchft-minbft-0.1.0 (c (n "benchft-minbft") (v "0.1.0") (h "02m17qmzckk5bk2rk3qjmdxsv7vza5ac457s3rwfv8j35l51kgs3") (y #t)))

(define-public crate-benchft-minbft-0.0.0-reserved (c (n "benchft-minbft") (v "0.0.0-reserved") (h "1hi48bsq9ykzkj7m2y74p1x0nlmbzr217j91lfgn4wfflc26bz14")))

