(define-module (crates-io be nc benchfun) #:use-module (crates-io))

(define-public crate-benchfun-0.1.6 (c (n "benchfun") (v "0.1.6") (h "06mh17rdpaqib56s5vvr6hi6y93qm4b10aq0gh94cwh3vdrx2azp") (y #t)))

(define-public crate-benchfun-0.1.0 (c (n "benchfun") (v "0.1.0") (h "1sw4yi81w60c2ybz2jmdwwspnlkzjwl1q8lc3brxcxc66kbcz9d6")))

(define-public crate-benchfun-0.1.1 (c (n "benchfun") (v "0.1.1") (h "1h6h16zs2qv0ib15k4r2nkliqyjpf4wrrfsrkwn7s6gqx14gy8yd")))

(define-public crate-benchfun-0.1.2 (c (n "benchfun") (v "0.1.2") (h "11ww1k453mapza8mkci0v141chzringifj9xa1lhb13c87s1qsx2")))

(define-public crate-benchfun-0.1.3 (c (n "benchfun") (v "0.1.3") (h "166va7ap67mkcbffnrzlcs9bid9d1bkj8m1ps920pdhynpp7wwfn")))

