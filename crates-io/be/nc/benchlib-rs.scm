(define-module (crates-io be nc benchlib-rs) #:use-module (crates-io))

(define-public crate-benchlib-rs-0.1.0 (c (n "benchlib-rs") (v "0.1.0") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0m991lw16x6nzm0h1k3vwl2pdy9hadn3rnbqlcpi2aaq3s0sw2j2")))

(define-public crate-benchlib-rs-0.1.1 (c (n "benchlib-rs") (v "0.1.1") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1syf1pip3ms3l2l1niwm2abb2j93832pdiwgzz4a971a0qz03sgh")))

(define-public crate-benchlib-rs-0.1.2 (c (n "benchlib-rs") (v "0.1.2") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0ic8baq3fmk9xnqj8lpr6admm3w505c3glafrhbz0ks44rlrmcl8")))

(define-public crate-benchlib-rs-0.2.0 (c (n "benchlib-rs") (v "0.2.0") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1bxl3rcma6iri867nmkkh420hgq8lddlvzrxz2ryxwrcpcvbybvv")))

(define-public crate-benchlib-rs-0.2.1 (c (n "benchlib-rs") (v "0.2.1") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0nkj01ar1r7pjxn0n05j60nifkp0xpxwbzmadh1z4yi9r39kqznx")))

(define-public crate-benchlib-rs-0.2.2 (c (n "benchlib-rs") (v "0.2.2") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1mymavq06yrli3ddy58jwaww64mcp000ksi2grih7fz5h26rmv4f")))

(define-public crate-benchlib-rs-0.3.0 (c (n "benchlib-rs") (v "0.3.0") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0a1blgjj9x02sjxap0rndrrzp6v3nn5bjj10dcznl7jmy61yvm5q")))

(define-public crate-benchlib-rs-0.3.1 (c (n "benchlib-rs") (v "0.3.1") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0814bjad62jz0yh0ppg0yq0xy7jncr9spq3rx85n1335kcj5gcg2")))

(define-public crate-benchlib-rs-0.3.2 (c (n "benchlib-rs") (v "0.3.2") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "163figw2bafb6acxn1d8sijy7w8vw42z7nv2ycz531r2qpx4nld4")))

(define-public crate-benchlib-rs-0.3.3 (c (n "benchlib-rs") (v "0.3.3") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0j8nc951g1mm0siclysh836d01rq4y1r12lb7h5l283jw73d9sqx")))

(define-public crate-benchlib-rs-0.3.4 (c (n "benchlib-rs") (v "0.3.4") (d (list (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "11af1phqm22spaxisaqrclyz1l9rz359sxdrgrli0n1v80r8kq2p")))

(define-public crate-benchlib-rs-0.4.0 (c (n "benchlib-rs") (v "0.4.0") (d (list (d (n "howlong") (r "^0.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0j5py4w1j947kn945vr7aads8b6hnl532ihdi4178gvrcgn8nxl3")))

(define-public crate-benchlib-rs-0.4.1 (c (n "benchlib-rs") (v "0.4.1") (d (list (d (n "howlong") (r "^0.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "038w19jzh0y58vrb323vk8l87fm68ymfb7b4w21449pzkvhr59jw")))

(define-public crate-benchlib-rs-0.4.2 (c (n "benchlib-rs") (v "0.4.2") (d (list (d (n "howlong") (r "^0.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0gxz958a5wxv9bhcac9qn7pxa3kk8pqfbyw084l5gwl654znmb0z")))

