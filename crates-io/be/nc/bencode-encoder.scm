(define-module (crates-io be nc bencode-encoder) #:use-module (crates-io))

(define-public crate-bencode-encoder-0.1.0 (c (n "bencode-encoder") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "01ciz5skjby2gbngvyk3qm69qk2yjvibfbky529f0c25jsi266lh")))

(define-public crate-bencode-encoder-0.1.1 (c (n "bencode-encoder") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "11pwhwrhj0gd1lnkv4a4rhsabsah66kqmlp3f8sv6cwiicv7ib24")))

(define-public crate-bencode-encoder-0.1.2 (c (n "bencode-encoder") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "18mlb2a09wc99wdcdknqmwvz2xdhipzzr1r1jbdjzq3vk8n20v69")))

