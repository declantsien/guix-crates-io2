(define-module (crates-io be nc bench) #:use-module (crates-io))

(define-public crate-bench-1.0.0 (c (n "bench") (v "1.0.0") (d (list (d (n "clap") (r "^2.28") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "criterion") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "loggerv") (r "^0.6") (d #t) (k 0)))) (h "0ag9py4yjqzxmlmvq22bwf8cjfv4v174kln3bn25rd5qr9y16k1a")))

(define-public crate-bench-1.1.0 (c (n "bench") (v "1.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.10") (d #t) (k 0)))) (h "1hqq4s6is0aw1kgg0gfhbcyi72hdcz249nki7ha6g97k7nfab10m")))

