(define-module (crates-io be nc benchmark_suite) #:use-module (crates-io))

(define-public crate-benchmark_suite-0.1.0 (c (n "benchmark_suite") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0jaa0k0855m43nbdmw8f89qdxf9aw58mprjdfac8wfdw2b1wyz3v")))

