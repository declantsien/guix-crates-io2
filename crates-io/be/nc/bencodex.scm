(define-module (crates-io be nc bencodex) #:use-module (crates-io))

(define-public crate-bencodex-0.1.0 (c (n "bencodex") (v "0.1.0") (h "1z57hh0vblic4dk1kv71aii8r7g2hdbmna0pdjl9mvq2lhnypxap") (y #t)))

(define-public crate-bencodex-0.1.1 (c (n "bencodex") (v "0.1.1") (h "03vkmagkx4m4mg7hwraa1s2ynv4j3fii1vrab1d9hl94svz4dgd4")))

(define-public crate-bencodex-0.1.2 (c (n "bencodex") (v "0.1.2") (h "1yi529iymdd1fdh5h1b2c11lgc3mjxhvhdwkxvhyssi91ifa1z8j")))

(define-public crate-bencodex-0.1.3 (c (n "bencodex") (v "0.1.3") (h "0rydjkjg3mclms31lnm2gab421x13g4996kqay99q7z49xm7lz5r")))

(define-public crate-bencodex-0.2.0 (c (n "bencodex") (v "0.2.0") (h "1sd4isnkhffz663i00fyvm6f95h9fqy9vg193kgivwvlab7n7kwd") (y #t)))

(define-public crate-bencodex-0.2.1 (c (n "bencodex") (v "0.2.1") (h "1gsmkmfl275vm9myn2cwkw8zafawh91lm9rqa22a5309xdqr6kb4")))

