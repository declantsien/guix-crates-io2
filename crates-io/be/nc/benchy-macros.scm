(define-module (crates-io be nc benchy-macros) #:use-module (crates-io))

(define-public crate-benchy-macros-0.1.0 (c (n "benchy-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nm1l5w9gvbl3xm9asx8bh9fgf7r18c8dzv2j7x4pvvik4dv48cm")))

(define-public crate-benchy-macros-0.1.1 (c (n "benchy-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "197dy11mii22jl5h5qysjgmbmv7iifkvqrs1rvlsx482bchpphi4")))

