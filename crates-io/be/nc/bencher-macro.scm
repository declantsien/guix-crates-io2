(define-module (crates-io be nc bencher-macro) #:use-module (crates-io))

(define-public crate-bencher-macro-0.1.0 (c (n "bencher-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "081n9mrdir1frb606iw3mc6lb8qpd80ycfmam7air4mq4g9f6npa")))

(define-public crate-bencher-macro-0.1.1 (c (n "bencher-macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "07psa4d3d6v1wlcfaj1s8kd1mya1yn2dqv126y16ww9r74s3pqkp") (f (quote (("track-allocator"))))))

