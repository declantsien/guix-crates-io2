(define-module (crates-io be nc bench-trie) #:use-module (crates-io))

(define-public crate-bench-trie-0.12.2 (c (n "bench-trie") (v "0.12.2") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 0)) (d (n "hash-db") (r "^0.12.2") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.12.2") (d #t) (k 0)) (d (n "memory-db") (r "^0.12.2") (d #t) (k 0)) (d (n "susy-codec") (r "^3.0") (d #t) (k 0)) (d (n "susy-trie-standardmap") (r "^0.12.2") (d #t) (k 0)) (d (n "trie-db") (r "^0.12.2") (d #t) (k 0)) (d (n "trie-root") (r "^0.12.2") (d #t) (k 0)))) (h "1wm8cf3wd3085d2d9d0332jgji1y5i92c3881l3hk236wh3hphri")))

(define-public crate-bench-trie-0.12.0 (c (n "bench-trie") (v "0.12.0") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 0)) (d (n "hash-db") (r "^0.12.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.12.0") (d #t) (k 0)) (d (n "memory-db") (r "^0.12.0") (d #t) (k 0)) (d (n "susy-codec") (r "^3.0") (d #t) (k 0)) (d (n "susy-trie-standardmap") (r "^0.12.0") (d #t) (k 0)) (d (n "trie-db") (r "^0.12.0") (d #t) (k 0)) (d (n "trie-root") (r "^0.12.0") (d #t) (k 0)))) (h "0nb2lp3nbmnpjdzvym84p530nzc78hsws1m2pxyqvrca9mf64cli")))

