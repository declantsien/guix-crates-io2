(define-module (crates-io be rg bergwerk) #:use-module (crates-io))

(define-public crate-bergwerk-0.1.0-pre (c (n "bergwerk") (v "0.1.0-pre") (d (list (d (n "blake3") (r "=0.3.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kvpxy672j7vfyzmy64xnaqkm3p42dn1awmxdp9b089d0xvnsxyd") (y #t)))

(define-public crate-bergwerk-0.1.0 (c (n "bergwerk") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0v8bfrq5zwrbcmh3s8m5jj7fhvkndfyr4z9vga3hk2hpyps5sldg")))

