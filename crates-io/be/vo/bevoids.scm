(define-module (crates-io be vo bevoids) #:use-module (crates-io))

(define-public crate-bevoids-0.1.0 (c (n "bevoids") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)))) (h "16l74kvhshrdbvaxzv11q5a9sgk8s3k9dkjbfz9cab7iq9z7lq1g")))

