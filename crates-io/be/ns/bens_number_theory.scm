(define-module (crates-io be ns bens_number_theory) #:use-module (crates-io))

(define-public crate-bens_number_theory-0.2.2 (c (n "bens_number_theory") (v "0.2.2") (h "132pgbz805ky5gmav7igq3zxhvqnhcnpbh5gxwjhyw1bnni6p0d4") (y #t)))

(define-public crate-bens_number_theory-0.3.0 (c (n "bens_number_theory") (v "0.3.0") (h "0rvicrd89j6lxmijdf6g6ssgkijpg7768gziwd83kh0j735b2wkv") (y #t)))

(define-public crate-bens_number_theory-0.3.1 (c (n "bens_number_theory") (v "0.3.1") (h "0j4javq7nc6lf5flc7w27z3n91rzlzya0p22wiic11sdyqrz8p3g") (y #t)))

(define-public crate-bens_number_theory-0.3.2 (c (n "bens_number_theory") (v "0.3.2") (h "1kw45vyjkhsp8divdmi6w22sc6bgph71vbcy3hm2d7brwwy3ys9p") (y #t)))

(define-public crate-bens_number_theory-0.3.3 (c (n "bens_number_theory") (v "0.3.3") (h "0dn9sjlxpgq683ggc76dy304plcbvl922v5acfrwbrw8gvl75bn7") (y #t)))

(define-public crate-bens_number_theory-0.3.4 (c (n "bens_number_theory") (v "0.3.4") (h "10v5p62g5laqy4ad5idlnq82zgs2hsmy05dfyc2q8n3q1n907xmk") (y #t)))

(define-public crate-bens_number_theory-0.3.5 (c (n "bens_number_theory") (v "0.3.5") (h "0h6kzi6hbhii1h1fmms6vfggjrhx8pasrc5n123afvxy75gxafyj") (y #t)))

(define-public crate-bens_number_theory-0.3.6 (c (n "bens_number_theory") (v "0.3.6") (h "11y9shkihjala9ak84j3dqjhsn6wqnbi9pdlmwkn42qri049hrgj") (y #t)))

(define-public crate-bens_number_theory-0.3.7 (c (n "bens_number_theory") (v "0.3.7") (h "085fzi5xsiaw11f62qmhmbgs9vmgs0l5gkm71zrj1fkjbk74h0m3") (y #t)))

(define-public crate-bens_number_theory-0.3.8 (c (n "bens_number_theory") (v "0.3.8") (d (list (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)))) (h "1ahs5fj9chyyxw26a1djnpifyq3x6sldangrf9lnl1zmdlzkd00g") (y #t)))

(define-public crate-bens_number_theory-0.3.9 (c (n "bens_number_theory") (v "0.3.9") (d (list (d (n "bigdecimal") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)))) (h "17bg88lv7643qf8zdbv3xhi5i8zcnwkzg0616rfgws4h89i1l2wh") (y #t)))

(define-public crate-bens_number_theory-0.3.10 (c (n "bens_number_theory") (v "0.3.10") (d (list (d (n "bigdecimal") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)))) (h "1zhgpws7nxpkckfi2ap5vm09jd53g3a1xggqsbblrfqshiwxww77") (y #t)))

(define-public crate-bens_number_theory-0.3.11 (c (n "bens_number_theory") (v "0.3.11") (d (list (d (n "bigdecimal") (r "^0.4.3") (d #t) (k 0)))) (h "1m8zl1kwxza56zdxlwmvdgbmhphxwg7dcmb8gld7qv15yf86v23z") (y #t)))

(define-public crate-bens_number_theory-0.3.12 (c (n "bens_number_theory") (v "0.3.12") (d (list (d (n "bigdecimal") (r "^0.4.3") (d #t) (k 0)))) (h "0qrfdjj7w8rcjgqn43fx991icsljh3snqrpclrcpk4k446fy2m8r") (y #t)))

(define-public crate-bens_number_theory-0.4.0 (c (n "bens_number_theory") (v "0.4.0") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "0zai0lxwd7qils42s331hjdn016yxmlhyrvbymwga5grdv08psrx") (y #t)))

(define-public crate-bens_number_theory-0.4.1 (c (n "bens_number_theory") (v "0.4.1") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "1iwym3sngrlrm41nzhbhn66ny7064h9fdd3z5m5s8k4i1ivvr4f2") (y #t)))

(define-public crate-bens_number_theory-0.4.2 (c (n "bens_number_theory") (v "0.4.2") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "1m4k2d3calgd21grr7fi9kavl05x8pb6c647l98h21ayjfc53xql") (y #t)))

(define-public crate-bens_number_theory-0.4.3 (c (n "bens_number_theory") (v "0.4.3") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "1rxgf00yhzrljcvpi33s7gh4n7j6dd2lgga4sdxy520b83595zlx") (y #t)))

(define-public crate-bens_number_theory-0.4.4 (c (n "bens_number_theory") (v "0.4.4") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "0s9pq93xk7nszbzcm2fzxpf7zhpsn6bj5rp0c7jf54ac7an7ac68") (y #t)))

(define-public crate-bens_number_theory-0.5.0 (c (n "bens_number_theory") (v "0.5.0") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "1h7ry1y4cmrfcx1dwghpcpxrwak6nih4q486bh7kx96w6g8051sz") (y #t)))

(define-public crate-bens_number_theory-0.5.1 (c (n "bens_number_theory") (v "0.5.1") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "0nlpgzb5rgmhdyq51iw1216523cyhirv759w2wjvrz01kd89xxzp") (y #t)))

(define-public crate-bens_number_theory-0.5.2 (c (n "bens_number_theory") (v "0.5.2") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "08pvhxb5nwpps7s5b9lr7jla6mbvalhvlpphbdm49vl8bnz9y4ys")))

