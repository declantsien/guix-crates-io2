(define-module (crates-io be ns bens_naive_rational_number) #:use-module (crates-io))

(define-public crate-bens_naive_rational_number-0.1.0 (c (n "bens_naive_rational_number") (v "0.1.0") (h "1asa4va0b9wk9fhkmyc985l2p1ybwbhl2jsrfi7xgcj67ywiyfvd") (y #t)))

(define-public crate-bens_naive_rational_number-0.1.1 (c (n "bens_naive_rational_number") (v "0.1.1") (h "0shvqxjgb2rjw4irviag355dlk1afi6760ck9948plq9x3f747yn") (y #t)))

(define-public crate-bens_naive_rational_number-0.1.2 (c (n "bens_naive_rational_number") (v "0.1.2") (h "0nyas6gv37qxdk4dbb8ahhzr8mny7zv71rfkc722vv4npjrgh57k") (y #t)))

(define-public crate-bens_naive_rational_number-0.1.3 (c (n "bens_naive_rational_number") (v "0.1.3") (h "08109wgl08jj3yqalz5j8pjphmp7sw8spqg14hba3y8k3gr01hq8") (y #t)))

(define-public crate-bens_naive_rational_number-0.1.4 (c (n "bens_naive_rational_number") (v "0.1.4") (h "08zisi89c4xp1bykwgnpb9fr3xxrjnbzm3wlwhis82flicdcf91s") (y #t)))

(define-public crate-bens_naive_rational_number-0.1.5 (c (n "bens_naive_rational_number") (v "0.1.5") (h "0l84kxa0aic7zvpd8h5v9b7f5g1zpphzsp2dqcvjmplx5dsm5hph") (y #t)))

