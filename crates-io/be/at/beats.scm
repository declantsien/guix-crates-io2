(define-module (crates-io be at beats) #:use-module (crates-io))

(define-public crate-beats-0.1.0 (c (n "beats") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "06qqmgw3nrl1gqzjqjjzwkj2pw4di8rn41xhd7804d8abc1ms3vp")))

(define-public crate-beats-0.1.1 (c (n "beats") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "11l7pnwqbihacq9xyplb8as0s6cbjfcvqcmm9rgw58mqa5xf2ig8")))

(define-public crate-beats-0.1.2 (c (n "beats") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1dwirnz9q638m93ysnsgsy188qqgwspb7h3ylwyc080gjrnpdn21")))

(define-public crate-beats-0.1.3 (c (n "beats") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0s0fplwqss9ald5cqhnjjiqlfpmhhqknvmi18sm1x907yi3cgczx")))

