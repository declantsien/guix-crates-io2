(define-module (crates-io be at beatsabermaprs) #:use-module (crates-io))

(define-public crate-beatsabermaprs-0.1.0 (c (n "beatsabermaprs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1xpi357pvd38zacmqzrb94qsbf4bcfqa3pp74577d0dcwffi5sxa")))

