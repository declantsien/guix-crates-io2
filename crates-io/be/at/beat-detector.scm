(define-module (crates-io be at beat-detector) #:use-module (crates-io))

(define-public crate-beat-detector-0.0.0 (c (n "beat-detector") (v "0.0.0") (d (list (d (n "lowpass-filter") (r "^0.2.3") (d #t) (k 0)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)))) (h "1dwvsyzqg037yw1yvxvff7pgjv4dbfc1sghnaxx78xflf0zfd6i1")))

(define-public crate-beat-detector-0.1.0 (c (n "beat-detector") (v "0.1.0") (d (list (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (d #t) (k 2)) (d (n "lowpass-filter") (r "^0.2.4") (d #t) (k 0)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "spectrum-analyzer") (r "^0.5.2") (d #t) (k 0)) (d (n "ws2818-rgb-led-spi-driver") (r "^2.0.0") (d #t) (k 2)))) (h "12a02hs8k37p0rm9bvm269gh0vhis73bsxg34j67irirg81606bk")))

(define-public crate-beat-detector-0.1.2 (c (n "beat-detector") (v "0.1.2") (d (list (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (d #t) (k 2)) (d (n "lowpass-filter") (r "^0.2.4") (d #t) (k 0)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ringbuffer") (r "^0.7.1") (d #t) (k 0)) (d (n "spectrum-analyzer") (r "^1.1.0") (d #t) (k 0)) (d (n "ws2818-rgb-led-spi-driver") (r "^2.0.0") (d #t) (k 2)))) (h "17jxi2nlksgrsd2fghflw3gnda7yyvpz4jl4xn2zjblnpmykq8pa")))

