(define-module (crates-io be ru berusty) #:use-module (crates-io))

(define-public crate-berusty-0.1.0 (c (n "berusty") (v "0.1.0") (h "1fd6bkybhpaksmiw8yrr8gryzh0z4z7n6ryqnmps6srd5mn4sp5w")))

(define-public crate-berusty-0.1.1 (c (n "berusty") (v "0.1.1") (h "1z3clargxp6bb4s2qv9sin597mwmr1wp32a4hb17bi2grr3lcf8z")))

