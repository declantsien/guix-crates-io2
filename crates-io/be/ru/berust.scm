(define-module (crates-io be ru berust) #:use-module (crates-io))

(define-public crate-berust-0.1.0 (c (n "berust") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.6") (d #t) (k 0)))) (h "0bp4lppb0mjy92ikmc27hsh5gw8g6xpnlgfsvcshh6rqfwgjimn2")))

