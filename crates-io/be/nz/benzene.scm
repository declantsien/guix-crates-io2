(define-module (crates-io be nz benzene) #:use-module (crates-io))

(define-public crate-benzene-0.1.0 (c (n "benzene") (v "0.1.0") (d (list (d (n "carboxyl") (r "^0.2") (d #t) (k 0)))) (h "1asfa5jvldwdgvp420yz5mskn4rsky0x1iiqc6q03psb4c7bkw46")))

(define-public crate-benzene-0.1.1 (c (n "benzene") (v "0.1.1") (d (list (d (n "carboxyl") (r "^0.2") (d #t) (k 0)))) (h "0na9lahppnjysh7q5syc07j57cy2alrd18anhwyp7hb54dcs5hvj")))

(define-public crate-benzene-0.2.0 (c (n "benzene") (v "0.2.0") (d (list (d (n "carboxyl") (r "^0.2") (d #t) (k 0)))) (h "06s785zsldykcagims7jchklihvlwvni7gz73n3zrvihgx8xifhk")))

