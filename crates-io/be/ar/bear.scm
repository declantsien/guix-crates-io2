(define-module (crates-io be ar bear) #:use-module (crates-io))

(define-public crate-bear-0.1.0 (c (n "bear") (v "0.1.0") (h "0ifk23lgdhhi5s2mw4npkrv7fckbwxshnfkcsplbbwkq8gshr51w")))

(define-public crate-bear-0.1.1 (c (n "bear") (v "0.1.1") (h "0nq64ffgcbaickalpbv237vz4cimpgx1zf12ibppqahdpcgcwg0s")))

(define-public crate-bear-0.2.0 (c (n "bear") (v "0.2.0") (h "04bih4pl3di6q2q7x0w3riif2f457afgwjj4n8416a56a7ak4cq9")))

(define-public crate-bear-0.2.1 (c (n "bear") (v "0.2.1") (h "13sr4r83z34v0jc6xy6a7h819n7dz6r3fvm6gdqi58159hxprxxn")))

(define-public crate-bear-0.2.2 (c (n "bear") (v "0.2.2") (h "13hkylpdc4658wrv2hpg8v79pbqvaic5kmfnn39x68r23vi4p367")))

