(define-module (crates-io be ar bearnote) #:use-module (crates-io))

(define-public crate-bearnote-0.1.0 (c (n "bearnote") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1hmg3gglipmml3k909q7q3nvhd34zyzghdc69yvmcclgyqz520d9")))

