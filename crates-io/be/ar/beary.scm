(define-module (crates-io be ar beary) #:use-module (crates-io))

(define-public crate-beary-0.1.0 (c (n "beary") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "diesel") (r "^2.1.0") (f (quote ("sqlite" "chrono" "uuid" "serde_json" "r2d2" "64-column-tables"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "rust-texas") (r "^0.2.6") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (d #t) (k 0)))) (h "0zbiq5v647g2rg1galnhpw536c8pys5y5x0hg5vmhnlrz0yy8348")))

