(define-module (crates-io be ar beard) #:use-module (crates-io))

(define-public crate-beard-0.1.0 (c (n "beard") (v "0.1.0") (h "1xrrg78ia6ysny2d6qz5x9jdvlpqkp11ns99vjmw8b8v79al5c5w")))

(define-public crate-beard-0.2.0 (c (n "beard") (v "0.2.0") (h "10v52dflms4h0ag88zpihxnb57g22n84378rsx0dxbavx08psrfa")))

(define-public crate-beard-0.2.1 (c (n "beard") (v "0.2.1") (h "0fy6cbdwjy91mhdv7hdb3nq6vp0nh0p2sr4gf2f5hcj06wh265wj")))

