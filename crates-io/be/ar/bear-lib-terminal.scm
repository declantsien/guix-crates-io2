(define-module (crates-io be ar bear-lib-terminal) #:use-module (crates-io))

(define-public crate-bear-lib-terminal-0.9.0 (c (n "bear-lib-terminal") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "17k007y54szf48wcgr9jc8gnx7dbyh297mds2gws7y9fw34vl9gx")))

(define-public crate-bear-lib-terminal-1.0.0 (c (n "bear-lib-terminal") (v "1.0.0") (d (list (d (n "bear-lib-terminal-sys") (r "^1.1.0") (d #t) (k 0)))) (h "0b2qrjmfnvnb5bmz98dpvvqra6mfi1wz82dhbfxn66ab2725dcv1")))

(define-public crate-bear-lib-terminal-1.0.1 (c (n "bear-lib-terminal") (v "1.0.1") (d (list (d (n "bear-lib-terminal-sys") (r "^1.1.1") (d #t) (k 0)))) (h "102jdz15yznjkpji1ixkc8n6cl3xx3mzry0vm07v8lybvnc705b8")))

(define-public crate-bear-lib-terminal-1.1.0 (c (n "bear-lib-terminal") (v "1.1.0") (d (list (d (n "bear-lib-terminal-sys") (r "^1.1.1") (d #t) (k 0)))) (h "1ihn61gps8a7jyy09lhxh8blmhkmdhdn87y60rahwmc8msanyrhs")))

(define-public crate-bear-lib-terminal-1.1.1 (c (n "bear-lib-terminal") (v "1.1.1") (d (list (d (n "bear-lib-terminal-sys") (r "^1.1.1") (d #t) (k 0)))) (h "08gxcdx527hxk2j71l9c6lwzigx65nql7116qya0304gawlwl5q2")))

(define-public crate-bear-lib-terminal-1.2.0 (c (n "bear-lib-terminal") (v "1.2.0") (d (list (d (n "bear-lib-terminal-sys") (r "^1.1.1") (d #t) (k 0)))) (h "1fsihgap8gx4rk7j83q0js2g9ifskzb3wvnly257ifpzffm072v4")))

(define-public crate-bear-lib-terminal-1.3.0 (c (n "bear-lib-terminal") (v "1.3.0") (d (list (d (n "bear-lib-terminal-sys") (r "^1.1.1") (d #t) (k 0)))) (h "07qz5ydgihd3ygs2iavcvy0zv8f7b4j8nqja9hqqijmq2j52vz68")))

(define-public crate-bear-lib-terminal-1.3.1 (c (n "bear-lib-terminal") (v "1.3.1") (d (list (d (n "bear-lib-terminal-sys") (r "^1.1.1") (d #t) (k 0)))) (h "0ldid02wv2wj63x1019agcny6khdi1b24i2a2bvi1w7kvf3s10c5")))

(define-public crate-bear-lib-terminal-1.3.2 (c (n "bear-lib-terminal") (v "1.3.2") (d (list (d (n "bear-lib-terminal-sys") (r "^1.1.1") (d #t) (k 0)))) (h "1xl6b4jbln9pzbpihfpkjlq8dki80b9jyq5yfx3lcc5i18dpkf0f")))

(define-public crate-bear-lib-terminal-1.3.3 (c (n "bear-lib-terminal") (v "1.3.3") (d (list (d (n "bear-lib-terminal-sys") (r "^1.2") (d #t) (k 0)))) (h "046fl78i8b04mxspvsqlm0fk9c0zn0d449265y83cb1jgzssp0h3")))

(define-public crate-bear-lib-terminal-1.4.0 (c (n "bear-lib-terminal") (v "1.4.0") (d (list (d (n "bear-lib-terminal-sys") (r "^1.3") (d #t) (k 0)))) (h "0rz7f9h06sky00fnmdkp19lp8iij1c3agfw35g6sya9i9rma2w4r")))

(define-public crate-bear-lib-terminal-2.0.0 (c (n "bear-lib-terminal") (v "2.0.0") (d (list (d (n "bear-lib-terminal-sys") (r "^1.3") (d #t) (k 0)))) (h "05kij0gr0i0phikh1qc5mdgi73f3ly9x1fhy3snzizm3jpv4jm9i")))

