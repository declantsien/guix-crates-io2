(define-module (crates-io be ar bearssl) #:use-module (crates-io))

(define-public crate-bearssl-0.0.1 (c (n "bearssl") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.31.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.32") (d #t) (k 0)))) (h "0q6sxcznyqjnqs51y9dj7z6ip64fzhw0jhwsxvm3w4ni31f08cqi")))

