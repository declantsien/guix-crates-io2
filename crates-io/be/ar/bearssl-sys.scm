(define-module (crates-io be ar bearssl-sys) #:use-module (crates-io))

(define-public crate-bearssl-sys-0.0.1 (c (n "bearssl-sys") (v "0.0.1") (h "096s76drblsbdhwghx3s93yl42gngqkisjxnjv7w0nr4yckmlhyy")))

(define-public crate-bearssl-sys-0.0.2 (c (n "bearssl-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rust_c") (r "^0.1.0") (f (quote ("build"))) (d #t) (k 1)))) (h "1qfpij40w7k12fr9p35wyy98d6085lf4i0nzqpdy6ryjh6yzqlwm")))

(define-public crate-bearssl-sys-0.0.3 (c (n "bearssl-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zl4z087d44l7zdi3iflpb6lqcv2fmr2pi3z1jfrgzi664dwj50g")))

(define-public crate-bearssl-sys-0.0.4 (c (n "bearssl-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gh2lvmfwndx0k7424wiisqsbq7f2z783267r1q8qbfzwxxw7kkx")))

