(define-module (crates-io be ar bear-lib-terminal-sys) #:use-module (crates-io))

(define-public crate-bear-lib-terminal-sys-1.0.0 (c (n "bear-lib-terminal-sys") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "11kzhbqsfr0aq4lrkyl7srj3ymdhfqhi2kby0284mvqbp28vn0q9")))

(define-public crate-bear-lib-terminal-sys-1.1.0 (c (n "bear-lib-terminal-sys") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0rkd52b5lpk57wcsrh5j09x6lz095s44ax8flyjbshqlvrqpia43")))

(define-public crate-bear-lib-terminal-sys-1.1.1 (c (n "bear-lib-terminal-sys") (v "1.1.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0ivglshqd4lpgg4spb4kkwfqf8c8mqw35cfrik5wlhifmjcb90v7")))

(define-public crate-bear-lib-terminal-sys-1.2.0 (c (n "bear-lib-terminal-sys") (v "1.2.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "05rvqwkic7qsm6a2as1nrzgl3ip9biap3547vpsw3h7hxci4r91g")))

(define-public crate-bear-lib-terminal-sys-1.2.1 (c (n "bear-lib-terminal-sys") (v "1.2.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1lh3cm8dw99hsrs2mwgssygxqlw00wsiyf90dksiqp5fc93izi3s")))

(define-public crate-bear-lib-terminal-sys-1.2.2 (c (n "bear-lib-terminal-sys") (v "1.2.2") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1b0qiczrrk1imysxb3vq8jmmmlhjaycf66kfrhyjd5nmjsczc7a5")))

(define-public crate-bear-lib-terminal-sys-1.3.0 (c (n "bear-lib-terminal-sys") (v "1.3.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "17r71abbdz4l0vkwwv9p3lsmvy21sai8xabz2wl73w025hxwh11s")))

