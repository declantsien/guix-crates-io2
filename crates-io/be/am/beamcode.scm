(define-module (crates-io be am beamcode) #:use-module (crates-io))

(define-public crate-beamcode-0.1.0 (c (n "beamcode") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "beam_file") (r "^0.3") (d #t) (k 2)) (d (n "beamcode_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00cgwwgf0xrhbsbmgdc5fk6w33nn26ki690i0jwqvr37y23lb1yd")))

