(define-module (crates-io be am beams) #:use-module (crates-io))

(define-public crate-beams-0.1.0 (c (n "beams") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^0.4.5") (f (quote ("json"))) (d #t) (k 0)))) (h "0yqr8806ny68iy4a76dkpc7pfahak1s4ykm6rcdibsf8hksq2x0c")))

