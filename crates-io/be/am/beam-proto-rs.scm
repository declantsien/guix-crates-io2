(define-module (crates-io be am beam-proto-rs) #:use-module (crates-io))

(define-public crate-beam-proto-rs-2.41.0 (c (n "beam-proto-rs") (v "2.41.0") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "protobuf") (r "^3.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.1") (o #t) (d #t) (k 1)))) (h "0qqvzw2f3pjpnc2dnzry2faj7c2qzxh9441db73xf51divl0rmqv") (s 2) (e (quote (("build-proto" "dep:glob" "dep:protobuf-codegen")))) (r "1.63.0")))

