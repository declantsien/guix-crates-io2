(define-module (crates-io be am beamtrace) #:use-module (crates-io))

(define-public crate-beamtrace-0.1.0 (c (n "beamtrace") (v "0.1.0") (d (list (d (n "beambook") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "ndarray-image") (r "^0.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "006ld8z0gbywlhw0b5qjp0yjck2xfba2m99wqxhd2vg9y2mw3hja")))

