(define-module (crates-io be am beam) #:use-module (crates-io))

(define-public crate-beam-0.1.0 (c (n "beam") (v "0.1.0") (h "1a226vyski4qpil3y559rpxps25rjfrpq2pi1l7apwjwan0hzy12")))

(define-public crate-beam-0.2.0 (c (n "beam") (v "0.2.0") (d (list (d (n "expectest") (r "^0.9") (d #t) (k 2)) (d (n "point") (r "^0.3.0") (d #t) (k 0)))) (h "1lvscsgwfcv9nfs17fdqmshwfsmpvb7rhf4lni2d12bq1wly2ddn")))

