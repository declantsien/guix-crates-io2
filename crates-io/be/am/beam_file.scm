(define-module (crates-io be am beam_file) #:use-module (crates-io))

(define-public crate-beam_file-0.1.0 (c (n "beam_file") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "1jbyf69qy6vmja61hhxmcp9k6c4xkcx6br6vnvkfdzh67s864zi0")))

(define-public crate-beam_file-0.2.0 (c (n "beam_file") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0wbj5l86viv3bjbcrrbx1b93krb2lchsqzhg7a4br47w3rmipin5")))

(define-public crate-beam_file-0.2.1 (c (n "beam_file") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)))) (h "1kxm1qh66g1azqm4mzdah79p39nii4ay1zbxp5ijr2w6bh3blxmh")))

(define-public crate-beam_file-0.2.2 (c (n "beam_file") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)))) (h "0lkw9dqiww5kg3vml0i2f9sfqx2f7mzjzi22c7p4mqcnwgqrax58")))

(define-public crate-beam_file-0.2.3 (c (n "beam_file") (v "0.2.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)))) (h "0l36amhn8qqx1k0v6nizdzihkyx3gcm3f1vwby0yjl934wls37ry")))

(define-public crate-beam_file-0.2.4 (c (n "beam_file") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)))) (h "1kzm5zcb1jzff7zlhyhrq0dydx3zkfg2wvk3w5ps0x3wmm5ycmb0")))

(define-public crate-beam_file-0.3.0 (c (n "beam_file") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "libflate") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sapam323q1hkpb0rw4fm3m7d2sw9g1wlpw0r8rwb7mw66mh6mpg")))

(define-public crate-beam_file-0.3.1 (c (n "beam_file") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "libflate") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1nwdw7g7ammjnda2lg64cisfr82303xhbzhavyj3lsmif8sibhbp")))

