(define-module (crates-io be am beamcode_derive) #:use-module (crates-io))

(define-public crate-beamcode_derive-0.1.0 (c (n "beamcode_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "015smzgmfs7aday2f120nmm1kb9jm4hs5ka3yfi1c73zcsdhbpwv")))

