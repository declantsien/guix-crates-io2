(define-module (crates-io be am beamdpr) #:use-module (crates-io))

(define-public crate-beamdpr-0.1.0 (c (n "beamdpr") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)))) (h "1kirnp8xfl1ylhnmxi7ykz0nsl3y0b95bqflnrhxp2h7my2farls")))

(define-public crate-beamdpr-0.1.1 (c (n "beamdpr") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)))) (h "1fmg1xfrj575yvf4av0ak6k95cz9f30nq8plic4vpp040v06xcjp")))

(define-public crate-beamdpr-0.1.2 (c (n "beamdpr") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)))) (h "0h7a3bq1zrcyfp5h76jljz59bsx1s8cjvkjrvky6azy3ky9w45rw")))

(define-public crate-beamdpr-0.1.3 (c (n "beamdpr") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)))) (h "02iyq27m55n561rzmfy67dwxx5d0d8rd3wlvp6sfx83gyvw47yqa")))

(define-public crate-beamdpr-0.1.4 (c (n "beamdpr") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)))) (h "1l38wfbr68ks5sc17iscafc0i3m6a554mpl65m8l1diirryawb0d")))

(define-public crate-beamdpr-0.1.5 (c (n "beamdpr") (v "0.1.5") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1c020hkc4j13bhyi3rvligg3ax0g8bxp7m8ha5rjp99xq6zjjsas")))

(define-public crate-beamdpr-0.1.7 (c (n "beamdpr") (v "0.1.7") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0y7qm667d0acngv8h56gd71mywxq8mciaa8a0p1kpg126w6i8f2b")))

(define-public crate-beamdpr-0.2.0 (c (n "beamdpr") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1kky0g4l39wg7fqpdllswvbks9vgnh1nmla9b9qjrm7vdrigak5c")))

(define-public crate-beamdpr-0.2.2 (c (n "beamdpr") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1rmlahginzvalrs9vyal3v4y7dpss4j1q2lj22vqgywz5rjvfin9")))

(define-public crate-beamdpr-0.2.3 (c (n "beamdpr") (v "0.2.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "06raaaaqcfcjmw2c2s3548gwvxjhpzg7rd7qrmnzsrsr3rsmycjm")))

(define-public crate-beamdpr-0.2.4 (c (n "beamdpr") (v "0.2.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "13s6iy353k4m1af2ygsraa7hj95wagjjgpcn01fqpkgxmpvnl2zg")))

(define-public crate-beamdpr-1.0.0 (c (n "beamdpr") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1537sr86bv533l7wnwb9c8xm9lhwq8z6318cazk11znnf3rby4ii")))

(define-public crate-beamdpr-1.0.1 (c (n "beamdpr") (v "1.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0w9hmfx4sxh20g31289qv19arzap0h0c8xlc2nhlw2qnjrxkv3ji")))

(define-public crate-beamdpr-1.0.2 (c (n "beamdpr") (v "1.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1irax4rf7hinlpysjql1qbrpjiiajy5kmv9wmwxl63lmm7f9gb2b")))

