(define-module (crates-io be am beam_bvm_interface) #:use-module (crates-io))

(define-public crate-beam_bvm_interface-0.1.0 (c (n "beam_bvm_interface") (v "0.1.0") (h "1az3cp69jagqbp4cs0620v7dzqmvzb5ri96jln655bakp5zspnfx")))

(define-public crate-beam_bvm_interface-7.1.13105 (c (n "beam_bvm_interface") (v "7.1.13105") (h "0znss9d0yaw973xvgrk3qm7izmin30y2jjl0sn9chfacq1rfngh6")))

(define-public crate-beam_bvm_interface-7.1.13106 (c (n "beam_bvm_interface") (v "7.1.13106") (h "0l1b67pn0kcqy7g4mdn9rxr42pxg49yd23b361h7991a9vrb0wq2")))

(define-public crate-beam_bvm_interface-7.1.13107 (c (n "beam_bvm_interface") (v "7.1.13107") (h "1pr6p72vzi7nx12qdavyknd39ixgb54cqk8w8n6l6bis7zja44a0")))

