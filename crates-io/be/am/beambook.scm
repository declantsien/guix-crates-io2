(define-module (crates-io be am beambook) #:use-module (crates-io))

(define-public crate-beambook-0.1.0 (c (n "beambook") (v "0.1.0") (d (list (d (n "rmp-serde") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fvy2958nqs6rk9yka4wvxqqp3qxpzlinv42g8zvkzx90gh474q4")))

