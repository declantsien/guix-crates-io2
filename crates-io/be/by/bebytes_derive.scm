(define-module (crates-io be by bebytes_derive) #:use-module (crates-io))

(define-public crate-bebytes_derive-0.2.0 (c (n "bebytes_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1akcbzw28yh2adwpar0wx5c9ryi6y2311vqdm18pgff5fl9m6wc8")))

(define-public crate-bebytes_derive-0.2.1 (c (n "bebytes_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1sqiq06pa4alwrnwgf1yzf4r0xv11b4jf9b9rqs9g16cf85vrmdw")))

(define-public crate-bebytes_derive-0.2.2 (c (n "bebytes_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1x9k3wzas35dc20xpxrgx3vxlfn6rnhnfv2cvpmrlb9kfaj7ykg6")))

(define-public crate-bebytes_derive-0.2.3 (c (n "bebytes_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0smwfvldv5a99jh4d1p8ljqxsvzj41ckdknr1ndjn47drqi53xrn")))

(define-public crate-bebytes_derive-0.2.5 (c (n "bebytes_derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0azh35i1fd6l2i98l50x7qpckva41s7xzpwfqkk77x534h30ibx4")))

(define-public crate-bebytes_derive-0.2.6 (c (n "bebytes_derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0nl28sx41rg9d32kim6iyih9nhbxfh9rrid5aqqka2gb2xa1c3s1")))

(define-public crate-bebytes_derive-0.2.7 (c (n "bebytes_derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "127ry9fr19s8b0majvdy8as23czrwfw9lsyfgzyb9pig6gpfc5lm")))

(define-public crate-bebytes_derive-0.2.8 (c (n "bebytes_derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0jm51f2xx7sgdqyjmcrly04ngh0yrp6azdzk512gd6a0z5b3bbj9") (y #t)))

(define-public crate-bebytes_derive-0.2.9 (c (n "bebytes_derive") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "18n3slycjqqchnf53jl75mcx2gcf6fg01r0nyzcn1294n0p671q5") (y #t)))

(define-public crate-bebytes_derive-0.2.10 (c (n "bebytes_derive") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1f0b98hix858mc4xjpvyv94bqhs6hfcg1h1y23zvnqp12mhkw77l") (y #t)))

(define-public crate-bebytes_derive-0.2.12 (c (n "bebytes_derive") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "187g2vh5rlqgi9k5kpvkadd0j8f5r5l6vir5p4nqv05v75s8rqn1")))

(define-public crate-bebytes_derive-0.2.13 (c (n "bebytes_derive") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "19vxvc05045lrflb9hchh4d50ag262zl1vh39sv9yy2wbqwmx6f4")))

(define-public crate-bebytes_derive-0.2.14 (c (n "bebytes_derive") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1q5206fgfaj7lq0cfavh3b6jir3pzqwwv10l8kdrqwglazk19gy9")))

(define-public crate-bebytes_derive-0.3.0 (c (n "bebytes_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "04q12vdb0bvg3lzf3majcj48d6ymz27fnkbvfxm95dc7nkapm5i0")))

(define-public crate-bebytes_derive-0.4.0 (c (n "bebytes_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1fdyb5k925vck9dja2g7x5rhsgdw2axmfjisbidvvirv0wcxbra3")))

