(define-module (crates-io be by bebytes) #:use-module (crates-io))

(define-public crate-bebytes-0.1.0 (c (n "bebytes") (v "0.1.0") (d (list (d (n "bebytes_derive") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1j5wwc17sh8hjv3c8x27jvb8xw1i13z46rl2s1fi7ms742gq3mi0")))

(define-public crate-bebytes-0.1.1 (c (n "bebytes") (v "0.1.1") (d (list (d (n "bebytes_derive") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0wgmim5fl6j4yzsxrgdcs7n6aq56zii2jwyr9aq47nizahdfixpp")))

(define-public crate-bebytes-0.1.2 (c (n "bebytes") (v "0.1.2") (d (list (d (n "bebytes_derive") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1vb2312485gk43alik21xzg8z8nk733s8917yk2dr3p17wz61zmz")))

(define-public crate-bebytes-0.1.3 (c (n "bebytes") (v "0.1.3") (d (list (d (n "bebytes_derive") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "104g1x84p33d2i247039yvybzqhw6r7jg7rmynj04clvgvb9vjc4")))

(define-public crate-bebytes-0.1.4 (c (n "bebytes") (v "0.1.4") (d (list (d (n "bebytes_derive") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "08s5zxpld26vhgi8lgjz3mbdqqrq40sbzmbfvnd139d0qvzxkd2l")))

(define-public crate-bebytes-0.1.5 (c (n "bebytes") (v "0.1.5") (d (list (d (n "bebytes_derive") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cysyrr8qjqjs3viq905abksl3pqphwfbkb73q8apq26xcls03fq")))

(define-public crate-bebytes-0.2.0 (c (n "bebytes") (v "0.2.0") (d (list (d (n "bebytes_derive") (r "^0.3") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "09rymz23g5r6329j87bnw83zvg5l6dccrff96bhq3ddhi71za2k4")))

(define-public crate-bebytes-0.2.1 (c (n "bebytes") (v "0.2.1") (d (list (d (n "bebytes_derive") (r "^0.4") (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "10xbnnqp39rqw5r91ablcrrj9n76cxsdbgvb7jn2vrz50z59sv1r")))

