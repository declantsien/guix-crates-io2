(define-module (crates-io be rz berzelius) #:use-module (crates-io))

(define-public crate-berzelius-0.0.0 (c (n "berzelius") (v "0.0.0") (d (list (d (n "berzelius-lints") (r "^0.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "102rgh5v1hg2sf3z9qqc83ld1zlj3jnnzn5inm66kw6b2rsrfl98") (y #t)))

