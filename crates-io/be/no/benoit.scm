(define-module (crates-io be no benoit) #:use-module (crates-io))

(define-public crate-benoit-3.0.1 (c (n "benoit") (v "3.0.1") (d (list (d (n "enum-iterator") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (o #t) (d #t) (k 0)))) (h "1krj9pl3bajbnipydsmwxfhnl3q4qq3vkdga8f2p9ilcv8zwxy5m") (f (quote (("wgpu-colour" "wgpu"))))))

(define-public crate-benoit-3.0.2 (c (n "benoit") (v "3.0.2") (d (list (d (n "enum-iterator") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "rug") (r "^1.24.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "073xpadh5gs3754qcj7v8s555mzzwk9w5m9mmrxr7f87y1xn8mss") (f (quote (("wgpu-colour" "wgpu"))))))

(define-public crate-benoit-3.0.3 (c (n "benoit") (v "3.0.3") (d (list (d (n "enum-iterator") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "rug") (r "^1.24.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1x4n51d1lddhl1r06qwc713q4jpphh0dj3x6smgv6xkjnw09v9xv") (f (quote (("wgpu-colour" "wgpu"))))))

