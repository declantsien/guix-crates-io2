(define-module (crates-io be rt bert_tokenizer) #:use-module (crates-io))

(define-public crate-bert_tokenizer-0.1.0 (c (n "bert_tokenizer") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "0gzc7gcsahknqp4mm9sw96ji83yksrvzljsa8ssg1igpv5hb7gkd") (y #t)))

(define-public crate-bert_tokenizer-0.1.1 (c (n "bert_tokenizer") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "19cdzancnlmkr8mwha0ijnhq6nylg0nzyssjpq0gk7aw13fpfh2a") (y #t)))

(define-public crate-bert_tokenizer-0.1.2 (c (n "bert_tokenizer") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "00sqipaz5wx714q452w24xfcddza3s6jr0ilb6wnsrzbjip4fbaa") (y #t)))

(define-public crate-bert_tokenizer-0.1.3 (c (n "bert_tokenizer") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "1k1qiq7s4az5bs5cs93wka9l8b5a40dygvkrlryln09ghfxgnhfl")))

