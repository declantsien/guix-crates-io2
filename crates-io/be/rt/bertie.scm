(define-module (crates-io be rt bertie) #:use-module (crates-io))

(define-public crate-bertie-0.0.0 (c (n "bertie") (v "0.0.0") (h "1blxj7shdfhw6068ry7di5rh6qw9h87mvcprph5rsw1mqdah2j6l")))

(define-public crate-bertie-0.1.0-pre.2 (c (n "bertie") (v "0.1.0-pre.2") (d (list (d (n "backtrace") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "dhat") (r "^0.3.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libcrux") (r "^0.0.2-pre.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0md9dkvj5vl435hm9qr409nk9jhzqr6ydks30k1sxwwbg7mj0bj3") (f (quote (("test_utils") ("secret_integers") ("default" "secret_integers" "api") ("api"))))))

