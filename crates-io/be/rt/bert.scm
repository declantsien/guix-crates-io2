(define-module (crates-io be rt bert) #:use-module (crates-io))

(define-public crate-bert-0.1.0 (c (n "bert") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "serde") (r "^0.8.7") (d #t) (k 0)) (d (n "serde_macros") (r "0.8.*") (d #t) (k 2)))) (h "1wwkbil9xlg9x02n2bjvqkniqyvplsbdyfkp2sjk7g4421dz8yry")))

