(define-module (crates-io be la belajar-say-hello) #:use-module (crates-io))

(define-public crate-belajar-say-hello-0.1.0 (c (n "belajar-say-hello") (v "0.1.0") (h "0qipizkc7qx7ixy7hsnmb4zk2icw63gl1aaw315pvxfh8l5930xv")))

(define-public crate-belajar-say-hello-0.2.0 (c (n "belajar-say-hello") (v "0.2.0") (h "0sf7ldqyhg1mfrknj4ry7s1jwmhxr19kyx500p021ngj2pfg83gb")))

(define-public crate-belajar-say-hello-0.3.0 (c (n "belajar-say-hello") (v "0.3.0") (h "1f2mrm0qydj3iw0iaj5s84vdp57p7qq9gqp6rcvw7r012n645an4") (f (quote (("hello") ("default" "hello") ("bye") ("all" "hello" "bye"))))))

