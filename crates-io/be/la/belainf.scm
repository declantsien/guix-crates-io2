(define-module (crates-io be la belainf) #:use-module (crates-io))

(define-public crate-belainf-0.1.0 (c (n "belainf") (v "0.1.0") (d (list (d (n "sysinfo") (r "^0.24.1") (d #t) (k 0)))) (h "0jf780gilisf9v3pj37r8j87k21zc5zxwv92m1k2x17xgm95akad")))

(define-public crate-belainf-0.1.1 (c (n "belainf") (v "0.1.1") (d (list (d (n "sysinfo") (r "^0.24.1") (d #t) (k 0)))) (h "1gvc4l4wbl2x1gd2ipikf0crmq81fnfas314wk5wgp96zzcm6zly")))

(define-public crate-belainf-0.1.2 (c (n "belainf") (v "0.1.2") (d (list (d (n "sysinfo") (r "^0.24.1") (d #t) (k 0)))) (h "1dckfkr71rdkf6f8ax90yv0brl9h7d1ia027fpn14b8jznkg0gd3")))

