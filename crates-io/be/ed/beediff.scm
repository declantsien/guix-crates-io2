(define-module (crates-io be ed beediff) #:use-module (crates-io))

(define-public crate-beediff-0.1.0 (c (n "beediff") (v "0.1.0") (h "0qvmd74bd7b144vm7d129nnlcim4d6gskwdj2apqyvdh396254v6")))

(define-public crate-beediff-0.1.1 (c (n "beediff") (v "0.1.1") (h "0jzbq4ahsgk15sxip13q12maf66c1d2qknd50z578ff3kdy0qiaf")))

(define-public crate-beediff-0.1.2 (c (n "beediff") (v "0.1.2") (h "0mikmksq8y81w605182d9j5gxjl02v4n6mb797f2al820nbqjsda")))

