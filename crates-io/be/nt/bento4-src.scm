(define-module (crates-io be nt bento4-src) #:use-module (crates-io))

(define-public crate-bento4-src-0.1.0 (c (n "bento4-src") (v "0.1.0") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)))) (h "15nkh4j905xkj268ffz013wcvn1s1267bn2i6sgxr5g120qvr9h0")))

(define-public crate-bento4-src-0.1.1 (c (n "bento4-src") (v "0.1.1") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)))) (h "0ln4z9f05fdhlnw0x9sp2659ghv8wi94fndh422hs94fjwl5w638")))

(define-public crate-bento4-src-0.1.2 (c (n "bento4-src") (v "0.1.2") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)))) (h "0sh5g2nn2kbvc5d85zk38q0fidsi1hzh7n67l98ahz9qa2najj1n")))

