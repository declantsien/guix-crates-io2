(define-module (crates-io be nt bentobox) #:use-module (crates-io))

(define-public crate-bentobox-0.1.0 (c (n "bentobox") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)) (d (n "tinyrand-alloc") (r "^0.5.0") (d #t) (k 2)))) (h "0qjnn1ajalbzk4gphqsllyg49mzffj2adhcab85knm1chmvm24fw")))

(define-public crate-bentobox-0.1.1 (c (n "bentobox") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)) (d (n "tinyrand-alloc") (r "^0.5.0") (d #t) (k 2)))) (h "0j4b5mdmzm5apsn289x9izcizxl7klfpbqz0l9jx9v7ap2yzsfxs")))

