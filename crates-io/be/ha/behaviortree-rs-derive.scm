(define-module (crates-io be ha behaviortree-rs-derive) #:use-module (crates-io))

(define-public crate-behaviortree-rs-derive-0.1.0 (c (n "behaviortree-rs-derive") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0lxg8rqldsv2xxzlwyqazxvp7p9k6wpdhmpywnziyd5160kh49z9")))

(define-public crate-behaviortree-rs-derive-0.2.0 (c (n "behaviortree-rs-derive") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "07b2k930q7ackbhr35lszpc9dm66jm6ncm8g25mxjl05051i9wkn")))

(define-public crate-behaviortree-rs-derive-0.2.1 (c (n "behaviortree-rs-derive") (v "0.2.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "14bqk39bjdgj54wxyh4fdcx900zxnmkyzi1k443yjz71jcvq8vrm")))

(define-public crate-behaviortree-rs-derive-0.2.2 (c (n "behaviortree-rs-derive") (v "0.2.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "visit-mut" "visit"))) (d #t) (k 0)))) (h "0i556r91w05vj3zpvxyhczz3x8dp142rj7yyxgkiddjhc8vc3pbz")))

(define-public crate-behaviortree-rs-derive-0.2.3 (c (n "behaviortree-rs-derive") (v "0.2.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "visit-mut" "visit"))) (d #t) (k 0)))) (h "0jldzsmzihlnkajn1xjigwxrfayy23fy18f4g81va2d6d2hlnr75")))

(define-public crate-behaviortree-rs-derive-0.2.4 (c (n "behaviortree-rs-derive") (v "0.2.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "visit-mut" "visit"))) (d #t) (k 0)))) (h "1dlx9s88kk7b7jrw2gli6x7kj5bn1waijinnkwspsxwhkqx63di9")))

