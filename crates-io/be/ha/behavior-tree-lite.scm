(define-module (crates-io be ha behavior-tree-lite) #:use-module (crates-io))

(define-public crate-behavior-tree-lite-0.3.2 (c (n "behavior-tree-lite") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)))) (h "14dapww14hjrxvvkf7p6pq0x04awchlfgbjvswaa91kjv6rvzmg0")))

