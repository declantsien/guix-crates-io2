(define-module (crates-io be ha behavior-tree) #:use-module (crates-io))

(define-public crate-behavior-tree-0.0.1 (c (n "behavior-tree") (v "0.0.1") (h "1j3wkpc3rlyc3s4li8cwkqcq7nz6vg5y9cyqmmvlslih0s286f5b")))

(define-public crate-behavior-tree-0.0.2 (c (n "behavior-tree") (v "0.0.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1r1l287l1ik20skdnni4hrp8j7vn97fp0yddk46c9k8sm6pakk4h")))

(define-public crate-behavior-tree-0.0.3 (c (n "behavior-tree") (v "0.0.3") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1xp6bjsw3a2k0jn22m2npg07gmvg9ir1h0jkb4a64h576yzh2dl1")))

(define-public crate-behavior-tree-0.0.4 (c (n "behavior-tree") (v "0.0.4") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1rnawwxr2dds5c40vwbnz48xana3b9av3brnjgkymnz4y4nr3nsk")))

(define-public crate-behavior-tree-0.0.5 (c (n "behavior-tree") (v "0.0.5") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05ipyygh81r2madwhwiq440wbkyj41mh8rbb4lndr0zgsix1xb9b")))

(define-public crate-behavior-tree-0.0.6 (c (n "behavior-tree") (v "0.0.6") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "18bdk1908gr7wcgddi7kpacnqp4fvmp6zgn9r09gg2g0bzdny1x3")))

(define-public crate-behavior-tree-0.0.7 (c (n "behavior-tree") (v "0.0.7") (d (list (d (n "puffin") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1664v00d9kx6s6sq2y9bw9dr2368hlgqzcjmyl0q0lbz2di2v1bi") (f (quote (("profiling" "puffin"))))))

(define-public crate-behavior-tree-0.0.8 (c (n "behavior-tree") (v "0.0.8") (d (list (d (n "puffin") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "04nvpkj754g4k3gyvndb37igdzdn42g9mrxsdhjrm572c7p4s615") (f (quote (("profiling" "puffin"))))))

(define-public crate-behavior-tree-0.1.0 (c (n "behavior-tree") (v "0.1.0") (d (list (d (n "puffin") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0j089wdg4x25l0sgjaav5awalc8b5cihcrhwn0mny13vss9vdmdd") (f (quote (("profiling" "puffin"))))))

