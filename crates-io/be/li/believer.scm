(define-module (crates-io be li believer) #:use-module (crates-io))

(define-public crate-believer-0.1.0 (c (n "believer") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "1gwz88km89cpv0xrbfsnq80074pkglaqpldggjhlx0gpvr77v6cv")))

(define-public crate-believer-0.1.1 (c (n "believer") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "160yggkwbaw4p1w9g9dz27jfirhl54fn0mc5gjdhxl510f1g2vzf")))

(define-public crate-believer-0.1.2 (c (n "believer") (v "0.1.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "03x8hyivf7l7ci7ppvy1jw4wwabnd2rg4xv0b0qd1nalgnmyd1sv")))

(define-public crate-believer-0.1.3 (c (n "believer") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "0agh1n0ihbqd19f08518g2nj6b4j9piwncf1lhrmw027sji5f6k5")))

(define-public crate-believer-0.1.4 (c (n "believer") (v "0.1.4") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "0n7z99y20p947s1y4g4x6ga43c8ykg9488lmz80iblx6xbr54vy1")))

(define-public crate-believer-0.2.0 (c (n "believer") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)))) (h "0a8ridg4vq4vzllsbrn4gqw1gchyr7mh70lfjh35yp72rm9nbq9v")))

(define-public crate-believer-0.2.1 (c (n "believer") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)))) (h "1wdsljdw37s0x4c04yswcgzjbk5s6r709xs6vq81xl7nrslfdzhj")))

