(define-module (crates-io be li belief-spread) #:use-module (crates-io))

(define-public crate-belief-spread-0.0.1 (c (n "belief-spread") (v "0.0.1") (d (list (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "02hm85kpqs1wiicq6iqvgw654fan79jajppzxl41p621yia27g4c")))

(define-public crate-belief-spread-0.1.0 (c (n "belief-spread") (v "0.1.0") (d (list (d (n "mockall") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0bgjqcwx9vvndv5in8a3bdz594if526binywh8mk04r47yjvdrsb")))

(define-public crate-belief-spread-0.2.0 (c (n "belief-spread") (v "0.2.0") (d (list (d (n "mockall") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "04hwwxgy9fyqqav51cglmlk9k5f9kr6al9difx29rs985cbpvcw8")))

(define-public crate-belief-spread-0.3.0 (c (n "belief-spread") (v "0.3.0") (d (list (d (n "mockall") (r "^0.11.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "13n16vm4n9sq2h2y05kp0gczz5sqfra9rmzmqaqh7sdjwbx55s6r")))

(define-public crate-belief-spread-0.4.0 (c (n "belief-spread") (v "0.4.0") (d (list (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0nkldwypr4lwfwk4ksd8rhz5h2scv9lypw6y4pmspxcqh3926lbi")))

(define-public crate-belief-spread-0.4.1 (c (n "belief-spread") (v "0.4.1") (d (list (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0pjkr2gdk7l68alp141ji0d3wa84nccadm4m5br1g7y5xyvxczl8")))

(define-public crate-belief-spread-0.5.0 (c (n "belief-spread") (v "0.5.0") (d (list (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "18rlfzqb9xqqqnzwnyiz489bhkjjnlc5yv1n3i3ic26z4b4n2caa")))

(define-public crate-belief-spread-0.6.0 (c (n "belief-spread") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0gjj6131jynzkxczljjbpiyp2pfd8a715k38x95i23n68lc1naww")))

(define-public crate-belief-spread-0.7.0 (c (n "belief-spread") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1l1p6i8hx6hx109mr950hcyryjlra55i0sh515x2xzchjc6rylgw")))

(define-public crate-belief-spread-0.8.0 (c (n "belief-spread") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0g0zy1sw9w5r5kgwlqnlf8frca0yfc7yivq8xa23rrmch77d2w1h")))

(define-public crate-belief-spread-0.9.0 (c (n "belief-spread") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "13pz0phw3am7zjbzwh35y01cf9ljxb9w6xk6h6qsv1q3ljcppsmh")))

(define-public crate-belief-spread-0.9.1 (c (n "belief-spread") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1rinq14gf3z6lpv6n00pys2c0dp3w0w9fdcb4gh9k8c9a46v0wx9")))

(define-public crate-belief-spread-0.10.0 (c (n "belief-spread") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1wfvpg6r3q91524wg1ldcjajxc23r4a861yqwjsahhd2kx45cyid")))

(define-public crate-belief-spread-0.10.1 (c (n "belief-spread") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0j8imr65jcfrhb4s7gvpk4a42yvrcs1sirv6iwnyknjy2s2w629i")))

(define-public crate-belief-spread-0.11.0-pre1 (c (n "belief-spread") (v "0.11.0-pre1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1faab6irjw5ycryb97c9sl2xsa16krl10in2f6r25yyms2flfgi8")))

(define-public crate-belief-spread-0.11.0-pre3 (c (n "belief-spread") (v "0.11.0-pre3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0n85b6wmsbdm070ajkqlnvw6k0dkxpkdx70shgxl5nx0g1mzb1rp")))

(define-public crate-belief-spread-0.11.0-pre4 (c (n "belief-spread") (v "0.11.0-pre4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0b8x8200w0j53i8vs5b1a2jvnv309a676h44xj6c8jblc9aj11yd")))

(define-public crate-belief-spread-0.11.0-pre5debug1 (c (n "belief-spread") (v "0.11.0-pre5debug1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1df28ffl31a03qw5wc21znlpcjm3jpmpvic2ilckdax893g55846")))

(define-public crate-belief-spread-0.11.0-pre5debug2 (c (n "belief-spread") (v "0.11.0-pre5debug2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "01pvz5795z5jb1qk9kh5f55r3w059nw2bi30z30xf8g6qcglvd7j")))

(define-public crate-belief-spread-0.11.0-pre5debug3 (c (n "belief-spread") (v "0.11.0-pre5debug3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "10m3cy690yqx5wal1hybjbrf13yilib46kh4w47gj2ms04k2wcb8")))

(define-public crate-belief-spread-0.11.0-pre5debug4 (c (n "belief-spread") (v "0.11.0-pre5debug4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0d6sh2i5sw8slsqhwhd381kinwpwak86s3x9y6khhh9p5v5fq36v")))

(define-public crate-belief-spread-0.11.0-pre5debug4.1 (c (n "belief-spread") (v "0.11.0-pre5debug4.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1p720yj7ny4439r4cnkqx679bjs437iwwpcyvdfqg6kffgik901s")))

(define-public crate-belief-spread-0.11.0-pre5debug5 (c (n "belief-spread") (v "0.11.0-pre5debug5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0x9fqb70ini5ykjwp80yd3yrzip4f5gycid1f4320rzh0kayg7xj")))

(define-public crate-belief-spread-0.11.0-pre5debug6 (c (n "belief-spread") (v "0.11.0-pre5debug6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0crvapfyy38wakgzg0ay2ijlvbq6pkxnmgixzrpsskljn2vmz6bb")))

(define-public crate-belief-spread-0.11.0-pre5debug7 (c (n "belief-spread") (v "0.11.0-pre5debug7") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1j4cdbgsnr5n7dam2plrspyps9l38g0kb3h4lbvpmysmib229r3h")))

(define-public crate-belief-spread-0.11.0-pre5debug8 (c (n "belief-spread") (v "0.11.0-pre5debug8") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "030c40pxw8hcxh4vs6awpfhlbgiar0l6yvs13nyj0a19adv3wgw6")))

(define-public crate-belief-spread-0.11.0-pre5debug9 (c (n "belief-spread") (v "0.11.0-pre5debug9") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1j8zxn79ajc060r3qvx8c0vp7z5sfarr6kbnw23b0iz619csx64y")))

(define-public crate-belief-spread-0.11.0-pre6 (c (n "belief-spread") (v "0.11.0-pre6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "0m8kvgc72hbqbp2yl90dd9scnds2nyhh9gm8v7b0wvpk0023p4f4")))

