(define-module (crates-io be ta betadin) #:use-module (crates-io))

(define-public crate-betadin-0.1.0 (c (n "betadin") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)))) (h "0fmw23q6s286h8n5hrnw787biaq7d1xzf81bq42kjjxf86rdb2sf")))

(define-public crate-betadin-0.1.1 (c (n "betadin") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)))) (h "1slmxaz71969k5w1d4r4g9ig8a3zbxzvy43a4s631fi867bqvjag")))

(define-public crate-betadin-0.1.2 (c (n "betadin") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)))) (h "04mnaszkb7dcf9z1rl765x2q52blfg3x1zd4h3bqga5h5zkabadz")))

(define-public crate-betadin-0.2.0 (c (n "betadin") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)))) (h "0f1zqfhgpxbw3w885h1kzgkkbp3iygbj8d6hynnsmvgia182sdrr")))

