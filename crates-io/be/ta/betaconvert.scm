(define-module (crates-io be ta betaconvert) #:use-module (crates-io))

(define-public crate-betaconvert-0.1.0 (c (n "betaconvert") (v "0.1.0") (d (list (d (n "betacode") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "04q1zl6hwlsyq1zm3c9xr92xjypi31jf0n01a6lzi8l3dh815khz")))

(define-public crate-betaconvert-0.1.1 (c (n "betaconvert") (v "0.1.1") (d (list (d (n "betacode") (r ">=1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xalq5z5q0l1j71sxpvbvm7dhcv9vnzpa7kyxy4rdcf41zqsycw9")))

(define-public crate-betaconvert-0.1.4 (c (n "betaconvert") (v "0.1.4") (d (list (d (n "betacode") (r ">=1.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k72dmnx73x59hkcqasg30nf19ixk6vxiqhb0kya8i4r8qma6bia")))

