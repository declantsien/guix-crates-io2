(define-module (crates-io be ta betacode2) #:use-module (crates-io))

(define-public crate-betacode2-0.1.0 (c (n "betacode2") (v "0.1.0") (h "1mvpm6i695bz92m7w1w0ax5is7zc9bd0ykxa79pdi3hlm0gz2cs8")))

(define-public crate-betacode2-0.1.2 (c (n "betacode2") (v "0.1.2") (h "0mgl7amxz8h86r7bg839fpqqz9chm6879hk7vpiv9rjzfqj42f7c")))

(define-public crate-betacode2-0.1.3 (c (n "betacode2") (v "0.1.3") (h "0zp7j6r6i3bfq0id3hc9aymdxvmic655sn7h4r8awyfqg68kjxv4")))

(define-public crate-betacode2-0.1.4 (c (n "betacode2") (v "0.1.4") (h "1zy7mvzmcyv6k6jbps3vlyd3wfb5s3glssywady5r307q9zmxh4l")))

