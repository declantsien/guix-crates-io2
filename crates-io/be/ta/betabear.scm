(define-module (crates-io be ta betabear) #:use-module (crates-io))

(define-public crate-betabear-0.1.0 (c (n "betabear") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.48") (d #t) (k 0)))) (h "09xqgcxvp8c1x09y8yi4353s38ps1vkii2x7zb2kws14vvllknwa")))

(define-public crate-betabear-0.1.1 (c (n "betabear") (v "0.1.1") (d (list (d (n "regex") (r "^0.1.48") (d #t) (k 2)))) (h "1k9iidqm777i464pl58zh5js0217x1ihm0ddlcqdfz1q256nlnsi") (f (quote (("unstable"))))))

