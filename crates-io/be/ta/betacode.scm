(define-module (crates-io be ta betacode) #:use-module (crates-io))

(define-public crate-betacode-0.1.0 (c (n "betacode") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1jvgyy9lnr5ab9qg2ns0wsdrcbxli9jmwbpzidlrn1zhdqs26sc7") (y #t)))

(define-public crate-betacode-0.1.1 (c (n "betacode") (v "0.1.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "13a9i083jdddprzqcj944sjymw2kghaxinh9k0ys6gwzjyl9pj13") (y #t)))

(define-public crate-betacode-0.1.2 (c (n "betacode") (v "0.1.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0cmsmfxlfq4xbg94claqgbplnzxr6v75kxbbvrpfq97ldzximll7") (y #t)))

(define-public crate-betacode-0.1.3 (c (n "betacode") (v "0.1.3") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "17b3sc8aq7y0glyc8ym32gcjzmycclfx0djy23g7x1b3nan8l8cc") (y #t)))

(define-public crate-betacode-0.1.4 (c (n "betacode") (v "0.1.4") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0ag6s5jjkwpgn56c6hg13m98fw3cpgm9gbkhax1xkax4pfnz529h") (y #t)))

(define-public crate-betacode-0.1.5 (c (n "betacode") (v "0.1.5") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1zcmymn0l03cf1s00ximvla5285lhx52nahxrgicd0s6xw02zhp6") (y #t)))

(define-public crate-betacode-0.1.6 (c (n "betacode") (v "0.1.6") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1b47dyajxz4q8wp9z424ghbga7d7qq52clh4fy641vadmy23rzzm")))

(define-public crate-betacode-0.1.7 (c (n "betacode") (v "0.1.7") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1l2vi8gdr3bcrw7hgq6kbpvsxbfphzn1qsplinbbvkhs33rwb7b3")))

(define-public crate-betacode-0.1.8 (c (n "betacode") (v "0.1.8") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "1qglwysr1jzhixw4f0dsqfa3pz2m2ny9a9drqip86ybjj51d0i8p")))

(define-public crate-betacode-0.2.0 (c (n "betacode") (v "0.2.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "0msdva1yps2cg8bcb12f54vxhpn3ffmylq20ycycmcai8vd87xis")))

(define-public crate-betacode-1.0.0 (c (n "betacode") (v "1.0.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "0ldbmbwbxrs10nz26p7vc8yb2kkf396rkxsr7ah4qliq4b3cqa67")))

(define-public crate-betacode-1.0.1 (c (n "betacode") (v "1.0.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "0q48wak04zszrrk1f4zyavj6x46mzc3fkckq1ypfsnn1sqhqaxzh")))

(define-public crate-betacode-1.0.2 (c (n "betacode") (v "1.0.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "1712v28iayxi1ldml4d5l9sb8xn0045hh4ybrzir6qjfmyiglg57")))

(define-public crate-betacode-1.0.3 (c (n "betacode") (v "1.0.3") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "0glmj3ybfj3h7p524ykzwpanw6s8ld7fk8as7dp8lxma9jd58jy7")))

(define-public crate-betacode-1.1.0 (c (n "betacode") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "1lw9qiphrg1ww6x1shn32ai9bgmznicpa81anr1b96qnms2j049m")))

(define-public crate-betacode-1.2.0 (c (n "betacode") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "1n7fwnychhw79y7bncnhzgc32m0dz6yz3piymglkdqcb9dqp5905")))

