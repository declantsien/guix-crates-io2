(define-module (crates-io iu no iunorm) #:use-module (crates-io))

(define-public crate-iunorm-0.1.0 (c (n "iunorm") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "0caq9qd81v66g069dy9v4qbai419sx3p4rj479pf4xiqnbqybia0") (r "1.58.1")))

(define-public crate-iunorm-0.1.1 (c (n "iunorm") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "1ycqpk0x3iazcmm7g0shddsxs1rj29mqsch8py9r0h1k5wgzl0l6") (r "1.58.1")))

(define-public crate-iunorm-0.1.2 (c (n "iunorm") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "twofloat") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1a7xsgz3m89fd823p6s6i8gwyfcqpx33nw9my1bfbay8l05y6ylm") (f (quote (("std" "twofloat")))) (r "1.58.1")))

(define-public crate-iunorm-0.2.0 (c (n "iunorm") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (k 2)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "1v79vshnkd4ydlb98wkdxbnw94l36czdfjsi5mnih9g28ghlxm91") (f (quote (("std") ("default" "std")))) (r "1.58.1")))

(define-public crate-iunorm-0.2.1 (c (n "iunorm") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.1") (k 2)) (d (n "paste") (r "^1.0.10") (d #t) (k 0)))) (h "0jb0i25c3ywwg6nnwav4qhax1gvkqmh8qnjdfply8xpq314hp634") (f (quote (("std") ("default" "std")))) (r "1.43.1")))

(define-public crate-iunorm-0.2.2 (c (n "iunorm") (v "0.2.2") (d (list (d (n "approx") (r "^0.5.1") (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "18is7cdry6m6rcglm90m2714y23khcsw200h5svcxfgzn7zaix2s") (f (quote (("std") ("nightly_docs" "std") ("default")))) (r "1.43.1")))

