(define-module (crates-io iu p- iup-sys) #:use-module (crates-io))

(define-public crate-iup-sys-0.0.1 (c (n "iup-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "02n4bvpsd6za08hwh9l3nh5lkccm1arnyynll5hw3xczsqa79z9f")))

(define-public crate-iup-sys-0.0.2 (c (n "iup-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1ngps0zx1dli24cc5q46mz9d1fr5d6r2aqs9cdfl5j60yvprc734")))

(define-public crate-iup-sys-0.0.3 (c (n "iup-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "13xn8dlbix33knc6j2xqx4qcx9wr6l19wphnv4z6qfv4vjjx3ngj")))

