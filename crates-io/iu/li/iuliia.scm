(define-module (crates-io iu li iuliia) #:use-module (crates-io))

(define-public crate-iuliia-0.1.0-beta.0 (c (n "iuliia") (v "0.1.0-beta.0") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0bab64x9m6x5d6pnd99cm0d3bd6csmlvsxzxjjqkh2kvfv1n98zg")))

(define-public crate-iuliia-0.1.0-beta.1 (c (n "iuliia") (v "0.1.0-beta.1") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "05pmgj8v6vzbwv76hjmwwq91lbxnxirrd66gzg8a36g02ryjs8gk")))

(define-public crate-iuliia-0.1.0-beta.2 (c (n "iuliia") (v "0.1.0-beta.2") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "084hs18zrj5qg4spflx2w8zx1vd3brdgj4zcz0n2gh48dcwl06bj")))

