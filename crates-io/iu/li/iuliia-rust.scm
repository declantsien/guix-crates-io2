(define-module (crates-io iu li iuliia-rust) #:use-module (crates-io))

(define-public crate-iuliia-rust-0.1.0 (c (n "iuliia-rust") (v "0.1.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11jw8yk7yxwdpax9la81mbs1g58a324hazvhhy0hhfj5xxaqdgxc")))

(define-public crate-iuliia-rust-0.1.1 (c (n "iuliia-rust") (v "0.1.1") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1svc9lqz232dc8zdqpms36bmm94i7v5idpjhrhxiwqi6r8mi2gxh")))

