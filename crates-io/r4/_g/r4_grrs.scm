(define-module (crates-io r4 _g r4_grrs) #:use-module (crates-io))

(define-public crate-r4_grrs-0.1.0 (c (n "r4_grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "18hlayvswy2zs09p3h3vma75lh3ad9wfzhr0hwi1i3vzcy7lm3q0")))

(define-public crate-r4_grrs-0.1.1 (c (n "r4_grrs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "01c3c97w272ixwbw8vpiiqpmhagry13bwm8y059rypa695kggck3")))

