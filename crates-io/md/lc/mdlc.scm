(define-module (crates-io md lc mdlc) #:use-module (crates-io))

(define-public crate-mdlc-0.4.0 (c (n "mdlc") (v "0.4.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "19lbp28kq7dzir76b37mi9c9jnbfdy8cywyj7xg573yw7xszmq1g")))

(define-public crate-mdlc-0.5.0 (c (n "mdlc") (v "0.5.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "clap") (r ">2.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "regex") (r "~1.3") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "073wiv29r7l8v436jyx8cwyw9xa6jning3ssbp2px82x3ym2zwmx")))

(define-public crate-mdlc-0.6.0 (c (n "mdlc") (v "0.6.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "regex") (r "~1.3") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)))) (h "0fydic7c96ah2m84srsmmp259xilhzi2fn4yrhvipfs476z34fvr")))

(define-public crate-mdlc-0.7.0 (c (n "mdlc") (v "0.7.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "rayon") (r "~1.5") (d #t) (k 0)) (d (n "regex") (r "~1.3") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)))) (h "1jbfwyyfv039iy4vk6m1k07xbyxbz128w2h8b0kgivmshdg9lwz1")))

(define-public crate-mdlc-0.8.1 (c (n "mdlc") (v "0.8.1") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "rayon") (r "~1.5") (d #t) (k 0)) (d (n "regex") (r "~1.3") (d #t) (k 0)) (d (n "reqwest") (r "~0.11.23") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)))) (h "1081v69yv7cvch9pxz75cdh3i6qgvk6bmfcni533wqaxwjg972va")))

