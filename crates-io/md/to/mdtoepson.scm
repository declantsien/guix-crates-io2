(define-module (crates-io md to mdtoepson) #:use-module (crates-io))

(define-public crate-mdtoepson-0.1.0 (c (n "mdtoepson") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "11bdf9dn83dvplsrxz9k775qmdfdazgy7879j90jvhrsrjig05f4")))

(define-public crate-mdtoepson-0.1.1 (c (n "mdtoepson") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "118x40cha0hb5ky2isann85w7d99sdwm5w1yl723xpzzccla1lcg")))

(define-public crate-mdtoepson-0.1.2 (c (n "mdtoepson") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "1qp4yvc1rv0wycq17r8y5fwlpmh0q3cka7lh428n2iwd1aqimcg4")))

