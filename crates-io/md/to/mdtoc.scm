(define-module (crates-io md to mdtoc) #:use-module (crates-io))

(define-public crate-mdtoc-0.1.0 (c (n "mdtoc") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "15g5yzf7ljywfhacp9dcz7mljf6hnk7k44a3c5wj0b5s9fw8cj2k")))

(define-public crate-mdtoc-0.2.0 (c (n "mdtoc") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "14z70n2myddfwbr15kswq402a0qs7wikq2q7qvwbz400wbd0cm4z")))

(define-public crate-mdtoc-0.2.1 (c (n "mdtoc") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1kyq3jg4g11q6g03ahzvxgz9m9463iqg7819s2phc3z6fnjdfpmh")))

