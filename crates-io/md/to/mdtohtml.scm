(define-module (crates-io md to mdtohtml) #:use-module (crates-io))

(define-public crate-mdtohtml-1.0.0 (c (n "mdtohtml") (v "1.0.0") (h "1czygzf0gn194y147frlxp1x9v7d264f3zanljwviwqfzp0ib1pz") (y #t)))

(define-public crate-mdtohtml-1.1.0 (c (n "mdtohtml") (v "1.1.0") (h "0ami2byqklb87zwrahwn7bq95asg9qfmx8fs1c58l4zsmj8kf7fk") (y #t)))

(define-public crate-mdtohtml-2.0.0 (c (n "mdtohtml") (v "2.0.0") (h "0c179kfnwpq2pjaqm66shljc8bcgkkfqhrsw38p7s8wx98nfclc8")))

