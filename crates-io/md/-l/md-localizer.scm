(define-module (crates-io md -l md-localizer) #:use-module (crates-io))

(define-public crate-md-localizer-0.1.0 (c (n "md-localizer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "18xy1xh1qsskwha1yfyav44pgd4l9nlc8q123l933cjnk1lkhjdk")))

(define-public crate-md-localizer-0.1.1 (c (n "md-localizer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1glh5dsvsg2r8h5zwx5a9qwz7lzblbd0jxjmyg4rqqq1whsvyvm0")))

