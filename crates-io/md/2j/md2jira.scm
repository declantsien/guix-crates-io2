(define-module (crates-io md #{2j}# md2jira) #:use-module (crates-io))

(define-public crate-md2jira-0.1.0 (c (n "md2jira") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0j40icw348hgli3ykam422i3x46d5p7wpqrvwvccvk6wa491jgzz")))

(define-public crate-md2jira-0.1.1 (c (n "md2jira") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1h33hz1b5qd6wy47jrj8hkg1s9mygj08rk8i35bl5r1l1v4d4akd")))

