(define-module (crates-io md d- mdd-lib) #:use-module (crates-io))

(define-public crate-mdd-lib-0.1.0 (c (n "mdd-lib") (v "0.1.0") (h "1f4kz6v4b07x8aqi6032zwczgb0spllqzyz0jbxfd4xa6aak5gm6")))

(define-public crate-mdd-lib-0.1.1 (c (n "mdd-lib") (v "0.1.1") (h "0r7jrcf2s238cvfyv2ra7n2gbx837rv7grd3lh0vckg7kvvspxhm")))

(define-public crate-mdd-lib-0.1.2 (c (n "mdd-lib") (v "0.1.2") (h "14s6plbx9mip30qwz1cwcvnrkdfg0hrhcp4im71l6mdkangccshn")))

(define-public crate-mdd-lib-0.1.3 (c (n "mdd-lib") (v "0.1.3") (h "14nnw6frpy7prvzjq462p97l23a6mgacrmshw4cvr3qwwxl7khp1")))

(define-public crate-mdd-lib-0.1.4 (c (n "mdd-lib") (v "0.1.4") (h "0v2wamps03hnssl21jgfif334w4fygighva6l357zw2xls76pq1d")))

(define-public crate-mdd-lib-0.1.5 (c (n "mdd-lib") (v "0.1.5") (h "10xxjnyypw869kvcp63lyhhwrhf9ra057pz50ivh2b1wghk3nxmk")))

(define-public crate-mdd-lib-0.1.6 (c (n "mdd-lib") (v "0.1.6") (h "0fqh8lzzg967s7ay7hgr50zfzaycfzx86354s9pvfwy64csxf9d4")))

(define-public crate-mdd-lib-0.1.7 (c (n "mdd-lib") (v "0.1.7") (h "00vlgjw6hxmnsj5yl25vy923yq8yaal6ig4plazfsjcyj4i2g413")))

(define-public crate-mdd-lib-0.1.8 (c (n "mdd-lib") (v "0.1.8") (h "1r8ci2pll2sxqcbx53y7pvkcl2fyjli1hnp1j6hgm0lqaf9ppi6i")))

