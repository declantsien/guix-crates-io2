(define-module (crates-io md d- mdd-soundcloud) #:use-module (crates-io))

(define-public crate-mdd-soundcloud-0.1.0 (c (n "mdd-soundcloud") (v "0.1.0") (d (list (d (n "mdd-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0ci3v2862f10qhxqrd5m2l35gnvazz1m21kzr16i6pvfmzrs166x")))

(define-public crate-mdd-soundcloud-0.1.1 (c (n "mdd-soundcloud") (v "0.1.1") (d (list (d (n "mdd-lib") (r "^0.1.1") (d #t) (k 0)))) (h "1ia04d5fq5h6jv2m2gzx5mbfcfnpih8ljg5k24c30x4li7i191ml")))

(define-public crate-mdd-soundcloud-0.1.2 (c (n "mdd-soundcloud") (v "0.1.2") (d (list (d (n "mdd-lib") (r "^0.1.2") (d #t) (k 0)))) (h "1h1c44s8fypd85zbajpjifyqwkmdiwy68by6bcirizp3j96h4ndl")))

(define-public crate-mdd-soundcloud-0.1.3 (c (n "mdd-soundcloud") (v "0.1.3") (d (list (d (n "mdd-lib") (r "^0.1.2") (d #t) (k 0)))) (h "0d961gr4wp7zwlx64p99bzcjr59y15hnzspyd4gqrb5dz0vawga5")))

(define-public crate-mdd-soundcloud-0.1.4 (c (n "mdd-soundcloud") (v "0.1.4") (d (list (d (n "mdd-lib") (r "^0.1.2") (d #t) (k 0)))) (h "1m8ghfpwpp0vg6il4m9flddbwbmwhp8w8qpvw6wn0m9bk196r5z1")))

(define-public crate-mdd-soundcloud-0.1.6 (c (n "mdd-soundcloud") (v "0.1.6") (d (list (d (n "mdd-lib") (r "^0.1.3") (d #t) (k 0)))) (h "1i6vzbwxkdan2pidxrhm2yh5b1z75ba8bpdib5lvzahbqinkbbbg")))

(define-public crate-mdd-soundcloud-0.1.7 (c (n "mdd-soundcloud") (v "0.1.7") (d (list (d (n "mdd-lib") (r "^0.1.3") (d #t) (k 0)))) (h "0mdzg3s10x08gayvylkzmd0zhbraw7mcvwglc2dh38v6nvlpvgg9")))

(define-public crate-mdd-soundcloud-0.1.8 (c (n "mdd-soundcloud") (v "0.1.8") (d (list (d (n "mdd-lib") (r "^0.1.3") (d #t) (k 0)))) (h "07zzk42p8cir844fbrqvng9zxa4bbhzqda07jfn6118v5k3b6nmg")))

(define-public crate-mdd-soundcloud-0.1.9 (c (n "mdd-soundcloud") (v "0.1.9") (d (list (d (n "mdd-lib") (r "^0.1.3") (d #t) (k 0)))) (h "1kma2vf9sp619k7m4sjsgzh2bz73ldyp72bwcmxg5ngzmq3g4chn")))

(define-public crate-mdd-soundcloud-0.1.10 (c (n "mdd-soundcloud") (v "0.1.10") (d (list (d (n "mdd-lib") (r "^0.1.4") (d #t) (k 0)))) (h "0zhhy22adxh34qy6yp37qiarp8iqgxaq3wp5m0nlif3dxyc26fba")))

(define-public crate-mdd-soundcloud-0.1.11 (c (n "mdd-soundcloud") (v "0.1.11") (d (list (d (n "mdd-lib") (r "^0.1.5") (d #t) (k 0)))) (h "0j0k8p2rpjbll9r2cjjp8ckpsb4rwjpc5irpaifsxnk9amg5995w")))

(define-public crate-mdd-soundcloud-0.1.12 (c (n "mdd-soundcloud") (v "0.1.12") (d (list (d (n "mdd-lib") (r "^0.1.5") (d #t) (k 0)))) (h "18j1a3yv6cmhpgg7qjimjrvxmh7mdw65pys277b2v56gcli5y4n6")))

(define-public crate-mdd-soundcloud-0.1.13 (c (n "mdd-soundcloud") (v "0.1.13") (d (list (d (n "mdd-lib") (r "^0.1.5") (d #t) (k 0)))) (h "193n2wkdvkmas2afirjpv3888aczjf56v8c86kxgpxs1nbq0ks52")))

(define-public crate-mdd-soundcloud-0.1.14 (c (n "mdd-soundcloud") (v "0.1.14") (d (list (d (n "mdd-lib") (r "^0.1.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.1") (d #t) (k 0)))) (h "065rkz2dxhqr6frv8pnky24895mqb8cf4nznn7cwbbq5vr9plgjm")))

(define-public crate-mdd-soundcloud-0.1.15 (c (n "mdd-soundcloud") (v "0.1.15") (d (list (d (n "mdd-lib") (r "^0.1.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.1") (d #t) (k 0)))) (h "1hv4ylbpwln14fv33xfdqlh20apq6wf7mhdl8pqb4g55gqs3km69")))

