(define-module (crates-io md _h md_htm) #:use-module (crates-io))

(define-public crate-md_htm-0.1.0 (c (n "md_htm") (v "0.1.0") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "htmlize") (r "^1.0.4") (f (quote ("unescape_fast"))) (d #t) (k 0)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "rany") (r "^0.1.5") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0gz1gvqr8fy4r15m8ii91dw8qmk5m7qirlhpfci948kwjwgbrbkj")))

