(define-module (crates-io md o- mdo-future) #:use-module (crates-io))

(define-public crate-mdo-future-0.1.0 (c (n "mdo-future") (v "0.1.0") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "futures-cpupool") (r "0.1.*") (d #t) (k 2)) (d (n "mdo") (r "^0.3") (d #t) (k 2)))) (h "0mb017x094i87xhf8bb9d9ax3lw3c302a0pz2bhknvwzymlw0l9x")))

(define-public crate-mdo-future-0.2.0 (c (n "mdo-future") (v "0.2.0") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "futures-cpupool") (r "0.1.*") (d #t) (k 2)) (d (n "mdo") (r "^0.3") (d #t) (k 2)))) (h "0id8rgh12pgmmc6i8c9vyjykcwcsww70fdg5xjsw16isvh4a9qxk")))

