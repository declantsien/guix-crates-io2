(define-module (crates-io md tg mdtg) #:use-module (crates-io))

(define-public crate-mdtg-0.1.0 (c (n "mdtg") (v "0.1.0") (h "1whiccfb0hhxc9k2xp0hz30zyklf5spm64xkbx02ps86vylagjk1") (y #t)))

(define-public crate-mdtg-1.0.0 (c (n "mdtg") (v "1.0.0") (h "1iyrnfpg1ciqavicvyj7pk8ixi6zjfjps7p1yywvpzrdn51f0fc7")))

(define-public crate-mdtg-1.0.1 (c (n "mdtg") (v "1.0.1") (h "16qsbnb0p4xgxj8zy0zshccm9xirwh4pk4im33mfhnjaa7f4cq2x")))

