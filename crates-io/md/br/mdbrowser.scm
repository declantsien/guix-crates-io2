(define-module (crates-io md br mdbrowser) #:use-module (crates-io))

(define-public crate-mdbrowser-0.1.0 (c (n "mdbrowser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gettid") (r "^0.1.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (f (quote ("simd"))) (k 0)))) (h "018s8kkbmkjn2vfhb0p6k46dcml645k045pg5f75f69vbgmrjl0a")))

(define-public crate-mdbrowser-0.2.0 (c (n "mdbrowser") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (f (quote ("simd"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1amhl14ysgp3dl0s8xq44mbbz2skpsbpisy6mp20163ha66bx8cl")))

(define-public crate-mdbrowser-0.5.0 (c (n "mdbrowser") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (f (quote ("simd"))) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1zaqldqcnqs2s7x6zw6aa96df6qpynn4six7rbi79s231zbsc9qs")))

