(define-module (crates-io md #{2h}# md2html) #:use-module (crates-io))

(define-public crate-md2html-0.1.0 (c (n "md2html") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "maud") (r "^0.22.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "000055bh5rcca2j5ijssl53c0112p9d6wy4lmcysqiwhbvinqbwz")))

(define-public crate-md2html-0.1.1 (c (n "md2html") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "maud") (r "^0.22.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "0y48b95p68b09f14n0w2g3dk2kgxv9m6z28xxy0kwp7pmn6vk3jq")))

(define-public crate-md2html-0.1.2 (c (n "md2html") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "maud") (r "^0.22.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "1ca5bkqf9hrh197rwp4wgvz0d1gjshb29hqi1i6b54cqc47pznvg")))

