(define-module (crates-io md re mdrend-dont-use-test) #:use-module (crates-io))

(define-public crate-mdrend-dont-use-test-0.1.0 (c (n "mdrend-dont-use-test") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "maud") (r "^0.21.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)))) (h "129wqb6g51pbpksnqji0mxwmdpfnhv16k4lm47q75f86hc21rwbn")))

