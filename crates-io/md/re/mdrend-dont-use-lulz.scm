(define-module (crates-io md re mdrend-dont-use-lulz) #:use-module (crates-io))

(define-public crate-mdrend-dont-use-lulz-0.1.0 (c (n "mdrend-dont-use-lulz") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "maud") (r "^0.22.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "09chc652va6v9fbijxhw9wwmmqp9wrbvkbz60vs1dqbaabm519yf")))

