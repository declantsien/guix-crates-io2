(define-module (crates-io md re mdread-donot-use-test-only) #:use-module (crates-io))

(define-public crate-mdread-donot-use-test-only-0.1.0 (c (n "mdread-donot-use-test-only") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "maud") (r "^0.22") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.2") (d #t) (k 0)))) (h "0j5b145f7yrnylwzf6ndwrq7mk45dzdcg8950prall15zkp9wfr7")))

