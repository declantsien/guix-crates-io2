(define-module (crates-io md re mdrend) #:use-module (crates-io))

(define-public crate-mdrend-0.1.0 (c (n "mdrend") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "maud") (r "^0.20.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (d #t) (k 0)))) (h "0gfsrbw84wd5dqrfk96zwb90fqwml0jd3l5xfbcim2j36pmm0bv1")))

