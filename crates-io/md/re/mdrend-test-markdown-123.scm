(define-module (crates-io md re mdrend-test-markdown-123) #:use-module (crates-io))

(define-public crate-mdrend-test-markdown-123-0.1.0 (c (n "mdrend-test-markdown-123") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "maud") (r "^0.20.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.4.1") (d #t) (k 0)))) (h "0zfidzf0zz8ccay6dvnn743krqc9kik3a264g8bsmlaz6pzxv0lw")))

