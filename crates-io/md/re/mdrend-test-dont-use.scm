(define-module (crates-io md re mdrend-test-dont-use) #:use-module (crates-io))

(define-public crate-mdrend-test-dont-use-0.1.0 (c (n "mdrend-test-dont-use") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "maud") (r "^0.21.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)))) (h "1w8ynh9jc0iv6hysdagnsfigcvn5jx475xw02fk7vi1xb9yn6va3")))

