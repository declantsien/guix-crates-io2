(define-module (crates-io md re mdrend-dont-use-my-version) #:use-module (crates-io))

(define-public crate-mdrend-dont-use-my-version-0.1.0 (c (n "mdrend-dont-use-my-version") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "maud") (r "^0.22.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "03jv5gfpmjhxmvjdic0s58kxwc110bks3qd77nq6yja5l4kfdmj2") (y #t)))

