(define-module (crates-io md re mdrend-learning-project-not-for-public-use) #:use-module (crates-io))

(define-public crate-mdrend-learning-project-not-for-public-use-0.1.0 (c (n "mdrend-learning-project-not-for-public-use") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "maud") (r "^0.22.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.2") (d #t) (k 0)))) (h "07jr4fa70g3vpbrsrwvmxrj0mwvnwsjfm42wscidb4289821ny42")))

