(define-module (crates-io md re mdrend-this-is-only-a-beta-demo) #:use-module (crates-io))

(define-public crate-mdrend-this-is-only-a-beta-demo-0.1.0 (c (n "mdrend-this-is-only-a-beta-demo") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (d #t) (k 0)) (d (n "maud") (r "^0.25.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)))) (h "1ngpmc3zc2f8vpzbf3hgnm7382pgbiqxk2d3y9z0pfqrnqm0mqk5")))

