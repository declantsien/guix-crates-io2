(define-module (crates-io md re mdrend-dont-use-test-senju) #:use-module (crates-io))

(define-public crate-mdrend-dont-use-test-senju-0.1.0 (c (n "mdrend-dont-use-test-senju") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "maud") (r "^0.20.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.4.1") (d #t) (k 0)))) (h "0hq7bm0waf3ia7r5jskarzp10r76lbd6g8dmnkz29wf7qhqj8g4q")))

