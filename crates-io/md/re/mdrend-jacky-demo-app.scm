(define-module (crates-io md re mdrend-jacky-demo-app) #:use-module (crates-io))

(define-public crate-mdrend-jacky-demo-app-0.1.0 (c (n "mdrend-jacky-demo-app") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "maud") (r "^0.20.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.4.1") (d #t) (k 0)))) (h "1h1wwjxgiamz5b3f3hh74654jkcsqgpr1yf8qpwhia0jrhwdy81k")))

