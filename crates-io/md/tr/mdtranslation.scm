(define-module (crates-io md tr mdtranslation) #:use-module (crates-io))

(define-public crate-mdtranslation-0.1.0 (c (n "mdtranslation") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "1nhvqfya5dh0lig2nl8nijyqix3rmd77mknlq9mxhlddimjbh8d5")))

(define-public crate-mdtranslation-0.1.1 (c (n "mdtranslation") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "09b3zczkcfxy5fbb2vx09609lrj40saghyasn4h56qhb4kyrxij8")))

(define-public crate-mdtranslation-0.1.2 (c (n "mdtranslation") (v "0.1.2") (d (list (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "1q47j5h1bxwdil2hxk6r15z67pjfj75j2hisdlb8nlccyav0iif9")))

