(define-module (crates-io md tr mdtrans) #:use-module (crates-io))

(define-public crate-mdtrans-0.1.0 (c (n "mdtrans") (v "0.1.0") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0gm7wr86wxkylm1bpgqk06mrallp103hvh10g238w8gaf0zc5s12")))

(define-public crate-mdtrans-0.1.1 (c (n "mdtrans") (v "0.1.1") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "03hvsmvc9dhp4zlxd7j6cs5vgwyjr9cn28nzpnmbnd80cm9qk0jk")))

(define-public crate-mdtrans-0.1.2 (c (n "mdtrans") (v "0.1.2") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1y4x045h639h8dviijqsb2wakzffrlm7rsbj3517kmdxlya832g0") (y #t)))

(define-public crate-mdtrans-0.1.3 (c (n "mdtrans") (v "0.1.3") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "17d3m0kh6c5pnxzklz1v2c3xbazl6a8b1d1hxp84s2pgymcbw7m4")))

(define-public crate-mdtrans-0.1.4 (c (n "mdtrans") (v "0.1.4") (d (list (d (n "pest") (r "^2.7.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0xc6r3sjq3ciwvy41l6108p0j4v78l3ajslmcpadygnfi7c6hszp")))

(define-public crate-mdtrans-0.1.5 (c (n "mdtrans") (v "0.1.5") (d (list (d (n "pest") (r "^2.7.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0hy719lwg3gp7nmhx52zrdnsbsh9ajg1wlg9rqckl2rn9ypcnkiq")))

