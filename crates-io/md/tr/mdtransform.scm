(define-module (crates-io md tr mdtransform) #:use-module (crates-io))

(define-public crate-mdtransform-0.1.0 (c (n "mdtransform") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0bqnxn1kmyrz84nqixdn68l5vfrbslpf9w2wfaj6bw6cc895www1")))

(define-public crate-mdtransform-0.2.0 (c (n "mdtransform") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0wjkhnh3bc34vjmp9lmkhfy4fpxfr62shg04f79qk7n2v9kq1mzb")))

(define-public crate-mdtransform-0.2.1 (c (n "mdtransform") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0iszsq91hm9ky0sjmsgkzwgn1pbs0m4gm3pg0dprckyv8nswnl0z")))

