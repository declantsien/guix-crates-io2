(define-module (crates-io md tr mdtranslation-cli) #:use-module (crates-io))

(define-public crate-mdtranslation-cli-0.1.0 (c (n "mdtranslation-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "mdtranslation") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1fd6h7hj3qv5fpw3g4m6l9f513663cfy7c2nxlwignyqkmn7wvay")))

