(define-module (crates-io md ev mdev-parser) #:use-module (crates-io))

(define-public crate-mdev-parser-0.1.0 (c (n "mdev-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.30") (d #t) (k 0)))) (h "0lbj7l2wd9xq82a6jphdafdy70h8n5fv6zwhzg1kvkijdf4mfajk")))

(define-public crate-mdev-parser-0.1.1 (c (n "mdev-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "pest") (r "^2.7.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.7") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1my3y83gb8ii73c4gms5inpq3qbymg7aal8kwq98mavyihw1xqsf")))

