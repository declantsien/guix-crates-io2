(define-module (crates-io md dd mddd-macro) #:use-module (crates-io))

(define-public crate-mddd-macro-0.1.0 (c (n "mddd-macro") (v "0.1.0") (d (list (d (n "mddd-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1npn8apa0hw96lj2wh0a7584j71bw8a7y6h2iymw22blhrxzf1d6")))

(define-public crate-mddd-macro-0.1.1 (c (n "mddd-macro") (v "0.1.1") (d (list (d (n "mddd-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17f8pg1fs4rcdyls2gzp7qb3g1dbjcqfiyyn06sjr4xh30p6si6x")))

(define-public crate-mddd-macro-0.1.2 (c (n "mddd-macro") (v "0.1.2") (d (list (d (n "mddd-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lxkjl5l2wjfv23ngp9xnl92wczwn0z1sgs37dfyxsd3j79d8ngz")))

(define-public crate-mddd-macro-0.2.0 (c (n "mddd-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fkrrf2l4jyvb55qaycif7pw53qnq4zp1hvnadwh78fcfskzz0b4")))

(define-public crate-mddd-macro-0.2.1 (c (n "mddd-macro") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10l53l4417m36c7ik0l0bf80nhg1zz36afw0s3224b65fynsh45k")))

(define-public crate-mddd-macro-0.2.2 (c (n "mddd-macro") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11nmnwnc9kl06a3yshi8ly7wgqr1wxr0bsy2gdzvl7gksilvf8ck")))

(define-public crate-mddd-macro-0.2.3 (c (n "mddd-macro") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "025gby3v8kwgz0cj9rwjyp035af38kbrcs5yymi54dwxyb5qdgx8")))

(define-public crate-mddd-macro-0.3.0 (c (n "mddd-macro") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k1skba32mhkcjjnv8iv3hgqka32sqr89vfbrnncdqb573llxw15")))

(define-public crate-mddd-macro-0.3.1 (c (n "mddd-macro") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yrrvlfc9c7zr9px46aqs1gqhh2ys0vv0hdf4gww3ppvya25nxgl")))

(define-public crate-mddd-macro-0.3.2 (c (n "mddd-macro") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xcmr8xc1qwqn2gcfgjka2f7sh1h92p66h639qb9yy7pwfw7dgph")))

