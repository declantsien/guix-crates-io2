(define-module (crates-io md dd mddd-std) #:use-module (crates-io))

(define-public crate-mddd-std-0.1.0 (c (n "mddd-std") (v "0.1.0") (d (list (d (n "mddd-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j1y7qbnd4ij0jy4i04vc2lfm933rqgccf8vf1j9z7vpgxik4nwm")))

(define-public crate-mddd-std-0.2.0 (c (n "mddd-std") (v "0.2.0") (d (list (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hz949525w8cwdbnhlh46z7l535ka8n6993c73c27rqaa1in4vc1")))

(define-public crate-mddd-std-0.2.1 (c (n "mddd-std") (v "0.2.1") (d (list (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aa9ig0s6bx4gzzs3ff32jw723bp5pfrbsqnzfm7hy9h96k412yg")))

(define-public crate-mddd-std-0.2.2 (c (n "mddd-std") (v "0.2.2") (d (list (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "166vhgpfg2dq66b1b8cq8h5a079syjnx9agp7y08gsqi8v31hqpw")))

(define-public crate-mddd-std-0.2.3 (c (n "mddd-std") (v "0.2.3") (d (list (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n3zf9cplzqpcn0sxy8splqk0j3w9y6s044gjadkgk4j38inpsbv")))

(define-public crate-mddd-std-0.2.4 (c (n "mddd-std") (v "0.2.4") (d (list (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jzg95vl2swflqkpdkbp268nrqfgy50a6h0k47132yadqcisgpr6")))

(define-public crate-mddd-std-0.3.1 (c (n "mddd-std") (v "0.3.1") (d (list (d (n "mddd-traits") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lmkdkilah742pglcgvwsdx7wfbpz5jdkw8q5iw30m5fiw53qwi8")))

