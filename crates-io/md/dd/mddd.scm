(define-module (crates-io md dd mddd) #:use-module (crates-io))

(define-public crate-mddd-0.1.0 (c (n "mddd") (v "0.1.0") (d (list (d (n "mddd-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "mddd-std") (r "^0.1.0") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.1.0") (d #t) (k 0)))) (h "0ksm2ig66c6m1mbkr8xi3lx63ga03sd6zz5nm1j5ib0pc86c0fjn") (y #t)))

(define-public crate-mddd-0.1.1 (c (n "mddd") (v "0.1.1") (d (list (d (n "mddd-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "mddd-std") (r "^0.1.0") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.1.0") (d #t) (k 0)))) (h "0bhvnb4v4rs9jxrlgjww5j3k2pp2crqrm04624n1rz0l3c5jbk48") (y #t)))

(define-public crate-mddd-0.1.2 (c (n "mddd") (v "0.1.2") (d (list (d (n "mddd-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "mddd-std") (r "^0.1.0") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.1.0") (d #t) (k 0)))) (h "1j0sr2is1gblcjdq6xsqvxddlz8gc7nl758yf4rqi28v33md801p")))

(define-public crate-mddd-0.2.0 (c (n "mddd") (v "0.2.0") (d (list (d (n "mddd-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "mddd-std") (r "^0.2.0") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0xzp2kafp2ksnbfyr6ms0n3wni8kxzi2bkjhi4n08qp80nqrxjsx") (y #t)))

(define-public crate-mddd-0.2.1 (c (n "mddd") (v "0.2.1") (d (list (d (n "mddd-macro") (r "^0.2.1") (d #t) (k 0)) (d (n "mddd-std") (r "^0.2.0") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)))) (h "02j26syhaj4av5hzwa69nlc9jd24nk57da67i1z4aljinmym7sy9") (y #t)))

(define-public crate-mddd-0.2.2 (c (n "mddd") (v "0.2.2") (d (list (d (n "mddd-macro") (r "^0.2.2") (d #t) (k 0)) (d (n "mddd-std") (r "^0.2.1") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)))) (h "00zadh994dnl9wy8qslz93jkl5wagbl4d9kh30px6pb77z5mhz64") (y #t)))

(define-public crate-mddd-0.2.3 (c (n "mddd") (v "0.2.3") (d (list (d (n "mddd-macro") (r "^0.2.2") (d #t) (k 0)) (d (n "mddd-std") (r "^0.2.2") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)))) (h "12wp6q4kzf4bdzjfyx03ya72dnnglm3pg9k702a7rqsvb7hkrwlr") (y #t)))

(define-public crate-mddd-0.2.4 (c (n "mddd") (v "0.2.4") (d (list (d (n "mddd-macro") (r "^0.2.3") (d #t) (k 0)) (d (n "mddd-std") (r "^0.2.3") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0zh03y4qyvapnsviyl455miwr4288zq9b4pd65ppsmpc0g96bfg3") (y #t)))

(define-public crate-mddd-0.2.5 (c (n "mddd") (v "0.2.5") (d (list (d (n "mddd-macro") (r "^0.2.3") (d #t) (k 0)) (d (n "mddd-std") (r "^0.2.4") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)))) (h "02rqwbbx2bfrisywmfqbyp76xm2hyarl2jclgv4pn3pf0wxg53yn")))

(define-public crate-mddd-0.3.0 (c (n "mddd") (v "0.3.0") (d (list (d (n "mddd-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "mddd-std") (r "^0.2.4") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.2.0") (d #t) (k 0)))) (h "1a5316cgw2srv2pdg4iaaw6zd490s2ff41z4dpl7fk5671jqzn4p")))

(define-public crate-mddd-0.3.1 (c (n "mddd") (v "0.3.1") (d (list (d (n "mddd-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "mddd-std") (r "^0.3.1") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.3.1") (d #t) (k 0)))) (h "1ssk8mg93jq318d77gpx77gnmzm2wxwwlc1gva3cama0g7jzp7a2") (y #t)))

(define-public crate-mddd-0.3.2 (c (n "mddd") (v "0.3.2") (d (list (d (n "mddd-macro") (r "^0.3.2") (d #t) (k 0)) (d (n "mddd-std") (r "^0.3.1") (d #t) (k 0)) (d (n "mddd-traits") (r "^0.3.1") (d #t) (k 0)))) (h "0kgwrb7lgp9r22044j06c9rxw6wa0rd6w1b44imlzazl8wflzs57")))

