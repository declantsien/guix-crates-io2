(define-module (crates-io md dd mddd-traits) #:use-module (crates-io))

(define-public crate-mddd-traits-0.1.0 (c (n "mddd-traits") (v "0.1.0") (h "1x0cgdq00jlxpdbk6pgqkn0y8wb35h8y4rrcr9ylyyipf33qmcpb")))

(define-public crate-mddd-traits-0.2.0 (c (n "mddd-traits") (v "0.2.0") (h "1m9lp9wcb76lk5sv55yqmpyn4vlry7zh3pimch192crb4sh85yaj")))

(define-public crate-mddd-traits-0.3.1 (c (n "mddd-traits") (v "0.3.1") (h "15c2g9nivfsvysxg96gw0mkhdg32by24myynbnnrghn9i42fxvbn")))

