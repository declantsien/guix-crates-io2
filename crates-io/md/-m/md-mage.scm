(define-module (crates-io md -m md-mage) #:use-module (crates-io))

(define-public crate-md-mage-0.1.0 (c (n "md-mage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "0r3nwxnh1kkbfflcppjpiqngr0bk187cjxzsqw800nj0d6skhnlh")))

