(define-module (crates-io md ic mdict-parser) #:use-module (crates-io))

(define-public crate-mdict-parser-0.1.0 (c (n "mdict-parser") (v "0.1.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "compress") (r "^0.2.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "minilzo-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "ripemd") (r "^0.1.3") (d #t) (k 0)) (d (n "salsa20") (r "^0.10.2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (d #t) (k 0)))) (h "11q01ss0j8idygbcf79qwwrnrfvjp5i2sfyypqv291zrlnm7s01y")))

(define-public crate-mdict-parser-0.1.1 (c (n "mdict-parser") (v "0.1.1") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "compress") (r "^0.2.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "minilzo-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "ripemd") (r "^0.1.3") (d #t) (k 0)) (d (n "salsa20") (r "^0.10.2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (d #t) (k 0)))) (h "1zbvncdbvxq9qr7sm5pzx7l7zkrldyql0y58zyr8j9b72yizplpf")))

