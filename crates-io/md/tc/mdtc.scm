(define-module (crates-io md tc mdtc) #:use-module (crates-io))

(define-public crate-mdtc-0.1.0 (c (n "mdtc") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "02pfvi4z31vjzhcvmklhpb9y0brn09kdsw3n4bscp0lglrigggx5")))

