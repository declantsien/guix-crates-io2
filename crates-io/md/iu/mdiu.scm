(define-module (crates-io md iu mdiu) #:use-module (crates-io))

(define-public crate-mdiu-0.1.0 (c (n "mdiu") (v "0.1.0") (d (list (d (n "http") (r "^0.2.6") (d #t) (k 0)))) (h "0wbyzlpkikdkfgsgn430m4nys521x3pl3r0154060r5dbrgl7j1n") (f (quote (("markdown") ("html"))))))

(define-public crate-mdiu-0.1.1 (c (n "mdiu") (v "0.1.1") (d (list (d (n "http") (r "^0.2.6") (d #t) (k 0)))) (h "1kcda8finmwqzkcrsh6a05sa1i831cx7vkblwfn6khvp9kz633j3") (f (quote (("markdown") ("html"))))))

(define-public crate-mdiu-0.1.2 (c (n "mdiu") (v "0.1.2") (d (list (d (n "http") (r "^0.2.6") (d #t) (k 0)))) (h "1jzmyjfi2x1whiwa7li4s1cza7mk5a5kialbxc1kb0jz4zq77pgd") (f (quote (("markdown") ("html"))))))

