(define-module (crates-io md #{2p}# md2pdf) #:use-module (crates-io))

(define-public crate-md2pdf-0.0.0 (c (n "md2pdf") (v "0.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.2") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.11") (d #t) (k 0)))) (h "17zafh6wzalr5iawm10ps0yl736isp71464spinfl0k4l8hm23r5")))

(define-public crate-md2pdf-0.0.1 (c (n "md2pdf") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.2") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.11") (d #t) (k 0)))) (h "144x7chwfp9icqsrgc23dv30vw3z8j47wlcanc8k0k818jfb90an")))

(define-public crate-md2pdf-0.0.2 (c (n "md2pdf") (v "0.0.2") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.3") (d #t) (k 0)) (d (n "tectonic") (r "^0.5.2") (d #t) (k 0)))) (h "1biacrr1n9k0z2n8jasb5rvx2w9328mfmzaq5b7nwjl596lzr988")))

(define-public crate-md2pdf-0.0.3 (c (n "md2pdf") (v "0.0.3") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.3") (d #t) (k 0)) (d (n "tectonic") (r "^0.9") (d #t) (k 0)))) (h "0qxfahqg8rdggsf5wbq5l1bm645a27vxkx4hrqgf3qzfwi60nxld")))

