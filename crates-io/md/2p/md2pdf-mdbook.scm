(define-module (crates-io md #{2p}# md2pdf-mdbook) #:use-module (crates-io))

(define-public crate-md2pdf-mdbook-0.0.1 (c (n "md2pdf-mdbook") (v "0.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.2") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1229phw2y1lhy56ny9v6xy48ihbp8b62mjmy7lrklavnwxncq8hh")))

