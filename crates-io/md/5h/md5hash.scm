(define-module (crates-io md #{5h}# md5hash) #:use-module (crates-io))

(define-public crate-md5hash-0.1.0 (c (n "md5hash") (v "0.1.0") (h "0gwqm1fyzn48r0jb8330ni5g0rirl5sbf4jmkyqqpazch3sph5xn")))

(define-public crate-md5hash-0.1.1 (c (n "md5hash") (v "0.1.1") (h "166qqxcqvzv4d14bwn4kw244l28i1zrc4lkm5i0zcpbxlbjqzajf")))

