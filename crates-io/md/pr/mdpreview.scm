(define-module (crates-io md pr mdpreview) #:use-module (crates-io))

(define-public crate-mdpreview-0.1.0 (c (n "mdpreview") (v "0.1.0") (d (list (d (n "open") (r "^1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.11") (k 0)))) (h "02b2pp35bkz2rvhcz0mhv3f9s8l3l54hkjbrzick3q10cbwkzqcx")))

