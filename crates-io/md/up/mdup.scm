(define-module (crates-io md up mdup) #:use-module (crates-io))

(define-public crate-mdup-0.0.0 (c (n "mdup") (v "0.0.0") (h "1mhgrs682hxy5ds012aphnyyxama8xlzyi3s9jw2zd8qzrx3lv09")))

(define-public crate-mdup-0.0.1 (c (n "mdup") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.1") (d #t) (k 0)))) (h "0da7rfqc8yqfmnsyhdzz7rm1284fblllfcfpmn4j751ajfsqmga5")))

