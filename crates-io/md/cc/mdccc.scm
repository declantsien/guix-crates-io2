(define-module (crates-io md cc mdccc) #:use-module (crates-io))

(define-public crate-mdccc-0.1.0 (c (n "mdccc") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.1.2") (d #t) (k 0)))) (h "1xnr772601wk2nmi2llhmzwr6jrr8ryna5mwjxags8nhg86szyyk")))

(define-public crate-mdccc-0.1.1 (c (n "mdccc") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.1.2") (d #t) (k 0)))) (h "1w4dhkp6bn928xrzv40ss5l38g96575pgh9vl4mmglxynpaw8x74")))

