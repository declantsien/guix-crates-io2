(define-module (crates-io md #{2n}# md2nb) #:use-module (crates-io))

(define-public crate-md2nb-0.1.0 (c (n "md2nb") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.1") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.2.1") (d #t) (k 0)) (d (n "wolfram-expr") (r "^0.1.1") (d #t) (k 0)) (d (n "wstp") (r "^0.2.0") (d #t) (k 0)))) (h "0j4mfn37agrrgj7dbky02j0d2rqskx5fhl4lf1h3bbaiypi5dh4l")))

