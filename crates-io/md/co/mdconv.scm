(define-module (crates-io md co mdconv) #:use-module (crates-io))

(define-public crate-mdconv-0.1.0 (c (n "mdconv") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "comrak") (r "^0.7") (d #t) (k 0)))) (h "147kihkysmjbpsb18m7rfic8r38qszd3yv3cnmy487a9lvgabkqm")))

(define-public crate-mdconv-0.2.0 (c (n "mdconv") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "comrak") (r "^0.7") (d #t) (k 0)))) (h "0nqp5dnihva19y00lg158glnvy0sw3wy29pxlq3s19a0zbbavyv5")))

(define-public crate-mdconv-0.3.0 (c (n "mdconv") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.1") (d #t) (k 0)))) (h "15lznp4cyx9hmmxjmvi8mj23hd92hbrb20gjy5bkfa1s0yc0262k")))

