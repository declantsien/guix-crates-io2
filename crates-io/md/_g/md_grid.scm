(define-module (crates-io md _g md_grid) #:use-module (crates-io))

(define-public crate-md_grid-0.1.0 (c (n "md_grid") (v "0.1.0") (d (list (d (n "prettytable-rs") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1fmgj5j4cwa3a3f88cn1r4a94s1vdyqsqdhrinddiks16azn3550") (f (quote (("prettytable" "prettytable-rs") ("default"))))))

(define-public crate-md_grid-0.2.0 (c (n "md_grid") (v "0.2.0") (d (list (d (n "prettytable-rs") (r "^0.8") (o #t) (d #t) (k 0)))) (h "04ib13m676ixdhchckv28icxw08mrwrp17q2i7m1znzgx5gf6h2n") (f (quote (("prettytable" "prettytable-rs") ("default"))))))

(define-public crate-md_grid-0.2.1 (c (n "md_grid") (v "0.2.1") (d (list (d (n "prettytable-rs") (r "^0.8") (o #t) (d #t) (k 0)))) (h "15j5ns3ccjllkm5254mdis697sapxfmil7gnjinwzx1zhj0hnfnm") (f (quote (("prettytable" "prettytable-rs") ("default"))))))

