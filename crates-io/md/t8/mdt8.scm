(define-module (crates-io md t8 mdt8) #:use-module (crates-io))

(define-public crate-mdt8-0.1.1 (c (n "mdt8") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xdg") (r "^2") (d #t) (k 0)))) (h "10mmqz5493zms4m7a1ci689bw355pv6f1z59crp4lk8gdzp61aj2")))

(define-public crate-mdt8-0.2.0 (c (n "mdt8") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xdg") (r "^2") (d #t) (k 0)))) (h "11z84fwh40xlzvhf5lgsyfmmscnbp5g267mhs9bq610l6cj4lw8b")))

