(define-module (crates-io md if mdify) #:use-module (crates-io))

(define-public crate-mdify-0.1.0 (c (n "mdify") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "comrak") (r "^0.18.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1mqvbbhikaf461v6zfklp96ma9nwg5y3fi44nq67fmmy2184y6fb")))

(define-public crate-mdify-0.2.0 (c (n "mdify") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "comrak") (r "^0.18.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "17q2r4b8nmndq0m03khjjx641hbc42pwpwd1i4234afyqss3g6nv")))

(define-public crate-mdify-0.3.0 (c (n "mdify") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "comrak") (r "^0.18.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1ikpim3z5xy1f0fm8mx5cy8x3ryq7a4n6bphwjfj696vywgyyr1j")))

(define-public crate-mdify-0.4.0 (c (n "mdify") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "comrak") (r "^0.18.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "1vg6xibjpf96jjzjrnaka2hd51mqa8wk6fjcmvms3vqywbcrpr09")))

(define-public crate-mdify-0.4.1 (c (n "mdify") (v "0.4.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "comrak") (r "^0.18.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "1xxv7kw5z7gmbd265qcxb5ywg4qfdbjfvjxya8wv9q83l7immfrz")))

