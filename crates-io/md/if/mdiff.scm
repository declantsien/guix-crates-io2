(define-module (crates-io md if mdiff) #:use-module (crates-io))

(define-public crate-mdiff-0.1.0 (c (n "mdiff") (v "0.1.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "gio") (r "^0.7.0") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.7.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "sourceview") (r "^0.7.0") (d #t) (k 0)))) (h "1khg9inxzm4qqpg6w1fr97md0f52hrbf4cas3l798slkc2b567j9")))

(define-public crate-mdiff-0.1.1 (c (n "mdiff") (v "0.1.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "gio") (r "^0.7.0") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.7.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "sourceview") (r "^0.7.0") (d #t) (k 0)))) (h "0f4n3gvkh0rz8kscq8i8hnmiypmmlg1pb19b54xf032lyz6qpzr7")))

