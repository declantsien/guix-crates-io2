(define-module (crates-io md -b md-bakery) #:use-module (crates-io))

(define-public crate-md-bakery-1.0.0 (c (n "md-bakery") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0w10jhd8qs9a0f4ihg2l6xa2mcwhm7jlm3s5hvxvgnwc1ygcq5sc")))

(define-public crate-md-bakery-1.1.0 (c (n "md-bakery") (v "1.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1pc5m6c7cqrrdzb22b3xj2w9cfh77h5ra5n8ckag7dzh3s4c4xd5")))

(define-public crate-md-bakery-1.2.0 (c (n "md-bakery") (v "1.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1ghgli6j3yhhymwghf21c9qp1lhijg70z6d0qc52qza7lrpn7mr8")))

