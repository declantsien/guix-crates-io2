(define-module (crates-io md at mdatp-diagnostic-parser) #:use-module (crates-io))

(define-public crate-mdatp-diagnostic-parser-0.1.0 (c (n "mdatp-diagnostic-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "psutil") (r "^3.3.0") (f (quote ("process"))) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_with") (r "^3.5.1") (d #t) (k 0)))) (h "00glyyxw9v5cynsh2n8dkyc8z60lzismd615dpwy9x3rr3vqjbn6")))

