(define-module (crates-io md #{2g}# md2gemtext) #:use-module (crates-io))

(define-public crate-md2gemtext-0.1.0 (c (n "md2gemtext") (v "0.1.0") (d (list (d (n "gemtext") (r "^0.2.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)))) (h "1w3ygq3pdrp788gvw4a3xrnd42ixp1yb3q3zlw75aisz91y45lis")))

