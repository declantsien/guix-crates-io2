(define-module (crates-io md ba mdbat) #:use-module (crates-io))

(define-public crate-mdbat-0.1.0 (c (n "mdbat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.9") (d #t) (k 0)) (d (n "termimad") (r "^0.26.1") (d #t) (k 0)))) (h "1yl33pdzlvay3fwbpkjzy59rdd1ad4gbmlg21v9k9rbv5jnnwwm3")))

