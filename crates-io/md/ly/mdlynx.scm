(define-module (crates-io md ly mdlynx) #:use-module (crates-io))

(define-public crate-mdlynx-0.1.0 (c (n "mdlynx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (f (quote ("simd"))) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1813ca50l6w612x0myw366arghwp6k4nbn4mk8qi22i0sdw7xa8l") (f (quote (("default" "parallel")))) (s 2) (e (quote (("parallel" "dep:rayon"))))))

