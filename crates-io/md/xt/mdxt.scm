(define-module (crates-io md xt mdxt) #:use-module (crates-io))

(define-public crate-mdxt-0.1.0 (c (n "mdxt") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1cqh8j6mhl9xv38j55y4nz64bi01cp8qxf0llwmi80g78n7l1l03")))

(define-public crate-mdxt-0.2.0 (c (n "mdxt") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1n2si8r8cn7s31gh85m7d62x29prxzfzvbsrlc5mm50igqhhg0v3")))

(define-public crate-mdxt-0.3.0 (c (n "mdxt") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0jpc9bdqjdn2d7ykd63vqpmdsjr240rjhdi6ymnb44g5hsz4fkc2")))

(define-public crate-mdxt-0.3.1 (c (n "mdxt") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0fpixj8697j07y4xra5byhpbqg1apw6qa84qq9rsjfqh5bxh70l3")))

(define-public crate-mdxt-0.3.2 (c (n "mdxt") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0afs3cai586lnkg0v00xndzchdjypj21sqxfrl0qs13lb2k9k8nj")))

(define-public crate-mdxt-0.4.0 (c (n "mdxt") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0l2pm471kxxm2dh2n8cfzhzqd802akp2mb8s6dlxslknxl0mln7v")))

(define-public crate-mdxt-0.4.1 (c (n "mdxt") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1lbb6h6l23dgqylsy9dq2s75i9gmy7dx4lfy052vyzbhab3876vn")))

(define-public crate-mdxt-0.4.2 (c (n "mdxt") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0sac2nxj6dqm3yxz8v5vgwr7hlrw5sar6b7ynvjdp48i5pymcpca")))

(define-public crate-mdxt-0.4.3 (c (n "mdxt") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "10j176lzi5w6gvspd2rnhx6b5vgl0q2ynccx8i5yx9nha9lqha7p")))

(define-public crate-mdxt-0.4.4 (c (n "mdxt") (v "0.4.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0297fys3bba5i3856j5jcv1z2hffypvxwbc6x18v5qpjg3i519wb")))

(define-public crate-mdxt-0.4.5 (c (n "mdxt") (v "0.4.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0qlxaq9c1f1qv0vms3w4ah2705mp359ja46icc8rllaqi9wx93vn")))

(define-public crate-mdxt-0.4.6 (c (n "mdxt") (v "0.4.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "17gfpb148yb0v788ynhqkxwpjkqclw2lrfmqdpj2g0wbq99xhj7i")))

(define-public crate-mdxt-0.4.7 (c (n "mdxt") (v "0.4.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0d3fkli6vxmbf2pn7y0csks2glaxvrvb2yylhsq0gzb18yzgy6f5")))

(define-public crate-mdxt-0.4.8 (c (n "mdxt") (v "0.4.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0qnnm0692hd6pzhsdx0v94misssxrxiy71hnppzdkqqwpf04b64j")))

(define-public crate-mdxt-0.4.9 (c (n "mdxt") (v "0.4.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "05n8922f5c3ihfnzf6d9cf5ddiilly6618gmyfzkk6a1gp25hp19")))

(define-public crate-mdxt-0.4.10 (c (n "mdxt") (v "0.4.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "18dla3i9aa0vfzgcpm1l7nl49jziyy2dnh6av691d72capvyn6ap")))

(define-public crate-mdxt-0.4.11 (c (n "mdxt") (v "0.4.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0xcf715fmhnrwrk0d7qahsn9130mb6dfjc1c1l7ynaz4a8z3h59c")))

(define-public crate-mdxt-0.5.0 (c (n "mdxt") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1b7inqhqcal7gjz4cnw9jms0cb6p966wj2b796wqrcaqinshf16r")))

(define-public crate-mdxt-0.5.1 (c (n "mdxt") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1i5c269bprk1nlp2lrj5c1z1hn80m6j3gvrn5clbjh2ai05padza")))

(define-public crate-mdxt-0.5.2 (c (n "mdxt") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0iap1v1abhf710nbg1aszc4hifxl15bmiba4j1nvmbg921inx4fb")))

(define-public crate-mdxt-0.5.3 (c (n "mdxt") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0jivnj9piyzrwj4wwh1zyy5dnd5jp74gm3kcpjh7sn41ymrdcmsi")))

(define-public crate-mdxt-0.5.4 (c (n "mdxt") (v "0.5.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1sdmm33404k0w5vy24vb3wlbs1ykwspsfdgw2s53857x5rb4b3a7")))

(define-public crate-mdxt-0.5.5 (c (n "mdxt") (v "0.5.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0abxb00g8bqn9g4cgvgzailhkhd6mqp3105zh8q5ixdrlqvzbs3v")))

(define-public crate-mdxt-0.5.6 (c (n "mdxt") (v "0.5.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1ggry2v0shm01mbpc0qgza1sw56wm8vx4z8k8bc43jyz26ijshcn")))

(define-public crate-mdxt-0.5.7 (c (n "mdxt") (v "0.5.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0dk3nl8f95hwn275ac9l07ca79c20rr1vcngp13ichlhvf35wz5w")))

(define-public crate-mdxt-0.5.8 (c (n "mdxt") (v "0.5.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0c6lrnckhbac696cbh2aplqqk9ayr0dxbzcxg7afk9d987wg54j6")))

(define-public crate-mdxt-0.5.9 (c (n "mdxt") (v "0.5.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0qzyvzy087nwmvjsrbblqw54kv73q4228w2941zfiw5i977ghqca")))

(define-public crate-mdxt-0.5.10 (c (n "mdxt") (v "0.5.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0pzf0y3f42jiyfzqwgav7vvfnsm5rpgqkir9kgph9m4gyzlpmb2b")))

(define-public crate-mdxt-0.5.11 (c (n "mdxt") (v "0.5.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1b1wv79d3hcb4273nc3sh4j17j63phrar2sq53ybakkcss7zbkqi")))

(define-public crate-mdxt-0.5.12 (c (n "mdxt") (v "0.5.12") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1jy9mv4krd97ir6i785jlha3h4jqak9xr5fk9zfbr029fs47jg0r")))

(define-public crate-mdxt-0.5.13 (c (n "mdxt") (v "0.5.13") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1dhd7fn8fb3s8w6v03qp4lwqf3xz9mq4zyl2yg033gs6ji9shgax")))

(define-public crate-mdxt-0.5.14 (c (n "mdxt") (v "0.5.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0cmk9czfy6d4qbinrjs4bi4fq36gl7dqyd2jb5p2rd1x0s02l1ky")))

(define-public crate-mdxt-0.5.15 (c (n "mdxt") (v "0.5.15") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0rn3l1zkpaylvsq0awv3x5qlgrsydfz7lyk78ppgvn0dz0bjjbmf")))

(define-public crate-mdxt-0.5.16 (c (n "mdxt") (v "0.5.16") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1bpv586dl363pv5zds4lmxi51ka6zw4n0kd9m5jwkp0crp44rvwq")))

(define-public crate-mdxt-0.5.17 (c (n "mdxt") (v "0.5.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "195k6kg00jz47k2z27q0h48q3j4h507ia400y2h8r63s7r90rwi8")))

(define-public crate-mdxt-0.5.18 (c (n "mdxt") (v "0.5.18") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0vwfi28gwh6rrsxgr3fgpmhjxgriyacx0hi4pc0hh0pz6i1q74q9")))

(define-public crate-mdxt-0.6.0 (c (n "mdxt") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0kfimj5zkrh2n9r5r775narqm1qs8a3y379jwncry71xxidfsnc1")))

(define-public crate-mdxt-0.6.1 (c (n "mdxt") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "16xaj1ixj39wwp1cz0jhf6zn4djzawidqq3kkx2wj2qx6wxash5p")))

(define-public crate-mdxt-0.6.2 (c (n "mdxt") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "03r4jz7fs05gb5wgah7qa29s0vffnh1yzvpskzaz96xcnyjhs7jk")))

(define-public crate-mdxt-0.6.3 (c (n "mdxt") (v "0.6.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "15h0a5y5kdrni4kynrlv998vz18fzyiwvkf2adrsdcxw6xi9j73x")))

(define-public crate-mdxt-0.7.0 (c (n "mdxt") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0wc3ggrwzd92xvv24afnqs6nif2vs9dbq12gpdh0bck6j9j985b9")))

(define-public crate-mdxt-0.7.1 (c (n "mdxt") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "063znx80f21zqcfxdn51bx1cw01m2w8ygwihzyza9147jvsljdh8")))

(define-public crate-mdxt-0.7.2 (c (n "mdxt") (v "0.7.2") (d (list (d (n "hxml") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0813mvsjzw0f8r8jypamdnw7nsn7v7lk7rv1wkpbc25w7sc70hj7")))

(define-public crate-mdxt-0.7.3 (c (n "mdxt") (v "0.7.3") (d (list (d (n "hxml") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.1.0") (f (quote ("default-fancy"))) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "10y3yyd9ka1d9wl2dvqaasciwkdk189zz80fafifc3iz33yc276h")))

