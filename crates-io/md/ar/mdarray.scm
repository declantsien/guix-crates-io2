(define-module (crates-io md ar mdarray) #:use-module (crates-io))

(define-public crate-mdarray-0.1.0 (c (n "mdarray") (v "0.1.0") (h "1lgy0krd5kx847wnrz6nqpxq4ahch48krns03j658m42jbr377kr")))

(define-public crate-mdarray-0.2.0 (c (n "mdarray") (v "0.2.0") (h "1falssxj3aqvja7lmzp8c746sgz83rxd3yy1w9km2xnxkmncdcvz")))

(define-public crate-mdarray-0.3.0 (c (n "mdarray") (v "0.3.0") (h "1ccm3nqpyanrfrxkqmkakn7g67wvd7dak5b56mnxczrprahv3mxa")))

(define-public crate-mdarray-0.4.0 (c (n "mdarray") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0vrkm2bdbkc585v0qkx7zvkk4bi9yhha2blprjpx40lx7s6bzjx2") (f (quote (("permissive-provenance") ("nightly")))) (r "1.65")))

(define-public crate-mdarray-0.5.0 (c (n "mdarray") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0lkf7s219bl7w95f86sdw5pfb2j12z1i7lfprscyzsdcxk8946a2") (f (quote (("nightly")))) (r "1.65")))

