(define-module (crates-io md _m md_match_derive) #:use-module (crates-io))

(define-public crate-md_match_derive-0.1.0 (c (n "md_match_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6p67sywzc5as2ppqz5z46z8y1q7x5b5kqxdi239d5icamvzba2")))

