(define-module (crates-io md -p md-postprocess) #:use-module (crates-io))

(define-public crate-md-postprocess-0.1.0 (c (n "md-postprocess") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0majbcfy0i2wqv9p8hiils4b8md8xldpvpiy89bq81ic26jc4sw9")))

