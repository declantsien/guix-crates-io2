(define-module (crates-io md -u md-ulb-pwrap) #:use-module (crates-io))

(define-public crate-md-ulb-pwrap-0.0.1 (c (n "md-ulb-pwrap") (v "0.0.1") (d (list (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "unicode-linebreak") (r "^0.1.4") (d #t) (k 0)))) (h "16xssnh3fn8csjsjgxpaib44sx2rd7i062qmkmdnclx4ji50mnm0")))

(define-public crate-md-ulb-pwrap-0.0.2 (c (n "md-ulb-pwrap") (v "0.0.2") (d (list (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "unicode-linebreak") (r "^0.1.4") (d #t) (k 0)))) (h "19ph7qszz4xm79fw639kx8qmxg76dsdkncafx7dv7683m7b6mbjl")))

(define-public crate-md-ulb-pwrap-0.0.3 (c (n "md-ulb-pwrap") (v "0.0.3") (d (list (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "unicode-linebreak") (r "^0.1.4") (d #t) (k 0)))) (h "1ry29b5vgiq24grijbv7kkaq1vc51v8afvsyb2ynhp8hag14bzna")))

(define-public crate-md-ulb-pwrap-0.1.0 (c (n "md-ulb-pwrap") (v "0.1.0") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "unicode-linebreak") (r "^0.1.4") (d #t) (k 0)))) (h "13rk549livbmf7v37q1cw20vfggxc5pmsf9zsbd3bv7680k3w3hh")))

(define-public crate-md-ulb-pwrap-0.1.1 (c (n "md-ulb-pwrap") (v "0.1.1") (d (list (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "unicode-linebreak") (r "^0.1") (d #t) (k 0)))) (h "1wqpnaqla9i7bkj3ab479q0n411cxbfjqxkzyp5ikiyvpz3qwkz4")))

