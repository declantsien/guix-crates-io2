(define-module (crates-io md bx mdbx-sys) #:use-module (crates-io))

(define-public crate-mdbx-sys-0.7.0 (c (n "mdbx-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.53.2") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1nj56288jm29hqs5k6bdvhsinkgd96dqy5xdhnclirnknrhab8nk")))

(define-public crate-mdbx-sys-0.7.1 (c (n "mdbx-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.53.2") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mvdvj9zrgx6v3dkbs9rpgdbs1zgwvakp3fb3n3nsiwvjfy2wp69") (f (quote (("with-fuzzer-no-link") ("with-fuzzer") ("with-asan") ("default"))))))

(define-public crate-mdbx-sys-0.8.0 (c (n "mdbx-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.53.2") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0avrr0nh3fiqq1bmy8piw35bh7mlj7j8yv36smypn1ijcx61d88i") (f (quote (("with-fuzzer-no-link") ("with-fuzzer") ("with-asan") ("default"))))))

(define-public crate-mdbx-sys-0.11.1 (c (n "mdbx-sys") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pd08954qdx6hw8fr5hvwm0byadvjz7r5vp3kbdp88mwn2b09yyn") (f (quote (("cmake-build"))))))

(define-public crate-mdbx-sys-0.11.4-git.20210105 (c (n "mdbx-sys") (v "0.11.4-git.20210105") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qcbkw2pnlhhc2p9s0ya1rmpq2cmnqrjxvc8cc7qhp1sxw6kw6xj") (f (quote (("cmake-build"))))))

(define-public crate-mdbx-sys-0.11.5 (c (n "mdbx-sys") (v "0.11.5") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x4q5i4ic9xqhayanf3skg99fksspknzlrlz3m61n0v5m9r74akl") (f (quote (("cmake-build"))))))

(define-public crate-mdbx-sys-0.11.6 (c (n "mdbx-sys") (v "0.11.6") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gpimnldbkl9v9cpkqmjbcnskk35cgjwln9x11vc5j4k1zhiwiyb") (f (quote (("cmake-build"))))))

(define-public crate-mdbx-sys-0.11.6-4 (c (n "mdbx-sys") (v "0.11.6-4") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b84rp4dkmcdzxfl9azj41cnrva3d4w6aq1lhs26gx2xlc735plx") (f (quote (("cmake-build"))))))

(define-public crate-mdbx-sys-0.11.7-6 (c (n "mdbx-sys") (v "0.11.7-6") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rwqjwb244risnfagbs0sf6c6p7aciidpzdl3di58ykmmz4r3qf6") (f (quote (("cmake-build"))))))

(define-public crate-mdbx-sys-0.11.8-0 (c (n "mdbx-sys") (v "0.11.8-0") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0031kfn4q5xrpawaah6q7kfrwr292jf5qbz61ajwz32p8kw0m11f")))

(define-public crate-mdbx-sys-0.11.9-0 (c (n "mdbx-sys") (v "0.11.9-0") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14gjd3h609y86lsk7v7a0xfkmaw4bxk8bjhgyac2c0dqs9yxlak5")))

(define-public crate-mdbx-sys-0.12.1-0 (c (n "mdbx-sys") (v "0.12.1-0") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ai00b3zd15dr0wmm65vzrmd94ykpiyfak53vnvnyj1dpn84klsx")))

(define-public crate-mdbx-sys-0.12.1-1 (c (n "mdbx-sys") (v "0.12.1-1") (d (list (d (n "bindgen") (r "^0.61") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09iyn01hg20csb097i9r7idav75ikxfrq9n4hm3hmvahp8ja93ch")))

(define-public crate-mdbx-sys-0.12.1-2 (c (n "mdbx-sys") (v "0.12.1-2") (d (list (d (n "bindgen") (r "^0.61") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jjww60h1xh0dj7xb2bqs8b0vq85pkx8pb92cb1jrsjb1hc3w0f4")))

(define-public crate-mdbx-sys-0.12.2-2 (c (n "mdbx-sys") (v "0.12.2-2") (d (list (d (n "bindgen") (r "^0.61") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03p3jz7k0jpmgwhrdmpdm315040pm4k0c4yx1l6a5bc2pb068qfn")))

(define-public crate-mdbx-sys-0.12.3-0 (c (n "mdbx-sys") (v "0.12.3-0") (d (list (d (n "bindgen") (r "^0.63") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "182kwnwicdc1r8pig5dk57fg0jz5z9cqins3cnn3kbbp72qj19hd")))

(define-public crate-mdbx-sys-0.12.3-16 (c (n "mdbx-sys") (v "0.12.3-16") (d (list (d (n "bindgen") (r "^0.63") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "078gq5jx60f054x73ngd9zxmzwib0lkgq71479cdz25j2i6hdb96")))

(define-public crate-mdbx-sys-0.12.3-16-1 (c (n "mdbx-sys") (v "0.12.3-16-1") (d (list (d (n "bindgen") (r "^0.64") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pfqhw9vxpan4jymb20l78rcpvg6kzwydpjhir3kc34rgpk6myjr")))

(define-public crate-mdbx-sys-0.12.4-0 (c (n "mdbx-sys") (v "0.12.4-0") (d (list (d (n "bindgen") (r "^0.64") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12r4rv7n4p3q5idx902zdal2s7ah0ld95w9g2z8wzz2a3bpas9z7")))

(define-public crate-mdbx-sys-0.12.4 (c (n "mdbx-sys") (v "0.12.4") (d (list (d (n "bindgen") (r "^0.65") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "042xpbdfc23n67bfay152474vhsbx6sslr0js3nqh12dvkq4smb5")))

(define-public crate-mdbx-sys-0.12.6 (c (n "mdbx-sys") (v "0.12.6") (d (list (d (n "bindgen") (r "^0.65") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sxhyhk4w6g905a478z8li9lji4gacrxrvhzzkvz3cizm2dyilm1")))

(define-public crate-mdbx-sys-0.12.7 (c (n "mdbx-sys") (v "0.12.7") (d (list (d (n "bindgen") (r "^0.66") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b15d3328b5hskr9x1iiy5pdpkggds40072ir5n69ysmsvw2k8r1")))

(define-public crate-mdbx-sys-12.7.0 (c (n "mdbx-sys") (v "12.7.0") (d (list (d (n "bindgen") (r "^0.68") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hm3c6naq8grmy7rhv25fvvcw7cam13jwnaxkl9lpkpvr1nr5vr4")))

(define-public crate-mdbx-sys-12.8.0 (c (n "mdbx-sys") (v "12.8.0") (d (list (d (n "bindgen") (r "^0.68") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cmn1sfa2v7l7fq55mw02ndfl8ixyza63xmp1aqar5v9qq0a7i92")))

(define-public crate-mdbx-sys-12.9.0 (c (n "mdbx-sys") (v "12.9.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fhw985wbmxkkhv7hkr88vp77bva572q85pgfadg2d0hlx0ab4br")))

(define-public crate-mdbx-sys-12.10.0 (c (n "mdbx-sys") (v "12.10.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w5f405d4g34lzgkgj09n08hv30f7r32achn3v0q1i19kj5gvw0l")))

