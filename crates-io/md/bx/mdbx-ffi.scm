(define-module (crates-io md bx mdbx-ffi) #:use-module (crates-io))

(define-public crate-mdbx-ffi-0.0.1 (c (n "mdbx-ffi") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.108") (d #t) (k 0)))) (h "10z1m3a4mmp6ssfj9gybwz6gzax336hcq6dm5q72xgkfzq5k5c4p")))

(define-public crate-mdbx-ffi-0.0.2 (c (n "mdbx-ffi") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.108") (d #t) (k 0)))) (h "0barqv439kgdrg6awr1b12wlqivmz1wz28pf04mc6ksgkbmsqla6")))

(define-public crate-mdbx-ffi-0.0.3 (c (n "mdbx-ffi") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.108") (d #t) (k 0)))) (h "07nr014mn87w6ramsl858y9r8f338q29qylvaral25i9ic8vf5d5")))

(define-public crate-mdbx-ffi-0.0.4 (c (n "mdbx-ffi") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.108") (d #t) (k 0)))) (h "1wd4yc02z4j91frk1k1660vxwzhq6qxcxdm9lix4f4bi1kvd667i")))

(define-public crate-mdbx-ffi-0.0.5 (c (n "mdbx-ffi") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.108") (d #t) (k 0)))) (h "150ywbpvfh7sgv116fa9h6d41xam6fvwfjl9v19v46nvz0jklgss")))

