(define-module (crates-io md bx mdbx-proc) #:use-module (crates-io))

(define-public crate-mdbx-proc-0.1.0 (c (n "mdbx-proc") (v "0.1.0") (h "11xfdbz20pn9k6f3pxpbyssh6g0kzpwvhnbli6n793gvj7s1qyyn")))

(define-public crate-mdbx-proc-0.1.1 (c (n "mdbx-proc") (v "0.1.1") (h "0czzzas9z67bl3p10bwvwrvy1a47j9x007x9xdchk7iavm30absq")))

