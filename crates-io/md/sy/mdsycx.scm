(define-module (crates-io md sy mdsycx) #:use-module (crates-io))

(define-public crate-mdsycx-0.1.0 (c (n "mdsycx") (v "0.1.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "mdsycx-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "sycamore") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (d #t) (k 0)))) (h "0fp8piyf5iyb9fc2rcsyc7033kf1asb2rzcs5z144z2gm7ff3zjp")))

