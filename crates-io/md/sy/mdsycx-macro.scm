(define-module (crates-io md sy mdsycx-macro) #:use-module (crates-io))

(define-public crate-mdsycx-macro-0.1.0 (c (n "mdsycx-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full"))) (d #t) (k 0)))) (h "1ckylvwnf8rjdl6qvjx9vrwwnyd51pmd75n808skhqlkkrv19q5i")))

