(define-module (crates-io md al mdal) #:use-module (crates-io))

(define-public crate-mdal-0.8.0 (c (n "mdal") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "directories") (r "^1.0") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14") (d #t) (k 0)) (d (n "rust-embed") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)))) (h "16wp0n46znicj4gbi5dy4j42gwwiib4157fn5xyavrknsv7aknlb")))

