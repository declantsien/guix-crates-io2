(define-module (crates-io md -t md-tangle-engine) #:use-module (crates-io))

(define-public crate-md-tangle-engine-0.1.0 (c (n "md-tangle-engine") (v "0.1.0") (h "0fn0n0dqyrcm0gy9l7mnyrsdyq4f713277rf5zlxkp28jzacnvi9") (y #t)))

(define-public crate-md-tangle-engine-0.1.1 (c (n "md-tangle-engine") (v "0.1.1") (h "1vsh0b4andlq83z737i8nq75h7rl41qdnrngzb9bkky5cby3rn8b") (y #t)))

(define-public crate-md-tangle-engine-0.1.2 (c (n "md-tangle-engine") (v "0.1.2") (h "0j7dhzkab9rm1lgzihchcm28h0m2smqmvkw7kl29a8mpz2zly45g") (y #t)))

(define-public crate-md-tangle-engine-0.1.3 (c (n "md-tangle-engine") (v "0.1.3") (h "0gqvpvrc4nmim19lyyj8v5b1rzv28gwcgw7l343n7n9bjadg40sd") (y #t)))

(define-public crate-md-tangle-engine-0.1.4 (c (n "md-tangle-engine") (v "0.1.4") (h "0ny2j7cf21isk89x12bhs4ch354r7b2vcb215ah9cn9q792nparc") (y #t)))

