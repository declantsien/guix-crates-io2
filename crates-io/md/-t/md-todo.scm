(define-module (crates-io md -t md-todo) #:use-module (crates-io))

(define-public crate-md-todo-0.1.0 (c (n "md-todo") (v "0.1.0") (h "0y9l43bvv50y0l89394j2f77lcwqhll3jfgkxlca0idk27zhrpab")))

(define-public crate-md-todo-0.1.1 (c (n "md-todo") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "md-5") (r "^0.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07vqfaribfc8k77c7r06j9ffxcsdypigai2kcp23pczag8fgkxqb")))

(define-public crate-md-todo-0.1.2 (c (n "md-todo") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "md-5") (r "^0.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jwixg7dvi4yj4qbmhgd82x2jrhxg9ybpvvg6cwnk496niihplrd")))

