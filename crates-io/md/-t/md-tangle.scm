(define-module (crates-io md -t md-tangle) #:use-module (crates-io))

(define-public crate-md-tangle-0.1.0 (c (n "md-tangle") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0n24mk7vs910481gdbiiq20ki4g2xay050fja1pip0y5yhp3n12c") (y #t)))

(define-public crate-md-tangle-0.1.1 (c (n "md-tangle") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "md-tangle-engine") (r "^0.1") (d #t) (k 0)))) (h "1q2r9qzyldkfz15ig456lr136b1dz5d1xbxdhxi6cgkdpa9l2vq9") (y #t)))

(define-public crate-md-tangle-0.1.2 (c (n "md-tangle") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "md-tangle-engine") (r "^0.1") (d #t) (k 0)))) (h "1hk1q2y8rxfhmp6ha5n84pzlksq7qwiqkl7zhrxa6y72hg1hjby3") (y #t)))

(define-public crate-md-tangle-0.1.3 (c (n "md-tangle") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "md-tangle-engine") (r "^0.1") (d #t) (k 0)))) (h "0knnlfiawhcmi92qw3rb054z293ql40qfsq58jxss1y80yla10hd") (y #t)))

(define-public crate-md-tangle-0.1.4 (c (n "md-tangle") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "md-tangle-engine") (r "^0.1") (d #t) (k 0)))) (h "0672vq1dypc9kiiw7x3kldqdj3kd5wb12g78kyccs4ka1r5j6r77") (y #t)))

