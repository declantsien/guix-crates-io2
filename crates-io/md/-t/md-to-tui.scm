(define-module (crates-io md -t md-to-tui) #:use-module (crates-io))

(define-public crate-md-to-tui-0.1.0 (c (n "md-to-tui") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "09v1z0h1af4f1s4jslklsz9b31bx12z8zm88yv3yc8svnqhhqbn2")))

(define-public crate-md-to-tui-0.1.1 (c (n "md-to-tui") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0smhzm89faqqakhlp5x4fmdaixj0108yhdr7m8fmwcspayh8njvw")))

(define-public crate-md-to-tui-0.1.2 (c (n "md-to-tui") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0a5rbcsl9lcyj4hr2p36dj4wdg4ps9ns28yqf87p9pqbbdad0b5c")))

