(define-module (crates-io md #{-i}# md-icons-helper) #:use-module (crates-io))

(define-public crate-md-icons-helper-0.1.0 (c (n "md-icons-helper") (v "0.1.0") (h "00nb8l5kjwi0jff96djsf93mmbf8h10qznp1yfmnkmz370653i0q")))

(define-public crate-md-icons-helper-0.2.0 (c (n "md-icons-helper") (v "0.2.0") (d (list (d (n "maud") (r "^0") (o #t) (d #t) (k 0)))) (h "074r26is9xlknf212681650a9gb00685377sz61drxi1hph7i7c9") (s 2) (e (quote (("maud" "dep:maud"))))))

(define-public crate-md-icons-helper-0.2.1 (c (n "md-icons-helper") (v "0.2.1") (d (list (d (n "maud") (r "^0") (o #t) (d #t) (k 0)))) (h "1rpksyd2p8g818xpsck4qvhk77pgkb7abwlj0rvwfwhkv2jgf6z8") (s 2) (e (quote (("maud" "dep:maud"))))))

(define-public crate-md-icons-helper-0.2.2 (c (n "md-icons-helper") (v "0.2.2") (d (list (d (n "maud") (r "^0") (o #t) (d #t) (k 0)))) (h "09hbwkblcva1iqjyhyilkspiwpikk89wsqvc3fz2ak02yvyp59g1") (s 2) (e (quote (("maud" "dep:maud"))))))

(define-public crate-md-icons-helper-0.3.0 (c (n "md-icons-helper") (v "0.3.0") (d (list (d (n "leptos") (r "^0") (o #t) (d #t) (k 0)) (d (n "maud") (r "^0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "12k3qh6p1kf36q8chm4x1ijni07ni11qbn6giwc7hbxggm9zxksc") (s 2) (e (quote (("maud" "dep:maud") ("leptos" "dep:leptos"))))))

(define-public crate-md-icons-helper-0.3.2 (c (n "md-icons-helper") (v "0.3.2") (d (list (d (n "leptos") (r "^0") (o #t) (d #t) (k 0)) (d (n "maud") (r "^0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "14nkc6b71052xhpl2flhqf4m4dg3vw5jyvqyg8vqgggyxw6aakl3") (s 2) (e (quote (("maud" "dep:maud") ("leptos" "dep:leptos"))))))

