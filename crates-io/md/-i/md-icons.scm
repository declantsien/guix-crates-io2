(define-module (crates-io md #{-i}# md-icons) #:use-module (crates-io))

(define-public crate-md-icons-0.1.0 (c (n "md-icons") (v "0.1.0") (d (list (d (n "md-icons-helper") (r "^0.1.0") (d #t) (k 0)))) (h "1vw7i19d0bq9djpm5hfdc0b26s2703has52fgl04iq6d0w17bc1p")))

(define-public crate-md-icons-0.2.0 (c (n "md-icons") (v "0.2.0") (d (list (d (n "maud") (r "^0") (o #t) (d #t) (k 0)) (d (n "md-icons-helper") (r "^0.2.0") (d #t) (k 0)))) (h "1w5y7ic1zkihjdynpcaq6zh6ijyzcwvq996vkk4258pl9mfs9gya") (s 2) (e (quote (("maud" "dep:maud" "md-icons-helper/maud"))))))

(define-public crate-md-icons-0.2.1 (c (n "md-icons") (v "0.2.1") (d (list (d (n "maud") (r "^0") (o #t) (d #t) (k 0)) (d (n "md-icons-helper") (r "^0.2.0") (d #t) (k 0)))) (h "02ym0zph9mim5hh1291a1qrz7k98rnbsb2gk53kvgwm34h3jap1h") (s 2) (e (quote (("maud" "dep:maud" "md-icons-helper/maud"))))))

(define-public crate-md-icons-0.2.2 (c (n "md-icons") (v "0.2.2") (d (list (d (n "maud") (r "^0") (o #t) (d #t) (k 0)) (d (n "md-icons-helper") (r "^0.2.0") (d #t) (k 0)))) (h "1id3chnhy1dynilw5m2qqv3iv0whqvjna4ic2k3x34h1d02pv2zi") (s 2) (e (quote (("maud" "dep:maud" "md-icons-helper/maud"))))))

(define-public crate-md-icons-0.3.0 (c (n "md-icons") (v "0.3.0") (d (list (d (n "leptos") (r "^0") (o #t) (d #t) (k 0)) (d (n "maud") (r "^0") (o #t) (d #t) (k 0)) (d (n "md-icons-helper") (r "^0.3.0") (d #t) (k 0)))) (h "13sam36hr73xblhzi8j0ad0zaj4fda4q192ywv56hrhbwwmfg89v") (s 2) (e (quote (("maud" "dep:maud" "md-icons-helper/maud") ("leptos" "dep:leptos" "md-icons-helper/leptos"))))))

(define-public crate-md-icons-0.3.1 (c (n "md-icons") (v "0.3.1") (d (list (d (n "leptos") (r "^0") (o #t) (d #t) (k 0)) (d (n "maud") (r "^0") (o #t) (d #t) (k 0)) (d (n "md-icons-helper") (r "^0.3.0") (d #t) (k 0)))) (h "0ma9js5giha291bvjgfrb928dy07991aqg460bz09z3aqw41394m") (s 2) (e (quote (("maud" "dep:maud" "md-icons-helper/maud") ("leptos" "dep:leptos" "md-icons-helper/leptos"))))))

(define-public crate-md-icons-0.3.2 (c (n "md-icons") (v "0.3.2") (d (list (d (n "leptos") (r "^0") (o #t) (d #t) (k 0)) (d (n "maud") (r "^0") (o #t) (d #t) (k 0)) (d (n "md-icons-helper") (r "^0.3.0") (d #t) (k 0)))) (h "198lwpbx57bnbwxb94mhw4i3afzb1q9nb4k88ak6k1pgw16a7a4j") (s 2) (e (quote (("maud" "dep:maud" "md-icons-helper/maud") ("leptos" "dep:leptos" "md-icons-helper/leptos"))))))

