(define-module (crates-io md #{-i}# md-inc) #:use-module (crates-io))

(define-public crate-md-inc-0.1.0 (c (n "md-inc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1s0zdmw46nigwbhz6njm1g6vkaak8k4mysxm3jva431zswq8kbkk")))

(define-public crate-md-inc-0.2.0 (c (n "md-inc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1d2s9mw5ksmhiaabaxb5b4hci4a5lr4vscsvzqhwr40hlf23y2xn")))

(define-public crate-md-inc-0.3.0 (c (n "md-inc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0cq7djmsgzrd004y219wnjm0ks681vvb180mic744qvnc1ncpgad")))

(define-public crate-md-inc-0.3.1 (c (n "md-inc") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "196s7wr0mhx9zp26a40sdj6j18hh9qkkjkacjyq6br9j9rwrwcci")))

