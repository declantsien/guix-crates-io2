(define-module (crates-io md #{-i}# md-include) #:use-module (crates-io))

(define-public crate-md-include-0.1.0 (c (n "md-include") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "09pw2g5jwm9zmifri24y6vrl2hipyq3cbx1imzn81y05ya3y2d40")))

