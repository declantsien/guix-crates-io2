(define-module (crates-io md fm mdfmt) #:use-module (crates-io))

(define-public crate-mdfmt-1.0.0 (c (n "mdfmt") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "09mwm79a480s8syldbcir476cvh3z9ib5kmp79c24wpiy642ah6k")))

