(define-module (crates-io md ns mdns) #:use-module (crates-io))

(define-public crate-mdns-0.1.0 (c (n "mdns") (v "0.1.0") (d (list (d (n "dns-parser") (r "^0.5") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1pz5arbw3ky7v0r5m6y9fzaz21g0f6c5nkm92c8qwck4qrn2ggm8")))

(define-public crate-mdns-0.1.1 (c (n "mdns") (v "0.1.1") (d (list (d (n "dns-parser") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.6") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "00zp67fqkazc230ad07gxrc49xvjb1qfxrfqsvfk6nkvmhfzh1f5")))

(define-public crate-mdns-0.1.2 (c (n "mdns") (v "0.1.2") (d (list (d (n "dns-parser") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.6") (d #t) (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "0racnhhiig0wbbfc7sgjdl6qkcwhqix84jj697lbhh7ici69w7cp")))

(define-public crate-mdns-0.1.4 (c (n "mdns") (v "0.1.4") (d (list (d (n "dns-parser") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.6") (d #t) (k 0)) (d (n "ifaces") (r "^0.0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "0nsqbrbpy4z19c9xmgarl7avmbfmrv5q5kf1lmd2f82pnhwlpinr")))

(define-public crate-mdns-0.2.0 (c (n "mdns") (v "0.2.0") (d (list (d (n "dns-parser") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "ifaces") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "04p39v0xxj8l19ih4269g3q0kwjyxqn211y9kn7h1p6jcjv1g1m4")))

(define-public crate-mdns-0.2.1 (c (n "mdns") (v "0.2.1") (d (list (d (n "dns-parser") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "ifaces") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1z3wk9s6v00s44l4p2g61k1b2agmf028anx476gw6gkw8yj0p2sy")))

(define-public crate-mdns-0.2.2 (c (n "mdns") (v "0.2.2") (d (list (d (n "dns-parser") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "ifaces") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "0w4nbafgyndx98b3mf12h93xv73y4njl566zfavfhm0f48jnb05a")))

(define-public crate-mdns-0.2.3 (c (n "mdns") (v "0.2.3") (d (list (d (n "dns-parser") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "ifaces") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "0x96gmhq1l991gdqraqrzivm6zzg08g0058lfb7z8f8rfjfqg5p0")))

(define-public crate-mdns-0.3.0 (c (n "mdns") (v "0.3.0") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "ifaces") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1g5xwc95xmzpm2h25m3ci30rrby729l2cqpipplaj8v98nnmfsq1")))

(define-public crate-mdns-0.3.1 (c (n "mdns") (v "0.3.1") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "16n4xbg1iad3cgja91qmisdx6jj532w5b288ns37lcdrhk9gv47y")))

(define-public crate-mdns-0.3.2 (c (n "mdns") (v "0.3.2") (d (list (d (n "dns-parser") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "0a96id6nzpbqhdiv90fdmaji0nlmfsdcbk1nvrmapv5xw6fiaclw")))

(define-public crate-mdns-1.0.0 (c (n "mdns") (v "1.0.0") (d (list (d (n "async-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("time" "udp" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "1w5b1ax8209qrq6ni3m323fh6p7m65q0imibc69f1lblsdhy9lfi")))

(define-public crate-mdns-1.1.0 (c (n "mdns") (v "1.1.0") (d (list (d (n "async-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("time" "udp" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "0znf6ml4zpmly8vwdx0yi84mi6ffjjxw0al73hk1l6p7sjxbmk6a")))

(define-public crate-mdns-2.0.0 (c (n "mdns") (v "2.0.0") (d (list (d (n "async-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1xi07c9vyk4qf93qbgyza8w7g48mnxnr1gl61yz5j2jh6zxijrpy")))

(define-public crate-mdns-2.0.1 (c (n "mdns") (v "2.0.1") (d (list (d (n "async-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "02cd5d06prj0d67i61zfq41c9bampg41823h4z2iv2j7qndlf4c1")))

(define-public crate-mdns-2.0.2 (c (n "mdns") (v "2.0.2") (d (list (d (n "async-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "11wjj9p6blgyi6xjgic24bm5ck88pajrqpkbsh1d7zzzx4jpj0kz")))

(define-public crate-mdns-3.0.0 (c (n "mdns") (v "3.0.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "async-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1wzky0z1wwz5dahq64sr7l0lrkfc9f1jfawjy0vs8vjsqwm9csf7")))

