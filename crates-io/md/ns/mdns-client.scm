(define-module (crates-io md ns mdns-client) #:use-module (crates-io))

(define-public crate-mdns-client-0.1.0 (c (n "mdns-client") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "dns-message-parser") (r "^0.4.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.33") (d #t) (k 0)) (d (n "pnet") (r "^0.27.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12hhkhcw7mqbf3hnprhc7qbji3rvnrbk81rsra885qw2lycl41qk")))

