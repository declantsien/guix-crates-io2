(define-module (crates-io md ta mdtable) #:use-module (crates-io))

(define-public crate-mdtable-0.1.0 (c (n "mdtable") (v "0.1.0") (h "1kbnzq36rf9jra81k427r6dlwq119jmzpkv8yilx205mnlq7xg8x")))

(define-public crate-mdtable-0.1.1 (c (n "mdtable") (v "0.1.1") (h "0a12p4nk93zwxlqy4kgbppvvnfw5q4bhxkyawxqs9iyywl66gjvf")))

