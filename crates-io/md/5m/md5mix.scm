(define-module (crates-io md #{5m}# md5mix) #:use-module (crates-io))

(define-public crate-md5mix-0.1.0 (c (n "md5mix") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0ci81n526ranigp4p8vmsj6gqd7zr0ppliw1yq65cdbr9xk7k7b1")))

(define-public crate-md5mix-0.1.1 (c (n "md5mix") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "079slsq5d5r8ipafw8nmrpmhy0zxszaha3vwsry9vfgphszwi192")))

(define-public crate-md5mix-0.1.2 (c (n "md5mix") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1r7ncs7wg9rn17lkybgy78b2llh4hrmk13jh64ph2xqk5vzb78pm")))

