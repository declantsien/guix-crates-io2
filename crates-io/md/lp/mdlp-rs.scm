(define-module (crates-io md lp mdlp-rs) #:use-module (crates-io))

(define-public crate-mdlp-rs-0.1.0 (c (n "mdlp-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "1wg76y30lhszp8fxgnyb6nqjwvnxqv8x7vgy38m58ap0p2ppp6p6")))

