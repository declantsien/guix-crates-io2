(define-module (crates-io md bo mdbook-wordcount) #:use-module (crates-io))

(define-public crate-mdbook-wordcount-1.0.0 (c (n "mdbook-wordcount") (v "1.0.0") (d (list (d (n "mdbook") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)))) (h "1jx2r92m9j9dnfyrhq26q9n0w4mrxswljvkzpv6r8jj223ndvddp")))

(define-public crate-mdbook-wordcount-1.0.1 (c (n "mdbook-wordcount") (v "1.0.1") (d (list (d (n "lazy_static") (r "~1.4") (d #t) (k 0)) (d (n "mdbook") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1.5") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)))) (h "0gkcr2kkn52sx6qq4n5pqz7j0yjhjkj2zrgv8qzp0g3qir7fisn8")))

