(define-module (crates-io md bo mdbook-gabc) #:use-module (crates-io))

(define-public crate-mdbook-gabc-0.0.1 (c (n "mdbook-gabc") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "clap") (r "^4.0.29") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.21") (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "toml_edit") (r "^0.15.0") (k 0)))) (h "0shjr6m19lsp6vpg3npqxbdr9ixyf1fvdsrbapzinp2ja6d2r9kp")))

