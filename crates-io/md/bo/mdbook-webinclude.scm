(define-module (crates-io md bo mdbook-webinclude) #:use-module (crates-io))

(define-public crate-mdbook-webinclude-0.1.0 (c (n "mdbook-webinclude") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.34") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "04q75j82hd3x0dlg46d8jmpqiq3p6mxbb407ss04427hxbg38syb")))

