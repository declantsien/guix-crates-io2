(define-module (crates-io md bo mdbook-post) #:use-module (crates-io))

(define-public crate-mdbook-post-0.1.0 (c (n "mdbook-post") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.64") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "1qwsxij00npn3mn1bv41k4lcj9syf3c4g86yzffl967vabcmx94c") (r "1.72")))

(define-public crate-mdbook-post-0.1.1 (c (n "mdbook-post") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.64") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "1b1s03p2wd02vrn4mq6gyxpzprai2mqham8avc5dzl975swrh1pv") (r "1.72")))

(define-public crate-mdbook-post-0.1.2 (c (n "mdbook-post") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.64") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "02dn3b7vjym4210x65iqavpw5skxfbqxrky7vnfziw2kqfndxm6m") (r "1.72")))

(define-public crate-mdbook-post-0.1.3 (c (n "mdbook-post") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "doe") (r "^0.2.37") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "0m9y8r8aaqan6rd8px0brb43g99zfqnlwvljx4ykchzg70i6nlqb") (r "1.72")))

