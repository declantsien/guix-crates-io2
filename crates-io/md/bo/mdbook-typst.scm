(define-module (crates-io md bo mdbook-typst) #:use-module (crates-io))

(define-public crate-mdbook-typst-0.1.0 (c (n "mdbook-typst") (v "0.1.0") (d (list (d (n "mdbook") (r "^0.4.35") (k 0)) (d (n "pullup") (r "^0.3.1") (f (quote ("builder" "mdbook" "markdown" "typst" "tracing"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1mdk3fh40xz2mbap1j3qf6fffdkbq2sids2j1q6634dlqps4v2rs")))

(define-public crate-mdbook-typst-0.1.1 (c (n "mdbook-typst") (v "0.1.1") (d (list (d (n "mdbook") (r "^0.4.35") (k 0)) (d (n "pullup") (r "^0.3.2") (f (quote ("builder" "mdbook" "markdown" "typst" "tracing"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1p5bkhjcbh3f6j0flh83zy9ydkjac0sfwizra8gfjmpn0fzlp3wb")))

(define-public crate-mdbook-typst-0.1.2 (c (n "mdbook-typst") (v "0.1.2") (d (list (d (n "mdbook") (r "^0.4.35") (k 0)) (d (n "pullup") (r "^0.3.3") (f (quote ("builder" "mdbook" "markdown" "typst" "tracing"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0848pc2z4wi7dp77p0429cj8l48h97zprj6rsla8vfkxh8jdmdia")))

