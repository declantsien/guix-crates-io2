(define-module (crates-io md bo mdbook-image-size) #:use-module (crates-io))

(define-public crate-mdbook-image-size-0.1.0 (c (n "mdbook-image-size") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "11dsq5whyblhig1l214xa7nijh5h523ff5sbgr6cqzblzzj2czc5")))

