(define-module (crates-io md bo mdbook-preprocessor-boilerplate) #:use-module (crates-io))

(define-public crate-mdbook-preprocessor-boilerplate-0.1.0 (c (n "mdbook-preprocessor-boilerplate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.14") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a2hb3isjzmsb6x69gx9h0gwmql08fv1j97qikq03c3bvnsh61hj")))

(define-public crate-mdbook-preprocessor-boilerplate-0.1.1 (c (n "mdbook-preprocessor-boilerplate") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.34") (k 0)) (d (n "mdbook") (r "^0.4.14") (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mknia2c0ssv7v05rqsj4c9zpibz5j0la7r12b0hghv8ww5nj64i")))

(define-public crate-mdbook-preprocessor-boilerplate-0.1.2 (c (n "mdbook-preprocessor-boilerplate") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (k 0)) (d (n "mdbook") (r "^0.4.28") (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1xhfd0dhkvl180hw8rgzqm20hbhv9a6kpq269w24hfa92jvz80dp")))

