(define-module (crates-io md bo mdbook-rss) #:use-module (crates-io))

(define-public crate-mdbook-rss-0.1.0 (c (n "mdbook-rss") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "globset") (r "^0.4.6") (d #t) (k 0)) (d (n "gray_matter") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.7") (d #t) (k 0)) (d (n "rss") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0dxlsz1dzjrjs1x1ah232g234x8az1n0vqxhlhc5zrc7ran4nmp0")))

