(define-module (crates-io md bo mdbook-checklist) #:use-module (crates-io))

(define-public crate-mdbook-checklist-0.1.0 (c (n "mdbook-checklist") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1jal2zaybjw4k3lv4sgm13vgaqjy35amhr8lmhbb957hl4j4jry3")))

(define-public crate-mdbook-checklist-0.1.1 (c (n "mdbook-checklist") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0v0x2lmd6x6af8jil7mlaq5pw8zrpalbhk6pz1hnsxxqv9s63d17")))

