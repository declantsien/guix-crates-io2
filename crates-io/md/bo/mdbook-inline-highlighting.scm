(define-module (crates-io md bo mdbook-inline-highlighting) #:use-module (crates-io))

(define-public crate-mdbook-inline-highlighting-0.1.0 (c (n "mdbook-inline-highlighting") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^11.0.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "1a2nsq6y7kffas8mxgnp5cfhpl8k0kw1ykgdvv581913aij5j6a9")))

