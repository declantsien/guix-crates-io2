(define-module (crates-io md bo mdbook-yapp) #:use-module (crates-io))

(define-public crate-mdbook-yapp-0.0.0 (c (n "mdbook-yapp") (v "0.0.0") (h "0267zj2929mna2mchm9xa5p3nd0bsdz6rcsyd2fhk6bnp1520rjx")))

(define-public crate-mdbook-yapp-0.0.1 (c (n "mdbook-yapp") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1pg8x0101lax27f5gch7say107dx7a8k2gg11mwcxphy5xd95i7n")))

(define-public crate-mdbook-yapp-0.0.2 (c (n "mdbook-yapp") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "09ih8w1w3rakhrzf5a3hd1blnfc350bywi3yhhvfywqya4fxfyrq")))

(define-public crate-mdbook-yapp-0.0.3 (c (n "mdbook-yapp") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1gniznx8z09pd6f2naq1zw7z5wry1fk3mxzpb62rmyp7zkyq1qmf")))

(define-public crate-mdbook-yapp-0.1.0 (c (n "mdbook-yapp") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1gdrg7vsqlk483rd81bcqmzicd1d5k1y5hali9y95qwaa0mr64j4")))

(define-public crate-mdbook-yapp-1.0.0 (c (n "mdbook-yapp") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0zvp5cxp9gw09yc09q00ja9iavv408y75bmz5zmbwy4p4mabz9sa")))

(define-public crate-mdbook-yapp-1.0.1 (c (n "mdbook-yapp") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "1jfzsmy96dmpdclswvdqvlmnkw95y3zwg4k0a36bzmcbkz9wr155")))

(define-public crate-mdbook-yapp-1.0.2 (c (n "mdbook-yapp") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0x11gxsrzc8aslkgm84hdqm5dxz3v2bjcnlfcizaymdd7qr1spv4")))

(define-public crate-mdbook-yapp-1.0.3 (c (n "mdbook-yapp") (v "1.0.3") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "199xc0mjd30b53wjlv3lvrij32wigb3kpffqlkizz1w4grgy9i20")))

(define-public crate-mdbook-yapp-1.1.0 (c (n "mdbook-yapp") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1k8lch1b012wgrbcfziv9nqcgi45pd24mr4jca1zii5mvmnmsvji")))

