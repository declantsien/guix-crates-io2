(define-module (crates-io md bo mdbook-classy) #:use-module (crates-io))

(define-public crate-mdbook-classy-0.1.0 (c (n "mdbook-classy") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gs9161m40n6pl8z4pbninzrj9p7y75vwdy7i68262k3mvw0m3r4")))

