(define-module (crates-io md bo mdbook-repl) #:use-module (crates-io))

(define-public crate-mdbook-repl-0.1.0 (c (n "mdbook-repl") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1c49px2njyizliamc6h4p2041gh7gc2vqiidbp9azipss23m9g8l")))

(define-public crate-mdbook-repl-0.1.1 (c (n "mdbook-repl") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0l3lb1bx61m3ha5pr72g3pfly4kb7hxlmb6i23p1hk176wkmg2qk")))

(define-public crate-mdbook-repl-0.1.2 (c (n "mdbook-repl") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "10qc9ly3zih1vdlmgkwmzjaps409fnanzvybwp9fv23ff51i15zz")))

(define-public crate-mdbook-repl-0.2.0 (c (n "mdbook-repl") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "01caj9srvpa9byh8nplg6g0xnsqw0375pzxnq5yg35141p1c8h9w")))

(define-public crate-mdbook-repl-0.2.1 (c (n "mdbook-repl") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ydm1szqfihd53f3sq5hn52q20kls53ndvl2zjnccnacsb107paf")))

(define-public crate-mdbook-repl-0.2.2 (c (n "mdbook-repl") (v "0.2.2") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vcwn5d5bnkcwvwdl176sklgj5al5mhxddm86nl0j8059bdzia3p")))

(define-public crate-mdbook-repl-0.2.3 (c (n "mdbook-repl") (v "0.2.3") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1sbrbx08xwp0yq4f46p0zh5h7r1p69p9h3aqr8h2bcg1z3fng4rr")))

(define-public crate-mdbook-repl-0.2.4 (c (n "mdbook-repl") (v "0.2.4") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1av8f5ab13vdsy4rfsyjn7vas4b8fdpzan5fqn1ak7r7dmpphx08")))

