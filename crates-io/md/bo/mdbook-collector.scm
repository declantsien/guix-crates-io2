(define-module (crates-io md bo mdbook-collector) #:use-module (crates-io))

(define-public crate-mdbook-collector-0.1.0 (c (n "mdbook-collector") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "0qgaj60003fjwbyfkrpnc4z4k4mhwdpahgbqf0qxscrqxyl65sls")))

(define-public crate-mdbook-collector-0.2.0 (c (n "mdbook-collector") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "0fpyhzhmvkhg08ahk4rh2wdzvnrb547xxw72r8crmn72rdsyxn0a")))

