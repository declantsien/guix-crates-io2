(define-module (crates-io md bo mdbook-open-git-repo) #:use-module (crates-io))

(define-public crate-mdbook-open-git-repo-0.0.2 (c (n "mdbook-open-git-repo") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0akzq9yxa59n44dnln1ar380sx8l3x4rzbpcm232cykryhiy9i9p")))

(define-public crate-mdbook-open-git-repo-0.0.3 (c (n "mdbook-open-git-repo") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1jhfyk4k1z157z042lmpcp85xg994v9wpw6g69bvvcp2j6nzw30r")))

(define-public crate-mdbook-open-git-repo-0.0.4 (c (n "mdbook-open-git-repo") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0c3dfwvhfwpa116gjpzh89rqhinvpmsa0lvy732bf7gcvdczmf61")))

