(define-module (crates-io md bo mdbook-section-validator) #:use-module (crates-io))

(define-public crate-mdbook-section-validator-0.1.0 (c (n "mdbook-section-validator") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^6.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "serde_json" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1fbnawj92c1jy9m6jbdq8574miklc7ynb3g188bmfbvnbjaqy3f1")))

