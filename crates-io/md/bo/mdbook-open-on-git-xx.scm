(define-module (crates-io md bo mdbook-open-on-git-xx) #:use-module (crates-io))

(define-public crate-mdbook-open-on-git-xx-0.0.1 (c (n "mdbook-open-on-git-xx") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "08a9awawwhbd3q3wi7njpb2xsawvd4xlail4yqawil0xsg7hmrij")))

