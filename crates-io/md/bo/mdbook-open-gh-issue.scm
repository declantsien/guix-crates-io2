(define-module (crates-io md bo mdbook-open-gh-issue) #:use-module (crates-io))

(define-public crate-mdbook-open-gh-issue-0.1.0 (c (n "mdbook-open-gh-issue") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.36") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "1ghd6c3a0kyqmblnz79lhxn79lb81jr8dq9rmyyjgld56bv0hvr8")))

(define-public crate-mdbook-open-gh-issue-0.1.1 (c (n "mdbook-open-gh-issue") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.36") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "15b5i8v9105fp0h7cvnr9wsrivwp1wp622rdk4ar68sz31hkqm6q")))

