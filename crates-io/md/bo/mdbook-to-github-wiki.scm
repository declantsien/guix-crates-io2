(define-module (crates-io md bo mdbook-to-github-wiki) #:use-module (crates-io))

(define-public crate-mdbook-to-github-wiki-0.1.0 (c (n "mdbook-to-github-wiki") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "16qgx0xs37gk8909j151d3fzrclpipphr9arcbfilrmc2l3lqm9v")))

(define-public crate-mdbook-to-github-wiki-0.1.1 (c (n "mdbook-to-github-wiki") (v "0.1.1") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1iz40768wvyc4s7r6znhvsnn94b9an474xp24i2r2n7s9gg1ka0i")))

(define-public crate-mdbook-to-github-wiki-0.1.2 (c (n "mdbook-to-github-wiki") (v "0.1.2") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0pv18ymzl7kkcpfhk1c3kvjjwwb5mqv0qspxxdysjwv8mfs1lvck")))

