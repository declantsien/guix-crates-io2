(define-module (crates-io md bo mdbook-tag) #:use-module (crates-io))

(define-public crate-mdbook-tag-0.0.1 (c (n "mdbook-tag") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mdbook") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1a6jiwddc3kyblnxgq3pglhvsljngwm3h0z7c3csxrxz5f1zizfr")))

(define-public crate-mdbook-tag-0.0.2 (c (n "mdbook-tag") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mdbook") (r "= 0.3.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1.2.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1sfhv8samwq98pyck2fv0jjwf0104g3pm5k65z7z5l88ik7bq9lf")))

