(define-module (crates-io md bo mdbook-newday) #:use-module (crates-io))

(define-public crate-mdbook-newday-0.1.0 (c (n "mdbook-newday") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "088vfr9y9si038mqfbq7dsq3bkggnqa1avilb0mghmn7q9xf69w3")))

(define-public crate-mdbook-newday-0.2.0 (c (n "mdbook-newday") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)))) (h "1fzkkl2s1h54wyr88dbj9cmjsm3wcy4vv4b8jv12lpn9nmjqranv")))

