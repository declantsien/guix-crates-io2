(define-module (crates-io md bo mdbook-wavedrom-rs) #:use-module (crates-io))

(define-public crate-mdbook-wavedrom-rs-0.1.0 (c (n "mdbook-wavedrom-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.30") (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^10.0.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "wavedrom") (r "^0.1.0") (d #t) (k 0)))) (h "0qb84fj9g2wrcbgc5lx801qil9z76dyqdrlh6b51q34agykyx10f")))

