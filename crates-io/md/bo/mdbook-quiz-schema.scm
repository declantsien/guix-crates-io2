(define-module (crates-io md bo mdbook-quiz-schema) #:use-module (crates-io))

(define-public crate-mdbook-quiz-schema-0.1.0 (c (n "mdbook-quiz-schema") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 2)) (d (n "ts-rs") (r "^7.0.0") (d #t) (k 0)))) (h "13lpb5ym6abxy2dvpa5vw3zp1sc5kn5sfl2s228mkg23ibydwq4j")))

(define-public crate-mdbook-quiz-schema-0.1.1 (c (n "mdbook-quiz-schema") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 2)) (d (n "ts-rs") (r "^7.0.0") (d #t) (k 0)))) (h "1anixf33kk496gwpapx77mf4490kdr3s3khkpmml0mwg9jgqwq3f")))

(define-public crate-mdbook-quiz-schema-0.2.0 (c (n "mdbook-quiz-schema") (v "0.2.0") (d (list (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 2)) (d (n "ts-rs") (r "^7.0.0") (o #t) (d #t) (k 0)))) (h "0gr0s0zjsi4zpvvf25qks5fv7rcs6m68xc0xm66v5zq23y1j8va9") (s 2) (e (quote (("ts" "dep:ts-rs") ("json-schema" "dep:schemars" "dep:serde_json"))))))

(define-public crate-mdbook-quiz-schema-0.3.4 (c (n "mdbook-quiz-schema") (v "0.3.4") (d (list (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 2)) (d (n "ts-rs") (r "^7.0.0") (o #t) (d #t) (k 0)))) (h "0cx0l26l3fvh30209xwxq8nnv55by8hdgxg2f3yv6bsh3ihiyxrm") (s 2) (e (quote (("ts" "dep:ts-rs") ("json-schema" "dep:schemars" "dep:serde_json"))))))

(define-public crate-mdbook-quiz-schema-0.3.5 (c (n "mdbook-quiz-schema") (v "0.3.5") (d (list (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 2)) (d (n "ts-rs") (r "^7.0.0") (o #t) (d #t) (k 0)))) (h "1pw45hkwa6pvk820rqrsjxf4i6kq1dhgksj7ka6b0lqch9ahvn8i") (s 2) (e (quote (("ts" "dep:ts-rs") ("json-schema" "dep:schemars" "dep:serde_json"))))))

