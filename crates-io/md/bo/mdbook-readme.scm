(define-module (crates-io md bo mdbook-readme) #:use-module (crates-io))

(define-public crate-mdbook-readme-0.0.1 (c (n "mdbook-readme") (v "0.0.1") (d (list (d (n "lazy-regex") (r "^2.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.12") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1dsp9xjw6c4qx3g4k3kn9lvrkvxd78lrsqjsf09jv2nw0031l5i8")))

(define-public crate-mdbook-readme-0.0.2 (c (n "mdbook-readme") (v "0.0.2") (d (list (d (n "lazy-regex") (r "^2.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.12") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "048f19vqjfvrdrpj6c9qyvkmilp0cif1b6752a88b6nb7kwa34f0")))

(define-public crate-mdbook-readme-0.0.3 (c (n "mdbook-readme") (v "0.0.3") (d (list (d (n "lazy-regex") (r "^2.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.12") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0mars35h4bglqxzg8yv0fi7m95a9784a2j8mk6q2gf62y80ghp2a")))

(define-public crate-mdbook-readme-0.0.4 (c (n "mdbook-readme") (v "0.0.4") (d (list (d (n "lazy-regex") (r "^2.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.12") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "10kgd8mdf5h9kl0qbfz5srwznbfbnf14vqapf2cqlv6hvb2snrij")))

