(define-module (crates-io md bo mdbook-indexing) #:use-module (crates-io))

(define-public crate-mdbook-indexing-0.1.0 (c (n "mdbook-indexing") (v "0.1.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "0.9.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0fw95lwniaj49pzamllxa0rvbbxl2s0g5ck6r6x6cbw8hz92xsj8")))

(define-public crate-mdbook-indexing-0.1.1 (c (n "mdbook-indexing") (v "0.1.1") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "0.9.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "00vlvjbm0bqbgqdg4a5f4b97y5pnfrin7ph0h26lldq241yykpfk")))

(define-public crate-mdbook-indexing-0.1.2 (c (n "mdbook-indexing") (v "0.1.2") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "0.9.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0cvg8zzwc0nn7knx7k9q0zrj2bpjzbzfa7ja8gjfw17h8yk1bgjc")))

