(define-module (crates-io md bo mdbook-tocjs) #:use-module (crates-io))

(define-public crate-mdbook-tocjs-0.1.0 (c (n "mdbook-tocjs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "11grr91xqirh9b5zlhmbplccffkxw5sq6z6f51mf4kl1gdvgm5d8")))

(define-public crate-mdbook-tocjs-0.1.1 (c (n "mdbook-tocjs") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "0ifg1lnkhmwbzp9bkzfxhmsqk79kfc8dvj9gmvl1sy35zj4p9m2b")))

