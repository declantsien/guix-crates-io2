(define-module (crates-io md bo mdbook-embed) #:use-module (crates-io))

(define-public crate-mdbook-embed-0.1.0 (c (n "mdbook-embed") (v "0.1.0") (d (list (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "0ipcp0svvq7dnkq6gzp8h3gv2gp36p6hbffvn01v83kxx28xrxaz")))

(define-public crate-mdbook-embed-0.2.0 (c (n "mdbook-embed") (v "0.2.0") (d (list (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "12agzyb03ibxxwaab4phyh6vcwczkspc5a0xzra477hv2dpah1n7")))

