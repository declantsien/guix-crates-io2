(define-module (crates-io md bo mdbook-abbr) #:use-module (crates-io))

(define-public crate-mdbook-abbr-0.1.0 (c (n "mdbook-abbr") (v "0.1.0") (d (list (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "104dcrsxacpvk4bc6j7dav60z9zg7c542akqynlxf06vjjfj527m")))

