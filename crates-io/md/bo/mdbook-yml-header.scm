(define-module (crates-io md bo mdbook-yml-header) #:use-module (crates-io))

(define-public crate-mdbook-yml-header-0.1.0 (c (n "mdbook-yml-header") (v "0.1.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "10bnlxwpzb0mxxhyizaa5sxn78hik3m80bvxbxy15zxd8y5ji47w")))

(define-public crate-mdbook-yml-header-0.1.1 (c (n "mdbook-yml-header") (v "0.1.1") (d (list (d (n "clap") (r "3.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "0gppdyn2sqfissbi7d5giw356vgz1g3drbring970d95fn71anp2")))

(define-public crate-mdbook-yml-header-0.1.2 (c (n "mdbook-yml-header") (v "0.1.2") (d (list (d (n "clap") (r "3.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "1vj2yia42q67p3yj03imkzx3m1ig1jc4k3hnw1wsh4akry36lrsl")))

(define-public crate-mdbook-yml-header-0.1.3 (c (n "mdbook-yml-header") (v "0.1.3") (d (list (d (n "clap") (r "3.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "0mpw8hxin34gljz5rf85nd1a074ww91xc09cc6z8yjsn818cwk32")))

