(define-module (crates-io md bo mdbook-wavedrom) #:use-module (crates-io))

(define-public crate-mdbook-wavedrom-0.10.0 (c (n "mdbook-wavedrom") (v "0.10.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "toml_edit") (r "^0.13.0") (d #t) (k 0)))) (h "0ns9fr3dxmz708awknii6104vxnw6yqcjw3wyb1can4jzcyqrfcm")))

