(define-module (crates-io md bo mdbook-puml) #:use-module (crates-io))

(define-public crate-mdbook-puml-0.1.0 (c (n "mdbook-puml") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mdbook") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04ndrlwnl7j65b0hnkzg8955p3k31851wygg1xr5k2kp2cp5ds4s")))

(define-public crate-mdbook-puml-0.1.1 (c (n "mdbook-puml") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mdbook") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wgbnq6j6yb6q4m89j72mc6jlkjay3dm0hg86i4mc1g329qrnp8m")))

