(define-module (crates-io md bo mdbook-man) #:use-module (crates-io))

(define-public crate-mdbook-man-0.1.0 (c (n "mdbook-man") (v "0.1.0") (d (list (d (n "comrak") (r "^0.11") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "roffman") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "166cc8ayhm2rwfpc71c1gy53kcrw0a0y40p32fmqjhf2r0cz6bmi")))

