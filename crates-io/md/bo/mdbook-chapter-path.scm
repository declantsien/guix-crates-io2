(define-module (crates-io md bo mdbook-chapter-path) #:use-module (crates-io))

(define-public crate-mdbook-chapter-path-0.1.0 (c (n "mdbook-chapter-path") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "18pk5h3xdmm79shcwm5dl63dnmn3vmyn1ma85krfn6hrqll7g2lc")))

