(define-module (crates-io md bo mdbook-api) #:use-module (crates-io))

(define-public crate-mdbook-api-0.4.0 (c (n "mdbook-api") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "mdbook") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "19jz131wxw2ybnsbzlc0s8rkkkb9036wyhig9jvsp12zddfqkblq")))

