(define-module (crates-io md bo mdbook-i18n) #:use-module (crates-io))

(define-public crate-mdbook-i18n-0.1.0 (c (n "mdbook-i18n") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1kzhjz108xv374l0bbrdq1yx7l3l4k79h67vqsm8vab910zf7nml")))

(define-public crate-mdbook-i18n-0.1.1 (c (n "mdbook-i18n") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0vwbici0kpbps71qhvb7b64qjfyjpdws6s2xk61lbijxd3r7adjv")))

(define-public crate-mdbook-i18n-0.1.2 (c (n "mdbook-i18n") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0iw9rvlz8axpgzih53is4iiq2y92hpr2mfbraz6bq228bk07fyh5")))

(define-public crate-mdbook-i18n-0.1.3 (c (n "mdbook-i18n") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02f6kapqqqcv2rkfn660cnr9s30bhy1figam789wbwgasm9x6r8s")))

