(define-module (crates-io md bo mdbook-typst-piggsoft) #:use-module (crates-io))

(define-public crate-mdbook-typst-piggsoft-0.1.0 (c (n "mdbook-typst-piggsoft") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (f (quote ("simd"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m88m46sgr65x01vjiq1lm6knf0a2gl5il5dx05az7f7hq09bcr7") (y #t)))

