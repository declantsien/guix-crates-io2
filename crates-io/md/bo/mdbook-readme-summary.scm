(define-module (crates-io md bo mdbook-readme-summary) #:use-module (crates-io))

(define-public crate-mdbook-readme-summary-1.1.0 (c (n "mdbook-readme-summary") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (d #t) (k 0)) (d (n "fs") (r "^0.0.5") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pathbuf") (r "^0.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1jv54ghvmidci1ms5a0nd62q0mhykhw2dv9mmmmfcsr4f72ahp1q")))

(define-public crate-mdbook-readme-summary-1.1.1 (c (n "mdbook-readme-summary") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (d #t) (k 0)) (d (n "fs") (r "^0.0.5") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pathbuf") (r "^0.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1g0z4fwhpd8jmgn5l46xpf0kkg16cpvkv1q4ng38ng50klnldf4g")))

