(define-module (crates-io md bo mdbook-latex) #:use-module (crates-io))

(define-public crate-mdbook-latex-0.1.0 (c (n "mdbook-latex") (v "0.1.0") (d (list (d (n "md2pdf-mdbook") (r "^0.0.1") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.11") (d #t) (k 0)))) (h "1kghjzvm9cf6v3nn0j0f38crljahcy503yxx6kpgpq1c07knrg3r")))

(define-public crate-mdbook-latex-0.1.1 (c (n "mdbook-latex") (v "0.1.1") (d (list (d (n "md2tex") (r "^0.0.1") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0vpkjynwyxwy88fp62kxq3h0xm9px2pwh8l0ilgmz0ibsw151wh1")))

(define-public crate-mdbook-latex-0.1.2 (c (n "mdbook-latex") (v "0.1.2") (d (list (d (n "md2tex") (r "^0.1.1") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.11") (d #t) (k 0)))) (h "1n926kf29nqh9hc0zifr95fxi5aca4a639brgcfv2l37gfjpyy7x")))

(define-public crate-mdbook-latex-0.1.3 (c (n "mdbook-latex") (v "0.1.3") (d (list (d (n "md2tex") (r "^0.1.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.11") (d #t) (k 0)))) (h "0r3yga1c5vwii2c5lrbqgsq5q27x1icv19z3pj5h636s5g4abc63")))

(define-public crate-mdbook-latex-0.1.4 (c (n "mdbook-latex") (v "0.1.4") (d (list (d (n "md2tex") (r "^0.1.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.11") (d #t) (k 0)))) (h "191bzj0ckx3kszcn072hlk4xc0rhjwhk0qbzj3b2dm7m4wddpgjq")))

(define-public crate-mdbook-latex-0.1.5 (c (n "mdbook-latex") (v "0.1.5") (d (list (d (n "md2tex") (r "^0.1.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.11") (d #t) (k 0)))) (h "1n2br4cnva1mchp8ysk6455cjhgbnv0gp610js8gypzncyww3v2a")))

(define-public crate-mdbook-latex-0.1.24 (c (n "mdbook-latex") (v "0.1.24") (d (list (d (n "md2tex") (r "^0.1.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "tectonic") (r "^0.1.12") (d #t) (k 0)))) (h "0afqns2yqrkxjj888ybcnbnh1x843ayrc2rhp421pqsp3gp1d423")))

