(define-module (crates-io md bo mdbook-translation) #:use-module (crates-io))

(define-public crate-mdbook-translation-0.1.0 (c (n "mdbook-translation") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (k 0)) (d (n "mdtranslation") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "104q1vqa1disg42kdqfn00g1d1vvcvnal3afx9yc9rpgrib0khwl")))

(define-public crate-mdbook-translation-0.1.1 (c (n "mdbook-translation") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (k 0)) (d (n "mdtranslation") (r "^0.1.2") (d #t) (k 0)) (d (n "semver") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1b10sc2j5z9iwwpas85fsjqi1yrv7x8h13v36nbl5phr4yx8ggr9")))

