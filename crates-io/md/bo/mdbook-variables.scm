(define-module (crates-io md bo mdbook-variables) #:use-module (crates-io))

(define-public crate-mdbook-variables-0.1.0 (c (n "mdbook-variables") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0zpqkslwa8mq11br092npfmpd54nfzfvnhg46zj8maqn31d244vb")))

(define-public crate-mdbook-variables-0.2.0 (c (n "mdbook-variables") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "06crvazinfn129h3qzrgzv8s87mfygpk5ld6p0n0pp9ydsgfi2ma")))

(define-public crate-mdbook-variables-0.2.1 (c (n "mdbook-variables") (v "0.2.1") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1hbfjfj4fjkipd9yvjzi8i39qwp7rd2abba7khhm297div83cf3q")))

(define-public crate-mdbook-variables-0.2.2 (c (n "mdbook-variables") (v "0.2.2") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0ym76xwzyf711asaq88gp5jvq5z1n249an7mk4kc6gdcwqx7hjhw")))

(define-public crate-mdbook-variables-0.2.3 (c (n "mdbook-variables") (v "0.2.3") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0b24gwx964h04r9jrjj0bd6d5cgdbxr7pvhscbwjv27vv32yyx1x")))

(define-public crate-mdbook-variables-0.2.4 (c (n "mdbook-variables") (v "0.2.4") (d (list (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0hvpa6a9mqbgclp6h85r0fr3warsnkraqklnnigvfw2gq1v0g0vl")))

