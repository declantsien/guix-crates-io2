(define-module (crates-io md bo mdbook-mathpunc) #:use-module (crates-io))

(define-public crate-mdbook-mathpunc-0.1.0 (c (n "mdbook-mathpunc") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0qi3y2g6n1lpla93visjk9vbaiwg1c88v2c36sv8whh9cd37dk6q")))

(define-public crate-mdbook-mathpunc-0.2.0 (c (n "mdbook-mathpunc") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1pk25q9j5f00hhklhcramxrj9dhm5jyw23yglxl9ydxq1n6v4l26")))

