(define-module (crates-io md bo mdbook-inline-mathjax) #:use-module (crates-io))

(define-public crate-mdbook-inline-mathjax-0.1.0 (c (n "mdbook-inline-mathjax") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.18") (d #t) (k 0)) (d (n "semver") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1qbrpbz3x21lcwm4nf9vykjac3dmy7kygvx0dfrgaz8nw55h3ifl") (y #t)))

(define-public crate-mdbook-inline-mathjax-0.1.1 (c (n "mdbook-inline-mathjax") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.18") (d #t) (k 0)) (d (n "semver") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1fff1prw4lmbibakj9brdp44r4hgj3i31jlx6zan25ih4w99z2wk") (y #t)))

(define-public crate-mdbook-inline-mathjax-0.1.2 (c (n "mdbook-inline-mathjax") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.18") (d #t) (k 0)) (d (n "semver") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0hb6psp4923y5cf01ifammv6a90hpkwb1wvqy4sah3pi0s8ih95b") (y #t)))

(define-public crate-mdbook-inline-mathjax-0.1.3 (c (n "mdbook-inline-mathjax") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.18") (d #t) (k 0)) (d (n "semver") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1y8qczx3jxdcqlzdy7zyz7ymbv4y88i9s9hbb2a70yfi2i2kpyj0") (y #t)))

(define-public crate-mdbook-inline-mathjax-0.1.4 (c (n "mdbook-inline-mathjax") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.6") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.18") (d #t) (k 0)) (d (n "semver") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "18pm7bib3fx54jw8wr6ld7z0qbj8pdd4ml7w1ms2mkb9263cqnrz") (y #t)))

