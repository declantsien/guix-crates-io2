(define-module (crates-io md bo mdbook-najan) #:use-module (crates-io))

(define-public crate-mdbook-najan-0.1.1 (c (n "mdbook-najan") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.25") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1kyyyblhk8d991568qw9bzr138c4d7srbd0q7fhk952k9az834ps")))

(define-public crate-mdbook-najan-0.1.2 (c (n "mdbook-najan") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "12qjs3qd89w6gr759jl9pwyi3ghm212xhv2ijpjzysqm14r3ly2m")))

(define-public crate-mdbook-najan-0.3.0 (c (n "mdbook-najan") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "lexi") (r "^0.4.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0bk2p2f60r8gcvlg1v5ax6kyhyxgb92d7y5k70yrm14qpq2v6lh5")))

(define-public crate-mdbook-najan-0.3.1 (c (n "mdbook-najan") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "lexi") (r "^0.4.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0c1573w77dxs08plps3zjbzvdakycd8pkmijby58n24w4nlma3gj")))

