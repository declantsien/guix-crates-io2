(define-module (crates-io md bo mdbook-external-links) #:use-module (crates-io))

(define-public crate-mdbook-external-links-0.1.0 (c (n "mdbook-external-links") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.25") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^10.0.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1xwi2ipaz0bs4a9ffwmlza8jklni44nh0h9rmjvyfph0mn3msvj3")))

(define-public crate-mdbook-external-links-0.1.1 (c (n "mdbook-external-links") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.25") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^10.0.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1mk1r8ajssx3pd9zcdz8x8wpk9l4fgaq80firrb066i9cz00xcwb")))

