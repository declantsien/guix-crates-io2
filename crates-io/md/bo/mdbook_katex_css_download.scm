(define-module (crates-io md bo mdbook_katex_css_download) #:use-module (crates-io))

(define-public crate-mdbook_katex_css_download-0.1.0 (c (n "mdbook_katex_css_download") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0wcxfhrfppysb4r8bzlsidff5g3g2921sp8z4zx37kngkfpy19kk") (f (quote (("native-tls-vendored" "reqwest/native-tls-vendored"))))))

(define-public crate-mdbook_katex_css_download-0.2.0 (c (n "mdbook_katex_css_download") (v "0.2.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0kbjygyf2vk6zagy1i3q5n9b375vb15g95z0ygk99zvrsjwv0p72") (f (quote (("native-tls-vendored" "reqwest/native-tls-vendored"))))))

(define-public crate-mdbook_katex_css_download-0.2.1 (c (n "mdbook_katex_css_download") (v "0.2.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0ysaikim7cyq3lvnjpz00ax1dkmdlc9jx4c408kh5pckk10aw4n4") (f (quote (("native-tls-vendored" "reqwest/native-tls-vendored"))))))

(define-public crate-mdbook_katex_css_download-0.2.2 (c (n "mdbook_katex_css_download") (v "0.2.2") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1hlgwa4fp3j83w1pfm217jdbfyyr3dldapd2m81ryff4l5jxzq2y") (f (quote (("native-tls-vendored" "reqwest/native-tls-vendored"))))))

