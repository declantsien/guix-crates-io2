(define-module (crates-io md bo mdbook-wikilink0) #:use-module (crates-io))

(define-public crate-mdbook-wikilink0-0.4.1 (c (n "mdbook-wikilink0") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.12") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0bx3f3cby8ssqq6wkh1ld5v9j88p8h1hpppip5ip3palrfnam6sd")))

