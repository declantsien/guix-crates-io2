(define-module (crates-io md bo mdbook-tagger) #:use-module (crates-io))

(define-public crate-mdbook-tagger-0.1.0 (c (n "mdbook-tagger") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "1bckq63k3r4ci7s1bp12340k6wgqh20x5a1bxlyzxp9vd6snx3si")))

(define-public crate-mdbook-tagger-0.2.0 (c (n "mdbook-tagger") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "0i0mmg5x2r1wfg7igzslrjwnaf49mvxan5ymvnzwhaa76vhyx370")))

