(define-module (crates-io md bo mdbook-regex) #:use-module (crates-io))

(define-public crate-mdbook-regex-0.0.1 (c (n "mdbook-regex") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0d6sgb2ilwp310f4frq92z39mazmvyfgjj95dcsxppkf2lll205g")))

(define-public crate-mdbook-regex-0.0.2 (c (n "mdbook-regex") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0qykikj8zx6vw38by1vcmdvbc66f5byinpqmcyw8ygrj43d8a1kd")))

