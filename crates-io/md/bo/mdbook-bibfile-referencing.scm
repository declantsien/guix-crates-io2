(define-module (crates-io md bo mdbook-bibfile-referencing) #:use-module (crates-io))

(define-public crate-mdbook-bibfile-referencing-0.1.0 (c (n "mdbook-bibfile-referencing") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.4") (k 0)) (d (n "pandoc") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.10") (d #t) (k 0)))) (h "0qygawrzgjfiy3mmsdalb24s81hmr829m7m2b9pqxq5y61wmyfjj")))

(define-public crate-mdbook-bibfile-referencing-0.2.0 (c (n "mdbook-bibfile-referencing") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.4") (k 0)) (d (n "pandoc") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)))) (h "0nwczdika9s2wgfyvicwd5f9di04zl4djr8id70ryblh5kbwrs3h")))

(define-public crate-mdbook-bibfile-referencing-0.3.0 (c (n "mdbook-bibfile-referencing") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.21") (k 0)) (d (n "pandoc") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)))) (h "1pkqysj5h6r3fsv9z670qyh3lhxcfdqzlfk8g49ymg0n7p9njzn9")))

