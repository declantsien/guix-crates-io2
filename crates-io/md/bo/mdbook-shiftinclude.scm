(define-module (crates-io md bo mdbook-shiftinclude) #:use-module (crates-io))

(define-public crate-mdbook-shiftinclude-0.1.0 (c (n "mdbook-shiftinclude") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "1cwls4ryz551i1k8yy4ip93xjm77118837cm8c1ph4dd407lavq4")))

