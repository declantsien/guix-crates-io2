(define-module (crates-io md bo mdbook-obsidian) #:use-module (crates-io))

(define-public crate-mdbook-obsidian-0.1.0 (c (n "mdbook-obsidian") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.36") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1farmcb8fpbjma7fxxva0m7by6lvq92m7w4w8sis5rl10qawal4q")))

