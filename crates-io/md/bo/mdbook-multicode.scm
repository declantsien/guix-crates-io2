(define-module (crates-io md bo mdbook-multicode) #:use-module (crates-io))

(define-public crate-mdbook-multicode-0.1.0 (c (n "mdbook-multicode") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02rmk8av3a8iw0fdc65whr0z848fr1an8f4kv4whyg9ns9nsryh9")))

