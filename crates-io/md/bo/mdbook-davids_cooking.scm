(define-module (crates-io md bo mdbook-davids_cooking) #:use-module (crates-io))

(define-public crate-mdbook-davids_cooking-0.1.0 (c (n "mdbook-davids_cooking") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.32") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0a9fhyl9wyhlali6sgrqh6axcvl3mj20b40h7nv4n6mzkjzyzj1z")))

(define-public crate-mdbook-davids_cooking-0.1.1 (c (n "mdbook-davids_cooking") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.32") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "1xxphcism96v24ff1j19mrfn0454bhvy0040axhb8czjjfx85r3m")))

(define-public crate-mdbook-davids_cooking-0.1.2 (c (n "mdbook-davids_cooking") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.32") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0cs0lx813xkrb245z9kqm50p4q61s9dxdgfy1hisagp9rcim9gsw")))

(define-public crate-mdbook-davids_cooking-0.1.3 (c (n "mdbook-davids_cooking") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0f5qvc6zlmfzidqwzxbff5wkw1bm4rh2m821mpawh1qq0nwshcvd")))

