(define-module (crates-io md bo mdbook-tailwindcss) #:use-module (crates-io))

(define-public crate-mdbook-tailwindcss-0.1.0 (c (n "mdbook-tailwindcss") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.9") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.1") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^10.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tailwind-css") (r "^0.12.4") (d #t) (k 0)))) (h "0ayf417v1891j89fwmmlgqz5rhrkmimac78h8xjy2r1sbfl6xpqy")))

(define-public crate-mdbook-tailwindcss-0.1.1 (c (n "mdbook-tailwindcss") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.9") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.1") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^10.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tailwind-css") (r "^0.12.4") (d #t) (k 0)))) (h "1zzv1p7vpljqhc4p0ghai9x0fbws4i4954lf0z2byfqmviy23359")))

