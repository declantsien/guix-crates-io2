(define-module (crates-io md bo mdbook-combiner) #:use-module (crates-io))

(define-public crate-mdbook-combiner-0.1.0 (c (n "mdbook-combiner") (v "0.1.0") (d (list (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)))) (h "1dsni5cgl69cmvrcffcdmj6sh1gqzsbk2drbavi2gy35vdcam1za")))

(define-public crate-mdbook-combiner-0.1.1 (c (n "mdbook-combiner") (v "0.1.1") (d (list (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)))) (h "0rcidvvvzbmvmbwplblxfpvn91zq1af9fvxcs58w0lkzanmdd4ay")))

(define-public crate-mdbook-combiner-0.1.2 (c (n "mdbook-combiner") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "02a117x3yszqi5vnbirqgy5iz6b2caww0718h22rjlkymp9p6as5")))

(define-public crate-mdbook-combiner-0.1.3 (c (n "mdbook-combiner") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0q4p4rpr4zwa10q60pjbln7ij8xw7p9ypsv315bm9kijfvb2iay8")))

(define-public crate-mdbook-combiner-0.1.4 (c (n "mdbook-combiner") (v "0.1.4") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0g15d17wn9s3rnr9yhzai7kfn1p4d3h5850fsinkfbljh7yj5mxp")))

(define-public crate-mdbook-combiner-0.1.5 (c (n "mdbook-combiner") (v "0.1.5") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1pha79h8jsdsldydr313gvqbyzbmyahpal0966v5vj44r7czhmx9")))

(define-public crate-mdbook-combiner-0.1.6 (c (n "mdbook-combiner") (v "0.1.6") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "15dw7xp4v5njdrqwmb4lzywqj449c8vkp7gwwp9b1jcbp1i4j0hj")))

(define-public crate-mdbook-combiner-0.1.7 (c (n "mdbook-combiner") (v "0.1.7") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0v52y70ssmngsc9mapiasz5qj2w2cq9nza7zfnw26kdk75118r63")))

(define-public crate-mdbook-combiner-0.1.8 (c (n "mdbook-combiner") (v "0.1.8") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0wi8rbilza7acwv0nn8avvhcr2zldy2bs8chb1sm36irq3lh61a3")))

(define-public crate-mdbook-combiner-0.1.9 (c (n "mdbook-combiner") (v "0.1.9") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "16rr2cvv5b59wg5x4l2v3vaad88sgck7cpyzqmymqkfmikz1ci77")))

(define-public crate-mdbook-combiner-0.2.0 (c (n "mdbook-combiner") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0rgy2ms5j0jrgz758xw6j0airdw8haa689087q5dk1wqdgxazak9") (y #t)))

(define-public crate-mdbook-combiner-0.1.10 (c (n "mdbook-combiner") (v "0.1.10") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "14g8v56dkjhr82ihzss6ds932l0nai0kj7lchrbf47xissb4jkrz")))

(define-public crate-mdbook-combiner-0.1.11 (c (n "mdbook-combiner") (v "0.1.11") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "13hkxizahylymjz19k8nva96i7fn0l5hp97blh3cgkjwc4ssspip")))

(define-public crate-mdbook-combiner-0.1.12 (c (n "mdbook-combiner") (v "0.1.12") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1rpxkbavnr0688cyhx04bd6j06yg00032c4b1v54qh5xji5vimk6")))

(define-public crate-mdbook-combiner-0.1.13 (c (n "mdbook-combiner") (v "0.1.13") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0c1fg861fkxpzx9hly2xy6cj0pmbc1m3165fd5ax4igrdp9md2rh")))

(define-public crate-mdbook-combiner-0.1.14 (c (n "mdbook-combiner") (v "0.1.14") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0x59wsan83i27zaml656nxg9fqmrhh83fgfnx7fn408895fyz4f3")))

(define-public crate-mdbook-combiner-0.1.15 (c (n "mdbook-combiner") (v "0.1.15") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "19f1gnczz2a0lszb4mz6dq7gx36m50hzl62vsl6b4jpbx28pn7sp")))

(define-public crate-mdbook-combiner-0.1.16 (c (n "mdbook-combiner") (v "0.1.16") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0vhfxpzfnc869grzcmqdyqlyjv79x0p6bq4935cs1ccq97r7y45h")))

