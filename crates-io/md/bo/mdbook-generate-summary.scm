(define-module (crates-io md bo mdbook-generate-summary) #:use-module (crates-io))

(define-public crate-mdbook-generate-summary-0.1.0 (c (n "mdbook-generate-summary") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0gwdbcvrsnqrhgm5zy57l4cxx0khdm83ygrms5j97sy3nr7pqfk6")))

(define-public crate-mdbook-generate-summary-0.2.0 (c (n "mdbook-generate-summary") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0q96p7y03sjal4nwp7vv446v2438mdni9cz69vywy56bcmkrhhj2")))

