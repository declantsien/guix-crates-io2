(define-module (crates-io md bo mdbook-fix-cjk-spacing) #:use-module (crates-io))

(define-public crate-mdbook-fix-cjk-spacing-0.1.0 (c (n "mdbook-fix-cjk-spacing") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.2") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^4.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "0xajr889chv8p6dpjxjldbqjmfr8x5mzy71vvvypvid08d7xv970")))

(define-public crate-mdbook-fix-cjk-spacing-0.1.1 (c (n "mdbook-fix-cjk-spacing") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.2") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^4.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "17hdivibx4kf66xqcpgy51ybbxm6hdck0z5d98lv7qkwd6k13ny6")))

