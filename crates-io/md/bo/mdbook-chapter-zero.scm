(define-module (crates-io md bo mdbook-chapter-zero) #:use-module (crates-io))

(define-public crate-mdbook-chapter-zero-0.1.0 (c (n "mdbook-chapter-zero") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.37") (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "1iz8c06qzyf48gwy7z0d6m8mmsai3lqf4bywahx2hc5s0l46y14z")))

