(define-module (crates-io md bo mdbook-unlink) #:use-module (crates-io))

(define-public crate-mdbook-unlink-0.1.0 (c (n "mdbook-unlink") (v "0.1.0") (d (list (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)))) (h "1gbxc3iaz4gw15sm3lsxnijd3sn456sy1aa50ihah3xyar9p7fc0")))

