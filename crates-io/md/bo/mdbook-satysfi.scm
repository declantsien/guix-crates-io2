(define-module (crates-io md bo mdbook-satysfi) #:use-module (crates-io))

(define-public crate-mdbook-satysfi-0.0.1 (c (n "mdbook-satysfi") (v "0.0.1") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "12x8xl0167qk04697si88m333disp44hji92p6j4iazrfd49r2a9")))

(define-public crate-mdbook-satysfi-0.0.2 (c (n "mdbook-satysfi") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0z70sk8xq79aivx731m2xdkvp9f9p5gq82ahpnzcfnyqs7dnx4qw")))

(define-public crate-mdbook-satysfi-0.0.3 (c (n "mdbook-satysfi") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.9") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "075y9vc2lrnf5r5ssi432ffpwqdzs0b2wgc60q6259vvwkgs9sx0")))

(define-public crate-mdbook-satysfi-0.0.4 (c (n "mdbook-satysfi") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.10") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0sx34p02pzs1q120z4kr9bhf9mff8rjr4m3k3azx2v4hb5q60f9p")))

(define-public crate-mdbook-satysfi-0.0.5 (c (n "mdbook-satysfi") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.10") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0lckgcsvciaddp31nklv7sxfb586b13r8bxdipdcww67ypadp765")))

(define-public crate-mdbook-satysfi-0.0.6 (c (n "mdbook-satysfi") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0jcylpfpl8hqr4l3945mxccx3sqgrlwg2xcwp0qgbrq9mrvmnfpi")))

(define-public crate-mdbook-satysfi-0.0.7 (c (n "mdbook-satysfi") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.25") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "1sx3qvcvq8da0jvd4h68vz26i68bvzjfq7dxqj1p0dia5k48g9sx")))

