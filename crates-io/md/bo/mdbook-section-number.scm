(define-module (crates-io md bo mdbook-section-number) #:use-module (crates-io))

(define-public crate-mdbook-section-number-0.1.0 (c (n "mdbook-section-number") (v "0.1.0") (d (list (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.21") (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0284xg21f1r7v47k3mabaiw2f0vjgyhkiym6r990wfl4y01x7v62") (y #t)))

