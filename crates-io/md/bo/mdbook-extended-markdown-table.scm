(define-module (crates-io md bo mdbook-extended-markdown-table) #:use-module (crates-io))

(define-public crate-mdbook-extended-markdown-table-0.1.0 (c (n "mdbook-extended-markdown-table") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.21") (d #t) (k 0)) (d (n "semver") (r "^1.0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0gg4fmma47a20ka7z55h5399p05nk5sw02cqqyx1j0v9qsacnx6y")))

