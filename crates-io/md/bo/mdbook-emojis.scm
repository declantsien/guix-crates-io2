(define-module (crates-io md bo mdbook-emojis) #:use-module (crates-io))

(define-public crate-mdbook-emojis-0.1.0 (c (n "mdbook-emojis") (v "0.1.0") (d (list (d (n "emojis") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fgkij4yi6y5lk6p0xfz45kri5v44j11nwyrk8xfk3wfand9innh")))

(define-public crate-mdbook-emojis-0.1.2 (c (n "mdbook-emojis") (v "0.1.2") (d (list (d (n "emojis") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1v864rv0kmygxffk6qdmrnj9hvprvgc67yp15sn4qcjyg4p1c9p4")))

