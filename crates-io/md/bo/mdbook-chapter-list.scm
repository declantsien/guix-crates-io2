(define-module (crates-io md bo mdbook-chapter-list) #:use-module (crates-io))

(define-public crate-mdbook-chapter-list-0.1.0 (c (n "mdbook-chapter-list") (v "0.1.0") (d (list (d (n "mdbook") (r "^0.4.36") (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0llxlm0b98wcqmygpzkczs42c9m4g0p6pbrd01rzf69cd9n2dycg")))

