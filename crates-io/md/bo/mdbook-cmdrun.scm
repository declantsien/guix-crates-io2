(define-module (crates-io md bo mdbook-cmdrun) #:use-module (crates-io))

(define-public crate-mdbook-cmdrun-0.1.0 (c (n "mdbook-cmdrun") (v "0.1.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "clap") (r "3.2.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0ykacxdz7zky28rcc272n86j8ki8vsk0vzv3vwxczs02dl1ni87p")))

(define-public crate-mdbook-cmdrun-0.2.0 (c (n "mdbook-cmdrun") (v "0.2.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "clap") (r "3.2.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1qfmj8yzfcbjfrz9id1hp9440f7xkmf6bq76h8j5mn1n4xvx9z55")))

(define-public crate-mdbook-cmdrun-0.3.0 (c (n "mdbook-cmdrun") (v "0.3.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "clap") (r "3.2.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "03a1ngnq369c0h96hqnzjh1706f0chv0xky2cgadqw71ldammlxb")))

(define-public crate-mdbook-cmdrun-0.4.0 (c (n "mdbook-cmdrun") (v "0.4.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "clap") (r "3.2.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0b5jqha5618kcrn1yrl3565as0b9ia0azab4i2r98x8qw3ahcc6h")))

(define-public crate-mdbook-cmdrun-0.5.0 (c (n "mdbook-cmdrun") (v "0.5.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "clap") (r "3.2.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0lp610h42bfp18vvhrym6h7gp7fb6c23ykix3bm80ph6xjxrb6pa")))

(define-public crate-mdbook-cmdrun-0.6.0 (c (n "mdbook-cmdrun") (v "0.6.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "1fbi30a30w2h06zkl1j3pg9sppm67aibdbly9ki9kjw3qd8srf33")))

