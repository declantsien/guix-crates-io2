(define-module (crates-io md bo mdbook-footnote) #:use-module (crates-io))

(define-public crate-mdbook-footnote-0.1.0 (c (n "mdbook-footnote") (v "0.1.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "1sll1dpaji9samn05b23jdkhf87jbc2xl82gb38cagdlns60xdig")))

(define-public crate-mdbook-footnote-0.1.1 (c (n "mdbook-footnote") (v "0.1.1") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.28") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "0vl7vdwcwaglwnvkjxsm8yii05yckfx9rwq51bpzqrxxk81s7a41")))

