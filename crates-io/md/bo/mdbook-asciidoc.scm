(define-module (crates-io md bo mdbook-asciidoc) #:use-module (crates-io))

(define-public crate-mdbook-asciidoc-0.1.0 (c (n "mdbook-asciidoc") (v "0.1.0") (d (list (d (n "env_logger") (r "0.9.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "mdbook") (r "0.4.*") (d #t) (k 0)) (d (n "pulldown-cmark") (r "0.9.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "semver") (r "1.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)) (d (n "url") (r "2.*") (d #t) (k 0)))) (h "03z7k71wnzw7n3qwmn144v82p1j6amqwcal9sqd0vlgjb6ij7zhj")))

