(define-module (crates-io md bo mdbook-nix-eval) #:use-module (crates-io))

(define-public crate-mdbook-nix-eval-1.0.0 (c (n "mdbook-nix-eval") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1f4zzg526qbw9nm50plidfsdnlzlngvxdpxb7ar76qhr629f5p8q")))

(define-public crate-mdbook-nix-eval-1.0.1 (c (n "mdbook-nix-eval") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0hm0mcaah6a50f23a0mhlvcl0jq8c31yfw4mvpxazxjahsy6cbp8")))

