(define-module (crates-io md bo mdbook-hide) #:use-module (crates-io))

(define-public crate-mdbook-hide-0.1.0 (c (n "mdbook-hide") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.31") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)))) (h "0qg2j384bign5xynsx4hvkbz1zkq1dhsmg02j2y5g2d5r0sa6sma")))

(define-public crate-mdbook-hide-0.2.0 (c (n "mdbook-hide") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.32") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)))) (h "07prb202q9hk909xd1wkcvqb0jpp0v79j4wk9pyils9bc6k2z1zd")))

(define-public crate-mdbook-hide-0.3.0 (c (n "mdbook-hide") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.34") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)))) (h "1dd2sjd3ypk9nxsfh450r8xmxaxclin571qwyb84qsqywkraxb3j")))

(define-public crate-mdbook-hide-0.4.0 (c (n "mdbook-hide") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)))) (h "10c7rv2lk37464x3qab6740kxkvl43z81zcvx0jy85m5f47yx0ga")))

