(define-module (crates-io md bo mdbook-sitemap-generator) #:use-module (crates-io))

(define-public crate-mdbook-sitemap-generator-0.1.0 (c (n "mdbook-sitemap-generator") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5") (d #t) (k 0)))) (h "182fkdxjh567l4njgqisjxklrddl9k82hhm561ba0i2xyb5y8inx")))

(define-public crate-mdbook-sitemap-generator-0.2.0 (c (n "mdbook-sitemap-generator") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08rq9g4p8s6gfd5sihqdphbp9sz259mbbmnp07phyqsj1qjimhf1")))

