(define-module (crates-io md bo mdbook-chapter-number) #:use-module (crates-io))

(define-public crate-mdbook-chapter-number-0.1.0 (c (n "mdbook-chapter-number") (v "0.1.0") (d (list (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.21") (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0hhh8i2r61alyv21wc3scn7fnhyy4is9m3y9bjaix34wzlqg682z")))

(define-public crate-mdbook-chapter-number-0.1.1 (c (n "mdbook-chapter-number") (v "0.1.1") (d (list (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.21") (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1jvqz9prkv483kdr5ks4klqmqaj7wlzclpa6lsq0cdwrrbabnfnz")))

(define-public crate-mdbook-chapter-number-0.1.2 (c (n "mdbook-chapter-number") (v "0.1.2") (d (list (d (n "mdbook") (r "^0.4.21") (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^10.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0917vxg4ja16b6iad7kdzlncv39plxzys415xifn79fpcbw4y1in")))

