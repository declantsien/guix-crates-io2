(define-module (crates-io md bo mdbook-summary-generate) #:use-module (crates-io))

(define-public crate-mdbook-summary-generate-0.1.0 (c (n "mdbook-summary-generate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0z6kh9d0z4kns2wscsz7c038qgk7xcvs0ksyfc8masyxc58v63zz")))

(define-public crate-mdbook-summary-generate-0.1.1 (c (n "mdbook-summary-generate") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "10yzl2x44vdk62c9c2vfzq0ssxqhcgz0p0kcqkz4lb0zh0rcvar7")))

(define-public crate-mdbook-summary-generate-0.1.2 (c (n "mdbook-summary-generate") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.35") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0p6ci95g0fwyj75cpfjvwyphrhnsif3qfnhnvaa4vzkifl0c2jkz")))

