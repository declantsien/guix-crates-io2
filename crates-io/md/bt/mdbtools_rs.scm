(define-module (crates-io md bt mdbtools_rs) #:use-module (crates-io))

(define-public crate-mdbtools_rs-0.1.0 (c (n "mdbtools_rs") (v "0.1.0") (h "041ccjrzb2bw1czfjsaxjviwrbjdvra59kv5wjspnfadr5r63dn4")))

(define-public crate-mdbtools_rs-0.1.1 (c (n "mdbtools_rs") (v "0.1.1") (h "1p48j2xy8a3v33c8fln4pzjfi0l731g2j9cwbdl38c2gx9jvzcdb")))

(define-public crate-mdbtools_rs-0.1.2 (c (n "mdbtools_rs") (v "0.1.2") (h "15x45ix0qvrn096x1m0w6b9w9z55g7alcapdbzidc6ary7szc6yb")))

(define-public crate-mdbtools_rs-0.1.3 (c (n "mdbtools_rs") (v "0.1.3") (d (list (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "15l1bmf6fsyaqyqa6vjirnvwn52c6kfgcf2sq2l3hw8pbanr371i")))

