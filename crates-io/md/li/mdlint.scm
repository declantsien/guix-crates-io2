(define-module (crates-io md li mdlint) #:use-module (crates-io))

(define-public crate-mdlint-0.1.0 (c (n "mdlint") (v "0.1.0") (d (list (d (n "comrak") (r "^0.2") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^1") (d #t) (k 0)))) (h "0yqxji1yrhikq0hrg2sd9dvvjymjq54z7a8i3qijhqyzigjkw19b")))

