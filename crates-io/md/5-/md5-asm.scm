(define-module (crates-io md #{5-}# md5-asm) #:use-module (crates-io))

(define-public crate-md5-asm-0.1.0 (c (n "md5-asm") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "11zdhanjq5sfy7n0n35hbms3adqaw1vb80jww9vx88d3kd3n5gz7")))

(define-public crate-md5-asm-0.2.0 (c (n "md5-asm") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "16p62k077xsp7wapqdsishsfpcx90cwyg3wzx82pxw3qip9y27zy")))

(define-public crate-md5-asm-0.2.1 (c (n "md5-asm") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0my52myn4frqhjs3k3s0i7nsv80hhrlhi7098bp0lh5la7xsni45")))

(define-public crate-md5-asm-0.3.0 (c (n "md5-asm") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "01w1p6hpiw7qc25d3nhmjx6pnc83wzlgkk1j1bwpxdjw21ly9b49")))

(define-public crate-md5-asm-0.4.0 (c (n "md5-asm") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0yfvmi8mf6f6b8sc62qy0d9bpvd20fvxhmykcra5v9qj6g0in21a")))

(define-public crate-md5-asm-0.4.1 (c (n "md5-asm") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "03xni4yyk80762mdvka8w7lk4lvspja3680lgy9ws7fivka2402s")))

(define-public crate-md5-asm-0.4.2 (c (n "md5-asm") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0qajy90x4ycj10px2dl0csphaq7zlyr8fxgvspszlgxsqsin13iq")))

(define-public crate-md5-asm-0.4.3 (c (n "md5-asm") (v "0.4.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0gpk5647js1k084jc7pg2gji0cvl6hjkkbfia6lnpk8y4shyairv")))

(define-public crate-md5-asm-0.5.0 (c (n "md5-asm") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ixmkg8j7sqy9zln6pz9xi2dl2d9zpm8pz6p49za47n1bvradfbk")))

(define-public crate-md5-asm-0.5.1 (c (n "md5-asm") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0p1f9494s065i4sbnycj1dzwaavf1bpbnar81qs60p7yrp23plv1")))

(define-public crate-md5-asm-0.5.2 (c (n "md5-asm") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1pz217kwlvrw4bj4hil5acyp3l7g37vwf25psdc210bxzkkqx6yi")))

