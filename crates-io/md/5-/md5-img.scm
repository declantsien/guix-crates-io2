(define-module (crates-io md #{5-}# md5-img) #:use-module (crates-io))

(define-public crate-md5-img-0.1.0 (c (n "md5-img") (v "0.1.0") (d (list (d (n "bmp") (r "^0.3.0") (d #t) (k 0)) (d (n "md5") (r "^0.3.7") (d #t) (k 0)))) (h "0y4zsbmpv69rj1p7ky98bfj6rmcd2rgxfdmax2w2gl7521mhiapx")))

(define-public crate-md5-img-0.1.1 (c (n "md5-img") (v "0.1.1") (d (list (d (n "bmp") (r "^0.3.0") (d #t) (k 0)) (d (n "md5") (r "^0.3.7") (d #t) (k 0)))) (h "06ivdscdsbvw09r4k911wxm2i676jyqh4lpxi1kcr7h5sjmm0lcx")))

(define-public crate-md5-img-0.1.2 (c (n "md5-img") (v "0.1.2") (d (list (d (n "bmp") (r "^0.3.0") (d #t) (k 0)) (d (n "md5") (r "^0.3.7") (d #t) (k 0)))) (h "0simvyk0ipjaz0nbccwykjdlviay5s6xds4n7a3d6qdsda5j7zc6")))

(define-public crate-md5-img-0.1.3 (c (n "md5-img") (v "0.1.3") (d (list (d (n "bmp") (r "^0.3.0") (d #t) (k 0)) (d (n "md5") (r "^0.3.7") (d #t) (k 0)))) (h "1ddkmf8kfyk53c6c1i3585mph6xbfh2l9w8qnzn0zgyal5b8j6dh")))

