(define-module (crates-io md #{5-}# md5-core) #:use-module (crates-io))

(define-public crate-md5-core-0.1.0 (c (n "md5-core") (v "0.1.0") (h "1w9mdz97wd3fz7iw6mabckwxbvms0s50kv3pg0wc1sx00ksx6qjn")))

(define-public crate-md5-core-0.2.0 (c (n "md5-core") (v "0.2.0") (h "1pal54n78wq3i6n1nr85nlin6qjffq7x4ys984bkp731wv9hi3yq")))

(define-public crate-md5-core-0.2.1 (c (n "md5-core") (v "0.2.1") (h "0bxghjl429xzlb7yqak2vbaz8faxw4fzmm176dkp645yfw30kw2y")))

