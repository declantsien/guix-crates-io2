(define-module (crates-io md #{5-}# md5-rs) #:use-module (crates-io))

(define-public crate-md5-rs-0.1.0 (c (n "md5-rs") (v "0.1.0") (h "0g8rd2pi1xhd9hc4wyrari51s64ca7j547wgspwgbxl52c83k6hb")))

(define-public crate-md5-rs-0.1.1 (c (n "md5-rs") (v "0.1.1") (h "1qz1r3wyrwxg3glxs0rcgspg6z85li8343q96bni2xhhkdxywizs")))

(define-public crate-md5-rs-0.1.3 (c (n "md5-rs") (v "0.1.3") (h "0sc2bknzrzsmj2w637rfsjpsqbx3p2rmn01dq2s28hqrgbhrl8aq")))

(define-public crate-md5-rs-0.1.4 (c (n "md5-rs") (v "0.1.4") (h "1fzlbzp2ipiyzb0zsibfm65j9679w33yn95dfp08rih3gynld45h")))

(define-public crate-md5-rs-0.1.5 (c (n "md5-rs") (v "0.1.5") (h "1qgvb6acgwahx8zii784fpxr3w1bfjjbzxkm4j34fva1pipxf8g7")))

