(define-module (crates-io md ka mdka) #:use-module (crates-io))

(define-public crate-mdka-0.0.1 (c (n "mdka") (v "0.0.1") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "0c9ql42f9b2vcacq43286cgjfsm82piymx38cd2krmfwzsl51c79") (r "1.75.0")))

(define-public crate-mdka-0.0.2 (c (n "mdka") (v "0.0.2") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "05vvn2z7vqdcjdvz139w9ws8qksv6k9zh6h3ypsnjv3h4h9cg4sv") (r "1.75.0")))

(define-public crate-mdka-0.0.3 (c (n "mdka") (v "0.0.3") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1pzdpg6ahnva6dw8xisajrcmcsrx6qrqprp8hmrna4dm39835g24") (r "1.75.0")))

(define-public crate-mdka-0.0.4 (c (n "mdka") (v "0.0.4") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1awm5dwg1kq8510agyq2681y6sadkqg1yi1mmxkdh8qw1ih755ym") (r "1.75.0")))

(define-public crate-mdka-0.0.5 (c (n "mdka") (v "0.0.5") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "11az6kvdnl6rv3c2lfr9yp2c3cghlsyf8ixiy9mccak4zzp4d5f2") (r "1.75.0")))

(define-public crate-mdka-0.0.6 (c (n "mdka") (v "0.0.6") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "05a4fb5y722wnvmjy0fxfb8x97jbqimvy3czq438hqmapp7giddh") (r "1.75.0")))

(define-public crate-mdka-0.0.8 (c (n "mdka") (v "0.0.8") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "0y7cyvrjdxmd3skffdqyh1ch2l48gzv2iqqry9yjjfazmmvcs9vw") (r "1.75.0")))

(define-public crate-mdka-0.0.7 (c (n "mdka") (v "0.0.7") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "0zskn6dcm37ry9r76iajjhh39mqsp5w5iijfz92crywv6dvhm637") (r "1.75.0")))

(define-public crate-mdka-0.0.9 (c (n "mdka") (v "0.0.9") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1nc97qrk2ryb66nd2bs5jqjchpmfcj7qqvdcmhncb2rwg471ysng") (r "1.75.0")))

(define-public crate-mdka-0.0.10 (c (n "mdka") (v "0.0.10") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "15kyrlnclxpnhvm7rr6ksp6mx66xkfw0p5qbsq3nc1n0ya36lbwr") (r "1.75.0")))

(define-public crate-mdka-0.1.0 (c (n "mdka") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "154gj62qm006cipi74c6351786v6fmv4wbxvr4whqnwk3rnr4qnl") (r "1.75.0")))

(define-public crate-mdka-0.2.0 (c (n "mdka") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "0hzyw2fw68psjs9v4z8dxp2vm4hnzvyffys603fzns5nff57pcya") (r "1.75.0")))

(define-public crate-mdka-0.2.1 (c (n "mdka") (v "0.2.1") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "09v8wdr9rm5ir5yimm4paay9jp7bslilxzs58w0x2jjl8wzbq2m1") (r "1.75.0")))

(define-public crate-mdka-0.3.0 (c (n "mdka") (v "0.3.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1bsllgqwhg6jjddvvdb5cl9ys0acjiz76rhd3h4mavaz1kc35060") (r "1.75.0")))

(define-public crate-mdka-0.4.0 (c (n "mdka") (v "0.4.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "12y9zim9khkw0l6jldvd42gdlx8jymbd74ps39n4bba47m68iz01") (r "1.75.0")))

(define-public crate-mdka-0.5.0 (c (n "mdka") (v "0.5.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "15r2rrypi6njvyxvlk23w5172vggah5vccmq5zmf5afg9vplzn51") (r "1.75.0")))

(define-public crate-mdka-0.5.1 (c (n "mdka") (v "0.5.1") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1n6jh3d197d6pnymkhyvj3ygd4w4h69b3x096z85abrcryvb7fyv") (r "1.75.0")))

(define-public crate-mdka-1.0.0 (c (n "mdka") (v "1.0.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1cvmq9pyw9hd973k911gqc46sgc0gr6dbnn32zm6k12f8v4phdji") (r "1.75.0")))

(define-public crate-mdka-1.0.1 (c (n "mdka") (v "1.0.1") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1ib7l8fjdjymdkzk9js6ak0qdspj7cjvz0wmd0i9rx4xhsk00bik") (r "1.75.0")))

(define-public crate-mdka-1.0.2 (c (n "mdka") (v "1.0.2") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "0qa7hwa1a4d7kcbb6wyl3z8ldgkxxh5yhxg63191rry60pb5m79n") (r "1.75.0")))

(define-public crate-mdka-1.0.3 (c (n "mdka") (v "1.0.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "0c3f8m0hm4n7w10yijbw7qfay2ddgwqm58nl0vfpmv6d5p9xchzl") (r "1.75.0")))

(define-public crate-mdka-1.0.4 (c (n "mdka") (v "1.0.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "15h3cjkbzk46yc76irii0bx9czgx3fwa518pbaqr9shjxd23p2x2") (r "1.75.0")))

(define-public crate-mdka-1.0.5 (c (n "mdka") (v "1.0.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "0689kaafg6kmg3lkvfd0sgcrrrp0rhivy8vd9fkpw0nxxh7idhg1") (r "1.75.0")))

(define-public crate-mdka-1.0.6 (c (n "mdka") (v "1.0.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "071mllhpq3cyj3jm7170d4wssi8x32q9ra762ksccmwbf4qs0z3x") (r "1.75.0")))

(define-public crate-mdka-1.0.7 (c (n "mdka") (v "1.0.7") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "19wx5va3hb8b8l7y8zv6h9zm1995g2g13pa9g4d0jrld2nl89hv4") (r "1.75.0")))

(define-public crate-mdka-1.0.8 (c (n "mdka") (v "1.0.8") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1igb22xlsgjvp24271jyz7q0yj9r59z1daacikkdasj5ydzkg8wg") (r "1.75.0")))

(define-public crate-mdka-1.1.0 (c (n "mdka") (v "1.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)))) (h "161awhpm7m6sa7bh3bjnh686m94lkqmnsvciy459rak82rbpyphz") (r "1.74.1")))

(define-public crate-mdka-1.1.1 (c (n "mdka") (v "1.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)))) (h "1iwnm8ba8cxvmz8fyh42x4yb585lcmjc07mx5zn1mifqm9x84mpp") (r "1.74.1")))

(define-public crate-mdka-1.2.0 (c (n "mdka") (v "1.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)))) (h "0ynp9mzf5q4iv3qn8l7yikxlx4djx4rll2hlgjhmqpwf2m5aj9n6") (r "1.74.1")))

(define-public crate-mdka-1.2.1 (c (n "mdka") (v "1.2.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)))) (h "0153s2wb9m7sfjwic24d6f4bky6k5a6xjbi2gymxz53n88bxsp1c") (r "1.75.0")))

(define-public crate-mdka-1.2.2 (c (n "mdka") (v "1.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)))) (h "0fh10j6xbw9abc9x28kyg1q8wfxzd349p1b9m0lsqq2qh0001kyv") (r "1.76.0")))

(define-public crate-mdka-1.2.3 (c (n "mdka") (v "1.2.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)))) (h "1k7rrc9d72wfclrkcfk1gyxg8v6gc6r6ajklh38gf2c0375bdilh") (r "1.76.0")))

(define-public crate-mdka-1.2.4 (c (n "mdka") (v "1.2.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)))) (h "1my9gnbpy7x5hpkzn4svm4lsfdwh1j550iccr1dwws2bkccxa3hb") (r "1.74.0")))

(define-public crate-mdka-1.2.5 (c (n "mdka") (v "1.2.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "html5ever") (r "^0.27") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.3") (d #t) (k 0)))) (h "0n14w4iv7kpfwsa42ldnii4v85g9a75rfvp2wvrpjihxbxp88xmq") (r "1.74.0")))

