(define-module (crates-io md ka mdka-rs) #:use-module (crates-io))

(define-public crate-mdka-rs-0.0.1 (c (n "mdka-rs") (v "0.0.1") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "05xxv2pdqwcak2r5h9hxjfxyq8y0icsz9jlsjrl2ff7kwnmsjqr5") (y #t) (r "1.75.0")))

(define-public crate-mdka-rs-0.0.2 (c (n "mdka-rs") (v "0.0.2") (h "174mag92sff0q46hfsy88lv16hi53lxzsfxlj0rmz89a66x1h1pv") (y #t)))

(define-public crate-mdka-rs-0.0.3 (c (n "mdka-rs") (v "0.0.3") (h "1ffrj6z3sn9j55jxh9km564y43dqh4v83b6aj1h8gj9m4kqx3rhl") (y #t)))

