(define-module (crates-io md ur mdurl) #:use-module (crates-io))

(define-public crate-mdurl-0.1.0 (c (n "mdurl") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0f31yz3c4qwjq7jxw46lqn2g12d60hv9s3w1l9vbipqyz6y886mj")))

(define-public crate-mdurl-0.1.1 (c (n "mdurl") (v "0.1.1") (d (list (d (n "once_cell") (r ">=0.2.0, <2") (d #t) (k 0)) (d (n "regex") (r ">=0.2.0, <2") (d #t) (k 0)))) (h "0rbl513h0sy2bfi4bf329mnnqa2mzh0i38cjm87c55y3p1fxhplj")))

(define-public crate-mdurl-0.2.0 (c (n "mdurl") (v "0.2.0") (d (list (d (n "idna") (r ">=0.1.0, <0.4") (d #t) (k 0)) (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "regex") (r ">=0.2.0, <2") (d #t) (k 0)))) (h "0fm4fdlnavfvlg81cq2wjjx2jkjg658rlhazkrawdycz0nw7p219")))

(define-public crate-mdurl-0.3.0 (c (n "mdurl") (v "0.3.0") (d (list (d (n "idna") (r ">=0.1.0, <0.4") (d #t) (k 0)) (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "regex") (r ">=0.2.0, <2") (d #t) (k 0)))) (h "0pl7mbpkvn2j3n62ya45qdsxxx330nqvbwl5jia79217ir23jnqh")))

(define-public crate-mdurl-0.3.1 (c (n "mdurl") (v "0.3.1") (d (list (d (n "idna") (r ">=0.1.0, <0.4") (d #t) (k 0)) (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "regex") (r ">=0.2.0, <2") (d #t) (k 0)))) (h "1lphpz6yc5151w23m5zcp8c8ll2yx26gi5x8k767r3xcpd2vldjp")))

