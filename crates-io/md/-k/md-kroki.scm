(define-module (crates-io md -k md-kroki) #:use-module (crates-io))

(define-public crate-md-kroki-0.1.0 (c (n "md-kroki") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.2") (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "sscanf") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)))) (h "0nzb5jmj5iinqn84smw498v139gjvpfmms4lrf8ywbb187r8ca3j")))

