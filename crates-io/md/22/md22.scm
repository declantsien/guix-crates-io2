(define-module (crates-io md #{22}# md22) #:use-module (crates-io))

(define-public crate-md22-0.1.0 (c (n "md22") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 0)))) (h "0gc5jrc4zsy668z7b48821kl0s6vf0ia8a24bkrv7kdqq50ilv2g")))

