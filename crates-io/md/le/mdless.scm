(define-module (crates-io md le mdless) #:use-module (crates-io))

(define-public crate-mdless-0.1.0 (c (n "mdless") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "02yi686jkvdn1x9c8a6qz1rpdcrk9jvpndwl8cxawpiq5irq8xfw") (y #t)))

(define-public crate-mdless-0.1.1 (c (n "mdless") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1r5v3dbxpic2k7w0g3ya6nvjsix1w09r6yvax6la5b6zb9cc1ikl") (y #t)))

(define-public crate-mdless-0.2.0 (c (n "mdless") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)) (d (n "syntect") (r "^2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1jlqb0kap7w812ilwy2i0kwdggb0ysfmvx54p6m6qm28rpnm8yhy") (y #t)))

