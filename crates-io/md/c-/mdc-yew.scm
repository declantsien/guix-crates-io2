(define-module (crates-io md c- mdc-yew) #:use-module (crates-io))

(define-public crate-mdc-yew-0.1.0 (c (n "mdc-yew") (v "0.1.0") (h "080sn70sadkprs5j0kwh3xqsaxdijn45nb3lpkn1hpbg2smlhvpr")))

(define-public crate-mdc-yew-0.1.1 (c (n "mdc-yew") (v "0.1.1") (d (list (d (n "mdc-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "yew") (r "^0.10") (k 0)))) (h "0wp34hjr8917hfj7mickanrcwy43py45q7scjm7xrj4zb7x39wa8")))

