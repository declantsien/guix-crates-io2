(define-module (crates-io md c- mdc-sys) #:use-module (crates-io))

(define-public crate-mdc-sys-0.1.0 (c (n "mdc-sys") (v "0.1.0") (h "11h4jd7p2dyhmxl0vb6442pya3fim6dcbi663gld139w4jhcv109")))

(define-public crate-mdc-sys-0.1.1 (c (n "mdc-sys") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.57") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.34") (f (quote ("Element" "Window" "Document" "Event"))) (d #t) (k 0)))) (h "1cwiknisnlvp1s35a93rqj6jcnwx33kwqxbv84jk69023pm2k5dr")))

