(define-module (crates-io md rv mdrv) #:use-module (crates-io))

(define-public crate-mdrv-0.0.4 (c (n "mdrv") (v "0.0.4") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0") (d #t) (k 0)))) (h "0sb20g65lw5k6p8y689k5n82c6a64p8h9m5vg2ggjscqaz25303c")))

(define-public crate-mdrv-0.0.5 (c (n "mdrv") (v "0.0.5") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0") (d #t) (k 0)))) (h "0ixxhaddb8i8ghc9wv9wbik8bh7y8787s5jc3nrkxi1yvzr7m9lj")))

