(define-module (crates-io md #{2s}# md2src) #:use-module (crates-io))

(define-public crate-md2src-0.0.1 (c (n "md2src") (v "0.0.1") (d (list (d (n "pulldown-cmark") (r "^0.7") (f (quote ("simd"))) (k 0)))) (h "19s8aaqc4svm3vpxrk93zrivk80k5nlkyca9bf5vms087srhjfzz")))

(define-public crate-md2src-1.0.0 (c (n "md2src") (v "1.0.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7") (f (quote ("simd"))) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "11wb94cfw4305jbh8wprwd53caw9j9n6yzgga7yrpx8q9y387czv")))

(define-public crate-md2src-1.0.1 (c (n "md2src") (v "1.0.1") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7") (f (quote ("simd"))) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1f76fk655dn6ybwrvcxqmssw0rn33bv5249fszd3kj0qkll2pk7a")))

(define-public crate-md2src-1.0.2 (c (n "md2src") (v "1.0.2") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7") (f (quote ("simd"))) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1na92vr0pi992mzr6n8qqrl89cgivyqzvqz6snnrg7i7c91da26v")))

(define-public crate-md2src-1.1.0 (c (n "md2src") (v "1.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7") (f (quote ("simd"))) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1qd7mkkw6g6lrqbmbr7dna0nha72i3f9sakxfm2dqqlvvpxbmw9x")))

