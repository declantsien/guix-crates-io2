(define-module (crates-io md sh mdsh) #:use-module (crates-io))

(define-public crate-mdsh-0.1.0 (c (n "mdsh") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1hq32fgyzgzhx97dfdf1lcrv199pildw93g4kz898z989xxxjm0p")))

(define-public crate-mdsh-0.1.1 (c (n "mdsh") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1ighlx6hql4zw0kkz6chhyhm1mykmq460vshz4ydf0i1kpg87hp9")))

(define-public crate-mdsh-0.1.2 (c (n "mdsh") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "17hipkdavbn04s9qlz71addppmzyia0hxkz6why2c992n0am280m")))

(define-public crate-mdsh-0.1.3 (c (n "mdsh") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1swx76d22f2s2k5vvvpv9h9jzf0jpr3bpk1l32n6dg59izxazg2n")))

(define-public crate-mdsh-0.1.5 (c (n "mdsh") (v "0.1.5") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0dv8i4666xfl2s19s1rm9qfk8smrk2j1bbxnq8bx7zliyxj47kqj")))

(define-public crate-mdsh-0.4.0 (c (n "mdsh") (v "0.4.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "00d6ygqa4jxr0nkbsjlq3wsrppwxr7ibylyc4slbbfa0sdmz569k")))

(define-public crate-mdsh-0.5.0 (c (n "mdsh") (v "0.5.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1ip5c4ybv62xjhf9l4x1wnhgf5jnn0sijlip8lbv55nijav9lq7v")))

(define-public crate-mdsh-0.6.0 (c (n "mdsh") (v "0.6.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0jljhz7xd6h6jbxpb3jnfbpmy347qh6zblb5k50wdk3qwq5xlpcb")))

(define-public crate-mdsh-0.7.0 (c (n "mdsh") (v "0.7.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0nrasgh3q25mhzzfp82329prh24i652gsywr3f6rzgvrsy57va17")))

