(define-module (crates-io gq db gqdb) #:use-module (crates-io))

(define-public crate-gqdb-0.1.0-alpha.1 (c (n "gqdb") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "codes-iso-3166") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "secp256k1") (r "^0.28.0") (f (quote ("global-context" "rand-std" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0zpxjrqd30xz8bwbmi31l0dmj1xwc3vx1d0s0v7hy3w7sygjivqm")))

