(define-module (crates-io gq lo gqlog) #:use-module (crates-io))

(define-public crate-gqlog-0.1.0 (c (n "gqlog") (v "0.1.0") (d (list (d (n "graphql-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1myjs9p6vpvrikd65sydxd3q57ppqjxwyq1djsw6kz0l0zc71dxd")))

(define-public crate-gqlog-0.1.1 (c (n "gqlog") (v "0.1.1") (d (list (d (n "graphql-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "0p4xyhwahhhswflbvwlj7kfg81i9hxxm1kdm3x17j1fbfr88fkqi")))

(define-public crate-gqlog-1.0.0 (c (n "gqlog") (v "1.0.0") (d (list (d (n "graphql-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1npfaxp98rwnh2hvs4sdb7dgmw1ma955vlpq2wr61frpaa5i5h0i")))

(define-public crate-gqlog-1.0.1 (c (n "gqlog") (v "1.0.1") (d (list (d (n "graphql-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0brw9az74yzi6zzzlc5wzr2gx0mi6x5mcscn0nf6cwn3sxgyxrha")))

(define-public crate-gqlog-1.0.2 (c (n "gqlog") (v "1.0.2") (d (list (d (n "graphql-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0z03hrvvi0d88mzpk53fcbs8qfz9y5613kbyfh7ig3g1zyjh694k")))

(define-public crate-gqlog-1.0.3 (c (n "gqlog") (v "1.0.3") (d (list (d (n "graphql-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0n929m4mgxky8mbm1plaxhw5y7icllagwga0h6p7b5jnsd6gyvxp")))

