(define-module (crates-io gq lr gqlrequest) #:use-module (crates-io))

(define-public crate-gqlrequest-0.1.1 (c (n "gqlrequest") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1il1vdjfdjxy0plsp0gyyhaq2s8lbp60m3w9qslz5syq61sjyk9l")))

