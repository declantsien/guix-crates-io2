(define-module (crates-io gq l_ gql_client) #:use-module (crates-io))

(define-public crate-gql_client-0.1.0 (c (n "gql_client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "15rbbmkw3ffci0xhz6yagx1xhimfcyi1djkfs6j7p3ylhnh1zyqp")))

(define-public crate-gql_client-0.1.1 (c (n "gql_client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1gwf961hd834h4b0lrcig013xdp0pi02jhlwb88hrhz4226bapz9")))

(define-public crate-gql_client-0.2.1 (c (n "gql_client") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "11k3cyn5ls0hn44gi0nkgixh6liamq6m2lf3ddc8c7qp7c05273c")))

(define-public crate-gql_client-1.0.0 (c (n "gql_client") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0r05z659cmi3i8rikdjgj60wdvdzr89xww97rid1ndyih31inlfj")))

(define-public crate-gql_client-1.0.2 (c (n "gql_client") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1kzlkgavyg4rjiz9881gwxjiqjvr3jd5i84yqwijx3ppwng8wmk0")))

(define-public crate-gql_client-1.0.3 (c (n "gql_client") (v "1.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1423p89gv31a2fi33xcfnxi8wgj60pdjfi810rz3n8lf783wsf36")))

(define-public crate-gql_client-1.0.4 (c (n "gql_client") (v "1.0.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0qcsllbwkp52cdc0n2b6id7qffpzbsp143jmx3m8v554nb8aa5yq")))

(define-public crate-gql_client-1.0.6 (c (n "gql_client") (v "1.0.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0zfq2zq5pxlvgw6jr4b602fhqi81yzh9jbvn4g6klw7dinbhpbd2")))

(define-public crate-gql_client-1.0.7 (c (n "gql_client") (v "1.0.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1nxi2w766sgmz3k9a65widbk63k2gq9s4gzhipz4zkg5sdi4hzz5")))

