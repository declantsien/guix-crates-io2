(define-module (crates-io gq li gqlint) #:use-module (crates-io))

(define-public crate-gqlint-0.1.0 (c (n "gqlint") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "graphql-lint") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "045c5d81pbyygf2lpnkhrinqmsllldz4g3p7b4hz330zjlf5hvfl")))

(define-public crate-gqlint-0.1.1 (c (n "gqlint") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "graphql-lint") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0l0zyypyn2zblh2rhjpa3sr81qmsdiikkwzjqx5h6lzgqad6j1x2")))

