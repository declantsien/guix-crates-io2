(define-module (crates-io gq ai gqair) #:use-module (crates-io))

(define-public crate-gqair-0.1.0 (c (n "gqair") (v "0.1.0") (d (list (d (n "gdk") (r "^0.12") (d #t) (k 0)) (d (n "gio") (r "^0.8.1") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.8.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "qair") (r "^0.6.0") (k 0)) (d (n "qr2cairo") (r "^0.1.0") (d #t) (k 0)))) (h "14gi31f4xyb6hap0agbf2r6iifpchvs33iq1jg183ilqbskc68qf")))

(define-public crate-gqair-0.1.1 (c (n "gqair") (v "0.1.1") (d (list (d (n "gdk") (r "^0.12") (d #t) (k 0)) (d (n "gio") (r "^0.8.1") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.8.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "qair") (r "^0.7.0") (k 0)) (d (n "qr2cairo") (r "^0.1.0") (d #t) (k 0)))) (h "0kml5l3vgqwlp7m9q26g155j7asjyjxb6vcfpb9ncpwjx5grbfl5")))

