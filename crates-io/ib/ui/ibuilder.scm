(define-module (crates-io ib ui ibuilder) #:use-module (crates-io))

(define-public crate-ibuilder-0.1.0 (c (n "ibuilder") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "07fm9pjrv3sg2679la1v11xicv7f3rz3aa36pnij2d5c9m99vcrp") (f (quote (("derive" "ibuilder_derive") ("default" "derive"))))))

(define-public crate-ibuilder-0.1.1 (c (n "ibuilder") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0fdcfh5phsgvm6drzzjjmg5aq6dqks4fzizdhibz6803vw67gm7f") (f (quote (("derive" "ibuilder_derive") ("default" "derive"))))))

(define-public crate-ibuilder-0.1.2 (c (n "ibuilder") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1prn354mvqima4hhj66ll44kczhyc43q3chi0grznxl26zgfxf4x") (f (quote (("derive" "ibuilder_derive") ("default" "derive"))))))

(define-public crate-ibuilder-0.1.3 (c (n "ibuilder") (v "0.1.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.3") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1wrclgm72a385imfbvlwxc01ybz0iahjdahyb6kwpjnc22y223pc") (f (quote (("derive" "ibuilder_derive") ("default" "derive"))))))

(define-public crate-ibuilder-0.1.4 (c (n "ibuilder") (v "0.1.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nzz7gnnjkc3i8apgjvx1jy8shx1i1y25257f4padyjzils3q7yx") (f (quote (("derive" "ibuilder_derive") ("default" "derive"))))))

(define-public crate-ibuilder-0.1.5 (c (n "ibuilder") (v "0.1.5") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "003w49w63gfsnpzzdz8hk869ycj8rk9zf6mikf6vvzhwkapqb795") (f (quote (("derive" "ibuilder_derive") ("default" "derive"))))))

(define-public crate-ibuilder-0.1.6 (c (n "ibuilder") (v "0.1.6") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.6") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0pmfsvgwx6g9h30scmk9ng67wg7ygvjmsm37jzyrajwzsw34irlz") (f (quote (("derive" "ibuilder_derive") ("default" "derive"))))))

(define-public crate-ibuilder-0.1.7 (c (n "ibuilder") (v "0.1.7") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.7") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0y0q48d9a5b5b2dgka1asy9fdxkr1i944w9aaz511c9dp5dhxr1q") (f (quote (("derive" "ibuilder_derive") ("default" "derive"))))))

(define-public crate-ibuilder-0.1.8 (c (n "ibuilder") (v "0.1.8") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "ibuilder_derive") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "09k9gy802gfwars71sszgwkgzvy4awghd3zmwggj50j1qmzsdbrw") (f (quote (("derive" "ibuilder_derive") ("default" "derive"))))))

