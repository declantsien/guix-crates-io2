(define-module (crates-io ib ui ibuilder_derive) #:use-module (crates-io))

(define-public crate-ibuilder_derive-0.1.0 (c (n "ibuilder_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17w63xlmmb6b9nnipqyqmnsxndbscd276l06dih74905ijs6v4ha")))

(define-public crate-ibuilder_derive-0.1.1 (c (n "ibuilder_derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02y6agxral5hnbg658jkf899zd3awj82xcs8gp6wbqrr7nm6cgrq")))

(define-public crate-ibuilder_derive-0.1.2 (c (n "ibuilder_derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0c329xa5z91l9232k2fswbkarisvq9zsyvxvy1mjv97j2y50f9p1")))

(define-public crate-ibuilder_derive-0.1.3 (c (n "ibuilder_derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qcq2c5z7y2gj7i32r7kgcn8l24qrfxgplzkq945ph9ixgbrv7ab")))

(define-public crate-ibuilder_derive-0.1.4 (c (n "ibuilder_derive") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1365ln3nj57mxk9lldmif2l3pcry3x3b6sfwn8pl3fchv11ga8kd")))

(define-public crate-ibuilder_derive-0.1.5 (c (n "ibuilder_derive") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0abn11yzvbvlfaq29g3paqhvp0a03a708y2i0glwq6ykig2aig7j")))

(define-public crate-ibuilder_derive-0.1.6 (c (n "ibuilder_derive") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17n8j015q1lrhwi830mwhspps7zrz9x4kkvlsmk6zan6mibhvcsb")))

(define-public crate-ibuilder_derive-0.1.7 (c (n "ibuilder_derive") (v "0.1.7") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nq9kd3fg5xsjglvszddpj8splhcc1fwzfwblyg9yib712rflr7c")))

(define-public crate-ibuilder_derive-0.1.8 (c (n "ibuilder_derive") (v "0.1.8") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wd3cyf9yd5p2r2mqjkr2p8wwal3x0fx58llfvir0g0pha4yyp2b")))

