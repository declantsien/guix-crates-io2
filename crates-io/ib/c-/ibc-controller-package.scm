(define-module (crates-io ib c- ibc-controller-package) #:use-module (crates-io))

(define-public crate-ibc-controller-package-1.0.0 (c (n "ibc-controller-package") (v "1.0.0") (d (list (d (n "astroport-governance") (r "^1") (d #t) (k 0)) (d (n "astroport-ibc") (r "^1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)))) (h "0cvw3xkqfl4wb2asdhh1sn9a64aw5albcssynknby5hpd7sr9kyz")))

