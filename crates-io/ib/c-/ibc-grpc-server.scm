(define-module (crates-io ib c- ibc-grpc-server) #:use-module (crates-io))

(define-public crate-ibc-grpc-server-0.1.0 (c (n "ibc-grpc-server") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ibc") (r "^0.20") (d #t) (k 0)) (d (n "ibc-proto") (r "^0.21") (f (quote ("server"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)))) (h "1csz5p2a3bs7xl1caiiw0yb1cj6a1vcdb45lwydpapqzs95m8yry")))

