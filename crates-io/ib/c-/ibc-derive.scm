(define-module (crates-io ib c- ibc-derive) #:use-module (crates-io))

(define-public crate-ibc-derive-0.1.0 (c (n "ibc-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0d0i4hv8rhmhwyk5fh1v82h0nnrmlkyp32c8p4ry19zdyk20hlfq")))

(define-public crate-ibc-derive-0.2.0 (c (n "ibc-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "19nh94gl786idd38ydl4ssx38rjyhvns05ni6kacagdh55w8aa16")))

(define-public crate-ibc-derive-0.3.0 (c (n "ibc-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0x0nq6h3p5j7icgrvxc7v74w2qs7vlxi8v5pmmdlkjp96n2m5wcj")))

(define-public crate-ibc-derive-0.4.0 (c (n "ibc-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0j25xf3dd3vslz6i1nx86sdy8rvrw0rpcjdp0qajjpp6q5dvy1yz")))

(define-public crate-ibc-derive-0.5.0 (c (n "ibc-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0qf4z6c31zw37r8isk32wklnfg9fq2yparmqbdwbyamrg8y1xb4c")))

(define-public crate-ibc-derive-0.6.0 (c (n "ibc-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0vh1r1njwq1gm78f45p2n3hvay40zhkbndhaf95hkmnpz6gydq9x")))

(define-public crate-ibc-derive-0.6.1 (c (n "ibc-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0m61pbmd6xhn39y87va7jr7qixx94ij98nyh4jfc1zmpyfn10l1z")))

(define-public crate-ibc-derive-0.7.0 (c (n "ibc-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1acamlzbyhddwyy5bamsmzw6x44ih2vjwpc3c6cj5mag37963n93")))

