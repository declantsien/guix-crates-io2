(define-module (crates-io ib c- ibc-types-domain-type) #:use-module (crates-io))

(define-public crate-ibc-types-domain-type-0.3.0 (c (n "ibc-types-domain-type") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "110z2b1gkjbflzg665z4wd17sg50m32q09bk6lkmawdl3qyxabcj") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.4.0 (c (n "ibc-types-domain-type") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "08603vavg62ny9g16vfl1k8q1klakyya3g6gyfmi3in8ysdrl3zn") (y #t) (r "1.60")))

(define-public crate-ibc-types-domain-type-0.5.0 (c (n "ibc-types-domain-type") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "1031n7smf1yvlg44swwsbhzvfndi4xrw99cnpqrrshdmkzviz4zr") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.6.0 (c (n "ibc-types-domain-type") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "00v0ydm9rjf0gwl4v3j339jyavab2pgb5zv0s6k6srijssp05smj") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.6.1 (c (n "ibc-types-domain-type") (v "0.6.1") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "1yv6ypvdqfy1bpnl0dn76wcwy2nc84dz1i2gvs2zx4nqcz922bvg") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.6.2 (c (n "ibc-types-domain-type") (v "0.6.2") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "022s4izbfff5r5vg575x6jrkywlmbzxs04rg2dmhdf42n78jhwzj") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.6.3 (c (n "ibc-types-domain-type") (v "0.6.3") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "0syc4rhkqakyncygsmyidfg8hlq85m2hgbyc8svg6zlsymx82m74") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.6.4 (c (n "ibc-types-domain-type") (v "0.6.4") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "10a5zkqqc94fvmd0wp2dp6b3cdsbc2i1q523x9plix0znryfcsg4") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.7.0 (c (n "ibc-types-domain-type") (v "0.7.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "0fdccjhj2wmjvwr5i1crrfmyb4kr9gmmdk8px2dyrl3l3i1aqshp") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.8.0 (c (n "ibc-types-domain-type") (v "0.8.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "05d0ahcqfq5ghg2xyfjmci93k44q4rgacz6x0miblnzprbr2xw8l") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.9.0 (c (n "ibc-types-domain-type") (v "0.9.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "0zc1r7hn3c0psfv207f79fw8cv601d2xhaz8mqrj68c6srydqs2z") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.10.0 (c (n "ibc-types-domain-type") (v "0.10.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "1d0w8vjz658ba6hq1j0n6hb0k5zl4j136bibnyci7h2a6p5a0jiw") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.11.0 (c (n "ibc-types-domain-type") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "0jpx92q6w3rkwqjv1b7yqnn9inihz3x2qd94d35k2051vmqn7h8a") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.11.1 (c (n "ibc-types-domain-type") (v "0.11.1") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "0ya78ilhjjadm7idxhkzqfajhli9hkwz6z7gaizrdym4va1rcr3k") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.12.0 (c (n "ibc-types-domain-type") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "06qhws2lbgrszvl9cbgqsvjd4wnsblrpzz259y002wnxp4crdg1s") (r "1.60")))

(define-public crate-ibc-types-domain-type-0.13.0 (c (n "ibc-types-domain-type") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "02s019v523wi610afz7wpf8d8k1zi1lnz7ga8pc9366gdyqj4ykp") (r "1.60")))

