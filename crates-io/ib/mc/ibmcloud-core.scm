(define-module (crates-io ib mc ibmcloud-core) #:use-module (crates-io))

(define-public crate-ibmcloud-core-0.1.0 (c (n "ibmcloud-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1rgm0f9m399mkjs9l2m3krcjsjvxwp1i4qnvr44kgyj2x0r5x577") (y #t)))

(define-public crate-ibmcloud-core-0.1.1 (c (n "ibmcloud-core") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "00pbni0ldqhqjqfgljhqgfx08659ps16srdbb8dspl5kglgzaz5r") (y #t)))

(define-public crate-ibmcloud-core-0.1.2 (c (n "ibmcloud-core") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "16ra5ad3prnl8sfxhpwl4vd42cipdvdwhh77z6xgjrxrl7im3wk6") (y #t)))

(define-public crate-ibmcloud-core-0.1.3 (c (n "ibmcloud-core") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1c7b4r3if4j0in6yq7w7mjwqcyb4bgr8db2brl8ywim65jd6mn57") (y #t)))

(define-public crate-ibmcloud-core-0.0.1 (c (n "ibmcloud-core") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "mockito") (r "^0.30.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1rvhy2b5kblgsx6pw63wj1cr3ngbkx3q94kw8bfi83j33j39j99v")))

