(define-module (crates-io ib mf ibmfloat) #:use-module (crates-io))

(define-public crate-ibmfloat-0.1.0 (c (n "ibmfloat") (v "0.1.0") (h "16lvl1vpaqxayvngw5d120dxw27z7rd1nfmh0j5qz7nbm5lhslsr")))

(define-public crate-ibmfloat-0.1.1 (c (n "ibmfloat") (v "0.1.1") (h "0pf8y5w36qlicb134fgmychb8q08nca4hiwwk883vvck8jxl6n47") (f (quote (("std") ("default" "std"))))))

