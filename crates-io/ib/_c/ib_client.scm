(define-module (crates-io ib _c ib_client) #:use-module (crates-io))

(define-public crate-ib_client-1.0.0 (c (n "ib_client") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "15f4jg6vlgcrkavj093j5b4g3nhipfb8fbrpar6ikpigjnvc0827")))

