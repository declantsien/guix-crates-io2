(define-module (crates-io ib an iban_validator) #:use-module (crates-io))

(define-public crate-iban_validator-0.1.0 (c (n "iban_validator") (v "0.1.0") (h "1v4i0p6mniq2ic077zynpmpd6c5ca28in8i1bds72mrh1fxzsihc")))

(define-public crate-iban_validator-0.1.2 (c (n "iban_validator") (v "0.1.2") (h "1cpxyw5dxg340rzxw505yx16fxxzvs7rykzh27gapi55awni5jby")))

(define-public crate-iban_validator-0.2.0 (c (n "iban_validator") (v "0.2.0") (h "1gf2rpx0m4qg1qnh60j88all5p20ni1q5igcz9sfsqrrr2wzgn66")))

(define-public crate-iban_validator-0.2.1 (c (n "iban_validator") (v "0.2.1") (h "0b97ycfbd29cm4mdq6670ig59fc9xmk2392bnb4wnsgwbzwq8lm8")))

(define-public crate-iban_validator-0.2.2 (c (n "iban_validator") (v "0.2.2") (h "1kc5qxnpy7i34yyjg321l9vmgn5wvygmmdm6hms74alg9y7v13mf")))

