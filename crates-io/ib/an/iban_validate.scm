(define-module (crates-io ib an iban_validate) #:use-module (crates-io))

(define-public crate-iban_validate-0.1.0 (c (n "iban_validate") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0x5l03k4vx8qajr15cbdnjl1ld335dccn7zzbn6kbbi82a3sq83m")))

(define-public crate-iban_validate-0.1.1 (c (n "iban_validate") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1sqa6rw1fl8i1z23yg61c8i56nx3sfnw78sz0w5z55hvz7v795ji")))

(define-public crate-iban_validate-0.2.0 (c (n "iban_validate") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)))) (h "052z4b56w52bv2clxfir8rjq689559ry8w73775asvi95gx9jaqp")))

(define-public crate-iban_validate-0.2.1 (c (n "iban_validate") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)))) (h "0vkwj0alkbminypw76k6wr44z1hqfpicypwjlyym2h48md8yl6ys")))

(define-public crate-iban_validate-0.3.1 (c (n "iban_validate") (v "0.3.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)))) (h "0nqwky2cqrxzg91l2gmrhyr50x3nv7ppwzrdrrxm2n2bp8nfjy3b")))

(define-public crate-iban_validate-0.4.0 (c (n "iban_validate") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0mx9xfkrzlnjk55lf9bibfc3s60i0xmlf5il694ggb81jhmknr9n")))

(define-public crate-iban_validate-1.0.0 (c (n "iban_validate") (v "1.0.0") (d (list (d (n "expectest") (r "^0.7.0") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1dcwf9c1wqxj32simbda64sa13wnb0p6qp0n8adp2ki1max8x5h9")))

(define-public crate-iban_validate-1.0.1 (c (n "iban_validate") (v "1.0.1") (d (list (d (n "expectest") (r "^0.7.0") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0xb5yriizb62flfzzm6gj8rha3zkykw1f587vgi5hx99pkg49yhb")))

(define-public crate-iban_validate-1.0.2 (c (n "iban_validate") (v "1.0.2") (d (list (d (n "expectest") (r "^0.9.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "0zb59da7zd4wq0i35463g0k7vcj5iw2lcmdkmpp1nfcqv6an1p3d")))

(define-public crate-iban_validate-1.0.3 (c (n "iban_validate") (v "1.0.3") (d (list (d (n "expectest") (r "^0.9.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1jgs82kmdqs8a3rqv5igl6l4kpy508b427mpbldwrsdygfaxrfk5")))

(define-public crate-iban_validate-2.0.0 (c (n "iban_validate") (v "2.0.0") (d (list (d (n "expectest") (r "^0.11.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "055iizan3ljaxy9y0vb3krgrhsfbi5rik981zz4yfsv5ssig64kl")))

(define-public crate-iban_validate-3.0.0 (c (n "iban_validate") (v "3.0.0") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128"))) (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "serde"))) (d #t) (t "cfg(feature = \"serde\")") (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.2") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ji5yl6rcjblw9bbfc0gn3fkwyzaqxrza1idj9mk8yjaim4pg4q9") (f (quote (("intra_rustdoc_links"))))))

(define-public crate-iban_validate-4.0.0 (c (n "iban_validate") (v "4.0.0") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128"))) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "05g17xyg92fz7s4xag86mz5i3072yd47394zlbnz6ksk8qhbn7df") (f (quote (("std") ("intra_rustdoc_links") ("default" "std"))))))

(define-public crate-iban_validate-4.0.1 (c (n "iban_validate") (v "4.0.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "027dh6gnqlfwi7ai52wr2nirq2knkmf4j6vgcpl1k678ga7ka7fc") (f (quote (("std") ("intra_rustdoc_links") ("default" "std"))))))

