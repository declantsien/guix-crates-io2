(define-module (crates-io ib an iban_parser) #:use-module (crates-io))

(define-public crate-iban_parser-0.1.0 (c (n "iban_parser") (v "0.1.0") (h "015br3jkqilp5x9gfa875kbgkv1zcdj0j1zak5x10rgbj9zna17m")))

(define-public crate-iban_parser-0.1.2 (c (n "iban_parser") (v "0.1.2") (h "08ivfhjq0qpr0qcjr9b75qfkra0ss2937hcghm39rzpxv0wgnfkc")))

(define-public crate-iban_parser-0.2.0 (c (n "iban_parser") (v "0.2.0") (h "19bzhbyc7hhcyw3bb85ycmb6s03zg3nqpxf48y60lfisb6nxbj1c")))

(define-public crate-iban_parser-0.2.1 (c (n "iban_parser") (v "0.2.1") (h "1jjkz7fj7sql9krq5m2xhcqc5cl73c2rmrvc0gbpv4c8wfdh4wkc")))

(define-public crate-iban_parser-0.2.2 (c (n "iban_parser") (v "0.2.2") (h "16x368qbagd7z4jksxba6n5j4qhh7ag00k7bk38ij2nfyhkjczy4")))

