(define-module (crates-io ib an ibanexplode) #:use-module (crates-io))

(define-public crate-ibanexplode-0.0.1 (c (n "ibanexplode") (v "0.0.1") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "fints-institute-db") (r "^1.0") (d #t) (k 0)) (d (n "iban_validate") (r "^4") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "0ibp5g6zl95ikx3hxzgxgz29hsb5r5s780xsy7d0scfm52l301mr")))

