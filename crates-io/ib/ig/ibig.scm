(define-module (crates-io ib ig ibig) #:use-module (crates-io))

(define-public crate-ibig-0.0.0 (c (n "ibig") (v "0.0.0") (h "1nc955wrc46yjlx9aq7v2b8iavsddrja0nykpq17aj0adhmhqpdr")))

(define-public crate-ibig-0.0.1 (c (n "ibig") (v "0.0.1") (h "0jp72qadpa3zbl78r5prakwhr7rjppwpwdsh7rfk1nf2q4sqi656") (f (quote (("std") ("default" "std"))))))

(define-public crate-ibig-0.1.0 (c (n "ibig") (v "0.1.0") (d (list (d (n "ascii") (r "^1.0.0") (k 0)))) (h "1ikmv01c2ihg2calqsg6azpgxlx0vzv4mak7vnv3ngv8qf8m47ca") (f (quote (("std") ("default" "std"))))))

(define-public crate-ibig-0.1.1 (c (n "ibig") (v "0.1.1") (d (list (d (n "ascii") (r "^1.0") (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1831fj2m9xv9a3qq1khk4kryc052gq0w5x2y33yw6h0gsdqw6jak") (f (quote (("std") ("default" "std" "rand"))))))

(define-public crate-ibig-0.1.2 (c (n "ibig") (v "0.1.2") (d (list (d (n "ascii") (r "^1.0") (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1csjsvw3x9i3ywrk5hm9fy11miqd1791mvm69mlc24xrbfy6333r") (f (quote (("std") ("default" "std" "rand"))))))

(define-public crate-ibig-0.2.0 (c (n "ibig") (v "0.2.0") (d (list (d (n "ascii") (r "^1.0") (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1vg5nk58nrf6ka74kwq6q5p9g4hxji7zbpcbc8lsy0hsvmz9br8f") (f (quote (("std") ("default" "std" "rand"))))))

(define-public crate-ibig-0.2.1 (c (n "ibig") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1kzfx5kva40vrljijyfc5lkhlvnqkp3x4nwfcq1w0k9v2r8angp3") (f (quote (("std") ("force-64-bit") ("force-32-bit") ("force-16-bit") ("default" "std" "rand" "num-traits"))))))

(define-public crate-ibig-0.2.2 (c (n "ibig") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0vp1dhmk1qryrba5yxfzrn5c41k4hhifpn5k82x11w3brwqlg1vg") (f (quote (("std") ("force-64-bit") ("force-32-bit") ("force-16-bit") ("default" "std" "rand" "num-traits"))))))

(define-public crate-ibig-0.3.0 (c (n "ibig") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1n14vpwx0czyp2bkaqpzs9idwgsh39g2lvpw4l7l32fys72121b3") (f (quote (("std") ("default" "std" "rand" "num-traits"))))))

(define-public crate-ibig-0.3.1 (c (n "ibig") (v "0.3.1") (d (list (d (n "const_fn_assert") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0fqfm7g1ydxl7kqypn46fb5svfjapc8iacriffpd8f65aynvlbm4") (f (quote (("std") ("default" "std" "rand" "num-traits"))))))

(define-public crate-ibig-0.3.2 (c (n "ibig") (v "0.3.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "const_fn_assert") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1dzqhaig7qs731y3xwzb7j819kc2r274pp1gpnqgx8ppwwp05ib1") (f (quote (("std") ("default" "std" "rand" "num-traits"))))))

(define-public crate-ibig-0.3.3 (c (n "ibig") (v "0.3.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "const_fn_assert") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "11475pqi16d2y1mlsm5c2k1d5pmppqap0nv4y7nrqqcm82w6aall") (f (quote (("std") ("default" "std" "rand" "num-traits"))))))

(define-public crate-ibig-0.3.4 (c (n "ibig") (v "0.3.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "const_fn_assert") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.130") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1vcx00lka5lwwlpr56jqq7qg756vymwa4liqkfqyvcqffgzj9ig5") (f (quote (("std") ("default" "std" "rand" "num-traits")))) (r "1.49")))

(define-public crate-ibig-0.3.5 (c (n "ibig") (v "0.3.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.130") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0nsr5l2rf0g5933p4an21rm1w3ksa7frizs1fys3fjnas19lml6p") (f (quote (("std") ("default" "std" "rand" "num-traits")))) (r "1.49")))

(define-public crate-ibig-0.3.6 (c (n "ibig") (v "0.3.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.130") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1fqp0yqnqh9ffci63a9zl9bakh9r0qva2r3mwzfpkh5j2vrwgz6i") (f (quote (("std") ("default" "std" "rand" "num-traits")))) (r "1.49")))

