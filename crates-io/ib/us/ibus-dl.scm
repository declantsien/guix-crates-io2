(define-module (crates-io ib us ibus-dl) #:use-module (crates-io))

(define-public crate-ibus-dl-0.1.0 (c (n "ibus-dl") (v "0.1.0") (d (list (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "0yxp92xbkrni5h7pzj3lb2dr46y4mrfydg9qn857sjh6fk6q0wyq")))

