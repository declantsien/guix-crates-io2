(define-module (crates-io ib us ibus) #:use-module (crates-io))

(define-public crate-ibus-0.1.0 (c (n "ibus") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xncnln8q4440ddvqhc6i0p4fx0jbmf0sbi9m0gvh5j0bw74qa1r")))

(define-public crate-ibus-0.2.0 (c (n "ibus") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sj4pjrvbi49yq6f4zp20x642ns8j4x5paip2fk0wfwxhh6drgrj")))

