(define-module (crates-io ib en ibench) #:use-module (crates-io))

(define-public crate-ibench-0.1.0 (c (n "ibench") (v "0.1.0") (h "1mdh8vdwyq6pfm7p4i5rs6q42qkw7lj72hnlfvqd3j9ll8366hax")))

(define-public crate-ibench-0.1.1 (c (n "ibench") (v "0.1.1") (h "1qml1xnx8d2lr9q4g5k6dpi368538lf95ra4n544ny9m4mhxyqga")))

