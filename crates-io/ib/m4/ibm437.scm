(define-module (crates-io ib m4 ibm437) #:use-module (crates-io))

(define-public crate-ibm437-0.1.0 (c (n "ibm437") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6") (d #t) (k 0)))) (h "14x8qnmsd5xvpks8fsp3iw3za2477grf60mrhb6b05hikfy27r4b")))

(define-public crate-ibm437-0.1.1 (c (n "ibm437") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.6") (d #t) (k 0)))) (h "1nijjzizxbz37rqwlhj1rmnpri05by1zayrwmn2p8zfmishjrzkb")))

(define-public crate-ibm437-0.1.2 (c (n "ibm437") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.6") (d #t) (k 0)))) (h "0dfcrmpy40sf116z05q9az40cy53hz5pivjx8kdz5plzzdxmh6mc")))

(define-public crate-ibm437-0.1.3 (c (n "ibm437") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.6") (d #t) (k 0)))) (h "0y06xphn176nwdfy6fl3lh70001haf341aynx17c2wsxy8ivxnbr")))

(define-public crate-ibm437-0.1.4 (c (n "ibm437") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (d #t) (k 2)))) (h "04hzvmhx142l358zcm161lkg1c6l82bryyh2fxdjw7x6921v0bvz")))

(define-public crate-ibm437-0.2.0 (c (n "ibm437") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 1)))) (h "12amw0q0956xfrk51ggdghliyxanssypvqm79pg93i1bg26v8ypn")))

(define-public crate-ibm437-0.2.1 (c (n "ibm437") (v "0.2.1") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 1)))) (h "0xq448hf9ni2n8c59hhx1qrhxlymwn46ryfs2vikz47vswdyj584")))

(define-public crate-ibm437-0.2.3 (c (n "ibm437") (v "0.2.3") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 1)))) (h "0sckk6nz7pfgr58b82wmp2cwwmaxq71b0kjwi7z8ls34yrx0q0m4")))

(define-public crate-ibm437-0.3.0 (c (n "ibm437") (v "0.3.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 1)))) (h "0jxpc5f9mh1ja4fnp09h8zp7xd0ni91jc0qfna0sv8ilycbki94v") (f (quote (("regular9x14") ("regular8x8") ("default" "regular8x8" "bold8x8" "regular9x14") ("bold8x8"))))))

(define-public crate-ibm437-0.3.1 (c (n "ibm437") (v "0.3.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 1)))) (h "0fnm4hz58szs1cbbdrd3ix47mpphcajyzymm1gy2ji14i5dwr66m") (f (quote (("regular9x14") ("regular8x8") ("default" "regular8x8" "bold8x8" "regular9x14") ("bold8x8"))))))

(define-public crate-ibm437-0.3.2 (c (n "ibm437") (v "0.3.2") (d (list (d (n "bitvec") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 1)))) (h "17iwlln0qqrg05gml5l62vb20lvxcp4gkpb14xk6jv1sgl318hxk") (f (quote (("regular9x14") ("regular8x8") ("default" "regular8x8" "bold8x8" "regular9x14") ("bold8x8"))))))

(define-public crate-ibm437-0.3.3 (c (n "ibm437") (v "0.3.3") (d (list (d (n "bitvec") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.6") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 1)))) (h "0and6rphxg9sc2qyb7s1rbyvr8m99w0glz4svbbzqi0884xqql61") (f (quote (("regular9x14") ("regular8x8") ("default" "regular8x8" "bold8x8" "regular9x14") ("bold8x8"))))))

