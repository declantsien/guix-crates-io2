(define-module (crates-io ib ex ibex_macros) #:use-module (crates-io))

(define-public crate-ibex_macros-0.1.0 (c (n "ibex_macros") (v "0.1.0") (d (list (d (n "ibex_core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qcqa3p6fj2ha4w9xxhlbnlfcp4a02v68znl8b0jyhv7i3qszrbs")))

