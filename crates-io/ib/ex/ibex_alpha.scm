(define-module (crates-io ib ex ibex_alpha) #:use-module (crates-io))

(define-public crate-ibex_alpha-0.1.0 (c (n "ibex_alpha") (v "0.1.0") (d (list (d (n "grass") (r "^0.13.1") (d #t) (k 0)) (d (n "ibex_core") (r "^0.1.0") (d #t) (k 0)) (d (n "ibex_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "1i4g1z107z7ipspwvx8ay7khadklbpah0gidp1cpww7vs7hcsygp")))

