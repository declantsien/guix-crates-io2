(define-module (crates-io ib ve ibverbs-sys) #:use-module (crates-io))

(define-public crate-ibverbs-sys-0.1.0 (c (n "ibverbs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0jg8j6f1x7wxx9033jwddrgnckgdlby8qxy5z80m3k9cm7kvpyl6") (l "ibverbs")))

(define-public crate-ibverbs-sys-0.1.1 (c (n "ibverbs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)))) (h "1nasawy130n0filzdbx01dg1s7wackkdx6rhkpw3dan7hxqdddkp") (l "ibverbs")))

(define-public crate-ibverbs-sys-0.1.2 (c (n "ibverbs-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "regex") (r "^1.6") (d #t) (t "cfg(any())") (k 0)))) (h "0798np0ygw78agmxgm1lbys97bm6zhbarzmkb89bc34ygrq7dwvh") (l "ibverbs")))

