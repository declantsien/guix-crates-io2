(define-module (crates-io rw -s rw-stream-sink) #:use-module (crates-io))

(define-public crate-rw-stream-sink-0.1.0 (c (n "rw-stream-sink") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "19a64cir3m2rm371al407ln6hv6bn2rvf952a3n6da2vsv1xg2hh")))

(define-public crate-rw-stream-sink-0.1.1 (c (n "rw-stream-sink") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1mi2dlwaq7daypxj1jh9sn4a5f69ziwpnic29gapffkww47s8j6m")))

(define-public crate-rw-stream-sink-0.1.2 (c (n "rw-stream-sink") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0y7yh5ssrc22nbsm4nlf7cl108w7w4wvxdrbbc0x6m84q9hvx71g")))

(define-public crate-rw-stream-sink-0.2.0 (c (n "rw-stream-sink") (v "0.2.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "029syz6j7qy5l9lwj612zpx3x5jkznd7g82mapp09h4xchklf7bn")))

(define-public crate-rw-stream-sink-0.2.1 (c (n "rw-stream-sink") (v "0.2.1") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0810glqcrs36r3k3a5skg40kygd9hn914fw3zxfmlvy4ajqgr9ad")))

(define-public crate-rw-stream-sink-0.3.0 (c (n "rw-stream-sink") (v "0.3.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "016wwxj9sfja9xyhhkyyyr90pjb7fyphbsimn62inwmv15g8ycr6")))

(define-public crate-rw-stream-sink-0.4.0 (c (n "rw-stream-sink") (v "0.4.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "18g9bxl1lb6har1f9i09zcdh10v32lzjig2vwjjkvwnjymph5jfq") (r "1.65.0")))

