(define-module (crates-io rw ar rwarchivefs) #:use-module (crates-io))

(define-public crate-rwarchivefs-0.1.0 (c (n "rwarchivefs") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fuser") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)))) (h "0nn1wc9sd5zgrfb2w64zg97ypicyg1y34ndd368ysl755ln78c3n")))

