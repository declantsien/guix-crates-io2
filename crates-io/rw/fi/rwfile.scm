(define-module (crates-io rw fi rwfile) #:use-module (crates-io))

(define-public crate-rwfile-0.1.0 (c (n "rwfile") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "031hc5ngwq4lwipqmm9xxvacjxmr7bd3gpxj591yzlz7bmi8rci3")))

