(define-module (crates-io rw ut rwutil) #:use-module (crates-io))

(define-public crate-rwutil-0.3.0 (c (n "rwutil") (v "0.3.0") (d (list (d (n "byteorder") (r "~0") (d #t) (k 0)))) (h "0bf3wdjl02x6i2gkq07irjkrbz7pv3861d7261kjp2inzn7m4xhs")))

(define-public crate-rwutil-0.3.1 (c (n "rwutil") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "19w834gp12k259h5vdqhb6v8s6mczqwg3w7pskv7nggyw38gn8mp")))

(define-public crate-rwutil-0.4.0 (c (n "rwutil") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "0z5kagrrsavfmv1gyb7521fnb8csd4imv419r5d26xp7yk78i8ba")))

(define-public crate-rwutil-1.0.0 (c (n "rwutil") (v "1.0.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "1cksabcywda1y0xagpapj8d0kxqr9zbhaafjsvfxxqz87x41dxn9")))

(define-public crate-rwutil-1.0.1 (c (n "rwutil") (v "1.0.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "1xdw3zvzbnhzd6hjmriv4r7a0nl9f7xc2cxlydkwi4vazx90v427")))

(define-public crate-rwutil-2.0.0 (c (n "rwutil") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1nqr67krpjk6ibcpbbnnp07094dsqv5pjvvl3lrngq6fqf616vmr")))

