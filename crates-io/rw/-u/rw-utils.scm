(define-module (crates-io rw -u rw-utils) #:use-module (crates-io))

(define-public crate-rw-utils-0.0.1 (c (n "rw-utils") (v "0.0.1") (d (list (d (n "encoding") (r "^0.2.33") (o #t) (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 2)) (d (n "leb128") (r "^0.2.5") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0hhqirvx72r0plxsmb8n0724xlanfznfrp4dbgfsycngxficjgla") (f (quote (("to_write") ("string_write" "encoding") ("string_read") ("num_write") ("num_read") ("leb128_write") ("leb128_read") ("from_read") ("all" "num_read" "num_write" "string_read" "string_write" "to_write" "from_read" "leb128_read" "leb128_write"))))))

