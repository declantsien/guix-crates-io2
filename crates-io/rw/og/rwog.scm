(define-module (crates-io rw og rwog) #:use-module (crates-io))

(define-public crate-rwog-0.1.0 (c (n "rwog") (v "0.1.0") (d (list (d (n "caps") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "groups") (r "^0.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "passwd") (r "^0.0.1") (d #t) (k 0)))) (h "0gmfz0vb64x24ki6nwh86g6lf0kd9i4wmlpb8g3g119b3r1h1qda")))

(define-public crate-rwog-0.1.1 (c (n "rwog") (v "0.1.1") (d (list (d (n "caps") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "groups") (r "^0.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "passwd") (r "^0.0.1") (d #t) (k 0)))) (h "0mnq245c8q8g8nngcbphyipsjcb6v3jjz3qvjwd22jjvq2127qh0")))

(define-public crate-rwog-0.2.1 (c (n "rwog") (v "0.2.1") (d (list (d (n "caps") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "users") (r "^0.6.1") (d #t) (k 0)))) (h "1k2asr4i2wq66s58hd424xn9jj22vx2kzagaidi0s72k2p8361za")))

(define-public crate-rwog-0.2.3 (c (n "rwog") (v "0.2.3") (d (list (d (n "caps") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "users") (r "^0.6.1") (d #t) (k 0)))) (h "0hr98rmnb3vjnnv60z8zkmwg9qg2azrvrcmshldbiq1n8qbqk7dk")))

