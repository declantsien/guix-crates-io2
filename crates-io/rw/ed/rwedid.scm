(define-module (crates-io rw ed rwedid) #:use-module (crates-io))

(define-public crate-rwedid-0.3.1 (c (n "rwedid") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)))) (h "1gg2mvxwf4g2jis9q5ibgdxw8cr4gsdgxzdipy46a8iir9fin85h")))

(define-public crate-rwedid-0.3.2 (c (n "rwedid") (v "0.3.2") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)))) (h "0vaxkap9lv03bqjha2jvvbgdrk4d7qqxyw2w8fgfl6b3nfj039vm")))

