(define-module (crates-io rw eb rweb-macros) #:use-module (crates-io))

(define-public crate-rweb-macros-0.1.0 (c (n "rweb-macros") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1z0yf01vvij5394zi46c3dcysd7r09900ngib0ya46ciip99b2bg") (y #t)))

(define-public crate-rweb-macros-0.2.0 (c (n "rweb-macros") (v "0.2.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05cab6vyl9f56whxagcmwk2ara163figqy1l44malz67jd2anqj6")))

(define-public crate-rweb-macros-0.3.0-alpha.0 (c (n "rweb-macros") (v "0.3.0-alpha.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i8pja8hihilqm4a2sq57wh77q88h36q947dqli9b79d7lyrxcjr")))

(define-public crate-rweb-macros-0.3.0-alpha.1 (c (n "rweb-macros") (v "0.3.0-alpha.1") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1dbryxfrp24qy3gxwis9smddaqln2xzwjymksa30xqy5f7817a4q") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.4.0 (c (n "rweb-macros") (v "0.4.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0cjh2dny2q9vdsp1y3br8952v3c3ki79samq6jw3ahp2fh82imm2") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.4.1 (c (n "rweb-macros") (v "0.4.1") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1yrhv24pajpl4pvzajin18mrmggs5w4g9jibp5w34qn0naq3dpjk") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.4.2 (c (n "rweb-macros") (v "0.4.2") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1wdlzygq1gyk941ijfqb15kgbk2ipg21mx6hjlhmq0kk1dbxgkx8") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.4.4 (c (n "rweb-macros") (v "0.4.4") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1d1h5kyxfnbjvi3qglk2wh1v6c3y1c1ln0lf7w2aqc6yi6igiq8h") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.4.5 (c (n "rweb-macros") (v "0.4.5") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0mgzyw9fd41v90hj3l0v3ky162k6rzkqk0czv7fqklixvda4ybq8") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.4.6 (c (n "rweb-macros") (v "0.4.6") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0m2m9cd6g3q3sai2zvmxb2azqxr8k5qm91qwdimm21dhs9shvfkx") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.5.0 (c (n "rweb-macros") (v "0.5.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "11b5j1xxv17jksl61c3101ir98xy4b99g8gp5q8gkrvvz7ax5zms") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.6.0 (c (n "rweb-macros") (v "0.6.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1nacgk174zj5kbnxxd3nwphf3kpcsaszczw8h0dsngyb6fjfhwam") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.7.0 (c (n "rweb-macros") (v "0.7.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0ac4jcmp3ikkixvak1jsakr8mi5q5g2z3a7zbp6gwa24jlqn5jhx") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.8.0 (c (n "rweb-macros") (v "0.8.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0bfrp9zclqxx255hwj9ksp5nclbgldvzqn9knh39qkd26kaxnpy2") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.8.1 (c (n "rweb-macros") (v "0.8.1") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "17vpp3ispnfmb8bk8ab0gmash89qa9w3yzpfb4ji1p1619agcpqj") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.9.0 (c (n "rweb-macros") (v "0.9.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0015zq67yar7d8xlxkgksmpyyw05m7p4ny8nz356560jrfrd97dm") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.10.0 (c (n "rweb-macros") (v "0.10.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0dnnjwyah1x6xf9mmqgz9y6fgp9a25hin7kp9gn6105nizf7ld1r") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.10.1 (c (n "rweb-macros") (v "0.10.1") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0935h3lpad87g8znwr2n5g4sn70pb68jm778dvy1yxmxsyf5clqn") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.11.0 (c (n "rweb-macros") (v "0.11.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1a18ssn09j6nk1amp47ly390wibifrqm5xk1gqfznfg94av35zj5") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.11.1 (c (n "rweb-macros") (v "0.11.1") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "04jjsv9aq7l42mqq05chv97nim2zkxizsnaslp1n3f86s0yqq0s5") (f (quote (("openapi"))))))

(define-public crate-rweb-macros-0.11.2 (c (n "rweb-macros") (v "0.11.2") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1pvi25riji7d4azs3v6i4lm9jfgynmzvmjqaprk3m1fwj6xwpc0s") (f (quote (("openapi") ("boxed"))))))

(define-public crate-rweb-macros-0.11.3 (c (n "rweb-macros") (v "0.11.3") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "03yj0sfcikryxri8b7qsq73naj0frmwsasar5ayh1cbgddnd84kj") (f (quote (("openapi") ("boxed"))))))

(define-public crate-rweb-macros-0.12.0 (c (n "rweb-macros") (v "0.12.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0s267g994za7slp54gxmx0xw7qxzjy8brg9cza3ns918dmn9h2g2") (f (quote (("openapi") ("boxed"))))))

(define-public crate-rweb-macros-0.13.0 (c (n "rweb-macros") (v "0.13.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "09vxg42ivy5paah167fwdljzv4s33ccfvrb4hd4xw0rhpibg4nka") (f (quote (("openapi") ("boxed"))))))

(define-public crate-rweb-macros-0.14.0 (c (n "rweb-macros") (v "0.14.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rweb-openapi") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0b2csyy7fc84ka57099d0xwqqhxhymflmzi61nk03nra1l63jh43") (f (quote (("openapi") ("boxed"))))))

