(define-module (crates-io rw in rwini) #:use-module (crates-io))

(define-public crate-rwini-0.1.0 (c (n "rwini") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xc65whwb1v5k0gx590mnl106wm0k20s1jd62n6rxfa1r93az6by")))

(define-public crate-rwini-0.1.1 (c (n "rwini") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b6n8ys2r5hvxyyd3lh9xxzh8m0wf86cfym7a76lvnqsrnadg8q6")))

