(define-module (crates-io rw -e rw-exact-ext) #:use-module (crates-io))

(define-public crate-rw-exact-ext-0.1.0 (c (n "rw-exact-ext") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.17") (o #t) (d #t) (k 0)))) (h "1azk6146amh407jc6wj4mmdsgfsxxr7lqn5v1qas04var29jwj4x") (y #t)))

(define-public crate-rw-exact-ext-0.1.1 (c (n "rw-exact-ext") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.17") (o #t) (d #t) (k 0)))) (h "0dfkvm19mw28zk64198y3lw2b34fyylgn17nq9fqy2pb2801px14") (y #t)))

(define-public crate-rw-exact-ext-0.1.2 (c (n "rw-exact-ext") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.17") (o #t) (d #t) (k 0)))) (h "1v148a9xlmp0f2a36fd4gjqakkch1hj1292yibql3wc4x04xz8fy") (y #t)))

(define-public crate-rw-exact-ext-0.1.3 (c (n "rw-exact-ext") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.17") (o #t) (d #t) (k 0)))) (h "12vy5zfjpmia1z3c6m8nmyhgmqhgwn3x545dc0llscfqmxj2vlqy") (y #t)))

(define-public crate-rw-exact-ext-0.1.4 (c (n "rw-exact-ext") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.17") (o #t) (d #t) (k 0)))) (h "0l937qs9fcj7ri3g6k1b9jjwq5004s399932z3g8f8jv5979nc6m")))

(define-public crate-rw-exact-ext-0.1.5 (c (n "rw-exact-ext") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.17") (o #t) (d #t) (k 0)))) (h "11hcnbr7ifr98bhhajd7mnvkrqcxvk66kzwvrx8p5lhs9gbmicqr") (y #t)))

(define-public crate-rw-exact-ext-0.1.6 (c (n "rw-exact-ext") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.2.17") (o #t) (d #t) (k 0)))) (h "19spxqav2my169g7pn634y8inpvkp6gvhf19h221bn7mlrs1jzr9")))

(define-public crate-rw-exact-ext-0.1.7 (c (n "rw-exact-ext") (v "0.1.7") (d (list (d (n "num-traits") (r "^0.2.17") (o #t) (d #t) (k 0)))) (h "0dikhc52sbbbi0i64h7kjd7gdz643nq2iay6fbmnrd5w132l7jyl")))

