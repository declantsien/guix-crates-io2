(define-module (crates-io rw as rwasm) #:use-module (crates-io))

(define-public crate-rwasm-2020.1.0 (c (n "rwasm") (v "2020.1.0") (d (list (d (n "backtrace") (r "^0.3.55") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1kldn4yv07lwls2lw63wb0nfkfj2lnh1zjh0ra0jhjsr2jba9l0q") (y #t)))

(define-public crate-rwasm-2020.1.1 (c (n "rwasm") (v "2020.1.1") (d (list (d (n "backtrace") (r "^0.3.55") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "14m75a38xgn7a8kxl5mgm4xh44njkvkwcfjnwd0l2ajirbav27nr") (y #t)))

(define-public crate-rwasm-0.0.1 (c (n "rwasm") (v "0.0.1") (d (list (d (n "backtrace") (r "^0.3.55") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rwasm_macro") (r "^0.1.0") (d #t) (k 0)))) (h "001ycg9ywilgzml7nl682av1cmikvl3km3sgk4q0zwiicjnmdiq9")))

