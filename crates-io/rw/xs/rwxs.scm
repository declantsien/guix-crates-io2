(define-module (crates-io rw xs rwxs) #:use-module (crates-io))

(define-public crate-rwxs-0.1.0 (c (n "rwxs") (v "0.1.0") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)))) (h "19xfr5al0lk0c0yhppjirbn401bqrbwd5zzra48kcvfc8iw8ai8z") (y #t)))

(define-public crate-rwxs-0.1.1 (c (n "rwxs") (v "0.1.1") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)))) (h "1cwjclph6075ff5z51vdm4sjrdn752wrkm2k4cfsfqc1iqri1yk3") (y #t)))

