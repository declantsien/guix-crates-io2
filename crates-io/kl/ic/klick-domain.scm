(define-module (crates-io kl ic klick-domain) #:use-module (crates-io))

(define-public crate-klick-domain-0.4.0 (c (n "klick-domain") (v "0.4.0") (d (list (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "mailparse") (r "^0.14.1") (d #t) (k 0)) (d (n "pwhash") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1pq10v6wvkqnij78rjsf8g9fq8484aibvvshj4bkgq58zx9pmv9p") (r "1.76")))

(define-public crate-klick-domain-0.4.11 (c (n "klick-domain") (v "0.4.11") (d (list (d (n "bs58") (r "^0.5.1") (d #t) (k 0)) (d (n "mailparse") (r "^0.14.1") (d #t) (k 0)) (d (n "pwhash") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "167rfcpa1yix58xw1an4dcd9rfpcycaqcsrmiwzgcn655sxbzs3g") (r "1.76")))

