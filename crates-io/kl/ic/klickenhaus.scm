(define-module (crates-io kl ic klickenhaus) #:use-module (crates-io))

(define-public crate-klickenhaus-0.1.0 (c (n "klickenhaus") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "0483xxfnnkvgsmds427ypwggj69yp3cv463m50rria250mjjs7p1")))

(define-public crate-klickenhaus-0.1.1 (c (n "klickenhaus") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "11vzhiga8i5zrfi8if7i54cjswmq6zl2aivfsbc66fv99d6v25k7")))

(define-public crate-klickenhaus-0.1.2 (c (n "klickenhaus") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "08qfzwp5c0bmpwkfqjkpkmj42ypf68852s57sny62xsxdq9mr7x3")))

(define-public crate-klickenhaus-0.1.3 (c (n "klickenhaus") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "0i6dkb8mks6h9byv9v1y9hhgpzx8jjblmmi6jlsmwkszq8asyjxx")))

