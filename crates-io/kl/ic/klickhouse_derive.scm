(define-module (crates-io kl ic klickhouse_derive) #:use-module (crates-io))

(define-public crate-klickhouse_derive-0.1.0 (c (n "klickhouse_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n6s36spmmzjw9iarlzdmf6sbn8n0x9avnnvccwfhpi7q8br7k27")))

(define-public crate-klickhouse_derive-0.2.0 (c (n "klickhouse_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10nz3blh7sljjrp963zini3x2r61q284wydh6rx11zvdk47yy0gy")))

(define-public crate-klickhouse_derive-0.2.1 (c (n "klickhouse_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s1lkr2k994krgxg2h1v92yqg3m6nzkzrrzs3a2y5a7r7jyc6rnr")))

(define-public crate-klickhouse_derive-0.3.0 (c (n "klickhouse_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mmjb2g06xrvx6dvj0215f75kh4ifm2vc0bry0fhflc601y89r41")))

(define-public crate-klickhouse_derive-0.4.0 (c (n "klickhouse_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pgkli4ykxwdigv8g3qac1pg4azm5drr0clqasg7k03qfk8fpjpr")))

(define-public crate-klickhouse_derive-0.5.0 (c (n "klickhouse_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0idx8bi1nljyv41q8i8dwgh9xr7m5siaqw0y65qwsy6wlx85lq2f")))

(define-public crate-klickhouse_derive-0.6.2 (c (n "klickhouse_derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x5rysxzpcpnry6dbfcs8z6rcbqzy0vrxpx2i6qkkbncjbnjf7my")))

(define-public crate-klickhouse_derive-0.7.0 (c (n "klickhouse_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0if6bs16v7ir7s384ncvlgi33avby9krhgpp83v1jq023if448az")))

(define-public crate-klickhouse_derive-0.8.0 (c (n "klickhouse_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lmcbrgyp8bav1bq2zv1c6fc3fd3d2livps9d1jc69hhk7w4rjdh")))

(define-public crate-klickhouse_derive-0.8.1 (c (n "klickhouse_derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14911w09dysk13agpvg85l6jjbqxqwy14cgqhhlrld7x6zm6fxv1")))

(define-public crate-klickhouse_derive-0.8.2 (c (n "klickhouse_derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qnlnq9796vjwmm2c5fw8565052xc2b1g946bligqwjqzpj3rr13")))

(define-public crate-klickhouse_derive-0.8.5 (c (n "klickhouse_derive") (v "0.8.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lcg8q2k061g8kfvjr610pfwj1rbmjwrchi25yvxyyf0p3j0qac8")))

(define-public crate-klickhouse_derive-0.9.0 (c (n "klickhouse_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jhj14sf7kr7ax7xps9grccyjlg8cnww8irf157dj0hb950c3lmv")))

(define-public crate-klickhouse_derive-0.10.0 (c (n "klickhouse_derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lyzq4yymdiyb4flym6fdyl4hmgihiz1wv88slbpmsgcvmbkmvf1")))

(define-public crate-klickhouse_derive-0.11.0 (c (n "klickhouse_derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1icnvg25lzqd2jbq2i9x57blqw0p36l36qfcf336j4amcbxkal6w")))

(define-public crate-klickhouse_derive-0.11.1 (c (n "klickhouse_derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xz59d8dfmn483lirj9ffddii728ricjk1bnbxh8yhgyjq7p2h0n")))

(define-public crate-klickhouse_derive-0.12.0 (c (n "klickhouse_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "076x9m27qq3hbdq4f36x0rfb9h0236w7smzlg6lasjskld0sdily") (r "1.75.0")))

(define-public crate-klickhouse_derive-0.13.0 (c (n "klickhouse_derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1lckbvjyiaizvminjvl3xlgg2ldp1pqqc37fccfbwy441qskdk8j") (r "1.75.0")))

