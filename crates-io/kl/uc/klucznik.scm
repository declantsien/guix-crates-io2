(define-module (crates-io kl uc klucznik) #:use-module (crates-io))

(define-public crate-klucznik-0.1.0 (c (n "klucznik") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "08kb80mckb2m2hw2jwns7235bh3wydy40ws86bhp8fzw9jaa4qxi")))

(define-public crate-klucznik-0.1.1 (c (n "klucznik") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha256") (r "^1.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0lis1paqvs9wcd0x827vj3a0cvj2qgr4bz7bn3k0infigvqfj6ny")))

(define-public crate-klucznik-0.1.2 (c (n "klucznik") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha256") (r "^1.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1jvi1hbl18yrqh5s9x9i0qjy5d99i46qmvfxxi6i1ndr9jsaw4hr")))

