(define-module (crates-io kl ei kleio) #:use-module (crates-io))

(define-public crate-kleio-0.0.1 (c (n "kleio") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "kleio-macros") (r "^0.0.1") (k 0)) (d (n "sea-orm") (r "^0.10.0") (d #t) (k 0)))) (h "0br2935qd637r9wq1qm28id7cy454vixmxkc3g8f1j4k3jg004q8") (r "1.65")))

