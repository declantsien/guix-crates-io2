(define-module (crates-io kl ei kleio-macros) #:use-module (crates-io))

(define-public crate-kleio-macros-0.0.1 (c (n "kleio-macros") (v "0.0.1") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sea-orm") (r "^0.7.0") (f (quote ("macros" "debug-print" "runtime-tokio-native-tls" "sqlx-postgres"))) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "09wkry6f3kdn5jkrb6cq31xizv6qll06q1mqsqhfp4mn5qqfj14k")))

