(define-module (crates-io kl ei klein-sys) #:use-module (crates-io))

(define-public crate-klein-sys-0.1.0 (c (n "klein-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)))) (h "11157ncihqvywldf55ddhbjajy4di0f9rn8r1f9a5pb7sf12zmzz")))

(define-public crate-klein-sys-0.1.1 (c (n "klein-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)))) (h "0mh66s9ars3yrm7smc23lhc5mckr4zi0z757qdhxzan4s6aqwhn1")))

