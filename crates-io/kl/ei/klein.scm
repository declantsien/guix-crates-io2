(define-module (crates-io kl ei klein) #:use-module (crates-io))

(define-public crate-klein-0.1.0 (c (n "klein") (v "0.1.0") (d (list (d (n "klein-sys") (r "^0.1.0") (d #t) (k 0)))) (h "19x0hjqq7cya8xcxd54pp0a74y564h5mi8sryp1qnw22hsrmbc54")))

(define-public crate-klein-0.1.1 (c (n "klein") (v "0.1.1") (d (list (d (n "klein-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1sx31p8i2jyv1ybf80fs6x18kdxmjc1kn4qhw8kqrdfkg3jr22w8")))

