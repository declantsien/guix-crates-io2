(define-module (crates-io kl ee klee-rs) #:use-module (crates-io))

(define-public crate-klee-rs-0.1.0 (c (n "klee-rs") (v "0.1.0") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1wl1hllrc4lh29f8pcm8kszijnmcwximvhk3nlg02c5019kbfdf8")))

(define-public crate-klee-rs-0.1.1 (c (n "klee-rs") (v "0.1.1") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "14vlzcgg852b7sra1vcqlm8gfvda994r66r72a3nk09y038h17i3")))

