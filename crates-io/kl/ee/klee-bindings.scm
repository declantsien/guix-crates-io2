(define-module (crates-io kl ee klee-bindings) #:use-module (crates-io))

(define-public crate-klee-bindings-0.1.0 (c (n "klee-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1m1g3k4nmri98dk5s1g7cpqr0y6rrwn094qismwcjifgplqq45w2")))

