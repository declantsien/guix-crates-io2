(define-module (crates-io kl ub klub_stores) #:use-module (crates-io))

(define-public crate-klub_stores-0.0.1 (c (n "klub_stores") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "neo4rs") (r "^0.5.8") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "0w66q5gm1zfrq75bq3jsxjvz86kf0dipk7kn0ayg25451sjdpynm")))

(define-public crate-klub_stores-0.0.2 (c (n "klub_stores") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "neo4rs") (r "^0.5.8") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1za3cv12l3nhm489jxndm9ikjixqc6sfl2wj333cd839j6gs3bkk")))

