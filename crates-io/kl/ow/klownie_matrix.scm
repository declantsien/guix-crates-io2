(define-module (crates-io kl ow klownie_matrix) #:use-module (crates-io))

(define-public crate-klownie_matrix-0.1.0 (c (n "klownie_matrix") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1l6ahspwvdxn3kn5v5b7a3v0n34i9gi1lc6v60cmjdlndmm23h03") (y #t)))

(define-public crate-klownie_matrix-0.1.1 (c (n "klownie_matrix") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1b5rzqimwq0haa9syy9si0i8la7jrbzxcmmbrka1xprvb7vq9nv1") (y #t)))

