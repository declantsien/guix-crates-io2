(define-module (crates-io kl ow klownie_matrice) #:use-module (crates-io))

(define-public crate-klownie_matrice-0.1.2 (c (n "klownie_matrice") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "07vd9vw0lfw9r1wdkbb9haxbkmm0g1zy61yj18k1zjrrm8w73gdh")))

(define-public crate-klownie_matrice-0.1.3 (c (n "klownie_matrice") (v "0.1.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0zazz67fy3f1m76zih9mp31hxyk8w27klhhyzgv2f3rbqbcr912n")))

