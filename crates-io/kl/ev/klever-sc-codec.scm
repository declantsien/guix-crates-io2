(define-module (crates-io kl ev klever-sc-codec) #:use-module (crates-io))

(define-public crate-klever-sc-codec-0.18.1 (c (n "klever-sc-codec") (v "0.18.1") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "klever-sc-codec-derive") (r "=0.18.1") (o #t) (d #t) (k 0)) (d (n "klever-sc-codec-derive") (r "=0.18.1") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "14471kfiarzcr7xjkc2rpvawjalxkjxdfgdk9lv62chrfix2vxy8") (f (quote (("derive" "klever-sc-codec-derive") ("alloc"))))))

