(define-module (crates-io kl ev klever-sc-modules) #:use-module (crates-io))

(define-public crate-klever-sc-modules-0.43.3 (c (n "klever-sc-modules") (v "0.43.3") (d (list (d (n "klever-sc") (r "^0.43.3") (d #t) (k 0)))) (h "1r8lg2fs5jhynq59iz35cdk3imlbczqnp4924a72nk6n95iwczdy") (f (quote (("alloc" "klever-sc/alloc"))))))

