(define-module (crates-io kl ev klever-sc-derive) #:use-module (crates-io))

(define-public crate-klever-sc-derive-0.43.3 (c (n "klever-sc-derive") (v "0.43.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "048dj3g7kdgacv08nn536lsgyhs9d560p6mawc7l7ddj9ca8yzdr") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

