(define-module (crates-io kl ev klever-sc-codec-derive) #:use-module (crates-io))

(define-public crate-klever-sc-codec-derive-0.18.1 (c (n "klever-sc-codec-derive") (v "0.18.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wjf3645mjkmiaf361as8m688sjc3wc2ydiq1q17slff2n0nhzfn") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

