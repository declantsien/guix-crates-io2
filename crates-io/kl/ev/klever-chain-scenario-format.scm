(define-module (crates-io kl ev klever-chain-scenario-format) #:use-module (crates-io))

(define-public crate-klever-chain-scenario-format-0.20.0 (c (n "klever-chain-scenario-format") (v "0.20.0") (d (list (d (n "bech32") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "1hz61j9lwqli8890r8mdpzcip3jf1zp5qm90g9wafxz7d0rcwidx")))

