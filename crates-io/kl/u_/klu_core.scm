(define-module (crates-io kl u_ klu_core) #:use-module (crates-io))

(define-public crate-klu_core-0.1.0 (c (n "klu_core") (v "0.1.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 2)))) (h "1bzf10ld6qz8b8aq6v383j6d56fgi7ya0kpgyknhbwbv68s63sja")))

(define-public crate-klu_core-0.2.0 (c (n "klu_core") (v "0.2.0") (h "0zc7imcim3cg85il2ybbkfsvp7vdpnkp7wnb10arya310fcxqs29") (f (quote (("virtual_fs") ("all" "virtual_fs"))))))

(define-public crate-klu_core-0.2.1 (c (n "klu_core") (v "0.2.1") (h "1jvpbm9f99yjsvwflj07r1kigvpmwih4fxg40x76z6d9gmz86j6b") (f (quote (("virtual_fs") ("all" "virtual_fs"))))))

