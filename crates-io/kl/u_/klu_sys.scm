(define-module (crates-io kl u_ klu_sys) #:use-module (crates-io))

(define-public crate-klu_sys-0.1.0 (c (n "klu_sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1crw2r1xjv0cj7g5dplgv0wrbisnrjimnvjd8yk75vh17fhy7fgk") (f (quote (("dynamic") ("default")))) (l "klu")))

(define-public crate-klu_sys-0.2.0 (c (n "klu_sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12d6ygs2v7ghrksanc0ydy62pplbnwfydxaq62nzjljw56f74zxw") (f (quote (("dynamic") ("default")))) (l "klu")))

