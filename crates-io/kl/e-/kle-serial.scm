(define-module (crates-io kl e- kle-serial) #:use-module (crates-io))

(define-public crate-kle-serial-0.1.0 (c (n "kle-serial") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "csscolorparser") (r "^0.6") (f (quote ("named-colors"))) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "033370a1bhx2j829aklh9xhp9w0vqpc757ffayay6lnd0sz8rpfb") (r "1.63")))

(define-public crate-kle-serial-0.1.1 (c (n "kle-serial") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "csscolorparser") (r "^0.6") (f (quote ("named-colors"))) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1l09w74rix6npbvzpa352xd4q5q1a9f3q1mw97j0w8z32bqxia58") (r "1.63")))

(define-public crate-kle-serial-0.2.0 (c (n "kle-serial") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "csscolorparser") (r "^0.6") (f (quote ("named-colors"))) (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0m5i73i18dd8ydhnl9abfxzn8nx4fjxhji0490sz3xndmm2hhz4w") (r "1.63")))

(define-public crate-kle-serial-0.2.1 (c (n "kle-serial") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "csscolorparser") (r "^0.6") (f (quote ("named-colors"))) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06hbp653x734qvz4h6ys8hsk9p7911n378x2sz8bwxxgr64bg5aa") (r "1.63")))

(define-public crate-kle-serial-0.2.2 (c (n "kle-serial") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "csscolorparser") (r "^0.6") (f (quote ("named-colors"))) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1blyf1k82h9gs07pqycb17f9aydj54pklfg6s2kmdhm2lwsj5fzz") (r "1.63")))

(define-public crate-kle-serial-0.3.0 (c (n "kle-serial") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "csscolorparser") (r "^0.6") (f (quote ("named-colors"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0blk0yh5vc5zbxqz62qp6jpjvy1ha75gr351qm2kildydi6a9pf5") (r "1.63")))

