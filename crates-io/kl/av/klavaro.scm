(define-module (crates-io kl av klavaro) #:use-module (crates-io))

(define-public crate-klavaro-0.1.0 (c (n "klavaro") (v "0.1.0") (d (list (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)))) (h "03ibmdiwjyxvwm8jh597ivs19zi64qcv6v84zks2vp0znkrqadkp")))

(define-public crate-klavaro-1.0.0 (c (n "klavaro") (v "1.0.0") (d (list (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)))) (h "0794nab24gnqvxz7iwawdk5dqjdafnx39i2vpxqnkpbvbsa13g6h")))

(define-public crate-klavaro-1.0.1 (c (n "klavaro") (v "1.0.1") (d (list (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "087xx472kav4ycisq6vgq0q2qw35388fnwhpnq4wv2d8hhpsnp92")))

