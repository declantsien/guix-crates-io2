(define-module (crates-io kl av klavier-jack) #:use-module (crates-io))

(define-public crate-klavier-jack-0.1.1-beta.1 (c (n "klavier-jack") (v "0.1.1-beta.1") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jack") (r "^0") (d #t) (k 0)) (d (n "klavier-core") (r "^0.1.1-beta.9") (d #t) (k 0)) (d (n "klavier-helper") (r "^0.1.3-beta.5") (d #t) (k 0)))) (h "0x4c2a9p2wg43a0q0md9b6ydkk7l0zlx8qv0b7q8wqv4g1i49bnb") (y #t)))

(define-public crate-klavier-jack-0.1.2-beta.1 (c (n "klavier-jack") (v "0.1.2-beta.1") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jack") (r "^0") (d #t) (k 0)) (d (n "klavier-core") (r "^0.1.1-beta.9") (d #t) (k 0)) (d (n "klavier-helper") (r "^0.1.3-beta.5") (d #t) (k 0)))) (h "19ms3m4z2d9rbpzzbmig2carydivqq7p1hscmg38kc5972ly8zcd")))

(define-public crate-klavier-jack-0.1.3-beta.1 (c (n "klavier-jack") (v "0.1.3-beta.1") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jack") (r "^0") (d #t) (k 0)) (d (n "klavier-core") (r "^0.1.1-beta.9") (d #t) (k 0)) (d (n "klavier-helper") (r "^0.1.3-beta.5") (d #t) (k 0)))) (h "1f5ivzk839xvl22c4ycy20a60ii2m7cpvarfxr09cg0wykg60v8c")))

(define-public crate-klavier-jack-0.1.3-beta.2 (c (n "klavier-jack") (v "0.1.3-beta.2") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jack") (r "^0") (d #t) (k 0)) (d (n "klavier-core") (r "^0.1.1-beta.10") (d #t) (k 0)) (d (n "klavier-helper") (r "^0.1.3-beta.5") (d #t) (k 0)))) (h "0qyxq6b9dpkqgjs6k7fdv5wla9v1dp75xq7rn8kaqwdizr6n2xb0")))

(define-public crate-klavier-jack-0.1.3-beta.3 (c (n "klavier-jack") (v "0.1.3-beta.3") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jack") (r "^0") (d #t) (k 0)) (d (n "klavier-core") (r "^0.1.1-beta.10") (d #t) (k 0)) (d (n "klavier-helper") (r "^0.1.3-beta.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0") (d #t) (k 0)))) (h "125y4a0qv58m3syiyqscf551a4dnaxx3s659dpz3j30r2k259hr6")))

(define-public crate-klavier-jack-0.1.3-beta.4 (c (n "klavier-jack") (v "0.1.3-beta.4") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jack") (r "^0") (d #t) (k 0)) (d (n "klavier-core") (r "^0.1.1-beta.11") (d #t) (k 0)) (d (n "klavier-helper") (r "^0.1.3-beta.7") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0") (d #t) (k 0)))) (h "16k89kdfvl5kvnfrrjcaa2z0y5zvihqyvnxhafrhk3x6x07zkm46")))

(define-public crate-klavier-jack-0.1.3-beta.5 (c (n "klavier-jack") (v "0.1.3-beta.5") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "jack") (r "^0") (d #t) (k 0)) (d (n "klavier-core") (r "^0.1.1-beta.11") (d #t) (k 0)) (d (n "klavier-helper") (r "^0.1.3-beta.7") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0") (d #t) (k 0)))) (h "1gk5jcrab13qhljxsrjqnp1j6g5s2pinfzba56zf6324xn2z75cd")))

