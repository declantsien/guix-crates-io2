(define-module (crates-io kl av klave) #:use-module (crates-io))

(define-public crate-klave-0.0.1 (c (n "klave") (v "0.0.1") (d (list (d (n "secretarium") (r "^0.0.1") (d #t) (k 0)))) (h "1b19qhj8lnlx9p4wg70vx4bihh6yz42hr9crpyj3iz3k4855rwjj")))

(define-public crate-klave-0.0.2 (c (n "klave") (v "0.0.2") (d (list (d (n "secretarium") (r "^0.0.2") (d #t) (k 0)))) (h "0ph44wvn26jzsaxz6hp6phpz5sba8dbnfm3dvvhssfkbnnb2867k")))

(define-public crate-klave-0.0.3 (c (n "klave") (v "0.0.3") (d (list (d (n "secretarium") (r "^0.0.2") (d #t) (k 0)))) (h "1bidlvyzdcidb4cf0gkqzgdyzjp49bk6wpxidsz7iffbgvyayvrd")))

