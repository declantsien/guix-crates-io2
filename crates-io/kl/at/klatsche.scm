(define-module (crates-io kl at klatsche) #:use-module (crates-io))

(define-public crate-klatsche-1.0.0 (c (n "klatsche") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "00vlc41ysswmjdpagbavmy39wfx55nyhpqyg4nvhb3cnsmbizy0b")))

(define-public crate-klatsche-1.0.1 (c (n "klatsche") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "03pq99xhkj5xk3hiiz5pvp7y8ap45jbvlr1finm983j3pn129a9q")))

