(define-module (crates-io kl vm klvm-derive) #:use-module (crates-io))

(define-public crate-klvm-derive-0.2.12 (c (n "klvm-derive") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1w1608d89w0h4gdv5ckq9j0zh52zhy74lxqryl2n84p1bfabmcv1")))

(define-public crate-klvm-derive-0.2.14 (c (n "klvm-derive") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1cwnwy7xjkbmw2fcdcxzy5xyzvgap03n1liwcvlh9plamgfg1lhw")))

(define-public crate-klvm-derive-0.5.2 (c (n "klvm-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1wprnxw3j3hx630vc5v9z0v4lc9i4rq0v716m89vm2haj6dhzwgr")))

(define-public crate-klvm-derive-0.6.0 (c (n "klvm-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1jv69m3nm8a83yh9bkk2mbyndxvnhsw5cir2w6lb6vpmalh3aw7f")))

