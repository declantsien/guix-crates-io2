(define-module (crates-io kl vm klvm-utils) #:use-module (crates-io))

(define-public crate-klvm-utils-0.2.7 (c (n "klvm-utils") (v "0.2.7") (d (list (d (n "klvmr") (r "^0.2.8") (d #t) (k 0)))) (h "0z6sy7g5c0x1179carvrcx4ldn21fmckp5ir60yj25sw3qjwgjj6")))

(define-public crate-klvm-utils-0.2.12 (c (n "klvm-utils") (v "0.2.12") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "klvm-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "klvmr") (r "^0.3.0") (d #t) (k 0)))) (h "0sbwlnjaa0haw5kln6c2l0jzyfkadb5c2x7f5bzyy7n1xvj27fr3")))

(define-public crate-klvm-utils-0.2.14 (c (n "klvm-utils") (v "0.2.14") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "klvm-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "klvmr") (r "^0.3.0") (d #t) (k 0)))) (h "0rb7bmhs2s1jzhysvl1wfibl6byk7141n2mm2badwnklf51vk18d")))

(define-public crate-klvm-utils-0.3.0 (c (n "klvm-utils") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "klvm-traits") (r "=0.2.14") (d #t) (k 0)) (d (n "klvmr") (r "^0.3.0") (d #t) (k 0)))) (h "0fyqp5qrmfgmspqwf05md4i933rl4vjcmp392kk7g306fsc9v91f")))

(define-public crate-klvm-utils-0.3.3 (c (n "klvm-utils") (v "0.3.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "klvm-traits") (r "^0.3.3") (d #t) (k 0)) (d (n "klvmr") (r "^0.3.2") (d #t) (k 0)))) (h "070a134n79c4dyh138gcsrqfbhkcx0mg99b86sh54vm0kqs4lqp5")))

(define-public crate-klvm-utils-0.4.0 (c (n "klvm-utils") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "klvm-traits") (r "^0.3.3") (d #t) (k 0)) (d (n "klvmr") (r "^0.4.0") (d #t) (k 0)))) (h "1pmv6ffxlmpcsjwmsx3g3bcwcwyplvd70yqgm6y7pb87yyr9g9ff")))

(define-public crate-klvm-utils-0.5.1 (c (n "klvm-utils") (v "0.5.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "klvm-traits") (r "^0.5.1") (d #t) (k 0)) (d (n "klvmr") (r "^0.5.0") (d #t) (k 0)))) (h "1pjz3vvkbziqrsn58l4vb8wd7mzd9m7jiwawdlr6pr2bm50nrfp7")))

(define-public crate-klvm-utils-0.6.0 (c (n "klvm-utils") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "klvm-traits") (r "^0.6.0") (d #t) (k 0)) (d (n "klvmr") (r "^0.6.1") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "127yhcxyf15m4azsskayw8rigryxpqbpjpc9bpkgjq50n6pvaafx")))

(define-public crate-klvm-utils-0.7.0 (c (n "klvm-utils") (v "0.7.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "klvm-traits") (r "^0.7.0") (d #t) (k 0)) (d (n "klvmr") (r "^0.6.1") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0g2wcsn4mp5av6a4ihzga9zqh77qpn5zzsfjjb0f5ayvr15aalz7")))

(define-public crate-klvm-utils-0.8.0 (c (n "klvm-utils") (v "0.8.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "klvm-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "klvm-traits") (r "^0.8.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "klvmr") (r "^0.7.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "1dkhz2573zqv547y6kn3z8x270b5lc6y0pvrwb7xvg0956c4k4yy")))

