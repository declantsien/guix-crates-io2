(define-module (crates-io kl og klogger) #:use-module (crates-io))

(define-public crate-klogger-0.0.4 (c (n "klogger") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.21") (d #t) (k 0)))) (h "054amr70r34p8z7pslpc4g8sn72mba55lv6qjf95k82qcsqbs0ja") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.5 (c (n "klogger") (v "0.0.5") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.21") (d #t) (k 0)))) (h "0ps0cfciykaj3g9lpgccwjz40m1djvnxkk828l6xkjkilvqwq9wx") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.6 (c (n "klogger") (v "0.0.6") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.32") (d #t) (k 0)))) (h "0khxh743lfmvbd7p1l3d4h4g39m97njpm0b9qjgkpgiya29v9d4d") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.7 (c (n "klogger") (v "0.0.7") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.37") (d #t) (k 0)))) (h "1xpsr6icinkhsiirn9a6rf37ka9dc34vr7pcsn2lwc8ia06dac7b") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.8 (c (n "klogger") (v "0.0.8") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.37") (d #t) (k 0)))) (h "035blsc9nmkqnc4iw2024wj2bl1blg6dlz2k404s0y9rxvq0xb14") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.9 (c (n "klogger") (v "0.0.9") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.45") (d #t) (k 0)))) (h "08q9zq27ap80pakfv02x7m4zpr0252m8yz915n4rhlrf91nzfp1r") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.10 (c (n "klogger") (v "0.0.10") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.47") (d #t) (k 0)))) (h "0z17pxnyanjhfkcdflnq87v9snx9sy32mdfdxdfxg0swgqd8mz7y") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.11 (c (n "klogger") (v "0.0.11") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.47") (d #t) (k 0)))) (h "0rkqrdam0mvg61chj87yd08ybws6b599b27xk1bffc1fa7b7wbnx") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.12 (c (n "klogger") (v "0.0.12") (d (list (d (n "heapless") (r "^0.7.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.50") (d #t) (k 0)))) (h "143fdf6psrnb8zjrwj77xkfw977p66f77y8j4jsjdv9ck4pvqm2q") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.14 (c (n "klogger") (v "0.0.14") (d (list (d (n "heapless") (r "^0.7.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.51") (f (quote ("unstable"))) (d #t) (k 0)))) (h "0hal1i2gvs33jp3ndqccs7db7a260qzpxj2yykkqadk34xb17z5a") (f (quote (("use_ioports"))))))

(define-public crate-klogger-0.0.16 (c (n "klogger") (v "0.0.16") (d (list (d (n "armv8") (r "^0.0.1") (d #t) (t "cfg(target_arch = \"aarch64\")") (k 0)) (d (n "heapless") (r "^0.7.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pl011_drv") (r "^0.1.0") (d #t) (t "cfg(target_arch = \"aarch64\")") (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "termcodes") (r "^0.0.1") (d #t) (k 0)) (d (n "x86") (r "^0.52") (f (quote ("unstable"))) (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "002xqqg2ydwnf7q10c5wv72wj9wgg0vsir04nm4hy2s08kpr5h59") (f (quote (("use_ioports"))))))

