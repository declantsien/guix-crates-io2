(define-module (crates-io kl -h kl-http) #:use-module (crates-io))

(define-public crate-kl-http-0.1.0 (c (n "kl-http") (v "0.1.0") (d (list (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "httparse") (r "^1.2.4") (d #t) (k 0)))) (h "11r849fir77yiqvgblq4dc26ls6azsr8n08n410x2ywlcg9pj9h5") (y #t)))

(define-public crate-kl-http-0.1.1 (c (n "kl-http") (v "0.1.1") (d (list (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "httparse") (r "^1.2.4") (d #t) (k 0)))) (h "09lw0fpmd7qbvq544f243m4nkjd3w7s1nd5fqaiv4xirv2k67ydk") (y #t)))

(define-public crate-kl-http-0.2.0 (c (n "kl-http") (v "0.2.0") (d (list (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "httparse") (r "^1.2.4") (d #t) (k 0)))) (h "0yyvcv47si8c8lz3ypzni35arn91ldp795zlbc2hyx4hl4ln2js8") (y #t)))

(define-public crate-kl-http-0.2.1 (c (n "kl-http") (v "0.2.1") (d (list (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "httparse") (r "^1.2.4") (d #t) (k 0)))) (h "1bljlhva9y919qvcwva8c1vkvlgrdzzc3r192wh9ds7wyiqb943l") (y #t)))

