(define-module (crates-io kl -h kl-hyphenate-commons) #:use-module (crates-io))

(define-public crate-kl-hyphenate-commons-0.7.3 (c (n "kl-hyphenate-commons") (v "0.7.3") (d (list (d (n "atlatl") (r "^0.1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rbnks9zcmna4m3d2fbbpr1g4v4gdm584s34yslzjx3c5fc55nxp")))

