(define-module (crates-io kl l- kll-macros) #:use-module (crates-io))

(define-public crate-kll-macros-0.1.0 (c (n "kll-macros") (v "0.1.0") (h "07dli2vmirkqvb0b7xxpc4li3vpkxmn5rhsbgsgc43mxayi643x6")))

(define-public crate-kll-macros-0.1.1 (c (n "kll-macros") (v "0.1.1") (h "1yx20xl2hngqv58w4mqdn36n6sp6wkqshbpzqkyxf6csqfzvbbn4")))

(define-public crate-kll-macros-0.1.2 (c (n "kll-macros") (v "0.1.2") (h "18zvwghv6127xas2as72b03si6mjmcrvas5yd95b3i2m8yvsgygc")))

