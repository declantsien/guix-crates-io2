(define-module (crates-io kl l- kll-hid) #:use-module (crates-io))

(define-public crate-kll-hid-0.1.0 (c (n "kll-hid") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "0qf19zvm48skkmxjvm6nb181fqb8j4k41hxa1l4rjhicsnd9a9bc")))

(define-public crate-kll-hid-0.1.1 (c (n "kll-hid") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "137zp87nczr547xqavhf86cbw11vd1kb485jj9jbmz26aygb4nv5") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-kll-hid-0.1.2 (c (n "kll-hid") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1ly96dfv4rrv0i2qffwqh6cbjwqfx4d6gmzppq3y98xbbbs000sn") (s 2) (e (quote (("defmt" "dep:defmt"))))))

