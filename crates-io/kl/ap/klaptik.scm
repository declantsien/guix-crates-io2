(define-module (crates-io kl ap klaptik) #:use-module (crates-io))

(define-public crate-klaptik-0.0.0 (c (n "klaptik") (v "0.0.0") (h "0x4ppr5i6jkkqzi88frqlwvazakkg4cxjdi3911xf8gdw35si8k2")))

(define-public crate-klaptik-0.0.1 (c (n "klaptik") (v "0.0.1") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0arf0nm0818qy3rlhrxdc0zmj3l70fbw1j0raprb69a5cpj21ifj")))

(define-public crate-klaptik-0.0.2 (c (n "klaptik") (v "0.0.2") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1s8502jgd935p1ws3hcs6dmnpwgcjghdjjk74cl15p2m8sn55ldb")))

(define-public crate-klaptik-0.0.3 (c (n "klaptik") (v "0.0.3") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0mcd36b2whyymhhda1dqy9dv8s426m7qym1dhr8nzj87ah2mzp8s")))

(define-public crate-klaptik-0.0.4 (c (n "klaptik") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "0cakaih2i3nxrp7255m9vkczlq6gz0b18gs8ihvh31f1wacmfdr1")))

(define-public crate-klaptik-0.1.0 (c (n "klaptik") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ist7920") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "sh1106") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "ssd1306") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "ssd1309") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1v0kyp9nxswnd6s5ng4nlaamw5chidfr92x9n9i7qiv6rcg5cf9v") (f (quote (("st7567")))) (s 2) (e (quote (("ssd1309" "dep:ssd1309" "dep:display-interface") ("ssd1306" "dep:ssd1306") ("sh1106" "dep:sh1106") ("ist7920" "dep:ist7920" "dep:display-interface"))))))

(define-public crate-klaptik-0.2.0 (c (n "klaptik") (v "0.2.0") (d (list (d (n "display-interface") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ist7920") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "postcard") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (o #t) (k 0)) (d (n "sh1106") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "ssd1306") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "ssd1309") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "02d9n23bilpz9jg95lp1swwcx0gsry96hjy3f1kzssa0bgxqwpnb") (f (quote (("st7567") ("fx") ("ci" "ist7920" "sh1106" "ssd1306" "ssd1309" "st7567" "fx")))) (s 2) (e (quote (("ssd1309" "dep:ssd1309" "dep:display-interface") ("ssd1306" "dep:ssd1306") ("sh1106" "dep:sh1106") ("serde" "dep:serde" "dep:postcard") ("ist7920" "dep:ist7920" "dep:display-interface"))))))

(define-public crate-klaptik-0.2.1 (c (n "klaptik") (v "0.2.1") (d (list (d (n "display-interface") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ist7920") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "postcard") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (o #t) (k 0)) (d (n "sh1106") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "ssd1306") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "ssd1309") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "081lhbajh0bf7sf51zynw4xbhdb9m0j1zm2qp38s9chq1630z1r5") (f (quote (("st7567") ("fx") ("ci" "ist7920" "sh1106" "ssd1306" "ssd1309" "st7567" "fx")))) (s 2) (e (quote (("ssd1309" "dep:ssd1309" "dep:display-interface") ("ssd1306" "dep:ssd1306") ("sh1106" "dep:sh1106") ("serde" "dep:serde" "dep:postcard") ("ist7920" "dep:ist7920" "dep:display-interface"))))))

