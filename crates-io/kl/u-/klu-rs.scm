(define-module (crates-io kl u- klu-rs) #:use-module (crates-io))

(define-public crate-klu-rs-0.2.0 (c (n "klu-rs") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "klu_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1sryivsld4zhpf7h5agdnd1lgrxd135n33567c8kh0a2khiv2xns") (f (quote (("dynamic" "klu_sys/dynamic"))))))

(define-public crate-klu-rs-0.3.0 (c (n "klu-rs") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "klu_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0j93p8ysjwa632d6nf8wrqq6j1pyv15j5vxivbn8v2mk2hkq4z2r") (f (quote (("dynamic" "klu_sys/dynamic")))) (y #t)))

(define-public crate-klu-rs-0.3.1 (c (n "klu-rs") (v "0.3.1") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "klu_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1gbpmfxsrmjwczhxvpbr535836fh0xgw65hbrdgn750r6m4izbsy") (f (quote (("dynamic" "klu_sys/dynamic"))))))

(define-public crate-klu-rs-0.4.0 (c (n "klu-rs") (v "0.4.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "klu_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "109ilhxfjnl4qcx0c4zkklgf9sxyi9dl5l429lsrxbmspv1jc0sz") (f (quote (("dynamic" "klu_sys/dynamic"))))))

