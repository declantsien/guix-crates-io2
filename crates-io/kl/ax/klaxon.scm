(define-module (crates-io kl ax klaxon) #:use-module (crates-io))

(define-public crate-klaxon-0.0.1 (c (n "klaxon") (v "0.0.1") (h "03z2cjf1z1d8vpkh4jdgay61px29dls1mhb64ypzk82i9vjdcy8z")))

(define-public crate-klaxon-0.1.0 (c (n "klaxon") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "pagerduty-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1111z7nsw8jbxxnzz53hrg9s0prr1r8jsbxmgiqiisv34000z58m")))

(define-public crate-klaxon-0.2.0 (c (n "klaxon") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "pagerduty-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1i2fi2hwpriwd0v5cm3rz3b991z41nmxa86iwz1q2ydp8lvxapdn")))

(define-public crate-klaxon-0.2.1 (c (n "klaxon") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pagerduty-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0z8yd3r4idy0jmrwv98wz1rwrnvq5kpj4ndkgdx6xac7vrqzk0rk")))

