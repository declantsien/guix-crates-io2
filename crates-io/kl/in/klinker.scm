(define-module (crates-io kl in klinker) #:use-module (crates-io))

(define-public crate-klinker-1.0.2 (c (n "klinker") (v "1.0.2") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^2.0.5") (d #t) (k 0)))) (h "0xv0mhj0l68sxc2fr87m48vfglm06091ha6l3xl36a74w5f8a29d")))

(define-public crate-klinker-2.0.0 (c (n "klinker") (v "2.0.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.8") (d #t) (k 0)))) (h "1xlyprayxbgccrqbjbw9ir737iq8746ykdljcpawmv58wyqy8yqv")))

(define-public crate-klinker-2.0.1 (c (n "klinker") (v "2.0.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.8") (d #t) (k 0)))) (h "0gn0b6534w43ai9r26y8xkbgvfc5h4440ylriz82amx8x0f8i9aj")))

(define-public crate-klinker-2.0.2 (c (n "klinker") (v "2.0.2") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.9") (d #t) (k 0)))) (h "0fi22li6b9vc7h61ncrr0svwjd7qrqwcxhd5amdxx99z67dszm0s")))

(define-public crate-klinker-2.0.3 (c (n "klinker") (v "2.0.3") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.11") (d #t) (k 0)))) (h "07za8w3d667ygiyikn041j5hn6gcxzr33h0sj215f0j8wp8vb784")))

(define-public crate-klinker-2.0.4 (c (n "klinker") (v "2.0.4") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)))) (h "1pgm0g3z3iaf7wivjfnywkcjpmpiafwaxv16qr08xw0nca89wc48")))

(define-public crate-klinker-2.0.5 (c (n "klinker") (v "2.0.5") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)))) (h "1mi5vmwkmg894rm35b7fplmjb28yg3dw5a1y6lz0xz8d9zavmg3f")))

(define-public crate-klinker-2.0.6 (c (n "klinker") (v "2.0.6") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)))) (h "0ga66h0skx69nswrkdldmbmdjfdnnjb3y4169jyiwg5czvpnw2ag")))

(define-public crate-klinker-2.0.7 (c (n "klinker") (v "2.0.7") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)))) (h "0k6fd0kmc2bisvdzhsg2m9pr2132kkpipbnb5nkw4mvbrlpp8zzq")))

(define-public crate-klinker-2.0.8 (c (n "klinker") (v "2.0.8") (d (list (d (n "clap") (r "~2.34.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)))) (h "0qp4hbi9fimdayzkqaaz1cmh15g74r7znwkkp1kar5b6qd8i7wrk")))

(define-public crate-klinker-2.0.9 (c (n "klinker") (v "2.0.9") (d (list (d (n "clap") (r "~2.34.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)))) (h "1q4laa2vxvhz012acp4sjpi0hgxla162ypbi9c5bp33v1277w0kc")))

(define-public crate-klinker-2.0.10 (c (n "klinker") (v "2.0.10") (d (list (d (n "clap") (r "~2.34.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.13") (d #t) (k 0)))) (h "0svxvm94iq6ihi9png5cfnmqslia803gzc6w3irvylrcvv169nnn")))

(define-public crate-klinker-2.0.11 (c (n "klinker") (v "2.0.11") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.13") (d #t) (k 0)))) (h "1c3qn64mk1lsxb9sqpjyffscpvh3miniqx3bmq03z142vka20589")))

(define-public crate-klinker-3.0.0 (c (n "klinker") (v "3.0.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^4.0") (d #t) (k 0)))) (h "0wwgaa46xd7azmacd9g89rndqgwcq6iqrr2v41r9hyqyycjg5agw")))

(define-public crate-klinker-3.0.1 (c (n "klinker") (v "3.0.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^4.0") (d #t) (k 0)))) (h "14x8qp2xbvx1wcp9cfgfgvwmrgab2ivq2ndlk5vh67rjjvscnq72")))

(define-public crate-klinker-3.0.2 (c (n "klinker") (v "3.0.2") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^4.0.2") (d #t) (k 0)))) (h "1lwiwarqnj5xrnhpxqrnmiyikwhlv79if7qfldni45p8ngfskrwp")))

