(define-module (crates-io gi t2 git2-testing) #:use-module (crates-io))

(define-public crate-git2-testing-0.1.0 (c (n "git2-testing") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "git2") (r ">=0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 0)))) (h "0av9al27r48wnghi7dn5vngxvray91j7281w1yzk0cv8wh3b0hs8")))

