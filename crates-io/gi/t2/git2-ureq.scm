(define-module (crates-io gi t2 git2-ureq) #:use-module (crates-io))

(define-public crate-git2-ureq-0.1.0 (c (n "git2-ureq") (v "0.1.0") (d (list (d (n "git2") (r "^0.14") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0ga3x9js0cs3lllprx5kvgbv3jnnpcwksgwwgaa44iv9q4q0gr8d")))

(define-public crate-git2-ureq-0.1.1 (c (n "git2-ureq") (v "0.1.1") (d (list (d (n "git2") (r "^0.14") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1i42r9m8xaq91ygv5m3a5ajpibscs3axc3i0iadndb37hpcjd2ji") (f (quote (("socks-proxy" "ureq/socks-proxy"))))))

(define-public crate-git2-ureq-0.2.0 (c (n "git2-ureq") (v "0.2.0") (d (list (d (n "git2") (r "^0.15") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "05fqpzmg7gwn33z89bxvmh1ywsnsmmbqj0v4q6p0ghzmd52d81xa") (f (quote (("socks-proxy" "ureq/socks-proxy"))))))

(define-public crate-git2-ureq-0.3.0 (c (n "git2-ureq") (v "0.3.0") (d (list (d (n "git2") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0i8n8c4psgsffvj56kw9lhff6k0cikx86camgyidhhxiabnq35hi") (f (quote (("socks-proxy" "ureq/socks-proxy"))))))

