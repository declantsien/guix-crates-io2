(define-module (crates-io gi t2 git2ext) #:use-module (crates-io))

(define-public crate-git2ext-0.0.1 (c (n "git2ext") (v "0.0.1") (d (list (d (n "git2") (r "^0.14") (k 0)))) (h "0bfcqh0m3nsg7sgmx7frnddqhf57prr4igcq28nab6kpn8vmx5n1")))

(define-public crate-git2ext-0.0.3 (c (n "git2ext") (v "0.0.3") (d (list (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "git-fixture") (r "^0.2") (d #t) (k 2)) (d (n "git2") (r "^0.14") (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "1mkqh3hrzdirqv9pxy3p6qx1im9z966kfi2dgpgwzfkzgfyzbbyd")))

