(define-module (crates-io gi t2 git2-hooks) #:use-module (crates-io))

(define-public crate-git2-hooks-0.1.0 (c (n "git2-hooks") (v "0.1.0") (d (list (d (n "git2") (r ">=0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "shellexpand") (r "^3.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1phfnkm58r412hd5q6hlnky0h3l8dd5axv13gp1lidzycvr3yicw")))

(define-public crate-git2-hooks-0.2.0 (c (n "git2-hooks") (v "0.2.0") (d (list (d (n "git2") (r ">=0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "shellexpand") (r "^3.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0czydbzshcivjj8wm9l4jp77ic6l4h5hn73vc7pc4scgp9qn0601")))

(define-public crate-git2-hooks-0.3.0 (c (n "git2-hooks") (v "0.3.0") (d (list (d (n "git2") (r ">=0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "shellexpand") (r "^3.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "005qcds0fn9k4bg4vcv4yzpbgfqijrnhqbw40pgvn5iam7p6p9qg")))

(define-public crate-git2-hooks-0.3.1 (c (n "git2-hooks") (v "0.3.1") (d (list (d (n "git2") (r ">=0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "shellexpand") (r "^3.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1grq79ggjp2b10yxl205kjkfqcijmmncnf47az3g1g713irpzgwx")))

