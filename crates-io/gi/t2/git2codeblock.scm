(define-module (crates-io gi t2 git2codeblock) #:use-module (crates-io))

(define-public crate-git2codeblock-0.1.0 (c (n "git2codeblock") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0r10if6dd10wq49izl8vp5w6slxqg7ahmk4dh27nkn77di6wc7pk")))

