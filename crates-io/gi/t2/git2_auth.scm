(define-module (crates-io gi t2 git2_auth) #:use-module (crates-io))

(define-public crate-git2_auth-0.1.0 (c (n "git2_auth") (v "0.1.0") (d (list (d (n "git2") (r "^0.16") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ckjvfd6af05hx0k2mk6w66jmcqkhbl255l9waw91xwmrj88r8q2")))

