(define-module (crates-io gi ta gitar) #:use-module (crates-io))

(define-public crate-gitar-0.1.0 (c (n "gitar") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "minstrel") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "0rqcpiad7d2zdi0xfj7k6ziz6czklwzrb7cl74gwaimbs5gqh5hy")))

(define-public crate-gitar-0.1.1 (c (n "gitar") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "minstrel") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "13p6zgb14r2m87bbmgbqxvpdfway7c8b2p7pz8cj41nmiqh410p8")))

