(define-module (crates-io gi ta gita) #:use-module (crates-io))

(define-public crate-gita-0.0.1 (c (n "gita") (v "0.0.1") (d (list (d (n "git2") (r "^0.13.6") (d #t) (k 0)))) (h "10rzanfr0fngi480yi6x5aykpjl44y4f13y3bbahs6zb2lqmdw98")))

(define-public crate-gita-0.0.2 (c (n "gita") (v "0.0.2") (d (list (d (n "git2") (r "^0.13.6") (d #t) (k 0)))) (h "18p1w03whspmkmay2zyz5n1crjsz7pmiclci6ssiryp15fxj2q5n")))

