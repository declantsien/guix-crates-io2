(define-module (crates-io gi ta gitarena-macros) #:use-module (crates-io))

(define-public crate-gitarena-macros-0.0.1 (c (n "gitarena-macros") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "=1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.27") (d #t) (k 0)) (d (n "quote") (r "=1.0.9") (d #t) (k 0)) (d (n "syn") (r "=1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0hhflripz1v98amnk93qg87d4bnw0c99ly4r6m6d3k981jacvz42") (y #t)))

(define-public crate-gitarena-macros-0.0.0 (c (n "gitarena-macros") (v "0.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1ip80vzbaap840ayk915rjqyi5a8svlnx4ifn0vn9krm0vfkc5z6")))

