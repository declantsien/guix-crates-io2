(define-module (crates-io gi ta gitapi-rs) #:use-module (crates-io))

(define-public crate-gitapi-rs-0.1.0 (c (n "gitapi-rs") (v "0.1.0") (h "0dbak2l9c47yvwgqr8gkvdiwb3i6ccfhw2w18ksyfilzn7ykz6ap")))

(define-public crate-gitapi-rs-0.1.2 (c (n "gitapi-rs") (v "0.1.2") (d (list (d (n "cast-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1vc2ia5fqhcscl75vya4hrzln9y7dnyrpd27dhx44vcydrz3g5cr")))

