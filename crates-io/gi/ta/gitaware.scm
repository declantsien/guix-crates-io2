(define-module (crates-io gi ta gitaware) #:use-module (crates-io))

(define-public crate-gitaware-1.0.1 (c (n "gitaware") (v "1.0.1") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "1h93b79mf9l7mx9brzlqxsplhmgyn7s5kc5x82f5fy69djvzl5bl")))

(define-public crate-gitaware-1.0.2 (c (n "gitaware") (v "1.0.2") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "07fg21aq3hkcjwa3q483c7cplxpmdb6q9wla6dbaamylpfz0ll8r")))

