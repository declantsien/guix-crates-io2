(define-module (crates-io gi _t gi_t) #:use-module (crates-io))

(define-public crate-gi_t-0.0.1 (c (n "gi_t") (v "0.0.1") (h "1y531pchcr25vv55iqyiarlp43xrxgbls9ddddcdgzad6531g3ay")))

(define-public crate-gi_t-0.0.2 (c (n "gi_t") (v "0.0.2") (d (list (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0f4gn05b589b0ymhrb37bb589f8wwwd9jgqagwlmfka8licznk45")))

(define-public crate-gi_t-0.0.3 (c (n "gi_t") (v "0.0.3") (d (list (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0lpa22qirzy8is861a7pxzjzd03isrn1j7lksz3vk8sp6pzwdy74")))

(define-public crate-gi_t-0.0.4 (c (n "gi_t") (v "0.0.4") (d (list (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "07qw1jwd85j8nan4r1x5bpk6mnb6xbffivkag7yaip74mwfk8d0g")))

(define-public crate-gi_t-0.0.5 (c (n "gi_t") (v "0.0.5") (d (list (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1nvxbfvib4ap37a9jzqdapz57k7gk1mp14w6rdly2v5xijyn4g69")))

(define-public crate-gi_t-0.0.6 (c (n "gi_t") (v "0.0.6") (d (list (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0ciy1vvmiyx9v8raa1fjbhymhkhx5xrcv1an19p45ss2iwqzk75f")))

