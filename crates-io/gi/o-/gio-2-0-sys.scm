(define-module (crates-io gi o- gio-2-0-sys) #:use-module (crates-io))

(define-public crate-gio-2-0-sys-0.46.3 (c (n "gio-2-0-sys") (v "0.46.3") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1q8rcqmqzfxfzag4rjnf2r5s4rxd7xra2hbv3chx2m4hk8dbrspm")))

(define-public crate-gio-2-0-sys-0.46.4 (c (n "gio-2-0-sys") (v "0.46.4") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "02py8qx2xlwlawblxi6y5ilp5wh86blqhspiz4kpsnm3n52mx3r8")))

