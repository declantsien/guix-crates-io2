(define-module (crates-io gi ts gitscanr) #:use-module (crates-io))

(define-public crate-gitscanr-0.2.0 (c (n "gitscanr") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "git2") (r "^0.14.4") (d #t) (k 0)))) (h "133l3q14k9z9pqyksv9cvxn1hvkxz6xkiyn76np6dq8x1wgvqdbj")))

(define-public crate-gitscanr-0.2.1 (c (n "gitscanr") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1gdq54h0w1wvi2fdhmkv9hmkks737zns6dv2k8hrdxhldwn3932m")))

