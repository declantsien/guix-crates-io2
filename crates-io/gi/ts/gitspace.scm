(define-module (crates-io gi ts gitspace) #:use-module (crates-io))

(define-public crate-gitspace-0.0.1 (c (n "gitspace") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "0qlkpldby2iphb1h748siq03ws7ni399a6352rxprmnlrkwdamb7")))

