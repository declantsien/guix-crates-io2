(define-module (crates-io gi ts gitstatusd) #:use-module (crates-io))

(define-public crate-gitstatusd-0.1.0 (c (n "gitstatusd") (v "0.1.0") (h "18jjbx67zxnxr4igkcirfvj3fw4wcrp2wl3ynqg30j0lvzzy6ikk")))

(define-public crate-gitstatusd-0.2.0 (c (n "gitstatusd") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0a59sqbpr245yil62pjkp38pasxr65ryy9mga3xms4ipi408qlb7")))

(define-public crate-gitstatusd-0.2.1 (c (n "gitstatusd") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "087mpnl4l80z2di1swykrbhvrisp9jii4r3nl0gxfjyl52q3j6yz")))

