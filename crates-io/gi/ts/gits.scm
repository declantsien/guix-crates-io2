(define-module (crates-io gi ts gits) #:use-module (crates-io))

(define-public crate-gits-0.1.0 (c (n "gits") (v "0.1.0") (h "0q88lyr5mgjaslxlmbx4fhi5mfzc7zv59mhbj2lhjw8p0bidhz3v") (y #t)))

(define-public crate-gits-0.2.0 (c (n "gits") (v "0.2.0") (h "1rs6s9y93qpdyn5s7x19wmb4xz3d6nk0v8n5ivghiy3sjvkpp7il") (y #t)))

