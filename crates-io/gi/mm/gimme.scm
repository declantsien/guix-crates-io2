(define-module (crates-io gi mm gimme) #:use-module (crates-io))

(define-public crate-gimme-0.1.0 (c (n "gimme") (v "0.1.0") (h "0bvfxrn5i87pk870qysvdkp739c73ssf0y3alq8k0bzf868wd5cs")))

(define-public crate-gimme-0.1.2 (c (n "gimme") (v "0.1.2") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0if8ljhvfzjjx3c6q23k932hzh2b2w19cd8k2p55fraswpsq99nq")))

(define-public crate-gimme-0.1.4 (c (n "gimme") (v "0.1.4") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0a717yyxfjkvf7hd1hnljgkhhm8s3b1zzh3zbc3jgxkyn3m1xa58")))

(define-public crate-gimme-0.1.5 (c (n "gimme") (v "0.1.5") (d (list (d (n "clap") (r "~2.33.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a7hlzfgfs1qkgh3hfsrjlhasn6fpdnlhj2jg3k8zg37j7cxywki")))

(define-public crate-gimme-0.1.6 (c (n "gimme") (v "0.1.6") (d (list (d (n "clap") (r "~2.33.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1pblfxylfc9fwim969hf9bc023mjxsidp8qjma74lkx8ps1swwil")))

(define-public crate-gimme-0.1.7 (c (n "gimme") (v "0.1.7") (d (list (d (n "clap") (r "~2.33.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14ynhb0dkyjxcx9nzviw3hsja7qzq2dx420sga0p813pni9lsy7k")))

(define-public crate-gimme-0.1.8 (c (n "gimme") (v "0.1.8") (d (list (d (n "clap") (r "~2.33.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1dxwv9is8c9fnc39gd1i8bkvvixf3hjv5c61ybinlnrgimbg3zkw")))

(define-public crate-gimme-0.1.9 (c (n "gimme") (v "0.1.9") (d (list (d (n "clap") (r "~2.33.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkify") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0whifip1s0rlvs01xgl5y3arb464q0wiha9yxxfv5k6zg03dcjmp")))

(define-public crate-gimme-0.1.10 (c (n "gimme") (v "0.1.10") (d (list (d (n "clap") (r "~2.33.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkify") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1q8jkr8qvafkd4lljr4d49wcckwy1nhrr0y0g9j0kwzc3drz7jnr")))

(define-public crate-gimme-0.1.11 (c (n "gimme") (v "0.1.11") (d (list (d (n "clap") (r "~2.33.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkify") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1v7v9d6hq15679mpg6gsdwfbnkqfjk85z5718slsm32hcc1l6l0x")))

(define-public crate-gimme-0.1.12 (c (n "gimme") (v "0.1.12") (d (list (d (n "clap") (r "~2.33.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkify") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19s87zax1pvqv5cjga9iq31zkhkmq9z2rdss40gww43027wn5vv8")))

(define-public crate-gimme-0.1.13 (c (n "gimme") (v "0.1.13") (d (list (d (n "clap") (r "~3.0.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkify") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0l4hbcq6ggffsrk9z06y2b3gyyxgjx7d04yjm5gjki40qjy6fbmc")))

