(define-module (crates-io gi mm gimmiepass) #:use-module (crates-io))

(define-public crate-gimmiepass-0.1.0 (c (n "gimmiepass") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0li7v4jhpqmlax4c703lxsjhfxrjry77j10navhiqx1p9x3f6ckf")))

(define-public crate-gimmiepass-0.1.1 (c (n "gimmiepass") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1i5bi12m689kz5rwr6i5jfk1sbq7p8xd9vikpgc0skk7vn4pr6vw")))

(define-public crate-gimmiepass-0.1.2 (c (n "gimmiepass") (v "0.1.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fql6rcmp06azqsfwqm8q700h2qva8zf5if21v0cy9x09l4406s8")))

(define-public crate-gimmiepass-0.1.3 (c (n "gimmiepass") (v "0.1.3") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "193xamx7d8qabpl38vz466naicc8sf7svjj79fg1v3fkzmakh8lb")))

