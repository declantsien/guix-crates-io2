(define-module (crates-io gi x- gix-fs) #:use-module (crates-io))

(define-public crate-gix-fs-0.1.0 (c (n "gix-fs") (v "0.1.0") (d (list (d (n "gix-features") (r "^0.29.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1nwg9mvrhwr7177p6idfzfky9bmxl5yv9bnv3cf3y3pans34jd8f") (r "1.64")))

(define-public crate-gix-fs-0.1.1 (c (n "gix-fs") (v "0.1.1") (d (list (d (n "gix-features") (r "^0.29.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1abqc9hh5s7r65p8rily3xy764z4lbwnglhbj44xq7v95y1s2dwv") (r "1.64")))

(define-public crate-gix-fs-0.2.0 (c (n "gix-fs") (v "0.2.0") (d (list (d (n "gix-features") (r "^0.30.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1ddah0c5ljpfpy2ab1gmgblli2lz4bpbgghm9vwqgnwa02bqknih") (r "1.64")))

(define-public crate-gix-fs-0.3.0 (c (n "gix-fs") (v "0.3.0") (d (list (d (n "gix-features") (r "^0.31.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "15i456v236xlqbyxszi14hbs20lajpvgr6935b398r95q1mra5dv") (r "1.65")))

(define-public crate-gix-fs-0.4.0 (c (n "gix-fs") (v "0.4.0") (d (list (d (n "gix-features") (r "^0.32.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0ya2krfvqhngmvw8fj9jfns1gcaiajbyxrnb134hlnwci39q3jl6") (r "1.65")))

(define-public crate-gix-fs-0.4.1 (c (n "gix-fs") (v "0.4.1") (d (list (d (n "gix-features") (r "^0.32.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0qc7fpcgxmwc55kvp31ll6ydpdls816akg1bl2j1xim26jfnwnsd") (r "1.65")))

(define-public crate-gix-fs-0.5.0 (c (n "gix-fs") (v "0.5.0") (d (list (d (n "gix-features") (r "^0.33.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1y3yhxgkjbkjligm277jarph9av4lq0ahx9sfdv7fr1q6fghimak") (r "1.65")))

(define-public crate-gix-fs-0.6.0 (c (n "gix-fs") (v "0.6.0") (d (list (d (n "gix-features") (r "^0.34.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1m4qqacfivk8zz3v0dsb3qg6sqsrsivavcn3nsd0qrjc7pd9ais0") (r "1.65")))

(define-public crate-gix-fs-0.7.0 (c (n "gix-fs") (v "0.7.0") (d (list (d (n "gix-features") (r "^0.35.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0db6bj773ssqvy03mi28glzy963cd1aaaxcbj4nv7s9glsmmz089") (r "1.65")))

(define-public crate-gix-fs-0.8.0 (c (n "gix-fs") (v "0.8.0") (d (list (d (n "gix-features") (r "^0.36.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1gbpr1r744795g0g6gi61r2rdxgbn4f61d77azfd0z79rb073lcc") (r "1.65")))

(define-public crate-gix-fs-0.8.1 (c (n "gix-fs") (v "0.8.1") (d (list (d (n "gix-features") (r "^0.36.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "01z1whm3qn0pinw4inbpvf53kbfw3kjq48h9vrd6lxzm82q6xs10") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-fs-0.9.0 (c (n "gix-fs") (v "0.9.0") (d (list (d (n "gix-features") (r "^0.37.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "18fh2p042wccakqp0gbw60njl8bjgmpk0s7cq19fl6dqjg71fw00") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.70")))

(define-public crate-gix-fs-0.9.1 (c (n "gix-fs") (v "0.9.1") (d (list (d (n "gix-features") (r "^0.37.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1pgcmp17qizq2zk0s7cn08kd9jhq9rlkk2fbpx5l6dsm00xc4mbm") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-fs-0.10.0 (c (n "gix-fs") (v "0.10.0") (d (list (d (n "gix-features") (r "^0.38.0") (f (quote ("fs-read-dir"))) (d #t) (k 0)) (d (n "gix-utils") (r "^0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0ys3393dihkg98bh8hcyby0mg0r294mp3f3phsqrz7vnsn1yhdj4") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-fs-0.10.1 (c (n "gix-fs") (v "0.10.1") (d (list (d (n "gix-features") (r "^0.38.1") (f (quote ("fs-read-dir"))) (d #t) (k 0)) (d (n "gix-utils") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "04q6dwf6w47and9cwr5cxyanh4y5x0jfmq2flz0h7bha7ds8ljv3") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-fs-0.10.2 (c (n "gix-fs") (v "0.10.2") (d (list (d (n "gix-features") (r "^0.38.1") (f (quote ("fs-read-dir"))) (d #t) (k 0)) (d (n "gix-utils") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1f1iwqvscsjvsvx23v974kczz27pmj0v9j1ig1kjj1ciwx04q672") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-fs-0.11.0 (c (n "gix-fs") (v "0.11.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 2)) (d (n "fastrand") (r "^2.1.0") (f (quote ("std"))) (k 0)) (d (n "gix-features") (r "^0.38.2") (f (quote ("fs-read-dir"))) (d #t) (k 0)) (d (n "gix-utils") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "033bfcr9fvqj68gz43a63i1fgp738nqk6fnpxw4mhynsvkbgfy1z") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-fs-0.11.1 (c (n "gix-fs") (v "0.11.1") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 2)) (d (n "fastrand") (r "^2.1.0") (f (quote ("std"))) (k 0)) (d (n "gix-features") (r "^0.38.2") (f (quote ("fs-read-dir"))) (d #t) (k 0)) (d (n "gix-utils") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0yppd6x6fa1gw8kivc3bcxr1lmvg676w0phqkwhgar115bwqycy3") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

