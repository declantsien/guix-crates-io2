(define-module (crates-io gi x- gix-command) #:use-module (crates-io))

(define-public crate-gix-command-0.2.3 (c (n "gix-command") (v "0.2.3") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)))) (h "1iaamkr9k2r9mkm3mxgjmc5iqlpn5cbydgig2rrfak1hazb1sdll") (r "1.64")))

(define-public crate-gix-command-0.2.4 (c (n "gix-command") (v "0.2.4") (d (list (d (n "bstr") (r "^1.3.0") (d #t) (k 0)))) (h "1azqfxr7hbbx7kz3wxxbn55ij1qkwak8023mkvilv4hg3rfggimj") (r "1.64")))

(define-public crate-gix-command-0.2.5 (c (n "gix-command") (v "0.2.5") (d (list (d (n "bstr") (r "^1.3.0") (d #t) (k 0)))) (h "1lgs2sshjbwd67c8vdbkhszcndshhmwk6bz44d92a8gv1jvl2qaz") (r "1.64")))

(define-public crate-gix-command-0.2.6 (c (n "gix-command") (v "0.2.6") (d (list (d (n "bstr") (r "^1.3.0") (d #t) (k 0)))) (h "1ssi7zwrbzcrbsyvbpk50pzps9s1wl82pjjv86rapc1pg9asnjdv") (r "1.65")))

(define-public crate-gix-command-0.2.7 (c (n "gix-command") (v "0.2.7") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (k 0)))) (h "19sncz871clpspqmr8rsrsj5n9alll6fsh2v5j7sa5knr29nm39p") (r "1.65")))

(define-public crate-gix-command-0.2.8 (c (n "gix-command") (v "0.2.8") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (k 0)))) (h "1pb2i0ib13k40zfsbz8q996jm9jjlmmp08v4sk7zjsxiiwaav0r7") (r "1.65")))

(define-public crate-gix-command-0.2.9 (c (n "gix-command") (v "0.2.9") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (k 0)))) (h "0mj2xr3rdcgagqjxaln3588g1n6bcvsf9irpaxf74psb31agca0g") (r "1.65")))

(define-public crate-gix-command-0.2.10 (c (n "gix-command") (v "0.2.10") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (k 0)))) (h "1ix44maislxlranv67yw5fan5k82lpgax22zgc4jrxvpypxnqmrw") (r "1.65")))

(define-public crate-gix-command-0.3.0 (c (n "gix-command") (v "0.3.0") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (k 0)) (d (n "gix-path") (r "^0.10.1") (d #t) (k 0)) (d (n "gix-trace") (r "^0.1.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)))) (h "1jghqjndy0nx3j0x6i0ba5c8h5pz95p4vffrvcmfzrk3ilflrddk") (r "1.65")))

(define-public crate-gix-command-0.3.1 (c (n "gix-command") (v "0.3.1") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (k 0)) (d (n "gix-path") (r "^0.10.2") (d #t) (k 0)) (d (n "gix-trace") (r "^0.1.5") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)))) (h "0l8fkf3gp6vlix82x1khv8vpgknp88sdyffns0zs3mddb76wigcs") (r "1.70")))

(define-public crate-gix-command-0.3.2 (c (n "gix-command") (v "0.3.2") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (k 0)) (d (n "gix-path") (r "^0.10.3") (d #t) (k 0)) (d (n "gix-trace") (r "^0.1.6") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)))) (h "0rw4wi1wbpi90pcj0icg77gw5bpg22qilw6hi2pbbkhqlj1sgvyy") (r "1.65")))

(define-public crate-gix-command-0.3.3 (c (n "gix-command") (v "0.3.3") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (k 0)) (d (n "gix-path") (r "^0.10.4") (d #t) (k 0)) (d (n "gix-trace") (r "^0.1.7") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)))) (h "029zy431in48i6nz4kb18ks2bdvwaaikgngcwvdbfl7vndyzq7yf") (r "1.65")))

(define-public crate-gix-command-0.3.4 (c (n "gix-command") (v "0.3.4") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std" "unicode"))) (k 0)) (d (n "gix-path") (r "^0.10.5") (d #t) (k 0)) (d (n "gix-trace") (r "^0.1.7") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)))) (h "044g8by50asvvcmkcdnj5mkp0szsw1gx3fs923k866g6jja5way8") (r "1.65")))

(define-public crate-gix-command-0.3.5 (c (n "gix-command") (v "0.3.5") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std" "unicode"))) (k 0)) (d (n "gix-path") (r "^0.10.6") (d #t) (k 0)) (d (n "gix-trace") (r "^0.1.7") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)))) (h "1z1p86ahd1swpvhjv1shzyrvrh4cgp05fdm7vb3ay0nn6a3s3gkz") (r "1.65")))

(define-public crate-gix-command-0.3.6 (c (n "gix-command") (v "0.3.6") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std" "unicode"))) (k 0)) (d (n "gix-path") (r "^0.10.7") (d #t) (k 0)) (d (n "gix-trace") (r "^0.1.8") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)))) (h "0xsbllakppwpn3z5qzyivlqa7q068wry2a7dpr3xxcy41l10j07r") (r "1.65")))

(define-public crate-gix-command-0.3.7 (c (n "gix-command") (v "0.3.7") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std" "unicode"))) (k 0)) (d (n "gix-path") (r "^0.10.7") (d #t) (k 0)) (d (n "gix-trace") (r "^0.1.8") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)))) (h "09f29p1lfqf57023nvvjznj9shym484mrp2wzqzw95a0663f08kc") (r "1.65")))

