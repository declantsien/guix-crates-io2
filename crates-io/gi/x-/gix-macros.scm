(define-module (crates-io gi x- gix-macros) #:use-module (crates-io))

(define-public crate-gix-macros-0.1.0 (c (n "gix-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1dhqxmj29k5b6l8y30ylkacscrzgz6ij18qr5l7mzmb8wrgcp2lx") (r "1.65")))

(define-public crate-gix-macros-0.1.1 (c (n "gix-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1bnkn52vdyf2w81f2lkk1jcaarnkfxzdxkki6153bn84cypvr982") (r "1.65")))

(define-public crate-gix-macros-0.1.2 (c (n "gix-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0fijb7v6gr1wf3i13dj6an2d0qsxkawim17w2z5ipxww31j7n87w") (r "1.70")))

(define-public crate-gix-macros-0.1.3 (c (n "gix-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fz8dqvavv28b6fksjdff4cj6sld3dvxh6nwdmgmk7q552vplpnp") (r "1.65")))

(define-public crate-gix-macros-0.1.4 (c (n "gix-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cj048i0a5xzqbv99514xfvhxwn833yzaclkmc9pfzp62j7l7zqx") (r "1.65")))

(define-public crate-gix-macros-0.1.5 (c (n "gix-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05ycxnh5sxjsn4lvay309n6knr8ksvkb6zx6f50ik24zc4iyk74r") (r "1.65")))

