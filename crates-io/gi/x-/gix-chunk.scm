(define-module (crates-io gi x- gix-chunk) #:use-module (crates-io))

(define-public crate-gix-chunk-0.4.1 (c (n "gix-chunk") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0y19g5pnx8f1r71j9jw3fc70y9sqfiqg3cvkpyw68r5hra1rblxh") (r "1.64")))

(define-public crate-gix-chunk-0.4.2 (c (n "gix-chunk") (v "0.4.2") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0h75sq4y7xrph4qx6przj5nkmsh5w7sxm1h04vxyi4abdjyg7b57") (r "1.64")))

(define-public crate-gix-chunk-0.4.3 (c (n "gix-chunk") (v "0.4.3") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1p9gczj2s35gny8kp3r6xjfkfc5wzmz3z6b5h8drnbhazk85xnrr") (r "1.65")))

(define-public crate-gix-chunk-0.4.4 (c (n "gix-chunk") (v "0.4.4") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "14s4f3g8n6yk6q28f60528wzcf10g8y8ycih04098y8g89jflhjv") (r "1.65")))

(define-public crate-gix-chunk-0.4.5 (c (n "gix-chunk") (v "0.4.5") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0pxm5mpaqssjzra4jchq3p2ap3n4xq4y9dsj685w5c2qnpcyq4fl") (r "1.65")))

(define-public crate-gix-chunk-0.4.6 (c (n "gix-chunk") (v "0.4.6") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1yq8rdxzf1djk4xjr9c26kzj4yll79xcb5vbjvny03c7d7pkgpia") (r "1.70")))

(define-public crate-gix-chunk-0.4.7 (c (n "gix-chunk") (v "0.4.7") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0y2wp3clilp5rdl727x4q4qw1chbgq91l9spq6h7d07nmkgccgh0") (r "1.65")))

(define-public crate-gix-chunk-0.4.8 (c (n "gix-chunk") (v "0.4.8") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0lhcmzamr5rlcw8h9bvsjqn9dak1mwj3ng2i1djaf6wnd48pbj25") (r "1.65")))

