(define-module (crates-io gi x- gix-utils) #:use-module (crates-io))

(define-public crate-gix-utils-0.1.0 (c (n "gix-utils") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "070fvrfm0d0wy7m0h8qcpk22nsjsy712kg3mqchz2i3z0kxq3p2p") (r "1.64")))

(define-public crate-gix-utils-0.1.1 (c (n "gix-utils") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "02f9cyvax1dkkqzznx7rj90dgqnx0wgpl63kys6wp6i1mjz6j2y1") (r "1.64")))

(define-public crate-gix-utils-0.1.2 (c (n "gix-utils") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "19ax86bbwbzkj1475ihdmpf5n12l68i7sil8d7bm7xby1hawpkyv") (r "1.64")))

(define-public crate-gix-utils-0.1.3 (c (n "gix-utils") (v "0.1.3") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "0dlsy92f0s8xbbglqqa2hdkhfr2rgafcbbjh81rc0nw4c31898hw") (r "1.65")))

(define-public crate-gix-utils-0.1.4 (c (n "gix-utils") (v "0.1.4") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "1pnxqqiivq57hiv9r34kgygpy071yvcgcdbx8nwgbz34857wjn3h") (r "1.65")))

(define-public crate-gix-utils-0.1.5 (c (n "gix-utils") (v "0.1.5") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "03rgnpcgy968sqqamm7w8197ykklhfas2lnr1rpf44w6fbf8jpdq") (r "1.65")))

(define-public crate-gix-utils-0.1.6 (c (n "gix-utils") (v "0.1.6") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "0gkaiilalddy83s2pzq78izzg88cgwq5a2ybyshia3ph6wcw90lz") (r "1.65")))

(define-public crate-gix-utils-0.1.7 (c (n "gix-utils") (v "0.1.7") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "1p515pxci6dx66nr8627mmkn1dx9lirdi5l0fczxb19fwfjpg4mb") (r "1.70")))

(define-public crate-gix-utils-0.1.8 (c (n "gix-utils") (v "0.1.8") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "0kz9sf2pwvinxv7dvarwzgqcw3v4653wrwfrlayfkdihvvi2aqny") (r "1.65")))

(define-public crate-gix-utils-0.1.9 (c (n "gix-utils") (v "0.1.9") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (k 0)))) (h "1f3a4slyvxw72b1xz988h3ppd8c0wyzacg9625j2k2vrs3rkks2n") (r "1.65")))

(define-public crate-gix-utils-0.1.10 (c (n "gix-utils") (v "0.1.10") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (k 0)))) (h "1dpf2vzkfsn44yhrhv8kwns502xijdxas5v83jpi2jzip4apl5b0") (s 2) (e (quote (("bstr" "dep:bstr")))) (r "1.65")))

(define-public crate-gix-utils-0.1.11 (c (n "gix-utils") (v "0.1.11") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (k 0)))) (h "14lg6k6v3pqb9y3zq3j1r9l1ycx5grcrl9wiy1vrhzr79hnl6rh0") (s 2) (e (quote (("bstr" "dep:bstr")))) (r "1.65")))

(define-public crate-gix-utils-0.1.12 (c (n "gix-utils") (v "0.1.12") (d (list (d (n "bstr") (r "^1.5.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (k 0)))) (h "1p6lschmdrg1j9cd3rm6q96dyrvivzi2305d7ck1588gzpvjs69m") (s 2) (e (quote (("bstr" "dep:bstr")))) (r "1.65")))

