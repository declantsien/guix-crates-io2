(define-module (crates-io gi x- gix-trace) #:use-module (crates-io))

(define-public crate-gix-trace-0.1.0 (c (n "gix-trace") (v "0.1.0") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "0ahvlavfyr02kjihfykg05l4r354g6a5cq8vlbjrkrlb4pjjj004") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.65")))

(define-public crate-gix-trace-0.1.1 (c (n "gix-trace") (v "0.1.1") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "1xnbn4ri4yhkm9b5gw6wnwyq8k9r27p3450qvmp4s3shfc0ady3g") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.65")))

(define-public crate-gix-trace-0.1.2 (c (n "gix-trace") (v "0.1.2") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "0qznabjadnhhgpval61jl9wk48hslmf0dj850sz3xghp2riaqghh") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.65")))

(define-public crate-gix-trace-0.1.3 (c (n "gix-trace") (v "0.1.3") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "0dmqswxz228in9p7vwhc0cq83r6sxkidcrwhnyn3yb0ml4ixddln") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.65")))

(define-public crate-gix-trace-0.1.4 (c (n "gix-trace") (v "0.1.4") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "15xlc23pmmnxypy7znf416j5786hh5jg18swawjrhfmmk5bs71mn") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.65")))

(define-public crate-gix-trace-0.1.5 (c (n "gix-trace") (v "0.1.5") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "17l09y37kpyfhm0gb7amxa4wf6pw7i0aaqipgh6a91nx8k5jm9mx") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.70")))

(define-public crate-gix-trace-0.1.6 (c (n "gix-trace") (v "0.1.6") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "129xhqmn7j07sg697g8bz4mas2xvv6hamh799y7vax84vrz15qg8") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.65")))

(define-public crate-gix-trace-0.1.7 (c (n "gix-trace") (v "0.1.7") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "1cd9288nj861b9ixm0pd79rsvn4avafaiiicdrczrzm7cvbh5ch2") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.65")))

(define-public crate-gix-trace-0.1.8 (c (n "gix-trace") (v "0.1.8") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "1ay5zplv97jysfs2ra214zizwrqv4n6w5943si3r8b7np0nqp0wv") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.65")))

(define-public crate-gix-trace-0.1.9 (c (n "gix-trace") (v "0.1.9") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.31") (o #t) (d #t) (k 0)))) (h "0zhm2lwqr070rq3bdn4b1zjs7mn7bhlkfgwfap6xspwi11s2c97r") (f (quote (("tracing-detail") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing-core")))) (r "1.65")))

