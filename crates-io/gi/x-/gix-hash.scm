(define-module (crates-io gi x- gix-hash) #:use-module (crates-io))

(define-public crate-gix-hash-0.10.2 (c (n "gix-hash") (v "0.10.2") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "15dkhqjg4d3bs3avdzxc0y851lca0nz5ny3kfn4j84dx1s5kkcp2") (f (quote (("serde1" "serde")))) (r "1.64")))

(define-public crate-gix-hash-0.10.3 (c (n "gix-hash") (v "0.10.3") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1w5y9pp71gn29himpblcbdrwlin7ylfk7fs6l174y7b29ngml30c") (f (quote (("serde1" "serde")))) (r "1.64")))

(define-public crate-gix-hash-0.10.4 (c (n "gix-hash") (v "0.10.4") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "19s1vs2n2hcm97hi99lpkfzf4d0yi8b0v7f5y78r5hbv8naqa99a") (f (quote (("serde1" "serde")))) (r "1.64")))

(define-public crate-gix-hash-0.11.0 (c (n "gix-hash") (v "0.11.0") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0w5kdpj0rn3kr1y4pi0x6pqyww21zd272rggxy6bmjdgkrq94rky") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.64")))

(define-public crate-gix-hash-0.11.1 (c (n "gix-hash") (v "0.11.1") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0hkwki4is53vjkbsjh9vqdfmrnk1nr671lnx1czw1340q8xfr3h7") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.64")))

(define-public crate-gix-hash-0.11.2 (c (n "gix-hash") (v "0.11.2") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1n0x63y8axxcqa48g6x3w698chm4mppglsvf8b258pwmsf2iq67f") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.64")))

(define-public crate-gix-hash-0.11.3 (c (n "gix-hash") (v "0.11.3") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0wl230sfhnvymsbnqwy9lbcyr32z1p46860izhr41ykzpv6mipd0") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-hash-0.11.4 (c (n "gix-hash") (v "0.11.4") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0bq986grpsfc6ddav5dlb8zvz1aky264dnnnmax2h1lsmpr2yhjb") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-hash-0.12.0 (c (n "gix-hash") (v "0.12.0") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "faster-hex") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1fgak1zddg0n251ydvya6pqwbgg39a9cllm1pvwc5w5aqfx9civx") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-hash-0.13.0 (c (n "gix-hash") (v "0.13.0") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "faster-hex") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0jmd44qhcg6r5nky02qnbc9ym44xzaisnqkw9amdv73p8dal5krc") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-hash-0.13.1 (c (n "gix-hash") (v "0.13.1") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "faster-hex") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0q6c3jjp6q17w7879lwi7r1xw2zr489yk75yq4bm51x03sscg10q") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-hash-0.13.2 (c (n "gix-hash") (v "0.13.2") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "faster-hex") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1jzk8cqiflbwlm9kgkgzc463kak1ww8p5qyjqxrfcnbpm1afbhcr") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-hash-0.13.3 (c (n "gix-hash") (v "0.13.3") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "faster-hex") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1q1xcp8f5prpyr4x62jixrlgm99snscnf87bny1faqvg4v1gi30z") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-hash-0.14.0 (c (n "gix-hash") (v "0.14.0") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "faster-hex") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0c0vzvl447f2vrw0zrz60rfpbwi3pkblmsyj7j1k2q3vl5q32b65") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.70")))

(define-public crate-gix-hash-0.14.1 (c (n "gix-hash") (v "0.14.1") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "faster-hex") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0lw64sr24z54bx6j746xqgiiv43pf0lc8w82r22ndqnwq76qkvdh") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-gix-hash-0.14.2 (c (n "gix-hash") (v "0.14.2") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "faster-hex") (r "^0.9.0") (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0pjdlxbqxd9lbkccryfw2ghifiq3gz9h8ylliw0va8b16vvpsggr") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

