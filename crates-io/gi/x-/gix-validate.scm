(define-module (crates-io gi x- gix-validate) #:use-module (crates-io))

(define-public crate-gix-validate-0.7.2 (c (n "gix-validate") (v "0.7.2") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0s1fds4inia9z1d4cc62d13dyf9g6q7rxigsilnx7b3axjsp1b3l") (r "1.64")))

(define-public crate-gix-validate-0.7.3 (c (n "gix-validate") (v "0.7.3") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0hz4qzsvji4a92j0smn9vdc1qdwqf1dxf638wram4im11rwdp7dn") (r "1.64")))

(define-public crate-gix-validate-0.7.4 (c (n "gix-validate") (v "0.7.4") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0jf0r2h9j39h2wai9nfab6sl1dwm8bypcpaqbrwf2wq7d39jkmkv") (r "1.64")))

(define-public crate-gix-validate-0.7.5 (c (n "gix-validate") (v "0.7.5") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0q1s790j7lzhs2nw69jv3mdgr9b9qdy25x49kn5p5iq6nm2misjp") (r "1.64")))

(define-public crate-gix-validate-0.7.6 (c (n "gix-validate") (v "0.7.6") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "04zcq0gg1lhyk7dnqjfq55pim9g87qv6slpy64x0mw4a9icjn2cd") (r "1.65")))

(define-public crate-gix-validate-0.7.7 (c (n "gix-validate") (v "0.7.7") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0h4hr3rpgwc7ixyynjp53s9il3sb0gq8ad332k8drwyfn8vkg6xs") (r "1.65")))

(define-public crate-gix-validate-0.8.0 (c (n "gix-validate") (v "0.8.0") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1idq44xvqjf2pxw9kxxjvi5cwklzc4wallp0arhqcnx40cmsnp70") (r "1.65")))

(define-public crate-gix-validate-0.8.1 (c (n "gix-validate") (v "0.8.1") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0gw84vj1ligj972xh8msvsy5s1pprjr6nzmw9cl9zrjb4zjdidvm") (r "1.65")))

(define-public crate-gix-validate-0.8.2 (c (n "gix-validate") (v "0.8.2") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1rv38bj4hkga5whbimzm868lv2lv14a6sgp45s7dkgx4payyn1gq") (r "1.70")))

(define-public crate-gix-validate-0.8.3 (c (n "gix-validate") (v "0.8.3") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0m1qi0xp59sa3540zdhdshjhfj44nsdjiyd0vindkmbb95pw6z5c") (r "1.65")))

(define-public crate-gix-validate-0.8.4 (c (n "gix-validate") (v "0.8.4") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0i8m4jv53yn8nhp2niifb03p6lcyj1sd8d6x36n5x624c3hcd7z3") (r "1.65")))

(define-public crate-gix-validate-0.8.5 (c (n "gix-validate") (v "0.8.5") (d (list (d (n "bstr") (r "^1.3.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1kqad8a2wdz69ma7hspi21pazgpkrc5hg4iw37gsvca99b9pvhl2") (r "1.65")))

