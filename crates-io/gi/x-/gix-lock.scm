(define-module (crates-io gi x- gix-lock) #:use-module (crates-io))

(define-public crate-gix-lock-3.0.2 (c (n "gix-lock") (v "3.0.2") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "gix-tempfile") (r "^3.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0pdmcb7c06dnc2h7yjxfvcbd44jlrdc0zn17fa5cgppskbq89zp5") (r "1.64")))

(define-public crate-gix-lock-4.0.0 (c (n "gix-lock") (v "4.0.0") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "gix-tempfile") (r "^4.0.0") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0vpnw77idz05w3hlqkw7al44z2szr25p3vrz0gmd15d3lkw9y4b6") (r "1.64")))

(define-public crate-gix-lock-5.0.0 (c (n "gix-lock") (v "5.0.0") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "gix-tempfile") (r "^5.0.0") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "152iy3256y69cl0xniqh4401qhlmgb6cbnldlhbq0paw0mr03f21") (r "1.64")))

(define-public crate-gix-lock-5.0.1 (c (n "gix-lock") (v "5.0.1") (d (list (d (n "gix-tempfile") (r "^5.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.1") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0m8m26w2c0pqf835zihhhi8r78yfqynm0wa6gi5af3vk0mzkss9c") (r "1.64")))

(define-public crate-gix-lock-6.0.0 (c (n "gix-lock") (v "6.0.0") (d (list (d (n "gix-tempfile") (r "^6.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.2") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1lpqi927lacn2vz22q2mhacc4plkrlz5whm779ax65kky3kdbi9y") (r "1.64")))

(define-public crate-gix-lock-7.0.0 (c (n "gix-lock") (v "7.0.0") (d (list (d (n "gix-tempfile") (r "^7.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.3") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "17r4m4d60vb1ak6wcfhkp7xwsp4ixxcl90xgmrn61aqksym513rj") (r "1.65")))

(define-public crate-gix-lock-7.0.1 (c (n "gix-lock") (v "7.0.1") (d (list (d (n "gix-tempfile") (r "^7.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.4") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1r18fw0fdjx9s9j5yfa5g2a1jav14m6mx73ff4rsr5brc89wnjvi") (r "1.65")))

(define-public crate-gix-lock-7.0.2 (c (n "gix-lock") (v "7.0.2") (d (list (d (n "gix-tempfile") (r "^7.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.5") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1ym6hp68bws6ww5byh4kkjgvb49v0qkd2gmz8h8gk0d2r0iyr0ky") (r "1.65")))

(define-public crate-gix-lock-8.0.0 (c (n "gix-lock") (v "8.0.0") (d (list (d (n "gix-tempfile") (r "^8.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.5") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1kp3y64pf0mnqysqhkspxiii74vnpzplxcvnnh31kcvp6l166hyy") (r "1.65")))

(define-public crate-gix-lock-9.0.0 (c (n "gix-lock") (v "9.0.0") (d (list (d (n "gix-tempfile") (r "^9.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.5") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0lpxn2nirn93pkx4bpf8hlp5fbccp1fmychgcx90vill0pcw6s0m") (r "1.65")))

(define-public crate-gix-lock-10.0.0 (c (n "gix-lock") (v "10.0.0") (d (list (d (n "gix-tempfile") (r "^10.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.5") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "15dazvw49bdx60366vngmrfn69rvxf0pr411a1ak6vbbigx9dz27") (r "1.65")))

(define-public crate-gix-lock-11.0.0 (c (n "gix-lock") (v "11.0.0") (d (list (d (n "gix-tempfile") (r "^11.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.5") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0dsg781jzslcf8vnbpij020b0hkasnfvmv92vi6kizh4sgfb3zpl") (r "1.65")))

(define-public crate-gix-lock-11.0.1 (c (n "gix-lock") (v "11.0.1") (d (list (d (n "gix-tempfile") (r "^11.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.6") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0drgl9qhkvlhjl0jc0lh2h7h3by1yg9wx4a8cqss8c4qlbk6ap3y") (r "1.65")))

(define-public crate-gix-lock-12.0.0 (c (n "gix-lock") (v "12.0.0") (d (list (d (n "gix-tempfile") (r "^12.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.7") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1nvl1ikgd9zqxvnwaninlfmw63bl4z8dld45k88kq8llxvfi5wbc") (r "1.70")))

(define-public crate-gix-lock-12.0.1 (c (n "gix-lock") (v "12.0.1") (d (list (d (n "gix-tempfile") (r "^12.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.8") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "001bh0yx1xnkhnbkfj3p3v649sp5hypm4pgq9jsk1qpijy9l62pl") (r "1.65")))

(define-public crate-gix-lock-13.0.0 (c (n "gix-lock") (v "13.0.0") (d (list (d (n "gix-tempfile") (r "^13.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.9") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1qgkzhqcqqlir4c8dmzb3ldniqvdgf9k37c0gf5x3ry59lblc7k5") (r "1.65")))

(define-public crate-gix-lock-13.1.0 (c (n "gix-lock") (v "13.1.0") (d (list (d (n "gix-tempfile") (r "^13.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.9") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0hlki84z2cp1h89aqn7pl6wwh75963a9jai6j6m36r0xgfh90r48") (r "1.65")))

(define-public crate-gix-lock-13.1.1 (c (n "gix-lock") (v "13.1.1") (d (list (d (n "gix-tempfile") (r "^13.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.11") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0jfh6i58rqq624w0cr20gf4a0zlvg0wwp6ricch3bf013zw5khz7") (r "1.65")))

(define-public crate-gix-lock-14.0.0 (c (n "gix-lock") (v "14.0.0") (d (list (d (n "gix-tempfile") (r "^14.0.0") (k 0)) (d (n "gix-utils") (r "^0.1.11") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "17g1sknpvjqaq2s29c693mbmkp8sign0174qfi3n3x7ijzi7zg73") (r "1.65")))

