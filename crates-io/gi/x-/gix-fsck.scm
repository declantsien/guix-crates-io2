(define-module (crates-io gi x- gix-fsck) #:use-module (crates-io))

(define-public crate-gix-fsck-0.1.0 (c (n "gix-fsck") (v "0.1.0") (d (list (d (n "gix-hash") (r "^0.13.2") (d #t) (k 0)) (d (n "gix-hashtable") (r "^0.4.1") (d #t) (k 0)) (d (n "gix-object") (r "^0.39.0") (d #t) (k 0)))) (h "01yhjr5qac1jp0ia894pjm9mpl4hbxanhq6wzpfbsqc8llmjhf13") (r "1.65")))

(define-public crate-gix-fsck-0.2.0 (c (n "gix-fsck") (v "0.2.0") (d (list (d (n "gix-hash") (r "^0.14.0") (d #t) (k 0)) (d (n "gix-hashtable") (r "^0.5.0") (d #t) (k 0)) (d (n "gix-object") (r "^0.40.0") (d #t) (k 0)))) (h "04f187m1jgzavynm70bqydrn851z4d4sdpifmqg11iyabvvz3qlk") (r "1.70")))

(define-public crate-gix-fsck-0.3.0 (c (n "gix-fsck") (v "0.3.0") (d (list (d (n "gix-hash") (r "^0.14.1") (d #t) (k 0)) (d (n "gix-hashtable") (r "^0.5.1") (d #t) (k 0)) (d (n "gix-object") (r "^0.41.1") (d #t) (k 0)))) (h "0zdvl2r0psa1cak9757a4innb9wsb5yrvv20d3nvqlwi97vlb9cw") (r "1.65")))

(define-public crate-gix-fsck-0.4.0 (c (n "gix-fsck") (v "0.4.0") (d (list (d (n "gix-hash") (r "^0.14.2") (d #t) (k 0)) (d (n "gix-hashtable") (r "^0.5.2") (d #t) (k 0)) (d (n "gix-object") (r "^0.42.1") (d #t) (k 0)))) (h "0ky7l753zr8ky0skr0265604hqyzxh32f0lgc2lcf88bh20s5vbw") (r "1.65")))

