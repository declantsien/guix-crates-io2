(define-module (crates-io gi tp gitpolicyenforcer) #:use-module (crates-io))

(define-public crate-GitPolicyEnforcer-0.2.0 (c (n "GitPolicyEnforcer") (v "0.2.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04vlmlx25cw4rlghsc04m9n0rkavncl604rgaza6yf6g1ljyrqgy")))

(define-public crate-GitPolicyEnforcer-0.3.0 (c (n "GitPolicyEnforcer") (v "0.3.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mmr8iq94m3k164bljszib5223pmqmlrjflxwbg04qw10zw7pnss")))

