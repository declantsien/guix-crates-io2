(define-module (crates-io gi tp gitprompt-rs) #:use-module (crates-io))

(define-public crate-gitprompt-rs-0.2.0 (c (n "gitprompt-rs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1045prlpc3zp3hvn22g55ywryjzhk7r0zb4cia68sn3s8khsipdf")))

(define-public crate-gitprompt-rs-0.3.0 (c (n "gitprompt-rs") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0mgr01jxszrxsca16i3rz1x0kih509y5lszk30pw387fbh0a5nr9")))

