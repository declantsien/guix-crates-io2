(define-module (crates-io gi tv gitversion) #:use-module (crates-io))

(define-public crate-gitversion-0.0.1 (c (n "gitversion") (v "0.0.1") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (d #t) (k 0)) (d (n "natord") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0wv6ycviv0w8pa71hlffx9ip2g5ls2ps8z90xbmwy6405gx0kzcz") (y #t)))

(define-public crate-gitversion-0.0.2 (c (n "gitversion") (v "0.0.2") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (d #t) (k 0)) (d (n "natord") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0qi7l7v3nb2hw56d9zvax5r2128jygs3p2c21dyg9whl65c7kwrf")))

(define-public crate-gitversion-0.0.3 (c (n "gitversion") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "natord") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0mf9pds1qm4hki1qvah7gznps9p9c9h05x8686gsc7swlldq6xdn")))

(define-public crate-gitversion-0.0.4 (c (n "gitversion") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "natord") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1g36d2f5y13cx8rjdmw0n47psyx0rk72a2pj71r98c5492qxnxh6")))

(define-public crate-gitversion-0.1.0 (c (n "gitversion") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "0mqcwk7iq0qmr819j38lyn59f13688rd0ngfw4mfky1hbzpzcd4w")))

(define-public crate-gitversion-0.1.1 (c (n "gitversion") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)))) (h "0xsv5c2pyaclqnwqg8fcvpm0jqxpxgxs47ngzq5dziv5yhfala1h")))

(define-public crate-gitversion-0.2.0 (c (n "gitversion") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)))) (h "1gcagcaazhp7mh4y9hik17hn9090c3rghrk16adbgmm1s67a1gmz")))

(define-public crate-gitversion-0.2.1 (c (n "gitversion") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)))) (h "0vv0js4g1y6m923fl1h97yiz6bzs86s0ymggwwchiqc43ldxaj16")))

