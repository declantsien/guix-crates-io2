(define-module (crates-io gi fs gifscii) #:use-module (crates-io))

(define-public crate-gifscii-1.0.0 (c (n "gifscii") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clio") (r "^0.3.5") (f (quote ("clap-parse" "http-ureq"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)))) (h "0g39xxphrfwvc0zyv53h84r740qk5bcpjazwyh70w8dbv3kmgpz5")))

