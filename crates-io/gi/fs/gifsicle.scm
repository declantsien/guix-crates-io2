(define-module (crates-io gi fs gifsicle) #:use-module (crates-io))

(define-public crate-gifsicle-1.92.0 (c (n "gifsicle") (v "1.92.0") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1i2y7710hvhw786mgcyijmg0zw366x7jx4g2wzgi824vcwdqq2s3") (y #t) (l "gifsicle")))

(define-public crate-gifsicle-1.92.1 (c (n "gifsicle") (v "1.92.1") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0p7rv5rbzdndvr7w5y8y4z0wh9fqr16gl4sbkcvg0yacyhvgjmar") (y #t) (l "gifsicle")))

(define-public crate-gifsicle-1.92.2 (c (n "gifsicle") (v "1.92.2") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1vhgag3hxqq3lmd0p3awd4kv3s9ipn2m8nn7qimwcxyqqlfvgl3f") (y #t) (l "gifsicle")))

(define-public crate-gifsicle-1.92.3 (c (n "gifsicle") (v "1.92.3") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0shxcvlb8k53ql3cxl28kwwgiw1vy5dqwc4xaqacdhx5xs17lj5m") (y #t) (l "gifsicle")))

(define-public crate-gifsicle-1.92.4 (c (n "gifsicle") (v "1.92.4") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1s0q4655kqdns0nl8pizbkynbznqbx8fvjinmjgfnr92y70343fr") (l "gifsicle")))

(define-public crate-gifsicle-1.92.5 (c (n "gifsicle") (v "1.92.5") (d (list (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "06iyzdckrf55vk28p4x22qi2xwga1642mj3lzn5nrlma2qiqm69n") (l "gifsicle")))

(define-public crate-gifsicle-1.93.0 (c (n "gifsicle") (v "1.93.0") (d (list (d (n "cc") (r "^1.0.76") (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "01xcmivmm36gw197ywpy6s18m8cwcq04vr9w6h9k6ahccywkab0d") (l "gifsicle")))

(define-public crate-gifsicle-1.94.0 (c (n "gifsicle") (v "1.94.0") (d (list (d (n "cc") (r "^1.0.76") (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "0vr1b7n77zmgd7kgvv3llmx9nxvasynnhp3qhd2g93n83dckkcjz") (l "gifsicle")))

