(define-module (crates-io gi g- gig-cli) #:use-module (crates-io))

(define-public crate-gig-cli-0.1.0 (c (n "gig-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.0") (f (quote ("json"))) (d #t) (k 0)))) (h "14qbjcb3llgjixpdxrlyhg0vpcjgycdrdrcys8ssnrvab82sgd1r")))

(define-public crate-gig-cli-0.1.1 (c (n "gig-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0kn3qlm03gz06cmx6jbad4g1nfchbcxm65lsqiwmscsg1p7cxjhp")))

(define-public crate-gig-cli-0.2.0 (c (n "gig-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.0") (f (quote ("json"))) (d #t) (k 0)))) (h "09a3qs3k1nkcxgkfg4y1yyf6fiph01g2d3a0hjiwi4zl65g4v5f7")))

