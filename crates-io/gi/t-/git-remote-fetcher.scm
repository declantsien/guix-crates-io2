(define-module (crates-io gi t- git-remote-fetcher) #:use-module (crates-io))

(define-public crate-git-remote-fetcher-0.1.0 (c (n "git-remote-fetcher") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)))) (h "1ycgy8j8d4dygmavvz1scpfb07zk42czaqjm6c7dmqsbhimhaz3d")))

