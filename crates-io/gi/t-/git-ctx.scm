(define-module (crates-io gi t- git-ctx) #:use-module (crates-io))

(define-public crate-git-ctx-0.1.0 (c (n "git-ctx") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1rrzvm7978iw6kw6myddpwdqkqwh721nnvni39wlvxpv2c6ysqig")))

(define-public crate-git-ctx-0.1.1 (c (n "git-ctx") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1hkijarfpxq94q61w4lcii1ksvnx8zwjaifaacxrgb1gjbys4gxv")))

