(define-module (crates-io gi t- git-branchless-opts) #:use-module (crates-io))

(define-public crate-git-branchless-opts-0.7.0-rc.1 (c (n "git-branchless-opts") (v "0.7.0-rc.1") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lib") (r "^0.7.0-rc.1") (d #t) (k 0) (p "git-branchless-lib")))) (h "1148k3f4r03ya33vqj25nc5gj6mf0hs5iby6rdvdwl54n5357syz")))

(define-public crate-git-branchless-opts-0.7.0 (c (n "git-branchless-opts") (v "0.7.0") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lib") (r "^0.7.0") (d #t) (k 0) (p "git-branchless-lib")))) (h "1bagpg3cbin2nksvbv9fcs3zrmn5pvzksscnyayag8dxwyhqz26l")))

(define-public crate-git-branchless-opts-0.8.0 (c (n "git-branchless-opts") (v "0.8.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "lib") (r "^0.8.0") (d #t) (k 0) (p "git-branchless-lib")) (d (n "scm-record") (r "^0.1.0") (f (quote ("scm-diff-editor"))) (d #t) (k 0)))) (h "0v3g278f4ryl5q24hq501wqr56igax343hvr4gxfjj3n24j7biln")))

(define-public crate-git-branchless-opts-0.9.0 (c (n "git-branchless-opts") (v "0.9.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "lib") (r "^0.9.0") (d #t) (k 0) (p "git-branchless-lib")) (d (n "scm-record") (r "^0.3.0") (f (quote ("scm-diff-editor"))) (d #t) (k 0)))) (h "0bv4bl3j5dx7gjhy4s73ma5pz71im8syxg4nfblbp0x61wyr5wna")))

