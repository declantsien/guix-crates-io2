(define-module (crates-io gi t- git-checks-config) #:use-module (crates-io))

(define-public crate-git-checks-config-0.1.0 (c (n "git-checks-config") (v "0.1.0") (d (list (d (n "erased-serde") (r "~0.3") (d #t) (k 0)) (d (n "git-checks-core") (r "^1.0") (d #t) (k 0)) (d (n "inventory") (r "~0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1xxjpvgmfnigfym5rz7hmvc937qkxn96lq0ib1a9dg21bi2fmh1s")))

(define-public crate-git-checks-config-0.2.0 (c (n "git-checks-config") (v "0.2.0") (d (list (d (n "erased-serde") (r "~0.3") (d #t) (k 0)) (d (n "git-checks-core") (r "^1.2") (d #t) (k 0)) (d (n "inventory") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.25") (d #t) (k 2)))) (h "0nzwqir4f2qwdsmxh8ipns5hh1x3ncqb0886w5ggr88ybk4hjd8c")))

(define-public crate-git-checks-config-0.2.1 (c (n "git-checks-config") (v "0.2.1") (d (list (d (n "erased-serde") (r "~0.3") (d #t) (k 0)) (d (n "git-checks-core") (r "^1.2") (d #t) (k 0)) (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.25") (d #t) (k 2)))) (h "021qmx611z0m1fq0m0jgvwi7l4m2mxz4vwpanrdm57041a79min0")))

(define-public crate-git-checks-config-0.2.2 (c (n "git-checks-config") (v "0.2.2") (d (list (d (n "erased-serde") (r "~0.3") (d #t) (k 0)) (d (n "git-checks-core") (r "^1.2") (d #t) (k 0)) (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.25") (d #t) (k 2)))) (h "1q88sw2g0nw8xpiwqrpj06cmm5ljcjjmqjq0839fh7h355x93d5b")))

