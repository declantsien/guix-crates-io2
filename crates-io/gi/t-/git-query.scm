(define-module (crates-io gi t- git-query) #:use-module (crates-io))

(define-public crate-git-query-0.1.0 (c (n "git-query") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0bp4zlzkf0p0p6q0mpwfpd6z55j7faml67903ycpw89xv5gfy12w")))

(define-public crate-git-query-0.2.0 (c (n "git-query") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "11v29aj26jaaf2gj9adgyn6vawlz20vn458d943dyi1485y3faxi")))

(define-public crate-git-query-0.3.0 (c (n "git-query") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "005v0214kvz3q7bbhc9v88aijlny2gcxkaxxpl0pznfsfd7vqff6")))

