(define-module (crates-io gi t- git-sha1) #:use-module (crates-io))

(define-public crate-git-sha1-1.0.1 (c (n "git-sha1") (v "1.0.1") (h "06k5xancw3vc0jdggy678161lvmibbf6vnkqg9z1w10bws0vxcri")))

(define-public crate-git-sha1-1.1.0 (c (n "git-sha1") (v "1.1.0") (h "1ll927m8mfvgswzci3lwvp4yxlcay5ag10l4w2mpq8j638gw8wnp")))

