(define-module (crates-io gi t- git-bump) #:use-module (crates-io))

(define-public crate-git-bump-0.1.0 (c (n "git-bump") (v "0.1.0") (d (list (d (n "git2") (r "^0.14.2") (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "mlua") (r "^0.7") (f (quote ("lua54" "vendored"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0bq2crckjir4c54fygfv17j0wdrbixqb8i9nzpjv3jbwmyahrfms")))

(define-public crate-git-bump-0.2.0 (c (n "git-bump") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "mlua") (r "^0.7") (f (quote ("lua54" "vendored"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0c9d0b654wiahl6dz6i884a58749h0rsvf411ppdzipfjdwdls5i")))

