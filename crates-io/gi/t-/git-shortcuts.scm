(define-module (crates-io gi t- git-shortcuts) #:use-module (crates-io))

(define-public crate-git-shortcuts-0.1.0-rc0 (c (n "git-shortcuts") (v "0.1.0-rc0") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g8fmznd5ms5x7v4ypc9srgrgnhgziyv5lrmsbn87c8pjin8gcmf")))

(define-public crate-git-shortcuts-0.1.1-rc0 (c (n "git-shortcuts") (v "0.1.1-rc0") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lpbpzfryd3gnv49sa7mlaznkikfzfc1pdl57dykw6hxycd5z133")))

