(define-module (crates-io gi t- git-snip) #:use-module (crates-io))

(define-public crate-git-snip-0.1.0 (c (n "git-snip") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "1zdiif1hjviw529s4x88dcpqyazwz3pr827djdlkz2rvchncn1lb")))

