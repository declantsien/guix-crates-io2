(define-module (crates-io gi t- git-remote-tor) #:use-module (crates-io))

(define-public crate-git-remote-tor-0.1.0 (c (n "git-remote-tor") (v "0.1.0") (h "026blzxbvhppzn92ixn7h3j1sgmwgg8v17adx5zkkwy4jlly04lm")))

(define-public crate-git-remote-tor-0.1.1 (c (n "git-remote-tor") (v "0.1.1") (h "17zfpyqzkvpi9j625hj1lqjfl5y5wsv2iw21krw3pqazlvakw4yg")))

(define-public crate-git-remote-tor-0.1.2 (c (n "git-remote-tor") (v "0.1.2") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 0)))) (h "0f25nfvx7aj1i7cclhn8gh4413kir0kznq9c861warb6pd7swsx4")))

(define-public crate-git-remote-tor-0.1.3 (c (n "git-remote-tor") (v "0.1.3") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 0)))) (h "0npjngaygfjcfsyx2mhigr96n24fr9lgdv2nv478gy9f2rj9lin2")))

(define-public crate-git-remote-tor-0.1.4 (c (n "git-remote-tor") (v "0.1.4") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 0)))) (h "146nyd5ww073iim48r71knfwnldq635xv732h3kl4ycsh2ki3ycx")))

