(define-module (crates-io gi t- git-gone) #:use-module (crates-io))

(define-public crate-git-gone-0.1.0 (c (n "git-gone") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.7") (d #t) (k 0)))) (h "1lcj7xidjndlys75y1s269za1dzpxha4nf846gvd8wamzdcsb4z9") (y #t)))

(define-public crate-git-gone-0.1.1 (c (n "git-gone") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.7") (d #t) (k 0)))) (h "1lvaqsydfmr1ywgizvmz1b9k6j9z42vqy1iybgq1hv21zk9kb08c") (y #t)))

(define-public crate-git-gone-0.1.2 (c (n "git-gone") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)))) (h "0apn656k3plrrcm0r0qq4ppwn98np1zlisn53ncvay1906wr1vzj") (y #t)))

(define-public crate-git-gone-0.2.0 (c (n "git-gone") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.10.2") (d #t) (k 0)))) (h "013l2rcp32afgfbldvqjvmbakgfv3b5c91s23fipaqcyd026bqaf") (y #t)))

(define-public crate-git-gone-0.3.0 (c (n "git-gone") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.10.2") (d #t) (k 0)))) (h "05c3psih307s75czx30rf38jbqanmzqk99jxpq2x88z2j0311d2i") (y #t)))

(define-public crate-git-gone-0.3.1 (c (n "git-gone") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "0q7m7rnq4l9bpr7k3lf7c4p3i5zskqcqc8n7jnv475332bibhpfw") (y #t)))

(define-public crate-git-gone-0.3.2 (c (n "git-gone") (v "0.3.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "1935b97q5c6bjn880s8h7w1l2jyi2i92pp6hiwzxq4x692jd7xm6") (y #t)))

(define-public crate-git-gone-0.3.3 (c (n "git-gone") (v "0.3.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)))) (h "1rq4j38fwy22vab6fmy9kq9pp3fnsyghx7j3phw6f8z1aia45l00") (y #t)))

(define-public crate-git-gone-0.3.4 (c (n "git-gone") (v "0.3.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)))) (h "1g6w2sazpmyn790jywwqrcxmb9hzgvywfg9rbsmf1gfp2khbk7ww") (y #t)))

(define-public crate-git-gone-0.3.5 (c (n "git-gone") (v "0.3.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)))) (h "06550zgbnf37dr39308xnvms8y9p83wymz90gqzqk9jmgvy0bi08") (y #t)))

(define-public crate-git-gone-0.3.6 (c (n "git-gone") (v "0.3.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)))) (h "0x77bnaqiwpd9ar60blsl7hv2ixc3ljy1s2w9vc2g29wy3q352a7") (y #t)))

(define-public crate-git-gone-0.3.7 (c (n "git-gone") (v "0.3.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)))) (h "0pm43sics2sv73br32sb08gcwjy3qr81hj2m162paplxfy44i6lw") (y #t)))

(define-public crate-git-gone-0.3.8 (c (n "git-gone") (v "0.3.8") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)))) (h "19l7qi7yz4pn1dmhmd92zcwkmcsy449lq2d6d6d352k6d84mi4zb") (y #t)))

(define-public crate-git-gone-0.4.0 (c (n "git-gone") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("std" "cargo" "deprecated"))) (k 0)) (d (n "git2") (r "^0.15.0") (k 0)))) (h "1bsgbbfqypgmrnbp6f85xqhknf8mw5qrispqvm62ql1ndaf0c1i9") (y #t)))

(define-public crate-git-gone-0.4.1 (c (n "git-gone") (v "0.4.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("std" "cargo" "deprecated"))) (k 0)) (d (n "git2") (r "^0.15.0") (k 0)))) (h "1anxdvj978ahqj8djvyi2y90xb2f8v3fdl2fpxgy0s6i8iy3kqa0") (y #t)))

(define-public crate-git-gone-0.4.2 (c (n "git-gone") (v "0.4.2") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (k 0)))) (h "0i5n0kv4lxdkxncrraiixijz5xxj2w6mxfwfwb4z0bw4pbpapx7c") (y #t)))

(define-public crate-git-gone-0.4.3 (c (n "git-gone") (v "0.4.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("std" "help" "usage" "error-context" "derive"))) (k 0)) (d (n "git2") (r "^0.15.0") (k 0)))) (h "0pw7jfymqxmygpc5pg72szjbqycbzqwjknq9zb86dp10lmsk8vjf") (y #t)))

(define-public crate-git-gone-0.5.0 (c (n "git-gone") (v "0.5.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("std" "help" "usage" "error-context" "derive"))) (k 0)) (d (n "git2") (r "^0.17.0") (k 0)))) (h "1ga93c152nbhm9d7jzflglg53s949nvav0cznll74gvwc7fkkga3") (y #t)))

(define-public crate-git-gone-1.0.0 (c (n "git-gone") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("std" "help" "usage" "error-context" "derive"))) (k 0)) (d (n "git2") (r "^0.17.0") (k 0)))) (h "0g953129q0k9zlpbjsfy6spr3xkvssknixqi50dj4xk8m0dzs132") (y #t)))

(define-public crate-git-gone-1.1.0 (c (n "git-gone") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("std" "help" "usage" "error-context" "derive"))) (k 0)) (d (n "git2") (r "^0.18.2") (k 0)))) (h "1c071ixlx0pal848p75qcjrpws79w15bka6lin1sd1qi5bjp9dj3") (y #t)))

