(define-module (crates-io gi t- git-open) #:use-module (crates-io))

(define-public crate-git-open-0.1.0 (c (n "git-open") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "10fxxk6zdfslinxgngyca1cb3g2g7mqyhbz6sr756xwla11xd39d")))

(define-public crate-git-open-0.1.1 (c (n "git-open") (v "0.1.1") (d (list (d (n "rust-ini") (r "^0.16") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0xya46zgy1yifzga9y1hlpkxs7xwyiqm720ynkibac1pqg646fal")))

