(define-module (crates-io gi t- git-branch-deleter) #:use-module (crates-io))

(define-public crate-git-branch-deleter-0.1.0 (c (n "git-branch-deleter") (v "0.1.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0gbwmg8im6rgk4hlvyp7lyqcll62xhpyd1zhsizl0bp4ldmphklf")))

(define-public crate-git-branch-deleter-0.1.1 (c (n "git-branch-deleter") (v "0.1.1") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1rwxcmf4bhmk2559qcxcrfff5mswzvh91ny6l8k1j03cmf3zhf4k")))

(define-public crate-git-branch-deleter-0.1.2 (c (n "git-branch-deleter") (v "0.1.2") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0d1dk7g98da4b4gs044pzjlsam410n9y8jjcs3cgihhl086w9b1v")))

(define-public crate-git-branch-deleter-0.1.3 (c (n "git-branch-deleter") (v "0.1.3") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0cj5jc73nix1mpyv69sljp4wvgl6ddg2ffyfa62svz87wkzrk1bn")))

(define-public crate-git-branch-deleter-0.1.4 (c (n "git-branch-deleter") (v "0.1.4") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0hcq1v30ic8zjg84m84vmd4pwxccsbv7qfylwpw3wij1f82ywi3j")))

(define-public crate-git-branch-deleter-0.1.5 (c (n "git-branch-deleter") (v "0.1.5") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0pv3ywprn2grziidpay73l7xr3ilqgwdvvy1bg6bl5wkf0nnws04")))

(define-public crate-git-branch-deleter-0.1.6 (c (n "git-branch-deleter") (v "0.1.6") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "17vqyyid5fb50qisi2pm86v2sihj9kkxlkypsjlhnzij8j0f8v0y")))

(define-public crate-git-branch-deleter-0.1.7 (c (n "git-branch-deleter") (v "0.1.7") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1ma0am62dxq5rw9pzybd9gqgqv1m04w2k22zss6lkgqfrrdy80nq")))

(define-public crate-git-branch-deleter-0.1.8 (c (n "git-branch-deleter") (v "0.1.8") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "0pch69b0b42f5inlc25kn8f2ipddxbvk3ksp7g6qa8ndkpdnfhsj")))

(define-public crate-git-branch-deleter-0.1.9 (c (n "git-branch-deleter") (v "0.1.9") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "14pg7ykq1mm2p2f13m7lqxs7ml1fl4mpyjf624zd5p862mkclsx5")))

(define-public crate-git-branch-deleter-0.1.10 (c (n "git-branch-deleter") (v "0.1.10") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1mfp8z8n1bknaz9yi8zw60372ccncg6r4p11b5mrmfliqhpps2q7")))

(define-public crate-git-branch-deleter-0.1.11 (c (n "git-branch-deleter") (v "0.1.11") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1p6h23gn1fn1p2fi607aqai4zaibvdza2vffvp72hn8wpvlbdwgg")))

