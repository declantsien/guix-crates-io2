(define-module (crates-io gi t- git-validate) #:use-module (crates-io))

(define-public crate-git-validate-0.1.0 (c (n "git-validate") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "git-testtools") (r "^0.1") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1lsq3vj5p9nf0pv9v8x3ziw2apfjxr6lvzz7lc71x01b9klhrj2h")))

(define-public crate-git-validate-0.4.0 (c (n "git-validate") (v "0.4.0") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "git-testtools") (r "^0.3") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "02qwxvy2zwpvqg3p9h9fqh3vsza0n6id9r92cv3zdb7yzfyb9x38")))

(define-public crate-git-validate-0.5.0 (c (n "git-validate") (v "0.5.0") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "git-testtools") (r "^0.4") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0j27qy93bf1h56ypcsa9gvs7my5ngmwq071h5qmg4sqzx346xpz9")))

(define-public crate-git-validate-0.5.1 (c (n "git-validate") (v "0.5.1") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0dl89pingj2cijbsspq3pkrfilxmkis0q0ypncxj6y9f4cwpxn3l")))

(define-public crate-git-validate-0.5.2 (c (n "git-validate") (v "0.5.2") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1jpcm2p6rv72fq4py1wd5zh84j26r0zkmhagv22vj9yzpqv22a3b")))

(define-public crate-git-validate-0.5.3 (c (n "git-validate") (v "0.5.3") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1p0130i11ndnqr24qmh3gyscsqnxh8qq4ivwk6amlibgrp3az2y5")))

(define-public crate-git-validate-0.5.4 (c (n "git-validate") (v "0.5.4") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1ym84rhv5fypgryzg82nn7k2d7p72l3prwi0md4krss2m6m1wb1c")))

(define-public crate-git-validate-0.5.5 (c (n "git-validate") (v "0.5.5") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "01z15znlnmsmm27xrwxv4w68b1fxghdmj90qyw71y0g6vwx4bwbs")))

(define-public crate-git-validate-0.6.0 (c (n "git-validate") (v "0.6.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0gcc5h8mp0wbg2v9l6nikxqvqgp2m6bix9vlvpx8v0yyl1m9shxm")))

(define-public crate-git-validate-0.7.0 (c (n "git-validate") (v "0.7.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "11l6nkmmwb8aby07c54bz6vq742d6s3vkgwfjg569h1gcfp3py6d")))

(define-public crate-git-validate-0.7.1 (c (n "git-validate") (v "0.7.1") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1hpkkw0zcqzppbjngiy3hqyfcm6fa7jnw1n9irydr5n5aa9wyc84")))

(define-public crate-git-validate-0.7.2 (c (n "git-validate") (v "0.7.2") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "03bm1hhckdmlp1hi0967gv8zyx1d00zdj69x6hr5s8iy64h4061r") (r "1.64")))

(define-public crate-git-validate-0.7.3 (c (n "git-validate") (v "0.7.3") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "12rziqag9v44vpl4b703dyfqpzijgjip4ldyqsqsd04qavs9a09r") (r "1.64")))

