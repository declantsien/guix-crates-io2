(define-module (crates-io gi t- git-checkout-interactive) #:use-module (crates-io))

(define-public crate-git-checkout-interactive-1.0.2 (c (n "git-checkout-interactive") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (k 0)))) (h "1lamil4hz6375lbmsnzj6h190lgginr3r86amqfjza68ldvmi6yp")))

(define-public crate-git-checkout-interactive-1.0.3 (c (n "git-checkout-interactive") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (k 0)))) (h "10xycih8cai73wnqc5m9y028gam3fm25n9x1q0i2yd688i5i8kvq")))

(define-public crate-git-checkout-interactive-1.0.4 (c (n "git-checkout-interactive") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (k 0)))) (h "03n921x4mnmcnvdfdxkvm60dgazky793lh2x8xgvcfmf4gxfzwj8")))

