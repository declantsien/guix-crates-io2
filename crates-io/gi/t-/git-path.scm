(define-module (crates-io gi t- git-path) #:use-module (crates-io))

(define-public crate-git-path-0.1.0 (c (n "git-path") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2.17") (f (quote ("std"))) (k 0)))) (h "1fgydv3srmj381nkfk27y8g9w7j0w99889yk8z1p8xg63a11jq49")))

(define-public crate-git-path-0.1.1 (c (n "git-path") (v "0.1.1") (d (list (d (n "bstr") (r "^0.2.17") (f (quote ("std"))) (k 0)))) (h "0rkb9piw58vd02hd5ya23zikxpbbvaa6si7m4jj2lpvyv4078mgj")))

(define-public crate-git-path-0.1.2 (c (n "git-path") (v "0.1.2") (d (list (d (n "bstr") (r "^0.2.17") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0ly786q3hcijm8sy0x1p6n56a0bdkgcgbwdh95glkhqhrf9r30kl")))

(define-public crate-git-path-0.1.3 (c (n "git-path") (v "0.1.3") (d (list (d (n "bstr") (r "^0.2.17") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "15dyxh8qvxpa31q1017k9pmfpsr8iifpkrxjsv29nd2x0gwngz1h")))

(define-public crate-git-path-0.2.0 (c (n "git-path") (v "0.2.0") (d (list (d (n "bstr") (r "^0.2.17") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "11v4gcjzxnwkzbsyicl3dh7gbyq5d37kfry618ffpfmarqi8bbmj")))

(define-public crate-git-path-0.3.0 (c (n "git-path") (v "0.3.0") (d (list (d (n "bstr") (r "^0.2.17") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0mi3rgh53v9xcs68jzy45snfc9s6a7h9zlgj4d62ma83pyqlhalv")))

(define-public crate-git-path-0.4.0 (c (n "git-path") (v "0.4.0") (d (list (d (n "bstr") (r "^0.2.17") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "13vf5kxg83jzz3wxxk3spm54a0by5y7y997h9rzmaggpz594f1bg")))

(define-public crate-git-path-0.4.1 (c (n "git-path") (v "0.4.1") (d (list (d (n "bstr") (r "^0.2.17") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0h7agn33rs2rdqzjwcpcgrw53c27nid14shcf1grfhnpjxhbk5gb")))

(define-public crate-git-path-0.4.2 (c (n "git-path") (v "0.4.2") (d (list (d (n "bstr") (r "^0.2.17") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "081lbqk2jmyl3yj4zkav9mfp1pxl1dyaf4kqnhlls7gmsrbkcp6h")))

(define-public crate-git-path-0.5.0 (c (n "git-path") (v "0.5.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0wi5h65g7svw6v6i9hsyc0ix614m9phbgr5xqpk17glh4q1c2pa2")))

(define-public crate-git-path-0.6.0 (c (n "git-path") (v "0.6.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1jgqci56n6ldycdlp2cjcw8m6a3y34vh9f7lymfxkzf07g0wnq2z")))

(define-public crate-git-path-0.7.0 (c (n "git-path") (v "0.7.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "12l7vgf40lc45s37l09mnpplxhirpsj8dzad7wyj9nh63946h3p4")))

(define-public crate-git-path-0.7.1 (c (n "git-path") (v "0.7.1") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1740pn31v2kxghwpaq2jhvd898s00w018p7iwhwn5znx8padqhdk") (r "1.64")))

(define-public crate-git-path-0.7.2 (c (n "git-path") (v "0.7.2") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0z1rvhhjndxw23ksf1b5kw11b6fkgkv9dihrw2as6pxh6rv3v023") (r "1.64")))

