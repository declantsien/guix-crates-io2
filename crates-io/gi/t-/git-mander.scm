(define-module (crates-io gi t- git-mander) #:use-module (crates-io))

(define-public crate-git-mander-0.1.0 (c (n "git-mander") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)))) (h "0rjf6nfva2n78y0j6ryql570rva9dcnkvz38f6wb5801lx5ahz6m") (y #t)))

(define-public crate-git-mander-0.1.1 (c (n "git-mander") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0v5xa1hjl0pixmjqcgxs3fr8lz5dn6mcs4x60b2grhih68g36hdk")))

