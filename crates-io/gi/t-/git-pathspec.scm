(define-module (crates-io gi t- git-pathspec) #:use-module (crates-io))

(define-public crate-git-pathspec-0.0.0 (c (n "git-pathspec") (v "0.0.0") (h "08h79c6slck0r2a5y51vsbq323r1vfxbylflrybkf0z9pgnyjr0d")))

(define-public crate-git-pathspec-0.1.0 (c (n "git-pathspec") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-attributes") (r "^0.8.3") (d #t) (k 0)) (d (n "git-glob") (r "^0.5.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1zzgjljlz79h1ws381qrgn33ppmlkvv13h77ma0b790pi3fcz2cs") (r "1.64")))

