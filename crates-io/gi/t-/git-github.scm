(define-module (crates-io gi t- git-github) #:use-module (crates-io))

(define-public crate-git-github-0.1.0 (c (n "git-github") (v "0.1.0") (h "03a7rvh15y5plh963gckwwrrzg5s6jcl4h2b756i1ngpd16balxr") (y #t)))

(define-public crate-git-github-0.1.1 (c (n "git-github") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (f (quote ("openssl-sys" "https"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)))) (h "1mp4szpfs336xw5i3dmlg6a4pkf738h8j00y5xky8hrfnl467gax")))

(define-public crate-git-github-0.1.2 (c (n "git-github") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (f (quote ("openssl-sys" "https"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "071b82qnc7s23pi5w8rj7gs2v8b5n1yx7dmw3rm29zyc60yglqrd")))

(define-public crate-git-github-0.1.3 (c (n "git-github") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (f (quote ("openssl-sys" "https"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0xh7sm9gsd7rax47lq1yvcndkiazfkvz1lp2l6qpjsjnvpmfar3n")))

