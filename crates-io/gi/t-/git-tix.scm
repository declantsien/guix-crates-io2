(define-module (crates-io gi t- git-tix) #:use-module (crates-io))

(define-public crate-git-tix-0.0.0 (c (n "git-tix") (v "0.0.0") (h "0f11m8yrgix7rfwn4rvrv26b2nk0n0sgbahmliard9pprl154gfq")))

(define-public crate-git-tix-0.1.0 (c (n "git-tix") (v "0.1.0") (h "09g9kvw9kskmmgcmibz17flqmy421xy6vkzyf3af4rsbvljv1vzx") (r "1.64")))

