(define-module (crates-io gi t- git-meta) #:use-module (crates-io))

(define-public crate-git-meta-0.0.1 (c (n "git-meta") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)))) (h "0kysb01f35ckmhxisa68p7335kfcc4shzp9whzissqjb52avfljv")))

(define-public crate-git-meta-0.1.0 (c (n "git-meta") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)))) (h "1b9d2mha5gplj25mj1vpjsrz418m8aznn5g2hbg3swlkzlpkvqav")))

(define-public crate-git-meta-0.2.0 (c (n "git-meta") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)))) (h "0fvvakyici6xy6s6yghbdcgg1a6w6n44j6r31x8dsc5azzpww301")))

(define-public crate-git-meta-0.2.1 (c (n "git-meta") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)))) (h "05153i2fglixykrv6nnpglrkkr102y25ab8d18rryh0aglfpgdik")))

(define-public crate-git-meta-0.3.0 (c (n "git-meta") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)))) (h "0y67inwwrsw2w2j5ws5nk2dgdic1vh2395b35780f223wbnb3hpw")))

(define-public crate-git-meta-0.3.1 (c (n "git-meta") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)))) (h "1qh3kga0zxdkyd6q15jf9z9r1qi8z41vjlpy58vlwv6z99vilbxv")))

(define-public crate-git-meta-0.4.0 (c (n "git-meta") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)))) (h "0bwiasz0rfgq8l27bszwajkhi1lvqr8l3jsilf66ybbs9hsywxz0")))

(define-public crate-git-meta-0.5.0 (c (n "git-meta") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0kvhpil302zm8nz3vhgg4dcqfh26fzld6f5bkm2pj01npn626qq5")))

(define-public crate-git-meta-0.6.0 (c (n "git-meta") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0xshg4cqsz9inzqczg40mx3i7yxmman2xhqy71a7459njq208xf8")))

