(define-module (crates-io gi t- git-filter) #:use-module (crates-io))

(define-public crate-git-filter-0.0.0 (c (n "git-filter") (v "0.0.0") (h "0ah77k00ldarw8dnk1q9h4nfqamlamqmv30ij2953lbka0v8dhyv")))

(define-public crate-git-filter-0.1.0 (c (n "git-filter") (v "0.1.0") (h "0cizn3mlilwy2yhxxrr4mdxy266v41cjkvhgzzfx449ynyjb406s") (r "1.64")))

