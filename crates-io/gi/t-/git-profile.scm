(define-module (crates-io gi t- git-profile) #:use-module (crates-io))

(define-public crate-git-profile-0.1.0 (c (n "git-profile") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "ramhorns") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1jla2qbh8yv55p0d7ii5azwqjj6v68j9vw0s3nsvdh60jw5w7y4y")))

