(define-module (crates-io gi t- git-conventional-commits) #:use-module (crates-io))

(define-public crate-git-conventional-commits-0.1.0 (c (n "git-conventional-commits") (v "0.1.0") (d (list (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.0") (d #t) (k 0)))) (h "1j3s5316wrlqkld6ni7n1g5hfa36z37kaddwxlzby0nf546l3l79")))

