(define-module (crates-io gi t- git-tidy) #:use-module (crates-io))

(define-public crate-git-tidy-2.0.0 (c (n "git-tidy") (v "2.0.0") (d (list (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1c020jq3cy6amdgs3lzn00700lwqlspw8j7jc6xl8fc1x3awpnsr")))

(define-public crate-git-tidy-2.0.1 (c (n "git-tidy") (v "2.0.1") (d (list (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dxa5n4idqd98r6460ps14m6swknhwan7l38pw0dw03mizpw30aw")))

