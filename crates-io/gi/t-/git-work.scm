(define-module (crates-io gi t- git-work) #:use-module (crates-io))

(define-public crate-git-work-0.1.0 (c (n "git-work") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.39") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (k 0)) (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.2") (d #t) (k 1)) (d (n "quote") (r "^1.0.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 1)))) (h "1j7p32j764wbj56xdnwpdfckn5liyq5j66vd3s0kl63ff1gbig2v")))

