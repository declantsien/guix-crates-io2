(define-module (crates-io gi t- git-traverse) #:use-module (crates-io))

(define-public crate-git-traverse-0.0.0 (c (n "git-traverse") (v "0.0.0") (d (list (d (n "git-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "git-object") (r "^0.8") (d #t) (k 0)) (d (n "git-odb") (r "^0.12") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1k20ywzk6vnlb2wac1i0sj16jfchxxmz9viz16zmhkl9c8hzmjdj")))

(define-public crate-git-traverse-0.1.0 (c (n "git-traverse") (v "0.1.0") (d (list (d (n "git-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "git-object") (r "^0.8") (d #t) (k 0)) (d (n "git-odb") (r "^0.12") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0bpzzm7ni4ay6fmajaxq5g4dvzl9daldvgr07ggs7bfa1kz5zvbj")))

(define-public crate-git-traverse-0.2.0 (c (n "git-traverse") (v "0.2.0") (d (list (d (n "git-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "git-object") (r "^0.9") (d #t) (k 0)) (d (n "git-odb") (r "^0.15") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "07ksv6dnk3fpc8pv7wsmaihq0708v60d389d5ynqzz3p390irwih")))

(define-public crate-git-traverse-0.3.0 (c (n "git-traverse") (v "0.3.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.10") (d #t) (k 0)) (d (n "git-odb") (r "^0.16") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0adj53dxdxjkml5c935hrxl146jsm5qi82nqbafblcc1pndf0qc0")))

(define-public crate-git-traverse-0.3.1 (c (n "git-traverse") (v "0.3.1") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.10") (d #t) (k 0)) (d (n "git-odb") (r "^0.16") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0aymy76nznccsy0rfvv6nfb7hm8p0jd2hwb7f19pmvqa3169ildh")))

(define-public crate-git-traverse-0.4.0 (c (n "git-traverse") (v "0.4.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.11") (d #t) (k 0)) (d (n "git-odb") (r "^0.16") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "038szqak3bqlp08bdmrb1ii596dk0gkcvvmzvjlgg2irfhiqcpzv")))

(define-public crate-git-traverse-0.5.0 (c (n "git-traverse") (v "0.5.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.11") (d #t) (k 0)) (d (n "git-odb") (r "^0.17") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0lfns2mqak5q00k4l3lc2sb5z79x38bbn6slmsk64y0c57fkb85m")))

(define-public crate-git-traverse-0.7.0 (c (n "git-traverse") (v "0.7.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.12.0") (d #t) (k 0)) (d (n "git-odb") (r "^0.20.0") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0in54hmivljwdai2wlfx3fbdwky310w5vlisv9v80vl2km1xacwl")))

(define-public crate-git-traverse-0.7.1 (c (n "git-traverse") (v "0.7.1") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1m3cd7cq8q8l01n193ryjjpi15458jbjpycg33imvjdv44gg5nwl")))

(define-public crate-git-traverse-0.7.2 (c (n "git-traverse") (v "0.7.2") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "02q2qsyvmjxxpai6qqjj2nais2siya3f3453hv4aglh9xk8ry01q")))

(define-public crate-git-traverse-0.8.0 (c (n "git-traverse") (v "0.8.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.13.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0fgrckpzy929asniljjxll9zr2gwhv4h0wbgxflq6i8l3bmrdx9w")))

(define-public crate-git-traverse-0.8.1 (c (n "git-traverse") (v "0.8.1") (d (list (d (n "git-hash") (r "^0.6.0") (d #t) (k 0)) (d (n "git-object") (r "^0.13.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1ky9i3x2z2mjd5lzryaavw6y90qgl7jifizb1n4187lfgy79gkcf")))

(define-public crate-git-traverse-0.8.2 (c (n "git-traverse") (v "0.8.2") (d (list (d (n "git-hash") (r "^0.6.0") (d #t) (k 0)) (d (n "git-object") (r "^0.14.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1a6k80qbaz2yxpsaz502bk02i57wl14xh8yymhj2bybg3yv4a42p")))

(define-public crate-git-traverse-0.9.0 (c (n "git-traverse") (v "0.9.0") (d (list (d (n "git-hash") (r "^0.7.0") (d #t) (k 0)) (d (n "git-object") (r "^0.14.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0jyyjzyr1zlhhpjwr32ibx6wazk7v45dn0y3kk86z7dj90nhd980")))

(define-public crate-git-traverse-0.10.0 (c (n "git-traverse") (v "0.10.0") (d (list (d (n "git-hash") (r "^0.8.0") (d #t) (k 0)) (d (n "git-object") (r "^0.15.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0f33mk706v0gd149044hhljg0b9xiilbpbadsa73vqwjj45i1k7m")))

(define-public crate-git-traverse-0.10.1 (c (n "git-traverse") (v "0.10.1") (d (list (d (n "git-hash") (r "^0.8.0") (d #t) (k 0)) (d (n "git-object") (r "^0.15.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "12h84sgina95pv773a14xz10sy46x0645qnxf3mxw9a5ryv3b89s")))

(define-public crate-git-traverse-0.11.0 (c (n "git-traverse") (v "0.11.0") (d (list (d (n "git-hash") (r "^0.8.0") (d #t) (k 0)) (d (n "git-object") (r "^0.16.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "04h0yf2gcwypjnrrwj55s13mqkmldcdf306lgh2fsf205bi5hzmg")))

(define-public crate-git-traverse-0.12.0 (c (n "git-traverse") (v "0.12.0") (d (list (d (n "git-hash") (r "^0.9.1") (d #t) (k 0)) (d (n "git-object") (r "^0.17.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0yqg7f0gy6k36q1rc4mjg4q307xr17jbsabcynv687sk1s1wc3rm")))

(define-public crate-git-traverse-0.13.0 (c (n "git-traverse") (v "0.13.0") (d (list (d (n "git-hash") (r "^0.9.3") (d #t) (k 0)) (d (n "git-object") (r "^0.18.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "06zbgb4vxhri7qbmw653lcdispf9d3z6kgwv9d69cy0gm0xrsgkz")))

(define-public crate-git-traverse-0.14.0 (c (n "git-traverse") (v "0.14.0") (d (list (d (n "git-hash") (r "^0.9.3") (d #t) (k 0)) (d (n "git-object") (r "^0.18.0") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0cl7mcr0vvvllydqyz693s7sjnyp7nsfrvz1cg89439mxb6g0i81")))

(define-public crate-git-traverse-0.15.0 (c (n "git-traverse") (v "0.15.0") (d (list (d (n "git-hash") (r "^0.9.4") (d #t) (k 0)) (d (n "git-object") (r "^0.19.0") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1z5a8d7cwl14jgaf0qzmc0bh4hbcx1jzzjvd3cc5b1g7hm8jjp4y")))

(define-public crate-git-traverse-0.16.0 (c (n "git-traverse") (v "0.16.0") (d (list (d (n "git-hash") (r "^0.9.6") (d #t) (k 0)) (d (n "git-object") (r "^0.20.0") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "09qq2ba80nii3br6wm6ygwns9r5cbrqz8vsmdmms9achgrvivxgz")))

(define-public crate-git-traverse-0.16.1 (c (n "git-traverse") (v "0.16.1") (d (list (d (n "git-hash") (r "^0.9.7") (d #t) (k 0)) (d (n "git-object") (r "^0.20.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1ak3j5zwkam9k1xwqmpqqd251vdqczlaiw86pjw4blaprml996y8")))

(define-public crate-git-traverse-0.16.2 (c (n "git-traverse") (v "0.16.2") (d (list (d (n "git-hash") (r "^0.9.8") (d #t) (k 0)) (d (n "git-object") (r "^0.20.2") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0lih72jcqc5zld34jqv7s22fb8r3lgf45gqrpbfx8mxr1qi8zp2y")))

(define-public crate-git-traverse-0.16.3 (c (n "git-traverse") (v "0.16.3") (d (list (d (n "git-hash") (r "^0.9.8") (d #t) (k 0)) (d (n "git-object") (r "^0.20.3") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "09s8d7liwwv7rmf26rfnm14ca54im4kwfnzsap0n411nhjdvf2xa")))

(define-public crate-git-traverse-0.16.4 (c (n "git-traverse") (v "0.16.4") (d (list (d (n "git-hash") (r "^0.9.8") (d #t) (k 0)) (d (n "git-object") (r "^0.20.3") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "047w9x9xmiqis7304aaz07snzyhj28s302sh56zlyfy107f72026")))

(define-public crate-git-traverse-0.17.0 (c (n "git-traverse") (v "0.17.0") (d (list (d (n "git-hash") (r "^0.9.10") (d #t) (k 0)) (d (n "git-object") (r "^0.21.0") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0sr7yyxqcnw1rjk4d5ccqgvdr5s9nyqnn44d5w5g59y2x5rwz9jy")))

(define-public crate-git-traverse-0.18.0 (c (n "git-traverse") (v "0.18.0") (d (list (d (n "git-hash") (r "^0.9.11") (d #t) (k 0)) (d (n "git-object") (r "^0.22.0") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "12dg0l2sr37saxl4q40zg4lh2xzb90ykgs5c8d7jk7y6fgbls30d")))

(define-public crate-git-traverse-0.19.0 (c (n "git-traverse") (v "0.19.0") (d (list (d (n "git-hash") (r "^0.10.0") (d #t) (k 0)) (d (n "git-object") (r "^0.23.0") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0nr8bk1wir5r47ldlzpbmipmj40mk055wd4x9zi55lljbj9lc9rd")))

(define-public crate-git-traverse-0.20.0 (c (n "git-traverse") (v "0.20.0") (d (list (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-hashtable") (r "^0.1.0") (d #t) (k 0)) (d (n "git-object") (r "^0.24.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0kcdb2bwbh8a7alhp16h68xf1iv1zc0hwxiw35hlh33dwpfl2lay")))

(define-public crate-git-traverse-0.21.0 (c (n "git-traverse") (v "0.21.0") (d (list (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-hashtable") (r "^0.1.0") (d #t) (k 0)) (d (n "git-object") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0pycy8ybjy0shcg9hvqwddxd3dr5b2386nmqasx624anbga1xfa3")))

(define-public crate-git-traverse-0.22.0 (c (n "git-traverse") (v "0.22.0") (d (list (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-hashtable") (r "^0.1.0") (d #t) (k 0)) (d (n "git-object") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0afv7iqpgki92p47yjwgn53cfhsmp16dy1gisjwnj3y03si6sddx")))

(define-public crate-git-traverse-0.22.1 (c (n "git-traverse") (v "0.22.1") (d (list (d (n "git-hash") (r "^0.10.2") (d #t) (k 0)) (d (n "git-hashtable") (r "^0.1.1") (d #t) (k 0)) (d (n "git-object") (r "^0.26.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "01fd68r8zx9g2hpkzpghgfg772dznl931z6ph3fjs7jw2hgjmfv8") (r "1.64")))

(define-public crate-git-traverse-0.22.2 (c (n "git-traverse") (v "0.22.2") (d (list (d (n "git-hash") (r "^0.10.3") (d #t) (k 0)) (d (n "git-hashtable") (r "^0.1.2") (d #t) (k 0)) (d (n "git-object") (r "^0.26.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1ppyydhl8xsxshlcra83ch6llwd87ghq0f9dgrq1lf6s053blxdz") (r "1.64")))

