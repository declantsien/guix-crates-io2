(define-module (crates-io gi t- git-subset) #:use-module (crates-io))

(define-public crate-git-subset-0.1.0 (c (n "git-subset") (v "0.1.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)))) (h "12l7lqc59dwf0g4mffcngrcdpxg48k0hinvvzq9l84i5w8q7rxpk")))

