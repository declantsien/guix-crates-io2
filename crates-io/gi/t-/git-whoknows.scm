(define-module (crates-io gi t- git-whoknows) #:use-module (crates-io))

(define-public crate-git-whoknows-0.1.0 (c (n "git-whoknows") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "08xqwi9z0w1vgqmf7i73kq81gj2a828hrnm4dxcvri97p7scsavp")))

