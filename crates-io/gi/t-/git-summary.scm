(define-module (crates-io gi t- git-summary) #:use-module (crates-io))

(define-public crate-git-summary-0.1.0 (c (n "git-summary") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "13k7fiyp4wa0k8pasw1r0z4hi9gm6j9cvzh1zq0ksvn197698dfi")))

(define-public crate-git-summary-0.1.1 (c (n "git-summary") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1jz6pzn5pzbmfkiss6ywjf80sqzaq1n8v78ach75rn4y6rxl5hr4")))

