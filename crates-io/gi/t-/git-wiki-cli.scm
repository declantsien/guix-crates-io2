(define-module (crates-io gi t- git-wiki-cli) #:use-module (crates-io))

(define-public crate-git-wiki-cli-0.0.1 (c (n "git-wiki-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1cbmy1d00x6pydllfp7yzi275s74dnk6mac8kwlvdn7c8d4nz4ig")))

(define-public crate-git-wiki-cli-0.0.2 (c (n "git-wiki-cli") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "023k2s63jhfydq6v5fsvflnakqvc8zi88hrgpdz3nzr22imi9300")))

