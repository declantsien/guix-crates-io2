(define-module (crates-io gi t- git-gud) #:use-module (crates-io))

(define-public crate-git-gud-0.1.1 (c (n "git-gud") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)))) (h "0vplmgq3ayy6hfp6r621sdyvnzkdj0736a4pzam9p9nqd9mwgz0b")))

(define-public crate-git-gud-0.1.2 (c (n "git-gud") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)))) (h "1sa10lbc6syh0vx4v1xvzr604lkah5a1xajygwlfhzlf9d1iak31")))

(define-public crate-git-gud-0.1.3 (c (n "git-gud") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)))) (h "1lrw8r8xsgbg0p14h5fk47i5imccn8xjssh44lz19lx79kziywdg")))

