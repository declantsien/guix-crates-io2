(define-module (crates-io gi t- git-kickflip) #:use-module (crates-io))

(define-public crate-git-kickflip-0.1.0 (c (n "git-kickflip") (v "0.1.0") (d (list (d (n "execute") (r "^0.2.9") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 0)))) (h "11c0n0qhn49c7wpdwgygqbgfb66dfrkray3zbwy6p7xkb2mbfmgj")))

