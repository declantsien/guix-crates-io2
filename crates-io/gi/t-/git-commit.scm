(define-module (crates-io gi t- git-commit) #:use-module (crates-io))

(define-public crate-git-commit-0.1.0 (c (n "git-commit") (v "0.1.0") (d (list (d (n "git-trailers") (r "^0.1.0") (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cy5v0s98wj5z6l9k14vp7w29r2iiaimdm9y54xknxkcn4l1h6if")))

(define-public crate-git-commit-0.2.0 (c (n "git-commit") (v "0.2.0") (d (list (d (n "git-trailers") (r "^0.1.0") (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sghngaj7i1akgjqmwjwkdb5lfi25k0b0ak0ajya8jm8mp1j0l9y")))

(define-public crate-git-commit-0.3.0 (c (n "git-commit") (v "0.3.0") (d (list (d (n "git-trailers") (r "^0.1.0") (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (f (quote ("vendored-libgit2"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1h9m3asw0c896vd1xqv7h6nf73q0y27qbspa0i1s61bdrf10cj22")))

