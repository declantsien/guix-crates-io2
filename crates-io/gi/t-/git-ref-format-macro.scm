(define-module (crates-io gi t- git-ref-format-macro) #:use-module (crates-io))

(define-public crate-git-ref-format-macro-0.1.0 (c (n "git-ref-format-macro") (v "0.1.0") (d (list (d (n "git-ref-format-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vs2r6gmnb6iwgabckykivglz9403w5pfxka32lqannlr52l8hv6")))

(define-public crate-git-ref-format-macro-0.2.0 (c (n "git-ref-format-macro") (v "0.2.0") (d (list (d (n "git-ref-format-core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "13nrhbjjmd6wifypbj7gzqvfws43irf8ak6ligv5xa93minp2n1h")))

(define-public crate-git-ref-format-macro-0.2.1 (c (n "git-ref-format-macro") (v "0.2.1") (d (list (d (n "git-ref-format-core") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0z6ynag38zvlg66q6g677jdx4ss4gv2ihfmh4kbvy3v5hk0x0a7m")))

(define-public crate-git-ref-format-macro-0.2.3 (c (n "git-ref-format-macro") (v "0.2.3") (d (list (d (n "git-ref-format-core") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0qn74q3nz76z019hn48g4wbvag1aq6rhpkd8bg2sss6r6ms3ddzn")))

(define-public crate-git-ref-format-macro-0.3.0 (c (n "git-ref-format-macro") (v "0.3.0") (d (list (d (n "git-ref-format-core") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1bnjv84l41z3ifsynvar27pbgpai0yrbh0lnlfms7n9v388d1zqq")))

