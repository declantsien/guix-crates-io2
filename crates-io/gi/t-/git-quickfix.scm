(define-module (crates-io gi t- git-quickfix) #:use-module (crates-io))

(define-public crate-git-quickfix-0.0.1 (c (n "git-quickfix") (v "0.0.1") (d (list (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0lglgknsakvgmmsv7c5na5dl346cgdygb0hrm2vc6zr3wpxlr3p5")))

(define-public crate-git-quickfix-0.0.2 (c (n "git-quickfix") (v "0.0.2") (d (list (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0zv2av2y2hvb6a066f03ql6vc2nsf06n2rallk562g4gmk79ca6h")))

(define-public crate-git-quickfix-0.0.3 (c (n "git-quickfix") (v "0.0.3") (d (list (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1i6dpx9z7vjb6wpvf865qfnwxnv84qa9dwps4yw0hvbwlngx6wbm")))

(define-public crate-git-quickfix-0.0.4 (c (n "git-quickfix") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "18h5d6pp5a0bhy9vyb07ygs4v2520zahq7ss02vbqpqiingxnfsq")))

(define-public crate-git-quickfix-0.0.5 (c (n "git-quickfix") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1gyb2zn5f87j5rqsb1fi9bk9andvzmiiiz5f7h007sphglr16x0l")))

(define-public crate-git-quickfix-0.1.0 (c (n "git-quickfix") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.1") (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0kd8rqz3lj7gil1acjqm563d8nlcv9p1vv43rljrijrsidimnqzf")))

