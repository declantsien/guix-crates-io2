(define-module (crates-io gi t- git-subcopy) #:use-module (crates-io))

(define-public crate-git-subcopy-0.1.0 (c (n "git-subcopy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.11") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "git2") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "11hx9zvwbc9bxzh78ypnpfg31wcfq27idrm7dfpvhlvs4y04v243")))

