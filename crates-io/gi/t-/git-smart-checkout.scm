(define-module (crates-io gi t- git-smart-checkout) #:use-module (crates-io))

(define-public crate-git-smart-checkout-0.1.0 (c (n "git-smart-checkout") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.9.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "1wia370chm9crybr4gd5cj9ai7imgn9bxg33xrfkrkdkpvmprpnk")))

(define-public crate-git-smart-checkout-0.1.1 (c (n "git-smart-checkout") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.9.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "0gjcqrrghfahv7kn8c1gji9sswq28gwjwzaamm69s69kwbbvhddi")))

(define-public crate-git-smart-checkout-0.1.2 (c (n "git-smart-checkout") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "18f90sry520l3cjpsv3qv5z6w0q182cg21jia9rg7rspsaqgvynf")))

(define-public crate-git-smart-checkout-0.1.3 (c (n "git-smart-checkout") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "0gb2w4zd0zgnwcapp4hvkqdb84w3i5bcj4cbhn7ck2322907hn9y")))

