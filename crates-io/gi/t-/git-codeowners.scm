(define-module (crates-io gi t- git-codeowners) #:use-module (crates-io))

(define-public crate-git-codeowners-0.1.0 (c (n "git-codeowners") (v "0.1.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "codeowners") (r "^0.1") (d #t) (k 0)))) (h "0rvfsi3im7ihgs40gfdib2y7j2khgnw7ajlqsrl0y7y0r7agzcd1")))

(define-public crate-git-codeowners-0.1.1 (c (n "git-codeowners") (v "0.1.1") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "codeowners") (r "^0.1.1") (d #t) (k 0)))) (h "1g5vbvqrmgkaakmirfcy4wrvi449vfrypsypx88jz5wph3b8vjvs")))

(define-public crate-git-codeowners-0.1.2 (c (n "git-codeowners") (v "0.1.2") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "codeowners") (r "^0.1.3") (d #t) (k 0)))) (h "0a50k33b3gkkknihjsr6fvakf0m4sm1nfph8623bapd8g1272csc")))

