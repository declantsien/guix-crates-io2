(define-module (crates-io gi t- git-home) #:use-module (crates-io))

(define-public crate-git-home-0.0.3 (c (n "git-home") (v "0.0.3") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)))) (h "02xvgl2i8svl15iv1xykdlm9wxshh4vpqd89z8gcszrxcx4kvbwn")))

(define-public crate-git-home-0.0.4 (c (n "git-home") (v "0.0.4") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)))) (h "08xi05ydfsnw4akd77v156yl6yzs3sd6whmj8pci7xh0f01q7991")))

(define-public crate-git-home-0.0.5 (c (n "git-home") (v "0.0.5") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)))) (h "0j4rb34wn849ndg82blvrkwfglz6b356ylw1z8iz9qnb1in685g8")))

(define-public crate-git-home-0.0.6 (c (n "git-home") (v "0.0.6") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)))) (h "00cl1vrmssz6qh1m8wdbg76w0b76vrg4anm08q683292m21hn5hr")))

(define-public crate-git-home-0.0.7 (c (n "git-home") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.14.4") (d #t) (k 0)))) (h "089d02sa8fp5bc2xvqzs8kiq9zw77yx7503zisgq1kmgi1lx3d2y")))

(define-public crate-git-home-0.1.1-alpha.1 (c (n "git-home") (v "0.1.1-alpha.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.14.4") (d #t) (k 0)))) (h "0l85qhkbrnvdcrvsy5klrg1wriywiws2x9prv7hiyj459c5x90b2")))

(define-public crate-git-home-0.1.1-alpha.2 (c (n "git-home") (v "0.1.1-alpha.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.14.4") (d #t) (k 0)))) (h "0wwqs9jbr7qnac684vq545nm5md7z35p8p273yb1lrykavczabd6")))

(define-public crate-git-home-0.1.1-alpha.3 (c (n "git-home") (v "0.1.1-alpha.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "edit") (r "^0.1.4") (d #t) (k 0)) (d (n "git2") (r "^0.14.4") (d #t) (k 0)))) (h "0pgwsfphl58vb68mz04sanc86793nqd79fzbflfla6kggdd4w0wq")))

