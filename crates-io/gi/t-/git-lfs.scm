(define-module (crates-io gi t- git-lfs) #:use-module (crates-io))

(define-public crate-git-lfs-0.0.0 (c (n "git-lfs") (v "0.0.0") (h "17x960imrn50zziing9ha2hmylx5a1h6d2vxmlil8cxwcfq891yd")))

(define-public crate-git-lfs-0.1.0 (c (n "git-lfs") (v "0.1.0") (h "0xsqs2r4rhbsmm19fppn954wmgwgk7v33g9c4433r86f7pzlm9jd") (r "1.64")))

