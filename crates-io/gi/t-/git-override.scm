(define-module (crates-io gi t- git-override) #:use-module (crates-io))

(define-public crate-git-override-0.1.0 (c (n "git-override") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)))) (h "0jgipv4gqs8spbz3ni92vgn54zljp49blh1w57z75l06asvs6kg4")))

