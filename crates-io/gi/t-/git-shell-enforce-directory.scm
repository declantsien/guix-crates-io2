(define-module (crates-io gi t- git-shell-enforce-directory) #:use-module (crates-io))

(define-public crate-git-shell-enforce-directory-1.0.0 (c (n "git-shell-enforce-directory") (v "1.0.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0d5bbx6dy0hrpc5zk7xih38lbbr32780k9bfwi27ypsv0373l6r5")))

(define-public crate-git-shell-enforce-directory-1.0.1 (c (n "git-shell-enforce-directory") (v "1.0.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1v6xh1361sp7x038idc0nfalqkh9zpbgpk5vlkjg2qbi6pl8znna")))

