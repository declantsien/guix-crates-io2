(define-module (crates-io gi t- git-nuke) #:use-module (crates-io))

(define-public crate-git-nuke-0.1.0 (c (n "git-nuke") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)))) (h "0gnvpa2xq76ml21s29mmpzaq04vlr580pkqd0scxda7y3hxds2qi")))

