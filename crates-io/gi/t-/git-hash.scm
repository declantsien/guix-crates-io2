(define-module (crates-io gi t- git-hash) #:use-module (crates-io))

(define-public crate-git-hash-0.1.0 (c (n "git-hash") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "199d52zcnfxcq3nqq0qbir8dd6jj2jg7f2s55an97jgph5nn8sk1") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-hash-0.1.1 (c (n "git-hash") (v "0.1.1") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "0nymabqr64m0jvpf69y54zdyw1yv2fkwaawarka53nnmmxjpcq63") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-hash-0.1.2 (c (n "git-hash") (v "0.1.2") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "0fixm0x6h49lfnlxkb3ya6pn9k1253ybcw46589ybyjmdkklnpcx") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-hash-0.2.0 (c (n "git-hash") (v "0.2.0") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1m16z9wrgjaqbjikmybw6l8d73r0ypcjx84637ggxi2izh0hln6d") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-hash-0.3.0 (c (n "git-hash") (v "0.3.0") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1pvqkq6g5vfprjxiqqrdihhlapkl98lj0xadfnrlja0vskqqjrzl") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-hash-0.4.0 (c (n "git-hash") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "0ci9kf31g0s86rmlkg93ln74z91936nwcz4hinqhb2mf6323zv46") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.5.0 (c (n "git-hash") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "07vam0yd65qfhvjfl5ilzya0sxscbn7jsjb11jzx8qkfbckwbghl") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.5.1 (c (n "git-hash") (v "0.5.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1mifr6z3w5xyb6pdsya2x1l7q18dnclh1vz7imdhibpm9kfxvy26") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.6.0 (c (n "git-hash") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1dp20ww0xrrsizskjsabszmsdlznlf8xri68pqkbyazh99h0bn5i") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.7.0 (c (n "git-hash") (v "0.7.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "18ixg7n0yvdmrp31ahb64z8ff7krs8hlm3azyrzl5046m9w8af7h") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.8.0 (c (n "git-hash") (v "0.8.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "0kl62y0vz7h50gj570n64fg8q8lif8p8mpdnfn46mxs396f7nw4r") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.0 (c (n "git-hash") (v "0.9.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1203nh54aaxi7my4avw1ivj4lhm6kd38vn44an3arvh7h9vzh9l4") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.1 (c (n "git-hash") (v "0.9.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1vcw9zn0cglnwcycvqvlp35c63wjrngiqgqkr3y3r1is3pmc3n12") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.2 (c (n "git-hash") (v "0.9.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1hmc2k8v8xrg2xjc3y95ib9jgzbgrpvza617mf5faa5z3dj5gxaz") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.3 (c (n "git-hash") (v "0.9.3") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1d1bvqhv0hr4fjpjy8jbygxyg1njcyg10zya1fzqawmr10x7qvhh") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.4 (c (n "git-hash") (v "0.9.4") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "07pi52s49sx44nn6l5bdb7mc68qkkpy83y6avj613x0k0rv7wpg0") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.5 (c (n "git-hash") (v "0.9.5") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "11gwmdcg3p2qwn5ir1ygxqf5advf4xg3chwp34sqkylr4j8r6grx") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.6 (c (n "git-hash") (v "0.9.6") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "10dy1cgckrya3spdjc2ah6zhxmn6a932339kv2777syxzl4hqapq") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.7 (c (n "git-hash") (v "0.9.7") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "0q2404f2dc0kfn2mjjpf4cz51hdcc7w3c5s6f8w5cdzlxnm2i1l9") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.8 (c (n "git-hash") (v "0.9.8") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1989x9k9h66irzhdnanl0c7lxv6v4fgmkdvq42p32malwiw1qqa2") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.9 (c (n "git-hash") (v "0.9.9") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "04vjcrybr7svsp1rpr2hxlrw8wcpg1hzciz4y0k6r0wj08jvxbqg") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.10 (c (n "git-hash") (v "0.9.10") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "18y4618bdqzv1gvfpl8y4wx79zcs2f81h2b717lk8ysbjbfj2kxm") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.9.11 (c (n "git-hash") (v "0.9.11") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "0m25ng3v75hylvz42d9m9ab01cv1cxm53gw7i91s938y5mn6xm0n") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.10.0 (c (n "git-hash") (v "0.10.0") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "15cp0mnmw5742lf3gnc19krsq0zlq5mlpycbdz5nv5clh4g2fkfp") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.10.1 (c (n "git-hash") (v "0.10.1") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "17dr295wf0yfs2fyxxvcy69ka7iiirb7nnswaj6jylrhz0mxhchm") (f (quote (("serde1" "serde"))))))

(define-public crate-git-hash-0.10.2 (c (n "git-hash") (v "0.10.2") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "00hn4ad76bjxpsv9inwpm50xgg2v3hnw8m6sppkmg9w737m37snz") (f (quote (("serde1" "serde")))) (r "1.64")))

(define-public crate-git-hash-0.10.3 (c (n "git-hash") (v "0.10.3") (d (list (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1185021fihqd3wf5hhddlqi4xkgzv0y7is9bbhflpnqzli0zcjic") (f (quote (("serde1" "serde")))) (r "1.64")))

