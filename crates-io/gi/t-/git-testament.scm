(define-module (crates-io gi t- git-testament) #:use-module (crates-io))

(define-public crate-git-testament-0.1.0 (c (n "git-testament") (v "0.1.0") (d (list (d (n "git-testament-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "02d9wv9p307kdm6rk0669ilgh976ndc9661aq2rrgmlri21256vf") (y #t)))

(define-public crate-git-testament-0.1.1 (c (n "git-testament") (v "0.1.1") (d (list (d (n "git-testament-derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "153mz5wwfc7gl9hwf7hyjjlby0fx877v1z3k1miv4aaclbxkvydk") (y #t)))

(define-public crate-git-testament-0.1.2 (c (n "git-testament") (v "0.1.2") (d (list (d (n "git-testament-derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "10rn8dirda457jpk3ypsyihkllx6k6kra4rkv4qxdgy8495x5x9r") (y #t)))

(define-public crate-git-testament-0.1.3 (c (n "git-testament") (v "0.1.3") (d (list (d (n "git-testament-derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "10ik4ppm0k4q45ck5cw43ppph82p75k3p49g93nj08yrlzz8idbl")))

(define-public crate-git-testament-0.1.4 (c (n "git-testament") (v "0.1.4") (d (list (d (n "git-testament-derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0jsqvh9wcw2fcg018vx5p2izybx3skjwzz2wga95vva4k1nga00b")))

(define-public crate-git-testament-0.1.5 (c (n "git-testament") (v "0.1.5") (d (list (d (n "git-testament-derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0r0wiln9bhh026bscy7kxiypa9fqwciv7sf1qclc36ybz5wbci83")))

(define-public crate-git-testament-0.1.6 (c (n "git-testament") (v "0.1.6") (d (list (d (n "git-testament-derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^1.1.6") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "15d6v06j3hcn70rbmaxdm1vc91fsdi6cjrsy91l3jkckbazbzf22")))

(define-public crate-git-testament-0.1.7 (c (n "git-testament") (v "0.1.7") (d (list (d (n "git-testament-derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "01sj0jypv6jannngsdpwjacvqp10n80v4kcv9riqz7vmx2501x1y")))

(define-public crate-git-testament-0.1.9 (c (n "git-testament") (v "0.1.9") (d (list (d (n "git-testament-derive") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "00wid7gx72id8ivh6dfk2p1yslzr9ihv38s7w4rl0x3568033wb0") (f (quote (("no-std") ("default"))))))

(define-public crate-git-testament-0.1.10 (c (n "git-testament") (v "0.1.10") (d (list (d (n "git-testament-derive") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "11hnspdi05f5js9mf8jf2i1bvgj18q87l07lg1jx9c1h0jcbamx5") (f (quote (("no-std") ("default"))))))

(define-public crate-git-testament-0.2.0 (c (n "git-testament") (v "0.2.0") (d (list (d (n "git-testament-derive") (r "^0.1.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "00kcxkzm6nnnwj5aabrnzxc6ja452qfpyhdzg78286bgmb4bjv09") (f (quote (("default" "alloc") ("alloc" "no-std-compat/alloc"))))))

(define-public crate-git-testament-0.2.1 (c (n "git-testament") (v "0.2.1") (d (list (d (n "git-testament-derive") (r "^0.1.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0ns3l8pmqx4ak59w6cg79pb1q0l6wgf19722fhsb2gr47kplf308") (f (quote (("default" "alloc") ("alloc" "no-std-compat/alloc"))))))

(define-public crate-git-testament-0.2.2 (c (n "git-testament") (v "0.2.2") (d (list (d (n "git-testament-derive") (r "^0.1.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0x5djhwi9akscfh2gnhlsx608w488xvb5kn5c9infcf0ha0wmp9b") (f (quote (("default" "alloc") ("alloc" "no-std-compat/alloc"))))))

(define-public crate-git-testament-0.2.3 (c (n "git-testament") (v "0.2.3") (d (list (d (n "git-testament-derive") (r "^0.1.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0pk9zbknkfkqck6c8m1f82dz6n4zcj6szqcz17p6mw9gnc6s8xsv") (f (quote (("default" "alloc") ("alloc" "no-std-compat/alloc"))))))

(define-public crate-git-testament-0.2.4 (c (n "git-testament") (v "0.2.4") (d (list (d (n "git-testament-derive") (r "^0.1.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1c9l10wpyz39vhb5cijvbym6gmpmw3y3nb35l2hg6w42h1ygaswq") (f (quote (("default" "alloc") ("alloc" "no-std-compat/alloc"))))))

(define-public crate-git-testament-0.2.5 (c (n "git-testament") (v "0.2.5") (d (list (d (n "git-testament-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "13pwvfrfgm4s7f80gk4ygzrl0rlqlaks0fx6bcpycilfnv97h33i") (f (quote (("default" "alloc") ("alloc"))))))

