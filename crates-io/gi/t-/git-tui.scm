(define-module (crates-io gi t- git-tui) #:use-module (crates-io))

(define-public crate-git-tui-0.0.0 (c (n "git-tui") (v "0.0.0") (h "1vkjmqvhgnvxfjsl1hqs9kva8w2fpakfyvinikpcy51kkhhm8yj2")))

(define-public crate-git-tui-0.1.0 (c (n "git-tui") (v "0.1.0") (h "1vlx4bs44ama8fnv4y9bk7pl0ms75sy95nkgwnpx65y6gzjdazir") (r "1.64")))

