(define-module (crates-io gi t- git-filter-server) #:use-module (crates-io))

(define-public crate-git-filter-server-0.1.0 (c (n "git-filter-server") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "19waq0bl1n86lsb8kqp98gqbpga3q67pnm877gphzjzm19vrja2q")))

(define-public crate-git-filter-server-0.1.1 (c (n "git-filter-server") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1d4z1swyh86x5xdsz22xqv2h04vdmhhy4mr8w0sxrqqc0cw8znb1")))

(define-public crate-git-filter-server-0.1.2 (c (n "git-filter-server") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "147fz0y4hc7jkr82az1n727q9irjnjb38cp24qqbjgs6qxn67af4")))

(define-public crate-git-filter-server-0.1.3 (c (n "git-filter-server") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1xidi3kyayk9cpla1lrgasjf7hymywz7vnmmqxzdxygbwvxfiajk")))

