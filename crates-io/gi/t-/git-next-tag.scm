(define-module (crates-io gi t- git-next-tag) #:use-module (crates-io))

(define-public crate-git-next-tag-0.1.0 (c (n "git-next-tag") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.12") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "09iqb1nbjdlmh3skzpqjij3kdgwpslj54vfj8s78wpsvwaqj3ha7")))

