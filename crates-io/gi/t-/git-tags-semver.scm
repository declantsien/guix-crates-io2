(define-module (crates-io gi t- git-tags-semver) #:use-module (crates-io))

(define-public crate-git-tags-semver-0.3.0 (c (n "git-tags-semver") (v "0.3.0") (d (list (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)))) (h "16l6fi2qk2k0k2fapajasmprbzhkxz2n4gmv7dmj42m33sh71r67") (f (quote (("build" "regex")))) (r "1.51")))

