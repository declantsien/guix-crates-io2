(define-module (crates-io gi t- git-switch) #:use-module (crates-io))

(define-public crate-git-switch-0.0.1 (c (n "git-switch") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1hbqkbvnqbivv7s8hyp87i642l93jwv7x0rxn5w1l1wy1n1j08x8")))

(define-public crate-git-switch-0.0.2 (c (n "git-switch") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0ajw0rhawidxv0sjga237mc0qvgw3n7s7sg44xpbgz2qf4qky4mq")))

(define-public crate-git-switch-0.0.3 (c (n "git-switch") (v "0.0.3") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "04ld6n635c7f01m8kqypgjp1x2v332rp4k96za9q7g7ll7pgfind")))

(define-public crate-git-switch-0.0.4 (c (n "git-switch") (v "0.0.4") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0hwcv3mnqxy8qqmjviq4p8a79421c6hdfqfw7jcirswhn8flbqnm")))

(define-public crate-git-switch-0.0.5 (c (n "git-switch") (v "0.0.5") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "035q1a0bgfkrgy3fsmff78j6yjfqiq4p2f5jc9pfp83jlj4id4la")))

