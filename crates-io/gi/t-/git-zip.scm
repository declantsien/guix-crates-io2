(define-module (crates-io gi t- git-zip) #:use-module (crates-io))

(define-public crate-git-zip-0.1.0 (c (n "git-zip") (v "0.1.0") (d (list (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "18xmh2amwg3jfhm4vslcz2slgkd4w66dg534s6givc0gvayly0ib")))

