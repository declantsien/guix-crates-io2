(define-module (crates-io gi t- git-function-history-gui) #:use-module (crates-io))

(define-public crate-git-function-history-gui-0.1.0 (c (n "git-function-history-gui") (v "0.1.0") (h "1nv8xf82gjzm5rdisg67pp00qjxr04m38ijcxximwif53l3w5d33")))

(define-public crate-git-function-history-gui-0.2.0 (c (n "git-function-history-gui") (v "0.2.0") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("dark-light"))) (d #t) (k 0)) (d (n "git_function_history") (r "^0.5.4") (d #t) (k 0)))) (h "1n0kll7r43gpbmgbzwsngk62c1bhkbk7l6fl2irp8vi791mhhx7h")))

(define-public crate-git-function-history-gui-0.2.1 (c (n "git-function-history-gui") (v "0.2.1") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("dark-light"))) (d #t) (k 0)) (d (n "git_function_history") (r "^0.5.4") (d #t) (k 0)))) (h "16h9v99nk3zbb0l1aj4yls2h1z3ra8wic0lnyvxp6i539ib21d85")))

(define-public crate-git-function-history-gui-0.2.2 (c (n "git-function-history-gui") (v "0.2.2") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("dark-light"))) (d #t) (k 0)) (d (n "function_history_backend_thread") (r "^0.1.0") (d #t) (k 0)) (d (n "git_function_history") (r "^0.5.4") (d #t) (k 0)))) (h "1k5b1a9z2xjzdzbqx7ffpr53q2mwf8m4c3zcr7rr0jnq2cpfb2dy")))

(define-public crate-git-function-history-gui-0.3.0 (c (n "git-function-history-gui") (v "0.3.0") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("dark-light"))) (d #t) (k 0)) (d (n "function_history_backend_thread") (r "^0.1.0") (d #t) (k 0)) (d (n "git_function_history") (r "^0.5.4") (d #t) (k 0)))) (h "16yc06cgacymsjimlqswwszj1cwyzsi6zkzn1db2s0kils7pdh28")))

(define-public crate-git-function-history-gui-0.3.1 (c (n "git-function-history-gui") (v "0.3.1") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("dark-light"))) (d #t) (k 0)) (d (n "function_history_backend_thread") (r "^0.1.0") (d #t) (k 0)) (d (n "git_function_history") (r "^0.5.4") (d #t) (k 0)))) (h "0jv2m0j1vxia2azam3p186jasf9my6pw0acyfg57j5r7ma9n7138")))

(define-public crate-git-function-history-gui-0.4.0 (c (n "git-function-history-gui") (v "0.4.0") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("dark-light"))) (d #t) (k 0)) (d (n "function_history_backend_thread") (r "^0.2.0") (d #t) (k 0)) (d (n "git_function_history") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_file_logger") (r "^0.2.0") (d #t) (k 0)))) (h "0lz5dvcx66ybg88rbp093ks9ifipgmx8yrzgjaja7xpwihfpyg4k")))

(define-public crate-git-function-history-gui-0.4.1 (c (n "git-function-history-gui") (v "0.4.1") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("dark-light"))) (d #t) (k 0)) (d (n "function_history_backend_thread") (r "^0.2.2") (k 0)) (d (n "git_function_history") (r "^0.6.2") (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_file_logger") (r "^0.2.0") (d #t) (k 0)))) (h "0f9gzxkwz0i73m56lm20lbd5wq72as92kncwc3q8ry4n00rxv6nq") (f (quote (("parallel" "git_function_history/parallel" "function_history_backend_thread/parallel") ("not-parallel") ("default" "parallel"))))))

