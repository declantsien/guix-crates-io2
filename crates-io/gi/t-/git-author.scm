(define-module (crates-io gi t- git-author) #:use-module (crates-io))

(define-public crate-git-author-1.0.0 (c (n "git-author") (v "1.0.0") (d (list (d (n "addr") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cxrk902xm4b7hqdar60w7jycihq65fj56rpcqqlwklq777rp5i2")))

(define-public crate-git-author-1.0.1 (c (n "git-author") (v "1.0.1") (d (list (d (n "addr") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wv24qjilx96hhpldc4qk0csz2k8ynsj18gzq4h18jkhlv058w5x")))

(define-public crate-git-author-2.0.0 (c (n "git-author") (v "2.0.0") (d (list (d (n "addr") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1063scba70pxw244kgccdn65lzxy61ih740iyl1lzyz9cnxif67q")))

