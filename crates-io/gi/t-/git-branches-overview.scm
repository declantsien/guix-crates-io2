(define-module (crates-io gi t- git-branches-overview) #:use-module (crates-io))

(define-public crate-git-branches-overview-0.1.0 (c (n "git-branches-overview") (v "0.1.0") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1if731q3v82hc8f8km36jnc19abn2fm723c152g8bp60b7l2bhdf")))

(define-public crate-git-branches-overview-0.1.1 (c (n "git-branches-overview") (v "0.1.1") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0c13c6j1jnhrjpizwmi7j3j02zfl0dw8pk04405k0p32ndq3ggnp")))

