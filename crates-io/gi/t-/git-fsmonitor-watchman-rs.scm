(define-module (crates-io gi t- git-fsmonitor-watchman-rs) #:use-module (crates-io))

(define-public crate-git-fsmonitor-watchman-rs-0.1.0 (c (n "git-fsmonitor-watchman-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 0)) (d (n "watchman_client") (r "^0.8.0") (d #t) (k 0)))) (h "0x46w270k0v148apf93m1b45ri02yj3w1llzah0gf1z9byqqp4dk")))

(define-public crate-git-fsmonitor-watchman-rs-0.1.1 (c (n "git-fsmonitor-watchman-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 0)) (d (n "watchman_client") (r "^0.8.0") (d #t) (k 0)))) (h "1fklwj3r83qp740pjyiiyw25413057029jg5yl1v3wpb2vwrpk8a")))

(define-public crate-git-fsmonitor-watchman-rs-0.1.2 (c (n "git-fsmonitor-watchman-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 0)) (d (n "watchman_client") (r "^0.8.0") (d #t) (k 0)))) (h "0pn3n842mv6s3sfma562khwdy149ld65qxw3v05qnnragz0gz856")))

