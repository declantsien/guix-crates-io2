(define-module (crates-io gi t- git-ac) #:use-module (crates-io))

(define-public crate-git-ac-0.1.0 (c (n "git-ac") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)))) (h "14wgzm24x95f2f80vx676sd5bfz750lq1spgw8iyzx325w2r83b4")))

