(define-module (crates-io gi t- git-local-ignore) #:use-module (crates-io))

(define-public crate-git-local-ignore-0.1.0 (c (n "git-local-ignore") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (k 0)))) (h "1a3y8888708qrgzqsz57zzwlf8v9nng05sjpdwryhdqka1gvrg2z")))

(define-public crate-git-local-ignore-1.0.0 (c (n "git-local-ignore") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (k 0)))) (h "0j6ql5wyg5bh8i9n9rp17bqiwjys9i2d59avci1nl6w1jv6mg49d")))

