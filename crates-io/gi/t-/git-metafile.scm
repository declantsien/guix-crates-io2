(define-module (crates-io gi t- git-metafile) #:use-module (crates-io))

(define-public crate-git-metafile-0.2.2 (c (n "git-metafile") (v "0.2.2") (d (list (d (n "argparse") (r "^0.2.0") (d #t) (k 0)) (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "005fw4i9jrd38bvgmncfdnww1i4jqpsgc34w9274ypis7mhcrid4")))

(define-public crate-git-metafile-0.2.2-rc.1 (c (n "git-metafile") (v "0.2.2-rc.1") (d (list (d (n "argparse") (r "^0.2.0") (d #t) (k 0)) (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "1scmzc39yyzmyy2blxrhx5dqzihzwdrgqgx3x8grkfj8bsjsm6i3") (y #t)))

(define-public crate-git-metafile-0.2.3 (c (n "git-metafile") (v "0.2.3") (d (list (d (n "argparse") (r "^0.2.0") (d #t) (k 0)) (d (n "boolinator") (r "^2.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "0k36clsbfx8z3kbd7myy692dwp8r946yx00js50ss03kygd2qswi")))

