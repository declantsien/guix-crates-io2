(define-module (crates-io gi t- git-lfs-spec) #:use-module (crates-io))

(define-public crate-git-lfs-spec-0.1.0 (c (n "git-lfs-spec") (v "0.1.0") (d (list (d (n "chrono") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "url_serde") (r "^0") (d #t) (k 0)))) (h "0nh5sy95smdnl7lpd4n3rwygmqnzmih52v8rinbfshqihcyi77cc")))

