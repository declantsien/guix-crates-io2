(define-module (crates-io gi t- git-anger-library) #:use-module (crates-io))

(define-public crate-git-anger-library-0.7.0 (c (n "git-anger-library") (v "0.7.0") (d (list (d (n "git2") (r "^0.13.5") (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "1yhclh7prm17bwbw2ykms658q982fvrpn3pp6yl7jnasn7b5f1y8") (f (quote (("table" "tabwriter") ("json" "serde" "serde_json") ("default" "json" "table"))))))

(define-public crate-git-anger-library-0.8.0 (c (n "git-anger-library") (v "0.8.0") (d (list (d (n "git2") (r "^0.13.5") (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "0fy88x94s334fdyvy3cn0dg772mbyxkndv9w88c6a4qgl4d6cccr") (f (quote (("table" "tabwriter") ("json" "serde" "serde_json") ("default"))))))

(define-public crate-git-anger-library-0.8.1 (c (n "git-anger-library") (v "0.8.1") (d (list (d (n "git2") (r "^0.13.5") (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "0iifll1jvfmncdpg2967bvp2va1gh5inz7wh82w7gkhyymnkzcqi") (f (quote (("table" "tabwriter") ("json" "serde" "serde_json") ("default"))))))

