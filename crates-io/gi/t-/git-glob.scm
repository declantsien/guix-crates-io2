(define-module (crates-io gi t- git-glob) #:use-module (crates-io))

(define-public crate-git-glob-0.1.0 (c (n "git-glob") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "git-quote") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1zckjq8nani7dw0vphm8kw09rjazfa2z9rxl3mmfw1n2p90vfc11") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-glob-0.2.0 (c (n "git-glob") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "0fy7sjy8nhl24qbk5giqkdgw3kh23xghhrsbcykhh0frwnqbyk9n") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-glob-0.3.0 (c (n "git-glob") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1328www69gfmrcp93wggdvz84k20d0h0897xpzbd33d96simr1sf") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-glob-0.3.1 (c (n "git-glob") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "03igjjz34dmgi719a7nn8d8fp8vsdmgwap5svl9a70g46xq6l8la") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-glob-0.3.2 (c (n "git-glob") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "0ipjdlw7a483zgaa4wgsa6pfj150x4f7m9cfhbp7pdawggi7j61x") (f (quote (("serde1" "serde" "bstr/serde1"))))))

(define-public crate-git-glob-0.4.0 (c (n "git-glob") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "05abv7bjd9xl2arplailjnmsfb20kjq3da2lxr9r7m7grvbw0hw2") (f (quote (("serde1" "serde" "bstr/serde"))))))

(define-public crate-git-glob-0.4.1 (c (n "git-glob") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "03r6pmar05rb6dzgc8ws1abhwvyflcskha0v4zpdc73rmv47zl8l") (f (quote (("serde1" "serde" "bstr/serde"))))))

(define-public crate-git-glob-0.4.2 (c (n "git-glob") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "145agpzf9109shqybxxi4x8h1c6dzmh6y8s9j34gh4ki4cq68xbd") (f (quote (("serde1" "serde" "bstr/serde"))))))

(define-public crate-git-glob-0.5.0 (c (n "git-glob") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "0xxxhi6zspdpcsf578141qy6mzh75ykiqrs53ssm9kigc08qd1gg") (f (quote (("serde1" "serde" "bstr/serde"))))))

(define-public crate-git-glob-0.5.1 (c (n "git-glob") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "12lfsiwijrghqyvnxg1wc9walgld6x14243acczppb3nkd64021r") (f (quote (("serde1" "serde" "bstr/serde"))))))

(define-public crate-git-glob-0.5.2 (c (n "git-glob") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "18w3ww436fqy6fic2s4pqp3yf8z3xg0hy9fgw66y4rhskjfcywxa") (f (quote (("serde1" "serde" "bstr/serde"))))))

(define-public crate-git-glob-0.5.3 (c (n "git-glob") (v "0.5.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "19pr8rb9j1l1n2prnmrky63g9hlk6kv9d7xsgnmjhz026qd8pji8") (f (quote (("serde1" "serde" "bstr/serde")))) (r "1.64")))

(define-public crate-git-glob-0.5.4 (c (n "git-glob") (v "0.5.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "document-features") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)))) (h "1v1clkam4rm7xs4ygfkidkvz99i8m2bb0mmpp82yc2idz5nv261h") (f (quote (("serde1" "serde" "bstr/serde")))) (r "1.64")))

