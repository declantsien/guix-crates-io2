(define-module (crates-io gi t- git-bitmap) #:use-module (crates-io))

(define-public crate-git-bitmap-0.0.0 (c (n "git-bitmap") (v "0.0.0") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0d36laghfifj4c6zd7jmgbp2j8spjdak50zsp6hzrz52rmi5qp2c")))

(define-public crate-git-bitmap-0.0.1 (c (n "git-bitmap") (v "0.0.1") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1aqdp1nl8nb17ll5ca0i3v5yy18pw7yh06bip5ys8wac3ay0av0c")))

(define-public crate-git-bitmap-0.1.0 (c (n "git-bitmap") (v "0.1.0") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0x9401bdv0mdmzysp149z1x2qac690p36sk2nlmyj7vnrnacq5bp")))

(define-public crate-git-bitmap-0.1.1 (c (n "git-bitmap") (v "0.1.1") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0ysh22jn12apckbr62q87n21nnnfwvrkfs7m6fij2l9qni2blhjl")))

(define-public crate-git-bitmap-0.1.2 (c (n "git-bitmap") (v "0.1.2") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1w1qxjsf22xywhhaf42xfn6q5k3m8fyjsq3igs6jkbi7mnkrhw1j")))

(define-public crate-git-bitmap-0.2.0 (c (n "git-bitmap") (v "0.2.0") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "08h71m5biag5yi4cggjh1wfml5isa0x5rha3nahsv836mj9l0c24")))

(define-public crate-git-bitmap-0.2.1 (c (n "git-bitmap") (v "0.2.1") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0krl07dva4n8andzzd3a805ngwcgwyq18xr8kqm78a01mq65zs0k") (r "1.64")))

(define-public crate-git-bitmap-0.2.2 (c (n "git-bitmap") (v "0.2.2") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1c94c53m9fwhh7294qqinz7z2xrhbs1q3gyv4nzs49rgr8mrfrb0") (r "1.64")))

