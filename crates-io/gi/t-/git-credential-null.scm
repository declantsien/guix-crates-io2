(define-module (crates-io gi t- git-credential-null) #:use-module (crates-io))

(define-public crate-git-credential-null-1.0.0 (c (n "git-credential-null") (v "1.0.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tiny_http") (r "^0.6.2") (d #t) (k 2)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 2)))) (h "1173lzhpb10lbd89gb1y0michf2zbwbj2zrpjhfz9i82py7sdc9z")))

(define-public crate-git-credential-null-1.0.1 (c (n "git-credential-null") (v "1.0.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tiny_http") (r "^0.6.2") (d #t) (k 2)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 2)))) (h "0bhmxd3nh0bc7xyrmzbzm5zlfai56zb94lmzkcpq6lpj34gq65kl")))

