(define-module (crates-io gi t- git-eq) #:use-module (crates-io))

(define-public crate-git-eq-0.1.0 (c (n "git-eq") (v "0.1.0") (h "1zz1h4gkk22yw9isk062664n56r8vjwy8cvhyghmgr82kmfgh65y") (y #t)))

(define-public crate-git-eq-0.1.1 (c (n "git-eq") (v "0.1.1") (d (list (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "1mv1m9j21vy16vk67yvk330kbsj8kc70awajf5apizwwfcdjdsyl") (y #t)))

(define-public crate-git-eq-0.1.2 (c (n "git-eq") (v "0.1.2") (d (list (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0ns0kjl1mi0r0f8mg7nz5qzxxh769mdby0d4l1cylnl2annaa2wn")))

(define-public crate-git-eq-0.2.0 (c (n "git-eq") (v "0.2.0") (d (list (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0lffwy3349k57kqw06cxv4qwvwsqcr8f84h73qzz734968ja60ay")))

(define-public crate-git-eq-0.3.0 (c (n "git-eq") (v "0.3.0") (d (list (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "1g9hd41z19mygf0wh2f6xign1g2m6w0lj2rfxhg6jhl3glfr9m7w")))

(define-public crate-git-eq-1.0.0 (c (n "git-eq") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "0bbrfpnl5s01fjcacq4dy7n3q0r705ayncgms2ch3pijfgdcwymb")))

(define-public crate-git-eq-1.0.1 (c (n "git-eq") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "1f087mwzymxz6qi0cdcqjj8rpdapf6al12nmly7qx54v9m9gq5p6")))

(define-public crate-git-eq-1.0.2 (c (n "git-eq") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "0q09xc53kmpl3bqbrjnhsgaw6pxz4h6f1434rr4pwqvcakfnln3g")))

