(define-module (crates-io gi t- git-historian) #:use-module (crates-io))

(define-public crate-git-historian-0.3.2 (c (n "git-historian") (v "0.3.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0s6cs73f3sqmg6ifl4hs38412n4qlwd3sp4qxrlm17r62sgi9rxw")))

(define-public crate-git-historian-0.3.3 (c (n "git-historian") (v "0.3.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1l6d0vjlvyyh4gyw88dr2sjgs6lyyjzj25b428cp2iymz394qa7l")))

(define-public crate-git-historian-0.3.4 (c (n "git-historian") (v "0.3.4") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0miahwqi7dgspbyq0l30mby8ibsrj6rzqsxx0v4zshkrrj08x05q") (y #t)))

(define-public crate-git-historian-0.4.0 (c (n "git-historian") (v "0.4.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "17vd4jnbcq90nrnizvckcfr4vri7nw0m9hwdbznmrizs9f41rwc4")))

