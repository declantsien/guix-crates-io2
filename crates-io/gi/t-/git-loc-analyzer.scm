(define-module (crates-io gi t- git-loc-analyzer) #:use-module (crates-io))

(define-public crate-git-loc-analyzer-0.1.0 (c (n "git-loc-analyzer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)))) (h "10wzf4cvimymh9scml8jgcd9j17gnwngcv5l5273018gxj29w9xg")))

