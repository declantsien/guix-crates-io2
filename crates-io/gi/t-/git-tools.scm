(define-module (crates-io gi t- git-tools) #:use-module (crates-io))

(define-public crate-git-tools-0.1.0 (c (n "git-tools") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "19b20f6fwhgnwlz025w6v25ns19sw1nmk9n85xc9ljahxlp2gq0k")))

(define-public crate-git-tools-0.1.2 (c (n "git-tools") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.0") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.7.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "1v1nv0i83pwxaj36gp6vgpqb1nflj8vh49h43yijldjdmsz1byfx")))

(define-public crate-git-tools-0.1.3 (c (n "git-tools") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.0") (d #t) (k 0)) (d (n "git2_credentials") (r "^0.7.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "0m5nigzj39ddwannj8lxy5gbp3i7s00ygmwhlgg4rnkjkw9r5idq")))

