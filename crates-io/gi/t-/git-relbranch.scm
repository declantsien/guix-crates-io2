(define-module (crates-io gi t- git-relbranch) #:use-module (crates-io))

(define-public crate-git-relbranch-0.1.0 (c (n "git-relbranch") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (d #t) (k 0)))) (h "1xnmbz0azjbaib0lk2s1p0ndym4l5j80hwcdmi445y3aixr6hwbx")))

