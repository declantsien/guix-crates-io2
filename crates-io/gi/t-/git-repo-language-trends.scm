(define-module (crates-io gi t- git-repo-language-trends) #:use-module (crates-io))

(define-public crate-git-repo-language-trends-0.1.2 (c (n "git-repo-language-trends") (v "0.1.2") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)))) (h "0316g8r8pv7x5ymddcb6mvwim2ff0qpa5d2zpyvmpgkcijbzncy6")))

(define-public crate-git-repo-language-trends-0.3.0 (c (n "git-repo-language-trends") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "11blh1f6jgbim22xw769pl9xgd6z5nd1mv3bb1gm3gnb3alfqnb7")))

(define-public crate-git-repo-language-trends-0.4.0 (c (n "git-repo-language-trends") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "git2") (r "^0.13") (k 0)) (d (n "indicatif") (r "^0.15") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1b7brq2vc600wj4qibq9a0sxv2lw2371sy3rbx0d233znamxfgj8")))

