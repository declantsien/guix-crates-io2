(define-module (crates-io gi t- git-latest-commit) #:use-module (crates-io))

(define-public crate-git-latest-commit-0.1.0 (c (n "git-latest-commit") (v "0.1.0") (d (list (d (n "crc64") (r "^1.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.5.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "1fqz8wixy558vqkrh1f9wmn3n6rzq5byi1fc8vqz5y1gsflad5xl")))

(define-public crate-git-latest-commit-0.1.1 (c (n "git-latest-commit") (v "0.1.1") (d (list (d (n "git2") (r "^0.5.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "0hlnshiyynfkl9s95cyfnbljl8frv05jmj3pjir6vmknpsax9paq")))

(define-public crate-git-latest-commit-0.1.2 (c (n "git-latest-commit") (v "0.1.2") (d (list (d (n "git2") (r "= 0.5.0") (d #t) (k 0)) (d (n "quick-error") (r "= 1.1.0") (d #t) (k 0)))) (h "1hv8hdihs8dd3fdmkcpi6ziqnqpm8kbxn8xaxyj4g0xh8k0fb6a4")))

(define-public crate-git-latest-commit-0.1.3 (c (n "git-latest-commit") (v "0.1.3") (d (list (d (n "git2") (r "^0.5.0") (k 0)) (d (n "quick-error") (r "= 1.1.0") (d #t) (k 0)))) (h "00gszbf7a2mwj9mmj7yy1710458b9jqv9jhqqmlgvm7vd13lxlk4")))

