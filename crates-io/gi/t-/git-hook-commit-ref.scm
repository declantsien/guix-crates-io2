(define-module (crates-io gi t- git-hook-commit-ref) #:use-module (crates-io))

(define-public crate-git-hook-commit-ref-1.0.0 (c (n "git-hook-commit-ref") (v "1.0.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "148xrsyqv1yx3mac5cb4nx8c618x8kcq4rp1qdwb64zcnc2j2nd4")))

(define-public crate-git-hook-commit-ref-1.1.0 (c (n "git-hook-commit-ref") (v "1.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1akgsxw0kp9ry79w992fzz67vhhy74dsglxcdh1363knv8w8b0wi")))

(define-public crate-git-hook-commit-ref-1.2.0 (c (n "git-hook-commit-ref") (v "1.2.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "12g4b0frym29amm4spffsdy120va38p3xy8hrq8z7vxyhw2m5lz9")))

(define-public crate-git-hook-commit-ref-1.3.0 (c (n "git-hook-commit-ref") (v "1.3.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "04fg4bgsv95azm0s8nkxqv3fpwij0fha8cyjqvk7lw0pnngkc94b") (y #t)))

(define-public crate-git-hook-commit-ref-1.3.1 (c (n "git-hook-commit-ref") (v "1.3.1") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "10fagzq316m1lzmnlq6yxs9ds4midpgvb11x2kffxjkskjjflb2k")))

(define-public crate-git-hook-commit-ref-1.4.0 (c (n "git-hook-commit-ref") (v "1.4.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "02jdq6fa0anii5kbwqa69f1g6d2lg0knjc0zx42n1r6qcy43is2b")))

(define-public crate-git-hook-commit-ref-1.4.1 (c (n "git-hook-commit-ref") (v "1.4.1") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1ryl30xsz0snjpsvpfkdk83fjvs15hs6wi7x4g72ns8yn6mhns0f")))

