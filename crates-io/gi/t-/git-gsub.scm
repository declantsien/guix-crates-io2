(define-module (crates-io gi t- git-gsub) #:use-module (crates-io))

(define-public crate-git-gsub-0.1.0 (c (n "git-gsub") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1ps9hqmip6jx1g3aa0vqjim9n5fv48d14iyafnn9wnhvsbq3hm7b")))

(define-public crate-git-gsub-0.1.1 (c (n "git-gsub") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "15m296iwpnkdfhg62pj3kligqyn44ldxpiih9mvhxvp03zhp6lp0")))

(define-public crate-git-gsub-0.1.2 (c (n "git-gsub") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0m5j46xvrbcg3dildyjxg164sbyvsbk9vfgihqkpnhg4plhyw6mh")))

(define-public crate-git-gsub-0.2.0 (c (n "git-gsub") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1sys9vzcz2jvgxzgj05qhrp2ycsna5xmkdv3mhfvyfb6kvv1z4i9")))

(define-public crate-git-gsub-0.2.1 (c (n "git-gsub") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1gjc8v4d6dl5vs5navlcwazskkgd5pjdfc6ps8djszyxvaah81j5")))

