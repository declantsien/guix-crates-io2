(define-module (crates-io gi t- git-anger-management) #:use-module (crates-io))

(define-public crate-git-anger-management-0.1.0 (c (n "git-anger-management") (v "0.1.0") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1s4b1vxc8h4v1jbx7lhygjw3fx1cpg3114bicli829rr30aphlxw") (f (quote (("fail-on-warnings"))))))

(define-public crate-git-anger-management-0.2.0 (c (n "git-anger-management") (v "0.2.0") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)))) (h "05fpk68v92rhhx93jjyfxhmy8mzcmsh5v0vh5sfvg71j0ki87g38") (f (quote (("fail-on-warnings"))))))

(define-public crate-git-anger-management-0.3.0 (c (n "git-anger-management") (v "0.3.0") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0pgffk4m8q9snmcl8jghq2vdqkla6jfmajh09b9i96p32cx5pjks") (f (quote (("fail-on-warnings"))))))

(define-public crate-git-anger-management-0.4.0 (c (n "git-anger-management") (v "0.4.0") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0g6yfmlbyrwxbafsf1k2ai7b120gf108cwqpjqv1zdkc6b4j7hbm") (f (quote (("fail-on-warnings"))))))

(define-public crate-git-anger-management-0.5.0 (c (n "git-anger-management") (v "0.5.0") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1m4nl7ydhp2nm3rr9nc69r33ba0rr3vflvc8fwggiz25hnb3ywi4") (f (quote (("fail-on-warnings"))))))

(define-public crate-git-anger-management-0.5.1 (c (n "git-anger-management") (v "0.5.1") (d (list (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0p6bdz9bm1cw9w4cgw9sy72vxpqmi9ki392as41snss1ldd7ydm3") (f (quote (("fail-on-warnings"))))))

(define-public crate-git-anger-management-0.6.0 (c (n "git-anger-management") (v "0.6.0") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0pd5b78lcy71a969jhav3grbns9gav3sibyllk0kzzgg5jc66i3r") (f (quote (("fail-on-warnings"))))))

(define-public crate-git-anger-management-0.7.0 (c (n "git-anger-management") (v "0.7.0") (d (list (d (n "console") (r "^0.11.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.5") (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "0c7s66wapxpzhy1h4fy70gia6pjxgyachkk29rjzh19xy6jy0sfm") (f (quote (("table" "tabwriter") ("json" "serde" "serde_json") ("default" "json" "table"))))))

