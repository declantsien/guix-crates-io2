(define-module (crates-io gi t- git-chlog) #:use-module (crates-io))

(define-public crate-git-chlog-0.1.0 (c (n "git-chlog") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "086h26hf7v65kqg3dhiwh6cmm4gi9zf9s43x4qhd4i9q26gvzjzi")))

