(define-module (crates-io gi t- git-point) #:use-module (crates-io))

(define-public crate-git-point-0.1.0 (c (n "git-point") (v "0.1.0") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("wrap_help" "derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.20") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "gix") (r "^0.62.0") (f (quote ("revision"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std"))) (d #t) (k 0)) (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)))) (h "1rcvfl6rskphy9fvv75hbhwxh1ncc88viihgvwky73v33blpjjbp")))

