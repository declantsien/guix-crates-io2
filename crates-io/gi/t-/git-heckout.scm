(define-module (crates-io gi t- git-heckout) #:use-module (crates-io))

(define-public crate-git-heckout-0.0.0 (c (n "git-heckout") (v "0.0.0") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (k 0)))) (h "1x4pw9653chlxq7y6yd65adx019aafh26lkmj3fg6nrnmh7j6hk2") (y #t)))

(define-public crate-git-heckout-0.0.2 (c (n "git-heckout") (v "0.0.2") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (k 0)))) (h "1mp8m16a1v9w1ps5pi8dgi7sbdmjjvyk1hspnxl4cc6xblkj6dnv")))

(define-public crate-git-heckout-0.1.0 (c (n "git-heckout") (v "0.1.0") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (k 0)))) (h "0i4qp5sqcajs1hqsdlzzv4ds517ixqkj4pzw84y1qbrig7sfss4k")))

(define-public crate-git-heckout-0.3.0 (c (n "git-heckout") (v "0.3.0") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (k 0)))) (h "1zh8nl26vwmhnsi2rj2vjd8pyv5sa9rxkmn4admk7r8zyi24rviq")))

(define-public crate-git-heckout-0.4.0 (c (n "git-heckout") (v "0.4.0") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (k 0)) (d (n "sublime_fuzzy") (r "^0.6.0") (d #t) (k 0)))) (h "16n6jljh8jgr0qaqzvw39bnv5ssbynw55ibp1vinkrigvyqi4d35")))

(define-public crate-git-heckout-0.4.1 (c (n "git-heckout") (v "0.4.1") (d (list (d (n "git2") (r "^0.8.0") (k 0)) (d (n "sublime_fuzzy") (r "^0.6.0") (d #t) (k 0)))) (h "1mah54q23a5rj3wp5sww7497ai1k06cr88jc9mm0gmm0bp5zpvpf")))

(define-public crate-git-heckout-0.5.0 (c (n "git-heckout") (v "0.5.0") (d (list (d (n "git2") (r "^0.9.1") (k 0)) (d (n "sublime_fuzzy") (r "^0.6.0") (d #t) (k 0)))) (h "0vj47fpqwsgbsjk3hx69a0kaf6gz5q63nfm9njn203q92yaf3a25")))

(define-public crate-git-heckout-0.5.1 (c (n "git-heckout") (v "0.5.1") (d (list (d (n "git2") (r "~0.11.0") (k 0)) (d (n "sublime_fuzzy") (r "~0.6.0") (d #t) (k 0)))) (h "1mg2vs6ph7zqd0h9vvvbsbljxdii0vf5a5mq5zl2hh9vbkl2vzjx")))

(define-public crate-git-heckout-0.6.0 (c (n "git-heckout") (v "0.6.0") (d (list (d (n "git2") (r "~0.13.17") (k 0)) (d (n "sublime_fuzzy") (r "~0.7.0") (d #t) (k 0)))) (h "16wih385ksghwkrg0lpq68k9was96xc10b0w7d8389zw4q8c4nb6")))

