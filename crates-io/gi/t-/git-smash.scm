(define-module (crates-io gi t- git-smash) #:use-module (crates-io))

(define-public crate-git-smash-0.0.1 (c (n "git-smash") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "1pw6rbc1qr5p8np4w84l4hr74yr0pydq4vargbk7f7dj1525s7n1")))

(define-public crate-git-smash-0.0.2 (c (n "git-smash") (v "0.0.2") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0k3liqsj6f425amzxw18v65fnb271s0gnn97n119an4zgzyflip1")))

(define-public crate-git-smash-0.1.0 (c (n "git-smash") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "02kvw6kc33g1nlrr78ri3rq4yaqfbszsyp7bdb6cn5n0hfx926g4")))

