(define-module (crates-io gi t- git-ignore-manager) #:use-module (crates-io))

(define-public crate-git-ignore-manager-0.1.0 (c (n "git-ignore-manager") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)))) (h "1n1gdmzknx0lgq2sxad53vx9z86pylryh4kcn3hav8p1nba2n841")))

(define-public crate-git-ignore-manager-0.1.1 (c (n "git-ignore-manager") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)))) (h "0j6ya6zxgscgkh8czm12zgxxx7z9crscvpkif6wp2r7jxv8qkch7")))

(define-public crate-git-ignore-manager-0.2.0 (c (n "git-ignore-manager") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)))) (h "1684q62ryrd9d68f0w9qsx5hjs8j6kqfkj0hiw73j6bamsdc3x2r")))

(define-public crate-git-ignore-manager-0.3.0 (c (n "git-ignore-manager") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)))) (h "0hm59s08kba2brjpj8f2q55w1y8s4g6vq0gffdb1xh1vcxr4hf76")))

