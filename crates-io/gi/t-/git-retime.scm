(define-module (crates-io gi t- git-retime) #:use-module (crates-io))

(define-public crate-git-retime-0.0.0 (c (n "git-retime") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1sxv4a6nwp49ksfrnjv04z6xcqnjrj8zw10hhrn422a1765q6944") (f (quote (("default"))))))

(define-public crate-git-retime-0.1.0 (c (n "git-retime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0c82pqf3sn8zrkdcdc4h267zla93fmgc462x3phw50569gah2h5a") (f (quote (("default"))))))

(define-public crate-git-retime-0.1.1 (c (n "git-retime") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git-utils") (r "^0.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wwzbil6rrx2fk4ji43klhdy61nglhp7ywipwgw8ynnp0wzga905") (f (quote (("default"))))))

