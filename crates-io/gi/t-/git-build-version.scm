(define-module (crates-io gi t- git-build-version) #:use-module (crates-io))

(define-public crate-git-build-version-0.1.0 (c (n "git-build-version") (v "0.1.0") (d (list (d (n "git2") (r "^0.3.3") (k 0)) (d (n "quick-error") (r "^0.1.4") (d #t) (k 0)))) (h "02cz433ciyhfpirhvj3dw7kl1vnba5pd47lp8qqcjrjvrr0zl15y")))

(define-public crate-git-build-version-0.1.1 (c (n "git-build-version") (v "0.1.1") (d (list (d (n "git2") (r "^0.3.3") (k 0)) (d (n "quick-error") (r "^0.1.4") (d #t) (k 0)))) (h "1h5bdazdhhwmwpf46bya0ng8k8635z9zmjjg8hwk7fm7spd1ina4")))

(define-public crate-git-build-version-0.1.2 (c (n "git-build-version") (v "0.1.2") (d (list (d (n "git2") (r "^0.4") (k 0)) (d (n "quick-error") (r "^0.1.4") (d #t) (k 0)))) (h "07pg79n2gfgbxflr0cvpyvm424zz1cm9f5324ppxx64y42qmzdf6")))

