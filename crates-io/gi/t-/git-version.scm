(define-module (crates-io gi t- git-version) #:use-module (crates-io))

(define-public crate-git-version-0.2.1 (c (n "git-version") (v "0.2.1") (h "19j45kqksjm1i4g4fqj2s5yfrq68n4i5gc1ln6j55n2h3w13gg51")))

(define-public crate-git-version-0.3.0 (c (n "git-version") (v "0.3.0") (d (list (d (n "git-version-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "19hjsmwjc40r7g5a6nyhdw750cd4dgq336zd2jajxmdk650c5j91")))

(define-public crate-git-version-0.3.1 (c (n "git-version") (v "0.3.1") (d (list (d (n "git-version-macro") (r "= 0.3.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0j5j033x62n1m2bxpf4ca6q2fird3zlby3mqp5yqpcv59jm08brh")))

(define-public crate-git-version-0.3.2 (c (n "git-version") (v "0.3.2") (d (list (d (n "git-version-macro") (r "= 0.3.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0x0y9iq2vh0pnkhx5ax136mw2jfai6k0fs5mb2zlyyml44kg6y1f")))

(define-public crate-git-version-0.3.3 (c (n "git-version") (v "0.3.3") (d (list (d (n "git-version-macro") (r "= 0.3.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1b34k7inz1rzyx6ajqfxg7vpqbz7fqil8jx4hwippqpq9ncfbirf")))

(define-public crate-git-version-0.3.4 (c (n "git-version") (v "0.3.4") (d (list (d (n "git-version-macro") (r "= 0.3.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1qj1rd19v8vg094b3fj0gy6ca53v93lhrl31wg1fs7g0y61qx4cl")))

(define-public crate-git-version-0.3.5 (c (n "git-version") (v "0.3.5") (d (list (d (n "git-version-macro") (r "=0.3.5") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "168qqvam36ipz35978yzzrzaf8mpfyzdr46kraf6nqzl0b6dxc7n")))

(define-public crate-git-version-0.3.6 (c (n "git-version") (v "0.3.6") (d (list (d (n "git-version-macro") (r "=0.3.6") (d #t) (k 0)))) (h "1fqnamg851v145vfhb8g7hsx56di8hwq33wlp77ni3i1q7c7611z")))

(define-public crate-git-version-0.3.7 (c (n "git-version") (v "0.3.7") (d (list (d (n "git-version-macro") (r "=0.3.6") (d #t) (k 0)))) (h "1xripipw7d2mc0k6adj063nww9332yfw2l8l6cs1lchgirka0q70") (y #t)))

(define-public crate-git-version-0.3.8 (c (n "git-version") (v "0.3.8") (d (list (d (n "git-version-macro") (r "=0.3.8") (d #t) (k 0)))) (h "0z55zpm36pvahr8sskx0hn445bg7lfrgzminp7l7y7r2m3zh3b8k")))

(define-public crate-git-version-0.3.9 (c (n "git-version") (v "0.3.9") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "git-version-macro") (r "=0.3.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "06ddi3px6l2ip0srn8512bsh8wrx4rzi65piya0vrz5h7nm6im8s")))

