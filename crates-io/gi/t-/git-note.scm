(define-module (crates-io gi t- git-note) #:use-module (crates-io))

(define-public crate-git-note-0.0.0 (c (n "git-note") (v "0.0.0") (h "1j8l8abdflmc1qhj92cysib0kshh248fvjbwd58kasm0gfwb0dwl")))

(define-public crate-git-note-0.1.0 (c (n "git-note") (v "0.1.0") (h "0g6q1qp1h2xd8pszm3s8vwjhzlr3fj0hgklgmav6a255qxfdh7gh") (r "1.64")))

