(define-module (crates-io gi t- git-bfg) #:use-module (crates-io))

(define-public crate-git-bfg-0.1.0 (c (n "git-bfg") (v "0.1.0") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.7.0") (d #t) (k 0)))) (h "02m4pbr4hqvff7jr818p739ncm6d91d91a565kag0g749y09pl4l") (f (quote (("default"))))))

(define-public crate-git-bfg-0.1.1 (c (n "git-bfg") (v "0.1.1") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.7.0") (d #t) (k 0)))) (h "06nwaaki6mzq995bk22p6g3bv5gwiwfwyzj5c5jrl61fz8v5fx85") (f (quote (("default"))))))

(define-public crate-git-bfg-0.2.0 (c (n "git-bfg") (v "0.2.0") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.7.0") (d #t) (k 0)))) (h "0w5pr99rdwjscf3y3hzrrzdk1dgqla4d92z67c7nkpd0vl754ll2") (f (quote (("default"))))))

