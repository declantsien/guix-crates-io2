(define-module (crates-io gi t- git-view) #:use-module (crates-io))

(define-public crate-git-view-0.1.0 (c (n "git-view") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "mockall") (r "^0.11.1") (d #t) (k 2)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.7.1") (d #t) (k 0)))) (h "1nbz4zrzjbpbv21mk9ih13n1lp9dn7r740k2ssh01kwqxkv9v2q4")))

(define-public crate-git-view-1.0.0 (c (n "git-view") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.25") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (f (quote ("nightly"))) (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "1sn36d49f8ic4sar26blis83anf1s7h1mnr66xm8a1sri4s9viw8")))

