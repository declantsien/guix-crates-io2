(define-module (crates-io gi t- git-sequencer) #:use-module (crates-io))

(define-public crate-git-sequencer-0.0.0 (c (n "git-sequencer") (v "0.0.0") (h "1d3ian9wv75xb910gfllz0shxxqgai5kb4p2dxz31pnl1dr44n4y")))

(define-public crate-git-sequencer-0.1.0 (c (n "git-sequencer") (v "0.1.0") (h "0qbxyqkzl1lisb81nhmjz2jbr8x8dv4hlqlxwaw5f9yrsdqhki5z") (r "1.64")))

