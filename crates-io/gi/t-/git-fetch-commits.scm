(define-module (crates-io gi t- git-fetch-commits) #:use-module (crates-io))

(define-public crate-git-fetch-commits-0.1.0 (c (n "git-fetch-commits") (v "0.1.0") (d (list (d (n "byte-unit") (r "^5.1.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "10rv3aqbgbb4q2xrmypw06cp9yfzb0m1zs2wddr15wpjwhk8adlw")))

