(define-module (crates-io gi t- git-branch-history) #:use-module (crates-io))

(define-public crate-git-branch-history-0.1.0 (c (n "git-branch-history") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1wsqlwrd5wm561f3qcvhkmdn9xjbv242imgi9wk3g0hf6bxd8pqy")))

