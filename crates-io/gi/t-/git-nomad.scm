(define-module (crates-io gi t- git-nomad) #:use-module (crates-io))

(define-public crate-git-nomad-0.1.0 (c (n "git-nomad") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "whoami") (r "^1") (d #t) (k 0)))) (h "0s16a9b9qh3xcvvm9p5nv3zgckjh3xl08mfqyksfi0bf5r2b05kp")))

(define-public crate-git-nomad-0.1.1 (c (n "git-nomad") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "whoami") (r "^1") (d #t) (k 0)))) (h "1m9bcpjbsb2d3mcifxs0idzzkxmz09nvg69lviwq9jv62jk46gb7")))

(define-public crate-git-nomad-0.2.0 (c (n "git-nomad") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "whoami") (r "^1.1.5") (d #t) (k 0)))) (h "08k3yzcvvk63wm77xaqk9z05gyf8m8935y2s6n3xa1dvifxz2xvp")))

(define-public crate-git-nomad-0.2.1 (c (n "git-nomad") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "whoami") (r "^1.1.5") (d #t) (k 0)))) (h "0p4ydffmshgfr1j5980s2rilz5la0jpp6bymhprczcf1nifx7jg2")))

(define-public crate-git-nomad-0.3.0 (c (n "git-nomad") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "1hic1dbfwd0iqhydwy225h8w3n2pl7izrgxvk7sy2iiplxbkczmf")))

(define-public crate-git-nomad-0.3.1 (c (n "git-nomad") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "0sn234xlw6b4n36pm9skmic9g0wz8s803j7krfxr1fz8xi85y105")))

(define-public crate-git-nomad-0.3.2 (c (n "git-nomad") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "0irqy6j0by61xkzrybq13vd6b2l19fdxw38w6jaib6mbmyr3g601")))

(define-public crate-git-nomad-0.4.0 (c (n "git-nomad") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "11ydxng714c6qriv0vk4nzz3105c7si6j06wpimf3m6fd9hycab3")))

(define-public crate-git-nomad-0.5.0 (c (n "git-nomad") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (f (quote ("cargo" "env" "std" "suggestions" "wrap_help"))) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "1d7nki8qv8yyl78zzfbcs0ixakqz23pjjfiya9jfmd5wfz2682ds")))

(define-public crate-git-nomad-0.6.0 (c (n "git-nomad") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.11") (f (quote ("std" "help" "usage" "error-context" "suggestions" "cargo" "env" "wrap_help" "string"))) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "0zcb2pzqm8r75rdz5pzh80fcsvzx4iq2wxqpw61r8k86p4lv258d")))

(define-public crate-git-nomad-0.7.0 (c (n "git-nomad") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("std" "help" "usage" "error-context" "suggestions" "cargo" "env" "wrap_help" "string"))) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "0grq21zdlkzvxx0b1m97cc09zg8q11ay2df2n0g5n7ph1fwsz9cv")))

(define-public crate-git-nomad-0.7.1 (c (n "git-nomad") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("std" "help" "usage" "error-context" "suggestions" "cargo" "env" "wrap_help" "string"))) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "08z5dkfq94lhm3jg448qy72hrp8yiqs8k4rgwvblgv2xspd5jx0n")))

