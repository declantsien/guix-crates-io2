(define-module (crates-io gi t- git-const) #:use-module (crates-io))

(define-public crate-git-const-1.0.0 (c (n "git-const") (v "1.0.0") (h "0bdrp5fra3b72fsbniqhkfrb1xzb16jkamp6qngvx63vvr19wp79")))

(define-public crate-git-const-1.1.0 (c (n "git-const") (v "1.1.0") (h "1xriggcaq3z4d9846q6r0p23dg3inwfzcihvq6y92gfr1k666kxa")))

