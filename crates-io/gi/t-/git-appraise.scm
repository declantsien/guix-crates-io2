(define-module (crates-io gi t- git-appraise) #:use-module (crates-io))

(define-public crate-git-appraise-0.1.0 (c (n "git-appraise") (v "0.1.0") (d (list (d (n "git2") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.29") (d #t) (k 1)))) (h "14v40jw45pvizsmw6zdj9b4iyhvf6f46snk4p8h696plpl3m4zvy") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-git-appraise-0.1.1 (c (n "git-appraise") (v "0.1.1") (d (list (d (n "git2") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.29") (d #t) (k 1)))) (h "1vviaawccd1jps8f0n31fz3rnf7h8132k7haaljvgl68gry85f65") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-git-appraise-0.2.0 (c (n "git-appraise") (v "0.2.0") (d (list (d (n "git2") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.32") (d #t) (k 1)))) (h "0mqlwwgzmkfpzzchxhcbhz8f8kpn2wa91rvwvfad01zdjc8jcdba") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

