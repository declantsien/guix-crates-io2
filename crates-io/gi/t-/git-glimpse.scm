(define-module (crates-io gi t- git-glimpse) #:use-module (crates-io))

(define-public crate-git-glimpse-0.1.0 (c (n "git-glimpse") (v "0.1.0") (h "0d8jp7c0nz9lg6y07jb0ic29lsarn5d6q97zqh7jfs8mf7g58nvh")))

(define-public crate-git-glimpse-0.2.0 (c (n "git-glimpse") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "13118rdl2hc39n0ldih1ybarjw8sygs6vplcwhm7jybyz4l1k5gh")))

(define-public crate-git-glimpse-0.3.0 (c (n "git-glimpse") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "06gk116fnf5832549jhnlz1rcb0mniw6dzlbmnrqlm19az6iiyih")))

(define-public crate-git-glimpse-0.3.1 (c (n "git-glimpse") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1a9amxn12kkixw1jmi07l037dpj35f1vk6xms6shx0n38ddd2hjg")))

(define-public crate-git-glimpse-0.3.2 (c (n "git-glimpse") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0vjvw3alpx817dning1v8rmj8h9dqhcqz0qnblqxildkql8yggrc")))

(define-public crate-git-glimpse-0.4.0 (c (n "git-glimpse") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0x0irxb2607gwmsfb6qm1k5mmlsk0l5xswwb00lcazzj2hi11hkz")))

(define-public crate-git-glimpse-0.4.1 (c (n "git-glimpse") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "00zanmk98g6zm0yb089qb2zb8mvbc3abs3988kvpxpy0fp3r1pck")))

(define-public crate-git-glimpse-0.4.2 (c (n "git-glimpse") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1n5psnrz3nsclpx4dxmf588b2g9swd07074jszgbl1w730zc9h0p")))

(define-public crate-git-glimpse-0.4.3 (c (n "git-glimpse") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1rxdg4v30xglhlv4wrifp2wkjfqidhwk7b49znlspv47k6m2qzfs")))

(define-public crate-git-glimpse-0.4.4 (c (n "git-glimpse") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0h1m1yx16x25jh9ivb8i97vxiih6bs24br5m3b50xks2rmbsr4xs")))

(define-public crate-git-glimpse-0.4.5 (c (n "git-glimpse") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ezcmd") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1ya6y6dmhpsgvhg1hp4rfdp1qzyi7sgcrab3zi5pa3c1hz3wxdra")))

