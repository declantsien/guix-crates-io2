(define-module (crates-io gi t- git-changes) #:use-module (crates-io))

(define-public crate-git-changes-0.1.0 (c (n "git-changes") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "markdown-composer") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("registry" "env-filter"))) (d #t) (k 0)))) (h "1j60ihw104kmfgrqryf0mp3d9qg4m3jjfnjl9w6s396lxwc8bsdh")))

