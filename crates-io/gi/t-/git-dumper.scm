(define-module (crates-io gi t- git-dumper) #:use-module (crates-io))

(define-public crate-git-dumper-0.1.0 (c (n "git-dumper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "http2" "client" "runtime"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("net" "sync" "rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "076975kpp1hib06flsqw73krh9i4zv7pd4vxndg0mxhffx2kca97")))

