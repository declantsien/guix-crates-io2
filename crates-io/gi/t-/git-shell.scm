(define-module (crates-io gi t- git-shell) #:use-module (crates-io))

(define-public crate-git-shell-0.1.0 (c (n "git-shell") (v "0.1.0") (d (list (d (n "dia-semver") (r ">=9, <10") (d #t) (k 0)))) (h "1bdxs0vi9nnhcym4qm2czw3ajbk3spwm32pfb912ki89m8hxq9h7") (y #t)))

(define-public crate-git-shell-0.1.1 (c (n "git-shell") (v "0.1.1") (d (list (d (n "dia-semver") (r ">=9, <10") (d #t) (k 0)))) (h "1dqc3mqqf2a1ng3ginyamvxkkyw56z24dnsqvnmn9hj2mxzmvxzs") (y #t)))

(define-public crate-git-shell-0.1.2 (c (n "git-shell") (v "0.1.2") (d (list (d (n "dia-semver") (r ">=9, <10") (d #t) (k 0)))) (h "1gd53if4ldd0cwl8vzzi9z4mjik21z6vqxca8dyikv40gvqrzzc3") (y #t)))

(define-public crate-git-shell-0.1.3 (c (n "git-shell") (v "0.1.3") (d (list (d (n "dia-semver") (r ">=9, <10") (d #t) (k 0)))) (h "1b3pwqi5b78df84rvz5s9xfwxl74birzzjrra5z3l9vw51mbmizl") (y #t)))

(define-public crate-git-shell-0.1.4 (c (n "git-shell") (v "0.1.4") (d (list (d (n "dia-semver") (r ">=9, <10") (d #t) (k 0)))) (h "0k471n0wkj05n8n7aaazqcjqmrgc09hhabv96a0bjp7pbwwb0xmr") (y #t)))

(define-public crate-git-shell-0.1.5 (c (n "git-shell") (v "0.1.5") (d (list (d (n "dia-semver") (r ">=9, <10") (d #t) (k 0)))) (h "1z26yvvsj7l6fh2qqgpxh6jva9d3gzp36r0jy53g5zkx3ayb6jn0") (y #t)))

(define-public crate-git-shell-0.1.6+main (c (n "git-shell") (v "0.1.6+main") (d (list (d (n "dia-semver") (r ">=9, <10") (d #t) (k 0)))) (h "0nxr4r54abgz3mg67rrgspz1c2lgd0crz592ppljacy7mq5yliwq") (y #t)))

(define-public crate-git-shell-0.1.7+main (c (n "git-shell") (v "0.1.7+main") (d (list (d (n "dia-semver") (r ">=9, <10") (d #t) (k 0)))) (h "0igxci2a3yhxrqmvaaab0ca8jz7kk7x802fd4zkbhv2y88n5yjca") (y #t)))

(define-public crate-git-shell-0.2.0 (c (n "git-shell") (v "0.2.0") (d (list (d (n "dia-semver") (r ">=9.0.1, <10") (d #t) (k 0)))) (h "0x70vfzw7bcgicj895g5chg3facsknkmwrgc2q75wpgb62yq0mvz") (y #t)))

(define-public crate-git-shell-0.3.0 (c (n "git-shell") (v "0.3.0") (d (list (d (n "dia-semver") (r ">=9.0.1, <10") (d #t) (k 0)))) (h "142jb305lkq3rf0prkpbc0wypsncq3bk3lp80m9h1sybh6m1ixap") (y #t)))

(define-public crate-git-shell-0.3.1 (c (n "git-shell") (v "0.3.1") (d (list (d (n "dia-semver") (r ">=9.0.1, <10") (d #t) (k 0)))) (h "1p717jas1r1in6bya1pg2j9i4zrhbiv3cqq2z4wmvhg3idak6zk2") (y #t)))

