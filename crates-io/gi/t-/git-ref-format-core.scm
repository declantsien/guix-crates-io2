(define-module (crates-io gi t- git-ref-format-core) #:use-module (crates-io))

(define-public crate-git-ref-format-core-0.1.0 (c (n "git-ref-format-core") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "minicbor") (r "^0.13") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rfyml5pxpwx3yb9lf71igzzsimxlqy8pajclrcdavmxhf4m0dv9") (f (quote (("link-literals"))))))

(define-public crate-git-ref-format-core-0.2.0 (c (n "git-ref-format-core") (v "0.2.0") (d (list (d (n "bstr") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "minicbor") (r "^0.13") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dk4yqzlw5iygh6qb5r0piswkbilprcnw7a6l63srl2c3p7lrgqw")))

(define-public crate-git-ref-format-core-0.2.1 (c (n "git-ref-format-core") (v "0.2.1") (d (list (d (n "bstr") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "minicbor") (r "^0.13") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15bdpxnwghw3mrj2hmmq4lh72ffh0mrhx0i82ddl4yfd0r37z4mp")))

(define-public crate-git-ref-format-core-0.2.3 (c (n "git-ref-format-core") (v "0.2.3") (d (list (d (n "bstr") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "minicbor") (r "^0.13") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i84jmkhq2xc6nxmb0kawm8gnyaqxvlsp0qqqlh8vczhnp3d5ljl")))

(define-public crate-git-ref-format-core-0.3.0 (c (n "git-ref-format-core") (v "0.3.0") (d (list (d (n "bstr") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "minicbor") (r "^0.13") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10h9wld5hvv1znld4ys0g6wzidyc1qxq535rmhi5gfk3vjfm9dpb")))

