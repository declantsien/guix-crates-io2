(define-module (crates-io gi t- git-together-ssh) #:use-module (crates-io))

(define-public crate-git-together-ssh-0.1.1 (c (n "git-together-ssh") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)))) (h "1gcnn9vsm044h6w91raz0iv13053rabxx5kzvn4x475bxahbpnr5")))

(define-public crate-git-together-ssh-0.2.0 (c (n "git-together-ssh") (v "0.2.0") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)))) (h "1a6nm002kdvv3sld68ar7n5lyr8351dmlyb7xfyas78p0fg7z0f5")))

(define-public crate-git-together-ssh-0.2.1 (c (n "git-together-ssh") (v "0.2.1") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "git2") (r "=0.17.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)))) (h "1095klkzhwpwvczp0nln4cmh8vdbc5gb230l3h7cf6av7n5f65qi")))

