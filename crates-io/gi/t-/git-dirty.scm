(define-module (crates-io gi t- git-dirty) #:use-module (crates-io))

(define-public crate-git-dirty-0.1.0 (c (n "git-dirty") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1hcr98s5mxvmrgzhb7ajfz88g0k81mwq73yird2kir2ziawspm0w")))

