(define-module (crates-io gi t- git-sweep) #:use-module (crates-io))

(define-public crate-git-sweep-1.0.0 (c (n "git-sweep") (v "1.0.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "trash") (r "^3.1.2") (d #t) (k 0)))) (h "0dssvk0rq5wvha2r3hhcg9l4yfdgwy91gql66f9s1207ydg6jy6k")))

