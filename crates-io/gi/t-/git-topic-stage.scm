(define-module (crates-io gi t- git-topic-stage) #:use-module (crates-io))

(define-public crate-git-topic-stage-1.0.0 (c (n "git-topic-stage") (v "1.0.0") (d (list (d (n "chrono") (r "~0.2") (d #t) (k 0)) (d (n "error-chain") (r "~0.7") (d #t) (k 0)) (d (n "git-workarea") (r "~1.0") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0cy86qd6mxla015ky0g5jslj90gl2f83agvfsb4d5xw7bm9ddhyp")))

(define-public crate-git-topic-stage-2.0.0 (c (n "git-topic-stage") (v "2.0.0") (d (list (d (n "chrono") (r "~0.3") (d #t) (k 0)) (d (n "error-chain") (r "~0.9") (d #t) (k 0)) (d (n "git-workarea") (r "~2.0") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0mh2n4gnz4cl1xvcbsf77d8q01bkn5az63944sldwp5lw5s7qfbp")))

(define-public crate-git-topic-stage-2.0.1 (c (n "git-topic-stage") (v "2.0.1") (d (list (d (n "chrono") (r "~0.3") (d #t) (k 0)) (d (n "error-chain") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "git-workarea") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "12d1jjv4g40rh4vhll3363r698k9p7q0cn5zjczcvy0i712wq4pb")))

(define-public crate-git-topic-stage-3.0.0 (c (n "git-topic-stage") (v "3.0.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "git-workarea") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "1dz5vgb6fawc5kd81bcxkrh06bjiss0zkkvkcjd2d4449y1kfz4v")))

(define-public crate-git-topic-stage-4.0.0 (c (n "git-topic-stage") (v "4.0.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "git-workarea") (r "^4.0") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)))) (h "11x14dw7r7sj0bv1cw9ylpmzmh1gdd6kb3ys7g0mv9gd17gg48sf")))

(define-public crate-git-topic-stage-4.1.0 (c (n "git-topic-stage") (v "4.1.0") (d (list (d (n "chrono") (r "~0.4.19") (f (quote ("clock"))) (k 0)) (d (n "git-workarea") (r "^4.2") (d #t) (k 0)) (d (n "log") (r "~0.4.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "1ziv0x1510hyk783yxyj5q0kp1196msh16dmqlpzzkikfk6jznq8")))

(define-public crate-git-topic-stage-4.1.1 (c (n "git-topic-stage") (v "4.1.1") (d (list (d (n "chrono") (r "~0.4.23") (f (quote ("clock"))) (k 0)) (d (n "git-workarea") (r "^4.2.2") (d #t) (k 0)) (d (n "log") (r "~0.4.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "1w9p1sjjz481ick2lzqqwm5jw48wdn77a9bvd0vccb1f0czw2ixp")))

