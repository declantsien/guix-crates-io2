(define-module (crates-io gi t- git-commit-stats) #:use-module (crates-io))

(define-public crate-git-commit-stats-0.1.0 (c (n "git-commit-stats") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "03khq1sc23v1bgixn2a2hiaw814hyrnbp1ly3rgflrra5npp545b")))

(define-public crate-git-commit-stats-0.1.1 (c (n "git-commit-stats") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "19nm3j1m9w4ysdm7j7913q6plra3x8rlgqsj6g14i5c1qx8lz9wr")))

(define-public crate-git-commit-stats-0.1.2 (c (n "git-commit-stats") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "17j7hb04z39chj3xxjxpnyd4nf4jl51245kcj5l4x2253dq1pzxi")))

(define-public crate-git-commit-stats-0.1.3 (c (n "git-commit-stats") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "0adq7727a6f2b4v34pibi31bgarp016yqb859kznzcmi7d5wk661")))

(define-public crate-git-commit-stats-0.1.4 (c (n "git-commit-stats") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "1f2iyyykxhp5rfbjh43j6940lhzsk6xryl7dfilfrfbkd8z55xlf")))

(define-public crate-git-commit-stats-0.1.5 (c (n "git-commit-stats") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "11blg2lklv76bgjf8h634vqd6vljwb6ni1fwprz6rhz8s384l3ch")))

(define-public crate-git-commit-stats-0.1.6 (c (n "git-commit-stats") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "00zg54ww5xgrbfw7z476xpz1k854hyzgw1i7ryngh42j17n1jbvf")))

(define-public crate-git-commit-stats-0.1.7 (c (n "git-commit-stats") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "13dw8nzlv2f432y4yygf1j068dncj7yz1isbrwmrbi7wljqqrwvf")))

(define-public crate-git-commit-stats-0.1.8 (c (n "git-commit-stats") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1izvcrkx6352f8iakddp56szbanrz2z0isaf7hpz9d7inxx34vc8")))

(define-public crate-git-commit-stats-0.1.9 (c (n "git-commit-stats") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0c4kqdkmx1k1w80k62j7rbqqrn38i9y9wac8y2wjvswxsawfpnkh")))

(define-public crate-git-commit-stats-0.1.10 (c (n "git-commit-stats") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0g7wlbmdjzb3c32053p2y6xpnx3xmps99az8yp1jwm7qv3m19fc9")))

(define-public crate-git-commit-stats-0.1.11 (c (n "git-commit-stats") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0mbpfklg7qn1djnnl0pia2wmfr2qda4lsn9d2d9y3l69rxhkn0dk")))

(define-public crate-git-commit-stats-0.1.12 (c (n "git-commit-stats") (v "0.1.12") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1vlqlhvsldfj6gnka2bjrn93blikm0ak547nfmywh321bwjn49z7")))

