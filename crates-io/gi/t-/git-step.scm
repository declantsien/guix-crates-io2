(define-module (crates-io gi t- git-step) #:use-module (crates-io))

(define-public crate-git-step-0.1.0 (c (n "git-step") (v "0.1.0") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "03xh96s5q76kgyjdiqql3y6njza7xpssqjdcyvqs416bjf0i2cy6") (y #t)))

(define-public crate-git-step-0.1.1 (c (n "git-step") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)) (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "rexpect") (r "^0.5.0") (d #t) (k 2)))) (h "1if0pl0b74wfb1q4gjq6pm9l1h4zfz2ziwwc107mmgxv31144h4d")))

