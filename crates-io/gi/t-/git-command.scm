(define-module (crates-io gi t- git-command) #:use-module (crates-io))

(define-public crate-git-command-0.0.0 (c (n "git-command") (v "0.0.0") (h "0gadgsjxr8aqd7k60r4avaqdx7hgxi7f3kbpk2bb43x486k1mm21")))

(define-public crate-git-command-0.1.0 (c (n "git-command") (v "0.1.0") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)))) (h "0hnw25pwhf3ds3nrnr5akn33wk6hs1vj5w66v97malb5gfch2jwy")))

(define-public crate-git-command-0.2.0 (c (n "git-command") (v "0.2.0") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)))) (h "17kv1ng08rnik14rddyhz7vk1gf2fvaiavhadhr9pxzy29iqmfd6")))

(define-public crate-git-command-0.2.1 (c (n "git-command") (v "0.2.1") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)))) (h "1bz2lpgm66sy68lajws1p6hl0g1z1lx1aaw7z735p9462v64al91")))

(define-public crate-git-command-0.2.2 (c (n "git-command") (v "0.2.2") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)))) (h "1vvw3xfvcwc20x739kxbrgv7q03arzv22im1nar6jj8bzhggw6as")))

(define-public crate-git-command-0.2.3 (c (n "git-command") (v "0.2.3") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)))) (h "15cbz0y450g2wvia3hk2vp7n3fgrvfyyslljhaplxm2jwq0hjgzm") (r "1.64")))

(define-public crate-git-command-0.2.4 (c (n "git-command") (v "0.2.4") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)))) (h "0wx2j4xfgmz6r681whl8wk0jnh3hq64zgavfby3c4zjbv73r2lkv") (r "1.64")))

