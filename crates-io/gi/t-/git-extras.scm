(define-module (crates-io gi t- git-extras) #:use-module (crates-io))

(define-public crate-git-extras-0.0.0 (c (n "git-extras") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1wkdsxs2qh4lkssmf4rbnw0iszz3rfv0vl1ydsz3sy3g545sfkv0") (f (quote (("git-alias") ("default" "git-alias")))) (y #t)))

