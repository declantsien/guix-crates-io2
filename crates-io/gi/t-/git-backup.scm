(define-module (crates-io gi t- git-backup) #:use-module (crates-io))

(define-public crate-git-backup-0.1.1 (c (n "git-backup") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)))) (h "1s2iczhn1sk7jj33sqj5h12yl5nhpkmkg88iia7s3298b9zlsc4c")))

