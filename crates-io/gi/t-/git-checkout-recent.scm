(define-module (crates-io gi t- git-checkout-recent) #:use-module (crates-io))

(define-public crate-git-checkout-recent-0.1.0 (c (n "git-checkout-recent") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.0.11") (d #t) (k 0)) (d (n "git2") (r "^0.13.5") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.4") (d #t) (k 0)))) (h "1194kha2ch3hqpjydps5vjq4gdpz9mbmaf2300k9myq2jnv2mcxv")))

(define-public crate-git-checkout-recent-0.1.1 (c (n "git-checkout-recent") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.0.11") (d #t) (k 0)) (d (n "git2") (r "^0.13.5") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.4") (d #t) (k 0)))) (h "0hqw66mg59lp6ya6fl32jiy2ivqxj06pvixlsv7k0pvlwgz8qwnh")))

(define-public crate-git-checkout-recent-0.1.2 (c (n "git-checkout-recent") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.0.11") (d #t) (k 0)) (d (n "git2") (r "^0.13.5") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.4") (d #t) (k 0)))) (h "1j9v99pg8dl1qr1b1crrzb4726frq4wkpi17sg5sjqk0i30s8r91")))

