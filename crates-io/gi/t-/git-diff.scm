(define-module (crates-io gi t- git-diff) #:use-module (crates-io))

(define-public crate-git-diff-0.0.0 (c (n "git-diff") (v "0.0.0") (h "1y2wh020qfx1bpv3sxfspqh7kk0if6bqpgwzxvp672br5lv5dy0h")))

(define-public crate-git-diff-0.1.0 (c (n "git-diff") (v "0.1.0") (d (list (d (n "git-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "git-object") (r "^0.8") (d #t) (k 0)) (d (n "git-odb") (r "^0.12") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1hsh15mvmh1mqhband2952pr07bc8pgy1mgjlcljklg6ii0b1dh2")))

(define-public crate-git-diff-0.2.0 (c (n "git-diff") (v "0.2.0") (d (list (d (n "git-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "git-object") (r "^0.8") (d #t) (k 0)) (d (n "git-odb") (r "^0.14") (d #t) (k 2)) (d (n "git-traverse") (r "^0.1") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "18hdpp6d5vqx7v6jisq4ldij0pm4vjkvajv98gs5ibnp4y4gbgbx")))

(define-public crate-git-diff-0.3.0 (c (n "git-diff") (v "0.3.0") (d (list (d (n "git-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "git-object") (r "^0.9") (d #t) (k 0)) (d (n "git-odb") (r "^0.15") (d #t) (k 2)) (d (n "git-traverse") (r "^0.2") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0il60qy4ihsy8sq4paaanfw3y5l6258wk7b8d8vzkqys4my8nvgl")))

(define-public crate-git-diff-0.4.0 (c (n "git-diff") (v "0.4.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.10") (d #t) (k 0)) (d (n "git-odb") (r "^0.16") (d #t) (k 2)) (d (n "git-traverse") (r "^0.3") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1x5a6isa9k1q3s4ifb626w779zvigacd7h97531gqm7rc7sypf5g")))

(define-public crate-git-diff-0.4.1 (c (n "git-diff") (v "0.4.1") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.10") (d #t) (k 0)) (d (n "git-odb") (r "^0.16") (d #t) (k 2)) (d (n "git-traverse") (r "^0.3") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "05qwys7yvd4917rrwn2kl582894v9zc831zxi54wx48ylswzyzyh")))

(define-public crate-git-diff-0.5.0 (c (n "git-diff") (v "0.5.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.11") (d #t) (k 0)) (d (n "git-odb") (r "^0.16") (d #t) (k 2)) (d (n "git-traverse") (r "^0.4") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0v6kkazjgcl1wd4xncxif26szg2w6sp59qcfb7mjxcz3lir9jbfk")))

(define-public crate-git-diff-0.6.0 (c (n "git-diff") (v "0.6.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.11") (d #t) (k 0)) (d (n "git-odb") (r "^0.17") (d #t) (k 2)) (d (n "git-traverse") (r "^0.5") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0ms5y03l6sn3r6xryx838ni6g63ycpf7m5wiqj1fpq7bxvqck62i")))

(define-public crate-git-diff-0.8.0 (c (n "git-diff") (v "0.8.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.12.0") (d #t) (k 0)) (d (n "git-odb") (r "^0.20.0") (d #t) (k 2)) (d (n "git-traverse") (r "^0.7.0") (d #t) (k 2)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "12fhgk36bfwwd70hsacrkqbl45b90s3234m1n9xfsxpx5np75g0n")))

(define-public crate-git-diff-0.8.1 (c (n "git-diff") (v "0.8.1") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "07z9xpbqzc91xaichl8zlpxa01mfa82lza8av3bf24jmmbb4w4q5")))

(define-public crate-git-diff-0.8.2 (c (n "git-diff") (v "0.8.2") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.12.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0c0iyjbzyhzxcyglc6y9g5jhgzcpz8xxgwr1wnvwfqz9xbjbxz1q")))

(define-public crate-git-diff-0.9.0 (c (n "git-diff") (v "0.9.0") (d (list (d (n "git-hash") (r "^0.5.0") (d #t) (k 0)) (d (n "git-object") (r "^0.13.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1i6c6ig2aljkg7fn4qm6anr7dcy4rld1z25lgygwkm0jrfxz0nj9")))

(define-public crate-git-diff-0.9.1 (c (n "git-diff") (v "0.9.1") (d (list (d (n "git-hash") (r "^0.6.0") (d #t) (k 0)) (d (n "git-object") (r "^0.13.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0rz7vy4vbfyz3w1hpmxsig4g53wbsv2kyjg2dikwbc5ridz3amki")))

(define-public crate-git-diff-0.9.2 (c (n "git-diff") (v "0.9.2") (d (list (d (n "git-hash") (r "^0.6.0") (d #t) (k 0)) (d (n "git-object") (r "^0.14.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0k7bmx27xnjl9y9712pa8pikb0r0y7y4bap9j17jlr9qqz4xhih2")))

(define-public crate-git-diff-0.10.0 (c (n "git-diff") (v "0.10.0") (d (list (d (n "git-hash") (r "^0.7.0") (d #t) (k 0)) (d (n "git-object") (r "^0.14.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0w5prjww136ydywa5ah4n2bvcwpw4h81mbahhawkrs89wfmb38nc")))

(define-public crate-git-diff-0.11.0 (c (n "git-diff") (v "0.11.0") (d (list (d (n "git-hash") (r "^0.8.0") (d #t) (k 0)) (d (n "git-object") (r "^0.15.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "13ikddpgj7hgsqy6xs0x6dsaq79j70n4lbc5qd8yi4b299p5k3fp")))

(define-public crate-git-diff-0.11.1 (c (n "git-diff") (v "0.11.1") (d (list (d (n "git-hash") (r "^0.8.0") (d #t) (k 0)) (d (n "git-object") (r "^0.15.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1qxj01bp6m7almbkqh4nfxyjz1dm2g8yypxa2xy5rwi962d02kkx")))

(define-public crate-git-diff-0.12.0 (c (n "git-diff") (v "0.12.0") (d (list (d (n "git-hash") (r "^0.8.0") (d #t) (k 0)) (d (n "git-object") (r "^0.16.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0qbxlpxgsszns6nhvb83k8fbkx6d73rwnyacm8sivsj71zk5xp76")))

(define-public crate-git-diff-0.13.0 (c (n "git-diff") (v "0.13.0") (d (list (d (n "git-hash") (r "^0.9.1") (d #t) (k 0)) (d (n "git-object") (r "^0.17.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1b8gs3rnrgicknfik45m0pk44jnacb6ai18d4amhnsgbcv4dmkss")))

(define-public crate-git-diff-0.14.0 (c (n "git-diff") (v "0.14.0") (d (list (d (n "git-hash") (r "^0.9.3") (d #t) (k 0)) (d (n "git-object") (r "^0.18.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1ngx1m23iprpwqbz9nazcbgiia6kn4wxmygygnrwni05jks6i1cr")))

(define-public crate-git-diff-0.15.0 (c (n "git-diff") (v "0.15.0") (d (list (d (n "git-hash") (r "^0.9.3") (d #t) (k 0)) (d (n "git-object") (r "^0.18.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0lgckq9i2zmx3f02wqv3rbg3sznsak43p4wwsiil157ngj24z2m7")))

(define-public crate-git-diff-0.16.0 (c (n "git-diff") (v "0.16.0") (d (list (d (n "git-hash") (r "^0.9.4") (d #t) (k 0)) (d (n "git-object") (r "^0.19.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0abl9why6a2zhsshqwy1zkdybhxsmp2kx3x85zra551hi49v3dg5")))

(define-public crate-git-diff-0.17.0 (c (n "git-diff") (v "0.17.0") (d (list (d (n "git-hash") (r "^0.9.6") (d #t) (k 0)) (d (n "git-object") (r "^0.20.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "068l447w1k24lnmdgm3g9p7i3nm4jr9f8id7pkx63sp08vqhg6fa")))

(define-public crate-git-diff-0.17.1 (c (n "git-diff") (v "0.17.1") (d (list (d (n "git-hash") (r "^0.9.7") (d #t) (k 0)) (d (n "git-object") (r "^0.20.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "0hn7zq55wpqn3lmzirqmjlgnzml6wjl0qwsynbwjppjqjqvx58y7")))

(define-public crate-git-diff-0.17.2 (c (n "git-diff") (v "0.17.2") (d (list (d (n "git-hash") (r "^0.9.8") (d #t) (k 0)) (d (n "git-object") (r "^0.20.2") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "12bnb7vbsn6fd8s9pv4q6pbpyy896m8pz26chm93q75kxdygzdiy")))

(define-public crate-git-diff-0.18.0 (c (n "git-diff") (v "0.18.0") (d (list (d (n "git-hash") (r "^0.9.8") (d #t) (k 0)) (d (n "git-object") (r "^0.20.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0i9yv7axfdgc6pp1hgkfjsxq99yfi5abms1p35xj5sxy53pwc5qn")))

(define-public crate-git-diff-0.18.1 (c (n "git-diff") (v "0.18.1") (d (list (d (n "git-hash") (r "^0.9.8") (d #t) (k 0)) (d (n "git-object") (r "^0.20.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1slm4xl6cvxy9wlb6v9q180lsm8pz7v0q9pcf4dcs6h3hjdvsbnr")))

(define-public crate-git-diff-0.19.0 (c (n "git-diff") (v "0.19.0") (d (list (d (n "git-hash") (r "^0.9.10") (d #t) (k 0)) (d (n "git-object") (r "^0.21.0") (d #t) (k 0)) (d (n "similar") (r "^2.2.0") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1f70nig3mjxdbc0g1ihv85ykqjym4kc0pf0zajip0pyi4ncv38y3")))

(define-public crate-git-diff-0.20.0 (c (n "git-diff") (v "0.20.0") (d (list (d (n "git-hash") (r "^0.9.11") (d #t) (k 0)) (d (n "git-object") (r "^0.22.0") (d #t) (k 0)) (d (n "similar") (r "^2.2.0") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "16bihkq4c8zrbhpwdncr5snklr3i74b7sz39m937wgzld1975p1c")))

(define-public crate-git-diff-0.21.0 (c (n "git-diff") (v "0.21.0") (d (list (d (n "git-hash") (r "^0.9.11") (d #t) (k 0)) (d (n "git-object") (r "^0.22.1") (d #t) (k 0)) (d (n "imara-diff") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1idbnaq61g83jhiav53qfkqcz96jd3gfzwnbl1amndp419m6d20a") (f (quote (("serde1" "serde" "git-hash/serde1" "git-object/serde1"))))))

(define-public crate-git-diff-0.22.0 (c (n "git-diff") (v "0.22.0") (d (list (d (n "git-hash") (r "^0.9.11") (d #t) (k 0)) (d (n "git-object") (r "^0.22.1") (d #t) (k 0)) (d (n "imara-diff") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0762h2sz7dzh8yc9b4x20slri3b74vhskjgbyfnn3ag0icsn2k1c") (f (quote (("serde1" "serde" "git-hash/serde1" "git-object/serde1"))))))

(define-public crate-git-diff-0.23.0 (c (n "git-diff") (v "0.23.0") (d (list (d (n "git-hash") (r "^0.10.0") (d #t) (k 0)) (d (n "git-object") (r "^0.23.0") (d #t) (k 0)) (d (n "imara-diff") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1cjvpxlih6r7njdp4qgf8brql98742rwysmjy6avwrqj703p9xw2") (f (quote (("serde1" "serde" "git-hash/serde1" "git-object/serde1"))))))

(define-public crate-git-diff-0.24.0 (c (n "git-diff") (v "0.24.0") (d (list (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-object") (r "^0.24.0") (d #t) (k 0)) (d (n "imara-diff") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0g0yzsbl5zqjvifbyryndfz2h1g10jqaggnzjb24b1lh8cd02c4g") (f (quote (("serde1" "serde" "git-hash/serde1" "git-object/serde1"))))))

(define-public crate-git-diff-0.25.0 (c (n "git-diff") (v "0.25.0") (d (list (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-object") (r "^0.25.0") (d #t) (k 0)) (d (n "imara-diff") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0yl5kkvyl40xlrimqp0x1ka3jxgr77jpjg8pbc8zw2gmarcpgbm8") (f (quote (("serde1" "serde" "git-hash/serde1" "git-object/serde1"))))))

(define-public crate-git-diff-0.26.0 (c (n "git-diff") (v "0.26.0") (d (list (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-object") (r "^0.26.0") (d #t) (k 0)) (d (n "imara-diff") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0nm2lkbcwn2kg0v7kk8286ks14ndmggvxinf0inn0vfj4924g1ya") (f (quote (("serde1" "serde" "git-hash/serde1" "git-object/serde1"))))))

(define-public crate-git-diff-0.26.1 (c (n "git-diff") (v "0.26.1") (d (list (d (n "git-hash") (r "^0.10.2") (d #t) (k 0)) (d (n "git-object") (r "^0.26.1") (d #t) (k 0)) (d (n "imara-diff") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1cmvrvkj5nvi3y8jh3yg9gpvg3diqdrrlv5z4qhczpa5bwmkpadf") (f (quote (("serde1" "serde" "git-hash/serde1" "git-object/serde1")))) (r "1.64")))

(define-public crate-git-diff-0.26.2 (c (n "git-diff") (v "0.26.2") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (o #t) (k 0)) (d (n "git-hash") (r "^0.10.3") (d #t) (k 0)) (d (n "git-object") (r "^0.26.2") (d #t) (k 0)) (d (n "imara-diff") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "06qs9hym9is7kfcw4q2n0llpaqwhv4nn3m2ri8x6hvxnq79fzl5x") (f (quote (("serde1" "serde" "git-hash/serde1" "git-object/serde1")))) (s 2) (e (quote (("wasm" "dep:getrandom")))) (r "1.64")))

