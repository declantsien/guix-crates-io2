(define-module (crates-io gi t- git-record) #:use-module (crates-io))

(define-public crate-git-record-0.1.0 (c (n "git-record") (v "0.1.0") (d (list (d (n "cursive") (r "^0.17.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive_buffered_backend") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.14.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "0gi1f5x8dp1spn6gj1v8nm6z0g8rkq0cc6gr5sabhp31snvgfz55") (r "1.56.0")))

(define-public crate-git-record-0.2.0 (c (n "git-record") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "cursive") (r "^0.19.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive_buffered_backend") (r "^0.6.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "insta") (r "^1.18.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1pfrdnfgn4pp400bwd5z9srpp09nadkf1qwvq7ns7dvawsf4zhyb") (r "1.61.0")))

(define-public crate-git-record-0.3.0 (c (n "git-record") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive_buffered_backend") (r "^0.6.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "15ba59xm7p0hpfmdyxyngwmdln61z61c910pi590clz9wqv76pln") (r "1.61.0")))

(define-public crate-git-record-0.4.0 (c (n "git-record") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive_buffered_backend") (r "^0.6.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1xa9qkm2ayxhs22wq6xm60a2ngmc0329a0kcpj947n0m5y2dfjfr") (y #t) (r "1.64.0")))

