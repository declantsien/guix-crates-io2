(define-module (crates-io gi t- git-whence) #:use-module (crates-io))

(define-public crate-git-whence-0.1.2 (c (n "git-whence") (v "0.1.2") (d (list (d (n "ansi-to-tui") (r "^3") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "git2") (r "^0.17") (k 0)) (d (n "timeago") (r "^0.4") (k 0)) (d (n "tui") (r "^0.20") (d #t) (k 0) (p "ratatui")))) (h "1kwb4wbyra0avgrsrla3nrgyigahdk3zvb6aabiz806l4klrrspw")))

(define-public crate-git-whence-0.2.0 (c (n "git-whence") (v "0.2.0") (d (list (d (n "ansi-to-tui") (r "^3") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "git2") (r "^0.17") (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "timeago") (r "^0.4") (k 0)) (d (n "tui") (r "^0.20") (d #t) (k 0) (p "ratatui")))) (h "0w8w2cmfridv6dv102snd8yv7gnkw667031ngpybvd7sk6givjdx")))

(define-public crate-git-whence-0.2.1 (c (n "git-whence") (v "0.2.1") (d (list (d (n "ansi-to-tui") (r "^3") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "git2") (r "^0.17") (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "timeago") (r "^0.4") (k 0)) (d (n "tui") (r "^0.20") (d #t) (k 0) (p "ratatui")))) (h "0kzpy4s0zpwpsgybplidkxhp86g0rjdyafnqsz12zhvxkc7jklxg")))

(define-public crate-git-whence-0.4.0 (c (n "git-whence") (v "0.4.0") (d (list (d (n "ansi-to-tui") (r "^3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "git2") (r "^0.17") (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "timeago") (r "^0.4") (k 0)) (d (n "tui") (r "^0.24") (d #t) (k 0) (p "ratatui")))) (h "0iidkjqivbi7fxvnf6mc5ax4bv3z9ll3as9g688l9hav84ryy9if")))

