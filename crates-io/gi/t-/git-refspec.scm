(define-module (crates-io gi t- git-refspec) #:use-module (crates-io))

(define-public crate-git-refspec-0.0.0 (c (n "git-refspec") (v "0.0.0") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1jszv1nznavlw6dscvx649gc3xw14m4cvv0g9icj8s97ms16rkcq")))

(define-public crate-git-refspec-0.1.0 (c (n "git-refspec") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.9.8") (d #t) (k 0)) (d (n "git-revision") (r "^0.4.2") (d #t) (k 0)) (d (n "git-validate") (r "^0.5.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0wbvb15swmz7qy0mmind6996j5rzg807j8zva8c1aacpjcw6921j")))

(define-public crate-git-refspec-0.1.1 (c (n "git-refspec") (v "0.1.1") (d (list (d (n "bstr") (r "^0.2.13") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.9.8") (d #t) (k 0)) (d (n "git-revision") (r "^0.4.4") (d #t) (k 0)) (d (n "git-validate") (r "^0.5.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0gfvarwss84hkckl4nl36m2c4nkri7x4658pk8p2l3lhz75gbanl")))

(define-public crate-git-refspec-0.2.0 (c (n "git-refspec") (v "0.2.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.9.10") (d #t) (k 0)) (d (n "git-revision") (r "^0.5.0") (d #t) (k 0)) (d (n "git-validate") (r "^0.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1909zk3l5xa981x1wij893rn129ya0d4xk9p0dy3vbsc468bzk6p")))

(define-public crate-git-refspec-0.3.0 (c (n "git-refspec") (v "0.3.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.9.11") (d #t) (k 0)) (d (n "git-revision") (r "^0.6.0") (d #t) (k 0)) (d (n "git-validate") (r "^0.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "09bhdk9a6l9ifvzci3ln0fivk9krzjbwx3b2fi31n1wdcrhr4h04")))

(define-public crate-git-refspec-0.3.1 (c (n "git-refspec") (v "0.3.1") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.9.11") (d #t) (k 0)) (d (n "git-revision") (r "^0.6.0") (d #t) (k 0)) (d (n "git-validate") (r "^0.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "11s1y4rfxd5qah7h8lx67crqvhp6m787vzskl3yqrbiq6mvsz5wl")))

(define-public crate-git-refspec-0.4.0 (c (n "git-refspec") (v "0.4.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.10.0") (d #t) (k 0)) (d (n "git-revision") (r "^0.7.0") (d #t) (k 0)) (d (n "git-validate") (r "^0.7.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "01q1dckqs7h8m6kzzgb6pallxz7dbavxyl80nq1qkm6mwwv8ybmc")))

(define-public crate-git-refspec-0.5.0 (c (n "git-refspec") (v "0.5.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-revision") (r "^0.8.0") (d #t) (k 0)) (d (n "git-validate") (r "^0.7.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1a7baii8hjq8yrgsg9q565hsxqwkq1g8nd6khv9hrmjn17dyjy6l")))

(define-public crate-git-refspec-0.6.0 (c (n "git-refspec") (v "0.6.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-revision") (r "^0.9.0") (d #t) (k 0)) (d (n "git-validate") (r "^0.7.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "14hhay2xdqrci1xsb8gi1ay7wmgb63kir6srixq311ds23rrkxpw")))

(define-public crate-git-refspec-0.7.0 (c (n "git-refspec") (v "0.7.0") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-revision") (r "^0.10.0") (d #t) (k 0)) (d (n "git-validate") (r "^0.7.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1z8xb8n2pc09dmkf6ylaf7q8jjxfskmllzbfz45aqa98bzni1wyx")))

(define-public crate-git-refspec-0.7.1 (c (n "git-refspec") (v "0.7.1") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "git-revision") (r "^0.10.1") (d #t) (k 0)) (d (n "git-validate") (r "^0.7.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "02pwpg1d8fag66aamb3l75pjf2wr45mbxcp8dmsa9p57ki3bm7s1")))

(define-public crate-git-refspec-0.7.2 (c (n "git-refspec") (v "0.7.2") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.10.2") (d #t) (k 0)) (d (n "git-revision") (r "^0.10.2") (d #t) (k 0)) (d (n "git-validate") (r "^0.7.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0lzwjhqdi6dbr7qnkvkmcq47s9kd3i8kfpkkkh4cdy75ybkc8vhi") (r "1.64")))

(define-public crate-git-refspec-0.7.3 (c (n "git-refspec") (v "0.7.3") (d (list (d (n "bstr") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "git-hash") (r "^0.10.3") (d #t) (k 0)) (d (n "git-revision") (r "^0.10.4") (d #t) (k 0)) (d (n "git-validate") (r "^0.7.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "17bqjb9pmfb6fm8c6mj7nnp8sl5k7p9czs6kvc7lmniskwcqbs93") (r "1.64")))

