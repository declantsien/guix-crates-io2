(define-module (crates-io gi t- git-hive-protocol) #:use-module (crates-io))

(define-public crate-git-hive-protocol-0.2.0 (c (n "git-hive-protocol") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "rmp") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "10afgd9kq79zkq08yr79hgq34hd137qyblc15z7bnx3cv4yjys83") (y #t)))

(define-public crate-git-hive-protocol-0.2.1 (c (n "git-hive-protocol") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "rmp") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "10kfdr1jf5dz330hbr7z9pw0xf37m7q3jg12554ic9yjwf9aya3s") (y #t)))

(define-public crate-git-hive-protocol-0.2.2 (c (n "git-hive-protocol") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "rmp") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "0cdjzcfn94477gfcql1gp3whkknsp25a462z2kmf6iyaxvi6cl3h") (y #t)))

(define-public crate-git-hive-protocol-0.2.3 (c (n "git-hive-protocol") (v "0.2.3") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)) (d (n "rmp") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "1g8w0afpyrqvicy6537alcpsrmzg90c0v55c8v0p0pzpm02x05qn")))

