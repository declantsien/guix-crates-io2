(define-module (crates-io gi t- git-semver) #:use-module (crates-io))

(define-public crate-git-semver-0.1.0 (c (n "git-semver") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "better-panic") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gix") (r "^0.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1lz2bn2ww1v5vqdis5bwvcldnl86ziarg0bws89bnkddax47lbgj")))

