(define-module (crates-io gi t- git-fetchhead) #:use-module (crates-io))

(define-public crate-git-fetchhead-0.0.0 (c (n "git-fetchhead") (v "0.0.0") (h "13p9fax8yb9y4nk6jp5lmlnhcgrk5qp750w8f3a37g8zki1kwwkz")))

(define-public crate-git-fetchhead-0.1.0 (c (n "git-fetchhead") (v "0.1.0") (h "1ihm9s20sd7r6apwh4bcrn95as7gh1ixyqvijh68gbhjc27mhz2q") (r "1.64")))

