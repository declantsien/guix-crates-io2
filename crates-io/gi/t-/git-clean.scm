(define-module (crates-io gi t- git-clean) #:use-module (crates-io))

(define-public crate-git-clean-0.2.0 (c (n "git-clean") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1z6bdpqmywdyh7j2k1in7np8kmaxnqj1x3q3j7450882x529iap8")))

(define-public crate-git-clean-0.2.1 (c (n "git-clean") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0njy95vagmzarhr75bhv1rs8nwcl8zl2bdqf32kbrp419dfyd14k")))

(define-public crate-git-clean-0.2.2 (c (n "git-clean") (v "0.2.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0c3pf6kbqj85f7j6y6klam163zb6hk0l3vspnspsc0phvfzyjhxk")))

(define-public crate-git-clean-0.3.0 (c (n "git-clean") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1g29dlzgw2b7k3hy8jawm4cpdl96qw02b1npk4x85bqpd0jjlc21")))

(define-public crate-git-clean-0.4.0 (c (n "git-clean") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0fzai76j8wl36p3jfdjiwhzapw5k1ikcvcprnp0zhqlfympawp89")))

(define-public crate-git-clean-0.4.1 (c (n "git-clean") (v "0.4.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0mxv2xdj2r8pk9jwrf1gbql7yrvk6cn4wqflfck4balzjj9vb5y3")))

(define-public crate-git-clean-0.4.2 (c (n "git-clean") (v "0.4.2") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0zjv94idkfnbc8wyfppxmf51mq8iki30w1mmx5j3nz67q59a8s7p")))

(define-public crate-git-clean-0.4.3 (c (n "git-clean") (v "0.4.3") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0z2m2y2qr6qg9qxglp8528arr6zbpgcyz2s1ab2fyvhykbqplfkg")))

(define-public crate-git-clean-0.4.4 (c (n "git-clean") (v "0.4.4") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0r3lp8z9a5yfaq6sl4xmi9p127ycf6l59gimy44lgvbb5xh7s172")))

(define-public crate-git-clean-0.5.0 (c (n "git-clean") (v "0.5.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "00zaz41hampyq778iqfjcpjwb0vph3mzszji4k2z50jk36b7mfx4")))

(define-public crate-git-clean-0.5.1 (c (n "git-clean") (v "0.5.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1adbyrg8pz6l5lc8n3bnx0k4rcjisa80pd4bf9zzswvqas8p6nf9")))

(define-public crate-git-clean-0.6.0 (c (n "git-clean") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0am47dam6zi50silnnh23n2pal7cwzcr9zss2sg94yxqn4dlb5ba")))

(define-public crate-git-clean-0.7.0 (c (n "git-clean") (v "0.7.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17j5qfva3gn9ahq3yf61pfrjdh5yzvr7a5di3m0vsdmvgabl4avg")))

(define-public crate-git-clean-0.8.0 (c (n "git-clean") (v "0.8.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ig971h2fm5pf2nqmbnqc7kq8n1475cn4y2b51s8vlij8krdlkly")))

