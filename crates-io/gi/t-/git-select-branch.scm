(define-module (crates-io gi t- git-select-branch) #:use-module (crates-io))

(define-public crate-git-select-branch-0.1.0 (c (n "git-select-branch") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "1kd9prnxfrmmsxfsq323b81ssa9rbfvq2d76sxhhc0fla3254xyq")))

(define-public crate-git-select-branch-0.1.1 (c (n "git-select-branch") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "11kicixsall8wp5rvfacskxi28iqx90zpafvisq1lyr2i9xh2y83")))

(define-public crate-git-select-branch-0.1.2 (c (n "git-select-branch") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "expect-exit") (r "^0.5.2") (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (d #t) (k 0)))) (h "0ma2ig9hqrnmvrkq6lndkx2y9yk2vmxxg46g9mwj6mfrc2bw8kgv")))

(define-public crate-git-select-branch-0.1.3 (c (n "git-select-branch") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "expect-exit") (r "^0.5.2") (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1p2bwg8rsh70myg7iimls17mcrsngf4z4zg2lq4813n5k1hvzfx4")))

(define-public crate-git-select-branch-0.2.0 (c (n "git-select-branch") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "expect-exit") (r "^0.5.2") (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0z6ykc32vhcg7y4zjyqia08ygsjmk1nc6xcpk33wvc51zspmfiff")))

(define-public crate-git-select-branch-0.2.3 (c (n "git-select-branch") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.68") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "expect-exit") (r "^0.5.2") (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0npzqb2zxncvz4mxzbcvpznz8n51pqypcy644dqbky0qhka7f2qp")))

