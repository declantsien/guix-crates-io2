(define-module (crates-io gi t- git-blamediff) #:use-module (crates-io))

(define-public crate-git-blamediff-0.1.0 (c (n "git-blamediff") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.10.0") (f (quote ("std"))) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("perf" "std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (k 2)))) (h "099kkmdkx1vyl93sf9k3193r5cq69sv0rpr17z4ip6hpngzgsq0d")))

(define-public crate-git-blamediff-0.1.1 (c (n "git-blamediff") (v "0.1.1") (d (list (d (n "diff-parse") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (f (quote ("std"))) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("perf" "std"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (k 2)))) (h "09jr01wpzxjn7c4gn45bmfql7h1ak4nbaxc8ik1yvlyypp57h63q")))

