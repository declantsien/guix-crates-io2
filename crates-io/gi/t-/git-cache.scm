(define-module (crates-io gi t- git-cache) #:use-module (crates-io))

(define-public crate-git-cache-0.1.0 (c (n "git-cache") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("cargo" "env" "string"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0n0062z67fj31qpac1gh000yj305w3x2w1k9xnrig5sjr005jdc1")))

(define-public crate-git-cache-0.1.1 (c (n "git-cache") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("cargo" "env" "string"))) (d #t) (k 0)) (d (n "fd-lock") (r "^4.0.2") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0r8njg1khhnsd3z779i3j3kxxdlcr0ml050b44bkq6g293jlsrsf")))

(define-public crate-git-cache-0.1.2 (c (n "git-cache") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("cargo" "env" "string"))) (d #t) (k 0)) (d (n "fd-lock") (r "^4.0.2") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1a4jq3ixc92af7fl78zq4hkw5zdv88xasvi283bgyzlw57z0xfzw")))

(define-public crate-git-cache-0.1.3 (c (n "git-cache") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("cargo" "env" "string"))) (d #t) (k 0)) (d (n "fd-lock") (r "^4.0.2") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1mjpdqj5n81r09sfabgsfay1si25ci30zpblgdn6vsnyi1msmpnw")))

(define-public crate-git-cache-0.1.4 (c (n "git-cache") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("cargo" "env" "string"))) (d #t) (k 0)) (d (n "fd-lock") (r "^4.0.2") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0b7732g70dv92g7wl9mzkx3v9yihpa1nwxdi780hwk7smfjl466h")))

(define-public crate-git-cache-0.1.5 (c (n "git-cache") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("cargo" "env" "string"))) (d #t) (k 0)) (d (n "fd-lock") (r "^4.0.2") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0r5kp09clxdksyg7hwln1hv47avclaw75w0y94b04s66dkksd4b8")))

