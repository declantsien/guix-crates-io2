(define-module (crates-io gi t- git-lock) #:use-module (crates-io))

(define-public crate-git-lock-0.0.0 (c (n "git-lock") (v "0.0.0") (d (list (d (n "git-tempfile") (r "^0.1.0") (d #t) (k 0)) (d (n "git-testtools") (r "^0.3") (d #t) (k 2)))) (h "0z75wg4sai8ld1fp8icijmdybwqma21sym4addr2ghngn2903sxn")))

(define-public crate-git-lock-0.1.0 (c (n "git-lock") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.4.1") (d #t) (k 0)) (d (n "git-tempfile") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "13bj97mm8yy6slfpassy6rc3vx5ispg4m4xb6flbydl9svf65w8r")))

(define-public crate-git-lock-0.2.0 (c (n "git-lock") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^0.5.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "18p64c28xzbl21jv0a32dz64f0nil8i71947ck5knfwlq3gya589")))

(define-public crate-git-lock-0.3.0 (c (n "git-lock") (v "0.3.0") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^0.6.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0wwsfb7ihi2g62j4nx8kzqpsv8y6mk9cbxlhmx5gpnyjcpqlaiq5")))

(define-public crate-git-lock-0.3.1 (c (n "git-lock") (v "0.3.1") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^0.6.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0ldp0d8xsb9grgfd2frbr0j12cyagabpqysj0b1nr8hq0qjcvsrb")))

(define-public crate-git-lock-0.3.2 (c (n "git-lock") (v "0.3.2") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^0.6.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0byqyw0mc2rz8ffiqn73xkj918gsc25m8mrbh72h26c83q1y4abv")))

(define-public crate-git-lock-1.0.0 (c (n "git-lock") (v "1.0.0") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^1.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1p3yiwmqr1gx0n4g5885m858y0m9ckr0hviv9ai8ww7djpzb03h7")))

(define-public crate-git-lock-1.0.1 (c (n "git-lock") (v "1.0.1") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^1.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1cphpi8yxa66pnggrnnh8y3gpg5v3z8623p1v04bx4yqqbkc1ii4")))

(define-public crate-git-lock-2.0.0 (c (n "git-lock") (v "2.0.0") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^2.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "16kfxwpassx0kd3v65ajccjdbm6awrp9q46m9macyllg4qbbzrxd")))

(define-public crate-git-lock-2.1.0 (c (n "git-lock") (v "2.1.0") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^2.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "14bfjghi42vj0kyxqvjlaiixcahdyi9is98frh797phc0xdr2i7f")))

(define-public crate-git-lock-2.1.1 (c (n "git-lock") (v "2.1.1") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^2.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0wqfcmpdjxgk7l0fi1pq2bccxw3dvcp1rf5rkhhkwmwkd9rsvxig")))

(define-public crate-git-lock-2.2.0 (c (n "git-lock") (v "2.2.0") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^2.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0y3wxg1i63rnv5ydqrykbrndid2gws2pgh5l6l9jpxk1z45y23sz")))

(define-public crate-git-lock-3.0.0 (c (n "git-lock") (v "3.0.0") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^3.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1hnfgkw7g7dbzn2221w3w87r2kiqprv1qrd6hgfsbhv8i9dz1r49")))

(define-public crate-git-lock-3.0.1 (c (n "git-lock") (v "3.0.1") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^3.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0dv43zi4mqq959yrm5hi8zczjag4w9200znbkfxk568skly6mkz7")))

(define-public crate-git-lock-3.0.2 (c (n "git-lock") (v "3.0.2") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^3.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1f2bmjg5bhvj754zlb3bsdbg2w70wx4yif3jihgh0k63hjgf6lxy") (r "1.64")))

(define-public crate-git-lock-3.0.3 (c (n "git-lock") (v "3.0.3") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "git-tempfile") (r "^3.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0xh2l5ssfx1nss5qkh6aj4pc9r9jvjp4jcxwz5zmb5m3b091kikd") (r "1.64")))

