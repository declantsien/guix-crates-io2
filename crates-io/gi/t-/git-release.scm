(define-module (crates-io gi t- git-release) #:use-module (crates-io))

(define-public crate-git-release-0.1.0 (c (n "git-release") (v "0.1.0") (d (list (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0pq0jxiwmmgcb3c0l2v94gvn5vq76yimsbsns6klxcjj90ksapwh")))

(define-public crate-git-release-0.1.1 (c (n "git-release") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "octocrab") (r "^0.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("color"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0cvmhijsfxhxr9a860y4y0ysdwjbdfflacrlqhr54r9jivwlkgzq")))

