(define-module (crates-io gi t- git-co) #:use-module (crates-io))

(define-public crate-git-co-0.1.0 (c (n "git-co") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0vckr98bzkykiz6jh639i5pwfc88vbma6hbx9w65j5n6l65plpq0")))

(define-public crate-git-co-0.1.1 (c (n "git-co") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "1ha47j2pk16bkz8js4ks6pr05zaifcal985zh24x7m2bzx4lc5ij")))

(define-public crate-git-co-0.1.2 (c (n "git-co") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tuikit") (r "^0.5.0") (d #t) (k 0)))) (h "0kdp2gkkxnjs1cnx9sahh6757hn9xn6jyc3780c71zjh5mh0s3aq")))

(define-public crate-git-co-0.2.0 (c (n "git-co") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tuikit") (r "^0.5.0") (d #t) (k 0)))) (h "1zfqdp8b7wp6cklhwsf94dvdm9s81ba18bwwpaaiwqb7imx6i7l5")))

(define-public crate-git-co-0.3.0 (c (n "git-co") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tuikit") (r "^0.5.0") (d #t) (k 0)))) (h "0lv0xhp299324sdlj9ms4a40gr6bkddaz6j4jzimbibjqgbchb0b")))

(define-public crate-git-co-0.3.1 (c (n "git-co") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tuikit") (r "^0.5.0") (d #t) (k 0)))) (h "0fnylhp5gjqc3f535n74hyq31ansdr2m8jnggvqy1n9q83lfhsfv")))

