(define-module (crates-io gi t- git-db) #:use-module (crates-io))

(define-public crate-git-db-0.1.0 (c (n "git-db") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "git2") (r "^0.6.11") (d #t) (k 0)) (d (n "ring") (r "^0.13.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1dry1kw8ha4790bfvzlx150grzivgdrhld82kihy7j91qpcm13x6")))

