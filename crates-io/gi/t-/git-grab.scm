(define-module (crates-io gi t- git-grab) #:use-module (crates-io))

(define-public crate-git-grab-0.1.1 (c (n "git-grab") (v "0.1.1") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0f2yhgd9n3329z23bp7r56kz5by0p4cmpy30gljfqx2cig3sgfgc")))

(define-public crate-git-grab-0.1.2 (c (n "git-grab") (v "0.1.2") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "11gmhdf8a9crn7s5alr7qg9lc0fi25hs1a21xjqapjs360hqk4ry")))

(define-public crate-git-grab-1.0.0 (c (n "git-grab") (v "1.0.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1xmy4xpgx0ah4zc86j98h32wwziif43j19cl7pm3nfyagbj58ih1")))

(define-public crate-git-grab-1.0.1 (c (n "git-grab") (v "1.0.1") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1ki515i1dvy924a1ndlp1pyh62h4kyvb70dq9mvp0d69j1vzc67k")))

(define-public crate-git-grab-2.0.0 (c (n "git-grab") (v "2.0.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0lh4pvav0kla65z90531hgi8dzqy4jx05shcih75nbsw80j2ssf1")))

(define-public crate-git-grab-2.1.0 (c (n "git-grab") (v "2.1.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1dwz73jh3442snpydh2bhwh9avdgnz3d0spyck0bcyr50a6q9c08")))

(define-public crate-git-grab-3.0.0 (c (n "git-grab") (v "3.0.0") (d (list (d (n "clipboard-win") (r "^5.3.1") (o #t) (d #t) (t "cfg(target_family = \"windows\")") (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0bw0h5zvq657zshvcsr9mcapvzalg3wn3fzaxyb83pgx2yy7w1aj") (f (quote (("default" "clipboard")))) (s 2) (e (quote (("clipboard" "dep:clipboard-win"))))))

