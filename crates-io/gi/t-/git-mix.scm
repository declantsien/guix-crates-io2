(define-module (crates-io gi t- git-mix) #:use-module (crates-io))

(define-public crate-git-mix-0.1.0 (c (n "git-mix") (v "0.1.0") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0lwvrpcgs3z125rdpwr9jsi68k3gb786331544xm4rvs11895zw2")))

(define-public crate-git-mix-0.1.1 (c (n "git-mix") (v "0.1.1") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "049r2avzl3076iknq3363j53vn5j3jxhz83p1rm2cz2azr3bcdi1")))

(define-public crate-git-mix-0.1.2 (c (n "git-mix") (v "0.1.2") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "08dniv93fi8bpnc7gigy2mpnn3b8w890y11w4s0wgzj5md2arp0f")))

(define-public crate-git-mix-0.1.3 (c (n "git-mix") (v "0.1.3") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1anzxcdiw2r9r8263078z67d8yrgz0ks73a039rd4jz43v60mdaw")))

(define-public crate-git-mix-0.1.4 (c (n "git-mix") (v "0.1.4") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0q50s6j853dnwhcn7skdigfrry17mx52w4jm9xcsmkbw92hlknav")))

(define-public crate-git-mix-0.1.5 (c (n "git-mix") (v "0.1.5") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0mi1xiqsh2ri8aykbklzlwl22az6wsjibdls4qpvd8ka1yzk2pfx")))

