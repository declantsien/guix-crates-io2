(define-module (crates-io gi t- git-calver) #:use-module (crates-io))

(define-public crate-git-calver-20.3.0 (c (n "git-calver") (v "20.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)))) (h "0pyv9m3zxdvd8482z1s19h6jk8vll98gqi8rq9qdj6m5mcyzc3j0")))

(define-public crate-git-calver-20.3.4 (c (n "git-calver") (v "20.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)))) (h "0yb9ddwrbxs4qfikr2k8hm602g23wh8jc6nvc1aff1fpnnj0i0zy")))

(define-public crate-git-calver-20.3.5 (c (n "git-calver") (v "20.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)))) (h "0cy4xmv9d12a755mxvlqmis2qysxrsl67zgz5hxb2ywx1zs98x5h")))

(define-public crate-git-calver-22.7.0 (c (n "git-calver") (v "22.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)))) (h "136zmw7pvqk71sydh30yrbrb8y1q0za2szlwgyhxmwr7d8zgc6jd")))

(define-public crate-git-calver-23.12.0 (c (n "git-calver") (v "23.12.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "git2") (r "^0.16") (f (quote ("vendored-openssl"))) (k 0)))) (h "042jjmxkp1g505yj00yix2in8khfpw3v7g4wlq9wmz0zpc8h1c2m")))

(define-public crate-git-calver-23.12.1 (c (n "git-calver") (v "23.12.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "git2") (r "^0.16") (f (quote ("vendored-openssl"))) (k 0)))) (h "0700pj9xlh5516jxhxfk6n3qn1kzypwsvgsdw6irlgxwshh2jc5w")))

(define-public crate-git-calver-24.1.0 (c (n "git-calver") (v "24.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "git2") (r "^0.16") (f (quote ("vendored-openssl"))) (k 0)))) (h "0jdmnjmg1whxizyp8zv2baian89936f228lqasca9571jszfj73j")))

(define-public crate-git-calver-24.1.1 (c (n "git-calver") (v "24.1.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^4.4.12") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (f (quote ("vendored-openssl"))) (k 0)))) (h "00f5ppaf4rhgav0k1r65gfk1g3j8swvi6iz08rwbjxw2nmiw65kk")))

