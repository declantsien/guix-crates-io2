(define-module (crates-io gi t- git-testament-derive) #:use-module (crates-io))

(define-public crate-git-testament-derive-0.1.0 (c (n "git-testament-derive") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.8") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0izs106f5i6hyaa99l5hqgb99jx3s0dq2glayl4mbw0j12583niz") (y #t)))

(define-public crate-git-testament-derive-0.1.1 (c (n "git-testament-derive") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1hybpvaanqny98pgs109winvbxvnd74yszq30a03bwnsvm1x1vxf") (y #t)))

(define-public crate-git-testament-derive-0.1.2 (c (n "git-testament-derive") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "006n9z7cdrdrcc04hydaxxa40zbi3wypr77r8nb4pgg506iyy1pj") (y #t)))

(define-public crate-git-testament-derive-0.1.3 (c (n "git-testament-derive") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1crxpqrdd9bqlgx6l89l9355xlhj6znz3n1zv767fplax9fxk9lw")))

(define-public crate-git-testament-derive-0.1.4 (c (n "git-testament-derive") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0rb0iq0z2mplxjikmdwihly34hhzmgm5b48namdp5s6iwkvyfpg6")))

(define-public crate-git-testament-derive-0.1.5 (c (n "git-testament-derive") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0piv7qpiss16lyp5y0q9v497is4z02rr9g4n8zn7rnm9j4yv5gsa")))

(define-public crate-git-testament-derive-0.1.6 (c (n "git-testament-derive") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0jjlqxdaz8hzghd0f9394wn7l4md5m6gn3y91hi8hy08xk3cr5b7")))

(define-public crate-git-testament-derive-0.1.7 (c (n "git-testament-derive") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "029zjzi7crwa7s1cfvagwqs4klkspkd4lxm7vi477hgps570rwr7")))

(define-public crate-git-testament-derive-0.1.8 (c (n "git-testament-derive") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hx55agsjc6ijvm4fxjmz3wqzqzw5hn26sbhmn6wxm1i4382q9nl")))

(define-public crate-git-testament-derive-0.1.9 (c (n "git-testament-derive") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v7wd7rg1sh9526xkks8y7jzlmmq1gzykm5ss40d0j921nyn9xva")))

(define-public crate-git-testament-derive-0.1.10 (c (n "git-testament-derive") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h4kp97ns86a0662ba8zsnf5vmzi15s7i2bf2x3c0pm3kk3x8sjj")))

(define-public crate-git-testament-derive-0.1.11 (c (n "git-testament-derive") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.1.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "079xc80d7vrjm5392slfh0hfchv0fn6743qfzd41d78np45vvfac")))

(define-public crate-git-testament-derive-0.1.12 (c (n "git-testament-derive") (v "0.1.12") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git-testament") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x1k4cfmz2ay03f1b13cgqjfv9h8qjch39f47x66ch8in3bxxkj5")))

(define-public crate-git-testament-derive-0.1.13 (c (n "git-testament-derive") (v "0.1.13") (d (list (d (n "git-testament") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "0b4sd8xxqpfq66b1f7qrgmr3yq1qiny839lzh3qdcj0sajc3i060")))

(define-public crate-git-testament-derive-0.1.14 (c (n "git-testament-derive") (v "0.1.14") (d (list (d (n "git-testament") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1rlais0i47mgsmp3r5jcqry2agjfyg5s9paj6mgvfykchssjsy2a")))

(define-public crate-git-testament-derive-0.2.0 (c (n "git-testament-derive") (v "0.2.0") (d (list (d (n "git-testament") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "0hk8r71jjr1adxz1gpxl3i1xrj4j3g15jdwlyqq6f6myzd74jccv")))

