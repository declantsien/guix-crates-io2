(define-module (crates-io gi t- git-bonsai) #:use-module (crates-io))

(define-public crate-git-bonsai-0.1.0 (c (n "git-bonsai") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10.2") (d #t) (k 2)) (d (n "assert_fs") (r "^0.13.1") (d #t) (k 2)) (d (n "console") (r "^0.9.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "17f578v83nfna927ajwshxxmsqpyr9ms3ai8zihjbmfxkshgkkss")))

(define-public crate-git-bonsai-0.2.0 (c (n "git-bonsai") (v "0.2.0") (d (list (d (n "assert_fs") (r "^1.0.1") (d #t) (k 2)) (d (n "claim") (r "^0.5") (d #t) (k 2)) (d (n "console") (r "^0.9.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1gyg52aq2qnlyknhhp1f8s0003q9hwpxhsl4zps9xh2ya8b6a8mk")))

(define-public crate-git-bonsai-0.2.1 (c (n "git-bonsai") (v "0.2.1") (d (list (d (n "assert_fs") (r "^1.0.1") (d #t) (k 2)) (d (n "claim") (r "^0.5") (d #t) (k 2)) (d (n "console") (r "^0.9.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0nmjnl5x7i3qiqrwwfyvgd6rkw8667f1ak0m27ir671qv6hpqmzd")))

(define-public crate-git-bonsai-0.2.2 (c (n "git-bonsai") (v "0.2.2") (d (list (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1ydk47qgvmwn0ar5drgsvgh22vn1ddpwmlsjp9k287qs9snv1lzq")))

(define-public crate-git-bonsai-0.3.0 (c (n "git-bonsai") (v "0.3.0") (d (list (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1dwja36g3j9rhx754b6kvsljjkl3ffwfdmw2licyyw4fdmf297wm")))

