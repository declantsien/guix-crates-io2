(define-module (crates-io gi t- git-chunk) #:use-module (crates-io))

(define-public crate-git-chunk-0.1.0 (c (n "git-chunk") (v "0.1.0") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "09qjh8jk5hpf3hvk4pz3jj2xpbc3911cdsq9jyxg091gs78awjfq")))

(define-public crate-git-chunk-0.2.0 (c (n "git-chunk") (v "0.2.0") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "1i6k9v9md9yw4a9771bm9k8ll01xmas4sk8a3s661r7pq0dvq3gr")))

(define-public crate-git-chunk-0.3.0 (c (n "git-chunk") (v "0.3.0") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "08jgplbij0f1p37yy465qv6hf3s1y7dwlbg6gsi70y4x01y0j5f9")))

(define-public crate-git-chunk-0.3.1 (c (n "git-chunk") (v "0.3.1") (d (list (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)))) (h "15zgi08w1abwd8q7g2pa40cfpgfizbnhj0b3aq2n1j5whjgsh8q0")))

(define-public crate-git-chunk-0.3.2 (c (n "git-chunk") (v "0.3.2") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "127mk2xrs7fa5iy10ixp4k0s7435jjj896ip61pasq5n6lbbrch7")))

(define-public crate-git-chunk-0.4.0 (c (n "git-chunk") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1a7iz1s7448q9krllqzh1f9sz6i5hc5hjc9ykf54izm3yjibm41h")))

(define-public crate-git-chunk-0.4.1 (c (n "git-chunk") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1dnd46hj26vijh5a2kbnqx2ryv4ds1g1ywnqs71n9plb1rl19c8y") (r "1.64")))

(define-public crate-git-chunk-0.4.2 (c (n "git-chunk") (v "0.4.2") (d (list (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "06c421ayahglkcr1r7m0hxx3l4zwzapmih3qv1ai3kmnibq2wx25") (r "1.64")))

