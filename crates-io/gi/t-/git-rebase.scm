(define-module (crates-io gi t- git-rebase) #:use-module (crates-io))

(define-public crate-git-rebase-0.0.0 (c (n "git-rebase") (v "0.0.0") (h "1nmk6jp5zcwrlppjlcp6wgpasc84adzx8wly43cg3hqds1xjgl28")))

(define-public crate-git-rebase-0.1.0 (c (n "git-rebase") (v "0.1.0") (h "0yrjd62qjfrapaq2j5zx91b94glzbphi821h6a0ic55k9rgqccwz") (r "1.64")))

