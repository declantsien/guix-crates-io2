(define-module (crates-io gi t- git-forge) #:use-module (crates-io))

(define-public crate-git-forge-0.1.0 (c (n "git-forge") (v "0.1.0") (d (list (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0150spr9p9wliwh273bdshxgbf4qm8yxvk2vqc15mm1lwzk0ihg1")))

