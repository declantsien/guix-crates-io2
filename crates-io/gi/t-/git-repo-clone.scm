(define-module (crates-io gi t- git-repo-clone) #:use-module (crates-io))

(define-public crate-git-repo-clone-0.2.0 (c (n "git-repo-clone") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.9") (d #t) (k 0)) (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)))) (h "10s44p6b5m3gl7g8zlwdsdi8pswsld7ni1nhy9f1rvb4919wwb19")))

