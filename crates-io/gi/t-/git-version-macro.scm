(define-module (crates-io gi t- git-version-macro) #:use-module (crates-io))

(define-public crate-git-version-macro-0.0.1 (c (n "git-version-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "15kbzfz9rzh88swmzxqmdp56l2d8f69g01mgn1lnqkgsz34s20d0")))

(define-public crate-git-version-macro-0.3.0 (c (n "git-version-macro") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0z4f5gs55mvpgzgsk1xq2f0m65r4v845f7xvy0hn8hli8c02zzxx") (f (quote (("nightly"))))))

(define-public crate-git-version-macro-0.3.1 (c (n "git-version-macro") (v "0.3.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1r91yrvs5145g97qk4iz0m3xzkfha0ymc8k6si7wczrh2z1sfgbx") (f (quote (("nightly"))))))

(define-public crate-git-version-macro-0.3.3 (c (n "git-version-macro") (v "0.3.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yilzzjlpmw6l261hd60sxd6swi6265hd99whyn7629ji34crqxk") (f (quote (("nightly"))))))

(define-public crate-git-version-macro-0.3.4 (c (n "git-version-macro") (v "0.3.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mynlf8sfaa4xx7qff0qgnr339fbf1svgr569yip067fzm97ma9l") (f (quote (("nightly"))))))

(define-public crate-git-version-macro-0.3.5 (c (n "git-version-macro") (v "0.3.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gwz2cih86qnc4z5vla7nh30m2nfk4xr8ki1mhmsya3fvg5z2sgy") (f (quote (("nightly"))))))

(define-public crate-git-version-macro-0.3.6 (c (n "git-version-macro") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1fp1nlka3shx16ygga2lsi1rz6d2pv818jx246z3qq06k3nl1adb") (f (quote (("nightly"))))))

(define-public crate-git-version-macro-0.3.7 (c (n "git-version-macro") (v "0.3.7") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0qq8l9wk0nxnccbx355zmkjnjgc13r7n9smx35w4jsy1i0420p7w") (f (quote (("nightly")))) (y #t)))

(define-public crate-git-version-macro-0.3.8 (c (n "git-version-macro") (v "0.3.8") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1bldb35v7yil2r010xkc7k6mcy2a2nlb8qqqvib6zb94vg5qqj44") (f (quote (("nightly"))))))

(define-public crate-git-version-macro-0.3.9 (c (n "git-version-macro") (v "0.3.9") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1h1s08fgh9bkwnc2hmjxcldv69hlxpq7a09cqdxsd5hb235hq0ak") (f (quote (("nightly"))))))

