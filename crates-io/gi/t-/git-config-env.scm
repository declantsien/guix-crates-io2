(define-module (crates-io gi t- git-config-env) #:use-module (crates-io))

(define-public crate-git-config-env-0.1.0 (c (n "git-config-env") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0abdmapxcii0gp9806ksvlzi0cnnxwbfcasj0rrdbr1bdw6q5sfz")))

(define-public crate-git-config-env-0.1.1 (c (n "git-config-env") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1kz8q6961h0lnw83ha5q00jknl490z2cb2v443p8zr6yknxcr051")))

(define-public crate-git-config-env-0.1.2 (c (n "git-config-env") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1xxmy7jwvki1xpvpl1ykdzzxc08zpd132jp7a1n8s8i95jvn061z")))

(define-public crate-git-config-env-0.1.3 (c (n "git-config-env") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "winnow") (r "^0.3.0") (d #t) (k 0)))) (h "1in1ahygbymlahdc8lgf292zf42yv1wgcdp5ym7hgh8ja811vv1f")))

(define-public crate-git-config-env-0.1.4 (c (n "git-config-env") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "winnow") (r "^0.3.0") (d #t) (k 0)))) (h "19jzr1sycvyq7bk4k6f71bp7r4jbdh81q5kz27igsba7nxmyfarm") (r "1.60.0")))

(define-public crate-git-config-env-0.1.5 (c (n "git-config-env") (v "0.1.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "winnow") (r "^0.4.0") (d #t) (k 0)))) (h "05nwjddk8f27lzrs2y79gvqzcnmi1qzaa3h32bp0mbxx98qrsqxk") (r "1.60.0")))

(define-public crate-git-config-env-0.2.0 (c (n "git-config-env") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "winnow") (r "^0.5.0") (d #t) (k 0)))) (h "093kzfv7bmn4dbkdirsddpz0imj30wjc5nqq64ryrckca4x227zp") (r "1.68.0")))

(define-public crate-git-config-env-0.2.1 (c (n "git-config-env") (v "0.2.1") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "winnow") (r "^0.6.0") (d #t) (k 0)))) (h "0f29m8bdykaja7vqvnbjmdff6fk598mllcbyvsdl27vvpbk7dlyp") (r "1.73")))

