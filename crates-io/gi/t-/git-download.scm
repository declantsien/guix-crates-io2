(define-module (crates-io gi t- git-download) #:use-module (crates-io))

(define-public crate-git-download-0.1.0 (c (n "git-download") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cmd_lib") (r "^1.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0h7aa6ip9s61i7k25zlvcccf2fm8b1ba830iymnmyj8v8b27x1ki")))

(define-public crate-git-download-0.1.1 (c (n "git-download") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cmd_lib") (r "^1.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1rv1b2ql9d2gfvbnd1pxzljgsc7m7s8m7fgdp8gw6lj0wdzx8b5s")))

