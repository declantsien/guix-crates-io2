(define-module (crates-io gi t- git-freq) #:use-module (crates-io))

(define-public crate-git-freq-0.0.1 (c (n "git-freq") (v "0.0.1") (d (list (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0hbjc2m5k5i4m1b1165p9hm9hmsgbfxab8v4zlcck0h853vlks9y")))

(define-public crate-git-freq-0.0.2 (c (n "git-freq") (v "0.0.2") (d (list (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1wgpmdm8gsizr0gzrjp1lzyrimx109bh3xhp5izlpsvqlbwbs0gh")))

