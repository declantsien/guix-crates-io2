(define-module (crates-io gi t- git-hashtable) #:use-module (crates-io))

(define-public crate-git-hashtable-0.1.0 (c (n "git-hashtable") (v "0.1.0") (d (list (d (n "git-hash") (r "^0.10.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (f (quote ("inline-more" "raw"))) (k 0)))) (h "1i6yn56l52dwlwy1pksciydx8yxh3zr6cqj2gw5hldncv1d64ay5")))

(define-public crate-git-hashtable-0.1.1 (c (n "git-hashtable") (v "0.1.1") (d (list (d (n "git-hash") (r "^0.10.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (f (quote ("inline-more" "raw"))) (k 0)))) (h "0lnzmf80kym2p1vq08b3pr7jvk3amz0arbk7c2v81ka4cdhzrn04") (r "1.64")))

(define-public crate-git-hashtable-0.1.2 (c (n "git-hashtable") (v "0.1.2") (d (list (d (n "git-hash") (r "^0.10.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (f (quote ("inline-more" "raw"))) (k 0)))) (h "057zzvxnkpf7sna4ga46rahblgisqrvf72hr5y6jkaqmwa6hj4mk") (r "1.64")))

