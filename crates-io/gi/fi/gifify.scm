(define-module (crates-io gi fi gifify) #:use-module (crates-io))

(define-public crate-gifify-0.1.0 (c (n "gifify") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "00lcynhrj4vnnhic3ds6lg78mp27kcwcjzxh3w4i5bmix6kxn566")))

(define-public crate-gifify-0.1.1 (c (n "gifify") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "09arnwp7fxf0q7h763vbdysx0gd7i4wv60cxm5dfkmmzp5yfddwq")))

(define-public crate-gifify-0.1.2 (c (n "gifify") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "024z8waw67p52rr9nyjs9dm3s7brzzaigq9wlib3ix1h0ghs138r")))

(define-public crate-gifify-0.1.3 (c (n "gifify") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0jm8zwrr9bybcarckg7ihndnzivpy9zc7vln0n5xdlgdz1qxcwxr")))

(define-public crate-gifify-0.2.0 (c (n "gifify") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "025gfr2ljs5s92v9gj27sff86690p0iqxalzkka1r1idj050bvi4")))

(define-public crate-gifify-0.2.1 (c (n "gifify") (v "0.2.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1iz707xx8ra1vx684iy59z2mw8y63dmiy1b8qws0p5gif60xngp5")))

(define-public crate-gifify-0.2.2 (c (n "gifify") (v "0.2.2") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vgxnncfkf5wlc9lr10xqf7lwkvd5snkk2vfxpna7wiizlw33c4z")))

