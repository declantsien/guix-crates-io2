(define-module (crates-io gi tn gitnu) #:use-module (crates-io))

(define-public crate-gitnu-0.0.1 (c (n "gitnu") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0ivzvl93q227prwrnlqq8lpghvxxd5djk4xysp8zwq0j363pp879") (y #t)))

(define-public crate-gitnu-0.0.2 (c (n "gitnu") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "11i49iabrgyh5y95blm205vqpcjgnifrgh5n9lhcgidq1hjws97v") (y #t)))

(define-public crate-gitnu-0.0.3 (c (n "gitnu") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1blvcfja9ygr157fyy5gl8vwqg35irlnlmhfqldr6a9qxli6s03m") (y #t)))

(define-public crate-gitnu-0.0.4 (c (n "gitnu") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "07k9wgxbkzfsmja3wshvaz79cc4acw6l72rnv43vb4fc8bj2q388") (y #t)))

(define-public crate-gitnu-0.0.5 (c (n "gitnu") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0j3z89i24akyijaa49ak0m2nhr3my97000hsib1sdpbf2ipnlgz9") (y #t)))

(define-public crate-gitnu-0.0.6 (c (n "gitnu") (v "0.0.6") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1ac7jvxf4gn49kcg289j4pwjbs1rvy6hcsvlqy91s62kmqj88fzi") (y #t)))

(define-public crate-gitnu-0.1.1 (c (n "gitnu") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "033xahkdp6hyn75hhq8y900dgdlygjfa63qpfxkql5hl9wx3kjyj") (y #t)))

(define-public crate-gitnu-0.1.0 (c (n "gitnu") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0qal7px1yqkscs9hpsr0qwc9n0qwbfgzn2x669l4vrdrr19gcxk4") (y #t)))

(define-public crate-gitnu-0.1.2 (c (n "gitnu") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1aqqy83l1x4qxjjjbiq8vflgwaj3pkc5z0x7ihcsa1war3sd1lhj") (y #t)))

(define-public crate-gitnu-0.1.3 (c (n "gitnu") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1k5739bf5gy5bf43dvd5xyvypb3idm1l06iidl3s2samffp46k18") (y #t)))

(define-public crate-gitnu-0.1.4 (c (n "gitnu") (v "0.1.4") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "122m93p1i4yz64w0xi0r2myibb4sv9g0abvqlf1rky5hj38rhsg8") (y #t)))

(define-public crate-gitnu-0.1.5 (c (n "gitnu") (v "0.1.5") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)))) (h "1czyl8j9asvyylj87i89nn3lmna536k8nlm8mmd9pgg0k3bsg64i") (y #t)))

(define-public crate-gitnu-0.1.6 (c (n "gitnu") (v "0.1.6") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)))) (h "0rlzan6yib5ga3dqsd2ccdkmbwp20219w1r1g5rcv0pgn9x00w84") (y #t)))

(define-public crate-gitnu-0.1.7 (c (n "gitnu") (v "0.1.7") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)))) (h "0sfmk8gc0653rnbvvvbvi59hbpyrv1ff04x16ay6rmhv5kn2sw5m") (y #t)))

(define-public crate-gitnu-0.1.8 (c (n "gitnu") (v "0.1.8") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)))) (h "1fnrr6mg7b961lsq9dy34hd504yvrkxqhswrmnl5mb169592lv6n") (y #t)))

(define-public crate-gitnu-0.1.9 (c (n "gitnu") (v "0.1.9") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)))) (h "1n4szbpnk5dx9jkfp0cgzsgxp6ksbrg559yf8lsazq3bdwp8mhy1") (y #t)))

(define-public crate-gitnu-0.2.0 (c (n "gitnu") (v "0.2.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)))) (h "1ns3wb7wnvzkq7rbkrdv6j62wyym9yw6nhmc9i0pxrjs4vwwj9g1") (y #t)))

(define-public crate-gitnu-0.2.1 (c (n "gitnu") (v "0.2.1") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)))) (h "12hfq7ms3viyd7jgwp7ppfy2h8n35ql1qfpkw0s104wjdm7ws7iv") (y #t)))

(define-public crate-gitnu-0.2.2 (c (n "gitnu") (v "0.2.2") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)))) (h "0v178xxl69mfllai623cqzs2skfy2i8rvfjf3s74x6zcb049kyjp") (y #t)))

(define-public crate-gitnu-0.2.3 (c (n "gitnu") (v "0.2.3") (h "0q7dz7vphf5h0jm5jmk98w2vyfpf7pb11mf5afcg0ninhg7ydymp") (y #t)))

(define-public crate-gitnu-0.3.1 (c (n "gitnu") (v "0.3.1") (h "0v7krpxv8wvvqiblipljijc8qm006hzyxmcyc3kazncv9a8v3asv") (y #t)))

(define-public crate-gitnu-0.3.0 (c (n "gitnu") (v "0.3.0") (h "1h6x8bw73wpf6h9csd6z057k6w5x2xqw7g74skzdgpcyvbc0n7s5") (y #t)))

(define-public crate-gitnu-0.4.0 (c (n "gitnu") (v "0.4.0") (h "0xa7gbamd8lxccpji6279srj2rbyqc11pc4fjk86m0j7wyxhx34c") (y #t)))

(define-public crate-gitnu-0.4.1 (c (n "gitnu") (v "0.4.1") (h "1p7kg1d3gxg34lkxrb45m068xdmqx7imn8fsq5idhrj5rlivlqf4") (y #t)))

(define-public crate-gitnu-0.4.2 (c (n "gitnu") (v "0.4.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0h0mbvg2d0zyl7k4n6nk28gm5ipidsl94dan0j9x8sw4z89hcknr") (y #t)))

(define-public crate-gitnu-0.4.3 (c (n "gitnu") (v "0.4.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "18fbjf0r51460wx6mlnf9q45psa9904z9h7d74ii7f9ppkam2sm1") (y #t)))

(define-public crate-gitnu-0.4.4 (c (n "gitnu") (v "0.4.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1klyr7b0qdfcj3k59dwzn9chvcc5rm6v80cy4w39374p4nlbvq6p") (y #t)))

(define-public crate-gitnu-0.4.5 (c (n "gitnu") (v "0.4.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0i5yryl7af77f01d1njzv28vcyf9jvn8xa3k1s7x1yf9f9fx0ha8") (y #t)))

(define-public crate-gitnu-0.4.6 (c (n "gitnu") (v "0.4.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0xvdwdi2yhgm268fvxn46sm8f76hw54fq3xfznd71c955qf3zxyz") (y #t)))

(define-public crate-gitnu-0.4.7 (c (n "gitnu") (v "0.4.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "13rxlbbfxj5dil1j75wk5bxjrv1krb6wk5vvhijxzvyvqgahj389") (y #t)))

(define-public crate-gitnu-0.4.8 (c (n "gitnu") (v "0.4.8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0mn4cnjzwx0avi6gyhqn8ql011zrknayqzk1qr50k96ap8hcsb8g") (y #t)))

(define-public crate-gitnu-0.5.0 (c (n "gitnu") (v "0.5.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0j2lc1ha07axm1ps26n6nvyply6l6hn0fsvsbxjlpz521a5ghx7d")))

(define-public crate-gitnu-0.5.1 (c (n "gitnu") (v "0.5.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "183wbrbxj16yc1fddk5f0v81xnbam1s7z99a7blwzmd172bc76lp")))

(define-public crate-gitnu-0.6.0 (c (n "gitnu") (v "0.6.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "063sy7vlnwmdvq552vk0cl191zljdhyiplci67xvh0fahbdhkvbh")))

(define-public crate-gitnu-0.6.1 (c (n "gitnu") (v "0.6.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1scwzrn6grgpqcnx1p18r2hngcab491js3j9kszg0hjba8pvbywl")))

(define-public crate-gitnu-0.6.2 (c (n "gitnu") (v "0.6.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1q6dzphwsv6kmwaay1rd6d2mp4xwkrx4is1zwkqwkqxwsh12d6d5")))

(define-public crate-gitnu-0.6.3 (c (n "gitnu") (v "0.6.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "02d126c2x2sfmhxg7n89n5bw1qxslzs08v5zwrgaslzrphr8dyv5")))

(define-public crate-gitnu-0.6.4 (c (n "gitnu") (v "0.6.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0yq42laim465bq94mf791iiaa74rmc07vcksf37na0fff0kl4d9h")))

(define-public crate-gitnu-0.6.5 (c (n "gitnu") (v "0.6.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0bg9fmdmy1dvs3wclqj35njxwadrwkbqxknjdg8k266ryalyq8f1")))

(define-public crate-gitnu-0.6.6-alpha1 (c (n "gitnu") (v "0.6.6-alpha1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0nyy9700nhd7c9xcy68i74d9rx4ykfx2bpplagbv4il5x3dpgzw1")))

(define-public crate-gitnu-0.6.6-alpha2 (c (n "gitnu") (v "0.6.6-alpha2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0vhdmpnndgx5l9397b48f4cqlzqlgm75qpb289k9b0yal69j8gni")))

(define-public crate-gitnu-0.6.6-alpha3 (c (n "gitnu") (v "0.6.6-alpha3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "043dh8gp4f3gjn6md2bl4kzg59yxb9gjwqimv4r5y6lysnpdbkmj")))

(define-public crate-gitnu-0.6.6-alpha4 (c (n "gitnu") (v "0.6.6-alpha4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0h6zqzpbwxms378434d8j9yvxw2yv2cx995zkhjq8hjg5g7x56ng")))

(define-public crate-gitnu-0.6.6-alpha5 (c (n "gitnu") (v "0.6.6-alpha5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "05nw7a9459cxxcvyawnxygv7ja3h4g8nybxqvi0w8p0rzlxvh6v9")))

(define-public crate-gitnu-0.6.6-alpha6 (c (n "gitnu") (v "0.6.6-alpha6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "113hbf1hxlrs5v9njk7k385dszfalg094krzzphlsm79hhvywlqj")))

(define-public crate-gitnu-0.6.6-alpha7 (c (n "gitnu") (v "0.6.6-alpha7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1bgyc2467mxgr4gprg6lyl7glcswfp1myfyw7ks7qbakwj0ifd05")))

(define-public crate-gitnu-0.6.6-alpha8 (c (n "gitnu") (v "0.6.6-alpha8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1qdjsmsw37rhcbq2s2q2i2nj0ca5d2qszhmxi52d511a7czwwykg")))

(define-public crate-gitnu-0.6.6-alpha9 (c (n "gitnu") (v "0.6.6-alpha9") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "19yzwfvp30q1f8jdmd9q2pqz1dpxz59j8d7qqkm4bf4sfm2i55qi")))

(define-public crate-gitnu-0.6.7 (c (n "gitnu") (v "0.6.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1rk0viahyvyv37sjxwfs7k5g3gsikxbv4q7ngk7igijjh4n8q4j2")))

(define-public crate-gitnu-0.6.8 (c (n "gitnu") (v "0.6.8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1iv4yf65x316m9prqhimjj8bjwgsm8a6faygfk5pgg9918ckgg38")))

(define-public crate-gitnu-0.6.9 (c (n "gitnu") (v "0.6.9") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "147mqwfb0v26zzn2qhdhpxqlrkpcbhkd39qpg1g93a1qacp4zxd5")))

(define-public crate-gitnu-0.7.0 (c (n "gitnu") (v "0.7.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1w26s4v4sj8rbdk1bbldcy499mbybcl6cfxp7p4h45zd8i3m2b4n")))

(define-public crate-gitnu-0.7.1 (c (n "gitnu") (v "0.7.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1ya3s6c91zq6lijxjr3s20wjprkix7hsv721b953f4sajl88sbpv")))

(define-public crate-gitnu-0.7.2 (c (n "gitnu") (v "0.7.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0nnwrac0ai7s9dsdij0g8wvjm3wziiwg0nz8q8j2r9xdm1x8870f")))

(define-public crate-gitnu-0.7.3 (c (n "gitnu") (v "0.7.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "04537mj07nh4l87q4rsab5wwvrd00w4ccbpqjvd1cjp098rnvn5j")))

(define-public crate-gitnu-0.7.4 (c (n "gitnu") (v "0.7.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0zi7gmgsl3hp5nz8vdmdpkv4wfbqnjircpi2qpy95g6ch3dl3r6v")))

(define-public crate-gitnu-0.7.5 (c (n "gitnu") (v "0.7.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1fgbjqzalbi409j31sgmaf46fq9f781m0c82dsxpg5qxsv1n3yx3")))

(define-public crate-gitnu-0.7.6 (c (n "gitnu") (v "0.7.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1jjlidfp1fx97gqxw7mcmxayv46c4gkqsblrn59lws0jki49983k")))

