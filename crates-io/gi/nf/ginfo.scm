(define-module (crates-io gi nf ginfo) #:use-module (crates-io))

(define-public crate-ginfo-0.1.0 (c (n "ginfo") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0icrv25myr5ighqmn10j9444kn84hz51mpal81bgs8q55q999cbk")))

(define-public crate-ginfo-0.1.1 (c (n "ginfo") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18km72z7723ggycjv0w4ah8xm01rwdxi7xxgsn0my556y6zwy8nc")))

