(define-module (crates-io gi vm givme) #:use-module (crates-io))

(define-public crate-givme-0.1.0 (c (n "givme") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nettle") (r "^7.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)) (d (n "whoami") (r "^1.1.5") (d #t) (k 0)))) (h "0155jfin8831hl0n6rpxbhlqb1k9vqf57spxamf0vgwbb402py2w")))

