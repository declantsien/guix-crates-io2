(define-module (crates-io gi rt girt-display) #:use-module (crates-io))

(define-public crate-girt-display-1.0.0 (c (n "girt-display") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "girt-config") (r "^1.0.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "08l51w9wy1zvhdfkv0dh6p4p7wbvv5lixhinh1llmzjsrc2aiy3i")))

(define-public crate-girt-display-2.2.0 (c (n "girt-display") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "girt-config") (r "^2.2.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.6.0") (d #t) (k 2)))) (h "1r2v595fqpb9gyq7vc4160r8fcxgklrd06vpmk30izwsnn83jsbd")))

(define-public crate-girt-display-2.3.0 (c (n "girt-display") (v "2.3.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "girt-config") (r "^2.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0lwyzxfnl53mzyzf2mkkyig1mypmlf9l0k0qbi5snkz2idmjv6b7")))

