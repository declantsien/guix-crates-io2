(define-module (crates-io gi rt girt-config) #:use-module (crates-io))

(define-public crate-girt-config-1.0.0 (c (n "girt-config") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 2)) (d (n "git2") (r "^0.13.20") (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "05k1xbnp6afzjyh898b3wisxdvs99jxiz6rc7k4z8mdskjcwasx0")))

(define-public crate-girt-config-2.2.0 (c (n "girt-config") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "girt-git") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serial_test") (r "^0.6.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1381f7vj6p4kvbj8mbr8f7q1w156anwjcyqs9a8xpvsxakiarp7n")))

(define-public crate-girt-config-2.3.0 (c (n "girt-config") (v "2.3.0") (d (list (d (n "girt-git") (r "^2.3.0") (d #t) (k 0)) (d (n "girt-testutils") (r "^2.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "12p46i90l59w12zap16y9vsbdx8gcf4n7f6h4sqjn1cgi64jm10s")))

