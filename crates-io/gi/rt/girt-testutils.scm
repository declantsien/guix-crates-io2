(define-module (crates-io gi rt girt-testutils) #:use-module (crates-io))

(define-public crate-girt-testutils-0.1.0 (c (n "girt-testutils") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0k2vyabkzifpw473zary52h1pz9017sw606xmlpk0mji95gxbyw1")))

(define-public crate-girt-testutils-2.3.0 (c (n "girt-testutils") (v "2.3.0") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0ih8vqqqgx5jrqbabi4f3ixzk6rv9gdlsb2q8yxcz1lx859cispa")))

