(define-module (crates-io gi rt girt-todo-file) #:use-module (crates-io))

(define-public crate-girt-todo-file-1.0.0 (c (n "girt-todo-file") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "concat-idents") (r "^1.1.2") (d #t) (k 2)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "19m1gln4bbv7d1xcsi3kj033n9m2kna86407fldz7qywxinbk8l6")))

(define-public crate-girt-todo-file-2.2.0 (c (n "girt-todo-file") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0inixcw0y0cvndw6nkj9fny4cs4d6wqqjn0gmlzsjj5fi3lsz94j")))

(define-public crate-girt-todo-file-2.3.0 (c (n "girt-todo-file") (v "2.3.0") (d (list (d (n "girt-testutils") (r "^2.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "version-track") (r "^0.1.0") (d #t) (k 0)))) (h "18ck3la9q9rykqv3lwva1vkc4d44d48zdnm666mml1bfjsr732jy")))

