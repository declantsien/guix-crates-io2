(define-module (crates-io gi rt girt-runtime) #:use-module (crates-io))

(define-public crate-girt-runtime-0.1.0 (c (n "girt-runtime") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "10c4csalfyj7zmlgn4qddvmgx58qimqg6bc1v7mkc5mc9psiidwj")))

(define-public crate-girt-runtime-2.3.0 (c (n "girt-runtime") (v "2.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "girt-testutils") (r "^2.3.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1a472safxzpcljng17bk2js876swx4dwbdjgwqd3z9q09sdvb86f")))

