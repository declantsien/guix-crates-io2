(define-module (crates-io gi td gitdb) #:use-module (crates-io))

(define-public crate-gitdb-0.1.0 (c (n "gitdb") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "git2") (r "^0.6.11") (d #t) (k 0)) (d (n "ring") (r "^0.13.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1sasj3svbd1ia8sif0ga9331z3m4jjdm9ah8649vpv1g7rl3y9fr")))

(define-public crate-gitdb-0.1.1 (c (n "gitdb") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.1.1") (d #t) (k 0)) (d (n "git2") (r "^0.6.11") (d #t) (k 0)) (d (n "ring") (r "^0.13.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0mhs4zpq99hzlzcmqs8pbj6w949fghppfs3n36y1qfj2pyam04l4")))

