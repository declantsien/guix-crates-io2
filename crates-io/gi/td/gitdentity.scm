(define-module (crates-io gi td gitdentity) #:use-module (crates-io))

(define-public crate-gitdentity-1.0.0 (c (n "gitdentity") (v "1.0.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git-config") (r "^0.1.0") (d #t) (k 0)) (d (n "promptly") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.2") (d #t) (k 0)))) (h "0ydfx8jczfmh4bvk10m9qrwk1fap73zpvm5s9rk1qg90jnv3g1f7") (f (quote (("bundled" "rusqlite/bundled"))))))

(define-public crate-gitdentity-1.0.1 (c (n "gitdentity") (v "1.0.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git-config") (r "^0.1.0") (d #t) (k 0)) (d (n "promptly") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.2") (d #t) (k 0)))) (h "1crz3c93r36mqsz6mfay7zwnhbpx8r9jyl1f43xm1y420nqh8064") (f (quote (("bundled" "rusqlite/bundled"))))))

(define-public crate-gitdentity-1.0.2 (c (n "gitdentity") (v "1.0.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git-config") (r "^0.1.0") (d #t) (k 0)) (d (n "promptly") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.2") (d #t) (k 0)))) (h "0m9426jg591p71gwqfj3rqm45v10zc72wka5kvpfsir789vhcr8l") (f (quote (("bundled" "rusqlite/bundled"))))))

