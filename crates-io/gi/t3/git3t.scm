(define-module (crates-io gi t3 git3t) #:use-module (crates-io))

(define-public crate-git3t-0.1.0 (c (n "git3t") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "graphql_client") (r "^0.12.0") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "036mwp2z4crniyasljvm677pm60g1f5vn2szp5zs8hs814f1pygf")))

