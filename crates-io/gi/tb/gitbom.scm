(define-module (crates-io gi tb gitbom) #:use-module (crates-io))

(define-public crate-gitbom-0.1.0 (c (n "gitbom") (v "0.1.0") (h "00qjyykfkcncgqlmsn26yvvq443g0w34nc2zlf2p3cax33zi9qqk") (y #t)))

(define-public crate-gitbom-0.1.1 (c (n "gitbom") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "gitoid") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "im") (r "^15") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("io-util" "fs" "rt" "macros"))) (d #t) (k 0)))) (h "0s38kh23mh5sf4x3jkq0c178mw4zrda5g61s0llxfxb5mpq45hbc") (y #t)))

(define-public crate-gitbom-0.1.2 (c (n "gitbom") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "gitoid") (r "^0.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("io-util" "fs" "rt" "macros"))) (d #t) (k 0)))) (h "08ln6yr5fxjkrx2g2hm738nak7sxjjmk5g3lvvyg771q3439s1im") (y #t)))

(define-public crate-gitbom-0.1.3 (c (n "gitbom") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "gitoid") (r "^0.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("io-util" "fs" "rt" "macros"))) (d #t) (k 0)))) (h "13jq8w2y1sv7vjqy0ylsj68nxw2iawswxxr5i1jsrzwg4r253g99") (y #t)))

(define-public crate-gitbom-0.1.4 (c (n "gitbom") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "gitoid") (r "^0.1.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("io-util" "fs" "rt" "macros"))) (d #t) (k 0)))) (h "0fm6yjgls3glxh55sn5hsgq5xbfp3pg3b70zml8wmw54l51i7z1j") (y #t)))

(define-public crate-gitbom-0.1.5 (c (n "gitbom") (v "0.1.5") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "gitoid") (r "^0.1.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("io-util" "fs" "rt" "macros"))) (d #t) (k 0)))) (h "0s6kvv604w843fp77rjqqcp1x2mjb1gr10gcpimzyqhip189nz8z") (y #t)))

(define-public crate-gitbom-0.1.6 (c (n "gitbom") (v "0.1.6") (d (list (d (n "gitoid") (r "^0.1.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "1fwd5qrhpx2ix8n6q7syyp33wq879jl6jm5wbdcf8v3brgh8iqx9") (y #t)))

(define-public crate-gitbom-0.1.7 (c (n "gitbom") (v "0.1.7") (d (list (d (n "gitoid") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "0b121s3jcbz9wy75jjjgkkzl6ql53m7x1drz3djjx7fcii77n9hq") (y #t)))

