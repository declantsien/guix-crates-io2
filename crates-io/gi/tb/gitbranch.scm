(define-module (crates-io gi tb gitbranch) #:use-module (crates-io))

(define-public crate-gitbranch-0.1.0 (c (n "gitbranch") (v "0.1.0") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)))) (h "1azbk8n8xa9bp6hb635znm0z9i570cqiixjzzs3879acpf3jxy18")))

(define-public crate-gitbranch-0.1.1 (c (n "gitbranch") (v "0.1.1") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)))) (h "1p00svqcqhnr5qcjf7ajyqh03n56gj7gkmkininsblj0zmh4rm3d")))

(define-public crate-gitbranch-0.1.2 (c (n "gitbranch") (v "0.1.2") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)))) (h "1h4p1khr4irmbrd151kvx0h30qxd03vlml426lp39zi90j954hzz")))

(define-public crate-gitbranch-0.1.3 (c (n "gitbranch") (v "0.1.3") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)))) (h "1cw8mwf49fcg141503vjr901lmcmiqf4kqibhqc4is005g9mqy4v")))

(define-public crate-gitbranch-0.1.4 (c (n "gitbranch") (v "0.1.4") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)))) (h "0qxj0whh3ffvcgcc3admawj6nchagdx8rw76c4n05qrlyj9fgikn")))

(define-public crate-gitbranch-0.1.5 (c (n "gitbranch") (v "0.1.5") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)))) (h "1qkbdlg6agrfczmc34nzqp77vrgkmkqbhn020villxslvi75qxnf")))

(define-public crate-gitbranch-0.1.6 (c (n "gitbranch") (v "0.1.6") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)))) (h "1pyiszd1va2vn5c9wvcyqgp5xckldjdrab4hq7j8f2bnxgl6wz94")))

(define-public crate-gitbranch-0.1.7 (c (n "gitbranch") (v "0.1.7") (d (list (d (n "git2") (r "^0.17") (d #t) (k 0)))) (h "17lkx4rhhzjngcx422xv8b1wj1gksby9hpgca2dwrpra81yz032s") (y #t)))

(define-public crate-gitbranch-1.0.0 (c (n "gitbranch") (v "1.0.0") (d (list (d (n "git2") (r "^0.17") (d #t) (k 0)))) (h "15k2gsyy2y9aygm91d15485k7wrdg87kgm7vr1i47pxd4qfmjp9i") (y #t)))

