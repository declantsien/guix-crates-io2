(define-module (crates-io gi tb gitbrowse) #:use-module (crates-io))

(define-public crate-gitbrowse-0.0.1 (c (n "gitbrowse") (v "0.0.1") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "0nlrqv4knaghxir6m7czpfgj9x49j2a7ls7a3fa90igk1svp24h0")))

(define-public crate-gitbrowse-0.0.2 (c (n "gitbrowse") (v "0.0.2") (d (list (d (n "git2") (r "^0.13") (k 0)))) (h "1w0f8ff6rbgp9njsdmvc16mh7mhhypzy95l0wp9rar9myps2ypak") (f (quote (("zlib-ng-compat" "git2/zlib-ng-compat") ("vendored-openssl" "git2/vendored-openssl") ("unstable" "git2/unstable") ("ssh_key_from_memory" "git2/ssh_key_from_memory") ("ssh" "git2/ssh") ("https" "git2/https") ("default" "git2/default"))))))

(define-public crate-gitbrowse-0.0.3 (c (n "gitbrowse") (v "0.0.3") (d (list (d (n "git2") (r "^0.13") (k 0)))) (h "12632rlr2p5kizg8bzzzcpjsniinnzjwg3g7401ic3hr3x8lc4f9") (f (quote (("zlib-ng-compat" "git2/zlib-ng-compat") ("vendored-openssl" "git2/vendored-openssl") ("unstable" "git2/unstable") ("ssh_key_from_memory" "git2/ssh_key_from_memory") ("ssh" "git2/ssh") ("https" "git2/https") ("default" "git2/default"))))))

(define-public crate-gitbrowse-0.0.4 (c (n "gitbrowse") (v "0.0.4") (d (list (d (n "git2") (r "^0.13") (k 0)))) (h "0qy06l51iah7805dxbrvzkbgg5vkid5lydrd3zi4s8kqbsqfxpc8") (f (quote (("zlib-ng-compat" "git2/zlib-ng-compat") ("vendored-openssl" "git2/vendored-openssl") ("unstable" "git2/unstable") ("ssh_key_from_memory" "git2/ssh_key_from_memory") ("ssh" "git2/ssh") ("https" "git2/https") ("default" "git2/default"))))))

(define-public crate-gitbrowse-0.0.5 (c (n "gitbrowse") (v "0.0.5") (d (list (d (n "git2") (r "^0.13") (k 0)))) (h "1sm06l30xczqcqpk6i8pnmcqkjdv1kbhww390cpkdqck3n0165kb") (f (quote (("zlib-ng-compat" "git2/zlib-ng-compat") ("vendored-openssl" "git2/vendored-openssl") ("unstable" "git2/unstable") ("ssh_key_from_memory" "git2/ssh_key_from_memory") ("ssh" "git2/ssh") ("https" "git2/https") ("default" "git2/default"))))))

(define-public crate-gitbrowse-0.0.6 (c (n "gitbrowse") (v "0.0.6") (d (list (d (n "git2") (r "^0.14") (k 0)))) (h "0546p7pajf5ln856px8xhlr063mvlwyxvh0506jzxg4cd2pm6m4d") (f (quote (("zlib-ng-compat" "git2/zlib-ng-compat") ("vendored-openssl" "git2/vendored-openssl") ("unstable" "git2/unstable") ("ssh_key_from_memory" "git2/ssh_key_from_memory") ("ssh" "git2/ssh") ("https" "git2/https") ("default" "git2/default"))))))

