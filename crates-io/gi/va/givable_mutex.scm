(define-module (crates-io gi va givable_mutex) #:use-module (crates-io))

(define-public crate-givable_mutex-0.1.0 (c (n "givable_mutex") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "0690zik6pbrjxqlj7mmazap13h7i130jl57hlj1n6kha8hd1kyfg") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-givable_mutex-0.2.0 (c (n "givable_mutex") (v "0.2.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "086kvlqb4pxsxr9vrh9drhyy70ndqb54jmws5sm4ky268vl8pkb0") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-givable_mutex-0.3.0 (c (n "givable_mutex") (v "0.3.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "031asj8fy2x53n219mvdyq20g9imma1r461iz4hzp685sxg1bmv3") (f (quote (("nightly") ("default" "nightly"))))))

