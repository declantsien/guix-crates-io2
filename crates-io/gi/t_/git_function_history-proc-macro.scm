(define-module (crates-io gi t_ git_function_history-proc-macro) #:use-module (crates-io))

(define-public crate-git_function_history-proc-macro-0.1.0 (c (n "git_function_history-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0wji3317vfxsnwvb43dhp0arpz9ba3ywrvpf5m9i56dbc08zsrn5") (r "1.56.1")))

