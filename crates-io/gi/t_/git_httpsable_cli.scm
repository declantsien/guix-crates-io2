(define-module (crates-io gi t_ git_httpsable_cli) #:use-module (crates-io))

(define-public crate-git_httpsable_cli-0.1.0 (c (n "git_httpsable_cli") (v "0.1.0") (d (list (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "1xryh13kcymlj1pp3yd7k5ybg7bzc08793nqf3imhg5m4ijqb8i2")))

(define-public crate-git_httpsable_cli-0.1.1 (c (n "git_httpsable_cli") (v "0.1.1") (d (list (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "1k8010lzm90hkprld2z26hb7g9w08xrq46wlzqxwz3cpy2ddz4q6")))

(define-public crate-git_httpsable_cli-0.2.0 (c (n "git_httpsable_cli") (v "0.2.0") (d (list (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "0y1npqx2k6x5npnigr8dqyc7af9wmsa9xzrqkpf163kzj0ylhfkm")))

(define-public crate-git_httpsable_cli-0.3.0 (c (n "git_httpsable_cli") (v "0.3.0") (d (list (d (n "git_httpsable") (r "^1.0.0") (d #t) (k 0)))) (h "1wc9h6sj7ccsvp8pakv5bi8flwza1mr7plsg67hsyxi8iid3ypw6")))

