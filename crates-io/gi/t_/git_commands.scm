(define-module (crates-io gi t_ git_commands) #:use-module (crates-io))

(define-public crate-git_commands-0.2.0 (c (n "git_commands") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1a3z7x050gqjpzyn4hl9lr2lnsnnc6f3pl558d6jii6iff5wiqh7")))

