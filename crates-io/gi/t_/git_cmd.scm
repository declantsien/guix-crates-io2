(define-module (crates-io gi t_ git_cmd) #:use-module (crates-io))

(define-public crate-git_cmd-0.1.0 (c (n "git_cmd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "test_logs") (r "^0.1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1rpbzrirna07wzfifg7ypwddrydgji9zsb1f2njacqfw8s7b8rz4") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.1.1 (c (n "git_cmd") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "claim") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1yslvxbas7915jsqkxlfwgh66p2r01pj76k493a6q5n6g6yinflp") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.1.2 (c (n "git_cmd") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "claim") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0zlxz0bycvqgrigvialj2nvh04nx6kny8g30nv4ak2kd4z5wbpnq") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.1.3 (c (n "git_cmd") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "claim") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fpfhjc0h5kqbc0bxc80ddn1slv2xi9j2xv17hd3p8il6j6hlb5a") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.1.4 (c (n "git_cmd") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "claim") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1lvrz85sm53lhr3n4z7bbxdxkcvly8n7gsbmkc9mmf60ram2cb4f") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.1.5 (c (n "git_cmd") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "04hx24hv3g6nb9j32qaidwxy3zgqsmxldv19q5jwfn46p56glgnp") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.1.6 (c (n "git_cmd") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "10bh3b83zsk3xhnnmwmyinchak8xk42a7a08n9k8vsx5g7fca7bc") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.0 (c (n "git_cmd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1yi5lvlfczvc0i225qqihf7rrv3a2vhghmgl5a30v57dj9n599ik") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.1 (c (n "git_cmd") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1wdrxj99198klhws0jm0cf97r3k31lbbgpnhp8hw99f2z4wr42yi") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.2 (c (n "git_cmd") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0r49dx137b5gj289nbdhrgda5jba9f0f8mj44n9kqs2zhmmrsg1c") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.3 (c (n "git_cmd") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0d67zp21dgdl7vg3zvkk6yg24jl6hha7qycss5bhn349qhsbgw2p") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.4 (c (n "git_cmd") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1jn81rg82a0ch6fhxms0a8p9s10fi9kc369y2c7s7qpw84q3y36j") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.5 (c (n "git_cmd") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0pml5yi1h98f47f5s17bnm06jzyvxrkm5vk32bmbz42rwkk5ys3f") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.6 (c (n "git_cmd") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "05c45r15jhlqihisswdi4p1krmmd0fahvhrkzprn8d7gvkbi1n84") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.7 (c (n "git_cmd") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1hi7k74vvxkfm40wnsm5n2lsw7qvdqnm3ysdvsydn5zyr0sy3jxf") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.8 (c (n "git_cmd") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0m00p3qilnk0cvqy7ma0b01fafd4c259hx8cfgnk26w9agws37y2") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.9 (c (n "git_cmd") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1mhhjpvhpxm02x7hw9rw589is96pqmsy5p8vbibycv4isvnh3q9j") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.10 (c (n "git_cmd") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1yahxx41d1x04w99vzbmjl8j8752j36bgh8gw6bwkwm7p8c4jl2j") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.11 (c (n "git_cmd") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "181wgx7y1d2lbcwsmv2kzwvid2x7rn7m1wzqm83nqz3q9d4xxvj9") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.12 (c (n "git_cmd") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0h6ln8rk2lbzcs9ynxpx3xijxpghxkilvv3s0fwhfxf2rlrkb6f5") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.13 (c (n "git_cmd") (v "0.2.13") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1cjm4hsg56py8dkf8s98md3gnc6yzyd3hc6g93il9v5ygaw3ddia") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.14 (c (n "git_cmd") (v "0.2.14") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1rv7al7k6rrg7rqvxlj06pr1d6vlacqsj6ymdxks5552nls66xpa") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.15 (c (n "git_cmd") (v "0.2.15") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1b86lns4ibqsj4mh2sd8z5mrzddfwlarr3n2p2h24y3fa1xzqsdl") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.2.16 (c (n "git_cmd") (v "0.2.16") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0aj0ypls31fn1b7iczjrnfp81k7rafhpxmqji6gfjjp1iskad6mz") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.3.0 (c (n "git_cmd") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0bb252skr5bf51xg0dmcvvacgibzx0k8zg2gpf9kb9mr1vkv1pbl") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.0 (c (n "git_cmd") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "03qk1hazjz0wrdy45cal4d5yq204r4vq8hvh2b20kfxhnzkyzs29") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.1 (c (n "git_cmd") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1ifdb0rq0wh2sfbxgkzhb0j4871jj0vfdimwakx3jwg4a7inbkd4") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.2 (c (n "git_cmd") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0sql8hiy5zwvy6hh6kcy8pcb7m5k978sjwj2x1bc8df75xz8a1bp") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.3 (c (n "git_cmd") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.38") (d #t) (k 0)))) (h "01jyf9bdgw9kyqhlr1ffzc1aik501mpf6x6sf11k7xsbhn92hx31") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.4 (c (n "git_cmd") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1ly0h46xf442nqkhr2d96z19m4p5gr724saqmrd5x7fijwpk1alf") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.5 (c (n "git_cmd") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "08sxvfddaf1rycmdpm6g4xqhxzc49wyllwmkv3hlwh75b9vs0xvn") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.6 (c (n "git_cmd") (v "0.4.6") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0zdj2gy0wwsiifnnnh5qcnz2j9i33fzws25cha5wv7rci1w2s99l") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.7 (c (n "git_cmd") (v "0.4.7") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "11036wzlbv9lfcnq5fyj3qav1dcv7l5yyz2za4pn3ah210cvkycw") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.8 (c (n "git_cmd") (v "0.4.8") (d (list (d (n "anyhow") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "015ybj3ckpc85i93q2y91zl3zw9sfx5dyk75pbzgk2dbyvgz4d7s") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.9 (c (n "git_cmd") (v "0.4.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1jcjnrcas4kvv5n12wxwylyxlzhnzbc6ki6prwiyamsvw02c9xkm") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.10 (c (n "git_cmd") (v "0.4.10") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "004mag16vfxvbykny2zwddwb0ggz89czh05knmij8kf5ybkq0bsy") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.11 (c (n "git_cmd") (v "0.4.11") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "09b5jnl7a93rmp1g7ggwsdh8gxmsxzi2r8frj2gnm7bplqqjpf9y") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.12 (c (n "git_cmd") (v "0.4.12") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0min92hhysp1ryj3mlqrdfz3cwlm0pi1v7j7y6ky92lrxdygb6ja") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.13 (c (n "git_cmd") (v "0.4.13") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.39") (d #t) (k 0)))) (h "090wgc8cyip02jaqi2ybilq7ly1j9f9py7bkm9w7zi0d8l0g3jjh") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.14 (c (n "git_cmd") (v "0.4.14") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1r0wjl86xx9mj13ld3j6iv0z13mc5f2f2l0wdl5pvnc59zkamai4") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.15 (c (n "git_cmd") (v "0.4.15") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1jpmfa2kx4x5lwzd378vmfygzypy308sja898k99i2snh11hdg9m") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.16 (c (n "git_cmd") (v "0.4.16") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "07nrxfnrsa6388ifm0sc9k4ci9skb11wfnz0k3xnxl5zj960yl38") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.17 (c (n "git_cmd") (v "0.4.17") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1jqfp26srlb2209v934c2dl5bqs68ssy431gs2jc16gcxmppk9s8") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.18 (c (n "git_cmd") (v "0.4.18") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1l2ys81kq2figr0y9c0w5cxh7rj49zwl2inlfr4bcdyjxmf3lapp") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.19 (c (n "git_cmd") (v "0.4.19") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "038js5j91b8rfqgqf0hap5nbyk5ajbqkkpsnysv3ky72sxals3ln") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.20 (c (n "git_cmd") (v "0.4.20") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "17vm7n6fysy5bw3kdc2zav9pyfyy08zccx86bkvyzxczcz1r1w59") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.21 (c (n "git_cmd") (v "0.4.21") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0f5jw86i5v1cm3skqmglhd2gqdcm9bib8gm486mb17r74ak2sjm3") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.4.22 (c (n "git_cmd") (v "0.4.22") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "19vcp4w7g9s9ww74xjnhsf5a77i1xmsbad27sq4xwgp702h5jhnd") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.5.0 (c (n "git_cmd") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0gaycdgm1c48lvhpf3sxicrh563gnpyizh6b803j058x78cznlw9") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.6.0 (c (n "git_cmd") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "04rpzmijh865sdamh0n9jxm9vdy7f7xp4ciwhf9x56683f31gr6y") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.6.1 (c (n "git_cmd") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1mgws0xqkgs26v34fjr619nffd6h5cl5qna4jg2rh8wi62ych4wr") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.6.2 (c (n "git_cmd") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0nmvam4b4jv3g6sznr0v8ggql12gfv5d9p8b36vb0l31gfdgpjrr") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.6.3 (c (n "git_cmd") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1px2wbsmvln0gcd9xd8yv486ksn9ibzinlvsvfbswb1zgc4fw1w3") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.6.4 (c (n "git_cmd") (v "0.6.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0xd3zv2qcins4bcsp1pa93cqx3blv984x2pm7byjdpc2hyxr2p8g") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.6.5 (c (n "git_cmd") (v "0.6.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1dh2mkm3k3adqrd1cz2nvckxdamagz8znly5kb36gc4k51kr650a") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.6.6 (c (n "git_cmd") (v "0.6.6") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "fs-err") (r "^2.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "16bsivzz74h4szdh5yhv9iyjm57wv1j18pyqi1p5dhrmwqs5fvkp") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.6.7 (c (n "git_cmd") (v "0.6.7") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "fs-err") (r "^2.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0h4764s70f1zpr0hd0m5z3ybc87dzm6ilhmkbl0pzz7j0wsbsfhr") (f (quote (("test_fixture"))))))

(define-public crate-git_cmd-0.6.8 (c (n "git_cmd") (v "0.6.8") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "camino") (r "^1.1.7") (d #t) (k 0)) (d (n "fs-err") (r "^2.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1xmgdwhfw6wq7pg58n74s1zsf3zlvdgz12chw9hl624lbh71c1nm") (f (quote (("test_fixture"))))))

