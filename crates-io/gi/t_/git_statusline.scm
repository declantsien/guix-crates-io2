(define-module (crates-io gi t_ git_statusline) #:use-module (crates-io))

(define-public crate-git_statusline-1.0.0 (c (n "git_statusline") (v "1.0.0") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "git2") (r "^0.7.1") (d #t) (k 0)))) (h "1wm3p2ix68v65fcspn560kbv3jflq9jf6pg1vj73asv51g070ssp")))

(define-public crate-git_statusline-1.1.0 (c (n "git_statusline") (v "1.1.0") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "git2") (r "^0.7.1") (d #t) (k 0)))) (h "1069gpsr8560xn69ybgyribrf693vb4a2pjfwidnzxs2bck1y293")))

(define-public crate-git_statusline-1.2.0 (c (n "git_statusline") (v "1.2.0") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "git2") (r "^0.7.1") (d #t) (k 0)))) (h "0anxbd26ajh6sn93chwf4fyyijfm80fmg0l901kfi9lils6d4w8m")))

(define-public crate-git_statusline-1.3.0 (c (n "git_statusline") (v "1.3.0") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "git2") (r "^0.7.1") (d #t) (k 0)))) (h "1lzw2wgnw5qjvj70zz52wagawsasgkmy513n8vp5nw5gk036mdh9")))

(define-public crate-git_statusline-1.3.1 (c (n "git_statusline") (v "1.3.1") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "git2") (r "^0.7.1") (d #t) (k 0)))) (h "07smdrvci369nrwzy31gx0fb9zljiaxinlwgy57c2s9zdg3v7vn4")))

(define-public crate-git_statusline-2.0.0 (c (n "git_statusline") (v "2.0.0") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)))) (h "1qiiafiixv449inz9kix0n330kr9qn9lr5fgp6pp1qbynjg53jvk")))

(define-public crate-git_statusline-2.0.1 (c (n "git_statusline") (v "2.0.1") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)))) (h "0pxl52waryr0j1g80migb2v2if92r5a3ax8cadv9hnq81a2ml1x7")))

