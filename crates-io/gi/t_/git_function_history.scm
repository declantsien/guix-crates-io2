(define-module (crates-io gi t_ git_function_history) #:use-module (crates-io))

(define-public crate-git_function_history-0.1.0 (c (n "git_function_history") (v "0.1.0") (h "0igr4dj21q48qq9kqkq7cl45gfanhfvw475818kpyf6ash8x528c")))

(define-public crate-git_function_history-0.1.1 (c (n "git_function_history") (v "0.1.1") (h "1p9q55mgxm90smhppi56g52clls1a4kvhhirc33ii2vad8d1hinv")))

(define-public crate-git_function_history-0.1.2 (c (n "git_function_history") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nnyjlsn4hhn9b6kgab78543jkjmfyxcvbw35ajdg4llvh7gcp7v")))

(define-public crate-git_function_history-0.1.3 (c (n "git_function_history") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qd9b4h6jfnf158aj1acypfljj4kgv4wzhr43s4vam47grv5lf9a")))

(define-public crate-git_function_history-0.2.0 (c (n "git_function_history") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02jmhyjq439xf2v73hcb0gdp1kq478jc7bijq443ryvzlsvl4xk6")))

(define-public crate-git_function_history-0.2.1 (c (n "git_function_history") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13dlqvxzixxcpy1m4blq89xxh7wnkwjpndvbcs159ixii8c99jvl")))

(define-public crate-git_function_history-0.2.2 (c (n "git_function_history") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g96nwvx6h72659fagzmbsgq1l9z4vc1cw9zn7lkfk0dxw14sxfs")))

(define-public crate-git_function_history-0.2.3 (c (n "git_function_history") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j0kk5kag7zrmfg284i87plcn08sqb6kx5vnlzqwfbsblk1aafaw")))

(define-public crate-git_function_history-0.3.0 (c (n "git_function_history") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02hmw6ji70qvnjsizvxjzjlbxj20ll8v6gzcvlfsrnwq49ybb4y2")))

(define-public crate-git_function_history-0.4.0 (c (n "git_function_history") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08msdc4iw8803wrnaq6ml54h6qkc0rij99cmkjmixs8zjvbhwzm6")))

(define-public crate-git_function_history-0.5.0 (c (n "git_function_history") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "12kd0q1xwc5vs19phyxzg7v7lbi3kxpnil2hdk7lx5hynd4w2ci4")))

(define-public crate-git_function_history-0.5.1 (c (n "git_function_history") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0ncg2j184ig847a77108q9hmk4z3cx04rn7wcbrayfdwlvssqksa")))

(define-public crate-git_function_history-0.5.2 (c (n "git_function_history") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0s5xa8sw8lfvhbbwz8mq9bnhsz74wdmjwxpnc62xdq2d4lkqc486")))

(define-public crate-git_function_history-0.5.3 (c (n "git_function_history") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0bcdl9nqqgzkk095kmsx50g4lxarwkvy1br8jsfif6r5vckd96qd")))

(define-public crate-git_function_history-0.5.4 (c (n "git_function_history") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1sviki3bzpmllf5yfkz7l6ziacjj5nylavhnxw271kqgrsi14myn")))

(define-public crate-git_function_history-0.6.0 (c (n "git_function_history") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.129") (d #t) (k 0)))) (h "06awwl14sch0g73vkciz8gl28b1bm4p60rqxy0gr83rzh711gagc")))

(define-public crate-git_function_history-0.6.1 (c (n "git_function_history") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.129") (d #t) (k 0)))) (h "1wzq3fx0fy1n5l22q4zfgxdrva6a41vylsszpq7ahj9fiy3b5sbk")))

(define-public crate-git_function_history-0.6.2 (c (n "git_function_history") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.131") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)))) (h "0sig4qkgl4d6ajlwpc1bcky6h0bq96nv6dcdgi7qnlqihxzrl25i") (f (quote (("default" "parallel")))) (s 2) (e (quote (("parallel" "dep:rayon"))))))

(define-public crate-git_function_history-0.7.0 (c (n "git_function_history") (v "0.7.0") (d (list (d (n "cached") (r "^0.42.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "git-features") (r "^0.26.1") (f (quote ("zlib" "once_cell"))) (d #t) (k 0)) (d (n "git-repository") (r "^0.33.0") (f (quote ("max-performance-safe"))) (k 0)) (d (n "gitoxide-core") (r "^0.22.0") (d #t) (k 0)) (d (n "gosyn") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "lib-ruby-parser") (r "^4.0.4") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.148") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.2.0") (f (quote ("lalrpop"))) (d #t) (k 0)) (d (n "umpl") (r "^1.1.0") (d #t) (k 0)))) (h "13q6wbmdvx5vl2j32maki1hhyhldwjk6izqrj6ddcsqy1902i6nl") (f (quote (("default" "parallel" "cache")))) (s 2) (e (quote (("unstable" "dep:gosyn") ("parallel" "dep:rayon" "git-features/parallel") ("cache" "dep:cached"))))))

(define-public crate-git_function_history-0.7.1 (c (n "git_function_history") (v "0.7.1") (d (list (d (n "cached") (r "^0.46.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "git_function_history-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "gix") (r "^0.55.2") (f (quote ("max-performance-safe" "revision"))) (k 0)) (d (n "gix-features") (r "^0.36.0") (f (quote ("zlib" "once_cell" "walkdir"))) (d #t) (k 0)) (d (n "gosyn") (r "^0.2.5") (d #t) (k 0)) (d (n "lib-ruby-parser") (r "^4.0.5") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.180") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.3.0") (f (quote ("lalrpop"))) (d #t) (k 0)) (d (n "umpl") (r "^1.1.1") (d #t) (k 0)))) (h "0a34zd1sy9a1dlspvv05hk4f4rjdqbj0mvpqmvmr5v7b27n55rn5") (f (quote (("default" "parallel" "cache")))) (s 2) (e (quote (("parallel" "dep:rayon" "gix-features/parallel" "gix-features/fs-walkdir-parallel") ("cache" "dep:cached")))) (r "1.70.0")))

