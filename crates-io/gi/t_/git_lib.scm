(define-module (crates-io gi t_ git_lib) #:use-module (crates-io))

(define-public crate-git_lib-0.1.0 (c (n "git_lib") (v "0.1.0") (d (list (d (n "git-url-parse") (r "^0.4.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "14rrqbncibqjrmq1i88da3p7vm8hnpibk5ccdmhz8h3zfr2jjbcv")))

(define-public crate-git_lib-0.1.1 (c (n "git_lib") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 2)) (d (n "git-url-parse") (r "^0.4.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "similar-asserts") (r "^1.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1cv1pj2k49s3h0ph5q39p5nhl40xnrp2a2w5wp426hsw7jzya2k0")))

