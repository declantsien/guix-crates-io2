(define-module (crates-io gi t_ git_recent) #:use-module (crates-io))

(define-public crate-git_recent-0.1.0 (c (n "git_recent") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "1ps6lyldx5fyxjvh3kc2vm9vv3c41wnf42p8rrh9aprxnk3s66l4") (y #t)))

(define-public crate-git_recent-0.2.0 (c (n "git_recent") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "14c2x2h0h82q2fmyjks2dkhsdap3xi1g1n15l51nwl0cv117xqsw")))

