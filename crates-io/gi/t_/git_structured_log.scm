(define-module (crates-io gi t_ git_structured_log) #:use-module (crates-io))

(define-public crate-git_structured_log-0.1.0 (c (n "git_structured_log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l6ajp1xnl91j77xx8x1fmgkw83mssj0na888p14mfgyc19k62sn")))

