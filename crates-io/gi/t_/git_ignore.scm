(define-module (crates-io gi t_ git_ignore) #:use-module (crates-io))

(define-public crate-git_ignore-0.1.0 (c (n "git_ignore") (v "0.1.0") (d (list (d (n "glob") (r "^0.2.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 0)))) (h "0ck64lih7is3z6yb09pajrgf3xir3y9xsg8x1vywy72ghwcw6fzv") (y #t)))

(define-public crate-git_ignore-0.1.1 (c (n "git_ignore") (v "0.1.1") (d (list (d (n "glob") (r "^0.2.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 0)))) (h "057n2afncl5dxvbgyqpk3q6d9rzbwrn3h266wmq36rng57nnk7f3") (y #t)))

(define-public crate-git_ignore-0.1.2 (c (n "git_ignore") (v "0.1.2") (h "1k3acmmshdlzh9hfc6h9vnl77gnci1qxkmaaivwwy91zf5b7xl03") (y #t)))

