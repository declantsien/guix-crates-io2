(define-module (crates-io gi t_ git_track) #:use-module (crates-io))

(define-public crate-git_track-0.1.0 (c (n "git_track") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)))) (h "1ws8zp4i5b37mx5jb4v3yi63kmswk7hgrf2iw99kxn0i6n3lq5x2")))

(define-public crate-git_track-0.2.0 (c (n "git_track") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "git2") (r "^0.6.11") (d #t) (k 0)))) (h "0ywvhfrkjiixs1jgqzpfcw1xykqbgl5g724mr7ax56rrmxb4y398")))

