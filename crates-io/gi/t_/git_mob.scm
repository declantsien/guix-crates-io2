(define-module (crates-io gi t_ git_mob) #:use-module (crates-io))

(define-public crate-git_mob-0.1.0 (c (n "git_mob") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "00gy2ra28sj3fmjw5g1w9xykrbglrc7sclryzgc2vjj8qarv1cab")))

(define-public crate-git_mob-0.2.0 (c (n "git_mob") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1i3q5m451brfc7vby6413dcw4fgkbx6ypw60gd5rs4khhnacjzjg")))

(define-public crate-git_mob-0.3.0 (c (n "git_mob") (v "0.3.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0ahxsr6zkw5wmsj7fqqrnp7likkgfiq87gih7ql1q5cgqfh831xz")))

(define-public crate-git_mob-0.3.1 (c (n "git_mob") (v "0.3.1") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0caqd7ylzyczmhk9b18waa96f949zxrh5rjc0yr8ncik0skp5r7i")))

(define-public crate-git_mob-0.3.2 (c (n "git_mob") (v "0.3.2") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0b672ydkl1qmcncfxbxdcjwvx9vm708kaiccg7rqmig5xp0zs6w4")))

(define-public crate-git_mob-0.3.3 (c (n "git_mob") (v "0.3.3") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1s5dsncq1ydivaik4mz2lf8j50nhhgmm7v4mwppjhylz26phgi9r")))

(define-public crate-git_mob-0.3.4 (c (n "git_mob") (v "0.3.4") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "131279lnbwvprpda3n56ylrga9zsrhx9m2b3bhi4zkzy3b37sv72")))

(define-public crate-git_mob-0.3.5 (c (n "git_mob") (v "0.3.5") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0qjk21yyqlnm0likzv2r3yhmv9mbi8wqpkwglhr3krgqdhjyl9ds")))

(define-public crate-git_mob-0.4.0 (c (n "git_mob") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qb5xhkjkpwc6ppcl2cr88h8crgmixilwm3vp7iiz3lj62fhky71")))

(define-public crate-git_mob-0.4.1 (c (n "git_mob") (v "0.4.1") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zmjvx6l57akmfx189gy0drrhcxpsj7p5m3hmbd4zpqyk9biwmcv")))

