(define-module (crates-io gi t_ git_rs) #:use-module (crates-io))

(define-public crate-git_rs-0.1.0 (c (n "git_rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.6") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lru") (r "^0.1.11") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1l1prasvhg05kmaa476gibhsz1j2m2vaq5wwp3b5anm3g355frm9")))

