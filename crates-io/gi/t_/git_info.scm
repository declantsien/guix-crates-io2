(define-module (crates-io gi t_ git_info) #:use-module (crates-io))

(define-public crate-git_info-0.1.0 (c (n "git_info") (v "0.1.0") (h "02933zldc9s64fv5svbzdr8fhw2v9hmdm3by4kn8gq5zmcpxqqj8")))

(define-public crate-git_info-0.1.1 (c (n "git_info") (v "0.1.1") (h "0vxhvy9rgg4aa9wjv8xjh4nv6fqwvn8niw2z2si0b2ccq31sklxd")))

(define-public crate-git_info-0.1.2 (c (n "git_info") (v "0.1.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "17bpdghz2arkv4ddvjq0ni3vy3wwgb0yz3j3jp2v5x3m0dzq86v4")))

