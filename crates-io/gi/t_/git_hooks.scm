(define-module (crates-io gi t_ git_hooks) #:use-module (crates-io))

(define-public crate-git_hooks-0.1.0 (c (n "git_hooks") (v "0.1.0") (h "1m0129hkzfa621gzg4v9nm9xk568699d5fblsblhg3x4x20fvqmw")))

(define-public crate-git_hooks-0.1.1 (c (n "git_hooks") (v "0.1.1") (h "1h77kxhs6qmyi5mxwbf8nnq1m28a0fnrp7qbl7hngh0p6dzjicnj")))

(define-public crate-git_hooks-0.1.2 (c (n "git_hooks") (v "0.1.2") (h "03n3ywrf13kb8mjxpsx6zrc257zbwnxb7qy1r058n084v31ywnzg")))

(define-public crate-git_hooks-0.1.3 (c (n "git_hooks") (v "0.1.3") (h "0mbrypad5kiiavh3r8wrv77qgdky8bgl0rq3xxy1mv51vda5p5mp")))

