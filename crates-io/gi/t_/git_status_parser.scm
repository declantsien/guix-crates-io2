(define-module (crates-io gi t_ git_status_parser) #:use-module (crates-io))

(define-public crate-git_status_parser-0.1.0 (c (n "git_status_parser") (v "0.1.0") (h "0ap8j67bhprh0qrp9ax2gjga36p593cnxmmwx5lrfv4ci6p9n61v")))

(define-public crate-git_status_parser-0.1.1 (c (n "git_status_parser") (v "0.1.1") (h "01gaaxpjykhyvsl7f301s9gvv13dmwdz4bhi9z8gyj089gpxnr2g")))

(define-public crate-git_status_parser-0.1.2 (c (n "git_status_parser") (v "0.1.2") (h "05kmbprggg8mhs7ai1980gnfp32f7ixxyrrzkh3m3a85694d2f47")))

(define-public crate-git_status_parser-0.1.3 (c (n "git_status_parser") (v "0.1.3") (h "187b4avgj92yy0jl3rhz68467sayqnfw7lhsgmh05grhdjsfcphm")))

(define-public crate-git_status_parser-0.1.4 (c (n "git_status_parser") (v "0.1.4") (h "1py8pisilanly7b48wjkh7m6bsvjzh66xq7lssw5ahgx6fc84s06")))

