(define-module (crates-io gi t_ git_describe_build_tool) #:use-module (crates-io))

(define-public crate-git_describe_build_tool-1.0.0 (c (n "git_describe_build_tool") (v "1.0.0") (h "12js1zdglbq1jv97fr6n3cnmaifmbrxxqw0gxqmsiayixzi54nzs")))

(define-public crate-git_describe_build_tool-1.0.2 (c (n "git_describe_build_tool") (v "1.0.2") (h "16lgp3jfk8ck3mxik4lixm2shnr36svnc9pqp82qf40lyw6qdb4p")))

