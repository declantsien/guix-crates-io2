(define-module (crates-io gi t_ git_statistic) #:use-module (crates-io))

(define-public crate-git_statistic-0.1.0 (c (n "git_statistic") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z8rpdklwnkqa5mgiwjwsvv15ph35raffgvvzp53ld3ngnqw458q")))

