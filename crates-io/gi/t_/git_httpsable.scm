(define-module (crates-io gi t_ git_httpsable) #:use-module (crates-io))

(define-public crate-git_httpsable-0.0.1 (c (n "git_httpsable") (v "0.0.1") (h "0r5g4gmnsrvgg6nbj1mqj5czvfylm6y2rgxbaxyabn1n6l8pawnd")))

(define-public crate-git_httpsable-0.1.0 (c (n "git_httpsable") (v "0.1.0") (d (list (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "0imy8hr7l0m7xl002d2jix8nsrqakkk7skny71360553v01kqvv3")))

(define-public crate-git_httpsable-1.0.0 (c (n "git_httpsable") (v "1.0.0") (d (list (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "084g9p7akvsi7dvw6sapb8lbbqcq2dnd2q33m66zjv7mr43k267b")))

(define-public crate-git_httpsable-1.0.1 (c (n "git_httpsable") (v "1.0.1") (d (list (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "0yy7pccvs7krvbcm7fn4m2s0gcgbj36yhmjzg59kd9cnjzyd89f0")))

