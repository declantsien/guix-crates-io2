(define-module (crates-io gi t_ git_auto_sync) #:use-module (crates-io))

(define-public crate-git_auto_sync-0.1.0 (c (n "git_auto_sync") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "watchexec") (r "^1.10.0") (d #t) (k 0)))) (h "0mxwbb0dw9nnrrspdnqg6gk8l1mg6dr8qimvz0dc0rh6rp7f07mp")))

(define-public crate-git_auto_sync-0.1.1 (c (n "git_auto_sync") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "watchexec") (r "^1.10.0") (d #t) (k 0)))) (h "15bjrmnqzdjkv9g2inq4vw41rdvfkazfjbr0718hgqcbw1b1mjva")))

(define-public crate-git_auto_sync-0.1.2 (c (n "git_auto_sync") (v "0.1.2") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "watchexec") (r "^1.10.0") (d #t) (k 0)))) (h "0lj5k72549sfz2maflp31qgkwf4b9c2qmy47bfkma571i7z96h8y")))

(define-public crate-git_auto_sync-0.1.3 (c (n "git_auto_sync") (v "0.1.3") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "watchexec") (r "^1.10.0") (d #t) (k 0)))) (h "1sl2fl511zfv2km6df4pp240lfh9xvgkwjkhcw3qcldvj3yg8c9y")))

