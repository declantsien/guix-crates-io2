(define-module (crates-io gi fr gifriend) #:use-module (crates-io))

(define-public crate-gifriend-0.1.0 (c (n "gifriend") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "lzw") (r "^0.10.0") (d #t) (k 0)))) (h "0hwkjb42f7g8mim5mff1117zshf5afbac3flw0a6vjx3shqxcdcc")))

(define-public crate-gifriend-0.1.1 (c (n "gifriend") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "lzw") (r "^0.10.0") (d #t) (k 0)))) (h "1r40mn1h934dwlr1dn7npvmyb51cakivc6wx1x0cyygaqsncyw6r")))

