(define-module (crates-io gi tm gitmap) #:use-module (crates-io))

(define-public crate-gitmap-0.1.0 (c (n "gitmap") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "016i92v0ccsg4i3ziyn2myikfjcfp8l1gz151vqcg0q71pck1ll7")))

