(define-module (crates-io gi tm gitmoji-changelog-rust) #:use-module (crates-io))

(define-public crate-gitmoji-changelog-rust-0.1.0 (c (n "gitmoji-changelog-rust") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "handlebars") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0dwy06wmnxapgq6wfafscp9yx2yfdszml77ir75hsq166xp8l40v")))

