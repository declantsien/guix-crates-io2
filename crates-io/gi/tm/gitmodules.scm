(define-module (crates-io gi tm gitmodules) #:use-module (crates-io))

(define-public crate-gitmodules-0.1.0 (c (n "gitmodules") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01lapcxr5ydrxpmm1izsyvryi63x4rpjksbrwyjq1rizi5gm349d")))

