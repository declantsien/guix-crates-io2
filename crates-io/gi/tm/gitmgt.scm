(define-module (crates-io gi tm gitmgt) #:use-module (crates-io))

(define-public crate-gitmgt-0.1.0 (c (n "gitmgt") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "085zhkha10fg666mabhf4h6rz02h56ggcr6lyk6fd19y3xvm385s") (y #t)))

(define-public crate-gitmgt-0.1.1 (c (n "gitmgt") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1fpkhd6l50ry7afhwhiws8am293zpdj6ypz7c959wmisp8xij4vh")))

(define-public crate-gitmgt-0.1.2 (c (n "gitmgt") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1jqmf26pflqr61cw9j7a8gkvhd4wnccvmkkh0mq2fqa89ml147an")))

