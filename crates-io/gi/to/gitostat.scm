(define-module (crates-io gi to gitostat) #:use-module (crates-io))

(define-public crate-gitostat-0.3.1 (c (n "gitostat") (v "0.3.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jmq6635npkal4imb5l0w0n1n1slygzk0jhgq8y61wjv4dmcaaxk")))

