(define-module (crates-io gi to giton-cli) #:use-module (crates-io))

(define-public crate-giton-cli-0.0.1-beta.1 (c (n "giton-cli") (v "0.0.1-beta.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.7") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 0)) (d (n "core") (r "^0.0.1-beta.1") (d #t) (k 0) (p "giton-core")) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "utils") (r "^0.0.1-beta.1") (d #t) (k 0) (p "giton-utils")))) (h "1fki4c11vfpq5bk2kk4ahy1zf2nkbqpafv1343bpzjbahrhqnlln")))

(define-public crate-giton-cli-0.0.1 (c (n "giton-cli") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.7") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 0)) (d (n "core") (r "^0.0.1") (d #t) (k 0) (p "giton-core")) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "utils") (r "^0.0.1") (d #t) (k 0) (p "giton-utils")))) (h "0fqi3rn2qsimgp9kd249h2f2ibgwsafmarrh1bjii90j4mcw0axy")))

(define-public crate-giton-cli-0.1.0 (c (n "giton-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.7") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 0)) (d (n "core") (r "^0.1.0") (d #t) (k 0) (p "giton-core")) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "utils") (r "^0.1.0") (d #t) (k 0) (p "giton-utils")))) (h "0scyhc91c8pcc81aa4p1cvqksv987n11ldn96daxjgnn5pshyshk")))

