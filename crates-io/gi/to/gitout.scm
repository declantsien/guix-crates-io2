(define-module (crates-io gi to gitout) #:use-module (crates-io))

(define-public crate-gitout-0.0.1 (c (n "gitout") (v "0.0.1") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "graphql_client") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1fzxwj0r10s1fkma4bq3k5wp0rj0qlb60m5rij2m9jy4ii9wb8qm") (y #t)))

(define-public crate-gitout-0.1.0 (c (n "gitout") (v "0.1.0") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "graphql_client") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1qm9b1ckxqwimyvims77c3bcyzlqnsklxzfnx5niiwf3cn0b0zkx")))

(define-public crate-gitout-0.2.0 (c (n "gitout") (v "0.2.0") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "graphql_client") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gnxmgjymkyzm6ilwmsnzrs78gp53n54mlslag8wppy9nkwkc25x")))

