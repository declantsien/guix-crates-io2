(define-module (crates-io gi to gitopen) #:use-module (crates-io))

(define-public crate-gitopen-1.1.1 (c (n "gitopen") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0lss53hl8ixgv2z3aqrhbbl9adagq0q1gcz4d38r5gw7djrjnhwp")))

(define-public crate-gitopen-1.2.1 (c (n "gitopen") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0h4ivnny5kpa3nmwrkp8812ca3jmbp3gqgpkh9lzjq18a4hy6di1")))

(define-public crate-gitopen-1.3.0 (c (n "gitopen") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0a282zswssy7wqijrf03mhf4y2h4vlyfjf0fg7yq5k2x397hlg6g")))

(define-public crate-gitopen-1.3.1 (c (n "gitopen") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1g0164nspih4fgszlj0lr1nwgzxm7kxhjvyz6ilb8apimwcgwybv")))

(define-public crate-gitopen-1.3.2 (c (n "gitopen") (v "1.3.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0n4fzdsa70m217bm39030wkbkph2xm3yid1xmfdiks668z42iy0d")))

(define-public crate-gitopen-1.3.3 (c (n "gitopen") (v "1.3.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.7") (d #t) (k 0)))) (h "1sabf8a1jhj1dz5ndgmjf1g43cprk2m63mrbld1xf2i3vvqdjdv5")))

(define-public crate-gitopen-1.4.0 (c (n "gitopen") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.7") (d #t) (k 0)))) (h "1qifslhzk5s92f5xfgvxmjbklyxahwqn9plp5frkjfvf4fhlgr89")))

(define-public crate-gitopen-1.4.1 (c (n "gitopen") (v "1.4.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.13") (d #t) (k 0)))) (h "1pn2j6knyxgci5vx6zjng2mzpk1fhllraxv0jbpzfb4vc16hkp57")))

