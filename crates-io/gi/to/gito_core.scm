(define-module (crates-io gi to gito_core) #:use-module (crates-io))

(define-public crate-gito_core-1.0.0 (c (n "gito_core") (v "1.0.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0r5zqbg5fs476scflzfan8m4msk7p5jcm9p64x2qhn6dg4s8622m") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-gito_core-1.0.1 (c (n "gito_core") (v "1.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0y118lgigmgf3ni9gd9z4hg185qykzbmghbmahlbzhczb47c80rc") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-gito_core-1.0.2 (c (n "gito_core") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07yp7n62aigwm088fr9zxd22qvnpsqp7xaw6jggw072mxxhkbpry")))

(define-public crate-gito_core-1.0.3 (c (n "gito_core") (v "1.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "022j74lbf858dp0d3kikl59c9hy7js1lwfqxmxnnngi6k5hqmv7v")))

(define-public crate-gito_core-1.0.4 (c (n "gito_core") (v "1.0.4") (d (list (d (n "napi") (r "^2.12.0") (f (quote ("napi4"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.12.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13j3k8vcf3gcyzkzmxfqfcn4kf6iz5gxrqqpikhn2r6a7qzg59b3") (f (quote (("independent" "napi-derive/noop"))))))

