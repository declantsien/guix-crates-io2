(define-module (crates-io gi rl girlfriend) #:use-module (crates-io))

(define-public crate-girlfriend-0.1.0 (c (n "girlfriend") (v "0.1.0") (h "0f3gl48hr2vr8fjxsg04kgwxnknmmh5i4mmvc3zd2hx79pmcg73d")))

(define-public crate-girlfriend-0.2.0 (c (n "girlfriend") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "deno_core") (r "^0.145.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ysx4sa7xsf6vjwag1ssb4pwwcif1lmsp9wlhl3xx1ijk5b75i42")))

(define-public crate-girlfriend-0.3.1 (c (n "girlfriend") (v "0.3.1") (d (list (d (n "deno_ast") (r "^0.22.0") (f (quote ("transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.174.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lck5dckq4gdcp0zls1aixs6c4b6i5l8s76hbqmymwiyxnndb0w2")))

