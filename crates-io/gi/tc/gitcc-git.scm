(define-module (crates-io gi tc gitcc-git) #:use-module (crates-io))

(define-public crate-gitcc-git-0.1.0 (c (n "gitcc-git") (v "0.1.0") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "0kp4pz0z4ql8kmpmkimsgyagf1bx868v5b6gpmhl8h2rxcwwqiij")))

(define-public crate-gitcc-git-0.2.0 (c (n "gitcc-git") (v "0.2.0") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "175g19h770vjgvfx3f0san5gk8zk9xpm07xbfdnxcp1rcf144wbd")))

(define-public crate-gitcc-git-0.3.0 (c (n "gitcc-git") (v "0.3.0") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1m2ffqbhig7zr84qnr3b74za1qi0chpb3xidymk7l6f1x0mihhms")))

(define-public crate-gitcc-git-0.4.0 (c (n "gitcc-git") (v "0.4.0") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1cpxsfvd516jcq7g91z7j0v9m4fdqjxyj1507is8y3634h0zv825")))

(define-public crate-gitcc-git-0.5.0 (c (n "gitcc-git") (v "0.5.0") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1qkhmbm2mldzj71j3hw8lxvwnqifi18n8zw7f0w8hyw4528p2q6f")))

