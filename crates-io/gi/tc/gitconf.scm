(define-module (crates-io gi tc gitconf) #:use-module (crates-io))

(define-public crate-gitconf-0.1.0 (c (n "gitconf") (v "0.1.0") (d (list (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "0jg4v6zksvp39aka1921v8w5rlcnszp2y4wlhpajyh9k8w4137im")))

