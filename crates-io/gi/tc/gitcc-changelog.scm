(define-module (crates-io gi tc gitcc-changelog) #:use-module (crates-io))

(define-public crate-gitcc-changelog-0.1.0 (c (n "gitcc-changelog") (v "0.1.0") (d (list (d (n "handlebars") (r "^4.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "041np96iqiz10gjql1ddn60q63x6g5ff046njqmwj0kmx8i5ifjf")))

(define-public crate-gitcc-changelog-0.2.0 (c (n "gitcc-changelog") (v "0.2.0") (d (list (d (n "handlebars") (r "^4.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "1r09kj4x2m5bxwcbpbv9skflqfgdhby5r2ghj4azwparml04jxs5")))

(define-public crate-gitcc-changelog-0.3.0 (c (n "gitcc-changelog") (v "0.3.0") (d (list (d (n "handlebars") (r "^4.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "0qwx1vpjs0bd16vzb36nv0qg35gk79nz170y6gr6raifznac9g4i")))

(define-public crate-gitcc-changelog-0.4.0 (c (n "gitcc-changelog") (v "0.4.0") (d (list (d (n "handlebars") (r "^4.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "07cabs2ds1waqwvyl92n5df7gam6g0gcd53vclwyfl979dld33vl")))

(define-public crate-gitcc-changelog-0.5.0 (c (n "gitcc-changelog") (v "0.5.0") (d (list (d (n "handlebars") (r "^4.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "1c1gl4dfs7q8pbbm8kinpyjigz17yvqvpnlmxbapl5js52fsl4mj")))

