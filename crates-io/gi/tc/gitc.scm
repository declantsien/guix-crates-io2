(define-module (crates-io gi tc gitc) #:use-module (crates-io))

(define-public crate-gitc-0.1.0 (c (n "gitc") (v "0.1.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0y8v07yqdv0dgk71129mc5gi8f1982ca3ar0apzgl8scswdzmk2x")))

(define-public crate-gitc-0.1.1 (c (n "gitc") (v "0.1.1") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0qnpwwk1wl0ra6cd13p3xz5r8rc2zp9vc7ir6hli8qzs5mqvmwc9")))

(define-public crate-gitc-0.1.2 (c (n "gitc") (v "0.1.2") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "10c75baqf286mnqdzhig7g7ls7jx2yzkar0qqqlsmf3dq3jwynr7")))

(define-public crate-gitc-0.1.3 (c (n "gitc") (v "0.1.3") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0dc7p5ma1zab3k9i9h0y9qkb69hclj0rfwrf03xm1i0wrmnz59v7")))

(define-public crate-gitc-0.1.4 (c (n "gitc") (v "0.1.4") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "080im9vpxxahp6cc543xabw2kgf7hvyakg099n9a76lv9kcg57n8")))

