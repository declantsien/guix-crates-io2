(define-module (crates-io gi tc gitconfig2json_cli) #:use-module (crates-io))

(define-public crate-gitconfig2json_cli-0.1.0 (c (n "gitconfig2json_cli") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "0gbxq7z0qm91ql59daasl6qmlnl8afb0gg6c92cij86dg1p372p7")))

(define-public crate-gitconfig2json_cli-0.1.1 (c (n "gitconfig2json_cli") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "0v5ndxssvy0laa1p4iy2x3ldxpqfl7w4gackzyg32n4dqhp6bi1m")))

(define-public crate-gitconfig2json_cli-0.2.0 (c (n "gitconfig2json_cli") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "gitconfig2json") (r "^0.1.1") (d #t) (k 0)))) (h "1v7b89wryp6n7a481axpmhkhy0vlkqgbnppn8irfj6x3931wsx3i")))

(define-public crate-gitconfig2json_cli-0.2.1 (c (n "gitconfig2json_cli") (v "0.2.1") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "gitconfig2json") (r "^0.4.0") (d #t) (k 0)))) (h "1664phiy7wjjzywhfp3g8hpmq9bc6dfy9w3cgswd15jfgnvyrih4")))

