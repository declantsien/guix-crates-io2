(define-module (crates-io gi tc gitconfig) #:use-module (crates-io))

(define-public crate-gitconfig-0.0.1 (c (n "gitconfig") (v "0.0.1") (h "0q9dxwfrdb15355jdm1n79hax9y8nvri6pvvn8z3yhm592p0r5m9")))

(define-public crate-gitconfig-0.1.0 (c (n "gitconfig") (v "0.1.0") (h "1y8fnylqzfm91mvx3snk1mmkfm70m3kbxkbcbdvfrff9cq6agasd")))

