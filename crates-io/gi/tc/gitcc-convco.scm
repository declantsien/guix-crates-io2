(define-module (crates-io gi tc gitcc-convco) #:use-module (crates-io))

(define-public crate-gitcc-convco-0.1.0 (c (n "gitcc-convco") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "08y5yyb62bxqgxqp7xq5ak04i9pmlrx48s6kp6749jz7z6hy8557")))

(define-public crate-gitcc-convco-0.2.0 (c (n "gitcc-convco") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1883pd1p620xbq4x244kwb26awajg0jrd5d8k0wjbbvs2ljlb8v7")))

(define-public crate-gitcc-convco-0.3.0 (c (n "gitcc-convco") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ggjdv6z2ciy3p7j4f814vhfrmdxn4qiz14gakm2jnsarkcg5bl3")))

(define-public crate-gitcc-convco-0.4.0 (c (n "gitcc-convco") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0c7s14f0xy6lkh8byxv96bi9qpyjg1gdr6vs9rasyqxah58apb60")))

(define-public crate-gitcc-convco-0.5.0 (c (n "gitcc-convco") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0p2m40zdg7zcd3kisirwja24wyvv8spzsyx84cxcaal9p4mshg01")))

