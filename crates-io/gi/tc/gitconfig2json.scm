(define-module (crates-io gi tc gitconfig2json) #:use-module (crates-io))

(define-public crate-gitconfig2json-0.1.0 (c (n "gitconfig2json") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "0cm4v76jhk7qnqqq7dpy5nq7s4zqblngwd057v4sq9sl5a85mlhh")))

(define-public crate-gitconfig2json-0.1.1 (c (n "gitconfig2json") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "0ha5zfqv66dp0c87046kcyaqmq6kw4p2hb5hn1xfhdgm6d8n8pg3")))

(define-public crate-gitconfig2json-0.2.0 (c (n "gitconfig2json") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "0jjx6acw7c2km1ybvscrsggkf5nkw8422gq6da072y7q0sjz9ihx")))

(define-public crate-gitconfig2json-0.3.0 (c (n "gitconfig2json") (v "0.3.0") (d (list (d (n "gitconfig") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1mppf1lallxx9hfz7xwi17i3jygdg4cw4b3954g02lh8vih1aiay")))

(define-public crate-gitconfig2json-0.3.1 (c (n "gitconfig2json") (v "0.3.1") (d (list (d (n "gitconfig") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1gss861finy7qj7gyk9m2qvsky900m84f03hbmx7ggamiifa63xy")))

(define-public crate-gitconfig2json-0.4.0 (c (n "gitconfig2json") (v "0.4.0") (d (list (d (n "gitconfig") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "0nms8jfpjz4y0l81ywggmjvzx5hr9s79r6qws7yq2arp0w6ig9g0")))

