(define-module (crates-io gi tc gitcc-cli) #:use-module (crates-io))

(define-public crate-gitcc-cli-0.1.0 (c (n "gitcc-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "gitcc-core") (r "^0.1.0") (d #t) (k 0)))) (h "1fsa6qwj674s4zzafx31i2dzvf2py1f0bk2vsac7r9kv86drdk58")))

(define-public crate-gitcc-cli-0.2.0 (c (n "gitcc-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "gitcc-core") (r "^0.2.0") (d #t) (k 0)))) (h "0x2kwzm2qjm2z99znh6fms2hdjicg0vp9rvign1jwavr3g3n1prl")))

(define-public crate-gitcc-cli-0.3.0 (c (n "gitcc-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "gitcc-core") (r "^0.3.0") (d #t) (k 0)))) (h "0ql8x0r6fv3cjjs4klvj2nfshvwbf8i0px9dgp973krlchg0i4jv")))

(define-public crate-gitcc-cli-0.4.0 (c (n "gitcc-cli") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "gitcc-core") (r "^0.4.0") (d #t) (k 0)))) (h "0skpic9cif13xfanq3lsl9db3nyy77cj4nghrj7d44w65fpdz8sz")))

(define-public crate-gitcc-cli-0.5.0 (c (n "gitcc-cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "gitcc-core") (r "^0.5.0") (d #t) (k 0)))) (h "0ds5f4n3w47z8fjq6ir01k9vp1i4mbnbgl5di9ilizssjn3j13dg")))

