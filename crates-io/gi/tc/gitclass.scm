(define-module (crates-io gi tc gitclass) #:use-module (crates-io))

(define-public crate-gitclass-0.5.0 (c (n "gitclass") (v "0.5.0") (d (list (d (n "clap") (r "^2.13.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.10.2") (d #t) (k 0)))) (h "0j2n7can3h8mh34f2f4xvb59vgy1advy7cymd7nqjjd35m579nlf")))

(define-public crate-gitclass-0.6.0 (c (n "gitclass") (v "0.6.0") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "clap") (r "^2.13.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.10.2") (d #t) (k 0)))) (h "1514n6527wgal622ilp9vi8m75q1gdpmjy04g8p2k4l6r349zdrj")))

(define-public crate-gitclass-0.7.0 (c (n "gitclass") (v "0.7.0") (d (list (d (n "clap") (r "^2.21.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "json") (r "^0.11.6") (d #t) (k 0)))) (h "0q1a8m0y9b2mfd2kvd561bcvhwnp9nvfsgi198vxnpg28pacyhvf")))

(define-public crate-gitclass-0.7.1 (c (n "gitclass") (v "0.7.1") (d (list (d (n "clap") (r "^2.29.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "json") (r "^0.11.6") (d #t) (k 0)))) (h "13rrlj55h81r3p0c6yhn5yxl4lskjn45vlwdss16xg2yf4zs088m")))

(define-public crate-gitclass-0.7.2 (c (n "gitclass") (v "0.7.2") (d (list (d (n "clap") (r "^2.29.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "json") (r "^0.11.6") (d #t) (k 0)))) (h "1719zzw835brkhkw3h8j2snrwpqcgggfdm6scfsqr6aplqznmwck")))

(define-public crate-gitclass-0.7.3 (c (n "gitclass") (v "0.7.3") (d (list (d (n "clap") (r "^2.29.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "json") (r "^0.11.6") (d #t) (k 0)))) (h "131spr6pddyp21dg4vnk0wygnqnvcjcqj3kly52y6qxs7zhs5v60")))

(define-public crate-gitclass-0.8.0 (c (n "gitclass") (v "0.8.0") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "0jxpvxh7nvbzklzgm3n6k5zz7skbx4fyjfv33pvvy31ixrli4526")))

