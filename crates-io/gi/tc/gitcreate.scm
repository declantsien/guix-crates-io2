(define-module (crates-io gi tc gitcreate) #:use-module (crates-io))

(define-public crate-gitcreate-0.0.0 (c (n "gitcreate") (v "0.0.0") (h "12lcxms756yn05dssc4ls2l5118sz49vslf2mg3v0lkybn1yr8cx") (y #t)))

(define-public crate-gitcreate-0.0.1 (c (n "gitcreate") (v "0.0.1") (h "0r0r26dgqpx8r4fs5nk69nsknw0x8ls6pynbp89i5kjcpx3xn133") (y #t)))

