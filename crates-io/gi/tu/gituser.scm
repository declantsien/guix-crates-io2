(define-module (crates-io gi tu gituser) #:use-module (crates-io))

(define-public crate-gituser-0.1.0 (c (n "gituser") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "config") (r "^0.13.1") (f (quote ("toml"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "14bb76l0agn53migf8izd0266z81n94wqbmyja32ahlhg3wbbbbs")))

