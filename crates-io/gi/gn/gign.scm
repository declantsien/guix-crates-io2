(define-module (crates-io gi gn gign) #:use-module (crates-io))

(define-public crate-gign-0.1.0 (c (n "gign") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0gcwb4c12zvsdbkvw6lx2jhgrfpq4j3px3bi7358viyq62jz1i1d")))

(define-public crate-gign-0.1.1 (c (n "gign") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1j5csvmwhip993ydss36l3fz5xn6rnj0cyh2il46fkvz1cy3phh0")))

(define-public crate-gign-0.1.2 (c (n "gign") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "06r5hsz5h7skk9b9fcjiprjff625rjrlr888m0npxli9afpg29cj")))

(define-public crate-gign-0.2.0 (c (n "gign") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0svsz7ccagjh3qhpk1mkjiv47abz9lm14q4wg9a7jl5v98va439w")))

(define-public crate-gign-0.3.0 (c (n "gign") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1bvha66k4zdgm91vwxf12n5i9wnmp446vkbmvpay1rpbyl0adz6c")))

