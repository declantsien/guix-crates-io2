(define-module (crates-io gi al giallo) #:use-module (crates-io))

(define-public crate-giallo-0.0.1 (c (n "giallo") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.8") (d #t) (k 0)) (d (n "tree-sitter-highlight") (r "^0.20.1") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "0s9fmvkrgyfby7c30g6dnbhg45dgfahhkrdz55cjcs2hsa9vgc1b") (f (quote (("default" "lang-rust")))) (s 2) (e (quote (("lang-rust" "dep:tree-sitter-rust"))))))

