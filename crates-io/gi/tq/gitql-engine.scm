(define-module (crates-io gi tq gitql-engine) #:use-module (crates-io))

(define-public crate-gitql-engine-0.1.0 (c (n "gitql-engine") (v "0.1.0") (d (list (d (n "git2") (r "^0.17.1") (k 0)) (d (n "gitql-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0n60xlbmn9f3hch3h9sy1c2lf8k4wmazdxw7na6kivdsm1y00cfx")))

(define-public crate-gitql-engine-0.2.0 (c (n "gitql-engine") (v "0.2.0") (d (list (d (n "git2") (r "^0.17.1") (k 0)) (d (n "gitql-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "131rp3xn2crvip1801qs07mmwi2q06wkfbfidwx2pjs605c71hlh")))

(define-public crate-gitql-engine-0.3.0 (c (n "gitql-engine") (v "0.3.0") (d (list (d (n "git2") (r "^0.17.1") (k 0)) (d (n "gitql-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0vj4d8g1pjamd2qkpv95flp93mpb4mms9qmi61wrbjbdm3akmax0")))

(define-public crate-gitql-engine-0.4.0 (c (n "gitql-engine") (v "0.4.0") (d (list (d (n "git2") (r "^0.17.1") (k 0)) (d (n "gitql-ast") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "08b9gabbivs4anv1ck0acnv6pm5ag3dl234h70kim50vynn0nh1z")))

(define-public crate-gitql-engine-0.5.0 (c (n "gitql-engine") (v "0.5.0") (d (list (d (n "git2") (r "^0.18.0") (k 0)) (d (n "gitql-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0hcnrynaz7npkf7pc5bmhdv6yp1zq6xfqkb6qxhk2i12ff6wjlma")))

(define-public crate-gitql-engine-0.6.0 (c (n "gitql-engine") (v "0.6.0") (d (list (d (n "git2") (r "^0.18.0") (k 0)) (d (n "gitql-ast") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0l2zxdjfmg4rcyhx7mvjpx21jglviayz69l583rx7kxyiym97y4v")))

(define-public crate-gitql-engine-0.7.0 (c (n "gitql-engine") (v "0.7.0") (d (list (d (n "git2") (r "^0.18.1") (k 0)) (d (n "gitql-ast") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1cfdc7pmj6vv2ff6nlck8wp0g7sg8qasv7nd0bllfrx2ffb2a36l")))

(define-public crate-gitql-engine-0.8.0 (c (n "gitql-engine") (v "0.8.0") (d (list (d (n "git2") (r "^0.18.1") (k 0)) (d (n "gitql-ast") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1q0wnp9xadys3p32whr307gxydm7v04684jgwwbm87axn11kfsvg")))

(define-public crate-gitql-engine-0.9.0 (c (n "gitql-engine") (v "0.9.0") (d (list (d (n "git2") (r "^0.18.1") (k 0)) (d (n "gitql-ast") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1lhd2ywg9z63vr0203gyh1ab15dq659kglrnr1d4y2bsnffsiyng")))

(define-public crate-gitql-engine-0.10.0 (c (n "gitql-engine") (v "0.10.0") (d (list (d (n "gitql-ast") (r "^0.8.0") (d #t) (k 0)) (d (n "gix") (r "^0.55.2") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "16567faaczlchfwfq392zy23liz8gq29b07zbnb3272sly60jdmm")))

(define-public crate-gitql-engine-0.11.0 (c (n "gitql-engine") (v "0.11.0") (d (list (d (n "gitql-ast") (r "^0.9.0") (d #t) (k 0)) (d (n "gix") (r "^0.57.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0is8h45c0xjnhk5qvdhbjspr395f4lk59142dld02s85ar0nbi7b")))

(define-public crate-gitql-engine-0.12.0 (c (n "gitql-engine") (v "0.12.0") (d (list (d (n "gitql-ast") (r "^0.10.0") (d #t) (k 0)) (d (n "gix") (r "^0.57.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "00zc81x0swflszflfb4j8w7k2g91s8razs917dfbha1hbk8qjl9k")))

(define-public crate-gitql-engine-0.13.0 (c (n "gitql-engine") (v "0.13.0") (d (list (d (n "gitql-ast") (r "^0.11.0") (d #t) (k 0)) (d (n "gix") (r "^0.58.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1i98bliz53nzj551nxf81a0vrvy1h13lg6g7qhclw04xh3ac4x2j")))

(define-public crate-gitql-engine-0.14.0 (c (n "gitql-engine") (v "0.14.0") (d (list (d (n "gitql-ast") (r "^0.12.0") (d #t) (k 0)) (d (n "gix") (r "^0.58.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "06wyd16cn7xnkxzypi5w9dcdnk5lk92bai480x8psdr4r9kl2222")))

(define-public crate-gitql-engine-0.15.0 (c (n "gitql-engine") (v "0.15.0") (d (list (d (n "gitql-ast") (r "^0.13.0") (d #t) (k 0)) (d (n "gix") (r "^0.58.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "159vq3267sg216r15gq1x7q4as9r24gl8601gb6lkbqk38rwwn23")))

(define-public crate-gitql-engine-0.16.0 (c (n "gitql-engine") (v "0.16.0") (d (list (d (n "gitql-ast") (r "^0.14.0") (d #t) (k 0)) (d (n "gix") (r "^0.60.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1nw4ca4vbiwyn76xwxwqxhgjn1nnixmydpa99qz112pvqlnidjli")))

(define-public crate-gitql-engine-0.17.0 (c (n "gitql-engine") (v "0.17.0") (d (list (d (n "gitql-ast") (r "^0.15.0") (d #t) (k 0)) (d (n "gix") (r "^0.61.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "01aplqyfxafabv9xg61l24dbkjz3s819w5izrn68wwi73c7lfcli")))

(define-public crate-gitql-engine-0.18.0 (c (n "gitql-engine") (v "0.18.0") (d (list (d (n "gitql-ast") (r "^0.16.0") (d #t) (k 0)) (d (n "gix") (r "^0.62.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "02qlf4viajp8yyk46ga9fi2p06wcpy0yllcqi0cknvgrk9ll8aiq")))

(define-public crate-gitql-engine-0.19.0 (c (n "gitql-engine") (v "0.19.0") (d (list (d (n "gitql-ast") (r "^0.17.0") (d #t) (k 0)) (d (n "gix") (r "^0.62.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0y25vrcn1i5frzf9vgvw90l2p7bqa2c4s5ivbhyd25rknwq06r56")))

(define-public crate-gitql-engine-0.19.1 (c (n "gitql-engine") (v "0.19.1") (d (list (d (n "gitql-ast") (r "^0.17.1") (d #t) (k 0)) (d (n "gix") (r "^0.62.0") (f (quote ("blob-diff"))) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "09s0kc6z1mpi6228dzinc5vp5782jwqm113dghc12nzj0gsbmxjn")))

