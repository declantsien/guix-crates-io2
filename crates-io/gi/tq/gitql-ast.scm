(define-module (crates-io gi tq gitql-ast) #:use-module (crates-io))

(define-public crate-gitql-ast-0.1.0 (c (n "gitql-ast") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1k83h2lh9m1p2wp18mshpzl6f5xplm27y555j54wlwlcm05mzh79")))

(define-public crate-gitql-ast-0.2.0 (c (n "gitql-ast") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0045w3n2hnj2cc0wjrnkwvfxmkkx1l8imgx3jg2r0gpxm18nqr97")))

(define-public crate-gitql-ast-0.3.0 (c (n "gitql-ast") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0xiamdigpyjy92sb0yfkki3bh99vqr1j5yn89wmi6fkznwqp2rnr")))

(define-public crate-gitql-ast-0.4.0 (c (n "gitql-ast") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08cwj94v2r8f57k4fq8dmgnz03lp97lkgln3d5xaz6hycqddi1d1")))

(define-public crate-gitql-ast-0.5.0 (c (n "gitql-ast") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0a6dnwhs1mq01c0xsm074kg6v0nikkpcpd94qnm5wnia9sr61acx")))

(define-public crate-gitql-ast-0.6.0 (c (n "gitql-ast") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1jbyy81006np506985ms83pyr8npxjy2z6aypn9s8ffl86dj8j4k")))

(define-public crate-gitql-ast-0.7.0 (c (n "gitql-ast") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0p7xh2jvpxfcxxqg9q4c5007r2rczjwi0lxsdgg8qmjxq26rsw3v")))

(define-public crate-gitql-ast-0.8.0 (c (n "gitql-ast") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0385367rinzyd34w7ldqld5spdpgcgfzqrwnsyz4yfzrscl8mycn")))

(define-public crate-gitql-ast-0.9.0 (c (n "gitql-ast") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01sp2zjn3yysbxzprbvnxvqabw6k3am9ihyx4x5qcnpvsmqkfmk0")))

(define-public crate-gitql-ast-0.10.0 (c (n "gitql-ast") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1in9f514mpay6vkg37sarx582bwj34spj46wsvjb1h2ry8914dv1")))

(define-public crate-gitql-ast-0.11.0 (c (n "gitql-ast") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "01pj2yca6frh1fvms2qmmhnqfzh4c0q2rwqc3kgcb4p661dnhfc7")))

(define-public crate-gitql-ast-0.12.0 (c (n "gitql-ast") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "06w22xw4krwna58bj5qpbgbc3pm70h2gmcsxx708lrcmryz8z86n")))

(define-public crate-gitql-ast-0.13.0 (c (n "gitql-ast") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "16gi04s446gfr3m8vvmw2ibmn8ir5byi25iklaj6sw58lrzyk1xk")))

(define-public crate-gitql-ast-0.14.0 (c (n "gitql-ast") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "18zby1q0hsh3r4y0mqjxhg3yzzjfa0shr4cmj5w7j2gg7nqgpfgi")))

(define-public crate-gitql-ast-0.15.0 (c (n "gitql-ast") (v "0.15.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "12aiccqkck7mwjd6xhi4pczq0v43hpvkcijv8r6vp9h6sybbzhz9")))

(define-public crate-gitql-ast-0.16.0 (c (n "gitql-ast") (v "0.16.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "1765qbkr039blbc6vph4f3mdlhw3y9znggp47dq489mv4g2dm573")))

(define-public crate-gitql-ast-0.17.0 (c (n "gitql-ast") (v "0.17.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rfbh88jgky0kxm98jxbs5l1q8hw27kvyc6hrvjv8gqn43lbr9c1")))

(define-public crate-gitql-ast-0.17.1 (c (n "gitql-ast") (v "0.17.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "17r2rg488js24mj391872fj7nk3iw2h3z6d34k2pyjbllym1akkd")))

