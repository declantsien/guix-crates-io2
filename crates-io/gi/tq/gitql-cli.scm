(define-module (crates-io gi tq gitql-cli) #:use-module (crates-io))

(define-public crate-gitql-cli-0.1.0 (c (n "gitql-cli") (v "0.1.0") (d (list (d (n "gitql-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1m6gzvdgrnkf6j8rmignlnxy8zl0630331w1b7rpm8dsys9wkqb2")))

(define-public crate-gitql-cli-0.2.0 (c (n "gitql-cli") (v "0.2.0") (d (list (d (n "gitql-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "0vj78nf78hn7f4f0gp5hpp0ybq5lg2nzm7mjjab6v59x1q2xsikv")))

(define-public crate-gitql-cli-0.3.0 (c (n "gitql-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gitql-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "0i47mkq7gw7sz17ws7m360hzih8vrjn2xfparyakiw2jz3778icg")))

(define-public crate-gitql-cli-0.4.0 (c (n "gitql-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gitql-ast") (r "^0.2.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "164q3p3jhc4yklng4ss73yqh0ma2zwaj26cwph0s0xgkwgkmxl52")))

(define-public crate-gitql-cli-0.5.0 (c (n "gitql-cli") (v "0.5.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gitql-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1nixs52ankpnrqyfv6x8kh2piwf3acc6h8i0wbjymzc1709bna78")))

(define-public crate-gitql-cli-0.6.0 (c (n "gitql-cli") (v "0.6.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gitql-ast") (r "^0.4.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.5.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1rrrg27qbiyfk34kfgqix8bq00dayz19b5y1n8jmakhmxkc586fl")))

(define-public crate-gitql-cli-0.7.1 (c (n "gitql-cli") (v "0.7.1") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gitql-ast") (r "^0.5.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.6.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1x77fcy4704z2gp7m1m1clagr0qgdv7b9n96jw09kpxc7ak623q7")))

(define-public crate-gitql-cli-0.8.0 (c (n "gitql-cli") (v "0.8.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.6.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1qpqn1y5zmn23h3n89ap99grv13r1ahjxsvy5qglpilkzzs7dvsv")))

(define-public crate-gitql-cli-0.9.0 (c (n "gitql-cli") (v "0.9.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.7.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0xiwfwhfjhhhjh2vm5gq4bshf3sfvxdpi46pzzwmawvr3iqcbmn7")))

(define-public crate-gitql-cli-0.10.0 (c (n "gitql-cli") (v "0.10.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.8.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "020h8sz77l00wkz16szwqmgyrlnnhl38h9avl3pbba9ps61b6w81")))

(define-public crate-gitql-cli-0.11.0 (c (n "gitql-cli") (v "0.11.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.9.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.10.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0yjx5w64phdwdgdx26vh9v3vbzpbzjm3s9a3717l36ablqs04wz6")))

(define-public crate-gitql-cli-0.12.0 (c (n "gitql-cli") (v "0.12.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.10.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.11.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0vh2hm8wn7i43d66icif1bphrsqvybcncs4x1kxz4c66cfql5pvb")))

(define-public crate-gitql-cli-0.13.0 (c (n "gitql-cli") (v "0.13.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.11.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0yflzd5l6jww652bw3hmmpcws6py73qmxv7fzibnpfsmb9rdr7q5")))

(define-public crate-gitql-cli-0.14.0 (c (n "gitql-cli") (v "0.14.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.12.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.13.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1qbqva5hgxyvhzj5s83lsxsbk3pqp5s02c8vgan5haw52gwjd5j8")))

(define-public crate-gitql-cli-0.15.0 (c (n "gitql-cli") (v "0.15.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.13.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.14.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "115fxyb8m26wb0m2n7z0bmhj0py9yzan222kz9k432h6l2svdq92")))

(define-public crate-gitql-cli-0.16.0 (c (n "gitql-cli") (v "0.16.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.14.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.15.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1fsgs0lhzini29a7sdw2zs2g2nvrhkdfm26f6hj1yr2v28qmn7yy")))

(define-public crate-gitql-cli-0.17.0 (c (n "gitql-cli") (v "0.17.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.15.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0qc0d5c6jibpz2clvsa0lzw04mxnp0zrya033hin0x3xm4n86j8m")))

(define-public crate-gitql-cli-0.18.0 (c (n "gitql-cli") (v "0.18.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.16.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.17.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1vrnpxc092vw9a50wzmzl3hny5a6sv09xm1ay8vm59ddy02df7wq")))

(define-public crate-gitql-cli-0.19.0 (c (n "gitql-cli") (v "0.19.0") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.17.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.18.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0087mc0kbfvxb7yaid1dp7bn7fbf7sd1z3lv432z3xpa1737zbff")))

(define-public crate-gitql-cli-0.19.1 (c (n "gitql-cli") (v "0.19.1") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.17.1") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.18.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "06sj4b29f58nbx1b1shrigw2s9y9aj8ahdsrqrczbmii1mycgx7s")))

