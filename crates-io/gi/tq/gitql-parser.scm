(define-module (crates-io gi tq gitql-parser) #:use-module (crates-io))

(define-public crate-gitql-parser-0.1.0 (c (n "gitql-parser") (v "0.1.0") (d (list (d (n "gitql-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0za94dgwjpyv02xqkmd7kkgw8qj877kmgwrffk3p90q2dapkdvfb")))

(define-public crate-gitql-parser-0.2.0 (c (n "gitql-parser") (v "0.2.0") (d (list (d (n "gitql-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fxk2dbig2i0z7p52nk67z7526x3d3sm93w0hbljkdchqbf3yzmx")))

(define-public crate-gitql-parser-0.3.0 (c (n "gitql-parser") (v "0.3.0") (d (list (d (n "gitql-ast") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "142nr3kc4khwg8cx0pirymf8z67kvbdq3m3r1jmys78ai0yw9n7a")))

(define-public crate-gitql-parser-0.4.0 (c (n "gitql-parser") (v "0.4.0") (d (list (d (n "gitql-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1p3byl7crp2sxcrxi32c92ncadg1mypxym3pn4ddyq9f3p8fxrsp")))

(define-public crate-gitql-parser-0.5.0 (c (n "gitql-parser") (v "0.5.0") (d (list (d (n "gitql-ast") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "02ryb8cfr1r2y5d584i806if8b0d3nhp4h8wcxjlmngnqsv6ivxr")))

(define-public crate-gitql-parser-0.6.0 (c (n "gitql-parser") (v "0.6.0") (d (list (d (n "gitql-ast") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0aarhjwnsvjck240z1zz2a142s827phps3x05yp7z0rzyki2f7hp")))

(define-public crate-gitql-parser-0.7.0 (c (n "gitql-parser") (v "0.7.0") (d (list (d (n "gitql-ast") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1p9zkj8zrsr3ncqxjnybidm4lx91fsyg40nvxm7pl0c52mv088rr")))

(define-public crate-gitql-parser-0.8.0 (c (n "gitql-parser") (v "0.8.0") (d (list (d (n "gitql-ast") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gq94j8bl41kc7fw779mlscabdvzk1h5dz0sjyikq03chc8mc6wx")))

(define-public crate-gitql-parser-0.9.0 (c (n "gitql-parser") (v "0.9.0") (d (list (d (n "gitql-ast") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1q7zfl8fg5s3h11kfwxyh5kpqdakhy4y1xl8pqplqzfhangsgp37")))

(define-public crate-gitql-parser-0.10.0 (c (n "gitql-parser") (v "0.10.0") (d (list (d (n "gitql-ast") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fd5iz8mc0rimh44zh80a9svqkxnm7brwfs09jsag9lvqskr3wjp")))

(define-public crate-gitql-parser-0.11.0 (c (n "gitql-parser") (v "0.11.0") (d (list (d (n "gitql-ast") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ys88y07hr99czcqbs7mcigni8cl0l4zmmqg1ln54s5fy93vx9i1")))

(define-public crate-gitql-parser-0.12.0 (c (n "gitql-parser") (v "0.12.0") (d (list (d (n "gitql-ast") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1d9fjdv5yh2zmwawn3f5wk75mx5cg5p1ya2zzlq02kfdgmhyp0gi")))

(define-public crate-gitql-parser-0.13.0 (c (n "gitql-parser") (v "0.13.0") (d (list (d (n "gitql-ast") (r "^0.12.0") (d #t) (k 0)))) (h "0sxn9ald3i1mwdsykd8xpbb15gprl44wic3y8h0nk9h16m5jifaw")))

(define-public crate-gitql-parser-0.14.0 (c (n "gitql-parser") (v "0.14.0") (d (list (d (n "gitql-ast") (r "^0.13.0") (d #t) (k 0)))) (h "1gvfqqsrkypdn46mljv4sal4r3cjid4wslbn10an133zaqfz7x1j")))

(define-public crate-gitql-parser-0.15.0 (c (n "gitql-parser") (v "0.15.0") (d (list (d (n "gitql-ast") (r "^0.14.0") (d #t) (k 0)))) (h "1zw3njacbfxm1kcjqdgnh207pmbxkbb7kbwrbv8hh5mrmj3pfya5")))

(define-public crate-gitql-parser-0.16.0 (c (n "gitql-parser") (v "0.16.0") (d (list (d (n "gitql-ast") (r "^0.15.0") (d #t) (k 0)))) (h "0s9f1yl5ajq7aazmhl5dg92pakbarkwa1d2m2gawkng4j5ybxb9m")))

(define-public crate-gitql-parser-0.17.0 (c (n "gitql-parser") (v "0.17.0") (d (list (d (n "gitql-ast") (r "^0.16.0") (d #t) (k 0)))) (h "1ak6xriv5wsp00fpj3pqm8sfk3lis7ywh4v89izy657i26iqpmnn")))

(define-public crate-gitql-parser-0.18.0 (c (n "gitql-parser") (v "0.18.0") (d (list (d (n "gitql-ast") (r "^0.17.0") (d #t) (k 0)))) (h "0gl72hr81xs5vc7iwksia9441h0fcd4dxsc7acm2n2nn57dp15rh")))

(define-public crate-gitql-parser-0.18.1 (c (n "gitql-parser") (v "0.18.1") (d (list (d (n "gitql-ast") (r "^0.17.1") (d #t) (k 0)))) (h "0wi82c400187921rqyv34rzlq8xmzpmqyaa5i4kc9qh43ihpzdcl")))

