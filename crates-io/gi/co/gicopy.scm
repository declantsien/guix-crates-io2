(define-module (crates-io gi co gicopy) #:use-module (crates-io))

(define-public crate-gicopy-0.1.0 (c (n "gicopy") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "1gf51n24bdk0k8bjrj91sbpxlpha0m6ydmz1wcanwzzbxycvslgp")))

(define-public crate-gicopy-0.1.1 (c (n "gicopy") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "15cj85h7h10gxlb3x9vyx27v22lnsx7sbsxmvh4930s0rzl5rrxg")))

