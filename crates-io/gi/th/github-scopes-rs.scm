(define-module (crates-io gi th github-scopes-rs) #:use-module (crates-io))

(define-public crate-github-scopes-rs-1.0.0 (c (n "github-scopes-rs") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "mockito") (r "^0.31.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0721fcrbqyvplwin8s4lc4ii91jfn553bh7sr7kwk1s1azapqd6n")))

(define-public crate-github-scopes-rs-1.0.1 (c (n "github-scopes-rs") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "mockito") (r "^0.31.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1dm7nxkw6mxvl2xkmb5jf3fg7b0kffx7jzdc9lr7f53rxcg9z53x")))

