(define-module (crates-io gi th github-actions-models) #:use-module (crates-io))

(define-public crate-github-actions-models-0.1.0 (c (n "github-actions-models") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "1wv17jryy4wylb498zsaghjq8zrgfgw6jw5ykq8idaqp7i1xnrd8")))

(define-public crate-github-actions-models-0.1.2 (c (n "github-actions-models") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "01lkqgfnmvy9q53agc16hbdrh1afsf0wfbaq6vf4w0lcpx0szvdl")))

(define-public crate-github-actions-models-0.2.0 (c (n "github-actions-models") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)))) (h "0qj3idzl3hxqm5jrd31yl0jw3m07sz1k73726n0ll8g1144kp40q")))

