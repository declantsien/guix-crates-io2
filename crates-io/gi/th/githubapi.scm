(define-module (crates-io gi th githubapi) #:use-module (crates-io))

(define-public crate-githubapi-0.1.0 (c (n "githubapi") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qbqn6fngnypa5rhc5bgzh50hqmhn1f61rg747fv4lvd71lf3n7x")))

