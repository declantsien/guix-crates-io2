(define-module (crates-io gi th github_webhook_message_validator) #:use-module (crates-io))

(define-public crate-github_webhook_message_validator-0.1.0 (c (n "github_webhook_message_validator") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)))) (h "017738h2v49zs8qd5srf3wh3cvm5aj1a1a3zhrxxxn2c0i8n7z87")))

(define-public crate-github_webhook_message_validator-0.1.1 (c (n "github_webhook_message_validator") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)))) (h "0pbyc7s34kczp5bgjang20v2rfswhl45xdznmhyn0ka776cy234y")))

(define-public crate-github_webhook_message_validator-0.1.2 (c (n "github_webhook_message_validator") (v "0.1.2") (d (list (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)))) (h "0f8p2xw4py725z7bkrw3bjfr12rngf7vggpcj5mi8x92b9w7bg5c")))

(define-public crate-github_webhook_message_validator-0.1.3 (c (n "github_webhook_message_validator") (v "0.1.3") (d (list (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)))) (h "0za1ddrci2qc1nc009zq1s57f23nyrswgqypqnwis5frz87ab8ii")))

(define-public crate-github_webhook_message_validator-0.1.5 (c (n "github_webhook_message_validator") (v "0.1.5") (d (list (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)))) (h "1fh07wi0f5pb95q98w1213k1b965nxahjxqv9sgihkz4ajnpimvb")))

(define-public crate-github_webhook_message_validator-0.1.6 (c (n "github_webhook_message_validator") (v "0.1.6") (d (list (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)))) (h "14m9v70csdg045mb8dqx59qi2mfmipwk588jm3jk1pc6j43b2j74")))

