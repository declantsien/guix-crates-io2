(define-module (crates-io gi th github-stats) #:use-module (crates-io))

(define-public crate-github-stats-0.1.0 (c (n "github-stats") (v "0.1.0") (d (list (d (n "big-bytes") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19ywixwm992qvfqyjwz2s7fid4k801ckrqc3fqj0mii08ybvp7q5")))

(define-public crate-github-stats-0.2.0 (c (n "github-stats") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0acplsjfkqphbq9ryxz1l0p8hzx8d3nll48spn4kzbf0cp62y9kd")))

(define-public crate-github-stats-0.3.0 (c (n "github-stats") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1sx7x8dcbzds0dsz1627bxhxkmbzy7vsf8m7np3xd4bcvqpjqd66")))

(define-public crate-github-stats-0.4.0 (c (n "github-stats") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1ii1vaw7j70amf4nkxrd3jg966lifd8vnn7pfk4k9yicjdy0p9k4")))

(define-public crate-github-stats-0.5.0 (c (n "github-stats") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "19fjnjxaldbsvhbpm03jhfa1ds5lhhi87av6b0z8ssr8z9icqr9p")))

