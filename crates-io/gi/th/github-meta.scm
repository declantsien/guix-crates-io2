(define-module (crates-io gi th github-meta) #:use-module (crates-io))

(define-public crate-github-meta-0.1.0 (c (n "github-meta") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0mz534rwmzb5zidyani0lwj1jbmfmd5j2fb2vdg8r0y1jbg3szwz")))

(define-public crate-github-meta-0.1.1 (c (n "github-meta") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0v1wjvdk1x3a35kmm6rfvll93z7xll3m1dpildwnvziyzgqlkhd2")))

(define-public crate-github-meta-0.2.0 (c (n "github-meta") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0iv1dmncsdpsn9p4dj5mk2fnp0xykw6ajikfiqmv24p6ad9m266n")))

(define-public crate-github-meta-0.3.0 (c (n "github-meta") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0ncpikgf1ncqh6d44dygv72mjdnziql917aqsmialv2wl6fp1wrh")))

(define-public crate-github-meta-0.4.0 (c (n "github-meta") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "04r8fpjgwd16dihv6b35h9gpci3xmpbjanaj4i1vl2d7f3rlc2xy")))

(define-public crate-github-meta-0.5.0 (c (n "github-meta") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "1fv8z0b7g4yx631k3f5b90y36ny3106xp1d8djcwxzjvdsf3a4rf")))

(define-public crate-github-meta-0.6.0 (c (n "github-meta") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "0vdmd7fa8pcchhina0afysm9j47xz9g1lnm1iy3zis5532a5l87a")))

(define-public crate-github-meta-0.8.0 (c (n "github-meta") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "03asqns88lln74s4scz8f8sf72bdfisy1j3zv29jmr2qgihx3hdc")))

(define-public crate-github-meta-0.10.0 (c (n "github-meta") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "18g8yjrqmwabvg389l55kc0k47i8ryw7f90z5ijmxhhsg7djc9bi")))

(define-public crate-github-meta-0.11.0 (c (n "github-meta") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 1)))) (h "07i6ar45xm203926505bwvb6jx9hxmpc8s3r9xs96wfraj74lb55")))

