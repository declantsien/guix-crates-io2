(define-module (crates-io gi th github) #:use-module (crates-io))

(define-public crate-github-0.0.1 (c (n "github") (v "0.0.1") (h "1a5lxrawrxv1d23xfry5jv1in2vfyi84y0ml9xjp8nwcf4zf8sji")))

(define-public crate-github-0.1.0 (c (n "github") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "022lr93v8valiz2ylwshaihbk5bkx5q9ml7wacncl4sj0863ysxs")))

(define-public crate-github-0.1.1 (c (n "github") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1c4ksdf6b9ydm91j5gdvfwdl20rw9znmadbxdjlhp0r98mnzcaql")))

(define-public crate-github-0.1.2 (c (n "github") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "18f8h3fkm5va2xybydnijdxf996d4k65i5fglb4ni32h31pg1x4f")))

