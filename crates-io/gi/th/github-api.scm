(define-module (crates-io gi th github-api) #:use-module (crates-io))

(define-public crate-github-api-0.1.0 (c (n "github-api") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "burgundy") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)))) (h "1mqqw2qrim6fndijymyv48ipd2z011cvs88ks9c6jkwrm7dagvlf")))

(define-public crate-github-api-0.1.1 (c (n "github-api") (v "0.1.1") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "burgundy") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)))) (h "1v8zci4lvf77n64rvf3swg1lydwlsnci5im09fmvppn6q2fdmx30")))

(define-public crate-github-api-0.2.0 (c (n "github-api") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "burgundy") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)))) (h "09lqm0fg38glkk7s8xnf16n3cga425j8w4gbzylmm760gigc1lr7")))

(define-public crate-github-api-0.2.1 (c (n "github-api") (v "0.2.1") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "burgundy") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)))) (h "0aaj1xsf2jcn8czi3w28f05099layjwciyxk3q3n9xd57q1dqsbm")))

(define-public crate-github-api-0.2.2 (c (n "github-api") (v "0.2.2") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "burgundy") (r "^0.2.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)))) (h "12s4zkckmwkb85r5bwjrs9jfciw3693myjz9k35iiyiyn0671baf")))

(define-public crate-github-api-0.2.3 (c (n "github-api") (v "0.2.3") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "burgundy") (r "^0.2.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)))) (h "0yph69yc41nnyxfdz58yipk774ny5s7ws6vhxcv3xw9spkmd3wfi")))

(define-public crate-github-api-0.2.4 (c (n "github-api") (v "0.2.4") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "burgundy") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)))) (h "1dz7jcp9lczv2wpj71vj0x9wazwvj7q8p341ixqhbkz7ccy3s7ps")))

(define-public crate-github-api-0.3.0 (c (n "github-api") (v "0.3.0") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "burgundy") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.19") (d #t) (k 0)))) (h "0jmg225dlip2byapngd7k4xlimn5hmv9f5c0k2ab4vp6cqxyb02q")))

(define-public crate-github-api-0.4.0 (c (n "github-api") (v "0.4.0") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "burgundy") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.19") (d #t) (k 0)))) (h "0z92ilxndvld6i15fqfc802ynn9vs0cdq6jilnp56mxx5cw0143x")))

(define-public crate-github-api-0.4.1 (c (n "github-api") (v "0.4.1") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "burgundy") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.19") (d #t) (k 0)))) (h "1rzmmxc1hrwgw3h513xinjsqbvhvwdhz7zp0835rn6bgcn3gnaxc")))

(define-public crate-github-api-0.4.2 (c (n "github-api") (v "0.4.2") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "burgundy") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.19") (d #t) (k 0)))) (h "0a5mpp3qba1nlzwjvykqmwgawzn7lppxvmg9iw6ij5zbw9mh9mrf")))

(define-public crate-github-api-0.4.3 (c (n "github-api") (v "0.4.3") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "burgundy") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.19") (d #t) (k 0)))) (h "09cjk8fjb7r4i6jzpx5ff1rs69pliifrnaql2lq5bahbialh5736")))

