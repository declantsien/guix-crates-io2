(define-module (crates-io gi th github-flows-macros) #:use-module (crates-io))

(define-public crate-github-flows-macros-0.1.0 (c (n "github-flows-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13l4a3c9jcy3ly6f1jrw7mkd5w4lg5hpimxc95bfhg6193k4japw")))

(define-public crate-github-flows-macros-0.2.0 (c (n "github-flows-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06j9pxafc5avnk37dv2xndqz4qq7ni0gimcs52b1pj91k3bv85rc")))

