(define-module (crates-io gi th github-slugger) #:use-module (crates-io))

(define-public crate-github-slugger-0.1.0 (c (n "github-slugger") (v "0.1.0") (d (list (d (n "once_cell") (r ">=1.0.1, <2") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0iriyj7000di7bbp43bzzdr9hkbmi0rbci71hba2gi5ixbs2063j")))

