(define-module (crates-io gi th github_username_regex) #:use-module (crates-io))

(define-public crate-github_username_regex-0.0.1 (c (n "github_username_regex") (v "0.0.1") (d (list (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0h8sphyc1h5w5bz5j5flfv7vws5rlf0nl1a6ygy3g2xix879ig80")))

(define-public crate-github_username_regex-0.0.4 (c (n "github_username_regex") (v "0.0.4") (d (list (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0awc5pjb47rpsrkrxg4ikk1px8y7kjwqs57cpbn73kv5163h04k8")))

(define-public crate-github_username_regex-1.0.0 (c (n "github_username_regex") (v "1.0.0") (d (list (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "03ib37xhndaz78fk5mr3mcbkl9khbczii22nj2gf2ipjnc6zi3z9")))

