(define-module (crates-io gi th github-openapi) #:use-module (crates-io))

(define-public crate-github-openapi-0.1.0 (c (n "github-openapi") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b3140kj5xzjbcaaw02fpy3814i6aprl9d5jcfds6znccfya7v9h")))

(define-public crate-github-openapi-0.1.1 (c (n "github-openapi") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qfjmaywacx0amz1b298mamgkka3v2nnvhglbpj56q219yngz4f8")))

