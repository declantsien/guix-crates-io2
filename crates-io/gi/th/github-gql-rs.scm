(define-module (crates-io gi th github-gql-rs) #:use-module (crates-io))

(define-public crate-github-gql-rs-0.0.1 (c (n "github-gql-rs") (v "0.0.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1iidssbffqhnggcm4c4xc3qxflc2783nr9rrkwavgq11yz97iz00") (y #t)))

