(define-module (crates-io gi th github-socialify-preview) #:use-module (crates-io))

(define-public crate-github-socialify-preview-1.0.0 (c (n "github-socialify-preview") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive-config") (r "^1") (f (quote ("json"))) (d #t) (k 0)) (d (n "headless_chrome") (r "^1") (f (quote ("fetch"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "1152mvfr28hqlr01flsn5bnkx5fxhqjk2kmll0n7yxsfs4080p0p") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "rustls-tls"))))))

