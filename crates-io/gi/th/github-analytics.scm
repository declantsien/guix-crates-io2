(define-module (crates-io gi th github-analytics) #:use-module (crates-io))

(define-public crate-github-analytics-0.1.0 (c (n "github-analytics") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "influxdb") (r "^0.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "text-tables") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mjb3a2qlm81wvr0v42g1h8g5j3rn5lzcffbqa1bzgxzcprdfq16")))

