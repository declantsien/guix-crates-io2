(define-module (crates-io gi th github-local-remote) #:use-module (crates-io))

(define-public crate-github-local-remote-0.1.0 (c (n "github-local-remote") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (d #t) (k 0)))) (h "0f1g9dv227lvwv7mwzagl5v2lcdpq2mrynjwxlivlg15hxjkq9c1")))

(define-public crate-github-local-remote-0.1.1 (c (n "github-local-remote") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (d #t) (k 0)))) (h "0hnbig80gi3c3ndaj5h3hbzilmisnjg29138jcqblkm26c13l8aa")))

