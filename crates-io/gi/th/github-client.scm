(define-module (crates-io gi th github-client) #:use-module (crates-io))

(define-public crate-github-client-0.1.0 (c (n "github-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1iz0pdqlq0wpvvg2dli41q47vckzfwpr18pfjmfyzd93qwmmbqrp")))

