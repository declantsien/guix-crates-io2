(define-module (crates-io gi th githist) #:use-module (crates-io))

(define-public crate-githist-0.1.0 (c (n "githist") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "timeago") (r "^0.4.1") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "13rk0mh43lhnjcg4dbynsi1vfgspsap542wv59gi06ax3rz4m53c")))

(define-public crate-githist-0.2.0 (c (n "githist") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "timeago") (r "^0.4.1") (d #t) (k 0)))) (h "0rhw1haq5j7zsfg4rgz70w47cjswrmm39q1prg3ljdddxbncm0id")))

