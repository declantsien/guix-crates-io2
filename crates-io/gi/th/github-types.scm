(define-module (crates-io gi th github-types) #:use-module (crates-io))

(define-public crate-github-types-0.1.0 (c (n "github-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v9p8msvvxw43rmb255irxy2xh8r84rzgsw5cb34y3yxx6ywngrg")))

(define-public crate-github-types-0.1.1 (c (n "github-types") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.14") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01f7rzr0w9ryx30cqgcg63ms549zv3pgkbvnrykc36kr6b60ccw9")))

