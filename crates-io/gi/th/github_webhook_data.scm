(define-module (crates-io gi th github_webhook_data) #:use-module (crates-io))

(define-public crate-github_webhook_data-0.0.1 (c (n "github_webhook_data") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "11x6h244122056mim7gzanx05i4jbry15rd9p8y1awvb0i6npwfx")))

(define-public crate-github_webhook_data-0.0.2 (c (n "github_webhook_data") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17hgx4mrhgs6qcacqi7s0c404jj6xxkyf1cgzi16kcb57xri7rwx")))

