(define-module (crates-io gi th githelper) #:use-module (crates-io))

(define-public crate-githelper-0.1.0 (c (n "githelper") (v "0.1.0") (d (list (d (n "git2") (r "^0.5") (d #t) (k 0)))) (h "1p14m0py64nn4hh48g0iz7nj6m4zbd2l714f62cpbfp5cjj9ch2d")))

(define-public crate-githelper-0.1.1 (c (n "githelper") (v "0.1.1") (d (list (d (n "git2") (r "^0.6") (d #t) (k 0)))) (h "0a3j10fqahhk7h3cw0ijmzpi23zh8b9ayx6vvkx802cs5icvk4j4")))

(define-public crate-githelper-0.2.0 (c (n "githelper") (v "0.2.0") (h "0lwy30b2lyhz18jhbki26vyclmmrr7404s1vqhl2yyk3bfr1maxc")))

(define-public crate-githelper-0.3.0 (c (n "githelper") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0anxnb08p9kryq9a8lwgrsm8fd2zq15zlgp76s79xq2cdmsc1jqi")))

