(define-module (crates-io gi th github_release_rs) #:use-module (crates-io))

(define-public crate-github_release_rs-0.1.0 (c (n "github_release_rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "serde_json" "json" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02mchls880n3qyfx07sdnf1p464ilyplgsa082cnv85n6zr6n80i") (y #t)))

