(define-module (crates-io gi th github-actions-autodocs) #:use-module (crates-io))

(define-public crate-github-actions-autodocs-0.1.0 (c (n "github-actions-autodocs") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pwzcc7i2rirx9a8ljsk55y0sh4n2vd6zy1zqdpgyaldhfwvcw2z")))

(define-public crate-github-actions-autodocs-0.1.1 (c (n "github-actions-autodocs") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gc9p8jq4grabjzx6vwmg6x7b5bsj345znnv8h2g3ypmr4mpwdva")))

(define-public crate-github-actions-autodocs-0.1.2 (c (n "github-actions-autodocs") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (f (quote ("no-color"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wj60j3s38dkaxgka9vwbfv6xxkqfvxjrxbxcp4dbisnafxivlin")))

(define-public crate-github-actions-autodocs-0.1.3 (c (n "github-actions-autodocs") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wyify53hfiap73i4k26spfh8vjp6ky9hxsjisk48bfxar2j0mi5")))

(define-public crate-github-actions-autodocs-0.1.4 (c (n "github-actions-autodocs") (v "0.1.4") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bzgpakmndx5wk2fm6yg7r9kxabchgkqpf9dda59b0kvg01diqa1")))

