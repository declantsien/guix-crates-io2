(define-module (crates-io gi th github-webhook-dts-downloader) #:use-module (crates-io))

(define-public crate-github-webhook-dts-downloader-0.2.0 (c (n "github-webhook-dts-downloader") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0xd59zb2svv6yi5lxqk1pzf1r6ay82546zwxvxaj88sksav3s2gz")))

(define-public crate-github-webhook-dts-downloader-0.2.1 (c (n "github-webhook-dts-downloader") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1gpf58jfk1l9qnpix2pbn1z6nlcpfz26ca28bd99yz9gpfk6ld6w")))

(define-public crate-github-webhook-dts-downloader-0.2.2 (c (n "github-webhook-dts-downloader") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)))) (h "0wgqkgs40bgkqdzpnadwzhnay9wifh93psp1n6mf4bplbs29i73z")))

(define-public crate-github-webhook-dts-downloader-0.2.3 (c (n "github-webhook-dts-downloader") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)))) (h "1s0g5jwp6cphyyy1lzjggkzfhk4xv40f03fm6g1x462gmp1nl10g")))

(define-public crate-github-webhook-dts-downloader-0.2.4 (c (n "github-webhook-dts-downloader") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)))) (h "15lsijdchvckwlyqbdic8gvjvr8sz6gb5bw08lajixipsycawh6c")))

(define-public crate-github-webhook-dts-downloader-0.3.0 (c (n "github-webhook-dts-downloader") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)))) (h "14r9b2yh2ka3497gqqn3z0zgzjxa9kkdwxfpwxipsj185nbfi6fg")))

(define-public crate-github-webhook-dts-downloader-0.3.1 (c (n "github-webhook-dts-downloader") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "08wplca7k84i2i47y48qd9qfsj8jx15ry35x3fkr94k8z0m4n42b")))

(define-public crate-github-webhook-dts-downloader-0.3.2 (c (n "github-webhook-dts-downloader") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0aj1h3fb030ka7la2rymxz3m4zyxryssbp15m1cf199q167kfgnq")))

(define-public crate-github-webhook-dts-downloader-0.4.0 (c (n "github-webhook-dts-downloader") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1svvz7ckgp2sv5j7kjg9z7c61s7sb0ha5x03i33ywq254znsk1xr")))

(define-public crate-github-webhook-dts-downloader-0.5.0 (c (n "github-webhook-dts-downloader") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "04yp5l4snaps7d5l15lshah9qsqa8ix1viyzbb3za9z9m3wd0qhq") (r "1.64.0")))

(define-public crate-github-webhook-dts-downloader-0.5.1 (c (n "github-webhook-dts-downloader") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1sb6fs6k18wxh13wnfhqwzd2lp0mmwvyryqq4axy9ahkh61irwz2") (r "1.64.0")))

(define-public crate-github-webhook-dts-downloader-0.5.2 (c (n "github-webhook-dts-downloader") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "github-webhook-type-generator") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)))) (h "186mxlc8irallgla5y0x1bxpxr97g4i1xh4752lga2sk1fx8hc00") (r "1.64.0")))

