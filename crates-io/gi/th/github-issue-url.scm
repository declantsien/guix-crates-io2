(define-module (crates-io gi th github-issue-url) #:use-module (crates-io))

(define-public crate-github-issue-url-0.1.0 (c (n "github-issue-url") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0lg5ma8mh4nqfg0v5fa83mgbhxhfwcndxqbfzgrlg9h1v0664b85")))

