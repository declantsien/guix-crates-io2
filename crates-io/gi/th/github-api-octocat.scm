(define-module (crates-io gi th github-api-octocat) #:use-module (crates-io))

(define-public crate-github-api-octocat-0.1.0 (c (n "github-api-octocat") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "00hawyd1px0r5xh7jyjb91mv88rzlp1xqnzj72il3z5jqvppqq18")))

