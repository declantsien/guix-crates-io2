(define-module (crates-io gi th github-heatmap) #:use-module (crates-io))

(define-public crate-github-heatmap-0.1.0 (c (n "github-heatmap") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0fprcrm8mf9rff10r98cqcfb279bn5kqrjsa43br3g1y008fsbhh")))

(define-public crate-github-heatmap-1.0.0 (c (n "github-heatmap") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0yia2wdgvc3igj6v7qf0m36qk2sxx9l0y278wjfj0l24i8rxw94v")))

