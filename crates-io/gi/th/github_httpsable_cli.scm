(define-module (crates-io gi th github_httpsable_cli) #:use-module (crates-io))

(define-public crate-github_httpsable_cli-0.1.0 (c (n "github_httpsable_cli") (v "0.1.0") (d (list (d (n "git_httpsable") (r "^1.0.0") (d #t) (k 0)))) (h "0syn40gnc495z2009r0b2ybcdc80sp9m3fh2ghayq1cqysnb5xw8")))

(define-public crate-github_httpsable_cli-1.0.0 (c (n "github_httpsable_cli") (v "1.0.0") (d (list (d (n "git_httpsable") (r "^1.0.0") (d #t) (k 0)))) (h "0s3d5w6x5gw2gj7awn2q49yrxc2ksflzwn0zf18fm6f9g07n4ky7")))

