(define-module (crates-io gi th github-actions-oidc-claims) #:use-module (crates-io))

(define-public crate-github-actions-oidc-claims-0.1.0 (c (n "github-actions-oidc-claims") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yaq92x2hmg8fkz0ggwbyayrsrb3kzxbylbpq60cjjq6szmz5wj3")))

(define-public crate-github-actions-oidc-claims-0.2.0 (c (n "github-actions-oidc-claims") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kzhp01i2zw7av1xd62gkx7blqgy9vgdj8fsdvpbs34rdwm3748v")))

(define-public crate-github-actions-oidc-claims-0.3.0 (c (n "github-actions-oidc-claims") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j2d3hwjdb7kq8v0jxjjjmcsasnl0s3x4pk67i2a1cp2cy4yz4mk")))

