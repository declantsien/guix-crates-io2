(define-module (crates-io gi th github-status) #:use-module (crates-io))

(define-public crate-github-status-1.1.0 (c (n "github-status") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("color" "suggestions" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "1wy1wdrajdlmr311qflhpnap364c1i1ig8dj06zj9f0avk860fcw")))

(define-public crate-github-status-1.2.0 (c (n "github-status") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("color" "suggestions" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0cjvffvzxnq8cac7mxj1ih0nqw1y1p9s7592ds071b4jhdxwcrhk")))

