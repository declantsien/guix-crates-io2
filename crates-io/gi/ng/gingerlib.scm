(define-module (crates-io gi ng gingerlib) #:use-module (crates-io))

(define-public crate-gingerlib-0.1.0 (c (n "gingerlib") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)))) (h "0aw1x00gwaldkd9fd41x92agdb3nd37sy2ylcjks8agpy4iqkr0l")))

(define-public crate-gingerlib-1.0.0 (c (n "gingerlib") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "test-log") (r "^0.2.14") (f (quote ("trace"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)))) (h "0ki2q14b41xnjxdnkkhbx2fcp02hgidzprn1vwzf21b3zzfkzq5d")))

