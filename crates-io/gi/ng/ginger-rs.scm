(define-module (crates-io gi ng ginger-rs) #:use-module (crates-io))

(define-public crate-ginger-rs-0.1.0 (c (n "ginger-rs") (v "0.1.0") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "17fhmsfm00j7pwswc7nlpf3kal8msq2980fwckxzbvml7b0cdbc8")))

(define-public crate-ginger-rs-0.1.1 (c (n "ginger-rs") (v "0.1.1") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "06lnxfxa5wikbkhvikyylsi2zv9s7nbj7xfr5rgglxzimczz329h")))

