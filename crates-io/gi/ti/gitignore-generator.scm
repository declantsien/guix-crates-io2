(define-module (crates-io gi ti gitignore-generator) #:use-module (crates-io))

(define-public crate-gitignore-generator-0.1.0 (c (n "gitignore-generator") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "0nv51ccragbsf26cnknn40dhia8q0fpj7lkgispcav9hd6c02frp")))

(define-public crate-gitignore-generator-0.1.1 (c (n "gitignore-generator") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "0jk4gq8qn404k7kv6dcg7imxjy4c56c49mhpmz9083lyghx42qy0")))

