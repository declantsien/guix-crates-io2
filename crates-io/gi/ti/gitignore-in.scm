(define-module (crates-io gi ti gitignore-in) #:use-module (crates-io))

(define-public crate-gitignore-in-0.1.0 (c (n "gitignore-in") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10.57") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1fqsvw0jk9fkm8qwyixgkgngynkblfgzjw2ivqh1m6199s7k652a")))

(define-public crate-gitignore-in-0.2.0 (c (n "gitignore-in") (v "0.2.0") (d (list (d (n "mktemp") (r "^0.5.1") (d #t) (k 2)) (d (n "openssl") (r "^0.10.57") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "15d539rrziikj7i3k0wvj4v9xpg4rirpnfvg5319qcb50wjfilhp")))

