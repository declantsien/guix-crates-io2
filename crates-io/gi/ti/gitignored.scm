(define-module (crates-io gi ti gitignored) #:use-module (crates-io))

(define-public crate-gitignored-0.1.0 (c (n "gitignored") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1r7a54aczxvakqqpaspyqimqzx75mpgk9vgvx2dxpkagxnxrjhdk")))

(define-public crate-gitignored-0.2.0 (c (n "gitignored") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0377lk4z9dx6kvhqi3vlwcy31rbs04x5l41smarssdzn72cd3fbp")))

(define-public crate-gitignored-0.2.1 (c (n "gitignored") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1sn7gc0zixw6ks5h6cla9sfvmj1jpnlz2wnrzvh04zvai72qhnnw")))

(define-public crate-gitignored-0.3.0 (c (n "gitignored") (v "0.3.0") (d (list (d (n "globset") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0wnp9ygh2d4jnpjkxzkwj7rij65c2hgwx16ggl7zm0k2s0k2ylcd")))

(define-public crate-gitignored-0.3.1 (c (n "gitignored") (v "0.3.1") (d (list (d (n "globset") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1qdqash3dixsy8ng1v07nhj8vazq421gklg0lyxchnqdz4ij1jid")))

(define-public crate-gitignored-0.4.0 (c (n "gitignored") (v "0.4.0") (d (list (d (n "globset") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1iiwk4qkr81k8l074iqc9m78p678car33jv9wyl4q5pv4y69ndzm")))

