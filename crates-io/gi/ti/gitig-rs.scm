(define-module (crates-io gi ti gitig-rs) #:use-module (crates-io))

(define-public crate-gitig-rs-23.4.0 (c (n "gitig-rs") (v "23.4.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pager") (r "^0.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0yb5c948kmfiq2v2lgjpwx2c9xaqh8zgwpgs9aw5qcvzq4kbqbnv")))

