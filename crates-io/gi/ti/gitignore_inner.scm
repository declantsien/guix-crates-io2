(define-module (crates-io gi ti gitignore_inner) #:use-module (crates-io))

(define-public crate-gitignore_inner-0.1.0 (c (n "gitignore_inner") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("env" "unicode" "string"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("local-time" "env-filter"))) (d #t) (k 0)))) (h "0xd6s32xmliz37qspagymrbljhnmn58i0qscsy6idz1vl37x8m9q")))

