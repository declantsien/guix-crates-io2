(define-module (crates-io gi ti gitid) #:use-module (crates-io))

(define-public crate-gitid-0.1.0 (c (n "gitid") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12kpcfcpp5myfgc2x4rbisrxj0lb34arg9wj7q9c6xl66ysfkhid")))

(define-public crate-gitid-0.1.1 (c (n "gitid") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15m859yn780l1301i8p5c944zbngf6dncn0g4pz9vbq8a91pmsrv")))

(define-public crate-gitid-0.1.2 (c (n "gitid") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w08df8wgdhar18yz4vggaibr6avrk2diry9xc8fiydzx35vc7vz")))

