(define-module (crates-io gi dl gidle_future) #:use-module (crates-io))

(define-public crate-gidle_future-0.1.0 (c (n "gidle_future") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "glib") (r "^0.9.0") (d #t) (k 0)))) (h "1f23cpj6ivbkimhbvsiwjz0r32gccmwqd9m8x240myrn3fnkyg8f")))

(define-public crate-gidle_future-0.2.0 (c (n "gidle_future") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "glib") (r "^0.10.0") (d #t) (k 0)))) (h "1mz7wpw4lbr3zfp66mwv048w18rr9p3ljzc24las76dfrqqyhrd1")))

