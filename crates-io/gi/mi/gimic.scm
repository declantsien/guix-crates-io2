(define-module (crates-io gi mi gimic) #:use-module (crates-io))

(define-public crate-gimic-0.1.0 (c (n "gimic") (v "0.1.0") (h "1i8xig65qlxq24fxd9lkx3lk248zrz1hzmrnmi5prm6ci454nhf8")))

(define-public crate-gimic-0.1.1 (c (n "gimic") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "1ss90kr68bykdil68hsag55p6fdz1w07lvicnwki3l5zhgrz84gm")))

(define-public crate-gimic-0.1.2 (c (n "gimic") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "1g37r1ah43sdwkxgd23svpad8jjwvdm3ba24a6fzl83rs64hwyj5")))

(define-public crate-gimic-0.1.3 (c (n "gimic") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "1ya089rnd8p4mgz5r7472r4f4z554p9hcxxmm5csaf4bn0xh5mhc")))

(define-public crate-gimic-0.1.31 (c (n "gimic") (v "0.1.31") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "0l3gzlfr2khalv67a9an8dr2vdkjn7sjxkjzp5596qyamd28wzwv")))

(define-public crate-gimic-0.1.32 (c (n "gimic") (v "0.1.32") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "093jcsrwmdgfyfl8bfp993zl4si1lq4069q1p7sgh5jarp6dw8px")))

(define-public crate-gimic-0.1.33 (c (n "gimic") (v "0.1.33") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "06422nwdkd6s92w1cpbqccl2sw3dy1r20xgbmnrk3hk4smc358hc")))

