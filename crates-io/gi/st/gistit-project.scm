(define-module (crates-io gi st gistit-project) #:use-module (crates-io))

(define-public crate-gistit-project-0.1.0 (c (n "gistit-project") (v "0.1.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xvhw2hzh1jc8g6s44za6sab7hqpms0fhicgsvj1zp06pjz8gxsm")))

