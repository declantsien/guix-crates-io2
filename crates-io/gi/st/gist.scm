(define-module (crates-io gi st gist) #:use-module (crates-io))

(define-public crate-gist-0.3.1 (c (n "gist") (v "0.3.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1kzz31lm6i4a0ha0l62h6g70cv4qy2ixq6s6rzkdycx87ngsljxl")))

(define-public crate-gist-0.4.0 (c (n "gist") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0vi0s4g1wc81cp860f6i2vkmw2kv3v9p09ixzn1cjmyzb3gnaps1")))

(define-public crate-gist-0.4.1 (c (n "gist") (v "0.4.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13wkanhh5d943jigabmva3l8mqqm934ks22cci455x3yd3g1pn4i")))

(define-public crate-gist-0.4.3 (c (n "gist") (v "0.4.3") (d (list (d (n "error-chain") (r "~0.11") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.7") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "0sfldwnmp3frjhww1fx0g3vjzy383cscpdfgbkrjz9q07a35ap01") (y #t)))

(define-public crate-gist-0.4.4 (c (n "gist") (v "0.4.4") (d (list (d (n "error-chain") (r "~0.11") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.7") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1qknq71vk09gj3h1chbnwj4syjv29q331vhc15qrgg6qdil8lh2b")))

(define-public crate-gist-0.5.0 (c (n "gist") (v "0.5.0") (d (list (d (n "error-chain") (r "~0.11") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.7") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "07m52w7wl3vc62k7viipsz3k9jvqjhpdzpnl569x61jw2fzb30k2")))

(define-public crate-gist-0.6.1 (c (n "gist") (v "0.6.1") (d (list (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1xiaaxgbixwm1my0yaxidkr2lmmv1m0m0fzqg2b6dpynk3mihdpk")))

(define-public crate-gist-0.7.0 (c (n "gist") (v "0.7.0") (d (list (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1h64gai1rvdwwkkjg698c8h8gr89s7lb3h3h9v1s58inbs58zfl2")))

(define-public crate-gist-0.7.1 (c (n "gist") (v "0.7.1") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.3") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "1klkg4bfh47dfvgm9am3yfafqr445rdycihhk5vsf2mp46jx02nn")))

(define-public crate-gist-0.8.0 (c (n "gist") (v "0.8.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.3") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "1icq0b2b4yrzjav8m0cy7xr74w0wpp116igwf1lyvsyv568n2s6i")))

(define-public crate-gist-0.9.0 (c (n "gist") (v "0.9.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.3") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "0q62k1ym5snfxq36hpad6k7g0w42rlr6zbvsz2anyb4fzza6qy96")))

(define-public crate-gist-0.9.1 (c (n "gist") (v "0.9.1") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.7") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "1lwb3hcny5447m4fvw2hniflqndpk8qkdwqvac04ynyhmk2li0a2")))

(define-public crate-gist-0.9.2 (c (n "gist") (v "0.9.2") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.9") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "02i2sxnfpaprn773j7ilkjba0hv1ach7bwq96swrx5bzk16zdvxx")))

