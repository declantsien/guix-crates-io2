(define-module (crates-io gi st gist-i) #:use-module (crates-io))

(define-public crate-gist-i-0.1.0 (c (n "gist-i") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "~2.29") (d #t) (k 0)) (d (n "git2") (r "^0.6.10") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rand_derive") (r "^0.3") (d #t) (k 0)))) (h "0pcx8qh65p1la7csayx8kynrx7spv2ff447jk64ykb3yx48mhygh") (y #t)))

