(define-module (crates-io gi st gistit-ipc) #:use-module (crates-io))

(define-public crate-gistit-ipc-0.1.0 (c (n "gistit-ipc") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "00prphxqnrzkl3jl5nqpdifzvzyzyd661zwl10f2p3fndlh27z4j") (y #t)))

(define-public crate-gistit-ipc-0.1.1 (c (n "gistit-ipc") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mrq7hrcnwambigaj6xb40wpjad450qsbzcafnq0j3qi4gzn2xzz") (y #t)))

(define-public crate-gistit-ipc-0.2.0 (c (n "gistit-ipc") (v "0.2.0") (d (list (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "gistit-proto") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("net" "rt" "macros"))) (k 0)))) (h "17605vaam1i35qgkzinzgnw785l6n9lbb64ljvn60amqkw7av671")))

