(define-module (crates-io gi st gistcafe) #:use-module (crates-io))

(define-public crate-gistcafe-0.1.0 (c (n "gistcafe") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)))) (h "04dh1cn6mnh9cbwa2zacnnl8gsn34m5bf1f62gspyn69jcsmz7ny")))

(define-public crate-gistcafe-0.1.1 (c (n "gistcafe") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)))) (h "0g6jfhc910lwhvj0cxrajz65b0gldsk2fy874xq0zqjw1vnvpp9d")))

(define-public crate-gistcafe-0.1.2 (c (n "gistcafe") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)))) (h "07aj94yz347q713s441vcjacsm4nk9p24fg66i32l69j7q55nj25")))

