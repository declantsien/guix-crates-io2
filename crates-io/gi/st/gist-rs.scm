(define-module (crates-io gi st gist-rs) #:use-module (crates-io))

(define-public crate-gist-rs-0.1.0 (c (n "gist-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "1q84l8742vdp3c40grfidcwanracm4wv5mgfkx17bb2xicdligjd") (y #t)))

