(define-module (crates-io gi fl giflar) #:use-module (crates-io))

(define-public crate-giflar-0.0.1 (c (n "giflar") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "045a4mcrhfzgxkcis386gxvm88zgwqvj7sgm1l1lbi51q2ds11kj") (y #t)))

(define-public crate-giflar-0.0.2 (c (n "giflar") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "1izp2zz4hsnzszhl634fm8y08p4p5jzhgp7g7kl3h12lc2mz0328") (y #t)))

(define-public crate-giflar-0.0.3 (c (n "giflar") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "03n05j8m1sqaddnfcbx2gbjh2zg2vnq26m0pndpnaav40h2c1zax") (y #t)))

(define-public crate-giflar-0.0.4 (c (n "giflar") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1b77r6m5rz2kgnsa2ihbidhbq8bk24djcldd320ka8jqn6c8h0kv") (y #t)))

(define-public crate-giflar-0.0.5 (c (n "giflar") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "108v6lyxzn2h5l19i1h57klj0flphllxkvv51y40b7rykc7l4fmr") (y #t)))

(define-public crate-giflar-0.0.6 (c (n "giflar") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "00ixgq0ifhw1f72fclby5rk265r51x5wryk7i5yg9cgm1yxmz1lw")))

