(define-module (crates-io gi tx gitx) #:use-module (crates-io))

(define-public crate-gitx-0.0.1 (c (n "gitx") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustbox") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qaycm1z8drb7r9ii5gn92rbbp5s004cm4zd3w18hy7b3sjkzxdg")))

(define-public crate-gitx-0.0.2 (c (n "gitx") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustbox") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "06qi3h9vm7d5bxwhz1vg797pw9h1rkcq1b641bvfivp5vligmwng")))

(define-public crate-gitx-0.0.3 (c (n "gitx") (v "0.0.3") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustbox") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1w69f8iir3c6yl7j9l2aix7l0hl440wpcss5hvr397ka28b5j5nz")))

(define-public crate-gitx-0.0.4 (c (n "gitx") (v "0.0.4") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "rustbox") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "165q0lid46c86gdrxgxv07xy79sagr7nr2b6qlz5a0q41fh69qig")))

