(define-module (crates-io gi tt gitty) #:use-module (crates-io))

(define-public crate-gitty-0.1.0 (c (n "gitty") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)))) (h "19g3a83vpqpqavcwzr4qgxl7ff7r8byaz7bi3cagln89fqkjr2r6")))

(define-public crate-gitty-0.1.1 (c (n "gitty") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)))) (h "16sgmhhnvsii5lcc5shpgzf9p061h5qs7xv4vzm2a26l89cf27dg")))

(define-public crate-gitty-0.1.2 (c (n "gitty") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)))) (h "1rhbl0s7b37sxinjq2b3ffmzxsy285fwkw08z2b705x569nh1i5m")))

(define-public crate-gitty-0.1.3 (c (n "gitty") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)))) (h "1c73nnvyaf00zvfvbgw53bg7svdr1bl01cpzszhpl2c66n55qzpn")))

(define-public crate-gitty-0.1.4 (c (n "gitty") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)))) (h "0m1fr6ly7jlzdp7ha8hd6p1yj5v3wf8p14145q8wl9sw39hlsy6g")))

