(define-module (crates-io gi tt gitter) #:use-module (crates-io))

(define-public crate-gitter-0.1.0 (c (n "gitter") (v "0.1.0") (d (list (d (n "chrono") (r "= 0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5") (d #t) (k 0)))) (h "1fzzvzapx5v8l67w0r9m97j8yb1lbwp7fy872rvbpxg8g32cj5lh")))

(define-public crate-gitter-0.1.1 (c (n "gitter") (v "0.1.1") (d (list (d (n "chrono") (r "= 0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5") (d #t) (k 0)))) (h "0i3cj9fz8cbzar3yf3pi16h0bjh15fzqhd1fl5qp07r615gfffzl")))

(define-public crate-gitter-0.1.2 (c (n "gitter") (v "0.1.2") (d (list (d (n "chrono") (r "= 0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5") (d #t) (k 0)))) (h "04mkaxy4a7mx5w47vhpcdzy9z3k3pwl11v9qga4vy0pl218g5bpm")))

(define-public crate-gitter-0.1.3 (c (n "gitter") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5") (d #t) (k 0)))) (h "0a58vjczqaq69cvrcdbncghd8rhmsf4w8d0q1izm2b9rfkj38y8z")))

(define-public crate-gitter-0.2.0 (c (n "gitter") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5") (d #t) (k 0)))) (h "1l2779x5nfiqqh9mgydyx3axw6id5fjymvsv1a4sycbhswapils0")))

(define-public crate-gitter-0.3.0 (c (n "gitter") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5") (d #t) (k 0)))) (h "0mcgnpz05sc51980l8n9n3brhljca0akfsqrnc6xslyin7y9rw9x")))

(define-public crate-gitter-0.4.0 (c (n "gitter") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5") (d #t) (k 0)))) (h "10jbymzlrmjlzx5k16r22w42c2l10kwkwlf79f65vh12c0pvm9p3")))

