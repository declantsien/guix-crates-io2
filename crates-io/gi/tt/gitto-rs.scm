(define-module (crates-io gi tt gitto-rs) #:use-module (crates-io))

(define-public crate-gitto-rs-0.1.0 (c (n "gitto-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0j045fal12nc2za7vh3cifimii2jkvadqkx6a9wh29hc2lpzibqs")))

