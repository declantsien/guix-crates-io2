(define-module (crates-io gi tt gitt) #:use-module (crates-io))

(define-public crate-gitt-0.1.0 (c (n "gitt") (v "0.1.0") (d (list (d (n "cassowary") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)))) (h "1dlr3gcpypmddpd71zd44bjg8f740vs5bqc911067lw4gkvsn5pq")))

(define-public crate-gitt-0.2.0 (c (n "gitt") (v "0.2.0") (d (list (d (n "cassowary") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)))) (h "0l6nkj2zhk3axxsjpdl1jmw8anpmsxhqqlwhkshhi8w8fvgl30aq")))

(define-public crate-gitt-0.2.1 (c (n "gitt") (v "0.2.1") (d (list (d (n "cassowary") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)))) (h "14bkcb9hlb9gr4fypmz8vx9ly08bqwvvl2ach8icjp4z2phd54kv")))

(define-public crate-gitt-0.2.2 (c (n "gitt") (v "0.2.2") (d (list (d (n "cassowary") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "git2") (r "^0.13") (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)))) (h "113wl6aix746kmcv9xcv44an7h19k0zw2lcwyx1v3800yanbmvfr")))

