(define-module (crates-io gi tt gitter-slack) #:use-module (crates-io))

(define-public crate-gitter-slack-0.1.0 (c (n "gitter-slack") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "rustc-serialize"))) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1fyj5cha1lz3jili5zd5hgn9b47z2rkzk8l0gc3c20nlqcswaqmq")))

(define-public crate-gitter-slack-0.1.1 (c (n "gitter-slack") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "rustc-serialize"))) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1y6qjy0v11vd56cpavdd8skd4ibm6m35rdam9m2nybhsh5pcbzrh")))

