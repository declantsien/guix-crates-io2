(define-module (crates-io gi bb gibbon) #:use-module (crates-io))

(define-public crate-gibbon-0.1.0 (c (n "gibbon") (v "0.1.0") (h "0zsjxgnb84pk5v96k289w2mwpp8z98v5rxqd5px31yc6v0l61nfh")))

(define-public crate-gibbon-0.1.1 (c (n "gibbon") (v "0.1.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1f6mzzf2z4nmmdbi07vrhj2xxq5111l9cds61bw1vm82yjdpr7h1")))

(define-public crate-gibbon-0.1.2 (c (n "gibbon") (v "0.1.2") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0j0y2cq02hayvba87v0203rimjlii75r4w76yx6frhp1krvxp0ck")))

(define-public crate-gibbon-0.1.3 (c (n "gibbon") (v "0.1.3") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1v1rdqcx314jgdvn6mv231650walmi8yjmcws43q8snvrzih7c7f")))

