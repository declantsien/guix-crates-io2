(define-module (crates-io gi bb gibberish) #:use-module (crates-io))

(define-public crate-gibberish-0.1.0 (c (n "gibberish") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "msgpack_simple") (r "^1.0.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.1") (d #t) (k 0)))) (h "14wb0507bmpxba4fq4xq8dv1ij4abcksk6kia1c1s16wzr1yx1i1")))

