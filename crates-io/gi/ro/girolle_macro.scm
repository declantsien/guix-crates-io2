(define-module (crates-io gi ro girolle_macro) #:use-module (crates-io))

(define-public crate-girolle_macro-1.0.1 (c (n "girolle_macro") (v "1.0.1") (d (list (d (n "girolle") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1r77kiyf5jkkyvbk9wbvwbz3hzh2jlp4vy7paq7csl01m4mnavss")))

(define-public crate-girolle_macro-1.2.0 (c (n "girolle_macro") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0bgk4972wkfz05hp72vv312gbxnry0f28jw8vxlrqxbiaj60k1xl")))

(define-public crate-girolle_macro-1.2.1 (c (n "girolle_macro") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "03g443v0kv25ayjprxdyivzd7sq1v0p5wwpc5nsc98i7fr85p42x")))

(define-public crate-girolle_macro-1.3.0 (c (n "girolle_macro") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1g8lrxs14fjl4qadj09pkwv1mw5v5yi5np5yd42asccjkzb29vya")))

(define-public crate-girolle_macro-1.3.1 (c (n "girolle_macro") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0dr2pyjflva06pwklhmnz6gf3zkwiy43l8lrghjz6lsw20k1wgv5")))

(define-public crate-girolle_macro-1.4.0 (c (n "girolle_macro") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0jvjivr78lw04595lx05zr8lfddw0g0hcgnvcwi1kwhq2d5712bi")))

