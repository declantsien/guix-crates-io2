(define-module (crates-io gi ro giro) #:use-module (crates-io))

(define-public crate-giro-0.1.0 (c (n "giro") (v "0.1.0") (h "1xjy2zahv1ap6ss4i46g8sy9x74x3xqkwnvwpndpsq5cxydaix9z")))

(define-public crate-giro-0.1.1 (c (n "giro") (v "0.1.1") (h "1mph4d28h1vgzsczqhqv916fpisdffyiaz7702qv8aplj6qrgry0")))

(define-public crate-giro-0.1.2 (c (n "giro") (v "0.1.2") (h "0lbymkp92mywmkccp9wqk07jg4bamg0icn3gwn52qkgpxv0zgsv7")))

