(define-module (crates-io gi ro giron) #:use-module (crates-io))

(define-public crate-giron-0.1.0 (c (n "giron") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ucd") (r "^0.1.1") (d #t) (k 0)))) (h "0slf7x52zzvm0cmmcz9c2lh9zpl3d3xy7c9v4ys3a8aw8ycjcjv8")))

(define-public crate-giron-0.1.1 (c (n "giron") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ucd") (r "^0.1.1") (d #t) (k 0)))) (h "1g8pp9g8jc7is4gd1b98mp61xq7vspyzq0r8q624ygkcingj7l1g")))

(define-public crate-giron-0.1.2 (c (n "giron") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ucd") (r "^0.1.1") (d #t) (k 0)))) (h "0syad44zagsfw8vvi3bzqywl7dblxm5v1miby7xql0rpir495kd7")))

