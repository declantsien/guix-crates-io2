(define-module (crates-io gi ml gimli-hash) #:use-module (crates-io))

(define-public crate-gimli-hash-0.1.0 (c (n "gimli-hash") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "gimli-permutation") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "08imgwlmmv7fqzf1zxazyl680p0q0mdbpcdwm5wf91q73lda2hlh")))

(define-public crate-gimli-hash-0.1.1 (c (n "gimli-hash") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "gimli-permutation") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1w0zv8w5fihn87g8vh6s6rgqpcwrw7s9najh4m1hdjm9hjczsips")))

(define-public crate-gimli-hash-0.1.2 (c (n "gimli-hash") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "gimli-permutation") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1y9v7jm025xn40yqm8687g6rgnhrci6v15p7dak21h75n4w82z5y") (f (quote (("simd" "gimli-permutation/simd"))))))

(define-public crate-gimli-hash-0.1.3 (c (n "gimli-hash") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "gimli-permutation") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "0yq4w51b6x9k36ixz5hffy899h0qmk4jhmvfchpvmyayyl8jqlxq") (f (quote (("simd" "gimli-permutation/simd"))))))

(define-public crate-gimli-hash-0.1.4 (c (n "gimli-hash") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "gimli-permutation") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "0f34f3q2h3bzjac25d8l90a86j5hlcjrbfl94irw4v702z4fxvik") (f (quote (("simd" "gimli-permutation/simd"))))))

(define-public crate-gimli-hash-0.1.5 (c (n "gimli-hash") (v "0.1.5") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "gimli-permutation") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1nysqfxbnw2di59wizghgn5wms7cy0haxh5m98px4lr7k7fvyypa") (f (quote (("simd" "gimli-permutation/simd"))))))

(define-public crate-gimli-hash-0.2.0 (c (n "gimli-hash") (v "0.2.0") (d (list (d (n "gimli-permutation") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1x0cxffbv8jhblnyk0567ljc57qfvklpg3jcfpc60v30avj482p3") (f (quote (("simd" "gimli-permutation/simd"))))))

