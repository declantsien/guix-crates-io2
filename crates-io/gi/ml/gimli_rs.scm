(define-module (crates-io gi ml gimli_rs) #:use-module (crates-io))

(define-public crate-gimli_rs-0.1.0 (c (n "gimli_rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "138as15vcsysc54kwjvi986j857flmi2f0gw50k0pyzdcicb7ria")))

(define-public crate-gimli_rs-0.2.0 (c (n "gimli_rs") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0crxbl9pp994xm4vzqaqv34knk5fl65iqn4g7r3gxcqhqnq3dhim")))

