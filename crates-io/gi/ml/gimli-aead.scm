(define-module (crates-io gi ml gimli-aead) #:use-module (crates-io))

(define-public crate-gimli-aead-0.1.0 (c (n "gimli-aead") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "gimli-permutation") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "17dbl51b0dh491c822v4jin9f00qrp63rybxgffx65h88hrajvsv") (f (quote (("simd" "gimli-permutation/simd")))) (y #t)))

(define-public crate-gimli-aead-0.2.0 (c (n "gimli-aead") (v "0.2.0") (d (list (d (n "gimli-permutation") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "0fmrzxyj25a36lyhvqw70z5fh8wpw11k2gk62rzdxi8vway4s40x") (f (quote (("simd" "gimli-permutation/simd"))))))

