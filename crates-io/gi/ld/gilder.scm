(define-module (crates-io gi ld gilder) #:use-module (crates-io))

(define-public crate-gilder-0.1.0 (c (n "gilder") (v "0.1.0") (h "1sxvj0h25lr5av3519q9c2w0wvyyiwndzw6aiq3k2813py97gpbz")))

(define-public crate-gilder-0.1.1 (c (n "gilder") (v "0.1.1") (h "07dfz1ias4x2102k3plzd78jn2c2baz5jas3x9r017nf4jv7yaqp")))

(define-public crate-gilder-0.1.2 (c (n "gilder") (v "0.1.2") (h "1pfj70b59i9ldailmy668r5d6b375rhf0jni5ib7wd9203404lm4") (y #t)))

(define-public crate-gilder-0.1.3 (c (n "gilder") (v "0.1.3") (h "0zwrmrfskp80bw9hbj69l4r1fg83z47xxp9c3fjrxswr1qylmpzj")))

(define-public crate-gilder-0.1.4 (c (n "gilder") (v "0.1.4") (h "1v2xrlaraa3159qhjs3skasgbijrh4xgqz55l94ibmil7gzj9c0l")))

