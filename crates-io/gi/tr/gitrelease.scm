(define-module (crates-io gi tr gitrelease) #:use-module (crates-io))

(define-public crate-gitrelease-0.1.0 (c (n "gitrelease") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1bw61lcj9r5qx9g6iqkcn04k0aj3v7y70pldgqh75dglihlkn4i8")))

(define-public crate-gitrelease-0.1.1 (c (n "gitrelease") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04b47177fwr8b9hvlkvi8a0qgsq05hwcxgwjfavqhgxy97sy1p2c")))

