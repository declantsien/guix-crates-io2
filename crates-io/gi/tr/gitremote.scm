(define-module (crates-io gi tr gitremote) #:use-module (crates-io))

(define-public crate-GITREMOTE-0.1.0 (c (n "GITREMOTE") (v "0.1.0") (d (list (d (n "open") (r "^3.2.0") (d #t) (k 0)))) (h "0na5bwk6nm6rl4dw0gij3p2m09w0fh8v371013hnfhvrpg13xla4")))

(define-public crate-GITREMOTE-0.1.1 (c (n "GITREMOTE") (v "0.1.1") (d (list (d (n "open") (r "^3.2.0") (d #t) (k 0)))) (h "1x6ydmrwrb8raxjb09fv9lad2dk0acihbmss59wksc9x29rrb2ww")))

