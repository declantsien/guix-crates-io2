(define-module (crates-io gi tr gitree) #:use-module (crates-io))

(define-public crate-gitree-0.1.0 (c (n "gitree") (v "0.1.0") (d (list (d (n "run_script") (r "^0.6") (d #t) (k 0)))) (h "15n0rr8h23mwplk97d138qiy6rxh7b7dfwn41r13x5m8ky4qqw69")))

(define-public crate-gitree-0.2.0 (c (n "gitree") (v "0.2.0") (d (list (d (n "run_script") (r "^0.6") (d #t) (k 0)))) (h "1vx142x0v635017yr2cm4pjc9kqk0zbvmpk0kh33d27xq9261zc4")))

(define-public crate-gitree-0.3.0 (c (n "gitree") (v "0.3.0") (d (list (d (n "run_script") (r "^0.6") (d #t) (k 0)))) (h "01vyb3ynkdmfy6jsznnrjvrprjz1vi5lms4g59x16b6scfs9lm79")))

