(define-module (crates-io gi tr gitrepoman) #:use-module (crates-io))

(define-public crate-gitrepoman-0.1.0 (c (n "gitrepoman") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "github-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "gitlab") (r "^0.1008.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.20") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "094jlc97vx0hp096ca77r7r0l3nn5kxipdfhrhpg393rfbapg4j9")))

