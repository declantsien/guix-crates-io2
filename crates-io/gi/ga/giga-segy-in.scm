(define-module (crates-io gi ga giga-segy-in) #:use-module (crates-io))

(define-public crate-giga-segy-in-0.3.0 (c (n "giga-segy-in") (v "0.3.0") (d (list (d (n "encoding8") (r "^0.3") (d #t) (k 0)) (d (n "giga-segy-core") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0qz3anap82h9riscki36l7n0sri01r14cw3c6gxg5v2my7cc0pj2") (f (quote (("to_json" "giga-segy-core/to_json") ("serde" "giga-segy-core/serde") ("default"))))))

(define-public crate-giga-segy-in-0.3.1 (c (n "giga-segy-in") (v "0.3.1") (d (list (d (n "encoding8") (r "^0.3") (d #t) (k 0)) (d (n "giga-segy-core") (r "^0.3.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0ka2rdwflx8xyqa2yal9jsq5ab9mkvnhz7cl0kr7wkfphvpw2gyq") (f (quote (("to_json" "giga-segy-core/to_json") ("serde" "giga-segy-core/serde") ("default"))))))

(define-public crate-giga-segy-in-0.3.2 (c (n "giga-segy-in") (v "0.3.2") (d (list (d (n "encoding8") (r "^0.3") (d #t) (k 0)) (d (n "giga-segy-core") (r "^0.3.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1da2xds73icmlma986fys8wm56ykzh3m1b3cgvc8plbx8rrf6v6g") (f (quote (("to_json" "giga-segy-core/to_json") ("serde" "giga-segy-core/serde") ("default"))))))

(define-public crate-giga-segy-in-0.4.0 (c (n "giga-segy-in") (v "0.4.0") (d (list (d (n "encoding8") (r "^0.3") (d #t) (k 0)) (d (n "giga-segy-core") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1wjnb0620zrc4yg3wr6cmk9z8h50gm72zl5izmpavvlfv03sk07k") (f (quote (("to_json" "giga-segy-core/to_json") ("serde" "giga-segy-core/serde") ("default"))))))

