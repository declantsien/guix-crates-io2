(define-module (crates-io gi ga giga-segy-out) #:use-module (crates-io))

(define-public crate-giga-segy-out-0.3.0 (c (n "giga-segy-out") (v "0.3.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "giga-segy-core") (r "^0.3.0") (d #t) (k 0)) (d (n "giga-segy-in") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "tinyvec") (r "^1.5") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0i15fz97pf5452bp6i490gqlksysndnqvjsqpzdkmi62kqlr3cvc")))

(define-public crate-giga-segy-out-0.3.1 (c (n "giga-segy-out") (v "0.3.1") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "giga-segy-core") (r "^0.3.1") (d #t) (k 0)) (d (n "giga-segy-in") (r "^0.3.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "tinyvec") (r "^1.5") (f (quote ("alloc"))) (d #t) (k 0)))) (h "11zcw1233n9mpcbrsjnmn3pnp4k1f4i7919kfb4kdvvbnw2l40ys")))

(define-public crate-giga-segy-out-0.3.2 (c (n "giga-segy-out") (v "0.3.2") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "giga-segy-core") (r "^0.3.1") (d #t) (k 0)) (d (n "giga-segy-in") (r "^0.3.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "tinyvec") (r "^1.5") (f (quote ("alloc"))) (d #t) (k 0)))) (h "12ljbz5qnsppxr0hnadp0dldbq5ckv2mmxn721jpfq0rx20m215p")))

(define-public crate-giga-segy-out-0.4.0 (c (n "giga-segy-out") (v "0.4.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "giga-segy-core") (r "^0.4") (d #t) (k 0)) (d (n "giga-segy-in") (r "^0.4") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "tinyvec") (r "^1.5") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0j4bdbks0rz4idrc0gih0i63qi0w5mx1jdhdkmp2z7nfigrzx7zl")))

