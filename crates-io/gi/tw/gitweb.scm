(define-module (crates-io gi tw gitweb) #:use-module (crates-io))

(define-public crate-gitweb-0.1.4 (c (n "gitweb") (v "0.1.4") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)))) (h "19n2fcinlk9r6ywipjfmc7fflxaqnsrmqzfvmb8anhls17y28jcj")))

(define-public crate-gitweb-0.1.5 (c (n "gitweb") (v "0.1.5") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)))) (h "1614266c5s6g7wmnmbs0dls8yqy784byclh813bk7bqgac0v3ahy")))

(define-public crate-gitweb-0.1.6 (c (n "gitweb") (v "0.1.6") (d (list (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "01yjh2frv1dj9n041fwq02asdk08x8i01by8shp0vqplbgh4swzc")))

(define-public crate-gitweb-0.1.7 (c (n "gitweb") (v "0.1.7") (d (list (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1gh5xy18ryccq0jdx76zc9dm5mlym0w8vzb736h9zyx6lwnbs6vp")))

(define-public crate-gitweb-0.1.8 (c (n "gitweb") (v "0.1.8") (d (list (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "12925mijy109bcw6q489a6zd9rk39rzrbklz1gfr33svc7p0bn5v")))

(define-public crate-gitweb-0.1.10 (c (n "gitweb") (v "0.1.10") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07spmkl9ka3cfrf23l47nlnysm98zs4wkwcx75wafvmsrfzql0qk")))

(define-public crate-gitweb-0.1.11 (c (n "gitweb") (v "0.1.11") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0g3asg6wiqy1jqhb13vc5yxh81l4azl0qv6cglil8laljgs511j2")))

(define-public crate-gitweb-0.1.12 (c (n "gitweb") (v "0.1.12") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0lk91814w7xb5fwgpvp9mjh6yxf0jixg3i9jl0ssqylrkpl826kf")))

(define-public crate-gitweb-0.1.13 (c (n "gitweb") (v "0.1.13") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1jn1254d2il5zm03m2zfxka709qjky88zfxg1jnvkjqr1n6pmls5")))

(define-public crate-gitweb-0.2.0 (c (n "gitweb") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.15") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06bgyw6halb9ckhlq0zqfh8vl9ndxnmlpc6x4rgrwqn0nxr77bg1")))

(define-public crate-gitweb-0.2.1 (c (n "gitweb") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.16") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "073j4ip0y7fls35mp2byx8ihk4jzc4449xj0l2gd5nnnj7k9vi32")))

(define-public crate-gitweb-0.2.2 (c (n "gitweb") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.16") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qh62738ph1bxmfyvbk64673x96zsldn90p52rq0n7plarwx39dl")))

(define-public crate-gitweb-0.2.3 (c (n "gitweb") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.16") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xysqs48qffs5x59794gc9bqzjn2hz1i4963fk9gsvpnk1xqzx6k")))

(define-public crate-gitweb-0.2.4 (c (n "gitweb") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.16") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00adkmx4ig204srmjg85apjm4668fmkx62fkkmj67f0gcs1pp6aw")))

(define-public crate-gitweb-0.2.5 (c (n "gitweb") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.16") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1683hwmlbwa75y3mpgc0gqmhhw70qgiaqgkzfyl7agbwn438cccr")))

(define-public crate-gitweb-0.3.0 (c (n "gitweb") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.17") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0602w8zfwr4xb5kkmva6br6pj4lzpd0p5nrvxrzpy8vc0kc24s8k")))

(define-public crate-gitweb-0.3.1 (c (n "gitweb") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.17") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^1.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02p7yav576a1qs09dbx3dbw7anhd40l0w9s7pap96kwv51l69pv7")))

(define-public crate-gitweb-0.3.2 (c (n "gitweb") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.18") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "194gjqzm76qf47n7y15h7w2ka35pxl98q3j103ax7i3d2x6gh68v")))

(define-public crate-gitweb-0.3.3 (c (n "gitweb") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.19") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "067mb6lbz8aqx847svbrh8ahq0w9zidp25ia8af3jm205jh8zj7p")))

(define-public crate-gitweb-0.3.4 (c (n "gitweb") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22") (f (quote ("use_chrono_for_offset"))) (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ifwc4d6b1bx395h5mr9cdxbwhp5amxcwdijprwa2sd3lall21sm")))

(define-public crate-gitweb-0.3.5 (c (n "gitweb") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22") (f (quote ("use_chrono_for_offset"))) (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12d72mss075kgjggkbpz2s08nqpsfv8wmxgi352plqnsx59bham3")))

