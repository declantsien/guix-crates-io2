(define-module (crates-io gi f_ gif_parser) #:use-module (crates-io))

(define-public crate-gif_parser-0.1.0 (c (n "gif_parser") (v "0.1.0") (d (list (d (n "weezl") (r "^0.1.7") (d #t) (k 0)))) (h "02qv38k6pn5jlwa0np72cddsqph09zpi4brgjsixhfniyghvbvsy")))

(define-public crate-gif_parser-0.2.0 (c (n "gif_parser") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "weezl") (r "^0.1.7") (d #t) (k 0)))) (h "07l5cng4md1qf40dw779wi1lbjwsrkp1kpacjmcgnxz29lcldj4p")))

(define-public crate-gif_parser-0.2.1 (c (n "gif_parser") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "weezl") (r "^0.1.7") (d #t) (k 0)))) (h "1zhgpxszgr213n4dn6bs8axx1q1x692yggfw9cv6gj3vxc0p0rb7")))

