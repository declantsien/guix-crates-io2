(define-module (crates-io gi rd gird) #:use-module (crates-io))

(define-public crate-gird-0.1.0 (c (n "gird") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "070s6f1b7kiw26accp7rb4b8c3vxmx61yrnvqvcr72gbf5nyapf6")))

(define-public crate-gird-0.1.1 (c (n "gird") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "17z7yzxk8s6p7aq8zi9nvacfac8q5p3wlp9hs8gc3808abynh8zd")))

(define-public crate-gird-0.1.2 (c (n "gird") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "0kvvwpm6cqcbfiazggvdgbyc1hxk4gnjfk1nix798vrsscng6qn4")))

(define-public crate-gird-0.2.0 (c (n "gird") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "04v3gqkqfaxq7iy5ds7wfrpm2gsrvy7w831dzi7gsc472rldmrym")))

