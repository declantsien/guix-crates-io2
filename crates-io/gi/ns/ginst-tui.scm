(define-module (crates-io gi ns ginst-tui) #:use-module (crates-io))

(define-public crate-ginst-tui-0.1.0 (c (n "ginst-tui") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "libginst") (r "^0.1.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0lyh4qfnvx67xhjpdrywrp768j21ywilgkrqi4gnfngzb2558rql")))

