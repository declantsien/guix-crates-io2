(define-module (crates-io gi ns ginst) #:use-module (crates-io))

(define-public crate-ginst-0.0.2 (c (n "ginst") (v "0.0.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0viak4sj8m7w2i5x4m995a2c9cgnnbdpqaqwwrqg76738rf94ps2")))

(define-public crate-ginst-0.0.3 (c (n "ginst") (v "0.0.3") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1d4lcyy78kg1myjz7klpnf3rzh9igl5pqsqwc0vgd02mkngfw608")))

(define-public crate-ginst-0.1.0 (c (n "ginst") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1wlqyqqnb4v5gvk5h6yxavnwlxkw7qrf0vcs6c8kp8p77n99bq01")))

(define-public crate-ginst-0.1.1 (c (n "ginst") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1cka1rfz4bmd5hd6mz9ib157rsf6lwwwjiv4a5kbv2j7vdwfv4r7")))

(define-public crate-ginst-0.1.2 (c (n "ginst") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0pflnmrxmipnjx2w1r1z5him2cis2rh1lw48sb53rnkc27684m7d")))

(define-public crate-ginst-0.1.3 (c (n "ginst") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "06fldvm1z7ws2q1vmxc4cq5zmwym23dwkzn79a22k6f93mnd45p9")))

(define-public crate-ginst-0.1.4 (c (n "ginst") (v "0.1.4") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0prnv3jbx8xlm3f3vd165rzpa2qkj7fsddw6z1qidqwm0issidlj")))

(define-public crate-ginst-0.1.5 (c (n "ginst") (v "0.1.5") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "05ay90da2ak578q6pryaf17qm0k80cbirqpxnn6xyp01kxr68klf")))

(define-public crate-ginst-0.1.6 (c (n "ginst") (v "0.1.6") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1b10jqxsxqzp6957ddh5g28f0dw3j6axfw5irl10z711nglvs8gl")))

(define-public crate-ginst-0.1.8 (c (n "ginst") (v "0.1.8") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0mbdi2yd3wnfajccrrb1y68dq7hcnfrs6h95946y9wvpdvv0270l")))

(define-public crate-ginst-0.1.9 (c (n "ginst") (v "0.1.9") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1i0f864sp0cwxmnhqr1ffcq579cwimg8z112wqnlsqkvfac5lyjb")))

(define-public crate-ginst-1.0.0 (c (n "ginst") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1l7j8i16qlbv67shv55g4csb467hxddbd45lrka5s69zi6z3mspk")))

(define-public crate-ginst-1.0.1 (c (n "ginst") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libginst") (r "^0.1.0") (d #t) (k 0)))) (h "1cnip4b212gvssb2wf44nk5bij5n2lbww83hdv0nmri28f9b3ckz")))

