(define-module (crates-io gi te gitea-rs) #:use-module (crates-io))

(define-public crate-gitea-rs-1.19.0 (c (n "gitea-rs") (v "1.19.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "03vq9xhfj8k0lqwdbz6bzmv7yczpcqgb6b034ahx7mjp37nig9vx")))

