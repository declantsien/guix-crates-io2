(define-module (crates-io gi te gitea-migrate) #:use-module (crates-io))

(define-public crate-gitea-migrate-0.1.0 (c (n "gitea-migrate") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y72x90ak9jfq6zkrjkzwi5ra4xcv5wjfqas5myxxi6by7qfajzc") (y #t)))

