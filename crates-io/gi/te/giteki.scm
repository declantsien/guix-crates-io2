(define-module (crates-io gi te giteki) #:use-module (crates-io))

(define-public crate-giteki-0.1.0 (c (n "giteki") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0p02qawylwimd9ywi9zx7iy4q0si6pk7zdvg0gyffnlva9s1hsqi")))

(define-public crate-giteki-0.1.1 (c (n "giteki") (v "0.1.1") (d (list (d (n "bson") (r "^0.14") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "11x9p598m1gncxpyp7pjf8ig11f76hfd4nm1l45aryd3nj7rykm9")))

(define-public crate-giteki-0.1.2 (c (n "giteki") (v "0.1.2") (d (list (d (n "bson") (r "^0.14") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1cr5j8l5k78rc214fmd03s0wr2aa3a259b4nl876i65iwvklib34")))

