(define-module (crates-io gi te gitee) #:use-module (crates-io))

(define-public crate-gitee-0.1.0 (c (n "gitee") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "1xkqr774l2ly1ixgg2xy0j7p15hv7yvishkzj4q9dr1a1ikjwpms")))

(define-public crate-gitee-0.1.1 (c (n "gitee") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "0sdbqr2agss53skrxscfflwlhl5222fy892217gqllcdlka0gzyw")))

(define-public crate-gitee-0.1.2 (c (n "gitee") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "1rnn2z2zx61ip3im6xg8k84b73ylpi9qv3z0n8i5wsapbjh4lpix")))

