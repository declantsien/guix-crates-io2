(define-module (crates-io gi -r gi-repository) #:use-module (crates-io))

(define-public crate-gi-repository-0.1.0 (c (n "gi-repository") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.1") (d #t) (k 0) (p "gi-repository-sys")) (d (n "glib") (r "^0.16.4") (d #t) (k 0)) (d (n "glib-sys") (r "^0.16.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vf6fbkvaz4rcvga0dzamnf58r6h6ngdrn9mchcrj113p3aykydf") (f (quote (("dox" "glib/dox" "ffi/dox"))))))

