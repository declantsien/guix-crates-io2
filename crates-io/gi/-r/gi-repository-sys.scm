(define-module (crates-io gi -r gi-repository-sys) #:use-module (crates-io))

(define-public crate-gi-repository-sys-0.0.1 (c (n "gi-repository-sys") (v "0.0.1") (d (list (d (n "glib") (r "^0.16.3") (d #t) (k 0) (p "glib-sys")) (d (n "gobject") (r "^0.16.3") (d #t) (k 0) (p "gobject-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1aiwl514mr7929ykrda9fc4kmhr67xsn0sy94y64adfgdl1vbq83") (f (quote (("dox" "glib/dox" "gobject/dox"))))))

