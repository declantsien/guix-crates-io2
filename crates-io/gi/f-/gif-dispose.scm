(define-module (crates-io gi f- gif-dispose) #:use-module (crates-io))

(define-public crate-gif-dispose-1.0.0 (c (n "gif-dispose") (v "1.0.0") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "rgb") (r "^0.5.2") (d #t) (k 0)))) (h "192bb39b36ggsc5kviximif8if26ax633achp2zs3yylhpjs47lr") (y #t)))

(define-public crate-gif-dispose-1.0.1 (c (n "gif-dispose") (v "1.0.1") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "rgb") (r "^0.5.2") (d #t) (k 0)))) (h "169p3nrsxvdngxqi37xi5jichq88709w7d3xcf9l5kcyybpfdjxm") (y #t)))

(define-public crate-gif-dispose-1.0.2 (c (n "gif-dispose") (v "1.0.2") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "0h8wl4q6xmzwsypdkrspcd484wy1mql6dp5xy8458afjyl4b652c")))

(define-public crate-gif-dispose-2.0.0 (c (n "gif-dispose") (v "2.0.0") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "imgref") (r "^1.2.2") (d #t) (k 0)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "1dqyj7q5q2g9vh553b0a8z5z71rfkbxsg4gh5zknng04a7cdz1y9") (y #t)))

(define-public crate-gif-dispose-2.0.1 (c (n "gif-dispose") (v "2.0.1") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "imgref") (r "^1.2.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1xqhzqy61arqmrm51pvw50fkycidsw3nkrn282a9qygw61agbl7l")))

(define-public crate-gif-dispose-2.1.0 (c (n "gif-dispose") (v "2.1.0") (d (list (d (n "gif") (r "^0.9.2") (d #t) (k 0)) (d (n "imgref") (r "^1.2.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.4") (d #t) (k 0)))) (h "1yklnj6aq0ajh35vs6zs54aiv32djwvpm55xpachslclp3003sws")))

(define-public crate-gif-dispose-2.1.1 (c (n "gif-dispose") (v "2.1.1") (d (list (d (n "gif") (r "^0.10.0") (d #t) (k 0)) (d (n "imgref") (r "^1.3.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.9") (d #t) (k 0)))) (h "1ndxiq0qp6xl7i2bncp485qb33sazy3xri18vkc74clzm0chq3mp")))

(define-public crate-gif-dispose-2.2.0 (c (n "gif-dispose") (v "2.2.0") (d (list (d (n "gif") (r "^0.10.0") (d #t) (k 0)) (d (n "imgref") (r "^1.3.0") (d #t) (k 0)) (d (n "lodepng") (r "^2.4.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.9") (d #t) (k 0)))) (h "1m5zsqdhfxqg5r2za7h6k0pi00zmlqjm526vz130kshck8vgwzag")))

(define-public crate-gif-dispose-2.3.0 (c (n "gif-dispose") (v "2.3.0") (d (list (d (n "gif") (r "^0.10.3") (d #t) (k 0)) (d (n "imgref") (r "^1.4.1") (d #t) (k 0)) (d (n "lodepng") (r "^2.5.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8.16") (d #t) (k 0)))) (h "04rdjrq6sbcm9za8byxnh0mirzn4m3769xj2cmd2s1jsvz13l0rq")))

(define-public crate-gif-dispose-2.4.0 (c (n "gif-dispose") (v "2.4.0") (d (list (d (n "gif") (r "^0.10.3") (d #t) (k 0)) (d (n "imgref") (r "^1.5.0") (d #t) (k 0)) (d (n "lodepng") (r "^2.6.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8.17") (d #t) (k 0)))) (h "1absjv0akabfm9xb6nhkl55r495c8vgnvflpc8lcmnjjipby49px")))

(define-public crate-gif-dispose-2.4.1 (c (n "gif-dispose") (v "2.4.1") (d (list (d (n "gif") (r "^0.10.3") (d #t) (k 0)) (d (n "imgref") (r "^1.6.1") (d #t) (k 0)) (d (n "lodepng") (r "^3.0.0") (d #t) (k 2)) (d (n "rgb") (r "^0.8.20") (d #t) (k 0)))) (h "1srca1bcj2mqh1zgi5qr5mrh1l8iwa22rnydc52w2lqzqbp996c5")))

(define-public crate-gif-dispose-3.0.0 (c (n "gif-dispose") (v "3.0.0") (d (list (d (n "gif") (r "^0.11.1") (d #t) (k 0)) (d (n "imgref") (r "^1.7.0") (d #t) (k 0)) (d (n "lodepng") (r "^3.2.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "0sglg2916zlvlkky74svvzj9j45zryk6iyhls3ndxi03lc7n659l")))

(define-public crate-gif-dispose-3.0.1 (c (n "gif-dispose") (v "3.0.1") (d (list (d (n "gif") (r "^0.11.1") (d #t) (k 0)) (d (n "imgref") (r "^1.7.0") (d #t) (k 0)) (d (n "lodepng") (r "^3.2.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "0fg2h5b07k38258l9r3ysmq7ydk2i1pirl1njg99jr41jpgdnf9g")))

(define-public crate-gif-dispose-2.4.2 (c (n "gif-dispose") (v "2.4.2") (d (list (d (n "gif") (r "^0.10.3") (d #t) (k 0)) (d (n "imgref") (r "^1.7.0") (d #t) (k 0)) (d (n "lodepng") (r "^3.2.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "1lp842iinga85wdvrpj6vg4vgg4kz802fs04k87yy2q6slgibzi8")))

(define-public crate-gif-dispose-3.1.0 (c (n "gif-dispose") (v "3.1.0") (d (list (d (n "gif") (r "^0.11.1") (d #t) (k 0)) (d (n "imgref") (r "^1.7.1") (d #t) (k 0)) (d (n "lodepng") (r "^3.2.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "0d9bzxrfslhw5fd9mbda479f860wz9sgpvay44d4ggwc6w5fjj9l")))

(define-public crate-gif-dispose-3.1.1 (c (n "gif-dispose") (v "3.1.1") (d (list (d (n "gif") (r "^0.11.1") (d #t) (k 0)) (d (n "imgref") (r "^1.7.1") (d #t) (k 0)) (d (n "lodepng") (r "^3.4.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "1my1jmnsshv71nysgbfs3g223q6sp63rs1hjrf0l2f7lx13h6gi4")))

(define-public crate-gif-dispose-3.2.0 (c (n "gif-dispose") (v "3.2.0") (d (list (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "imgref") (r "^1.9.4") (d #t) (k 0)) (d (n "lodepng") (r "^3.7.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)))) (h "0qp888jds4v1fp83c285nchxfxvqr45xkhr8an24dgyaxkqa0g67") (y #t)))

(define-public crate-gif-dispose-4.0.0 (c (n "gif-dispose") (v "4.0.0") (d (list (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "imgref") (r "^1.9.4") (d #t) (k 0)) (d (n "lodepng") (r "^3.7.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)))) (h "0l6bc0cbmg7203clmi8gnycd7rlfl68nlz6bfyzwpq2rpvszs3d4")))

(define-public crate-gif-dispose-4.0.1 (c (n "gif-dispose") (v "4.0.1") (d (list (d (n "gif") (r "^0.12.0") (f (quote ("std"))) (k 0)) (d (n "imgref") (r "^1.9.4") (d #t) (k 0)) (d (n "lodepng") (r "^3.7.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)))) (h "1my32v4sx5nrqgyp0m49gcvfgrp7cfxhl4vdlyijbjh39bhglyil")))

(define-public crate-gif-dispose-5.0.0-beta.1 (c (n "gif-dispose") (v "5.0.0-beta.1") (d (list (d (n "gif") (r "^0.13.0-beta.1") (f (quote ("std"))) (k 0)) (d (n "imgref") (r "^1.9.4") (d #t) (k 0)) (d (n "lodepng") (r "^3.7.2") (d #t) (k 2)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)))) (h "1j7c6rd5g5an42jrdcgiz5yzjnpicz6xy0j1yx82dpirwirbi88y")))

(define-public crate-gif-dispose-5.0.0-beta.2 (c (n "gif-dispose") (v "5.0.0-beta.2") (d (list (d (n "gif") (r "^0.13") (f (quote ("std"))) (k 0)) (d (n "imgref") (r "^1.9.4") (d #t) (k 0)) (d (n "lodepng") (r "^3.10.1") (d #t) (k 2)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)))) (h "1vdr8pmvq2ji7ny1dq2s3id191qrzx993qv0q82zfpz108w0mlmh") (r "1.60")))

(define-public crate-gif-dispose-5.0.0 (c (n "gif-dispose") (v "5.0.0") (d (list (d (n "gif") (r "^0.13") (f (quote ("std"))) (k 0)) (d (n "imgref") (r "^1.9.4") (d #t) (k 0)) (d (n "lodepng") (r "^3.10.1") (d #t) (k 2)) (d (n "rgb") (r "^0.8.34") (d #t) (k 0)))) (h "0l9dijhzplz3qzyg5g5wds252cc2kpvnsn76sczp4k2vk2jha43q") (r "1.60")))

