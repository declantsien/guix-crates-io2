(define-module (crates-io gi tl gitlab-version-bot) #:use-module (crates-io))

(define-public crate-gitlab-version-bot-0.1.0 (c (n "gitlab-version-bot") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clogger") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1qxidp455l2vl0wzl120m01kkpyczxvb86p8cks7472m88d08gyz")))

