(define-module (crates-io gi tl gitlab-clone-group) #:use-module (crates-io))

(define-public crate-gitlab-clone-group-0.1.0 (c (n "gitlab-clone-group") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)))) (h "1ir17xwl2lnx0qwi26j5a05pmqggf80ry2v1qxhv3mm68jxkqkps")))

