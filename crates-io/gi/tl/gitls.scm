(define-module (crates-io gi tl gitls) #:use-module (crates-io))

(define-public crate-gitls-0.1.0 (c (n "gitls") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "lsp-server") (r "^0.7.2") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0pazidvxg7607s5id744dvf44vm1j3jrvy44ij53rim05wdg1api")))

