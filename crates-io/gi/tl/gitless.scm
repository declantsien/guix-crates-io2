(define-module (crates-io gi tl gitless) #:use-module (crates-io))

(define-public crate-gitless-1.0.0 (c (n "gitless") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1ai7p1vrf4vpgjz26mxdlxiy95jbk88x4qi0ym2v83g77l23i67j")))

