(define-module (crates-io gi tl gitlab_ci_parser) #:use-module (crates-io))

(define-public crate-gitlab_ci_parser-0.0.3 (c (n "gitlab_ci_parser") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (k 0)) (d (n "serde_yaml") (r "^0.8") (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "yaml-merge-keys") (r "^0.4") (f (quote ("serde_yaml"))) (d #t) (k 0)))) (h "1rb5xs0asiyxwk2pkqv022j25sa3v834v64z4gm8wayfffmgnd53")))

(define-public crate-gitlab_ci_parser-0.0.4 (c (n "gitlab_ci_parser") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (k 0)) (d (n "serde_yaml") (r "^0.8") (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "yaml-merge-keys") (r "^0.4") (f (quote ("serde_yaml"))) (d #t) (k 0)))) (h "1hj5xjrqsr7rdy736zbzfm3ycj72xdif0izpca1kh2nzys2msj21")))

(define-public crate-gitlab_ci_parser-0.0.5 (c (n "gitlab_ci_parser") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (k 0)) (d (n "serde_yaml") (r "^0.8") (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "yaml-merge-keys") (r "^0.4") (f (quote ("serde_yaml"))) (d #t) (k 0)))) (h "0w0gv8fgwjlq3xx3spk9xm5a6bkg3c5bq0ax93nwrgf580pnm4qn")))

