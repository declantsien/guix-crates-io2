(define-module (crates-io gi tl gitlab_tagger) #:use-module (crates-io))

(define-public crate-gitlab_tagger-0.1.1 (c (n "gitlab_tagger") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07krmx9ziggyb4i1vbi7mpd3z7ma8zxhc6hbq01ns3cg4s6q7h1z")))

(define-public crate-gitlab_tagger-0.1.2 (c (n "gitlab_tagger") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ar6qrriiw4bk9idid5n0yjrxn5miig1kq79h3z4i1pcv5zwahc7")))

(define-public crate-gitlab_tagger-0.1.3 (c (n "gitlab_tagger") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cfxf565kfaql1i2lqczib82msgqikqi8bq5lip98d0ggn2dp577")))

