(define-module (crates-io gi ft gift-of-tongues) #:use-module (crates-io))

(define-public crate-gift-of-tongues-0.1.0 (c (n "gift-of-tongues") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jgcnk4rjiqdbbyii8vy3g6irmq3sn90v9r5dd9l7g18ldi7r4ab")))

(define-public crate-gift-of-tongues-0.1.1 (c (n "gift-of-tongues") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04p3dcc92yb27dwnych718sarcz971s2gmjkcvp66ryad0ig61j6")))

(define-public crate-gift-of-tongues-0.2.0 (c (n "gift-of-tongues") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xllbgwxpr0jb0scq21n97457bixi187ggy47xjvnph2fmsm2zci")))

(define-public crate-gift-of-tongues-0.2.1 (c (n "gift-of-tongues") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cc8vglds7x0dd72wm5ci20f7i07xzkwc4caz76kr7bxq4xaf777")))

(define-public crate-gift-of-tongues-0.2.2 (c (n "gift-of-tongues") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06q9i8r9jgciw2wlm7bn3ni4haxixdgj2pji2mivx5305dfvf506")))

