(define-module (crates-io gi ft giftwrap) #:use-module (crates-io))

(define-public crate-giftwrap-0.1.0 (c (n "giftwrap") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("extra-traits" "parsing" "printing" "derive"))) (d #t) (k 0)))) (h "1xcb8ydq1frg6pi0vnxgzzymbv7kdmnz0wnbym38rf1zkn7i5qfr")))

(define-public crate-giftwrap-0.2.0 (c (n "giftwrap") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("extra-traits" "parsing" "printing" "derive"))) (d #t) (k 0)))) (h "1q747s9bxrb45v1qxz81laixwac9pm9wyiw19lr8ndxj6pbswwgc")))

(define-public crate-giftwrap-0.3.0 (c (n "giftwrap") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("extra-traits" "parsing" "printing" "derive"))) (d #t) (k 0)))) (h "0vzjyvhis5x968vcksw3smhqshgmcv33zasi7497pc6k87c1l5sr")))

(define-public crate-giftwrap-0.3.2 (c (n "giftwrap") (v "0.3.2") (d (list (d (n "harled") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("extra-traits" "parsing" "printing" "derive"))) (d #t) (k 0)))) (h "0b5yc03b84chiz5l6a8ib8p2rp7vrrgym10ial2sagqaf36nx2g2") (y #t)))

(define-public crate-giftwrap-0.4.0 (c (n "giftwrap") (v "0.4.0") (d (list (d (n "harled") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("extra-traits" "parsing" "printing" "derive"))) (d #t) (k 0)))) (h "0y6cx8hvz3bviwrpsd29cz4k0afxz88dynfsaj5a946fkxh5wph9")))

(define-public crate-giftwrap-0.5.0 (c (n "giftwrap") (v "0.5.0") (d (list (d (n "harled") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "printing" "derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0w08n6b6hwfwwny5180bbgzc48pziqq2d083v9ndy4nx090j4248")))

