(define-module (crates-io gi nk ginko) #:use-module (crates-io))

(define-public crate-ginko-0.0.1 (c (n "ginko") (v "0.0.1") (d (list (d (n "assert_unordered") (r "^0.3.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1d784kw4fwrm48idg5m09116nh5nmar3qdfhiznax5jmw3cihbam")))

(define-public crate-ginko-0.0.2 (c (n "ginko") (v "0.0.2") (d (list (d (n "assert_unordered") (r "^0.3.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0giy9fcwy2iqsknyd3sk3r1bjl3aky943896hmnmbsn47lzcvbxj")))

(define-public crate-ginko-0.0.3 (c (n "ginko") (v "0.0.3") (d (list (d (n "assert_unordered") (r "^0.3.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "091hdc96y2h1j2pfyf8sy7vqjy12k9lhdz9x9s21a8l4dhaqcxgr")))

(define-public crate-ginko-0.0.4 (c (n "ginko") (v "0.0.4") (d (list (d (n "assert_unordered") (r "^0.3.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1pzc55vd2kz65dcl93vaacpkmmaagnl0zf3bn30lmishwb4fry7g")))

(define-public crate-ginko-0.0.5 (c (n "ginko") (v "0.0.5") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 0)) (d (n "assert_unordered") (r "^0.3.5") (d #t) (k 2)) (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1hxcsm45gyg9plmnn4fakbg3jr75p41p6nlq3iw32rpgz63d80fy")))

