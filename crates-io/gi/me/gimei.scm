(define-module (crates-io gi me gimei) #:use-module (crates-io))

(define-public crate-gimei-0.1.0 (c (n "gimei") (v "0.1.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 2)) (d (n "regex_macros") (r "^0.1.17") (d #t) (k 2)) (d (n "yaml-rust") (r "*") (d #t) (k 0)))) (h "09ikcb2ncr7wchyvasrf32fcfhjn4a6pbdh2iwnxp9jafhgyrgkj")))

(define-public crate-gimei-0.1.1 (c (n "gimei") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1r6wj6mz9asjd4blh8drhwhna6kajzixvf1128037zbqx22xcp18")))

(define-public crate-gimei-0.2.0 (c (n "gimei") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "03xhssy7x1xis3zkwc18alc320mrb67k8h0myrj9kcxyamp85dsl")))

