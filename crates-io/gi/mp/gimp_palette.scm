(define-module (crates-io gi mp gimp_palette) #:use-module (crates-io))

(define-public crate-gimp_palette-0.1.0 (c (n "gimp_palette") (v "0.1.0") (h "1crgzsarrdjwp2sk3lx2vwsgm9b53v1024jsbpkk29h49xksz946")))

(define-public crate-gimp_palette-0.1.1 (c (n "gimp_palette") (v "0.1.1") (h "1nndfq4rw6wq4fmw0jsnzbb43i2pvyv72whgd97n9bd2v19qvaz2")))

