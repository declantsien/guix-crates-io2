(define-module (crates-io gi ve give-me-some-fibonacci) #:use-module (crates-io))

(define-public crate-give-me-some-fibonacci-0.1.0 (c (n "give-me-some-fibonacci") (v "0.1.0") (h "06py3w26w5qhl4f1828v45qxb5cxz3ypi6644a78ryv3n7fc1y9c")))

(define-public crate-give-me-some-fibonacci-0.1.1 (c (n "give-me-some-fibonacci") (v "0.1.1") (h "0rqcvb8kd835cjm89h5wiis4s947vda2k1896jrk8n4npaa8ax3k")))

