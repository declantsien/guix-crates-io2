(define-module (crates-io gi ve give_a_sheet) #:use-module (crates-io))

(define-public crate-give_a_sheet-0.1.0 (c (n "give_a_sheet") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.2.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "18jyyjgpfibm0mn34svzlp6r5gwqvqv2vwla1rnqx7a7vva8nsvl") (y #t)))

(define-public crate-give_a_sheet-0.1.1 (c (n "give_a_sheet") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.2.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0rvq7aw3yjx4qjq1aw2l3f7aplvgg5wyfln3yipd5v2bzw1wjf4m")))

(define-public crate-give_a_sheet-0.1.2 (c (n "give_a_sheet") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.2.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "012ca4kffba2khkgc5jisbhrc4hpr9iv6r0w7m5a210kw4pdd147")))

