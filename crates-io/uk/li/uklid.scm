(define-module (crates-io uk li uklid) #:use-module (crates-io))

(define-public crate-uklid-0.1.0 (c (n "uklid") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "human_bytes") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1jfcpb1n8dmfvc5wkr1nahxv8dkbhfiyjz4wf4lkmybbhaicn1r9")))

(define-public crate-uklid-0.1.1 (c (n "uklid") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "human_bytes") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1qn8ga0na3jwlq445ndl77q2z4b2bf73sgg2dwrcxmflq3bkjwms")))

(define-public crate-uklid-0.2.0 (c (n "uklid") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "expectrl") (r "^0.3.0") (d #t) (k 0)) (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "human_bytes") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1zr8gihja3wkph9nasfyqn3bm9jn6fgd77xijhxggqnqjqmiiydi")))

(define-public crate-uklid-0.3.0 (c (n "uklid") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "expectrl") (r "^0.3.0") (d #t) (k 0)) (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "human_bytes") (r "^0.3.1") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "spinners") (r "^4.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "078r5pqrshzpq0dsvfvxkvjldvq6fcbscaiksvlrrrc1bji5xaii")))

