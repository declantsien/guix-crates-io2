(define-module (crates-io uk ey ukeygen) #:use-module (crates-io))

(define-public crate-ukeygen-0.1.1 (c (n "ukeygen") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17r5hw8673d97p11vnjkdknfv3mqc3hj3n4j7y3h25v6k9bfc56p")))

(define-public crate-ukeygen-0.1.2 (c (n "ukeygen") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0x2j8av5fy7xk4d5zcw4v2hxrhmxlpsyr5ymbry4xbg8ln42wa9x")))

