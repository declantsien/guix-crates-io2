(define-module (crates-io uk an ukanren) #:use-module (crates-io))

(define-public crate-ukanren-0.0.1 (c (n "ukanren") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rpds") (r "^0.9.0") (d #t) (k 0)))) (h "1zz0m9r79vciyjawakshdgfww0c0m9gyfnps9bfj5hj5klb3bxvc")))

(define-public crate-ukanren-0.0.2 (c (n "ukanren") (v "0.0.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rpds") (r "^0.9.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "1mlb46x73qdmc87a4jy3lgif7w43hwfc4sl3dykcymv4m6j0vgl3")))

(define-public crate-ukanren-0.0.3 (c (n "ukanren") (v "0.0.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rpds") (r "^0.9.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "1v0yl5q095fs2vaylbpgwsmpb9ks5ii5nnskh44mgdb2vi7y3wdx")))

(define-public crate-ukanren-0.0.4 (c (n "ukanren") (v "0.0.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rpds") (r "^0.9.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "1ffz5h1qva6hfd3x62ndh8afprnzcim6x031llzn57fwfrapjlps")))

(define-public crate-ukanren-0.0.5 (c (n "ukanren") (v "0.0.5") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rpds") (r "^0.9.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "1yqdnbs0fxyvs527q7mqym8a1g5c3wjn3nqjbq6xv3jf4i41wlxd")))

