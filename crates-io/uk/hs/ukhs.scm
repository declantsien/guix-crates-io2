(define-module (crates-io uk hs ukhs) #:use-module (crates-io))

(define-public crate-ukhs-0.3.3 (c (n "ukhs") (v "0.3.3") (d (list (d (n "bbhash") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "nthash") (r "^0.4.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1xz4zlsmcc80vpxqrr9dsdp9cfkl76baxq1q6kvy0zqh0zhglmyv")))

(define-public crate-ukhs-0.3.4 (c (n "ukhs") (v "0.3.4") (d (list (d (n "bbhash") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "nthash") (r "^0.4.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1gvfxd56iw83grfd62bbmxz5g1lkw8sc1sslpympb3in3zfd4gvq")))

