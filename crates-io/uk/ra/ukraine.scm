(define-module (crates-io uk ra ukraine) #:use-module (crates-io))

(define-public crate-ukraine-1.0.0 (c (n "ukraine") (v "1.0.0") (h "0ng9h7kb87rfgfvsv8z2s6alm1r1309bpvcn6dzbx0h1qq7z6l67")))

(define-public crate-ukraine-1.0.1 (c (n "ukraine") (v "1.0.1") (h "1xwy05pw281przmq3kydc74j0njsfzn5jhrwkcl4i2yibmy7dmiz")))

(define-public crate-ukraine-1.0.2 (c (n "ukraine") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1wvk9rl0bxl46w2kd30b98618n3f3ci978xfg9j2bgppjyg6q4mi") (f (quote (("lozynsky") ("default" "lozynsky"))))))

