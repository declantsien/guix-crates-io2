(define-module (crates-io uk ha ukhasnet-parser) #:use-module (crates-io))

(define-public crate-ukhasnet-parser-0.1.0 (c (n "ukhasnet-parser") (v "0.1.0") (d (list (d (n "nom") (r "^1.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0y6c9p5wnmzn77h7h2gnwc1vsgbksadvm95wafa95lkjxwxfhlws")))

(define-public crate-ukhasnet-parser-0.1.1 (c (n "ukhasnet-parser") (v "0.1.1") (d (list (d (n "nom") (r "^1.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0y77x8pwsmkzr1knjj4ad8khsy9n8n0f4y04wzsyz8mwydlmszjj")))

(define-public crate-ukhasnet-parser-0.1.2 (c (n "ukhasnet-parser") (v "0.1.2") (d (list (d (n "nom") (r "^1.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cs0s82b05lhdc6dq4hhidym8kalc6g260bld6na8k5wvz2riav8")))

(define-public crate-ukhasnet-parser-0.2.0 (c (n "ukhasnet-parser") (v "0.2.0") (d (list (d (n "nom") (r "^1.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "07nkv3r3bz3243ab5wa5z34sxqcgkb65nsfqncqrpjpq7x2yfm5y")))

(define-public crate-ukhasnet-parser-0.2.1 (c (n "ukhasnet-parser") (v "0.2.1") (d (list (d (n "nom") (r "^1.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "11p2vs1vsayd6j4k4ss78p6syv88l542gaij6d28njpjwyzv65hw")))

(define-public crate-ukhasnet-parser-0.2.2 (c (n "ukhasnet-parser") (v "0.2.2") (d (list (d (n "nom") (r "^1.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0nhln0wdx11n6lriarvqwba8aikqp4fc73c9c4f18rfrjq31xmz8")))

(define-public crate-ukhasnet-parser-0.2.4 (c (n "ukhasnet-parser") (v "0.2.4") (d (list (d (n "nom") (r "^1.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0mpvlscvvfkayc6nnyi2hfywvc3q1hjq6aciazyn8rqz9w18a7ad")))

(define-public crate-ukhasnet-parser-0.4.0 (c (n "ukhasnet-parser") (v "0.4.0") (d (list (d (n "pest") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1q5wm8r5ir2bwpgmkb0zjhyzbwfln7kik5ygj3a19vplj7chs9hy")))

