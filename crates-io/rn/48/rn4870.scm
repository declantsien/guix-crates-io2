(define-module (crates-io rn #{48}# rn4870) #:use-module (crates-io))

(define-public crate-rn4870-0.1.0 (c (n "rn4870") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1jzvfh20bpazrkhflhyg2dhra8887lsxlqsya5r748dws2gys8yy")))

(define-public crate-rn4870-0.2.0 (c (n "rn4870") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1l5vwcy5wg2p6ascjm24lhvxzjphlv236pyf33z187x9kcsfkcwn")))

(define-public crate-rn4870-0.2.1 (c (n "rn4870") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1llimazwsjbqq47bfixa34cwfsgxwpj84vcysx38bm6a1ij7zfcq")))

(define-public crate-rn4870-0.2.2 (c (n "rn4870") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0ig65h01vqzbwy3j0b0n79iwwz7dm7dsq97by27lhairhkj2qxkl")))

(define-public crate-rn4870-0.3.0 (c (n "rn4870") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0qfhhb2viqlw0yxlm1rw62hds6a6gg00b2mrrjicpl3lgmnr3ypj")))

