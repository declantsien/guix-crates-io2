(define-module (crates-io rn g_ rng_animate) #:use-module (crates-io))

(define-public crate-rng_animate-0.1.0 (c (n "rng_animate") (v "0.1.0") (d (list (d (n "crossterm") (r ">=0.18.2, <0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r ">=3.1.7, <4.0.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "term_size") (r ">=0.3.2, <0.4.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)))) (h "1ghj8zj1znwf9x4adxfwagrhxk46pb5gjdjsnv9d6zwj70bv0dwk")))

(define-public crate-rng_animate-9000.1.0 (c (n "rng_animate") (v "9000.1.0") (d (list (d (n "crossterm") (r ">=0.18.2, <0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r ">=3.1.7, <4.0.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "term_size") (r ">=0.3.2, <0.4.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)))) (h "11qymg3qqh5b4a2d4042hy2s0cmlb0m2fw9kgsq9arrlx1bzh82x")))

