(define-module (crates-io rn g_ rng_trait) #:use-module (crates-io))

(define-public crate-rng_trait-0.1.0 (c (n "rng_trait") (v "0.1.0") (h "1r630kpjcgy9fg3ivv0z0k9vdc4x3mr1v25zi5lzr4cf130y3vh8")))

(define-public crate-rng_trait-0.1.1 (c (n "rng_trait") (v "0.1.1") (h "1n46fq2pbz7q0ha2psqb01kazr4q6y4dh9wpijw024rwh45bpz16")))

