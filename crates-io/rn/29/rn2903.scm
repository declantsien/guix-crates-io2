(define-module (crates-io rn #{29}# rn2903) #:use-module (crates-io))

(define-public crate-rn2903-0.1.0 (c (n "rn2903") (v "0.1.0") (d (list (d (n "quick-error") (r "^1") (d #t) (k 0)) (d (n "serialport") (r "^3.3") (d #t) (k 0)))) (h "0hkqf54gr5wngvqz8qqc4gwhzpxraisc75rajz1nlv0ji9x199kx")))

(define-public crate-rn2903-0.2.0 (c (n "rn2903") (v "0.2.0") (d (list (d (n "quick-error") (r "^1") (d #t) (k 0)) (d (n "serialport") (r "^3.3") (d #t) (k 0)))) (h "08j6dwvy6x40vzdw2k5madhy74lav768dsrnz05gdaamnk2dakky")))

