(define-module (crates-io rn jw rnjw) #:use-module (crates-io))

(define-public crate-Rnjw-0.1.1 (c (n "Rnjw") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "16gq1fi60bckcrlgvwx6qr435iwr8w85pfjqfnipag0zhfskbq22")))

(define-public crate-Rnjw-0.1.2 (c (n "Rnjw") (v "0.1.2") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0vbfnrcpxsi0lj65ahbigf86bggxiq1vb4vrlq586dqk4vziqw74")))

(define-public crate-Rnjw-0.1.3 (c (n "Rnjw") (v "0.1.3") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1iqzlnncd4hhfx8ii183bzr8rz74pc7v21hqm89nv29d2mp97k91")))

(define-public crate-Rnjw-0.1.6 (c (n "Rnjw") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1a8x2yc6kr1scfmxsbvc7ps9bf7xbi4a1wcawh7hi8w2gx3p6xd1")))

(define-public crate-Rnjw-0.2.1 (c (n "Rnjw") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.13.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "jsonwebtoken") (r "^9.2.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0s9frgy9hhgzzdbcgvh7jj6n30r1rhc7yv99pysw8k9x8mhc1prr")))

(define-public crate-Rnjw-0.2.3 (c (n "Rnjw") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.13.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "jsonwebtoken") (r "^9.2.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "17s2ylcn4lz6rw1zxm66a9yrkmrplkyckl9w42352svnaxnpk70p")))

(define-public crate-Rnjw-0.2.4 (c (n "Rnjw") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bb8-redis") (r "^0.13.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "jsonwebtoken") (r "^9.2.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "qzsaa") (r "^0.1.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0by8kiiyhg7np7jr23bicdvwvxzzvv44yyrhabh3ri71fw6b0hfz")))

