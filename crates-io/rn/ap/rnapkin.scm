(define-module (crates-io rn ap rnapkin) #:use-module (crates-io))

(define-public crate-rnapkin-0.1.0 (c (n "rnapkin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1i6acsysipr9jgjw0w2q69gx1v5lq19rc3db30i2zrkq91zzpl6q")))

(define-public crate-rnapkin-0.1.1 (c (n "rnapkin") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "0rl803fq6w2p3rbqgwjfld5dp5wpi7qn1r94974xssck8nixk3wg")))

(define-public crate-rnapkin-0.2.0 (c (n "rnapkin") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "0jvp4nxc2gn895ii7x60c481s82yjzdw8sy6h6fr1w2cdgnhbvk2")))

(define-public crate-rnapkin-0.3.0 (c (n "rnapkin") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1rjg7fwvxl18cvl0xmvcrrqk34qxcyd7sj90nf68n0r433dp5a45")))

(define-public crate-rnapkin-0.3.1 (c (n "rnapkin") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "0xz2di1l1lwdy11dnqj50xxf8ckkpajsj2fbym266ndil1saxl6j")))

(define-public crate-rnapkin-0.3.2 (c (n "rnapkin") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "0vmck4i8j5y293q5jizjy88dj267pv3ssgi8qhg4lqb4wi47ajcy")))

(define-public crate-rnapkin-0.3.3 (c (n "rnapkin") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "0dfl4bafnp6jy5r3k3vhgzzadn6s2ddz3ffq8ihl7v7g60ginxfx")))

(define-public crate-rnapkin-0.3.4 (c (n "rnapkin") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "06zy09x0lzkyqz3lfm0cwf7gna0r73l28bf0327aa74id49s92pd")))

(define-public crate-rnapkin-0.3.5 (c (n "rnapkin") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1q3d9jh4i7hmhlfync8drkgbzmwrf32hf2w8qvfjddk811nqv5gr")))

(define-public crate-rnapkin-0.3.6 (c (n "rnapkin") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1rvj9lmis1r2sxr27i3fbr8h98s59afqy2r6kpl59wsyjxhzgkmv")))

(define-public crate-rnapkin-0.3.7 (c (n "rnapkin") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1x6r4lmbzgdhkcbkn55nbjdkr87n9rp90591kkfpld3wzfyp43h3")))

(define-public crate-rnapkin-0.3.8 (c (n "rnapkin") (v "0.3.8") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1njsi5ins9yfzhzyx5c4yvg9vvgw4x12h71k8rxa3y8k0ygv88yi")))

(define-public crate-rnapkin-0.3.9 (c (n "rnapkin") (v "0.3.9") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "05qgs3pasdx8mv3c3f8kjq71kgqfnik6lk932sdyvk71jw0nk5a4")))

