(define-module (crates-io rn ix rnix-hashes) #:use-module (crates-io))

(define-public crate-rnix-hashes-0.1.0 (c (n "rnix-hashes") (v "0.1.0") (d (list (d (n "base16") (r "~0.2.1") (d #t) (k 0)) (d (n "base64") (r "~0.12.3") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0l1n05h4d3qscdga3m0ksnckmdz9kf8wabfcai1l4j8gcfdndzwz")))

(define-public crate-rnix-hashes-0.2.0 (c (n "rnix-hashes") (v "0.2.0") (d (list (d (n "base16") (r "~0.2.1") (d #t) (k 0)) (d (n "base64") (r "~0.12.3") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "unindent") (r "~0.1.6") (d #t) (k 0)))) (h "0pg9mgj1rb56pslsllypjvynf8dqfda7jfs6ikcbbd7gfyy6qpf9")))

