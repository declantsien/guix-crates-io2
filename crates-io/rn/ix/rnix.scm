(define-module (crates-io rn ix rnix) #:use-module (crates-io))

(define-public crate-rnix-0.1.0 (c (n "rnix") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1lnip39wnk5p6vph05b94pcr53fln9d7sli4gah0x23l312zzgz9")))

(define-public crate-rnix-0.2.0 (c (n "rnix") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1h5vm8b80mllnrcin87zjgizg0zvw42g8pfrpppm1nm7d1jjllv7")))

(define-public crate-rnix-0.3.0 (c (n "rnix") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "174pbaw2wd5qs50szapxvv2295y9msrwcycdikrmnzidb2w3ryc7") (f (quote (("default" "smol_str"))))))

(define-public crate-rnix-0.4.0 (c (n "rnix") (v "0.4.0") (d (list (d (n "arenatree") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "051xjfif1pc3jcmr8h2b6bfxw5c82sa5yqdrvf87fsp5gr24yvq9") (f (quote (("default" "smol_str"))))))

(define-public crate-rnix-0.4.1 (c (n "rnix") (v "0.4.1") (d (list (d (n "arenatree") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0v1kgifhzvi4d7hb3bn8wi86g3ryk92n7p2sd9hkkp96yhmssq0i") (f (quote (("default" "smol_str"))))))

(define-public crate-rnix-0.4.2 (c (n "rnix") (v "0.4.2") (d (list (d (n "arenatree") (r "^0.1.3") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1wxnjfkp0y6yckfl41jc509h5nx6vj1lhnw8zdyh09ik0rx8hddf") (f (quote (("default" "smol_str"))))))

(define-public crate-rnix-0.5.0 (c (n "rnix") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rowan") (r "^0.1.3") (d #t) (k 0)))) (h "0qz0hhfcxy8a1wa1zh0b2y5g4cidq5vcn4bzmvaigh7cd4hjy8bi")))

(define-public crate-rnix-0.5.1 (c (n "rnix") (v "0.5.1") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rowan") (r "^0.3.2") (d #t) (k 0)))) (h "0zx4ww8fc2lkjx53yk2jrcilbgbfqz1q814r4j27vm49dk408d5q") (y #t)))

(define-public crate-rnix-0.5.2 (c (n "rnix") (v "0.5.2") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rowan") (r "^0.3.3") (d #t) (k 0)))) (h "1cdyk351n341xjxzwkz00ncalncj1jjn8zyq0y3dcqhyi3fv6knz")))

(define-public crate-rnix-0.6.0 (c (n "rnix") (v "0.6.0") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "rowan") (r "^0.6.0") (d #t) (k 0)))) (h "1fzrnlc7q3kj5fpl1knslmj4f0dcphfs7nchnii4pbxd43s9vx75")))

(define-public crate-rnix-0.7.0 (c (n "rnix") (v "0.7.0") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.6.2") (d #t) (k 0)))) (h "0rjs9y0zl2x58v1ip303q1slxlf1wvcickpz2myvyq5sk06hwh5a")))

(define-public crate-rnix-0.7.1 (c (n "rnix") (v "0.7.1") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.6.2") (d #t) (k 0)))) (h "0b00626jk69pdlg22wxhl2kprik0zwnigvabgba2nmafn8b6h7db")))

(define-public crate-rnix-0.7.2 (c (n "rnix") (v "0.7.2") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.6.2") (d #t) (k 0)))) (h "05271szjg2zqb0gybbqch370jibn5haihxxkxphfcfrq1yfh74i1")))

(define-public crate-rnix-0.7.3 (c (n "rnix") (v "0.7.3") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.9.0") (d #t) (k 0)))) (h "0ybbqchz52wfp7mdyhi1lxcqsmjpidzds39q9cfwfx6riimy2ihf") (y #t)))

(define-public crate-rnix-0.8.0 (c (n "rnix") (v "0.8.0") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.9.0") (d #t) (k 0)))) (h "0qv8dis9kkprnm811f2xivig1n3mb3sds5j3z9ig9fz52k3s9gnb")))

(define-public crate-rnix-0.8.1 (c (n "rnix") (v "0.8.1") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.9.0") (d #t) (k 0)))) (h "0ysrl4scz88k4xx7g7mk1b8a2ilzk4idswv4zkdlg96v1rgn96qa")))

(define-public crate-rnix-0.9.0 (c (n "rnix") (v "0.9.0") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.12.5") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.17") (d #t) (k 0)))) (h "1pddh205876286h1fhcf4ci7xbv0hr2pdw0x2h34cdd00ypzhdqv")))

(define-public crate-rnix-0.9.1 (c (n "rnix") (v "0.9.1") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.12.5") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.17") (d #t) (k 0)))) (h "0dvapfpby8j2s3j9nyf9fp508qi6j3r2v88hdkcrdi2qiysfqjr9")))

(define-public crate-rnix-0.10.0 (c (n "rnix") (v "0.10.0") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.12.5") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.17") (d #t) (k 0)))) (h "1qfkkzn6zka9p9d9ai8aijm9f0p7nxc72hgp8qdyc8l8lv45kwk1")))

(define-public crate-rnix-0.10.1 (c (n "rnix") (v "0.10.1") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.12.5") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.17") (d #t) (k 0)))) (h "1jdjp6y6ns0zw8s2g6kszygxdmwd5mpc9g97f877nzlgisn5wp06")))

(define-public crate-rnix-0.10.2 (c (n "rnix") (v "0.10.2") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rowan") (r "^0.12.5") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.17") (d #t) (k 0)))) (h "1afsci9wmbnz909valg3jkp7yd9kv00dq0qj0mfilvw3x0isa940")))

(define-public crate-rnix-0.11.0 (c (n "rnix") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "rowan") (r "^0.15.0") (d #t) (k 0)))) (h "0pybq9gp4b7lp0066236jpqi9lgb1bzvqc9axymwrq3hxgdwwddv")))

