(define-module (crates-io rn pm rnpmrc) #:use-module (crates-io))

(define-public crate-rnpmrc-0.1.0 (c (n "rnpmrc") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "18qc039yckja7wz0sazz7cvplylgx68xlsvjhihxazy5igxqxdf6")))

(define-public crate-rnpmrc-0.2.0 (c (n "rnpmrc") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1p3dal841pbc9kssn4hbslnwfkgndvpkjwbbxy7phmh62ykj7z2p")))

(define-public crate-rnpmrc-0.3.0 (c (n "rnpmrc") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "06a6pccqa1am93g4iqnn3nidfm6zkwjh4mi16x1l5qlyg46sqi1s")))

(define-public crate-rnpmrc-0.4.0 (c (n "rnpmrc") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "12ixywz3lrlsj9426cmzbgrabjfk8fwppp57xpvj1vdgcck491ja")))

