(define-module (crates-io rn g- rng-cli) #:use-module (crates-io))

(define-public crate-rng-cli-0.1.0 (c (n "rng-cli") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.0") (d #t) (k 0)) (d (n "rand_hc") (r "^0.2.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1ydfrqvmr3g0fmh5p0qqynih21hj2az0a4myhraqiyhcgp7k4vfz")))

(define-public crate-rng-cli-0.2.0 (c (n "rng-cli") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.0") (d #t) (k 0)) (d (n "rand_hc") (r "^0.2.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "18a5k021c6jl0wnr6gldpmn5j7qblw8dx9nvpmp497v59gyw1xk3")))

