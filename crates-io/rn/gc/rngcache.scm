(define-module (crates-io rn gc rngcache) #:use-module (crates-io))

(define-public crate-rngcache-0.1.0 (c (n "rngcache") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1vccsqi08nfrssbvdma8p2kia8bk95zp26fl3qv50pdrrnai4pys")))

(define-public crate-rngcache-0.1.1 (c (n "rngcache") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "150d3kwxah3gf797ynihza44zkkd9jcnahhn9dldh2s7jiwbhvbx")))

(define-public crate-rngcache-0.1.2 (c (n "rngcache") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "007dqwsa45g71grv3fyaw89grgvp2vxkh562icsshcg6w16bgd9f")))

(define-public crate-rngcache-0.1.3 (c (n "rngcache") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0lcarhmh6jblsrdc2b8k4mrdmlg4wpjw5sqmy5c7yv41f2cf0279")))

