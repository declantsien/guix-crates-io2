(define-module (crates-io rn gc rngcheck) #:use-module (crates-io))

(define-public crate-rngcheck-0.1.0 (c (n "rngcheck") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (k 0)) (d (n "libm") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (d #t) (k 2)))) (h "03ra1i51hmph0l9nl5cpdzngnsgwagasa94jzxzx90k4q9cxrg6z")))

(define-public crate-rngcheck-0.1.1 (c (n "rngcheck") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 2)) (d (n "libm") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (d #t) (k 2)))) (h "00sjdcs3mj0rlfcihfqsky4x101gbxsyl361qvvhzsv6059sw28y")))

