(define-module (crates-io rn rm rnrm) #:use-module (crates-io))

(define-public crate-rnrm-0.1.0 (c (n "rnrm") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dyl06602yklahsy1r4iflx7fg763x3ic7j3awc297rr1621l0v8")))

(define-public crate-rnrm-0.1.1 (c (n "rnrm") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "open") (r "^2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "1pvpfihgvn0sjb444gjb3iskw4myw21k8068grbb9rzjpack6rjw")))

(define-public crate-rnrm-0.1.2 (c (n "rnrm") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "open") (r "^2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "05nd3imrx1n1kals3kgzgly7iyj7ay6hxc9gl451d885almsnphp")))

(define-public crate-rnrm-0.1.3 (c (n "rnrm") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "open") (r "^2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "0xfnga04cnficdhj46sd0m489jq2yfl01dmg75i3hf8s87va93g6")))

(define-public crate-rnrm-0.1.4 (c (n "rnrm") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "open") (r "^2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "1xz0r16b8q4vn3rgan6zzxg73pa02msq2k0xhv9mlpp2rbnz8ps5")))

(define-public crate-rnrm-0.1.5 (c (n "rnrm") (v "0.1.5") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "open") (r "^2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "0ny0mqszvdw05ygyzbryc756x2ryk1vwbn10j0fm0y3m3s6l4zyx")))

(define-public crate-rnrm-0.1.6 (c (n "rnrm") (v "0.1.6") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "open") (r "^2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "1prnds21dypap28vkh6d0mwbcpskgbdhgq5kls2g8zz6z0kwvn1d")))

(define-public crate-rnrm-0.1.7 (c (n "rnrm") (v "0.1.7") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "open") (r "^2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "11dr8cywy1cn5m7951v2xwf66g3pk60x2678nqfxcl31592nsh84")))

(define-public crate-rnrm-0.1.8 (c (n "rnrm") (v "0.1.8") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "open") (r "^2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "1krvr4aq20mscqcrcc39fbb6k9lxc479sxdq5hh0cfnmnsf7v987")))

