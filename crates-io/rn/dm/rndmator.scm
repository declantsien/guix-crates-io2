(define-module (crates-io rn dm rndmator) #:use-module (crates-io))

(define-public crate-rndmator-0.1.0 (c (n "rndmator") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1507vhg3niyp2wvcvxhqdk6pzlk1bggsqhyyvfdqy2v56zdsdr26")))

