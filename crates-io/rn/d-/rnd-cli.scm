(define-module (crates-io rn d- rnd-cli) #:use-module (crates-io))

(define-public crate-rnd-cli-0.1.0 (c (n "rnd-cli") (v "0.1.0") (d (list (d (n "filename") (r "^0.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.5") (d #t) (k 0)))) (h "1wmnc1q1y8bv7cc42jqw99lgvjyi2fmyjbxbksslbyp08rq1xwic")))

(define-public crate-rnd-cli-0.1.1 (c (n "rnd-cli") (v "0.1.1") (d (list (d (n "filename") (r "^0.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.5") (d #t) (k 0)))) (h "0bvswa3w13wifswl4hjgafiqvm10k0vsq9vqbd3v1gzh5271dzhi")))

(define-public crate-rnd-cli-0.1.2 (c (n "rnd-cli") (v "0.1.2") (d (list (d (n "filename") (r "^0.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.5") (d #t) (k 0)))) (h "107xhgwfm0lszzsmnyhb900399z3awclv3z8ysqsypl7lj1kl5qb")))

