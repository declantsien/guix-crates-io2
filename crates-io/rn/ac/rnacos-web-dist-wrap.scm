(define-module (crates-io rn ac rnacos-web-dist-wrap) #:use-module (crates-io))

(define-public crate-rnacos-web-dist-wrap-0.1.0 (c (n "rnacos-web-dist-wrap") (v "0.1.0") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1z8y1j0zfvp22kpz49wwaz30wp4ycjdjbck62l5gvjb3cx6f4ajq")))

(define-public crate-rnacos-web-dist-wrap-0.1.3 (c (n "rnacos-web-dist-wrap") (v "0.1.3") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0ynrl1yqv0x2z0c0ffm83lcvwmm88k5zhws6l6ihp0dxahhp6sql")))

(define-public crate-rnacos-web-dist-wrap-0.1.4 (c (n "rnacos-web-dist-wrap") (v "0.1.4") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "16hc22yr8jmy0g0c9v28krh7lhjk56qh7nm57hds4qxim451cqs5")))

(define-public crate-rnacos-web-dist-wrap-0.1.5 (c (n "rnacos-web-dist-wrap") (v "0.1.5") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "01bl2xgilis9f8sfb2dszswnli9whqhxf1p0agpabknsi1fc46s9")))

(define-public crate-rnacos-web-dist-wrap-0.1.6 (c (n "rnacos-web-dist-wrap") (v "0.1.6") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1q17q59r05imi36429hqvw70bx745z5j41b8vjh4v66xaldx25mv")))

(define-public crate-rnacos-web-dist-wrap-0.1.9 (c (n "rnacos-web-dist-wrap") (v "0.1.9") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1r68wnka65gkapcx8cg11khcwzm7m0rh9wv70akcv6q2vywcz6wf")))

(define-public crate-rnacos-web-dist-wrap-0.2.0 (c (n "rnacos-web-dist-wrap") (v "0.2.0") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0prvrl3yhwzjyp16qqy9163vxk3ss1fhikhlg85i17pcjdv3a8i7")))

(define-public crate-rnacos-web-dist-wrap-0.2.1 (c (n "rnacos-web-dist-wrap") (v "0.2.1") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1nwi74rqh62r07dhr61c6pvhg63c2p1brhaq9kmnlyp0q4yymns0")))

(define-public crate-rnacos-web-dist-wrap-0.1.10 (c (n "rnacos-web-dist-wrap") (v "0.1.10") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0q59wl4yy2jvg81wwpszgqxxak5p6xyjqf4zcx6md8ml8jd702fd")))

(define-public crate-rnacos-web-dist-wrap-0.2.2 (c (n "rnacos-web-dist-wrap") (v "0.2.2") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "16j5rr4a4m3x5f9vmfs4977i2cxg709sqw2wfjx18piw8brrpcc5")))

(define-public crate-rnacos-web-dist-wrap-0.2.3-beta.1 (c (n "rnacos-web-dist-wrap") (v "0.2.3-beta.1") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0wxw07456rdv4vfq5ld4qkhf7h0mhvxh7mljk8xgd636q2hllqqx")))

(define-public crate-rnacos-web-dist-wrap-0.2.3-beta.2 (c (n "rnacos-web-dist-wrap") (v "0.2.3-beta.2") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0l2946cvd0m30lrcrjds2a23q0l06hsv36a7z6spkf7vsrqi6gw0")))

(define-public crate-rnacos-web-dist-wrap-0.2.3-beta.3 (c (n "rnacos-web-dist-wrap") (v "0.2.3-beta.3") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "07w3dxg360y7kvyj747z6mszrnrkz390hg3sr0sw87p26vpzjflz")))

(define-public crate-rnacos-web-dist-wrap-0.2.3-beta.4 (c (n "rnacos-web-dist-wrap") (v "0.2.3-beta.4") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0qg7r66bg52lfx0acyj7khgzyl9c2p28gvk99cqj5a1mci38q1yv")))

(define-public crate-rnacos-web-dist-wrap-0.2.3-beta.5 (c (n "rnacos-web-dist-wrap") (v "0.2.3-beta.5") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1kn7k1imkkymz6qbn0gbygngvx2gsqif8nyvq2dniswhbhqd75w9")))

(define-public crate-rnacos-web-dist-wrap-0.2.3-beta.6 (c (n "rnacos-web-dist-wrap") (v "0.2.3-beta.6") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "10v8hl617diclx77cdysyliax52fj6dqm3n0nvls7dfg7gcji6ip")))

(define-public crate-rnacos-web-dist-wrap-0.2.3-beta.7 (c (n "rnacos-web-dist-wrap") (v "0.2.3-beta.7") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1h3s9yjk8smlm70rx1saqq7q3z0jn0qgc22frsv04wn7mmmdzrin")))

(define-public crate-rnacos-web-dist-wrap-0.3.0 (c (n "rnacos-web-dist-wrap") (v "0.3.0") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0pxqn5xspxqbf99jja498r1i3l4s1hm4322g0l6dd5ndsx4idplb")))

(define-public crate-rnacos-web-dist-wrap-0.3.1 (c (n "rnacos-web-dist-wrap") (v "0.3.1") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1fmpyz5mdzh5hyzvzyajj1jb0qhi7ksz00s64z11cw9yz2gm47gs")))

(define-public crate-rnacos-web-dist-wrap-0.3.2 (c (n "rnacos-web-dist-wrap") (v "0.3.2") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0ar3hxvk6g1iri5g75fh6xsyc6r4xkq1376vfmgjdimvbmakrggk")))

(define-public crate-rnacos-web-dist-wrap-0.3.3 (c (n "rnacos-web-dist-wrap") (v "0.3.3") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0jhba59yck31637c2nbsnj2al8cx14pwrwi6yns57q99c6anx1za")))

(define-public crate-rnacos-web-dist-wrap-0.3.4 (c (n "rnacos-web-dist-wrap") (v "0.3.4") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "14s95r9lr7y5yw1n58hkfvldgkvzzpycbbxj0j9wa0qbi8diqx82")))

(define-public crate-rnacos-web-dist-wrap-0.3.5 (c (n "rnacos-web-dist-wrap") (v "0.3.5") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1cni3kp1b3sx3gv880dlhkp3f7jnigvk6yz83brjyfamwi4v03k2")))

(define-public crate-rnacos-web-dist-wrap-0.3.6 (c (n "rnacos-web-dist-wrap") (v "0.3.6") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1cgc72m6jxlqrvph9vicnrpnzszabpfyhy4yrxhx3yfylnwsnggb")))

(define-public crate-rnacos-web-dist-wrap-0.3.7 (c (n "rnacos-web-dist-wrap") (v "0.3.7") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0aphf7xj22h9khg0axl5zzhz287gl80qmscbgr05jf13lbavqsrk")))

(define-public crate-rnacos-web-dist-wrap-0.3.8 (c (n "rnacos-web-dist-wrap") (v "0.3.8") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "01bq8w8hgpp0qmgknrhh9gikf2f6g0w6qmb4lrwdq8cnij77ig1q")))

(define-public crate-rnacos-web-dist-wrap-0.3.9 (c (n "rnacos-web-dist-wrap") (v "0.3.9") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "198ajjbk7cjffv6054xkgp75rj92w4k4xxc4s580k58g35yrj4db")))

(define-public crate-rnacos-web-dist-wrap-0.3.10 (c (n "rnacos-web-dist-wrap") (v "0.3.10") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1ny0xd6b7sqi64rj0kl428q15cw3g4sr6ssylfa1wvsqg1wxyiai")))

(define-public crate-rnacos-web-dist-wrap-0.3.11 (c (n "rnacos-web-dist-wrap") (v "0.3.11") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1bikpwabq2miy6pj8gry7d107hlw0sihqxb8pkavd2g069x2wf2y")))

(define-public crate-rnacos-web-dist-wrap-0.3.12 (c (n "rnacos-web-dist-wrap") (v "0.3.12") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1dawl9xir33hd8i2ig6k0k5ii17wpcpm85s66nhw39g58a4yvk03")))

(define-public crate-rnacos-web-dist-wrap-0.4.0-beta.1 (c (n "rnacos-web-dist-wrap") (v "0.4.0-beta.1") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0fcqic9nb55kmcv08jy6i3qchk3fgrdw8d4fn0nwhkjpg8jix7m8")))

(define-public crate-rnacos-web-dist-wrap-0.4.0-beta.2 (c (n "rnacos-web-dist-wrap") (v "0.4.0-beta.2") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "01yz48pzay9g461adjin1zhlchffc1sxhmy2l63c1w3xbbwxgxny")))

(define-public crate-rnacos-web-dist-wrap-0.4.0-beta.4 (c (n "rnacos-web-dist-wrap") (v "0.4.0-beta.4") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "167xhvl2yljipfhmasyhq6p5lwccxg5xgi3qzbv9ynzqym9h3k9w")))

(define-public crate-rnacos-web-dist-wrap-0.4.0-beta.5 (c (n "rnacos-web-dist-wrap") (v "0.4.0-beta.5") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "0pmpkmha2qb87g61sgib3rw7mm9q2rhxgs9fxbjf9a46446zw3m4")))

(define-public crate-rnacos-web-dist-wrap-0.4.0 (c (n "rnacos-web-dist-wrap") (v "0.4.0") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "058lihysqjrxihamyz351f1bn15sifsavhi8fvc1fzvmkr3w62hf")))

(define-public crate-rnacos-web-dist-wrap-0.4.1 (c (n "rnacos-web-dist-wrap") (v "0.4.1") (d (list (d (n "rust-embed") (r "^6.6.1") (d #t) (k 0)))) (h "1krr610nn88ixakb09mz976k27226g70di2aw51gbi13d2vvvrdb")))

