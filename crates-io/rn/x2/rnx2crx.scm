(define-module (crates-io rn x2 rnx2crx) #:use-module (crates-io))

(define-public crate-rnx2crx-0.0.1 (c (n "rnx2crx") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "rinex") (r "^0.6.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0j963vyz0g9372120q6xlhs30q670zaddi033rr98yj7gvh4fbqy")))

(define-public crate-rnx2crx-1.1.0 (c (n "rnx2crx") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "^0.10.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1raqlck0zr6bxn0vf0qrgj0axds6q7rwjsny7znl76mm9y0ymwfj")))

(define-public crate-rnx2crx-1.1.1 (c (n "rnx2crx") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mch6qmg2afv11r5l56mrb1yn92ybpymq92hkinga5qxh8qcxcx8")))

(define-public crate-rnx2crx-1.1.2 (c (n "rnx2crx") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13npgdy5mc9jhp180lsdpigqr2xfbw36qfxghrk71n7skd6myxdf")))

(define-public crate-rnx2crx-1.1.3 (c (n "rnx2crx") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bwqrkvyh18xnbh75qrzidlnz2q7bv3x7q99iyjvd83aharm5wis")))

(define-public crate-rnx2crx-1.2.0 (c (n "rnx2crx") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00sqz277fi3ra0vd8687vapmmx65xq3zw6rr3bcx39fabqcb6kc0")))

(define-public crate-rnx2crx-1.2.1 (c (n "rnx2crx") (v "1.2.1") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fb0lcqvy4phld3cj3ggswv1zy3992im9psixwaw12kk15y6mnmj")))

(define-public crate-rnx2crx-1.2.3 (c (n "rnx2crx") (v "1.2.3") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.16.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09vafq5idyf455kd4xxkkdi6g8rszc2vl7x8lf7cja24q066mvyh")))

(define-public crate-rnx2crx-1.2.4 (c (n "rnx2crx") (v "1.2.4") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.16.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ciy8hy0hvhn438vakij0hva7dgr3yr61lvnf8y9jm94aqrvmcrl")))

