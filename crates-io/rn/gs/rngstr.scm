(define-module (crates-io rn gs rngstr) #:use-module (crates-io))

(define-public crate-rngstr-0.1.0 (c (n "rngstr") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m7fr13rz2dm93cjn0p8myghy1xr64nj16l2jwl46g978f8gwgf5")))

(define-public crate-rngstr-0.1.1 (c (n "rngstr") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1irrssahikp1zjhzq49k4qlc9f5vh9s1j2nvmb78s654p8g5yal3")))

(define-public crate-rngstr-0.2.0 (c (n "rngstr") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1zf292gahli5p70g9fp6kr77w52ngs0nwhyzf10nziia893aimsn")))

