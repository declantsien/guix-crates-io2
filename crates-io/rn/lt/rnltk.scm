(define-module (crates-io rn lt rnltk) #:use-module (crates-io))

(define-public crate-rnltk-0.1.0 (c (n "rnltk") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "15jrfnn57pi6pprq3n11b383jjslpb83bpgpc5j3bdw8000jg2zs")))

(define-public crate-rnltk-0.1.1 (c (n "rnltk") (v "0.1.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "18j2538h73i9md857kbp1mmmzkp47s5k7bjfpmyw76f2n657327h")))

(define-public crate-rnltk-0.1.2 (c (n "rnltk") (v "0.1.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0kfhszn86psg1v215h1r6fhyan8hmy956bz27lfkmjxwmnpna61f")))

(define-public crate-rnltk-0.1.3 (c (n "rnltk") (v "0.1.3") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0jg5jz0jhzspwdv3qa24l1yqvhd0pfpmf7kcm5gxkf6xxxb9574f")))

(define-public crate-rnltk-0.1.4 (c (n "rnltk") (v "0.1.4") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1qljajsv6g576psb07nqxvnmc50d05d3x49ddn8h59i3s3759z48")))

(define-public crate-rnltk-0.1.5 (c (n "rnltk") (v "0.1.5") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1a6psmk4k7zqjddmql8id8pqvy5pv88pplfizkddnb7pngfvprqa")))

(define-public crate-rnltk-0.2.0 (c (n "rnltk") (v "0.2.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0phzp60fcjydfslps2119xbzaq4ww44bszq0jiwr3c0gimvj2v0k")))

(define-public crate-rnltk-0.3.0 (c (n "rnltk") (v "0.3.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0gds2f6x83fmj4pgp2n6y17i0vi7rpa8i9hsvrba4z8j55ccx0iv")))

(define-public crate-rnltk-0.4.0 (c (n "rnltk") (v "0.4.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1gwkyvwdyc6bq85qn8165llyy804lrqgh8c4hnd7iykkqcdvn5d4")))

