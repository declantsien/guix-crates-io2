(define-module (crates-io rn -x rn-xlsx2csv) #:use-module (crates-io))

(define-public crate-rn-xlsx2csv-0.1.0 (c (n "rn-xlsx2csv") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1kbc5y62w07iwmm7gjny1jmz1lfvb6fb6d8qd8iqs255lbxwnqhq")))

(define-public crate-rn-xlsx2csv-0.1.1 (c (n "rn-xlsx2csv") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "017qsjrlpcqfzaz04ydll266zmxg3n3nywxqcglzh2wdxrmclj9i")))

(define-public crate-rn-xlsx2csv-0.2.0 (c (n "rn-xlsx2csv") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0mgrvbiwzn549z8dccm768z24hwdmzfiwnp2z0g9q6j9i72w71av")))

