(define-module (crates-io rn a- rna-ss-params) #:use-module (crates-io))

(define-public crate-rna-ss-params-0.1.0 (c (n "rna-ss-params") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1knn904m2j9c3sixdg1dcvi5zf3zynxi83lsp8kailmy2qwd9ksk")))

(define-public crate-rna-ss-params-0.1.1 (c (n "rna-ss-params") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0v77d5ag113j6zgyfx63fa021v0kzaiqmkdcpjsiibyh1d2c2174")))

(define-public crate-rna-ss-params-0.1.2 (c (n "rna-ss-params") (v "0.1.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0qdrhcp15149vbwxxh0hf9a5pq02z99hk0c430lh9lf8cxvlch4w")))

(define-public crate-rna-ss-params-0.1.3 (c (n "rna-ss-params") (v "0.1.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "03v1fwircbhx8yw6xlzpiy59wh7wlv0ilq4c13ap467f90bx3wwz")))

(define-public crate-rna-ss-params-0.1.4 (c (n "rna-ss-params") (v "0.1.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0ng6sfm29aqhl1528v10rvrrqrx02ad8ph4llrwhncv9k2dqk21c")))

(define-public crate-rna-ss-params-0.1.5 (c (n "rna-ss-params") (v "0.1.5") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1v7vfr6p3bbjy5raxqbdn8pwbmjc7wx8j0g6rjpwihr7z8dxdk5k")))

(define-public crate-rna-ss-params-0.1.6 (c (n "rna-ss-params") (v "0.1.6") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1x4niv235y3na08nf79avxj96ymcyj6xwqmx3b71llqqgnf0yg84")))

(define-public crate-rna-ss-params-0.1.7 (c (n "rna-ss-params") (v "0.1.7") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "04snx63wy6c6hljglsbkv1r0z2wir6cxsrbn127dx6dmfam9wq5k")))

(define-public crate-rna-ss-params-0.1.8 (c (n "rna-ss-params") (v "0.1.8") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0zsjxhyn1zni3ynnnnpnixkjz32nvsh9j5qr089mv9cch3aajmj0")))

(define-public crate-rna-ss-params-0.1.9 (c (n "rna-ss-params") (v "0.1.9") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1i3sdmb8gqa5b35cz5hcpxxahb1v4fbadkf2ah2i8360hvns9shv")))

(define-public crate-rna-ss-params-0.1.10 (c (n "rna-ss-params") (v "0.1.10") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1rxafprc0j0j1niinah2l72v38df0iah5jb7q618kjy0mzg5rahc")))

(define-public crate-rna-ss-params-0.1.11 (c (n "rna-ss-params") (v "0.1.11") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0cz6ibpwapgn0swdqg5gc3gm95lls5fn2x7yri3qlgyzjv9sfdia")))

(define-public crate-rna-ss-params-0.1.12 (c (n "rna-ss-params") (v "0.1.12") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1i9yxkq6xsgljspqzki3n2m3js469wmhc6clbzpdngzsj03qk74m")))

(define-public crate-rna-ss-params-0.1.13 (c (n "rna-ss-params") (v "0.1.13") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0p9663iw2kb26ajlnpzkcgrbm80fz9gkwsjh6q8nhzhxx7dh1v52")))

(define-public crate-rna-ss-params-0.1.14 (c (n "rna-ss-params") (v "0.1.14") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0v9rc2cl9rc347pygqxr67mvh6pwhm0x9qiwmx4f6w5m6zy3ml3y")))

(define-public crate-rna-ss-params-0.1.15 (c (n "rna-ss-params") (v "0.1.15") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1xdvl910qib4c0fwy5z04bmpmrfniza4fi9qyskficzqixz3sxlp")))

(define-public crate-rna-ss-params-0.1.16 (c (n "rna-ss-params") (v "0.1.16") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0wfw9s1q0av3f93vrg6hxz5sas8nlaw1byska690mjh2nlxmwhnx")))

(define-public crate-rna-ss-params-0.1.17 (c (n "rna-ss-params") (v "0.1.17") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "12sblwybckzk4ay51jv9xh4c6rz0x2r58fkkr5wnb7z65yn59y77")))

(define-public crate-rna-ss-params-0.1.18 (c (n "rna-ss-params") (v "0.1.18") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1dq2mixw2h4jkzx0a94cjc0hmiwfz7had1r7g0y2k8mhi6yxjllj")))

(define-public crate-rna-ss-params-0.1.19 (c (n "rna-ss-params") (v "0.1.19") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0857hgs6gx7sjnrvm0zy5i2iihrx8nwdi2zxxpm3aa8299kk0zfa")))

(define-public crate-rna-ss-params-0.1.20 (c (n "rna-ss-params") (v "0.1.20") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0k30hrjzbfqh379nnivw8izvizjdpq4yb9zfjjqfr2i9pxinl2vv")))

(define-public crate-rna-ss-params-0.1.21 (c (n "rna-ss-params") (v "0.1.21") (d (list (d (n "bio-seq-algos") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1fsc6ih6rgbc71i204zqfylsk5qlyas0q2h7b985vcr2prfwibsy")))

(define-public crate-rna-ss-params-0.1.22 (c (n "rna-ss-params") (v "0.1.22") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0gb2y2pwl5nkfbngk4k0cv26z6f0jk9cbambw9b3yi9h33kslxgp")))

(define-public crate-rna-ss-params-0.1.23 (c (n "rna-ss-params") (v "0.1.23") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0m76v5i0jin428n538rdqf1sk8c63kqwk3yknp021l3rmb1d24yx")))

(define-public crate-rna-ss-params-0.1.24 (c (n "rna-ss-params") (v "0.1.24") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0sgyvaxdlhlm9f9x9645zpj0i5d2l9zpn6vr5001jmyyssj9xcgj")))

