(define-module (crates-io rn -r rn-run) #:use-module (crates-io))

(define-public crate-rn-run-0.1.0 (c (n "rn-run") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rkc6gkl1815f8hn41x5m1g7623k25x2hc8cvki9bvr2w2b2pw7h")))

(define-public crate-rn-run-0.1.1 (c (n "rn-run") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "007wiyh38862j4k62ghqxsz9np5ic7fabdnd2q2n3fk8n0ly0ykn")))

