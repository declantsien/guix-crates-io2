(define-module (crates-io rn af rnafamprob) #:use-module (crates-io))

(define-public crate-rnafamprob-0.1.0 (c (n "rnafamprob") (v "0.1.0") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rna-algos") (r "^0.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "17li52mk3d1y6h6s846b4jkf5lyc49w5yhp2dvar8nn35mpa8aas")))

