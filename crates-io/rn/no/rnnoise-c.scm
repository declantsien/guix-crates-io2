(define-module (crates-io rn no rnnoise-c) #:use-module (crates-io))

(define-public crate-rnnoise-c-0.1.0 (c (n "rnnoise-c") (v "0.1.0") (d (list (d (n "rnnoise-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1khhnbb0lqlr0v6r992ms8qnnly33k7dxlkxsd253mv98hw6hdb1")))

(define-public crate-rnnoise-c-0.2.0 (c (n "rnnoise-c") (v "0.2.0") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "rnnoise-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 2)))) (h "1dmdyjdvcg29bmpxsnbys39jx2ilmqgm8k7hzpdl3pmazkk0pizg")))

(define-public crate-rnnoise-c-0.2.1 (c (n "rnnoise-c") (v "0.2.1") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "rnnoise-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 2)))) (h "0mdd93v6sh253mxkxh1vj87ajwr81sgq750110v7ckfmqh6xza9p")))

