(define-module (crates-io rn no rnnoise-sys) #:use-module (crates-io))

(define-public crate-rnnoise-sys-0.1.0 (c (n "rnnoise-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (k 1)))) (h "01k3nl21n7x7dmxidclm5i0k1a22bdc9y5xg1605xrqp7wfjdd8g")))

(define-public crate-rnnoise-sys-0.1.1 (c (n "rnnoise-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (k 1)))) (h "1a8w0jxdhyyf4vwyj2zvq70qh296qaaaaasa03f4py0nn0cc5am5")))

(define-public crate-rnnoise-sys-0.1.2 (c (n "rnnoise-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (k 1)))) (h "1aid5m22kdfv9yh3a12z2bpmg0n6jp9skwlvkhvh3pc97mzql5j4")))

(define-public crate-rnnoise-sys-0.1.3 (c (n "rnnoise-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (k 1)))) (h "00nrbrgvs7ij6c20hcwlgcj4pb5jknkn16pxcav738qgc80wh7zq")))

