(define-module (crates-io rn et rnet) #:use-module (crates-io))

(define-public crate-rnet-0.1.0 (c (n "rnet") (v "0.1.0") (d (list (d (n "linkme") (r "^0.2.7") (d #t) (k 0)) (d (n "rnet-macros") (r "^0.1.0") (d #t) (k 0)))) (h "06sd3cs9nk6m3vcpsxgkqng41fxisy6vhm1w2djr02mrv476mygg")))

(define-public crate-rnet-0.2.0 (c (n "rnet") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "linkme") (r "^0.2.7") (d #t) (k 0)) (d (n "rnet-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "0vg5gg744kjb45lfqp3md2x5ynhggfl12zfw5a46z7zhkm286mn2")))

(define-public crate-rnet-0.3.0 (c (n "rnet") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "linkme") (r "^0.2.7") (d #t) (k 0)) (d (n "rnet-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "0i271ni8nnmhp6bxzg0r1yb6km6lwxpjlj2xl3bz0rrbj0sj1790")))

(define-public crate-rnet-0.3.1 (c (n "rnet") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "linkme") (r "^0.2.7") (d #t) (k 0)) (d (n "rnet-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "02g4ph21lbp9kzqfl4k8pvq1m2iz252khczdhasn2ppfr2ivy4dz")))

