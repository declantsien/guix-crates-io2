(define-module (crates-io rn et rnet-macros) #:use-module (crates-io))

(define-public crate-rnet-macros-0.1.0 (c (n "rnet-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "194a2aax38s2yqcnxl8brl5ymcn814amg29mldymf7pfwvw3j928")))

(define-public crate-rnet-macros-0.2.0 (c (n "rnet-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "173fhclhkry1s03550629rxc6znmbmqgkm36xh2ac7f1b902mn8k")))

(define-public crate-rnet-macros-0.3.0 (c (n "rnet-macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1nd7dq42mjbfgpahnsrb2wlr438fx5k2fxn14filpa72jbsg96sl")))

(define-public crate-rnet-macros-0.3.1 (c (n "rnet-macros") (v "0.3.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "06nj7c8ixcjgkcw4f6gdb342h6hms41p0b7qraysr6g9rvsrv0a4")))

