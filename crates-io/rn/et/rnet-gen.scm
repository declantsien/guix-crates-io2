(define-module (crates-io rn et rnet-gen) #:use-module (crates-io))

(define-public crate-rnet-gen-0.1.0 (c (n "rnet-gen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "rnet") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dsf753arjzz8qjfxabcr4y9ld399i7rxfmhc5bma04y89nmamrs")))

(define-public crate-rnet-gen-0.2.0 (c (n "rnet-gen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "rnet") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0hsm2nqrcqqp3rxf55mxc9j9jlb56plycl0in5gssqnyghffz3s5")))

(define-public crate-rnet-gen-0.3.0 (c (n "rnet-gen") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "rnet") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "12vlivvgg8wbll7hq5zkf3dfkr1mnjy1wgddd7qw68sj463cf4pg")))

(define-public crate-rnet-gen-0.3.1 (c (n "rnet-gen") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "rnet") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "03j38w27wx4nb36xbrvg01a693x19ahvn9ghrmjxpwkqmsbwwaki")))

